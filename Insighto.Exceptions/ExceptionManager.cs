﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.Security;
using System.Diagnostics;
using System.Reflection;
using Microsoft.Win32;


namespace Insighto.Exceptions
{
    public class ExceptionManager
    {
        #region Variable Declarations
        private const string ERROR_REDIRECT_PAGE_NAME = "Exception.aspx";
        public const string END_USER_EXCEPTION_MESSAGE = "ExceptionPublished";
        #endregion

        #region Constants
        public const string NAME_SPACE = "Insighto";
        #endregion


        #region Private Methods
        #region GetMethodFileNames
        /// <summary>
        /// Preparing class and method names from start to end where exception raised.
        /// </summary>
        /// <returns>StringBuilder contains class and method names</returns>
        private static StringBuilder GetMethodFileNames()
        {
            int stackTraceFramesCount = 0;
            StringBuilder objMethodFileNames = null;
            StackTrace objStackTrace = null;
            StackFrame objStackFrame = null;
            MethodBase objMethodBase = null;

            objStackTrace = new StackTrace();
            objMethodFileNames = new StringBuilder();

            for (stackTraceFramesCount = 1; stackTraceFramesCount < objStackTrace.FrameCount; stackTraceFramesCount++)
            {
                objStackFrame = objStackTrace.GetFrame(stackTraceFramesCount);
                objMethodBase = objStackFrame.GetMethod();

                if (objMethodBase.DeclaringType.Namespace == null)
                {
                    objMethodFileNames.AppendLine("Class Name: " + objMethodBase.DeclaringType.FullName + ", MehtodName: " + objMethodBase.Name);
                }
                else if ((objMethodBase.DeclaringType.Namespace.ToUpper().IndexOf(NAME_SPACE.ToUpper()) == 0 || objMethodBase.DeclaringType.Namespace.Trim() == "") && (objMethodBase.DeclaringType.FullName.ToUpper() != "Insighto.EXCEPTIONHANDLER.ExceptionManager".ToUpper()))
                {
                    objMethodFileNames.AppendLine("Class Name: " + objMethodBase.DeclaringType.FullName + ", MehtodName: " + objMethodBase.Name);
                }
            }

            return objMethodFileNames;
        }
        #endregion
        #endregion

        #region Methods
        #region Publish Exception
        /// <summary>
        /// Formating exception message to write into log files.
        /// </summary>
        /// <param name="exception">Exception exception</param>
        /// <return>No returns</return>
        public static void PublishException(Exception exception)
        {
            StringBuilder exceptionMessage = null;
            try
            {
                if (exception.Message.ToUpper() != "Exception of type 'System.Exception' was thrown.")
                {
                    exceptionMessage = GetMethodFileNames();

                    //Formatting error message to write into system event log
                    exceptionMessage.AppendLine("Exception Message: " + exception.Message);
                    exceptionMessage.AppendLine("Exception Source: " + exception.Source);
                    if (exception.TargetSite != null)
                    {
                        exceptionMessage.AppendLine("Exception TargetSite: " + exception.TargetSite.Name);
                    }

                    if (exception.InnerException != null)
                    {
                        exceptionMessage.AppendLine("Inner Exception: " + exception.InnerException.Message);
                    }

                    if (exception.StackTrace != null)
                    {
                        exceptionMessage.AppendLine("Exception StackTrace: " + exception.StackTrace);
                    }

                    WriteMessageToEventLog(exceptionMessage.ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Data[END_USER_EXCEPTION_MESSAGE] == null)
                {
                    ex.Data.Add(END_USER_EXCEPTION_MESSAGE, true);
                }
                throw ex;
            }
        }
        #endregion

        #region Publish Exception
        /// <summary>
        /// Formating exception message to write into log files.
        /// </summary>
        /// <param name="exception">Exception exception</param>
        /// <return>No returns</return>
        public static void PublishException(Exception exception, string sqlCommandText)
        {
            StringBuilder exceptionMessage = null;
            try
            {
                //Formatting error message to write into system event log
                exceptionMessage = GetMethodFileNames();
                exceptionMessage.AppendLine("Sql Command Text: " + sqlCommandText);
                exceptionMessage.AppendLine("Exception Message: " + exception.Message);
                exceptionMessage.AppendLine("Exception Source: " + exception.Source);
                if (exception.TargetSite != null)
                {
                    exceptionMessage.AppendLine("Exception TargetSite: " + exception.TargetSite.Name);
                }

                if (exception.InnerException != null)
                {
                    exceptionMessage.AppendLine("Inner Exception: " + exception.InnerException.Message);
                }

                if (exception.StackTrace != null)
                {
                    exceptionMessage.AppendLine("Exception StackTrace: " + exception.StackTrace);
                }

                WriteMessageToEventLog(exceptionMessage.ToString());
            }
            catch (Exception ex)
            {
                if (ex.Data[END_USER_EXCEPTION_MESSAGE] == null)
                {
                    ex.Data.Add(END_USER_EXCEPTION_MESSAGE, true);
                }
                throw ex;
            }
        }
        #endregion

        #region Write Message To Log
        /// <summary>
        /// Writing message into system event log
        /// </summary>
        /// <param name="EventLogMessage">string EventLogMessage</param>
        /// <param name="eventLogEntryType">EventLogEntryType eventLogEntryType</param>
        /// <return>No returns</return>
        public static void WriteMessageToEventLog(string exceptionLogMessage)
        {
            string exceptionLogFilePath = HttpContext.Current.Server.MapPath("~/ExceptionLogFiles/ExceptionLog.txt");

            StreamWriter objStreamWriter = new StreamWriter(exceptionLogFilePath, true);
            objStreamWriter.WriteLine(DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss"));
            objStreamWriter.WriteLine(exceptionLogMessage);
            objStreamWriter.WriteLine("******************************************************************************************************************");
            objStreamWriter.Flush();
            objStreamWriter.Dispose();
            objStreamWriter.Close();
            objStreamWriter = null;
        }
        #endregion
        #endregion
    }
}
