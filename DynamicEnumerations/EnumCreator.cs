﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Reflection;
using System.Reflection.Emit;
using Insighto.Data;
using System.Linq;

namespace DynamicEnums
{
    public class EnumCreator
    {
        // after running for first time rename this method to Main1
        static void Main(string[] args)
        {
            string strAssemblyName = "DynamicEnums";
            bool flagFileExists = System.IO.File.Exists(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + strAssemblyName + ".dll");

            // Get the current application domain for the current thread
            AppDomain currentDomain = AppDomain.CurrentDomain;

            // Create a dynamic assembly in the current application domain, and allow it to be executed and saved to disk.
            AssemblyName name = new AssemblyName(strAssemblyName);
            AssemblyBuilder assemblyBuilder = currentDomain.DefineDynamicAssembly(name, AssemblyBuilderAccess.RunAndSave);

            // Define a dynamic module in "MyEnums" assembly. For a single-module assembly, the module has the same name as the assembly.
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule(name.Name, name.Name + ".dll");

            #region GetTheDataFromTheDatabase

            List<string> featureGroups = new List<string>();
            List<osm_features> lstFeatures = new List<osm_features>();
            using (InsightoV2Entities db2 = new InsightoV2Entities())
            {
                lstFeatures = db2.osm_features.Where(f => f.DELETED == 0).ToList();
            }

            #endregion GetTheDataFromTheDatabase

            featureGroups = lstFeatures.Select(f => f.FEATURE_GROUP).Distinct().ToList();
            foreach (var fGroup in featureGroups)
            {
                // Define a public enumeration with the name "MyEnum" and an underlying type of Integer.
                EnumBuilder myEnum = moduleBuilder.DefineEnum(string.Format("{0}.{1}", "FeatureTypes", fGroup.Replace(" ", "")), TypeAttributes.Public, typeof(int));
                foreach (var feature in lstFeatures.Where(f => f.FEATURE_GROUP == fGroup).ToList())
                {
                    myEnum.DefineLiteral(feature.FEATURE_NAME.Replace(" ", "_"), Convert.ToInt32(feature.PK_FEATURE_ID));
                }

                // Create the enum
                myEnum.CreateType();
            }

            // Finally, save the assembly
            assemblyBuilder.Save(name.Name + ".dll");
        }
    }
}