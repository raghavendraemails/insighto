﻿namespace Insighto.Business.Enumerations
{
    public enum PageBreakType
    {
        NoPageBreakWithNoSkip = 0,
        PageBreak = 1,
        NoPageBreakWithSkip = 2,
        PageBreakWithSkip = 3
    }
}
