﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Enumerations
{
    public enum SurveyResponseStatus
    {
        None,
        Visits,
        Partial,
        Complete,
        Overquota,
        Bounce,
        Thankyou,
        ScreenOut
    }
}
