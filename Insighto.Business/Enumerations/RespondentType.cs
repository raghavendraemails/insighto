﻿namespace Insighto.Business.Enumerations
{
    public enum RespondentType
    {
        OnlyOnce = 0,
        OnlyOnceWithPassword = 2,
        MultipleTimes = 1
    }   
}
