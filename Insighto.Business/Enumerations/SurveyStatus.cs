﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Enumerations
{
    public enum SurveyStatus
    {
        None = 0,
        Active = 1,
        InActive = 2,
        Draft = 3,
        ReadyToLaunch = 4,
        Closed = 5
    }
}
