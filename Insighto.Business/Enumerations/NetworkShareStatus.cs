﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Enumerations
{
    public enum NetworkShareCodes
    {
        Shared = 0,
        Expired = 1,
        Reshared = 2,
        Renewed = 3,
        Error = -1
    }
}
