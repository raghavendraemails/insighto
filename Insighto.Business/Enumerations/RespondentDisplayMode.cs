﻿namespace Insighto.Business.Enumerations
{
    public enum RespondentDisplayMode
    {
        Respondent = 0,
        Preview = 1,
        PreviewWithPagination = 2,
        Print = 3,
        Word = 4,
        PreviewWithTemplate = 5,
        RespondentWithRedirection = 6,
    }   
}
