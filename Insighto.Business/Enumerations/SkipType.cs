﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Enumerations
{
    public enum SkipType
    {
        None = 0,
        End = -1,
        NoSkip = -2,
        ScreenOut = -3,
        Thankyou = -4
    }   
}
