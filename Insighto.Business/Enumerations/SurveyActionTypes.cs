﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Enumerations
{
    public enum SurveyActionTypes
    {
        None = 0,
        Add = 1,
        Edit = 2,
        Copy = 3,
        Remove = 4,
        AddMiddle=5
    }
}
