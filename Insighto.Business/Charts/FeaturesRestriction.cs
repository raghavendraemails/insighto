using System;
using System.Collections.Generic;
using System.Text;

namespace Insighto.Business.Charts
{
    public class FeaturesRestriction
    {
        int QsingleSelect, QmultiSelect, QDropdown, QYesNo, QMSingleSelect, QMMultiSelect, QMSideBySide, QTComment, QTSingleRow, QTMultiRow, QNumeric, QEmail, QRanking, QConstantSum, QDatetime, QNarration, QPageHeader, QquestionHeader, QContactInfo, QImage, DataCharts, CustomCharts, Reportcustomiz, Filter, CrossTab, IndividualResponses, TrackEmailId, Export,
             Templates, SkipLogic, Introduction, PageBreak, ResponseRequired, RandomizeAnswer, Folder, Category, TextEditor, SpellCheck, OthersText, CustomizedLastpage, Kiosk, Alerts, Reminders, AutoThankuMail, PrintSurvey, CopyQuestions, InsertLogos,EmailCount,
            LaunchThroughInsighto, LaunchThroughExternal, ScheduleSurveyLaunch, ScheduleSurveyClose, PersonalizedEmail, ExcelUpload, CSVUpload, SendReminders, PrelaunchCheck, Header, Footer, Theme, ButtonSettings, CustomizeFont, ProgressBar, CustomURL, Charts, Characterlimit, Answeralign, Reorder, SurFromScratch, SurFromExisting, CopyQuesFromTemp, CopyQuesFromSurvey,
            SurveySavetoWord, AlertMinMax, RedirectionURL, LaunchNewEmailist, EditActiveSurvey, UniqueRep, UniqueRespPassword, SaveandContinue, ReviewLink, Replytoemailaddress, Subject, SurveyLogo,SenderName, SenderEmailAddress,
            ReportsData, ExportPPT, ExportExcel, ExportWord, ExportPDF, ExportCrosstab, ExcludeblankResp, RestExclRespCompletes,Partials, RespEmailAddress, RestExclRespPartials, RawDataExport, SaveSurveyLink, ExitSurveyLink, BrowserAlert,Resppreview;
        string QuestionPerSurvey, NoOfResponses, NumberofSurveys, BasicChartGraph;
        

        public int Q_SINGLESELECT
        {
            get
            {
                return QsingleSelect;
            }
            set
            {
                QsingleSelect = value;
            }
        }
        public int Q_MULTISELECT
        {
            get
            {
                return QmultiSelect;
            }
            set
            {
                QmultiSelect = value;
            }
        }
        public int Q_DROPDOWN
        {
            get
            {
                return QDropdown;
            }
            set
            {
                QDropdown = value;
            }
        }
        public int Q_YESNO
        {
            get
            {
                return QYesNo;
            }
            set
            {
                QYesNo = value;
            }
        }
        public int QM_SINGLESELECT
        {
            get
            {
                return QMSingleSelect;
            }
            set
            {
                QMSingleSelect = value;
            }
        }
        public int QM_MULTISELECT
        {
            get
            {
                return QMMultiSelect;
            }
            set
            {
                QMMultiSelect = value;
            }
        }
        public int QM_SIDEBYSIDE
        {
            get
            {
                return QMSideBySide;
            }
            set
            {
                QMSideBySide = value;
            }
        }
        public int QT_COMMENT
        {
            get
            {
                return QTComment;
            }
            set
            {
                QTComment = value;
            }
        }
        public int QT_SINGLEROW
        {
            get
            {
                return QTSingleRow;
            }
            set
            {
                QTSingleRow = value;
            }
        }
        public int QT_MULTIROW
        {
            get
            {
                return QTMultiRow;
            }
            set
            {
                QTMultiRow = value;
            }
        }
        public int Q_NUMERIC
        {
            get
            {
                return QNumeric;
            }
            set
            {
                QNumeric = value;
            }
        }
        public int Q_EMAIL
        {
            get
            {
                return QEmail;
            }
            set
            {
                QEmail = value;
            }
        }
        public int Q_RANKING
        {
            get
            {
                return QRanking;
            }
            set
            {
                QRanking = value;
            }
        }
        public int Q_CONSTANTSUM
        {
            get
            {
                return QConstantSum;
            }
            set
            {
                QConstantSum = value;
            }
        }
        public int Q_DATETIME
        {
            get
            {
                return QDatetime;
            }
            set
            {
                QDatetime = value;
            }
        }
        public int Q_NARRATION
        {
            get
            {
                return QNarration;
            }
            set
            {
                QNarration = value;
            }
        }
        public int Q_PAGEHEADER
        {
            get
            {
                return QPageHeader;
            }
            set
            {
                QPageHeader = value;
            }
        }
        public int Q_QUESTIONHEADER
        {
            get
            {
                return QquestionHeader;
            }
            set
            {
                QquestionHeader = value;
            }
        }
        public int Q_CONTACTINFO
        {
            get
            {
                return QContactInfo;
            }
            set
            {
                QContactInfo = value;
            }
        }
        public int Q_IMAGE
        {
            get
            {
                return QImage;
            }
            set
            {
                QImage = value;
            }
        }
        public int ANSWER_ALIGN
        {
            get
            {
                return Answeralign;
            }
            set
            {
                Answeralign = value;
            }
        }
        public int DATACHART
        {
            get
            {
                return DataCharts;
            }
            set
            {
                DataCharts = value;
            }
        }
        public int CHART
        {
            get
            {
                return Charts;
            }
            set
            {
                Charts= value;
            }
        }
         public int CHARACTER_LIMIT
        {
            get
            {
                return Characterlimit;
            }
            set
            {
                Characterlimit= value;
            }
        }
         public int COPY_QUESFROMTEMPLATE
         {
             get
             {
                 return CopyQuesFromTemp;
             }
             set
             {
                 CopyQuesFromTemp = value;
             }

         }
         public int COPY_QUESFROMSURVEY
         {
             get
             {
                 return CopyQuesFromSurvey;
             }
             set
             {
                 CopyQuesFromSurvey = value;
             }

         }
        public int CUSTOMCHART
        {
            get
            {
                return CustomCharts;
            }
            set
            {
                CustomCharts = value;
            }
        }
        public int REPORTCUSTOM
        {
            get
            {
                return Reportcustomiz;
            }
            set
            {
                Reportcustomiz = value;
            }
        }
        public int FILTER
        {
            get
            {
                return Filter;
            }
            set
            {
                Filter = value;
            }
        }
        public int CROSS_TAB
        {
            get
            {
                return CrossTab;
            }
            set
            {
                CrossTab = value;
            }
        }
        public int INDIVIDUAL_RESPONSES
        {
            get
            {
                return IndividualResponses;
            }
            set
            {
                IndividualResponses = value;
            }
        }
        public int TRACKEMAIL_ID
        {
            get
            {
                return TrackEmailId;
            }
            set
            {
                TrackEmailId = value;
            }
        }
        public int EXPORT
        {
            get
            {
                return Export;
            }
            set
            {
                Export = value;
            }
        }
        public int TEMPLATE
        {
            get
            {
                return Templates;
            }
            set
            {
                Templates = value;
            }
        }
        public int SKIPLOGIC
        {
            get
            {
                return SkipLogic;
            }
            set
            {
                SkipLogic = value;
            }
        }
        public int INTRODUCTION
        {
            get
            {
                return Introduction;
            }
            set
            {
                Introduction = value;
            }
        }
        public int PAGEBREAK
        {
            get
            {
                return PageBreak;
            }
            set
            {
                PageBreak = value;
            }
        }
        public int RESPONSE_REQUIRED
        {
            get
            {
                return ResponseRequired;
            }
            set
            {
                ResponseRequired = value;
            }
        }
        public int RANDOMIZE_ANSWER
        {
            get
            {
                return RandomizeAnswer;
            }
            set
            {
                RandomizeAnswer = value;
            }
        }
        public int FOLDER
        {
            get
            {
                return Folder;
            }
            set
            {
                Folder = value;
            }
        }
        public int CATEGORY
        {
            get
            {
                return Category;
            }
            set
            {
                Category = value;
            }
        }
        public int TEXT_EDITOR
        {
            get
            {
                return TextEditor;
            }
            set
            {
                TextEditor = value;
            }
        }
        public int SPELL_CHECK
        {
            get
            {
                return SpellCheck;
            }
            set
            {
                SpellCheck = value;
            }
        }
        public int OTHERS_TEXT
        {
            get
            {
                return OthersText;
            }
            set
            {
                OthersText = value;
            }
        }
        public int CUSTOMIZE_LASTPAGE
        {
            get
            {
                return CustomizedLastpage;
            }
            set
            {
                CustomizedLastpage = value;
            }
        }
        public int KIOSK
        {
            get
            {
                return Kiosk;
            }
            set
            {
                Kiosk = value;
            }
        }
        public int ALERTS
        {
            get
            {
                return Alerts;
            }
            set
            {
                Alerts = value;
            }
        }
        public int REMINDERS
        {
            get
            {
                return Reminders;
            }
            set
            {
                Reminders = value;
            }
        }
        public int AUTO_THANKUMAIL
        {
            get
            {
                return AutoThankuMail;
            }
            set
            {
                AutoThankuMail = value;
            }
        }
        public int PRINT_SURVEY
        {
            get
            {
                return PrintSurvey;
            }
            set
            {
                PrintSurvey = value;
            }
        }
        public int COPY_QUESTIONS
        {
            get
            {
                return CopyQuestions;
            }
            set
            {
                CopyQuestions = value;
            }
        }
        public int INSERT_LOGOS
        {
            get
            {
                return InsertLogos;
            }
            set
            {
                InsertLogos = value;
            }
        }
        public int LAUNCH_THROUGHINSIGHTO
        {
            get
            {
                return LaunchThroughInsighto;
            }
            set
            {
                LaunchThroughInsighto = value;
            }
        }
        public int LAUNCH_THROUGHEXTERNAL
        {
            get
            {
                return LaunchThroughExternal;
            }
            set
            {
                LaunchThroughExternal = value;
            }
        }
        public int REORDER
        {
            get
            {
                return Reorder ;
            }
            set
            {
                Reorder= value;
            }
        }
        public int SCHEDULE_SURVEYLAUNCH
        {
            get
            {
                return ScheduleSurveyLaunch;
            }
            set
            {
                ScheduleSurveyLaunch = value;
            }
        }
        public int SCHEDULE_SURVEYCLOSE
        {
            get
            {
                return ScheduleSurveyClose;
            }
            set
            {
                ScheduleSurveyClose = value;
            }
        }
        public int SURVEY_FROMSCRATCH
        {
            get
            {
                return SurFromScratch;
            }
            set
            {
                SurFromScratch = value;
            }
        }
        public int SURVEY_FROMEXISTING
        {
            get
            {
                return SurFromExisting;
            }
            set
            {
                SurFromExisting = value;
            }
        }
        
        public int PERSONAL_EMAIL
        {
            get
            {
                return PersonalizedEmail;
            }
            set
            {
                PersonalizedEmail = value;
            }
        }
        public int EXCEL_UPLOAD
        {
            get
            {
                return ExcelUpload;
            }
            set
            {
                ExcelUpload = value;
            }
        }
        public int CSV_UPLOAD
        {
            get
            {
                return CSVUpload;
            }
            set
            {
                CSVUpload = value;
            }
        }
        public int SEND_REMAINDERS
        {
            get
            {
                return SendReminders;
            }
            set
            {
                SendReminders = value;
            }
        }
        public int PRELAUNCH_CHECK
        {
            get
            {
                return PrelaunchCheck;
            }
            set
            {
                PrelaunchCheck = value;
            }
        }
        public int HEADER
        {
            get
            {
                return Header;
            }
            set
            {
                Header = value;
            }
        }
        public int FOOTER
        {
            get
            {
                return Footer;
            }
            set
            {
                Footer = value;
            }
        }
        public int THEME
        {
            get
            {
                return Theme;
            }
            set
            {
                Theme = value;
            }
        }
        public int BUTTONSETTINGS
        {
            get
            {
                return ButtonSettings;
            }
            set
            {
                ButtonSettings = value;
            }
        }
        public int CUSTOMIZE_FONT
        {
            get
            {
                return CustomizeFont;
            }
            set
            {
                CustomizeFont = value;
            }
        }
        public int PROGRESS_BAR
        {
            get
            {
                return ProgressBar;
            }
            set
            {
                ProgressBar = value;
            }
        }
        public int CUSTOM_URL
        {
            get
            {
                return CustomURL;
            }
            set
            {
                CustomURL = value;
            }
        }

        public string QUESTION_PER_SURVEY
        {
            get
            {
                return QuestionPerSurvey;
            }
            set
            {
                QuestionPerSurvey = value;
            }
        }
        public string NO_OF_RESPONSES
        {
            get
            {
                return NoOfResponses;
            }
            set
            {
                NoOfResponses = value;
            }
        }
        public string BASICCHART
        {
            get
            {
                return BasicChartGraph;
            }
            set
            {
                BasicChartGraph = value;
            }
        }
        public string NO_OF_SURVEYS
        {
            get
            {
                return NumberofSurveys;
            }
            set
            {
                NumberofSurveys = value;
            }
        }
        public int SAVE_TOWORD
        {

            get
            {
                return SurveySavetoWord;
            }
            set
            {
                SurveySavetoWord= value;
            }
        }
        public int ALERT_MINMAXRESP
        {

            get
            {
                return AlertMinMax;
            }
            set
            {
                AlertMinMax = value;
            }

        }
          public int REDIRECTION_URL
        {

            get
            {
                return RedirectionURL;
            }
            set
            {
                RedirectionURL = value;
            }

        }
           public int LAUNCH_NEWEMAILLIST
        {

            get
            {
                return LaunchNewEmailist;
            }
            set
            {
                LaunchNewEmailist = value;
            }

        }
        public int EDIT_ACTIVESURVEY
        {

            get
            {
                return EditActiveSurvey;
            }
            set
            {
                EditActiveSurvey = value;
            }

        }
        public int UNIQUE_RESP
        {

            get
            {
                return UniqueRep;
            }
            set
            {
                UniqueRep= value;
            }

        }

        public int UNIQUE_RESP_PASSWORD
        {

            get
            {
                return UniqueRespPassword;
            }
            set
            {
                UniqueRespPassword = value;
            }

        }
         public int SAVE_CONTINUE
        {
            get
            {
                return SaveandContinue;
            }
            set
            {
                SaveandContinue= value;
            }

        }
        public int REVIEW_LINK
        {
            get
            {
                return ReviewLink;
            }
            set
            {
                ReviewLink= value;
            }

        }
           public int SENDER_NAME
        {
            get
            {
                return SenderName;
            }
            set
            {
                SenderName= value;
            }

        }
          public int SENDER_EMAILADDRESS
        {
            get
            {
                return SenderEmailAddress;
            }
            set
            {
                SenderEmailAddress= value;
            }
        }

        public int REPLYTO_EMAILADDRESS
        {
            get
            {
                return Replytoemailaddress;
            }
            set
            {
                Replytoemailaddress= value;
            }
       }
        public int SUBJECT
        {
            get
            {
                return Subject;
            }
            set
            {
                Subject= value;
            }
        }
        public int SURVEY_LOGO
        {
            get
            {
                return SurveyLogo;
            }
            set
            {
                SurveyLogo = value;
            }

        }
         public int DATA
        {
            get
            {
                return ReportsData;
            }
            set
            {
                ReportsData= value;
            }

        }
         public int EXPORT_PPT
        {
            get
            {
                return ExportPPT;
            }
            set
            {
                ExportPPT = value;
            }

        }
         public int EXPORT_EXCEL
        {
            get
            {
                return ExportExcel;
            }
            set
            {
                ExportExcel = value;
            }

        }
         public int EXPORT_WORD
        {
            get
            {
                return ExportWord;
            }
            set
            {
                ExportWord = value;
            }

        }
         public int EXPORT_PDF
        {
            get
            {
                return ExportPDF;
            }
            set
            {
                ExportPDF = value;
            }

        }
         public int EXPORT_CROSSTAB
        {
            get
            {
                return ExportCrosstab;
            }
            set
            {
                ExportCrosstab= value;
            }

        }
        public int EXCLUDE_BLANKRESPONSE
        {
            get
            {
                return ExcludeblankResp;
            }
            set
            {
                ExcludeblankResp= value;
            }

        }
        public int RESTOREEXCLUDE_RESP_COMPLETES
        {
            get
            {
                return RestExclRespCompletes;
            }
            set
            {
                RestExclRespCompletes= value;
            }

        }
         public int RESP_EMAILADDRESS
        {
            get
            {
                return RespEmailAddress;
            }
            set
            {
                RespEmailAddress= value;
            }

        }
         public int RESTOREEXCLUDE_RESP_PARTIALS
        {
            get
            {
                return RestExclRespPartials;
            }
            set
            {
                RestExclRespPartials= value;
            }

        }
         public int PARTIALS
         {
             get
             {
                 return Partials;
             }
             set
             {
                 Partials = value;
             }

         }

        public int RAW_DATAEXPORT
        {
            get
            {
                return RawDataExport;
            }
            set
            {
                RawDataExport= value;
            }

        }
        public int SAVE_SURVEYLINK
        {
            get
            {
                return SaveSurveyLink;
            }
            set
            {
                SaveSurveyLink = value;
            }

        }
         public int EXIT_SURVEYLINK
        {
            get
            {
                return ExitSurveyLink;
            }
            set
            {
                ExitSurveyLink = value;
            }

        }
         public int BROWSER_ALERT
        {
            get
            {
                return BrowserAlert;
            }
            set
            {
                BrowserAlert = value;
            }

        }
         public int RESP_PREVIEW
         {
             get
             {
                 return Resppreview;
             }
             set
             {
                 Resppreview = value;
             }

         }
         public int EMAIL_COUNT
         {
             get
             {
                 return EmailCount;
             }
             set
             {
                 EmailCount = value;
             }

         }

    }
}
