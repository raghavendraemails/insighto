using System;
using System.Collections.Generic;
using System.Text;

namespace Insighto.Business.Charts
{
    public class SurveyOptions
    {

        int surveyID, surveycontrolID, SurveyLaunchType, onlunchAlert, SurveyCloseType, onCloseAlert, NoOfResponseToClose, EnablePassword, uniquRespondent, saveContinue, autoThanksMail, alertMinResponse, alertMaxResponse, contactlistID, pendingVerf, bounced, duplicate, themeID, status, sendTO, totalResponse, partialResponse, screenOuts, thanksPageType, closePageType, screenoutPageType, overquotaPageType, URLredirectionStatus, FinishOptionStatus, SurveyUrlType, EmailListType, LaunchOptionStatus;
        string unsubscribed, surveyPassword, surveyURL, remainderText, thanksPage, closePage, screenoutPage, overquotaPage, URLredirection, emailInvitation, timeForRedirection,SurveyLaunchUrl,emailFromAddress,emailSubject,emailsendername,emailreplytoaddress;
        DateTime launchDate, minAlertDate, maxAlertDate, closeDate, reminderDate, launchedOn;
        
        public int SURVEY_ID
        {
            get
            {
                return surveyID;
            }
            set
            {
                surveyID = value;
            }
        }
        public int SURVEYCONTROL_ID
        {
            get
            {
                return surveycontrolID;
            }
            set
            {
                surveycontrolID = value;
            }
        }
        public int SURVEYLAUNCHTYPE
        {
            get
            {
                return SurveyLaunchType;
            }
            set
            {
                SurveyLaunchType = value;
            }
        }
        public int SURVEYCLOSETYPE
        {
            get
            {
                return SurveyCloseType;
            }
            set
            {
                SurveyCloseType = value;
            }
        }
        public int ONCLOSEALERT
        {
            get
            {
                return onCloseAlert;
            }
            set
            {
                onCloseAlert = value;
            }
        }
        public int NOOFRESPONSESTOCLOSEE
        {
            get
            {
                return NoOfResponseToClose;
            }
            set
            {
                NoOfResponseToClose = value;
            }
        }
        public int ENABLEPASSWORD
        {
            get
            {
                return EnablePassword;
            }
            set
            {
                EnablePassword = value;
            }
        }
        public int CONTACTLIST_ID
        {
            get
            {
                return contactlistID;
            }
            set
            {
                contactlistID = value;
            }
        }
        public int PENDING_VERFICATION
        {
            get
            {
                return pendingVerf;
            }
            set
            {
                pendingVerf = value;
            }
        }
        public int BOUNCED
        {
            get
            {
                return bounced;
            }
            set
            {
                bounced = value;
            }
        }
        public int DUPLICATED
        {
            get
            {
                return duplicate;
            }
            set
            {
                duplicate = value;
            }
        }
        public int THEME_ID
        {
            get
            {
                return themeID;
            }
            set
            {
                themeID = value;
            }
        }
        public int STATUS
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        public int ONLAUNCH_ALERT
        {
            get
            {
                return onlunchAlert;
            }
            set
            {
                onlunchAlert = value;
            }
        }
        public int UNIQUE_RESPONDENT
        {
            get
            {
                return uniquRespondent;
            }
            set
            {
                uniquRespondent = value;
            }
        }
        public int SAVE_AND_CONTINUE
        {
            get
            {
                return saveContinue;
            }
            set
            {
                saveContinue = value;
            }
        }
        public int AUTOMATIC_THANKYOU_EMAIL
        {
            get
            {
                return autoThanksMail;
            }
            set
            {
                autoThanksMail = value;
            }
        }
        public int ALERT_ON_MIN_RESPONSES
        {
            get
            {
                return alertMinResponse;
            }
            set
            {
                alertMinResponse = value;
            }
        }
        public int ALERT_ON_MAX_RESPONSES
        {
            get
            {
                return alertMaxResponse;
            }
            set
            {
                alertMaxResponse = value;
            }
        }
        public int SEND_TO
        {
            get
            {
                return sendTO;
            }
            set
            {
                sendTO = value;
            }
        }
        public int TOTAL_RESPONSES
        {
            get
            {
                return totalResponse;
            }
            set
            {
                totalResponse = value;
            }
        }
        public int PARTIAL_RESPONSES
        {
            get
            {
                return partialResponse;
            }
            set
            {
                partialResponse = value;
            }
        }
        public int SCREEN_OUTS
        {
            get
            {
                return screenOuts;
            }
            set
            {
                screenOuts = value;
            }
        }
        
        public int THANKU_PAGETYPE
        {
            get
            {
                return thanksPageType;
            }
            set
            {
                thanksPageType = value;
            }
        }
        public int CLOSE_PAGETYPE
        {
            get
            {
                return closePageType;
            }
            set
            {
                closePageType = value;
            }
        }
        public int SCREENOUT_PAGETYPE
        {
            get
            {
                return screenoutPageType;
            }
            set
            {
                screenoutPageType = value;
            }
        }
        public int OVERQUOTA_PAGETYPE
        {
            get
            {
                return overquotaPageType;
            }
            set
            {
                overquotaPageType = value;
            }
        }

        public int URLREDIRECTION_STATUS
        {
            get
            {
                return URLredirectionStatus;
            }
            set
            {
                URLredirectionStatus = value;
            }
        }
        public int FINISHOPTIONS_STATUS
        {
            get
            {
                return FinishOptionStatus;
            }
            set
            {
                FinishOptionStatus = value;
            }
        }
       
        public int SURVEYURL_TYPE
        {
            get
            {
                return SurveyUrlType;
            }
            set
            {
                SurveyUrlType = value;
            }
        }

        public int EMAILLIST_TYPE
        {
            get
            {
                return EmailListType;
            }
            set
            {
                EmailListType = value;
            }
        }
        public int LAUNCHOPTIONS_STATUS
        {
            get
            {
                return LaunchOptionStatus;
            }
            set
            {
                LaunchOptionStatus = value;
            }
        }
        public string SURVEYLAUNCH_URL
        {
            get
            {
                return SurveyLaunchUrl;
            }
            set
            {
                SurveyLaunchUrl = value;
            }
        }
       
        public string TIME_FOR_URL_REDIRECTION
        {
            get
            {
                return timeForRedirection;
            }
            set
            {
                timeForRedirection = value;
            }
        }
        public string UNSUBSCRIBED
        {
            get
            {
                return unsubscribed;
            }
            set
            {
                unsubscribed = value;
            }
        }
        public string SURVEY_PASSWORD
        {
            get
            {
                return surveyPassword;
            }
            set
            {
                surveyPassword = value;
            }
        }
        public string SURVEY_URL
        {
            get
            {
                return surveyURL;
            }
            set
            {
                surveyURL = value;
            }
        }
        public string REMINDER_TEXT
        {
            get
            {
                return remainderText;
            }
            set
            {
                remainderText = value;
            }
        }
        public string THANKYOU_PAGE
        {
            get
            {
                return thanksPage;
            }
            set
            {
                thanksPage = value;
            }
        }
        public string CLOSE_PAGE
        {
            get
            {
                return closePage;
            }
            set
            {
                closePage = value;
            }
        }
        public string SCREENOUT_PAGE
        {
            get
            {
                return screenoutPage;
            }
            set
            {
                screenoutPage = value;
            }
        }
        public string OVERQUOTA_PAGE
        {
            get
            {
                return overquotaPage;
            }
            set
            {
                overquotaPage = value;
            }
        }
        public string URL_REDIRECTION
        {
            get
            {
                return URLredirection;
            }
            set
            {
                URLredirection = value;
            }
        }
        public string EMAIL_INVITATION
        {
            get
            {
                return emailInvitation;
            }
            set
            {
                emailInvitation = value;
            }
        }

        public string EMAIL_FROMADDRESS
        {
            get
            {
                return emailFromAddress;
            }
            set
            {
                emailFromAddress = value;
            }
        }
        public string EMAIL_SUBJECT
        {
            get
            {
                return emailSubject;
            }
            set
            {
                emailSubject = value;
            }
        }
        public string EMAIL_SENDERNAME
        {
            get
            {
                return emailsendername;
            }
            set
            {
                emailsendername = value;
            }
        }
        public string EMAIL_REPLYTOADDRESS
        {
            get
            {
                return emailreplytoaddress;
            }
            set
            {
                emailreplytoaddress= value;
            }
        }
        public DateTime LAUNCH_ON_DATE
        {
            get
            {
                return launchDate;
            }
            set
            {
                launchDate = value;
            }
        }
        public DateTime ALERT_ON_MIN_RESPONSES_BY_DATE
        {
            get
            {
                return minAlertDate;
            }
            set
            {
                minAlertDate = value;
            }
        }
        public DateTime ALERT_ON_MAX_RESPONSES_BY_DATE
        {
            get
            {
                return maxAlertDate;
            }
            set
            {
                maxAlertDate = value;
            }
        }
        public DateTime CLOSE_ON_DATE
        {
            get
            {
                return closeDate;
            }
            set
            {
                closeDate = value;
            }
        }
        public DateTime REMINDER_ON_DATE
        {
            get
            {
                return reminderDate;
            }
            set
            {
                reminderDate = value;
            }
        }
        public DateTime LAUNCHED_ON
        {
            get
            {
                return launchedOn;
            }
            set
            {
                launchedOn = value;
            }
        }

         
    }
}
