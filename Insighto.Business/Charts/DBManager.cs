using System;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.SqlTypes;
using System.Data.Sql;


namespace Insighto.Business.Charts
{
	/// <summary>
	/// Summary description for DBManager.
	/// </summary>
	public class DBManager
	{
		
		private IDbConnection idbConnection;
		private IDbTransaction idbTransaction =null;
		private string strConnection;
 
		public DBManager()
		{
 
		}
 
		public IDbConnection Connection
		{
			get
			{
				return idbConnection;
			}
		}
 
		public string ConnectionString
		{
			get
			{
				return strConnection;
			}
		}
 
		public void OpenConnection(string connStr)
		{
			try
			{
				idbConnection = GetConnection(DataProvider.SqlServer);
				idbConnection.ConnectionString =connStr;
				if (idbConnection.State !=ConnectionState.Open)
					idbConnection.Open();
				this.strConnection=connStr;
			}
			catch(Exception ex)
			{
				throw(ex);
			}
		}

		public void Close()
		{
			GC.SuppressFinalize(this);
			if (idbConnection.State != ConnectionState.Closed)
				idbConnection.Close();
			this.idbConnection = null;
		}
 
		public void BeginTransaction()
		{
			IDbCommand idbCommand = GetCommand(DataProvider.SqlServer);
			if (this.idbTransaction == null)
			idbTransaction = GetTransaction(DataProvider.SqlServer);
			idbCommand.Transaction =idbTransaction;
		}
 
		public void CommitTransaction()
		{
			if (this.idbTransaction != null)
				this.idbTransaction.Commit();
			idbTransaction = null;
		}
		public void RollbackTransaction()
		{
			if (this.idbTransaction != null)
				this.idbTransaction.Rollback();
			idbTransaction = null;
		}
 
		private void PrepareCommand(IDbCommand command, IDbConnection
			connection,
			IDbTransaction transaction, CommandType commandType, string	commandText)
		{
			command.Connection = connection;
			command.CommandText = commandText;
			command.CommandType = commandType;
 
			if (transaction != null)
			{
				command.Transaction = transaction;
			}
		}

		public IDataReader ExecuteReader(CommandType commandType, string commandText)
		{

            //DispatchController.GetInstance().LogLine("READER: " + commandText);
			IDataReader idrToRet = null;
			IDbCommand idbCommand = GetCommand(DataProvider.SqlServer);
			idbCommand =GetCommand(DataProvider.SqlServer);
			PrepareCommand(idbCommand, this.Connection, idbTransaction,
				commandType, commandText);
			idrToRet =idbCommand.ExecuteReader();
			return idrToRet;
		}
 
		public int ExecuteNonQuery(CommandType commandType, string commandText)
		{
            //DispatchController.GetInstance().LogLine("NON-QUERY: " + commandText);
            
            IDbCommand idbCommand = GetCommand(DataProvider.SqlServer);
            PrepareCommand(idbCommand, this.Connection, idbTransaction,
				commandType, commandText);
			int returnValue = idbCommand.ExecuteNonQuery();
			return returnValue;
		}

        public int SPExecuteNonQuery(CommandType commandType, string commandText, SqlCommand Cmd)
        {
            IDbCommand idbCommand = Cmd;
            PrepareCommand(idbCommand, this.Connection, idbTransaction,
                commandType, commandText);
            int returnValue = idbCommand.ExecuteNonQuery();
            return returnValue;
        }

        public DataSet ExecuteDataSet(CommandType commandType, string commandText, SqlCommand Cmd)
        {
            DataSet ds = new DataSet();
            IDbDataAdapter objAdp = null;
            IDbCommand idbCommand = Cmd;
            PrepareCommand(idbCommand, this.Connection, idbTransaction,
                commandType, commandText);
            objAdp.SelectCommand = idbCommand;
            objAdp.Fill(ds);
            return ds;
        }

        public object ExecuteScalar(CommandType commandType, string commandText)
		{
            //DispatchController.GetInstance().LogLine("SCALAR: " + commandText);
            
            IDbCommand idbCommand = GetCommand(DataProvider.SqlServer);
			PrepareCommand(idbCommand,this.Connection, idbTransaction,commandType,
				commandText);
			object returnValue = idbCommand.ExecuteScalar();
			if ( returnValue != null )
				return returnValue;
			else
				return -1;
		}
 
		public DataSet ExecuteDataSet(CommandType commandType, string commandText)
		{
            //DispatchController.GetInstance().LogLine("DATASET: " + commandText);
            
            IDbCommand idbCommand = GetCommand(DataProvider.SqlServer);
            DateTime dt1 = DateTime.Now;
			PrepareCommand(idbCommand,this.Connection, idbTransaction,
				commandType, commandText);
			IDbDataAdapter dataAdapter =GetDataAdapter (DataProvider.SqlServer);
			dataAdapter.SelectCommand = idbCommand;
			DataSet dataSet = new DataSet();
			dataAdapter.Fill(dataSet);
            DateTime dt2 = DateTime.Now;
            long msec = Convert.ToInt64((dt2.Ticks - dt1.Ticks) / 10000);
            //DispatchController.GetInstance().LogLine(((msec > 2000)? "++++++++++++++" : "") + "TIME TAKEN (ms): " + msec.ToString());
            return dataSet;
		}

		////DBManager
		///
		protected static IDbConnection GetConnection(DataProvider providerType)
		{
			IDbConnection iDbConnection = null;
			switch (providerType)
			{
				case DataProvider.SqlServer:
                    iDbConnection = new SqlConnection();
					break;
				default:
					return null;
			}
			return iDbConnection;
		}

		protected static IDbCommand GetCommand(DataProvider providerType)
		{
			switch (providerType)
			{
				case DataProvider.SqlServer:
                    return new SqlCommand();
				default:
					return null;
			}
		}

		protected static IDbDataAdapter GetDataAdapter(DataProvider providerType)
		{
			switch (providerType)
			{
				case DataProvider.SqlServer:
                    return new SqlDataAdapter();
				default:
					return null;
			}
		}

		protected static IDbTransaction GetTransaction(DataProvider providerType)
		{
			IDbConnection iDbConnection =GetConnection(providerType);
			IDbTransaction iDbTransaction =iDbConnection.BeginTransaction();
			return iDbTransaction;
		}
	}
}
