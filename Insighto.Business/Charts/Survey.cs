using System;
using System.Collections.Generic;
using System.Text;

namespace Insighto.Business.Charts
{
    public class Survey
    {
        int surveyID, folderID, userID,PageBreak,ResponseRequired,CheckBlockWords,SurveyIntroPageBreak,BrowserbackButton;
        string surveyName, surveyCategory, active, status, SurveyINTRO, SurveyHeader, SurveyFooter, surveyFontType, SurveyFontSize, SurveyFontColor, surveyButtonType, CustomStarttext, CustomSubmittext,Theme,Logo;
        private List<SurveyOptions> surveyOpt;
        private List<SurveyQuestion> surveyQue;        
        public int SURVEY_ID
        {
            get
            {
                return surveyID;
            }
            set
            {
                surveyID = value;
            }
        }
        public int FOLDER_ID
        {
            get
            {
                return folderID;
            }
            set
            {
                folderID = value;
            }
        }
        public int USER_ID
        {
            get
            {
                return userID;
            }
            set
            {
                userID = value;
            }
        }
        public int PAGE_BREAK
        {
            get
            {
                return PageBreak;
            }
            set
            {
                PageBreak = value;
            }
        }

        public int SURVEYINTRO_PAGEBREAK
        {
            get
            {
                return SurveyIntroPageBreak;
            }
            set
            {
                SurveyIntroPageBreak = value;
            }
        }
        public int RESPONSE_REQUIRED
        {
            get
            {
                return ResponseRequired;
            }
            set
            {
                ResponseRequired = value;
            }
        }
        public int CHECK_BLOCKWORDS
        {
            get
            {
                return CheckBlockWords;
            }
            set
            {
                CheckBlockWords = value;
            }
        }
        public string SURVEY_INTRO
        {
            get
            {
                return SurveyINTRO;
            }
            set
            {
                SurveyINTRO = value;
            }
        }
        public string SURVEY_NAME
        {
            get
            {
                return surveyName;
            }
            set
            {
                surveyName = value;
            }
        }
        public string SURVEY_CATEGORY
        {
            get
            {
                return surveyCategory;
            }
            set
            {
                surveyCategory = value;
            }
        }
        public string ACTIVE
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }
        public string STATUS
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        
        public string SURVEY_HEADER
        {
            get
            {
                return SurveyHeader;
            }
            set
            {
                SurveyHeader = value;
            }
        }
        
        public string SURVEY_FOOTER
        {
            get
            {
                return SurveyFooter;
            }
            set
            {
                SurveyFooter = value;
            }
        }
        public string SURVEY_FONT_TYPE
        {
            get
            {
                return surveyFontType;
            }
            set
            {
                surveyFontType = value;
            }
        }
        public string SURVEY_FONT_SIZE
        {
            get
            {
                return SurveyFontSize;
            }
            set
            {
                SurveyFontSize = value;
            }
        }
        public string SURVEY_FONT_COLOR
        {
            get
            {
                return SurveyFontColor;
            }
            set
            {
                SurveyFontColor = value;
            }
        }
       
        
         public string SURVEY_BUTTON_TYPE
         {
            get
            {
                return surveyButtonType;
            }
            set
            {
                surveyButtonType = value;
            }
         }
        public string CUSTOMSTART_TEXT
        {
            get
            {
                return CustomStarttext;
            }
            set
            {
                CustomStarttext = value;
            }
        }
        public string CUSTOMSUBMITTEXT
        {
            get
            {
                return CustomSubmittext;
            }
            set
            {
                CustomSubmittext = value;
            }
        }
        public string THEME
        {
            get
            {
                return Theme;
            }
            set
            {
                Theme = value;
            }
        }
        public int BROWSER_BACKBUTTON_STATUS
        {
            get
            {
                return BrowserbackButton;
            }
            set
            {
                BrowserbackButton = value;
            }
        }

        public string SURVEY_LOGO
        {
            get
            {
                return Logo;
            }
            set
            {
                Logo = value;
            }
        }
        public List<SurveyOptions> surveyOptns
        {
            get
            {
                return surveyOpt;
            }
            set
            {
                surveyOpt = value;
            }
        }
        public List<SurveyQuestion> surveyQues
        {
            get
            {
                return surveyQue;
            }
            set
            {
                surveyQue = value;
            }
        }
  
    }
}
