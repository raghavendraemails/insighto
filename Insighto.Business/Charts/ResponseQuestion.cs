using System;
using System.Collections.Generic;
using System.Text;

namespace Insighto.Business.Charts
{
    public class ResponseQuestion
    {
        int ResponseID,RespondentID, QuestionID, AnswerID,QType;
        string AnswerText;
       
        public int RESPONSE_ID
        {
            get
            {
                return ResponseID;
            }
            set
            {
                ResponseID = value;
            }
        }
        public int RESPONDENT_ID
        {
            get
            {
                return RespondentID;
            }
            set
            {
                RespondentID = value;
            }
        }
        public int QUESTION_ID
        {
            get
            {
                return QuestionID;
            }
            set
            {
                QuestionID = value;
            }
        }
        public int ANSWER_ID
        {
            get
            {
                return AnswerID;
            }
            set
            {
                AnswerID = value;
            }
        }
        public string ANSWER_TEXT
        {
            get
            {
                return AnswerText;
            }
            set
            {
                AnswerText = value;
            }
        }
        public int QUES_TYPE
        {
            get
            {
                return QType;
            }
            set
            {
                QType = value;
            }
        }
    }
}
