using System;
using System.Collections.Generic;
using System.Text;

namespace Insighto.Business.Charts
{
    public class SurveyQuestion : IComparable
    {

        int surveyID, questionID, QuestionType, QuestionSeq, SkipLogic, ResponseRequired, PageBreak, ImageID, OtherAns, ResponseCount, RandomAns, CharLimit, ShowLabel, LabelAngle, ValueAspercent, ZoomPercent, Transparency, PerspectiveAngle, MarkerSize, ShowMarker, Inverted, LegendEquallySpacedItems, LegendMaxHorizontalPercentage, LegendMaxVerticalPercentage, HideLegand, HideXaxis, Staggered, otherrespcount, QUESTIONno;
        string QuestionLabel, QuestionNo, ChartTitle, Comments, ChartType, ChartAppearance, ChartPalette, ChartAligment, ChartDock, AnswerAlignStyle, LabelPosition, MarkerKind, HoleRadius, DiagramType, TextDirection, FunctionType, ExplodedPoint, LegendHorizontalAlignment, LegendVerticalAlignment, LegendDirection;
        DateTime CreatedOn,LastModifiedOn;
        private List<SurveyAnswers> surveyAns;
        
        public int SURVEY_ID
        {
            get
            {
                return surveyID;
            }
            set
            {
                surveyID = value;
            }
        }
        public int QUESTION_ID
        {
            get
            {
                return questionID;
            }
            set
            {
                questionID = value;
            }
        }
        
        public int QUESTION_TYPE
        {
            get
            {
                return QuestionType;
            }
            set
            {
                QuestionType = value;
            }
        }
        public int QUESTION_SEQ
        {
            get
            {
                return QuestionSeq;
            }
            set
            {
                QuestionSeq = value;
            }
        }
        public int QUESTION_no
        {
            get
            {
                return QUESTIONno;
            }
            set
            {
                QUESTIONno = value;
            }
        }
        public int SKIP_LOGIC
        {
            get
            {
                return SkipLogic;
            }
            set
            {
                SkipLogic = value;
            }
        }
        public int RESPONSE_REQUIRED
        {
            get
            {
                return ResponseRequired;
            }
            set
            {
                ResponseRequired = value;
            }
        }
        public int PAGE_BREAK
        {
            get
            {
                return PageBreak;
            }
            set
            {
                PageBreak = value;
            }
        }
        public int IMAGE_ID
        {
            get
            {
                return ImageID;
            }
            set
            {
                ImageID = value;
            }
        }
        public int OTHER_ANS
        {
            get
            {
                return OtherAns;
            }
            set
            {
                OtherAns = value;
            }
        }
        public int RESPONSE_COUNT
        {
            get
            {
                return ResponseCount;
            }
            set
            {
                ResponseCount = value;
            }
        }
        public int RANDOM_ANSWERS
        {
            get
            {
                return RandomAns;
            }
            set
            {
                RandomAns = value;
            }
        }
        public int CHAR_LIMIT
        {
            get
            {
                return CharLimit;
            }
            set
            {
                CharLimit = value;
            }
        }
       
        public int SHOW_LABEL
        {
            get
            {
                return ShowLabel;
            }
            set
            {
                ShowLabel = value;
            }
        }
        public int LABEL_ANGLE
        {
            get
            {
                return LabelAngle;
            }
            set
            {
                LabelAngle = value;
            }
        }
        public int VALUEAS_PERCENT
        {
            get
            {
                return ValueAspercent;
            }
            set
            {
                ValueAspercent = value;
            }
        }
        public int ZOOMPERCENT
        {
            get
            {
                return ZoomPercent;
            }
            set
            {
                ZoomPercent = value;
            }
        }
        public int TRANSPARENCY
        {
            get
            {
                return Transparency;
            }
            set
            {
                Transparency = value;
            }
        }
        public int PERSPECTIVE_ANGLE
        {
            get
            {
                return PerspectiveAngle;
            }
            set
            {
                PerspectiveAngle = value;
            }
        }
        public int MARKER_SIZE
        {
            get
            {
                return MarkerSize;
            }
            set
            {
                MarkerSize = value;
            }
        }
        public int SHOW_MARKER
        {
            get
            {
                return ShowMarker;
            }
            set
            {
                ShowMarker = value;
            }
        }
        public int INVERTED
        {
            get
            {
                return Inverted;
            }
            set
            {
                Inverted = value;
            }
        }
        public int LEGEND_EQUALLYSPACED_ITEMS
        {
            get
            {
                return LegendEquallySpacedItems;
            }
            set
            {
                LegendEquallySpacedItems = value;
            }
        }
        public int HIDE_LEGAND
        {
            get
            {
                return HideLegand;
            }
            set
            {
                HideLegand = value;
            }
        }
        public int HIDE_XAXIS
        {
            get
            {
                return HideXaxis;
            }
            set
            {
                HideXaxis = value;
            }
        }
        public int STAGGERED
        {
            get
            {
                return Staggered;
            }
            set
            {
                Staggered = value;
            }
        }
        public string LEGEND_HORIZONTALALIGN
        {
            get
            {
                return LegendHorizontalAlignment;
            }
            set
            {
                LegendHorizontalAlignment = value;
            }
        }
        public string LEGEND_VERTICALALIGN
        {
            get
            {
                return LegendVerticalAlignment;
            }
            set
            {
                LegendVerticalAlignment = value;
            }
        }
        public string LEGEND_DIRECTION
        {
            get
            {
                return LegendDirection;
            }
            set
            {
                LegendDirection = value;
            }
        }
        public int LEGEND_MAX_HORIZONTALALIGN
        {
            get
            {
                return LegendMaxHorizontalPercentage;
            }
            set
            {
                LegendMaxHorizontalPercentage = value;
            }
        }
        public int LEGEND_MAX_VERTICALALIGN
        {
            get
            {
                return LegendMaxVerticalPercentage;
            }
            set
            {
                LegendMaxVerticalPercentage = value;
            }
        }
        
        public string ANSWER_ALIGNSTYLE
        {
            get
            {
                return AnswerAlignStyle;
            }
            set
            {
                AnswerAlignStyle = value;
            }
        }
        public string QUESTION_LABEL
        {
            get
            {
                return QuestionLabel;
            }
            set
            {
                QuestionLabel = value;
            }
        }
        public string QUESTION_NO
        {
            get
            {
                return QuestionNo;
            }
            set
            {
                QuestionNo = value;
            }
        }
        public string CHART_TITLE
        {
            get
            {
                return ChartTitle;
            }
            set
            {
                ChartTitle = value;
            }
        }
        public string COMMENTS
        {
            get
            {
                return Comments;
            }
            set
            {
                Comments = value;
            }
        }
        public string CHART_TYPE
        {
            get
            {
                return ChartType;
            }
            set
            {
                ChartType = value;
            }
        }
        public string CHART_APPEARANCE
        {
            get
            {
                return ChartAppearance;
            }
            set
            {
                ChartAppearance = value;
            }
        }
        public string CHART_PALETTE
        {
            get
            {
                return ChartPalette;
            }
            set
            {
                ChartPalette = value;
            }
        }
        public string CHART_ALIGNMENT
        {
            get
            {
                return ChartAligment;
            }
            set
            {
                ChartAligment = value;
            }
        }
        public string CHART_DOCK
        {
            get
            {
                return ChartDock;
            }
            set
            {
                ChartDock = value;
            }
        }
        
        public string LABEL_POSITION
        {
            get
            {
                return LabelPosition;
            }
            set
            {
                LabelPosition = value;
            }
        }
        public string MARKER_KIND
        {
            get
            {
                return MarkerKind;
            }
            set
            {
                MarkerKind = value;
            }
        }
        public string HOLE_RADIUS
        {
            get
            {
                return HoleRadius;
            }
            set
            {
                HoleRadius = value;
            }
        }
        public string DIAGRAM_TYPE
        {
            get
            {
                return DiagramType;
            }
            set
            {
                DiagramType = value;
            }
        }
        public string TEXT_DIRECTION
        {
            get
            {
                return TextDirection;
            }
            set
            {
                TextDirection = value;
            }
        }
        public string FUNCTION_TYPE
        {
            get
            {
                return FunctionType;
            }
            set
            {
                FunctionType = value;
            }
        } 
        public string EXPLODED_POINT
        {
            get
            {
                return ExplodedPoint;
            }
            set
            {
                ExplodedPoint = value;
            }
        }
        public DateTime CREATED_ON
        {
            get
            {
                return CreatedOn;
            }
            set
            {
                CreatedOn = value;
            }
        }
        public DateTime LASTMODIFIED_ON
        {
            get
            {
                return LastModifiedOn;
            }
            set
            {
                LastModifiedOn = value;
            }
        }

        public List<SurveyAnswers> surveyAnswers
        {
            get
            {
                return surveyAns;
            }
            set
            {
                surveyAns = value;
            }
        }

       public int CompareTo(object obj)
        {
            if (obj is SurveyQuestion)
            {
                SurveyQuestion QuesOrder = (SurveyQuestion)obj;
                return this.QUESTION_SEQ.CompareTo(QuesOrder.QUESTION_SEQ);
            }
            else
            {
                throw new Exception("The object is not Question.");
            }
        }

        public int OTHER_RESPCOUNT
        {
            get
            {
                return otherrespcount;
            }
            set
            {
                otherrespcount = value;
            }
        }

    }
}
