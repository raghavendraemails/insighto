﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Insighto.Business.Helpers
{
    /// <summary>
    /// This class queries Project Honeypot's Http:BL IP address blacklist.
    /// See http://projecthoneypot.org/ for more information.
    /// Implemented by Balint Farkas (balint.farkas@windowslive.com).
    /// This is free software, use for whatever purpose you want.
    /// </summary>
    public static class HttpBlQuerier
    {
        /// <summary>
        /// Postfix to be used for queries to Http:BL.
        /// </summary>
        private const string httpBlDomain = "dnsbl.httpbl.org";
        private const string AccessKey = "bdfihatbeqes";

        /// <summary>
        /// Holds possible return values for a Http:BL lookup.
        /// </summary>
        public enum HttpBlLookupResult
        {
            ErrorOccurred,
            OnBlacklist,
            NotOnBlacklist
        }

        /// <summary>
        /// Looks up the IP on Project Honeypot's Http:BL blacklist.
        /// </summary>
        public static string Lookup(string strIP)
        {
            try
            {
                //Create the query
                string reversedIp = ReverseIp(strIP);
                string query = string.Format("{0}.{1}.{2}", AccessKey, reversedIp, httpBlDomain);

                //Perform the query.
                //As per the API specification, the query gives back a NXDOMAIN result
                //when an address is not on the blacklist.
                //This translates to a .NET SocketException with SocketError.HostNotFound error code.
                string queryResult = null;
                try
                {
                    queryResult = Dns.GetHostAddresses(query).First().ToString();
                }
                catch (SocketException ex)
                {
                    if ((ex.SocketErrorCode == SocketError.HostNotFound) ||
                        (ex.SocketErrorCode == SocketError.NoData)) // Based on .NET framework
                    {
                        return HttpBlLookupResult.NotOnBlacklist.ToString();
                    }
                }

                //Interpret the result
                string[] octets = queryResult.Split('.');

                //As per the API specifications, the first octet has to be 127.
                //If it isn't 127, some kind of error has occurred.
                if (octets[0] != "127")
                {
                    return HttpBlLookupResult.ErrorOccurred.ToString();
                }

                //If we received a valid DNS result and the request was not in error,
                //the IP is on the blacklist
                return HttpBlLookupResult.OnBlacklist.ToString();
            }
            catch (Exception)
            {
                return HttpBlLookupResult.ErrorOccurred.ToString();
            }
        }

        /// <summary>
        /// Octet-reverses the given IPv4 address.
        /// Assumes the input string to be in the correct format.
        /// </summary>
        public static string ReverseIp(string input)
        {
            string[] octets = input.Split('.');
            return string.Format("{0}.{1}.{2}.{3}", octets[3], octets[2], octets[1], octets[0]);
        }
    }
}