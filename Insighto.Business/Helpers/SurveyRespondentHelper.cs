﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Helpers
{
    public class SurveyRespondentHelper
    {
        public static string FormatControlId(string prefix, object questionTypeId, object questionId, object answerId = null)
        {
            if (answerId == null)
                return string.Format("{0}{1}_{2}", prefix, questionTypeId, questionId);

            return string.Format("{0}{1}_{2}_{3}", prefix, questionTypeId, questionId, answerId);
        }
    }
}
