﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.Web;

namespace Insighto.Business.Helpers
{
    public class GenericHelper
    {
        /// <summary>
        /// Strings to enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumString">The enum string.</param>
        /// <returns></returns>
        public static T ToEnum<T>(string enumString)
        {
            if (!string.IsNullOrEmpty(enumString) && Enum.IsDefined(typeof(T), enumString))
                return (T)Enum.Parse(typeof(T), enumString);
            return default(T);
        }

        public static T ToEnum<T>(int enumValue)
        {
            if (Enum.IsDefined(typeof(T), enumValue))
                return (T)Enum.ToObject(typeof(T), enumValue);
            return default(T);
        }

        public static List<T> EnumToList<T>()
        {

            Type enumType = typeof(T);

            // Can't use type constraints on value types, so have to do check like this
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            return new List<T>(Enum.GetValues(enumType) as IEnumerable<T>);
        }
    }
}
