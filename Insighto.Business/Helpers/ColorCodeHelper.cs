﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Helpers
{
    public static class ColorCodeHelper
    {
        public static string GetColorCodes(string colorName)
        {
            colorName = string.IsNullOrEmpty(colorName) ? "purple" : colorName.Trim();
            var ColorCodes = new Dictionary<string, string>();
            ColorCodes.Add("amber", "#FF7E00");
            ColorCodes.Add("aqua", "#00FFFF");
            ColorCodes.Add("black", "#000000");
            ColorCodes.Add("blue", "#2065A8");
            ColorCodes.Add("brown", "#A52A2A");
            ColorCodes.Add("burnt orange", "#EA9E6A");
            ColorCodes.Add("dark azure", "#0038A8");
            ColorCodes.Add("dark green", "#006400");
            ColorCodes.Add("dark olive", "#556B2F");
            ColorCodes.Add("gold", "#FFD700");
            ColorCodes.Add("gray", "#808080");
            ColorCodes.Add("grayish blue", "#53868B");
            ColorCodes.Add("green", "#00873B");
            ColorCodes.Add("indigo", "#4B0082");
            ColorCodes.Add("light sky blue", "#87CEFA");
            ColorCodes.Add("light yellow", "#FFFFE0");
            ColorCodes.Add("lime", "#00FF00");
            ColorCodes.Add("magneta", "#FF00FF");
            ColorCodes.Add("maroon", "#800000");
            ColorCodes.Add("medium gray", "696969");
            ColorCodes.Add("navy blue", "#23238E");
            ColorCodes.Add("olive", "#808000");
            ColorCodes.Add("pale cyan", "#00FFFF");
            ColorCodes.Add("pale green", "#98FB98");
            ColorCodes.Add("peach", "#FFDAB9");
            ColorCodes.Add("pink", "#FFC0CB");
            ColorCodes.Add("plum", "#DDA0DD");
            ColorCodes.Add("purple", "#845092");
            ColorCodes.Add("red", "#DF0418");
            ColorCodes.Add("royal blue", "#4169E1");
            ColorCodes.Add("sea green", "#2E8B57");
            ColorCodes.Add("silver", "#C0C0C0");
            ColorCodes.Add("sky blue", "#87CEEB");
            ColorCodes.Add("teal", "#008080");
            ColorCodes.Add("turquoise", "#40E0D0");
            ColorCodes.Add("very dark gray", "#A9A9A9");
            ColorCodes.Add("white", "#FFFFFF");
            ColorCodes.Add("yellow", "#FFFF00");
            ColorCodes.Add("yellow green", "#9ACD32");

            return ColorCodes[colorName.ToLower()];
        }
    }
}
