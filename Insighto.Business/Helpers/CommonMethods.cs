﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;
using System.Drawing;


namespace Insighto.Business.Helpers
{
    public delegate void DataBindingHandler();

    public static class CommonMethods
    {
        //used by LINQ
        /// <summary>
        /// Pages the specified source.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="page">The page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        public static IEnumerable<T> Page<T>(this IEnumerable<T> source, int page = 1, int pageSize = 10)
        {
            return source.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public static string clearHTML(string HTMLcontent)
        {
            string plainHTML = "";
            if (HTMLcontent != null)
            {
                
                string pattern1html = "<(.|\n)+?>";
                plainHTML = Regex.Replace(HTMLcontent, pattern1html, string.Empty);
                string patternHtml = "<.*?>";
                
                plainHTML = Regex.Replace(plainHTML, patternHtml, string.Empty);

                
                plainHTML = plainHTML.Replace("&lt;", "<");
                plainHTML = plainHTML.Replace("&gt;", ">");
                plainHTML = plainHTML.Replace("&quot;", "\"");
                plainHTML = plainHTML.Replace("&apos;", "'");
                plainHTML = plainHTML.Replace("&amp;", "&");
                plainHTML = plainHTML.Replace("&nbsp;", " ");
                plainHTML = plainHTML.Replace("&copy;", "\u00a9");
                plainHTML = plainHTML.Replace("\r\n", " ");

            }
            return (plainHTML);
        }

        public static void ResizeImage(string OriginalFile, string NewFile, int NewWidth, int MaxHeight, bool OnlyResizeIfWider)
        {
            Image FullsizeImage = Image.FromFile(OriginalFile);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            if (OnlyResizeIfWider)
            {
                if (FullsizeImage.Width <= NewWidth)
                {
                    NewWidth = FullsizeImage.Width;
                }
            }

            int NewHeight = FullsizeImage.Height * NewWidth / FullsizeImage.Width;

            if (FullsizeImage.Height >= MaxHeight)
            {
                NewHeight = MaxHeight;
            }       
            Image NewImage = FullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

            // Clear handle to original file so that we can overwrite it if necessary
            FullsizeImage.Dispose();

            // Save resized picture
            NewImage.Save(NewFile);
        }

        //returns date by adding bias value used in survey controls
        public static DateTime GetConvertedDatetime(DateTime date, int bias, bool server_flag)
        {
            DateTime ret_value = date;
            if (date != null)
            {
                if (server_flag == true)
                    ret_value = date.AddMinutes(bias);
                else
                    ret_value = DateTime.SpecifyKind(date, DateTimeKind.Utc);

            }
            return ret_value;

        }

        #region ConvertToFlvFile
        /// <summary>
        /// Convert video file into flv format
        /// </summary>
        /// <param name="srcFilePath"></param>
        /// <param name="srcFilename"></param>
        /// <param name="targetFilePath"></param>
        /// <returns></returns>
        public static string ConvertToFlvFile(string srcFilePath, string srcFilename, string targetFilePath, string VideoType)
        {
            string standardError = "";
            string[] TempFilename = srcFilename.Split('.');

            string targetFilename = VideoType + "_" + TempFilename[0] + ".flv";
            if (!Directory.Exists(targetFilePath))
            {
                Directory.CreateDirectory(targetFilePath);
            }
            try
            {
                //LogException(System.IO.Path.Combine(srcFilePath, +'"' + srcFilename + '"'));
                string exePath = System.IO.Path.GetDirectoryName(HttpContext.Current.Request.PhysicalApplicationPath.ToString());
                System.Diagnostics.Process flvConvertProcess = new System.Diagnostics.Process();
                flvConvertProcess.StartInfo.FileName = System.IO.Path.GetDirectoryName("~/FileConverterFile/ffmpeg.exe"); //@"F:\Test\TestWindowsApplication\TestWindowsApplication\ffmpeg.exe";
                flvConvertProcess.StartInfo.Arguments = " -i  " + System.IO.Path.Combine(srcFilePath, srcFilename) + " -ar 22050 -ab 64 -f flv -s 320*240 " + System.IO.Path.Combine(targetFilePath, targetFilename);
                flvConvertProcess.StartInfo.UseShellExecute = false;
                flvConvertProcess.StartInfo.RedirectStandardError = true;
                flvConvertProcess.StartInfo.RedirectStandardOutput = true;
                flvConvertProcess.StartInfo.CreateNoWindow = true;
                flvConvertProcess.Start();
                standardError = flvConvertProcess.StandardError.ReadToEnd();
                flvConvertProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return targetFilename;
        }
        #endregion


        public static void ConvertNow(string cmd)
        {
            string exepath;
            string AppPath = HttpContext.Current.Request.PhysicalApplicationPath;
            //Get the application path 
            exepath = AppPath + "ffmpeg.exe";
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = exepath;
            //Path of exe that will be executed, only for "filebuffer" it will be "flvtool2.exe" 
            proc.StartInfo.Arguments = cmd;
            //The command which will be executed 
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.RedirectStandardOutput = false;
            proc.Start();

            while (proc.HasExited == false)
            {

            }
        }

        /// <summary>
        /// Strings to enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumString">The enum string.</param>
        /// <returns></returns>
        public static T ToEnum<T>(string enumString)
        {
            if (!string.IsNullOrEmpty(enumString) && Enum.IsDefined(typeof(T), enumString))
                return (T)Enum.Parse(typeof(T), enumString);
            return default(T);
        }

        public static T ToEnum<T>(int enumValue)
        {
            if (Enum.IsDefined(typeof(T), enumValue))
                return (T)Enum.ToObject(typeof(T), enumValue);
            return default(T);
        }

        public static List<T> EnumToList<T>()
        {

            Type enumType = typeof(T);

            // Can't use type constraints on value types, so have to do check like this
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            // ReSharper disable AssignNullToNotNullAttribute
            return new List<T>(Enum.GetValues(enumType) as IEnumerable<T>);
            // ReSharper restore AssignNullToNotNullAttribute
        }
    }
}
