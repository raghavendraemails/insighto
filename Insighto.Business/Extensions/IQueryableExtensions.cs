﻿using System;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Web.UI.WebControls;
using Insighto.Business.ValueObjects;

namespace Insighto.Business.Extensions
{
    public static class IQueryableExtensions
    {
        /// <summary>
        /// returns paginated list for ordered query
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PagedList<T> ToPagedList<T>(this IQueryable<T> query,int page, int pageSize)
        {
                var count = query.Count();
                var skip = (page-1) * pageSize;
                var entities = query.Skip(skip).Take(pageSize).ToList();

                return new PagedList<T>
                {
                    Entities = entities,
                    HasNext = (skip + pageSize < pageSize),
                    HasPrevious = (skip > 0),
                    TotalCount = count,
                    TotalPages = System.Math.Ceiling(Convert.ToDouble(count) / pageSize)
                };
            
        }

        /// <summary>
        /// return paginated list based on paging criteria
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="pagingCriteria"></param>
        /// <returns></returns>
        public static PagedList<T> ToPagedList<T>(this IQueryable<T> query, PagingCriteria pagingCriteria)
        {
            query = query.OrderBy(pagingCriteria.SortString);

            return ToPagedList<T>(query,pagingCriteria.PageIndex, pagingCriteria.PageSize);

        }

        public static IQueryable<T> Sort<T, PT>(this IQueryable<T> source, string sortExpression, SortDirection sortDirection)
        {
            var param = Expression.Parameter(typeof(T), "item");

            var sortLambda = Expression.Lambda<Func<T, PT>>(Expression.Convert(Expression.Property(param, sortExpression), typeof(PT)), param);

            if (sortDirection == SortDirection.Ascending)
            {
                return source.OfType<T>().AsQueryable<T>().OrderBy<T, PT>(sortLambda);
            }
            else
            {
                return source.OfType<T>().AsQueryable<T>().OrderByDescending<T, PT>(sortLambda);
            }
        }        
    }
}
