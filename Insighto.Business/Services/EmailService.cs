﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Exceptions;
using System.IO;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using Insighto.Business.Extensions;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using CovalenseUtilities.Helpers;



namespace Insighto.Business.Services
{
    public class EmailService : BusinessServiceBase
    {
        #region "Method:GetEmailsByContactListId"
        /// <summary>
        /// Get Emails by Contact List Id with Paging & Sorting
        /// </summary>
        /// <param name="contactListId"></param>
        /// <param name="pagedCriteria"></param>
        /// <returns></returns>
        public PagedList<osm_emaillist> GetEmailsByContactListId(int contactListId, PagingCriteria pagedCriteria)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var emailList = dbCon.osm_emaillist.Where(e => e.CONTACTLIST_ID == contactListId)
                                                   .ToPagedList(pagedCriteria);


                return emailList;
            }
        }
        #endregion

        #region "Method:GetEmailsByContactListId"
        /// <summary>
        /// Get Emails by Contact List Id with Paging & Sorting
        /// </summary>
        /// <param name="contactListId"></param>
        /// <param name="pagedCriteria"></param>
        /// <returns></returns>
        public List<osm_emaillist> GetEmailsByContactListId(int contactListId)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var emailList = dbCon.osm_emaillist.Where(e => e.CONTACTLIST_ID == contactListId)
                                                   .ToList();


                return emailList;
            }
        }
        #endregion

        #region "Method:SaveEmailContactList"
        /// <summary>
        /// To save Email Contact List By UserId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="contactListName"></param>
        /// <returns></returns>
        public osm_contactlist SaveEmailContactList(int userId, string contactListName)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var contacts = new osm_contactlist();
                var contactList = (from list in dbCon.osm_contactlist
                                   where list.CONTACTLIST_NAME == contactListName
                                         && list.CREATED_BY == userId
                                   select list).SingleOrDefault();
                if (contactList == null)
                {
                    contacts.CREATED_BY = userId;
                    contacts.CONTACTLIST_NAME = contactListName;
                    contacts.CREATED_ON = DateTime.Now;
                    contacts.LASTMODIFIED_BY = userId;
                    contacts.LAST_MODIFIED_ON = DateTime.Now;
                    contacts.NO_EMAILIDS = 0;
                    contacts.DELETED = 0;
                    dbCon.osm_contactlist.Add(contacts);
                    dbCon.SaveChanges();
                    return contacts;
                }
                else
                    return contactList;
            }
        }
        #endregion


        #region "Method:IsContactListExistsByUserId"
        /// <summary>
        /// Check whether the Contact list already exists if exists already return true else false
        /// </summary>
        /// <param name="osmFolder"></param>
        /// <returns></returns>
        public osm_contactlist IsContactListExistsByUserId(int userId, string contactListName)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {


                return null;
            }
        }
        #endregion


        #region "Method:SaveEmailsByList"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        public int SaveEmailsByList(osm_emaillist osmEmail)
        {
            bool isExist = default(bool);
            int retValue = 0;

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                isExist = IsEmailIdExistByListId(ValidationHelper.GetInteger(osmEmail.CONTACTLIST_ID, 0), osmEmail.EMAIL_ADDRESS);


                if (!isExist)
                {
                    //   var unsubscribeService = ServiceFactory.GetService<UnSubscribeList>();
                    //   isExist = unsubscribeService.GetStatus(osmEmail.CREATED_BY,osmEmail.EMAIL_ADDRESS);
                    dbCon.osm_emaillist.Add(osmEmail);
                    var contactList = dbCon.osm_contactlist.Where(c => c.CONTACTLIST_ID == osmEmail.CONTACTLIST_ID).SingleOrDefault();
                    contactList.NO_EMAILIDS = contactList.NO_EMAILIDS + 1;
                    retValue = dbCon.SaveChanges();
                }
                return retValue;

            }
        }

        #endregion

        #region "Method:IsEmailIdExistByListId"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contactListId"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool IsEmailIdExistByListId(int contactId, string emailId)
        {
            bool isExist = default(bool);
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var emailList = dbCon.osm_emaillist.Where(e => e.CONTACTLIST_ID == contactId && e.EMAIL_ADDRESS == emailId).ToList();
                if (emailList.Any())
                    isExist = true;
            }
            return isExist;
        }
        #endregion


        #region "Method:SaveAddressBookColumnsByContactListId"
        public int SaveAddressBookColumnsByContactListId(osm_contactlist contact)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                var contactList = dbCon.osm_contactlist.Where(c => c.CONTACTLIST_ID == contact.CONTACTLIST_ID).FirstOrDefault();

                contactList.CUSTOM_HEADER1 = contact.CUSTOM_HEADER1;
                contactList.CUSTOM_HEADER2 = contact.CUSTOM_HEADER2;
                contactList.CUSTOM_HEADER3 = contact.CUSTOM_HEADER3;
                contactList.CUSTOM_HEADER4 = contact.CUSTOM_HEADER4;
                contactList.CUSTOM_HEADER5 = contact.CUSTOM_HEADER5;
                contactList.CUSTOM_HEADER6 = contact.CUSTOM_HEADER6;
                contactList.CUSTOM_HEADER7 = contact.CUSTOM_HEADER7;
                contactList.CUSTOM_HEADER8 = contact.CUSTOM_HEADER8;
                return dbCon.SaveChanges();
            }

        }
        #endregion

        #region "Method:GetAddressBookByUserId"
        public PagedList<osm_contactlist> GetAddressBookByUserId(int userId, PagingCriteria pagingCriteria)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_contactlist.Where(c => c.CREATED_BY == userId)
                                      .ToPagedList(pagingCriteria);
            }
        }
        #endregion




        #region "Method:GetEmailListByEmailId"
        public osm_emaillist GetEmailListByEmailId(int emailId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_emaillist.Where(e => e.EMAIL_ID == emailId).SingleOrDefault();

            }
        }
        #endregion
        public int DeleteContactByContactListId(int contactListId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var emaillist = (from email in dbCon.osm_emaillist
                                 where email.CONTACTLIST_ID == contactListId
                                 select email).ToList();
                var contactlist = dbCon.osm_contactlist.Where(c => c.CONTACTLIST_ID == contactListId).SingleOrDefault();
                foreach (osm_emaillist email in emaillist)
                {
                    dbCon.osm_emaillist.Remove(email);
                }

                dbCon.osm_contactlist.Remove(contactlist);
                return dbCon.SaveChanges();
            }
        }

        #region "Method:DeleteEmailById"
        public int DeleteEmailById(int emailId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var email = dbCon.osm_emaillist.Where(e => e.EMAIL_ID == emailId).SingleOrDefault();
                dbCon.osm_emaillist.Remove(email);
                UpdateNoOfEmailIds(ValidationHelper.GetInteger(email.CONTACTLIST_ID, 0));
                return dbCon.SaveChanges();
            }
        }
        #endregion
        public void UpdateNoOfEmailIds(int CONTACTLIST_ID)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var list = dbCon.osm_contactlist.Where(c => c.CONTACTLIST_ID == CONTACTLIST_ID).SingleOrDefault();
                list.NO_EMAILIDS = list.NO_EMAILIDS - 1;
                dbCon.SaveChanges();
            }
        }


        #region "Method:UpdateEmailsByList"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        public int UpdateEmailsByList(osm_emaillist emailList)
        {
            int rtnValue = 0;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var email = dbCon.osm_emaillist.Where(e => e.EMAIL_ADDRESS == emailList.EMAIL_ADDRESS && e.CONTACTLIST_ID == emailList.CONTACTLIST_ID && e.EMAIL_ID != emailList.EMAIL_ID).ToList();

                if (email.Count < 1)
                {
                    var emailInfo = dbCon.osm_emaillist.Where(e => e.EMAIL_ID == emailList.EMAIL_ID).SingleOrDefault();

                    emailInfo.EMAIL_ADDRESS = emailList.EMAIL_ADDRESS;
                    emailInfo.MODIFIED_ON = emailList.MODIFIED_ON;
                    emailInfo.FIRST_NAME = emailList.FIRST_NAME;
                    emailInfo.LAST_NAME = emailList.LAST_NAME;
                    emailInfo.CUSTOM_VAR1 = emailList.CUSTOM_VAR1;
                    emailInfo.CUSTOM_VAR2 = emailList.CUSTOM_VAR2;
                    emailInfo.CUSTOM_VAR3 = emailList.CUSTOM_VAR3;
                    emailInfo.CUSTOM_VAR4 = emailList.CUSTOM_VAR4;
                    emailInfo.CUSTOM_VAR5 = emailList.CUSTOM_VAR5;
                    emailInfo.CUSTOM_VAR6 = emailList.CUSTOM_VAR6;
                    emailInfo.CUSTOM_VAR7 = emailList.CUSTOM_VAR7;
                    emailInfo.CUSTOM_VAR8 = emailList.CUSTOM_VAR8;
                    dbCon.SaveChanges();
                    rtnValue = 1;

                }
                return rtnValue;
            }

        }

        #endregion


        #region "Method:GetContactListByContactListId"
        public List<osm_contactlist> GetContactListByContactListId(int contactListId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_contactlist.Where(c => c.CONTACTLIST_ID == contactListId).ToList();

            }
        }
        #endregion


        #region "Method:FindEmailsByContactListId"
        /// <summary>
        /// Get Emails by Contact List Id with Paging & Sorting
        /// </summary>
        /// <param name="contactListId"></param>
        /// <param name="pagedCriteria"></param>
        /// <returns></returns>
        public List<osm_emaillist> FindEmailsByContactListId(int contactListId)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var emailList = dbCon.osm_emaillist.Where(e => e.CONTACTLIST_ID == contactListId).OrderBy("EMAIL_ADDRESS").ToList();
                return emailList;
            }
        }
        #endregion


        #region "Method:FindDetailsByEmailId"
        /// <summary>
        /// Get contact List details by Email Id 
        /// </summary>
        /// <param name="contactListId"></param>      
        /// <returns></returns>
        public List<osm_emaillist> FindDetailsByEmailId(int emailId)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var emailList = dbCon.osm_emaillist.Where(e => e.EMAIL_ID == emailId).ToList();
                return emailList;
            }
        }
        #endregion

        #region "Method:UpdateUnsubscribeStatus"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        public int UpdateUnsubscribeStatus(string emailAddress, int userId)
        {
            int rtnValue = 0;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var email = dbCon.osm_emaillist.Where(e => e.EMAIL_ADDRESS == emailAddress && e.CREATED_BY == userId).ToList();

                if (email != null)
                {
                    foreach (var cl in email)
                    {
                        cl.STATUS = "Unsubscribe";
                        dbCon.SaveChanges();
                    }

                }
                return rtnValue;
            }

        }

        #endregion

        /// <summary>
        /// Gets the bounced emails.
        /// </summary>
        /// <param name="pagedCriteria">The paged criteria.</param>
        /// <param name="surveyId">The survey id.</param>
        /// <returns></returns>
        public PagedList<osm_emaillist> GetBouncedEmails(PagingCriteria pagedCriteria, int surveyId = 0)
        {
            PagedList<osm_emaillist> emailList = new PagedList<osm_emaillist>();
            emailList.Entities = new List<osm_emaillist>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var bouncedEmails = surveyId > 0 ? dbCon.osm_bouncedmailList.Where(b => b.SURVEY_ID == surveyId).ToList() : dbCon.osm_bouncedmailList.ToList();
                if (bouncedEmails != null)
                    foreach (var item in bouncedEmails)
                    {
                        var email = dbCon.osm_emaillist.Where(e => e.EMAIL_ID == item.EMAIL_ID).FirstOrDefault();
                        if (email == null)
                            continue;
                        emailList.Entities.Add(email);
                    }
                return emailList;
            }
        }

        #region "Method:SearchEmailAddress"
        /// <summary>
        /// Search Email Address
        /// </summary>
        /// <param name="contactListId"></param>      
        /// <returns></returns>
        public PagedList<osm_emaillist> SearchEmailAddress(int contactListId, string searchText, PagingCriteria pagedCriteria)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_emaillist.Where(e => e.CONTACTLIST_ID == contactListId && (e.EMAIL_ADDRESS.Contains(searchText) || e.FIRST_NAME.Contains(searchText) || e.LAST_NAME.Contains(searchText))).ToPagedList(pagedCriteria);
                
            }
        }
        #endregion

        

    }
}
