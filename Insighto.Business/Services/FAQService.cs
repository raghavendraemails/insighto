﻿using System;
using System.Collections.Generic;
using System.Linq;
using CovalenseUtilities.Services;
using Insighto.Business.Extensions;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using System.Collections;

namespace Insighto.Business.Services
{
    public class FAQService : BusinessServiceBase
    {
        #region "Method : GetPickList"
        /// <summary>
        /// Used to get survey category list.
        /// </summary>
        /// <param name="osmPickList"></param>
        /// <returns></returns>

       public List<osm_faqpages> GetFAQPages(osm_faqpages osmPages)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var faqPageList = (from faqPages in dbCon.osm_faqpages  
                                   where faqPages.Page_Name_Group  == osmPages.Page_Name_Group
                                   select faqPages).ToList();

                return faqPageList;
            }

        }

        #endregion

       public List<osm_faqpages> GetSubPages(string parentName)
       {
           using (InsightoV2Entities dbCon = new InsightoV2Entities())
           {
               var childPages = dbCon.osm_faqpages.Where(s => s.Page_Name_Parent == parentName).ToList();

               return childPages;

           }
       }

       public osm_faqpages  GetPageURL(string pageName,string parentNode)
       {
           using (InsightoV2Entities dbCon = new InsightoV2Entities())
           {
               return dbCon.osm_faqpages.FirstOrDefault(f => f.Page_Name == pageName.Trim() && f.Page_Name_Parent == parentNode.Trim());
           }
       }

     
       public osm_faqpages  Update(osm_faqpages  faqPage)
       {
           using (InsightoV2Entities dbCon = new InsightoV2Entities())
           {
               var faqPageInfo = dbCon.osm_faqpages.Where(h => h.Page_Name == faqPage.Page_Name && h.Page_Name_Group == faqPage.Page_Name_Group && h.Page_Name_Parent == faqPage.Page_Name_Parent).FirstOrDefault();
               faqPageInfo.Page_Name = faqPage.Page_Name  ;
               faqPageInfo.Page_Sort = faqPageInfo.Page_Sort ;
               faqPageInfo.Page_Content  = faqPage.Page_Content;
               faqPageInfo.Page_Name_Group = faqPageInfo.Page_Name_Group;
               faqPageInfo.Page_URL = faqPageInfo.Page_URL;
               faqPageInfo.Page_Name_Parent = faqPageInfo.Page_Name_Parent;   
               dbCon.SaveChanges();
                return faqPageInfo;
           }

      }

       

    }
}
