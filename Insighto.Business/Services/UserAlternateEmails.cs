﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Exceptions;
using System.IO;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using System.Web;
using CovalenseUtilities.Helpers;
using Insighto.Business.Extensions;

namespace Insighto.Business.Services
{
    public class UserAlternateEmails : BusinessServiceBase
    {
        public bool FindEmailExist(string email)
        {
            bool isExist = default(bool);
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var alternateEmails = dbCon.osm_useralternateemails.Where(u => u.EMAIL == email).SingleOrDefault();
                if (alternateEmails != null)
                    isExist = true;
                else
                {
                    var user = dbCon.osm_user.Where(u => u.LOGIN_NAME == email).SingleOrDefault();
                    if (user != null)
                        isExist = true;
                }
            }
            return isExist;
        }

        public int SaveAlternateEmails(osm_useralternateemails emails)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                
                dbCon.osm_useralternateemails.Add(emails);
                dbCon.SaveChanges();
                return emails.ID;
            }        
        }

        public void DeleteEmailsByUserId(int userId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
               var emailList = dbCon.osm_useralternateemails.Where(u=> u.USERID == userId).ToList();
               foreach (var email in emailList)
               {
                   dbCon.osm_useralternateemails.Remove(email);
                   dbCon.SaveChanges();
               }              
            }

        }
    }
}
