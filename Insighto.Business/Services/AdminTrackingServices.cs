﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Services;
using Insighto.Business.Extensions;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;

namespace Insighto.Business.Services
{
    public class AdminTrackingServices : BusinessServiceBase
    {
        public PagedList<AdminTrackingInfo> FindAdminTracking(PagingCriteria pagingCriteria)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var trakingInfo = (from u in dbCon.osm_adminuser
                              join t in dbCon.osm_admintracking 
                              on u.ADMINUSERID equals t.ADMINID
                              select new AdminTrackingInfo
                           {
                               TRACKINGID = t.TRACKINGID,
                               ADMINID = t.ADMINID,
                               CREATED_ON = t.CREATED_ON,
                               ACTION_DONE_ON_PAGE = t.ACTION_DONE_ON_PAGE,
                               EVENT_ACTION_DESCRIPTION = t.EVENT_ACTION_DESCRIPTION,
                               FKUSERID = t.FKUSERID,
                               LOGIN_NAME = u.LOGIN_NAME,
                               NAME = u.FIRST_NAME + u.LOGIN_NAME
                           }).ToPagedList(pagingCriteria);

                return trakingInfo;
               
                     
            }
        }

        public osm_admintracking InsertAdminTracking(osm_admintracking admintracking)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_admintracking.Add(admintracking);
                int id = dbCon.SaveChanges();
                dbCon.SaveChanges();
                return admintracking;
            }
        }
    }
}
