﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Services;

namespace Insighto.Business.Services
{
    public class NetworkShareService : BusinessServiceBase
    {
        public NetworkShareCodes AddNetworkTrial(int insighto_user_ID, string network_name, string social_login_id, 
                                                int social_count, int premium_days)
        {
            // Check if an entry exists with shared status
            SqlConnection sqlconn;
            SqlCommand sqlcomm;
            string connect = ConfigurationManager.ConnectionStrings["InsightoConnString"].ToString();
            int result = 0, count=0;
            String errMessage = "OK";
            NetworkShareCodes SC = NetworkShareCodes.Shared;

            sqlconn = new SqlConnection(connect);
            sqlconn.Open();
            sqlcomm = new SqlCommand();
            sqlcomm.Connection = sqlconn;
            sqlcomm.CommandType = CommandType.StoredProcedure;
            sqlcomm.CommandText = "SP_GET_NETWORK_SHARE_STATUS";
            
            // Return value is count
            SqlParameter returnValue = new SqlParameter("@cnt", SqlDbType.Int);
            returnValue.Direction = ParameterDirection.ReturnValue;
            sqlcomm.Parameters.Add(returnValue);
            sqlcomm.Parameters.Add(new SqlParameter("@insighto_user_ID", SqlDbType.Int)).Value = insighto_user_ID;
            sqlcomm.Parameters.Add(new SqlParameter("@network_name", SqlDbType.NVarChar)).Value = network_name;
            sqlcomm.Parameters.Add(new SqlParameter("@social_login_id", SqlDbType.NVarChar)).Value = social_login_id;
            sqlcomm.Parameters.Add(new SqlParameter("@share_status", SqlDbType.SmallInt)).Value = NetworkShareCodes.Shared;

            try
            {
                result = sqlcomm.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                errMessage = ex.Message;
                sqlcomm.Dispose();
                sqlconn.Close();
                return NetworkShareCodes.Error;
            }

            sqlcomm.Dispose();

            // Check if record exisits
            count = Convert.ToInt32(returnValue.Value);
            if (count == 1)
                SC = NetworkShareCodes.Reshared;

            sqlcomm = new SqlCommand();
            sqlcomm.Connection = sqlconn;
            sqlcomm.CommandType = CommandType.StoredProcedure;
            sqlcomm.CommandText = "SP_INSERT_NETWORK_SHARE";

            sqlcomm.Parameters.Add(new SqlParameter("@insighto_user_ID", SqlDbType.Int)).Value = insighto_user_ID;
            sqlcomm.Parameters.Add(new SqlParameter("@network_name", SqlDbType.NVarChar)).Value = network_name;
            sqlcomm.Parameters.Add(new SqlParameter("@social_login_id", SqlDbType.NVarChar)).Value = social_login_id;
            sqlcomm.Parameters.Add(new SqlParameter("@social_count", SqlDbType.Int)).Value = social_count;
            sqlcomm.Parameters.Add(new SqlParameter("@premium_days", SqlDbType.SmallInt)).Value = premium_days;
            sqlcomm.Parameters.Add(new SqlParameter("@share_status", SqlDbType.SmallInt)).Value = SC;

            try
            {
                result = sqlcomm.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                errMessage = ex.Message;
                sqlcomm.Dispose();
                sqlconn.Close();
                return NetworkShareCodes.Error;
            }

            sqlcomm.Dispose();

            if (SC == NetworkShareCodes.Reshared)
            {
                sqlconn.Close();
                return NetworkShareCodes.Reshared; // can't offer premium
            }

            // Update user table for premium
            sqlcomm = new SqlCommand();
            sqlcomm.Connection = sqlconn;
            sqlcomm.CommandType = CommandType.StoredProcedure;
            sqlcomm.CommandText = "SP_NETWORK_UPDATE_OSM_USER";

            sqlcomm.Parameters.Add(new SqlParameter("@userID", SqlDbType.Int)).Value = insighto_user_ID;
            sqlcomm.Parameters.Add(new SqlParameter("@premium_days", SqlDbType.SmallInt)).Value = premium_days;

            try
            {
                result = sqlcomm.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                errMessage = ex.Message;
                sqlcomm.Dispose();
                sqlconn.Close();
                return NetworkShareCodes.Error;
            }

            sqlcomm.Dispose();
            sqlconn.Close();
            return NetworkShareCodes.Shared;
        }
    }
}
