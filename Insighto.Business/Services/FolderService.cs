﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using Insighto.Exceptions;
using Insighto.Business.Extensions;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using System.Collections;

namespace Insighto.Business.Services
{
    public class FolderService : BusinessServiceBase
    {
        public int SurveyFlag
        {
            get
            {
                var key = QueryStringHelper.GetString("key");
                if (String.IsNullOrEmpty(key))
                    return 0;

                Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
                if (ht != null && ht.Contains("surveyFlag"))
                {
                    return ValidationHelper.GetInteger(ht["surveyFlag"], 0);
                }
                return 0;
            }

        }

        #region GetPagedList
        public PagedList<FolderInfo> GetFoldersByUserId(int userId, PagingCriteria pagingCriteria,int surveyFlag)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var folders = dbCon.osm_cl_folders.Where(f => f.USERID == userId && f.DELETED == 0)
                                                    .ToPagedList(pagingCriteria);

                var pagedFolderInfo = new PagedList<FolderInfo>();
                var folderInfo = new List<FolderInfo>();
                foreach (var f in folders.Entities)
                {
                    var folder = new FolderInfo();
                    folder.FOLDER_ID = f.FOLDER_ID;
                    folder.FOLDER_NAME = f.FOLDER_NAME;
                    string editUrl = EncryptHelper.EncryptQuerystring("ManageFolderPopUp.aspx", "FolderId=" + f.FOLDER_ID.ToString() + "&surveyFlag=" + surveyFlag);
                    string viewUrl = EncryptHelper.EncryptQuerystring("ViewFolderPopUp.aspx", "FolderId=" + f.FOLDER_ID.ToString() + "&surveyFlag=" + surveyFlag);
                    folder.editUrl = editUrl;
                    folder.viewUrl = viewUrl;
                    folderInfo.Add(folder);
                }
                pagedFolderInfo.Entities = folderInfo;
                pagedFolderInfo.HasNext = folders.HasNext;
                pagedFolderInfo.HasPrevious = folders.HasPrevious;
                pagedFolderInfo.TotalCount = folders.TotalCount;
                pagedFolderInfo.TotalPages = folders.TotalPages;
                return pagedFolderInfo;

            }
        }
        #endregion


        #region "Method : GetFoldersByUserId"
        /// <summary>
        /// Used to get folder details by UserId.
        /// </summary>
        /// <param name="osmFolder"></param>
        /// <returns></returns>
        /// 

        public osm_cl_folders GetFoldersByUserId(int userId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var folders = dbCon.osm_cl_folders.Where(f => f.USERID == userId && f.DELETED == 0).FirstOrDefault();
                 return folders;
            }

        }
        #endregion

        public List<osm_cl_folders> GetFolders(osm_cl_folders osmFolder)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

              var folders =  (from folder in dbCon.osm_cl_folders
                 where folder.USERID == osmFolder.USERID && folder.DELETED == 0
                 orderby folder.FOLDER_NAME
                 select folder).ToList();
              foreach (var a in folders)
              {
                  a.FOLDER_NAME = System.Web.HttpContext.Current.Server.HtmlDecode(a.FOLDER_NAME);
              }
                return folders;
            }

        }

        //(from folder in dbCon.osm_cl_folders
        //           where folder.USERID == osmFolder.USERID && folder.DELETED == 0
        //           orderby folder.FOLDER_NAME
        //           select folder).ToList();

        #region "Method:IsFolderExsists"
        /// <summary>
        /// Check whether the folder already exists if exists already return true else false
        /// </summary>
        /// <param name="osmFolder"></param>
        /// <returns></returns>
        public bool  IsFolderExsists(osm_cl_folders osmFolder)
        {
            bool isFolder = default(bool);
           
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var folders = (from folder in dbCon.osm_cl_folders
                                where folder.FOLDER_NAME == osmFolder.FOLDER_NAME && folder.DELETED == 0 && folder.USERID == osmFolder.USERID
                                select folder.FOLDER_NAME).ToList();

                if (folders.Count > 0)
                {
                    isFolder = false;
                }
                else
                {
                    isFolder = true;
                }
                    
            }           

            return isFolder;
        }
        #endregion

        #region "Mentod: AddFolder"
        public osm_cl_folders AddFolder(osm_cl_folders osmFolder)
        {
            
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_cl_folders.Add(osmFolder);
                dbCon.SaveChanges();
            }
            return osmFolder;
            
        }
        #endregion

        #region "Method: UpdateFolder"
        public osm_cl_folders UpdateFolder(osm_cl_folders osmFolder)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())            
            {
                var folder = dbCon.osm_cl_folders.Where
                    (
                        f => f.FOLDER_ID != osmFolder.FOLDER_ID &&
                           f.FOLDER_NAME == osmFolder.FOLDER_NAME && f.USERID == osmFolder.USERID && f.DELETED == 0
                    ).SingleOrDefault();

                if (folder == null)
                {
                   folder = dbCon.osm_cl_folders.Where
                    (
                        f => f.FOLDER_ID == osmFolder.FOLDER_ID && f.USERID == osmFolder.USERID
                    ).SingleOrDefault();
                    folder.FOLDER_NAME = osmFolder.FOLDER_NAME;
                    dbCon.SaveChanges();
                    return folder;
                }
                else
                {
                    return null;
                }          
             
            }
           

        }
        #endregion

        #region "Method : GetFolders"
        /// <summary>
        /// Used to get folder details.
        /// </summary>
        /// <param name="osmFolder"></param>
        /// <returns></returns>

        public osm_cl_folders GetFoldersByFolderID(osm_cl_folders osmFolder)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

          
                var folders = (from folder in dbCon.osm_cl_folders
                               where folder.FOLDER_ID == osmFolder.FOLDER_ID && folder.DELETED == 0
                               select folder).FirstOrDefault();

                return folders;
            }
        }
        #endregion

        #region "Method : DeleteFolderByFolderId
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <returns></returns>
        #endregion
        public int DeleteFolderByFolderId(int folderId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var folders = dbCon.osm_survey.Where(s => s.FOLDER_ID == folderId && s.DELETED == 0).ToList();
                if (folders.Any())
                {
                    return 0;
                }
                else
                {
                    var folderList = (from folder in dbCon.osm_cl_folders
                                        where folder.FOLDER_ID == folderId
                                        select folder).SingleOrDefault();                   
                    folderList.DELETED = 1;
                    return dbCon.SaveChanges();
                }
            }
        }
    }
}
