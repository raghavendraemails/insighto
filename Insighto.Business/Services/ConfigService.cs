﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Exceptions;
using CovalenseUtilities.Services;

namespace Insighto.Business.Services
{
    public class ConfigService : BusinessServiceBase
    {
        #region "Method : Get Config Values"
        /// <summary>
        /// Used for AGet the confi values. 
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public  List<oms_config> GetConfigurationValues(List<string> configKeyValue)
        {
            var config=new List<oms_config>();                     
               
                using (InsightoV2Entities dbCon = new InsightoV2Entities())
                {
                   
                     config = (from configkey in dbCon.oms_config
                                  where configKeyValue.Contains(configkey.CONFIG_KEY) && configkey.DELETED == 0
                               orderby configkey.CONFIG_KEY ascending
                                  select configkey).ToList();     
                }
          
            return config;
        }

        public  List<oms_config> GetConfigurationValues()
        {           
                using (InsightoV2Entities dbCon = new InsightoV2Entities())
                {
                    return dbCon.oms_config.ToList();
                }           
        }

        #endregion

        public int SaveConfigurationValues(oms_config config)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var configRecord = dbCon.oms_config.Where(c => c.CONFIG_KEY == config.CONFIG_KEY && c.DELETED == 0).SingleOrDefault();
                configRecord.CONFIG_VALUE = config.CONFIG_VALUE;
                configRecord.FromDate = config.FromDate;
                configRecord.ToDate = config.ToDate;
                return dbCon.SaveChanges();
            }
        }

       public string GetConfigurationValueByDate(DateTime dt,string configKey)
        {
            var configValue = string.Empty;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var config = dbCon.oms_config.Where(c => c.CONFIG_KEY == configKey && c.DELETED == 0).SingleOrDefault();
                if ((config.FromDate == null && config.ToDate == null) || (config.FromDate <= dt && config.ToDate > dt))
                {
                    configValue = config.CONFIG_VALUE;
                }
               
            }
            return configValue;
        }

       
    }
}
