﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Extensions;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Net;


namespace Insighto.Business.Services
{
    public class SurveyService : BusinessServiceBase
    {
     public int SurveyFlag
        {
            get
            {
                var key = QueryStringHelper.GetString("key");
                if (String.IsNullOrEmpty(key))
                    return 0;

                Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
                if (ht != null && ht.Contains("surveyFlag"))
                {
                    return ValidationHelper.GetInteger(ht["surveyFlag"], 0);
                }
                return 0;
            }

        }

        #region "Method: TestPagingAndSorting"
        public PagedList<osm_survey> TestPagingAndSorting(int page, int pageSize = 10)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var query1 = dbCon.osm_survey.Select("new(SURVEY_NAME As SName, USERID)")
                             .OrderBy("SURVEY_NAME DESC");
                var query = dbCon.osm_survey.OrderBy("SURVEY_NAME DESC");
                var pagedList = query.ToPagedList(page, pageSize);
                return pagedList;
            }

        }
        #endregion
     
        #region "Method: GetSurveyBySurveyId"
        public List<osm_survey> GetSurveyBySurveyId(int SurveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_survey.Where(survey => survey.SURVEY_ID == SurveyId).ToList();
            }

        }
        #endregion

        #region "Method: GetSurveys"
        public List<osm_survey> GetSurveys()
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveys = (from survey in dbCon.osm_survey
                               where survey.USERID != -2 && survey.DELETED == 0
                               select survey).ToList();

                return surveys;
            }

        }
        #endregion

        #region FindSurveys
        public PagedList<SurveyDashboardView> FindSurveys(int userId, PagingCriteria pagingCriteria, int FolderId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveys = dbCon.SurveyDashboardView.OrderBy(s => s.SURVEY_ID).Where(s => s.USERID == userId && s.FOLDER_ID == FolderId && s.DELETED == 0);
                // .ToPagedList(pagingCriteria);
                surveys = from p in surveys
                          group p by p.SURVEY_ID into grp
                          select grp.OrderByDescending(g => g.LAUNCH_ID).FirstOrDefault();

                var pagedList = surveys.ToPagedList(pagingCriteria);


                return pagedList;
            }

        }
        #endregion

        #region  FindSurveysByCategory
        public PagedList<SurveyDashboardView> FindSurveysByCategory(int userId, PagingCriteria pagingCriteria, string category)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveys = dbCon.SurveyDashboardView.Where(s => s.USERID == userId && s.SURVEY_CATEGORY == category && s.DELETED == 0);
                surveys = from p in surveys
                          group p by p.SURVEY_ID into grp
                          select grp.OrderByDescending(g => g.LAUNCH_ID).FirstOrDefault();
                var pagedList = surveys.ToPagedList(pagingCriteria);
                return pagedList;
            }

        }
        #endregion


        #region GetSurveysvoice
        public PagedList<SurveyInfo> GetSurveysvoice(int userId, string status, PagingCriteria pagingCriteria, int widgetFlag = 0, int top = 0)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveys = dbCon.SurveyDashboardView.OrderBy(s => s.SURVEY_ID).Where(s => s.SurveyWidgetFlag == widgetFlag &&
                                                                    s.DELETED == 0 &&
                                                                    s.USERID == userId);
                if (status != "All")
                    surveys = surveys.Where(s => s.STATUS == status);
                //surveys = from p in surveys
                //          group p by p.SURVEY_ID into grp
                //          select grp.OrderByDescending(g => g.LAUNCH_ID).FirstOrDefault();               
                var pagedList = surveys.ToPagedList(pagingCriteria);
                return GetSurveyInfoListvoice(pagedList, widgetFlag);
            }
        }
        #endregion

        [Flags]
        enum LaunchMethod
        {
            Emailthr = 1,
            Webthr = 2,
            Embedthr = 4,
            Fbthr = 8,
            Twthr = 16
        }


        public PagedList<SurveyInfo> GetSurveyInfoListvoice(PagedList<SurveyDashboardView> surveyPagedList, int widgetFlag)
        {
            var pagedListInfo = new PagedList<SurveyInfo>();
            var listInfo = new List<SurveyInfo>();
            foreach (var survey in surveyPagedList.Entities)
            {
                var surveyInfo = new SurveyInfo();
                //todo: load the survey info object
                string strEditSurvey = "";
                string strReportUrl = "";
                if (survey.STATUS == "Active")
                {
                    //strEditSurvey = "ActiveEdit:" + EncryptHelper.EncryptQuerystring("SurveyMiddlePage.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    //surveyInfo.ASEditUrl = strEditSurvey;

                    strReportUrl = "Report:" + EncryptHelper.EncryptQuerystring("Reports.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    surveyInfo.ReportUrl = strReportUrl;

                    strEditSurvey = "ActiveEdit:#";
                    surveyInfo.ASEditUrl = strEditSurvey;


                }
                else if (survey.STATUS == "Draft")
                {
                    //strEditSurvey = "ActiveEdit:" + EncryptHelper.EncryptQuerystring("SurveyMiddlePage.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    //surveyInfo.ASEditUrl = strEditSurvey;

                    strReportUrl = "Report:" + EncryptHelper.EncryptQuerystring("Reports.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    surveyInfo.ReportUrl = strReportUrl;

                    strEditSurvey = "ActiveEdit:#";
                    surveyInfo.ASEditUrl = strEditSurvey;
                }
                else if (survey.STATUS == "Closed")
                {
                    strReportUrl = "Report:" + EncryptHelper.EncryptQuerystring("Reports.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    surveyInfo.ReportUrl = strReportUrl;

                    strEditSurvey = "ActiveEdit:#";
                    surveyInfo.ASEditUrl = strEditSurvey;
                }

                if (survey.LAUNCHED_ON == null)
                {
                    surveyInfo.Launchflag = 0;
                    surveyInfo.Launchon = null;
                }
                else
                {
                    surveyInfo.Launchflag = 1;
                    DateTime dt = Convert.ToDateTime(survey.LAUNCHED_ON.ToString());
                    dt = dt.AddMinutes(330);
                    string launchOn = Convert.ToDateTime(dt).ToString("dd-MMM-yyyy");
                    surveyInfo.Launchon = Convert.ToDateTime(launchOn);
                }
                surveyInfo.Preview = "Preview";
                string strPreviewSurvey = "Preview:" + EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewURL().Replace("~/", ""), "Mode=" + RespondentDisplayMode.PreviewWithPagination.ToString() + "&SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag + "&Layout=" + survey.SURVEY_LAYOUT);
                surveyInfo.PreviewLink = strPreviewSurvey;
                string strRemainderSurvey = "";
                if (survey.STATUS == "Active")
                {
                    //if (Convert.ToInt32(survey.EMAILLIST_TYPE.ToString()) == 1)
                    //{
                    //    strRemainderSurvey = "Remainder:" + EncryptHelper.EncryptQuerystring("SendReminder.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    //    surveyInfo.SendRemainderUrl = strRemainderSurvey;
                    //    surveyInfo.SendRemainderText = "Reminders";
                    //}
                    //else
                    //{
                    strRemainderSurvey = "Remainder:#";
                    surveyInfo.SendRemainderUrl = strRemainderSurvey;


                    // }
                }
                else
                {
                    strRemainderSurvey = "Remainder:#";
                    surveyInfo.SendRemainderUrl = strRemainderSurvey;
                }

                surveyInfo.SurveyEditUrl = "";


                string Nvurlstr = EncryptHelper.EncryptQuerystring("SurveyManager.aspx", "SurId=" + survey.SURVEY_ID.ToString() + "&SurName=" + survey.SURVEY_NAME.ToString() + "&surveyFlag=" + widgetFlag);
                surveyInfo.SurveyUrl = Nvurlstr;

                string SurveyManagerUrl = EncryptHelper.EncryptQuerystring("SurveyManager.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                surveyInfo.SurveyManagerUrl = String.Format("<a href='{0}'>{1}</a>", SurveyManagerUrl, survey.SURVEY_NAME.ToString());


                string Navurlstr = EncryptHelper.EncryptQuerystring("ManageFolders.aspx", "FId=" + survey.FOLDER_ID.ToString() + "&SId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                surveyInfo.Url = Navurlstr;

                string launchUrl = "";
                if (survey.STATUS == "Active" || survey.STATUS == "Closed" || survey.STATUS == "Draft")
                {
                    strReportUrl = "Report:" + EncryptHelper.EncryptQuerystring("Reports.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    surveyInfo.ReportUrl = strReportUrl;
                }
                if (survey.STATUS == "Draft")
                {
                    //strReportUrl = "Report:#";
                    //surveyInfo.ReportUrl = strReportUrl;
                    //surveyInfo.LaunchUrl = launchUrl;
                    //launchUrl = "Launch:" + EncryptHelper.EncryptQuerystring(PathHelper.GetLaunchSurveyURL().Replace("~/", ""), "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    strReportUrl = "Report:" + EncryptHelper.EncryptQuerystring("Reports.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    surveyInfo.ReportUrl = strReportUrl;
                    launchUrl = "Launch:#";
                }
                else
                {
                    launchUrl = "Launch:#";
                }

               // surveyInfo.voicestatus = "VOICE";
                //added by krishna reddy for binding links in mysurvey page
                //+ "~" + strDraftEdit
                surveyInfo.appendedUrls = survey.STATUS + "~" + survey.SURVEY_ID + "~" + strEditSurvey + "~" + strPreviewSurvey + "~" + strReportUrl + "~" + strRemainderSurvey + "~" + launchUrl + "~" + "VOICE";

                //surveyInfo.appendedUrls = survey.STATUS + "~" + survey.SURVEY_ID +  "~" + strPreviewSurvey + "~" + strReportUrl + "~" + strRemainderSurvey + "~" + launchUrl;


                surveyInfo.FOLDER_ID = survey.FOLDER_ID;
                surveyInfo.SurveyID = survey.SURVEY_ID;
                surveyInfo.CHECK_BLOCKWORDS = survey.CHECK_BLOCKWORDS;
                surveyInfo.SURVEY_NAME = survey.SURVEY_NAME;
                surveyInfo.TOTAL_RESPONSES = survey.TOTAL_RESPONSES;
                surveyInfo.SEND_TO = survey.SEND_TO;
                surveyInfo.RESPONSE_RATE = survey.RESPONSE_RATE;
                surveyInfo.NAME = survey.NAME;
                surveyInfo.USERID = survey.USERID;
                surveyInfo.CREATED_ON = survey.CREATED_ON;
                surveyInfo.MODIFIED_ON = Convert.ToDateTime(survey.MODIFIED_ON).ToString("dd-MMM-yyyy");
                //surveyInfo.LAUNCHED_ON = survey.LAUNCHED_ON;
                surveyInfo.Complete = survey.Complete;
                surveyInfo.Partial = survey.Partial;
                surveyInfo.EMAILLIST_TYPE = survey.EMAILLIST_TYPE;
                surveyInfo.Status = survey.STATUS;
                surveyInfo.FOLDER_NAME = survey.FOLDER_NAME;
                surveyInfo.SURVEY_CATEGORY = survey.SURVEY_CATEGORY;

                listInfo.Add(surveyInfo);

            }
            pagedListInfo.Entities = listInfo;
            pagedListInfo.HasNext = surveyPagedList.HasNext;
            pagedListInfo.HasPrevious = surveyPagedList.HasPrevious;
            pagedListInfo.TotalCount = surveyPagedList.TotalCount;
            pagedListInfo.TotalPages = surveyPagedList.TotalPages;
            return pagedListInfo;
        }

        public DataSet getSurveyType(int surveyid)
        {
            try
            {
                SqlConnection con = new SqlConnection();

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_getSurveyType", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dssurveytype = new DataSet();
                sda.Fill(dssurveytype);
                con.Close();
                return dssurveytype;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int getLaunchTypeNumber(int surveyid)
        {
            try
            {
                SqlConnection con = new SqlConnection();

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_getLaunchTypeNumber", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dsupdatesurveylaunch = new DataSet();
                sda.Fill(dsupdatesurveylaunch);
                con.Close();
                int number = Convert.ToInt32(dsupdatesurveylaunch.Tables[0].Rows[0]["launchsurveytype"]);
                return number;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region GetSurveys
        public PagedList<SurveyInfo> GetSurveys(int userId, string status,string searchKey,PagingCriteria pagingCriteria, int widgetFlag = 0, int top = 0)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (searchKey != null && searchKey != "")
            {
                var surveys = dbCon.SurveyDashboardView.OrderBy(s => s.SURVEY_ID).Where(s => s.SurveyWidgetFlag == widgetFlag && 
                                                                    s.DELETED == 0 &&
                                                                    s.USERID == userId && (s.SURVEY_NAME.Contains(searchKey)||s.STATUS.Contains(searchKey)) );
                if (status != "All")
                    surveys = surveys.Where(s => s.STATUS == status);
                //surveys = from p in surveys
                //          group p by p.SURVEY_ID into grp
                //          select grp.OrderByDescending(g => g.LAUNCH_ID).FirstOrDefault();               
                var pagedList = surveys.ToPagedList(pagingCriteria);
                return GetSurveyInfoList(pagedList, widgetFlag);
             }
              else
              {
               var surveys = dbCon.SurveyDashboardView.OrderBy(s => s.SURVEY_ID).Where(s => s.SurveyWidgetFlag == widgetFlag &&
                                                   s.DELETED == 0 &&
                                                   s.USERID == userId);
                if (status != "All")
                 surveys = surveys.Where(s => s.STATUS == status);              
                var pagedList = surveys.ToPagedList(pagingCriteria);
                return GetSurveyInfoList(pagedList, widgetFlag);
               }
            }
        }
        #endregion

        public DataSet GetPollsAll(int userid)
        {
            try
            {
                SqlConnection con = new SqlConnection();
                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetPollManagerInfo", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dspoll = new DataSet();
                //DataTable dt = new DataTable();
                sda.Fill(dspoll);
                con.Close();
                //dt = dspoll.Tables[0];
                return dspoll;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public DataTable getPollThemes(int userid)
        {
            try
            {
                SqlConnection con = new SqlConnection();
                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetPollManagerInfo", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dspoll = new DataSet();
                DataTable dt = new DataTable();
                sda.Fill(dspoll);
                con.Close();
                dt = dspoll.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public DataSet getPollVisitsInfo(int pollid)
        {
            try
            {
                SqlConnection con = new SqlConnection();
                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetPollVisitsInfo", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dspoll = new DataSet();
                DataTable dt = new DataTable();
                sda.Fill(dspoll);
                con.Close();
                return dspoll;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getPollManagerInfoStatus(int userid, string searchkey, string status)
        {
            try
            {
                SqlConnection con = new SqlConnection();
                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetPollManagerInfoStatus", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
                if (!String.IsNullOrWhiteSpace(searchkey))
                {
                    scom.Parameters.Add(new SqlParameter("@searchkey", SqlDbType.VarChar)).Value = searchkey;
                }
                else
                {
                    scom.Parameters.Add(new SqlParameter("@searchkey", SqlDbType.VarChar)).Value = "";
                }
                scom.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar)).Value = status;
                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dspoll = new DataSet();
                DataTable dt = new DataTable();
                sda.Fill(dspoll);
                con.Close();
                dt = dspoll.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable getPollThemesSearchKey(int userid, string searchkey, string status)
        {
            try
            {
                SqlConnection con = new SqlConnection();
                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetPollManagerInfoSearch", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
                scom.Parameters.Add(new SqlParameter("@searchkey", SqlDbType.VarChar)).Value = searchkey;
                if (!String.IsNullOrWhiteSpace(status))
                {
                    scom.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar)).Value = status;
                }
                else {
                    scom.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar)).Value = "";
                } 
                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dspoll = new DataSet();
                DataTable dt = new DataTable();
                sda.Fill(dspoll);
                con.Close();
                dt = dspoll.Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public DataSet getsuperpolldeleted(int pollid)
        {
            try
            {
                SqlConnection con = new SqlConnection();
                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_superpolldeleted", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@pollId", SqlDbType.Int)).Value = pollid;
                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dspoll = new DataSet();
                sda.Fill(dspoll);
                con.Close();
                return dspoll;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #region GetPolls
        public PagedList<PollInfo> GetPolls(int userId, string status, string searchKey, PagingCriteria pagingCriteria, int widgetFlag = 0, int top = 0)
        {
            var listPollInfo = new List<PollInfo>();
            var pagedListPollInfo = new PagedList<PollInfo>();
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                DataTable dtinv = new DataTable();
                if (searchKey != null && searchKey != "")
                {
                    if (status == "All")
                    {
                        dtinv = getPollThemesSearchKey(userId, searchKey, "");
                    }
                    else {
                        dtinv = getPollThemesSearchKey(userId, searchKey, status);
                    }
                }
                else
                {
                    if (status != "All")
                    {
                        dtinv = getPollManagerInfoStatus(userId, searchKey, status);
                    }
                    else
                    {
                        dtinv = getPollThemes(userId);
                    }
                }
                var results = from pollin in dtinv.AsEnumerable()
                              select pollin;
                  
                var pollDetails = from poll in results.AsQueryable()
                                   select new
                                   {
                                       name = poll.Field<string>("name"),
                                       type = poll.Field<string>("type"),
                                       modified_on = poll.Field<DateTime>("modified_on"),
                                       pk_pollid = poll.Field<int>("pk_pollid"),
                                       status = poll.Field<string>("status"),
                                       frameheight = poll.Field<int>("frameheight"),
                                       framewidth = poll.Field<int>("framewidth"),
                                       questiontext = poll.Field<string>("questiontext"),
                                       launched_on = poll.Field<DateTime>("launched_on"),
                                       closed_on = poll.Field<DateTime>("closed_on")

                                   };
                var pollPagedList = pollDetails.ToPagedList(pagingCriteria);
                
                int i = 0;

                string strEditSurvey = "";
                string strReportUrl = "";
                foreach (var poll in pollPagedList.Entities)
                {
                    List<PollAnswerOptions> pollansweroptions = new List<PollAnswerOptions>();
                    pollansweroptions = getPollAnswerWiseCount(poll.pk_pollid);
                    var totalresponses = 0;
                    foreach (var item in pollansweroptions) {
                        int thiscount = 0;
                        Int32.TryParse(item.answercount.ToString(), out thiscount);
                        totalresponses += thiscount;
                    }
                    var pollInfo = new PollInfo();
                    pollInfo.pk_pollid = poll.pk_pollid;
                    pollInfo.type = poll.type;
                    pollInfo.modified_on = Convert.ToDateTime(poll.modified_on).ToString("dd-MMM-yyyy");
                    pollInfo.launched_on = Convert.ToDateTime(poll.launched_on).ToString("dd-MMM-yyyy");
                    pollInfo.closed_on = Convert.ToDateTime(poll.closed_on).ToString("dd-MMM-yyyy");
                    pollInfo.name = poll.name;
                    pollInfo.Status = poll.status;
                    DataSet dtVisits = new DataSet();
                    dtVisits = getPollVisitsInfo(pollInfo.pk_pollid);
                    pollInfo.Complete = Convert.ToInt32(dtVisits.Tables[0].Rows[0][0].ToString());
                    pollInfo.Partial = Convert.ToInt32(dtVisits.Tables[1].Rows[0][0].ToString()); ;
                    pollInfo.TOTAL_RESPONSES = totalresponses;
                    strEditSurvey = "ActiveEdit:" + EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid);
                    pollInfo.ASEditUrl = strEditSurvey;
                    if (pollInfo.Status == "Active" || pollInfo.Status=="Closed")
                    {
                        strReportUrl = "Report:" + EncryptHelper.EncryptQuerystring("/Polls/PollReports.aspx", "PollId=" + pollInfo.pk_pollid);
                        pollInfo.ReportUrl = strReportUrl;
                    }
                    else
                    {
                        strReportUrl = "Report:#";
                        pollInfo.ReportUrl = strReportUrl;
                    }

                    DataSet dsdelete = getsuperpolldeleted(pollInfo.pk_pollid);
                    string PollManagerUrl = EncryptHelper.EncryptQuerystring("PollManager.aspx", "PollId=" + pollInfo.pk_pollid);
                    pollInfo.PollManagerUrl = String.Format("<a href='{0}'>{1}</a>", PollManagerUrl, pollInfo.name);
                    string Nvurlstr = EncryptHelper.EncryptQuerystring("PollManager.aspx", "PollId=" + pollInfo.pk_pollid + "&PollName=" + pollInfo.name);
                    pollInfo.PollUrl = Nvurlstr;
                    string launchUrl = "";
                    launchUrl = "Launch:" + EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid + "&launch=" + "launch");
                    pollInfo.LaunchUrl = launchUrl;
                    string strPreviewPoll = "";
                    strPreviewPoll = "Preview:" + EncryptHelper.EncryptQuerystring("PollPreview.aspx", "PollId=" + pollInfo.pk_pollid);
                    pollInfo.PreviewLink = strPreviewPoll;
                    string upgrede;
                    upgrede = EncryptHelper.EncryptQuerystring("SurveyCredits.aspx", "&PollId=" + pollInfo.pk_pollid + "&status=" + pollInfo.Status);
                    var sessionService = ServiceFactory.GetService<SessionStateService>();
                    var userInfo = sessionService.GetLoginUserDetailsSession();
                    string surveytype = GenericHelper.ToEnum<UserType>(userInfo.LicenseType).ToString();
                    pollInfo.UpgradeUrl = upgrede + "~" + pollInfo.pk_pollid + "~" + "PPS_PREMIUM" + "~" + surveytype + "~" + "10" + "~" + "10" + "~" + pollInfo.Status;
                    pollInfo.appendedUrls = pollInfo.Status + "~" + pollInfo.pk_pollid + "~" + dsdelete.Tables[0].Rows[0][0].ToString() + "~" + strEditSurvey + "~" + strPreviewPoll + "~" + strReportUrl + "~" + launchUrl;

                    pollInfo.frameheight = poll.frameheight;
                    pollInfo.framewidth = poll.framewidth;
                    pollInfo.pollansweroptions = pollansweroptions;
                    if (poll.type == "Image")
                    {
                        pollInfo.PollIcon = "photo_size_select_actual";
                    }
                    else if (poll.type == "Video")
                    {
                        pollInfo.PollIcon = "play_circle_filled";
                    }
                    else {
                        pollInfo.PollIcon = "help";
                    }
                    pollInfo.question = poll.questiontext;
                    listPollInfo.Add(pollInfo);
                    i++;
                }
                
                pagedListPollInfo.Entities = listPollInfo;
                pagedListPollInfo.HasNext = pollPagedList.HasNext;
                pagedListPollInfo.HasPrevious = pollPagedList.HasPrevious;
                pagedListPollInfo.TotalCount = pollPagedList.TotalCount;
                pagedListPollInfo.TotalPages = pollPagedList.TotalPages;
            }
            return pagedListPollInfo;
        }

        #endregion

        public PagedList<PollInfo> GetPollsAll(int userId, string status, string searchKey, PagingCriteria pagingCriteria, int widgetFlag = 0, int top = 0)
        {
            var listPollInfo = new List<PollInfo>();
            var pagedListPollInfo = new PagedList<PollInfo>();
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            int totalpollcount = 0;//THIS WILL INCLUDE DELETED POLLS AS WELL 
            int timezone = userDetails.StandardBIAS;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                
                DataTable dtinv = new DataTable();
                DataSet dsgetpollsall = new DataSet();
                dsgetpollsall = GetPollsAll(userId);
                if (dsgetpollsall.Tables.Count > 1)
                {
                    dtinv = dsgetpollsall.Tables[0];
                    Int32.TryParse(dsgetpollsall.Tables[1].Rows[0][0].ToString(), out totalpollcount);
                }

                var results = from pollin in dtinv.AsEnumerable()
                              select pollin;

                var pollDetails = from poll in results.AsQueryable()
                                  select new
                                  {
                                      name = poll.Field<string>("name"),
                                      type = poll.Field<string>("type"),
                                      modified_on = poll.Field<DateTime>("modified_on"),
                                      closed_on = poll.Field<DateTime>("closed_on"),
                                      launched_on = poll.Field<DateTime>("launched_on"),
                                      created_on = poll.Field<DateTime>("created_on"),
                                      pk_pollid = poll.Field<int>("pk_pollid"),
                                      status = poll.Field<string>("status"),
                                      frameheight = poll.Field<int>("frameheight"),
                                      framewidth = poll.Field<int>("framewidth"),
                                      questiontext = poll.Field<string>("questiontext"),
                                      answercount = poll.Field<int>("AnswerCount"),
                                      embedcode = poll.Field<string>("embedcode"),
                                      imagelink = poll.Field<string>("imagelink"),
                                      videolink = poll.Field<string>("videolink")
                                   };
                var pollPagedList = pollDetails.ToPagedList(pagingCriteria);
                
                int i = 0;

                string strEditSurvey = "";
                string strReportUrl = "";
                foreach (var poll in pollPagedList.Entities)
                {
                    
                    string replaceurl = Insighto.Business.Helpers.EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + poll.pk_pollid.ToString() + "&polltype=superpoll");
                    string weblinkurl = Insighto.Business.Helpers.EncryptHelper.EncryptQuerystring("PollWebLink.aspx", "PollId=" + poll.pk_pollid.ToString() + "&pollsource=weblink");

                    List<PollAnswerOptions> pollansweroptions = new List<PollAnswerOptions>();
                    pollansweroptions = getPollAnswerWiseCount(poll.pk_pollid);
                    var totalresponses = 0;

                    DataSet dsAnalytics = getpollSurveyAnalytics(poll.pk_pollid);
                    totalresponses  = Convert.ToInt32(dsAnalytics.Tables[2].Rows[0][0].ToString());
                    if (totalresponses == 0) {
                        pollansweroptions.Add(new PollAnswerOptions {answercount=1,answerid=1,answeroption="No results" });
                    }
                    var pollInfo = new PollInfo();
                    pollInfo.pk_pollid = poll.pk_pollid;
                    pollInfo.type = poll.type;

                    //ADDED  TIMEZONE BIAS SATISH 10-AUG-16
                    pollInfo.created_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.created_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    pollInfo.modified_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.modified_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    if (poll.closed_on != null)
                    {
                        pollInfo.closed_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.closed_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    }
                    else
                    {
                        pollInfo.launched_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.created_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    }
                    if (poll.launched_on != null)
                    {
                        pollInfo.launched_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.launched_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    }
                    else{
                        pollInfo.launched_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.created_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    }
                    pollInfo.name = poll.name;
                    pollInfo.Status = poll.status;
                    DataSet dtVisits = new DataSet();
                    dtVisits = getPollVisitsInfo(pollInfo.pk_pollid);
                    pollInfo.Complete = Convert.ToInt32(dtVisits.Tables[0].Rows[0][0].ToString());
                    pollInfo.Partial = Convert.ToInt32(dtVisits.Tables[1].Rows[0][0].ToString()); ;
                    pollInfo.TOTAL_RESPONSES = totalresponses;
                    pollInfo.PollEditUrl = "" + EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid);
                    strEditSurvey = "ActiveEdit:" + EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid);
                    pollInfo.ASEditUrl = strEditSurvey;
                    strReportUrl = "" + EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + pollInfo.pk_pollid);
                    pollInfo.ReportUrl = strReportUrl;

                    DataSet dsdelete = getsuperpolldeleted(pollInfo.pk_pollid);
                    string PollManagerUrl = EncryptHelper.EncryptQuerystring("PollManager.aspx", "PollId=" + pollInfo.pk_pollid);
                    pollInfo.PollManagerUrl = String.Format("<a href='{0}'>{1}</a>", PollManagerUrl, pollInfo.name);
                    string Nvurlstr = EncryptHelper.EncryptQuerystring("PollManager.aspx", "PollId=" + pollInfo.pk_pollid + "&PollName=" + pollInfo.name);
                    pollInfo.PollUrl = Nvurlstr;
                    string launchUrl = "";

                    if (poll.status == "Draft")
                    {
                        launchUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid + "&launch=" + "launch");
                    }
                    else {
                        launchUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid + "&launch=" + "launchedpoll");
                    }
                    
                    pollInfo.LaunchUrl = launchUrl;
                    string strPreviewPoll = "";
                    strPreviewPoll = "Preview:" + EncryptHelper.EncryptQuerystring("PollPreview.aspx", "PollId=" + pollInfo.pk_pollid);
                    pollInfo.PreviewLink = strPreviewPoll;
                    string upgrede;
                    upgrede = EncryptHelper.EncryptQuerystring("SurveyCredits.aspx", "&PollId=" + pollInfo.pk_pollid + "&status=" + pollInfo.Status);
                    var sessionService = ServiceFactory.GetService<SessionStateService>();
                    var userInfo = sessionService.GetLoginUserDetailsSession();
                    string surveytype = GenericHelper.ToEnum<UserType>(userInfo.LicenseType).ToString();
                    pollInfo.UpgradeUrl = upgrede + "~" + pollInfo.pk_pollid + "~" + "PPS_PREMIUM" + "~" + surveytype + "~" + "10" + "~" + "10" + "~" + pollInfo.Status;
                    pollInfo.appendedUrls = pollInfo.Status + "~" + pollInfo.pk_pollid + "~" + dsdelete.Tables[0].Rows[0][0].ToString() + "~" + strEditSurvey + "~" + strPreviewPoll + "~" + strReportUrl + "~" + launchUrl;

                    pollInfo.frameheight = poll.frameheight;
                    pollInfo.framewidth = poll.framewidth;
                    pollInfo.pollansweroptions = pollansweroptions;
                    if (poll.type == "Image")
                    {
                        pollInfo.PollIcon = "photo_size_select_actual";
                    }
                    else if (poll.type == "Video")
                    {
                        pollInfo.PollIcon = "play_circle_filled";
                    }
                    else {
                        pollInfo.PollIcon = "help";
                    }
                    if(String.IsNullOrWhiteSpace( poll.questiontext)){
                        pollInfo.question = "";
                    }
                    else{
                        pollInfo.question = poll.questiontext;
                        
                    }
                    pollInfo.answercount = poll.answercount;
                    pollInfo.EmbedCode = poll.embedcode;
                    pollInfo.ImageLink = poll.imagelink;
                    pollInfo.VideoLink = WebUtility.HtmlDecode(poll.videolink);
                    pollInfo.IsImagePoll = "false";
                    pollInfo.IsVideoPoll = "false";
                    if (poll.status == "Active")
                    {
                        pollInfo.ActiveSince = DateTime.Now.Subtract(poll.launched_on).Days;
                    }
                    else if (poll.embedcode != "")
                    {
                        pollInfo.ActiveSince = poll.closed_on.Subtract(poll.launched_on).Days;
                    }
                    else {
                        pollInfo.ActiveSince = 0;// DateTime.Now.Subtract(poll.closed_on).Days;
                    }
                    if (poll.type == "Video")
                    {
                        pollInfo.IsVideoPoll = "true";
                    }
                    else if (poll.type == "Image")
                    {
                        pollInfo.IsImagePoll = "true";
                    }
                    pollInfo.ReplaceUrl = replaceurl;
                    pollInfo.WebLinkUrl = weblinkurl;
                    listPollInfo.Add(pollInfo);
                    i++;
                }
                
                pagedListPollInfo.Entities = listPollInfo;
                pagedListPollInfo.HasNext = pollPagedList.HasNext;
                pagedListPollInfo.HasPrevious = pollPagedList.HasPrevious;
                pagedListPollInfo.TotalCount = totalpollcount;//SEND THIS AS THE TOTAL POLL COUNT INCLUDING DELETED TO BE USED FOR SHOWING ALERTS
                pagedListPollInfo.TotalPages = pollPagedList.TotalPages;
            }
            return pagedListPollInfo;
        }

        public IList<EntityInfo> GetAllPolls(int userId)
        {
            var listPollInfo = new List<EntityInfo>();

            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            int totalpollcount = 0;//THIS WILL INCLUDE DELETED POLLS AS WELL 
            int timezone = userDetails.StandardBIAS;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                DataTable dtinv = new DataTable();
                DataSet dsgetpollsall = new DataSet();
                dsgetpollsall = GetPollsAll(userId);
                if (dsgetpollsall.Tables.Count > 1)
                {
                    dtinv = dsgetpollsall.Tables[0];
                    Int32.TryParse(dsgetpollsall.Tables[1].Rows[0][0].ToString(), out totalpollcount);
                }

                var results = from pollin in dtinv.AsEnumerable()
                              select pollin;

                var pollDetails = from poll in results.AsQueryable()
                                  select new
                                  {
                                      name = poll.Field<string>("name"),
                                      type = poll.Field<string>("type"),
                                      modified_on = poll.Field<DateTime>("modified_on"),
                                      closed_on = poll.Field<DateTime>("closed_on"),
                                      launched_on = poll.Field<DateTime>("launched_on"),
                                      created_on = poll.Field<DateTime>("created_on"),
                                      pk_pollid = poll.Field<int>("pk_pollid"),
                                      status = poll.Field<string>("status"),
                                      frameheight = poll.Field<int>("frameheight"),
                                      framewidth = poll.Field<int>("framewidth"),
                                      questiontext = poll.Field<string>("questiontext"),
                                      answercount = poll.Field<int>("AnswerCount"),
                                      embedcode = poll.Field<string>("embedcode"),
                                      imagelink = poll.Field<string>("imagelink"),
                                      videolink = poll.Field<string>("videolink")
                                  };
                var pollPagedList = pollDetails.ToList();

                int i = 0;

                string strEditSurvey = "";
                string strReportUrl = "";
                foreach (var poll in pollPagedList)
                {

                    string replaceurl = Insighto.Business.Helpers.EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + poll.pk_pollid.ToString() + "&polltype=superpoll");
                    string weblinkurl = Insighto.Business.Helpers.EncryptHelper.EncryptQuerystring("PollWebLink.aspx", "PollId=" + poll.pk_pollid.ToString() + "&pollsource=weblink");

                    List<PollAnswerOptions> pollansweroptions = new List<PollAnswerOptions>();
                    pollansweroptions = getPollAnswerWiseCount(poll.pk_pollid);
                    var totalresponses = 0;

                    DataSet dsAnalytics = getpollSurveyAnalytics(poll.pk_pollid);
                    totalresponses = Convert.ToInt32(dsAnalytics.Tables[2].Rows[0][0].ToString());
                    if (totalresponses == 0)
                    {
                        pollansweroptions.Add(new PollAnswerOptions { answercount = 1, answerid = 1, answeroption = "No results" });
                    }
                    var pollInfo = new EntityInfo();
                    pollInfo.pk_pollid = poll.pk_pollid;
                    pollInfo.type = poll.type;

                    //ADDED  TIMEZONE BIAS SATISH 10-AUG-16
                    pollInfo.created_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.created_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    pollInfo.modified_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.modified_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    if (poll.closed_on != null)
                    {
                        pollInfo.closed_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.closed_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    }
                    else
                    {
                        pollInfo.launched_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.created_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    }
                    if (poll.launched_on != null)
                    {
                        pollInfo.launched_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.launched_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    }
                    else
                    {
                        pollInfo.launched_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(poll.created_on), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
                    }
                    pollInfo.name = poll.name;
                    pollInfo.Status = poll.status;
                    DataSet dtVisits = new DataSet();
                    dtVisits = getPollVisitsInfo(pollInfo.pk_pollid);
                    pollInfo.Complete = Convert.ToInt32(dtVisits.Tables[0].Rows[0][0].ToString());
                    pollInfo.Partial = Convert.ToInt32(dtVisits.Tables[1].Rows[0][0].ToString()); ;
                    pollInfo.TOTAL_RESPONSES = totalresponses;
                    pollInfo.PollEditUrl = "" + EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid);
                    strEditSurvey = "ActiveEdit:" + EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid);
                    pollInfo.ASEditUrl = strEditSurvey;
                    strReportUrl = "" + EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + pollInfo.pk_pollid);
                    pollInfo.ReportUrl = strReportUrl;

                    DataSet dsdelete = getsuperpolldeleted(pollInfo.pk_pollid);
                    string PollManagerUrl = EncryptHelper.EncryptQuerystring("PollManager.aspx", "PollId=" + pollInfo.pk_pollid);
                    pollInfo.PollManagerUrl = String.Format("<a href='{0}'>{1}</a>", PollManagerUrl, pollInfo.name);
                    string Nvurlstr = EncryptHelper.EncryptQuerystring("PollManager.aspx", "PollId=" + pollInfo.pk_pollid + "&PollName=" + pollInfo.name);
                    pollInfo.PollUrl = Nvurlstr;
                    string launchUrl = "";

                    if (poll.status == "Draft")
                    {
                        launchUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid + "&launch=" + "launch");
                    }
                    else
                    {
                        launchUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid + "&launch=" + "launchedpoll");
                    }

                    pollInfo.LaunchUrl = launchUrl;
                    string strPreviewPoll = "";
                    strPreviewPoll = "Preview:" + EncryptHelper.EncryptQuerystring("PollPreview.aspx", "PollId=" + pollInfo.pk_pollid);
                    pollInfo.PreviewLink = strPreviewPoll;
                    string upgrede;
                    upgrede = EncryptHelper.EncryptQuerystring("SurveyCredits.aspx", "&PollId=" + pollInfo.pk_pollid + "&status=" + pollInfo.Status);
                    var sessionService = ServiceFactory.GetService<SessionStateService>();
                    var userInfo = sessionService.GetLoginUserDetailsSession();
                    string surveytype = GenericHelper.ToEnum<UserType>(userInfo.LicenseType).ToString();
                    pollInfo.UpgradeUrl = upgrede + "~" + pollInfo.pk_pollid + "~" + "PPS_PREMIUM" + "~" + surveytype + "~" + "10" + "~" + "10" + "~" + pollInfo.Status;
                    pollInfo.appendedUrls = pollInfo.Status + "~" + pollInfo.pk_pollid + "~" + dsdelete.Tables[0].Rows[0][0].ToString() + "~" + strEditSurvey + "~" + strPreviewPoll + "~" + strReportUrl + "~" + launchUrl;

                    pollInfo.frameheight = poll.frameheight;
                    pollInfo.framewidth = poll.framewidth;
                    pollInfo.pollansweroptions = pollansweroptions;
                    if (poll.type == "Image")
                    {
                        pollInfo.PollIcon = "photo_size_select_actual";
                    }
                    else if (poll.type == "Video")
                    {
                        pollInfo.PollIcon = "play_circle_filled";
                    }
                    else
                    {
                        pollInfo.PollIcon = "help";
                    }
                    if (String.IsNullOrWhiteSpace(poll.questiontext))
                    {
                        pollInfo.question = "";
                    }
                    else
                    {
                        pollInfo.question = poll.questiontext;

                    }
                    pollInfo.answercount = poll.answercount;
                    pollInfo.EmbedCode = poll.embedcode;
                    pollInfo.ImageLink = poll.imagelink;
                    pollInfo.VideoLink = WebUtility.HtmlDecode(poll.videolink);
                    pollInfo.IsImagePoll = "false";
                    pollInfo.IsVideoPoll = "false";
                    if (poll.status == "Active")
                    {
                        pollInfo.ActiveSince = DateTime.Now.Subtract(poll.launched_on).Days;
                    }
                    else if (poll.embedcode != "")
                    {
                        pollInfo.ActiveSince = poll.closed_on.Subtract(poll.launched_on).Days;
                    }
                    else
                    {
                        pollInfo.ActiveSince = 0;// DateTime.Now.Subtract(poll.closed_on).Days;
                    }
                    if (poll.type == "Video")
                    {
                        pollInfo.IsVideoPoll = "true";
                    }
                    else if (poll.type == "Image")
                    {
                        pollInfo.IsImagePoll = "true";
                    }
                    pollInfo.ReplaceUrl = replaceurl;
                    pollInfo.WebLinkUrl = weblinkurl;
                    listPollInfo.Add(pollInfo);
                    i++;
                }

            }
            return listPollInfo;
        }

        //public IList<EntityInfo> GetAllSurveys(int userId)
        //{
        //    var listPollInfo = new List<EntityInfo>();
            
        //    var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        //    int totalpollcount = 0;//THIS WILL INCLUDE DELETED POLLS AS WELL 
        //    int timezone = userDetails.StandardBIAS;
        //    using (InsightoV2Entities dbCon = new InsightoV2Entities())
        //    {

        //        List<osm_survey> allsurveys = new List<osm_survey>();
        //        allsurveys = GetSurveyByUserID(userId);

        //        int i = 0;

        //        string strEditSurvey = "";
        //        string strReportUrl = "";
        //        foreach (var survey in allsurveys)
        //        {

        //            string replaceurl = "";
        //            string weblinkurl = "";

        //            //List<PollAnswerOptions> pollansweroptions = new List<PollAnswerOptions>();
        //            //pollansweroptions = getPollAnswerWiseCount(poll.pk_pollid);
        //            var totalresponses = 0;

        //            //DataSet dsAnalytics = getpollSurveyAnalytics(poll.pk_pollid);
        //            //totalresponses = Convert.ToInt32(dsAnalytics.Tables[2].Rows[0][0].ToString());
        //            //if (totalresponses == 0)
        //            //{
        //            //    pollansweroptions.Add(new PollAnswerOptions { answercount = 1, answerid = 1, answeroption = "No results" });
        //            //}
        //            var pollInfo = new EntityInfo();
        //            pollInfo.pk_pollid = survey.SURVEY_ID;
        //            pollInfo.type = "Text";

        //            //ADDED  TIMEZONE BIAS SATISH 10-AUG-16
        //            pollInfo.created_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(survey.CREATED_ON), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
        //            pollInfo.modified_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(survey.MODIFIED_ON), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
        //            if (survey.SURVEY_CLOSE_DATE != null)
        //            {
        //                pollInfo.closed_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(survey.SURVEY_CLOSE_DATE), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
        //            }
        //            else
        //            {
        //                pollInfo.closed_on = null;
        //            }
        //            if (survey.STATUS == "Active")
        //            {
        //                pollInfo.launched_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(survey.MODIFIED_ON), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
        //            }
        //            else
        //            {
        //                pollInfo.launched_on = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(survey.CREATED_ON), userDetails.StandardBIAS, true).ToString("dd-MMM-yyyy");
        //            }
        //            pollInfo.name = survey.SURVEY_NAME;
        //            pollInfo.Status = survey.STATUS;
        //            DataSet dtVisits = new DataSet();
        //            dtVisits = getPollVisitsInfo(pollInfo.pk_pollid);
        //            pollInfo.Complete = Convert.ToInt32(dtVisits.Tables[0].Rows[0][0].ToString());
        //            pollInfo.Partial = Convert.ToInt32(dtVisits.Tables[1].Rows[0][0].ToString()); ;
        //            pollInfo.TOTAL_RESPONSES = totalresponses;
        //            pollInfo.PollEditUrl = "" + EncryptHelper.EncryptQuerystring("/SurveyMiddlePage.aspx", "PollId=" + pollInfo.pk_pollid);
        //            strEditSurvey = "ActiveEdit:" + EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid);
        //            pollInfo.ASEditUrl = strEditSurvey;
        //            strReportUrl = "" + EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + pollInfo.pk_pollid);
        //            pollInfo.ReportUrl = strReportUrl;

        //            DataSet dsdelete = getsuperpolldeleted(pollInfo.pk_pollid);
        //            string PollManagerUrl = EncryptHelper.EncryptQuerystring("PollManager.aspx", "PollId=" + pollInfo.pk_pollid);
        //            pollInfo.PollManagerUrl = String.Format("<a href='{0}'>{1}</a>", PollManagerUrl, pollInfo.name);
        //            string Nvurlstr = EncryptHelper.EncryptQuerystring("PollManager.aspx", "PollId=" + pollInfo.pk_pollid + "&PollName=" + pollInfo.name);
        //            pollInfo.PollUrl = Nvurlstr;
        //            string launchUrl = "";

        //            if (survey.STATUS == "Draft")
        //            {
        //                launchUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid + "&launch=" + "launch");
        //            }
        //            else
        //            {
        //                launchUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollInfo.pk_pollid + "&launch=" + "launchedpoll");
        //            }

        //            pollInfo.LaunchUrl = launchUrl;
        //            string strPreviewPoll = "";
        //            strPreviewPoll = "Preview:" + EncryptHelper.EncryptQuerystring("PollPreview.aspx", "PollId=" + pollInfo.pk_pollid);
        //            pollInfo.PreviewLink = strPreviewPoll;
        //            string upgrede;
        //            upgrede = EncryptHelper.EncryptQuerystring("SurveyCredits.aspx", "&PollId=" + pollInfo.pk_pollid + "&status=" + pollInfo.Status);
        //            var sessionService = ServiceFactory.GetService<SessionStateService>();
        //            var userInfo = sessionService.GetLoginUserDetailsSession();
        //            string surveytype = GenericHelper.ToEnum<UserType>(userInfo.LicenseType).ToString();
        //            pollInfo.UpgradeUrl = upgrede + "~" + pollInfo.pk_pollid + "~" + "PPS_PREMIUM" + "~" + surveytype + "~" + "10" + "~" + "10" + "~" + pollInfo.Status;
        //            pollInfo.appendedUrls = pollInfo.Status + "~" + pollInfo.pk_pollid + "~" + dsdelete.Tables[0].Rows[0][0].ToString() + "~" + strEditSurvey + "~" + strPreviewPoll + "~" + strReportUrl + "~" + launchUrl;

        //            pollInfo.frameheight = poll.frameheight;
        //            pollInfo.framewidth = poll.framewidth;
        //            pollInfo.pollansweroptions = pollansweroptions;
        //            if (poll.type == "Image")
        //            {
        //                pollInfo.PollIcon = "photo_size_select_actual";
        //            }
        //            else if (poll.type == "Video")
        //            {
        //                pollInfo.PollIcon = "play_circle_filled";
        //            }
        //            else
        //            {
        //                pollInfo.PollIcon = "help";
        //            }
        //            if (String.IsNullOrWhiteSpace(poll.questiontext))
        //            {
        //                pollInfo.question = "";
        //            }
        //            else
        //            {
        //                pollInfo.question = poll.questiontext;

        //            }
        //            pollInfo.answercount = poll.answercount;
        //            pollInfo.EmbedCode = poll.embedcode;
        //            pollInfo.ImageLink = poll.imagelink;
        //            pollInfo.VideoLink = WebUtility.HtmlDecode(poll.videolink);
        //            pollInfo.IsImagePoll = "false";
        //            pollInfo.IsVideoPoll = "false";
        //            if (poll.status == "Active")
        //            {
        //                pollInfo.ActiveSince = DateTime.Now.Subtract(poll.launched_on).Days;
        //            }
        //            else if (poll.embedcode != "")
        //            {
        //                pollInfo.ActiveSince = poll.closed_on.Subtract(poll.launched_on).Days;
        //            }
        //            else
        //            {
        //                pollInfo.ActiveSince = 0;// DateTime.Now.Subtract(poll.closed_on).Days;
        //            }
        //            if (poll.type == "Video")
        //            {
        //                pollInfo.IsVideoPoll = "true";
        //            }
        //            else if (poll.type == "Image")
        //            {
        //                pollInfo.IsImagePoll = "true";
        //            }
        //            pollInfo.ReplaceUrl = replaceurl;
        //            pollInfo.WebLinkUrl = weblinkurl;
        //            pollInfo.EntityType = "survey";
        //            listPollInfo.Add(pollInfo);
        //            i++;
        //        }

        //    }
        //    return listPollInfo;
        //}

        public PagedList<SurveyInfo> GetSurveyInfoList(PagedList<SurveyDashboardView> surveyPagedList, int widgetFlag)
        {
            var pagedListInfo = new PagedList<SurveyInfo>();
            var listInfo = new List<SurveyInfo>();
            foreach (var survey in surveyPagedList.Entities)
            {
                var surveyInfo = new SurveyInfo();
                //todo: load the survey info object
                string strEditSurvey = "";
                if (survey.STATUS == "Active")
                {
                    strEditSurvey = "ActiveEdit:" + EncryptHelper.EncryptQuerystring("SurveyMiddlePage.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    surveyInfo.ASEditUrl = strEditSurvey;
                }
                else if (survey.STATUS == "Draft")
                {
                    strEditSurvey = "ActiveEdit:" + EncryptHelper.EncryptQuerystring("SurveyMiddlePage.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    surveyInfo.ASEditUrl = strEditSurvey;
                }
                else if (survey.STATUS == "Closed")
                {
                    strEditSurvey = "ActiveEdit:#";
                    surveyInfo.ASEditUrl = strEditSurvey;
                }

                if (survey.LAUNCHED_ON == null)
                {
                    surveyInfo.Launchflag = 0;
                    surveyInfo.Launchon = null;
                }
                else
                {
                    surveyInfo.Launchflag = 1;
                    DateTime dt = Convert.ToDateTime(survey.LAUNCHED_ON.ToString());
                    dt = dt.AddMinutes(330);
                    string launchOn = Convert.ToDateTime(dt).ToString("dd-MMM-yyyy");
                    surveyInfo.Launchon = Convert.ToDateTime(launchOn);
                }
                surveyInfo.Preview = "Preview";
                string strPreviewSurvey = "Preview:" + EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewURL().Replace("~/", ""), "Mode=" + RespondentDisplayMode.PreviewWithPagination.ToString() + "&SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag + "&Layout=" + survey.SURVEY_LAYOUT);
                surveyInfo.PreviewLink = strPreviewSurvey;
                string strRemainderSurvey = "";
                if (survey.STATUS == "Active")
                {
                    DataSet dsppstype = getSurveyTypemdeploy(survey.SURVEY_ID);
                    

                    string launchcnt = dsppstype.Tables[2].Rows[0][0].ToString();
                    int launchcountval = Convert.ToInt32(launchcnt);
                   int launchtypenumber = getLaunchTypeNumber(survey.SURVEY_ID);
                   LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                   if ((LaunchMethod.Emailthr & launchnew) == LaunchMethod.Emailthr)
                   {
                       strRemainderSurvey = "Remainder:" + EncryptHelper.EncryptQuerystring("SendReminder.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                       surveyInfo.SendRemainderUrl = strRemainderSurvey;
                       surveyInfo.SendRemainderText = "Reminders";
                   }
                   else
                   {
                       if (Convert.ToInt32(survey.EMAILLIST_TYPE.ToString()) == 1)
                       {
                           strRemainderSurvey = "Remainder:" + EncryptHelper.EncryptQuerystring("SendReminder.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                           surveyInfo.SendRemainderUrl = strRemainderSurvey;
                           surveyInfo.SendRemainderText = "Reminders";
                       }
                       else
                       {
                           strRemainderSurvey = "Remainder:#";
                           surveyInfo.SendRemainderUrl = strRemainderSurvey;
                       }
                   }
                }
                else
                {
                    strRemainderSurvey = "Remainder:#";
                    surveyInfo.SendRemainderUrl = strRemainderSurvey;
                }

                surveyInfo.SurveyEditUrl = "";


                string Nvurlstr = EncryptHelper.EncryptQuerystring("SurveyManager.aspx", "SurId=" + survey.SURVEY_ID.ToString() + "&SurName=" + survey.SURVEY_NAME.ToString() + "&surveyFlag=" + widgetFlag);
                surveyInfo.SurveyUrl = Nvurlstr;

                string SurveyManagerUrl = EncryptHelper.EncryptQuerystring("SurveyManager.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                surveyInfo.SurveyManagerUrl = String.Format("<a href='{0}'>{1}</a>", SurveyManagerUrl, survey.SURVEY_NAME.ToString());

                string upgradesurl = EncryptHelper.EncryptQuerystring("Price.aspx", "");
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userInfo = sessionService.GetLoginUserDetailsSession();
                string surveytype = GenericHelper.ToEnum<UserType>(userInfo.LicenseType).ToString();
               
               DataSet  dsppscredits = getCreditsCount(survey.USERID);

               if(surveytype != "PRO_YEARLY" && surveytype != "PREMIUM_YEARLY" && surveytype != "PRO_MONTHLY")
               {
                 
                 string Survey_Types=getsurveytype(survey.USERID,survey.SURVEY_ID);
                  if( dsppscredits.Tables[0].Rows.Count ==0 && dsppscredits.Tables[1].Rows.Count ==0 )
                  {
                      if (Survey_Types == "PPS_PRO" && survey.STATUS=="Draft")
                        {
                          DataSet dsproupdate = updatesurveyType(survey.USERID, "", survey.SURVEY_ID, "",survey.STATUS);
                        }
                      if (Survey_Types == "PPS_PREMIUM" && survey.STATUS=="Draft")
                        {
                          DataSet dspremupdate = updatesurveyType(survey.USERID, "", survey.SURVEY_ID, "",survey.STATUS);
                        }

                  }
                   else if(dsppscredits.Tables[0].Rows.Count ==0 )
                  {
                        if (Survey_Types == "PPS_PRO" && survey.STATUS=="Draft")
                        {
                          DataSet dsproupdate = updatesurveyType(survey.USERID, "", survey.SURVEY_ID, "",survey.STATUS);
                        }
                  }
                  else if(dsppscredits.Tables[1].Rows.Count ==0 )
                  {
                   if (Survey_Types == "PPS_PREMIUM" && survey.STATUS=="Draft")
                        {
                          DataSet dspremupdate = updatesurveyType(survey.USERID, "", survey.SURVEY_ID, "",survey.STATUS);
                        }
                  }

                     
               }

                string Survey_Type=getsurveytype(survey.USERID,survey.SURVEY_ID);

                if (userInfo.LicenseType == "PREMIUM_YEARLY")
                {
                    surveyInfo.survey_type = "";
                }
                else
                {
                    if (surveytype == "PREMIUM_YEARLY")
                    {

                        surveyInfo.survey_type = "<img src='App_Themes/Classic/Images/Prem_Pic1.png' title= 'Premium-Single' width='17px' height='16px' />";
                    }
                    else if (surveytype == "PRO_YEARLY")
                    {
                        if (Survey_Type == "PPS_PREMIUM")
                        {
                            surveyInfo.survey_type = "<img src='App_Themes/Classic/Images/Prem_Pic1.png' title= 'Premium-Single' width='17px' height='16px' />";
                        }
                        else
                        {

                            surveyInfo.survey_type = "<img src='App_Themes/Classic/Images/Pro_Pic1.png' title= 'Pro-Single' width='17px' height='16px' />";
                        }


                    }
                    else if (surveytype == "PRO_MONTHLY")
                    {

                        if (Survey_Type == "PPS_PREMIUM")
                        {
                            surveyInfo.survey_type = "<img src='App_Themes/Classic/Images/Prem_Pic1.png' title= 'Premium-Single' width='17px' height='16px' />";
                        }
                        else
                        {

                            surveyInfo.survey_type = "<img src='App_Themes/Classic/Images/Pro_Pic1.png' title= 'Pro-Single' width='17px' height='16px' />";
                        }

                    }
                    else
                    {

                        if (Survey_Type == "PPS_PREMIUM")
                        {
                            surveyInfo.survey_type = "<img src='App_Themes/Classic/Images/Prem_Pic1.png' title= 'Premium-Single' width='17px' height='16px' />";
                        }
                        else if (Survey_Type == "PPS_PRO")
                        {

                            surveyInfo.survey_type = "<img src='App_Themes/Classic/Images/Pro_Pic1.png' title= 'Pro-Single' width='17px' height='16px' />";
                        }
                        else
                        {
                            surveyInfo.survey_type = "<img src='App_Themes/Classic/Images/Free_Pic1.png' title= 'Basic' width='17px' height='16px' />";
                            Survey_Type = "BASIC";
                            //surveyInfo.survey_type = String.Format("<img src='App_Themes/Classic/Images/Free_Pic 1.png' width='17px' height='16px'/>", "pro_yearly");
                        }

                    }
                }

                

              //  surveyInfo.UpgradeUrl = String.Format("<a style='text-decoration: none', href='{0}'>{1}</a>", upgradesurl, "<img src='App_Themes/Classic/Images/ppsupgrade.png' width='55px' height='25px' />");
                string upgrede;
                //if ((dsppscredits.Tables[0].Rows.Count == 0) && (dsppscredits.Tables[1].Rows.Count == 0))
                //{
                //    upgrede = EncryptHelper.EncryptQuerystring("price.aspx", "UserId=" + survey.USERID.ToString() + "&FromPartner=No" + "&SurveyId=" + survey.SURVEY_ID.ToString() + "&status=" + survey.STATUS);
                //}
                //else
                //{
                    upgrede = EncryptHelper.EncryptQuerystring("SurveyCredits.aspx", "&SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag + "&surveyname=" + survey.SURVEY_NAME.ToString() + "&Userid=" + survey.USERID.ToString() + "&Survey_Type=" + Survey_Type + "&status=" + survey.STATUS);
              //  }
              //  string upgrede = String.Format("<a style='text-decoration: none', href='#'>{1}</a>", upgradesurl, "<img src='App_Themes/Classic/Images/ppsupgrade.png' width='55px' height='25px' />");
                
                //  surveyInfo.UpgradeUrl = string.Format("asp:HyperLink style='padding:4px 8px 4px 8px; font-size:12px;' Text='Upgrade' NavigateUrl='Pricing.aspx'");

                    surveyInfo.UpgradeUrl = upgrede + "~" + survey.SURVEY_ID + "~" + Survey_Type + "~" + surveytype + "~" + dsppscredits.Tables[0].Rows.Count + "~" + dsppscredits.Tables[1].Rows.Count + "~" + survey.STATUS;               
                string Navurlstr = EncryptHelper.EncryptQuerystring("ManageFolders.aspx", "FId=" + survey.FOLDER_ID.ToString() + "&SId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                surveyInfo.Url = Navurlstr;
                string strReportUrl = "";
                string launchUrl = "";
                if (survey.STATUS == "Active" || survey.STATUS == "Closed")
                {
                    strReportUrl = "Report:" + EncryptHelper.EncryptQuerystring("Reports.aspx", "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    surveyInfo.ReportUrl = strReportUrl;
                }
                if (survey.STATUS == "Draft")
                {
                    strReportUrl = "Report:#";
                    surveyInfo.ReportUrl = strReportUrl;
                    surveyInfo.LaunchUrl = launchUrl;
                    launchUrl = "Launch:" + EncryptHelper.EncryptQuerystring(PathHelper.GetLaunchSurveyURL().Replace("~/", ""), "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                }
                else
                {
                    DataSet dsppstype = getSurveyTypemdeploy(survey.SURVEY_ID);

                    string launchcnt = dsppstype.Tables[2].Rows[0][0].ToString();
                    int launchcountval = Convert.ToInt32(launchcnt);

                    string launchsurvey = dsppstype.Tables[0].Rows[0][0].ToString();


                    if ((launchsurvey == "PPS_PRO") || (launchsurvey == "PPS_PREMIUM") || (userInfo.LicenseType == "PREMIUM_YEARLY") )
                    {
                        surveyInfo.LaunchUrl = launchUrl;
                        launchUrl = "Launch:" + EncryptHelper.EncryptQuerystring(PathHelper.GetLaunchSurveyURL().Replace("~/", ""), "SurveyId=" + survey.SURVEY_ID.ToString() + "&surveyFlag=" + widgetFlag);
                    }
                    else
                    {
                        launchUrl = "Launch:#";
                    }
                }
                //added by krishna reddy for binding links in mysurvey page
                //+ "~" + strDraftEdit
                surveyInfo.appendedUrls = survey.STATUS + "~" + survey.SURVEY_ID + "~" + strEditSurvey + "~" + strPreviewSurvey + "~" + strReportUrl + "~" + strRemainderSurvey + "~" + launchUrl;
                surveyInfo.FOLDER_ID = survey.FOLDER_ID;
                surveyInfo.SurveyID = survey.SURVEY_ID;
                surveyInfo.CHECK_BLOCKWORDS = survey.CHECK_BLOCKWORDS;
                surveyInfo.SURVEY_NAME = survey.SURVEY_NAME;
                surveyInfo.TOTAL_RESPONSES = survey.TOTAL_RESPONSES;
                surveyInfo.SEND_TO = survey.SEND_TO;
                surveyInfo.RESPONSE_RATE = survey.RESPONSE_RATE;
                surveyInfo.NAME = survey.NAME;
                surveyInfo.USERID = survey.USERID;
                surveyInfo.CREATED_ON = survey.CREATED_ON;
                surveyInfo.MODIFIED_ON = Convert.ToDateTime(survey.MODIFIED_ON).ToString("dd-MMM-yyyy");
                //surveyInfo.LAUNCHED_ON = survey.LAUNCHED_ON;
                surveyInfo.Complete = survey.Complete;
                surveyInfo.Partial = survey.Partial;
                surveyInfo.EMAILLIST_TYPE = survey.EMAILLIST_TYPE;
                surveyInfo.Status = survey.STATUS;
                surveyInfo.FOLDER_NAME = survey.FOLDER_NAME;
                surveyInfo.SURVEY_CATEGORY = survey.SURVEY_CATEGORY;
                listInfo.Add(surveyInfo);

            }
            pagedListInfo.Entities = listInfo;
            pagedListInfo.HasNext = surveyPagedList.HasNext;
            pagedListInfo.HasPrevious = surveyPagedList.HasPrevious;
            pagedListInfo.TotalCount = surveyPagedList.TotalCount;
            pagedListInfo.TotalPages = surveyPagedList.TotalPages;
            return pagedListInfo;
        }


        public DataSet getCreditsCount(int userid)
        {
            try
            {
                  string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection con = new SqlConnection(connStr);
                SqlCommand scom = new SqlCommand("sp_GetCreditsCount", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dscreditscount = new DataSet();
                sda.Fill(dscreditscount);
                con.Close();
                return dscreditscount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public string getsurveytype(int USERID,int SURVEY_ID )
        {

            try
            {
                 string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection strconn = new SqlConnection(connStr);
                strconn.Open();

                SqlCommand scom = new SqlCommand("sp_ppstype", strconn);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@USERID", SqlDbType.VarChar)).Value = USERID;
                scom.Parameters.Add(new SqlParameter("@SURVEY_ID", SqlDbType.VarChar)).Value = SURVEY_ID;
                SqlDataAdapter sda = new SqlDataAdapter(scom);

                DataSet pricedetails = new DataSet();
                sda.SelectCommand = scom;
                sda.Fill(pricedetails);

                string Survey_Type = pricedetails.Tables[0].Rows[0][0].ToString();

                strconn.Close();
                return Survey_Type;
            }
             catch (Exception ex)
            {
               throw ex;
            }
        }

   public DataSet updatesurveyType(int userid,string surveytype,int surveyid, string licensetype,string status)
    {
        try
        {
            
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);    
            SqlCommand scom = new SqlCommand("sp_updatesurveytype", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@surveytype", SqlDbType.VarChar)).Value = surveytype;
            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@license_type", SqlDbType.VarChar)).Value = licensetype;
            scom.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar)).Value = status;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesurveytype = new DataSet();
            sda.Fill(dsupdatesurveytype);
            con.Close();
            return dsupdatesurveytype;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

   public DataSet getSurveyTypemdeploy(int surveyid)
   {
       try
       {
           string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
           SqlConnection con = new SqlConnection(connStr);    
           SqlCommand scom = new SqlCommand("sp_getSurveyType", con);
           scom.CommandType = CommandType.StoredProcedure;

           scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;

           SqlDataAdapter sda = new SqlDataAdapter(scom);
           DataSet dssurveytype = new DataSet();
           sda.Fill(dssurveytype);
           con.Close();
           return dssurveytype;
       }
       catch (Exception ex)
       {
           throw ex;
       }
   }

        #region "Method: AddSurvey"
        /// <summary>
        /// Add Survey
        /// </summary>
        /// <param name="osmSurvey"></param>
        /// <returns></returns>
        public osm_survey AddSurvey(osm_survey osmSurvey)
        {
            string serverPath = "";
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osmSurvey.CREATED_ON = DateTime.UtcNow;
                osmSurvey.MODIFIED_ON = DateTime.UtcNow;
                osmSurvey.SURVEY_CLOSE_DATE = null;
                osmSurvey.CHECK_BLOCKWORDS = 0;
                // osmSurvey.SurveyWidgetFlag = 0;
                osmSurvey.THEME = "Insighto Classic";
                osmSurvey.TEMPLATE_SUBCATEGORY = osmSurvey.TEMPLATE_SUBCATEGORY;
                osmSurvey.TEMPLATE_CATEGORY = osmSurvey.TEMPLATE_CATEGORY;
                osmSurvey.DELETED = 0;

                var isSurvey = (from survey in dbCon.osm_survey

                                where survey.USERID == osmSurvey.USERID && survey.SURVEY_NAME == osmSurvey.SURVEY_NAME && survey.DELETED == 0
                                select survey).ToList();

                if (isSurvey.Any())
                {
                    return null;
                }
                else
                {
                    dbCon.osm_survey.Add(osmSurvey);
                    dbCon.SaveChanges();
                    if (osmSurvey.SURVEY_ID > 0)
                    {
                        var surveyService = ServiceFactory.GetService<SurveyControls>();
                        surveyService.AddSurveyControls(osmSurvey.SURVEY_ID);
                    }
                }

            }
            serverPath = HttpContext.Current.Server.MapPath("~/");
            DirectoryInfo dir = new DirectoryInfo(serverPath);
            if (dir.Exists)
                Directory.CreateDirectory(serverPath + "In\\UserImages\\" + osmSurvey.USERID + "\\" + osmSurvey.SURVEY_ID.ToString());
            Addlaunchsurveytype(osmSurvey.SURVEY_ID);
            return osmSurvey;
        }
        #endregion

        public SqlConnection strconnectionstring()
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection strconn = new SqlConnection(connStr);

                strconn.Open();
                return strconn;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Addlaunchsurveytype(int surveyid)
        {
            try
            {
                //var sessionService = ServiceFactory.GetService<SessionStateService>();
                //var userInfo = sessionService.GetLoginUserDetailsSession();

                SqlConnection con = new SqlConnection();

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_savelaunchtypenumber", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;
                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dssurveyprofile = new DataSet();
                sda.Fill(dssurveyprofile);
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    
        #region "Method:GetSuvrveyDetailsBasedOnSurveyId"
        /// <summary>
        /// GetSuvrveyDetailsBasedOnSurveyId
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <returns></returns>
        public List<osm_survey> GetSuvrveyDetailsBasedOnSurveyId(int surveyId)
        {
            //var sfolder = new List<SurveyInfo>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {


                var sfolder = (from survey in dbCon.osm_survey
                               join folder in dbCon.osm_cl_folders on
                               survey.FOLDER_ID equals folder.FOLDER_ID
                               where survey.FOLDER_ID == folder.FOLDER_ID && survey.SURVEY_ID == surveyId && survey.DELETED == 0
                               select survey).ToList();

                return sfolder;
            }



        }
        #endregion

        public void ChangeSurveyFolder(List<osm_survey> osmsurvey)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                foreach (var surveyRecord in osmsurvey)
                {
                    var survey = dbCon.osm_survey.Where(u => u.SURVEY_ID == surveyRecord.SURVEY_ID && u.USERID == surveyRecord.USERID).FirstOrDefault();
                    survey.FOLDER_ID = surveyRecord.FOLDER_ID;
                    survey.MODIFIED_ON = DateTime.UtcNow;
                    dbCon.SaveChanges();
                }

            }

        }

        public void ChangeSurveyCategory(List<osm_survey> osmsurvey)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                foreach (var surveyRecord in osmsurvey)
                {
                    var survey = dbCon.osm_survey.Where(u => u.SURVEY_ID == surveyRecord.SURVEY_ID && u.USERID == surveyRecord.USERID).FirstOrDefault();
                    survey.SURVEY_CATEGORY = surveyRecord.SURVEY_CATEGORY;
                    survey.MODIFIED_ON = DateTime.UtcNow;
                    dbCon.SaveChanges();
                }

            }

        }

        public int DeleteSurveyBySurveyId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var outputParameter = new System.Data.Objects.ObjectParameter("returnvalue", typeof(int));
                dbCon.sp_DeleteSurveyBySurveyId(surveyId, outputParameter);
                return ValidationHelper.GetInteger(outputParameter.ToString(), 0);
            }

        }

        public List<osm_survey> GetSurveyByUserID(int UserId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveys = (from survey in dbCon.osm_survey
                               where survey.USERID == UserId && survey.DELETED == 0
                               select survey).ToList();

                return surveys;

            }

        }

        #region "Method:UpdateSurvey"
        /// <summary>
        /// Update Survey by surveyId
        /// </summary>
        /// <param name="osmSurvey"></param>
        /// <returns></returns>
        public bool UpdateSurvey(osm_survey osmSurvey)
        {
            bool isUpdated = default(bool);

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var isSurvey = (from survey in dbCon.osm_survey
                                where survey.USERID == osmSurvey.USERID && survey.SURVEY_NAME == osmSurvey.SURVEY_NAME && survey.DELETED == 0
                                && survey.SURVEY_ID != osmSurvey.SURVEY_ID
                                select survey).ToList();
                if (isSurvey.Any())
                {
                    isUpdated = false;
                }
                else
                {
                    osm_survey survey = dbCon.osm_survey.Single(s => s.SURVEY_ID == osmSurvey.SURVEY_ID && s.USERID == osmSurvey.USERID);
                    survey.SURVEY_CATEGORY = osmSurvey.SURVEY_CATEGORY;
                    survey.SURVEY_NAME = osmSurvey.SURVEY_NAME;
                    survey.FOLDER_ID = osmSurvey.FOLDER_ID;
                    survey.MODIFIED_ON = DateTime.UtcNow;
                    dbCon.SaveChanges();
                    isUpdated = true;
                }

            }
            return isUpdated;
        }
        #endregion

        #region "Method:SetSurveySetting"

        /// <summary>
        /// insert into osm_SurveySetiing by surveyId
        /// </summary>
        /// <param name="SurveyId"></param>
        public void SetSurveySetting(osm_surveysetting osmSurveySetting)
        {
            var surveySettings = ServiceFactory.GetService<SurveySetting>();
            surveySettings.SaveSurveySetting(osmSurveySetting);
        }
        #endregion

        #region "Method : UpdateModifedDateBySurId"
        public void UpdateModifedDateBySurId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_survey survey = dbCon.osm_survey.Single(s => s.SURVEY_ID == surveyId);
                survey.MODIFIED_ON = DateTime.UtcNow;
                dbCon.SaveChanges();
            }
        }
        #endregion

        public SurveyBasicInfoView UpdateSurveyLaunchOptions(SurveyBasicInfoView surveyOptions, int Launch, string Status = "")
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (surveyOptions.SURVEY_ID > 0)
                {
                    if (Status == "Active")
                    {
                        var surveyControlDetails = ServiceFactory.GetService<SurveyControls>().GetSurveycontrolsBySurveyId(surveyOptions.SURVEY_ID)
                                                    .FirstOrDefault(s => s.DELETED == 0);
                        var surveyLaunch = GetLaunchFromInfo(surveyOptions, surveyControlDetails);

                            ServiceFactory.GetService<LaunchSurveyService>().InsertLaunch(surveyLaunch);
                        var emailCount = ServiceFactory<EmailService>.Instance.GetEmailsByContactListId(surveyOptions.CONTACTLIST_ID);
                        ServiceFactory<SurveyControls>.Instance.UpdateEmailsCount(surveyOptions.SURVEY_ID, emailCount.Count);
                        if (surveyOptions.SCHEDULE_ON_ALERT == 1)
                        {
                            InsertIntoAlertManagement(surveyOptions.LAUNCH_ON_DATE, "SurveyLaunchOnDate", surveyOptions.SURVEY_ID);
                        }
                        // ServiceFactory.GetService<SurveyControls>().InsertSurveyControlsLog(surveyOptions.SURVEY_ID);
                    }
                    //Update the Survey Controls
                    UpdateSurveyControlsBasedonInfo(surveyOptions);
                    if (Status == "")
                    {
                        osm_survey survey = dbCon.osm_survey.FirstOrDefault(s => s.SURVEY_ID == surveyOptions.SURVEY_ID);
                        survey.MODIFIED_ON = DateTime.UtcNow;
                        dbCon.SaveChanges();

                        ServiceFactory.GetService<QuestionService>().SetPageBreak(surveyOptions.SURVEY_ID);

                        if (Launch == 0)
                        {
                            UpdateSurveyStatusToActive(surveyOptions.SURVEY_ID);
                            if (surveyOptions.REMINDER_TEXT.Length > 0)
                            {
                                var reminderDate = Convert.ToDateTime(surveyOptions.REMINDER_ON_DATE);
                                InsertIntoAlertManagement(reminderDate, surveyOptions.REMINDER_TEXT, surveyOptions.SURVEY_ID);
                            }
                        }
                    }
                }
            }
            return surveyOptions;
        }

        public void launchinsert(int surveyid, DateTime launchDate, int access, int layout, int emailcount, int completes, int remindercount)
        {
            try
            {
                SqlConnection con = new SqlConnection();

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_insertlaunchinfo", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@launchDate", SqlDbType.DateTime)).Value = launchDate;
                scom.Parameters.Add(new SqlParameter("@access", SqlDbType.Int)).Value = access;
                scom.Parameters.Add(new SqlParameter("@layout", SqlDbType.Int)).Value = layout;
                scom.Parameters.Add(new SqlParameter("@emailcount", SqlDbType.Int)).Value = emailcount;
                scom.Parameters.Add(new SqlParameter("@completes", SqlDbType.Int)).Value = completes;
                scom.Parameters.Add(new SqlParameter("@remindercount", SqlDbType.Int)).Value = remindercount;
                scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;
                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dssurveytype = new DataSet();
                sda.Fill(dssurveytype);
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateSurveyControlsTwFb(int surveyid, int access, int layout, string surveyLaunchURL, int status, int surveyurltype)
        {
            try
            {
                SqlConnection con = new SqlConnection();

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_updatesurveycontrolsnetsharing", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@access", SqlDbType.Int)).Value = access;
                scom.Parameters.Add(new SqlParameter("@layout", SqlDbType.Int)).Value = layout;
                scom.Parameters.Add(new SqlParameter("@status", SqlDbType.Int)).Value = status;
                scom.Parameters.Add(new SqlParameter("@surveyurltype", SqlDbType.Int)).Value = surveyurltype;
                scom.Parameters.Add(new SqlParameter("@surveyLaunchURL", SqlDbType.VarChar)).Value = surveyLaunchURL;
                scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;
                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dssurveytype = new DataSet();
                sda.Fill(dssurveytype);
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateSurveyLaunchOptionswithId(int surveyid, int access, int layout, string surveyLaunchURL, int Launch, string Status = "")
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (surveyid > 0)
                {
                    DataSet dsppstype = getSurveyType(surveyid);
                    string launchcnt = dsppstype.Tables[2].Rows[0][0].ToString();
                    int launchcountval = Convert.ToInt32(launchcnt);
                    int launchtypenumber = getLaunchTypeNumber(surveyid);
                    LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                    if (Status == "Active")
                    {
                        var surveyControlDetails = ServiceFactory.GetService<SurveyControls>().GetSurveycontrolsBySurveyId(surveyid)
                                                    .FirstOrDefault(s => s.DELETED == 0);
                        DateTime launchDate = Convert.ToDateTime(surveyControlDetails.LAUNCH_ON_DATE);
                        launchDate = (launchDate > Convert.ToDateTime("1/1/1900 12:00:00 AM")) ? launchDate : DateTime.UtcNow;

                        launchinsert(surveyid, launchDate, access, layout, 0, 0, 0);

                    }
                    //Update the Survey Controls
                    UpdateSurveyControlsTwFb(surveyid, access, layout, surveyLaunchURL, 1, 1);
                    if (Status == "")
                    {
                        osm_survey survey = dbCon.osm_survey.FirstOrDefault(s => s.SURVEY_ID == surveyid);
                        survey.MODIFIED_ON = DateTime.UtcNow;
                        dbCon.SaveChanges();

                        ServiceFactory.GetService<QuestionService>().SetPageBreak(surveyid);

                        if (Launch == 0)
                        {
                            UpdateSurveyStatusToActive(surveyid);
                        }
                    }
                }
            }

        }


        public void UpdateSurveyControlsBasedonInfo(SurveyBasicInfoView surveyOptions)
        {
            var surveyControls = ServiceFactory.GetService<SurveyControls>().GetSurveycontrolsBySurveyId(surveyOptions.SURVEY_ID)
                                .FirstOrDefault(s => s.SURVEYCONTROL_ID == surveyOptions.SURVEYCONTROL_ID);
            // var emailCount = ServiceFactory<EmailService>.Instance.GetEmailsByContactListId(surveyOptions.CONTACTLIST_ID);
            surveyControls.SURVEYURL_TYPE = surveyOptions.SURVEYURL_TYPE;
            surveyControls.EMAILLIST_TYPE = surveyOptions.EMAILLIST_TYPE;
            surveyControls.CONTACTLIST_ID = surveyOptions.CONTACTLIST_ID;
            if ((surveyControls.SURVEY_URL == "") || (surveyControls.SURVEY_URL == null))
            {
                surveyControls.SURVEYLAUNCH_URL = surveyOptions.SURVEYLAUNCH_URL;
            }
            surveyControls.EMAIL_INVITATION = surveyOptions.EMAIL_INVITATION;
            surveyControls.FROM_MAILADDRESS = surveyOptions.FROM_MAILADDRESS;
            surveyControls.MAIL_SUBJECT = surveyOptions.MAIL_SUBJECT;
            surveyControls.SENDER_NAME = surveyOptions.SENDER_NAME;
            surveyControls.REPLY_TO_EMAILADDRESS = surveyOptions.REPLY_TO_EMAILADDRESS;
            surveyControls.LAUNCHOPTIONS_STATUS = surveyOptions.LAUNCHOPTIONS_STATUS;
            surveyControls.UNIQUE_RESPONDENT = surveyOptions.UNIQUE_RESPONDENT;
            if ((surveyControls.SURVEY_URL == "") || (surveyControls.SURVEY_URL == null))
            {
                surveyControls.SURVEY_URL = surveyOptions.SURVEY_URL;
            }
            surveyControls.LAUNCHOPTIONS_STATUS = surveyOptions.LAUNCHOPTIONS_STATUS;
            surveyControls.SURVEY_LAYOUT = Convert.ToInt32(surveyOptions.SURVEY_LAYOUT);
            ServiceFactory.GetService<SurveyControls>().updateSurveyControls(surveyControls);
        }

        private void InsertIntoAlertManagement(DateTime reminderdate, string reminderText, int surveyId)
        {
            reminderdate = (reminderdate > Convert.ToDateTime("1/1/1900 12:00:00 AM")) ? reminderdate : Convert.ToDateTime("1/1/1900 12:00:00 AM");
            osm_alertmanagement alertMsg = new osm_alertmanagement();
            alertMsg.SURVEY_ID = surveyId;
            alertMsg.ALERT_TYPE = "ReminderText";
            alertMsg.ALERT_INFO = reminderText;
            alertMsg.ALERT_DATE = reminderdate;
            alertMsg.DELETED = ValidationHelper.GetInteger(AlertType.Active.ToString(), 0);
            alertMsg = ServiceFactory.GetService<AlertManagementService>().InsertAlertmanagement(alertMsg);
        }

        private void UpdateSurveyStatusToActive(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveyupdate = dbCon.osm_survey.FirstOrDefault(s => s.SURVEY_ID == surveyId);
                surveyupdate.STATUS = "Active";
                dbCon.SaveChanges();
            }
        }

        private osm_surveylaunch GetLaunchFromInfo(SurveyBasicInfoView surveyOptions, osm_surveycontrols surveyControlDetails)
        {
            DateTime launchDate = Convert.ToDateTime(surveyControlDetails.LAUNCH_ON_DATE);
            launchDate = (launchDate > Convert.ToDateTime("1/1/1900 12:00:00 AM")) ? launchDate : DateTime.UtcNow;
            var surveyLaunch = new osm_surveylaunch();
            surveyLaunch.SURVEY_ID = surveyOptions.SURVEY_ID;
            surveyLaunch.LAUNCHED_ON = launchDate;
            surveyLaunch.CONTACTLIST_ID = surveyOptions.CONTACTLIST_ID;
            surveyLaunch.FROM_MAILADDRESS = surveyOptions.FROM_MAILADDRESS;
            surveyLaunch.MAIL_SUBJECT = surveyOptions.MAIL_SUBJECT;
            surveyLaunch.EMAIL_INVITATION = surveyOptions.EMAIL_INVITATION;
            surveyLaunch.CREATED_ON = DateTime.UtcNow;
            surveyLaunch.SENDER_NAME = surveyOptions.SENDER_NAME;
            surveyLaunch.REPLY_TO_EMAILADDRESS = surveyOptions.REPLY_TO_EMAILADDRESS;
            surveyLaunch.UNIQUE_RESPONDENT = surveyOptions.UNIQUE_RESPONDENT;
            surveyLaunch.EMAIL_COUNT = 0;
            surveyLaunch.COMPLETES = 0;
            surveyLaunch.REMINDER_COUNT = 0;
            surveyLaunch.SURVEY_LAYOUT = surveyOptions.SURVEY_LAYOUT;
            return surveyLaunch;
        }

        private void UpdateSendToCount(osm_surveylaunch surveyLaunch)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (surveyLaunch.CONTACTLIST_ID > 0)
                {
                    var emailCount = (from email in dbCon.osm_emaillist
                                      where email.CONTACTLIST_ID == surveyLaunch.CONTACTLIST_ID && email.DELETED == 0
                                      && email.STATUS != "Unsubscribe"
                                      select email.EMAIL_ID
                                        ).Count();

                    var sendToCount = (from sendTo in dbCon.osm_surveycontrols
                                       where sendTo.SURVEY_ID == surveyLaunch.SURVEY_ID
                                       select sendTo.SEND_TO
                                      ).Take(1);
                    osm_surveycontrols surveyControl = dbCon.osm_surveycontrols.FirstOrDefault(s => s.SURVEY_ID == surveyLaunch.SURVEY_ID);
                    surveyControl.SEND_TO = ValidationHelper.GetInteger(emailCount.ToString(), 0) + ValidationHelper.GetInteger(sendToCount.ToString(), 0);
                    ServiceFactory.GetService<SurveyControls>().updateSurveyControls(surveyControl);
                }
            }
        }

        /// <summary>
        /// Gets the survey response questions by id.
        /// </summary>
        /// <param name="respondentId">The respondent id.</param>
        /// <returns></returns>
        public static List<osm_responsequestions> GetSurveyResponseQuestionsById(int respondentId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_responsequestions.Where(rs => rs.RESPONDENT_ID == respondentId).ToList();
            }
        }

        #region "Method: GetSurveyOptions"
        public List<osm_surveycontrols> GetSurveyOptions(int SurveyId, int SurveyControlId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_surveycontrols.Where(cont => cont.SURVEY_ID == SurveyId && cont.SURVEYCONTROL_ID == SurveyControlId).ToList();
            }

        }
        #endregion

        #region "Method: GetSurveyOptions"
        public List<osm_surveycontrols> GetSurveyOptions(int SurveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_surveycontrols.Where(cont => cont.SURVEY_ID == SurveyId && cont.DELETED == 0).ToList();
            }

        }
        #endregion

        #region "Method: InsertAlertManagement"
        public osm_alertmanagement InsertAlertManagement(osm_alertmanagement alertMsg)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_alertmanagement.Add(alertMsg);
                dbCon.SaveChanges();
            }
            return alertMsg;

        }
        #endregion


        #region "Method: CheckEmailLimit"
        public bool CheckEmailLimit(SurveyBasicInfoView survey, int EmailLimit)
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var count = dbCon.osm_emaillist.Where(email => email.DELETED == 0 && email.STATUS != "Unsubscribe" && email.CONTACTLIST_ID == survey.CONTACTLIST_ID).Count();
                if (EmailLimit == -1)
                {
                    return true;
                }
                else if (survey.LICENSE_TYPE == UserType.FREE.ToString())
                {
                    var surveyLaunch = dbCon.osm_surveylaunch.Where(email => email.SURVEY_ID == survey.SURVEY_ID).ToList();
                    var sum = surveyLaunch.Sum(s => s.EMAIL_COUNT);
                    if (count + sum > EmailLimit)
                        return false;
                    else
                        return true;
                }
                else
                {
                    if (survey.EMAIL_COUNT + count > EmailLimit)
                        return false;
                    else
                        return true;

                }


            }


        }
        #endregion


        public bool UpdateSurveyBlock(int surveyId)
        {
            bool blockStatus = false;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                //var osmSurvey = (from survey in dbCon.osm_survey
                //                 where survey.SURVEY_ID == surveyId && survey.DELETED == 0
                //                 select survey).SingleOrDefault();
                //osmSurvey.CHECK_BLOCKWORDS = 0;
                //dbCon.SaveChanges();

                dbCon.osm_survey.Where(sr => sr.SURVEY_ID == surveyId).FirstOrDefault().CHECK_BLOCKWORDS = 0;
                dbCon.SaveChanges();
                blockStatus = true;
            }

            return blockStatus;
        }

        //public PagedList<osm_survey> GetPagedSurveys(PagingCriteria pagingCriteria)
        //{
        //    using (InsightoV2Entities dbCon = new InsightoV2Entities())
        //    {

        //    }
        //}

        public List<osm_survey> GetSurveysByCategory(string category)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveys = dbCon.osm_survey.Where(s => s.USERID == -1 && s.TEMPLATE_CATEGORY == category).ToList();


                return surveys;

            }
        }

        public PagedList<osm_survey> GetPagedSurveysByCategory(string category, PagingCriteria pagingCriteria)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_survey.Where(s => s.USERID == -1 && (s.TEMPLATE_CATEGORY == category || s.TEMPLATE_SUBCATEGORY == category))
                                       .ToPagedList(pagingCriteria);
            }
        }


        ///Need to move  survey response surveys.
        public ResponseCountView GetResponseSurveyBySurveyId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.ResponseCountViews.Where(s => s.SURVEY_ID == surveyId).FirstOrDefault();
            }
        }



        #region "Method :CopySurveyDet "
        /// <summary>
        /// Copy all Survey details to another survey.
        /// </summary>
        /// <param name="copyFromSurId"></param>
        /// <param name="copyToSurId"></param>
        public void CopySurveyDet(int copyFromSurId, int copyToSurId, string flag = "")
        {
            var surveyIntroductionService = ServiceFactory.GetService<SurveyIntroductionService>();
            var surveySettingService = ServiceFactory.GetService<SurveySetting>();
            var questionService = ServiceFactory.GetService<QuestionService>();
            var surveyContrrolService = ServiceFactory.GetService<SurveyControls>();
            var surveyFromDetails = ServiceFactory<SurveyService>.Instance.GetSurveyBySurveyId(copyFromSurId).FirstOrDefault();
            ChangeSurveyTheme(surveyFromDetails != null ? surveyFromDetails.THEME : "Insighto Classic", copyToSurId);
            surveyIntroductionService.CopySurveyIntro(copyFromSurId, copyToSurId);
            surveySettingService.CopySurveySetting(copyFromSurId, copyToSurId);
            questionService.CopySurveyQuestion(copyFromSurId, copyToSurId);
            surveyContrrolService.UpdateCopySurveyControls(copyFromSurId, copyToSurId);
        }
        #endregion

        #region FindSurveysByUserId
        public PagedList<osm_survey> FindSurveysByUserId(int userId, string category, PagingCriteria pagingCriteria)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (userId == -1)
                {
                    if (category != "All")
                    {
                       var survey= dbCon.osm_survey.Where(s => s.USERID == -1 && (s.TEMPLATE_SUBCATEGORY == category) && s.STATUS == "Draft")
                            .ToPagedList(pagingCriteria);
                       if (survey.TotalCount == 0)
                       {
                           return dbCon.osm_survey.Where(s => s.USERID == userId && s.TEMPLATE_CATEGORY == category && s.STATUS == "Draft")
                                                                                 .ToPagedList(pagingCriteria);
                       }
                       else
                       {
                       return dbCon.osm_survey.Where(s => s.USERID == -1 && (s.TEMPLATE_SUBCATEGORY == category) && s.STATUS == "Draft")
                                                      .ToPagedList(pagingCriteria);
                       }
                       
                    }
                    else
                        return dbCon.osm_survey.Where(s => s.USERID == -1 && s.STATUS == "Draft")
                        .ToPagedList(pagingCriteria);

                }
                else
                {
                    return dbCon.osm_survey.Where(s => s.USERID == userId && s.SURVEY_CATEGORY == category)
                                                       .ToPagedList(pagingCriteria);
                }
            }

        }
        #endregion

        #region "Method : GetCategoriesBySurveyId"
        /// <summary>
        /// This method has been written to get surveys other than created survey in copy 
        /// exsisting survey functionality
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="category"></param>
        /// <param name="pagingCriteria"></param>
        /// <returns></returns>
        public PagedList<osm_survey> GetCategoriesBySurveyId(int userId, string category, PagingCriteria pagingCriteria, int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (userId == -1)
                {

                    return FindSurveysByUserId(userId, category, pagingCriteria);
                }
                else
                {
                    if (category != "All")
                    {
                        return dbCon.osm_survey.OrderBy(s => s.SURVEY_ID).Where(s => s.USERID == userId && s.SURVEY_CATEGORY == category && s.SURVEY_ID != surveyId && s.DELETED == 0)
                                                              .ToPagedList(pagingCriteria);
                    }
                    else
                    {
                        return dbCon.osm_survey.OrderBy(s => s.SURVEY_ID).Where(s => s.USERID == userId && s.SURVEY_ID != surveyId && s.DELETED == 0)
                                                             .ToPagedList(pagingCriteria);
                    }


                }
            }

        }
        #endregion

        public List<SurveyManagerView> FindSuveysByUserIdBySurveyId(int userId, int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.SurveyManagerViews.Where(d => d.USERID == userId && d.SURVEY_ID == surveyId).ToList();
            }
        }

        public List<osm_surveylaunch> FindSurveyLaunchBySurveyId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_surveylaunch.Where(l => l.SURVEY_ID == surveyId).ToList();
            }

        }

        public List<osm_alertmanagement> FindSurveyAlertsBySurveyId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_alertmanagement.Where(a => a.SURVEY_ID == surveyId && a.DELETED == 0).ToList();
            }

        }


        #region Method: GetSendReminderSurveys
        /// <summary>
        /// Used to get the Send Reminder Surveys.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>

        public List<SurveySendReminder> GetSendReminderSurveys(int userId)
        {
            var srInfo = new List<SurveySendReminder>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                srInfo = (from sdv in dbCon.SurveyDashboardView
                          where sdv.USERID == userId && sdv.EMAILLIST_TYPE == 1 && sdv.DELETED == 0

                          select new SurveySendReminder
                          {
                              SURVEY_ID = sdv.SURVEY_ID,
                              SURVEY_NAME = sdv.SURVEY_NAME,
                              Unique_respondent = sdv.EMAILLIST_TYPE
                          }
                         ).ToList();
            }
            return srInfo;
        }
        #endregion

        #region Method: GetSendReminderSurveysInvitation
        /// <summary>
        /// Used to get the Send Reminder Surveys invitations.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>

        public List<ReminderEmailEnvitation> GetSendReminderSurveysInvitation(int surveyId)
        {
            var sriInfo = new List<ReminderEmailEnvitation>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                sriInfo = (from sl in dbCon.osm_surveylaunch
                           join cl in dbCon.osm_contactlist on sl.CONTACTLIST_ID equals cl.CONTACTLIST_ID
                           where sl.SURVEY_ID == surveyId

                           select new ReminderEmailEnvitation
                           {
                               LAUNCH_ID = sl.LAUNCH_ID,
                               SURVEY_ID = sl.SURVEY_ID,
                               LAUNCHED_ON = sl.LAUNCHED_ON,
                               CONTACTLIST_ID = sl.CONTACTLIST_ID,
                               SEND_TO = sl.SEND_TO,
                               TOTAL_RESPONSES = sl.TOTAL_RESPONSES,
                               PARTIAL_RESPONSES = sl.PARTIAL_RESPONSES,
                               SCREEN_OUTS = sl.SCREEN_OUTS,
                               STATUS = sl.STATUS,
                               DELETED = sl.DELETED,
                               FROM_MAILADDRESS = sl.FROM_MAILADDRESS,
                               MAIL_SUBJECT = sl.MAIL_SUBJECT,
                               EMAIL_INVITATION = sl.EMAIL_INVITATION,
                               CREATED_ON = sl.CREATED_ON,
                               LAUNCHMAIL_STATUS = sl.LAUNCHMAIL_STATUS,
                               SENDER_NAME = sl.SENDER_NAME,
                               REPLY_TO_EMAILADDRESS = sl.REPLY_TO_EMAILADDRESS,
                               EMAIL_COUNT = sl.EMAIL_COUNT,
                               CONTACTLIST_NAME = sl.CONTACTLIST_NAME,
                               COMPLETES = sl.COMPLETES,
                               REMINDER_COUNT = sl.REMINDER_COUNT,
                               UNIQUE_RESPONDENT = sl.UNIQUE_RESPONDENT,
                               CONLIST_CONTACTLIST_NAME = cl.CONTACTLIST_NAME,
                               CONLIST_CREATED_ON = cl.CREATED_ON,
                               CONLIST_DELETED = cl.DELETED,
                               EmailCount = (from emaillist in dbCon.osm_emaillist
                                             where emaillist.CONTACTLIST_ID == cl.CONTACTLIST_ID
                                             group emaillist by emaillist.EMAIL_ADDRESS).Count()
                           }
                         ).ToList();
            }
            return sriInfo;
        }
        #endregion

        #region Method: GetSendReminderSurveys
        /// <summary>
        /// Used to get the Send Reminder info.
        /// </summary>
        /// <param name="reminderId"></param>
        /// <returns></returns>

        public List<osm_surveyreminder> GetSendReminderInfo(int reminderId)
        {
            var srInfo = new List<osm_surveyreminder>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                srInfo = (from sr in dbCon.osm_surveyreminder
                          where sr.REMINDER_ID == reminderId
                          select sr).ToList();
            }

            return srInfo;
        }
        #endregion

        #region Method: GetBouncedMailCountByLaunchId
        /// <summary>
        /// Used to get the Send Reminder info.
        /// </summary>
        /// <param name="reminderId"></param>
        /// <returns></returns>

        public int GetBouncedMailCountByLaunchId(int launchId)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return (from sr in dbCon.osm_surveylaunch
                        join list in dbCon.osm_emaillist
                        on sr.CONTACTLIST_ID equals list.CONTACTLIST_ID
                        join bmail in dbCon.osm_BouncedMails
                        on list.EMAIL_ADDRESS equals bmail.EMAIL_ID
                        where sr.LAUNCH_ID == launchId
                        select sr).Count();
            }

        }
        #endregion

        #region Method: SaveSendReminder
        /// <summary>
        /// Used to save the Send Reminder info.
        /// </summary>
        /// <param name="reminderId"></param>
        /// <returns></returns>

        public int SaveSendReminder(osm_surveyreminder rEI, int surveyId)
        {
            int ReminderID = 0;
            DateTime? dt = DateTime.UtcNow;
            int? isurveyId = surveyId;
            var srInfo = new List<osm_surveyreminder>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.sp_SurveySendReminderMail(rEI.LAUNCH_ID, rEI.FROM_MAILADDRESS, rEI.MAIL_SUBJECT, rEI.EMAIL_INVITATION, dt, rEI.SENDER_NAME, rEI.REPLY_TO_EMAILADDRESS, isurveyId, 0);
            }

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                ReminderID = (from sr in dbCon.osm_surveyreminder
                              where sr.LAUNCH_ID == rEI.LAUNCH_ID && sr.FROM_MAILADDRESS == rEI.FROM_MAILADDRESS && sr.REPLY_TO_EMAILADDRESS == rEI.REPLY_TO_EMAILADDRESS && sr.MAIL_SUBJECT == rEI.MAIL_SUBJECT && sr.SENDER_NAME == rEI.SENDER_NAME
                              select sr.REMINDER_ID).Max();
            }
            return ReminderID;
        }
        #endregion


        public int ChangeSurveyStatus(int surveyId, string status)
        {
            int result = 0;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var survey = dbCon.osm_survey.Where(s => s.SURVEY_ID == surveyId).FirstOrDefault();
                if (survey != null)
                {
                    survey.STATUS = status;
                    survey.MODIFIED_ON = DateTime.UtcNow;
                    result = dbCon.SaveChanges();
                }
            }
            return result;
        }

        public PagedList<osm_survey> FindSurveysByContactListId(int contactListId, PagingCriteria pagingCriteria)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                var survey = (from s in dbCon.osm_survey
                              from l in dbCon.osm_surveylaunch
                              where l.SURVEY_ID == s.SURVEY_ID
                                  && l.CONTACTLIST_ID == contactListId
                              select s).ToPagedList(pagingCriteria);


                return survey;
            }
        }

        public bool CheckSurveyProFeatures(int surveyId)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            var surveyQuestions = ServiceFactory.GetService<QuestionService>().GetAllSurveyQuestionsBySurveyId(surveyId);
            var surveyControl = ServiceFactory.GetService<SurveyControls>().GetSurveycontrolsBySurveyId(surveyId).FirstOrDefault();
            var surveySettings = ServiceFactory.GetService<SurveySetting>().GetSurveySettingsBySurveyId(surveyId).FirstOrDefault();

            int chkFlag = 0;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var launchId = dbCon.osm_surveylaunch.Where(l => l.SURVEY_ID == surveyId).FirstOrDefault();
                if (listFeatures.Find(licenceType, FeatureSurveyCreation.SKIPLOGIC).Value == 0)
                {
                    foreach (var qns in surveyQuestions)
                    {
                        if (qns.SKIP_LOGIC == 1)
                        {
                            return false;
                        }
                    }
                }
                if (chkFlag == 0 && listFeatures.Find(licenceType, FeatureSurveyCreation.QUESTION_PER_SURVEY).Value != -1)
                {
                    if (surveyQuestions.Count > listFeatures.Find(licenceType, FeatureSurveyCreation.QUESTION_PER_SURVEY).Value)
                    {
                        return false;
                    }
                }
                if (chkFlag == 0)
                {
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYLAUNCH).Value == 0 && surveyControl.SCHEDULE_ON == 1)
                    {
                        return false;
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYCLOSE).Value == 0 && surveyControl.SCHEDULE_ON_CLOSE == 1)
                    {
                        return false;
                    }
                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 0 && (surveyControl.THANKUPAGE_TYPE == 1 || surveyControl.CLOSEPAGE_TYPE == 1 || surveyControl.SCREENOUTPAGE_TYPE == 1 || surveyControl.OVERQUOTAPAGE_TYPE == 1))
                    {
                        return false;
                    }

                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 0 && (surveyControl.THANKUPAGE_TYPE == 1 || surveyControl.CLOSEPAGE_TYPE == 1 || surveyControl.SCREENOUTPAGE_TYPE == 1 || surveyControl.OVERQUOTAPAGE_TYPE == 1))
                    {
                        return false;
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.REDIRECTION_URL).Value == 0 && (surveyControl.URL_REDIRECTION != null && surveyControl.URL_REDIRECTION.Length > 0))
                    {
                        return false;
                    }
                    if ((listFeatures.Find(licenceType, FeatureDeploymentType.UNIQUE_RESPONDENT).Value == 0 && (surveyControl.UNIQUE_RESPONDENT == 0)) || (listFeatures.Find(licenceType, FeatureDeploymentType.UNIQUE_RESPONDENT).Value == 0 && (surveyControl.UNIQUE_RESPONDENT == 2)))
                    {
                        return false;
                    }

                    if (listFeatures.Find(licenceType, FeatureDeploymentType.SENDER_NAME).Value == 5 && surveyControl.EMAILLIST_TYPE == 1 && surveyControl.SENDER_NAME != null && surveyControl.SENDER_NAME.ToLower() == userInfo.FirstName)
                    {
                        return false;
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.SENDER_EMAILADDRESS).Value == 5 && surveyControl.EMAILLIST_TYPE == 1 && surveyControl.FROM_MAILADDRESS != null && surveyControl.FROM_MAILADDRESS.ToLower() == userInfo.LoginName)
                    {
                        return false;
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.EMAIL_SUBJECT).Value == 5 && surveyControl.EMAILLIST_TYPE == 1 && surveyControl.MAIL_SUBJECT != null && ((surveyControl.MAIL_SUBJECT.ToLower() == "survey invitation") || (surveyControl.MAIL_SUBJECT.ToLower() != "")))
                    {
                        return false;
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.REPLY_TO_EMAILADDRESS).Value == 5 && surveyControl.EMAILLIST_TYPE == 1 && surveyControl.REPLY_TO_EMAILADDRESS != null && surveyControl.REPLY_TO_EMAILADDRESS.ToLower() == userInfo.LoginName)
                    {
                        return false;
                    }

                    if (listFeatures.Find(licenceType, FeaturePersonalisationType.SURVEY_LOGO).Value == 0 && surveySettings.SURVEY_LOGO == "0" && surveySettings.SURVEY_LOGO != null && surveySettings.SURVEY_LOGO.Length > 0)
                    {
                        return false;
                    }

                    if (listFeatures.Find(licenceType, FeaturePersonalisationType.HEADER).Value == 0 && surveySettings.SURVEY_HEADER != null && surveySettings.SURVEY_HEADER.Length > 0)
                    {
                        return false;
                    }

                    if (listFeatures.Find(licenceType, FeaturePersonalisationType.FOOTER).Value == 0 && surveySettings.SURVEY_FOOTER != null && surveySettings.SURVEY_FOOTER.Length > 0)
                    {
                        return false;
                    }

                    if (listFeatures.Find(licenceType, FeaturePersonalisationType.BUTTONSETTINGS).Value == 0 && surveySettings.SURVEY_BUTTON_TYPE != null && surveySettings.SURVEY_BUTTON_TYPE.Length > 0)
                    {
                        return false;
                    }
                    if (listFeatures.Find(licenceType, FeaturePersonalisationType.CUSTOMIZE_FONT).Value == 0 && (surveySettings.SURVEY_FONT_TYPE != "Arial" || surveySettings.SURVEY_FONT_COLOR != "Black" || surveySettings.SURVEY_FONT_SIZE != "12"))
                    {
                        return false;
                    }

                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.PAGEBREAK).Value == 0)
                    {
                        foreach (var qns in surveyQuestions)
                            if (qns.PAGE_BREAK == 1)
                            {
                                return false;
                            }
                    }

                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.RESPONSE_REQUIRED).Value == 0)
                    {
                        foreach (var qns in surveyQuestions)
                        {
                            if (qns.RESPONSE_REQUIRED == 1)
                            {
                                return false;
                            }
                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.OTHERS_TEXT).Value == 0)
                    {
                        foreach (var qns in surveyQuestions)
                        {
                            if (qns.OTHER_ANS == 1)
                            {
                                return false;
                            }
                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.CHARACTER_LIMIT).Value == 0)
                    {
                        foreach (var qns in surveyQuestions)
                        {
                            if (qns.QUESTION_TYPE_ID == ValidationHelper.GetInteger(QuestionType.Text_CommentBox, 0) && qns.CHAR_LIMIT != 0 && qns.CHAR_LIMIT != 2000)
                            {
                                return false;

                            }
                            else if ((qns.QUESTION_TYPE_ID == ValidationHelper.GetInteger(QuestionType.Text_SingleRowBox, 0) || qns.QUESTION_TYPE_ID == ValidationHelper.GetInteger(QuestionType.Text_MultipleBoxes, 0) || qns.QUESTION_TYPE_ID == ValidationHelper.GetInteger(QuestionType.ConstantSum, 0)) && qns.CHAR_LIMIT != 0 && qns.CHAR_LIMIT != 500)
                            {
                                return false;
                            }

                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureQuestionType.Q_Video).Value == 0)
                    {
                        foreach (var qns in surveyQuestions)
                        {
                            if (qns.QUESTION_TYPE_ID == ValidationHelper.GetInteger(QuestionType.Video, 0))
                            {
                                return false;
                            }
                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureQuestionType.Q_IMAGE).Value == 0)
                    {
                        foreach (var qns in surveyQuestions)
                        {
                            if (qns.QUESTION_TYPE_ID == ValidationHelper.GetInteger(QuestionType.Image, 0))
                            {
                                return false;
                            }
                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureQuestionType.QM_SIDEBYSIDE).Value == 0)
                    {
                        foreach (var qns in surveyQuestions)
                        {
                            if (qns.QUESTION_TYPE_ID == ValidationHelper.GetInteger(QuestionType.Matrix_SideBySide, 0))
                            {
                                return false;
                            }
                        }
                    }

                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.RANDOMIZE_ANSWER).Value == 0)
                    {
                        foreach (var qns in surveyQuestions)
                        {
                            if (qns.RANDOMIZE_ANSWERS == 1)
                            {
                                return false;
                            }
                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.ANSWER_ALIGN).Value == 0)
                    {
                        foreach (var qns in surveyQuestions)
                        {
                            if (qns.ANSWER_ALIGNSTYLE != "Horizontal" && qns.ANSWER_ALIGNSTYLE != "1Column")
                            {
                                return false;
                            }
                        }
                    }

                    if (listFeatures.Find(licenceType, FeatureDeploymentType.UNIQUE_RESPONDENT).Value == 0)
                    {
                        int count = dbCon.osm_surveylaunch.Where(l => l.SURVEY_ID == surveyId && l.CONTACTLIST_ID > 0 && l.UNIQUE_RESPONDENT == 0).Count();
                        if (count > 0)
                        {
                            return false;
                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.UNIQUE_RESPONDENT_PASSWORD).Value == 0)
                    {
                        int count = dbCon.osm_surveylaunch.Where(l => l.SURVEY_ID == surveyId && l.CONTACTLIST_ID > 0 && l.UNIQUE_RESPONDENT == 2).Count();
                        if (count > 0)
                        {
                            return false;
                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.SENDER_NAME).Value == 5)
                    {
                        int count = dbCon.osm_surveylaunch.Where(l => l.CONTACTLIST_ID > 0 && l.SURVEY_ID == surveyId && l.SENDER_NAME != userInfo.FirstName).Count();
                        int respondentCount = dbCon.osm_surveyreminder.Where(r => r.LAUNCH_ID == launchId.LAUNCH_ID && r.SENDER_NAME != userInfo.FirstName).Count();
                        if (count > 0 || respondentCount > 0)
                        {
                            return false;
                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.SENDER_EMAILADDRESS).Value == 5)
                    {
                        int count = dbCon.osm_surveylaunch.Where(l => l.SURVEY_ID == surveyId && l.CONTACTLIST_ID > 0 && l.FROM_MAILADDRESS != userInfo.LoginName).Count();
                        int respondentCount = dbCon.osm_surveyreminder.Where(r => r.LAUNCH_ID == launchId.LAUNCH_ID && r.FROM_MAILADDRESS != userInfo.LoginName).Count();
                        if (count > 0)
                        {
                            return false;
                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.EMAIL_SUBJECT).Value == 5)
                    {
                        int count = dbCon.osm_surveylaunch.Where(l => l.SURVEY_ID == surveyId && l.CONTACTLIST_ID > 0 && l.MAIL_SUBJECT != "Survey Invitation" && l.MAIL_SUBJECT != "").Count();
                        int respondentCount = dbCon.osm_surveyreminder.Where(r => r.LAUNCH_ID == launchId.LAUNCH_ID && r.MAIL_SUBJECT != "Survey Invitation" && r.MAIL_SUBJECT != "").Count();
                        if (count > 0)
                        {
                            return false;
                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.REPLY_TO_EMAILADDRESS).Value == 5)
                    {
                        int count = dbCon.osm_surveylaunch.Where(l => l.SURVEY_ID == surveyId && l.CONTACTLIST_ID > 0 && l.REPLY_TO_EMAILADDRESS != userInfo.LoginName).Count();
                        int respondentCount = dbCon.osm_surveyreminder.Where(r => r.LAUNCH_ID == launchId.LAUNCH_ID && r.REPLY_TO_EMAILADDRESS != userInfo.LoginName).Count();
                        if (count > 0)
                        {
                            return false;
                        }
                    }

                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.NO_OF_RESPONSES).Value != -1)
                    {
                        int count = dbCon.osm_responsesurvey.Where(r => r.SURVEY_ID == surveyId && r.STATUS == "Complete").Count();
                        if (count > 0)
                        {
                            return false;
                        }
                    }
                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.EMAIL_LIMIT).Value != -1)
                    {
                        var emailCount = (from e in dbCon.osm_surveylaunch
                                          where e.SURVEY_ID == surveyId && e.CONTACTLIST_ID > 0 && e.DELETED == 0
                                          select e.EMAIL_COUNT
                                            ).FirstOrDefault();
                        if (emailCount != null && emailCount > listFeatures.Find(licenceType, FeatureSurveyCreation.EMAIL_LIMIT).Value)
                        {
                            return false;
                        }
                    }

                }
            }
            return true;
        }

        #region "Method: GetSurveysUserId"
        public List<SurveyDashboardView> GetSurveysUserId(int userId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveys = dbCon.SurveyDashboardView.Where(s => s.USERID == userId);
                surveys = from p in surveys
                          group p by p.SURVEY_ID into grp
                          select grp.OrderByDescending(g => g.LAUNCH_ID).FirstOrDefault();
                return surveys.ToList();
            }

        }
        #endregion

        #region "Method: GetSurveysByUserId"
        public List<osm_survey> GetSurveysByUserId(int userId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_survey.Where(s => s.USERID == userId && s.DELETED == 0).OrderBy(s => s.SURVEY_NAME).ToList();
            }

        }
        #endregion


        #region Method: GetUserSurveyDetails
        public PagedList<UserSurveyInfo> GetUserSurveyDetails(int userId, PagingCriteria pagingCriteria)
        {
            var surveyList = new List<UserSurveyInfo>();
            var uSurveyInfo = new PagedList<UserSurveyInfo>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var SurveyInfo = (from osmSurvey in dbCon.osm_survey
                                  join sControl in dbCon.osm_surveycontrols on
                                  osmSurvey.SURVEY_ID equals sControl.SURVEY_ID
                                  where osmSurvey.USERID == userId && osmSurvey.DELETED == 0
                                  select new UserSurveyInfo
                                  {
                                      UserId = osmSurvey.USERID,
                                      SurveyId = osmSurvey.SURVEY_ID,
                                      SurveyName = osmSurvey.SURVEY_NAME,
                                      CategoryName = osmSurvey.SURVEY_CATEGORY,
                                      Status = osmSurvey.STATUS,
                                      SendTo = sControl.SEND_TO,
                                      Responses = sControl.TOTAL_RESPONSES,

                                  }
                         ).ToPagedList(pagingCriteria);
                foreach (var survey in SurveyInfo.Entities)
                {
                    var info = new UserSurveyInfo();
                    info.url = EncryptHelper.EncryptQuerystring("../CreateQuestionLibrary.aspx", "SurveyId=" + survey.SurveyId + "&UserId=" + survey.UserId);
                    info.SurveyId = survey.SurveyId;
                    info.SurveyName = survey.SurveyName;
                    info.CategoryName = survey.CategoryName;
                    info.Status = survey.Status;
                    info.SendTo = survey.SendTo;
                    info.Responses = survey.Responses;
                    surveyList.Add(info);
                }
                uSurveyInfo.Entities = surveyList;
                uSurveyInfo.HasNext = SurveyInfo.HasNext;
                uSurveyInfo.HasPrevious = SurveyInfo.HasPrevious;
                uSurveyInfo.TotalCount = SurveyInfo.TotalCount;
                uSurveyInfo.TotalPages = SurveyInfo.TotalPages;

            }
            return uSurveyInfo;

        }
        #endregion

        #region "Method:GetUserDeletedSurveyDetails"

        public PagedList<SurveyDashboard> GetUserDeletedSurveyDetails(int userId, PagingCriteria pagingCriteria)
        {
            var surveyList = new List<SurveyDashboard>();
            var uSurveyInfo = new PagedList<SurveyDashboard>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var SurveyInfo = (from osmSurvey in dbCon.SurveyDashboardView
                                  where osmSurvey.USERID == userId && osmSurvey.DELETED == 1 && osmSurvey.SurveyWidgetFlag == 0
                                  select new SurveyDashboard
                                  {
                                      UserId = osmSurvey.USERID,
                                      SurveyId = osmSurvey.SURVEY_ID,
                                      SurveyName = osmSurvey.SURVEY_NAME,
                                      CategoryName = osmSurvey.SURVEY_CATEGORY,
                                      Status = osmSurvey.STATUS,
                                      SendTo = osmSurvey.SEND_TO,
                                      Responses = osmSurvey.TOTAL_RESPONSES,
                                      Name = osmSurvey.NAME,

                                  }
                         );
                SurveyInfo = from p in SurveyInfo
                             group p by p.SurveyId into grp
                             select grp.OrderByDescending(g => g.SurveyName).FirstOrDefault();
                //}
                var pagedList = SurveyInfo.ToPagedList(pagingCriteria);

                uSurveyInfo.Entities = pagedList.Entities;
                uSurveyInfo.HasNext = pagedList.HasNext;
                uSurveyInfo.HasPrevious = pagedList.HasPrevious;
                uSurveyInfo.TotalCount = pagedList.TotalCount;
                uSurveyInfo.TotalPages = pagedList.TotalPages;

            }
            return uSurveyInfo;

        }
        #endregion

        //#region "Method:GetUserDeletedSurveyDetails"

        //      public List<osm_survey> GetTemplateCategories()
        //      {
        //          using (InsightoV2Entities dbCon = new InsightoV2Entities())
        //          {
        //             //var templateCategory = (from p in dbCon.osm_survey
        //             //                     orderby p.TEMPLATE_CATEGORY ascending
        //             //                     group p.TEMPLATE_SUBCATEGORY by p.TEMPLATE_CATEGORY into subcats
        //             //                     select subcats);
        //             //return templateCategory.ToList();
        //              var listInfo = (from p in dbCon.osm_survey
        //                              group p by new { p.TEMPLATE_CATEGORY} into newInfo
        //                              select new newInfo);


        //          }
        //      }
        //#endregion
        public int UpdateBlockedWordStatus(int surveyId)
        {
            int result = 0;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var survey = dbCon.osm_survey.Where(s => s.SURVEY_ID == surveyId).FirstOrDefault();
                if (survey != null)
                {
                    survey.STATUS = "Active";
                    survey.CHECK_BLOCKWORDS = 2;
                    survey.MODIFIED_ON = DateTime.UtcNow;
                    result = dbCon.SaveChanges();
                }
            }
            return result;
        }

        #region Method: GetUserSurveyDetails
        public PagedList<SurveyDashboard> GetUserDashoboard(int userId, PagingCriteria pagingCriteria)
        {
            var surveyList = new List<SurveyDashboard>();
            var uSurveyInfo = new PagedList<SurveyDashboard>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var SurveyInfo = (from osmSurvey in dbCon.SurveyDashboardView
                                  where osmSurvey.USERID == userId && osmSurvey.DELETED == 0 && osmSurvey.SurveyWidgetFlag == 0
                                  select new SurveyDashboard
                                  {
                                      UserId = osmSurvey.USERID,
                                      SurveyId = osmSurvey.SURVEY_ID,
                                      SurveyName = osmSurvey.SURVEY_NAME,
                                      CategoryName = osmSurvey.SURVEY_CATEGORY,
                                      Status = osmSurvey.STATUS,
                                      SendTo = osmSurvey.SEND_TO,
                                      Responses = osmSurvey.TOTAL_RESPONSES,

                                  }
                         );
                SurveyInfo = from p in SurveyInfo
                             group p by p.SurveyId into grp
                             select grp.OrderByDescending(g => g.SurveyName).FirstOrDefault();
                //}
                var pagedList = SurveyInfo.ToPagedList(pagingCriteria);
                foreach (var survey in pagedList.Entities)
                {
                    var info = new SurveyDashboard();
                    info.url = EncryptHelper.EncryptQuerystring("../CreateQuestionLibrary.aspx", "SurveyId=" + survey.SurveyId + "&UserId=" + survey.UserId);
                    info.SurveyId = survey.SurveyId;
                    info.SurveyName = survey.SurveyName;
                    info.CategoryName = survey.CategoryName;
                    info.Status = survey.Status;
                    info.SendTo = survey.SendTo;
                    info.Responses = survey.Responses;
                    surveyList.Add(info);
                }
                uSurveyInfo.Entities = surveyList;
                uSurveyInfo.HasNext = pagedList.HasNext;
                uSurveyInfo.HasPrevious = pagedList.HasPrevious;
                uSurveyInfo.TotalCount = pagedList.TotalCount;
                uSurveyInfo.TotalPages = pagedList.TotalPages;

            }
            return uSurveyInfo;

        }
        #endregion

        public int RestoreSurveyBySurveyId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var outputParameter = new System.Data.Objects.ObjectParameter("returnvalue", typeof(int));
                dbCon.RestoreDeletedSurveyBySurveyId(surveyId, outputParameter);
                return ValidationHelper.GetInteger(outputParameter.ToString(), 0);
            }

        }

        public MigrationAuditInfo GetMigratedAuditInfoByV1Survey(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.MigrationAuditInfo.Where(s => s.V1SurveyId == surveyId).FirstOrDefault();
            }
        }

        public void ChangeSurveyTheme(string strTheme, int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var survey = dbCon.osm_survey.FirstOrDefault(s => s.SURVEY_ID == surveyId);
                if (survey != null)
                {
                    survey.THEME = strTheme;
                    dbCon.SaveChanges();
                }
            }
        }

        public List<PollAnswerOptions> getPollAnswerWiseCount(int pollid)
        {
            List<PollAnswerOptions> pollansweroptions = new List<PollAnswerOptions>();
            DataSet dspollansweroptionscount = new DataSet();
            try
            {
                //pollansweroptions = new List<PollAnswerOptions>();
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection con = new SqlConnection(connStr);
                SqlCommand scom = new SqlCommand("sp_GetPollAnswerWiseCount", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@PollId", SqlDbType.Int)).Value = pollid;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                sda.Fill(dspollansweroptionscount);
                con.Close();
                if (dspollansweroptionscount.Tables.Count > 0)
                {
                    if (dspollansweroptionscount.Tables[0].Rows.Count > 0)
                    {
                        for (int x = 0; x < dspollansweroptionscount.Tables[0].Rows.Count; x++)
                        {
                            pollansweroptions.Add(new PollAnswerOptions
                            {
                                answerid = (int)dspollansweroptionscount.Tables[0].Rows[x]["pk_answerid"],
                                answeroption = dspollansweroptionscount.Tables[0].Rows[x]["answeroption"].ToString(),
                                answercount = (int)dspollansweroptionscount.Tables[0].Rows[x]["answercount"]
                            });
                        }
                    }
                }
            }
            catch (Exception)
            {
                //return 
            }
            return pollansweroptions;
        }

        public DataSet getpollSurveyAnalytics(int pollid)
        {
            try
            {

                SqlConnection con = new SqlConnection();

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_pollsurveyAnalytics", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@pollID", SqlDbType.Int)).Value = pollid;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dssurveyprofile = new DataSet();
                sda.Fill(dssurveyprofile);
                con.Close();
                return dssurveyprofile;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


    }


}


