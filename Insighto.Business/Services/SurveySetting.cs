﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using System.Diagnostics;
using System.Globalization;
using CovalenseUtilities.Helpers;
using System.IO;
using Insighto.Business.Enumerations;
using System.Data;
using System.Data.Objects;
using System.Collections;

namespace Insighto.Business.Services
{
    public class SurveySetting : BusinessServiceBase
    {
        public UserType GetLicenceType
        {
            get
            {
                var serviceSession = ServiceFactory.GetService<SessionStateService>();
                var userDet = serviceSession.GetLoginUserDetailsSession();
                if (userDet != null)
                {
                    return GenericHelper.ToEnum<UserType>(userDet.LicenseType);
                }
                else
                {
                    return GenericHelper.ToEnum<UserType>(LicenceType);
                    
                }
            }
        }
        public string LicenceType
        {
            get
            {
                Hashtable ht = new Hashtable();
                string licenseType="";
                if (HttpContext.Current.Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(HttpContext.Current.Request["key"].ToString());
                    if (ht != null && ht.Contains("UserId"))
                    {
                        var userDetails = ServiceFactory.GetService<UsersService>().GetUserDetails(ValidationHelper.GetInteger(ht["UserId"].ToString(), 0)).FirstOrDefault();
                        if (userDetails != null)
                            licenseType= userDetails.LICENSE_TYPE;
                    }
                }
                return licenseType;
            }
        }


        #region "Method:GetSurveySettings"
        /// <summary>
        /// 
        /// </summary>
        public List<osm_surveysetting> GetSurveySettings(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var suveySettings = (from surSetting in dbCon.osm_surveysetting
                                     where surSetting.SURVEY_ID == surveyId && surSetting.DELETED == 0
                                     select surSetting).ToList();

                return suveySettings;
            }


        }
        #endregion

        #region "Method : SaveSurveySetting"
        public void SaveSurveySetting(osm_surveysetting  osmSurveySetting)
        {
            var surveyService = ServiceFactory.GetService<SurveyService>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                //osmSurveySetting.SURVEYSETTING_ID = 2221;           
               
                dbCon.osm_surveysetting.Add(osmSurveySetting);
                dbCon.SaveChanges();
                surveyService.UpdateModifedDateBySurId(osmSurveySetting.SURVEY_ID);
            }
        }
        #endregion

        #region "Method : UpdateSurveySetting"
        public void UpdateSurveySetting(osm_surveysetting osmSurveySetting)
        {
            var surveyService = ServiceFactory.GetService<SurveyService>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveySettings =
                        dbCon.osm_surveysetting.Where(
                            s =>
                            s.SURVEY_ID == osmSurveySetting.SURVEY_ID);
                foreach (var deletedItem in surveySettings.ToList())
                {
                    dbCon.osm_surveysetting.Remove(deletedItem);
                    dbCon.SaveChanges();
                }
                dbCon.osm_surveysetting.Add(osmSurveySetting);
                dbCon.SaveChanges();
                surveyService.UpdateModifedDateBySurId(osmSurveySetting.SURVEY_ID);
            }
        }
        #endregion

        #region "Method:GetSurveySettingsBySurveyId"
        /// <summary>
        /// GetSurveySettingsBySurveyId
        /// </summary>
        public List<osm_surveysetting> GetSurveySettingsBySurveyId(int surveyId)
        {
            var suveySettings = new List<osm_surveysetting>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                suveySettings = (from surSetting in dbCon.osm_surveysetting
                                 where surSetting.SURVEY_ID == surveyId && surSetting.DELETED == 0
                                 select surSetting).ToList();

                return suveySettings;
            }


        }
        #endregion

        #region "Method : GetThemes"
        /// <summary>
        /// GetSurveySettingsBySurveyId
        /// </summary>
        public List<osm_themes> GetThemes()
        {
            var themes = new List<osm_themes>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                themes = (from theme in dbCon.osm_themes
                          where theme.DELETED == 0
                          select theme).ToList();
                return themes;
            }


        }
        #endregion

        #region "Method : Save"
        /// <summary>
        /// Used to update survey theme.
        /// </summary>
        /// <param name="osmSurveyIntro"></param>
        /// <returns></returns>

        public void Save(osm_survey osmSurvey)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var isSurvey = (from survey in dbCon.osm_survey
                                where survey.SURVEY_ID == osmSurvey.SURVEY_ID && survey.DELETED == 0
                                select survey).ToList();
                if (isSurvey.Any())
                {
                    osm_survey survey = dbCon.osm_survey.FirstOrDefault(si => si.SURVEY_ID == osmSurvey.SURVEY_ID && si.DELETED == 0);
                    survey.THEME = osmSurvey.THEME;
                    dbCon.SaveChanges();

                    int surveyId = ValidationHelper.GetInteger(osmSurvey.SURVEY_ID.ToString(), 0);
                    var surveyService = ServiceFactory.GetService<SurveyService>();
                    surveyService.UpdateModifedDateBySurId(surveyId);
                }

            }
        }
        #endregion

        #region "Method : SaveOrUpdateSurveySetting"
        /// <summary>
        /// Used to update survey settings based on flag.
        /// </summary>
        /// <param name="osmsurveysetting"></param>
        /// <param name="flag"></param>

        public void SaveOrUpdateSurveySetting(osm_surveysetting osmsurveysetting, string flag)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var isSurveysetting = (from surveysetting in dbCon.osm_surveysetting
                                       where surveysetting.SURVEY_ID == osmsurveysetting.SURVEY_ID && surveysetting.DELETED == 0
                                       select surveysetting).ToList();

                if (isSurveysetting.Any())
                {
                    osm_surveysetting surveysetting = dbCon.osm_surveysetting.FirstOrDefault(si => si.SURVEY_ID == osmsurveysetting.SURVEY_ID && si.DELETED == 0);

                    if (flag == "H")
                    {
                        surveysetting.SURVEY_HEADER = osmsurveysetting.SURVEY_HEADER;
                        surveysetting.SURVEY_FOOTER = osmsurveysetting.SURVEY_FOOTER;
                    }

                    if (flag == "F")
                    {
                        surveysetting.SURVEY_FONT_TYPE = osmsurveysetting.SURVEY_FONT_TYPE;
                        surveysetting.SURVEY_FONT_SIZE = osmsurveysetting.SURVEY_FONT_SIZE;
                        surveysetting.SURVEY_FONT_COLOR = osmsurveysetting.SURVEY_FONT_COLOR;
                    }

                    if (flag == "B")
                    {
                        surveysetting.SURVEY_BUTTON_TYPE = osmsurveysetting.SURVEY_BUTTON_TYPE;
                        surveysetting.CUSTOMSTART_TEXT = osmsurveysetting.CUSTOMSTART_TEXT;
                        surveysetting.CUSTOMSUBMITTEXT = osmsurveysetting.CUSTOMSUBMITTEXT;
                        surveysetting.CUSTOM_CONTINUE_TEXT = osmsurveysetting.CUSTOM_CONTINUE_TEXT;
                        surveysetting.BUTTON_BG_COLOR = osmsurveysetting.BUTTON_BG_COLOR;
                        surveysetting.BROWSER_BACKBUTTON = osmsurveysetting.BROWSER_BACKBUTTON;   
                    }
                    if (flag == "L")
                    {
                        surveysetting.SURVEY_LOGO = osmsurveysetting.SURVEY_LOGO;
                        surveysetting.COMPANY_NAME = osmsurveysetting.COMPANY_NAME;
                        surveysetting.Show_Logo = osmsurveysetting.Show_Logo;   
                    }

                    if (flag == "A")
                    {
                        surveysetting.SURVEY_HEADER = osmsurveysetting.SURVEY_HEADER;
                        surveysetting.SURVEY_FOOTER = osmsurveysetting.SURVEY_FOOTER;
                        surveysetting.SURVEY_FONT_TYPE = osmsurveysetting.SURVEY_FONT_TYPE;
                        surveysetting.SURVEY_FONT_SIZE = osmsurveysetting.SURVEY_FONT_SIZE;
                        surveysetting.SURVEY_FONT_COLOR = osmsurveysetting.SURVEY_FONT_COLOR;
                        surveysetting.SURVEY_BUTTON_TYPE = osmsurveysetting.SURVEY_BUTTON_TYPE;
                        surveysetting.CUSTOMSTART_TEXT = osmsurveysetting.CUSTOMSTART_TEXT;
                        surveysetting.CUSTOMSUBMITTEXT = osmsurveysetting.CUSTOMSUBMITTEXT;
                        surveysetting.SURVEY_LOGO = osmsurveysetting.SURVEY_LOGO;
                        surveysetting.COMPANY_NAME = osmsurveysetting.COMPANY_NAME;
                    }

                    if (flag == "U")
                    {
                        surveysetting.SURVEY_LOGO = osmsurveysetting.SURVEY_LOGO;
                        surveysetting.Show_Logo = osmsurveysetting.Show_Logo;  
                    }
                    if (flag == "P")
                    {
                        if (String.IsNullOrEmpty(osmsurveysetting.ProgressbarText))
                        {
                            surveysetting.ProgressbarText ="Questions viewed";
                        }
                        else
                        {
                            surveysetting.ProgressbarText = osmsurveysetting.ProgressbarText;
                        }
                        surveysetting.Show_Progressbar = osmsurveysetting.Show_Progressbar;
                        surveysetting.Progressbar_Type = osmsurveysetting.Progressbar_Type;  
                        surveysetting.Show_Progressbartext = osmsurveysetting.Show_Progressbartext;  
                    }
                   
                    dbCon.SaveChanges();

                }
                else
                {

                    dbCon.osm_surveysetting.Add(osmsurveysetting);
                    dbCon.SaveChanges();
                }

            }

        }

        #endregion

        #region "Method:CopySurveySetting "
        /// <summary>
        /// Copy  SurveySetting from one to another
        /// </summary>
        /// <param name="copyFromSurId"></param>
        /// <param name="copyToSurId"></param>
        public void CopySurveySetting(int copyFromSurId, int copyToSurId)
        {
            var surSett = ServiceFactory.GetService<SurveySetting>();
            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            osm_surveysetting surDet = surSett.GetSurveySettingsBySurveyId(copyFromSurId).FirstOrDefault();

            if (surDet != null)
            {
                surDet.SURVEY_HEADER = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.HEADER).Value == 0) ? "" : surDet.SURVEY_HEADER;
                surDet.SURVEY_FOOTER = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.FOOTER).Value == 0) ? "" : surDet.SURVEY_FOOTER;
                surDet.SURVEY_LOGO = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.SURVEY_LOGO).Value == 0) ? "" : surDet.SURVEY_LOGO;
                surDet.SURVEY_FONT_TYPE = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.CUSTOMIZE_FONT).Value == 0) ? "Arial" : surDet.SURVEY_FONT_TYPE;
                surDet.SURVEY_FONT_COLOR = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.CUSTOMIZE_FONT).Value == 0) ? "Black" : surDet.SURVEY_FONT_COLOR;
                surDet.SURVEY_FONT_SIZE = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.CUSTOMIZE_FONT).Value == 0) ? "9" : surDet.SURVEY_FONT_SIZE;
                surDet.RESPONSE_REQUIRED = (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.RESPONSE_REQUIRED).Value == 0) ? 0 : surDet.RESPONSE_REQUIRED;
                surDet.PAGE_BREAK = (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.PAGEBREAK).Value == 0) ? 0 : surDet.PAGE_BREAK;

                if ((serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.BUTTONSETTINGS).Value != 1))
                {
                    surDet.SURVEY_BUTTON_TYPE = "";
                    surDet.CUSTOMSTART_TEXT = "";
                    surDet.CUSTOMSUBMITTEXT = "";
                }
                surDet.SURVEY_ID = copyToSurId;
                surSett.UpdateSurveySetting(surDet);
            }            
        }
        #endregion

        #region "Method:CopySurveySettingForQuestionLibrary "
        /// <summary>
        /// Copy  SurveySetting from one to another
        /// </summary>
        /// <param name="copyFromSurId"></param>
        /// <param name="copyToSurId"></param>
        public void CopySurveySettingForQuestionLibrary(int copyFromSurId, int copyToSurId)
        {
            var surSett = ServiceFactory.GetService<SurveySetting>();         
            osm_surveysetting surDet = surSett.GetSurveySettingsBySurveyId(copyFromSurId).FirstOrDefault();

            if (surDet != null)
            {
                //surDet.SURVEY_HEADER = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.HEADER).Value == 0) ? "" : surDet.SURVEY_HEADER;
                //surDet.SURVEY_FOOTER = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.FOOTER).Value == 0) ? "" : surDet.SURVEY_FOOTER;
                //surDet.SURVEY_LOGO = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.SURVEY_LOGO).Value == 0) ? "" : surDet.SURVEY_LOGO;
                //surDet.SURVEY_FONT_TYPE = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.CUSTOMIZE_FONT).Value == 0) ? "Arial" : surDet.SURVEY_FONT_TYPE;
                //surDet.SURVEY_FONT_COLOR = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.CUSTOMIZE_FONT).Value == 0) ? "Black" : surDet.SURVEY_FONT_COLOR;
                //surDet.SURVEY_FONT_SIZE = (serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.CUSTOMIZE_FONT).Value == 0) ? "9" : surDet.SURVEY_FONT_SIZE;
                //surDet.RESPONSE_REQUIRED = (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.RESPONSE_REQUIRED).Value == 0) ? 0 : surDet.RESPONSE_REQUIRED;
                //surDet.PAGE_BREAK = (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.PAGEBREAK).Value == 0) ? 0 : surDet.PAGE_BREAK;

                //if ((serviceFeature.Find(GetLicenceType, FeaturePersonalisationType.BUTTONSETTINGS).Value != 1))
                //{
                //    surDet.SURVEY_BUTTON_TYPE = "";
                //    surDet.CUSTOMSTART_TEXT = "";
                //    surDet.CUSTOMSUBMITTEXT = "";
                //}
                surDet.SURVEY_ID = copyToSurId;
                surSett.UpdateSurveySetting(surDet);
            }
        }
        #endregion

        #region "Method : GetProgressBar"
        /// <summary>
        /// GetSurveySettingsBySurveyId
        /// </summary>
        public List<osm_progressbar> GetProgressBar()
        {
            var progressBars = new List<osm_progressbar>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                progressBars = (from progressBar in dbCon.osm_progressbar
                                where progressBar.DELETED == 0
                                select progressBar).ToList();
                return progressBars;
            }


        }
        #endregion

        #region "Method : RemoveSurveyLogo"
        public void RemoveSurveyLogo(int surveyId)
        {
            var surveyService = ServiceFactory.GetService<SurveyService>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_surveysetting surveysetting =
                        dbCon.osm_surveysetting.Where(
                            s =>
                            s.SURVEY_ID == surveyId).FirstOrDefault();

                surveysetting.SURVEY_LOGO = string.Empty;
                surveysetting.Show_Logo = false;
                dbCon.SaveChanges();               
            }
        }
        #endregion
    }
}
