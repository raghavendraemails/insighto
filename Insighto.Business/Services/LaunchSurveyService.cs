﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;
using CovalenseUtilities.Services;

namespace Insighto.Business.Services
{
   public class LaunchSurveyService : BusinessServiceBase
    {
        #region "Method: InsertLaunch"
        public osm_surveylaunch InsertLaunch(osm_surveylaunch launch)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_surveylaunch.Add(launch);
                dbCon.SaveChanges();
            }
            return launch;

        }
        #endregion

        #region "Method: GetLaunchBySurveyId"
        public List<osm_surveylaunch> GetLaunchBySurveyId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
               return dbCon.osm_surveylaunch.Where(l => l.SURVEY_ID == surveyId).ToList();              
            }
            

        }
        #endregion

        #region "Method: InsertBouncedMails"
        public void InsertBouncedMails(osm_BouncedMails bounced)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_BouncedMails.Add(bounced);
                dbCon.SaveChanges();
            }       

        }
        #endregion
    }
}
