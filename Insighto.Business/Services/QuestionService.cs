﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Insighto.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using System.Diagnostics;
using System.Globalization;
using CovalenseUtilities.Helpers;
using System.IO;
using Insighto.Business.Enumerations;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace Insighto.Business.Services
{
    public class QuestionService : BusinessServiceBase
    {

        public UserType GetLicenceType
        {
            get
            {
                var serviceSession = ServiceFactory.GetService<SessionStateService>();
                var userDet = serviceSession.GetLoginUserDetailsSession();
                if (userDet != null)
                {
                    return GenericHelper.ToEnum<UserType>(userDet.LicenseType);
                }
                else
                {
                    return GenericHelper.ToEnum<UserType>(LicenceType);

                }
            }
        }
        public string LicenceType
        {
            get
            {
                Hashtable ht = new Hashtable();
                string licenseType = "";
                if (HttpContext.Current.Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(HttpContext.Current.Request["key"].ToString());
                    if (ht != null && ht.Contains("UserId"))
                    {
                        var userDetails = ServiceFactory.GetService<UsersService>().GetUserDetails(ValidationHelper.GetInteger(ht["UserId"].ToString(), 0)).FirstOrDefault();
                        if (userDetails != null)
                            licenseType = userDetails.LICENSE_TYPE;
                    }
                }
                return licenseType;
            }
        }

        #region "Method : "
        public void AddORUpdateSurveyQuestion(int questionID, List<string> ans, osm_surveyquestion osmQuestions, int insertQuesSeq)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (questionID > 0)
                {
                    UpdateSurveyQuestion(osmQuestions);

                    if (osmQuestions.QUESTION_TYPE_ID == 4)
                    {

                    }

                }
                else
                {

                    if (insertQuesSeq == 0)
                    {
                        //int sFolder = (from question in dbCon.osm_surveyquestion
                        //               where question.SURVEY_ID == osmQuestions.SURVEY_ID
                        //               orderby 
                        //               select question.QUESTION_SEQ);

                        int? sFolder = dbCon.osm_surveyquestion.Where(s => s.SURVEY_ID == osmQuestions.SURVEY_ID)
                                                               .OrderByDescending(s => s.QUESTION_SEQ)
                                                               .Select(s => s.QUESTION_SEQ)
                                                               .FirstOrDefault();

                        if (sFolder != null)
                        {
                            osmQuestions.QUESTION_SEQ = sFolder + 1;
                        }

                        if (osmQuestions.QUESTION_SEQ == 0)
                        {
                            osmQuestions.QUESTION_SEQ = 1;
                        }


                    }

                    else if (insertQuesSeq > 0)
                    {
                        osmQuestions.QUESTION_SEQ = insertQuesSeq;
                    }

                    int quesId = AddSurveyQuestion(osmQuestions);

                   

                    if (quesId > 0)
                    {
                        osm_answeroptions osmAnsOptions = new osm_answeroptions();
                        int questionTypeId;
                        questionTypeId = Convert.ToInt16(osmQuestions.QUESTION_TYPE_ID);
                        if (questionTypeId == 4)
                        {
                            for (int k = 0; k <= 1; k++)
                            {
                                if (k == 0)
                                {
                                    osmAnsOptions.ANSWER_OPTIONS = "Yes";
                                }
                                else if (k == 1)
                                {
                                    osmAnsOptions.ANSWER_OPTIONS = "No";
                                }
                                osmAnsOptions.QUESTION_ID = quesId;
                                osmAnsOptions.DEMOGRAPIC_BLOCKID = 0;
                                osmAnsOptions.RESPONSE_REQUIRED = 0;

                                AddAnsOptions(osmAnsOptions);
                            }

                        }
                        else
                        {
                            //string val = "";

                            for (int i = 0; i < ans.Count; i++)
                            {
                                int contResReqID = (questionTypeId == 20) ? Convert.ToInt32(ans[i].Split('~')[1]) : 0;

                                if (questionTypeId == 20)
                                {
                                    if (Convert.ToString(ans[i]).Split('~')[0] != " ")
                                    {
                                        osmAnsOptions.ANSWER_OPTIONS = Convert.ToString(ans[i].Split('~')[0]);
                                        osmAnsOptions.QUESTION_ID = quesId;
                                        osmAnsOptions.DEMOGRAPIC_BLOCKID = i + 1;
                                        osmAnsOptions.RESPONSE_REQUIRED = contResReqID;

                                        osmAnsOptions.SKIP_QUESTION_ID = 0;
                                        osmAnsOptions.DELETED = 0;

                                        AddAnsOptions(osmAnsOptions);
                                    }

                                }
                                else
                                {
                                    osmAnsOptions.ANSWER_OPTIONS = Convert.ToString(ans[i]);
                                    osmAnsOptions.QUESTION_ID = quesId;
                                    osmAnsOptions.DEMOGRAPIC_BLOCKID = 0;
                                    osmAnsOptions.RESPONSE_REQUIRED = 0;
                                    osmAnsOptions.SKIP_QUESTION_ID = 0;
                                    osmAnsOptions.DELETED = 0;

                                    AddAnsOptions(osmAnsOptions);
                                }


                            }// end of for

                        }

                    }

                }// end of main if
            }
        }

        public void AddORUpdateSurveyQuestion(int questionID, List<string> ans, osm_surveyquestion osmQuestions, int insertQuesSeq,string othertextval)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (questionID > 0)
                {
                    UpdateSurveyQuestion(osmQuestions);

                    if (osmQuestions.QUESTION_TYPE_ID == 4)
                    {

                    }

                }
                else
                {

                    if (insertQuesSeq == 0)
                    {
                        //int sFolder = (from question in dbCon.osm_surveyquestion
                        //               where question.SURVEY_ID == osmQuestions.SURVEY_ID
                        //               orderby 
                        //               select question.QUESTION_SEQ);

                        int? sFolder = dbCon.osm_surveyquestion.Where(s => s.SURVEY_ID == osmQuestions.SURVEY_ID)
                                                               .OrderByDescending(s => s.QUESTION_SEQ)
                                                               .Select(s => s.QUESTION_SEQ)
                                                               .FirstOrDefault();

                        if (sFolder != null)
                        {
                            osmQuestions.QUESTION_SEQ = sFolder + 1;
                        }

                        if (osmQuestions.QUESTION_SEQ == 0)
                        {
                            osmQuestions.QUESTION_SEQ = 1;
                        }


                    }

                    else if (insertQuesSeq > 0)
                    {
                        osmQuestions.QUESTION_SEQ = insertQuesSeq;
                    }

                    int quesId = AddSurveyQuestion(osmQuestions);
                    DataSet dsupdateother = updateOtherText(quesId, othertextval);


                    if (quesId > 0)
                    {
                        osm_answeroptions osmAnsOptions = new osm_answeroptions();
                        int questionTypeId;
                        questionTypeId = Convert.ToInt16(osmQuestions.QUESTION_TYPE_ID);
                        if (questionTypeId == 4)
                        {
                            for (int k = 0; k <= 1; k++)
                            {
                                if (k == 0)
                                {
                                    osmAnsOptions.ANSWER_OPTIONS = "Yes";
                                }
                                else if (k == 1)
                                {
                                    osmAnsOptions.ANSWER_OPTIONS = "No";
                                }
                                osmAnsOptions.QUESTION_ID = quesId;
                                osmAnsOptions.DEMOGRAPIC_BLOCKID = 0;
                                osmAnsOptions.RESPONSE_REQUIRED = 0;

                                AddAnsOptions(osmAnsOptions);
                            }

                        }
                        else
                        {
                            //string val = "";

                            for (int i = 0; i < ans.Count; i++)
                            {
                                int contResReqID = (questionTypeId == 20) ? Convert.ToInt32(ans[i].Split('~')[1]) : 0;

                                if (questionTypeId == 20)
                                {
                                    if (Convert.ToString(ans[i]).Split('~')[0] != " ")
                                    {
                                        osmAnsOptions.ANSWER_OPTIONS = Convert.ToString(ans[i].Split('~')[0]);
                                        osmAnsOptions.QUESTION_ID = quesId;
                                        osmAnsOptions.DEMOGRAPIC_BLOCKID = i + 1;
                                        osmAnsOptions.RESPONSE_REQUIRED = contResReqID;

                                        osmAnsOptions.SKIP_QUESTION_ID = 0;
                                        osmAnsOptions.DELETED = 0;

                                        AddAnsOptions(osmAnsOptions);
                                    }

                                }
                                else
                                {
                                    osmAnsOptions.ANSWER_OPTIONS = Convert.ToString(ans[i]);
                                    osmAnsOptions.QUESTION_ID = quesId;
                                    osmAnsOptions.DEMOGRAPIC_BLOCKID = 0;
                                    osmAnsOptions.RESPONSE_REQUIRED = 0;
                                    osmAnsOptions.SKIP_QUESTION_ID = 0;
                                    osmAnsOptions.DELETED = 0;

                                    AddAnsOptions(osmAnsOptions);
                                }


                            }// end of for

                        }

                    }

                }// end of main if
            }
        }
        #endregion

        #region
        public void AddAnsOptions(osm_answeroptions osmAnsOptions)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osmAnsOptions.CREATED_ON = DateTime.UtcNow;
                osmAnsOptions.LAST_MODIFIED_ON = DateTime.UtcNow;

                dbCon.osm_answeroptions.Add(osmAnsOptions);
                dbCon.SaveChanges();
            }
        }
        #endregion

        #region "Method: GetSurveyquestions"
        public List<osm_surveyquestion> GetSurveyquestionsBySurveyId(int SurveyId)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var maxQuestionSeq = (from qns in dbCon.osm_surveyquestion
                                      where qns.DELETED == 0 && qns.SURVEY_ID == SurveyId
                                      select qns.QUESTION_SEQ).Max();
                return dbCon.osm_surveyquestion.Where(qns => qns.SURVEY_ID == SurveyId && qns.DELETED == 0 && qns.QUESTION_SEQ == maxQuestionSeq).ToList<osm_surveyquestion>();
            }

        }
        #endregion

        #region "Method: DeleteQuestionByQuestionId"
        /// <summary>
        /// This method is used to delete Question by QuestionId.
        /// </summary>
        /// <param name="QuestionId"></param>

        public void DeleteQuestionByQuestionId(int QuestionId)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.sp_DeleteQuestion(QuestionId);

            }

        }
        #endregion

        #region "Method : GetSurveyquestionsDet"
        /// <summary>
        /// GetSurveyquestionsDet
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <returns></returns>
        public List<osm_surveyquestion> GetSurveyquestionsDet(int SurveyId)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var QuestDet = (from qns in dbCon.osm_surveyquestion
                                where qns.DELETED == 0 && qns.SURVEY_ID == SurveyId
                                select qns.QUESTION_SEQ).Max();
                return dbCon.osm_surveyquestion.Where(qns => qns.SURVEY_ID == SurveyId && qns.DELETED == 0).ToList<osm_surveyquestion>();
            }

        }
        #endregion

        #region "UpdateSurveyQuestion"
        public void UpdateSurveyQuestion(osm_surveyquestion osmQuestions)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_surveyquestion surQuestion = dbCon.osm_surveyquestion.FirstOrDefault(s => s.QUESTION_ID == osmQuestions.QUESTION_ID);
                surQuestion.QUSETION_LABEL = osmQuestions.QUSETION_LABEL;
                surQuestion.QUESTION_TYPE_ID = osmQuestions.QUESTION_TYPE_ID;
                surQuestion.QUESTION_SEQ = osmQuestions.QUESTION_SEQ;
                surQuestion.SKIP_LOGIC = osmQuestions.SKIP_LOGIC;
                surQuestion.RESPONSE_REQUIRED = osmQuestions.RESPONSE_REQUIRED;
                surQuestion.RANDOMIZE_ANSWERS = osmQuestions.RANDOMIZE_ANSWERS;
                surQuestion.PAGE_BREAK = osmQuestions.PAGE_BREAK;
                surQuestion.OTHER_ANS = osmQuestions.OTHER_ANS;
                surQuestion.LASTMODIFIED_ON = DateTime.UtcNow;
                surQuestion.ANSWER_ALIGNSTYLE = osmQuestions.ANSWER_ALIGNSTYLE;
                surQuestion.CHAR_LIMIT = osmQuestions.CHAR_LIMIT;
                dbCon.SaveChanges();
            }
        }
        #endregion

        #region "Method : UpdatePageBreakOrReposeRequired"
        /// <summary>
        /// Used to update PageBreak or ResposeRequired flag with osm_surveyquestion table.
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <param name="questionId"></param>
        /// <param name="pageBreakOrReposeRequired"></param>
        /// <param name="mode"></param>


        public void UpdatePageBreakOrReposeRequired(int surveyId, int questionId, int pageBreakOrReposeRequired, int mode)
        {
            var qTypeIdsFPageBreak = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 19, 20, 21 };
            var qTypeIdsFRR = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 21 };

            using (var dbCon = new InsightoV2Entities())
            {
                if (mode == 1)
                {
                    var rowQuestion = dbCon.osm_surveyquestion.FirstOrDefault(x => x.QUESTION_ID == questionId && qTypeIdsFPageBreak.Contains(x.QUESTION_TYPE_ID ?? 0));
                    if (rowQuestion != null)
                    {
                        if (pageBreakOrReposeRequired > 0)
                        {
                            rowQuestion.PAGE_BREAK += 1;
                        }
                        else
                        {
                            rowQuestion.PAGE_BREAK -= 1;
                        }
                        dbCon.SaveChanges();
                    }
                }
                else
                {
                    var rowQuestion = dbCon.osm_surveyquestion.FirstOrDefault(x => x.QUESTION_ID == questionId && qTypeIdsFRR.Contains(x.QUESTION_TYPE_ID ?? 0));
                    if (rowQuestion != null)
                    {
                        rowQuestion.RESPONSE_REQUIRED = pageBreakOrReposeRequired;
                        dbCon.SaveChanges();
                    }
                }



                var surveyService = ServiceFactory.GetService<SurveyService>();
                surveyService.UpdateModifedDateBySurId(surveyId);
            }
        }

        #endregion

        #region "Method : CheckPageBreaksAllQuestions"
        /// <summary>
        /// used to check page break applied to all the questions or not.
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <param name="pageBreak"></param>

        public bool CheckPageBreaksAllQuestions(int surveyId, int pageBreak)
        {
            bool update = false;
            using (var dbCon = new InsightoV2Entities())
            {
                var qTypeIdsFPageBreak = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20 };
                var surveyQuestions = dbCon.osm_surveyquestion.Where(sq => sq.SURVEY_ID == surveyId && sq.SKIP_LOGIC == 0 && qTypeIdsFPageBreak.Contains(sq.QUESTION_TYPE_ID ?? 0) && sq.PAGE_BREAK == pageBreak).ToList();
                if (surveyQuestions.Any())
                {
                    update = true;
                }

            }

            return update;
        }
        #endregion

        #region "Method : CheckResponseRequiredAllQuestions"
        /// <summary>
        /// used to check respose required applied all the questions or not.
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <param name="resposeRequired"></param>

        public bool CheckResponseRequiredAllQuestions(int surveyId, int resposeRequired)
        {
            bool update = false;

            using (var dbCon = new InsightoV2Entities())
            {
                var questionTypeIds = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20 }; //16, 17, 18, 19, 21
                var surveyQuestions = dbCon.osm_surveyquestion.Where(sq => sq.SURVEY_ID == surveyId && sq.SKIP_LOGIC == 0 && questionTypeIds.Contains(sq.QUESTION_TYPE_ID ?? 0) && sq.RESPONSE_REQUIRED == resposeRequired && sq.DELETED == 0).ToList();
                if (surveyQuestions.Any())
                {
                    update = true;
                }

            }

            return update;
        }
        #endregion

        #region "Method : UpdatePageBreaksAllQuestions"
        /// <summary>
        /// Update page break flag to all quations except skiplogic is not equal to one.
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <param name="pageBreak"></param>

        public int UpdatePageBreaksAllQuestions(int surveyId, bool enablePageBreak)
        {
            int update = 0;
            using (var dbCon = new InsightoV2Entities())
            {
                var qTypeIdsFPageBreak = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20 };//19
                var surveyQuestions = dbCon.osm_surveyquestion.Where(sq => sq.SURVEY_ID == surveyId && sq.SKIP_LOGIC == 0 && qTypeIdsFPageBreak.Contains(sq.QUESTION_TYPE_ID ?? 0)).ToList();
                if (surveyQuestions.Any())
                {
                    if (enablePageBreak)
                    {
                        surveyQuestions.ForEach(sq => sq.PAGE_BREAK += 1);
                    }
                    else
                    {
                        surveyQuestions.ForEach(sq => sq.PAGE_BREAK -= 1);
                    }
                    dbCon.SaveChanges();

                    var surveyService = ServiceFactory.GetService<SurveyService>();
                    surveyService.UpdateModifedDateBySurId(surveyId);
                    update = 1;
                }
                else
                {
                    update = 0;
                }
            }

            return update;
        }
        #endregion

        #region "Method : UpdateResponseRequiredAllQuestions"
        /// <summary>
        /// used to update Response required flag to all quations based on survey id except questions type id 16,17,18,19,21 and skiplogic is not equal to one.
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <param name="resposeRequired"></param>

        public int UpdateResponseRequiredAllQuestions(int surveyId, int resposeRequired)
        {
            int update = 0;

            using (var dbCon = new InsightoV2Entities())
            {
                var questionTypeIds = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20 }; //16, 17, 18, 19, 21

                var surveyQuestions = dbCon.osm_surveyquestion.Where(sq => sq.SURVEY_ID == surveyId && sq.SKIP_LOGIC == 0 && questionTypeIds.Contains(sq.QUESTION_TYPE_ID ?? 0)).ToList();

                if (surveyQuestions.Any())
                {
                    surveyQuestions.ForEach(sq => sq.RESPONSE_REQUIRED = resposeRequired);
                    dbCon.SaveChanges();

                    var surveyService = ServiceFactory.GetService<SurveyService>();
                    surveyService.UpdateModifedDateBySurId(surveyId);

                    update = 1;
                }
                else
                {
                    update = 0;
                }
            }

            return update;
        }
        #endregion

        #region "Method : GetAllSurveyQuestionsBySurveyId"
        /// <summary>
        /// Used to get all survey Questions based on surveyId
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <returns></returns>

        public List<osm_surveyquestion> GetAllSurveyQuestionsBySurveyId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var allSurveyQuestions = (from qns in dbCon.osm_surveyquestion
                                          where qns.DELETED == 0 && qns.SURVEY_ID == surveyId
                                          orderby qns.QUESTION_SEQ
                                          select qns).ToList();
                return allSurveyQuestions;
            }
        }

        #endregion

        #region "Method : GetAllSurveyAnswersByQuestionId"
        /// <summary>
        ///  Used to get all survey answers based on questionid.
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>

        public List<osm_answeroptions> GetAllSurveyAnswersByQuestionId(int questionId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var allSurveyAnswers = (from ans in dbCon.osm_answeroptions
                                        where ans.DELETED == 0 && ans.QUESTION_ID == questionId
                                        select ans).ToList();
                return allSurveyAnswers;
            }
        }

        #endregion

        #region "Method : UpdateSkipLogicAnswerOptions"
        /// <summary>
        /// Used to update skip logic question and answerid's.
        /// </summary>
        /// <param name="questionId"></param>
        /// <param name="answerId"></param>
        /// <param name="skipQuestionId"></param>
        /// <param name="mode"></param>

        public void UpdateSkipLogicAnswerOptions(int questionId, int answerId, int skipQuestionId, int mode, int skipApplyorRemove)
        {
            using (var dbCon = new InsightoV2Entities())
            {
                if (mode == 1)
                {
                    var rowQuestion = dbCon.osm_surveyquestion.FirstOrDefault(x => x.QUESTION_ID == questionId);
                    rowQuestion.SKIP_LOGIC = skipApplyorRemove;
                    rowQuestion.PAGE_BREAK = skipApplyorRemove;
                    rowQuestion.RESPONSE_REQUIRED = skipApplyorRemove;
                    dbCon.SaveChanges();
                }
                else
                {
                    var rowAnswer = dbCon.osm_answeroptions.FirstOrDefault(x => x.ANSWER_ID == answerId);
                    rowAnswer.SKIP_QUESTION_ID = skipQuestionId;
                    dbCon.SaveChanges();
                }

            }
        }

        #endregion

        #region "Method : ReOrderQuestions"
        /// <summary>
        /// Used to reorder the questions.
        /// </summary>
        /// <param name="surveyID"></param>
        /// <param name="QuesReOrder"></param>

        public void ReOrderQuestions(int surveyID, Hashtable QuesReOrder)
        {
            using (var dbCon = new InsightoV2Entities())
            {
                if (QuesReOrder != null && QuesReOrder.Count > 0)
                {
                    for (int i = 1; i < QuesReOrder.Count + 1; i++)
                    {
                        int questionId = ValidationHelper.GetInteger(QuesReOrder[i], 0);
                        var rowQuestion = dbCon.osm_surveyquestion.FirstOrDefault(x => x.SURVEY_ID == surveyID && x.QUESTION_ID == questionId);
                        rowQuestion.QUESTION_SEQ = i;
                        dbCon.SaveChanges();

                        var surveyService = ServiceFactory.GetService<SurveyService>();
                        surveyService.UpdateModifedDateBySurId(surveyID);

                    }
                }
            }
        }
        #endregion

        #region "Method : ApplyFreeUserRestrictions"
        public void ApplyFreeUserRestrictions()
        {

        }
        #endregion

        #region "Method :CopySurveyQuestion"
        /// <summary>
        /// CopySurveyQuestion to another survey 
        /// </summary>
        /// <param name="copyFromSurId"></param>
        /// <param name="copyToSurId"></param>
        public void CopySurveyQuestion(int copyFromSurId, int copyToSurId)
        {
            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var ansService = ServiceFactory.GetService<SurveyAnswerService>();
            var quesSer = ServiceFactory.GetService<QuestionService>();
            var quesDet = quesSer.GetSurveyquestionsDet(copyFromSurId);
            var questDetToSurId = quesSer.GetSurveyquestionsDet(copyToSurId);
            var questSeq = questDetToSurId.Max(s => s.QUESTION_SEQ);
            questSeq = questSeq == null ? 0 : questSeq;
            // var ansDet = ansService.GetSurveyAnswersbySurveyId(quesDet);
            List<string> ansList = new List<string>();
            List<string> quesIds = new List<string>();
            osm_surveyquestion osmQues = new osm_surveyquestion();
            int questionPerSurvey = Convert.ToInt16(serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.QUESTION_PER_SURVEY).Value);
            int ques_count = 1;
            if (questDetToSurId != null)
                ques_count = questDetToSurId.Count;

            if (quesDet.Count > 0 && quesDet != null)
            {
                foreach (var ques in quesDet)
                {
                    if (CheckQuestionType(Convert.ToInt16(ques.QUESTION_TYPE_ID)))
                    {
                        if (questionPerSurvey != -1 && (ques_count >= questionPerSurvey))
                            break;
                        var ansDet = ansService.GetAnswersbyQuestionId(ques.QUESTION_ID);
                        if (ansDet != null && ansDet.Count > 0)
                        {

                            foreach (var answers in ansDet)
                            {
                                if (ques.QUESTION_TYPE_ID != 20)
                                {
                                    ansList.Add(answers.ANSWER_OPTIONS);
                                }
                                else if (ques.QUESTION_TYPE_ID == 20)
                                {
                                    if ((serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.RESPONSE_REQUIRED).Value == 0))
                                    {
                                        ansList.Add(answers.ANSWER_OPTIONS + "~" + Convert.ToString(0));
                                    }
                                    else
                                    {
                                        ansList.Add(answers.ANSWER_OPTIONS + "~" + Convert.ToString(answers.RESPONSE_REQUIRED));
                                    }
                                }

                            }
                        }

                        osmQues.SKIP_LOGIC = (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.SKIPLOGIC).Value == 0) ? 0 : ques.SKIP_LOGIC;
                        osmQues.OTHER_ANS = (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.OTHERS_TEXT).Value == 0) ? 0 : ques.OTHER_ANS;
                        osmQues.RESPONSE_REQUIRED = (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.RESPONSE_REQUIRED).Value == 0) ? 0 : ques.RESPONSE_REQUIRED;
                        osmQues.PAGE_BREAK = (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.PAGEBREAK).Value == 0) ? 0 : ques.PAGE_BREAK;
                        osmQues.RANDOMIZE_ANSWERS = (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.RANDOMIZE_ANSWER).Value == 0) ? 0 : ques.RANDOMIZE_ANSWERS;
                        osmQues.ANSWER_ALIGNSTYLE = (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.ANSWER_ALIGN).Value == 0) ? "1" : ques.ANSWER_ALIGNSTYLE;
                        osmQues.CHAR_LIMIT = ques.CHAR_LIMIT;

                        if (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.CHARACTER_LIMIT).Value == 0)
                        {
                            if (ques.QUESTION_TYPE_ID == 5)
                            {
                                osmQues.CHAR_LIMIT = 2000;
                            }
                            else if (ques.QUESTION_TYPE_ID == 6 || ques.QUESTION_TYPE_ID == 9 || ques.QUESTION_TYPE_ID == 13)
                                osmQues.CHAR_LIMIT = 500;
                            else
                                osmQues.CHAR_LIMIT = 0;
                        }
                        osmQues.QUESTION_TYPE_ID = ques.QUESTION_TYPE_ID;
                        osmQues.QUESTION_SEQ = ques.QUESTION_SEQ + questSeq;
                        osmQues.SURVEY_ID = copyToSurId;
                        osmQues.QUSETION_LABEL = ques.QUSETION_LABEL;

                        string othertext = Selectsurveyques_other(ques.QUESTION_ID);
                        quesSer.AddORUpdateSurveyQuestion(0, ansList, osmQues, ValidationHelper.GetInteger(osmQues.QUESTION_SEQ, 0),othertext);
                        

                        ques_count += 1;
                        quesIds.Add(Convert.ToString(ques.QUESTION_ID));
                        ansList.Clear();
                    }
                }// end of question loop

                if (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.SKIPLOGIC).Value == 1)
                {
                    CopySkipQuestionIds(copyToSurId, quesIds, copyFromSurId);
                }
            }


        }
        #endregion

        #region "Method :CopySurveyQuestionForQuestionLibrary"
        /// <summary>
        /// CopySurveyQuestion to another survey 
        /// </summary>
        /// <param name="copyFromSurId"></param>
        /// <param name="copyToSurId"></param>
        public void CopySurveyQuestionForQuestionLibrary(int copyFromSurId, int copyToSurId)
        {
            //  var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var ansService = ServiceFactory.GetService<SurveyAnswerService>();
            var quesSer = ServiceFactory.GetService<QuestionService>();
            var quesDet = quesSer.GetSurveyquestionsDet(copyFromSurId);
            var questDetToSurId = quesSer.GetSurveyquestionsDet(copyToSurId);
            var questSeq = questDetToSurId.Max(s => s.QUESTION_SEQ);
            questSeq = questSeq == null ? 0 : questSeq;
            // var ansDet = ansService.GetSurveyAnswersbySurveyId(quesDet);
            List<string> ansList = new List<string>();
            List<string> quesIds = new List<string>();
            osm_surveyquestion osmQues = new osm_surveyquestion();
            // int questionPerSurvey = Convert.ToInt16(serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.QUESTION_PER_SURVEY).Value);
            int ques_count = 1;
            if (questDetToSurId != null)
                ques_count = questDetToSurId.Count;

            if (quesDet.Count > 0 && quesDet != null)
            {
                foreach (var ques in quesDet)
                {
                    if (CheckQuestionType(Convert.ToInt16(ques.QUESTION_TYPE_ID)))
                    {
                        //if (questionPerSurvey != -1 && (ques_count >= questionPerSurvey))
                        //    break;
                        var ansDet = ansService.GetAnswersbyQuestionId(ques.QUESTION_ID);
                        if (ansDet != null && ansDet.Count > 0)
                        {

                            foreach (var answers in ansDet)
                            {
                                if (ques.QUESTION_TYPE_ID != 20)
                                {
                                    ansList.Add(answers.ANSWER_OPTIONS);
                                }
                                else if (ques.QUESTION_TYPE_ID == 20)
                                {
                                    //if ((serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.RESPONSE_REQUIRED).Value == 0))
                                    //{
                                    //    ansList.Add(answers.ANSWER_OPTIONS + "~" + Convert.ToString(0));
                                    //}
                                    //else
                                    //{
                                    ansList.Add(answers.ANSWER_OPTIONS + "~" + Convert.ToString(answers.RESPONSE_REQUIRED));
                                    //}
                                }

                            }
                        }

                        osmQues.SKIP_LOGIC = ques.SKIP_LOGIC;
                        osmQues.OTHER_ANS = ques.OTHER_ANS;
                        osmQues.RESPONSE_REQUIRED = ques.RESPONSE_REQUIRED;
                        osmQues.PAGE_BREAK = ques.PAGE_BREAK;
                        osmQues.RANDOMIZE_ANSWERS = ques.RANDOMIZE_ANSWERS;
                        osmQues.ANSWER_ALIGNSTYLE = ques.ANSWER_ALIGNSTYLE;
                        osmQues.CHAR_LIMIT = ques.CHAR_LIMIT;

                        // if (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.CHARACTER_LIMIT).Value == 0)
                        // {
                        if (ques.QUESTION_TYPE_ID == 5)
                        {
                            osmQues.CHAR_LIMIT = 2000;
                        }
                        else if (ques.QUESTION_TYPE_ID == 6 || ques.QUESTION_TYPE_ID == 9 || ques.QUESTION_TYPE_ID == 13)
                            osmQues.CHAR_LIMIT = 500;
                        else
                            osmQues.CHAR_LIMIT = 0;
                        // }
                        osmQues.QUESTION_TYPE_ID = ques.QUESTION_TYPE_ID;
                        osmQues.QUESTION_SEQ = ques.QUESTION_SEQ + questSeq;
                        osmQues.SURVEY_ID = copyToSurId;
                        osmQues.QUSETION_LABEL = ques.QUSETION_LABEL;

                        quesSer.AddORUpdateSurveyQuestion(0, ansList, osmQues, ValidationHelper.GetInteger(osmQues.QUESTION_SEQ, 0));
                        ques_count += 1;
                        quesIds.Add(Convert.ToString(ques.QUESTION_ID));
                        ansList.Clear();
                    }
                }// end of question loop

                //if (serviceFeature.Find(GetLicenceType, FeatureSurveyCreation.SKIPLOGIC).Value == 1)
                //{
                CopySkipQuestionIds(copyToSurId, quesIds, copyFromSurId);
                //}
            }


        }
        #endregion

        #region
        /// <summary>
        /// check question type exsiss in enum
        /// </summary>
        /// <param name="questionType"></param>
        /// <returns></returns>
        public bool CheckQuestionType(int questionType)
        {
            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var questionName = GenericHelper.ToEnum<FeatureQuestionType>(questionType);
            var chk = serviceFeature.Find(GetLicenceType, questionName).Value;
            return Convert.ToBoolean(chk);
        }
        #endregion

        #region "Method : CheckQuestions"
        /// <summary>
        /// used to check questions exits or not.
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <param name="pageBreak"></param>

        public bool CheckQuestions(int surveyId)
        {
            bool update = false;
            using (var dbCon = new InsightoV2Entities())
            {
                var surveyQuestions = dbCon.osm_surveyquestion.Where(sq => sq.SURVEY_ID == surveyId && sq.DELETED == 0).ToList();
                if (surveyQuestions.Any())
                {
                    update = true;
                }

            }

            return update;
        }
        #endregion

        #region "Method: copyskipquestionids"
        public void CopySkipQuestionIds(int copytoSurveyId, List<string> quesionIds, int copyfromSurveyId)
        {
            var ansService = ServiceFactory.GetService<SurveyAnswerService>();
            var quesSer = ServiceFactory.GetService<QuestionService>();
            var quesDet = quesSer.GetSurveyquestionsDet(copytoSurveyId);
            var quesDetCopySurId = quesSer.GetSurveyquestionsDet(copyfromSurveyId);

            List<string> copiedQues = new List<string>();
            if (quesDet != null && quesDet.Count > 0)
            {
                //quesDet.Sort();
                foreach (var ques in quesDet)
                {
                    copiedQues.Add(Convert.ToString(ques.QUESTION_ID));
                }
                // quesDetCopySurId.Sort();
            }
            if (quesionIds.Count > 0 && copiedQues.Count > 0 && quesionIds.Count == copiedQues.Count)
            {
                foreach (var ques in quesDetCopySurId)
                {
                    if (ques.SKIP_LOGIC == 1)
                    {
                        List<string> ans_skipquesid = new List<string>();
                        var ansDetCopyFromSurId = ansService.GetAnswersbyQuestionId(ques.QUESTION_ID);

                        if (ansDetCopyFromSurId != null && ansDetCopyFromSurId.Count > 0)
                        {
                            foreach (var ans in ansDetCopyFromSurId)
                            {
                                ans_skipquesid.Add(Convert.ToString(ans.SKIP_QUESTION_ID));
                            }
                        }
                        List<string> eq_ans_skipids = new List<string>();
                        if (ans_skipquesid != null && ans_skipquesid.Count > 0)
                        {
                            for (int y = 0; y < ans_skipquesid.Count; y++)
                            {
                                if (Convert.ToInt32(ans_skipquesid[y]) > 0)
                                {
                                    int x = ValidationHelper.GetInteger(quesionIds.IndexOf(ans_skipquesid[y]), 0);
                                    int z = ValidationHelper.GetInteger((copiedQues[x]), 0);
                                    eq_ans_skipids.Add(Convert.ToString(z));
                                }
                                else if (Convert.ToInt32(ans_skipquesid[y]) < 0)
                                {
                                    int val = ValidationHelper.GetInteger((ans_skipquesid[y]), 0);
                                    if (val == -1 || val == -2 || val == -3)
                                    {
                                        eq_ans_skipids.Add(Convert.ToString(val));

                                    }

                                }
                            }

                        }
                        if (eq_ans_skipids != null && ans_skipquesid != null && eq_ans_skipids.Count > 0 && eq_ans_skipids.Count == ans_skipquesid.Count)
                        {
                            int l = quesionIds.IndexOf(Convert.ToString(ques.QUESTION_ID));
                            if (quesDet != null && quesDet.Count > 0)
                            {
                                foreach (var quesCopytoSurId in quesDet)
                                {
                                    if (quesCopytoSurId.QUESTION_ID == Convert.ToInt32(copiedQues[l]))
                                    {
                                        var ansDetCopytoSurId = ansService.GetAnswersbyQuestionId(quesCopytoSurId.QUESTION_ID);
                                        if (ansDetCopytoSurId != null && ansDetCopytoSurId.Count > 0)
                                        {
                                            int p = 0;
                                            foreach (var ansDets in ansDetCopytoSurId)
                                            {
                                                UpdateSkipQuestionId(Convert.ToInt32(eq_ans_skipids[p]), ansDets.ANSWER_ID);
                                                p += 1;
                                            }
                                        }
                                        break;
                                    }

                                }
                            }


                        }

                    }

                }
            }
        }

        #endregion

        #region "Method:Updateskipquestioid"
        public void UpdateSkipQuestionId(int skipId, int answerId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var ans = dbCon.osm_answeroptions.FirstOrDefault(ansDet => ansDet.ANSWER_ID == answerId);
                ans.SKIP_QUESTION_ID = skipId;
                dbCon.SaveChanges();
            }
        }
        #endregion

        public void SetPageBreak(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var maxQuestionSeq = (from qns in dbCon.osm_surveyquestion
                                      where qns.DELETED == 0 && qns.SURVEY_ID == surveyId
                                      select qns.QUESTION_SEQ).Max();
                osm_surveyquestion surveyQuestions = dbCon.osm_surveyquestion.FirstOrDefault(qns => qns.SURVEY_ID == surveyId && qns.DELETED == 0 && qns.QUESTION_SEQ == maxQuestionSeq);
                if (surveyQuestions != null)
                {
                    surveyQuestions.PAGE_BREAK = 0;
                    dbCon.SaveChanges();
                }
            }
        }


        #region "Method : GetQuestionIdBySequence"
        /// <summary>
        ///  Used to get question id by sequence top.
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>

        public int GetQuestionIdBySequence(int surveyid)
        {
            //int iQid1 = 0;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var iQid1 = (from sqn in dbCon.osm_surveyquestion
                             where sqn.DELETED == 0 && sqn.SURVEY_ID == surveyid
                             orderby sqn.QUESTION_SEQ descending
                             select sqn.QUESTION_ID).First();
                return iQid1;
            }
        }

        #endregion

        #region "Method: CheckQuestionCount"
        /// <summary>
        /// Check the total questions added per survey with featuresevice Question_per_survey 
        /// </summary>
        /// <param name="surveyId"></param>
        /// <returns></returns>
        public bool CheckQuestionCount(int surveyId)
        {
            var session = ServiceFactory.GetService<SessionStateService>();
            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var sessdet = session.GetLoginUserDetailsSession();
            var questionService = ServiceFactory.GetService<QuestionService>();
            var licenceType = GenericHelper.ToEnum<UserType>(sessdet.LicenseType);
            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));
            var questionDet = questionService.GetAllSurveyQuestionsBySurveyId(surveyId);
            bool checkCountStatus = true;
            if (sessdet.UserId > 0 && listFeatures != null)
            {
                if (serviceFeature.Find(licenceType, FeatureSurveyCreation.QUESTION_PER_SURVEY).Value > 0 && serviceFeature.Find(licenceType, FeatureSurveyCreation.QUESTION_PER_SURVEY).Value <= questionDet.Count)
                {
                    return checkCountStatus = false;

                }
            }
            return checkCountStatus;
        }
        #endregion

        #region "Method:UpdateQuestionSequenceBySurveyId"
        /// <summary>
        /// Update QuestionSeq when click on add question
        /// </summary>
        /// <param name="osmQuestion"></param>
        public void UpdateQuestionSeqBySurveyId(osm_surveyquestion osmQuestion)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var rowQuestion = dbCon.osm_surveyquestion.FirstOrDefault(questId => questId.QUESTION_ID == osmQuestion.QUESTION_ID && questId.SURVEY_ID == osmQuestion.SURVEY_ID);
                rowQuestion.QUESTION_SEQ = osmQuestion.QUESTION_SEQ;
                dbCon.SaveChanges();
                var surveyService = ServiceFactory.GetService<SurveyService>();
                surveyService.UpdateModifedDateBySurId(osmQuestion.SURVEY_ID.Value);
            }
        }
        #endregion

        public List<osm_surveyquestion> GetQuestionDetByQuestionId(int questionId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveyQuest = (from quest in dbCon.osm_surveyquestion
                                   where quest.QUESTION_ID == questionId && quest.DELETED == 0
                                   select quest).ToList();
                return surveyQuest;
            }
        }
        #region "Method Get Image/Video Count By SurveyId"
        public List<osm_surveyquestion> GetImageVideoCount(int SurveyId, int QuestionTypeId)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                var surveyDet = (from ans in dbCon.osm_surveyquestion
                                 where ans.DELETED == 0 && ans.SURVEY_ID == SurveyId && ans.QUESTION_TYPE_ID == QuestionTypeId
                                 select ans).ToList();
                return surveyDet;
            }

        }
        #endregion
        #region "Method : CheckPageBreakForDisable"
        /// <summary>
        /// used to check page break applied to all the questions or not.
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <param name="pageBreak"></param>

        public bool CheckPageBreakForDisable(int surveyId)
        {
            bool update = false;
            using (var dbCon = new InsightoV2Entities())
            {
                var qTypeIdsFPageBreak = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20 };
                var surveyQuestions = dbCon.osm_surveyquestion.Where(sq => sq.SURVEY_ID == surveyId && sq.SKIP_LOGIC == 0 && qTypeIdsFPageBreak.Contains(sq.QUESTION_TYPE_ID ?? 0) && sq.DELETED == 0).ToList();
                if (surveyQuestions.Any())
                {
                    update = true;
                }

            }

            return update;
        }
        #endregion

        #region "Method : CheckPageBreakForDisable"
        /// <summary>
        /// used to check respose required applied all the questions or not.
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <param name="resposeRequired"></param>

        public bool CheckResponseRequiredForDisable(int surveyId)
        {
            bool update = false;

            using (var dbCon = new InsightoV2Entities())
            {
                var questionTypeIds = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20 }; //16, 17, 18, 19, 21
                var surveyQuestions = dbCon.osm_surveyquestion.Where(sq => sq.SURVEY_ID == surveyId && sq.SKIP_LOGIC == 0 && questionTypeIds.Contains(sq.QUESTION_TYPE_ID ?? 0) && sq.DELETED == 0).ToList();
                if (surveyQuestions.Any())
                {
                    update = true;
                }

            }

            return update;
        }
        #endregion

        #region SaveSurveyQuestion
        public void SaveQuestion(List<SurveyQuestionAndAnswerOptions> questionAndAnswers, SurveyActionTypes actionType, QuestionType questionType)
        {
            switch (actionType)
            {
                case SurveyActionTypes.None:
                    break;

                case SurveyActionTypes.Add:
                    AddQuestionAndAnswers(questionAndAnswers.FirstOrDefault());
                    break;

                case SurveyActionTypes.Edit:
                    if (questionType == QuestionType.Matrix_SideBySide || questionType == QuestionType.Matrix_MultipleSelect || questionType == QuestionType.Matrix_SingleSelect)
                    {

                    }
                    else
                    {
                        UpdateQuestionAndAnswers(questionAndAnswers.FirstOrDefault());
                    }
                    break;

                case SurveyActionTypes.Copy:
                    break;

                case SurveyActionTypes.Remove:
                    break;

                case SurveyActionTypes.AddMiddle:
                    var currentQuestionSequence = questionAndAnswers.FirstOrDefault().Question.QUESTION_SEQ;
                    questionAndAnswers.FirstOrDefault().Question.QUESTION_SEQ = -1;
                    AddQuestionAndAnswers(questionAndAnswers.FirstOrDefault());
                    ReOrderQuestions(Convert.ToInt32(currentQuestionSequence), Convert.ToInt32(questionAndAnswers.FirstOrDefault().Question.SURVEY_ID));
                    break;

                default:
                    break;
            }
        }

        private void UpdateQuestionAndAnswers(SurveyQuestionAndAnswerOptions surveyQuestionAndAnswerOptions)
        {
            return;
        }

        private void ReOrderQuestions(int currentQuestionSequence, int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var allQuestions = dbCon.osm_surveyquestion.Where(sq => sq.SURVEY_ID == surveyId && sq.DELETED == 0).OrderBy(q => q.QUESTION_SEQ).ToList();
                var latestAddedQuestion = allQuestions.FirstOrDefault(sq => sq.QUESTION_SEQ == -1);
                for (int i = currentQuestionSequence + 1; i < allQuestions.Count; i++)
                {
                    var currentquestionId = allQuestions.ToList()[i].QUESTION_ID;
                    var question = dbCon.osm_surveyquestion.Where(q => q.QUESTION_ID == currentquestionId && q.DELETED == 0).FirstOrDefault();
                    question.QUESTION_SEQ = latestAddedQuestion.QUESTION_ID == question.QUESTION_ID ? currentQuestionSequence : i;

                    dbCon.SaveChanges();
                }
            }
        }

        public void SaveSurveyQuesAndAnswers(SurveyQuestionAndAnswerOptions quesAndanswer, DbOperationType dbOperationType)
        {
            switch (dbOperationType)
            {
                case DbOperationType.Insert:
                    AddQuestionAndAnswers(quesAndanswer);
                    break;

                case DbOperationType.Update:
                    UpdateQuestionAndAnswer(quesAndanswer);
                    break;

                case DbOperationType.UpdateStatus:
                    break;

                default:
                    break;
            }

        }

        private void AddQuestionAndAnswers(SurveyQuestionAndAnswerOptions questionAndanswers)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var questionId = AddSurveyQuestion(questionAndanswers.Question);
                if (questionId > 0)
                {
                    if (questionAndanswers.AnswerOption.Count > 0)
                    {
                        foreach (var answerOption in questionAndanswers.AnswerOption)
                        {
                            answerOption.QUESTION_ID = questionId;
                            AddAnsOptions(answerOption);

                        }
                    }

                }
            }
        }

        private void UpdateQuestionAndAnswer(SurveyQuestionAndAnswerOptions quesAndanswer)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (quesAndanswer.Question.QUESTION_ID > 0)
                {
                    UpdateSurveyQuestion(quesAndanswer.Question);
                }
            }

        }

        public int AddSurveyQuestion(osm_surveyquestion osmQuestion)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osmQuestion.CREATED_ON = DateTime.UtcNow;
                osmQuestion.LASTMODIFIED_ON = DateTime.UtcNow;
                osmQuestion.DELETED = 0;
                osmQuestion.SHOW_LABEL = 1;
                osmQuestion.LABEL_ANGLE = 0;
                osmQuestion.VALUEAS_PERCENT = 0;
                osmQuestion.ZOOMPERCENT = 100;
                osmQuestion.TRANSPARENCY = 45;
                osmQuestion.PERSPECTIVE_ANGLE = 30;
                osmQuestion.MARKER_SIZE = 0;
                osmQuestion.SHOW_MARKER = 0;
                osmQuestion.INVERTED = 0;
                osmQuestion.LEGEND_EQUALLYSPACED_ITEMS = 1;
                osmQuestion.LEGEND_MAX_HORIZONTALALIGN = 0;
                osmQuestion.LEGEND_MAX_VERTICALALIGN = 0;
                osmQuestion.HIDE_LEGAND = 1;
                osmQuestion.HIDE_XAXIS = 1;
                osmQuestion.STAGGERED = 0;
                dbCon.osm_surveyquestion.Add(osmQuestion);
                dbCon.SaveChanges();

                return osmQuestion.QUESTION_ID;
            }
        }


        public string Selectsurveyques_other(int QuestionId)
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("Selectquestion_othertext", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@QUESTION_ID", SqlDbType.Int)).Value = QuestionId;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet ds = new DataSet();
            sda.SelectCommand = scom;
            sda.Fill(ds, "Other_txt");
            //DataTable dt= ds.Tables["Other_txt"];
            string othertext = ds.Tables[0].Rows[0][0].ToString();
            return othertext;
        }

        public DataSet updateOtherText(int questionid,string othertext)
        {
            try
            {
                SqlConnection con = new SqlConnection();

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("updatequestion_othertext", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@QUESTION_ID", SqlDbType.Int)).Value = questionid;
                scom.Parameters.Add(new SqlParameter("@otherText", SqlDbType.VarChar)).Value = othertext;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dssurveytype = new DataSet();
                sda.Fill(dssurveytype);
                con.Close();
                return dssurveytype;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SqlConnection strconnectionstring()
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection strconn = new SqlConnection(connStr);

                strconn.Open();
                return strconn;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion


        /// <summary>
        /// Updates the question by question id.
        /// </summary>
        /// <param name="pageBreakValue">The page break value.</param>
        /// <param name="questionId">The question id.</param>
        public void UpdateQuestionByQuestionId(int pageBreakValue, int questionId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var questionToUpdate = dbCon.osm_surveyquestion.Where(q => q.QUESTION_ID == questionId).FirstOrDefault();
                questionToUpdate.PAGE_BREAK = pageBreakValue;
                dbCon.SaveChanges();
            }
        }
    }



}
