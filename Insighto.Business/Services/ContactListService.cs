﻿using System;
using System.Collections.Generic;
using System.Linq;
using CovalenseUtilities.Services;
using Insighto.Business.Extensions;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using System.Collections;

namespace Insighto.Business.Services
{
    public class ContactListService : BusinessServiceBase
    {
        public int SurveyFlag
        {
            get
            {
                var key = QueryStringHelper.GetString("key");
                if (String.IsNullOrEmpty(key))
                    return 0;

                Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
                if (ht != null && ht.Contains("surveyFlag"))
                {
                    return ValidationHelper.GetInteger(ht["surveyFlag"], 0);
                }
                return 0;
            }

        }
        public osm_contactlist FindByContactListID(int contactListId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_contactlist.Where(c => c.CONTACTLIST_ID == contactListId).SingleOrDefault();

            }
        }

        public osm_contactlist SaveEmailContactList(int userId, string contactListName)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var contactList = new osm_contactlist();
                var contacts = dbCon.osm_contactlist.Where(c => c.CONTACTLIST_NAME == contactListName && c.CREATED_BY == userId).SingleOrDefault();

                if (contacts == null)
                {
                    contactList.CREATED_BY = userId;
                    contactList.CONTACTLIST_NAME = contactListName;
                    contactList.CREATED_ON = DateTime.Now;
                    contactList.LASTMODIFIED_BY = userId;
                    contactList.LAST_MODIFIED_ON = DateTime.Now;
                    contactList.NO_EMAILIDS = 0;
                    contactList.DELETED = 0;
                    dbCon.osm_contactlist.Add(contactList);
                    dbCon.SaveChanges();
                    contactList = dbCon.osm_contactlist.Where(c => c.CONTACTLIST_NAME == contactListName && c.CREATED_BY == userId).SingleOrDefault();
                    return contactList;
                }
                else
                {
                    return null;
                }
               
            }
        }

        public bool UpdateContactList(int contactId, int userId, string contactListName)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                bool isUpdated = default(bool);
                var contactList = dbCon.osm_contactlist.Where(c => c.CONTACTLIST_NAME == contactListName && c.CREATED_BY == userId && c.CONTACTLIST_ID != contactId).SingleOrDefault();

                if (contactList == null)
                {
                    var contacts = dbCon.osm_contactlist.Where(c => c.CONTACTLIST_ID == contactId).SingleOrDefault();
                    contacts.CONTACTLIST_NAME = contactListName;
                    contacts.LASTMODIFIED_BY = contactId;
                    contacts.LAST_MODIFIED_ON = DateTime.Now;                    
                    dbCon.SaveChanges();
                    isUpdated = true;
                }
                return isUpdated;             

            }
        }


        #region GetPagedList
        public PagedList<ContactListInfo> FindContactListByUserId(int userId, PagingCriteria pagingCriteria)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var contactList = dbCon.osm_contactlist.Where(c => c.CREATED_BY == userId)
                                                        .ToPagedList(pagingCriteria);                         
                var pagedListInfo = new PagedList<ContactListInfo>();
                var listInfo = new List<ContactListInfo>();
                foreach (var contact in contactList.Entities)
                {
                    var contactInfo = new ContactListInfo();
                    contactInfo.CONTACTLIST_ID = contact.CONTACTLIST_ID;
                    contactInfo.NO_EMAILIDS = contact.NO_EMAILIDS;
                    contactInfo.CREATED_BY = contact.CREATED_BY;
                    contactInfo.CreatedDate = Convert.ToDateTime(contact.CREATED_ON).ToString("dd-MMM-yyyy");
                    contactInfo.ModifiedDate = Convert.ToDateTime(contact.LAST_MODIFIED_ON).ToString("dd-MMM-yyyy"); 
                    contactInfo.CONTACTLIST_NAME = contact.CONTACTLIST_NAME;

                    string editUrl = EncryptHelper.EncryptQuerystring("ContactList.aspx", "ContactId=" + contact.CONTACTLIST_ID.ToString()+"&surveyFlag="+SurveyFlag);
                    string viewUrl = EncryptHelper.EncryptQuerystring("CreateEmailList.aspx", "ContactId=" + contact.CONTACTLIST_ID.ToString() + "&Control=AddEmail&surveyFlag="+SurveyFlag);
                    string pastUrl = EncryptHelper.EncryptQuerystring("ContactListPastHistory.aspx", "ContactId=" + contact.CONTACTLIST_ID.ToString() + "&surveyFlag=" + SurveyFlag);
                    contactInfo.contactUrl = String.Format("<a href='{0}' class='gridHyperLink' rel='framebox' w='700' h='250'>{1}</a>", editUrl, contactInfo.CONTACTLIST_NAME.ToString());
                    contactInfo.passHistoryUrl = pastUrl;
                    contactInfo.editUrl = viewUrl;
                    listInfo.Add(contactInfo);
                }
                pagedListInfo.Entities = listInfo;
                pagedListInfo.HasNext = contactList.HasNext;
                pagedListInfo.HasPrevious = contactList.HasPrevious;
                pagedListInfo.TotalCount = contactList.TotalCount;
                pagedListInfo.TotalPages = contactList.TotalPages;
                return pagedListInfo;
                
            }
        }
        #endregion
        public osm_contactlist FindContactsByContactId(int contactListId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_contactlist.Where(c => c.CONTACTLIST_ID == contactListId).SingleOrDefault();

            }
        }

        public List<osm_contactlist> GetListByUserId(int userId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_contactlist.Where(c => c.CREATED_BY == userId).ToList();
            }
        }
    }


}
