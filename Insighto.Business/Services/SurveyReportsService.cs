﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Services;
using Insighto.Data;

namespace Insighto.Business.Services
{
    public class SurveyReportsService : BusinessServiceBase
    {
        public List<ResponseQuestionsInfo> GetResponseQuestionsAnswers(int surveyId)
        {
            using (var dbCon = new InsightoV2Entities())
            {
                return dbCon.GETRESPONSEQUESTIONS(surveyId).ToList();
            }
        }

        public List<osm_surveyquestion> GetQuestionsBySurveyId(int surveyId)
        {
            using (var dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_surveyquestion.Where(sq => (int)sq.SURVEY_ID == surveyId).ToList();
            }
        }
    }
}
