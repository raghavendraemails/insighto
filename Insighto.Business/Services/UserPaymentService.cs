﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Exceptions;
using System.IO;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using System.Web;
using CovalenseUtilities.Helpers;
using Insighto.Business.Extensions;
namespace Insighto.Business.Services
{

    public class UserPaymentService : BusinessServiceBase
    {
        #region "Method : UserInsertPaymentInfo"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userPaymentInfo"></param>
        /// <returns></returns>
        public osm_userpaymentinfo SavePaymentInfo(osm_userpaymentinfo userPaymentInfo)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_userpaymentinfo.Add(userPaymentInfo);
                dbCon.SaveChanges();
            }
            return userPaymentInfo;
        }

        #endregion
    }
}
