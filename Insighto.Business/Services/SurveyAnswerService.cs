﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using Insighto.Business.Extensions;
using Insighto.Business.Helpers;
using Insighto.Business.Enumerations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Insighto.Business.Services
{
    public class SurveyAnswerService : BusinessServiceBase
    {
        #region

        /// <summary>
        /// Adds the ans options.
        /// </summary>
        /// <param name="osmAnsOptions">The osm ans options.</param>
        /// <param name="previousAnswerOptions">The previous answer options.</param>
        //List<osm_answeroptions> currentAnswerOptions = null;
        List<AnswerOptionsInfo> previousAnswerOptions = null;
        public void AddAnsOptions(osm_answeroptions osmAnsOptions)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osmAnsOptions.CREATED_ON = DateTime.UtcNow;
                osmAnsOptions.LAST_MODIFIED_ON = DateTime.UtcNow;
                dbCon.osm_answeroptions.Add(osmAnsOptions);
                dbCon.SaveChanges(); 
            }
        }
        
        //CREATED THIS TO SEE IF LONG TIME TAKEN TO SAVE THE ANSWER OPTION ON SURVEY EDIT IS FIXED OR NOT SATISH 11-SEP-15
        public void AddAnsOptions(osm_answeroptions osmAnsOptions, InsightoV2Entities db)
        {
            osmAnsOptions.CREATED_ON = DateTime.UtcNow;
            osmAnsOptions.LAST_MODIFIED_ON = DateTime.UtcNow;
            db.osm_answeroptions.Add(osmAnsOptions);
        }
        #endregion

        #region
        public List<osm_answeroptions> GetSurveyAnswersbySurveyId(List<osm_surveyquestion> osmQuestions)
        {
            var getQuestionIds = (from xx in osmQuestions
                                  select xx.QUESTION_ID);
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                var ansDet = (from ans in dbCon.osm_answeroptions
                              where getQuestionIds.Contains(ans.QUESTION_ID)
                              select ans).ToList();

                return ansDet;
            }
        }
        #endregion

        #region "GetAnswersbyQuestionId"
        public List<osm_answeroptions> GetAnswersbyQuestionId(int questionId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var ansOpt = (from ans in dbCon.osm_answeroptions
                              where ans.QUESTION_ID == questionId && ans.DELETED == 0
                              select ans).ToList();
                return ansOpt;
            }
        }
        #endregion

        #region "UpdateSurveyQuesAnswers"

        /// <summary>
        /// Updates the survey ques answers.
        /// </summary>
        /// <param name="ans">The ans.</param>
        /// <param name="osmQues">The osm ques.</param>
        /// <param name="previousAnswerOptions">The previous answer options.</param>
        public void UpdateSurveyQuesAnswers(List<string> ans, osm_surveyquestion osmQues, int surveyId)
        {
            previousAnswerOptions = new List<AnswerOptionsInfo>();
            StringBuilder sb = new StringBuilder();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.Configuration.AutoDetectChangesEnabled = false;
                var ansSer = ServiceFactory.GetService<SurveyAnswerService>();
                var ansDet = ansSer.GetAnswersbyQuestionId(osmQues.QUESTION_ID);
                previousAnswerOptions = ansDet.Select(an => new AnswerOptionsInfo { PreviousAnswerId = an.ANSWER_ID }).ToList();
                if (osmQues.QUESTION_TYPE_ID == 4)
                {
                    ans.Add("Yes");
                    ans.Add("No");
                }

                //remove previous options from osm_answeroptions
                foreach (var an in ansDet)
                {
                    osm_answeroptions ansOp = dbCon.osm_answeroptions.FirstOrDefault(aop => aop.ANSWER_ID == an.ANSWER_ID);
                    dbCon.osm_answeroptions.Remove(ansOp);
                }
                dbCon.SaveChanges();
                dbCon.Configuration.AutoDetectChangesEnabled = false;
                int index = 0;
                //add current answer options into osm_answeroptions
                for (int i = 0; i < ans.Count; i++)
                {
                    int contResReqID = (osmQues.QUESTION_TYPE_ID == 20) ? Convert.ToInt32(ans[i].Split('~')[1]) : 0;
                    int demoGraphicBlockID = (osmQues.QUESTION_TYPE_ID == 20) ? Convert.ToInt32(ans[i].Split('~')[2]) : 0;
                    osm_answeroptions ansOpt = new osm_answeroptions();
                    if (osmQues.QUESTION_TYPE_ID == 20)
                    {
                        if (Convert.ToString(ans[i]).Split('~')[0] != " ")
                        {
                            ansOpt.ANSWER_OPTIONS = Convert.ToString(ans[i].Split('~')[0]);
                            ansOpt.QUESTION_ID = osmQues.QUESTION_ID;
                            ansOpt.DEMOGRAPIC_BLOCKID = demoGraphicBlockID;
                            ansOpt.RESPONSE_REQUIRED = contResReqID;
                            ansOpt.SKIP_QUESTION_ID = 0;
                            ansOpt.DELETED = 0;
                            AddAnsOptions(ansOpt);

                            if (previousAnswerOptions != null && previousAnswerOptions.Count > 0 && index < previousAnswerOptions.Count)
                            {
                                previousAnswerOptions[index].CurrentAnswerId = ansOpt.ANSWER_ID;
                                index++;
                            }
                        }
                    }
                    else if (osmQues.QUESTION_TYPE_ID == 19 || osmQues.QUESTION_TYPE_ID == 21)
                    {
                        ansOpt.DEMOGRAPIC_BLOCKID = demoGraphicBlockID;
                        ansOpt.RESPONSE_REQUIRED = contResReqID;
                        ansOpt.ANSWER_OPTIONS = Convert.ToString(ans[i]);
                        ansOpt.QUESTION_ID = osmQues.QUESTION_ID;
                        ansOpt.DELETED = 0;
                        ansOpt.SKIP_QUESTION_ID = 0;
                        AddAnsOptions(ansOpt);

                        if (previousAnswerOptions != null && previousAnswerOptions.Count > 0)
                        {
                            previousAnswerOptions[i].CurrentAnswerId = ansOpt.ANSWER_ID;
                        }
                    }
                    else
                    {
                        ansOpt.DEMOGRAPIC_BLOCKID = demoGraphicBlockID;
                        ansOpt.RESPONSE_REQUIRED = contResReqID;
                        ansOpt.ANSWER_OPTIONS = Convert.ToString(ans[i].Split('~')[0]);
                        ansOpt.QUESTION_ID = osmQues.QUESTION_ID;
                        ansOpt.DELETED = 0;
                        ansOpt.SKIP_QUESTION_ID = 0;
                        AddAnsOptions(ansOpt);

                        if (previousAnswerOptions != null && previousAnswerOptions.Count > 0 && i < previousAnswerOptions.Count)
                        {
                            previousAnswerOptions[i].CurrentAnswerId = ansOpt.ANSWER_ID;
                        }
                    }
                }
                if (ans.Count > 0)
                {
                    dbCon.SaveChanges(); //added by satish 11-sep-15. rather than save for each answer option. save for entire set in one go.
                }
                //THIS HAS BEEN CHANGED AS PERFORMANCE AS DEGRADED WITH ENTITY FRAMEWORK FOR OSM_RESPONSEQUESTIONS TABLE 
                //THIS IS DUE TO NUMBER OF RECORDS 3.8M AS ON 21-SEP-15
                var responsesCount = ServiceFactory<RespondentService>.Instance.GetResponseCount(surveyId);
                if (previousAnswerOptions.Count > 0 && responsesCount > 0)
                {
                    foreach (var item in previousAnswerOptions) {
                        sb.Append("UPDATE osm_responsequestions SET ANSWER_ID = " + item.CurrentAnswerId.ToString() + " WHERE ANSWER_ID = " + item.PreviousAnswerId.ToString() + " AND QUESTION_ID = " + osmQues.QUESTION_ID.ToString() + ";");
                    }
                    if (sb.Length > 0) {
                        string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                        SqlConnection con = new SqlConnection(connStr);
                        con.Open();
                        SqlCommand scom = new SqlCommand(sb.ToString(), con);
                        scom.CommandType = CommandType.Text;
                        scom.ExecuteNonQuery();
                        con.Close();
                        scom.Dispose();
                        con.Dispose();
                    }
                }
                //////////////////////////////////////////////////////////////////////////////////////
                //dbCon.SaveChanges();
            }
            //COMMENTED OUT AS PERFORMANCE HAS DEGRADED USING ENTITY FRAMEWORK FOR 3.8M RECORDS IN OSM_RESPONSEQUESTIONS 21-SEP-15
            //var responsesCount = ServiceFactory<RespondentService>.Instance.GetResponseCount(surveyId);
            //if (previousAnswerOptions.Count > 0 && responsesCount > 0)
            //    UpdateExistingRespondentAnswerIds(previousAnswerOptions, osmQues.QUESTION_ID);

        }

        /// <summary>
        /// Updates the existing respondent answer ids.
        /// </summary>
        /// <param name="previousAnswerOptions">The previous answer options.</param>
        /// <param name="currentAnswerOptions">The current answer options.</param>
        /// <param name="surveyId">The survey id.</param>
        private void UpdateExistingRespondentAnswerIds(List<AnswerOptionsInfo> previousAnswerOptions, int questionId, int surveyId = 0)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.Configuration.AutoDetectChangesEnabled = false;
                int indexCtr = 0;
				var responsesAnswerOptions = dbCon.osm_responsequestions.ToList().Where(rq => rq.QUESTION_ID == questionId).ToList();
                foreach (var responseOption in responsesAnswerOptions)
                {
                    if (responseOption.ANSWER_ID == 0)
                        continue;

                    //updating respondent question with new answer Id
                    var previousOption = previousAnswerOptions.Where(pa => pa.PreviousAnswerId == responseOption.ANSWER_ID).FirstOrDefault();
                    if (previousOption == null)
                        continue;

                    responseOption.ANSWER_ID = previousOption.CurrentAnswerId;
				}
				//dbCon.SaveChanges();
                indexCtr++;
            }
        }

        private void UpdateExistingRespondentAnswerIds(List<AnswerOptionsInfo> previousAnswerOptions, int questionId, InsightoV2Entities dbCon1, int surveyId = 0)
        {
            int indexCtr = 0;
            var responsesAnswerOptions = dbCon1.osm_responsequestions.ToList().Where(rq => rq.QUESTION_ID == questionId).ToList();
            foreach (var responseOption in responsesAnswerOptions)
            {
                if (responseOption.ANSWER_ID == 0)
                    continue;

                //updating respondent question with new answer Id
                var previousOption = previousAnswerOptions.Where(pa => pa.PreviousAnswerId == responseOption.ANSWER_ID).FirstOrDefault();
                if (previousOption == null)
                    continue;

                responseOption.ANSWER_ID = previousOption.CurrentAnswerId;
            }
            indexCtr++;
        }

        #endregion

        public void DeleteAnswerOptions(List<string> list, int questionId, QuestionType QuestionType)
        {
            throw new NotImplementedException();
        }
    }

    public class AnswerOptionsInfo
    {
        public int PreviousAnswerId { get; set; }

        public int CurrentAnswerId { get; set; }
    }
}
