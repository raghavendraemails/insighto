﻿using System;
using System.Linq;
using CovalenseUtilities.Services;
using Insighto.Business.Extensions;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System.Collections.Generic;

namespace Insighto.Business.Services
{
     public class  ContactUsService : BusinessServiceBase
     {
         #region "Method:InsertContactUs"
         public void InsertContactUs(osm_contactusdetails osmContactUs)
         {
             using (InsightoV2Entities dbCon = new InsightoV2Entities())
             {
                 osmContactUs.CREATED_ON = DateTime.UtcNow;
                 osmContactUs.DELETED = 0;
                 dbCon.osm_contactusdetails.Add(osmContactUs);
                 dbCon.SaveChanges();
             }
         }
         #endregion

         #region "Method:Contact Us list"
         public PagedList<osm_contactusdetails> GetContactList(PagingCriteria pageCriteria)
         {
             using (InsightoV2Entities dbCon = new InsightoV2Entities())
             {
                 var contacts = (from c in dbCon.osm_contactusdetails
                                   where c.DELETED == 0
                                   select c).ToPagedList(pageCriteria);

                 return contacts;
             }
         }
        #endregion

         public osm_contactusdetails FindContacusDetailsById(int contactUsId)
         {
             using (InsightoV2Entities dbCon = new InsightoV2Entities())
             {
                 return dbCon.osm_contactusdetails.Where(c => c.CONTACTUSUSERID == contactUsId).SingleOrDefault();
             }
         }

     }
}
