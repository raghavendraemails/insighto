﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using System.Diagnostics;
using System.Globalization;
using CovalenseUtilities.Helpers;
using System.IO;
using System.Collections;



using Insighto.Business.Enumerations;
namespace Insighto.Business.Services
{
    public class SurveyControls : BusinessServiceBase
    {
        public UserType GetLicenceType
        {
            get
            {
                var serviceSession = ServiceFactory.GetService<SessionStateService>();
                var userDet = serviceSession.GetLoginUserDetailsSession();
                if (userDet != null)
                {
                    return GenericHelper.ToEnum<UserType>(userDet.LicenseType);
                }
                else
                {
                    return GenericHelper.ToEnum<UserType>(LicenceType);

                }
            }
        }
        public string LicenceType
        {
            get
            {
                Hashtable ht = new Hashtable();
                string licenseType = "";
                if (HttpContext.Current.Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(HttpContext.Current.Request["key"].ToString());
                    if (ht != null && ht.Contains("UserId"))
                    {
                        var userDetails = ServiceFactory.GetService<UsersService>().GetUserDetails(ValidationHelper.GetInteger(ht["UserId"].ToString(), 0)).FirstOrDefault();
                        if (userDetails != null)
                            licenseType = userDetails.LICENSE_TYPE;
                    }
                }
                return licenseType;
            }
        }

        #region "Method :   "
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <returns></returns>
        public int AddSurveyControls(int surveyId)
        {
            osm_surveycontrols surveyControls = new osm_surveycontrols();
            string thanku = "", close = "", screenOut = "", overQuota = "";
            Hashtable ht = new Hashtable();
            ht = GetDefaultThankUpageContents();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var configKeys = new[] { "INSIGHTO_DEFAULTTHANKYOUCONTENT", "INSIGHTO_DEFAULTCLOSEPAGE", "INSIGHTO_DEFAULTSCREENOUTPAGE", "INSIGHTO_DEFAULTOVERQUOTA" };
                var vfolder = (from config in dbCon.oms_config
                               where configKeys.Contains(config.CONFIG_KEY)
                               select config).ToList();

                if (vfolder.Count > 0)
                {
                    for (int i = 0; i < vfolder.Count; i++)
                    {
                        if (vfolder[i].CONFIG_KEY == "INSIGHTO_DEFAULTTHANKYOUCONTENT")
                        {
                            thanku = vfolder[i].CONFIG_VALUE;
                        }
                        else if (vfolder[i].CONFIG_KEY == "INSIGHTO_DEFAULTCLOSEPAGE")
                        {
                            close = vfolder[i].CONFIG_VALUE;
                        }

                        else if (vfolder[i].CONFIG_KEY == "INSIGHTO_DEFAULTSCREENOUTPAGE")
                        {
                            screenOut = vfolder[i].CONFIG_VALUE;
                        }

                        else if (vfolder[i].CONFIG_KEY == "INSIGHTO_DEFAULTOVERQUOTA")
                        {
                            overQuota = vfolder[i].CONFIG_VALUE;
                        }
                    }// end of for
                } // end  of if 

                surveyControls.SURVEY_ID = surveyId;
                surveyControls.STATUS = 1;
                surveyControls.SCHEDULE_ON = 0;
                surveyControls.SCHEDULE_ON_ALERT = 1;
                surveyControls.SCHEDULE_ON_CLOSE = 2;
                surveyControls.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS = 0;
                surveyControls.SCHEDULE_ON_CLOSE_ALERT = 0;
                surveyControls.UNIQUE_RESPONDENT = 1;
                surveyControls.SAVE_AND_CONTINUE = 1;
                surveyControls.AUTOMATIC_THANKYOU_EMAIL = 0;
                surveyControls.ALERT_ON_MIN_RESPONSES = 0;
                surveyControls.ALERT_ON_MAX_RESPONSES = 0;
                surveyControls.NO_OF_RESPONSES = 0;
                surveyControls.LAUNCHOPTIONS_STATUS = 0;
                surveyControls.SURVEY_PASSWORD = "";
                surveyControls.SURVEY_URL = "";
                surveyControls.URL_REDIRECTION = "";
                surveyControls.REMINDER_TEXT = "";
                surveyControls.LAUNCH_ON_DATE = Convert.ToDateTime("1/1/1800");
                surveyControls.CLOSE_ON_DATE = Convert.ToDateTime("1/1/1800");
                surveyControls.ALERT_ON_MIN_RESPONSES_BY_DATE = Convert.ToDateTime("1/1/1800");
                surveyControls.ALERT_ON_MAX_RESPONSES_BY_DATE = Convert.ToDateTime("1/1/1800");
                surveyControls.REMINDER_ON_DATE = Convert.ToDateTime("1/1/1800");
                surveyControls.LAUNCHED_ON = Convert.ToDateTime("1/1/1800");

                surveyControls.TIME_FOR_URL_REDIRECTION = "";
                surveyControls.THANKYOU_PAGE = thanku;
                surveyControls.CLOSE_PAGE = close;
                surveyControls.SCREENOUT_PAGE = screenOut;
                surveyControls.OVERQUOTA_PAGE = overQuota;
                surveyControls.SURVEY_LAYOUT = 1;

                try
                {
                    dbCon.osm_surveycontrols.Add(surveyControls);
                    dbCon.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return surveyControls.SURVEYCONTROL_ID;
            }

        }

        #endregion
        #region
        public Hashtable GetDefaultThankUpageContents()
        {
            Hashtable ht = new Hashtable();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var configKeys = new[] { "INSIGHTO_DEFAULTTHANKYOUCONTENT", "INSIGHTO_DEFAULTCLOSEPAGE", "INSIGHTO_DEFAULTSCREENOUTPAGE", "INSIGHTO_DEFAULTOVERQUOTA" };
                var vfolder = (from config in dbCon.oms_config
                               where configKeys.Contains(config.CONFIG_KEY)
                               select config).ToList();


                if (vfolder.Count > 0)
                {
                    for (int i = 1; i < vfolder.Count; i++)
                    {
                        if (vfolder[i].CONFIG_KEY == "INSIGHTO_DEFAULTTHANKYOUCONTENT")
                        {
                            ht.Add("ThankUPage", vfolder[i].CONFIG_VALUE);
                        }
                        else if (vfolder[i].CONFIG_KEY == "INSIGHTO_DEFAULTCLOSEPAGE")
                        {
                            ht.Add("ClosePage", vfolder[i].CONFIG_VALUE);
                        }

                        else if (vfolder[i].CONFIG_KEY == "INSIGHTO_DEFAULTSCREENOUTPAGE")
                        {
                            ht.Add("ScreenOutPage", vfolder[i].CONFIG_VALUE);
                        }

                        else if (vfolder[i].CONFIG_KEY == "INSIGHTO_DEFAULTOVERQUOTA")
                        {
                            ht.Add("OverQuotaPage", vfolder[i].CONFIG_VALUE);
                        }
                    }// end of for
                } // end  of if
            }
            return ht;
        }
        #endregion
        #region
        public List<osm_surveycontrols> GetSurveycontrolsBySurveyId(int SurveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surControls = (from control in dbCon.osm_surveycontrols
                                   where control.SURVEY_ID == SurveyId && control.DELETED == 0
                                   select control).ToList();
                return surControls;
            }
        }
        #endregion

        #region
        public osm_surveycontrols GetSurveycontrolDetailsBySurveyId(int SurveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surControls = (from control in dbCon.osm_surveycontrols
                                   where control.SURVEY_ID == SurveyId && control.DELETED == 0
                                   select control).FirstOrDefault();
                return surControls;
            }
        }
        #endregion
        #region
        public void updateSurveyControls(osm_surveycontrols surveyControls)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_surveycontrols surControls = dbCon.osm_surveycontrols.FirstOrDefault(s => s.SURVEY_ID == surveyControls.SURVEY_ID);
                surControls.SCHEDULE_ON = surveyControls.SCHEDULE_ON;
                surControls.SCHEDULE_ON_CLOSE = surveyControls.SCHEDULE_ON_CLOSE;
                surControls.UNIQUE_RESPONDENT = surveyControls.UNIQUE_RESPONDENT;
                surControls.LAUNCHED_ON = surveyControls.LAUNCHED_ON;
                surControls.CLOSE_ON_DATE = surveyControls.CLOSE_ON_DATE;

                surControls.ALERT_ON_MAX_RESPONSES_BY_DATE = surveyControls.ALERT_ON_MAX_RESPONSES_BY_DATE;
                surControls.ALERT_ON_MIN_RESPONSES_BY_DATE = surveyControls.ALERT_ON_MIN_RESPONSES_BY_DATE;
                surControls.ALERT_ON_MAX_RESPONSES = surveyControls.ALERT_ON_MAX_RESPONSES;
                surControls.ALERT_ON_MIN_RESPONSES = surveyControls.ALERT_ON_MIN_RESPONSES;

                surControls.THANKUPAGE_TYPE = surveyControls.THANKUPAGE_TYPE;
                surControls.CLOSEPAGE_TYPE = surveyControls.CLOSEPAGE_TYPE;
                surControls.SCREENOUTPAGE_TYPE = surveyControls.SCREENOUTPAGE_TYPE;
                surControls.OVERQUOTAPAGE_TYPE = surveyControls.OVERQUOTAPAGE_TYPE;


                surControls.THANKYOU_PAGE = surveyControls.THANKYOU_PAGE;
                surControls.CLOSE_PAGE = surveyControls.CLOSE_PAGE;
                surControls.SCREENOUT_PAGE = surveyControls.SCREENOUT_PAGE;
                surControls.OVERQUOTA_PAGE = surveyControls.OVERQUOTA_PAGE;

                surControls.URL_REDIRECTION = surveyControls.URL_REDIRECTION;
                surControls.URLREDIRECTION_STATUS = surveyControls.URLREDIRECTION_STATUS;
                surControls.TIME_FOR_URL_REDIRECTION = surveyControls.TIME_FOR_URL_REDIRECTION;
                surControls.SURVEY_ID = surveyControls.SURVEY_ID;
                surControls.SEND_TO = surveyControls.SEND_TO;

                surControls.SURVEYURL_TYPE = surveyControls.SURVEYURL_TYPE;
                surControls.EMAILLIST_TYPE = surveyControls.EMAILLIST_TYPE;
                surControls.CONTACTLIST_ID = surveyControls.CONTACTLIST_ID;
                surControls.SURVEYLAUNCH_URL = surveyControls.SURVEYLAUNCH_URL;
                surControls.EMAIL_INVITATION = surveyControls.EMAIL_INVITATION;
                surControls.FROM_MAILADDRESS = surveyControls.FROM_MAILADDRESS;
                surControls.MAIL_SUBJECT = surveyControls.MAIL_SUBJECT;
                surControls.SENDER_NAME = surveyControls.SENDER_NAME;
                surControls.REPLY_TO_EMAILADDRESS = surveyControls.REPLY_TO_EMAILADDRESS;
                surControls.LAUNCHOPTIONS_STATUS = surveyControls.LAUNCHOPTIONS_STATUS;
                surControls.UNIQUE_RESPONDENT = surveyControls.UNIQUE_RESPONDENT;
                surControls.SURVEY_URL = surveyControls.SURVEY_URL;

                surControls.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS = surveyControls.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS;
                surControls.SCHEDULE_ON_CLOSE_ALERT = surveyControls.SCHEDULE_ON_CLOSE_ALERT;
                surControls.SAVE_AND_CONTINUE = surveyControls.SAVE_AND_CONTINUE;
                surControls.AUTOMATIC_THANKYOU_EMAIL = surveyControls.AUTOMATIC_THANKYOU_EMAIL;
                surControls.SURVEY_PASSWORD = surveyControls.SURVEY_PASSWORD;
                surControls.REMINDER_TEXT = surveyControls.REMINDER_TEXT;
                surControls.REMINDER_ON_DATE = surveyControls.REMINDER_ON_DATE;
                surControls.FINISHOPTIONS_STATUS = surveyControls.FINISHOPTIONS_STATUS;
                surControls.STATUS = surveyControls.STATUS;
                surControls.LAUNCH_ON_DATE = surveyControls.LAUNCH_ON_DATE;
                surControls.SURVEY_LAYOUT = surveyControls.SURVEY_LAYOUT;
                
                dbCon.SaveChanges();
            }
        }
        #endregion

        #region
        public void UpdateSurveyControlsFromView(SurveyBasicInfoView controls)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_surveycontrols surControls = dbCon.osm_surveycontrols.FirstOrDefault(s => s.SURVEY_ID == controls.SURVEY_ID && s.SURVEYCONTROL_ID == controls.SURVEYCONTROL_ID);
                surControls.URL_REDIRECTION = controls.URL_REDIRECTION;
                surControls.TIME_FOR_URL_REDIRECTION = controls.TIME_FOR_URL_REDIRECTION;
                surControls.THANKYOU_PAGE = controls.THANKYOU_PAGE;
                surControls.THANKUPAGE_TYPE = controls.THANKUPAGE_TYPE;
                surControls.CLOSE_PAGE = controls.CLOSE_PAGE;
                surControls.CLOSEPAGE_TYPE = controls.CLOSEPAGE_TYPE;
                surControls.SCREENOUT_PAGE = controls.SCREENOUT_PAGE;
                surControls.SCREENOUTPAGE_TYPE = controls.SCREENOUTPAGE_TYPE;
                surControls.OVERQUOTA_PAGE = controls.OVERQUOTA_PAGE;
                surControls.OVERQUOTAPAGE_TYPE = controls.OVERQUOTAPAGE_TYPE;
                surControls.FINISHOPTIONS_STATUS = controls.FINISHOPTIONS_STATUS;
                surControls.STATUS = ValidationHelper.GetInteger(controls.STATUS, 0);
                surControls.SCHEDULE_ON = controls.SCHEDULE_ON;
                surControls.SCHEDULE_ON_ALERT = controls.SCHEDULE_ON_ALERT;
                surControls.SCHEDULE_ON_CLOSE = controls.SCHEDULE_ON_CLOSE;
                surControls.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS = controls.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS;
                surControls.SCHEDULE_ON_CLOSE_ALERT = controls.SCHEDULE_ON_CLOSE_ALERT;
                surControls.ALERT_ON_MIN_RESPONSES = controls.ALERT_ON_MIN_RESPONSES;
                surControls.ALERT_ON_MAX_RESPONSES = controls.ALERT_ON_MAX_RESPONSES;
                surControls.REMINDER_TEXT = controls.REMINDER_TEXT;
                surControls.LAUNCH_ON_DATE = controls.LAUNCH_ON_DATE;
                surControls.CLOSE_ON_DATE = controls.CLOSE_ON_DATE;
                surControls.ALERT_ON_MIN_RESPONSES_BY_DATE = controls.ALERT_ON_MIN_RESPONSES_BY_DATE;
                surControls.ALERT_ON_MAX_RESPONSES_BY_DATE = controls.ALERT_ON_MAX_RESPONSES_BY_DATE;
                surControls.REMINDER_ON_DATE = controls.REMINDER_ON_DATE;
                surControls.SURVEY_STATUS = controls.SURVEY_STATUS;
                surControls.URLREDIRECTION_STATUS = controls.URLREDIRECTION_STATUS;
                surControls.SURVEY_LAYOUT = Convert.ToInt32(controls.SURVEY_LAYOUT);
                dbCon.SaveChanges();

            }
            // return controls;
        }
        #endregion

        #region
        public void InsertSurveyControlsFromView(SurveyBasicInfoView controls)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_surveycontrols surControls = dbCon.osm_surveycontrols.FirstOrDefault(s => s.SURVEY_ID == controls.SURVEY_ID && s.SURVEYCONTROL_ID == controls.SURVEYCONTROL_ID);
                surControls.URL_REDIRECTION = controls.URL_REDIRECTION;
                surControls.TIME_FOR_URL_REDIRECTION = controls.TIME_FOR_URL_REDIRECTION;
                surControls.THANKYOU_PAGE = controls.THANKYOU_PAGE;
                surControls.THANKUPAGE_TYPE = controls.THANKUPAGE_TYPE;
                surControls.CLOSE_PAGE = controls.CLOSE_PAGE;
                surControls.CLOSEPAGE_TYPE = controls.CLOSEPAGE_TYPE;
                surControls.SCREENOUT_PAGE = controls.SCREENOUT_PAGE;
                surControls.SCREENOUTPAGE_TYPE = controls.SCREENOUTPAGE_TYPE;
                surControls.OVERQUOTA_PAGE = controls.OVERQUOTA_PAGE;
                surControls.OVERQUOTAPAGE_TYPE = controls.OVERQUOTAPAGE_TYPE;
                surControls.FINISHOPTIONS_STATUS = controls.FINISHOPTIONS_STATUS;
                surControls.STATUS = ValidationHelper.GetInteger(controls.STATUS, 0);
                surControls.SCHEDULE_ON = controls.SCHEDULE_ON;
                surControls.SCHEDULE_ON_ALERT = controls.SCHEDULE_ON_ALERT;
                surControls.SCHEDULE_ON_CLOSE = controls.SCHEDULE_ON_CLOSE;
                surControls.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS = controls.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS;
                surControls.SCHEDULE_ON_CLOSE_ALERT = controls.SCHEDULE_ON_CLOSE_ALERT;
                surControls.ALERT_ON_MIN_RESPONSES = controls.ALERT_ON_MIN_RESPONSES;
                surControls.ALERT_ON_MAX_RESPONSES = controls.ALERT_ON_MAX_RESPONSES;
                surControls.REMINDER_TEXT = controls.REMINDER_TEXT;
                surControls.LAUNCH_ON_DATE = controls.LAUNCH_ON_DATE;
                surControls.CLOSE_ON_DATE = controls.CLOSE_ON_DATE;
                surControls.ALERT_ON_MIN_RESPONSES_BY_DATE = controls.ALERT_ON_MIN_RESPONSES_BY_DATE;
                surControls.ALERT_ON_MAX_RESPONSES_BY_DATE = controls.ALERT_ON_MAX_RESPONSES_BY_DATE;
                surControls.REMINDER_ON_DATE = controls.REMINDER_ON_DATE;
                surControls.SURVEY_STATUS = controls.SURVEY_STATUS;
                dbCon.osm_surveycontrols.Add(surControls);
                dbCon.SaveChanges();

            }
            // return controls;
        }
        #endregion

        #region
        public void EditActiveAlertsUpdate(int surveyId, int alertLaunchFlag)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                //if (alertLaunchFlag == 0)
                //{
                //    var alert = dbCon.osm_alertmanagement.Where(a => a.SURVEY_ID == surveyId);
                //    foreach (var a in alert)
                //    {
                //        a.DELETED = 1;
                //        dbCon.SaveChanges();
                //    }
                //}
                //else
                //{
                //    var alert = dbCon.osm_alertmanagement.Where(a => a.SURVEY_ID == surveyId && a.ALERT_TYPE != "SurveyLaunchOnDate");
                //    foreach (var a in alert)
                //    {
                //        a.DELETED = 1;
                //        dbCon.SaveChanges();
                //    }
                //}
                var control = GetSurveycontrolsBySurveyId(surveyId).FirstOrDefault();
                DateTime launchDate;
                if (control != null)
                {
                    launchDate = Convert.ToDateTime(control.LAUNCH_ON_DATE);
                    launchDate = (launchDate > Convert.ToDateTime("1/1/1900 12:00:00 AM")) ? launchDate : DateTime.UtcNow;

                    if (control.SCHEDULE_ON_ALERT == 1 && alertLaunchFlag == 0)
                    {
                        osm_alertmanagement alert = new osm_alertmanagement();
                        alert.SURVEY_ID = surveyId;
                        alert.ALERT_TYPE = "SurveyLaunchOnDate";
                        alert.ALERT_INFO = "";
                        alert.ALERT_DATE = launchDate;
                        alert.DELETED = ValidationHelper.GetInteger(AlertType.Active, 0);
                        SaveorUpdateSurveyAlerts(alert);
                    }
                    if (control.SCHEDULE_ON_CLOSE == 0 && control.SCHEDULE_ON_CLOSE_ALERT == 1)
                    {
                        var alertInfo = control.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS;
                        osm_alertmanagement alert = new osm_alertmanagement();
                        alert.SURVEY_ID = surveyId;
                        alert.ALERT_TYPE = "SurveyCloseOnResponseCount";
                        alert.ALERT_INFO = alertInfo.ToString();
                        alert.ALERT_DATE = Convert.ToDateTime("1/1/1900 12:00:00 AM");
                        alert.DELETED = ValidationHelper.GetInteger(AlertType.Active, 0);
                        SaveorUpdateSurveyAlerts(alert);
                    }
                    if (control.SCHEDULE_ON_CLOSE == 1 && control.SCHEDULE_ON_CLOSE_ALERT == 1)
                    {
                        DateTime SurveyClose = Convert.ToDateTime(control.CLOSE_ON_DATE);
                        SurveyClose = (SurveyClose > Convert.ToDateTime("1/1/1900 12:00:00 AM")) ? SurveyClose : Convert.ToDateTime("1/1/1900 12:00:00 AM");
                        if (SurveyClose > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                        {
                            osm_alertmanagement alert = new osm_alertmanagement();
                            alert.SURVEY_ID = surveyId;
                            alert.ALERT_TYPE = "SurveyCloseOnDate";
                            alert.ALERT_INFO = "";
                            alert.ALERT_DATE = SurveyClose;
                            alert.DELETED = ValidationHelper.GetInteger(AlertType.Active, 0);
                            SaveorUpdateSurveyAlerts(alert);
                        }
                    }
                    DateTime MinResponsedate = Convert.ToDateTime(control.ALERT_ON_MIN_RESPONSES_BY_DATE);
                    if (control.ALERT_ON_MIN_RESPONSES > 0 || MinResponsedate > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                    {
                        MinResponsedate = (MinResponsedate > Convert.ToDateTime("1/1/1900 12:00:00 AM")) ? MinResponsedate : Convert.ToDateTime("1/1/1900 12:00:00 AM");
                        osm_alertmanagement alert = new osm_alertmanagement();
                        alert.SURVEY_ID = surveyId;
                        alert.ALERT_TYPE = "MinResponse";
                        alert.ALERT_INFO = control.ALERT_ON_MIN_RESPONSES.ToString();
                        alert.ALERT_DATE = MinResponsedate;
                        alert.DELETED = ValidationHelper.GetInteger(AlertType.Active, 0);
                        SaveorUpdateSurveyAlerts(alert);
                    }
                    DateTime MaxResponsedate = Convert.ToDateTime(control.ALERT_ON_MAX_RESPONSES_BY_DATE);
                    if (control.ALERT_ON_MAX_RESPONSES > 0 || MaxResponsedate > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                    {
                        MaxResponsedate = (MaxResponsedate > Convert.ToDateTime("1/1/1900 12:00:00 AM")) ? MaxResponsedate : Convert.ToDateTime("1/1/1900 12:00:00 AM");
                        osm_alertmanagement alert = new osm_alertmanagement();
                        alert.SURVEY_ID = surveyId;
                        alert.ALERT_TYPE = "MaxResponse";
                        alert.ALERT_INFO = control.ALERT_ON_MAX_RESPONSES.ToString();
                        alert.ALERT_DATE = MaxResponsedate;
                        alert.DELETED = ValidationHelper.GetInteger(AlertType.Active, 0);
                        SaveorUpdateSurveyAlerts(alert);
                    }
                    if (control.REMINDER_TEXT.ToString().Length > 0)
                    {
                        DateTime Reminderdate = Convert.ToDateTime(control.REMINDER_ON_DATE);
                        Reminderdate = (Reminderdate > Convert.ToDateTime("1/1/1900 12:00:00 AM")) ? Reminderdate : Convert.ToDateTime("1/1/1900 12:00:00 AM");
                        osm_alertmanagement alert = new osm_alertmanagement();
                        alert.SURVEY_ID = surveyId;
                        alert.ALERT_TYPE = "ReminderText";
                        alert.ALERT_INFO = control.REMINDER_TEXT.ToString();
                        alert.ALERT_DATE = Reminderdate;
                        alert.DELETED = ValidationHelper.GetInteger(AlertType.Active, 0);
                        SaveorUpdateSurveyAlerts(alert);
                    }



                }


            }
        }
        #endregion

        #region
        public void InsertSurveyAlerts(osm_alertmanagement alerts)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_alertmanagement.Add(alerts);
                dbCon.SaveChanges();

            }
        }
        #endregion

        #region
        public void SaveorUpdateSurveyAlerts(osm_alertmanagement alerts)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var alert = dbCon.osm_alertmanagement.Where(a => a.SURVEY_ID == alerts.SURVEY_ID && a.ALERT_TYPE == alerts.ALERT_TYPE).FirstOrDefault();
                if (alert == null)
                {
                    dbCon.osm_alertmanagement.Add(alerts);
                    dbCon.SaveChanges();
                }
                else
                {
                    alert.SURVEY_ID = alerts.SURVEY_ID;
                    alert.DELETED = ValidationHelper.GetInteger(AlertType.Active, 0);
                    alert.ALERT_TYPE = alerts.ALERT_TYPE;
                    alert.ALERT_INFO = alerts.ALERT_INFO;
                    alert.ALERT_DATE = alerts.ALERT_DATE;
                    dbCon.SaveChanges();
                }


            }
        }
        #endregion

        public void UpdateCopySurveyControls(int copyFromSurId, int copyToSurId)
        {
            var featureService = ServiceFactory.GetService<FeatureService>();
            var surContrlSer = ServiceFactory.GetService<SurveyControls>();
            var surCopyToControls = surContrlSer.GetSurveycontrolsBySurveyId(copyToSurId);
            osm_surveycontrols osmControl = new osm_surveycontrols();
            var surCopyFromControls = surContrlSer.GetSurveycontrolsBySurveyId(copyFromSurId);
            Hashtable ht = new Hashtable();
            ht = surContrlSer.GetDefaultThankUpageContents();
            if (surCopyToControls[0].SURVEYCONTROL_ID > 0 && surCopyFromControls != null && surCopyFromControls.Count > 0)
            {
                foreach (var surControl in surCopyFromControls)
                {
                    osmControl.SCHEDULE_ON = (featureService.Find(GetLicenceType, FeatureDeploymentType.SCHEDULE_SURVEYLAUNCH).Value == 0) ? 0 : surControl.SCHEDULE_ON;
                    osmControl.SCHEDULE_ON_CLOSE = ((featureService.Find(GetLicenceType, FeatureDeploymentType.SCHEDULE_SURVEYCLOSE).Value == 0) && (surControl.SCHEDULE_ON_CLOSE == 1)) ? 2 : surControl.SCHEDULE_ON_CLOSE;
                    osmControl.UNIQUE_RESPONDENT = ((featureService.Find(GetLicenceType, FeatureDeploymentType.UNIQUE_RESPONDENT).Value == 0) && (surControl.UNIQUE_RESPONDENT == 2) && (featureService.Find(UserType.FREE, FeatureDeploymentType.UNIQUE_RESPONDENT_PASSWORD).Value == 0)) ? 1 : surControl.UNIQUE_RESPONDENT;
                    osmControl.LAUNCH_ON_DATE = (featureService.Find(GetLicenceType, FeatureDeploymentType.SCHEDULE_SURVEYLAUNCH).Value == 0) ? Convert.ToDateTime("1/1/1800") : surControl.LAUNCH_ON_DATE;
                    osmControl.CLOSE_ON_DATE = ((featureService.Find(GetLicenceType, FeatureDeploymentType.SCHEDULE_SURVEYCLOSE).Value == 0) && (surControl.CLOSEPAGE_TYPE == 1)) ? Convert.ToDateTime("1/1/1800") : surControl.CLOSE_ON_DATE;
                    osmControl.ALERT_ON_MAX_RESPONSES_BY_DATE = surControl.ALERT_ON_MAX_RESPONSES_BY_DATE;
                    osmControl.ALERT_ON_MIN_RESPONSES_BY_DATE = surControl.ALERT_ON_MIN_RESPONSES_BY_DATE;
                    osmControl.ALERT_ON_MAX_RESPONSES = surControl.ALERT_ON_MAX_RESPONSES;
                    osmControl.ALERT_ON_MIN_RESPONSES = surControl.ALERT_ON_MIN_RESPONSES;

                    if (featureService.Find(GetLicenceType, FeatureDeploymentType.ALERT_MINMAXNO_RESPONDENTS).Value == 0)
                    {
                        osmControl.ALERT_ON_MAX_RESPONSES = 0;
                        osmControl.ALERT_ON_MIN_RESPONSES = 0;
                        osmControl.ALERT_ON_MAX_RESPONSES_BY_DATE = Convert.ToDateTime("1/1/1800");
                        osmControl.ALERT_ON_MIN_RESPONSES_BY_DATE = Convert.ToDateTime("1/1/1800");
                    }

                    osmControl.THANKUPAGE_TYPE = ((featureService.Find(GetLicenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 0) || (surControl.THANKUPAGE_TYPE == 1) && (surControl.THANKUPAGE_TYPE == 2) && (surControl.THANKUPAGE_TYPE == 1) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.KIOSK).Value == 0)) ? 0 : surControl.THANKUPAGE_TYPE;
                    osmControl.CLOSEPAGE_TYPE = ((surControl.CLOSEPAGE_TYPE == 0) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 0) || (surControl.CLOSEPAGE_TYPE == 2) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.KIOSK).Value == 0)) ? 0 : surControl.CLOSEPAGE_TYPE;
                    osmControl.SCREENOUTPAGE_TYPE = ((surControl.SCREENOUTPAGE_TYPE == 1) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 0) || (surControl.SCREENOUTPAGE_TYPE == 2) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.KIOSK).Value == 0)) ? 0 : surControl.SCREENOUTPAGE_TYPE;
                    osmControl.OVERQUOTAPAGE_TYPE = ((surControl.OVERQUOTAPAGE_TYPE == 1) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 0) || (surControl.OVERQUOTAPAGE_TYPE == 2) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.KIOSK).Value == 0)) ? 0 : surControl.OVERQUOTAPAGE_TYPE;

                    osmControl.THANKYOU_PAGE = ((surControl.THANKUPAGE_TYPE == 1) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 0) || (surControl.THANKUPAGE_TYPE == 2) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.KIOSK).Value == 0)) ? Convert.ToString(ht["ThankUPage"]) : surControl.THANKYOU_PAGE;
                    osmControl.CLOSE_PAGE = ((surControl.CLOSEPAGE_TYPE == 1) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 0) || (surControl.CLOSEPAGE_TYPE == 2) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.KIOSK).Value == 0)) ? Convert.ToString(ht["ClosePage"]) : surControl.CLOSE_PAGE;
                    osmControl.SCREENOUT_PAGE = ((surControl.SCREENOUTPAGE_TYPE == 1) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 0) || (surControl.SCREENOUTPAGE_TYPE == 2) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.KIOSK).Value == 0)) ? Convert.ToString(ht["ScreenOutPage"]) : surControl.SCREENOUT_PAGE;
                    osmControl.OVERQUOTA_PAGE = ((surControl.OVERQUOTAPAGE_TYPE == 1) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 0) || (surControl.OVERQUOTAPAGE_TYPE == 2) && (featureService.Find(GetLicenceType, FeatureSurveyCreation.KIOSK).Value == 0)) ? Convert.ToString(ht["OverQuotaPage"]) : surControl.OVERQUOTA_PAGE;

                    osmControl.URL_REDIRECTION = (featureService.Find(GetLicenceType, FeatureDeploymentType.REDIRECTION_URL).Value == 0) ? "" : surControl.URL_REDIRECTION;
                    osmControl.URLREDIRECTION_STATUS = (featureService.Find(GetLicenceType, FeatureDeploymentType.REDIRECTION_URL).Value == 0) ? 0 : surControl.URLREDIRECTION_STATUS;
                    osmControl.TIME_FOR_URL_REDIRECTION = (featureService.Find(GetLicenceType, FeatureDeploymentType.REDIRECTION_URL).Value == 0) ? "0" : surControl.TIME_FOR_URL_REDIRECTION;

                    osmControl.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS = surControl.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS;
                    osmControl.SCHEDULE_ON_CLOSE_ALERT = surControl.SCHEDULE_ON_CLOSE_ALERT;
                    osmControl.SAVE_AND_CONTINUE = surControl.SAVE_AND_CONTINUE;
                    osmControl.AUTOMATIC_THANKYOU_EMAIL = surControl.AUTOMATIC_THANKYOU_EMAIL;
                    osmControl.SURVEY_PASSWORD = surControl.SURVEY_PASSWORD;
                    osmControl.REMINDER_TEXT = surControl.REMINDER_TEXT;
                    osmControl.REMINDER_ON_DATE = surControl.REMINDER_ON_DATE;
                    osmControl.FINISHOPTIONS_STATUS = 1;
                    osmControl.STATUS = 1;
                    osmControl.LAUNCHED_ON = surControl.LAUNCHED_ON;
                    osmControl.SURVEY_ID = copyToSurId;
                    if (surControl.SURVEY_LAYOUT == 0)
                        osmControl.SURVEY_LAYOUT = 1;
                    else
                        osmControl.SURVEY_LAYOUT = surControl.SURVEY_LAYOUT;
                }

                surContrlSer.updateSurveyControls(osmControl);
            }
        }

        public void InsertSurveyControlsLog(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveyControls = dbCon.osm_surveycontrols.Where(s => s.SURVEY_ID == surveyId).FirstOrDefault();
                osm_surveycontrols_log surveyControlsLog = new osm_surveycontrols_log();
                surveyControlsLog.ALERT_ON_MAX_RESPONSES = surveyControls.ALERT_ON_MAX_RESPONSES;
                surveyControlsLog.ALERT_ON_MAX_RESPONSES_BY_DATE = surveyControls.ALERT_ON_MAX_RESPONSES_BY_DATE;
                surveyControlsLog.ALERT_ON_MIN_RESPONSES = surveyControls.ALERT_ON_MIN_RESPONSES;
                surveyControlsLog.ALERT_ON_MIN_RESPONSES_BY_DATE = surveyControls.ALERT_ON_MIN_RESPONSES_BY_DATE;
                surveyControlsLog.AUTOMATIC_THANKYOU_EMAIL = surveyControls.AUTOMATIC_THANKYOU_EMAIL;
                surveyControlsLog.CLOSE_ON_DATE = surveyControls.CLOSE_ON_DATE;
                surveyControlsLog.CLOSE_ON_NO_RESPONDENTS = surveyControls.CLOSE_ON_NO_RESPONDENTS;
                surveyControlsLog.CLOSE_PAGE = surveyControls.CLOSE_PAGE;
                surveyControlsLog.CLOSEPAGE_TYPE = surveyControls.CLOSEPAGE_TYPE;
                surveyControlsLog.CONTACTLIST_ID = surveyControls.CONTACTLIST_ID;
                surveyControlsLog.CONTINUOUS_SURVEY = surveyControls.CONTINUOUS_SURVEY;
                surveyControlsLog.DELETED = surveyControls.DELETED;
                surveyControlsLog.EMAIL_INVITATION = surveyControls.EMAIL_INVITATION;
                surveyControlsLog.EMAILLIST_TYPE = surveyControls.EMAILLIST_TYPE;
                surveyControlsLog.FINISHOPTIONS_STATUS = surveyControls.FINISHOPTIONS_STATUS;
                surveyControlsLog.FROM_MAILADDRESS = surveyControls.FROM_MAILADDRESS;
                surveyControlsLog.LAUNCH_ON_DATE = surveyControls.LAUNCH_ON_DATE;
                surveyControlsLog.LAUNCHED_ON = surveyControls.LAUNCHED_ON;
                surveyControlsLog.LAUNCHOPTIONS_STATUS = surveyControls.LAUNCHOPTIONS_STATUS;
                surveyControlsLog.MAIL_SUBJECT = surveyControls.MAIL_SUBJECT;
                surveyControlsLog.NO_OF_RESPONSES = surveyControls.NO_OF_RESPONSES;
                surveyControlsLog.ONLAUNCH_ALERT = surveyControls.ONLAUNCH_ALERT;
                surveyControlsLog.OVERQUOTA_PAGE = surveyControls.OVERQUOTA_PAGE;
                surveyControlsLog.OVERQUOTAPAGE_TYPE = surveyControls.OVERQUOTAPAGE_TYPE;
                surveyControlsLog.PARTIAL_RESPONSES = surveyControls.PARTIAL_RESPONSES;
                surveyControlsLog.PARTIAL_RESPONSES_EXT = surveyControls.PARTIAL_RESPONSES_EXT;
                surveyControlsLog.REMINDER_ON_DATE = surveyControls.REMINDER_ON_DATE;
                surveyControlsLog.REMINDER_TEXT = surveyControls.REMINDER_TEXT;
                surveyControlsLog.REPLY_TO_EMAILADDRESS = surveyControls.REPLY_TO_EMAILADDRESS;
                surveyControlsLog.SAVE_AND_CONTINUE = surveyControls.SAVE_AND_CONTINUE;
                surveyControlsLog.SCHEDULE_ON = surveyControls.SCHEDULE_ON;
                surveyControlsLog.SCHEDULE_ON_ALERT = surveyControls.SCHEDULE_ON_ALERT;
                surveyControlsLog.SCHEDULE_ON_CLOSE = surveyControls.SCHEDULE_ON_CLOSE;
                surveyControlsLog.SCHEDULE_ON_CLOSE_ALERT = surveyControls.SCHEDULE_ON_CLOSE_ALERT;
                surveyControlsLog.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS = surveyControls.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS;
                surveyControlsLog.SCREEN_OUTS = surveyControls.SCREEN_OUTS;
                surveyControlsLog.SCREEN_OUTS_EXT = surveyControls.SCREEN_OUTS_EXT;
                surveyControlsLog.SCREENOUT_PAGE = surveyControls.SCREENOUT_PAGE;
                surveyControlsLog.SCREENOUTPAGE_TYPE = surveyControls.SCREENOUTPAGE_TYPE;
                surveyControlsLog.SEND_TO = surveyControls.SEND_TO;
                surveyControlsLog.SEND_TO_EXT = surveyControls.SEND_TO_EXT;
                surveyControlsLog.SENDER_NAME = surveyControls.SENDER_NAME;
                surveyControlsLog.STATUS = surveyControls.STATUS;
                surveyControlsLog.SURVEY_ID = surveyControls.SURVEY_ID;
                surveyControlsLog.SURVEY_PASSWORD = surveyControls.SURVEY_PASSWORD;
                surveyControlsLog.SURVEY_STATUS = surveyControls.SURVEY_STATUS;
                surveyControlsLog.SURVEY_URL = surveyControls.SURVEY_URL;
                surveyControlsLog.SURVEYCONTROL_ID = surveyControls.SURVEYCONTROL_ID;
                surveyControlsLog.SURVEYLAUNCH_URL = surveyControls.SURVEYLAUNCH_URL;
                surveyControlsLog.SURVEYURL_TYPE = surveyControls.SURVEYURL_TYPE;
                surveyControlsLog.THANKUPAGE_TYPE = surveyControls.THANKUPAGE_TYPE;
                surveyControlsLog.THANKYOU_PAGE = surveyControls.THANKYOU_PAGE;
                surveyControlsLog.TIME_FOR_URL_REDIRECTION = surveyControls.TIME_FOR_URL_REDIRECTION;
                surveyControlsLog.TOTAL_RESPONSES = surveyControls.TOTAL_RESPONSES;
                surveyControlsLog.TOTAL_RESPONSES_EXT = surveyControls.TOTAL_RESPONSES_EXT;
                surveyControlsLog.UNIQUE_RESPONDENT = surveyControls.UNIQUE_RESPONDENT;
                surveyControlsLog.UNSUBSCRIBED = surveyControls.UNSUBSCRIBED;
                surveyControlsLog.URL_REDIRECTION = surveyControls.URL_REDIRECTION;
                surveyControlsLog.URLREDIRECTION_STATUS = surveyControls.URLREDIRECTION_STATUS;
                dbCon.osm_surveycontrols_log.Add(surveyControlsLog);
                dbCon.SaveChanges();

            }

        }

        public void UpdateExternalCount(int surveyId, int externalResponses)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surControls = (from control in dbCon.osm_surveycontrols
                                   where control.SURVEY_ID == surveyId
                                   select control).SingleOrDefault();
                surControls.SEND_TO_EXT = externalResponses;
                dbCon.SaveChanges();
            }
        }

        public void UpdateEmailsCount(int surveyId, int emailCount)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var surveycontrols = dbCon.osm_surveycontrols.FirstOrDefault(s => s.SURVEY_ID == surveyId); //ServiceFactory<SurveyControls>.Instance.GetSurveycontrolsBySurveyId(surveyId).FirstOrDefault();
                surveycontrols.SEND_TO = surveycontrols.SEND_TO + emailCount;
                dbCon.SaveChanges();
            }
        }

    }
}
