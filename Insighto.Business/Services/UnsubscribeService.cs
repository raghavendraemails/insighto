﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Exceptions;
using System.IO;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using Insighto.Business.Extensions;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using CovalenseUtilities.Helpers;

namespace Insighto.Business.Services
{
    public class UnsubscribeService : BusinessServiceBase
    {
        #region "Method:SaveEmailUnsubscribeList"

        public osm_unsubscribelist SaveEmailUnsubscribeList(osm_unsubscribelist usList)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_unsubscribelist.Add(usList);
                dbCon.SaveChanges();
                return usList;
            }

        }
        #endregion

        #region "Method:GetUnsubscribeListByEmailId"

        public List<osm_unsubscribelist> GetUnsubscribeListByEmailId(int emailId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_unsubscribelist.Where(u => u.EMAIL_ID == emailId).ToList();
            }

        }
        #endregion

        #region "Method:GetUnsubscribeListByUserId"

        public List<osm_unsubscribelist> GetUnsubscribeListByUserId(int userId,string emailAddress)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_unsubscribelist.Where(u => u.CREATEDBYID == userId && u.EMAILADDRESS == emailAddress).ToList();
            }

        }
        #endregion
    }
}
