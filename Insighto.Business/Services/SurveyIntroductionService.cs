﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using Insighto.Business.Extensions;


namespace Insighto.Business.Services
{
    public class SurveyIntroductionService : BusinessServiceBase
    {
        #region "Method: GetSurveyIntroBySurveyId"
        /// <summary>
        /// Used to get the survey Introduction.
        /// </summary>
        /// <param name="SurveyId"></param>
        /// <returns></returns>

        public List<osm_surveyIntro> FindBySurveyId(int SurveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_surveyIntro.Where(surveyIntro => surveyIntro.SURVEY_ID == SurveyId).ToList();
            }

        }
        #endregion


        #region "Method : InsertOrUpdateSurveyIntro"
        /// <summary>
        /// Used to insert or update survey introduction.
        /// </summary>
        /// <param name="osmSurveyIntro"></param>
        /// <returns></returns>

        public bool Save(osm_surveyIntro osmSurveyIntro)
        {
            bool isUpdated = default(bool);

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var isSurvey = (from surveyIntro in dbCon.osm_surveyIntro
                                where surveyIntro.SURVEY_ID == osmSurveyIntro.SURVEY_ID && surveyIntro.DELETED == 0
                                select surveyIntro).ToList();
                if (isSurvey.Any())
                {
                    osm_surveyIntro surveyIntro = dbCon.osm_surveyIntro.FirstOrDefault(si => si.SURVEY_ID == osmSurveyIntro.SURVEY_ID && si.DELETED == 0);
                    surveyIntro.SURVEY_ID = osmSurveyIntro.SURVEY_ID;
                    surveyIntro.USERID = osmSurveyIntro.USERID;
                    surveyIntro.SURVEY_INTRO = osmSurveyIntro.SURVEY_INTRO;
                    surveyIntro.CREATED_ON = DateTime.UtcNow;
                    surveyIntro.STATUS = osmSurveyIntro.STATUS;
                    surveyIntro.PAGE_BREAK = osmSurveyIntro.PAGE_BREAK;
                    dbCon.SaveChanges();
                    isUpdated = true;

                    int surveyId = ValidationHelper.GetInteger(osmSurveyIntro.SURVEY_ID.ToString(),0);
                    var surveyService = ServiceFactory.GetService<SurveyService>();
                    surveyService.UpdateModifedDateBySurId(surveyId);
                }
                else
                {

                    dbCon.osm_surveyIntro.Add(osmSurveyIntro);
                    dbCon.SaveChanges();
                }

            }
            return isUpdated;
        }
        #endregion

        public void CopySurveyIntro(int copyFromSurId, int copyToSurId)
        {
            var serviceIntro = ServiceFactory.GetService<SurveyIntroductionService>();
            var surIntroDet = serviceIntro.FindBySurveyId(copyFromSurId);
            if (surIntroDet.Count > 0)
            {
                osm_surveyIntro surveyIntro = new osm_surveyIntro();
                surveyIntro.SURVEY_ID = copyToSurId;
                surveyIntro.SURVEY_INTRO = surIntroDet[0].SURVEY_INTRO;
                surveyIntro.USERID = surIntroDet[0].USERID;
                surveyIntro.STATUS = surIntroDet[0].STATUS;
                surveyIntro.PAGE_BREAK = surIntroDet[0].PAGE_BREAK;
                surveyIntro.CREATED_ON = DateTime.UtcNow;
                surveyIntro.DELETED = surIntroDet[0].DELETED;
                serviceIntro.Save(surveyIntro);
            }
            
        }
    }
}
