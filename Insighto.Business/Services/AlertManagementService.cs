﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;
using CovalenseUtilities.Services;

namespace Insighto.Business.Services
{
   public class AlertManagementService : BusinessServiceBase
    {
        #region "Method: InsertAlertmanagement"
       public osm_alertmanagement InsertAlertmanagement(osm_alertmanagement alert)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_alertmanagement.Add(alert);
                dbCon.SaveChanges();
            }
            return alert;

        }
        #endregion
    }
}
