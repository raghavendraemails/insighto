﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Extensions;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;

namespace Insighto.Business.Services
{
    public class WidgetService : BusinessServiceBase
    {
        public osm_widget AddWidget(osm_widget osmWidget)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_widget.Add(osmWidget);
                dbCon.SaveChanges();
                return osmWidget;
            }
        }

        public osm_widget GetWidgetDetailsBySurveyId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
               return dbCon.osm_widget.Where(w => w.SurveyId == surveyId).FirstOrDefault(); 
               
            }
        }
    }
}
