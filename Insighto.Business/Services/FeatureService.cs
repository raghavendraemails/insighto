﻿using System.Collections.Generic;
using System.Linq;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using Insighto.Business.Extensions;


namespace Insighto.Business.Services
{
    public class FeatureService : BusinessServiceBase
    {
        public List<FeatureInfo> Find(UserType userType)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                switch (userType)
                {
                    case UserType.FREE:
                        return dbCon.osm_features.Where(u => u.DELETED == 0).
                               Select(s => new FeatureInfo { Feature = s.FEATURE, Value = s.FREE, PK_FEATURE_ID = s.PK_FEATURE_ID }).ToList();
                    case UserType.PRO_MONTHLY:
                        return dbCon.osm_features.Where(u => u.DELETED == 0).
                            Select(s => new FeatureInfo { Feature = s.FEATURE, Value = s.PRO_MONTHLY, PK_FEATURE_ID = s.PK_FEATURE_ID }).ToList();
                    case UserType.PRO_QUARTERLY:
                        return dbCon.osm_features.Where(u => u.DELETED == 0).
                            Select(s => new FeatureInfo { Feature = s.FEATURE, Value = s.PRO_QUARTERLY, PK_FEATURE_ID = s.PK_FEATURE_ID }).ToList();
                    case UserType.PRO_YEARLY:
                        return dbCon.osm_features.Where(u => u.DELETED == 0).
                            Select(s => new FeatureInfo { Feature = s.FEATURE, Value = s.PRO_YEARLY, PK_FEATURE_ID = s.PK_FEATURE_ID }).ToList();
                    case UserType.PREMIUM_MONTHLY:
                        return dbCon.osm_features.Where(u => u.DELETED == 0).
                            Select(s => new FeatureInfo { Feature = s.FEATURE, Value = s.PREMIUM_MONTHLY, PK_FEATURE_ID = s.PK_FEATURE_ID }).ToList();
                    case UserType.PREMIUM_QUARTERLY:
                        return dbCon.osm_features.Where(u => u.DELETED == 0).
                            Select(s => new FeatureInfo { Feature = s.FEATURE, Value = s.PREMIUM_QUARTERLY, PK_FEATURE_ID = s.PK_FEATURE_ID }).ToList();
                    case UserType.PREMIUM_YEARLY:
                        return dbCon.osm_features.Where(u => u.DELETED == 0).
                            Select(s => new FeatureInfo { Feature = s.FEATURE, Value = s.PREMIUM_YEARLY, PK_FEATURE_ID = s.PK_FEATURE_ID }).ToList();
                    default:
                        return new List<FeatureInfo>();
                }
            }
        }

        private FeatureInfo Find(UserType userType, string feature)
        {
            var featureList = Find(userType);

            return featureList.Where(f => f.Feature.ToLower() == feature.ToLower()).FirstOrDefault();
        }

        public FeatureInfo Find(UserType userType, FeatureQuestionType feature)
        {
            return Find(userType, feature.ToString());
        }

        public FeatureInfo Find(UserType userType, FeatureReportType feature)
        {
            return Find(userType, feature.ToString());
        }

        public FeatureInfo Find(UserType userType, FeaturePersonalisationType feature)
        {
            return Find(userType, feature.ToString());
        }

        public FeatureInfo Find(UserType userType, FeatureDeploymentType feature)
        {
            return Find(userType, feature.ToString());
        }

        public FeatureInfo Find(UserType userType, FeatureSurveyCreation feature)
        {
            return Find(userType, feature.ToString());
        }

        public List<FeatureInfo> GetCommonFeaturesByFlag(int flag)
        {
            var uFeatureInfo = new List<FeatureInfo>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                uFeatureInfo = (from osmfeature in dbCon.osm_featurelist
                                where osmfeature.FEATURE_TYPE_FLAG == flag
                                select new FeatureInfo
                                {
                                    FeatureGroup = osmfeature.FEATURE_GROUP,
                                    FeatureName = osmfeature.FEATURE_NAME,
                                    Free_Licensetype = osmfeature.FREE_LICENSE_TYPE,
                                    Prof_Licensetype = osmfeature.PROF_LICENSE_TYPE,
                                    Crop_Licensetype = osmfeature.CORP_LICENSE_TYPE,
                                    PK_FEATURE_ID = osmfeature.PK_FEATURE_ID
                                }
                       ).ToList();
            }
            return uFeatureInfo;
        }

        public List<FeatureInfo> GetFeaturesGroups()
        {
            var osmFeatures = new List<FeatureInfo>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osmFeatures = (from osmfeature in dbCon.osm_features
                               orderby osmfeature.FEATURE_GROUP ascending
                               where osmfeature.DELETED == 0
                             
                               select new FeatureInfo
                               {
                                   FeatureGroup = osmfeature.FEATURE_GROUP
                               }).Distinct().ToList();

                return osmFeatures;
            }
        }

        public PagedList<osm_features> GetFeaturesByGroup(string featureGroup, PagingCriteria pagingCriteria)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_features.Where(f=>f.FEATURE_GROUP == featureGroup && f.DELETED == 0).ToPagedList(pagingCriteria);
            }
        }

        public osm_features GetFeaturesById(int featureId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_features.Where(f => f.PK_FEATURE_ID == featureId).SingleOrDefault();
            }
        }

        public int SaveFeatures(osm_features features)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                //Is Exist or not  Need to implemented
                var featureList = dbCon.osm_features.Where(f=> f.FEATURE == features.FEATURE && f.DELETED == 0).ToList();
                if (featureList.Count < 1)               
                dbCon.osm_features.Add(features);
                return dbCon.SaveChanges();
            }
        }

        public int UpdateFeatures(osm_features features)
        {
            int rtnValue = 0;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                var featureList = dbCon.osm_features.Where(f => f.FEATURE_NAME == features.FEATURE_NAME && f.DELETED == 0 && f.PK_FEATURE_ID != features.PK_FEATURE_ID).ToList();
                if (featureList.Count < 1)
                {
                    var featureGroup = dbCon.osm_features.Where(f => f.PK_FEATURE_ID == features.PK_FEATURE_ID).SingleOrDefault();                                                       
                    featureGroup.FREE = features.FREE;
                    featureGroup.PREMIUM_MONTHLY = features.PREMIUM_MONTHLY;
                    featureGroup.PREMIUM_QUARTERLY = features.PREMIUM_QUARTERLY;
                    featureGroup.PREMIUM_YEARLY = features.PREMIUM_YEARLY;
                    featureGroup.PRO_MONTHLY = features.PRO_MONTHLY;
                    featureGroup.PRO_QUARTERLY = features.PRO_QUARTERLY;
                    featureGroup.PRO_YEARLY = features.PRO_YEARLY;
                     dbCon.SaveChanges();
                     rtnValue = 1;
                }
                return rtnValue;
            }
        }


        public int DeleteFeatureById(int featureId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var feature = dbCon.osm_features.Where(c => c.PK_FEATURE_ID == featureId).SingleOrDefault();
                feature.DELETED = 1;
                return dbCon.SaveChanges();
            }
        }

    
    }

} 
