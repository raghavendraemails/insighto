﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Extensions;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;

namespace Insighto.Business.Services
{
   public class HelpPageService : BusinessServiceBase
    {
       public void Save(osm_HelpPage helpPage)
       {
           using (InsightoV2Entities dbCon = new InsightoV2Entities())
           {
               dbCon.osm_HelpPage.Add(helpPage);
               dbCon.SaveChanges();
           }

       }

       public osm_HelpPage Update(osm_HelpPage helpPage)
       {
           using (InsightoV2Entities dbCon = new InsightoV2Entities())
           {
               var helpPageInfo = dbCon.osm_HelpPage.Where(h => h.PageName == helpPage.PageName).FirstOrDefault();
               helpPageInfo.Category = helpPage.Category;
               helpPageInfo.CreatedDate = helpPage.CreatedDate;
               helpPageInfo.ModifiedDate = helpPage.ModifiedDate;
               helpPageInfo.Order = helpPage.Order;
               helpPageInfo.PageContent = helpPage.PageContent;
               helpPageInfo.ParentId = helpPage.ParentId;
               dbCon.SaveChanges();
               return helpPageInfo;
           }

       }

       public osm_HelpPage GetHelpPageContent(string helpPageName)
       {
           using (InsightoV2Entities dbCon = new InsightoV2Entities())
           {
               return dbCon.osm_HelpPage.Where(h => h.PageName == helpPageName).FirstOrDefault();              
           }
       }
    }
}
