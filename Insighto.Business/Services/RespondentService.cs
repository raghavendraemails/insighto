﻿using System;
using System.Collections.Generic;
using System.Linq;
using CovalenseUtilities.Helpers;
using Insighto.Business.Enumerations;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Insighto.Business.Services
{
    public class RespondentService : BusinessServiceBase
    {
        /// <summary>
        /// Gets the survey question and answer options list.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="includeAnswerOptions">if set to <c>true</c> [include answer options].</param>
        /// <param name="includeIntroductiontext">if set to <c>true</c> [include introductiontext].</param>
        /// <returns></returns>
        public List<SurveyQuestionAndAnswerOptions> GetSurveyQuestionAndAnswerOptionsList(int surveyId, bool IsRequestFromRespondent, bool includeAnswerOptions = true, bool includeIntroductiontext = false, int pageIndex = 0)
        {
            var questionAndAnsweroptionsList = new List<SurveyQuestionAndAnswerOptions>();
            using (var dbCon = new InsightoV2Entities())
            {
                const int pageCtr = 1;
                const int ranking = 0;

                //Add survey introduction text here.
                var questions = dbCon.osm_surveyquestion.Where(q => q.SURVEY_ID == surveyId && q.DELETED == 0).ToList();
                if (includeIntroductiontext)
                    AddSurveyIntroductionText(questions, surveyId);

                //Set Pagination here
                SetPagination(questions, questionAndAnsweroptionsList, pageCtr, IsRequestFromRespondent);

                //Set questions ranking here/
                SetRanking(questionAndAnsweroptionsList, ranking);

                //Wherther answer options need to incude or not.
                IncludeAnswerOptions(dbCon, questionAndAnsweroptionsList, includeAnswerOptions);
            }
            return pageIndex > 0 ? questionAndAnsweroptionsList.Where(qa => qa.PageIndex == pageIndex).ToList()
                : questionAndAnsweroptionsList;
        }

        /// <summary>
        /// Adds the survey introduction text.
        /// </summary>
        /// <param name="lstQuestions">The LST questions.</param>
        /// <param name="surveyId">The survey id.</param>
        private static void AddSurveyIntroductionText(IList<osm_surveyquestion> lstQuestions, int surveyId)
        {
            using (var dbCon = new InsightoV2Entities())
            {
                var question = new osm_surveyquestion();
                var introductionText = dbCon.osm_surveyIntro.Where(q => q.SURVEY_ID == surveyId && q.DELETED == 0).FirstOrDefault();

                if (introductionText == null || lstQuestions == null) return;
                question.QUSETION_LABEL = introductionText.SURVEY_INTRO;
                question.PAGE_BREAK = ValidationHelper.GetInteger(introductionText.PAGE_BREAK, 0);
                question.QUESTION_TYPE_ID = -2;
                question.QUESTION_ID = new Random().Next(DateTime.UtcNow.Millisecond);

                lstQuestions.Insert(0, question);
            }
        }

        /// <summary>
        /// Includes the answer options.
        /// </summary>
        /// <param name="dbCon">The db con.</param>
        /// <param name="questionAndAnsweroptionsList">The question and answeroptions list.</param>
        /// <param name="includeAnswerOptions">if set to <c>true</c> [include answer options].</param>
        private static void IncludeAnswerOptions(InsightoV2Entities dbCon, IEnumerable<SurveyQuestionAndAnswerOptions> questionAndAnsweroptionsList, bool includeAnswerOptions)
        {
            if (!includeAnswerOptions) return;
            foreach (var question in questionAndAnsweroptionsList.ToList())
            {
                var question1 = question;
                IEnumerable<osm_answeroptions> options = dbCon.osm_answeroptions.Where(ao => ao.QUESTION_ID == question1.Question.QUESTION_ID && ao.DELETED == 0).ToList();
                question.AnswerOption = options.ToList();
            }
        }

        /// <summary>
        /// Sets the ranking.
        /// </summary>
        /// <param name="questionAndAnsweroptionsList">The question and answeroptions list.</param>
        /// <param name="ranking">The ranking.</param>
        private static void SetRanking(IList<SurveyQuestionAndAnswerOptions> questionAndAnsweroptionsList, int ranking)
        {
            for (var index = 0; index < questionAndAnsweroptionsList.ToList().Count; index++)
            {
                var questionItem = questionAndAnsweroptionsList[index];
                if (!(questionItem.QuestionType == QuestionType.Narration || questionItem.QuestionType == QuestionType.PageHeader || questionItem.QuestionType == QuestionType.QuestionHeader || questionItem.QuestionType == QuestionType.IntroductionText || questionItem.QuestionType == QuestionType.Image || questionItem.QuestionType == QuestionType.Video))
                {
                    ranking++;
                    questionItem.RankIndex = ranking;
                    continue;
                }
                questionItem.RankIndex = ranking;
            }
        }

        /// <summary>
        /// Sets the pagination.
        /// </summary>
        /// <param name="questions">The questions.</param>
        /// <param name="questionAndAnsweroptionsList">The question and answeroptions list.</param>
        /// <param name="pageCtr">The page CTR.</param>
        private static void SetPagination(IEnumerable<osm_surveyquestion> questions, ICollection<SurveyQuestionAndAnswerOptions> questionAndAnsweroptionsList, int pageCtr, bool IsRequestFromRespondent = false)
        {
            foreach (var item in questions.OrderBy(q => q.QUESTION_SEQ))
            {
                questionAndAnsweroptionsList.Add(new SurveyQuestionAndAnswerOptions
                                                     {
                                                         PageIndex = pageCtr,
                                                         Question = item
                                                     });
                
                var pageBreakType = CommonMethods.ToEnum<PageBreakType>(item.PAGE_BREAK);
                if (IsRequestFromRespondent && pageBreakType != PageBreakType.NoPageBreakWithNoSkip) //only for respondent page
                {
                    pageCtr++;
                }
                else if (!IsRequestFromRespondent && (pageBreakType == PageBreakType.PageBreak || pageBreakType == PageBreakType.PageBreakWithSkip)) // for rest like draft page, etc.
                {
                    pageCtr++;
                }
            }
        }

        /// <summary>
        /// Gets the survey view information.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <returns></returns>
        public SurveyBasicInfoView GetSurveyViewInformation(int surveyId)
        {
            using (var dbCon = new InsightoV2Entities())
            {
                return dbCon.SurveyBasicInfoViews.Where(q => q.SURVEY_ID == surveyId).FirstOrDefault();
            }
        }


        //WE WILL GET DETAILS OF THE QUIZ VIA THIS including number of correct answers(if any)
        public DataSet QuizDetails(int surveyid, int respondentid) {
            List<string> quizdetails = new List<string>();
            try
            {
                SqlConnection con = new SqlConnection();
                var effectedRecords = 0;
                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetQuizDetails", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;
                scom.Parameters.Add(new SqlParameter("@respondentId", SqlDbType.Int)).Value = respondentid;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dsquiz = new DataSet();
                sda.Fill(dsquiz);
                con.Close();
                return dsquiz;
            }
            catch (Exception ex)
            {
                return null;
            }

        }


        /// <summary>
        /// Saves the visit info.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <param name="emailId">The email id.</param>
        /// <param name="fillStatus">The fill status.</param>
        /// <param name="sessionId">The session id.</param>
        /// <param name="operationMode">The operation mode.</param>
        /// <param name="respondentId">The respondent id.</param>
        /// <param name="lastSeqNo">The last seq no.</param>
        /// <param name="launchId">The launch id.</param>
        /// <param name="isLimited"></param>
        /// <returns></returns>
        public static int SaveVisitInfo(int surveyId, int emailId, SurveyResponseStatus fillStatus, string sessionId, DbOperationType operationMode, int respondentId = 0, int? lastSeqNo = 0, int launchId = 0, bool isQuotaReached = true)
        {
            try
            {
                var effectedRecords = 0;
                osm_responsesurvey item = null;
                using (var dbCon = new InsightoV2Entities())
                {
                    switch (operationMode)
                    {
                        case DbOperationType.Insert:
                            item = new osm_responsesurvey
                                       {
                                           SURVEY_ID = surveyId,
                                           EMAIL_ID = emailId,
                                           STATUS = fillStatus.ToString(),
                                           SESSIONID = sessionId,
                                           CREATED_ON = DateTime.UtcNow,
                                           EMAIL_FLAG = 0,
                                           VISABLE_LIMIT = 0,
                                           RANDOMSESSIONID = string.Empty,
                                           LASTQUESNO = lastSeqNo,
                                           FLAG_COMPLETESTATUS = 0
                                       };
                            dbCon.osm_responsesurvey.Add(item);
                            break;

                        case DbOperationType.UpdateStatus:
                            item = dbCon.osm_responsesurvey.Where(rs => rs.RESPONDENT_ID == respondentId).FirstOrDefault();
                            item.STATUS = fillStatus.ToString();

                            item.VISABLE_LIMIT = fillStatus != SurveyResponseStatus.ScreenOut && isQuotaReached ? 1 : 0;
                            item.LASTQUESNO = lastSeqNo;
                            item.FLAG_COMPLETESTATUS = 0;

                            if (emailId > 0 && launchId > 0 && fillStatus == SurveyResponseStatus.Complete)
                            {
                                var launch = dbCon.osm_surveylaunch.Where(ln => ln.LAUNCH_ID == launchId).FirstOrDefault();
                                launch.COMPLETES = launch.COMPLETES + 1;
                            }
                            else if (emailId > 0 && launchId == 0 && fillStatus == SurveyResponseStatus.Complete)
                            {
                                var launch = dbCon.osm_surveylaunch.Where(ln => ln.LAUNCH_ID == launchId && ln.SURVEY_ID == surveyId && ln.CONTACTLIST_ID > 0).FirstOrDefault();
                                launch.COMPLETES = launch.COMPLETES + 1;
                            }

                            if (fillStatus == SurveyResponseStatus.ScreenOut)
                            {
                                var controls = dbCon.osm_surveycontrols.Where(ln => ln.SURVEY_ID == surveyId && ln.DELETED == 0).FirstOrDefault();
                                controls.SCREEN_OUTS = controls.SCREEN_OUTS + 1;
                                controls.TOTAL_RESPONSES = dbCon.osm_responsesurvey.Where(rs => rs.SURVEY_ID == surveyId).ToList().Count;
                            }
                            break;

                        case DbOperationType.Update:
                            item = dbCon.osm_responsesurvey.Where(rs => rs.RESPONDENT_ID == respondentId).FirstOrDefault();
                            item.SURVEY_ID = surveyId;
                            item.EMAIL_ID = emailId;
                            item.STATUS = fillStatus.ToString();
                            item.SESSIONID = sessionId;
                            break;
                    }

                    if (item != null)
                    {
                        dbCon.SaveChanges();
                        effectedRecords = item.RESPONDENT_ID;

                    }
                }
                return effectedRecords;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public SqlConnection strconnectionstring()
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection strconn = new SqlConnection(connStr);

                strconn.Open();
                return strconn;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Save Embed

        public int updateembed(int respondentId, string embed)
        {
            try
            {
                SqlConnection con = new SqlConnection();
                var effectedRecords = 0;
                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_updateembed", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@respondentId", SqlDbType.Int)).Value = respondentId;
                scom.Parameters.Add(new SqlParameter("@referel", SqlDbType.VarChar)).Value = embed;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dsupdatesurveylaunch = new DataSet();
                sda.Fill(dsupdatesurveylaunch);
                con.Close();
                return effectedRecords = respondentId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Saves the response.
        /// </summary>
        /// <param name="lstResponse">The LST response.</param>
        /// <param name="isInsert">if set to <c>true</c> [is insert].</param>
        /// <returns></returns>
        public int Save(List<osm_responsequestions> lstResponse, bool isInsert, bool deleteAll = false)
        {
            try
            {
                //DeleteRecords(lstResponse, deleteAll);
                var effectedRecords = 0;
                using (var dbCon = new InsightoV2Entities())
                {


                   foreach (var item in lstResponse)
                    {

                        var itemtype = dbCon.osm_responsesurvey.Where(rs => rs.RESPONDENT_ID == item.RESPONDENT_ID).FirstOrDefault();

                        var questiontype = dbCon.osm_surveyquestion.Where(q => q.SURVEY_ID == itemtype.SURVEY_ID && q.QUESTION_ID==item.QUESTION_ID &&  q.DELETED == 0).FirstOrDefault();

                        if (item == null || item.RESPONDENT_ID == 0)
                            continue;

                        var item1 = item;


//                       1,3,4,5,6,7,8,14 - SA
//2,9,10,11,12,13,15,20 - MA

                        if ((questiontype.QUESTION_TYPE_ID == 1)||(questiontype.QUESTION_TYPE_ID == 3)||(questiontype.QUESTION_TYPE_ID == 4)||(questiontype.QUESTION_TYPE_ID == 5)||(questiontype.QUESTION_TYPE_ID == 6)||(questiontype.QUESTION_TYPE_ID == 7)||(questiontype.QUESTION_TYPE_ID == 8)||(questiontype.QUESTION_TYPE_ID == 14))
                        {
                            var responseItemcount =  dbCon.osm_responsequestions.Where(rq => rq.RESPONDENT_ID == item1.RESPONDENT_ID && rq.QUESTION_ID == item1.QUESTION_ID);

                            if (responseItemcount.Count() == 0)
                            {
                                dbCon.osm_responsequestions.Add(item);
                            }
                            else
                            {
                                var responseItem =
                            dbCon.osm_responsequestions.Where(
                                rq =>
                                rq.RESPONDENT_ID == item1.RESPONDENT_ID && rq.QUESTION_ID == item1.QUESTION_ID).FirstOrDefault();

                                responseItem.ANSWER_ID = item1.ANSWER_ID;
                                responseItem.ANSWER_TEXT = item1.ANSWER_TEXT;
                                responseItem.RESPONSE_RECEIVED_ON = DateTime.UtcNow;
                            }
                            effectedRecords = dbCon.SaveChanges();
                        }
                        else if ((questiontype.QUESTION_TYPE_ID == 2)||(questiontype.QUESTION_TYPE_ID == 9)||(questiontype.QUESTION_TYPE_ID == 10)||(questiontype.QUESTION_TYPE_ID == 11)||(questiontype.QUESTION_TYPE_ID == 12)||(questiontype.QUESTION_TYPE_ID == 13)||(questiontype.QUESTION_TYPE_ID == 15)||(questiontype.QUESTION_TYPE_ID == 20))
                        {
                            var responseItemcount = dbCon.osm_responsequestions.Where(rq => rq.RESPONDENT_ID == item1.RESPONDENT_ID && rq.QUESTION_ID == item1.QUESTION_ID
                                                    && rq.ANSWER_ID == item1.ANSWER_ID);

                            if (responseItemcount.Count() == 0)
                            {
                                dbCon.osm_responsequestions.Add(item);
                                effectedRecords = dbCon.SaveChanges();
                            }
                            else
                            {

                                   var responseItem =
                                    dbCon.osm_responsequestions.Where(
                                        rq =>
                                        rq.RESPONDENT_ID == item1.RESPONDENT_ID && rq.QUESTION_ID == item1.QUESTION_ID && rq.ANSWER_ID == item1.ANSWER_ID).FirstOrDefault();

                                
                                        responseItem.ANSWER_ID = item1.ANSWER_ID;
                                        responseItem.ANSWER_TEXT = item1.ANSWER_TEXT;
                                        responseItem.DELETED = 0;
                                        responseItem.RESPONSE_RECEIVED_ON = DateTime.UtcNow;
                                        effectedRecords = dbCon.SaveChanges();


                                        var responseItemcl = dbCon.osm_responsequestions.Where(rq =>
                                      rq.RESPONDENT_ID == item1.RESPONDENT_ID && rq.QUESTION_ID == item1.QUESTION_ID).ToList();



                                        HashSet<int> sentIDs = new HashSet<int>(lstResponse.Select(s => s.ANSWER_ID));

                                        var results = responseItemcl.Where(m => !sentIDs.Contains(m.ANSWER_ID)).ToList();

                                        foreach (var updel in results)
                                        {
                                            var responseItemdel =
                                   dbCon.osm_responsequestions.Where(
                                       rq =>
                                       rq.RESPONDENT_ID == updel.RESPONDENT_ID && rq.QUESTION_ID == updel.QUESTION_ID && rq.ANSWER_ID == updel.ANSWER_ID).FirstOrDefault();

                                            responseItemdel.DELETED = 1;
                                            effectedRecords = dbCon.SaveChanges();
                                        }
                                      

                         
                            }
                            
                        }

                        //var responseItem =
                        //    dbCon.osm_responsequestions.Where(
                        //        rq =>
                        //        rq.RESPONDENT_ID == item1.RESPONDENT_ID && rq.QUESTION_ID == item1.QUESTION_ID &&
                        //        rq.ANSWER_ID == item1.ANSWER_ID).FirstOrDefault();
                        //if (responseItem != null)
                        //{
                        //    responseItem.ANSWER_ID = item.ANSWER_ID;
                        //    responseItem.ANSWER_TEXT = item.ANSWER_TEXT;
                        //    responseItem.RESPONSE_RECEIVED_ON = DateTime.UtcNow;
                        //}
                        //else
                        //    dbCon.osm_responsequestions.Add(item);

                       // effectedRecords = dbCon.SaveChanges();
                    }
                }
                return effectedRecords;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        /// <summary>
        /// Deletes the records.
        /// </summary>
        /// <param name="lstResponse">The LST response.</param>
        public void DeleteRecords(IEnumerable<osm_responsequestions> lstResponse, bool deleteAll)
        {
            using (var dbCon = new InsightoV2Entities())
            {
                foreach (var item in lstResponse)
                {
                    if (item == null || item.RESPONDENT_ID == 0)
                        continue;


                    var item1 = item;
                    var responseItems = deleteAll == true ?
                        dbCon.osm_responsequestions.Where(rq => rq.RESPONDENT_ID == item1.RESPONDENT_ID && rq.QUESTION_ID == item1.QUESTION_ID && rq.ANSWER_ID == item1.ANSWER_ID)
                        : dbCon.osm_responsequestions.Where(rq => rq.RESPONDENT_ID == item1.RESPONDENT_ID && rq.QUESTION_ID == item1.QUESTION_ID);
                    foreach (var deletedItem in responseItems.ToList())
                    {
                        dbCon.osm_responsequestions.Remove(deletedItem);
                        dbCon.SaveChanges();
                    }
                }
            }
        }

        public void DeleteRecords(int respondentId)
        {
            if (respondentId <= 0)
                return;

            using (var dbCon = new InsightoV2Entities())
            {
                var responseItems = dbCon.osm_responsequestions.Where(
                        rq => rq.RESPONDENT_ID == respondentId);
                foreach (var deletedItem in responseItems.ToList())
                {
                    dbCon.osm_responsequestions.Remove(deletedItem);
                    dbCon.SaveChanges();
                }

            }
        }

        /// <summary>
        /// Gets the no of responses.
        /// </summary>
        /// <param name="userType">Type of the user.</param>
        /// <returns></returns>
        public static double GetNoOfResponses(UserType userType)
        {
            var featureInfo = ServiceFactory<FeatureService>.Instance.Find(userType, FeatureSurveyCreation.NO_OF_RESPONSES);
            var noOfResponses = featureInfo.Value >= 0
                                    ? Convert.ToDouble(featureInfo.Value)
                                    : float.PositiveInfinity;

            return noOfResponses;

        }

        /// <summary>
        /// Saves the respondent data.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <param name="emailId">The email id.</param>
        /// <param name="fillStatus">The fill status.</param>
        /// <param name="sessionId">The session id.</param>
        /// <param name="operationMode">The operation mode.</param>
        /// <param name="respondentId">The respondent id.</param>
        /// <param name="lastSeqNo">The last seq no.</param>
        /// <param name="launchId">The launch id.</param>
        /// <param name="isQuotaReached">if set to <c>true</c> [is quota reached].</param>
        /// <returns></returns>
        public int SaveRespondentData(int surveyId, int emailId, SurveyResponseStatus fillStatus, string sessionId, DbOperationType operationMode, int respondentId = 0, int? lastSeqNo = 0, int launchId = 0, bool isQuotaReached = true)
        {
            return SaveVisitInfo(surveyId, emailId, fillStatus, sessionId, operationMode, respondentId, lastSeqNo,
                                 launchId, isQuotaReached);
        }

        /// <summary>
        /// Gets the response count.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <returns></returns>
        public int GetResponseCount(int surveyId)
        {
            using (var dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_responsesurvey.Where(rs => rs.SURVEY_ID == surveyId && rs.STATUS == "Complete").ToList().Count;
            }
        }

        /// <summary>
        /// Gets the survey launch details.
        /// </summary>
        /// <param name="SurveyId">The survey id.</param>
        /// <returns></returns>
        public List<osm_surveylaunch> GetSurveyLaunchDetails(int surveyId)
        {
            using (var dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_surveylaunch.Where(rs => rs.SURVEY_ID == surveyId).ToList();
            }
        }

        /// <summary>
        /// Determines whether [is user filled survey complete] [the specified survey id].
        /// </summary>
        /// <param name="SurveyId">The survey id.</param>
        /// <param name="EmailId">The email id.</param>
        /// <param name="LaunchId">The launch id.</param>
        /// <returns>
        ///   <c>true</c> if [is user filled survey complete] [the specified survey id]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsUserFilledSurveyComplete(int SurveyId, int EmailId, int LaunchId)
        {
            using (var dbCon = new InsightoV2Entities())
            {
                var responses = dbCon.osm_responsesurvey.Where(rs => rs.STATUS == "Complete" && rs.EMAIL_ID == EmailId && rs.SURVEY_ID == SurveyId).ToList();
                return (responses != null && responses.Count > 0);
            }
        }
    }
}