﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using Insighto.Business.Extensions;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;


namespace Insighto.Business.Services
{
    public class OrderInfoService : BusinessServiceBase
    {
        public int SurveyFlag
        {
            get
            {
                var key = QueryStringHelper.GetString("key");
                if (String.IsNullOrEmpty(key))
                    return 0;

                Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
                if (ht != null && ht.Contains("surveyFlag"))
                {
                    return ValidationHelper.GetInteger(ht["surveyFlag"], 0);
                }
                return 0;
            }

        }
        #region "Method: GetOrderInofByUserId"
        public PagedList<OrderInfo> GetOrderInofByUserId(int UserId, PagingCriteria pagingCriteria)
        {
            var listOrderInfo = new List<OrderInfo>();
            var pagedListOrderInfo = new PagedList<OrderInfo>();
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();

            
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                DataSet dsinv = getInvoiceInfo(UserId);
                DataTable dtinv = dsinv.Tables[0];

                var results = from order1 in  dtinv.AsEnumerable()
	                          select order1;

                var orderDetails = from order in results.AsQueryable()
                           select new
                           {
                                ORDERID = order.Field<string>("ORDERID"),
                                PK_ORDERID = order.Field<Int32>("PK_ORDERID"),
                                Product = order.Field<string>("Product"),
                                PAID_DATE = order.Field<DateTime>("PAID_DATE"),
                                LICENSE_EXPIRY_DATE = order.Field<DateTime>("LICENSE_EXPIRY_DATE"),
                                TOTAL_PRICE = order.Field<double>("TOTAL_PRICE"),
                                TRANSACTION_STATUS = order.Field<Int32>("TRANSACTION_STATUS"),
                                USERID = order.Field<Int32>("USERID"),
                                CURRENCYTYPE = order.Field<string>("CURRENCYTYPE")

                           };
             
               

                //var orderDetails = from order in dbCon.osm_orderinfo
                //                   where order.DELETED == 0 && order.USERID == UserId
                //                   from user in dbCon.osm_user.Where(user => user.USERID == order.USERID).DefaultIfEmpty()
                //                   select new
                //                   {
                //                       order.ORDERID,
                //                       order.PK_ORDERID,
                //                       order.TOTAL_PRICE,
                //                       order.PAID_DATE,
                //                       user.LICENSE_EXPIRY_DATE,
                //                       order.TRANSACTION_STATUS,
                //                       order.USERID
                //                       //history.LICENSE_STARTDATE,
                //                       // history.LICENSE_ENDDATE
                //                   };

                       

                var orderPagedList = orderDetails.ToPagedList(pagingCriteria);
                var licenseDetails = ServiceFactory<UsersService>.Instance.GetLicenseDetailsDateUserId(UserId);
                int i = 0;


                foreach (var order in orderPagedList.Entities)
                {
                    var orderInfo = new OrderInfo();
                    
                    if (order.TRANSACTION_STATUS == 1)
                    {
                        DateTime PaidDate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(order.PAID_DATE), userDetails.StandardBIAS, true);
                        DateTime LicenseExpiryDate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(order.LICENSE_EXPIRY_DATE), userDetails.StandardBIAS, true);
                       orderInfo.Range = Convert.ToDateTime(licenseDetails[i].LICENSE_STARTDATE).ToString("dd-MMM-yyyy") + " to " + Convert.ToDateTime(licenseDetails[i].LICENSE_ENDDATE).ToString("dd-MMM-yyy");
                       //i++;
                    }
                    else
                    {
                        orderInfo.Range = "";
                    }
                    orderInfo.PK_ORDERID = order.PK_ORDERID;
                    orderInfo.ORDERID = order.ORDERID;
                    orderInfo.TOTAL_PRICE = order.TOTAL_PRICE;
                    if (order.CURRENCYTYPE == "USD")
                    {
                        orderInfo.Amount = "$" + Convert.ToString(orderInfo.TOTAL_PRICE);
                    }
                    else if (order.CURRENCYTYPE == "INR")
                    {
                        orderInfo.Amount = "&#8377;" + Convert.ToString(orderInfo.TOTAL_PRICE);
                    }
                    orderInfo.PaidDate = Convert.ToDateTime(order.PAID_DATE).ToString("dd-MMM-yyyy");
                    orderInfo.Product = order.Product;
                    orderInfo.LICENSEEXPIRYDATE = Convert.ToDateTime(order.LICENSE_EXPIRY_DATE).ToString("dd-MMM-yyyy"); 
                   // orderInfo.Url = EncryptHelper.EncryptQuerystring(PathHelper.GetInvoiceUrl().Replace("~/", ""), "UserId=" + order.USERID + "&OrderId=" + order.PK_ORDERID + "&HistoryId=" + licenseDetails[i].PK_ACCOUNTID) + "~" + EncryptHelper.EncryptQuerystring(PathHelper.GetInvoiceUrl().Replace("~/", ""), "UserId=" + order.USERID + "&OrderId=" + order.PK_ORDERID + "&Print=1&surveyFlag=" + SurveyFlag + "&HistoryId=" + licenseDetails[i].PK_ACCOUNTID);

                    DataSet dssaasyrefno = getSaasyorder(orderInfo.PK_ORDERID);
                    if (dssaasyrefno.Tables[0].Rows[0][0].ToString() == "")
                    {
                        orderInfo.Url = EncryptHelper.EncryptQuerystring(PathHelper.GetInvoiceUrl().Replace("~/", ""), "UserId=" + order.USERID + "&OrderId=" + order.PK_ORDERID + "&HistoryId=" + licenseDetails[i].PK_ACCOUNTID) + "~" + EncryptHelper.EncryptQuerystring(PathHelper.GetInvoiceUrl().Replace("~/", ""), "UserId=" + order.USERID + "&OrderId=" + order.PK_ORDERID + "&Print=1&surveyFlag=" + SurveyFlag + "&HistoryId=" + licenseDetails[i].PK_ACCOUNTID);
                        
                    }
                    else
                    {
                        orderInfo.Url = "https://sites.fastspring.com/insighto/order/invoice/" + dssaasyrefno.Tables[0].Rows[0][0].ToString() + "/pdf";
                    }
                  //  orderInfo.Url = "https://sites.fastspring.com/insighto/order/invoice/KNO131203-4714-31106/pdf";

                    listOrderInfo.Add(orderInfo);
                    i++;
                }
                pagedListOrderInfo.Entities = listOrderInfo;
                pagedListOrderInfo.HasNext = orderPagedList.HasNext;
                pagedListOrderInfo.HasPrevious = orderPagedList.HasPrevious;
                pagedListOrderInfo.TotalCount = orderPagedList.TotalCount;
                pagedListOrderInfo.TotalPages = orderPagedList.TotalPages;
            }
            return pagedListOrderInfo;
        }
        #endregion


        #region "Method: GetPollOrderInofByUserId"
        public PagedList<OrderInfo> GetPollOrderInofByUserId(int UserId, PagingCriteria pagingCriteria)
        {
            var listOrderInfo = new List<OrderInfo>();
            var pagedListOrderInfo = new PagedList<OrderInfo>();
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();


            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                DataSet dsinv = getPollInvoiceInfo(UserId);
                DataTable dtinv = dsinv.Tables[0];

                var results = from order1 in dtinv.AsEnumerable()
                              select order1;

                var orderDetails = from order in results.AsQueryable()
                                   select new
                                   {
                                       ORDERID = order.Field<string>("ORDERID"),
                                       PK_ORDERID = order.Field<Int32>("PK_ORDERID"),
                                       Product = order.Field<string>("Product"),
                                       PAID_DATE = order.Field<DateTime>("PAID_DATE"),
                                       LICENSE_EXPIRY_DATE = order.Field<DateTime>("LICENSE_EXPIRY_DATE"),
                                       TOTAL_PRICE = order.Field<double>("TOTAL_PRICE"),
                                       TRANSACTION_STATUS = order.Field<Int32>("TRANSACTION_STATUS"),
                                       USERID = order.Field<Int32>("USERID"),
                                       CURRENCYTYPE = order.Field<string>("CURRENCYTYPE")

                                   };

                
                var orderPagedList = orderDetails.ToPagedList(pagingCriteria);

               // DataTable dtuserhist = dsinv.Tables[1];
               // var resultsuserhist = from order1hist in dtuserhist.AsEnumerable()
               //                       select order1hist;

               // var licenseDetails1 = from orderhist in resultsuserhist.AsQueryable()
               //                    select new
               //                    {
               //                        PK_ACCOUNTID = orderhist.Field<Int32>("PK_ACCOUNTID"),
               //                        USERID = orderhist.Field<Int32>("USERID"),
               //                        ORDERID = orderhist.Field<string>("ORDERID"),
               //                        LICENSE_STARTDATE = orderhist.Field<DateTime>("LICENSE_STARTDATE"),
               //                        LICENSE_ENDDATE = orderhist.Field<DateTime>("LICENSE_ENDDATE")
               //                    };

               //var licenseDetails = ServiceFactory<UsersService>.Instance.GetLicenseDetailsDateUserId(UserId);
                int i = 0;
                

                foreach (var order in orderPagedList.Entities)
                {
                    var orderInfo = new OrderInfo();

                    if (order.TRANSACTION_STATUS == 1)
                    {
                        DateTime PaidDate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(order.PAID_DATE), userDetails.StandardBIAS, true);
                        DateTime LicenseExpiryDate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(order.LICENSE_EXPIRY_DATE), userDetails.StandardBIAS, true);
                      //  orderInfo.Range = Convert.ToDateTime(licenseDetails[i].LICENSE_STARTDATE).ToString("dd-MMM-yyyy") + " to " + Convert.ToDateTime(licenseDetails[i].LICENSE_ENDDATE).ToString("dd-MMM-yyy");
                        orderInfo.Range = Convert.ToDateTime(dsinv.Tables[1].Rows[0]["LICENSE_STARTDATE"].ToString()).ToString("dd-MMM-yyyy") + " to " + Convert.ToDateTime(dsinv.Tables[1].Rows[0]["LICENSE_ENDDATE"].ToString()).ToString("dd-MMM-yyy");
                        //i++;
                    }
                    else
                    {
                        orderInfo.Range = "";
                    }
                    var invoiceid = "";
                    string licensetype = order.Product;
                    invoiceid = order.ORDERID;
                    if (licensetype.ToUpper().Contains("PROFESSIONAL"))
                    {
                        invoiceid = invoiceid.Replace("POLL", "POLL-PRO");
                    }
                    else if (licensetype.ToUpper().Contains("BUSINESS"))
                    {
                        invoiceid = invoiceid.Replace("POLL", "POLL-BUS");
                    }
                    else if (licensetype.ToUpper().Contains("PUBLISHER"))
                    {
                        invoiceid = invoiceid.Replace("POLL", "POLL-PUB");
                    }

                    orderInfo.PK_ORDERID = order.PK_ORDERID;
                    //orderInfo.ORDERID = order.ORDERID;
                    orderInfo.ORDERID = invoiceid;
                    orderInfo.TOTAL_PRICE = order.TOTAL_PRICE;
                    if (order.CURRENCYTYPE == "USD")
                    {
                        orderInfo.Amount = "$" + Convert.ToString(orderInfo.TOTAL_PRICE);
                    }
                    else if (order.CURRENCYTYPE == "INR")
                    {
                        orderInfo.Amount = "&#8377;" + Convert.ToString(orderInfo.TOTAL_PRICE);
                    }
                    DateTime paiddate = new DateTime();
                    DateTime expirydate = new DateTime();
                    paiddate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(order.PAID_DATE), userDetails.StandardBIAS, true);
                    if (licensetype.ToLower().Contains("monthly"))
                    {
                        expirydate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(order.PAID_DATE), userDetails.StandardBIAS, true).AddMonths(1);
                    }
                    else if (licensetype.ToLower().Contains("yearly"))
                    {
                        expirydate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(order.PAID_DATE), userDetails.StandardBIAS, true).AddYears(1);
                    }
                    orderInfo.PaidDate = paiddate.ToString("dd-MMM-yyyy");
                    orderInfo.Product = licensetype;
                    orderInfo.LICENSEEXPIRYDATE = expirydate.ToString("dd-MMM-yyyy");
                    // orderInfo.Url = EncryptHelper.EncryptQuerystring(PathHelper.GetInvoiceUrl().Replace("~/", ""), "UserId=" + order.USERID + "&OrderId=" + order.PK_ORDERID + "&HistoryId=" + licenseDetails[i].PK_ACCOUNTID) + "~" + EncryptHelper.EncryptQuerystring(PathHelper.GetInvoiceUrl().Replace("~/", ""), "UserId=" + order.USERID + "&OrderId=" + order.PK_ORDERID + "&Print=1&surveyFlag=" + SurveyFlag + "&HistoryId=" + licenseDetails[i].PK_ACCOUNTID);

                    DataSet dssaasyrefno = getPollSaasyorder(orderInfo.PK_ORDERID);
                    if (dssaasyrefno.Tables[0].Rows[0][0].ToString() == "")
                    {
                        orderInfo.Url = EncryptHelper.EncryptQuerystring(PathHelper.GetPollsInvoiceUrl().Replace("~/", ""), "UserId=" + order.USERID + "&OrderId=" + order.PK_ORDERID + "&HistoryId=" + dsinv.Tables[1].Rows[0]["PK_ACCOUNTID"]) + "~" + EncryptHelper.EncryptQuerystring(PathHelper.GetInvoiceUrl().Replace("~/", ""), "UserId=" + order.USERID + "&OrderId=" + order.PK_ORDERID + "&Print=1&surveyFlag=" + SurveyFlag + "&HistoryId=" + dsinv.Tables[1].Rows[0]["PK_ACCOUNTID"]);
                    }
                    else
                    {
                        orderInfo.Url = "https://sites.fastspring.com/insighto/order/invoice/" + dssaasyrefno.Tables[0].Rows[0][0].ToString() + "/pdf";
                    }

                    listOrderInfo.Add(orderInfo);
                    i++;
                }
                pagedListOrderInfo.Entities = listOrderInfo;
                pagedListOrderInfo.HasNext = orderPagedList.HasNext;
                pagedListOrderInfo.HasPrevious = orderPagedList.HasPrevious;
                pagedListOrderInfo.TotalCount = orderPagedList.TotalCount;
                pagedListOrderInfo.TotalPages = orderPagedList.TotalPages;
            }
            return pagedListOrderInfo;
        }
        #endregion

        public DataSet getSaasyorder(int orderid)
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection con = new SqlConnection(connStr);
                SqlCommand scom = new SqlCommand("sp_GetSaasyOrderRef", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@pkorderID", SqlDbType.Int)).Value = orderid;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dssaasyorder = new DataSet();
                sda.Fill(dssaasyorder);
                con.Close();
                return dssaasyorder;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet getPollSaasyorder(int orderid)
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection con = new SqlConnection(connStr);
                SqlCommand scom = new SqlCommand("sp_GetPollSaasyOrderRef", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@pkorderID", SqlDbType.Int)).Value = orderid;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dssaasyorder = new DataSet();
                sda.Fill(dssaasyorder);
                con.Close();
                return dssaasyorder;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet getInvoiceInfo(int userid)
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection con = new SqlConnection(connStr);
                SqlCommand scom = new SqlCommand("sp_GetInvoiceInfo", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dssaasyorder = new DataSet();
                sda.Fill(dssaasyorder);
                con.Close();
                return dssaasyorder;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet getPollInvoiceInfo(int userid)
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection con = new SqlConnection(connStr);
                SqlCommand scom = new SqlCommand("sp_GetPollInvoiceInfo", con);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dssaasyorder = new DataSet();
                sda.Fill(dssaasyorder);
                con.Close();
                return dssaasyorder;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region "Method : GetOrderInfo"
        /// <summary>
        /// Used To Get Authenticated User Details
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>

        public List<osm_orderinfo> GetOrderInfo(int pkOrderId, int userId)
        {
            var orderInfo = new List<osm_orderinfo>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_orderinfo.Where(order => order.PK_ORDERID == pkOrderId && order.USERID == userId).ToList();
            }

        }
        #endregion

        #region "Method : GetOrderInfoByOrderId"
        /// <summary>
        /// Used To Get Authenticated User Details By OrderId
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>

        public List<osm_orderinfo> GetOrderInfoByOrderId(string orderId)
        {
            var orderInfo = new List<osm_orderinfo>();

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_orderinfo.Where(order => order.ORDERID == orderId && order.DELETED == 0).ToList();
            }

        }
        #endregion

        public PagedList<OrderInfo> FindPendingInvoices(PagingCriteria pagingCriteria, string orderType)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (orderType == "Renewal")
                {
                    return (from user in dbCon.osm_user
                            join oInfo in dbCon.osm_orderinfo on
                            user.USERID equals oInfo.USERID
                            where oInfo.TRANSACTION_STATUS == 0 && (oInfo.MODEOFPAYMENT == 2 ||oInfo.MODEOFPAYMENT == 4 )  && (oInfo.ORDER_TYPE == "U" || oInfo.ORDER_TYPE == "R")
                            select new OrderInfo
                            {
                                USERID = user.USERID,
                                LOGIN_NAME = user.LOGIN_NAME,
                                FIRST_NAME = user.FIRST_NAME,
                                LAST_NAME = user.LAST_NAME,
                                PHONE = user.PHONE,
                                CREATED_ON = user.CREATED_ON,
                                ACCOUNT_TYPE = user.ACCOUNT_TYPE,
                                PK_ORDERID = oInfo.PK_ORDERID,
                                ORDERID = oInfo.ORDERID,
                                CUSTOMER_ID = user.CUSTOMER_ID,
                                STATUS = user.STATUS,
                                LICENSE_OPTED = user.LICENSE_OPTED,
                                LICENSE_TYPE = user.LICENSE_TYPE,

                            }
                                    ).ToPagedList(pagingCriteria);
                }
                else
                {
                    return (from user in dbCon.osm_user
                            join oInfo in dbCon.osm_orderinfo on
                            user.USERID equals oInfo.USERID
                            where user.APPROVAL_FLAG == 0 && oInfo.TRANSACTION_STATUS == 0 && (oInfo.MODEOFPAYMENT == 2 || oInfo.MODEOFPAYMENT == 4) && oInfo.ORDER_TYPE == "N"
                            select new OrderInfo
                            {
                                USERID = user.USERID,
                                LOGIN_NAME = user.LOGIN_NAME,
                                FIRST_NAME = user.FIRST_NAME,
                                LAST_NAME = user.LAST_NAME,
                                PHONE = user.PHONE,
                                CREATED_ON = user.CREATED_ON,
                                ACCOUNT_TYPE = user.ACCOUNT_TYPE,
                                PK_ORDERID = oInfo.PK_ORDERID,
                                ORDERID = oInfo.ORDERID,
                                CUSTOMER_ID = user.CUSTOMER_ID,
                                STATUS = user.STATUS,
                                LICENSE_OPTED = user.LICENSE_OPTED,
                                LICENSE_TYPE = user.LICENSE_TYPE
                            }
                                       ).ToPagedList(pagingCriteria);

                }
            }
        }

        public OrderInfo FindOrderInfoByOrderId(int pkOrderId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return (from user in dbCon.osm_user
                        join oInfo in dbCon.osm_orderinfo on
                        user.USERID equals oInfo.USERID
                        where oInfo.PK_ORDERID == pkOrderId && oInfo.TRANSACTION_STATUS == 0 && (oInfo.MODEOFPAYMENT == 2 || oInfo.MODEOFPAYMENT == 4) && (oInfo.ORDER_TYPE == "U" || oInfo.ORDER_TYPE == "R" || oInfo.ORDER_TYPE == "N")
                        select new OrderInfo
                        {
                            USERID = user.USERID,
                            LOGIN_NAME = user.LOGIN_NAME,
                            FIRST_NAME = user.FIRST_NAME,
                            LAST_NAME = user.LAST_NAME,
                            PHONE = user.PHONE,
                            CREATED_ON = user.CREATED_ON,
                            ACCOUNT_TYPE = user.ACCOUNT_TYPE,
                            PK_ORDERID = oInfo.PK_ORDERID,
                            ORDERID = oInfo.ORDERID,
                            CUSTOMER_ID = user.CUSTOMER_ID,
                            STATUS = user.STATUS,
                            LICENSE_OPTED = user.LICENSE_OPTED,
                            LICENSE_TYPE = user.LICENSE_TYPE,
                            LICENSE_EXPIRY_DATE = user.LICENSE_EXPIRY_DATE
                        }
                        ).SingleOrDefault();
            }

        }

        public int UpgradeUsers(osm_user user, osm_orderinfo orderInfo, osm_useraccounthistory accountHistory, int userId, int orderId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                string order = orderId.ToString();
                var userInfo = dbCon.osm_user.Where(u => u.USERID == userId).SingleOrDefault();
                var ordInfo = dbCon.osm_orderinfo.Where(o => o.PK_ORDERID == orderId).SingleOrDefault();
                var accountInfo = dbCon.osm_useraccounthistory.Where(a => a.ORDERID == order).SingleOrDefault();
                if (user.LICENSE_EXPIRY_DATE != null)
                    userInfo.LICENSE_EXPIRY_DATE = Convert.ToDateTime(user.LICENSE_EXPIRY_DATE);
                if (user.LICENSE_TYPE != null)
                    userInfo.LICENSE_TYPE = user.LICENSE_TYPE;
                if (user.LICENSE_CHANGEFLAG != null)
                    userInfo.LICENSE_CHANGEFLAG = user.LICENSE_CHANGEFLAG;
                if (user.EMAIL_COUNT != null)
                    userInfo.EMAIL_COUNT = user.EMAIL_COUNT;
                if (user.LICENSETYPE_PENDING != null)
                    userInfo.LICENSETYPE_PENDING = user.LICENSETYPE_PENDING;
                if (user.LICENSEPENDING_STARTDATE != null)
                    userInfo.LICENSEPENDING_STARTDATE = user.LICENSEPENDING_STARTDATE;

                userInfo.ACTIVATION_FLAG = user.ACTIVATION_FLAG;
                userInfo.EMAIL_FLAG = user.EMAIL_FLAG;
                userInfo.RENEWAL_ALERT = user.RENEWAL_ALERT;
                userInfo.RENEWAL_FLAG = user.RENEWAL_FLAG;

                ordInfo.PAID_DATE = DateTime.UtcNow;
                ordInfo.TRANSACTION_STATUS = 1;
                //ordInfo.PK_ORDERID = orderInfo.PK_ORDERID;

                accountInfo.LICENSE_ENDDATE = accountHistory.LICENSE_ENDDATE;
                accountInfo.LICENSE_STARTDATE = accountHistory.LICENSE_STARTDATE;
                accountInfo.LICENSE_TYPEFLAG = "0";
                accountInfo.EMAIL_FLAG = 1;
                if (accountHistory == null)
                {
                    dbCon.osm_useraccounthistory.Add(accountInfo);
                }


                return dbCon.SaveChanges();
            }

        }

        public List<osm_orderinfo> GetUserIdByOrderId(int orderId)
        {
            var orderInfo = new List<osm_orderinfo>();

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_orderinfo.Where(order => order.PK_ORDERID == orderId && order.DELETED == 0).ToList();
            }

        }

        public string UpgradeLicense(int orderId)
        {
            var orderInfoService = ServiceFactory.GetService<OrderInfoService>();
            var orderInfo = orderInfoService.FindOrderInfoByOrderId(orderId);

            string lic_tobe_update = "", curr_lic = "";
            DateTime dt = DateTime.UtcNow;
            DateTime dt_now = dt;
            int userId = 0, activ_flag = 0;
            string ord_type = "N";
            if (orderInfo != null)
            {
                lic_tobe_update = orderInfo.LICENSE_OPTED;
                userId = orderInfo.USERID;
                dt = Convert.ToDateTime(orderInfo.LICENSE_EXPIRY_DATE);
                curr_lic = orderInfo.LICENSE_TYPE;
                ord_type = orderInfo.ORDER_TYPE;
                activ_flag = ValidationHelper.GetInteger(orderInfo.ACTIVATION_FLAG, 0);

            }
            DateTime dt_expiry = DateTime.UtcNow;
            if (dt != Convert.ToDateTime("1/1/1800"))
            {
                dt_expiry = dt;

            }
            if (dt_now == Convert.ToDateTime("1/1/1800"))
            {
                dt_now = DateTime.UtcNow;

            }
            if (curr_lic != "FREE")
            {

                dt_now = dt;
            }

            if (lic_tobe_update == "PRO_MONTHLY")
                dt_expiry = dt_expiry.AddMonths(1);
            else if (lic_tobe_update == "PRO_QUARTERLY")
                dt_expiry = dt_expiry.AddMonths(3);
            else if (lic_tobe_update == "PRO_YEARLY")
                dt_expiry = dt_expiry.AddMonths(12);
            else if (lic_tobe_update == "PREMIUM_YEARLY")
                dt_expiry = dt_expiry.AddMonths(12);

          //  dt_expiry = Convert.ToDateTime(dt_expiry.Date).AddDays(1);
            dt_expiry = dt_expiry.ToUniversalTime();

            var user = new osm_user();
            user.LICENSE_EXPIRY_DATE = dt_expiry;

            if ((curr_lic == "FREE") || (curr_lic == "PRO_MONTHLY" && (lic_tobe_update == "PRO_QUARTERLY" || lic_tobe_update == "PRO_YEARLY")) || (curr_lic == "PRO_QUARTERLY" && lic_tobe_update == "PRO_YEARLY") || (curr_lic == "PREMIUM_YEARLY"))
            {
                user.LICENSE_TYPE = lic_tobe_update;
                user.LICENSE_CHANGEFLAG = 2;
                user.EMAIL_COUNT = 0;
            }
            else
            {
                user.LICENSETYPE_PENDING = lic_tobe_update;
                user.LICENSEPENDING_STARTDATE = dt;
                user.LICENSE_CHANGEFLAG = 1;
            }
            user.ACTIVATION_FLAG = 1;
            user.EMAIL_FLAG = 1;
            user.RENEWAL_ALERT = 0;
            user.RENEWAL_FLAG = 1;

            var ordInfo = new osm_orderinfo();
            ordInfo.PAID_DATE = DateTime.UtcNow;
            ordInfo.TRANSACTION_STATUS = 1;

            var accountHistory = new osm_useraccounthistory();
            accountHistory.LICENSE_ENDDATE = dt_expiry;
            accountHistory.LICENSE_STARTDATE = dt_now;
            accountHistory.LICENSE_TYPEFLAG = "0";
            accountHistory.EMAIL_FLAG = 1;

            int rtnValue = orderInfoService.UpgradeUsers(user, ordInfo, accountHistory, userId, orderId);
            return rtnValue.ToString();

        }


        public string UpgradeLicense(int orderId, string mode)
        {
            var orderInfoService = ServiceFactory.GetService<OrderInfoService>();
            var orderInfo = orderInfoService.FindOrderInfoByOrderId(orderId);

            string lic_tobe_update = "", curr_lic = "";
            DateTime dt = DateTime.UtcNow;
            DateTime dt_now = dt;
            int userId = 0, activ_flag = 0;
            string ord_type = mode;
            TimeSpan ts = DateTime.UtcNow - DateTime.UtcNow;
            if (orderInfo != null)
            {
                lic_tobe_update = orderInfo.LICENSE_OPTED;
                userId = orderInfo.USERID;

                ts = Convert.ToDateTime(orderInfo.LICENSE_EXPIRY_DATE) - DateTime.UtcNow;
                dt = Convert.ToDateTime(orderInfo.LICENSE_EXPIRY_DATE);
                curr_lic = orderInfo.LICENSE_TYPE;
                ord_type = orderInfo.ORDER_TYPE;
                activ_flag = ValidationHelper.GetInteger(orderInfo.ACTIVATION_FLAG, 0);

            }
            DateTime dt_expiry = DateTime.UtcNow;
            if (dt != Convert.ToDateTime("1/1/1800"))
            {
                dt_expiry = dt;

            }
            if (dt_now == Convert.ToDateTime("1/1/1800"))
            {
                dt_now = DateTime.UtcNow;

            }
            if (curr_lic != "FREE")
            {

                dt_now = dt;
            }

            if (lic_tobe_update == "PRO_MONTHLY")
                dt_expiry = dt_expiry.AddMonths(1);
            else if (lic_tobe_update == "PRO_QUARTERLY")
                dt_expiry = dt_expiry.AddMonths(3);
            else if (lic_tobe_update == "PRO_YEARLY")
                dt_expiry = dt_expiry.AddMonths(12);
            else if (lic_tobe_update == "PREMIUM_YEARLY")
                dt_expiry = dt_expiry.AddMonths(12);

          //  dt_expiry = Convert.ToDateTime(dt_expiry.Date).AddDays(1);
            if (mode == "R")
            {
                if (ts.Days > 0)
                {
                    dt_expiry = dt_expiry.AddDays(ts.Days);
                }
            }
            dt_expiry = dt_expiry.ToUniversalTime();

            var user = new osm_user();
            user.LICENSE_EXPIRY_DATE = dt_expiry;

            if ((curr_lic == "FREE") || (curr_lic == "PRO_MONTHLY" && (lic_tobe_update == "PRO_QUARTERLY" || lic_tobe_update == "PRO_YEARLY")) || (curr_lic == "PRO_QUARTERLY" && lic_tobe_update == "PRO_YEARLY") || (curr_lic == "PREMIUM_YEARLY"))
            {
                user.LICENSE_TYPE = lic_tobe_update;
                user.LICENSE_CHANGEFLAG = 2;
                user.EMAIL_COUNT = 0;
            }
            else
            {
                user.LICENSETYPE_PENDING = lic_tobe_update;
                user.LICENSEPENDING_STARTDATE = dt;
                user.LICENSE_CHANGEFLAG = 1;
            }
            user.ACTIVATION_FLAG = 1;
            user.EMAIL_FLAG = 1;
            user.RENEWAL_ALERT = 0;
            user.RENEWAL_FLAG = 1;

            var ordInfo = new osm_orderinfo();
            ordInfo.PAID_DATE = DateTime.UtcNow;
            ordInfo.TRANSACTION_STATUS = 1;

            var accountHistory = new osm_useraccounthistory();
            accountHistory.LICENSE_ENDDATE = dt_expiry;
            accountHistory.LICENSE_STARTDATE = dt_now;
            accountHistory.LICENSE_TYPEFLAG = "0";
            accountHistory.EMAIL_FLAG = 1;

            int rtnValue = orderInfoService.UpgradeUsers(user, ordInfo, accountHistory, userId, orderId);
            return rtnValue.ToString();

        }

        public List<osm_orderinfo> GetOrderInfoByUserId(int userId)
        {
            var orderInfo = new List<osm_orderinfo>();

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_orderinfo.OrderByDescending(o =>o.PK_ORDERID).Where (order => order.USERID == userId && order.DELETED == 0).ToList() ;
            }

        }
        public bool CheckOrderInfoDetailsExists(int userId,int paymentMode)
        {
            bool exist = false;

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (paymentMode == 2 || paymentMode == 4)
                {
                    //var orderDetails = dbCon.osm_orderinfo.OrderByDescending(o => o.PK_ORDERID).Where(order => order.USERID == userId && (order.MODEOFPAYMENT == 2 || order.MODEOFPAYMENT == 4)).ToList();
                    //foreach (var order in orderDetails)
                    //{
                    //    if (Convert.ToDateTime(order.PAID_DATE).Date == DateTime.UtcNow.Date)
                    //    {
                    //        exist = true;
                    //    }
                    //}
                    var orderDetails = dbCon.osm_orderinfo.OrderByDescending(o => o.PK_ORDERID).Where(order => order.USERID == userId && order.TRANSACTION_STATUS == 0 && (order.MODEOFPAYMENT == 2 || order.MODEOFPAYMENT == 4)).ToList();
                    if (orderDetails.Count != 0)
                    {
                        exist = true;
                    }
                }
            }
            return exist;

        }

        public osm_countriespricing GetPriceByCountryName(string countryName)
        {         

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_countriespricing.FirstOrDefault(cp =>cp.COUNTRYNAME == countryName);
            }

        }
       
    }
}

        