﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Exceptions;
using CovalenseUtilities.Services;

namespace Insighto.Business.Services
{
    public class LoginInfoService : BusinessServiceBase
    {
        public void Save(osm_logininfo logininfo)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_logininfo.Add(logininfo);
                dbCon.SaveChanges();
            }
        }
    }
}
