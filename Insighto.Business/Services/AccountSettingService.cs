﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Enumerations;

namespace Insighto.Business.Services
{
    public class AccountSettingService : BusinessServiceBase
    {

        public osm_accountsetting FindByUserId(int userID)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_accountsetting.Where(ac => ac.USERID == userID).FirstOrDefault();
            }
        }

        public string GetUserSettingsBySurveyId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var accountSettings = (from ac in dbCon.osm_accountsetting                                      
                                      join s in dbCon.osm_survey
                                      on ac.USERID equals s.USERID
                                      where s.SURVEY_ID == surveyId
                                           select ac.SURVEY_LOGO
                                          
                                           

                                           ).FirstOrDefault();
                return accountSettings;
            }
        }


        public osm_accountsetting GetallUserSettingsByUserId(int userId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_accountsetting.FirstOrDefault(s => s.USERID == userId);
            }
        }

        public void SaveAccountSetting(int userId, string theme = "Insighto Classic")
        {
            osm_accountsetting account = new osm_accountsetting();


            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                account.USERID = userId;
                account.SURVEY_HEADER = "";
                account.SURVEY_FOOTER = "";
                account.SURVEY_FONTTYPE = "Arial";
                account.SURVEY_FONTSIZE = "12";
                account.SURVEY_FONTCOLOR = "Black";
                account.SURVEY_BUTTONTYPE = "";
                account.SURVEY_STARTTEXT = "";
                account.SURVEY_SUBMITTEXT = "";
                account.PAGE_BREAK = 0;
                account.RESPONSE_REQUIRED = 0;
                account.SURVEY_LOGO = "";
                account.THEME = theme;
                dbCon.osm_accountsetting.Add(account);
                dbCon.SaveChanges();
            }

        }

        public void UpdateUserTheme(int userid, string theme = "Insighto Classic")
        {
             using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var setting = dbCon.osm_accountsetting.FirstOrDefault(s => s.USERID == userid);
                setting.THEME = theme;
                dbCon.SaveChanges();
            }
        }


        #region "Method : SaveOrUpdateAccountSetting"
        /// <summary>
        /// Used to update account settings based on flag.
        /// </summary>
        /// <param name="osmaccountsetting"></param>
        /// <param name="flag"></param>

        public void SaveOrUpdateAccountSetting(osm_accountsetting osmaccountsetting, string flag)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var isAccountsetting = (from accountsetting in dbCon.osm_accountsetting
                                        where accountsetting.USERID == osmaccountsetting.USERID && accountsetting.DELETED == 0
                                        select accountsetting).ToList();

                if (isAccountsetting.Any())
                {
                    osm_accountsetting accountsetting = dbCon.osm_accountsetting.FirstOrDefault(si => si.USERID == osmaccountsetting.USERID && si.DELETED == 0);

                    if (flag == "H")
                    {
                        accountsetting.SURVEY_HEADER = osmaccountsetting.SURVEY_HEADER;
                        accountsetting.SURVEY_FOOTER = osmaccountsetting.SURVEY_FOOTER;
                    }

                    if (flag == "F")
                    {
                        accountsetting.SURVEY_FONTTYPE = osmaccountsetting.SURVEY_FONTTYPE;
                        accountsetting.SURVEY_FONTSIZE = osmaccountsetting.SURVEY_FONTSIZE;
                        accountsetting.SURVEY_FONTCOLOR = osmaccountsetting.SURVEY_FONTCOLOR;
                    }

                    if (flag == "B")
                    {
                        accountsetting.SURVEY_BUTTONTYPE = osmaccountsetting.SURVEY_BUTTONTYPE;
                        accountsetting.SURVEY_STARTTEXT = osmaccountsetting.SURVEY_STARTTEXT;
                        accountsetting.SURVEY_SUBMITTEXT = osmaccountsetting.SURVEY_SUBMITTEXT;
                    }
                    if (flag == "L")
                    {
                        accountsetting.SURVEY_LOGO = osmaccountsetting.SURVEY_LOGO;
                        accountsetting.COMPANY_NAME = osmaccountsetting.COMPANY_NAME;
                    }

                    if (flag == "A")
                    {
                        accountsetting.SURVEY_HEADER = osmaccountsetting.SURVEY_HEADER;
                        accountsetting.SURVEY_FOOTER = osmaccountsetting.SURVEY_FOOTER;
                        accountsetting.SURVEY_FONTTYPE = osmaccountsetting.SURVEY_FONTTYPE;
                        accountsetting.SURVEY_FONTSIZE = osmaccountsetting.SURVEY_FONTSIZE;
                        accountsetting.SURVEY_FONTCOLOR = osmaccountsetting.SURVEY_FONTCOLOR;
                        accountsetting.SURVEY_BUTTONTYPE = osmaccountsetting.SURVEY_BUTTONTYPE;
                        accountsetting.SURVEY_STARTTEXT = osmaccountsetting.SURVEY_STARTTEXT;
                        accountsetting.SURVEY_SUBMITTEXT = osmaccountsetting.SURVEY_SUBMITTEXT;
                        accountsetting.SURVEY_LOGO = osmaccountsetting.SURVEY_LOGO;
                        accountsetting.COMPANY_NAME = osmaccountsetting.COMPANY_NAME;
                    }

                    if (flag == "U")
                    {
                        accountsetting.SURVEY_LOGO = osmaccountsetting.SURVEY_LOGO;
                    }

                    dbCon.SaveChanges();

                }
                else
                {

                    dbCon.osm_accountsetting.Add(osmaccountsetting);
                    dbCon.SaveChanges();
                }

            }

        }

        #endregion
    }


}
