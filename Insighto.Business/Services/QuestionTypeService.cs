﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using System.Diagnostics;
using System.Globalization;
using CovalenseUtilities.Helpers;
using System.IO;

namespace Insighto.Business.Services
{
    public class QuestionTypeService
    {
        public List<osm_questiontypes> GetQuestionType()
        {
            InsightoV2Entities dbCon = new InsightoV2Entities();
            var questiontypes = (from qt in dbCon.osm_questiontypes
                                 where qt.Delete == 0
                                 select qt).ToList();



            return questiontypes;
        }
    }
}
