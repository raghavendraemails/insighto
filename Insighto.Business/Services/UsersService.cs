﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Exceptions;
using System.IO;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using System.Web;
using CovalenseUtilities.Helpers;
using Insighto.Business.Extensions;
using System.Data.SqlClient;


namespace Insighto.Business.Services
{
    [System.Runtime.InteropServices.GuidAttribute("D5FD20C3-C3C0-469B-ADC1-747079B7E0DF")]
    public class UsersService : BusinessServiceBase
    {

        #region "Method : getAuthenticateddUser"
        /// <summary>
        /// Used for to check Authenticate the User and to get the user details. 
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>

        public osm_user getAuthenticatedUser(string UserName, string Password)
        {

            osm_user user;
            string EncryptPassword = EncryptHelper.Encrypt(Password);
            
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                user = (from osm_user in dbCon.osm_user
                        where osm_user.LOGIN_NAME == UserName && osm_user.PASSWORD == EncryptPassword   
                        select osm_user).FirstOrDefault();

            }
            return user;
        }

        #endregion

        public bool AuthenticateUser(string UserName, string Password)
        {
            bool isUser = default(bool);
            var userlist = new List<osm_user>();
            string EncryptPassword = EncryptHelper.Encrypt(Password);
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var aUser = (from user in dbCon.osm_user
                             where user.LOGIN_NAME == UserName && user.PASSWORD == EncryptPassword
                             select user).ToList();

                if (aUser.Count > 0)
                {
                    isUser = true;
                }
                else
                {
                    isUser = false;
                }

            }

            return isUser;
        }

        #region "Method : GetAuthenticateUser"
        /// <summary>
        /// Used To Get Authenticated User Details
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>

        public List<osm_user> GetAuthenticateUser(string UserName, string Password)
        {
            var aUser = new List<osm_user>();
            try
            {
                string EncryptPassword = EncryptHelper.Encrypt(Password);
                using (InsightoV2Entities dbCon = new InsightoV2Entities())
                {
                    aUser = (from user in dbCon.osm_user
                             where user.LOGIN_NAME == UserName && user.PASSWORD == EncryptPassword
                             select user).ToList();

                }
                SessionStateService sessionStateService = new SessionStateService();
                sessionStateService.SaveUserToSession(aUser[0]);
            }
            catch (Exception ex)
            {
                ExceptionManager.PublishException(ex);
                throw ex;
            }
            return aUser;
        }
        #endregion

        #region "Method : IsEmailIdExists"
        /// <summary>
        /// Used To Check the EmailId Exist or not
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>

        public bool IsEmailIdExists(string email)
        {
            var user = FindUserByEmailId(email);
            return (user != null);
        }


        #endregion

        public osm_user FindUserByEmailId(string email)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var user = dbCon.osm_user.Where(u => u.LOGIN_NAME == email && u.ACTIVATION_FLAG == 1).FirstOrDefault();
               // var userAlternate = dbCon.osm_useralternateemails.Where(a => a.EMAIL == email).FirstOrDefault();

                if (user == null)
                    return null;
                else
                    return user;
            }
        }

        public osm_adminuser FindUserByAdminEmailId(string email)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var user = dbCon.osm_adminuser.Where(u => u.LOGIN_NAME == email).FirstOrDefault();
                var userAlternate = dbCon.osm_useralternateemails.Where(a => a.EMAIL == email).FirstOrDefault();

                if (user == null)
                    return null;
                else
                    return user;
            }
        }

        #region "Method : SingUpFreeUser"
        /// <summary>
        /// Used to Register Free User
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        /// 

        public osm_user SaveUser(osm_user user)
        {


            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (user.USERID <= 0)
                {
                    var isUser = (from userkey in dbCon.osm_user
                                  where userkey.LOGIN_NAME == user.EMAIL
                                  select userkey).ToList();
                    if (isUser.Any())
                    {
                        return null; // If email id already exist                           
                    }
                    else
                    {
                        dbCon.osm_user.Add(user);
                        int id = dbCon.SaveChanges();
                        int Inc = 99999;
                        user.CUSTOMER_ID = (user.USERID + Inc).ToString();
                        user.ACTIVATION_FLAG = 0; 
                        user.EMAIL_FLAG = 0;
                        user.DELETED = 0;
                        dbCon.SaveChanges();
                        osm_cl_folders folder = new osm_cl_folders();
                        folder.USERID = user.USERID;
                        folder.FOLDER_NAME = "My Folder";
                        dbCon.osm_cl_folders.Add(folder);
                        dbCon.SaveChanges();
                        //save user sample files
                        CreateUserFolders(user);

                    }
                }
                else
                {
                    osm_user userDetails = dbCon.osm_user.Where(u => u.USERID == user.USERID).FirstOrDefault();
                    userDetails.ACCOUNT_TYPE = user.ACCOUNT_TYPE;
                    if (userDetails.ACTIVATION_FLAG == 1)
                    {
                        userDetails.ACTIVATION_FLAG = 1;
                    }
                    else
                    {
                        userDetails.ACTIVATION_FLAG = 0;
                    }
                    userDetails.ADDRESS_LINE1 = user.ADDRESS_LINE1;
                    userDetails.ADDRESS_LINE2 = user.ADDRESS_LINE2;
                    userDetails.ADMINID = user.ADMINID;
                    userDetails.APPROVAL_FLAG = user.APPROVAL_FLAG;
                    userDetails.AUTHLINK_FLAG = user.AUTHLINK_FLAG;
                    userDetails.CITY = user.CITY;
                    userDetails.COMPANY = user.COMPANY;
                    userDetails.COMPANY_SIZE = user.COMPANY_SIZE;
                    userDetails.COUNTRY = user.COUNTRY;
                    userDetails.CREATED_ON = userDetails.CREATED_ON;
                    userDetails.DELETED = user.DELETED;
                    userDetails.DEPT = user.DEPT;
                    userDetails.EMAIL = user.EMAIL;
                    userDetails.EMAIL_COUNT = user.EMAIL_COUNT;
                    userDetails.EMAIL_FLAG = 2;
                    userDetails.FIRST_NAME = user.FIRST_NAME;
                    userDetails.IMAGES_SIZE = user.IMAGES_SIZE;
                    userDetails.JOB_FUNCTION = user.JOB_FUNCTION;
                    userDetails.LAST_NAME = user.LAST_NAME;
                    userDetails.LICENSE_CHANGEFLAG = user.LICENSE_CHANGEFLAG;
                    if (userDetails.LICENSE_EXPIRY_DATE > DateTime.UtcNow)
                    {
                        userDetails.LICENSE_EXPIRY_DATE = userDetails.LICENSE_EXPIRY_DATE;
                    }
                    else
                    {
                        userDetails.LICENSE_EXPIRY_DATE = user.LICENSE_EXPIRY_DATE;
                    }
                    userDetails.LICENSE_OPTED = user.LICENSE_OPTED;
                  //  userDetails.LICENSE_TYPE = user.LICENSE_TYPE;
                    userDetails.LICENSE_UPGRADE = user.LICENSE_UPGRADE;
                    userDetails.LICENSEPENDING_STARTDATE = user.LICENSEPENDING_STARTDATE;
                    userDetails.LICENSETYPE_PENDING = user.LICENSETYPE_PENDING;
                    userDetails.LOGIN_FLAG = user.LOGIN_FLAG;
                    userDetails.LOGIN_NAME = user.LOGIN_NAME;
                    userDetails.MAIL_REMINDER_DATE = user.MAIL_REMINDER_DATE;
                    userDetails.MAIL_REMINDER_FLAG = user.MAIL_REMINDER_FLAG;
                    userDetails.MODIFIED_ON = user.MODIFIED_ON;
                    userDetails.OPTOUT_STATUS = user.OPTOUT_STATUS;
                    userDetails.PARENT_MASTERID = user.PARENT_MASTERID;
                    // userDetails.PASSWORD = user.PASSWORD;
                    userDetails.PAYMENT_TYPE = user.PAYMENT_TYPE;
                    userDetails.PHONE = user.PHONE;
                    userDetails.POSTAL_CODE = user.POSTAL_CODE;
                    userDetails.QUICKFIRST_FLAG = user.QUICKFIRST_FLAG;
                    userDetails.QUICKUSERGUIDE_FLAG = user.QUICKUSERGUIDE_FLAG;
                    userDetails.RENEWAL_ALERT = user.RENEWAL_ALERT;
                    userDetails.RENEWAL_FLAG = user.RENEWAL_FLAG;
                   // userDetails.RESET = user.RESET;
                    if (user.STANDARD_BIAS > 0 )
                    {
                        userDetails.STANDARD_BIAS = user.STANDARD_BIAS;
                    }
                    userDetails.STATE = user.STATE;
                    userDetails.STATUS = user.STATUS;
                    userDetails.SURVEYMAIL_STATUS = user.SURVEYMAIL_STATUS;
                    if (!string.IsNullOrEmpty(user.TIME_ZONE))
                    {
                        userDetails.TIME_ZONE = user.TIME_ZONE;
                    }
                    else
                    {
                        userDetails.TIME_ZONE = userDetails.TIME_ZONE;  
                    }
                    userDetails.UPDATE_QUICKFLAG = user.UPDATE_QUICKFLAG;
                    userDetails.UPDATES_FLAG = user.UPDATES_FLAG;
                    userDetails.USER_COMPANY = user.USER_COMPANY;
                    userDetails.USER_DESIGNATION = user.USER_DESIGNATION;
                    userDetails.USER_TYPE = user.USER_TYPE;
                    userDetails.USERTYPE_ID = user.USERTYPE_ID;
                    userDetails.WALLETPRICEPLAN = user.WALLETPRICEPLAN;
                    userDetails.WORK_INDUSTRY = user.WORK_INDUSTRY;
                    userDetails.STANDARD_BIAS = user.STANDARD_BIAS;
                    int id = dbCon.SaveChanges();
                    int Inc = 99999;
                    user.CUSTOMER_ID = (user.USERID + Inc).ToString();
                    dbCon.SaveChanges();
                }
            }

            return user;
        }

        public osm_user RegisterFreeUser(string firstName, string LastName, string email, string country, int reset, string ip_address,string password)
        {
            //string password = GetRandomPasswordUsingGUID(5);
           // password = password.Substring(0, 2);
           // password = password + firstName.Substring(0, 2) + "ins$";
            osm_user user = new osm_user();
            user.LOGIN_NAME = email;
            user.FIRST_NAME = firstName;
            user.LAST_NAME = LastName;
            user.ADDRESS_LINE1 = "";
            user.ADDRESS_LINE2 = "";
            user.CITY = "";
            user.STATE = "";
            user.COUNTRY = "";
            user.COMPANY = "";
            user.WORK_INDUSTRY = "";
            user.JOB_FUNCTION = "";
            user.DEPT = "";
            user.EMAIL = email;
            user.LICENSE_TYPE = "FREE";
            user.STATUS = "Active";
            user.LICENSE_EXPIRY_DATE = Convert.ToDateTime("1/1/1800");
            user.LICENSE_OPTED = "FREE";
            user.ADMINID = 0;
            user.APPROVAL_FLAG = 0;
            user.UPDATES_FLAG = 0;
            user.CREATED_ON = DateTime.UtcNow;
            user.MODIFIED_ON = DateTime.UtcNow;
            user.DELETED = 0;
            user.USER_TYPE = 0;
            user.USER_COMPANY = "";
            user.USER_DESIGNATION = "";
            user.PAYMENT_TYPE = 0;
            user.LICENSE_UPGRADE = 0;
            user.RENEWAL_FLAG = 0;
            user.POSTAL_CODE = "0";
            user.COMPANY_SIZE = "";
            user.RESET = reset;
            user.MAIL_REMINDER_FLAG = 0;
            user.MAIL_REMINDER_DATE = DateTime.UtcNow;
            user.QUICKUSERGUIDE_FLAG = 0;
            user.OPTOUT_STATUS = "";
            user.ACCOUNT_TYPE = "";
            user.AUTHLINK_FLAG = 0;
            user.LICENSE_CHANGEFLAG = 0;
            user.EMAIL_COUNT = 0;
            user.EMAIL_FLAG = 0;
            user.WALLETPRICEPLAN = "FREE";
            user.PASSWORD = EncryptHelper.Encrypt(password);
            user.IP_ADDRESS = ip_address;
            user.SURVEY_FLAG = 0;
            return SaveUser(user);
        }

        public osm_user RegisterFreeUserForPolls(string firstName, string LastName, string email, string country, int reset, string ip_address, string password)
        {
            //string password = GetRandomPasswordUsingGUID(5);
            // password = password.Substring(0, 2);
            // password = password + firstName.Substring(0, 2) + "ins$";
            osm_user user = new osm_user();
            user.LOGIN_NAME = email;
            user.FIRST_NAME = firstName;
            user.LAST_NAME = LastName;
            user.ADDRESS_LINE1 = "";
            user.ADDRESS_LINE2 = "";
            user.CITY = "";
            user.STATE = "";
            user.COUNTRY = "";
            user.COMPANY = "";
            user.WORK_INDUSTRY = "";
            user.JOB_FUNCTION = "";
            user.DEPT = "";
            user.EMAIL = email;
            //user.LICENSE_TYPE = "FREE";
            user.STATUS = "Active";
            //*-user.LICENSE_EXPIRY_DATE = Convert.ToDateTime("1/1/1800");
            //user.LICENSE_OPTED = "FREE";
            user.ADMINID = 0;
            user.APPROVAL_FLAG = 0;
            user.UPDATES_FLAG = 0;
            user.CREATED_ON = DateTime.UtcNow;
            user.MODIFIED_ON = DateTime.UtcNow;
            user.DELETED = 0;
            user.USER_TYPE = 0;
            user.USER_COMPANY = "";
            user.USER_DESIGNATION = "";
            user.PAYMENT_TYPE = 0;
            user.LICENSE_UPGRADE = 0;
            user.RENEWAL_FLAG = 0;
            user.POSTAL_CODE = "0";
            user.COMPANY_SIZE = "";
            user.RESET = reset;
            user.MAIL_REMINDER_FLAG = 0;
            user.MAIL_REMINDER_DATE = DateTime.UtcNow;
            user.QUICKUSERGUIDE_FLAG = 0;
            user.OPTOUT_STATUS = "";
            user.ACCOUNT_TYPE = "";
            user.AUTHLINK_FLAG = 0;
            user.LICENSE_CHANGEFLAG = 0;
            user.EMAIL_COUNT = 0;
            user.EMAIL_FLAG = 0;
            user.WALLETPRICEPLAN = "FREE";
            user.PASSWORD = EncryptHelper.Encrypt(password);
            user.IP_ADDRESS = ip_address;
            user.SURVEY_FLAG = 0;
            return SaveUser(user);
        }

        public void CreateUserFolders(osm_user user, string applicationPath= null)
        {

            string path = PathHelper.GetUserImagesFolder(user);
            var serverPath = applicationPath == null ? HttpContext.Current.Server.MapPath("~/") : applicationPath;
            DirectoryInfo dir = new DirectoryInfo(serverPath);
            if (dir.Exists)
            {
                Directory.CreateDirectory(serverPath + path);
                File.Create(serverPath + path + "\\sample.ppt");
                File.Create(serverPath + path + "\\sample.png");
            }

            var accountSettings = ServiceFactory.GetService<AccountSettingService>();
            accountSettings.SaveAccountSetting(user.USERID);
        }
        #endregion

        #region "Method : InsertOrderInfo"
        /// <summary>
        /// Used to Insert Order Info
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public osm_orderinfo InsertOrderInfo(osm_orderinfo order)
        {
            try
            {
                using (InsightoV2Entities dbCon = new InsightoV2Entities())
                {
                    dbCon.osm_orderinfo.Add(order);
                    dbCon.SaveChanges();
                    order.ORDERID = order.ORDERID + order.PK_ORDERID;
                    dbCon.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                SqlException innerException = ex.InnerException as SqlException;
            }
            return order;
        }
        #endregion

        #region "Method : InsertUserAccountHistory"
        /// <summary>
        /// Used to Insert User Account History
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public int InsertUserAccountHistory(osm_useraccounthistory history)
        {
            int retValue = default(int);

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_useraccounthistory.Add(history);
                retValue = dbCon.SaveChanges();
                // Inserted
            }
            return retValue;
        }
        #endregion

        #region "Method : GetUserDetails"
        /// <summary>
        /// Used to Get User Details
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public List<osm_user> GetUserDetails(int UserID)
        {
            var aUser = new List<osm_user>();
            try
            {

                using (InsightoV2Entities dbCon = new InsightoV2Entities())
                {
                    aUser = (from user in dbCon.osm_user                            
                             where user.USERID == UserID && user.DELETED == 0
                             select user).ToList();

                }
            }
            catch (Exception ex)
            {
                ExceptionManager.PublishException(ex);
                throw ex;
            }
            return aUser;
        }
        #endregion

        #region "Method : GetListItems"
        /// <summary>
        /// Used to Get PickList Items
        /// </summary>
        /// <param name="Category"></param>
        /// <returns></returns>
        public List<osm_picklist> GetListItems(string Category)
        {
            var aList = new List<osm_picklist>();
            try
            {
                using (InsightoV2Entities dbCon = new InsightoV2Entities())
                {
                    aList = (from list in dbCon.osm_picklist
                             where list.CATEGORY == Category
                             select list).ToList();
                }

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishException(ex);
                throw ex;

            }
            return aList;
        }
        #endregion

        #region "Method : UpdateMyProfile"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userList"></param>
        /// <returns></returns>

        public bool UpdateMyProfile(List<osm_user> userList)
        {
            bool isUpdated = default(bool);

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                foreach (var userInfo in userList)
                {
                    osm_user user = dbCon.osm_user.FirstOrDefault(u => u.USERID == userInfo.USERID);
                    user.FIRST_NAME = userInfo.FIRST_NAME;
                    user.LAST_NAME = userInfo.LAST_NAME;
                    user.ADDRESS_LINE1 = userInfo.ADDRESS_LINE1;
                    user.ADDRESS_LINE2 = userInfo.ADDRESS_LINE2;
                    user.CITY = userInfo.CITY;
                    user.STATE = userInfo.STATE;
                    user.COUNTRY = userInfo.COUNTRY;
                    user.POSTAL_CODE = userInfo.POSTAL_CODE;
                    user.COMPANY = userInfo.COMPANY;
                    user.USER_COMPANY = userInfo.USER_COMPANY;
                    user.DEPT = userInfo.DEPT;
                    user.USER_DESIGNATION = userInfo.USER_DESIGNATION;
                    user.WORK_INDUSTRY = userInfo.WORK_INDUSTRY;
                    user.JOB_FUNCTION = userInfo.JOB_FUNCTION;
                    user.COMPANY_SIZE = userInfo.COMPANY_SIZE;
                    user.TIME_ZONE = userInfo.TIME_ZONE;
                    user.UPDATES_FLAG = userInfo.UPDATES_FLAG;
                    user.PHONE = userInfo.PHONE;
                    user.MODIFIED_ON = userInfo.MODIFIED_ON;
                    user.STANDARD_BIAS = userInfo.STANDARD_BIAS;
                    dbCon.SaveChanges();
                    isUpdated = true;
                }
            }
            return isUpdated;
        }
        #endregion

        #region "Method:UserTimeZone"
        public int UserTimeZone(osm_user userInfo)
        {
            int retValue = default(int);
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_user user = dbCon.osm_user.FirstOrDefault(u => u.USERID == userInfo.USERID);
                user.TIME_ZONE = userInfo.TIME_ZONE;
                user.STANDARD_BIAS = userInfo.STANDARD_BIAS;
                user.UPDATES_FLAG = userInfo.UPDATES_FLAG;
                dbCon.SaveChanges();
                retValue = 1;
            }
            return retValue;
        }
        #endregion

        #region "Method UpdatePassword"
        public bool UpdatePassword(int UserId, string Password, int reset)
        {
            bool isUpdated = default(bool);

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_user user = dbCon.osm_user.FirstOrDefault(u => u.USERID == UserId);
                user.PASSWORD = EncryptHelper.Encrypt(Password);
                user.RESET = reset;
                user.MODIFIED_ON = DateTime.UtcNow;

                dbCon.SaveChanges();
                isUpdated = true;
            }
            return isUpdated;
        }
        #endregion

        #region "Method UpdateEmailId"
        public bool UpdateEmailId(int UserId, string emailId)
        {
            bool isUpdated = default(bool);

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_user user = dbCon.osm_user.FirstOrDefault(u => u.USERID == UserId);
                user.LOGIN_NAME = emailId;
                user.MODIFIED_ON = DateTime.UtcNow;
                dbCon.SaveChanges();
                isUpdated = true;
            }
            return isUpdated;
        }
        #endregion

        #region "Method UpdateEmailId"
        public bool UpdateCompanyName(int UserId, string companyname)
        {
            bool isUpdated = default(bool);

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_user user = dbCon.osm_user.FirstOrDefault(u => u.USERID == UserId);
                user.COMPANY = companyname;
                user.MODIFIED_ON = DateTime.UtcNow;
                dbCon.SaveChanges();
                isUpdated = true;
            }
            return isUpdated;
        }
        #endregion

        #region "Method UpdateAdminPassword"
        public bool UpdateAdminPassword(decimal UserId, string Password, int reset)
        {
            bool isUpdated = default(bool);

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_adminuser user = dbCon.osm_adminuser.FirstOrDefault(u => u.ADMINUSERID == UserId);
                user.PASSWORD = EncryptHelper.Encrypt(Password);
                user.RESET = reset;
                user.MODIFIED_ON = DateTime.UtcNow;
                dbCon.SaveChanges();
                isUpdated = true;
            }
            return isUpdated;
        }
        #endregion

        #region "Method ActivateUser"
        public osm_user ActivateUser(int UserId)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_user user = dbCon.osm_user.SingleOrDefault(u => u.USERID == UserId);
                if (user != null)
                {
                    user.ACTIVATION_FLAG = 1;
                    dbCon.SaveChanges();
                    return user;
                }
            }

            return null;
        }
        #endregion

        #region "Method:GetrandomPasswordUsingGUID"
        private string GetRandomPasswordUsingGUID(int length)
        {
            // Get the GUID
            string guidResult = System.Guid.NewGuid().ToString();
            // Remove the hyphens
            guidResult = guidResult.Replace("-", string.Empty);
            // Make sure length is valid
            if (length <= 0 || length > guidResult.Length)
                throw new ArgumentException("Length must be between 1 and " + guidResult.Length);
            // Return the first length bytes        
            return guidResult.Substring(0, length);
        }
        #endregion

        #region "Method: ProcessUserLogin"
        /// <summary>
        /// Save user to session
        /// Log user logged in info to db
        /// </summary>
        /// <param name="user"></param>
        public void ProcessUserLogin(osm_user user)
        {
            //Save user to Session 
            ServiceFactory.GetService<SessionStateService>().SaveUserToSession(user);

            //Save User login information to LoginInfo
            osm_logininfo logininfo = new osm_logininfo();
            logininfo.USERID = user.USERID;
            logininfo.LOGIN_TIME = DateTime.UtcNow;
            logininfo.STATUS_FLAG = 0;
            ServiceFactory.GetService<LoginInfoService>().Save(logininfo);
        }
        #endregion

        #region "Method : UpgradeUserInvoice"
        /// <summary>
        /// Used To Upgrade User Invioce
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>

        public int UpgradeUserInvoice(int orderId)
        {
            int returnVal = 0;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var uInfo = (from user in dbCon.osm_user
                             join oInfo in dbCon.osm_orderinfo on
                            user.USERID equals oInfo.USERID
                             where oInfo.PK_ORDERID == orderId
                             select new
                             {
                                 user.LICENSE_OPTED,
                                 user.LICENSE_EXPIRY_DATE,
                                 user.USERID,
                                 user.LICENSE_TYPE,
                                 user.ACTIVATION_FLAG,
                                 oInfo.ORDER_TYPE
                             }
                          ).ToList();


                string lictobeupdate = "", currlicense = "";
                DateTime dt = DateTime.UtcNow;
                DateTime dt_now = dt;
                int userid = 0, activflag = 0;
                string orderType = "N";
                foreach (var user in uInfo)
                {
                    lictobeupdate = user.LICENSE_OPTED;
                    userid = user.USERID;
                    dt = Convert.ToDateTime(user.LICENSE_EXPIRY_DATE);
                    currlicense = user.LICENSE_TYPE;
                    orderType = user.ORDER_TYPE;
                    activflag = ValidationHelper.GetInteger(user.ACTIVATION_FLAG.ToString(), 0);
                }
                DateTime dt_expiry = DateTime.UtcNow;
                if (dt != Convert.ToDateTime("1/1/1800"))
                {
                    dt_expiry = dt;

                }
                if (dt_now == Convert.ToDateTime("1/1/1800"))
                {
                    dt_now = DateTime.UtcNow;

                }
                if (currlicense != "FREE")
                {

                    dt_now = dt;
                }
                if (lictobeupdate == "PRO_MONTHLY")
                {
                    dt_expiry = dt_expiry.AddMonths(1);
                }
                else if (lictobeupdate == "PRO_QUARTERLY")
                {
                    dt_expiry = dt_expiry.AddMonths(3);
                }
                else if (lictobeupdate == "PRO_YEARLY")
                {
                    dt_expiry = dt_expiry.AddMonths(12);
                }
                else if (lictobeupdate == "PREMIUM_YEARLY")
                {
                    dt_expiry = dt_expiry.AddMonths(12);
                }
              //  dt_expiry = Convert.ToDateTime(dt_expiry.Date).AddDays(1);
                dt_expiry = dt_expiry.ToUniversalTime();
                osm_user edituser = dbCon.osm_user.FirstOrDefault(u => u.USERID == userid);
                edituser.LICENSE_EXPIRY_DATE = dt_expiry;
                if ((currlicense == "FREE") || (currlicense == "PRO_MONTHLY" && (lictobeupdate == "PRO_QUARTERLY" || lictobeupdate == "PRO_YEARLY")) || (currlicense == "PRO_QUARTERLY" && lictobeupdate == "PRO_YEARLY") || (currlicense == "PREMIUM_YEARLY"))
                {
                    edituser.LICENSE_TYPE = lictobeupdate;
                    edituser.LICENSE_CHANGEFLAG = 2;
                    edituser.EMAIL_COUNT = 0;
                }
                else
                {
                    edituser.LICENSETYPE_PENDING = lictobeupdate;
                    edituser.LICENSEPENDING_STARTDATE = dt;
                    edituser.LICENSE_CHANGEFLAG = 1;
                }
                edituser.ACTIVATION_FLAG = 1;
                edituser.EMAIL_FLAG = 1;
                edituser.RENEWAL_ALERT = 0;
                edituser.RENEWAL_FLAG = 1;
                dbCon.SaveChanges();
               // osm_orderinfo editorderInfo = dbCon.osm_orderinfo.FirstOrDefault(ord => ord.USERID == userid);
                osm_orderinfo editorderInfo = dbCon.osm_orderinfo.FirstOrDefault(ord => ord.USERID == userid && ord.PK_ORDERID == orderId);
                editorderInfo.PAID_DATE = DateTime.UtcNow;
                editorderInfo.TRANSACTION_STATUS = 1;
              //  editorderInfo.PK_ORDERID = orderId;
                dbCon.SaveChanges();
                string orderID = orderId.ToString();
                osm_useraccounthistory editHistory = dbCon.osm_useraccounthistory.FirstOrDefault(history => history.USERID == userid && history.ORDERID == orderID);
                editHistory.LICENSE_STARTDATE = dt_now;
                editHistory.LICENSE_ENDDATE = dt_expiry;
                editHistory.LICENSE_TYPEFLAG = "0";
                editHistory.EMAIL_FLAG = 1;
                returnVal = dbCon.SaveChanges();
            }
            return returnVal;
        }
        #endregion

        #region "Method : UpgradeUserAccountHistory"
        /// <summary>
        /// Used To Upgrade User Account History
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>

        public int UpgradeUserAccountHistory(int userId, int orderId, int emailFlag)
        {
            int returnVal = 0;
            string orderID = orderId.ToString();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_useraccounthistory editHistory = dbCon.osm_useraccounthistory.FirstOrDefault(history => history.USERID == userId && history.ORDERID == orderID);
                editHistory.EMAIL_FLAG = emailFlag;
                returnVal = dbCon.SaveChanges();
            }
            return returnVal;
        }
        #endregion

        #region "Method : GetUserActivationFlagByUserId"
        /// <summary>
        /// Used To Get User Activation Flag By UserId
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>

        public string GetUserActivationFlagByUserId(int userId)
        {
            string user;
            try
            {

                using (InsightoV2Entities dbCon = new InsightoV2Entities())
                {
                    user = (from u in dbCon.osm_user
                            where u.USERID == userId && u.DELETED == 0
                            select new
                            {
                                u.ACTIVATION_FLAG
                            }).FirstOrDefault().ToString();

                }
            }
            catch (Exception ex)
            {
                ExceptionManager.PublishException(ex);
                throw ex;
            }
            return user;
        }
        #endregion

        #region "Method : DeclineOrder"
        /// <summary>
        /// Used To Decline Order
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>

        public int DeclineOrder(int userId, int orderId, int transStatus)
        {
            int returnVal = 0;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_orderinfo editorderInfo = dbCon.osm_orderinfo.FirstOrDefault(ord => ord.USERID == userId && ord.PK_ORDERID == orderId);
                editorderInfo.TRANSACTION_STATUS = transStatus;
                if (transStatus == 2)
                {
                    editorderInfo.PAID_DATE = DateTime.UtcNow;
                }
                dbCon.SaveChanges();
                string OrdId = orderId.ToString();
                osm_useraccounthistory editHistory = dbCon.osm_useraccounthistory.FirstOrDefault(history => history.USERID == userId && history.ORDERID == OrdId);
                editHistory.LICENSE_TYPEFLAG = "0";
                editHistory.EMAIL_FLAG = 1;
                returnVal = dbCon.SaveChanges();
            }
            return returnVal;
        }
        #endregion

        #region Method: GetUserOrderInfo
        public List<UserOrderInfo> GetUserOrderInfo(int userId, int orderId)
        {
            var uInfo = new List<UserOrderInfo>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                uInfo = (from user in dbCon.osm_user
                         join oInfo in dbCon.osm_orderinfo on
                        user.USERID equals oInfo.USERID
                         where oInfo.PK_ORDERID == orderId && user.USERID == userId
                         select new UserOrderInfo
                         {
                             activationFlag = user.ACTIVATION_FLAG,
                             orderType = oInfo.ORDER_TYPE,
                             productAmont = oInfo.PRODUCT_PRICE,
                             Total = oInfo.TOTAL_PRICE,
                             taxAmount = oInfo.TAX_PRICE
                         }
                         ).ToList();
            }
            return uInfo;
        }
        #endregion

        #region Method: SaveUserAlternateEmails
        public osm_useralternateemails SaveUserAlternateEmails(osm_useralternateemails emaillist)
        {
            var useremails = new osm_useralternateemails();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_useralternateemails.Add(emaillist);
                dbCon.SaveChanges();
                return emaillist;

            }

        }
        #endregion

        #region Method:  GetUserAlternateEmails
        public List<osm_useralternateemails> GetUserAlternateEmails(int UserId)
        {
            var userEmails = new List<osm_useralternateemails>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                userEmails = (from user in dbCon.osm_useralternateemails
                              where user.USERID == UserId
                              select user).ToList();

            }
            return userEmails;
        }
        #endregion

        #region Method:   UpdateAlternateEmail
        public void UpdateAlternateEmail(int Id, string email)
        {
            var userEmails = new List<osm_useralternateemails>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var user = dbCon.osm_useralternateemails.SingleOrDefault(u => u.ID == Id);
                //if (email == "") dbCon.osm_useralternateemails.Remove(user);
                // else 
                user.EMAIL = email;
                user.ISAUTHENTICATED = 0;
                dbCon.SaveChanges();
            }

        }
        #endregion

        #region Method:  ChangePrimaryEmail
        public string ChangePrimaryEmail(string email, int UserId)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                var alternateEmail = dbCon.osm_useralternateemails.SingleOrDefault(u => u.EMAIL == email);
                var primaryEmail = dbCon.osm_user.SingleOrDefault(u => u.USERID == UserId);

                if (alternateEmail != null && alternateEmail.ISAUTHENTICATED == 1)
                {

                    string Email = primaryEmail.LOGIN_NAME;
                    primaryEmail.LOGIN_NAME = alternateEmail.EMAIL;
                    primaryEmail.EMAIL = alternateEmail.EMAIL;
                    alternateEmail.EMAIL = Email;
                    dbCon.SaveChanges();

                }
                return primaryEmail.EMAIL;
            }

        }
        #endregion

        #region "Method:ActivateAlternateUser"
        public osm_useralternateemails ActivateAlternateUser(int Id)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osm_useralternateemails user = dbCon.osm_useralternateemails.SingleOrDefault(u => u.ID == Id);
                if (user != null)
                {
                    user.ISAUTHENTICATED = 1;
                    dbCon.SaveChanges();
                    return user;
                }
            }

            return null;


        }
        #endregion

        #region "Method:GetUserAccountSettingView"
        public UserAccountSettingView GetUserAccountSettingView(int userID)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.UserAccountSettingViews.Where(u => u.USERID == userID).FirstOrDefault();
            }
        }
        #endregion

        #region "Method:updateImageSizeInUser"
        public void updateImageSizeInUser(int imageSize, int userId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var sFolder = (from user in dbCon.osm_user
                               where user.STATUS == "Active" && user.USERID == userId
                               select user).ToList();

                if (sFolder.Count > 0)
                {
                    imageSize += Convert.ToInt32(sFolder[0].IMAGES_SIZE);
                }

                osm_user users = dbCon.osm_user.Single(s => s.USERID == userId);
                users.IMAGES_SIZE = imageSize;
                dbCon.SaveChanges();
            }
        }
        #endregion

        #region"Method:GetUserByEmailId"
        public osm_user GetUserByEmailId(string email)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_user.Where(u => u.EMAIL == email).FirstOrDefault();

            }
        }
        #endregion

        #region"Method:GetUserByLoginName"
        public osm_user GetUserByLoginName(string email)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_user.Where(u => u.LOGIN_NAME == email).FirstOrDefault();

            }
        }
        #endregion

        #region "Method FindUserByUserId"
        public osm_user FindUserByUserId(int userId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_user.Where(u => u.USERID == userId).SingleOrDefault();
            }
        }
        #endregion

        #region "Method GetLicenseStartDateUserId"
        public osm_useraccounthistory GetLicenseStartDateUserId(int userId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_useraccounthistory.OrderByDescending(u => u.PK_ACCOUNTID).FirstOrDefault(u => u.USERID == userId && u.DELETED == 0);
            }
        }
        #endregion

        #region "Method GetLicenseDetailsDateUserId"
        public List<osm_useraccounthistory> GetLicenseDetailsDateUserId(int userId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_useraccounthistory.Where(u => u.USERID == userId && u.DELETED == 0 && u.LICENSE_ENDDATE != null).ToList();
            }
        }
        #endregion



        #region "Method GetLicenseDetailsDateHistoryId"
        public List<osm_useraccounthistory> GetLicenseDetailsDateHistoryId(int historyId)
        {
            //using (InsightoV2Entities dbCon = new InsightoV2Entities())
            //{
            //    return dbCon.osm_useraccounthistory.Where(u => u.PK_ACCOUNTID == historyId && u.DELETED == 0).ToList();
            //}
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_useraccounthistory.Where(u => u.PK_ACCOUNTID == historyId && u.DELETED == 0).ToList();
            }
        }
        #endregion

        #region "Method GetLicenseDetailsDateByOrderID"
        public List<osm_useraccounthistory> GetLicenseDetailsDateByOrderID(string orderId)
        {
            //using (InsightoV2Entities dbCon = new InsightoV2Entities())
            //{
            //    return dbCon.osm_useraccounthistory.Where(u => u.PK_ACCOUNTID == historyId && u.DELETED == 0).ToList();
            //}
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_useraccounthistory.Where(u => u.ORDERID == orderId && u.DELETED == 0).OrderByDescending(a => a.CREATED_DATE).ToList();
            }
        }
        #endregion

        #region "Method : GetAllUserDetails"
        /// <summary>
        /// Used to Get All User Details
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public PagedList<osm_user> GetAllUserDetails(PagingCriteria pagingCriteria, string searchKey)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (searchKey != null && searchKey != "")
                {
                    return dbCon.osm_user.Where(a => a.DELETED == 0 && a.USERID !=-1 && (a.FIRST_NAME.Contains(searchKey) || a.LAST_NAME.Contains(searchKey) || a.LOGIN_NAME.Contains(searchKey))).ToPagedList(pagingCriteria);
                }
                else
                    return dbCon.osm_user.Where(a => a.DELETED == 0 && a.USERID !=-1).ToPagedList(pagingCriteria);
            }

        }
        #endregion

        #region "Method: DeleteUserByUserId"

        public int DeleteUserByUserId(int UserId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var user = dbCon.osm_user.Where(e => e.USERID == UserId).SingleOrDefault();
                //dbCon.osm_user.Remove(user);
                UpdateDeleteOsmUsers(ValidationHelper.GetInteger(user.USERID, 0));
                return dbCon.SaveChanges();
            }
        }

        #endregion

        #region "Method: Update Delete OsmUsers"

        public void UpdateDeleteOsmUsers(int USERID)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var list = dbCon.osm_user.Where(u => u.USERID == USERID).SingleOrDefault();
                list.DELETED = 1;
                dbCon.SaveChanges();
            }
        }
        #endregion

        #region "Method: UpdateChangePasswordFlag"

        public void UpdateChangePasswordFlag(int USERID)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var list = dbCon.osm_user.Where(u => u.USERID == USERID).FirstOrDefault();
                list.RESET = 0;
                dbCon.SaveChanges();
            }
        }
        #endregion

        #region "Method: UpdateChangePasswordFlag"

        public void UpdateSurveyFlagByUserId(int userId, int surveyFlag)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var list = dbCon.osm_user.Where(u => u.USERID == userId).FirstOrDefault();
                list.SURVEY_FLAG = surveyFlag;
                dbCon.SaveChanges();
            }
        }
        #endregion
      

        #region "Method: DeActivateUserByUserId"

        public int DeActivateUserByUserId(int UserId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var user = dbCon.osm_user.Where(e => e.USERID == UserId).SingleOrDefault();
                //dbCon.osm_user.Remove(user);
                UpdateDeactivateOsmUsers(ValidationHelper.GetInteger(user.USERID, 0));
                return dbCon.SaveChanges();
            }
        }

        #endregion

        #region "Method: Update Delete OsmUsers"

        public void UpdateDeactivateOsmUsers(int USERID)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var list = dbCon.osm_user.Where(u => u.USERID == USERID).SingleOrDefault();
                if (list.STATUS == "Active")
                    list.STATUS = "InActive";
                else 
                    list.STATUS = "Active";
                  dbCon.SaveChanges();
            }
        }
        #endregion



        #region "Method:FindPaymentPendingUsers"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pagingCriteria"></param>
        /// <param name="paymentType"></param>
        /// <returns></returns>

        public PagedList<osm_user> FindPaymentPendingUsers(PagingCriteria pagingCriteria, string paymentType)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (paymentType == "Pending")
                    return dbCon.osm_user.Where(u => u.APPROVAL_FLAG == 1 && u.LICENSE_UPGRADE == 0).ToPagedList(pagingCriteria);
                else
                    return dbCon.osm_user.Where(u => u.LICENSE_UPGRADE != 0 && u.APPROVAL_FLAG == 1).ToPagedList(pagingCriteria);
            }
        }
        #endregion

        #region "Method:UserAdvanceSearchDetails"
        public List<osm_user> UserAdvanceSearchDetails(PagingCriteria pagingCriteria, string firstName, string lastName,
            string city, string state,
            string company, string licensetype, string jobFunction, string workIndustry, string companysize, string EmailId, string createdDateFrom,
            string createDatetTo, string licenseExpiryDateFrom, string licenseExpiryDateTo,
            string customerId, int licenseExpiryPeriod, string userType)
        {
            //var infoList = new PagedList<osm_user>();
            //PagedList<osm_user> infoList=new PagedList<osm_user>();
            var userList = new List<osm_user>();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                userList = dbCon.GetUserAdvanceSearchDetails(firstName, lastName, city, state, company, userType, licensetype, jobFunction, workIndustry, companysize, EmailId, createdDateFrom, createDatetTo, licenseExpiryDateFrom, licenseExpiryDateTo, customerId, licenseExpiryPeriod).ToList();

            }

            return userList;
        }
        #endregion


        #region "Method: UpgradeLicenseLevelAfterPayment"

        public void UpgradeLicenseLevelAfterPayment(int USERID, string LicenseType)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var list = dbCon.osm_user.Where(u => u.USERID == USERID).SingleOrDefault();
                list.LICENSE_UPGRADE = 0;
                list.LICENSE_TYPE = LicenseType;
                dbCon.SaveChanges();
            }
        }
        #endregion

        #region"Method:GetUserEmailids"
        public List<osm_user> GetUserEmailIdsByType(string userType)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_user.Where(u => u.LICENSE_TYPE == userType && u.DELETED==0).ToList();

            }
        }
        #endregion

        #region "Method: UpdateApprovalFlag"

        public void UpdateApprovalFlag(int USERID)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var list = dbCon.osm_user.Where(u => u.USERID == USERID).SingleOrDefault();
                list.APPROVAL_FLAG = 0;
                dbCon.SaveChanges();
            }
        }
        #endregion

        #region"Method:GetUserEmailids"
        public osm_user GetUserActivationFlag(int userId)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_user.Where(u => u.USERID == userId ).FirstOrDefault();

            }
        }
        #endregion
    }


}


