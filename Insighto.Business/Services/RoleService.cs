﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using Insighto.Business.Extensions;

namespace Insighto.Business.Services
{
    public class RoleService : BusinessServiceBase
    {
         public List<osm_roles> FindAllRoles()
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var role = (from roles in dbCon.osm_roles
                                 where roles.DELETED == 0 
                                 orderby roles.ROLE_NAME
                                 select roles).ToList();

                return role;
            }

        }

         public PagedList<osm_roles> GetAllRoles(PagingCriteria pageCriteria)
         {
             using (InsightoV2Entities dbCon = new InsightoV2Entities())
             {
                 var role = (from roles in dbCon.osm_roles
                             where roles.DELETED == 0
                             orderby roles.ROLE_NAME
                             select roles).ToPagedList(pageCriteria);

                 return role;
             }

         }

         public osm_roles SaveRole(osm_roles role)
         {
             using (InsightoV2Entities dbCon = new InsightoV2Entities())
             {
                 if (role.ROLEID <= 0)
                 {
                     var isRole = (from roleKey in dbCon.osm_roles
                                   where roleKey.ROLE_NAME == role.ROLE_NAME
                                   select roleKey).ToList();
                     if (isRole.Any())
                     {
                         return null; // If RoleName already exist                           
                     }
                     else
                     {
                         dbCon.osm_roles.Add(role);
                         dbCon.SaveChanges();

                     }
                 }
                 else
                 {
                     osm_roles roleDetails = dbCon.osm_roles.Where(u => u.ROLEID == role.ROLEID).FirstOrDefault();
                     roleDetails.ROLE_NAME = role.ROLE_NAME;
                     roleDetails.MODIFIED_ON = role.MODIFIED_ON;
                     roleDetails.DELETED = 0; 
                     int id = dbCon.SaveChanges();
                 }
             }
             return role;
         }



    }
}
