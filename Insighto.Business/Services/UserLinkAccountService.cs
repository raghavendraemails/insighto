﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Enumerations;

namespace Insighto.Business.Services
{
    public class UserLinkAccountService : BusinessServiceBase
    {
        public osm_userlinkaccount Save(osm_userlinkaccount userLinkAccount)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                
                dbCon.osm_userlinkaccount.Add(userLinkAccount);
                dbCon.SaveChanges();

                return userLinkAccount;
            }
        }

        public osm_userlinkaccount Find(int userID, UserLinkAccountType accountType)
        {
            var acc  = accountType.ToString();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_userlinkaccount.Where(u => u.UserID == userID && u.AccountType == acc).FirstOrDefault();
            }


        }



        public int DeleteByUserIdAndAccountType(int userID, UserLinkAccountType accountType)
        {
            var acc = accountType.ToString();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var userAccount = dbCon.osm_userlinkaccount.Where(u => u.UserID == userID && u.AccountType == acc).SingleOrDefault();
                dbCon.osm_userlinkaccount.Remove(userAccount);
                return dbCon.SaveChanges();
            }
        }

        public osm_userlinkaccount FindUserIdByEmail(string email, UserLinkAccountType accountType)
        {
            var acc = accountType.ToString();
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_userlinkaccount.Where(u => u.Email == email && u.AccountType == acc).FirstOrDefault();
            }

        }

       
    }
}
