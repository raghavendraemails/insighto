﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using CovalenseUtilities.Services;

namespace Insighto.Business.Services
{
    public class SessionStateService : BusinessServiceBase
    {
        public void SaveUserToSession(osm_user user)
        {
            if(user == null)
                throw new Exception("User should not be null to assign to session");

            var accountSettings = ServiceFactory.GetService<AccountSettingService>().FindByUserId(user.USERID);
            var loggedInUserInfo = new LoggedInUserInfo{
                Email = user.EMAIL,
                UserId = user.USERID,
                FirstName = user.FIRST_NAME,
                LastName = user.LAST_NAME,
                Reset = user.RESET,
                AccountSetting = accountSettings,
                LicenseType=user.LICENSE_TYPE,
                LoginName=user.LOGIN_NAME,
                StandardBIAS=user.STANDARD_BIAS,
                TimeZone = user.TIME_ZONE,
                LicenseOpted=user.LICENSE_OPTED,
                ExpiryDate = user.LICENSE_EXPIRY_DATE,
                ActivationFlag=user.ACTIVATION_FLAG
               
            };
            HttpContext.Current.Session["UserInfo"] = loggedInUserInfo;
            HttpContext.Current.Session["UserId"] = loggedInUserInfo.UserId;
        }
        public void AddCaptchaToSession(string captchaValue)
        {
            HttpContext.Current.Session["Captcha"] = captchaValue;
        }
        public string GetCaptchaFromSession()
        {
           var captchaValue= HttpContext.Current.Session["Captcha"];
           return captchaValue.ToString();

        }

        public LoggedInUserInfo GetLoginUserDetailsSession()
        {
            var loggedInUserInfo = HttpContext.Current.Session["UserInfo"] as LoggedInUserInfo;
            return loggedInUserInfo;
        }

        public void EndUserSession() {
            HttpContext.Current.Session["UserInfo"] = null;
        }


        public void SaveAdminUserToSession(osm_adminuser adminUser)
        {
            if (adminUser == null)
                throw new Exception("User should not be null to assign to session");


            var loggedAdminInUserInfo = new LoggedInAdminUserInfo
            {
                Email = adminUser.LOGIN_NAME,
                AdminUserId = adminUser.ADMINUSERID,
                FirstName = adminUser.FIRST_NAME,
                LastName = adminUser.LAST_NAME,
                Reset = adminUser.RESET,                              
                StandardBIAS = adminUser.STANDARD_BIAS,
                TimeZone = adminUser.TIME_ZONE,
                AdminType=adminUser.ADMIN_TYPE,
                Password=adminUser.PASSWORD
            };
            HttpContext.Current.Session["AdminUserInfo"] = loggedAdminInUserInfo;
        }

        public LoggedInAdminUserInfo GetLoginAdminUserDetailsSession()
        {
            var loggedAdminInUserInfo = HttpContext.Current.Session["AdminUserInfo"] as LoggedInAdminUserInfo;
            return loggedAdminInUserInfo;
        }

        public void EndAdminUserSession()
        {
            HttpContext.Current.Session["AdminUserInfo"] = null;
        }
        public void AddRecordsCountToSession(int recordPerPage)
        {
            HttpContext.Current.Session["recordsCount"] = recordPerPage;
        }
        public string GetRecordsCountFromSession()
        {
            var recordsCount = HttpContext.Current.Session["recordsCount"];
            if (recordsCount != null)
            {
                return recordsCount.ToString();
            }
            else
            {
                return "10";
            }

        }
        public void AddCountryNameToSession(string countryName)
        {
            HttpContext.Current.Session["Country"] = countryName;
        }

        public string GetCountryNameFromSession()
        {
            var countryValue = HttpContext.Current.Session["Country"];
            return countryValue.ToString();

        }

    }
}
