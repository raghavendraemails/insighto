﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Services;
using System.IO;

namespace Insighto.Business.Services
{
    public class TemplateService : BusinessServiceBase
    {
        public string[] GetDataFromTempalte(string fileName, Dictionary<string, string> replaceValues)
        {
            if (!File.Exists(fileName))
                return new string[0];

            string templateContent;
            using (var streamReader = File.OpenText(fileName))
            {
                templateContent = streamReader.ReadToEnd();

                foreach (var item  in replaceValues)
                {
                    var  temp = templateContent.Replace(item.Key, item.Value);
                    templateContent = temp;
                }
            }
            return templateContent.Split(new[] { "##Separator##" }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
