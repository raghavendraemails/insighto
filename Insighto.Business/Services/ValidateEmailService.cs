﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;

using System.Web;
using CovalenseUtilities.Helpers;

namespace Insighto.Business.Services
{
    public class ValidateEmailService : BusinessServiceBase
    {
        public bool ValidateUserByEmailId(string email)
        {
            var isExist = default(bool);
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var user = dbCon.osm_user.Where(u => u.LOGIN_NAME == email).SingleOrDefault();
                if (user == null)
                {
                    var userAlternateEmail = dbCon.osm_useralternateemails.Where(a => a.EMAIL == email).SingleOrDefault();
                    if (userAlternateEmail == null)
                    {
                        //var userLinkAccounts = dbCon.osm_userlinkaccount.Where(l => l.Email == email).ToList();
                        //if (userLinkAccounts.Any())
                        //{
                        //    isExist = true;
                        //}
                    }
                    else
                        isExist = true;
                }
                else
                    isExist = true;
            }
            return isExist;
        }
    }
}
