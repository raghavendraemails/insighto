﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using Insighto.Business.Extensions;

namespace Insighto.Business.Services
{
    public class PickListService : BusinessServiceBase
    {
        #region "Method : GetPickList"
        /// <summary>
        /// Used to get survey category list.
        /// </summary>
        /// <param name="osmPickList"></param>
        /// <returns></returns>

        public List<osm_picklist> GetPickList(osm_picklist osmPickList)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var picklists = (from picklist in dbCon.osm_picklist
                                 where picklist.CATEGORY == osmPickList.CATEGORY && picklist.DELETED == 0
                                 orderby picklist.PARAMETER
                                 select picklist).ToList();

                return picklists;
            }

        }

        #endregion

        #region "Method : GetPickList"
        /// <summary>
        /// Used to get survey category list.
        /// </summary>
        /// <param name="osmPickList"></param>
        /// <returns></returns>

        public List<osm_contactlist> GetContactLists(int userID, int launchType, int SurveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                if (launchType > 1)
                {
                    return (from contactlist in dbCon.osm_contactlist
                            where contactlist.CREATED_BY == userID && contactlist.DELETED == 0

                            select contactlist).ToList();
                }
                else
                {
                    return (from contactlist in dbCon.osm_contactlist
                            where contactlist.CREATED_BY == userID && contactlist.DELETED == 0
                                        && !(from sl in dbCon.osm_surveylaunch
                                             where sl.SURVEY_ID == SurveyId
                                             select sl.CONTACTLIST_ID).Contains(contactlist.CONTACTLIST_ID)
                            select contactlist).ToList();
                }
               
            }

        }

        #endregion

        #region "Method : GetPickList with Paging and Sorting"
      

        /// <summary>
        /// 
        /// </summary>
        /// <param name="category"></param>
        /// <param name="pagingCriteria"></param>
        /// <returns></returns>
        public PagedList<osm_picklist> GetPickList(string category, PagingCriteria pagingCriteria)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {

                var picklists = dbCon.osm_picklist.Where(p => p.CATEGORY == category && p.DELETED == 0)
                                                  .ToPagedList(pagingCriteria);               

                return picklists;
            }

        }

        #endregion

        #region "Method : GetPickList"
        /// <summary>
        /// Used to get survey category list.
        /// </summary>
        /// <param name="osmPickList"></param>
        /// <returns></returns>

        public List<osm_picklist> GetPickListFontSize(osm_picklist osmPickList)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var picklists = (from picklist in dbCon.osm_picklist
                                 where picklist.CATEGORY == osmPickList.CATEGORY && picklist.DELETED == 0
                                 select picklist).ToList();

                return picklists;
            }

        }

        #endregion

        #region "Method : GetPickListParemeterName"
        /// <summary>
        /// Used to get survey category list.
        /// </summary>
        /// <param name="osmPickList"></param>
        /// <returns></returns>

        public string GetPickListParemeterName(string paramValue,string category)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var pickListValue= dbCon.osm_picklist.Where(p => p.PARAM_VALUE == paramValue && p.CATEGORY == category).FirstOrDefault();
                return pickListValue.PARAMETER;

            }

        }

        #endregion

        #region "Method: Find All List"
        ///
        public List<osm_picklist> FindAllList()
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_picklist.Where(p => p.DELETED == 0).ToList() ;

            }
        }
        #endregion

        #region "Method : SavePickListItem"
        ///
        public int SavePickListItem(osm_picklist pickList)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var pklist = dbCon.osm_picklist.Where(p => (p.PARAM_VALUE == pickList.PARAM_VALUE || p.PARAMETER == pickList.PARAMETER) && p.CATEGORY == pickList.CATEGORY).ToList();
                if (pklist.Count < 1)
                dbCon.osm_picklist.Add(pickList);
                return dbCon.SaveChanges();
            }
        }

        public int UpdatePickListItem(osm_picklist pickList)
        {
            int rtnValue = 0;
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var list = dbCon.osm_picklist.Where(p => p.PARAMETER == pickList.PARAMETER && p.CATEGORY == pickList.CATEGORY  && p.PICK_ID != pickList.PICK_ID).ToList();
                if (list.Count < 1)
                {
                    var pkList = dbCon.osm_picklist.Where(p => p.PICK_ID == pickList.PICK_ID).SingleOrDefault();
                   // pkList.PARAM_VALUE = pickList.PARAM_VALUE;
                    pkList.PARAMETER = pickList.PARAMETER;
                    dbCon.SaveChanges();
                    rtnValue = 1;
                }
                return rtnValue;
            }
        }

        #endregion


        public osm_picklist FindPickListByPickId(int pickId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_picklist.Where(p => p.PICK_ID == pickId).SingleOrDefault();
            }
        }
    }
}
