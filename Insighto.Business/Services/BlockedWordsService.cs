﻿using System.Collections.Generic;
using System.Linq;
using CovalenseUtilities.Services;
using Insighto.Business.Extensions;
using Insighto.Business.ValueObjects;
using Insighto.Data;

namespace Insighto.Business.Services
{
    public class BlockedWordsService : BusinessServiceBase
    {

        public List<osm_blockedwords> GetBlockedList()
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var blockWords = (from bw in dbCon.osm_blockedwords
                                  where bw.DELETED == 0
                                  select bw).ToList();

                return blockWords;
            }
        }

        public PagedList<osm_blockedwords> GetAllBlockedWords(PagingCriteria pageCriteria) 
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var blockWords = (from bw in dbCon.osm_blockedwords
                                  where bw.DELETED == 0
                                  select bw).ToPagedList(pageCriteria);

                return blockWords;
            }
        }

        public osm_blockedwords FindBlockWordById(int BlockId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_blockedwords.Where(b => b.BLOCKEDWORD_ID == BlockId).SingleOrDefault();
            }
        }

        #region "Method: Update Blocked word"

        public int UpdateBlockedWord(int BlockedWordId,string BlockWordName,string Description)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var list = dbCon.osm_blockedwords.Where(b => b.BLOCKEDWORD_ID == BlockedWordId).SingleOrDefault();
                list.BLOCKEDWORD_NAME = BlockWordName;
                list.BLOCKEDWORD_DESCRIPTION = Description;
                list.DELETED = 0; 
              return dbCon.SaveChanges();
            }
        }
        #endregion

        public int InsertBlockedWord(osm_blockedwords blockedword)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var word = dbCon.osm_blockedwords.Where(b => b.BLOCKEDWORD_NAME == blockedword.BLOCKEDWORD_NAME && b.DELETED == 0).ToList();
                if (word.Count < 1)
                {
                    dbCon.osm_blockedwords.Add(blockedword);                   
                }
                     //int id = dbCon.SaveChanges();
                return dbCon.SaveChanges();
            }
        }

        public int DeleteBlockedWord(int BlockedWordId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var list = dbCon.osm_blockedwords.Where(b => b.BLOCKEDWORD_ID == BlockedWordId).SingleOrDefault();
                list.DELETED = 1;
                return dbCon.SaveChanges();
            }
        }
        #region Method: ViewBlockedWordDetails
        public PagedList<BlockedWordList> ViewBlockedWordDetails(PagingCriteria pagingCriteria)
        {
            var bSurveyInfo = new PagedList<BlockedWordList>();

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                bSurveyInfo = (from osmSBlock in dbCon.osm_surveyblockwords
                                join osurvey in dbCon.osm_survey on osmSBlock.SURVEY_ID equals osurvey.SURVEY_ID into leftJoin
                                from ljoin in leftJoin.DefaultIfEmpty()
                                join oUser in dbCon.osm_user on ljoin.USERID equals oUser.USERID
                                where osmSBlock.DELETED == 0 && ljoin.CHECK_BLOCKWORDS == 0
                                select new BlockedWordList
                               {
                                   SurveyId = osmSBlock.SURVEY_ID,
                                   SurveyName = ljoin.SURVEY_NAME,
                                   UserName =oUser.FIRST_NAME + " " +oUser.LAST_NAME,
                                   LoginName =oUser.LOGIN_NAME  
                                  
                               }
                        ).ToPagedList(pagingCriteria);

            }
            return bSurveyInfo;

        }
        #endregion


        #region Method: ViewSurveyBlockedWordList
        public PagedList<BlockedWordList> ViewSurveyBlockedWordList(PagingCriteria pagingCriteria,int SurveyId)
        {
            var bSurveyInfo = new PagedList<BlockedWordList>();

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                bSurveyInfo = (from osmSBlock in dbCon.osm_surveyblockwords
                               join osurvey in dbCon.osm_survey on osmSBlock.SURVEY_ID equals osurvey.SURVEY_ID into leftJoin
                               from ljoin in leftJoin.DefaultIfEmpty()
                               join oUser in dbCon.osm_user on ljoin.USERID equals oUser.USERID
                               where osmSBlock.DELETED == 0 && ljoin.CHECK_BLOCKWORDS == 0 && ljoin.SURVEY_ID ==SurveyId
                               select new BlockedWordList
                               {
                                   SurveyId = osmSBlock.SURVEY_ID,
                                   SurveyName = ljoin.SURVEY_NAME,
                                   UserName = oUser.FIRST_NAME + " " + oUser.LAST_NAME,
                                   LoginName = oUser.LOGIN_NAME,
                                   BlockeWords =osmSBlock.BLOCK_WORDS,
                                   FeildId =osmSBlock.FEILD_ID,
                                   FeildName =osmSBlock.FEILD_NAME 
 
                                }
                        ).ToPagedList(pagingCriteria);

            }
            return bSurveyInfo;

        }
        #endregion
    }
}
