﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Services;
using Insighto.Business.Extensions;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;

namespace Insighto.Business.Services
{
    public class AdminUserService : BusinessServiceBase
    {
        public PagedList<osm_adminuser> FindAdminUsers(PagingCriteria pagingCriteria, string searchKey)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (searchKey != null && searchKey != "")
                {
                    return dbCon.osm_adminuser.Where(a => a.DELETED == 0 && (a.FIRST_NAME.Contains(searchKey) || a.LAST_NAME.Contains(searchKey) || a.LOGIN_NAME.Contains(searchKey))).ToPagedList(pagingCriteria);
                }
                else
                return dbCon.osm_adminuser.Where(a => a.DELETED == 0).ToPagedList(pagingCriteria);
            }
        }

        public int SaveAdminUsers(osm_adminuser user)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                if (FindAdminUserByEmailId(user.LOGIN_NAME).Count < 1)
                {
                    dbCon.osm_adminuser.Add(user);                    
                }
                return dbCon.SaveChanges();
            }
        }

        public List<osm_adminuser> FindAdminUserByEmailId(string email)
        {

            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_adminuser.Where(u => u.LOGIN_NAME == email && u.DELETED == 0).ToList();
            }
        }

        public int DeleteUserById(int adminUserId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var adminUser = dbCon.osm_adminuser.Where(a => a.ADMINUSERID == adminUserId).SingleOrDefault();
                adminUser.DELETED = 1;
                return dbCon.SaveChanges();
            }
        }

        public osm_adminuser ResetPassword(int adminUserId, string password)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var adminUser = dbCon.osm_adminuser.Where(a => a.ADMINUSERID == adminUserId).SingleOrDefault();
                password = adminUser.LOGIN_NAME.Substring(0, 2) + password.Substring(0, 3) + "ins$";
                adminUser.RESET = 1;
                adminUser.PASSWORD = EncryptHelper.Encrypt(password);
                 dbCon.SaveChanges();
                 return adminUser;
            }
        }
        public osm_adminuser FindAdminUserByUserId(int adminUserId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_adminuser.Where(u => u.ADMINUSERID == adminUserId).SingleOrDefault();
            }
        }

        public int UpdateAdminUserByUserId(osm_adminuser user)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                int rtnValue = 0;
                var adminUser = dbCon.osm_adminuser.Where(u => u.LOGIN_NAME == user.LOGIN_NAME && u.ADMINUSERID != user.ADMINUSERID).ToList();
                if (adminUser.Count < 1)
                {
                    var admin = dbCon.osm_adminuser.Where(u => u.ADMINUSERID == user.ADMINUSERID).SingleOrDefault();
                    admin.LOGIN_NAME = user.LOGIN_NAME;
                    admin.FIRST_NAME = user.FIRST_NAME;
                    admin.LAST_NAME = user.LAST_NAME;
                    admin.ADMIN_TYPE = user.ADMIN_TYPE;
                    admin.PHONE = user.PHONE;
                    admin.MODIFIED_ON = DateTime.UtcNow;
                    dbCon.SaveChanges();
                    rtnValue = 1;
                }
                return rtnValue;
            }
        }
        public osm_adminuser GetAuthenticatedUser(string userName, string password)
        {

            password = EncryptHelper.Encrypt(password);
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var user = dbCon.osm_adminuser.Where(u => u.LOGIN_NAME == userName && u.PASSWORD == password && u.DELETED == 0).SingleOrDefault();
                return user;
            }         
        }

        public int UpdateAdminProfileByUserId(osm_adminuser user,string flag)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                int rtnValue = 0;
                if (flag == "N")
                {
                    var adminUser = dbCon.osm_adminuser.Where(u => u.LOGIN_NAME == user.LOGIN_NAME && u.ADMINUSERID != user.ADMINUSERID).ToList();
                    if (adminUser.Count < 1)
                    {
                        var admin = dbCon.osm_adminuser.Where(u => u.ADMINUSERID == user.ADMINUSERID).SingleOrDefault();
                        admin.FIRST_NAME = user.FIRST_NAME;
                        admin.LAST_NAME = user.LAST_NAME;
                        admin.MODIFIED_ON = DateTime.UtcNow;
                        rtnValue = dbCon.SaveChanges();
                    }
                }
                if (flag == "C")
                {
                    //var adminUser = dbCon.osm_adminuser.Where(u => u.LOGIN_NAME == user.LOGIN_NAME && u.ADMINUSERID != user.ADMINUSERID).ToList();
                    //if (adminUser.Count < 1)
                    //{
                        var admin = dbCon.osm_adminuser.Where(u => u.ADMINUSERID == user.ADMINUSERID).FirstOrDefault();
                        admin.PASSWORD = user.PASSWORD;
                        admin.MODIFIED_ON = DateTime.UtcNow;
                        admin.RESET = user.RESET;
                        rtnValue = dbCon.SaveChanges();
                    //}
                }
                return rtnValue;
            }
        }
    }
}
