﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using Insighto.Exceptions;

namespace Insighto.Business.Services
{
    public class ImageService :  BusinessServiceBase
    {
        #region "AddImage"
        public int AddImage(osm_images osmImages)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                osmImages.CREATE_DATE = DateTime.UtcNow;
                osmImages.DELETED = 0;
                dbCon.osm_images.Add(osmImages);
                dbCon.SaveChanges();

                return osmImages.IMAGE_ID;
            }
        }
        #endregion

        #region "Method : GetImageDetailsBySurveyId"

        public osm_images GetImageDetailsBySurveyId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var  osmImages = (from images in dbCon.osm_images where 
                                  images.SURVEY_ID == surveyId && images.DELETED == 0
                                  select images).OrderByDescending(im=>im.IMAGE_ID).ToList().First();
                return osmImages;
            }
        }
        #endregion
    }
}
