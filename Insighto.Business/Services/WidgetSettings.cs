﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Extensions;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;

namespace Insighto.Business.Services
{
    public class WidgetSettings : BusinessServiceBase
    {
        public osm_WidgetSettings AddWidgetSettings(osm_WidgetSettings osmWidgetSettings)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                dbCon.osm_WidgetSettings.Add(osmWidgetSettings);
                dbCon.SaveChanges();
                return osmWidgetSettings;
            }
        }

        public osm_WidgetSettings GetWidgetSettingssBySurveyId(int surveyId)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                return dbCon.osm_WidgetSettings.Where(w => w.SurveyId == surveyId).FirstOrDefault();

            }
        }
    }
}
