﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Services;
using Insighto.Data;
using System.Collections;
using Insighto.Business.Helpers;
using System.Configuration;

namespace Insighto.Business.Services
{
    public class SendEmailService : BusinessServiceBase
    {
        public void ForgotPassword(string emailType, string userName, string fromEmail, string password, string toEmail)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var emailDet = dbCon.osm_emaildet.Where(e => e.EMAIL_TYPE == emailType && e.DELETED == 0).SingleOrDefault();
                if (emailDet != null)
                {
                    string subject = emailDet.EMAIL_SUBJECT;
                    string body = emailDet.EMAIL_BODY;
                    Hashtable ht = new Hashtable();
                    ht.Add("$$USER_NAME$$", userName);
                    ht.Add("$$LOGIN_NAME$$", toEmail);
                    ht.Add("$$PASSWORD$$", password);
                    body = ContentParser(body, ht);
                    MailHelper.SendMailMessage(fromEmail, toEmail, "", "", subject, body);
                }
            }
        }


        public void ChangePassword(string emailType, string userName, string fromEmail, string toEmail)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var emailDet = dbCon.osm_emaildet.Where(e => e.EMAIL_TYPE == emailType && e.DELETED == 0).SingleOrDefault();
                if (emailDet != null)
                {
                    string subject = emailDet.EMAIL_SUBJECT;
                    string body = emailDet.EMAIL_BODY;
                    Hashtable ht = new Hashtable();
                    ht.Add("$$USER_NAME$$", userName);
                    body = ContentParser(body, ht);
                    MailHelper.SendMailMessage(fromEmail, toEmail, "", "", subject, body);
                }
            }
        }


        public void RegIstrationEmailAlert(string emailType, int userId, string toEmail, string fromEmail)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var emailDet = dbCon.osm_emaildet.Where(e => e.EMAIL_TYPE == emailType && e.DELETED == 0).SingleOrDefault();
                var userDetails = dbCon.osm_user.Where(u => u.USERID == userId).FirstOrDefault();
                if (emailDet != null)
                {
                    string paymentType = string.Empty;
                    if (userDetails.PAYMENT_TYPE == 4)
                        paymentType = "Direct Deposit";
                    if (userDetails.PAYMENT_TYPE == 2)
                        paymentType = "Pay by Cheque";

                    string subject = emailDet.EMAIL_SUBJECT;
                    string body = emailDet.EMAIL_BODY;
                    Hashtable ht = new Hashtable();
                    ht.Add("$$USER_NAME$$", userDetails.FIRST_NAME + " " + userDetails.LAST_NAME);
                    ht.Add("$$PLAN_OPTED$$", userDetails.LICENSE_OPTED);
                    ht.Add("$$PAYMENT_TYPE$$", paymentType);
                    body = ContentParser(body, ht);
                    MailHelper.SendMailMessage(fromEmail, toEmail, "", "", subject, body);
                }
            }
        }

        public void MaintainenceEmailAlert(string fromEmail)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var emailIDs = ConfigurationManager.AppSettings["MaintainanceEmails"].ToString();
                string toEmail = emailIDs.Split(',')[0];
                string ccEmail = emailIDs.Split(',')[1];
                string subject = "Maintenence comm needed";
                string body ="<b>User Email Address: </b>  "+ fromEmail;
                MailHelper.SendMailMessage(fromEmail, toEmail, "", ccEmail, subject, body);

            }
        }

        public void BlockedWordsAlert(string fromEmail,string surveyname,string surveypreviewurl)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var emailIDs = ConfigurationManager.AppSettings["MaintainanceEmails"].ToString();
                string toEmail = emailIDs.Split(',')[0];
                string ccEmail = emailIDs.Split(',')[1];
                string subject = "Survey with blocked words";
                string body = "<b>Survey Name: </b>  " + surveyname + "<br>" + "<b>Survey PreviewURL: </b>" + surveypreviewurl;
                MailHelper.SendMailMessage(fromEmail, toEmail, "", ccEmail, subject, body);

            }
        }

        public string ContentParser(string Contnt, Hashtable ht)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Contnt);
            if (ht.Count > 0 && Contnt.Trim().Length > 0)
            {
                foreach (string key in ht.Keys)
                {
                    while (sb.ToString().Contains(key))
                    {
                        sb.Replace(key, Convert.ToString(ht[key]));
                    }
                }
            }
            return sb.ToString();
        }

    }
}
