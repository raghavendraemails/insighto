﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    public class PagingCriteria
    {
        public string SortColumn { get; set; }
        public string  SortOrder { get; set; }
        public int PageIndex { get; set; }
        public int PageSize  { get; set; }

        public string SortString
        {
            get
            {
                return String.Format("{0} {1}", SortColumn, SortOrder).Trim();
            }
        }
    }
    
}
