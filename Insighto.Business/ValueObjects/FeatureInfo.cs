﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    public class FeatureInfo
    {
        public string Feature { get; set; }
        public int? Value { get; set; }

        public string FeatureGroup { get; set; }
        public string FeatureName { get; set; }
        public string Free_Licensetype { get; set; }
        public string Prof_Licensetype { get; set; }
        public string Crop_Licensetype { get; set; }
        public decimal PK_FEATURE_ID { get; set; }
    }
}
