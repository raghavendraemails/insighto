﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    public class ContactListInfo
    {
        public int CONTACTLIST_ID { get; set; }
        public string CONTACTLIST_NAME { get; set; }
        public string LIST_TYPE { get; set; }
        public Nullable<int> NO_EMAILIDS { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<System.DateTime> LAST_MODIFIED_ON { get; set; }
        public string CUSTOM_HEADER1 { get; set; }
        public string CUSTOM_HEADER2 { get; set; }
        public string CUSTOM_HEADER3 { get; set; }
        public string CUSTOM_HEADER4 { get; set; }
        public string CUSTOM_HEADER5 { get; set; }
        public string CUSTOM_HEADER6 { get; set; }
        public string CUSTOM_HEADER7 { get; set; }
        public string CUSTOM_HEADER8 { get; set; }
        public Nullable<int> DELETED { get; set; }
        public Nullable<int> CREATED_BY { get; set; }
        public Nullable<int> LASTMODIFIED_BY { get; set; }
        public string contactUrl { get; set; }
        public string editUrl { get; set; }
        public string passHistoryUrl { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }

    }
}
