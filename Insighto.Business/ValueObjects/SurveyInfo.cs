﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;

namespace Insighto.Business.ValueObjects
{
    public class SurveyInfo
    {
        public osm_survey Survey { get; set; }
        public string Url { get; set; }
        public string ReportUrl { get; set; }
        public string SurveyUrl { get; set; }
        public int SurveyID { get; set; }
        public string SurveyEditUrl { get; set; }
        public string FQurl { get; set; }
        public string Preview { get; set; }
        public string PreviewLink { get; set; }
        public string ASEditUrl { get; set; }
        public DateTime? Launchon { get; set; }
        public int Launchflag { get; set; }
        public string SendRemainderUrl { get; set; }
        public string LaunchUrl { get; set; }
        public string SendRemainderText { get; set; }
        public string SurveyManagerUrl { get; set; }
        public int FOLDER_ID { get; set; }
        public Nullable<int> CHECK_BLOCKWORDS { get; set; }
        public string SURVEY_NAME { get; set; }
        public Nullable<int> TOTAL_RESPONSES { get; set; }
        public Nullable<int> SEND_TO { get; set; }
        public decimal RESPONSE_RATE { get; set; }
        public string NAME { get; set; }
        public int USERID { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public string  MODIFIED_ON { get; set; }
        public Nullable<System.DateTime> LAUNCHED_ON { get; set; }
        public Nullable<int> Complete { get; set; }
        public Nullable<int> Partial { get; set; }
        public Nullable<int> EMAILLIST_TYPE { get; set; }
        public string FOLDER_NAME { get; set; }
        public string SURVEY_CATEGORY { get; set; }
        public string THEME { get; set; }
        public string Status { get; set; }
        public string appendedUrls { get; set; }
        public string survey_type { get; set; }
        public string UpgradeUrl { get; set; }

    }
}
