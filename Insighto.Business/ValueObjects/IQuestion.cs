﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    interface IQuestion<T>
    {
        T Question { get; }
    }

    interface IMutableQuestion<T>
    {
        T Question { get; set; }
    }    
}

