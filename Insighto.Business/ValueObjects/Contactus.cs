﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    public class Contactus
    {
        public string  USERNAME { get; set; }
        public string  EMAILADDRESS { get; set; }
        public string  PHONENO { get; set; }
        public string ATTENTIONVALUE { get; set; }
        public string MAILMESSAGE { get; set; }
        public string ORGANISATION { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<int> DELETED { get; set; }
        public int CONTACTUSUSERID { get; set; }
    }
}
