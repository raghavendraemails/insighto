﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;

namespace Insighto.Business.ValueObjects
{
    public class EntityInfo
    {
        public int pk_pollid { get; set; }
        public string name { get; set; }
        public string modified_on { get; set; }
        public string created_on { get; set; }
        public string closed_on { get; set; }
        public string launched_on { get; set; }
        public string type { get; set; }
        public string Status { get; set; }
        public int Partial { get; set; }
        public int Complete { get; set; }
        public string appendedUrls { get; set; }
        public string PollEditUrl { get; set; }
        public string FQurl { get; set; }
        public string Preview { get; set; }
        public string PreviewLink { get; set; }
        public string ASEditUrl { get; set; }
        public string ReportUrl { get; set; }
        public string PollManagerUrl { get; set; }
        public string LaunchUrl { get; set; }
        public Nullable<int> TOTAL_RESPONSES { get; set; }
        public Nullable<int> VISITS { get; set; }
        public Nullable<int> PARTIALS { get; set; }
        public Nullable<int> COMPLETES { get; set; }
        public string UpgradeUrl { get; set; }
        public string PollUrl { get; set; }
        public int framewidth { get; set; }
        public int frameheight { get; set; }
        public List<PollAnswerOptions> pollansweroptions { get; set; }
        public string PollIcon { get; set; }
        public string question { get; set; }
        public int answercount { get; set; }
        public int ActiveSince { get; set; }
        public string EmbedCode { get; set; }
        public string VideoLink { get; set; }
        public string ImageLink { get; set; }
        public string IsImagePoll { get; set; }
        public string IsVideoPoll { get; set; }
        public string ReplaceUrl { get; set; }
        public string WebLinkUrl { get; set; }
        public string EntityType { get; set; }
    }
}
