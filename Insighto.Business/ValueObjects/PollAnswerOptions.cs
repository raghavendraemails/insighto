﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    public class PollAnswerOptions
    {
        public int answerid { get; set; }
        public string answeroption { get; set; }
        public int answercount { get; set; }
    }
}
