﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text;
using Insighto.Data;
namespace Insighto.Business.ValueObjects
{
    public class SurveyDashboard
    {
        public osm_survey Survey { get; set; }
        public string SurveyName { get; set; }
        public string CategoryName { get; set; }
        public string Status { get; set; }
        public int? SendTo { get; set; }
        public int? Responses { get; set; }
        public int SurveyId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string url { get; set; }
    }
}
