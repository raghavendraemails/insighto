﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    public class LoggedInAdminUserInfo
    {
        public decimal AdminUserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Reset { get; set; }
        public string LoginName { get; set; }
        public int StandardBIAS { get; set; }
        public string TimeZone { get; set; }
        public string AdminType { get; set; }
        public string Password { get; set; }
    }
}
