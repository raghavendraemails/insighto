﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;

namespace Insighto.Business.ValueObjects
{
    public class SurveyResponse
    {
        public osm_responsequestions ResponseQuestion { get; set; }
    }
}
