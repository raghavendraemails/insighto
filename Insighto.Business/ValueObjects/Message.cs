﻿
namespace Insighto.Business.ValueObjects
{
    public class Message
    {
        public string status { get; set; }

        public string created { get; set; }

        public string reason { get; set; }

        public string email { get; set; }


    }
}
