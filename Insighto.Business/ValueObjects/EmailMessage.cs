﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    public class EmailMessage
    {
        private string mstatus;
        private string mcreated;
        private string mreason;
        private string memail;

        public EmailMessage(string  status, string created,string reason,string email) 
    { 
        mstatus = status; 
        mcreated  = created;
        mreason = reason;
        memail = email;

    }  
  
 
    
    public string status 
    {
        get { return mstatus; }
        set { mstatus = value; } 
    }


    public string created
    {
        get { return mcreated; }
        set { mcreated = value; }
    }



    public string reason
    {
        get { return mreason; }
        set { mreason = value; }
    }

  
    public string email
    {
        get { return memail ; }
        set { memail = value; }
    } 

    }
}
