﻿using System;
using Insighto.Data;

namespace Insighto.Business.ValueObjects
{
    public class LoggedInUserInfo
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Reset { get; set; }
        public osm_accountsetting AccountSetting { get; set; }
        //public int BROWSER_BACKBUTTON_STATUS { get; set; }
        public string LicenseType { get; set; }
        public string LoginName { get; set; }
        public int StandardBIAS { get; set; }
        public string TimeZone { get; set; }
        public string LicenseOpted { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int? ActivationFlag { get; set; }
        
    }
}
