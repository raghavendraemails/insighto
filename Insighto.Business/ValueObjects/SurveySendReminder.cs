﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;

namespace Insighto.Business.ValueObjects
{
    public class SurveySendReminder
    {
        public int SURVEY_ID { get; set; }
        public string SURVEY_NAME { get; set; }
        public Nullable<int> Unique_respondent { get; set; }
    }
}
