﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;
namespace Insighto.Business.ValueObjects
{
   public class BlockedWordList
    {
        public osm_blockedwords  osmBlockedWords { get; set; }
        public int SurveyId { get; set; }
        public string SurveyName { get; set; }
        public string UserName { get; set; }
        public string LoginName { get; set; }
        public string FeildName { get; set; }
        public string BlockeWords { get; set; }
        public int FeildId { get; set; }
        
    }
}
