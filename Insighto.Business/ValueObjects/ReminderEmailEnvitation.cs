﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;

namespace Insighto.Business.ValueObjects
{
    public class ReminderEmailEnvitation
    {
        public int LAUNCH_ID { get; set; }
        public int SURVEY_ID { get; set; }
        public Nullable<DateTime> LAUNCHED_ON { get; set; }
        public int CONTACTLIST_ID { get; set; }
        public int SEND_TO { get; set; }
        public int TOTAL_RESPONSES { get; set; }
        public int PARTIAL_RESPONSES { get; set; }
        public int SCREEN_OUTS { get; set; }
        public int STATUS { get; set; }
        public int DELETED { get; set; }
        public string FROM_MAILADDRESS { get; set; }
        public string MAIL_SUBJECT { get; set; }
        public string EMAIL_INVITATION { get; set; }
        public Nullable<DateTime> CREATED_ON { get; set; }
        public int LAUNCHMAIL_STATUS { get; set; }
        public string SENDER_NAME { get; set; }
        public string REPLY_TO_EMAILADDRESS { get; set; }
        public Nullable<int> EMAIL_COUNT { get; set; }
        public string CONTACTLIST_NAME { get; set; }
        public Nullable<int> COMPLETES { get; set; }
        public Nullable<int> REMINDER_COUNT { get; set; }
        public Nullable<int> UNIQUE_RESPONDENT { get; set; }
        public string CONLIST_CONTACTLIST_NAME { get; set; }
        public Nullable<DateTime> CONLIST_CREATED_ON { get; set; }
        public Nullable<int> CONLIST_DELETED { get; set; }
        public int EmailCount { get; set; }
    }
}
