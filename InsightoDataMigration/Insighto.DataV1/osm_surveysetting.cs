//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Insighto.DataV1
{
    using System;
    using System.Collections.Generic;
    
    public partial class osm_surveysetting
    {
        public int SURVEYSETTING_ID { get; set; }
        public int SURVEY_ID { get; set; }
        public string SURVEY_HEADER { get; set; }
        public string SURVEY_FOOTER { get; set; }
        public string SURVEY_FONT_TYPE { get; set; }
        public string SURVEY_FONT_SIZE { get; set; }
        public string SURVEY_FONT_COLOR { get; set; }
        public string SURVEY_BUTTON_TYPE { get; set; }
        public string CUSTOMSTART_TEXT { get; set; }
        public string CUSTOMSUBMITTEXT { get; set; }
        public int DELETED { get; set; }
        public int PAGE_BREAK { get; set; }
        public int RESPONSE_REQUIRED { get; set; }
        public Nullable<int> BROWSER_BACKBUTTON { get; set; }
        public string SURVEY_LOGO { get; set; }
    }
}
