//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Insighto.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class osm_HelpPage
    {
        public int HelpPageId { get; set; }
        public string PageName { get; set; }
        public string PageContent { get; set; }
        public string Category { get; set; }
        public Nullable<int> ParentId { get; set; }
        public Nullable<int> Order { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
