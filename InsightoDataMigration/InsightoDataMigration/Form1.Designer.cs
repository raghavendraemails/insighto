﻿namespace InsightoDataMigration
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGet = new System.Windows.Forms.Button();
            this.lblUserCount = new System.Windows.Forms.Label();
            this.lblContactListCount = new System.Windows.Forms.Label();
            this.lblEmailListCount = new System.Windows.Forms.Label();
            this.btnMigrateSurvey = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblQuestions = new System.Windows.Forms.Label();
            this.lblControls = new System.Windows.Forms.Label();
            this.lblIntro = new System.Windows.Forms.Label();
            this.lblSurveyUsers = new System.Windows.Forms.Label();
            this.lblSurveysCount = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGet
            // 
            this.btnGet.Location = new System.Drawing.Point(25, 109);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(75, 23);
            this.btnGet.TabIndex = 0;
            this.btnGet.Text = "Get Records";
            this.btnGet.UseVisualStyleBackColor = true;
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // lblUserCount
            // 
            this.lblUserCount.AutoSize = true;
            this.lblUserCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserCount.Location = new System.Drawing.Point(46, 13);
            this.lblUserCount.Name = "lblUserCount";
            this.lblUserCount.Size = new System.Drawing.Size(81, 13);
            this.lblUserCount.TabIndex = 1;
            this.lblUserCount.Text = "User Count:0";
            // 
            // lblContactListCount
            // 
            this.lblContactListCount.AutoSize = true;
            this.lblContactListCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContactListCount.Location = new System.Drawing.Point(46, 41);
            this.lblContactListCount.Name = "lblContactListCount";
            this.lblContactListCount.Size = new System.Drawing.Size(123, 13);
            this.lblContactListCount.TabIndex = 2;
            this.lblContactListCount.Text = "Contact List Count:0";
            // 
            // lblEmailListCount
            // 
            this.lblEmailListCount.AutoSize = true;
            this.lblEmailListCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailListCount.Location = new System.Drawing.Point(46, 69);
            this.lblEmailListCount.Name = "lblEmailListCount";
            this.lblEmailListCount.Size = new System.Drawing.Size(109, 13);
            this.lblEmailListCount.TabIndex = 3;
            this.lblEmailListCount.Text = "Email List Count:0";
            // 
            // btnMigrateSurvey
            // 
            this.btnMigrateSurvey.Location = new System.Drawing.Point(49, 156);
            this.btnMigrateSurvey.Name = "btnMigrateSurvey";
            this.btnMigrateSurvey.Size = new System.Drawing.Size(75, 23);
            this.btnMigrateSurvey.TabIndex = 4;
            this.btnMigrateSurvey.Text = "Get Surveys";
            this.btnMigrateSurvey.UseVisualStyleBackColor = true;
            this.btnMigrateSurvey.Click += new System.EventHandler(this.btnMigrateSurvey_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblEmailListCount);
            this.groupBox1.Controls.Add(this.btnGet);
            this.groupBox1.Controls.Add(this.lblUserCount);
            this.groupBox1.Controls.Add(this.lblContactListCount);
            this.groupBox1.Location = new System.Drawing.Point(76, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(271, 143);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Address Book";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.lblQuestions);
            this.groupBox2.Controls.Add(this.lblControls);
            this.groupBox2.Controls.Add(this.lblIntro);
            this.groupBox2.Controls.Add(this.lblSurveyUsers);
            this.groupBox2.Controls.Add(this.btnMigrateSurvey);
            this.groupBox2.Controls.Add(this.lblSurveysCount);
            this.groupBox2.Location = new System.Drawing.Point(76, 173);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(271, 262);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Surveys";
            // 
            // lblQuestions
            // 
            this.lblQuestions.AutoSize = true;
            this.lblQuestions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuestions.Location = new System.Drawing.Point(46, 116);
            this.lblQuestions.Name = "lblQuestions";
            this.lblQuestions.Size = new System.Drawing.Size(111, 13);
            this.lblQuestions.TabIndex = 8;
            this.lblQuestions.Text = "Questions Count:0";
            // 
            // lblControls
            // 
            this.lblControls.AutoSize = true;
            this.lblControls.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblControls.Location = new System.Drawing.Point(46, 93);
            this.lblControls.Name = "lblControls";
            this.lblControls.Size = new System.Drawing.Size(97, 13);
            this.lblControls.TabIndex = 7;
            this.lblControls.Text = "ControlsCount:0";
            // 
            // lblIntro
            // 
            this.lblIntro.AutoSize = true;
            this.lblIntro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntro.Location = new System.Drawing.Point(46, 69);
            this.lblIntro.Name = "lblIntro";
            this.lblIntro.Size = new System.Drawing.Size(123, 13);
            this.lblIntro.TabIndex = 3;
            this.lblIntro.Text = "Introduction Count:0";
            // 
            // lblSurveyUsers
            // 
            this.lblSurveyUsers.AutoSize = true;
            this.lblSurveyUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurveyUsers.Location = new System.Drawing.Point(46, 13);
            this.lblSurveyUsers.Name = "lblSurveyUsers";
            this.lblSurveyUsers.Size = new System.Drawing.Size(81, 13);
            this.lblSurveyUsers.TabIndex = 1;
            this.lblSurveyUsers.Text = "User Count:0";
            // 
            // lblSurveysCount
            // 
            this.lblSurveysCount.AutoSize = true;
            this.lblSurveysCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurveysCount.Location = new System.Drawing.Point(46, 41);
            this.lblSurveysCount.Name = "lblSurveysCount";
            this.lblSurveysCount.Size = new System.Drawing.Size(100, 13);
            this.lblSurveysCount.TabIndex = 2;
            this.lblSurveysCount.Text = "Surveys Count:0";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(190, 156);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Get Surveys";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 447);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.Label lblUserCount;
        private System.Windows.Forms.Label lblContactListCount;
        private System.Windows.Forms.Label lblEmailListCount;
        private System.Windows.Forms.Button btnMigrateSurvey;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblIntro;
        private System.Windows.Forms.Label lblSurveyUsers;
        private System.Windows.Forms.Label lblSurveysCount;
        private System.Windows.Forms.Label lblControls;
        private System.Windows.Forms.Label lblQuestions;
        private System.Windows.Forms.Button button1;
    }
}

