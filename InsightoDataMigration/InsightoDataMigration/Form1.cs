﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using System.Text.RegularExpressions;
using Insighto.Business.Helpers;
using System.Configuration;
using System.Reflection;
//using Insighto.DataV1;


namespace InsightoDataMigration
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            var userDetails = ServiceFactory.GetService<UsersService>().GetUserDetails();
            lblUserCount.Text = "User Count:" + userDetails.Count;
            int listCount = 0;
            int emailCount = 0;
            foreach (var user in userDetails)
            {
                var v2UserDetails = ServiceFactory.GetService<UserServiceV2>().GetUserDetailsByLoginName(user.LOGIN_NAME);
                var contactDetails = ServiceFactory.GetService<UsersService>().GetContactDetailsByUserId(user.USERID);

                foreach (var contact in contactDetails)
                {
                    osm_contactlist cl = new osm_contactlist();
                    cl.CONTACTLIST_NAME = contact.CONTACTLIST_NAME;
                    cl.LIST_TYPE = contact.LIST_TYPE;
                    cl.NO_EMAILIDS = contact.NO_EMAILIDS;
                    cl.CREATED_ON = contact.CREATED_ON;
                    cl.LAST_MODIFIED_ON = contact.LAST_MODIFIED_ON;
                    cl.CUSTOM_HEADER1 = contact.CUSTOM_HEADER1;
                    cl.CUSTOM_HEADER2 = contact.CUSTOM_HEADER2;
                    cl.CUSTOM_HEADER3 = contact.CUSTOM_HEADER3;
                    cl.CUSTOM_HEADER4 = contact.CUSTOM_HEADER4;
                    cl.CUSTOM_HEADER5 = contact.CUSTOM_HEADER5;
                    cl.CUSTOM_HEADER6 = contact.CUSTOM_HEADER6;
                    cl.CUSTOM_HEADER7 = contact.CUSTOM_HEADER7;
                    cl.CUSTOM_HEADER8 = contact.CUSTOM_HEADER8;
                    cl.DELETED = 0;
                    cl.CREATED_BY = v2UserDetails.USERID;
                    cl.LASTMODIFIED_BY = v2UserDetails.USERID;
                    var v2ContactList = ServiceFactory.GetService<UserServiceV2>().SaveContactList(cl);
                    var emailDetails = ServiceFactory.GetService<UsersService>().GetEmailDetailsByContactId(contact.CONTACTLIST_ID);
                    foreach (var emaillData in emailDetails)
                    {
                        osm_emaillist el = new osm_emaillist();
                        el.EMAIL_ADDRESS = emaillData.EMAIL_ADDRESS;
                        el.CONTACTLIST_ID = v2ContactList.CONTACTLIST_ID;
                        el.FIRST_NAME = emaillData.FIRST_NAME;
                        el.LAST_NAME = emaillData.LAST_NAME;
                        el.CUSTOM_VAR1 = emaillData.CUSTOM_VAR1;
                        el.CUSTOM_VAR2 = emaillData.CUSTOM_VAR2;
                        el.CUSTOM_VAR3 = emaillData.CUSTOM_VAR3;
                        el.CUSTOM_VAR4 = emaillData.CUSTOM_VAR4;
                        el.CUSTOM_VAR5 = emaillData.CUSTOM_VAR5;
                        el.CUSTOM_VAR6 = emaillData.CUSTOM_VAR6;
                        el.CUSTOM_VAR7 = emaillData.CUSTOM_VAR7;
                        el.CUSTOM_VAR8 = emaillData.CUSTOM_VAR8;
                        el.DELETED = 0;
                        el.MODIFIED_ON = emaillData.MODIFIED_ON;
                        el.STATUS = emaillData.STATUS;
                        el.CREATED_BY = v2UserDetails.USERID;
                        el.LAST_MODIFIED_BY = v2UserDetails.USERID;
                        var email = ServiceFactory.GetService<UserServiceV2>().SaveEmails(el);
                        emailCount++;
                    }
                    //System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\test.txt",true);
                    //file.WriteLine(contact.CONTACTLIST_ID + "----" + emailDetails.Count + "----" + emailCount);
                    //file.Close();
                    //j++;
                    listCount++;
                }
                //i++;
            }
            lblContactListCount.Text = "Contact List Migrated:" + listCount;// +" " + i + " times";
            lblEmailListCount.Text = "Email List Migrated:" + emailCount;// +" " + j + " times";


        }

        private void btnMigrateSurvey_Click(object sender, EventArgs e)
        {
            List<Insighto.DataV1.osm_user> userDetails = ServiceFactory.GetService<UsersService>().GetUserDetails();
            foreach (Insighto.DataV1.osm_user user in userDetails)
            {
                //Fetching surveys by user Id from V1
                List<Insighto.DataV1.osm_survey> surveyDetailsV1 = GetSurveysByUserId(user.USERID);
                //Fetching folders by user Id from V1
                List<Insighto.DataV1.osm_cl_folders> folderDetailsV1 = GetFoldersByUserId(user.USERID);

                foreach (Insighto.DataV1.osm_cl_folders folder in folderDetailsV1)
                {
                    //checks folder exists or not by login name and folder name in V2
                    var folderExists = ServiceFactory.GetService<SurveyServiceV2>().GetFolderByUserNameFolderName(user.LOGIN_NAME, folder.FOLDER_NAME);
                    if (folderExists == null)
                    {
                        osm_cl_folders folderV2 = GetFolderProperty(folder);
                        folderV2 = ServiceFactory.GetService<SurveyServiceV2>().SaveFolder(folderV2);
                    }
                    else
                    {
                        //write into text file
                    }
                }
                foreach (Insighto.DataV1.osm_survey survey in surveyDetailsV1)
                {
                    //checks survey exists or not by login name and survey name in V2
                    var surveyExists = ServiceFactory.GetService<SurveyServiceV2>().GetSurveyByUserIdSurveyName(user.LOGIN_NAME, survey.SURVEY_NAME);
                    if (surveyExists == null)
                    {
                        //Assigns v1 survey properties to v2 survey properties
                        osm_survey surveyV2 = GetSurveyPropery(survey);

                        //inserts v2 survey properies into v2 database
                        surveyV2 = ServiceFactory.GetService<SurveyServiceV2>().SaveSurvey(surveyV2);

                        //Fetching survey intro by survey Id from V1
                        Insighto.DataV1.osm_surveyIntro surveyIntro = GetSurveyIntroBySurveyId(survey.SURVEY_ID).FirstOrDefault();
                        if (surveyIntro != null)
                        {
                            //Assigns v1 survey intro properties to v2 survey intro properties
                            osm_surveyIntro surveyIntroV2 = GetSurveyIntroProperty(surveyIntro);
                            //inserts v2 survey intro properies into v2 database
                            surveyIntroV2 = ServiceFactory.GetService<SurveyServiceV2>().SaveSurveyIntro(surveyIntroV2);
                        }

                        //Fetching survey controls by survey Id from V1
                        Insighto.DataV1.osm_surveycontrols surveyControl = GetSurveyControlsBySurveyId(survey.SURVEY_ID).FirstOrDefault();
                        if (surveyControl != null)
                        {
                            //Assigns v1 survey control properties to v2 survey control properties
                            osm_surveycontrols surveyControlV2 = GetSurveyControlProperty(surveyControl);
                            //inserts v2 survey intro properies into v2 database
                            surveyControlV2 = ServiceFactory.GetService<SurveyServiceV2>().SaveSurveyControl(surveyControlV2);
                        }
                        //Fetching survey questions by survey Id from V1
                        List<Insighto.DataV1.osm_surveyquestion> surveyQuestion = GetSurveyQuestionsBySurveyId(survey.SURVEY_ID);
                        if (surveyQuestion != null)
                        {
                            foreach (var questionv1 in surveyQuestion)
                            {
                                //Assigns v1 survey qustion properties to v2 survey question properties
                                osm_surveycontrols surveyControlV2 = GetSurveyControlProperty(surveyControl);
                                //inserts v2 survey question properies into v2 database
                                surveyControlV2 = ServiceFactory.GetService<SurveyServiceV2>().SaveSurveyControl(surveyControlV2);
                            }
                        }
                        //Fetching survey launch by survey Id from V1
                        Insighto.DataV1.osm_surveylaunch surveyLaunchV1 = GetSurveyLaunchBySurveyId(survey.SURVEY_ID).FirstOrDefault();
                        if (surveyLaunchV1 != null)
                        {
                            //Assigns v1 survey launch properties to v2 survey launch properties
                            osm_surveylaunch surveyLaunchV2 = GetSurveyLaunchProperty(surveyLaunchV1);
                            //inserts v2 survey launch properies into v2 database
                            surveyLaunchV2 = ServiceFactory.GetService<SurveyServiceV2>().SaveSurveyLaunch(surveyLaunchV2);
                        }
                        //Fetching survey blocked words by survey Id from V1
                        List<Insighto.DataV1.osm_surveyblockwords> surveyBlockedWords = GetSurveyBlockedWordsBySurveyId(survey.SURVEY_ID);
                        if (surveyBlockedWords != null)
                        {
                            foreach (Insighto.DataV1.osm_surveyblockwords blockedWord in surveyBlockedWords)
                            {
                                //Assigns v1 survey blocked words properties to v2 survey blocked words properties
                                osm_surveyblockwords surveyBlockedWordsV2 = GetSurveyBlockedWordsProperty(blockedWord);
                                //inserts v2 survey blocked words properies into v2 database
                                ServiceFactory.GetService<SurveyServiceV2>().SaveSurveyBlockedWords(surveyBlockedWordsV2);
                            }
                        }
                        //Fetching survey respondents by survey Id from V1
                        var surveyResponses = GetSurveyResponseBySurveyId(survey.SURVEY_ID);

                        foreach (var question in surveyQuestion)
                        {
                            //Fetching survey answer optyion by question Id from V1
                            var surveyAnswerOption = GetSurveyAnserOptionsByQuestionId(question.QUESTION_ID);
                        }

                        foreach (var respondent in surveyResponses)
                        {
                            //Fetching survey respondent answers by respondent Id from V1
                            var respondentAnswers = GetSurveyAnserOptionsByQuestionId(respondent.RESPONDENT_ID);
                        }

                        List<Insighto.DataV1.osm_surveylaunch> surveysLaunchV1 = GetSurveyLaunchBySurveyId(survey.SURVEY_ID);
                        foreach (Insighto.DataV1.osm_surveylaunch launch in surveysLaunchV1)
                        {
                            //Fetching survey reminder by launch Id from V1
                            var surveyReminders = GetSurveyReminderBySurveyId(launch.LAUNCH_ID);
                        }

                    }
                    else
                    {
                        //write into text file
                    }
                }
            }

        }
        #region Fetching Data From V1Methods
        private List<Insighto.DataV1.osm_survey> GetSurveysByUserId(int userId)
        {
            return ServiceFactory.GetService<SurveyServiceV1>().GetSurveyDetailsByUserId(userId);
        }

        private List<Insighto.DataV1.osm_cl_folders> GetFoldersByUserId(int userId)
        {
            return ServiceFactory.GetService<SurveyServiceV1>().GetFolderDetailsByUserId(userId);
        }

        private List<Insighto.DataV1.osm_surveyIntro> GetSurveyIntroBySurveyId(int surveyId)
        {
            return ServiceFactory.GetService<SurveyServiceV1>().GetSurveyIntoBySurveyId(surveyId);
        }

        private List<Insighto.DataV1.osm_surveycontrols> GetSurveyControlsBySurveyId(int surveyId)
        {
            return ServiceFactory.GetService<SurveyServiceV1>().GetSurveyControlBySurveyId(surveyId);
        }

        private List<Insighto.DataV1.osm_surveyquestion> GetSurveyQuestionsBySurveyId(int surveyId)
        {
            return ServiceFactory.GetService<SurveyServiceV1>().GetSurveyQuestionsBySurveyId(surveyId);
        }

        private List<Insighto.DataV1.osm_answeroptions> GetSurveyAnserOptionsByQuestionId(int questionId)
        {
            return ServiceFactory.GetService<SurveyServiceV1>().GetSurveyAnswerOptionsByQuestionId(questionId);
        }

        private List<Insighto.DataV1.osm_surveylaunch> GetSurveyLaunchBySurveyId(int surveyId)
        {
            return ServiceFactory.GetService<SurveyServiceV1>().GetSurveyLaunchBySurveyId(surveyId);
        }

        private List<Insighto.DataV1.osm_surveyblockwords> GetSurveyBlockedWordsBySurveyId(int surveyId)
        {
            return ServiceFactory.GetService<SurveyServiceV1>().GetSurveyBlockedWordsBySurveyId(surveyId);
        }

        private List<Insighto.DataV1.osm_responsesurvey> GetSurveyResponseBySurveyId(int surveyId)
        {
            return ServiceFactory.GetService<SurveyServiceV1>().GetSurveyResponsesBySurveyId(surveyId);
        }

        private List<Insighto.DataV1.osm_responsequestions> GetSurveyResponseByRespondentId(int respondentId)
        {
            return ServiceFactory.GetService<SurveyServiceV1>().GetSurveyResponseAnswersByRespondentId(respondentId);
        }

        private List<Insighto.DataV1.osm_surveyreminder> GetSurveyReminderBySurveyId(int launchId)
        {
            return ServiceFactory.GetService<SurveyServiceV1>().GetSurveyRemiderByLaunchId(launchId);
        }
        #endregion

        #region FillMethods


        private osm_cl_folders GetFolderProperty(Insighto.DataV1.osm_cl_folders folderV1)
        {

            osm_cl_folders folderV2 = new osm_cl_folders();
            folderV2.USERID = GetV2UserIdByV1UserId(folderV1.USERID);
            folderV2.PARENT_ID = folderV1.PARENT_ID;
            folderV2.FOLDER_NAME = folderV1.FOLDER_NAME;
            folderV2.FOLDER_STATUS = folderV1.FOLDER_STATUS;
            folderV2.DELETED = folderV1.DELETED;
            return folderV2;
        }
        private osm_survey GetSurveyPropery(Insighto.DataV1.osm_survey surveyV1)
        {
            osm_survey surveyV2 = new osm_survey();
            surveyV2.SURVEY_NAME = surveyV1.SURVEY_NAME;
            surveyV2.SURVEY_CATEGORY = surveyV1.SURVEY_CATEGORY;
            surveyV2.FOLDER_ID = GetV2FodlerIdByV1FolderId(surveyV1.FOLDER_ID);
            surveyV2.USERID = GetV2UserIdByV1UserId(surveyV1.USERID);
            surveyV2.CREATED_ON = surveyV1.CREATED_ON;
            surveyV2.DELETED = surveyV1.DELETED;
            surveyV2.STATUS = surveyV1.STATUS;
            surveyV2.TEMPLATE_CATEGORY = surveyV1.TEMPLATE_CATEGORY;
            surveyV2.TEMPLATE_SUBCATEGORY = surveyV1.TEMPLATE_SUBCATEGORY;
            surveyV2.THEME = surveyV1.THEME;
            surveyV2.MODIFIED_ON = surveyV1.MODIFIED_ON;
            surveyV2.SURVEY_CLOSE_DATE = surveyV1.SURVEY_CLOSE_DATE;
            surveyV2.CHECK_BLOCKWORDS = surveyV1.CHECK_BLOCKWORDS;
            surveyV2.SurveyWidgetFlag = surveyV1.SurveyWidgetFlag;
            return surveyV2;
        }

        private osm_surveyIntro GetSurveyIntroProperty(Insighto.DataV1.osm_surveyIntro surveyIntroV1)
        {
            osm_surveyIntro surveyIntroV2 = new osm_surveyIntro();
            surveyIntroV2.SURVEY_ID = GetV2SurveyIdByV1SurveyId(surveyIntroV1.SURVEY_ID);
            surveyIntroV2.USERID = GetV2UserIdByV1UserId(surveyIntroV1.USERID);
            surveyIntroV2.SURVEY_INTRO = surveyIntroV1.SURVEY_INTRO;
            surveyIntroV2.STATUS = surveyIntroV1.STATUS;
            surveyIntroV2.PAGE_BREAK = surveyIntroV1.PAGE_BREAK;
            surveyIntroV2.CREATED_ON = surveyIntroV1.CREATED_ON;
            surveyIntroV2.DELETED = surveyIntroV1.DELETED;
            return surveyIntroV2;
        }

        private osm_surveycontrols GetSurveyControlProperty(Insighto.DataV1.osm_surveycontrols surveyControlsV1)
        {
            int surveyIdV2 = GetV2SurveyIdByV1SurveyId(surveyControlsV1.SURVEY_ID);
            string navresurlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "SurveyId=" + surveyIdV2 + "&Layout=1");
            osm_surveycontrols surveyControlsV2 = new osm_surveycontrols();
            surveyControlsV2.SURVEY_ID = surveyIdV2;
            surveyControlsV2.SCHEDULE_ON = surveyControlsV1.SCHEDULE_ON;
            surveyControlsV2.SCHEDULE_ON_ALERT = surveyControlsV1.SCHEDULE_ON_ALERT;
            surveyControlsV2.SCHEDULE_ON_CLOSE = surveyControlsV1.SCHEDULE_ON_CLOSE;
            surveyControlsV2.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS = surveyControlsV1.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS;
            surveyControlsV2.SCHEDULE_ON_CLOSE_ALERT = surveyControlsV1.SCHEDULE_ON_CLOSE_ALERT;
            surveyControlsV2.STATUS = surveyControlsV1.STATUS;
            surveyControlsV2.TIME_FOR_URL_REDIRECTION = surveyControlsV1.TIME_FOR_URL_REDIRECTION;
            surveyControlsV2.PARTIAL_RESPONSES = surveyControlsV1.PARTIAL_RESPONSES;
            surveyControlsV2.TOTAL_RESPONSES = surveyControlsV1.TOTAL_RESPONSES;
            surveyControlsV2.SEND_TO = surveyControlsV1.SEND_TO;
            surveyControlsV2.ONLAUNCH_ALERT = surveyControlsV1.ONLAUNCH_ALERT;
            surveyControlsV2.CLOSE_ON_NO_RESPONDENTS = surveyControlsV1.CLOSE_ON_NO_RESPONDENTS;
            surveyControlsV2.CONTINUOUS_SURVEY = surveyControlsV1.CONTINUOUS_SURVEY;
            surveyControlsV2.UNIQUE_RESPONDENT = surveyControlsV1.UNIQUE_RESPONDENT;
            surveyControlsV2.SAVE_AND_CONTINUE = surveyControlsV1.SAVE_AND_CONTINUE;
            surveyControlsV2.AUTOMATIC_THANKYOU_EMAIL = surveyControlsV1.AUTOMATIC_THANKYOU_EMAIL;
            surveyControlsV2.ALERT_ON_MIN_RESPONSES = surveyControlsV1.ALERT_ON_MIN_RESPONSES;
            surveyControlsV2.ALERT_ON_MAX_RESPONSES = surveyControlsV1.ALERT_ON_MAX_RESPONSES;
            surveyControlsV2.THANKYOU_PAGE = ClearHTML(surveyControlsV1.THANKYOU_PAGE);
            surveyControlsV2.SCREENOUT_PAGE = ClearHTML(surveyControlsV1.SCREENOUT_PAGE);
            surveyControlsV2.SCREENOUT_PAGE = ClearHTML(surveyControlsV1.SCREENOUT_PAGE);
            surveyControlsV2.CLOSE_PAGE = ClearHTML(surveyControlsV1.CLOSE_PAGE);
            surveyControlsV2.OVERQUOTA_PAGE = ClearHTML(surveyControlsV1.OVERQUOTA_PAGE);
            surveyControlsV2.UNSUBSCRIBED = surveyControlsV1.UNSUBSCRIBED;
            surveyControlsV2.EMAIL_INVITATION = surveyControlsV1.EMAIL_INVITATION;
            surveyControlsV2.SURVEY_PASSWORD = surveyControlsV1.SURVEY_PASSWORD;
            surveyControlsV2.SURVEY_URL = ConfigurationManager.AppSettings["RootURL"].ToString() + navresurlstr;
            surveyControlsV2.REMINDER_TEXT = surveyControlsV1.REMINDER_TEXT;
            surveyControlsV2.LAUNCH_ON_DATE = surveyControlsV1.LAUNCH_ON_DATE;
            surveyControlsV2.CLOSE_ON_DATE = surveyControlsV1.CLOSE_ON_DATE;
            surveyControlsV2.LAUNCHED_ON = surveyControlsV1.LAUNCHED_ON;
            surveyControlsV2.ALERT_ON_MIN_RESPONSES_BY_DATE = surveyControlsV1.ALERT_ON_MIN_RESPONSES_BY_DATE;
            surveyControlsV2.ALERT_ON_MAX_RESPONSES_BY_DATE = surveyControlsV1.ALERT_ON_MAX_RESPONSES_BY_DATE;
            surveyControlsV2.REMINDER_ON_DATE = surveyControlsV1.REMINDER_ON_DATE;
            surveyControlsV2.DELETED = surveyControlsV1.DELETED;
            surveyControlsV2.NO_OF_RESPONSES = surveyControlsV1.NO_OF_RESPONSES;
            surveyControlsV2.SEND_TO_EXT = surveyControlsV1.SEND_TO_EXT;
            surveyControlsV2.TOTAL_RESPONSES_EXT = surveyControlsV1.TOTAL_RESPONSES_EXT;
            surveyControlsV2.PARTIAL_RESPONSES_EXT = surveyControlsV1.PARTIAL_RESPONSES_EXT;
            surveyControlsV2.SCREEN_OUTS_EXT = surveyControlsV1.SCREEN_OUTS_EXT;
            surveyControlsV2.URL_REDIRECTION = surveyControlsV1.URL_REDIRECTION;
            surveyControlsV2.THANKUPAGE_TYPE = surveyControlsV1.THANKUPAGE_TYPE;
            surveyControlsV2.CLOSEPAGE_TYPE = surveyControlsV1.CLOSEPAGE_TYPE;
            surveyControlsV2.SCREENOUTPAGE_TYPE = surveyControlsV1.SCREENOUTPAGE_TYPE;
            surveyControlsV2.OVERQUOTAPAGE_TYPE = surveyControlsV1.OVERQUOTAPAGE_TYPE;
            surveyControlsV2.URLREDIRECTION_STATUS = surveyControlsV1.URLREDIRECTION_STATUS;
            surveyControlsV2.FINISHOPTIONS_STATUS = surveyControlsV1.FINISHOPTIONS_STATUS;
            surveyControlsV2.SURVEYLAUNCH_URL = ConfigurationManager.AppSettings["RootURL"].ToString() + navresurlstr;
            surveyControlsV2.CONTACTLIST_ID = GetV2ContactIdByV1ContactId(surveyControlsV1.SURVEY_ID, surveyControlsV1.CONTACTLIST_ID);
            surveyControlsV2.EMAILLIST_TYPE = surveyControlsV1.EMAILLIST_TYPE;
            surveyControlsV2.SURVEYURL_TYPE = surveyControlsV1.SURVEYURL_TYPE;
            surveyControlsV2.LAUNCHOPTIONS_STATUS = surveyControlsV1.LAUNCHOPTIONS_STATUS;
            surveyControlsV2.SURVEY_STATUS = surveyControlsV1.SURVEY_STATUS;
            surveyControlsV2.FROM_MAILADDRESS = surveyControlsV1.FROM_MAILADDRESS;
            surveyControlsV2.MAIL_SUBJECT = surveyControlsV1.MAIL_SUBJECT;
            surveyControlsV2.SENDER_NAME = surveyControlsV1.SENDER_NAME;
            surveyControlsV2.REPLY_TO_EMAILADDRESS = surveyControlsV1.REPLY_TO_EMAILADDRESS;
            surveyControlsV2.SURVEY_LAYOUT = 1;
            return surveyControlsV2;
        }

        private osm_surveyblockwords GetSurveyBlockedWordsProperty(Insighto.DataV1.osm_surveyblockwords surveyBlockedWordsV1)
        {
            osm_surveyblockwords surveyBlockedWordsV2 = new osm_surveyblockwords();
            surveyBlockedWordsV2.SURVEY_ID = GetV2SurveyIdByV1SurveyId(surveyBlockedWordsV1.SURVEY_ID);
            surveyBlockedWordsV2.FEILD_ID = surveyBlockedWordsV1.FEILD_ID;
            surveyBlockedWordsV2.FEILD_NAME = surveyBlockedWordsV1.FEILD_NAME;
            surveyBlockedWordsV2.BLOCK_WORDS = surveyBlockedWordsV1.BLOCK_WORDS;
            surveyBlockedWordsV2.DELETED = surveyBlockedWordsV1.DELETED;
            return surveyBlockedWordsV2;
        }

        private osm_surveylaunch GetSurveyLaunchProperty(Insighto.DataV1.osm_surveylaunch surveyLaunchV1)
        {
            osm_surveylaunch surveyLaunchV2 = new osm_surveylaunch();
            surveyLaunchV2.SURVEY_ID = GetV2SurveyIdByV1SurveyId(surveyLaunchV1.SURVEY_ID);
            surveyLaunchV2.LAUNCHED_ON = surveyLaunchV1.LAUNCHED_ON;
            surveyLaunchV2.CONTACTLIST_ID = GetV2ContactIdByV1ContactId(surveyLaunchV1.SURVEY_ID, surveyLaunchV1.CONTACTLIST_ID);
            surveyLaunchV2.SEND_TO = surveyLaunchV1.SEND_TO;
            surveyLaunchV2.TOTAL_RESPONSES = surveyLaunchV1.TOTAL_RESPONSES;
            surveyLaunchV2.PARTIAL_RESPONSES = surveyLaunchV1.PARTIAL_RESPONSES;
            surveyLaunchV2.SCREEN_OUTS = surveyLaunchV1.SCREEN_OUTS;
            surveyLaunchV2.STATUS = surveyLaunchV1.STATUS;
            surveyLaunchV2.DELETED = surveyLaunchV1.DELETED;
            surveyLaunchV2.FROM_MAILADDRESS = surveyLaunchV1.FROM_MAILADDRESS;
            surveyLaunchV2.MAIL_SUBJECT = surveyLaunchV1.MAIL_SUBJECT;
            surveyLaunchV2.EMAIL_INVITATION = surveyLaunchV1.EMAIL_INVITATION;
            surveyLaunchV2.CREATED_ON = surveyLaunchV1.CREATED_ON;
            surveyLaunchV2.LAUNCHMAIL_STATUS = surveyLaunchV1.LAUNCHMAIL_STATUS;
            surveyLaunchV2.SENDER_NAME = surveyLaunchV1.SENDER_NAME;
            surveyLaunchV2.REPLY_TO_EMAILADDRESS = surveyLaunchV1.REPLY_TO_EMAILADDRESS;
            surveyLaunchV2.EMAIL_COUNT = surveyLaunchV1.EMAIL_COUNT;
            surveyLaunchV2.CONTACTLIST_NAME = surveyLaunchV1.CONTACTLIST_NAME;
            surveyLaunchV2.COMPLETES = surveyLaunchV1.COMPLETES;
            surveyLaunchV2.REMINDER_COUNT = surveyLaunchV1.REMINDER_COUNT;
            surveyLaunchV2.UNIQUE_RESPONDENT = surveyLaunchV1.UNIQUE_RESPONDENT;
            surveyLaunchV2.SURVEY_LAYOUT = 1;
            return surveyLaunchV2;
        }

        #endregion
        #region Methods

        /// <summary>
        /// Gets the name of the v2 U ser id by v1 user.
        /// </summary>
        /// <param name="loginName">Name of the login.</param>
        /// <returns></returns>
        private int GetV2UserIdByV1UserId(int? userid)
        {
            var userV1 = ServiceFactory.GetService<SurveyServiceV1>().GetUserNameByUserId(userid).FirstOrDefault();
            var userV2 = ServiceFactory.GetService<SurveyServiceV2>().GetUserIdByUserName(userV1.LOGIN_NAME);
            return userV2.USERID;
        }

        /// <summary>
        /// Gets the name of the v2 fodler id by v1 folder.
        /// </summary>
        /// <param name="folderId">The folder id.</param>
        /// <returns></returns>
        private int GetV2FodlerIdByV1FolderId(int folderId)
        {
            var folderV1 = ServiceFactory.GetService<SurveyServiceV1>().GetFolderNameByFolderId(folderId).FirstOrDefault();
            var folderV2 = ServiceFactory.GetService<SurveyServiceV2>().GetFolderIdByFolderName(folderV1.FOLDER_NAME);
            return folderV2.FOLDER_ID;
        }

        private int GetV2SurveyIdByV1SurveyId(int? surveyId)
        {
            var surveyV1 = ServiceFactory.GetService<SurveyServiceV1>().GetSurveyNameBySurveyId(surveyId);
            int userIdV2 = GetV2UserIdByV1UserId(surveyV1.USERID);
            var surveyV2 = ServiceFactory.GetService<SurveyServiceV2>().GetSurveyIdBySurveyNameUserId(surveyV1.SURVEY_NAME, userIdV2);
            return surveyV2.SURVEY_ID;
        }
        private int GetV2ContactIdByV1ContactId(int surveyId, int contactId)
        {
            var surveyV1 = ServiceFactory.GetService<SurveyServiceV1>().GetSurveyNameBySurveyId(surveyId);
            int userIdV2 = GetV2UserIdByV1UserId(surveyV1.USERID);
            var contactV1 = ServiceFactory.GetService<UsersService>().GetContactDetailsByContactListId(contactId, surveyV1.USERID).FirstOrDefault();
            var contactV2 = ServiceFactory.GetService<UserServiceV2>().GetContactIdByUserIdConcatName(userIdV2, contactV1.CONTACTLIST_NAME);
            return contactV2.CONTACTLIST_ID;
        }

        /// <summary>
        /// Clears the HTML.
        /// </summary>
        /// <param name="htmlContent">Content of the HTML.</param>
        /// <returns></returns>
        public static string ClearHTML(string htmlContent)
        {
            string plainHTML = "";
            if (htmlContent != null)
            {
                string pattern1html = "<(.|\n)+?>";
                plainHTML = Regex.Replace(htmlContent, pattern1html, string.Empty);
                string patternHtml = "<.*?>";
                //string patternNewline = "\".";
                //string patternNbsp = "&.*";
                plainHTML = Regex.Replace(plainHTML, patternHtml, string.Empty);
                plainHTML = plainHTML.Replace("&lt;", "<");
                plainHTML = plainHTML.Replace("&gt;", ">");
                plainHTML = plainHTML.Replace("&quot;", "\"");
                plainHTML = plainHTML.Replace("&apos;", "'");
                plainHTML = plainHTML.Replace("&amp;", "&");
                plainHTML = plainHTML.Replace("&nbsp;", " ");
                plainHTML = plainHTML.Replace("&copy;", "\u00a9");

            }
            return (plainHTML);
        }


        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            List<Insighto.DataV1.osm_user> userDetailsFromV1 = ServiceFactory.GetService<UsersService>().GetUserDetails();
            List<Insighto.Data.osm_user> userDetailsFromV2 = new List<Insighto.Data.osm_user>();
            foreach (Insighto.DataV1.osm_user user in userDetailsFromV1)
            {
                Insighto.Data.osm_user user2 = new Insighto.Data.osm_user();
                MergeFrom(user2, user);
                userDetailsFromV2.Add(user2);
            }
        }

        /// <summary>
        /// Merges the equivalent properties from the source to this object.
        /// </summary>
        /// <typeparam name="T">Any reference type.</typeparam>
        /// <param name="destination">This object, which receives the values.</param>
        /// <param name="source">The source object that the values are taken from.</param>
        public static void MergeFrom<T>(object destination, T source)
        {
            Type destinationType = destination.GetType();
            PropertyInfo[] propertyInfos = source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var propertyInfo in propertyInfos)
            {
                PropertyInfo destinationPropertyInfo = destinationType.GetProperty(propertyInfo.Name, BindingFlags.Public | BindingFlags.Instance);
                if (destinationPropertyInfo != null)
                {
                    if (destinationPropertyInfo.CanWrite && propertyInfo.CanRead)
                    {
                        object o = propertyInfo.GetValue(source, null);
                        destinationPropertyInfo.SetValue(destination, o, null);
                    }
                }
            }
        }
    }
}
   