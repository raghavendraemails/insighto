using System;

namespace Insighto.Business.Charts
{
	/// <summary>
	/// Summary description for DataProvider.
	/// </summary>
	public enum DataProvider
	{
		Oracle,SqlServer,OleDb,Odbc,MySql
	}
}
