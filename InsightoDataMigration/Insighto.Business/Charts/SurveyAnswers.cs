using System;
using System.Collections.Generic;
using System.Text;

namespace Insighto.Business.Charts
{
    public class SurveyAnswers
    {
        int answerID, questionID, skipQuestioID,ResponseCount,DemograpicBlockID,ResponseReq;
        string AnswerOpts,PipingText;
        DateTime CreatedOn, LastModifiedOn;

        public int ANSWER_ID
        {
            get
            {
                return answerID;
            }
            set
            {
                answerID = value;
            }
        }
        public int QUESTION_ID
        {
            get
            {
                return questionID;
            }
            set
            {
                questionID = value;
            }
        }
        public int SKIP_QUESTION_ID
        {
            get
            {
                return skipQuestioID;
            }
            set
            {
                skipQuestioID = value;
            }
        }
        public int RESPONSE_COUNT
        {
            get
            {
                return ResponseCount;
            }
            set
            {
                ResponseCount = value;
            }
        }
        public int DEMOGRAPIC_BLOCKID
        {
            get
            {
                return DemograpicBlockID;
            }
            set
            {
                DemograpicBlockID = value;
            }
        }
        public string ANSWER_OPTIONS
        {
            get
            {
                return AnswerOpts;
            }
            set
            {
                AnswerOpts = value;
            }
        }
        public string PIPING_TEXT
        {
            get
            {
                return PipingText;
            }
            set
            {
                PipingText = value;
            }
        }
        public DateTime CREATED_ON
        {
            get
            {
                return CreatedOn;
            }
            set
            {
                CreatedOn = value;
            }
        }
        public DateTime LAST_MODIFIED_ON
        {
            get
            {
                return LastModifiedOn;
            }
            set
            {
                LastModifiedOn = value;
            }
        }
        public int RESPONSE_REQUIRED
        {
            get
            {
                return ResponseReq;
            }
            set
            {
                ResponseReq = value;
            }
        }
    }
}
