using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Sql;
using Microsoft.Win32;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Configuration;

namespace Insighto.Business.Charts
{
   public class ValidationCore
    {
        SqlConnection objsql = new SqlConnection();
        public static System.Web.SessionState.HttpSessionState Sstate = null;
        DBManager dbm = new DBManager();
        string connect = ConfigurationManager.ConnectionStrings["InsightoConnString"].ToString();

        public ValidationCore()
        {
           // dbm.OpenConnection(connect);
        }        
        public void ValidateQuestion(string controlID, int QuesType, HtmlTableCell cntrlCell, int AnsOptCnt, int CharLimit)
        {           
            RegularExpressionValidator Regex = new RegularExpressionValidator();
            Regex.ID = "ctrlRegex" + controlID;

            if (QuesType == 5)//Comments and suggeations
            {
                if (CharLimit == 0 || CharLimit < 0)
                    CharLimit = 2000;
                Regex.ControlToValidate = controlID;
                Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s!@$%&()-_+=;""<>?/.]{1," + CharLimit + "}";
                Regex.ErrorMessage = "You have exceeded the maximum character limit of " + CharLimit + ". Please revise your answer.";
                Regex.Display = ValidatorDisplay.Dynamic;

            }
            if (QuesType == 6)//Single row Question
            {
                if (CharLimit == 0 || CharLimit < 0)
                    CharLimit = 500;
                Regex.ControlToValidate = controlID;
                Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s!@$%&()-_+=;""<>?/.]{1," + CharLimit + "}";
                Regex.ErrorMessage = "You have exceeded the maximum character limit of " + CharLimit + ". Please revise your answer.";
                Regex.Display = ValidatorDisplay.Dynamic;
            }
            if (QuesType == 7)//Numeric Input
            {
                Regex.ControlToValidate = controlID;
                Regex.ValidationExpression = @"[0-9]{1,20}";
                Regex.ErrorMessage = " Only numeric value (numbers only) up to 20 digits allowed";
                Regex.Display = ValidatorDisplay.Dynamic;

            }
            if (QuesType == 20)
            {
                Regex.ControlToValidate = controlID;
                Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s!@$%&()-_+=;""<>?/.]{1}";
                Regex.ErrorMessage = "You have exceeded the maximum character limit of 500. Please revise your answer.";
                Regex.Display = ValidatorDisplay.Dynamic;

            }
            if (QuesType == 8)//Email address
            {
                Regex.ControlToValidate = controlID;
                Regex.ValidationExpression = @"[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}";
                Regex.ErrorMessage = "Invalid email address format";
                Regex.Display = ValidatorDisplay.Dynamic;

            }
            if (QuesType == 9)
            {
                if (CharLimit == 0 || CharLimit < 0)
                    CharLimit = 500;
                Regex.ControlToValidate = controlID;
                Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s!@$%&()-_+=;""<>?/.]{1," + CharLimit + "}";
                Regex.ErrorMessage = "You have exceeded the maximum character limit of " + CharLimit + ". Please revise your answer.";
                Regex.Display = ValidatorDisplay.Dynamic;

            }
            //Block Ended
            if (QuesType == 13)//Constant Sum
            {
                if (CharLimit == 0 || CharLimit < 0)
                    CharLimit = 100;
                Regex.ControlToValidate = controlID;
                Regex.ValidationExpression = @"[0-9]{1,20}";
                Regex.ErrorMessage = "Only numeric value (numbers only) allowed � values must add up to " + CharLimit;
                //Only numeric value (numbers only) allowed � values must add up to X.
                Regex.Display = ValidatorDisplay.Dynamic;
            }
            if (QuesType == 15)//Ranking
            {
                Regex.ControlToValidate = controlID;
                Regex.ValidationExpression = @"[0-9]{1,20}";
                Regex.ErrorMessage = "Only numeric value (numbers only) allowed � ranking must be between 1 and " + AnsOptCnt;
                //Only numeric value (numbers only) allowed � ranking must be between 1 and X.
                Regex.Display = ValidatorDisplay.Dynamic;
            }
            if (QuesType == 14)//Date
            {
                Regex.ControlToValidate = controlID;
                Regex.ValidationExpression = @"([0-9]{1,2})/([0-9]{1,2})/([0-9]{4,4})";
                Regex.ErrorMessage = "Incorrect Date format";
                Regex.Display = ValidatorDisplay.Dynamic;
            }
            cntrlCell.Controls.Add(Regex);
        }
        public void ValidateContactInfoQuestion(string controlID, int QuesType, HtmlTableCell cntrlCell, int CharLimit, string errortext)
        {
            RegularExpressionValidator Regex = new RegularExpressionValidator();
            Regex.ID = "ctrlRegex" + controlID;
            if (QuesType == 20)//Date
            {
                Regex.ControlToValidate = controlID;
                Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s!@$%&()-_+=;""<>?/.]{1,500}";
                Regex.ErrorMessage = errortext + " is Mandatory";
                //Regex.ErrorMessage = "Allowed numerics upto 20 digits only";
                Regex.Display = ValidatorDisplay.Dynamic;
            }
            if (QuesType == 7)//Numeric Input
            {
                Regex.ControlToValidate = controlID;
                Regex.ValidationExpression = @"[0-9]{1,20}";
                Regex.ErrorMessage = " Only numeric value (numbers only) up to 20 digits allowed";
                //Regex.ErrorMessage = "Allowed numerics upto 20 digits only";
                Regex.Display = ValidatorDisplay.Dynamic;

            }
            cntrlCell.Controls.Add(Regex);

        }
      
       public string stripfonttags(string HTMLcontent)
       {
           string pattern1html = "</?font[^>]*>";
            string plainHTML = "";
            plainHTML = Regex.Replace(HTMLcontent, pattern1html, string.Empty);
            return pattern1html; 
       }       
        public HtmlTable GenerateProgressBar(int PresentQuesNoCnt, int TotQuesNoCnt)
       {
           float result = (PresentQuesNoCnt * 100 / TotQuesNoCnt);//calculations 
           float spent = 100 - result;          //result holders
           StringBuilder s = new StringBuilder();
           HtmlTextWriter writer = new HtmlTextWriter(new System.IO.StringWriter(s, System.Globalization.CultureInfo.InvariantCulture));
           s.Append("<table border=\"0\" width=\"300\">");
           s.Append("<tr>");
           s.Append("<td width=\"");
           s.Append(result);
           if (result == 100)
               s.Append("%\" bgcolor=LimeGreen height=\"12\"></td>");
           else
               s.Append("%\" bgcolor=Orange  height=\"12\"></td>");
           s.Append("<td width=\"");
           s.Append(spent);
           s.Append("%\" bgcolor=LightGoldenrodYellow >");
           s.Append("</tr>");
           s.Append("</table>");

           s.Append("<table border=\"0\" width=\"300\">" +
                 "<tr><td width=\"100%\">");
           s.Append("<p dir=\"ltr\" align=\"center\">");
           s.Append("</font><font size=\"3\" face=\"Impact\" " +
                  "color=\"#FF9933\">");
           s.Append(" </font><font color=\"#FF6600\" size=\"4\">");
           s.Append(result.ToString());// percentage
           s.Append("<font size=\"3\" color=\"#FF6600\">");
           s.Append(" % </font><font size=\"3\"  color=\"#8585AD\" " +
                ">                Viewed ");
           s.Append(" (" +PresentQuesNoCnt + " / " + TotQuesNoCnt+")");// acutally data
           s.Append("</font></td></tr></table>");
           s.Append("</center>");
           HtmlTable tableProgressBar = new HtmlTable();
           HtmlTableRow RowProgressBar = new HtmlTableRow();
           HtmlTableCell CellProgressBar = new HtmlTableCell();
           CellProgressBar.InnerHtml = Convert.ToString(s);
           RowProgressBar.Cells.Add(CellProgressBar);
           tableProgressBar.Rows.Add(RowProgressBar);
           return tableProgressBar;
       }

       public bool IsResponsesForSurvey(int surID)
       {
           bool IsResponse = false;
           try
           {
               dbm.OpenConnection(connect);
               string qry = "select count(respondent_id) from osm_responsesurvey where survey_id=" + surID;
               object iRespCount = dbm.ExecuteScalar(CommandType.Text, qry);

               if (Convert.ToInt16(iRespCount) > 0)
                   IsResponse = true;
               return (IsResponse);
           }
           catch (Exception ex) { return IsResponse; }
           finally
           {
               dbm.Connection.Dispose();
               dbm.Connection.Close();
           }
       }

    }
}
