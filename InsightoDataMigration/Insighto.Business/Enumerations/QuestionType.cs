﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Enumerations
{
    public enum QuestionType
    {
        MultipleChoice_SingleSelect = 1,
        MultipleChoice_MultipleSelect = 2,
        Menu_DropDown = 3,
        Choice_YesNo = 4,
        Text_CommentBox = 5,
        Text_SingleRowBox = 6,
        Numeric_SingleRowBox = 7,
        EmailAddress = 8,
        Text_MultipleBoxes = 9,
        Matrix_SingleSelect = 10,
        Matrix_MultipleSelect = 11,
        Matrix_SideBySide = 12,
        ConstantSum = 13,
        DateAndTime = 14,
        Ranking = 15,
        Narration = 16,
        PageHeader = 17,
        QuestionHeader = 18,
        Image = 19,
        ContactInformation = 20,
        Video = 21,
        PageBreak = -1, // this will do nothing with original question types. this will help developer's job easy to add page beak text whenever required.
        IntroductionText = -2 //// this will do nothing with original question types. this will help developer's job easy to add Survey Introduction Text.
    }
}
