﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Enumerations
{
    public enum LicenseType
    {
        None = 0,
        FREE = 150,
        PRO = 5000,
        CORP = -1 // unlimited
    }
}
