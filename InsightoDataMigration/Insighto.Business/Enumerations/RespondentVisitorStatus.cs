﻿namespace Insighto.Business.Enumerations
{
    public enum RespondentVisitorStatus
    {
        Exit = 0,
        InProgress = 1,
        SaveForLater = 2,
        Complete = 3
    }
}
