﻿namespace Insighto.Business.Enumerations
{
    public enum ProgressBarType
    {
        ProgressBarClassic,
        ProgressBarOne,
        ProgressBarTwo,
        ProgressBarThree,
        ProgressBarFour,
        ProgressBarFive
    }
}
