﻿namespace Insighto.Business.Enumerations
{
    public enum UserType
    {
        FREE = 0,
        PRO_MONTHLY = 1,
        PRO_QUARTERLY = 2,
        PRO_YEARLY = 3,
        PREMIUM_MONTHLY = 4,
        PREMIUM_QUARTERLY = 5,
        PREMIUM_YEARLY = 6,
        None = 7
    }
}
