﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Enumerations
{
    public enum RespondentLayOutTypes
    {
        SingleColumn = 1,
        TwoColumn = 2
    }
}
