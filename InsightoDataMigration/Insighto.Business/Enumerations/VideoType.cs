﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Enumerations
{
    public enum VideoTypes
    {
        None = 0,
        Upload_Video = 1,
        Upload_You_Tube_Video = 2
    }
}
