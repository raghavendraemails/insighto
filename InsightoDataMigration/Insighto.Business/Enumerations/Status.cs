﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Enumerations
{
    public enum Status
    {
        None = 0,
        Active = 1,
        InActive = 2,
        Approved = 3,
        Rejected = 4,
        Disabled = 5,
        Unsubscribe = 6
    }
}
