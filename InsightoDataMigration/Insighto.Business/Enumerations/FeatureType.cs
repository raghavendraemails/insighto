﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.Enumerations
{
    public enum FeatureQuestionType
    {
        Q_SINGLESELECT = 1,
        Q_MULTISELECT = 2,
        Q_DROPDOWN = 3,
        Q_YESNO = 4,
        QM_SINGLESELECT = 10,
        QM_MULTISELECT = 11,
        QM_SIDEBYSIDE = 12,
        QT_COMMENT = 5,
        QT_SINGLEROW = 6,
        QT_MULTIROW = 9,
        Q_NUMERIC = 7,
        Q_EMAIL = 8,
        Q_RANKING = 15,
        Q_CONSTANTSUM = 13,
        Q_DATETIME = 14,
        Q_NARRATION = 16,
        Q_PAGEHEADER = 17,
        Q_QUESTIONHEADER = 18,
        Q_CONTACTINFO = 20,
        Q_IMAGE = 19,
        Q_Video=21
    }

    public enum FeatureReportType
    {
        DATACHART,
        CUSTOMCHART,
        REPORTCUSTOM,
        FILTER,
        CROSS_TAB,
        INDIVIDUAL_RESPONSES,
        TRACKEMAIL_ID,
        EXPORT,
        CHART,
        REPORTS_DATA,
        EXPORT_PPT,
        EXPORT_EXCEL,
        EXPORT_WORD,
        EXPORT_PDF,
        EXPORTCROSSTAB_EXCEL,
        EXCLUDE_BLANKRESPONSES,
        RESTEXCL_RESPTOCOMPLETES,
        RESTEXCL_RESPPARTTOCOMPLETES,
        RAWDATA_EXPORT,
        PARTIALS
    }

    public enum FeaturePersonalisationType
    {
        INSERT_LOGOS,
        HEADER,
        FOOTER,
        THEME,
        BUTTONSETTINGS,
        CUSTOMIZE_FONT,
        PROGRESS_BAR,
        SURVEY_LOGO
    }

    public enum FeatureDeploymentType
    {
        LAUNCH_THROUGHINSIGHTO,
        LAUNCH_THROUGHEXTERNAL,
        SCHEDULE_SURVEYLAUNCH,
        SCHEDULE_SURVEYCLOSE,
        PERSONAL_EMAIL,
        EXCEL_UPLOAD,
        CSV_UPLOAD,
        SEND_REMAINDERS,
        PRELAUNCH_CHECK,
        ALERT_MINMAXNO_RESPONDENTS,
        REDIRECTION_URL,
        LAUNCH_NEWEMAILLIST,
        EDIT_ACTIVE_SURVEY,
        UNIQUE_RESPONDENT,
        UNIQUE_RESPONDENT_PASSWORD,
        SAVE_CONTINUE,
        REVIEW_LINK,
        SENDER_NAME,
        SENDER_EMAILADDRESS,
        REPLY_TO_EMAILADDRESS,
        EMAIL_SUBJECT
    }

    public enum FeatureSurveyCreation
    {

     TEMPLATE,
    SKIPLOGIC,
    INTRODUCTION,
    PAGEBREAK,
    RESPONSE_REQUIRED,
    RANDOMIZE_ANSWER,
    FOLDER,
    CATEGORY,
    TEXT_EDITOR,
    SPELL_CHECK,
    OTHERS_TEXT,
    CUSTOMIZE_LASTPAGE,
    KIOSK,
    ALERTS,
    REMINDERS,
    AUTO_THANKUMAIL,
    PRINT_SURVEY,
    COPY_QUESTIONS,
    CHARACTER_LIMIT,
    ANSWER_ALIGN,
    REORDER,
    SURVEY_SCRATCH,
    SURVEY_FROMEXISTING,
    COPYQUES_FROMEXISTING,
    COPYQUES_FROMTEMPLATE,
    SAVE_TOWORD,
    RESP_PREVIEW,
    SAVE_SURVEY_LINK,
    EXIT_SURVEY_LINK,
    BROWSER_ALERT,
    QUESTION_PER_SURVEY,
    NO_OF_RESPONSES,
    NO_OF_SURVEYS,
    BASICCHART,
    CUSTOM_URL,
    EMAIL_LIMIT
    }
}
