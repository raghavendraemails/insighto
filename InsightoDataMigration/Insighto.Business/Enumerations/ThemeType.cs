﻿namespace Insighto.Business.Enumerations
{
    public enum ThemeType
    {
        InsightoClassic,
        Blue,
        Purple,
        Green,
        Orange,
        Red
    }

    //public struct ThemeType
    //{
    //    public const string InsightoClassic = "Insighto Classic";
    //    public const string Blue = "Blue";
    //    public const string Purple = "Purple";
    //    public const string Green = "Green";
    //    public const string Orange = "Orange";
    //    public const string Red = "Red";
    //}
}
