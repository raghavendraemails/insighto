﻿using System.Collections.Generic;
using System.Linq;
using CovalenseUtilities.Services;
using Insighto.Data;
using System;

namespace Insighto.Business.Services
{
    public class UserServiceV2 : BusinessServiceBase
    {
        #region"Method:GetUserDetails By LoginName"
        public Insighto.Data.osm_user GetUserDetailsByLoginName(string loginName)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                return dbConV2.osm_user.Where(u => u.LOGIN_NAME == loginName).FirstOrDefault();
            }
        }
        #endregion

        #region"Method:GetContactIdByUserIdConcatName"
        public Insighto.Data.osm_contactlist GetContactIdByUserIdConcatName(int userId,string contactName)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                return dbConV2.osm_contactlist.Where(c => c.CONTACTLIST_NAME == contactName && c.CREATED_BY == userId).FirstOrDefault();
            }
        }
        #endregion

        public osm_contactlist SaveContactList(osm_contactlist contactList)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_contactlist.Add(contactList);
                dbConV2.SaveChanges();
                return contactList;
            }
        }

        public osm_emaillist SaveEmails(osm_emaillist email)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_emaillist.Add(email);
                dbConV2.SaveChanges();
                return email;
            }
        }
    }
}
