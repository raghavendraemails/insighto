﻿using System.Collections.Generic;
using System.Linq;
using CovalenseUtilities.Services;
using Insighto.DataV1;


namespace Insighto.Business.Services
{   
    public class UsersService : BusinessServiceBase
    {       

        #region"Method:GetUserDetails"
        public List<Insighto.DataV1.osm_user> GetUserDetails()
        {

            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                var survey = (from s in dbCon.osm_survey
                              select s.USERID);
                return (from user in dbCon.osm_user
                                   where survey.Contains(user.USERID)
                                   select user).ToList();

            }
        }
        #endregion

        #region"Method:GetContactDetailsBy UserId"
        public List<Insighto.DataV1.osm_contactlist> GetContactDetailsByUserId(int userId)
        {

            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_contactlist.Where(c => c.CREATED_BY == userId && c.DELETED == 0).ToList();

            }
        }
        #endregion

        #region"Method:GetContactDetailsBy ContactListId"
        public List<Insighto.DataV1.osm_contactlist> GetContactDetailsByContactListId(int contactId,int? userId)
        {

            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_contactlist.Where(c => c.CONTACTLIST_ID ==contactId && c.CREATED_BY == userId && c.DELETED == 0).ToList();

            }
        }
        #endregion

        #region"Method:GetEmailDetailsBy ContactId"
        public List<Insighto.DataV1.osm_emaillist> GetEmailDetailsByContactId(int contactId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_emaillist.Where(e => e.CONTACTLIST_ID == contactId && e.DELETED == 0).ToList();

            }
        }
        #endregion
    }


}


