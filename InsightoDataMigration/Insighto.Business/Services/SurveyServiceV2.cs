﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Services;
using Insighto.Data;

namespace Insighto.Business.Services
{
    public class SurveyServiceV2:BusinessServiceBase
    {
        #region"Method:GetSurveyByUserIdSurveyName"
        public List<osm_survey> GetSurveyByUserIdSurveyName(string userName, string surveyName)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {               
                return(from survey in dbConV2.osm_survey
                         join user in dbConV2.osm_user on survey.USERID equals user.USERID
                         where user.LOGIN_NAME == userName && survey.SURVEY_NAME == surveyName
                         select survey).ToList();
            }
        }
        #endregion

        #region"Method:GetFolderByUserNameFolderName"
        public List<osm_cl_folders> GetFolderByUserNameFolderName(string userName, string folderName)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                return (from folder in dbConV2.osm_cl_folders
                        join user in dbConV2.osm_user on folder.USERID equals user.USERID
                        where user.LOGIN_NAME == userName && folder.FOLDER_NAME == folderName
                        select folder).ToList();
            }
        }
        #endregion

        #region"Method:GetUserIdByUserName"
        public osm_user GetUserIdByUserName(string userName)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                return dbConV2.osm_user.Where(u => u.LOGIN_NAME == userName).FirstOrDefault();
            }
        }
        #endregion

        #region"Method:GetFolderIdByFolderName"
        public osm_cl_folders GetFolderIdByFolderName(string folderName)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                return dbConV2.osm_cl_folders.Where(f => f.FOLDER_NAME == folderName).FirstOrDefault();
            }
        }
        #endregion

        #region"Method:GetSurveyIdBySurveyNameUserId"
        public osm_survey GetSurveyIdBySurveyNameUserId(string surveyName, int userId)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                return dbConV2.osm_survey.Where(s => s.SURVEY_NAME == surveyName && s.USERID == userId).FirstOrDefault();
            }
        }
        #endregion


        public osm_cl_folders SaveFolder(osm_cl_folders folder)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_cl_folders.Add(folder);
                dbConV2.SaveChanges();
                return folder;
            }
        }

        public osm_survey SaveSurvey(osm_survey survey)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_survey.Add(survey);
                dbConV2.SaveChanges();
                return survey;
            }
        }

        public osm_surveyIntro SaveSurveyIntro(osm_surveyIntro surveyIntro)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_surveyIntro.Add(surveyIntro);
                dbConV2.SaveChanges();
                return surveyIntro;
            }
        }

        public osm_surveycontrols SaveSurveyControl(osm_surveycontrols surveyControls)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_surveycontrols.Add(surveyControls);
                dbConV2.SaveChanges();
                return surveyControls;
            }
        }

        public osm_surveylaunch SaveSurveyLaunch(osm_surveylaunch surveyLaunch)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_surveylaunch.Add(surveyLaunch);
                dbConV2.SaveChanges();
                return surveyLaunch;
            }
        }

        public osm_surveyblockwords SaveSurveyBlockedWords(osm_surveyblockwords surveyBlockedWords)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_surveyblockwords.Add(surveyBlockedWords);
                dbConV2.SaveChanges();
                return surveyBlockedWords;
            }
        }

        public osm_responsesurvey SaveSurveyRespondent(osm_responsesurvey surveyRespondent)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_responsesurvey.Add(surveyRespondent);
                dbConV2.SaveChanges();
                return surveyRespondent;
            }
        }

        public osm_surveyquestion SaveSurveyQuestions(osm_surveyquestion surveyQuestions)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_surveyquestion.Add(surveyQuestions);
                dbConV2.SaveChanges();
                return surveyQuestions;
            }
        }

        public osm_surveyreminder SaveSurveyReminder(osm_surveyreminder surveyReminder)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_surveyreminder.Add(surveyReminder);
                dbConV2.SaveChanges();
                return surveyReminder;
            }
        }

        public osm_answeroptions SaveSurveyAnswerOptions(osm_answeroptions surveyAnswerOptions)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_answeroptions.Add(surveyAnswerOptions);
                dbConV2.SaveChanges();
                return surveyAnswerOptions;
            }
        }

        public osm_responsequestions SaveSurveyResponseAnswers(osm_responsequestions surveyResponseAnswers)
        {
            using (InsightoV2Entities dbConV2 = new InsightoV2Entities())
            {
                dbConV2.osm_responsequestions.Add(surveyResponseAnswers);
                dbConV2.SaveChanges();
                return surveyResponseAnswers;
            }
        }
    }
}
