﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CovalenseUtilities.Services;
using Insighto.DataV1;

namespace Insighto.Business.Services
{
    public class SurveyServiceV1 : BusinessServiceBase
    {
        #region"Method:GetSurveyDetailsByUserId"
        public List<Insighto.DataV1.osm_survey> GetSurveyDetailsByUserId(int userId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_survey.Where(s => s.USERID == userId && s.DELETED ==0).ToList();
            }
        }
        #endregion

        #region"Method:GetFolderDetailsByUserId"
        public List<Insighto.DataV1.osm_cl_folders> GetFolderDetailsByUserId(int userId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_cl_folders.Where(s => s.USERID == userId && s.DELETED == 0).ToList();
            }
        }
        #endregion

        #region"Method:GetSurveyIntoBySurveyId"
        public List<Insighto.DataV1.osm_surveyIntro> GetSurveyIntoBySurveyId(int surveyId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_surveyIntro.Where(s => s.SURVEY_ID == surveyId && s.DELETED == 0).ToList();
            }
        }
        #endregion

        #region"Method:GetSurveyControlBySurveyId"
        public List<Insighto.DataV1.osm_surveycontrols> GetSurveyControlBySurveyId(int surveyId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_surveycontrols.Where(s => s.SURVEY_ID == surveyId && s.DELETED == 0).ToList();
            }
        }
        #endregion

        #region"Method:GetSurveyQuestionsBySurveyId"
        public List<Insighto.DataV1.osm_surveyquestion> GetSurveyQuestionsBySurveyId(int surveyId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_surveyquestion.Where(s => s.SURVEY_ID == surveyId && s.DELETED == 0).ToList();
            }
        }
        #endregion

        #region"Method:GetSurveyLaunchBySurveyId"
        public List<Insighto.DataV1.osm_surveylaunch> GetSurveyLaunchBySurveyId(int surveyId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_surveylaunch.Where(s => s.SURVEY_ID == surveyId && s.DELETED == 0).ToList();
            }
        }
        #endregion

        #region"Method:GetSurveyBlockedWordsBySurveyId"
        public List<Insighto.DataV1.osm_surveyblockwords> GetSurveyBlockedWordsBySurveyId(int surveyId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_surveyblockwords.Where(s => s.SURVEY_ID == surveyId && s.DELETED == 0).ToList();
            }
        }
        #endregion

        #region"Method:GetSurveyResponsesBySurveyId"
        public List<Insighto.DataV1.osm_responsesurvey> GetSurveyResponsesBySurveyId(int surveyId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_responsesurvey.Where(s => s.SURVEY_ID == surveyId && s.DELETED == 0).ToList();
            }
        }
        #endregion

        #region"Method:GetSurveyResponseAnswersByRespondentId"
        public List<Insighto.DataV1.osm_responsequestions> GetSurveyResponseAnswersByRespondentId(int respondentId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_responsequestions.Where(s => s.RESPONDENT_ID == respondentId && s.DELETED == 0).ToList();
            }
        }
        #endregion

        #region"Method:GetSurveyRemiderByLaunchId"
        public List<Insighto.DataV1.osm_surveyreminder> GetSurveyRemiderByLaunchId(int launchId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_surveyreminder.Where(s => s.LAUNCH_ID == launchId && s.DELETED == 0).ToList();
            }
        }
        #endregion

        #region"Method:GetSurveyAnswerOptionsByQuestionId"
        public List<Insighto.DataV1.osm_answeroptions> GetSurveyAnswerOptionsByQuestionId(int questionId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_answeroptions.Where(q => q.QUESTION_ID == questionId && q.DELETED == 0).ToList();
            }
        }
        #endregion

        #region"Method:GetFolderNameByFolderId"
        public List<osm_cl_folders> GetFolderNameByFolderId(int folderId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_cl_folders.Where(f => f.FOLDER_ID == folderId).ToList();
            }
        }
        #endregion

        #region"Method:GetSurveyNameBySurveyId"
        public osm_survey GetSurveyNameBySurveyId(int? surveyId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_survey.Where(s => s.SURVEY_ID == surveyId).FirstOrDefault();
            }
        }
        #endregion

        #region"Method:GetUserNameByUserId"
        public List<osm_user> GetUserNameByUserId(int? userId)
        {
            using (InsightoV1Entities dbCon = new InsightoV1Entities())
            {
                return dbCon.osm_user.Where(u => u.USERID == userId).ToList();
            }
        }
        #endregion

    }
}
