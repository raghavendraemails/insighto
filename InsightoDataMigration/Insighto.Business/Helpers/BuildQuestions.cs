﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Insighto.Business.Helpers
{
    public class BuildQuestions
    {
        #region "Method:GetFontSizeValue"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="FontSize"></param>
        /// <returns></returns>
        public static string GetFontSizeValue(string fontSize)
        {
            string fSize = "";
            if (fontSize != null && fontSize.Length > 0)
            {
                switch (fontSize.Trim())
                {
                    case "8":
                        fSize = "XX-Small";
                        break;
                    case "10":
                        fSize = "X-Small";
                        break;
                    case "12":
                        fSize = "Small";
                        break;
                    case "14":
                        fSize = "Medium";
                        break;
                    case "18":
                        fSize = "Large";
                        break;
                    case "24":
                        fSize = "X-Large";
                        break;
                    case "36":
                        fSize = "XX-Large";
                        break;

                    default:
                        fSize = fontSize + "pt";
                        break;

                }
            }
            return fSize;
        }
#endregion

        #region "Method GetFontColor"
        /// <summary>
        /// GetFontColor
        /// </summary>
        /// <param name="FontColor"></param>
        /// <returns></returns>
        public static string GetFontColor(string fontColor)
        {
            string clr = "Black";
            if (fontColor != null && fontColor.Length > 0)
            {
                switch (fontColor)
                {
                    case "AliceBlue":
                        clr = "AliceBlue";
                        break;
                    case "Amber":
                        clr = "#FF9900";
                        break;
                    case "AntiqueWhite":
                        clr = "AntiqueWhite";
                        break;
                    case "Aqua":
                        clr = "Aqua";
                        break;
                    case "Aquamarine":
                        clr = "Aquamarine";
                        break;
                    case "Azure":
                        clr = "Azure";
                        break;
                    case "Beige":
                        clr = "Beige";
                        break;
                    case "Bisque":
                        clr = "Bisque";
                        break;
                    case "Black":
                        clr = "Black";
                        break;
                    case "BlanchedAlmond":
                        clr = "BlanchedAlmond";
                        break;
                    case "Blue":
                        clr = "Blue";
                        break;
                    case "BlueViolet":
                        clr = "BlueViolet";
                        break;
                    case "Brown":
                        clr = "Brown";
                        break;
                    case "BurlyWood":
                        clr = "BurlyWood";
                        break;
                    case "Burnt Orange":
                        clr = "#993300";
                        break;
                    case "Dark azure":
                        clr = "#003366";
                        break;
                    case "Dark green":
                        clr = "DarkGreen";
                        break;
                    case "Dark olive":
                        clr = "#333300";
                        break;
                    case "Gold":
                        clr = "Gold";
                        break;
                    case "Gray":
                        clr = "Gray";
                        break;
                    case "Grayish blue":
                        clr = "#666699";
                        break;
                    case "Green":
                        clr = "Green";
                        break;
                    case "Indigo":
                        clr = "Indigo";
                        break;
                    case "Light sky blue":
                        clr = "SkyBlue";
                        break;
                    case "Light yellow":
                        clr = "LightYellow";
                        break;
                    case "Lime":
                        clr = "Lime";
                        break;
                    case "Magneta":
                        clr = "Magenta";
                        break;
                    case "Maroon":
                        clr = "Maroon";
                        break;
                    case "Medium gray":
                        clr = "#999999";
                        break;
                    case "Navy Blue":
                        clr = "#000080";
                        break;
                    case "Olive":
                        clr = "Olive";
                        break;
                    case "Pale cyan":
                        clr = "#CCFFFF";
                        break;
                    case "Pale green":
                        clr = "PaleGreen";
                        break;
                    case "Plum":
                        clr = "Plum";
                        break;
                    case "Peach":
                        clr = "#FFCC99";
                        break;
                    case "Pink":
                        clr = "Pink";
                        break;
                    case "Purple":
                        clr = "Purple";
                        break;
                    case "Red":
                        clr = "Red";
                        break;
                    case "Royal blue":
                        clr = "RoyalBlue";
                        break;
                    case "Sea green":
                        clr = "SeaGreen";
                        break;
                    case "Silver":
                        clr = "Silver";
                        break;
                    case "Sky blue":
                        clr = "SkyBlue";
                        break;
                    case "Teal":
                        clr = "Teal";
                        break;
                    case "Turquoise":
                        clr = "Turquoise";
                        break;
                    case "Very dark gray":
                        clr = "#333333";
                        break;
                    case "White":
                        clr = "White";
                        break;
                    case "Yellow":
                        clr = "Yellow";
                        break;
                    case "Yellow green":
                        clr = "YellowGreen";
                        break;

                }
            }
            return clr;
        }
        #endregion

        #region "Method:HTMLTextSettings"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string HTMLTextSettings(string text)
        {
            string str = "";
            if (text != null)
            {
                str = "<p>";
                str += text;
                str = str.Replace("</p><p>", "</p>\r\n<p>");
                str = str.Replace("<br />", "</p>\r\n<p>");
                str += "</p>";
                str = str.Replace("<p></p>", "<p>&nbsp;</p>");
                str = str.Replace("<p><p>", "<p>");
                str = str.Replace("</p></p>", "</p>");
            }
            return str;
        }
        #endregion

    }
}
