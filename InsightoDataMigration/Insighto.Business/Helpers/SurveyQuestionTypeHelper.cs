﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Business.Enumerations;

namespace Insighto.Business.Helpers
{
    public class SurveyQuestionTypeHelper
    {

        public static List<QuestionType> SurveyNonSkippableQuestionTypes()
        {
            var questionTypes = new List<QuestionType>
                                    {
                                        
                                        QuestionType.Choice_YesNo,
                                        //QuestionType.Text_CommentBox,
                                        //QuestionType.Text_SingleRowBox,
                                        //QuestionType.Numeric_SingleRowBox,
                                        //QuestionType.EmailAddress,                                        
                                        //QuestionType.DateAndTime,                                       
                                        //QuestionType.Narration,                                       
                                        //QuestionType.QuestionHeader,                                     
                                        //QuestionType.ContactInformation
                                       
                                    };

            return questionTypes;
        }

        public static List<QuestionType> SurveyAnswerControlQuestionTypes()
        {
            var questionTypes = new List<QuestionType>
                                    {
                                        
                                        QuestionType.MultipleChoice_SingleSelect,
                                        QuestionType.Menu_DropDown,
                                        QuestionType.MultipleChoice_MultipleSelect,
                                        QuestionType.Text_MultipleBoxes,
                                        QuestionType.ConstantSum,                                        
                                        QuestionType.Ranking
                                       
                                    };

            return questionTypes;
        }

        public static List<QuestionType> SurveyAnswerTextQuestionTypes()
        {
            var questionTypes = new List<QuestionType>
                                    {
                                        QuestionType.Text_CommentBox,
                                        QuestionType.Text_SingleRowBox,
                                        QuestionType.Text_MultipleBoxes,
                                        //QuestionType.ConstantSum
                                    };

            return questionTypes;
        }

        public static List<QuestionType> SurveyAnswerMatrixQuestionTypes()
        {
            var questionTypes = new List<QuestionType>
            {
                QuestionType.Matrix_SingleSelect,
                QuestionType.Matrix_MultipleSelect,
                QuestionType.Matrix_SideBySide
            };

            return questionTypes;
        }
    }
}
