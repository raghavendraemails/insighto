﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.Web;

namespace Insighto.Business.Helpers
{
    public class EncryptHelper
    {
        private const string Key = "#ABILITY";
        private const string IV = "IV!VALUE";
        public static string EncryptQuerystring(string page, string qrystring)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(page);
                if (qrystring != null && qrystring.Trim().Length > 0)
                {
                    string keycode = Encrypt(qrystring, Key, IV);
                    keycode = HttpUtility.UrlEncode(keycode);
                    sb.Append("?key=" + keycode);
                }
                return sb.ToString();
            }
            catch
            { return null; }
        }

        public static Hashtable DecryptQuerystringParam(string querystring)
        {
            try
            {
                Hashtable ht = new Hashtable();
                string QS = Decrypt(querystring, Key, IV);
                if (QS != null && QS.Trim().Length > 0)
                {
                    string[] QSvalues = QS.Split('&');
                    if (QSvalues != null && QSvalues.LongLength > 0)
                    {
                        for (int i = 0; i < QSvalues.LongLength; i++)
                        {
                            string obj = Convert.ToString(QSvalues[i]);
                            if (obj != null && obj.Trim().Length > 0)
                            {
                                string[] parms = obj.Split('=');
                                if (parms.LongLength == 2)
                                {
                                    ht.Add(parms[0], parms[1]);
                                }
                            }
                        }
                    }
                }
                return ht;
            }
            catch { return null; }
        }

        public static string Encrypt(string Input)
        {
            DESCryptoServiceProvider Des = new DESCryptoServiceProvider();
            ICryptoTransform Transform;
            MemoryStream MemStream = new MemoryStream();
            CryptoStream EncStream;
            Byte[] InputData;

            InputData = Encoding.ASCII.GetBytes(Input);

            Des.Mode = CipherMode.CBC;
            Des.Key = Encoding.ASCII.GetBytes(Key);
            Des.IV = Encoding.ASCII.GetBytes(IV);

            Transform = Des.CreateEncryptor();

            EncStream = new CryptoStream(MemStream, Transform, CryptoStreamMode.Write);
            EncStream.Write(InputData, 0, InputData.Length);

            EncStream.FlushFinalBlock();
            EncStream.Close();

            return Convert.ToBase64String(MemStream.ToArray());
        }

        public static string Encrypt(string Input, string Key, string IV)
        {
            DESCryptoServiceProvider Des = new DESCryptoServiceProvider();
            ICryptoTransform Transform;
            MemoryStream MemStream = new MemoryStream();
            CryptoStream EncStream;
            Byte[] InputData;

            InputData = Encoding.ASCII.GetBytes(Input);

            Des.Mode = CipherMode.CBC;
            Des.Key = Encoding.ASCII.GetBytes(Key);
            Des.IV = Encoding.ASCII.GetBytes(IV);

            Transform = Des.CreateEncryptor();

            EncStream = new CryptoStream(MemStream, Transform, CryptoStreamMode.Write);
            EncStream.Write(InputData, 0, InputData.Length);

            EncStream.FlushFinalBlock();
            EncStream.Close();

            return Convert.ToBase64String(MemStream.ToArray());
        }

        public static string Decrypt(string Input)
        {
            DESCryptoServiceProvider Des = new DESCryptoServiceProvider();
            ICryptoTransform Transform;
            MemoryStream MemStream = new MemoryStream();
            CryptoStream DecStream;
            Byte[] InputData;

            InputData = Convert.FromBase64String(Input);

            Des.Mode = CipherMode.CBC;
            Des.Key = Encoding.ASCII.GetBytes(Key);
            Des.IV = Encoding.ASCII.GetBytes(IV);

            Transform = Des.CreateDecryptor();

            DecStream = new CryptoStream(MemStream, Transform, CryptoStreamMode.Write);
            DecStream.Write(InputData, 0, InputData.Length);

            DecStream.FlushFinalBlock();
            DecStream.Close();

            return Encoding.ASCII.GetString(MemStream.ToArray());
        }

        public static string Decrypt(string Input, string Key, string IV)
        {
            DESCryptoServiceProvider Des = new DESCryptoServiceProvider();
            ICryptoTransform Transform;
            MemoryStream MemStream = new MemoryStream();
            CryptoStream DecStream;
            Byte[] InputData;

            InputData = Convert.FromBase64String(Input);

            Des.Mode = CipherMode.CBC;
            Des.Key = Encoding.ASCII.GetBytes(Key);
            Des.IV = Encoding.ASCII.GetBytes(IV);

            Transform = Des.CreateDecryptor();

            DecStream = new CryptoStream(MemStream, Transform, CryptoStreamMode.Write);
            DecStream.Write(InputData, 0, InputData.Length);

            DecStream.FlushFinalBlock();
            DecStream.Close();

            return Encoding.ASCII.GetString(MemStream.ToArray());
        }

        
    }
}
