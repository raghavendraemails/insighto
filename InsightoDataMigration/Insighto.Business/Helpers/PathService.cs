﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Exceptions;

namespace Insighto.Business.Helpers
{
    public static class PathHelper
    {
       
        public static string GetUserImagesFolder(osm_user user)
        {
            if (user == null)
                return string.Empty;

            string userImageFolderPath = String.Format("In\\UserImages\\{0}",user.USERID);

            return userImageFolderPath;
        }
        public static string GetUserWelcomePageURL()
        {
          
            var pageURL = "~/Welcome.aspx";
            return pageURL;
            
        }
        public static string GetUserChangePasswordPageURL()
        {

            var pageURL = "~/ChangePassword.aspx";
            return pageURL;

        }

        public static string GetUserMyAccountPageURL()
        {

            var pageURL = "~/MyAccounts.aspx";
            return pageURL;

        }
        public static string GetUserLoginPageURL()
        {

            var pageURL = "~/Login.aspx";
            return pageURL;

        }
        public static string GetUserSignUpFreeNextURL()
        {

            var pageURL = "~/In/SignUpFreeNext.aspx";
            return pageURL;

        }

    }

}
