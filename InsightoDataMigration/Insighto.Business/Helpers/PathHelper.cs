﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Insighto.Data;
using Insighto.Exceptions;
using Insighto.Business.Enumerations;


namespace Insighto.Business.Helpers
{
    public static class PathHelper
    {

        public static string GetUserImagesFolder(osm_user user)
        {
            if (user == null)
                return string.Empty;

            string userImageFolderPath = String.Format("In\\UserImages\\{0}", user.USERID);

            return userImageFolderPath;
        }
        public static string GetUserWelcomePageURL()
        {
            return "~/Welcome.aspx";
        }
        public static string GetUserChangePasswordPageURL()
        {
            return "~/ChangePassword.aspx";
        }
        public static string GetUserMyAccountPageURL()
        {
            return "~/MyAccounts.aspx";
        }
        public static string GetUserLoginPageURL()
        {
            return "~/Home.aspx";
        }
        public static string GetUserSignUpFreeNextURL()
        {
            return "~/In/SignUpFreeNext.aspx";
        }
        public static string GetUserCheckoutPageURL()
        {
            return "~/In/Checkout.aspx";
        }
        public static string GetUserInvoiceConfirmationURL()
        {
            return "~/In/InVoiceConfirmation.aspx";
        }
        public static string GetUserDeclinedCardURL()
        {
            return "~/In/CardDeclined.aspx";
        }
        public static string GetUserPendingInvoiceURL()
        {
            return "~/In/PendingInvoiceConfirmation.aspx";
        }
        public static string GetUserContactusURL()
        {
            return "~/In/ContactUs.aspx";
        }
        public static string GetUserInvoiceURL()
        {
            return "Invoice.aspx";
        }

        public static string GetCreateSurveyURL()
        {
            return "~/SurveyDrafts.aspx";
        }

        public static string GetSurveyLaunchConfirmationURL()
        {
            return "~/LaunchConfirmation.aspx";
        }
        public static string GetLaunchSurveyURL()
        {
            return "~/LaunchSurvey.aspx";
        }
        public static string GetPreLaunchSurveyURL()
        {
            return "~/PreLaunch.aspx";
        }
        public static string GetAddNewQuestionURL()
        {
            return "~/AddNewQuestion.aspx";
        }

        public static string GetPricingURL()
        {
            return "~/In/Pricing.aspx";
        }

        public static string GetUpgradeLicenseURL()
        {
            return "~/UpgradeLicense.aspx";
        }

        public static string GetSkipLogicURL()
        {
            return "~/SkipLogic.aspx";
        }

        public static string GetReorderURL()
        {
            return "~/ReorderQuestions.aspx";
        }

        public static string GetAddIntroductionURL()
        {
            return "~/AddIntroduction.aspx";
        }

        public static string GetReportsURL()
        {
            return "~/Reports.aspx";
        }

        public static string GetCustomizeReportsURL()
        {
            return "~/CustomizeReports.aspx";
        }

        public static string GetCrossTabReportsURL()
        {
            return "~/CrossTabReports.aspx";
        }

        public static string GetIndividualResponseURL()
        {
            return "~/IndividualResponse.aspx";
        }


        #region Survey Respondent Region
        public static string GetRespondentControlPath(QuestionType questionType)
        {
            switch (questionType)
            {
                case QuestionType.MultipleChoice_SingleSelect:
                    return "UserControls/RespondentControls/MOSSControl.ascx";

                case QuestionType.MultipleChoice_MultipleSelect:
                    return "UserControls/RespondentControls/MOMSControl.ascx";

                case QuestionType.Menu_DropDown:
                    return "UserControls/RespondentControls/MenuDropDownControl.ascx";

                case QuestionType.Choice_YesNo:
                    return "UserControls/RespondentControls/MOSSControl.ascx";

                case QuestionType.Text_CommentBox:
                    return "UserControls/RespondentControls/MultipleRowsTextControl.ascx";

                case QuestionType.Text_SingleRowBox:
                    return "UserControls/RespondentControls/MultipleRowsTextControl.ascx";

                case QuestionType.Numeric_SingleRowBox:
                    return "UserControls/RespondentControls/MultipleRowsTextControl.ascx";

                case QuestionType.EmailAddress:
                    return "UserControls/RespondentControls/MultipleRowsTextControl.ascx";

                case QuestionType.Text_MultipleBoxes:
                    return "UserControls/RespondentControls/MultipleRowsTextControl.ascx";

                case QuestionType.Matrix_SingleSelect:
                    return "UserControls/RespondentControls/MatrixSingleSelectControl.ascx";

                case QuestionType.Matrix_MultipleSelect:
                    return "UserControls/RespondentControls/MatrixSingleSelectControl.ascx";

                case QuestionType.Matrix_SideBySide:
                    return "UserControls/RespondentControls/MatrixSideBySideControl.ascx";

                case QuestionType.ConstantSum:
                    return "UserControls/RespondentControls/RanKSumTextControl.ascx";

                case QuestionType.DateAndTime:
                    return "UserControls/RespondentControls/DateTimePickerControl.ascx";

                case QuestionType.Ranking:
                    return "UserControls/RespondentControls/RanKSumTextControl.ascx";

                case QuestionType.Narration:
                    return "UserControls/RespondentControls/NarrationHeaderControl.ascx";

                case QuestionType.PageHeader:
                    return "UserControls/RespondentControls/NarrationHeaderControl.ascx";

                case QuestionType.QuestionHeader:
                    return "UserControls/RespondentControls/NarrationHeaderControl.ascx";

                case QuestionType.Image:
                    return "UserControls/RespondentControls/ImageConrol.ascx";

                case QuestionType.ContactInformation:
                    return "UserControls/RespondentControls/ContactInformationControl.ascx";

                case QuestionType.Video:
                    return "UserControls/RespondentControls/VideoControl.ascx";
                    //break;

                case QuestionType.PageBreak:
                    return "UserControls/RespondentControls/PageBreakControl.ascx";

                case QuestionType.IntroductionText:
                    return "UserControls/RespondentControls/IntroductionTextControl.ascx";

                default:
                    break;
            }

            return string.Empty;
        }

        public static string GetThemePath(ThemeType themeType)
        {
            switch (themeType)
            {
                case ThemeType.InsightoClassic:
                    return "Styles/RespondentBasicStyle.css";

                case ThemeType.Blue:
                    return "Styles/RespondentThemeBlue.css";

                case ThemeType.Green:
                    return "Styles/RespondentThemeGreen.css";

                case ThemeType.Purple:
                    return "Styles/RespondentThemePurple.css";

                case ThemeType.Orange:
                    return "Styles/RespondentThemeOrange.css";

                case ThemeType.Red:
                    return "Styles/RespondentThemeRed.css";

                default:
                    break;
            }
            return string.Empty;
        }

        public static string GetProgressBarPath(ProgressBarType progressBarType)
        {
            switch (progressBarType)
            {
                case ProgressBarType.ProgressBarClassic:
                    return "Styles/ProgressBarClassic.css";

                case ProgressBarType.ProgressBarOne:
                    return "Styles/ProgressBarOne.css";

                case ProgressBarType.ProgressBarTwo:
                    return "Styles/ProgressBarTwo.css";

                case ProgressBarType.ProgressBarThree:
                    return "Styles/ProgressBarThree.css";

                case ProgressBarType.ProgressBarFour:
                    return "Styles/ProgressBarFour.css";

                case ProgressBarType.ProgressBarFive:
                    return "Styles/ProgressBarFive.css";

                default:
                    break;
            }
            return string.Empty;
        }

        #endregion

        

        public static string GetApplicationURL()
        {
            return "";
        }

        public static string GetLogoutURL()
        {

            return "~/LogOut.aspx";
        }

        public static string GetSurveyBasicsURL()
        {
            return "~/SurveyBasics.aspx";
        }
        public static string GetSurveyControlsURL()
        {
            return "~/SurveyControls.aspx";
        }
        public static string GetSurveyRespondenURL()
        {
            return "SurveyRespondent.aspx";
        }

        public static string GetSendReminderMailInvitationURL()
        {
            return "~/SendReminderMailInvitation.aspx";
        }
        public static string GetUrlRedirectionURL()
        {
            return "~/URLRedirection.aspx";
        }

        public static string GetSendReminderURL()
        {
            return "~/SendReminder.aspx";
        }

        public static string GetSurveyTemplateURL()
        {
            return "~/SurveyTemplate.aspx";
        }

        public static string GetCopyExsistingSurveyURL()
        {
            return "~/CopyExsistingSurvey.aspx";
        }

        public static string GetTemplatePreviewURL()
        {
            return "~/TemplatePreview.aspx";
        }
        public static string GetSurveyRespondentPreviewURL()
        {
            return "~/SurveyRespondentPreview.aspx";
        }
        public static string GetSurveyRespondenURL1()
        {
            return "~/SurveyRespondent.aspx";
        }
        public static string SurveyAlertThankyouUrl()
        {
            return "App_Themes/Classic/Images/thankyou1.jpg";
        }
        public static string SurveyLogoImageUrl()
        {
            return "App_Themes/Classic/Images/logo-insighto-view.jpg";
        }
        public static string GetInvoiceUrl()
        {
            return "~/In/Invoice.aspx";
        }

        public static string GetRespondentPage(RespondentDisplayMode displayMode)
        {
            return displayMode == RespondentDisplayMode.Respondent ? @"SurveyRespondent.aspx" : @"SurveyRespondentPreview.aspx";
        }

        public static string GetReminderConfirmationURL()
        {
            return "~/ReminderConfirmation.aspx";
        }

        public static string GetInsightoSurveyLinkURL()
        {
            return "~/InsightoSurveyLink.aspx";
        }

        public static string GetSurveyRespondentPreviewLinkURL()
        {
            return "SurveyRespondentPreview.aspx";
        }
        public static string GetEmailInvitationPreviewLink()
        {
            return "~/EmailPreview.aspx";
        }

        public static string GetRegisterURL()
        {
            return "Register.aspx";
        }

        public static string GetAnswerContolURL()
        {
            return "UserControls/SurveyAnswerControl.ascx";
        }

        public static string GetYesNoControl()
        {
            return "UserControls/SurveyAnsYesNoControl.ascx";
        }
        public static string GetMatrixControlURL()
        {
            return "UserControls/SurveyMatrixControl.ascx";
        }

        public static string GetTextBoxControlURl()
        {
            return "UserControls/SurveyAnswerTextBoxControl.ascx";
        }

        public static string GetImageControlURL()
        {
            return "UserControls/SurveyImageControl.ascx"; ;
        }

        public static string GetContactInfoURL()
        {
            return  "UserControls/SurveyContactInfoControl.ascx";
        }
        public static string GetPreLaunchCheckURL()
        {
            return  "~/PreLaunchEmailInvitation.aspx";
        }

        public static string GetVideoControlURL()
        {
            return "UserControls/SurveyVideoControl.ascx";
        }
        public static string GetPreLaunchSurveyControlsURL()
        {
            return "~/PreLaunchSurveyControls.aspx";
        }
        public static string GetPreLaunchSurveyEndMessagesURL()
        {
            return "~/PreLaunchSurveyEndMessages.aspx";
        }
        public static string GetPreLaunchEmailInvitationURL()
        {
            return "~/PreLaunchEmailInvitation.aspx";
        }
        public static string GetAddressBookURL()
        {
            return "~/AddressBook.aspx";
        }
        public static string GetCreateEmailListURL()
        {
            return "~/CreateEmailList.aspx";
        }   
         public static string GetPreLaunchSurveyEmailListURL()
        {
            return "~/PreLaunchSurveyEmailList.aspx";
        }
         public static string GetEditUserURL()
         {
             return "~/EditUser.aspx";
         }
         public static string GetViewUserURL()
         {
             return "~/ViewUser.aspx";
         }  
      
         public static string GetContentManagementURL()
         {
             return "~/Admin/ContentManagement.aspx";
         }
         public static string GetSurveyManagerURL()
         {
             return "~/SurveyManager.aspx";
         }
         public static string GetOpenWidgetURL()
         {
             return "~/In/OpenWidget.aspx";
         }
         public static string GetWidgetBasicsURL()
         {
             return "~/WidgetBasics.aspx";
         }
         public static string GetSurveyAlertsURL()
         {
             return "~/SurveyAlerts.aspx";
         }
         public static string GetSurveyMessagesURL()
         {
             return "~/SurveyMessages.aspx";
         }
         public static string GetMyProfileURL()
         {
             return "~/MyProfile.aspx";
         }
         public static string GetManageFoldersURL()
         {
             return "~/ManageFolders.aspx";
         }
         public static string GetInvoiceHistroyUrl()
         {
             return "~/InvoiceHistory.aspx";
         }
         public static string GetManageLoginUrl()
         {
             return "~/ManageLogin.aspx";
         }
         public static string GetManageCategoryUrl()
         {
             return "~/ManageCategory.aspx";
         }


         public static string GetEditBlockWordUrl()
         {
             return "~/EditBlockedWord.aspx";
         }

         public static string GetBlockedWordListUrl()
         {
             return "~/ViewSurveyBlockedwords.aspx";
         }

         public static string GetAboutUsUrl()
         {
             return "~/In/aboutus.aspx";
         }
         public static string GetAntiSpamUrl()
         {
             return "~/In/antispam.aspx";
         }
         public static string GetPrivacyPolicyUrl()
         {
             return "~/In/privacypolicy.aspx";
         }
         public static string GetMarketingUrl()
         {
             return "~/In/Marketing.aspx";
         }
         public static string GetCustomerRelationUrl()
         {
             return "~/In/CustomerRelations.aspx";
         }
         public static string GetSalesUrl()
         {
             return "~/In/Sales.aspx";
         }
         public static string GetEducationalInstituteUrl()
         {
             return "~/In/EduationalInstitute.aspx";
         }
         public static string GetHumanResourceUrl()
         {
             return "~/In/HumanResource.aspx";
         }
         public static string GetTermsAndConditionsUrl()
         {
             return "~/In/termsandconditions.aspx";
         }  
         public static string GetWhyInsightoUrl()
         {
             return "~/In/whyinsighto.aspx";
         }
         public static string GetEditContactUsUrl()
         {
             return "~/EditContactusList.aspx";
         }

         public static string GetAdminLoginPageUrl()
         {
             return "~/Admin/Login.aspx";
         }

    }
    
    
}
