﻿using System;
using Insighto.Data;

namespace Insighto.Business.ValueObjects
{
    public class OrderInfo : osm_orderinfo
    {      
        public string Range { get; set; }
        public string Url { get; set; }
        public string PaidDate { get; set; }
        public new int USERID { get; set; }
        public Nullable<int> USERTYPE_ID { get; set; }
        public string LOGIN_NAME { get; set; }
        public string PASSWORD { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string ADDRESS_LINE1 { get; set; }
        public string ADDRESS_LINE2 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string POSTAL_CODE { get; set; }
        public string COUNTRY { get; set; }
        public string COMPANY { get; set; }
        public string WORK_INDUSTRY { get; set; }
        public string JOB_FUNCTION { get; set; }
        public string COMPANY_SIZE { get; set; }
        public string DEPT { get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; }
        public string ACCOUNT_TYPE { get; set; }
        public string LICENSE_TYPE { get; set; }
        public Nullable<System.DateTime> LICENSE_EXPIRY_DATE { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<System.DateTime> MODIFIED_ON { get; set; }
        public string STATUS { get; set; }
        public Nullable<int> DELETED { get; set; }
        public int RESET { get; set; }
        public int IMAGES_SIZE { get; set; }
        public decimal PARENT_MASTERID { get; set; }
        public Nullable<int> LOGIN_FLAG { get; set; }
        public Nullable<int> APPROVAL_FLAG { get; set; }
        public Nullable<int> ADMINID { get; set; }
        public string CUSTOMER_ID { get; set; }
        public Nullable<int> LICENSE_UPGRADE { get; set; }
        public int SURVEYMAIL_STATUS { get; set; }
        public string TIME_ZONE { get; set; }
        public int STANDARD_BIAS { get; set; }
        public Nullable<int> EMAIL_FLAG { get; set; }
        public Nullable<int> UPDATES_FLAG { get; set; }
        public Nullable<int> ACTIVATION_FLAG { get; set; }
        public Nullable<int> USER_TYPE { get; set; }
        public string USER_COMPANY { get; set; }
        public string USER_DESIGNATION { get; set; }
        public Nullable<int> PAYMENT_TYPE { get; set; }
        public string LICENSE_OPTED { get; set; }
        public Nullable<int> MAIL_REMINDER_FLAG { get; set; }
        public Nullable<System.DateTime> MAIL_REMINDER_DATE { get; set; }
        public Nullable<int> QUICKUSERGUIDE_FLAG { get; set; }
        public string OPTOUT_STATUS { get; set; }
        public Nullable<int> AUTHLINK_FLAG { get; set; }
        public Nullable<int> QUICKFIRST_FLAG { get; set; }
        public Nullable<int> UPDATE_QUICKFLAG { get; set; }
        public Nullable<int> RENEWAL_ALERT { get; set; }
        public Nullable<int> RENEWAL_FLAG { get; set; }
        public string LICENSETYPE_PENDING { get; set; }
        public Nullable<System.DateTime> LICENSEPENDING_STARTDATE { get; set; }
        public Nullable<int> LICENSE_CHANGEFLAG { get; set; }
        public Nullable<int> EMAIL_COUNT { get; set; }
        public string WALLETPRICEPLAN { get; set; }
        public string IP_ADDRESS { get; set; }
        public Nullable<int> SURVEY_FLAG { get; set; }
    }
}
