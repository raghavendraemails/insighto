﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Insighto.Business.ValueObjects
{
    public class AdminTrackingInfo
    {
        public int TRACKINGID { get; set; }
        public int ADMINID { get; set; }
        public System.DateTime CREATED_ON { get; set; }
        public string ACTION_DONE_ON_PAGE { get; set; }
        public string EVENT_ACTION_DESCRIPTION { get; set; }
        public int FKUSERID { get; set; }     
        public string LOGIN_NAME { get; set; }
        public string NAME { get; set; }
       
       
      
    }
}
