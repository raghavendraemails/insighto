﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    public abstract class Questionnaire<T> : IQuestion<T>, IMutableQuestion<T> where T : class
    {
        #region IMutableQuestion<T> Members

        private T _question;
        public T Question
        {
            get
            {
                return _question;
            }
            set
            {
                _question = value as T;
            }
        }

        #endregion
    }

}
