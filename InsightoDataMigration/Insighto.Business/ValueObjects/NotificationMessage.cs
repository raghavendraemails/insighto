﻿using Insighto.Business.Enumerations;

namespace Insighto.Business.ValueObjects
{
    public class NotificationMessage
    {
        public string Text { get; set; }
        public MessageType Type { get; set; }
        public bool AutoHide { get; set; }
    }
}