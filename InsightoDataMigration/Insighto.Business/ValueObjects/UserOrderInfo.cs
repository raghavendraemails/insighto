﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;

namespace Insighto.Business.ValueObjects
{
    public class UserOrderInfo
    {
        public double productAmont { get; set; }
        public double taxAmount { get; set; }
        public double Total { get; set; }
        public string orderType { get; set; }
        public Nullable<int>  activationFlag { get; set; }
    }
}
