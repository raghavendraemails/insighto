﻿using System;
using System.Collections.Generic;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using Insighto.Business.Helpers;
using Insighto.Business.Enumerations;

namespace Insighto.Business.ValueObjects
{
    public class SurveyQuestionAndAnswerOptions : IMutableQuestion<osm_surveyquestion>, IMutableAnswerOptions<osm_answeroptions>
    {

        #region IMutableQuestion<osm_surveyquestion> Members

        /// <summary>
        /// Gets or sets the index of the page.
        /// </summary>
        /// <value>
        /// The index of the page.
        /// </value>
        public int PageIndex { get; set; }

        /// <summary>
        /// Gets or sets the index of the rank.
        /// </summary>
        /// <value>
        /// The index of the rank.
        /// </value>
        public int RankIndex { get; set; }

        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        /// <value>
        /// The question.
        /// </value>
        public osm_surveyquestion Question { get; set; }

        public QuestionType QuestionType
        {
            get
            {
                if (Question == null)
                    throw new InvalidOperationException("Question is null");

                var quesTypeId = ValidationHelper.GetInteger(Question.QUESTION_TYPE_ID, 0);

                return GenericHelper.ToEnum<QuestionType>(quesTypeId);
            }
        }
        #endregion

        #region IMutableAnswerOptions<osm_answeroptions> Members

        /// <summary>
        /// Gets or sets the answer option.
        /// </summary>
        /// <value>
        /// The answer option.
        /// </value>
        public List<osm_answeroptions> AnswerOption { get; set; }

        #endregion
    }
}
