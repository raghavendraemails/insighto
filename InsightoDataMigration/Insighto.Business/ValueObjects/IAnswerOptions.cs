﻿using System.Collections.Generic;

namespace Insighto.Business.ValueObjects
{
    interface IAnswerOptions<T>
    {
        List<T> AnswerOption { get; }
    }

    public interface IMutableAnswerOptions<T>
    {
        List<T> AnswerOption { get; set; }
    }
}
