﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    public abstract class AnswerOptions<T> : IAnswerOptions<T>, IMutableAnswerOptions<T> where T : class
    {
        #region IMutableQuestion<T> Members

        private List<T> _answerOption;
        public List<T> AnswerOption
        {
            get
            {
                return _answerOption;
            }
            set
            {
                _answerOption = value as List<T>;
            }
        }

        #endregion
    }
}
