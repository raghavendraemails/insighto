﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    public class FolderInfo
    {
        public int FOLDER_ID { get; set; }
        public Nullable<int> USERID { get; set; }
        public Nullable<int> PARENT_ID { get; set; }
        public string FOLDER_NAME { get; set; }
        public Nullable<int> FOLDER_STATUS { get; set; }
        public int DELETED { get; set; }
        public string editUrl { get; set; }
        public string viewUrl { get; set; }
    }
}
