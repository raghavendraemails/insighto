﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Insighto.Business.ValueObjects
{
    public class PagedList<T>
    {
        public bool HasNext { get; set; }
        public bool HasPrevious { get; set; }
        public int TotalCount { get; set; }
        public Double TotalPages  { get; set; }
        public List<T> Entities { get; set; }
    }
    
}
