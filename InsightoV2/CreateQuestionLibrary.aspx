﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="CreateQuestionLibrary.aspx.cs" Inherits="CreateQuestionLibrary" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                <asp:Label ID="lblTitle" runat="server" Text="Create Question Library"></asp:Label>
            </h3>
        </div>
       <%-- <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick="closeModalSelf(false,'');" title="Close">
                &nbsp;</a>
        </div>--%>
        <!-- //popup heading -->
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupContentPanel">
        <!-- popup content panel -->
        <div class="successPanel" id="divSucessPanel" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccessMsg" runat="server" 
                    meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="divErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <div class="formPanel">
            <!-- form -->
            <div class="forgotpanel">
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblSurveyName" runat="server" Text="Survey Name" meta:resourcekey="lblSurveyName" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtSurveyName" runat="server" CssClass="textBoxMedium" 
                            MaxLength="25" meta:resourcekey="txtSurveyNameResource1"></asp:TextBox>
                    </div>
                </div>
                <div class="con_login_pnl">                   
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="regc" CssClass="lblRequired"
                            ControlToValidate="txtSurveyName" runat="server" ErrorMessage="Please enter survey name."
                            Display="Dynamic" meta:resourcekey="regcResource1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblTemplateCategory" runat="server" Text="Template  Category" meta:resourcekey="lblTemplateCategory" />
                    </div>
                    <div class="loginControls">
                        <asp:DropDownList ID="ddlTemplateCategory" CssClass="dropDown" runat="server" OnSelectedIndexChanged="ddlTemplateCategory_SelectedIndexChanged"
                            AutoPostBack="True" meta:resourcekey="ddlTemplateCategoryResource1">
                        </asp:DropDownList>
                        <%--<asp:RequiredFieldValidator SetFocusOnError="true"  ID="regc" CssClass="lblRequired" ControlToValidate="ddlSurveyCategory"
                                runat="server" ErrorMessage="Please select template category" ValidationExpression="^.{1,25}$"
                                Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblTemplateSubCategory" runat="server" Text="Template Sub Category"
                            meta:resourcekey="lblFolderNameResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:DropDownList ID="ddlTemplateSubCategory" CssClass="dropDown" 
                            runat="server" meta:resourcekey="ddlTemplateSubCategoryResource1">
                        </asp:DropDownList>
                        <%--                           <asp:RequiredFieldValidator SetFocusOnError="true"  ID="regSubcategory" CssClass="lblRequired" ControlToValidate="ddlTemplateSubCategory"
                                runat="server" ErrorMessage="Please select template sub category." Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblbtnNewFolder" runat="server" AssociatedControlID="btnSave" 
                            meta:resourcekey="lblbtnNewFolderResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:Button ID="btnSave" runat="server" CssClass="dynamicButton" ToolTip="Save" 
                            Text="Save" OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" />
                    </div>
                </div>
            </div>
    <!-- //form -->
    </div>
    <!-- //popup content panel -->
    </div>
</asp:Content>
