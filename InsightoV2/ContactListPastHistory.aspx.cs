﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;

namespace Insighto.Pages
{
    public partial class ContactListPastHistory : BasePage
    {
          private string _listId= string.Empty;
          public string ListId
          {
              get
              {
                  if (Request.QueryString["key"] != null)
                  {
                      Hashtable ht = new Hashtable();

                      ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                      if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                      {
                          int ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                          _listId = ContactId.ToString();

                      }
                  }
                  return _listId;
              }
              set
              {
                  _listId = value;
              }

          }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}