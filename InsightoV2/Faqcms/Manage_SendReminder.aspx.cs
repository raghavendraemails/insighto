﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using System.Collections;

public partial class Faqcms_Manage_SendReminder : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        Hashtable ht = new Hashtable();
        ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
        string page = string.Empty;
        string parentNode = string.Empty;
        if (ht != null && ht.Contains("pageName"))
        {
            page = ht["pageName"].ToString();
            parentNode = ht["parentNode"].ToString();
            GetpageContent(page, parentNode);
        }
    }

    protected void GetpageContent(string pageName, string parentNode)
    {
        var osmpageContent = ServiceFactory.GetService<FAQService>().GetPageURL(pageName, parentNode);
       // lblCategoryName.Text = osmpageContent.Page_Name_Group;
       // lblParentNode.Text = osmpageContent.Page_Name_Parent;
       // lblPageName.Text = osmpageContent.Page_Name;
        divManageSendReminder.InnerHtml = osmpageContent.Page_Content;
    }
}