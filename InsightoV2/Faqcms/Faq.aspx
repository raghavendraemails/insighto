﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FAQMaster.master" AutoEventWireup="true" CodeFile="Faq.aspx.cs" Inherits="Faqcms_Faq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="contentPanelHeader">
        <div class="faqPagePathway"><span>Create Survey</span> >> Create New Survey >> <b>About survey creation</b></div>
    </div>
    <div class="clear"></div>
    <div class="contentPanel">
        <div class="faqContentPanel">
            <div class="faqPageTitle"><h1>About survey creation:</h1></div>
            <h2>Insighto offers three easy ways to create your first survey.</h2>
            <p>Click on “Create Survey” tab to take the first step in survey creation. There are three options for building your surveys.</p>
            <ul>
                <li>Create New Survey</li>
                <li>Choose a Template</li>
                <li>Copy Existing Survey</li>
            </ul>
            <p>A. You will be navigated to “My Surveys”. Click on “Create Survey” Tab.</p>
            <p><img src="../images/faqimages/CNS_01.gif" /></p>
            <p>&nbsp;</p>
            <p>B. You may select the option of your choice and click on create to start building your survey.</p>
            <p><img src="../images/faqimages/CNS_02.gif" /></p>
        </div>
    </div>
</asp:Content>

