﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SurveyReport.master" AutoEventWireup="true" CodeFile="IndividualResponseVoice.aspx.cs" Inherits="IndividualResponseVoice" %>

  <%@ Register Src="~/UserControls/SurveyReportHeader.ascx" TagName="SurveyReportHeader" TagPrefix="uc" %>

<%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register src="~/UserControls/ErrorSuccessNotifier.ascx" tagname="ErrorSuccessNotifier" tagprefix="Insighto" %>
     <%@ Register assembly="DevExpress.Web.ASPxEditors.v9.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.v9.1" namespace="DevExpress.Web.ASPxPanel" tagprefix="dxp" %>
     <asp:Content ID="surveyReportHeader" ContentPlaceHolderID="SurveyReportHeader" runat="Server">
            <uc:SurveyReportHeader ID="ucSurveyReportHeader" runat="server" />
    </asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyReportMainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript">

        function OnRespCheckedChanged(s, e) {

            //var i = s.GetChecked();
            //=false;
            var a = clchk_validresponses.GetChecked();
            // alert(x);
            var b = clchk_excludedresponses.GetChecked();
            // alert(x);

            var c = clchk_partialresponses.GetChecked();
            // alert(x);
            var d = clchk_partialtocompletes.GetChecked();
            //alert(x);
            if (a == false && b == false && c == false && d == false) {
                e.processonServer = false;
                alert('Please Select Response type');
                //clchk_validresponses.SetChecked(true);
                // return false;
            }

            // return true;


        }

        function ValidateDigitsAndDot(e) {
            debugger;
            var key;
            var evt;
            if (window.event) {
                key = window.event.keyCode;
                evt = window.event;
            }
            else if (e) {
                key = e.htmlEvent.charCode;
                //e.htmlEvent.which;
                evt = e.htmlEvent;
            }
            //var key = window.event.keyCode;
            if (((key > 47) && (key < 58) || (key == 46))) {

                window.event.returnValue = true;
            }
            else {
                window.event.returnValue = false;
            }
        }

    </script>
    <asp:Panel runat="server" ID="pnlLoading" Style="height: 450px; vertical-align: middle;"
        HorizontalAlign="Center">
        <div style="width: 100%; height: 220px;">
        </div>
        <img src="In/homeimages/ajax-loader.gif" alt="LOADING..." /><br />
        <div style="width: 100%; height: 220px;">
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlMain" Style="display: none;">
        <div class="surveyCreateLeftMenu">
            <!-- survey left menu -->
            <ul class="reportsMenu">
                <li><a href="Reports.aspx?Key=<%= Key %>">Reports</a></li>
                <li><a id="CustomReports" runat="server">Customise Charts</a></li>
                <li><a id="CrossTabRpts" runat="server">Cross Tab Report</a></li>
            </ul>
            <ul class="reportsMenu">
                <li><a href="#" class="activeLink">Individual Responses</a></li>
            </ul>
            <div class="reportExpandPanel">
                <p>
                    <b>Filter Options:</b></p>
                <p class="defaultHeight">
                </p>
                <p><%--
                    <dxe:ASPxCheckBox ID="chk_respondentemail" runat="server" AutoPostBack="True" Text="Show Email Address"
                        OnCheckedChanged="chk_respondentemail_CheckedChanged">
                    </dxe:ASPxCheckBox>--%>
                    <dxe:ASPxCheckBox ID="chk_respondentemail" runat="server" AutoPostBack="True" Text="Show Caller Id" Checked="true"
                        OnCheckedChanged="chk_respondentemail_CheckedChanged">
                    </dxe:ASPxCheckBox>
                </p>
                <p>
                   <%-- <dxe:ASPxCheckBox ID="chk_blankresponse" runat="server" AutoPostBack="True" Text="Exclude All Blank Responses"
                        OnCheckedChanged="chk_blankresponse_CheckedChanged">
                    </dxe:ASPxCheckBox>--%>
                     <dxe:ASPxCheckBox ID="chk_blankresponse" runat="server" AutoPostBack="True" Text="Exclude All Blank Responses" Checked="true"
                        OnCheckedChanged="chk_blankresponse_CheckedChanged">
                    </dxe:ASPxCheckBox>
                </p>
            </div>
           <%-- <div class="reportExpandPanel">--%>
            <div class="reportExpandPanel" id="Responseview" runat="server">
                <p>
                    <b>Select Responses to View:</b></p>
                <p class="defaultHeight">
                </p>
                <p>
                    <dxe:ASPxCheckBox ID="chk_validresponses" runat="server" Checked="true" Text="Valid Responses"
                        AutoPostBack="true" ClientInstanceName="clchk_validresponses" OnCheckedChanged="chk_validresponses_CheckedChanged">
                        <ClientSideEvents CheckedChanged="function(s, e) { OnRespCheckedChanged(s,e); }" />
                    </dxe:ASPxCheckBox>
                </p>
                <p>
                    <dxe:ASPxCheckBox ID="chk_excludedresponses" runat="server" Text="Excluded Responses"
                        AutoPostBack="true" ClientInstanceName="clchk_excludedresponses" OnCheckedChanged="chk_validresponses_CheckedChanged">
                        <ClientSideEvents CheckedChanged="function(s, e) {  OnRespCheckedChanged(s,e); }" />
                    </dxe:ASPxCheckBox>
                </p>
                <p>
                    <dxe:ASPxCheckBox ID="chk_partialresponses" runat="server" Text="Partials" AutoPostBack="true"
                        ClientInstanceName="clchk_partialresponses" OnCheckedChanged="chk_validresponses_CheckedChanged">
                        <ClientSideEvents CheckedChanged="function(s, e) { OnRespCheckedChanged(s,e); }" />
                    </dxe:ASPxCheckBox>
                </p>
                <p>
                    <dxe:ASPxCheckBox ID="chk_partialtocompletes" runat="server" Text="Partials taken as completes"
                        AutoPostBack="true" ClientInstanceName="clchk_partialtocompletes" OnCheckedChanged="chk_validresponses_CheckedChanged">
                        <ClientSideEvents CheckedChanged="function(s, e) {  OnRespCheckedChanged(s,e); }" />
                    </dxe:ASPxCheckBox>
                </p>
                <p class="defaultHeight">
                </p>
                <p>
                    <asp:LinkButton ID="rawdatavalid" runat="server" Text="Export Valid Responses to Excel"
                        OnClick="rawdatavalid_Click"></asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton ID="rawdatapartial" runat="server" Text="Export Partials to Excel"
                        OnClick="rawdatapartial_Click"></asp:LinkButton>
                </p>
            </div>
            <!-- //survey left menu -->
        </div>
        <div class="surveyQuestionPanel">
          <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblSuccMsg" runat="server"></asp:Label></div>
            </div>
            <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label></div>
            </div>
              <div class="quationtxtPanel" id="divSendReport" runat="server" visible="false">
  
             <Insighto:ErrorSuccessNotifier ID="esSendReportMail" runat="server" />
            </div>
            <!-- survey question panel -->
            <div class="surveyQuestionHeader">
                <div class="surveyQuestionTitle">
                    INDIVIDUAL RESPONSES</div>
            </div>
            <!-- //survey question panel -->
            <div>
                <table width="100%">
                    <tr>
                        <td align="left">
                            <span class="helptxt">
                            View each individual response by clicking the arrow button below or type the response number and click <b>GO</b><br /> <a target="_blank" href="#">Learn more</a>
                            </span>
                        </td>
                    </tr>
                    <tr><td class="defaultHeight8px"></td></tr>
                </table>
                <table border="0" width="98%" cellpadding="0" cellspacing="0" id="reports">
                    <tr>
                        <td align="left" valign="top">
                            <asp:UpdatePanel ID="upCustomControls" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border-bottom:solid 1px #ccc; margin-bottom:10px;">
                                        <tr>
                                            <td style="width:50%; padding-left:10px;">&nbsp;</td>
                                           
                                            <td style="text-align:right; padding-right:10px; padding-top:6px;" valign="top">
                                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Exclude this Response"
                                                     BackColor="Transparent" OnClick="lbn_ExcludeResponse_Click"></asp:LinkButton>
                                            </td>
                                        </tr>
                                      
                                    </table>


                                    <table border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                         <td align="right">
                                                  <dxp:ASPxPanel ID="ASPxPanel3" runat="server" >
                                                        <PanelCollection>
                                                            <dxp:PanelContent ID="PanelContent1" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                         <td>
                                                                            <dxe:ASPxButton ID="btn_first" runat="server" OnClick="btn_first_Click" Text=" "
                                                                                CausesValidation="False" BackColor="Transparent" Height="28px" Width="25px">
                                                                                <Border BorderStyle="None" />
                                                                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Classic/Images/media_first.gif"
                                                                                    Repeat="NoRepeat" VerticalPosition="center" />
                                                                            </dxe:ASPxButton>
                                                                        </td>
                                                                        <td width="5">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxButton ID="btn_prev" runat="server" OnClick="btn_prev_Click" Text=" " CausesValidation="False"
                                                                                BackColor="Transparent" Height="28px" Width="25px">
                                                                                <Border BorderStyle="None" />
                                                                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Classic/Images/media_rev.gif"
                                                                                    Repeat="NoRepeat" VerticalPosition="center" />
                                                                            </dxe:ASPxButton>
                                                                        </td>
                                                                        <td width="5">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxLabel ID="lbl_currentpage" runat="server">
                                                                            </dxe:ASPxLabel>
                                                                        </td>
                                                                        <td width="5">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxButton ID="btn_nxt" runat="server" OnClick="btn_nxt_Click" Text=" " CausesValidation="False"
                                                                                BackColor="Transparent" Height="28px" Width="25px">
                                                                                <Border BorderStyle="None" />
                                                                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Classic/Images/media_for.gif"
                                                                                    Repeat="NoRepeat" VerticalPosition="center" />
                                                                            </dxe:ASPxButton>
                                                                        </td>
                                                                        <td width="5">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxButton ID="btn_last" runat="server" OnClick="btn_last_Click" Text=" " CausesValidation="False"
                                                                                BackColor="Transparent" Height="28px" Width="25px">
                                                                                <Border BorderStyle="None" />
                                                                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Classic/Images/media_last.gif"
                                                                                    Repeat="NoRepeat" VerticalPosition="center" />
                                                                            </dxe:ASPxButton>
                                                                        </td>
                                                                        <td width="5">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxTextBox ID="txt_req" runat="server" Width="30px" Height="18px" MaxLength="5">
                                                                                <ValidationSettings ErrorText="">
                                                                                    <RequiredField ErrorText="" />
                                                                                    <RegularExpression ErrorText="" />
                                                                                </ValidationSettings>
                                                                                <ClientSideEvents KeyPress="function(s,e){ValidateDigitsAndDot(e);}" />
                                                                            </dxe:ASPxTextBox>
                                                                        </td>
                                                                        <td width="5">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxButton ID="btn_go" runat="server" OnClick="btn_go_Click" Text="GO" ValidationGroup="gotop"
                                                                                UseSubmitBehavior="False">
                                                                                <Border BorderStyle="None" />
                                                                                <HoverStyle ForeColor="#D9D9D9" />
                                                                            </dxe:ASPxButton>
                                                                        </td>
                                                                     
                                                                    </tr>
                                                                    <tr> <td colspan="7">
                                                                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="id1" runat="server" ErrorMessage="Enter Page Number"
                                                                                ControlToValidate="txt_req" ValidationGroup="gotop" ForeColor="Red" ></asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td colspan="7">
                                                                            <asp:RegularExpressionValidator SetFocusOnError="true"  ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_req"
                                                                                ErrorMessage="Enter Valid Page No" ValidationExpression="^[0-9]*$"  ForeColor="Red" ValidationGroup="gotop"
                                                                               ></asp:RegularExpressionValidator>
                                                                        </td> </tr>
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxp:ASPxPanel>
                                                    <dxe:ASPxLabel ID="lbl_message" runat="server" ForeColor="Red" Visible="False">
                                                    </dxe:ASPxLabel>
                                            </td>
                                            </tr>
                                            <tr>
                                            <tr>
                                            <td style="text-align:right; padding-right:10px; padding-top:6px;" valign="top">
                                                <asp:LinkButton ID="lbn_ExcludeResponse" runat="server" Text="Exclude this Response"
                                                     BackColor="Transparent" OnClick="lbn_ExcludeResponse_Click"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    <tr>
                                            <td align="left">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                           <dxrp:ASPxRoundPanel ID="rndpanel_surveyresponses" runat="server" Width="650px" BackColor="white"
                                                                HeaderStyle-BackColor="whitesmoke" HeaderStyle-Height="25px" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Right">
                                                                <PanelCollection>
                                                                    <dxp:PanelContent ID="Panelcontent2" runat="server">
                                                                    </dxp:PanelContent>
                                                                </PanelCollection>
                                                                <HeaderStyle BackColor="White" CssClass="boxheads" ForeColor="#333">
                                                                    <Border BorderStyle="None" />
                                                                </HeaderStyle>
                                                                <Border BorderStyle="None" BorderWidth="0px" />
                                                                <NoHeaderTopLeftCorner Url="none" />
                                                            </dxrp:ASPxRoundPanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <input id="respondent_idvalue" type="hidden" name="respondent_idvalue" value="0"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="chk_validresponses" />
                                    <asp:AsyncPostBackTrigger ControlID="chk_excludedresponses" />
                                    <asp:AsyncPostBackTrigger ControlID="chk_partialresponses" />
                                    <asp:AsyncPostBackTrigger ControlID="chk_partialtocompletes" />
                                    <asp:AsyncPostBackTrigger ControlID="chk_respondentemail" />
                                    <asp:AsyncPostBackTrigger ControlID="chk_blankresponse" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upCustomControls"
                                DisplayAfter="0">
                                <ProgressTemplate>
                                    <div style="z-index: 40; left: 55%; top: 50%; visibility: visible; vertical-align: middle;
                                        position: absolute; top: 25%;" id="IMGDIV" align="center" runat="server" valign="middle">
                                        <img height="50" src="App_Themes/Classic/Images/ajax-loader.gif" width="100" alt="" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress></td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript" language="javascript">

        function pageLoad(sender, args) {
            //alert('hai');
            TogglePannel('<%= pnlMain.ClientID %>', true);
            TogglePannel('<%= pnlLoading.ClientID %>', false);
        }

        function TogglePannel(target, OnOff) {
            obj = document.getElementById(target);

            if (obj) {
                obj.style.display = (OnOff) ? 'inline' : 'none';
            }
        }
 
    </script>
</asp:Content>
