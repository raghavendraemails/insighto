<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="AddNewQuestion.aspx.cs" Inherits=" Insighto.Pages.AddNewQuestion" meta:resourcekey="PageResource1" %>

<%@ Register Src="~/UserControls/Header.ascx" TagPrefix="uc" TagName="Header" %>
<%@ Register Src="~/UserControls/SurveyQuestionControl.ascx" TagName="SurveyQuestion"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/SurveyAnswerControl.ascx" TagName="SurveyAnswer"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/SurveyAnsYesNoControl.ascx" TagName="SurveyYesNo"
    TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/SurveyMatrixControl.ascx" TagName="SurveyMatrixControl"
    TagPrefix="uc" %>
<%@ Register Src="~/UserControls/SurveyContactInfoControl.ascx" TagName="SurveyContactInfo"
    TagPrefix="uc" %>
<%@ Register Src="~/UserControls/SurveyImageControl.ascx" TagName="SurveyImageControl"
    TagPrefix="uc" %>
<%@ Register Src="~/UserControls/QuestionsLeftMenu.ascx" TagName="QuestionsLeftMenu"
    TagPrefix="uc" %>
<%@ Register Src="~/UserControls/SurveyAnswerTextBoxControl.ascx" TagName="SurveyAnswerTextBoxControl"
    TagPrefix="uc" %>
<%@ Register Src="~/UserControls/SuveyColumnControl.ascx" TagName="SuveyColumnControl"
    TagPrefix="uc" %>
<%@ Register Src="~/UserControls/SurveyVideoControl.ascx" TagName="SurveyVideoControl"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <script type="text/javascript" src="Scripts/tiny_mce/tiny_mce.js"></script>
  

    <script type="text/javascript">
        $(document).ready(function () {
            $(".chkMandatory").click(function () {
                var checkedElementLength = $(".chkMandatory input:checked").length;
                if (checkedElementLength > 0) {
                    $("#chkResponseRequired").prop('checked', true);
                }
                else {
                    $("#chkResponseRequired").prop('checked', false);
                }
            });

            $("#chkResponseRequired").click(function () {
                var isRespChecked = $(this).prop('checked');
                $(".chkMandatory input:checkbox").each(function () {
                    $(this).prop('checked', isRespChecked);
                });

            });
        });

        //please don't delete it..need dynamic function
//        $(document).ready(function () {
//            var inputsChanged = false;
//            $('#form1 input').change(function () {
//                var inputsChanged = true;
//                alert(inputsChanged);
//                document.getElementById("hlnkPopUp").onclick = new Function("closeModalSelf(true,'');");
//            });
//        });
    </script>
     <asp:ScriptManager ID="sManagerAddQuestion" runat="server">
    </asp:ScriptManager>
    <div class="popupHeading">
        <div class="popupTitle">
            <h3>
                <asp:Label ID="lblChangeQeustionMode" runat="server" Text="CREATE QUESTION" 
                    meta:resourcekey="lblChangeQeustionModeResource1"></asp:Label></h3>
        </div>
        <%--<div class="popupClosePanel">
            <a href="#" id="hlnkPopUp" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(true,'');">
                &nbsp;</a>
        </div>--%>
    </div>
    <div class="popupContentPanel">
        <div class="clear">
        </div>
        <div class="popupLeftMenuPanel" runat="server" id="dvPopupLeftMenuPanel">
            <uc:QuestionsLeftMenu ID="ucQuestionsLeftMenu" runat="server" />
        </div>
        <div class="createQuestionBody" runat="server" id="dvCreateQuestionBody">
            <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblSuccMsg" runat="server" Visible="False" 
                        Text="Question saved successfully." meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
            </div>
            <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblErrMsg" runat="server" Visible="False" 
                        meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
            </div>
            <div class="errorMessagePanel imageError" id="Div1" runat="server" style="display: none">
                <div>
                    <asp:Label ID="Label1" CssClass="lblErrMsg" Style="display: none" 
                        runat="server" meta:resourcekey="Label1Resource1"></asp:Label></div>
            </div>
            <div id="dvNoQuesionId" runat="server" class="informationControl" visible="false">
                <asp:Label ID="lblNoQuestion" runat="server" Visible="False" 
                    meta:resourcekey="lblNoQuestionResource1"></asp:Label>
                <div class="clear">
                </div>
            </div>
            <div id="dvPaidSubscription" runat="server" visible="false">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <div class="informationPanelDefault">
                                <div runat="server" id="dvNoOfQuestions" visible="false">
                                    <p>
                                        <b><asp:Label ID="lblNoOfQuestionsText" runat="server" 
                                            Text="You can add only 15 questions to the survey." 
                                            meta:resourcekey="lblNoOfQuestionsTextResource1"></asp:Label></b><br />
                                         <b><asp:Label ID="lblNoOfQuestionsText1" runat="server" Text="This feature is available only for paid subscriptions. Experience Insighto Paid
                                            Membership and get access to these Powerful features:" 
                                            meta:resourcekey="lblNoOfQuestionsText1Resource1"></asp:Label></b></p>
                                </div>
                                <div runat="server" id="dvImagePaidSubscription" visible="false">
                                    <p>
                                        <b><asp:Label ID="lblPaidSubscription" runat="server" Text="This feature is available only for paid subscriptions. Experience Insighto Paid Membership
                                            and get access to these Powerful features:" 
                                            meta:resourcekey="lblPaidSubscriptionResource1"></asp:Label></b></p>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="defaultHeight">
                        </td>
                    </tr>
                    <tr>
                        <td id="Td1">
                            <ul class="liInsighoIcon">
                                <li><b><asp:Label ID="lblInsighto1" runat="server"  
                                        Text="Get access to 20 question types" meta:resourcekey="lblInsighto1Resource1"></asp:Label></b></li>
                                <li><b><asp:Label ID ="lblInsighto2" runat="server" 
                                        Text="Create unlimited number of survey questions" 
                                        meta:resourcekey="lblInsighto2Resource1"></asp:Label></b></li>
                                <li><b><asp:Label ID="lblInsighto3" runat="server" 
                                        Text="Customize Charts and graphs" meta:resourcekey="lblInsighto3Resource1"></asp:Label></b></li>
                                <li><b><asp:Label ID="lblInsighto4" runat="server" 
                                        Text="Download results to PPT, Excel, Word or PDF" 
                                        meta:resourcekey="lblInsighto4Resource1"></asp:Label></b></li>
                                <li><b><asp:Label ID="lblInsighto5" runat="server" 
                                        Text="Customize your survey end page and more" 
                                        meta:resourcekey="lblInsighto5Resource1"></asp:Label></b></li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="upgradeNowBtnPanel">
                                <ul class="HyperlinkBtn">
                                    <li><a href="#" id="hlnkPricelink" runat="server" onclick="opener.focus()">
                                    <asp:Label ID="lblUpgradeNow" runat="server" Text="Upgrade Now!" 
                                            meta:resourcekey="lblUpgradeNowResource1"></asp:Label></a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="dvExsistsQuestionId" runat="server" visible="false">
                <div class="popupPathway">
                    <!-- pathway -->
                    <span class="questionMainName">
                        <asp:Label ID="lblQuestionName" runat="server" 
                        meta:resourcekey="lblQuestionNameResource1"></asp:Label></span>
                    <!-- //pathway -->
                </div>
                <div>
                    <uc1:SurveyQuestion ID="ucSurveyQuestion" runat="server" />
                    <p class="defaultHeight">
                    </p>
                    <asp:Label ID="lblanswerChoice" runat="server" 
                        meta:resourcekey="lblanswerChoiceResource1"></asp:Label>
                    <div id="dvrankText" runat="server" visible="false">
                    <div>
                        <p>
                            <b><asp:Label ID="lblranktext" runat="server" 
                                Text="Your survey participants will be communicated the following with this question:" 
                                meta:resourcekey="lblranktextResource1"></asp:Label>
                            </b>
                        </p>
                        <ul>
                            <li><asp:Label ID="lblRank1" runat="server" 
                                    Text="Ranks must be between 1 and total number of options you choose." 
                                    meta:resourcekey="lblRank1Resource1"></asp:Label></li>
                            <li><asp:Label ID="lblRank2" runat="server" Text="Ranks should not be repeated." 
                                    meta:resourcekey="lblRank2Resource1"></asp:Label></li>
                        </ul>
                        </div> 
                    </div>
                    <asp:Panel ID="pnlSurveyAnswerControl" runat="server" 
                        meta:resourcekey="pnlSurveyAnswerControlResource1" />
                    <div>
                        <p class="defaultHeight">
                        </p>
                        <div class="clear">
                        </div>
                        <div id="dvchkRandomAnswer" runat="server" visible="true">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkRandomAnswer" runat="server" CssClass="form-field" 
                                            Text="Randomize answer options" meta:resourcekey="chkRandomAnswerResource1" />
                                    </td>
                                    <td class="space">
                                        &nbsp;
                                    </td>
                                    <td class="p_rgt">
                                        <a href="#" class="helpLink1" onclick="javascript: window.open('help/2_2_13.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">&nbsp;</a>
                                        
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="dvchkPageBreak" runat="server" visible="true">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkPageBreak" CssClass="form-field" runat="server" 
                                            Text="Page break" meta:resourcekey="chkPageBreakResource1" />
                                    </td>
                                    <td class="space">
                                        &nbsp;
                                    </td>
                                    <td class="p_rgt">
                                        <a href="#" class="helpLink1" title="Help" onclick="javascript: window.open('help/2_2_14.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">&nbsp;</a>
                                        
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="dvchkResponseRequired" runat="server" visible="true">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkResponseRequired" ClientIDMode="Static" CssClass="form-field chkResponseRequired"
                                            runat="server" Text="Response required" 
                                            meta:resourcekey="chkResponseRequiredResource1" />
                                    </td>
                                    <td class="space">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <a href="#" class="helpLink1" title="Help" onclick="javascript: window.open('help/2_2_11.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">&nbsp;</a>
                                        
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clear"></div>
                        
                        <p class="defaultHeight">
                        </p>
                        <asp:CheckBox ID="chkOtherAnswer" runat="server" CssClass="form-field" 
                            Text="Check the box to add 'Other' as an additional answer option with its own text field allowing the respondent to &nbsp;key in a different answer" 
                            meta:resourcekey="chkOtherAnswerResource1" AutoPostBack="True" />&nbsp
                        <span id="divCheckOther" runat="server"><a href="#" class="helpLink1" title="help"
                            onclick="javascript: window.open('help/2_2_12.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">
                        </a></span>
                        <div>
                        </div>
                       <div>
                           <asp:TextBox ID="Txt_other" runat="server" Visible="false" CssClass="textBoxMedium txtAns" MaxLength="150" >Other</asp:TextBox>
                       </div>

                        <div id="divImageText" class="informationPanelDefault" runat="server" visible="false" style="margin-top:-20px;">
                            <div>
                            <asp:Label ID="lblImageText" runat="server" Text="Upon saving, you would be automatically directed to Create Questions menu to choose
                                the appropriate Question/Answer options corresponding to this." 
                                    meta:resourcekey="lblImageTextResource1"></asp:Label> 
                                </div>
                        </div>
                        
                        <div class="bottomButtonPanel" style="margin-top:20px;">
                            <asp:Button ID="btnCreate" runat="server" Text="Save" CssClass="dynamicButton btnCreate"
                                OnClientClick="tinyMCE.triggerSave(false,true);" OnClick="btnCreate_Click" 
                                meta:resourcekey="btnCreateResource1" />&nbsp;
                            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="dynamicButton" OnClick="btnClear_Click"
                                CausesValidation="False" meta:resourcekey="btnClearResource1" />
                        </div>
                        <input type="hidden" id="hdnImagVideoCount" runat="server" class="hdnCount" />
                        <input type="hidden" id="hdnImageType" runat="server" class="hdntype" />
                        <input type="hidden" id="hdnImageCount" runat="server" class="hdnImagCnt" /> 
                        <input type="hidden" id="hdnVideoCount" runat="server" class="hdnVideoCnt" />
                        <asp:HiddenField ID ="hdnQuesSeq" runat ="server" />
                        <!-- //form -->
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <script type="text/javascript">
        $(".btnCreate").click(function () {
            var count = $('.hdnCount').val();
            var imgCount = $('.hdnImagCnt').val();
            var videoCount = $('.hdnVideoCnt').val();
            var quesType = $('.hdntype').val();
            var comparecount = "";

            if (quesType == "21")
                comparecount = videoCount;
            if (quesType == "19")
                comparecount = imgCount;
                
             if (comparecount != "-1" && comparecount != "") {
               if (count < comparecount)
                    return true;
                else {
                    var msg = "";
                    if (quesType == "21") {
                        msg = "Exceeded quota for adding Video files.";
                    }
                    if (quesType == "19") {
                        msg = "Exceeded quota for adding Image files.";
                    }
                    $('.imageError').show();
                    $('.lblErrMsg').html(msg);
                    $('.lblErrMsg').show();
                    return false;
                }
            }
        });
    </script>
   
</asp:Content>
