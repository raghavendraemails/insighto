﻿using System;
using System.Collections;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using App_Code;
using System.Data;

public partial class ProgressBar : SurveyPageBase
{
    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int surveyId = 0;
    SurveyCore scapp = new SurveyCore();
    #endregion

    #region "Event:Page_Load"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var surveySetting = ServiceFactory.GetService<SurveySetting>();
            var lstProgressbar = surveySetting.GetProgressBar();
            var progressBarCategory = from pb in lstProgressbar
                                      orderby pb.PROGRESSBAR_CATEGORY descending
                                      group pb by pb.PROGRESSBAR_CATEGORY into pbg
                                      select new { PROGRESSBAR_CATEGORY = pbg.Key, osm_progressbar = pbg };


            lstProgressbarCategory.DataSource = progressBarCategory;
            lstProgressbarCategory.DataBind();
            BindDefaultInfo(); 
        }
    }
    #endregion

    #region "Method:BindDefaultInfo"
    private void BindDefaultInfo()
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            }

            if (surveyId > 0)
            {
                var surveySettingsService = ServiceFactory.GetService<SurveySetting>();
                var surveySettings = surveySettingsService.GetSurveySettings(surveyId);
                txtProgressBar.Text = surveySettings[0].ProgressbarText;
                if (surveySettings.Any())
                {
                    if (ValidationHelper.GetBool(surveySettings[0].Show_Progressbar.ToString(), false))
                    {
                        chkShowProgresBar.Checked = true;
                    }
                    else
                    {
                        chkShowProgresBar.Checked = false;
                    }
                    if (ValidationHelper.GetBool(surveySettings[0].Show_Progressbartext.ToString(), false))
                    {
                        chkProgressBarText.Checked = true;

                    }
                    else
                    {
                        chkProgressBarText.Checked = false;

                    }

                    if (String.IsNullOrEmpty(surveySettings[0].Progressbar_Type))
                    {
                        SelectRadio("ProgressBarClassic");
                    }
                    else
                    {
                        SelectRadio(surveySettings[0].Progressbar_Type);
                    }

                }

                DataSet dssrvysettings = scapp.getSurveySettings(surveyId);

                if (dssrvysettings.Tables[0].Rows.Count > 0)
                {
                    if (dssrvysettings.Tables[0].Rows[0]["showQM"].ToString() == "True")
                    {
                        chkQM.Checked = true;
                    }
                    else
                    {
                        chkQM.Checked = false;
                    }
                }
            }
            else
            {

            }
        }
    }
    #endregion

    #region "Method : SelectRadio"
    /// <summary>
    /// Used to select the radio button in list view.
    /// </summary>
    /// <param name="strSelect"></param>

    private void SelectRadio(string strSelect)
    {
        foreach (ListViewItem categoryItem in lstProgressbarCategory.Items)
        {
            ListView lstprogressbar1 = categoryItem.FindControl("lstProgressbar") as ListView;
            foreach (ListViewItem item in lstprogressbar1.Items)
            {
                HtmlInputRadioButton rbtProgressbar = item.FindControl("rbtnProgressbar") as HtmlInputRadioButton;
                if (rbtProgressbar.Value.Contains(strSelect))
                {
                    rbtProgressbar.Checked = true;
                }
            }
        }
    }
    #endregion

    #region "Event : lstprogressbar_ItemDataBound"
    /// <summary>
    /// Used to binding the data.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lstProgressbar_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            //src="App_Themes/Classic/Images/Theme_Blue_s.jpg"
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            string strImageUrl = "App_Themes/Classic/Images/Progress-bar/Progress-bar-model-" + ((Insighto.Data.osm_progressbar)(dataItem.DataItem)).PROGRESSBAR_NAME + ".gif";

            Image imgTheme = e.Item.FindControl("imgProgressBar") as Image;
            imgTheme.ImageUrl = strImageUrl;

            HtmlInputRadioButton rbtnProgress = e.Item.FindControl("rbtnProgressbar") as HtmlInputRadioButton;
            //rbtnTheme.GroupName = "mytest";
            string script = "SetSingleRadioButton('" + rbtnProgress.ClientID  + "',this)";
            rbtnProgress.Attributes.Add("onclick", script);

        }

    }

    #endregion

    #region "Event:btnSave_Click"

    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }

        osm_surveysetting osmSurveySetting = new osm_surveysetting();
        osmSurveySetting.SURVEY_ID = surveyId;
        osmSurveySetting.Progressbar_Type = SelectVal();
        if (chkProgressBarText.Checked)
        {
            osmSurveySetting.Show_Progressbartext = true;
            osmSurveySetting.ProgressbarText = txtProgressBar.Text;
            // osmSurveySetting.  = txtProgressBar.Text.Trim();
        }
        else
        {
            osmSurveySetting.Show_Progressbartext = false;
            osmSurveySetting.ProgressbarText = txtProgressBar.Text.Trim();
        }
        if (chkShowProgresBar.Checked)
        {
            osmSurveySetting.Show_Progressbar  = true;
        }
        else
        {
            osmSurveySetting.Show_Progressbar = false;
        }

        var surveySettingsService = ServiceFactory.GetService<SurveySetting>();
        surveySettingsService.SaveOrUpdateSurveySetting(osmSurveySetting, "P");

        if (chkQM.Checked)
        {
            DataSet dsappsrvysetting = scapp.updateQMSurveySettings(1, surveyId);
        }
        else
        {
            DataSet dsappsrvysetting = scapp.updateQMSurveySettings(0, surveyId);
        }

        lblSuccMsg.Text = Utilities.ResourceMessage("ProgressBar_Success_Save"); //"Progress bar  saved successfully.";
        dvSuccessMsg.Visible = true;
        BindDefaultInfo(); 
       

    }
    #endregion

    #region "Method : Select Radio button value"
    /// <summary>
    /// Used to select the radio button in list view.
    /// </summary>
    /// <param name="strSelect"></param>

    private string SelectVal()
    {
        string strVal = default(string);
        foreach (ListViewItem categoryItem in lstProgressbarCategory.Items)
        {
            ListView lstprogressbar1 = categoryItem.FindControl("lstProgressbar") as ListView;
            foreach (ListViewItem item in lstprogressbar1.Items)
            {
                HtmlInputRadioButton rbtnProgressbar = item.FindControl("rbtnProgressbar") as HtmlInputRadioButton;
                if (rbtnProgressbar.Checked)
                {
                    strVal = rbtnProgressbar.Value;
                }
            }
        }
        return strVal;
    }
    #endregion

}