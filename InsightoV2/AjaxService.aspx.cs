﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Insighto.Business.Helpers;
using Insighto.Data;
using System.Web.Services;
using CovalenseUtilities.Services;
using CovalenseUtilities.Extensions;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Helpers;
using Insighto.Business.Enumerations;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


public partial class AjaxService : System.Web.UI.Page
{
    SurveyCore surcore = new SurveyCore();
    public int SurveyFlag
    {
        get
        {
            var key = QueryStringHelper.GetString("key");
            if (String.IsNullOrEmpty(key))
                return 0;

            Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
            if (ht != null && ht.Contains("surveyFlag"))
            {
                return ValidationHelper.GetInteger(ht["surveyFlag"], 0);
            }
            return 0;
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {

        Response.Clear();
        Response.ContentType = "application/json";
        var responseData = "";
        if (Request["method"] != null)
        {
            var fnName = Request["method"];
            if (fnName == "GetFolders") responseData = GetFolders();
            else if (fnName == "GetCategory")
                responseData = GetCategory();
            else if (fnName == "GetSurveys") responseData = GetSurveys();
            else if (fnName == "GetSurveysByUser") responseData = GetSurveysByUser();
            else if (fnName == "DeleteSurveyBySurveyId")
            {
                responseData = DeleteSurveyBySurveyId();
            }
            else if (fnName == "Upgrade_ProCredits_SurveyBySurveyId") responseData = Upgrade_ProCredits_SurveyBySurveyId();

            else if (fnName == "Upgrade_PremCredits_SurveyBySurveyId") responseData = Upgrade_PremCredits_SurveyBySurveyId();
            else if (fnName == "BuySurveyCredits") responseData = BuySurveyCredits();
            
            else if (fnName == "DeleteFolderByFolderId")
            {
                responseData = DeleteFolderByFolderId();
            }
            else if (fnName == "DeleteContactByContactListId")
            {
                responseData = DeleteContactByContactListId();
            }
            else if (fnName == "GetEmailsByList")
                responseData = GetEmailsByList();
            else if (fnName == "FindSurveysByFolder") responseData = FindSurveysByFolder();
            else if (fnName == "FindSurveysByCategory") responseData = FindSurveysByCategory();
            else if (fnName == "GetAddressBook") responseData = GetAddressBook();
            else if (fnName == "DeleteEmailById") responseData = DeleteEmailById();
            else if (fnName == "CopySurveyFromTemplate") responseData = CopySurveyFromTemplate();
            else if (fnName == "GetSuveysByCategory") responseData = GetSuveysByCategory();
            else if (fnName == "SaveResponseAnswerOptions") responseData = SaveResponseAnswerOptions();
            else if (fnName == "GetOrderInofByUserId") responseData = GetOrderInofByUserId();
            else if (fnName == "GetSurveysByContactId") responseData = GetSurveysByContactId();
            else if (fnName == "ActivateSurveyBySurveyId") responseData = ActivateSurveyBySurveyId();
            else if (fnName == "FindAdminUsers") responseData = FindAdminUsers();
            else if (fnName == "GetAllUserDetails") responseData = GetAllUserDetails();
            else if (fnName == "FindAdminTracking") responseData = FindAdminTracking();
            else if (fnName == "DeleteAdminUser") responseData = DeleteAdminUser();


            else if (fnName == "ResetAdminPassword") responseData = ResetAdminPassword();
            else if (fnName == "UserAdvanceSearchDetails") responseData = UserAdvanceSearchDetails();
            else if (fnName == "GetUserSurveyDetails") responseData = GetUserSurveyDetails();
            else if (fnName == "GetUserDeletedSurveyDetails") responseData = GetDeletedSurveyDetails();

            else if (fnName == "GetPickListByCategory") responseData = GetPickListByCategory();
            else if (fnName == "FindPendingInvoices") responseData = FindPendingInvoices();
            else if (fnName == "UpgradeLicense") responseData = UpgradeLicense();
            else if (fnName == "FindPaymentPendingUsers") responseData = FindPaymentPendingUsers();
            else if (fnName == "GetAllBlockedWords") responseData = GetAllBlockedWords();
            else if (fnName == "DeleteUserByUserId") responseData = DeleteUserByUserId();
            else if (fnName == "DeleteBlockedWordById") responseData = DeleteBlockedWordById();
            else if (fnName == "ViewBlockedWordList") responseData = ViewBlockedWordList();
            else if (fnName == "ViewSurveyBlockedWordList") responseData = ViewSurveyBlockedWordList();
            else if (fnName == "GetContactuserList") responseData = GetContactuserList();
            else if (fnName == "GetRoles") responseData = GetRoles();
            else if (fnName == "GetCommonFeatures") responseData = GetCommonFeatures();
            else if (fnName == "CopySurveyForQuestionLibrary") responseData = CopySurveyForQuestionLibrary();
            else if (fnName == "GetBouncedEmails") responseData = GetBouncedEmails();
            else if (fnName == "UpdateSurveyStatus") responseData = UpdateSurveyStatus();
            else if (fnName == "GetFeaturesByGroup") responseData = GetFeaturesByGroup();
            else if (fnName == "DeleteFeatureById") responseData = DeleteFeatureById();
            else if (fnName == "SearchEmails") responseData = SearchEmails();
            else if (fnName == "RestoreSurveys") responseData = RestoreSurveys();

        }
        Response.ContentType = "text/plain";
        Response.Write(responseData);
        Response.End();
    }

    [WebMethod]
    private string GetFolders()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        ServiceFactory.GetService<SessionStateService>().AddRecordsCountToSession(pagingCriteria.PageSize);
        var folderService = ServiceFactory.GetService<FolderService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        int surveyFlag = ValidationHelper.GetInteger(Request["Surveyflag"].ToString(), 0);
        var pagedFolderlist = folderService.GetFoldersByUserId(userInfo.UserId, pagingCriteria, surveyFlag);

        if (pagedFolderlist.Entities.Find(x => x.FOLDER_NAME == Constants.MYFOLDER) != null)
        {
            pagedFolderlist.Entities.Find(x => x.FOLDER_NAME == Constants.MYFOLDER).editUrl = "0";
            pagedFolderlist.Entities.Find(x => x.FOLDER_NAME == Constants.MYFOLDER).FOLDER_ID = 0;
        }

        //anoymonous type of object
        var gridData = new
        {
            total = pagedFolderlist.TotalPages,
            page = pagedFolderlist.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedFolderlist.TotalCount,
            rows = pagedFolderlist.Entities
        };

        return jsonSerializer.Serialize(gridData);

    }

    [WebMethod]
    private string FindAdminUsers()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var adminService = ServiceFactory.GetService<AdminUserService>();
        var searchKey = string.Empty;
        if (Request["keyword"] != null || Request["keyword"] != "")
        {
            searchKey = Server.UrlDecode(Request["keyword"]);
        }
        else
            searchKey = "";

        var pagedAdminUsers = adminService.FindAdminUsers(pagingCriteria, searchKey);

        var data = (from d in pagedAdminUsers.Entities
                    select new
                    {
                        FIRST_NAME = d.FIRST_NAME,
                        LAST_NAME = d.LAST_NAME,
                        PHONE = d.PHONE.Replace("-"," "),
                        CREATED_ON = Convert.ToDateTime(d.CREATED_ON).ToString("dd-MMM-yyyy"),
                        ADMIN_TYPE = d.ADMIN_TYPE.Length > 0 ? d.ADMIN_TYPE : "Super Admin",
                        ADMINUSERID = d.ADMINUSERID,
                        LOGIN_NAME = d.LOGIN_NAME,
                        MODIFIED_ON = d.MODIFIED_ON,
                        EditLinkUrl = EncryptHelper.EncryptQuerystring("AddAdminUser.aspx", "AdminUserId=" + d.ADMINUSERID.ToString())
                    }).ToList();
        var gridData = new
        {
            total = pagedAdminUsers.TotalPages,
            page = pagedAdminUsers.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedAdminUsers.TotalCount,
            rows = data
        };
        return jsonSerializer.Serialize(gridData);

    }

    [WebMethod]
    private string GetPickListByCategory()
    {

        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var pickListService = ServiceFactory.GetService<PickListService>();
        string category = string.Empty;
        if (Request["category"] != null)
        {
            category = Server.UrlDecode(Request["category"]);
        }
        else
        {
            category = "";
        }
        var pagedCategoryList = pickListService.GetPickList(category, pagingCriteria);

        var data = (from d in pagedCategoryList.Entities
                    select new
                    {
                        PARAM_VALUE = d.PARAM_VALUE,
                        PICK_ID = d.PICK_ID,
                        PARAMETER = d.PARAMETER,
                        EditLinkUrl = EncryptHelper.EncryptQuerystring("AddPickList.aspx", "PickId=" + d.PICK_ID.ToString()+ "&Category="+d.CATEGORY.ToString())
                    }).ToList();

        //anoymonous type of object
        var gridData = new
        {
            total = pagedCategoryList.TotalPages,
            page = pagedCategoryList.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedCategoryList.TotalCount,
            rows = data
        };
        return jsonSerializer.Serialize(gridData);
    }

    [WebMethod]
    private string FindAdminTracking()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var adminTrackingService = ServiceFactory.GetService<AdminTrackingServices>();
        var pagedAdminTracking = adminTrackingService.FindAdminTracking(pagingCriteria);


          var data = (from t in pagedAdminTracking.Entities
                    select new
                    {
                        TRACKINGID = t.TRACKINGID,                        
                        ADMINID = t.ADMINID,
                        CREATED_ON = Convert.ToDateTime(t.CREATED_ON).ToString("dd-MMM-yyyy"),
                        ACTION_DONE_ON_PAGE = t.ACTION_DONE_ON_PAGE,
                        EVENT_ACTION_DESCRIPTION = t.EVENT_ACTION_DESCRIPTION,
                        FKUSERID = t.FKUSERID,
                        LOGIN_NAME = t.LOGIN_NAME,
                        NAME = t.NAME
                    }).ToList();       


        var gridData = new
        {
            total = pagedAdminTracking.TotalPages,
            page = pagedAdminTracking.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedAdminTracking.TotalCount,
            rows = data
        };
        return jsonSerializer.Serialize(gridData);

    }
    [WebMethod]
    private string FindPendingInvoices()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var orderInfoService = ServiceFactory.GetService<OrderInfoService>();

        string ord = string.Empty;
        if (Request["ordType"] != null)
        {
            ord = Server.UrlDecode(Request["ordType"]);
        }
        else
        {
            ord = "";
        }
        var pagedOrderInfo = orderInfoService.FindPendingInvoices(pagingCriteria, ord);

         var data = (from user in pagedOrderInfo.Entities
                    select new
                    {
                                USERID = user.USERID,
                                LOGIN_NAME = user.LOGIN_NAME,
                                FIRST_NAME = user.FIRST_NAME,
                                LAST_NAME = user.LAST_NAME,
                                PHONE = user.PHONE,
                                CREATED_ON =Convert.ToDateTime(user.CREATED_ON).ToString("dd-MMM-yyyy") ,
                                ACCOUNT_TYPE = user.ACCOUNT_TYPE,
                                PK_ORDERID = user.PK_ORDERID,
                                ORDERID = user.ORDERID,
                                CUSTOMER_ID = user.CUSTOMER_ID,
                                STATUS = user.STATUS,
                                LICENSE_OPTED = user.LICENSE_OPTED,
                                LICENSE_TYPE = user.LICENSE_TYPE,

                           }).ToList();     
        var gridData = new
        {
            total = pagedOrderInfo.TotalPages,
            page = pagedOrderInfo.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedOrderInfo.TotalCount,
            rows = data
        };
        return jsonSerializer.Serialize(gridData);

    }

    private string ResetAdminPassword()
    {
        var adminUserService = ServiceFactory.GetService<AdminUserService>();
        string newPassword = System.Guid.NewGuid().ToString();
        var adminUser = adminUserService.ResetPassword(ValidationHelper.GetInteger(Request["Id"].ToString(), 0), newPassword);
        string rtnMsg = string.Empty;
        if (adminUser != null)
        {
            rtnMsg = "Password has been Reset Successfully.";
            string fromEmail = ConfigurationManager.AppSettings["emailActivation"];
            string subject = "Your Insighto Password";
            string MsgBody = "Reset Password :" + EncryptHelper.Decrypt(adminUser.PASSWORD);
            MailHelper.SendMailMessage(fromEmail, adminUser.LOGIN_NAME, "", "", subject, MsgBody);
        }
        return rtnMsg.ToString();


    }

    private string FindPaymentPendingUsers()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var userService = ServiceFactory.GetService<UsersService>();

        string paymentType = string.Empty;
        if (Request["paymentType"] != null)
        {
            paymentType = Server.UrlDecode(Request["paymentType"]);
        }
        else
        {
            paymentType = "";
        }
        var pagedUserInfo = userService.FindPaymentPendingUsers(pagingCriteria, paymentType);
        var data = (from d in pagedUserInfo.Entities
                    select new
                    {
                        FIRST_NAME = d.FIRST_NAME + " " + d.LAST_NAME,
                        LOGIN_NAME = d.LOGIN_NAME,
                        PHONE = d.PHONE,
                        LICENSE_TYPE = d.LICENSE_TYPE,
                        LICENSE_UPGRADE = d.LICENSE_OPTED,
                        UpgradeLinkUrl = EncryptHelper.EncryptQuerystring("AddUserPaymentDetails.aspx", "UserId=" + d.USERID.ToString() +"&Mode=U")
                    }).ToList();

        var gridData = new
        {
            total = pagedUserInfo.TotalPages,
            page = pagedUserInfo.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedUserInfo.TotalCount,
            rows = data
        };
        return jsonSerializer.Serialize(gridData);
    }

    private string UpgradeLicense()
    {
        var orderInfoService = ServiceFactory.GetService<OrderInfoService>();
        int orderId = 0;
        if (Request["id"] != null)
        {
            orderId = ValidationHelper.GetInteger(Server.UrlDecode(Request["id"]), 0);
        }
        var orderInfo = orderInfoService.FindOrderInfoByOrderId(orderId);
        string lic_tobe_update = "", curr_lic = "";
        DateTime dt = DateTime.UtcNow;
        DateTime dt_now = dt;
        int userId = 0, activ_flag = 0;
        string ord_type = "N";
        if (orderInfo != null)
        {
            lic_tobe_update = orderInfo.LICENSE_OPTED;
            userId = orderInfo.USERID;
            dt = Convert.ToDateTime(orderInfo.LICENSE_EXPIRY_DATE);
            curr_lic = orderInfo.LICENSE_TYPE;
            ord_type = orderInfo.ORDER_TYPE;
            activ_flag = ValidationHelper.GetInteger(orderInfo.ACTIVATION_FLAG, 0);

        }
        DateTime dt_expiry = DateTime.UtcNow;
        if (dt != Convert.ToDateTime("1/1/1800"))
        {
            dt_expiry = dt;
        }
        if (dt_now == Convert.ToDateTime("1/1/1800"))
        {
            dt_now = DateTime.UtcNow;
        }
        if (curr_lic != "FREE")
        {
            dt_now = dt;
        }

        if (lic_tobe_update == "PRO_MONTHLY")
            dt_expiry = dt_expiry.AddMonths(1);
        else if (lic_tobe_update == "PRO_QUARTERLY")
            dt_expiry = dt_expiry.AddMonths(3);
        else if (lic_tobe_update == "PRO_YEARLY")
            dt_expiry = dt_expiry.AddMonths(12);
        else if (lic_tobe_update == "PREMIUM_YEARLY")
            dt_expiry = dt_expiry.AddMonths(12);
        
        
      //  dt_expiry = Convert.ToDateTime(dt_expiry.Date).AddDays(1);
        dt_expiry = dt_expiry.ToUniversalTime();

        var user = new osm_user();
        user.LICENSE_EXPIRY_DATE = dt_expiry;


        if ((curr_lic == "FREE") || (curr_lic == "PRO_MONTHLY" && (lic_tobe_update == "PRO_QUARTERLY" || lic_tobe_update == "PRO_YEARLY")) || (curr_lic == "PRO_QUARTERLY" && lic_tobe_update == "PRO_YEARLY") || (curr_lic == "PREMIUM_YEARLY"))
        {
            user.LICENSE_TYPE = lic_tobe_update;
            user.LICENSE_CHANGEFLAG = 2;
            user.EMAIL_COUNT = 0;
        }
        else
        {
            user.LICENSETYPE_PENDING = lic_tobe_update;
            user.LICENSEPENDING_STARTDATE = dt;
            user.LICENSE_CHANGEFLAG = 1;
        }
        user.ACTIVATION_FLAG = 1;
        user.EMAIL_FLAG = 1;
        user.RENEWAL_ALERT = 0;
        user.RENEWAL_FLAG = 1;

        var ordInfo = new osm_orderinfo();
        ordInfo.PAID_DATE = DateTime.UtcNow;
        ordInfo.TRANSACTION_STATUS = 1;


        var accountHistory = new osm_useraccounthistory();
        accountHistory.LICENSE_ENDDATE = dt_expiry;
        accountHistory.LICENSE_STARTDATE = dt_now;
        accountHistory.LICENSE_TYPEFLAG = "0";
        accountHistory.EMAIL_FLAG = 1;



        int rtnValue = orderInfoService.UpgradeUsers(user, ordInfo, accountHistory, userId, orderId);
        return rtnValue.ToString();

    }

    private string DeleteAdminUser()
    {
        var adminUserService = ServiceFactory.GetService<AdminUserService>();
        int returnvalue = adminUserService.DeleteUserById(ValidationHelper.GetInteger(Request["Id"].ToString(), 0));
        return returnvalue.ToString();
    }

    [WebMethod]
    private string GetCategory()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        ServiceFactory.GetService<SessionStateService>().AddRecordsCountToSession(pagingCriteria.PageSize);
        var picklistService = ServiceFactory.GetService<PickListService>();
        var pagedCategoryList = picklistService.GetPickList("SURVEY_CATEGORY", pagingCriteria);
        int surveyFlag = ValidationHelper.GetInteger(Request["surveyFlag"].ToString(), 0);
        var data = (from d in pagedCategoryList.Entities
                    select new
                    {
                        PARAM_VALUE = d.PARAM_VALUE,
                        PICK_ID = d.PICK_ID,
                        ViewLinkUrl = EncryptHelper.EncryptQuerystring("ManageCategoryPopup.aspx", "Category=" + d.PARAM_VALUE.ToString() + "&surveyFlag=" + surveyFlag)
                    }).ToList();

        //anoymonous type of object
        var gridData = new
        {
            total = pagedCategoryList.TotalPages,
            page = pagedCategoryList.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedCategoryList.TotalCount,
            rows = data
        };
        return jsonSerializer.Serialize(gridData);
    }

    [WebMethod]
    private string GetSurveysByUser()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var currentIndex = ValidationHelper.GetInteger(Request["page"], 1);
        int pageSize = ValidationHelper.GetInteger(Request["rows"], 10); // get how many rows we want to have into the grid 

        var sidx = Request["sidx"]; // get index row - i.e. user click to sort 
        var sord = Request["sord"]; // get the direction 
        string type = "";
        if (Request["mode"] != null)
        {
            type = Server.UrlDecode(Request["mode"]);
        }
        else
        {
            type = "Active";
        }

        int totalCount = 0;
        //var data = new  List<osm_survey>();
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var getUserInfo = sessionService.GetLoginUserDetailsSession();

        var surveyData = surveyService.GetSurveyByUserID(getUserInfo.UserId);
        //anoymonous type of object
        double totalPages = System.Math.Ceiling(Convert.ToDouble(totalCount / pageSize)) + 1;
        var gridData = new
        {
            total = totalPages,
            page = totalPages > 0 ? currentIndex : 0,
            records = totalCount,
            rows = surveyData
        };


        return jsonSerializer.Serialize(gridData);

    }
    public SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    [WebMethod]
    private string GetSurveys()
 {

            var searchKey = string.Empty;
            if (Request["keyword"] != null || Request["keyword"] != "")
            {
                searchKey = Server.UrlDecode(Request["keyword"]);
            }
            else
                searchKey = "";
           
         


        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
       
        string type = "";
        int surveyFlag = 0;
        if (Request["mode"] != null && Request["mode"] != "undefined")
        {
            type = Server.UrlDecode(Request["mode"]);
        }
        else
        {
            type = "All";
        }
        if (Request["surveyFlag"] != null)
        {
            surveyFlag = ValidationHelper.GetInteger(Server.UrlDecode(Request["surveyFlag"]), 0);
        }
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();

          
       

        var getUserInfo = sessionService.GetLoginUserDetailsSession();


        SqlConnection con = new SqlConnection();

        con = strconnectionstring();
        SqlCommand scom = new SqlCommand("sp_GetTeleUser", con);
        scom.CommandType = CommandType.StoredProcedure;

        scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = getUserInfo.UserId;

        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet dsteleuser = new DataSet();
        sda.Fill(dsteleuser);
        con.Close();

        var pickListService = ServiceFactory.GetService<PickListService>();

      
        if (getUserInfo != null)
        {

            //if (dsteleuser.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
            //{
            //    var surveyData = surveyService.GetSurveysvoice(getUserInfo.UserId, type, pagingCriteria, surveyFlag);

            //    var gridData = new
            //    {
            //        total = surveyData.TotalPages,
            //        page = surveyData.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            //        records = surveyData.TotalCount,
            //        rows = surveyData.Entities
            //    };

            //    return jsonSerializer.Serialize(gridData);
            //}
            //else
            //{
          //  ServiceFactory.GetService<SessionStateService>().AddRecordsCountToSession(pagingCriteria.PageSize);
               var surveyData = surveyService.GetSurveys(getUserInfo.UserId, type,searchKey, pagingCriteria, surveyFlag);
          //  var surveyData = surveyService.GetAllSurveyNames(pagingCriteria, searchKey);
      
                var gridData = new
                {
                    total = surveyData.TotalPages,
                    page = surveyData.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
                    records = surveyData.TotalCount,
                    rows = surveyData.Entities
                };
                return jsonSerializer.Serialize(gridData);
           // }
            
        }
        else
        {
            var gridData = new
            {
                total = 1,
                page = 1,
                records = -302, //http status code, redirects if session out [including '-' to avoid record count may equal.]
                rows = new List<SurveyInfo>()
            };
            return jsonSerializer.Serialize(gridData);
        }
    }

    private PagingCriteria GetPagingCriteria(HttpRequest request)
    {
        var sortcolumn = Request["sidx"];
        if ((sortcolumn == "CREDITCOUNT") || (sortcolumn == "POLLLICENCETYPE") || (sortcolumn == "POLLLICEXPIRYDATE") || (sortcolumn == "POLLLICENCETYPEOPTED"))
        {
            sortcolumn = "MODIFIED_ON";
        }
        var pagingCriteria = new PagingCriteria
        {
            PageIndex = ValidationHelper.GetInteger(Request["page"], 1),
            PageSize = ValidationHelper.GetInteger(Request["rows"], 10),
            SortColumn = sortcolumn,
            SortOrder = Request["sord"]
        };

        return pagingCriteria;
    }

    private string DeleteSurveyBySurveyId()
    {
        var surveyService = ServiceFactory.GetService<SurveyService>();

        int returnvalue = surveyService.DeleteSurveyBySurveyId(ValidationHelper.GetInteger(Request["Id"].ToString(), 0));
        return returnvalue.ToString();


    }

    private string Upgrade_ProCredits_SurveyBySurveyId()
    {

        // sp for pro credits

    int surveyid= ValidationHelper.GetInteger(Request["Id"].ToString(), 0);
        return ToString();
    }

    private string Upgrade_PremCredits_SurveyBySurveyId()
    {

        // sp for prem credits
        return ToString();
    }
    private string BuySurveyCredits()
    {
        return ToString();
    }


    private string DeleteFolderByFolderId()
    {
        var folderservice = ServiceFactory.GetService<FolderService>();

        int returnValue = folderservice.DeleteFolderByFolderId(ValidationHelper.GetInteger(Request["Id"].ToString(), 0));
        return returnValue.ToString();


    }

    private string DeleteContactByContactListId()
    {
        var emailService = ServiceFactory.GetService<EmailService>();

        int returnValue = emailService.DeleteContactByContactListId(ValidationHelper.GetInteger(Request["Id"].ToString(), 0));
        return returnValue.ToString();
    }

    private string DeleteEmailById()
    {

        var emailService = ServiceFactory.GetService<EmailService>();

        int returnValue = emailService.DeleteEmailById(ValidationHelper.GetInteger(Request["Id"].ToString(), 0));
        return returnValue.ToString();
    }

    private string SaveResponseAnswerOptions()
    {
        var respondentId = ValidationHelper.GetInteger(Request["respondentId"], 0);
        var questionId = ValidationHelper.GetInteger(Request["questionId"], 0);
        var answerOptionId = ValidationHelper.GetInteger(Request["answerOptionId"], 0);
        var value = Request["otherText"];

        var responseQuestion = GetResponseQuestionInstance(0);
        responseQuestion.RESPONDENT_ID = respondentId;
        responseQuestion.QUESTION_ID = questionId;
        responseQuestion.ANSWER_ID = answerOptionId;
        responseQuestion.ANSWER_TEXT = value;

        var responseList = new List<osm_responsequestions> { responseQuestion };
        ServiceFactory.GetService<RespondentService>().SaveRespondentData(0, 0, SurveyResponseStatus.Partial, null, DbOperationType.UpdateStatus, respondentId);
        var affectedRows = ServiceFactory.GetService<RespondentService>().Save(responseList, true);
        return affectedRows > 0 ? "Success" : "Failure";
    }

    [WebMethod]
    private string FindSurveysByCategory()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        ServiceFactory.GetService<SessionStateService>().AddRecordsCountToSession(pagingCriteria.PageSize);
        string type = "";
        if (Request["mode"] != null)
            type = Server.UrlDecode(Request["mode"]);
        else
            type = "All";

        string category = "";
        if (Request["cid"] != null && Server.UrlDecode(Request["reqType"]) == "category")
        {
            category = Server.UrlDecode(Request["cid"]);
        }
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        int surveyFlag = ValidationHelper.GetInteger(Request["surveyFlag"].ToString(), 0);
        var pagedSurveylist = surveyService.FindSurveysByCategory(userInfo.UserId, pagingCriteria, category);
        var data = (from d in pagedSurveylist.Entities
                    select new
                    {
                        SURVEY_ID = d.SURVEY_ID,
                        SURVEY_NAME = d.SURVEY_NAME,
                        SURVEY_CATEGORY = d.SURVEY_CATEGORY,
                        FOLDER_NAME = d.FOLDER_NAME,
                        NAME = d.NAME,
                        SEND_TO = d.SEND_TO,
                        TOTAL_RESPONSES = d.TOTAL_RESPONSES,
                        STATUS = d.STATUS,
                        SurveyUrl = "<a href='#' onclick='closeModalSelf(true,\"" + EncryptHelper.EncryptQuerystring("SurveyManager.aspx", "SurveyId=" + d.SURVEY_ID.ToString() + "&surveyFlag=" + surveyFlag) + "\")'>" + (d.SURVEY_NAME.Length > 15 ? d.SURVEY_NAME.Substring(0, 15) + "..." : d.SURVEY_NAME).ToString() + "</a>",
                        FolderUrl = "<a href='" + EncryptHelper.EncryptQuerystring("ViewFolderPopUp.aspx", "FolderId=" + d.FOLDER_ID.ToString()) + "'>" + d.FOLDER_NAME + "</a>"
                    }).ToList();

        //anoymonous type of object
        var gridData = new
        {
            total = pagedSurveylist.TotalPages,
            page = pagedSurveylist.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedSurveylist.TotalCount,
            rows = data
        };
        return jsonSerializer.Serialize(gridData);
    }


    [WebMethod]
    private string FindSurveysByFolder()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        ServiceFactory.GetService<SessionStateService>().AddRecordsCountToSession(pagingCriteria.PageSize);
        string type = "";
        if (Request["mode"] != null)
            type = Server.UrlDecode(Request["mode"]);
        else
            type = "All";

        int FolderId = 0;
        if (Request["cid"] != null && Server.UrlDecode(Request["reqType"]) == "folders")
            FolderId = Convert.ToInt32(Server.UrlDecode(Request["cid"]));

        var surveyService = ServiceFactory.GetService<SurveyService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        var pagedSurveylist = surveyService.FindSurveys(userInfo.UserId, pagingCriteria, FolderId);
        var data = (from d in pagedSurveylist.Entities
                    select new
                    {
                        SURVEY_ID=d.SURVEY_ID,
                        SURVEY_NAME = d.SURVEY_NAME,
                        SURVEY_CATEGORY = d.SURVEY_CATEGORY,
                        NAME = d.NAME,
                        SEND_TO = d.SEND_TO,
                        TOTAL_RESPONSES = d.TOTAL_RESPONSES,
                        STATUS = d.STATUS,
                        SurveyUrl = "<a href='#' onclick='closeModalSelf(true,\"" + EncryptHelper.EncryptQuerystring("SurveyManager.aspx", "SurveyId=" + d.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag) + "\")'>" + (d.SURVEY_NAME.Length > 15 ? d.SURVEY_NAME.Substring(0, 15) + "..." : d.SURVEY_NAME).ToString() + "</a>"
                    }).ToList();

        //anoymonous type of object
        var gridData = new
        {
            total = pagedSurveylist.TotalPages,
            page = pagedSurveylist.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedSurveylist.TotalCount,
            rows = data
        };
        return jsonSerializer.Serialize(gridData);
    }

    [WebMethod]
    private string GetEmailsByList()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        ServiceFactory.GetService<SessionStateService>().AddRecordsCountToSession(pagingCriteria.PageSize);
        int contactListId = 0;
        string issearch = "no";
        string searchtxt = "";
        if (Request["cid"] != null)
            contactListId = Convert.ToInt32(Server.UrlDecode(Request["cid"]));     
        if (Request["search"] != null)
            issearch = Server.UrlDecode(Request["search"]).ToString();    
        if (Request["searchtxt"] != null)
            searchtxt = Server.UrlDecode(Request["searchtxt"]).ToString();
        var emailService = ServiceFactory.GetService<EmailService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
         PagedList<osm_emaillist> pagedFolderlist;
        if (issearch == "yes")
        
           pagedFolderlist = emailService.SearchEmailAddress(contactListId, searchtxt, pagingCriteria);
        
        else
        
             pagedFolderlist = emailService.GetEmailsByContactListId(contactListId, pagingCriteria);
        
        var data = (from d in pagedFolderlist.Entities
                    select new
                    {
                        EMAIL_ADDRESS = d.EMAIL_ADDRESS.Trim(),
                        FIRST_NAME = d.FIRST_NAME != null ? d.FIRST_NAME.Trim() : "",
                        LAST_NAME = d.LAST_NAME != null ? d.LAST_NAME.Trim() : "",
                        CUSTOM_VAR1 = d.CUSTOM_VAR1 != null ? d.CUSTOM_VAR1.Trim() : "",
                        CUSTOM_VAR2 = d.CUSTOM_VAR2 != null ? d.CUSTOM_VAR2.Trim() : "",
                        CUSTOM_VAR3 = d.CUSTOM_VAR3 != null ? d.CUSTOM_VAR3.Trim() : "",
                        CUSTOM_VAR4 = d.CUSTOM_VAR4 != null ? d.CUSTOM_VAR4.Trim() : "",
                        CUSTOM_VAR5 = d.CUSTOM_VAR5 != null ? d.CUSTOM_VAR5.Trim() : "",
                        CUSTOM_VAR6 = d.CUSTOM_VAR6 != null ? d.CUSTOM_VAR6.Trim() : "",
                        CUSTOM_VAR7 = d.CUSTOM_VAR7 != null ? d.CUSTOM_VAR7.Trim() : "",
                        CUSTOM_VAR8 = d.CUSTOM_VAR8 != null ? d.CUSTOM_VAR8.Trim() : "",
                        EMAIL_ID = d.EMAIL_ID,
                        EditUrl = EncryptHelper.EncryptQuerystring("CreateEmailList.aspx", "EmailId=" + d.EMAIL_ID.ToString() + "&ContactId=" + d.CONTACTLIST_ID.ToString() + "&surveyFlag=" + SurveyFlag)
                    }).ToList();

        //anoymonous type of object
        var gridData = new
        {
            total = pagedFolderlist.TotalPages,
            page = pagedFolderlist.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedFolderlist.TotalCount,
            rows = data,
            cols = "[EMAIL]"
        };
        return jsonSerializer.Serialize(gridData);
    }

    private string GetAddressBook()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        ServiceFactory.GetService<SessionStateService>().AddRecordsCountToSession(pagingCriteria.PageSize);
        var contactService = ServiceFactory.GetService<ContactListService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        var pagedFolderlist = contactService.FindContactListByUserId(userInfo.UserId, pagingCriteria);
        //anoymonous type of object

        var gridData = new
        {
            total = pagedFolderlist.TotalPages,
            page = pagedFolderlist.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedFolderlist.TotalCount,
            rows = pagedFolderlist.Entities
        };
        return jsonSerializer.Serialize(gridData);
    }

    private string CopySurveyFromTemplate()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var surveyService = ServiceFactory.GetService<SurveyService>();

        int copySurveyID = ValidationHelper.GetInteger(Request["copySurveyId"].ToString(), 0);
        int newSurveyID = ValidationHelper.GetInteger(Request["newSurveyId"].ToString(), 0);

        surveyService.CopySurveyDet(copySurveyID, newSurveyID);
        var surveDet = surveyService.GetSurveyBySurveyId(newSurveyID);
        string url = EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + newSurveyID + "&surveyName=" + surveDet[0].SURVEY_NAME + "&surveyFlag=" + SurveyFlag);
        return url;
    }

    private string CopySurveyForQuestionLibrary()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var surveyService = ServiceFactory.GetService<SurveyService>();

        int copySurveyID = ValidationHelper.GetInteger(Request["copySurveyId"].ToString(), 0);
        //int newSurveyID = ValidationHelper.GetInteger(Request["newSurveyId"].ToString(), 0);
        var oldSurvey = surveyService.GetSurveyBySurveyId(copySurveyID).FirstOrDefault();
        oldSurvey.USERID = -1;
        var newSurvey = surveyService.AddSurvey(oldSurvey);

        surveyService.CopySurveyDet(copySurveyID, newSurvey.SURVEY_ID);
        var surveDet = surveyService.GetSurveyBySurveyId(newSurvey.SURVEY_ID);
        if (surveDet != null)
        {
            return "1";
        }
        else
        {
            return "0";
        }
    }

    private string GetSuveysByCategory()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        int newSurveyID = ValidationHelper.GetInteger(Request["newSurveyId"].ToString(), 0);
        int userId = -1;
        string categoryName = "";
        if (Request["category"] != null)
            categoryName = Server.UrlDecode(Request["category"]);
        string DisplayMode = Request["mode"].ToString();
        string Surveycreateurl = "";
        if (DisplayMode == "CopyExsistingSurvey")
        {
            userId = userInfo.UserId;
        }
        var pagedSurveylist = surveyService.GetCategoriesBySurveyId(userId, categoryName, pagingCriteria, newSurveyID);
        if (newSurveyID == 0)
        {
            Surveycreateurl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyBasicsURL(), "surveyFlag=" + SurveyFlag);
        }
        //var modeType = string.Empty;
        //if (DisplayMode == "CopyExsistingSurvey")
        //    modeType = RespondentDisplayMode.Preview.ToString();
        //else
        //    modeType = RespondentDisplayMode.Preview.ToString();

        var data = (from d in pagedSurveylist.Entities
                    select new
                    {
                        SURVEY_NAME = d.SURVEY_NAME,
                        STATUS = d.STATUS,
                        SURVEY_ID = d.SURVEY_ID+"~"+Surveycreateurl,
                        PreviewLinkUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewURL().Replace("~/", ""), "Mode=" + RespondentDisplayMode.Preview.ToString() + "&SurveyId=" + d.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag)
                    }).ToList();

        var gridData = new
        {
            total = pagedSurveylist.TotalPages,
            page = pagedSurveylist.TotalCount > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedSurveylist.TotalCount,
            rows = data
        };

        return jsonSerializer.Serialize(gridData);
    }

    [WebMethod]
    private string GetOrderInofByUserId()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);

        var orderInfoService = ServiceFactory.GetService<OrderInfoService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var getUserInfo = sessionService.GetLoginUserDetailsSession();
        if (getUserInfo != null)
        {
            var orderData = orderInfoService.GetOrderInofByUserId(getUserInfo.UserId, pagingCriteria);

            var gridData = new
            {
                total = orderData.TotalPages,
                page = orderData.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
                records = orderData.TotalCount,
                rows = orderData.Entities
            };
            return jsonSerializer.Serialize(gridData);
        }
        return string.Empty;
    }

    private string GetSurveysByContactId()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        int ContactId = 0;
        if (Request["cid"] != null)
        {
            ContactId = ValidationHelper.GetInteger(Server.UrlDecode(Request["cid"]), 0);
        }

        var surveyService = ServiceFactory.GetService<SurveyService>();

        var surveyData = surveyService.FindSurveysByContactListId(ContactId, pagingCriteria);

        var gridData = new
        {
            total = surveyData.TotalPages,
            page = surveyData.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = surveyData.TotalCount,
            rows = surveyData.Entities
        };
        return jsonSerializer.Serialize(gridData);
    }
    public string ActivateSurveyBySurveyId()
    {
        var surveyService = ServiceFactory.GetService<SurveyService>();

        DataSet dsppstype = surcore.getSurveyType(ValidationHelper.GetInteger(Request["Id"].ToString(), 0));
        bool isNotProFeatureExist;


        if (dsppstype.Tables[0].Rows[0][0].ToString() != "")
        {
            isNotProFeatureExist = false;
        }
        else
        {
            isNotProFeatureExist = surveyService.CheckSurveyProFeatures(ValidationHelper.GetInteger(Request["Id"].ToString(), 0));
        }

       // bool isNotProFeatureExist = surveyService.CheckSurveyProFeatures(ValidationHelper.GetInteger(Request["Id"].ToString(), 0));
        if (isNotProFeatureExist)
        {
            surveyService.ChangeSurveyStatus(ValidationHelper.GetInteger(Request["Id"].ToString(), 0), "Active");
            return "Activated";
        }
        else
        {
            return "ProFeatures";
        }


    }

    [WebMethod]
    private String GetAllUserDetails()
    {
        PollCreation pc= new PollCreation();
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var userService = ServiceFactory.GetService<UsersService>();

        if (Request["IsAdvancedSearch"] == null)
        {

            var searchKey = string.Empty;
            if (Request["keyword"] != null || Request["keyword"] != "")
            {
                if (Request["keyword"] != "CREDITCOUNT")
                {
                    searchKey = Server.UrlDecode(Request["keyword"]);
                }
            }
            else
                searchKey = "";
            var pagedAdminUsers = userService.GetAllUserDetails(pagingCriteria, searchKey);
            DataTable dtpolls = pc.getAllUsersPollLicences();
            var data = (from d in pagedAdminUsers.Entities
                        select new
                        {
                            LOGIN_NAME = d.LOGIN_NAME,
                            FIRST_NAME = d.FIRST_NAME,
                            LAST_NAME = d.LAST_NAME,
                            PHONE = d.PHONE,
                            CREATED_ON = Convert.ToDateTime(d.CREATED_ON).AddMinutes(d.STANDARD_BIAS).ToString("dd-MMM-yyyy"),
                            LICENSE_EXPIRY_DATE = (d.ACTIVATION_FLAG != 1 || d.ACTIVATION_FLAG==1) &&(d.LICENSE_TYPE=="FREE")&& (d.LICENSE_OPTED == "PREMIUM_YEARLY" || d.LICENSE_OPTED == "PRO_YEARLY" || d.LICENSE_OPTED == "PRO_MONTHLY") ? "No Expiry" : Convert.ToDateTime(d.LICENSE_EXPIRY_DATE).ToString("dd-MMM-yyyy") == "01-Jan-1800" ? "No Expiry" : Convert.ToDateTime(d.LICENSE_EXPIRY_DATE).ToString("dd-MMM-yyyy"),
                            STATUS = d.ACTIVATION_FLAG == 1?"Active":"Pending",
                            LICENSE_TYPE = d.LICENSE_TYPE,
                            LICENSE_OPTED = d.LICENSE_OPTED,
                            CUSTOMER_ID = d.CUSTOMER_ID,
                            USERID = d.USERID,
                            MODIFIED_ON = Convert.ToDateTime(d.MODIFIED_ON).ToString("dd-MMM-yyyy"),                            
                            EditUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetEditUserURL().Replace("~/", ""), "UserId=" + d.USERID.ToString()),
                            ViewUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetViewUserURL().Replace("~/", ""), "UserId=" + d.USERID.ToString()),
                            POLLLICENCETYPE = dtpolls.Select("UserID = " + d.USERID.ToString())[0][1].ToString(),
                            POLLLICEXPIRYDATE = Convert.ToDateTime(dtpolls.Select("UserID = " + d.USERID.ToString())[0][2].ToString()).AddMinutes(d.STANDARD_BIAS).ToString("dd-MMM-yyyy"),
                            POLLLICENCETYPEOPTED = dtpolls.Select("UserID = " + d.USERID.ToString())[0][3].ToString(),
                            CREDITCOUNT = pc.GetSurveyCreditCount(d.USERID)
                        }).ToList();
            
            if ((Request["sidx"] == "CREDITCOUNT") || (Request["sidx"] == "POLLLICENCETYPE") || (Request["sidx"] == "POLLLICEXPIRYDATE") || (Request["sidx"] == "POLLLICENCETYPEOPTED"))
            {
                if ((Request["sord"] == "asc") && (Request["sidx"] == "CREDITCOUNT"))
                {
                    data = data.OrderBy(a => a.CREDITCOUNT).ToList();
                }
                else if ((Request["sord"] == "desc") && (Request["sidx"] == "CREDITCOUNT"))
                {
                    data = data.OrderByDescending(a => a.CREDITCOUNT).ToList();
                }
                else if ((Request["sord"] == "asc") && (Request["sidx"] == "POLLLICENCETYPE"))
                {
                    data = data.OrderBy(a => a.POLLLICENCETYPE).ToList();
                }
                else if ((Request["sord"] == "desc") && (Request["sidx"] == "POLLLICENCETYPE"))
                {
                    data = data.OrderByDescending(a => a.POLLLICENCETYPE).ToList();
                }
                else if ((Request["sord"] == "asc") && (Request["sidx"] == "POLLLICEXPIRYDATE"))
                {
                    data = data.OrderBy(a => a.POLLLICEXPIRYDATE).ToList();
                }
                else if ((Request["sord"] == "desc") && (Request["sidx"] == "POLLLICEXPIRYDATE"))
                {
                    data = data.OrderByDescending(a => a.POLLLICEXPIRYDATE).ToList();
                }
                else if ((Request["sord"] == "asc") && (Request["sidx"] == "POLLLICENCETYPEOPTED"))
                {
                    data = data.OrderBy(a => a.POLLLICENCETYPEOPTED).ToList();
                }
                else if ((Request["sord"] == "desc") && (Request["sidx"] == "POLLLICENCETYPEOPTED"))
                {
                    data = data.OrderByDescending(a => a.POLLLICENCETYPEOPTED).ToList();
                }
                
            }
            var gridData = new
            {
                total = pagedAdminUsers.TotalPages,
                page = pagedAdminUsers.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
                records = pagedAdminUsers.TotalCount,
                rows = data//pagedAdminUsers.Entities
            };
            return jsonSerializer.Serialize(gridData);
        }
        else
        {
            return UserAdvanceSearchDetails();
        }

    }
    [WebMethod]
    private string DeleteUserByUserId()
    {
        var userService = ServiceFactory.GetService<UsersService>();
        int returnValue = userService.DeleteUserByUserId(ValidationHelper.GetInteger(Request["Id"].ToString(), 0));
        return returnValue.ToString();
    }
    /// <summary>
    /// Gets the instance.
    /// </summary>
    /// <param name="deleted">The deleted.</param>
    /// <returns></returns>
    private static osm_responsequestions GetResponseQuestionInstance(int deleted)
    {
        return new osm_responsequestions { DELETED = deleted, RESPONSE_RECEIVED_ON = DateTime.UtcNow };
    }


   
    [WebMethod]

    private String GetUserSurveyDetails()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var pagingCriteria = GetPagingCriteria(Request);
        var getUserInfo = sessionService.GetLoginUserDetailsSession();
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var pagedAdminUsers = surveyService.GetUserDashoboard(ValidationHelper.GetInteger(Request["UserId"].ToString(), 0), pagingCriteria);
        //var data = (from d in pagedAdminUsers.Entities
        //            select new
        //            {
        //                SurveyName = d.SurveyName,
        //                CategoryName = d.CategoryName,
        //                Status = d.Status,
        //                SendTo = d.SendTo,
        //                Responses = d.Responses,
        //                SurveyId = d.SurveyId
        //            }).ToList();

        var gridData = new
        {
            total = pagedAdminUsers.TotalPages,
            page = pagedAdminUsers.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedAdminUsers.TotalCount,
            rows = pagedAdminUsers.Entities//pagedAdminUsers.Entities
        };
        return jsonSerializer.Serialize(gridData);
    }

    [WebMethod]
    private String GetDeletedSurveyDetails()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var getUserInfo = sessionService.GetLoginUserDetailsSession();
        var pagingCriteria = GetPagingCriteria(Request);
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var pagedDeletedSurveys = surveyService.GetUserDeletedSurveyDetails(ValidationHelper.GetInteger(Request["UserId"].ToString(), 0), pagingCriteria);
        var gridData = new
        {
            total = pagedDeletedSurveys.TotalPages,
            page  = pagedDeletedSurveys.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedDeletedSurveys.TotalCount,
            rows = pagedDeletedSurveys.Entities//pagedAdminUsers.Entities
        };
        return jsonSerializer.Serialize(gridData);
    }

    private String UserAdvanceSearchDetails()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var userService = ServiceFactory.GetService<UsersService>();
        if (Request.QueryString["key"] != null)
        {
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
        }
        var pagedAdminUsers = userService.UserAdvanceSearchDetails(pagingCriteria, Server.UrlDecode(Request.QueryString[Constants.FirstName]), Server.UrlDecode(Request.QueryString[Constants.lastName]), Server.UrlDecode(Request.QueryString[Constants.City]), Server.UrlDecode(Request.QueryString[Constants.State]), Server.UrlDecode(Request.QueryString[Constants.Company]), Server.UrlDecode(Request.QueryString[Constants.LicenseType]),
            Server.UrlDecode(Request.QueryString[Constants.JobFunction]), Server.UrlDecode(Request.QueryString[Constants.WorkIndustry]), Server.UrlDecode(Request.QueryString[Constants.CompanySize]), Server.UrlDecode(Request.QueryString[Constants.EmailId]),
            Server.UrlDecode(Request.QueryString[Constants.CreatedFrom]), Server.UrlDecode(Request.QueryString[Constants.CreatedTo]), Server.UrlDecode(Request.QueryString[Constants.ExpiryDateFrom]), Server.UrlDecode(Request.QueryString[Constants.ExpiryDateTo]),
             Server.UrlDecode(Request.QueryString[Constants.CustomerId]),ValidationHelper.GetInteger(Server.UrlDecode(Request.QueryString[Constants.LicenseExpiryPeriod]),0), Server.UrlDecode(Request.QueryString[Constants.UserType]));
        var data = (from d in pagedAdminUsers
                    select new
                    {
                        LOGIN_NAME = d.LOGIN_NAME,
                        FIRST_NAME = d.FIRST_NAME,
                        LAST_NAME = d.LAST_NAME,
                        PHONE = d.PHONE,
                        CREATED_ON = Convert.ToDateTime(d.CREATED_ON).ToString("dd-MMM-yyyy"),
                        LICENSE_EXPIRY_DATE = Convert.ToDateTime(d.LICENSE_EXPIRY_DATE).ToString("dd-MMM-yyyy") == "01-Jan-1800" ? "No Expiry" : Convert.ToDateTime(d.LICENSE_EXPIRY_DATE).ToString("dd-MMM-yyyy") ,
                        STATUS = d.STATUS,
                        LICENSE_TYPE = d.LICENSE_TYPE,
                        LICENSE_OPTED = d.LICENSE_OPTED,
                        CUSTOMER_ID = d.CUSTOMER_ID,
                        USERID = d.USERID,
                        EditUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetEditUserURL().Replace("~/", ""), "UserId=" + d.USERID.ToString()),
                        ViewUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetViewUserURL().Replace("~/", ""), "UserId=" + d.USERID.ToString())
                    }).ToList();

        var gridData = new
        {
            //total = pagedAdminUsers.TotalPages,
            page = data.Count > 0 ? pagingCriteria.PageIndex : 0,
            //records = pagedAdminUsers.TotalCount,
            rows = data//pagedAdminUsers.Entities
        };
        return jsonSerializer.Serialize(gridData);
    }

    [WebMethod]
    private String GetAllBlockedWords()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var blockedService = ServiceFactory.GetService<BlockedWordsService>();
        var pagedBlockWords = blockedService.GetAllBlockedWords(pagingCriteria);

        var data = (from d in pagedBlockWords.Entities
                    select new
                    {
                        BLOCKEDWORD_NAME = d.BLOCKEDWORD_NAME,
                        BLOCKEDWORD_DESCRIPTION = d.BLOCKEDWORD_DESCRIPTION,
                        EditUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetEditBlockWordUrl().Replace("~/", ""), "BlockWordId=" + d.BLOCKEDWORD_ID.ToString()),
                        BLOCKEDWORD_ID = d.BLOCKEDWORD_ID
                    }).ToList();

        var gridData = new
        {
            total = pagedBlockWords.TotalPages,
            page = pagedBlockWords.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedBlockWords.TotalCount,
            rows = data
        };

        return jsonSerializer.Serialize(gridData);

    }

    [WebMethod]
    private string DeleteBlockedWordById()
    {
        var blockedService = ServiceFactory.GetService<BlockedWordsService>();
        int returnValue = blockedService.DeleteBlockedWord(ValidationHelper.GetInteger(Request["BlockedWordId"].ToString(), 0));
        return returnValue.ToString();
    }

    [WebMethod]
    private String ViewBlockedWordList()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var blockedService = ServiceFactory.GetService<BlockedWordsService>();
        var pagedBlockWords = blockedService.ViewBlockedWordDetails(pagingCriteria);
        var surveyFlag = 0;
        var Layout = 0;
        var data = (from d in pagedBlockWords.Entities
                    select new
                    {
                        SurveyName = d.SurveyName,
                        UserName = d.UserName,
                        LoginName = d.LoginName,
                        EditUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetBlockedWordListUrl().Replace("~/", ""), "SurveyId=" + d.SurveyId.ToString()),
                        Preview = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewURL().Replace("~/", "../"), Constants.SURVEYID + "=" + d.SurveyId.ToString() + "&Mode=" + RespondentDisplayMode.PreviewWithPagination.ToString() + "&surveyFlag=" + surveyFlag + "&Layout=" + Layout),
                        Legitimate = d.SurveyId,
                      }).ToList();

        var gridData = new
        {
            total = pagedBlockWords.TotalPages,
            page = pagedBlockWords.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedBlockWords.TotalCount,
            rows = data
        };

        return jsonSerializer.Serialize(gridData);

    }

    [WebMethod]
    private String ViewSurveyBlockedWordList()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var blockedService = ServiceFactory.GetService<BlockedWordsService>();
        var pagedBlockWords = blockedService.ViewSurveyBlockedWordList(pagingCriteria, ValidationHelper.GetInteger(Request["SurveyId"].ToString(), 0));
        var data = (from d in pagedBlockWords.Entities
                    select new
                    {
                        FeildName = d.FeildName.Replace("QUESTION_LABEL", "Question").Replace("ANSWER_OPTIONS", "Answer"),
                        BlockeWords = d.BlockeWords

                    }).ToList();

        var gridData = new
        {
            total = pagedBlockWords.TotalPages,
            page = pagedBlockWords.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedBlockWords.TotalCount,
            rows = data
        };

        return jsonSerializer.Serialize(gridData);
    }

    [WebMethod]
    private String GetContactuserList()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var contactService = ServiceFactory.GetService<ContactUsService>();
        var pagedContactList = contactService.GetContactList(pagingCriteria);

        var data = (from c in pagedContactList.Entities
                    select new
                    {
                        USERNAME = c.USERNAME,
                        EMAILADDRESS = c.EMAILADDRESS,
                        PHONENO = c.PHONENO,
                        ATTENTIONVALUE = c.ATTENTIONVALUE,
                        CREATED_ON =  Convert.ToDateTime(c.CREATED_ON).ToString("dd-MMM-yyyy"),
                        MAILMESSAGE = (c.MAILMESSAGE.Length > 15 ? c.MAILMESSAGE.Substring(0, 15) + "..." : c.MAILMESSAGE).ToString(),
                        EditUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetEditContactUsUrl().Replace("~/", ""), "ContactusId=" + c.CONTACTUSUSERID.ToString()),
                        CONTACTUSUSERID = c.CONTACTUSUSERID
                    }).ToList();

        var gridData = new
        {
            total = pagedContactList.TotalPages,
            page = pagedContactList.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = pagedContactList.TotalCount,
            rows = data
        };

        return jsonSerializer.Serialize(gridData);
    }

    [WebMethod]
    private String GetRoles()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var roleService = ServiceFactory.GetService<RoleService>();
        var roles = roleService.GetAllRoles(pagingCriteria);
        var data = (from d in roles.Entities
                    select new
                    {
                        ROLE_NAME = d.ROLE_NAME,
                        EditLinkUrl = EncryptHelper.EncryptQuerystring("AddRole.aspx", "RoleId=" + d.ROLEID.ToString() + "&RoleName=" + d.ROLE_NAME)

                    }).ToList();

        var gridData = new
        {
            total = roles.TotalPages,
            page = roles.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = roles.TotalCount,
            rows = data
        };

        return jsonSerializer.Serialize(gridData);
    }


    [WebMethod]
    private String GetCommonFeatures()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        var featureService = ServiceFactory.GetService<FeatureService>();
        var data = featureService.GetCommonFeaturesByFlag(1);
        var gridData = new
        {
            rows = data
        };
        return jsonSerializer.Serialize(gridData);
    }
    /// <summary>
    /// Gets the bounced emails.
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    private string GetBouncedEmails()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        int surveyId = 0;
       // if (Request["surveyId"] == null || Convert.ToInt32(Server.UrlDecode(Request["surveyId"])) == 0)
       // {
       //     var emptyData = new
       //{
       //    total = 0,
       //    page = 0,
       //    records = 0,
       //    rows = string.Empty
       //};
       //     return jsonSerializer.Serialize(emptyData);
       // }

        surveyId = Convert.ToInt32(Server.UrlDecode(Request["surveyId"]));
        var emailService = ServiceFactory.GetService<EmailService>();

        var emailList = ServiceFactory<EmailService>.Instance.GetBouncedEmails(pagingCriteria, surveyId);
        var data = (from d in emailList.Entities
                    select new
                    {
                        EMAIL_ADDRESS = d.EMAIL_ADDRESS.Trim(),
                        FIRST_NAME = !string.IsNullOrEmpty(d.FIRST_NAME) ? d.FIRST_NAME.Trim() : string.Empty,
                        LAST_NAME = !string.IsNullOrEmpty(d.LAST_NAME) ? d.LAST_NAME.Trim() : string.Empty,
                        EMAIL_ID = d.EMAIL_ID,
                        STATUS = d.STATUS
                    }).ToList();

        //anoymonous type of object
        var gridData = new
        {
            total = emailList.TotalPages,
            page = emailList.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = emailList.TotalCount,
            rows = data,
        };
        return jsonSerializer.Serialize(gridData);
  
    }
    private string UpdateSurveyStatus()
    {
        var surveyService = ServiceFactory.GetService<SurveyService>();
        int returnvalue = surveyService.UpdateBlockedWordStatus(ValidationHelper.GetInteger(Request["surveyId"],0));
        return returnvalue.ToString();
    }

     [WebMethod]
    private string GetFeaturesByGroup()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
        string featureGroup = string.Empty;
        featureGroup = Server.UrlDecode(Request["featureGroup"]);

        var featureService = ServiceFactory.GetService<FeatureService>();

        var features = featureService.GetFeaturesByGroup(featureGroup, pagingCriteria);
        var data = (from f in features.Entities
                    select new
                    {
                        FeatureGroup = f.FEATURE_GROUP.Trim(),
                        FeatureName = f.FEATURE_NAME.Trim(),
                        Free = f.FREE,
                        ProMonthly =f.PRO_MONTHLY ,                       
                        ProQuaterly =f.PRO_QUARTERLY,
                        ProYearly = f.PRO_YEARLY,
                        PremiumMonthly =f.PREMIUM_MONTHLY,
                        PreimumQuaterly =f.PREMIUM_QUARTERLY,
                        PremiumYearly = f.PREMIUM_YEARLY,
                        FeatureTypeFlag = f.FEATURE_TYPE_FLAG,
                        Feature = f.FEATURE,
                        FeatureID = f.PK_FEATURE_ID,
                        EditLinkUrl = EncryptHelper.EncryptQuerystring("AddFeatureGroup.aspx", "FeatureID=" + f.PK_FEATURE_ID.ToString() + "&FeatureGroup=" + f.FEATURE_GROUP.ToString())
                        
                    }).ToList();

        //anoymonous type of object
        var gridData = new
        {
            total = features.TotalPages,
            page = features.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = features.TotalCount,
            rows = data,
        };
        return jsonSerializer.Serialize(gridData);
    }

     private string DeleteFeatureById()
    {
        var featureService = ServiceFactory.GetService<FeatureService>();
        int returnValue = featureService.DeleteFeatureById(ValidationHelper.GetInteger(Request["Id"].ToString(), 0));
        return returnValue.ToString();
    }

     [WebMethod]
     private string SearchEmails()
     {
         var jsonSerializer = new JavaScriptSerializer();
         var pagingCriteria = GetPagingCriteria(Request);
         ServiceFactory.GetService<SessionStateService>().AddRecordsCountToSession(pagingCriteria.PageSize);
         int contactListId = 0;
         string searchtext="";
         if (Request["cid"] != null)
             contactListId = Convert.ToInt32(Server.UrlDecode(Request["cid"]));
         if (Request["searchtxt"] != null)
             searchtext =Server.UrlDecode(Request["searchtxt"]).ToString();
         var emailService = ServiceFactory.GetService<EmailService>();
         var sessionService = ServiceFactory.GetService<SessionStateService>();
         var userInfo = sessionService.GetLoginUserDetailsSession();
         var pagedFolderlist = emailService.SearchEmailAddress(contactListId,searchtext,pagingCriteria);
         var data = (from d in pagedFolderlist.Entities
                     select new
                     {
                         EMAIL_ADDRESS = d.EMAIL_ADDRESS.Trim(),
                         FIRST_NAME = d.FIRST_NAME != null ? d.FIRST_NAME.Trim() : "",
                         LAST_NAME = d.LAST_NAME != null ? d.LAST_NAME.Trim() : "",
                         CUSTOM_VAR1 = d.CUSTOM_VAR1 != null ? d.CUSTOM_VAR1.Trim() : "",
                         CUSTOM_VAR2 = d.CUSTOM_VAR2 != null ? d.CUSTOM_VAR2.Trim() : "",
                         EMAIL_ID = d.EMAIL_ID,
                         EditUrl = EncryptHelper.EncryptQuerystring("CreateEmailList.aspx", "EmailId=" + d.EMAIL_ID.ToString() + "&ContactId=" + d.CONTACTLIST_ID.ToString() + "&surveyFlag=" + SurveyFlag)
                     }).ToList();

         //anoymonous type of object
         var gridData = new
         {
             total = pagedFolderlist.TotalPages,
             page = pagedFolderlist.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
             records = pagedFolderlist.TotalCount,
             rows = data,
             cols = "[EMAIL]"
         };
         return jsonSerializer.Serialize(gridData);
     }
     [WebMethod]
     private string RestoreSurveys()
     {
         var surveyService = ServiceFactory.GetService<SurveyService>(); 
         var surveyId=Request["SurveyIds"].ToString();
         int retrunvalue=0;
         foreach (var uSurveyId in surveyId.Split(','))
         {
             retrunvalue = surveyService.RestoreSurveyBySurveyId(ValidationHelper.GetInteger(uSurveyId, 0));
         }
         return retrunvalue.ToString();  
     }

     private string UpdateUserCredits()
     {
         string retval = "";
         try
         {

             retval = "success";
         }
         catch (Exception)
         {
             retval = "error";
         }
         return retval;
     }
}
