﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using System.Web.Script.Serialization;

public partial class paymentwebhook : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //CovalenseUtilities.Extensions.ObjectJsonExtensions objJson = new CovalenseUtilities.Extensions.ObjectJsonExtensions();
        string postvars = "";
        string paymentid = "";
        decimal amount = 0M;
        int orderid = 0;
        string productslug = "";
        string producttitle = "";
        string status = "";
        string customfields = "";

        PollCreation pc;
        pc = new PollCreation();
        if (Request.Form.Count > 0)
        {
            paymentid = Request.Form["payment_id"];
            productslug = Request.Form["offer_slug"];
            producttitle = Request.Form["offer_title"];
            status = Request.Form["status"];
            Decimal.TryParse(Request.Form["amount"], out amount);
            customfields = Request.Form["custom_fields"];
        }
        else if (Request.QueryString.Count > 0)
        {
            paymentid = Request.QueryString["payment_id"];
            productslug = Request.QueryString["offer_slug"];
            producttitle = Request.QueryString["offer_title"];
            status = Request.QueryString["status"];
            Decimal.TryParse(Request.QueryString["amount"], out amount);
            customfields = Request.QueryString["custom_fields"];
        }
        JavaScriptSerializer js = new JavaScriptSerializer();
        if (productslug == "insighto-surveys-premium-annual")
        {
            clscustomfields str1 = js.Deserialize<clscustomfields>(customfields);
            Int32.TryParse((str1.field_90373 != null) ? str1.field_90373.value : "0", out orderid);
        }
        else if (productslug == "insighto-surveys-pro-single")
        {
            clscustomfields str1 = js.Deserialize<clscustomfields>(customfields);
            Int32.TryParse((str1.field_37341 != null) ? str1.field_37341.value : "0", out orderid);
        }
        else if (productslug == "insighto-surveys-premium-single")
        {
            clscustomfields str1 = js.Deserialize<clscustomfields>(customfields);
            Int32.TryParse((str1.field_15160 != null) ? str1.field_15160.value : "0", out orderid);
        }
        lbl1.Text = orderid.ToString();
        pc.SavePollPaymentInformation(paymentid, amount, productslug, producttitle, orderid, status, "Survey");
    }

}
public class clscustomfields
{
    public clscustomfields() { }
    public Field_90373 field_90373 { get; set; } //PREM-Y
    public Field_15160 field_15160 { get; set; } //PPS-PREM
    public Field_37341 field_37341 { get; set; } //PPS-PRO
}

public class Field_37341 : Fields { }
public class Field_90373 : Fields { }
public class Field_15160 : Fields { }

public class Fields
{
    public bool required { get; set; }
    public string type { get; set; }
    public string label { get; set; }
    public string value { get; set; }
}
