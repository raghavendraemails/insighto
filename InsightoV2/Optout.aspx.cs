﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using Insighto.Business.Helpers;
using App_Code;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class Optout : System.Web.UI.Page
{
    Hashtable ht = new Hashtable();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lnkFavIcon.Href = "http://" + Request.Url.Authority + "/insighto.ico";
            btnCancel.Attributes.Add("onclick", "javascript:WindowClose();");
          //  int mailId = 0;
            string mailId = "";
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 && ht.Contains("ID"))
            {
               // mailId = Convert.ToInt32(ht["ID"]);
                mailId = ht["ID"].ToString();
            }
          //  var unSubscribeDetails = ServiceFactory.GetService<UnsubscribeService>().GetUnsubscribeListByEmailId(mailId).FirstOrDefault();

            DataSet dsunsublist = getUnsubscribelist(mailId);


           // if (unSubscribeDetails != null)
            if (dsunsublist.Tables[0].Rows.Count != 0)
            {
                string temp = Utilities.ResourceMessage("Optout_Success_Remove");
               // string temp = "Opt Out Request has been accepted <br />";
               // temp += "You have been permanently removed from this sender's mailing list <br /> ";
                lblText.Text = temp;
                btnOk.Visible = false;
                btnCancel.Visible = true;
                btnCancel.Text = Utilities.ResourceMessage("Optout_Cancel_CloseWindow"); //"Close Window";
            }
            else
            {
                string temp = Utilities.ResourceMessage("Optout_Success_RemovePermanent");
                
                //string temp = "If you do not wish to receive further surveys&nbsp; from this sender, click Unsubscribe below.<br />";
                //temp += "Insighto will permanently remove you from this sender's mailing list.<br /> ";
                //temp += "<br />";
                //temp += "Are you sure that you want to permanently opt out from this sender's mailing list?.<br />";
                lblText.Text = temp;
                btnOk.Visible = true;
                btnCancel.Text = Utilities.ResourceMessage("Optout_Cancel"); //"Cancel";
            }
        }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        //int mailId = 0;
        string mailId="";
        ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
        if (ht != null && ht.Count > 0 && ht.Contains("ID"))
        {
            //mailId = Convert.ToInt32(ht["ID"]);
            mailId = ht["ID"].ToString();
        }
        if (mailId != "")
        {
            //var emailDetails = ServiceFactory.GetService<EmailService>().FindDetailsByEmailId(mailId).FirstOrDefault();
            //osm_unsubscribelist uslist = new osm_unsubscribelist();
            //uslist.CONTACTID = ValidationHelper.GetInteger(emailDetails.CONTACTLIST_ID, 0);
            //uslist.CREATEDBYID = ValidationHelper.GetInteger(emailDetails.CREATED_BY, 0);
            //uslist.DELETED = 0;
            //uslist.EMAIL_ID = ValidationHelper.GetInteger(emailDetails.EMAIL_ID, 0);
            //uslist.EMAILADDRESS = emailDetails.EMAIL_ADDRESS;            
            //uslist.STATUS = "Unsubscribe";
            //uslist = ServiceFactory.GetService<UnsubscribeService>().SaveEmailUnsubscribeList(uslist);
            //if (uslist.UNSUBSCRIBE_ID > 0)
            //{
            //    var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            //    ServiceFactory.GetService<EmailService>().UpdateUnsubscribeStatus(uslist.EMAILADDRESS, ValidationHelper.GetInteger(emailDetails.CREATED_BY,0));

            DataSet dsunsublist1 = getUnsubscribelist(mailId);
            int userid = Convert.ToInt32(dsunsublist1.Tables[1].Rows[0][0].ToString());
            DataSet dsinsertlist = insertUnsubscribelist(userid, userid, userid, mailId);

                string temp = Utilities.ResourceMessage("Optout_Success_Remove");
                
                //string temp = "Opt Out Request has been accepted <br />";
                //temp += "You have been permanently removed from this sender's mailling list <br /> ";
                lblText.Text = temp;
                btnOk.Visible = false;
                btnCancel.Visible = true;
                btnCancel.Text = Utilities.ResourceMessage("Optout_Cancel_CloseWindow"); //"Close Window";
           // }
        }
    }


    public SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getUnsubscribelist(string  emailaddress)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getUnsubscribelist", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@emailid", SqlDbType.VarChar)).Value = emailaddress;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsunsubscribelist = new DataSet();
            sda.Fill(dsunsubscribelist);
            con.Close();
            return dsunsubscribelist;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet insertUnsubscribelist(int CONTACTID, int CREATEDBYID, int EMAIL_ID, string EMAILADDRESS)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_insertUnsubscribelist", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@CONTACTID", SqlDbType.Int)).Value = CONTACTID;
            scom.Parameters.Add(new SqlParameter("@CREATEDBYID", SqlDbType.Int)).Value = CREATEDBYID;
            scom.Parameters.Add(new SqlParameter("@EMAIL_ID", SqlDbType.Int)).Value = EMAIL_ID;
            scom.Parameters.Add(new SqlParameter("@EMAILADDRESS", SqlDbType.VarChar)).Value = EMAILADDRESS;
                

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsinsertunsubscribelist = new DataSet();
            sda.Fill(dsinsertunsubscribelist);
            con.Close();
            return dsinsertunsubscribelist;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

}