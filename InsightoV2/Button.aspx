﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true"
    CodeFile="Button.aspx.cs" Inherits="Button"%>

 <%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" runat="Server">

    <div class="surveyQuestionPanel">
         <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div><asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1" ></asp:Label></div>
        </div>
         <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div><asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <!-- survey question panel -->
        <div class="surveyQuestionHeader">
            <div class="surveyQuestionTitle">
            <asp:Label ID="lblChangeButton" runat="server" Text=" Change Button Style" 
                    meta:resourcekey="lblChangeButtonResource1"></asp:Label>
               </div>
            <div class="previewPanel">
                 <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
            </div>
        </div>
        <div class="surveyQuestionTabPanel">
            <!-- survey question tabs -->
            <div class="chooseButtonPanel">
                <p>
                    <asp:RadioButton ID="rbtnDefault" runat="server" GroupName="button" 
                        Text="Default" meta:resourcekey="rbtnDefaultResource1" />
                </p>
                <div>
                    <table>
                        <tr>
                            <td>                                
                                  <asp:Button ID="Button7" runat="server" CssClass="btn_sample_gray_96"  Text="Start Survey"
                                    OnClientClick="javascript:return false;"></asp:Button>&nbsp;&nbsp;
                                <asp:Button ID="Button8" runat="server" CssClass="btn_sample_gray_96"  Text="Continue"
                                     OnClientClick="javascript:return false;"></asp:Button>&nbsp;&nbsp;
                                     <asp:Button ID="Button9" runat="server" CssClass="btn_sample_gray_96"  Text="Submit"
                                     OnClientClick="javascript:return false;"></asp:Button>&nbsp;&nbsp;
                            </td>
                            <td style="width:45px;"></td>
                            <td><asp:Label ID="lblBackgroundColor" runat="server" Text="Background Color" 
                                    meta:resourcekey="lblBackgroundColorResource1"></asp:Label></td>
                            <td>
                                
                                
                                <asp:DropDownList ID="ddlDefaultBC" runat="server" CssClass="dropDown" 
                                    meta:resourcekey="ddlDefaultBCResource1">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        
                    </table>
                </div>
            </div>
            <div class="chooseButtonPanel">
                <p>
                    <asp:RadioButton ID="rbtnArrow" runat="server" GroupName="button" Text="Arrow" 
                        meta:resourcekey="rbtnArrowResource1" />
                </p>
                <div>
                    <table>                        
                        <tr>
                            
                            <td>
                                
                                <img src="App_Themes/Classic/Images/btn-theme-pink1.gif" alt="" />&nbsp;&nbsp;
                                <img src="App_Themes/Classic/Images/btn-theme-pink2.gif" alt="" />&nbsp;&nbsp;
                                <img src="App_Themes/Classic/Images/btn-theme-pink3.gif" alt="" />&nbsp;&nbsp;
                            </td>                            
                            <td>
                                <asp:Label ID="lblBackgroundColor1" runat="server" Text=" Background Color" 
                                    meta:resourcekey="lblBackgroundColor1Resource1"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlArrowBC" runat="server" CssClass="dropDown" 
                                    meta:resourcekey="ddlArrowBCResource1">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkClearLabels" runat="server" 
                                    meta:resourcekey="chkClearLabelsResource1" />
                            </td>
                            <td>
                                <asp:Label ID="lblClearLabels" runat="server" Text="Clear Labels" 
                                    meta:resourcekey="lblClearLabelsResource1"></asp:Label>
                            </td>
                        </tr>
                      
                    </table>
                </div>
            </div>
            <div class="chooseButtonPanel">
                <p>
                    <asp:RadioButton ID="rbtnCustom" runat="server" GroupName="button" 
                        Text="Custom (Add custom label on buttons)" 
                        meta:resourcekey="rbtnCustomResource1" />
                </p>
                <div>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                            
                                <asp:Button ID="btnStart" runat="server" CssClass="btn_sample_gray_96" 
                                    meta:resourcekey="btnStartResource1" OnClientClick="javascript:return false;"></asp:Button>&nbsp;&nbsp;
                                <asp:Button ID="btnContinue" runat="server" CssClass="btn_sample_gray_96" 
                                    meta:resourcekey="btnContinueResource1" OnClientClick="javascript:return false;"></asp:Button>&nbsp;&nbsp;
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn_sample_gray_96" 
                                    meta:resourcekey="btnSubmitResource1" OnClientClick="javascript:return false;"></asp:Button>&nbsp;&nbsp;
                            </td>
                            <td style="width:15px;"></td>
                            <td><asp:Label ID="lblBackground3" runat="server" Text="Background Color" 
                                    meta:resourcekey="lblBackground3Resource1"></asp:Label>&nbsp;</td>
                            <td>
                               <asp:DropDownList ID="ddlCustomBC" runat="server" CssClass="dropDown" 
                                    meta:resourcekey="ddlCustomBCResource1">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-top:5px;">
                                <asp:TextBox ID="txtStart" runat="server" CssClass="customTextBox" 
                                    style="text-align:center;" MaxLength="20" meta:resourcekey="txtStartResource1"></asp:TextBox>&nbsp;&nbsp;
                                <asp:TextBox ID="txtContinue" runat="server" CssClass="customTextBox" 
                                    style="text-align:center;" MaxLength="20" 
                                    meta:resourcekey="txtContinueResource1"></asp:TextBox>&nbsp;&nbsp;
                                <asp:TextBox ID="txtSubmit" runat="server" CssClass="customTextBox" 
                                    style="text-align:center;" MaxLength="20" meta:resourcekey="txtSubmitResource1"></asp:TextBox>&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                          <td>
                        <asp:Button ID="Button5" runat="server" CssClass="btn_sample_gray_96"  Text="Exit this survey"
                                    OnClientClick="javascript:return false;"></asp:Button>&nbsp;&nbsp;
                                <asp:Button ID="Button6" runat="server" CssClass="btn_sample_gray_96"  Text="Save for later"
                                     OnClientClick="javascript:return false;"></asp:Button>&nbsp;&nbsp;
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>
                         <tr>
                            <td colspan="2" style="padding-top:5px;">
                                <asp:TextBox ID="txtExitbtn" runat="server" CssClass="customTextBox" 
                                    style="text-align:center;" MaxLength="20" Text="Exit this survey"></asp:TextBox>&nbsp;&nbsp;                                
                                <asp:TextBox ID="txtsavelaterbtn" runat="server" CssClass="customTextBox" 
                                    style="text-align:center;" MaxLength="20" Text="Save for later"></asp:TextBox>&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- //survey question tabs -->
            <div class="surveyQuestionHeader">
            <div class="surveyQuestionTitle">
            <asp:Label ID="lblBrowserBackbutton" runat="server" Text="Browser back / forward option" 
                    meta:resourcekey="lblBrowserBackbutton"></asp:Label>
               </div>
           
        </div>
        <div class="defaultHeight">
        </div>
         <div  class="informationPanelDefault">
         <div>
         <asp:Label ID="lblBrowserBackbuttonText"  meta:resourcekey="lblBrowserBackbuttonText" Text="Prevents respondents from going backwards in a survey to change responses on previous pages."  runat="server"></asp:Label>   
         </div>
          </div>

         <div class="chooseButtonPanel">
         <asp:CheckBox ID="chkBrowserBackButton" Checked="true"  meta:resourcekey="chkBrowserBackButton"  Text="Disable the browser Back button"  runat="server" />  
         </div>
        

        </div>
        <div class="clear">
        </div>
        <div class="bottomButtonPanel">
            <asp:Button ID="btnSave" runat="server" Text="Save" ToolTip="Save" 
                CssClass="dynamicButton" onclick="btnSave_Click" 
                meta:resourcekey="btnSaveResource1" />
        </div>
        <!-- //survey question panel -->
    </div>
    <script language="javascript" type="text/javascript">
        function changeVal() {
            document.getElementById('<%= btnStart.ClientID %>').value = document.getElementById('<%= txtStart.ClientID %>').value;
            document.getElementById('<%= btnContinue.ClientID %>').value = document.getElementById('<%= txtContinue.ClientID %>').value;
            document.getElementById('<%= btnSubmit.ClientID %>').value = document.getElementById('<%= txtSubmit.ClientID %>').value;
        } 
    </script>
</asp:Content>
