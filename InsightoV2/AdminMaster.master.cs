﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.Helpers;

public partial class AdminMaster :  MasterPageBase
{
    protected void Page_Init(object sender, EventArgs e)
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginAdminUserDetailsSession();
        if (userDetails == null)
            Response.Redirect(PathHelper.GetAdminLoginPageUrl());
        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
          //  lnkFavIcon..Href = "http://" + Request.Url.Authority + "/insighto.ico";
        }
    }
}
