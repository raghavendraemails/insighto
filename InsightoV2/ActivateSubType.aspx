﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="ActivateSubType.aspx.cs" Inherits="ActivateSubType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div class="popupheadsurvey" id="SurveyActivation" runat="server">
<div>
<div class="questionpopup" >You are currently not signed into Insighto Surveys.</div>
<div class="bgactivatesurveys"><asp:Button ID="btnActivate" runat="server" 
        CssClass="btnactivatesurveys" Text="Activate Free Subscription" 
        onclick="btnActivate_Click" /></div>
        </div>
    </div>
    
<div class="popupheadsurvey" id="PollActivation" runat="server">
        <div>
<div class="questionpopup">You are currently not signed into Insighto Polls.</div>
<div class="bgactivatesurveys"><asp:Button ID="btnActivatePoll" runat="server" 
        CssClass="btnactivatesurveys" Text="Activate Free Subscription" 
        onclick="btnActivatePoll_Click"/></div>
        </div>
    </div>
</asp:Content>

