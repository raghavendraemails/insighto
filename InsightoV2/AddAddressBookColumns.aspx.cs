﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using System.Web.UI.HtmlControls;
using System.Collections;
using Insighto.Business.Helpers;
using System.Configuration;
using App_Code;
using Insighto.Business.ValueObjects;

public partial class AddAddressBookColumns : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            // insert list if not exist

            var contactService = ServiceFactory.GetService<ContactListService>();
            int ContactId = 0;
            if (Request.QueryString["key"] != null)
            {
                Hashtable ht = new Hashtable();

                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);


                if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                {
                    ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                }

            }

            if (ContactId > 0)
            {
                var contactList = contactService.FindByContactListID(ContactId);
                if (contactList != null)
                    bindColumns(contactList);

            }
           

        }       
    }
    protected void ibtnSave_Click(object sender, EventArgs e)
    {
        //if (txtColumn1.Text == "" &&
        //    txtColumn2.Text == "" &&
        //    txtColumn3.Text == "" &&
        //    txtColumn4.Text == ""
        //    && txtColumn5.Text == "" && txtColumn6.Text == "" && txtColumn7.Text == ""
        //    && txtColumn8.Text == ""
        //    )
        //{
        //    dvSuccessMsg.Visible = false;
        //    dvErrMsg.Visible = true;
        //    lblErrMsg.Visible = true;
        //}
        //else
        //{
            SaveContactDetails();
        //}
    }

    protected void bindColumns(osm_contactlist columnList)
    {
       
        if (columnList != null)
        {
            List<string> lstStr = new List<string>();

            if (columnList.CUSTOM_HEADER1 != null && columnList.CUSTOM_HEADER1 != "")
            {
                // txtColumn1.Text = columnList.CUSTOM_HEADER1.Trim();
                if (columnList.CUSTOM_HEADER1.Trim() != "")
                {
                    lstStr.Add(columnList.CUSTOM_HEADER1.Trim());
                }
            }
            if (columnList.CUSTOM_HEADER2 != null && columnList.CUSTOM_HEADER2 != "")
            {
                //txtColumn2.Text = columnList.CUSTOM_HEADER2.Trim();
                if (columnList.CUSTOM_HEADER2.Trim() != "")
                {
                    lstStr.Add(columnList.CUSTOM_HEADER2.Trim());
                }
            }
            //if (columnList.CUSTOM_HEADER3 != null && columnList.CUSTOM_HEADER3 != "")
            //{
            //    // txtColumn3.Text = columnList.CUSTOM_HEADER3.Trim();
            //    if (columnList.CUSTOM_HEADER3.Trim() != "")
            //    {
            //        lstStr.Add(columnList.CUSTOM_HEADER3.Trim());
            //    }
            //}
            //if (columnList.CUSTOM_HEADER4 != null && columnList.CUSTOM_HEADER4 != "")
            //{
            //    // txtColumn4.Text = columnList.CUSTOM_HEADER4.Trim();
            //    if (columnList.CUSTOM_HEADER4.Trim() != "")
            //    {
            //        lstStr.Add(columnList.CUSTOM_HEADER4.Trim());
            //    }
            //}
            //if (columnList.CUSTOM_HEADER5 != null && columnList.CUSTOM_HEADER5 != "")
            //{
            //    // txtColumn5.Text = columnList.CUSTOM_HEADER5.Trim();
            //    if (columnList.CUSTOM_HEADER5.Trim() != "")
            //    {
            //        lstStr.Add(columnList.CUSTOM_HEADER5.Trim());
            //    }
            //}
            //if (columnList.CUSTOM_HEADER6 != null && columnList.CUSTOM_HEADER6 != "")
            //{
            //    // txtColumn6.Text = columnList.CUSTOM_HEADER6.Trim();
            //    if (columnList.CUSTOM_HEADER6.Trim() != "")
            //    {
            //        lstStr.Add(columnList.CUSTOM_HEADER6.Trim());
            //    }
            //}
            //if (columnList.CUSTOM_HEADER7 != null && columnList.CUSTOM_HEADER7 != "")
            //{
            //    //txtColumn7.Text = columnList.CUSTOM_HEADER7.Trim();
            //    if (columnList.CUSTOM_HEADER7.Trim() != "")
            //    {
            //        lstStr.Add(columnList.CUSTOM_HEADER7.Trim());
            //    }
            //}
            //if (columnList.CUSTOM_HEADER8 != null && columnList.CUSTOM_HEADER8 != "")
            //{
            //    //txtColumn8.Text = columnList.CUSTOM_HEADER8.Trim();
            //    if (columnList.CUSTOM_HEADER8.Trim() != "")
            //    {
            //        lstStr.Add(columnList.CUSTOM_HEADER8.Trim());
            //    }
            //}
            if (lstStr.Count > 0)
            {
                ucAddressbookColumns.Value = lstStr;               
            }            
            else
            {
                lstStr.Add(" ");
                ucAddressbookColumns.Value = lstStr;               
            }
            hdnContactListId.Value = ValidationHelper.GetInteger(columnList.CONTACTLIST_ID, 0).ToString();
        }
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        if (loggedInUserInfo.LicenseType == "FREE")
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>jAlert('This feature is available only to paid customers. <br> Upgrade your account to avail this.', 'Upgrade', function (r) {closeModalSelf(false, '');});</script>", false);
        }
       
    }   

    protected void SaveContactDetails()
    {
        

        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        var emailService = ServiceFactory.GetService<EmailService>();
        var contact = new osm_contactlist();
        int ContactId = 0;
        if (Request.QueryString["key"] != null)
        {
            Hashtable ht = new Hashtable();

            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);


            if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
            {
                ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
            }

        }

        List<string> list = new List<string>();
        list = ucAddressbookColumns.Value;
        if (list.Count > 0)
            contact.CUSTOM_HEADER1 = list[0].ToString().Trim();
        if (list.Count > 1)
        {
            if (list[0].ToString().Trim().ToUpper()  == list[1].ToString().Trim().ToUpper() )
            {
                lblSuccessMsg.Visible = false;
                dvSuccessMsg.Visible = false;
                dvErrMsg.Visible = true;
                lblErrMsg.Visible = true;
                lblErrMsg.Text = Utilities.ResourceMessage("lblColulmnsErrMsg");
                return;
            }

            contact.CUSTOM_HEADER2 = list[1].ToString().Trim();
         }
             contact.CONTACTLIST_ID = ContactId;
             int returnValue = emailService.SaveAddressBookColumnsByContactListId(contact);
             base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("CreateEmailList.aspx", "ContactId=" + contact.CONTACTLIST_ID + "&surveyFlag=" + SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
             lblSuccessMsg.Visible = true;
             dvSuccessMsg.Visible = true;
             dvErrMsg.Visible = false;
             
           
           
        //if (returnValue > 0)
        //{
        //    //HtmlMeta equiv = new HtmlMeta();
        //    //equiv.HttpEquiv = "refresh";
        //    //equiv.Content = "5; url= CreateEmailList.aspx";
        //    //Header.Controls.Add(equiv);
        //    //string retUrl = EncryptHelper.EncryptQuerystring("CreateEmailList.aspx", "ContactId=" + ContactId);
            
        //}       

       // emaillist.CONTACTLIST_ID = Convert.ToInt32(hdnContactListId.Value);

    }
}