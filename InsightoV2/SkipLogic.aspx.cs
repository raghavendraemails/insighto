﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using App_Code;

public partial class SkipLogic : SecurePageBase
{
    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int surveyId = 0, questionId = 0, edit = 0, questionSeq;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains("SurveyId"))
                surveyId = int.Parse(typeHt["SurveyId"].ToString());

            if (typeHt.Contains("QuestionId"))
                questionId = int.Parse(typeHt["QuestionId"].ToString());

            if (typeHt.Contains("QuestionSeq"))
                questionSeq = int.Parse(typeHt["QuestionSeq"].ToString());

            if (typeHt.Contains("Edit"))
                edit = int.Parse(typeHt["Edit"].ToString());
        }

        if (edit == 1)
            btnRmoveSkipLogic.Visible = true;
        else
            btnRmoveSkipLogic.Visible = false;

        if (!IsPostBack)
        {
            if (questionId > 0)
            {
                GetDetailForSkipLogic(surveyId, questionId, questionSeq);
            }
        }



    }


    public void GetDetailForSkipLogic(int surveyId, int questionId, int questionSeq)
    {

        var questionService = ServiceFactory.GetService<QuestionService>();
        var surveyQuestionsList = questionService.GetAllSurveyQuestionsBySurveyId(surveyId);
        int seqLastQID = questionService.GetQuestionIdBySequence(surveyId);
        //surveyQuestionsList.OrderBy
        var surveyAnswersList = new List<osm_answeroptions>();

        if (surveyQuestionsList.Any() && questionId > 0)
        {
            int? quesSeq = 0;
            int quesNo = 0;
            List<ListItem> itms = new List<ListItem>();
            foreach (var ques in surveyQuestionsList)
            {
                var questionTypeId = GenericHelper.ToEnum<QuestionType>(ValidationHelper.GetInteger(ques.QUESTION_TYPE_ID, 0));
                if (questionId == ques.QUESTION_ID)
                {
                    if (questionTypeId == QuestionType.MultipleChoice_SingleSelect || questionTypeId == QuestionType.Menu_DropDown || questionTypeId == QuestionType.Choice_YesNo)
                    {
                        surveyAnswersList = questionService.GetAllSurveyAnswersByQuestionId(questionId);
                        string strQuestion = ques.QUSETION_LABEL;
                        strQuestion = strQuestion.Replace("<p>", "");
                        strQuestion = strQuestion.Replace("</p>", "");
                        lblQuestion.Text = strQuestion; //ques.QUESTION_SEQ.ToString() + ". " + strQuestion; as per client requested removing the sequence no.
                        quesSeq = ques.QUESTION_SEQ;
                    }
                }

                if (questionTypeId != QuestionType.Narration && questionTypeId != QuestionType.PageHeader && questionTypeId != QuestionType.QuestionHeader && questionTypeId != QuestionType.Image && questionTypeId != QuestionType.Video)
                    quesNo++;
                if (ques.QUESTION_SEQ > quesSeq && quesSeq > 0)
                {
                    if (questionTypeId != QuestionType.Narration && questionTypeId != QuestionType.PageHeader && questionTypeId != QuestionType.QuestionHeader && questionTypeId != QuestionType.Image && questionTypeId != QuestionType.Video)
                    {
                        ListItem listitm = new ListItem(quesNo.ToString(), ques.QUESTION_ID.ToString());
                        itms.Add(listitm);
                    }
                    else if (questionTypeId == QuestionType.Narration)
                    {
                        ListItem listitm = new ListItem("N", ques.QUESTION_ID.ToString());
                        itms.Add(listitm);
                    }
                    else if (questionTypeId == QuestionType.PageHeader)
                    {
                        ListItem listitm = new ListItem("P", ques.QUESTION_ID.ToString());
                        itms.Add(listitm);
                    }
                    else if (questionTypeId == QuestionType.QuestionHeader)
                    {
                        ListItem listitm = new ListItem("Q", ques.QUESTION_ID.ToString());
                        itms.Add(listitm);
                    }
                    else if (questionTypeId == QuestionType.Image)
                    {
                        ListItem listitm = new ListItem("I", ques.QUESTION_ID.ToString());
                        itms.Add(listitm);
                    }
                    else if (questionTypeId == QuestionType.Video)
                    {
                        ListItem listitm = new ListItem("V", ques.QUESTION_ID.ToString());
                        itms.Add(listitm);
                    }

                }

            }

            if (surveyAnswersList.Any() && itms.Count > 0)
            {

                ListItem listitm = new ListItem("End", "-1");
                itms.Add(listitm);
                ListItem listit = new ListItem("NoSkip", "-2");
                itms.Add(listit);
                ListItem listits = new ListItem("Screen Out", "-3");
                itms.Add(listits);

                Cache["itms"] = itms.ToList();

                lstSkipOptions.DataSource = surveyAnswersList;
                lstSkipOptions.DataBind();

            }
        }


        if (surveyQuestionsList.Count == 1)
        {
            lblErrMsg.Text = Utilities.ResourceMessage("SkipLogic_Error_MorethanOne"); //"Survey should have more than one question to apply skip logic.";
            dvErrMsg.Visible = true;
            dvSkipLogic.Visible = false;
        }
        else
        {
            if (seqLastQID == questionId)
            {
                lblErrMsg.Text = Utilities.ResourceMessage("SkipLogic_Error_LastQuestion"); //"Skip logic cannot be applied to the last question of the survey.";
                dvErrMsg.Visible = true;
                dvSkipLogic.Visible = false;
            }
        }
    }


    protected void lstSkipOptions_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;

            Label lblAns = e.Item.FindControl("lblAns") as Label;
            lblAns.Text = ((Insighto.Data.osm_answeroptions)(dataItem.DataItem)).ANSWER_OPTIONS.ToString();

            Label lblAnswerId = e.Item.FindControl("lblAnswerId") as Label;
            lblAnswerId.Text = ((Insighto.Data.osm_answeroptions)(dataItem.DataItem)).ANSWER_ID.ToString();

            Label lblQuestionId = e.Item.FindControl("lblQuestionId") as Label;
            lblQuestionId.Text = ((Insighto.Data.osm_answeroptions)(dataItem.DataItem)).QUESTION_ID.ToString();

            Label lblSkipQuestionId = e.Item.FindControl("lblSkipQuestionId") as Label;
            lblSkipQuestionId.Text = ((Insighto.Data.osm_answeroptions)(dataItem.DataItem)).SKIP_QUESTION_ID.ToString();

            DropDownList cbSkip = e.Item.FindControl("cbSkip") as DropDownList;

            if (Cache["itms"] != null)
            {
                List<ListItem> itms = (List<ListItem>)Cache["itms"];

                cbSkip.DataSource = itms;
                cbSkip.DataTextField = "Text";
                cbSkip.DataValueField = "Value";
                cbSkip.DataBind();
            }

            DropDownList cbSkip1 = e.Item.FindControl("cbSkip") as DropDownList;
            if (cbSkip1 != null)
                cbSkip1.SelectedValue = lblSkipQuestionId.Text;

        }
    }

    protected void btnSaveSkipLogic_Click(object sender, EventArgs e)
    {
        var questionService = ServiceFactory.GetService<QuestionService>();

        //apply logical skip logic..
        foreach (ListViewItem item in lstSkipOptions.Items)
        {
            Label lblAnswerId = item.FindControl("lblAnswerId") as Label;
            Label lblQuestionId = item.FindControl("lblQuestionId") as Label;
            DropDownList cbSkip = item.FindControl("cbSkip") as DropDownList;

            questionService.UpdateSkipLogicAnswerOptions(int.Parse(lblQuestionId.Text), 0, 0, 1, 1);
            questionService.UpdateSkipLogicAnswerOptions(int.Parse(lblQuestionId.Text), int.Parse(lblAnswerId.Text), Convert.ToInt32(cbSkip.SelectedItem.Value), 0, 1);

        }

        //Clean up the skip logic before updating and  apply logical skip logic..
        var questions = questionService.GetAllSurveyQuestionsBySurveyId(surveyId);
        var skipEnabledQuestionIds = questions.Where(q => q.SKIP_LOGIC > 0 && q.QUESTION_ID == questionId).Select(qid => qid.QUESTION_ID).ToList();

        if (skipEnabledQuestionIds.Count <= 0)
            CleanSkipLogic(questions);

        foreach (ListViewItem item in lstSkipOptions.Items)
        {
            //updates the page break for logical page break
            DropDownList cbSkip = item.FindControl("cbSkip") as DropDownList;

            var selectedQuestionId = Convert.ToInt32(cbSkip.SelectedItem.Value);
            if (selectedQuestionId > 0)
            {
                var currentQuestion = questions.Where(q => q.QUESTION_ID == selectedQuestionId).FirstOrDefault();
                var logicalSkipQuestionSequenceId = currentQuestion.QUESTION_SEQ > 1 ? (currentQuestion.QUESTION_SEQ - 1) : currentQuestion.QUESTION_SEQ; //applying (n-1) for Nth question;
                var currentlogicalSkipQuestion = questions.Where(q => q.QUESTION_SEQ == logicalSkipQuestionSequenceId).FirstOrDefault();

                int pageBreak = currentlogicalSkipQuestion.PAGE_BREAK <= 1 ? currentlogicalSkipQuestion.PAGE_BREAK += 2 : currentlogicalSkipQuestion.PAGE_BREAK;
                questionService.UpdateQuestionByQuestionId(pageBreak, currentlogicalSkipQuestion.QUESTION_ID);
            }
        }


        base.CloseModal();
    }

    /// <summary>
    /// Cleans the skip logic.
    /// </summary>
    /// <param name="questions">The questions.</param>
    private void CleanSkipLogic(List<osm_surveyquestion> questions)
    {
        foreach (var question in questions)
        {
            int pageBreak = question.PAGE_BREAK == 3 ? 1 : question.PAGE_BREAK == 2 ? 0 : question.PAGE_BREAK;
            if (question.PAGE_BREAK == 2 || question.PAGE_BREAK == 3)
                ServiceFactory<QuestionService>.Instance.UpdateQuestionByQuestionId(pageBreak, question.QUESTION_ID);
        }
    }

    protected void btnRmoveSkipLogic_Click(object sender, EventArgs e)
    {
        var questionService = ServiceFactory.GetService<QuestionService>();
        var questions = questionService.GetAllSurveyQuestionsBySurveyId(surveyId);
        foreach (ListViewItem item in lstSkipOptions.Items)
        {

            Label lblAnswerId = item.FindControl("lblAnswerId") as Label;
            Label lblQuestionId = item.FindControl("lblQuestionId") as Label;
            DropDownList cbSkip = item.FindControl("cbSkip") as DropDownList;

            questionService.UpdateSkipLogicAnswerOptions(int.Parse(lblQuestionId.Text), 0, 0, 1, 0);
            questionService.UpdateSkipLogicAnswerOptions(int.Parse(lblQuestionId.Text), int.Parse(lblAnswerId.Text), 0, 0, 0);


            //updates the page break for logical page break
            var selectedQuestionId = Convert.ToInt32(cbSkip.SelectedItem.Value);
            if (selectedQuestionId > 0)
            {
                var currentQuestion = questions.Where(q => q.QUESTION_ID == selectedQuestionId).FirstOrDefault();
                var logicalSkipQuestionSequenceId = currentQuestion.QUESTION_SEQ > 1 ? (currentQuestion.QUESTION_SEQ - 1) : currentQuestion.QUESTION_SEQ; //applying (n-1) for Nth question;
                var currentlogicalSkipQuestion = questions.Where(q => q.QUESTION_SEQ == logicalSkipQuestionSequenceId).FirstOrDefault();

                int pageBreak = currentlogicalSkipQuestion.PAGE_BREAK -= 2;
                questionService.UpdateQuestionByQuestionId(pageBreak, currentlogicalSkipQuestion.QUESTION_ID);
            }
        }

        base.CloseModal();
    }
}