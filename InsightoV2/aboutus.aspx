﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="aboutus.aspx.cs" Inherits="new_AboutUs" MasterPageFile="~/Home.Master" %>
<%@ Register TagPrefix="uc" TagName="OtherHeader" Src="~/UserControls/OtherHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc:OtherHeader ID="OtherHeader" runat="server" />
<div> 

  
  <!--=== Content Part ===-->
  <div class="breadcrumbs-v3 img-v1 text-center">
    <div class="container">
      <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;"> <span>About Us </span></h2>
      <!--<h1 style="color:#fff900 !important; text-transform:none !important;">Survey Services</h1>
      <h5 style="color:#FFF !important;">Don’t let your lack of time or skill to create surveys abstain you from useful insights. </h5>
      <h5 style="color:#FFF !important;">Let Insighto Experts build powerful surveys for you.</h5>--> 
    </div>
  </div>
  <p>&nbsp;</p>
  <!--=== Content Part ===-->
  <div class="container">
    <div class="row margin-bottom-20 margin-top-20">
      <div class="col-sm-5"> <img src="assets/img/mockup/img12.jpg" alt="" class="img-responsive"> </div>
      <div class="col-sm-7">
        <p>Insighto suite of products enables Organisations and Professionals therein gain invaluable insights and strengthen the  engagement with the key stakeholders. Thus accelerating their Growth & Success.</p>
        <p>Insighto is proudly presented by Knowience Insights - a company set up by two experienced management professionals who had worked in  senior positions in global organisations.</p>
        <p>Recognising the the need for simple yet powerful technology tools that can aid in making informed decisions and engagement-based organisational growth, they embarked upon to strengthen that Knowledge in organisations by bringing some Science to the Art of Management and Decision making.. </p>
        <p>Leveraging the power of Insighto, Knowience consults with organisations - big and small - startups to well grounded companies in four critical areas.</p>
        <p><em>More info on "Knowience Consulting" at <a href="http://www.knowience.com/" target="_blank">www.knowience.com</a></em></p>
        <br>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="container text-center">
        <div class="col-md-12">
          <h2 class="title-v2 title-center" style="margin-top:-20px;">OUR TEAM</h2>
          <p class="space-lg-hor" style="font-size: 15px;">Insighto is being built by a passionate team led by its founders</p>
        </div>
      </div>
    </div>
    <br>
    <div class="row team">
      <div class="col-sm-1"></div>
      <div class="col-sm-5">
        <div class="thumbnail-style"> <!--<img class="img-responsive hoverZoomLink" src="assets/img/team/img20-md.jpg" alt="">-->
          <h3><a>Dhruvanth B. Shenoy</a> <small> Co-Founder & CEO</small> <small><span style="color:#6479DD; text-transform:lowercase;">dhruv@knowience.com</span></small></h3>
          <p>A seasoned Marketing professional with more than 25 years in the Industry, Dhruv was Country Manager - Middle East for Monster.com before
            starting  Knowience along with Ram. Having built the marketing function ground up for Monster's Indian and Asia Pac operations, he brings in rich experience in B2B and Consumer Insights and Decision making, Branding and Digital businesses. Prior to Monster, Dhruv was with Ogilvy & Mather Advertising - managing their key and high profile accounts.</p>
          <p>Apart from building Insighto, Dhruv also assists startups and established companies in their marketing growth planning, brand strengthening and organising for their  digital footprints.</p>
          <ul class="list-unstyled list-inline team-socail">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            
          </ul>
        </div>
      </div>
      <div class="col-sm-5">
        <div class="thumbnail-style"> <!--<img class="img-responsive hoverZoomLink" src="assets/img/team/img35-md.jpg" alt="">-->
          <h3><a>Dasarath Ram CSS</a> <small>Co-Founder & COO</small> <small><span style="color:#6479DD; text-transform:lowercase;">ram@knowience.com</span></small></h3>
          <p>With rich experience in Planning and Operations management in global companies, Ram brings with him the process, systems and people management capabilities. A former Country Manager India of a USA based Software & Services company - DST Worldwide, as an 'intrapreneur' he had played a key role in building three organisations earlier - right from their foundation to making each of them become a thousand plus people organisation. Ram’s key skills include Long term planning, Operations, Customer Service and Global Services.</p>
          <p> Passionate about entrepreneurship, Ram helps  startups and wannabie entrepreneurs sharpen their arsenal and strengthen their execution. </p>
          <ul class="list-unstyled list-inline team-socail">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
  </div>
  <!--/container--> 

</div>
<!--/wrapper--> 
</asp:Content>
