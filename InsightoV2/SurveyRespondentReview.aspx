﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SurveyRespondent.master" AutoEventWireup="true" CodeFile="SurveyRespondentReview.aspx.cs" Inherits="SurveyRespondentReview" %>

<%@ Register Src="~/UserControls/RespondentControls/RespondentPopupHeader.ascx" TagName="RespondentPopupHeader"
    TagPrefix="uc" %>
<%@ Register Src="~/UserControls/RespondentControls/SurveyRespondentControl.ascx"
    TagName="SurveyRespondentControl" TagPrefix="uc" %>
<asp:Content ContentPlaceHolderID="ContentHead" runat="server" ID="headerPlaceHolder">
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentMain" runat="server" ID="Content1">
   <div class="questionViewpanel">
        <div class="informationPanelDefault">
            <div>
                Preview of the survey that would be seen by respondent. Responses to the preview
                will not be recorded</div>
        </div>
        <div class="clear"></div>
        <div class="questionIncludePanel">
        <uc:SurveyRespondentControl ID="ucSurveyRespondentCo0ntrol" runat="server" />
        </div>
    </div>
   <script type="text/javascript">

       var _gaq = _gaq || [];
       _gaq.push(['_setAccount', 'UA-29026379-1']);
       _gaq.push(['_setDomainName', 'insighto.com']);
       _gaq.push(['_setAllowLinker', true]);
       _gaq.push(['_trackPageview']);

       (function () {
           var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
           ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
           var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
       })();

</script>

    </asp:Content>
