﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="InsightoSurveyLink.aspx.cs" Inherits="InsightoSurveyLink"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="js/jquery_002.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery_003.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#copybutton").zclip({
                path: "js/ZeroClipboard.swf",
                copy: function () {
                    return $(this).prev().val();
                }
            });



        });
    </script>
    <div class="popupHeading">
        <div class="popupTitle">
            <h3>
              <asp:Label ID="lblInsightoSurveyLink" runat="server" Text="Insighto Survey Link" 
                    meta:resourcekey="lblInsightoSurveyLinkResource1"></asp:Label></h3>
        </div>
       <%-- <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick="closeModalSelf(false,'');" title="Close">
                &nbsp;</a>
        </div>--%>
    </div>
    <div class="popupContentPanel">
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1" /></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div><asp:Label ID="lblErrMsg" runat="server" CssClass="errorMesg" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <div class="formPanel">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td width="100%">
                        <asp:TextBox ID="txtSurveyUrl" runat="server" CssClass="textBoxSurveyLink" 
                            ReadOnly="True" meta:resourcekey="txtSurveyUrlResource1" />
                        <a class="" id="copybutton"  href="#"><asp:Label ID="lblCopy" Text="Copy" 
                            runat="server" meta:resourcekey="lblCopyResource1"></asp:Label>  </a>

                        <asp:TextBox onfocus="this.value='';" runat="server" ID="txtName" 
                            Visible="False" meta:resourcekey="txtNameResource1" />
                        <div class="snippet-container" style="">
                        </div>
                    </td>
                   
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
