﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="WidgetBasics.aspx.cs" Inherits="WidgetBasics"%>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
      
        <div class="popupTitle">
            <h3>  <asp:Label ID="Label2" runat="server" 
                    meta:resourcekey="Label2Resource1"></asp:Label>
               </h3>
        </div>
        <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(false,'');">
                &nbsp;</a>
        </div>
        <!-- //popup heading -->
    </div>
    <div class="defaultHeight">
    </div>
    <div class="popupContentPanel">
        <!-- popup content panel -->
        <div class="formPanel">
            <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblErrMsg" runat="server"  CssClass="errorMesg" Visible="False" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
            </div>
            <!-- form -->
            <div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblWidgetName" CssClass="requirelbl" runat="server" 
                             meta:resourcekey="lblWidgetNameResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtWidgetName" runat="server" CssClass="textBoxMedium" 
                            MaxLength="25" meta:resourcekey="txtWidgetNameResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqWidgetName" ControlToValidate="txtWidgetName"
                             CssClass="lblRequired" runat="server"
                            Display="Dynamic" meta:resourcekey="reqWidgetNameResource1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblDomainName" CssClass="requirelbl" runat="server" 
                            meta:resourcekey="lblDomainNameResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtDomainName" runat="server" CssClass="textBoxMedium" 
                            MaxLength="100" meta:resourcekey="txtDomainNameResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqDomainName" ControlToValidate="txtDomainName"
                             CssClass="lblRequired" runat="server"
                            Display="Dynamic" meta:resourcekey="reqDomainNameResource1"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator SetFocusOnError="True"  ID="regUrl" 
                            ControlToValidate="txtDomainName" CssClass="lblRequired"
                            ValidationExpression="^http\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$" 
                            Display="Dynamic" runat="server" meta:resourcekey="regUrlResource1"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                    </div>
                    <div class="loginControls">
                        (Ex:http://domain.com, http://domain.co.xx)
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblReportType" CssClass="requirelbl" runat="server" 
                            meta:resourcekey="lblReportTypeResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:DropDownList ID="ddlWidgetCategory" runat="server" 
                            CssClass="dropdownMedium" meta:resourcekey="ddlWidgetCategoryResource1">
                            <asp:ListItem meta:resourcekey="ListItemResource1">Category</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqReportCategory" ControlToValidate="ddlWidgetCategory"
                            CssClass="lblRequired" runat="server"
                            Display="Dynamic" meta:resourcekey="reqReportCategoryResource1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblSurveyCategory" CssClass="requirelbl" runat="server" 
                            meta:resourcekey="lblSurveyCategoryResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="dropdownMedium" 
                            meta:resourcekey="ddlCategoryResource1">
                            <asp:ListItem meta:resourcekey="ListItemResource2">Category</asp:ListItem>
                        </asp:DropDownList>
                        <%--  
                       <asp:Label ID="lblReqSurveyCategory" CssClass="requirelbl" runat="server"  AssociatedControlID="reqSurveyCategory" />
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqSurveyCategory" ControlToValidate="ddlCategory"  ErrorMessage ="Please select survey category."
							CssClass="lblRequired" runat="server" Display="Dynamic" ></asp:RequiredFieldValidator>
                        --%>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblFolder" CssClass="requirelbl" runat="server" 
                             meta:resourcekey="lblFolderResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:DropDownList ID="ddlFolder" runat="server" CssClass="dropdownMedium" 
                            meta:resourcekey="ddlFolderResource1" />
                    </div>
                </div>
                <div class="con_login_pnl" id="addNewFolderLink">
                    <div class="loginlbl">
                        <label for="addFolder">
                        </label>
                    </div>
                    <div class="loginControls">
                        <a href="#" id="addFolder" onclick="showContents('addNewFolderLink')">+ Add Folder</a>
                    </div>
                </div>
                <div id="addNewFolderUL" style="display: none;">
                    <div class="con_login_pnl">
                        <div class="loginlbl">
                            <asp:Label ID="Label1" CssClass="requirelbl" runat="server" 
                                meta:resourcekey="Label1Resource1" />
                        </div>
                        <div class="loginControls">
                            <asp:TextBox ID="txtNewFolder" runat="server" CssClass="textBoxMedium" 
                                MaxLength="25" meta:resourcekey="txtNewFolderResource1" />
                        </div>
                    </div>
                    <div class="con_login_pnl">
                        <div class="reqlbl">
                            <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqNewFolder" 
                                ControlToValidate="txtNewFolder" ValidationGroup="vgSave"
                                 CssClass="lblRequired" meta:resourcekey="ReqNewFolderResource1"
                                runat="server" Display="Dynamic" />
                        </div>
                    </div>
                    <div class="con_login_pnl">
                        <div class="loginlbl">
                            <label for="btnNewFolder">
                            </label>
                        </div>
                        <div class="loginControls">
                            <asp:Button ID="btnSave" runat="server" 
                                CssClass="btn_small" OnClick="btnSaveFolder_Click" 
                                ValidationGroup="vgSave" meta:resourcekey="btnSaveResource1" />
                        </div>
                    </div>
                    <div class="con_login_pnl">
                        <div class="reqlbl btmpadding">
                            <asp:Label ID="lblSucess" runat="server" CssClass="errorMesg" 
                                meta:resourcekey="lblSucessResource1"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <b>
                            <asp:Label ID="lblRespondents" runat="server" 
                            meta:resourcekey="lblRespondentsResource1" /></b>
                    </div>
                    <div class="loginControls">
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblMin" runat="server" Text="Minimum" 
                            meta:resourcekey="lblMinResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtMinRespondents" MaxLength="10" runat="server" CssClass="textBoxMedium"
                            onkeypress="javascript:return onlyNumbers(event);" 
                            meta:resourcekey="txtMinRespondentsResource1"></asp:TextBox>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblMax" runat="server" Text="Maximum" 
                            meta:resourcekey="lblMaxResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtMaxRespondents" MaxLength="10" runat="server" CssClass="textBoxMedium"
                            onkeypress="javascript:return onlyNumbers(event);" 
                            meta:resourcekey="txtMaxRespondentsResource1"></asp:TextBox>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl">
                        <asp:CompareValidator ID="cmpvRespondent" ControlToValidate="txtMinRespondents" ControlToCompare="txtMaxRespondents"
                            Type="Integer" Operator="LessThanEqual" CssClass="lblRequired" runat="server"
                            ErrorMessage="Min respondent should be less than or equal to max respondent."
                            Display="Dynamic" meta:resourcekey="cmpvRespondentResource1"></asp:CompareValidator>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <label for="widgetAnimIcond">
                            <img src="App_Themes/Classic/Images/widgeticon15x15.gif" id="widgetAnimIcond" alt="" />
                            Widget Button</label>
                    </div>
                    <div class="loginControls">
                        <table cellpadding="0" cellspacing="0">
                            <tr><td class="defaultHeight"></td></tr>
                            <tr>
                                <td>
                                   <asp:RadioButton ID="rbtnOnlyIcon" Checked="True" runat="server" GroupName="setting"
                                        CssClass="rbtnOnlyIcon" 
                                        onclick="document.getElementById('widgetIcons').style.display = 'none'" 
                                        meta:resourcekey="rbtnOnlyIconResource1" />
                                </td>
                                <td>
                                    Only icon<%--<input type="radio" id="radioOnlyOne" name="1" checked="checked" onclick="document.getElementById('widgetIcons').style.display = 'none'" />--%>
                                </td>
                                <td rowspan="4" class="widgeticonspad">
                                    <input type="image" alt="" src="App_Themes/Classic/Images/widgeticon30x30.gif" id="imgIconOnly" />
                                    <img src="App_Themes/Classic/Images/widgeticon20x20.gif" alt="" id="imgSmallIcon"
                                        style="display: none" />
                                    <img src="App_Themes/Classic/Images/widgeticon40x40.gif" alt="" id="imgLargeIcon"
                                        style="display: none" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnSmallIconTitle" runat="server" CssClass="rbtnSmallIconTitle"
                                        GroupName="setting" 
                                        onclick="document.getElementById('widgetIcons').style.display = 'block'" 
                                        meta:resourcekey="rbtnSmallIconTitleResource1" />
                                    <%-- <input type="radio" id="radioSmallIcon" name="1" onclick="document.getElementById('widgetIcons').style.display = 'block'" />--%>
                                </td>
                                <td>
                                    Small icon and title
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnLargeIconTitle" runat="server" GroupName="setting" CssClass="rbtnLargeIconTitle"
                                        onclick="document.getElementById('widgetIcons').style.display = 'block'" 
                                        meta:resourcekey="rbtnLargeIconTitleResource1" />
                                    <%--<input type="radio" id="radioLargeIcon" name="1" onclick="document.getElementById('widgetIcons').style.display = 'block'" />--%>
                                </td>
                                <td>
                                    Large icon and title
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnOnlyTitle" runat="server" CssClass="rbtnOnlyTitle" GroupName="setting"
                                        onclick="document.getElementById('widgetIcons').style.display = 'block'" 
                                        meta:resourcekey="rbtnOnlyTitleResource1" />
                                    <%--                        <input type="radio" id="radioOnlyTitle" name="1" onclick="document.getElementById('widgetIcons').style.display = 'block'" />--%>
                                </td>
                                <td>
                                    Only title
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                    </div>
                </div>
                <!-- /////////////// widget icons ///////////////- --->
                <div id="widgetIcons" style="display: none;">
                    <div class="con_login_pnl">
                        <div class="loginlbl">
                        </div>
                        <div class="loginControls">
                            <label for="Widget Title">
                                <img src="App_Themes/Classic/Images/widgeticon15x15.gif" alt="" />
                                Widget Title</label>
                        </div>
                    </div>
                    <div class="con_login_pnl">
                        <div class="loginlbl">
                            <asp:Label ID="lblTitle" runat="server" AssociatedControlID="txtTitle" 
                                meta:resourcekey="lblTitleResource1" />
                        </div>
                        <div class="loginControls">
                            <asp:TextBox ID="txtTitle" runat="server" MaxLength="50" CssClass="textBoxMedium"
                                Text="Your Opinion" meta:resourcekey="txtTitleResource1"></asp:TextBox>
                        </div>
                    </div>
                    <div class="con_login_pnl">
                        <div class="loginlbl">
                            <label for="Font Settings">
                                <img src="App_Themes/Classic/Images/widgeticon15x15.gif" alt="" />
                                Font Settings</label>
                        </div>
                        <div class="loginControlsLable">
                            <table cellpadding="0">
                                <tr>
                                    <td>
                                        Font Type
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlFontType" CssClass="dropdownSmall2" runat="server" 
                                            meta:resourcekey="ddlFontTypeResource1">
                                            <asp:ListItem meta:resourcekey="ListItemResource3">Type</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        Size
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlFontSize" CssClass="dropdownSmall2" runat="server" 
                                            meta:resourcekey="ddlFontSizeResource1">
                                            <asp:ListItem meta:resourcekey="ListItemResource4">Size</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        Color
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlFontColor" CssClass="dropdownSmall2" runat="server" 
                                            meta:resourcekey="ddlFontColorResource1">
                                            <asp:ListItem meta:resourcekey="ListItemResource5">Color</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /////////////// widget icons ///////////////- --->
                <div class="con_login_pnl" id="radioNewSurvey">
                    <div class="loginlbl">
                    </div>
                    <div class="loginControls">
                        <asp:Button ID="btnCreateWidget" runat="server" CssClass="btn_small" ToolTip="Create"
                            Text="Create" OnClick="btnCreateWidget_Click" 
                            meta:resourcekey="btnCreateWidgetResource1" />
                    </div>
                </div>
            </div>
            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>
    <script type="text/javascript">
        function showContents(itemName) {
            if (itemName == 'addNewFolderLink') {
                document.getElementById('addNewFolderLink').style.display = 'none';
                document.getElementById('addNewFolderUL').style.display = 'block';
                document.getElementById('successMessage').style.display = 'none';
            } else if (itemName == 'addNewFolderUL') {
                document.getElementById('addNewFolderLink').style.display = 'block';
                document.getElementById('addNewFolderUL').style.display = 'none';
                document.getElementById('successMessage').style.display = 'block';
            }
        }

        $('.rbtnOnlyIcon').click(function () {
            $('#imgIconOnly').show();
            $('#imgSmallIcon').hide();
            $('#imgLargeIcon').hide();
        });

        $('.rbtnSmallIconTitle').click(function () {
            $('#imgIconOnly').hide();
            $('#imgSmallIcon').show();
            $('#imgLargeIcon').hide();
        });
        $('.rbtnLargeIconTitle').click(function () {
            $('#imgIconOnly').hide();
            $('#imgSmallIcon').hide();
            $('#imgLargeIcon').show();
        });
        $('.rbtnOnlyTitle').click(function () {
            $('#imgIconOnly').hide();
            $('#imgSmallIcon').hide();
            $('#imgLargeIcon').hide();
        });

        //Allows only numerics and incase country code textbox allows + also.
        function onlyNumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            if (key > 31 && (key < 48 || key > 57)) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
