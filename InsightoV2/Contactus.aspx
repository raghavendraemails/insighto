﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.master" AutoEventWireup="true"
    CodeFile="Contactus.aspx.cs" Inherits="Insighto.Pages.Contactus" EnableEventValidation = "true"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="marginNewPanel">
        <div class="signUpPnl">
            <h3>
                <asp:Label ID="lblTitle" runat="server" Text="CONTACT US" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>
            </h3>
            <div class="mandatoryPanel">
                <asp:Label ID="lblRequiredFields" runat="server" 
                    Text="indicates required fields" meta:resourcekey="lblRequiredFieldsResource1"></asp:Label>
            </div>
            <div class="errorPanel" id="dvErrMsg" runat="server">
                <asp:Label ID="lblErrMsg" runat="server" CssClass="errorMesg" Visible="False" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="lblName" runat="server"  Text="Your Name"
                        CssClass="requirelbl" meta:resourcekey="lblNameResource1" />
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="txtName" runat="server" CssClass="textBoxMedium" MaxLength="50"
                        TabIndex="1" meta:resourcekey="txtNameResource1"></asp:TextBox>
                </div>
            </div>
             <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="Label1" runat="server" Text="Your Email"
                        CssClass="requirelbl" meta:resourcekey="Label1Resource1" />
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="txtMail" runat="server" CssClass="textBoxMedium" MaxLength="50"
                        TabIndex="1" meta:resourcekey="txtMailResource1"></asp:TextBox>
                </div>
            </div>
             <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="Label2" runat="server" Text="Phone Number"
                        CssClass="requirelbl" meta:resourcekey="Label2Resource1" />
                </div>
                <div class="loginControls">
                     <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><asp:TextBox ID="txtCountryCode" runat="server" TabIndex="9" MaxLength="5" 
                                        CssClass="phoneCountry" meta:resourcekey="txtCountryCodeResource1"></asp:TextBox></td>
                                <td class="spacepanel">&#45;</td>
                                <td><asp:TextBox ID="txtAreaCode" runat="server" TabIndex="10" MaxLength="5" 
                                        CssClass="phoneArea" meta:resourcekey="txtAreaCodeResource1"></asp:TextBox></td>
                                <td class="spacepanel">&#45;</td>
                                <td><asp:TextBox ID="txtPhno" runat="server" MaxLength="10"  
                                        CssClass="phoneNum_long" meta:resourcekey="txtPhnoResource1"></asp:TextBox></td>
                            </tr>
                        </table><asp:Label ID="lblPasswordHelpText" CssClass="note" 
                         Text="Country Code - Area Code - Phone Number."  runat="server" 
                         meta:resourcekey="lblPasswordHelpTextResource1" />
                </div>
            </div>
             <%--<div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="Label3" runat="server" Text="Your Email"
                        CssClass="requirelbl" />
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="TextBox2" runat="server" CssClass="textBoxMedium" MaxLength="50"
                        TabIndex="1"></asp:TextBox>
                </div>
            </div>--%>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="Label4" runat="server" Text="Attention" CssClass="requirelbl" 
                        meta:resourcekey="Label4Resource1" />
                </div>
                <div class="loginControls">
                    <asp:DropDownList ID="ddlAttentionvalues" CssClass="dropdownMedium" 
                        runat="server" TabIndex="13" meta:resourcekey="ddlAttentionvaluesResource1">
                    </asp:DropDownList>
                </div>
            </div>
              <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="Label5" runat="server" Text="Mail Message"
                        CssClass="requirelbl" meta:resourcekey="Label5Resource1" />
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="txtMessage"  TextMode="MultiLine" CssClass="textAreaMedium" 
                        runat="server" meta:resourcekey="txtMessageResource1" />
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                </div>
                <div class="loginControls">
                <div class="top_padding">
                    <asp:Button ID="btnsubmit" runat="server" CssClass="btn_small_65" ToolTip="Submit"
                            Text="Submit" onclick="btnsubmit_Click" 
                        meta:resourcekey="btnsubmitResource1" /></div>
                </div>
            </div>
        </div>
    </div>
    <div class="paddingNPanel">
        <div class="signUpPnlRight">
            <div class="alreadyAccount" >
                <h3><asp:Label ID="lblQuickLinks" runat="server" Text="Quick Links" 
                        meta:resourcekey="lblQuickLinksResource1"></asp:Label></h3>
                
                <div class='suckerdiv'>
                    <asp:Label ID="lblQuickLinksNames" runat="server" 
                        Text=" <ul id='suckertree1'><li><a href='#'>Resources</a></li><li><a href='#'>Organize</a></li><li><a href='#'>Pricing</a></li><li><a href='#'>FAQ</a></li></ul>" 
                        meta:resourcekey="lblQuickLinksNamesResource1"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="clear">
    </div>
</asp:Content>
