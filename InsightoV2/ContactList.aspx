﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="ContactList.aspx.cs" Inherits="Insighto.Pages.ContactList" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="server">
<div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
         
               <h3><asp:Label runat="server" ID="lblCreate" Text="Create List " Visible="False" ></asp:Label>
               <asp:Label runat="server" ID="lblEdit" Text="Edit Name Of Email List" Visible="False" ></asp:Label> </h3>
            
        </div>
      <%--  <div class="popupClosePanel">
           <a href="#" class="popupCloseLink" onclick ="closeModalSelf(true,'addressbook.aspx?Key=<%= Key %>');" title="Close">&nbsp;</a>
        </div>--%>
       
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <div class="popupContentPanel" >
        <!-- popup content panel -->
           <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                <div><asp:Label ID="lblSuccessMsg"  runat="server" Visible="False" 
                    Text="Contact list has been created succesfully." 
                        meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
           </div>
           <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">                
                <div>
                    <asp:Label ID="lblErrMsg"  runat="server" Visible="False"   
                    Text="Contact list already exists." ></asp:Label></div>
            </div>
        <div class="formPanel">
            <!-- form -->
            <div class="forgotpanel">
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblListName" runat="server" Text="Name Of Email List" 
                             />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtListName" runat="server" CssClass="textBoxMedium" 
                            MaxLength="25" meta:resourcekey="txtListNameResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqList" 
                            ControlToValidate="txtListName" CssClass="lblRequired"
                            ErrorMessage="Please enter list name." runat="server" Display="Dynamic" 
                            meta:resourcekey="reqListResource1"></asp:RequiredFieldValidator>
                    </div>
                </div>                
                  <div class="con_login_pnl">
                    <div class="loginlbl"> <asp:Label ID="lblbtnNewFolder" runat="server" AssociatedControlID="btnSave" meta:resourcekey="lblbtnNewFolderResource1" />
                    </div>
                    <div class="loginControls"> <asp:Button ID="btnSave" runat="server" CssClass="btn_small_65" Text="Save"
                            ToolTip="Save"  meta:resourcekey="ibtnSaveResource1" 
                            onclick="btnSave_Click" />
                    </div>
                </div>
               <div class="con_login_pnl" id="successMessage">
                    <div class="reqlbl btmpadding">
                        <asp:Label ID="lbl" runat="server" meta:resourcekey="lblResource1"></asp:Label>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            
            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>     
</asp:Content>

