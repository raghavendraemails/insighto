﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Insighto.Business.Charts;
using Insighto.Business.Helpers;
using App_Code;

public partial class DescriptiveResponses : SurveyPageBase
{
    int quesID = 0;
    int surveyID = 0;
    Hashtable ht = new Hashtable();
    SurveyCore surcore = new SurveyCore();

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["TempChart_det"] = null;
        ValidationCore vcore = new ValidationCore();
        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 && ht.Contains("QuesID"))
            {
                quesID = Convert.ToInt32(ht["QuesID"]);

                if (ht.Contains(Constants.SURVEYID))
                    surveyID = Convert.ToInt32(ht[Constants.SURVEYID]);


                int i = 0;
                int flag = 0;
                SurveyCore cor = new SurveyCore();
                ArrayList ans_ids = new ArrayList();
                ArrayList ans_opt = new ArrayList();
                DataSet ds;
                SelectedSurvey = surcore.GetSurveyWithResponsesCount(surveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                Insighto.Business.Charts.Survey sur = SelectedSurvey;
                if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
                {
                    foreach (SurveyQuestion ques in SelectedSurvey.surveyQues)
                    {
                        if (ques.QUESTION_ID == quesID)
                        {
                            string str = "";
                            str = Utilities.ClearHTML(Convert.ToString(ques.QUESTION_LABEL));
                            str = System.Text.RegularExpressions.Regex.Replace(str, "<span[^>]*>", "");
                            str = str.Replace("<p>", "");
                            str = str.Replace("</p>", "");
                            str = str.Replace("\r\n", "");
                            str = str.Replace("<br>", "");
                            str = str.Replace("</br>", "");
                            lblHeader.Text = "<b>Responses for the Question:</b> " + str;
                            if (ques.QUESTION_TYPE == 9 || ques.QUESTION_TYPE == 13 || ques.QUESTION_TYPE == 15 || ques.QUESTION_TYPE == 20)
                                flag = 1;
                            if (ques.QUESTION_TYPE == 1 || ques.QUESTION_TYPE == 2 || ques.QUESTION_TYPE == 3 || ques.QUESTION_TYPE == 4)
                            {
                                flag = 2;
                                lblHeader.Text = "\""+surcore.Selectsurveyques_other(ques.QUESTION_ID)+"\"" +" Responses for the Question : " + str;
                            }
                        }
                    }
                }
                if (flag == 1)
                {
                    if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
                    {
                        foreach (SurveyQuestion surques in SelectedSurvey.surveyQues)
                        {
                            if (surques.QUESTION_ID == quesID)
                            {
                                if (surques.surveyAnswers != null)
                                {
                                    foreach (SurveyAnswers surans in surques.surveyAnswers)
                                    {
                                        ans_ids.Add(surans.ANSWER_ID);
                                        ans_opt.Add(surans.ANSWER_OPTIONS);
                                    }
                                }
                            }
                        }
                    }
                }
                if (!IsPostBack)
                {
                    if (flag != 1)
                    {
                        if (flag == 2)
                            ds = cor.GetTextResponses(quesID, 1, SelectedSurvey.SURVEY_ID);
                        else
                            ds = cor.GetTextResponses(quesID, 0, SelectedSurvey.SURVEY_ID);                       

                        BoundField bFieldbalnk = new BoundField();
                        bFieldbalnk.HeaderText = "";
                        bFieldbalnk.ItemStyle.Width = Unit.Percentage(20);

                    }
                }

                if (flag == 1)
                {
                    DataSet ds_responselist = new DataSet();
                    ds_responselist = cor.GetRankingConstanSumCommentBoxTexResponses(SelectedSurvey.SURVEY_ID, quesID, ans_ids);
                    if (ds_responselist != null && ds_responselist.Tables.Count > 0)
                    {
                        if (ds_responselist.Tables[0].Columns.Count > 0)
                        {
                            if (!IsPostBack)
                            {
                                foreach (DataColumn col in ds_responselist.Tables[0].Columns)
                                {                                    
                                    BoundField bFieldtxtcol = new BoundField();
                                    bFieldtxtcol.DataField = col.ColumnName;                                   
                                    if (i == 4)
                                    {
                                        bFieldtxtcol.HeaderText = "S.No";                                        
                                        bFieldtxtcol.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                                    }

                                    ++i;
                                    if (i == 1 || i == 2 || i == 3 || i == 4)
                                        bFieldtxtcol.Visible = false;
                                    if (i != 1 && i != 2 && i != 3 && i != 4 && i != 5)
                                    {
                                        DataColumn col1 = new DataColumn();
                                        col1.Caption = Convert.ToString(ans_opt[i - 6]);

                                        bFieldtxtcol.HeaderText = col1.Caption;                                        
                                        bFieldtxtcol.ItemStyle.HorizontalAlign = HorizontalAlign.Left;

                                    }
                                    grdResponses.Columns.Add(bFieldtxtcol);

                                }
                            }
                            if (ds_responselist.Tables[0].Rows.Count > 0)
                            {
                                grdResponses.DataSource = ds_responselist.Tables[0];
                                grdResponses.DataBind();
                            }
                        }
                    }
                }
                else
                {
                    if (flag == 2)
                        ds = cor.GetTextResponses(quesID, 1, SelectedSurvey.SURVEY_ID);
                    else
                        ds = cor.GetTextResponses(quesID, 0, SelectedSurvey.SURVEY_ID);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Columns.Count != null && ds.Tables[0].Columns.Count > 0)
                        {
                            if (!IsPostBack)
                            {
                                int seq = 0;
                                foreach (DataColumn col in ds.Tables[0].Columns)
                                {                                    
                                    BoundField bFieldtxtcol = new BoundField();
                                    if (seq == 0)
                                    {
                                        bFieldtxtcol.HeaderText = "S.No";
                                        bFieldtxtcol.HeaderStyle.Width = Unit.Percentage(7);
                                        bFieldtxtcol.ItemStyle.Width = Unit.Percentage(7);
                                    }
                                    else
                                        bFieldtxtcol.HeaderText = "Answers";

                                    bFieldtxtcol.DataField = col.ColumnName;
                                    grdResponses.Columns.Add(bFieldtxtcol);

                                    seq += 1;
                                }
                            }
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                grdResponses.DataSource = ds.Tables[0];
                                grdResponses.DataBind();                                
                            }

                        }
                    }
                }
            }
        }
    }

    protected void cmdBack_Click(object sender, EventArgs e)
    {
        string navReports = "";

        if (ht.Contains(Constants.SURVEYID))
            surveyID = Convert.ToInt32(ht[Constants.SURVEYID]);

        if (ht != null && ht.Count > 0 && ht.Contains("Veiw"))
        {
            string ViewAllQues = Convert.ToString(ht["Veiw"]);
            if (ViewAllQues == "All")
                navReports += "Veiw=All&" + Constants.SURVEYID + "=" + surveyID;
        }
        else if (quesID > 0)
        {
            navReports += "QuesID=" + quesID + "&" + Constants.SURVEYID + "=" + surveyID;
        }

        if (ht != null && ht.Count > 0 && ht.Contains("Mode"))
        {
            int Mode = Convert.ToInt32(ht["Mode"]);
            navReports += "&Mode=" + Mode;
        }
        if (ht != null && ht.Count > 0 && ht.Contains("Date_Start"))
        {
            navReports += "&Date_Start=" + ht["Date_Start"];
        }
        if (ht != null && ht.Count > 0 && ht.Contains("Date_Start"))
        {           
            navReports += "&Date_End=" + ht["Date_End"];
        }
        string urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", navReports+"&surveyFlag="+SurveyFlag);
        Response.Redirect(urlstr);
    }
}