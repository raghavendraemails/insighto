﻿using System;
using System.Collections.Generic;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WidgetBasics : SecurePageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCategory();
            BindFolders();
            BindWidgetCategory();
            BindFontType();
            BindFontSize();
            BindFontColor();
            ddlFontType.SelectedIndex = this.ddlFontType.Items.IndexOf(ddlFontType.Items.FindByValue("Arial"));
            ddlFontSize.SelectedIndex = this.ddlFontSize.Items.IndexOf(ddlFontSize.Items.FindByValue("9"));
            ddlFontColor.SelectedIndex = this.ddlFontColor.Items.IndexOf(ddlFontColor.Items.FindByValue("Black"));

        }
    }

    #region "Method : BindCategory"
    /// <summary>
    /// Used to get survey category list and bind values to dropdownlist.
    /// </summary>

    private void BindCategory()
    {
        // object on osm_picklist class
        osm_picklist osmPickList = new osm_picklist();
        //Assign vaule category
        osmPickList.CATEGORY = Constants.CATEGORY;
        // object on PickListService class
        var pickListService = ServiceFactory.GetService<PickListService>();
        //inovke method GetPickList
        var vPickList = pickListService.GetPickList(osmPickList);

        if (vPickList.Count > 0)
        {
            //Bind list values to dropdownlist
            this.ddlCategory.DataSource = vPickList;
            this.ddlCategory.DataTextField = Constants.PARAMETER;
            this.ddlCategory.DataValueField = Constants.PARAMETER;
            this.ddlCategory.DataBind();
            //Default selection (Others)
            this.ddlCategory.SelectedValue = Constants.OTHERS;
        }

    }
    #endregion

    #region "Method : BindWidgetCategory"
    /// <summary>
    /// Used to get widget category list and bind values to dropdownlist.
    /// </summary>

    private void BindWidgetCategory()
    {
        // object on osm_picklist class
        osm_picklist osmPickList = new osm_picklist();
        //Assign vaule category
        osmPickList.CATEGORY = Constants.WIDGETCATEGORY;
        // object on PickListService class
        var pickListService = ServiceFactory.GetService<PickListService>();
        //inovke method GetPickList
        var vPickList = pickListService.GetPickList(osmPickList);

        if (vPickList.Count > 0)
        {
            //Bind list values to dropdownlist
            this.ddlWidgetCategory.DataSource = vPickList;
            this.ddlWidgetCategory.DataTextField = Constants.PARAMETER;
            this.ddlWidgetCategory.DataValueField = Constants.PARAM_VALUE;
            this.ddlWidgetCategory.DataBind();
            ddlWidgetCategory.Items.Insert(0, new ListItem("--Select--", ""));           
            this.ddlWidgetCategory.SelectedIndex = 0;
            //Default selection (Others)
            //this.ddlWidgetCategory.SelectedValue = Constants.OTHERS;
        }

    }
    #endregion

    #region "Method : BindFolders"
    /// <summary>   
    /// Used to get user folders list and bind values to dropdownlist.
    /// </summary>

    private void BindFolders()
    {
        // object on osm_cl_folders class
        osm_cl_folders osmFolder = new osm_cl_folders();
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        //Assign vaule userid
        if (loggedInUserInfo != null)
        {
            osmFolder.USERID = loggedInUserInfo.UserId;
            // object on FolderService class
            var folderService = ServiceFactory.GetService<FolderService>();
            //inovke method GetFolders
            var vfolder = folderService.GetFolders(osmFolder);

            if (vfolder.Count > 0)
            {
                //Bind list values to dropdownlist
                this.ddlFolder.DataSource = vfolder;
                this.ddlFolder.DataTextField = "FOLDER_NAME";
                this.ddlFolder.DataValueField = "FOLDER_ID";
                this.ddlFolder.DataBind();
                //Default selection (Others)
                if (vfolder.Count > 0)
                {
                    foreach (var folders in vfolder)
                    {
                        if (folders.FOLDER_NAME == "My Folder")
                        {
                            this.ddlFolder.SelectedValue = folders.FOLDER_ID.ToString();
                        }
                    }
                }
                else
                {
                    this.ddlFolder.SelectedItem.Text = Constants.MYFOLDER;
                }

            }
        }
        else
            base.CloseModal("Home.aspx");
    }

    #endregion

    #region "Method : BindFontType"
    /// <summary>
    /// used to bind font types.
    /// </summary>

    public void BindFontType()
    {
        var pickListService = ServiceFactory.GetService<PickListService>();
        osm_picklist osmPicklist = new osm_picklist();
        osmPicklist.CATEGORY = Constants.FONTTYPE;
        List<osm_picklist> picklist = pickListService.GetPickList(osmPicklist);
        ddlFontType.DataSource = picklist;
        ddlFontType.DataTextField = Constants.PARAMETER;
        ddlFontType.DataValueField = Constants.PARAMETER;
        ddlFontType.DataBind();
    }
    #endregion

    #region "Method : BindFontColor"
    /// <summary>
    /// used to bind font colors.
    /// </summary>

    public void BindFontColor()
    {
        var pickListService = ServiceFactory.GetService<PickListService>();
        osm_picklist osmPicklist = new osm_picklist();
        osmPicklist.CATEGORY = Constants.FONTCOLOR;
        List<osm_picklist> picklist = pickListService.GetPickList(osmPicklist);
        ddlFontColor.DataSource = picklist;
        ddlFontColor.DataTextField = Constants.PARAMETER;
        ddlFontColor.DataValueField = Constants.PARAMETER;
        ddlFontColor.DataBind();
    }

    #endregion

    #region "Method : BindFontSize"
    /// <summary>
    /// used to bind font sizes.
    /// </summary>

    public void BindFontSize()
    {
        var pickListService = ServiceFactory.GetService<PickListService>();
        osm_picklist osmPicklist = new osm_picklist();
        osmPicklist.CATEGORY = Constants.FONTSIZE;
        List<osm_picklist> picklist = pickListService.GetPickListFontSize(osmPicklist);
        ddlFontSize.DataSource = picklist;
        ddlFontSize.DataTextField = Constants.PARAMETER;
        ddlFontSize.DataValueField = Constants.PARAMETER;
        ddlFontSize.DataBind();
    }

    #endregion

    #region "btnSaveFolderClick"
    /// <summary>
    /// SaveFolder click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSaveFolder_Click(object sender, EventArgs e)
    {
        bool folderExists = default(bool);

        osm_cl_folders osmFolder = new osm_cl_folders();
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        var loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        if (loggedInUserInfo != null)
        {
            osmFolder.USERID = loggedInUserInfo.UserId;
            osmFolder.FOLDER_NAME = txtNewFolder.Text.Trim();

            var folderService = ServiceFactory.GetService<FolderService>();
            folderExists = folderService.IsFolderExsists(osmFolder);
            if (folderExists == true)
            {
                osmFolder = folderService.AddFolder(osmFolder);
                BindFolders();
                ddlFolder.SelectedValue = osmFolder.FOLDER_ID.ToString();
                lblSucess.Text = "";
                //lblSucess.Visible = false;
            }
            else
            {
                lblSucess.Text = "Folder name already exists.";
                string param = "addNewFolderLink";
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "test", "showContents('" + param + "');", true);
            }
        }

        else
        {
            base.CloseModal("Home.aspx");
        }


    }

    #endregion   

    #region "Method:UpdateSurveyBySurveyId"
    /// <summary>
    /// Update Survey by SurveyId
    /// </summary>
    /// <param name="SurveyId"></param>
    private void UpdateSurveyBySurveyId(int surveyId)
    {
        osm_survey osmSurvey = new osm_survey();
        SessionStateService sessionStateService = new SessionStateService();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        if (loggedInUserInfo != null)
        {
            osmSurvey.USERID = loggedInUserInfo.UserId;
            //osmSurvey.STATUS = "Draft";
            osmSurvey.SURVEY_ID = surveyId;
            osmSurvey.SURVEY_CATEGORY = ddlCategory.SelectedItem.Text;
            osmSurvey.SURVEY_NAME = txtWidgetName.Text.Trim();
            osmSurvey.FOLDER_ID = Convert.ToInt16(ddlFolder.SelectedItem.Value);
            var surveyService = ServiceFactory.GetService<SurveyService>();
            var retValue = surveyService.UpdateSurvey(osmSurvey);
            if (retValue == false)
            {               
                //lblErrMsg.Text = "Wiget name already exists.";
                lblErrMsg.Visible = true;
                dvErrMsg.Visible = true;
            }
            else
            {
                lblErrMsg.Visible = false;
                lblErrMsg.Text = "";
                dvErrMsg.Visible = false;
                base.CloseModal(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + osmSurvey.SURVEY_ID + "&surveyName=" + osmSurvey.SURVEY_NAME + "&surveyFlag=" + base.SurveyFlag));                
            }
        }
        else
        {
            base.CloseModal("Home.aspx");
        }
    }
    #endregion

    #region "Method : AddWidget"
    /// <summary>
    /// Create New Widget
    /// </summary>
    private void AddWidget()
    {
        osm_survey osmSurvey = new osm_survey();
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();       
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        osm_surveysetting osmSurveySetting = new osm_surveysetting();
        if (loggedInUserInfo != null)
        {

            osmSurvey.USERID = loggedInUserInfo.UserId;
            osmSurvey.STATUS = "Draft";
            osmSurvey.SURVEY_CATEGORY = ddlCategory.SelectedItem.Text;
            osmSurvey.SURVEY_NAME = txtWidgetName.Text.Trim();
            osmSurvey.FOLDER_ID = Convert.ToInt16(ddlFolder.SelectedItem.Value);
            osmSurvey.SurveyWidgetFlag = 1;
            var surveyService = ServiceFactory.GetService<SurveyService>();
             var surveycontrolService = ServiceFactory.GetService<SurveyControls>();
            var widgetService = ServiceFactory.GetService<WidgetService>();
            var widgetSettingsService = ServiceFactory.GetService<WidgetSettings>(); 
                    osmSurvey = surveyService.AddSurvey(osmSurvey);

                    if (osmSurvey == null)
                    {
                        //lblErrMsg.Text = "Widget name already exist.";
                        lblErrMsg.Visible = true;
                        dvErrMsg.Visible = true;
                    }
                    else
                    {
                        var accountSettingService = ServiceFactory.GetService<AccountSettingService>();
                        var accountSetDet = accountSettingService.FindByUserId(loggedInUserInfo.UserId);
                        osmSurveySetting.SURVEY_ID = osmSurvey.SURVEY_ID;
                        osmSurveySetting.SURVEY_HEADER = accountSetDet.SURVEY_HEADER;
                        osmSurveySetting.SURVEY_LOGO = accountSetDet.SURVEY_LOGO;
                        osmSurveySetting.SURVEY_FOOTER = accountSetDet.SURVEY_FOOTER;
                        osmSurveySetting.SURVEY_FONT_TYPE = accountSetDet.SURVEY_FONTTYPE;
                        osmSurveySetting.SURVEY_FONT_SIZE = accountSetDet.SURVEY_FONTSIZE;
                        osmSurveySetting.SURVEY_FONT_COLOR = accountSetDet.SURVEY_FONTCOLOR;
                        osmSurveySetting.SURVEY_BUTTON_TYPE = accountSetDet.SURVEY_BUTTONTYPE;
                        osmSurveySetting.CUSTOMSTART_TEXT = accountSetDet.SURVEY_STARTTEXT;
                        osmSurveySetting.CUSTOMSUBMITTEXT = accountSetDet.SURVEY_SUBMITTEXT;
                        osmSurveySetting.PAGE_BREAK = accountSetDet.PAGE_BREAK;
                        osmSurveySetting.RESPONSE_REQUIRED = accountSetDet.RESPONSE_REQUIRED;
                        osmSurveySetting.BROWSER_BACKBUTTON = 1;
                        osmSurveySetting.DELETED = 0;
                        osmSurveySetting.BUTTON_BG_COLOR = "Purple";
                        osmSurveySetting.SURVEY_LOGO = "<App_Themes/Classic/Images/insighto_logo_medium.jpg><224_81_medium_224_81><Left>";
                        osmSurveySetting.Show_Logo = true;  
                        osmSurveySetting.ProgressbarText = "Questions viewed";
                        osmSurveySetting.Show_Progressbar = true;
                        osmSurveySetting.Progressbar_Type = "ProgressBarClassic";
                        osmSurveySetting.Show_Progressbartext = true;  
                        surveyService.SetSurveySetting(osmSurveySetting);
                        osm_widget osmWidget = new osm_widget();
                        osmWidget.DomainName = txtDomainName.Text.Trim();
                        osmWidget.Category = ValidationHelper.GetInteger(ddlWidgetCategory.SelectedItem.Value, 0);
                        osmWidget.SurveyId = osmSurvey.SURVEY_ID;
                        osmWidget.WidgetTitle = txtTitle.Text.Trim();
                        osmWidget.CreatedOn = DateTime.Now;
                        osmWidget.ModifiedOn = DateTime.Now;
                        widgetService.AddWidget(osmWidget);
                        var surControls = ServiceFactory.GetService<SurveyControls>().GetSurveycontrolDetailsBySurveyId(osmSurvey.SURVEY_ID);
                        surControls.ALERT_ON_MAX_RESPONSES = ValidationHelper.GetInteger(txtMaxRespondents.Text.Trim(), 0);
                        surControls.ALERT_ON_MIN_RESPONSES = ValidationHelper.GetInteger(txtMinRespondents.Text.Trim(), 0);
                        surControls.SURVEY_URL = EncryptHelper.EncryptQuerystring(PathHelper.GetOpenWidgetURL(), "SurveyId=" + osmSurvey.SURVEY_ID);
                        surveycontrolService.updateSurveyControls(surControls);
                        osm_WidgetSettings osmWidgetSettings = new osm_WidgetSettings();
                        osmWidgetSettings.SurveyId = osmSurvey.SURVEY_ID;
                        osmWidgetSettings.FontSize = ddlFontSize.SelectedItem.Value;
                        osmWidgetSettings.FontType = ddlFontType.SelectedItem.Value;
                        osmWidgetSettings.FontColor = ddlFontColor.SelectedItem.Value;
                        if (rbtnOnlyIcon.Checked)
                        {
                            osmWidgetSettings.LogoFlag = 1;
                            osmWidgetSettings.ImageName = "widgeticon30x30.gif";
                        }
                        else if (rbtnSmallIconTitle.Checked)
                        {
                            osmWidgetSettings.LogoFlag = 2;
                            osmWidgetSettings.ImageName = "widgeticon20x20.gif";
                        }
                        else if (rbtnLargeIconTitle.Checked)
                        {
                            osmWidgetSettings.LogoFlag = 3;
                            osmWidgetSettings.ImageName = "widgeticon40x40.gif";
                        }
                        else
                        {
                            osmWidgetSettings.LogoFlag = 4;
                            osmWidgetSettings.ImageName = "";
                            ddlFontType.SelectedIndex = this.ddlFontType.Items.IndexOf(ddlFontType.Items.FindByValue("Arial"));
                            ddlFontSize.SelectedIndex = this.ddlFontSize.Items.IndexOf(ddlFontSize.Items.FindByValue("9"));
                            ddlFontColor.SelectedIndex = this.ddlFontColor.Items.IndexOf(ddlFontColor.Items.FindByValue("Black"));
                        }
                        widgetSettingsService.AddWidgetSettings(osmWidgetSettings);
                        base.CloseModal(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + osmSurvey.SURVEY_ID + "&surveyName=" + osmSurvey.SURVEY_NAME + "&surveyFlag=" + base.SurveyFlag));
                        lblErrMsg.Visible = false;
                        lblErrMsg.Text = "";
                        dvErrMsg.Visible = false;
                    }
                
        }
        else
        {
            base.CloseModal("Home.aspx");
        }
    }
    #endregion

    protected void btnCreateWidget_Click(object sender, EventArgs e)
    {
        AddWidget();
    }
}