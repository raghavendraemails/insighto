﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using Resources;

namespace Insighto.Pages
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            var surveyFlag = Session[CommonMessages.SurveyFlag] != null ? Session[CommonMessages.SurveyFlag] : 0;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/NoLuck.aspx", "surveyFlag=" + surveyFlag + "&Page=" + Request.QueryString["aspxerrorpath"].ToString().Replace("/", "")));
        }
    }
}