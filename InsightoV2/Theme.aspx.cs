﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using App_Code;


public partial class Theme : SurveyPageBase
{
    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int surveyId = 0;
    int userId;

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = false;
        var userDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
        if (userDetails != null)
        {
            userId = userDetails.UserId;
        }

        if (!IsPostBack)
        {
            var surveySetting = ServiceFactory.GetService<SurveySetting>();
            var lstThemes = surveySetting.GetThemes();
            var themesCategory = from th in lstThemes
                                 orderby th.THEME_CATEGORY descending
                                 group th by th.THEME_CATEGORY into thg
                                 select new { THEME_CATEGORY = thg.Key, osm_themes = thg };


            lstThemesCategory.DataSource = themesCategory;
            lstThemesCategory.DataBind();
            BindDefaultInfo();

            lblDefaultTheme.Text = String.Format(Utilities.ResourceMessage("lblDefaultTheme"), SurveyMode, SurveyMode.ToLower());

        }
    }

    private void BindDefaultInfo()
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }

        if (surveyId > 0)
        {
            var surveyInfo = ServiceFactory.GetService<SurveyService>().GetSurveyBySurveyId(surveyId);
            var settings = ServiceFactory<AccountSettingService>.Instance.GetallUserSettingsByUserId(userId);

            if (surveyInfo.Any())
            {
                if (surveyInfo[0].THEME == "")
                {
                    SelectRadio("Insighto Classic");
                }
                else
                {
                    SelectRadio(surveyInfo[0].THEME);
                }

                if (settings.THEME == null)
                {
                    chkDefault.Checked = false;
                }
                else
                {
                    chkDefault.Checked = true;
                }
            }

        }
        else
        {
            SelectRadio("Insighto Classic");
        }
    }

    #region "Method : SelectRadio"
    /// <summary>
    /// Used to select the radio button in list view.
    /// </summary>
    /// <param name="strSelect"></param>

    private void SelectRadio(string strSelect)
    {
        foreach (ListViewItem categoryItem in lstThemesCategory.Items)
        {
            ListView lstThemes1 = categoryItem.FindControl("lstThemes") as ListView;
            foreach (ListViewItem item in lstThemes1.Items)
            {
                RadioButton rbtnTheme = item.FindControl("rbtnTheme") as RadioButton;
                if (rbtnTheme.Text == strSelect)
                {
                    rbtnTheme.Checked = true;
                }
            }
        }
    }
    #endregion

    #region "Event : lstThemes_ItemDataBound"
    /// <summary>
    /// Used to binding the data.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lstThemes_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            //src="App_Themes/Classic/Images/Theme_Blue_s.jpg"
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            string strImageUrl = "App_Themes/Classic/Images/Theme_" + ((Insighto.Data.osm_themes)(dataItem.DataItem)).THEME_NAME + "_s.jpg";

            Image imgTheme = e.Item.FindControl("imgTheme") as Image;
            imgTheme.ImageUrl = strImageUrl;

            RadioButton rbtnTheme = e.Item.FindControl("rbtnTheme") as RadioButton;
            //rbtnTheme.GroupName = "mytest";
            string script = "SetSingleRadioButton('" + rbtnTheme.ClientID + "',this)";
            rbtnTheme.Attributes.Add("onclick", script);

        }

    }

    #endregion

    protected void chkDefault_CheckedChanged(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }

        if (surveyId > 0)
        {
            var surveyInfo = ServiceFactory.GetService<SurveyService>().GetSurveyBySurveyId(surveyId);

            if (surveyInfo.Any())
            {
                if (surveyInfo[0].THEME != "Insighto Classic")
                {
                    UnCheckVal();
                    SaveTheme("Insighto Classic", surveyId);
                    BindDefaultInfo();

                    lblSuccMsg.Text = Utilities.ResourceMessage("saveMsg"); //"Default theme saved successfully.";
                    dvSuccessMsg.Visible = true;
                }
            }
            SetAsDefaultTheme(SelectVal(), surveyId);
        }
        else
        {
            lblErrMsg.Text = String.Format(Utilities.ResourceMessage("errCreateSurvey"), SurveyMode.ToLower()); //"This survey not created yet. Please create the survey.";
            dvErrMsg.Visible = true;
        }
    }

    private void SaveTheme(string strTheme, int SurveyId)
    {
        osm_survey survey = new osm_survey();
        survey.SURVEY_ID = SurveyId;
        survey.THEME = strTheme;

        var surveySettingService = ServiceFactory.GetService<SurveySetting>();
        surveySettingService.Save(survey);

    }

    private void SetAsDefaultTheme(string strTheme, int userId)
    {
        ServiceFactory<AccountSettingService>.Instance.UpdateUserTheme(userId, strTheme);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "GetVal", "getSelectedRadioButton()",true);

        //SelectVal();

        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }


        if (surveyId > 0)
        {
            SaveTheme(SelectVal(), surveyId);
            if (chkDefault.Checked)
            {
                SetAsDefaultTheme(SelectVal(), userId);
            }
            else
            {
                SetAsDefaultTheme(null, userId);
            }
            BindDefaultInfo();

            lblSuccMsg.Text = Utilities.ResourceMessage("saveTheme"); //"Theme saved successfully.";
            dvSuccessMsg.Visible = true;
        }
        else
        {
            lblErrMsg.Text = String.Format(Utilities.ResourceMessage("errCreateSurvey"), SurveyMode.ToLower()); //"This survey not created yet. Please create the survey.";
            dvErrMsg.Visible = true;
        }
    }

    #region "Method : SelectRadio"
    /// <summary>
    /// Used to select the radio button in list view.
    /// </summary>
    /// <param name="strSelect"></param>

    private string SelectVal()
    {
        string strVal = default(string);
        foreach (ListViewItem categoryItem in lstThemesCategory.Items)
        {
            ListView lstThemes1 = categoryItem.FindControl("lstThemes") as ListView;
            foreach (ListViewItem item in lstThemes1.Items)
            {
                RadioButton rbtnTheme = item.FindControl("rbtnTheme") as RadioButton;
                if (rbtnTheme.Checked)
                {
                    strVal = rbtnTheme.Text;
                }
            }
        }
        return strVal;
    }
    #endregion


    #region "Method : UnCheckVal"
    /// <summary>
    /// Used to uncheck the radio button in list view.
    /// </summary>

    private void UnCheckVal()
    {
        foreach (ListViewItem categoryItem in lstThemesCategory.Items)
        {
            ListView lstThemes1 = categoryItem.FindControl("lstThemes") as ListView;
            foreach (ListViewItem item in lstThemes1.Items)
            {
                RadioButton rbtnTheme = item.FindControl("rbtnTheme") as RadioButton;
                if (rbtnTheme.Checked)
                {
                    rbtnTheme.Checked = false;
                }
            }
        }
    }
    #endregion
}