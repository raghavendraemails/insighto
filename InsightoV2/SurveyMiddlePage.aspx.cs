﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using System.Configuration;
using System.Web.UI.HtmlControls;

public partial class SurveyMiddlePage : SurveyPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SurveyBasicInfoView != null && SurveyBasicInfoView.DELETED == 0)
        {
            if (IsSurveyActive)
            {
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/EditActiveSurveyCautions.aspx", "SurveyId=" + SurveyID + "&surveyFlag=" + SurveyFlag));
            }
            else
            {
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/SurveyDrafts.aspx", "SurveyId=" + SurveyID + "&surveyFlag=" + SurveyFlag));
            }
        }
        else
        {
           // Response.AddHeader("REFRESH", ConfigurationManager.AppSettings["TimeOut"]+";URL="+EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(),"surveyFlag="+SurveyFlag));
            HtmlMeta equiv = new HtmlMeta();
            equiv.HttpEquiv = "refresh";
            equiv.Content = "5; url="+EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL().Replace("~/",""),"surveyFlag="+SurveyFlag);
            Header.Controls.Add(equiv); 
        }
    }
}