﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SurveyRespondent.aspx.cs"
    MasterPageFile="~/SurveyRespondent.master" Inherits="Insighto.Pages.SurveyRespondent" MaintainScrollPositionOnPostback="true" %>

<%@ Reference VirtualPath="~/UserControls/RespondentControls/SurveyRespondentControl.ascx" %>
<%@ Register Src="~/UserControls/RespondentControls/SurveyRespondentControl.ascx"
    TagName="SurveyRespondentControl" TagPrefix="Insighto" %>
<asp:Content ContentPlaceHolderID="ContentHead" runat="server" ID="headerPlaceHolder">
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentMain" runat="server" ID="Content1">
    <div class="questionViewpanel">
        <Insighto:SurveyRespondentControl ID="SurveyRespondentControl1" runat="server" />
    </div>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-29026379-1']);
        _gaq.push(['_setDomainName', 'insighto.com']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script>

    <script type="text/javascript" language="javascript">
        var questionId = '';
        var answerOptionId = '';
        var value = '';
        var isTextBox = false;

        $(".textBoxMedium").change(function () {
            isTextBox = true;

            var id;
            if ($(this).attr("data") == undefined) {
                id = $(this).attr("id");
            } else {
                id = $(this).attr("data");
            }
            var splitIds = id.split("_").reverse();
            value = $(this).val();
            questionId = splitIds[1];
            answerOptionId = splitIds[0];
            $(this).val(value.replace(/^\s+|\s+$/g, ''));
            value = $(this).val();

            postData();
        });

        $(".textAreaCommentBox").change(function () {
            isTextBox = true;

            var id;
            if ($(this).attr("data") == undefined) {
                id = $(this).attr("id");
            } else {
                id = $(this).attr("data");
            }
            var splitIds = id.split("_").reverse();
            value = $(this).val();
            questionId = splitIds[1];
            answerOptionId = splitIds[0];
            $(this).val(value.replace(/^\s+|\s+$/g, ''));
            value = $(this).val();

            postData();
        });

        $(".dropDown").change(function (e) {
            isTextBox = false;
            value = $(this).val();
            var splitVal = value.split("_").reverse();
            questionId = splitVal[1];
            answerOptionId = splitVal[0];

            postData();
        });

        $(".radio").live("click", function () {
            isTextBox = false;
            value = $(this).next().val();
            var splitVal = value.split("_").reverse();
            questionId = splitVal[1];
            answerOptionId = splitVal[0];           
            postData();
        });

        $(".checkbox").live("click", function () {
            isTextBox = false;
            value = $(this).next().val();
            var splitVal = value.split("_").reverse();
            questionId = splitVal[1];
            answerOptionId = splitVal[0];

            postData();
        });


        function postData() {
            if (isTextBox == false)
                value = "";

            var respondentId = $(".hdnRespondentId").val();
            $.post("AjaxService.aspx", { "method": "SaveResponseAnswerOptions",
                "respondentId": respondentId,
                "questionId": questionId,
                "answerOptionId": answerOptionId,
                "otherText": value.length > 0 ? value : ""
            },
                   function (result) {
                   });
        }

        //Highlight row and column         tblMatrix2D
   
    </script>
</asp:Content>
