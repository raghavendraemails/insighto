﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;


public partial class ManageCategory : BasePage 
{    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnSurveyFlag.Value = SurveyFlag.ToString();
            lblHelpText.Text = String.Format(Utilities.ResourceMessage("lblHelpText"), SurveyMode.ToLower());
        }
    }
}