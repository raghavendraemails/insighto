﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Insighto.Business.Services;
using Insighto.Exceptions;
using DotNetOpenAuth.OpenId.RelyingParty;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;
using DotNetOpenAuth.OAuth2;
using DotNetOpenAuth.ApplicationBlock;
using System.Configuration;
using System.Net;
using DotNetOpenAuth.ApplicationBlock.Facebook;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Data;
using Insighto.Business.Enumerations;


public partial class LoginIntermediate : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["acc"] == "google")
        {
            LoginGoogleUser();
        }
        else
        {
            LoginFaceBookUser();
        }
    }

    private void LoginFaceBookUser()
    {
            var graph = Session["facebookGraph"] as FacebookGraph;
            if (graph == null)
            {
                Response.Write("<script>window.opener.location = 'Home.aspx'; window.close();</script>");
            }
            SaveUser(graph.FirstName, graph.LastName, graph.Email, string.Empty, UserLinkAccountType.FaceBook);
            
    }

    private void LoginGoogleUser()
    {
        var fetchResponse = Session["FetchResponse"] as FetchResponse ;
        if (fetchResponse == null)
        {
            Response.Write("<script>window.opener.location = 'Home.aspx'; window.close();</script>");
           
        }

        var email = fetchResponse.GetAttributeValue(WellKnownAttributes.Contact.Email);
        var firstName = fetchResponse.GetAttributeValue(WellKnownAttributes.Name.First);
        var lastName = fetchResponse.GetAttributeValue(WellKnownAttributes.Name.Last);
        var country = fetchResponse.GetAttributeValue(WellKnownAttributes.Contact.HomeAddress.Country);
        SaveUser(firstName, lastName, email, country, UserLinkAccountType.Google);
    }

    private void SaveUser(string firstName, string lastName, string email, string country, UserLinkAccountType accountType)
    {
        var userService = ServiceFactory.GetService<UsersService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        int userId = 0;
        string userEmail = string.Empty, userTimeZone = string.Empty;


        if (userInfo == null) //before login 
        {
            var validateService = ServiceFactory.GetService<ValidateEmailService>();
            //check emailid exist or not
            var isExist = validateService.ValidateUserByEmailId(email);

            // user not logged in and email id doesnt exist in the system.. so create new user and corrseponding link account
           // with supplied accountType
            if (!isExist)
            {
                //Register new user 
                var user = userService.RegisterFreeUser(firstName, lastName, email, country, 0, "","");
                userService.ProcessUserLogin(user); // user session created
                userService.ActivateUser(user.USERID);//user activated to login 
                userId = user.USERID; 
                userEmail = user.LOGIN_NAME;
                userTimeZone = user.TIME_ZONE;
                var userLinkAccountService = ServiceFactory.GetService<UserLinkAccountService>();

                //Find user exist with google and facebook account types
                var userLinkAccount = userLinkAccountService.Find(userId, accountType);

                //if account type not exist then create user in userlinkaccount table
                if (userLinkAccount == null)
                {
                    userLinkAccount = new osm_userlinkaccount
                    {
                        UserID = userId,
                        Email = email,
                        AccountType = accountType.ToString(),
                        AccountReferenceID = 0
                    };
                    userLinkAccountService.Save(userLinkAccount);
                }


            }
            else // Registered user
            {
                var userLinkService = ServiceFactory.GetService<UserLinkAccountService>();
                //find user exist with google or facebook                 
                var userAccount = userLinkService.FindUserIdByEmail(email, accountType);

                if (userAccount != null)
                {
                    //get details for user by userid exist in link account 
                    var user = userService.FindUserByUserId(userAccount.UserID);
                    if (user != null)
                    {
                        userId = user.USERID;
                        userEmail = user.LOGIN_NAME;
                        userTimeZone = user.TIME_ZONE;
                        //Save user session
                        userService.ProcessUserLogin(user);
                    }

                    
                }
                else
                {
                    var userLinkAccountService = ServiceFactory.GetService<UserLinkAccountService>();
                    var userDet =  userService.FindUserByEmailId(email);
                    osm_userlinkaccount userLinkAcct = new osm_userlinkaccount();
                    userLinkAcct.UserID = userDet.USERID;
                    userLinkAcct.Email = userDet.EMAIL;
                    userLinkAcct.AccountType = accountType.ToString();
                    userLinkAcct.AccountReferenceID = 0;
                    var userLinkSavedDet = userLinkAccountService.Save(userLinkAcct);
                    if (userLinkSavedDet != null)
                    {
                        userService.ProcessUserLogin(userDet);
                    }

                   // Response.Redirect(PathHelper.GetUserMyAccountPageURL());
                }

            }

        }

        else  //after login
        {
            userId = userInfo.UserId;
            userEmail = userInfo.LoginName;
            userTimeZone = userInfo.TimeZone;

            string status = string.Empty;
            var userLinkAccountService = ServiceFactory.GetService<UserLinkAccountService>();
            //find user with account type
            var userLinkAccount = userLinkAccountService.Find(userId, accountType);

            //create google/facebook account if not exist
            if (userLinkAccount == null)
            {
                var validateService = ServiceFactory.GetService<ValidateEmailService>();
                var isExist = validateService.ValidateUserByEmailId(email);
                if (!isExist || userEmail==email)
                {
                    userLinkAccount = new osm_userlinkaccount
                    {
                        UserID = userId,
                        Email = email,
                        AccountType = accountType.ToString(),
                        AccountReferenceID = 0
                    };
                    userLinkAccountService.Save(userLinkAccount);
                    status = "Success";
                }
            }
            string url = EncryptHelper.EncryptQuerystring("MyProfile.aspx", "Status=" + status + "&surveyFlag=" + SurveyFlag);
            Response.Write("<script>window.opener.location = '" + url + "';window.close();</script>");


        }


        if (userTimeZone == null)
            Response.Write("<script>window.opener.location = 'Welcome.aspx?accountType="+accountType+"'; window.close();</script>");



        else
            Response.Write("<script>window.opener.location = 'MyAccounts.aspx';window.close();</script>");

    }

}