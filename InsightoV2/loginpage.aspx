﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="loginpage.aspx.cs" Inherits="loginpage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 274px;
        }
        .headerlogo
        {
            height: 83px;border: 0px none;overflow: hidden;font-family: Arial,sans-serif;font-size: 13px;color: #404040;
            
        }
        .headertext
        {
            font-family: Arial,sans-serif;font-size: 13px;color: #404040;direction: ltr;text-align: center;
         }
         .h1
         {
             font-size: 38px;margin-bottom: 15px;font-family: 'Open Sans',arial;color: #555;font-weight: 300;margin-top: 0px;margin: 0px 0px 15px;
         }
         .h2
         {
            margin-bottom: 15px;font-family: 'Open Sans',arial;color: #555;font-size: 24px;font-weight: 400;margin: 0px 0px 15px; 
          }
          .imagelogo
          {
              width: 96px;
              height: 96px;
              margin: 0px auto 10px;
              display: block;
              border-radius: 50%;font-family: Arial,sans-serif;
              font-size: 13px;
              color: #404040;
              direction: ltr;
          }
          .textbox
          {
              margin-bottom: 0px;border-left: 1px solid #D9D9D9;
            border-top: 1px solid #C0C0C0;
            border-bottom: 1px solid #D9D9D9;
            direction: ltr;font-size: 16px; width: 100%;display: block;margin-bottom: 10px;z-index: 1;position: relative;-moz-box-sizing: border-box; -moz-appearance: none;display: inline-block;height: 40px;padding: 0px 8px;margin: 0px;background: none repeat scroll 0% 0% #FFF;border-right: 1px solid #D9D9D9;-moz-border-top-colors: none;-moz-border-right-colors: none;-moz-border-bottom-colors: none;-moz-border-left-colors: none;border-image: none;-moz-box-sizing: border-box;border-radius: 1px;font-size: 15px;color: #404040;
            top: 0px;
            left: 0px;
        }
        .lblRequired
         {
          background: url('App_Themes/Classic/Images/required_red.jpg') no-repeat 0 -2px;
          padding-left: 20px;
          color: red;
          ForeColor:Red;
          text-align:top;
          Font-Size:12px;
          font-family:Arial;
         }
    </style>
    
<script language="javascript" type="text/javascript">

    function WaterMark(objtxt, event) {
        //        alert("event="+event);
        //        alert("objid=" + objtxt.id);
        
    
        var defaultEmailText = "Email";
        var defaultpwdText = "Password";
        // Condition to check textbox length and event type
        if (objtxt.id == "txtEmail" || objtxt.id == "txtpassword1") {
            if (objtxt.value.length == 0 & event.type == "blur") {
                //  alert(event.type);
                //if condition true then setting text color and default text in textbox
                
                if (objtxt.id == "txtEmail") {
                    objtxt.style.color = "Gray";
                    objtxt.value = defaultEmailText;
                    document.getElementById("<%=lblvalidmsgemail.ClientID%>").innerText = "";

                }
                else if (objtxt.id == "txtpassword1") {
                    document.getElementById("<%= txttempwd.ClientID %>").style.display = "block";
                    objtxt.style.display = "none";
                    document.getElementById("<%=lblvalidmsgpwd.ClientID%>").innerText = "";
                }
               
            }
            else if (objtxt.value.length != 0 & event.type == "blur") {

                // alert("length>0" + event.type);

               
                if (objtxt.id == "txtEmail") {
                    document.getElementById("<%=lblvalidmsgemail.ClientID%>").innerText = "";

                }
                else if (objtxt.id == "txtpassword1") {
                    document.getElementById("<%=lblvalidmsgpwd.ClientID%>").innerText = "";
                }
                
            }
        }

        // Condition to check textbox value and event type

        if ((objtxt.value == defaultEmailText || objtxt.value == defaultpwdText ) & event.type == "focus") {

            //  alert(event.type);

            
            if (objtxt.id == "txtEmail") {
                objtxt.style.color = "black";
                objtxt.value = "";
            }
            if (objtxt.id == "txttempwd") {
                objtxt.style.display = "none";
                document.getElementById("<%= txtpassword1.ClientID %>").style.display = "";
                document.getElementById("<%= txtpassword1.ClientID %>").focus();
            }
            
        }
    }

</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="100%">
    <tr>
    <td class="headerlogo" align="center">
    <img src="App_Themes/Classic/Images/insighto_logo.png" border="0" alt="" />
    </td>
    </tr>
    <tr>
    <td align="center" class="headertext" >
  <h1 class="h1">
  Create smarter, more professional surveys.
  </h1>
  <div>&nbsp;</div>
    </td>
    </tr>
    <tr align="center">
    <td style="padding: 0px 44px;">
    <table>
    <tr>
    <td style=" padding:40px;margin-bottom: 20px;background-color:White;border-radius: 2px;box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3);font-family: Arial,sans-serif;font-size: 13px;color: #404040;direction: ltr;" 
            class="style1">
    <div align="center">
    <h2 class="h2">
      Sign in to My Surveys</h2>
    </div>
    <p style="font-size: 16px;font-weight: bold;text-align: center;margin: 10px 0px 0px;min-height: 1em;color: #404040;font-size: 13px;font-family: Arial,sans-serif;direction: ltr;">
    
    </p>
    <div>
       
    <asp:TextBox ID="txtEmail" runat="server" class="textbox"  onblur="WaterMark(this, event);" Text="Email" onfocus="WaterMark(this, event);"  size="40" ForeColor="Gray" Font-Names="Arial"></asp:TextBox>
    
    </div>
    <div>&nbsp;</div>
    <div>
    <asp:TextBox ID="txttempwd" Text="Password" runat="server"  onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" class="textbox"  size="40"/> 
    <asp:TextBox ID="txtpassword1" MaxLength="16" TextMode="password" Text="Password" runat="server" Style="display:none" onblur="WaterMark(this, event);" Font-Names="Arial" class="textbox"  size="40" />
    </div>
    
    <div style=" height:30px; padding-top:4px;">&nbsp;
      <asp:Label ID="lblvalidmsgemail" runat="server" Text=""  class="lblRequired" Visible="false"></asp:Label>
        <asp:Label ID="lblvalidmsgpwd" runat="server" Text=""  class="lblRequired" Visible="false"></asp:Label>
        <asp:Label ID="lblerrmsg" runat="server" Text=""   class="lblRequired" Visible="false"></asp:Label>
    </div>
    
    <div>
    <asp:Button ID="BTNSIGNIN" runat="server" Text="Sign in" 
            style="width: 100%;display: block;margin-bottom: 0px; z-index: 1;position: relative;-moz-box-sizing: border-box;font-family: Arial,sans-serif;font-size: 17px;border: 1px solid #3079ED;color: #FFF;text-shadow: 0px 1px rgba(0, 0, 0, 0.1);background-color: #4D90FE;background-image: -moz-linear-gradient(center top , #4D90FE, #4787ED);min-width: 46px;text-align: center;font-weight: 700;height: 40px;line-height: 36px;border-radius: 3px;transition: all 0.218s ease 0s;-moz-user-select: none;cursor: default; top: 0px; left: 0px;" 
            onclick="BTNSIGNIN_Click" />
    </div>
    
    </td>
    </tr>

    </table>
    </td>
    </tr>
    <tr>
    <td>
    <div style="font-family: Arial,sans-serif;font-size: 13px;color: #404040;direction: ltr;" >
    <%--<p style="margin-bottom: 30px; margin: 0px 0px 10px;
color: #555;
font-size: 14px;
text-align: center;">
    <a style="color: #427FED;
cursor: pointer;
text-decoration: none;font-size: 14px;
text-align: center;"> Create an account </a>
    </p>--%>
    </div>
    
    </td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
