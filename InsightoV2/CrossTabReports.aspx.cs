﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using DevExpress.Web.ASPxEditors;
using FeatureTypes;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using System.Collections.Generic;
using System.Linq;
using Insighto.Business.ValueObjects;


public partial class CrossTabReports : SurveyPageBase
{

    SurveyCore surcore = new SurveyCore();

    DataSet ds_questions = new DataSet();
    DataSet ds_reports = new DataSet();
    DataSet ds_rowcolcount = new DataSet();
    DataSet ds_rowcolcountmultipleselect = new DataSet();
    DataSet ds_rowcountmulselect = new DataSet();
    DataSet ds_cross_saved_reports = new DataSet();
    SurveyCore obj_questions = new SurveyCore();

    string[] idarray = new string[2];
    string str1 = "";
    int chkboxcount = 0;
    int colquestiontype = 0;
    int colquestionid = 0;
    int rowquestionid = 0;
    int status_flag = 0;
    int tem1, tem2 = 0;
    string[] ques_array = new string[2];
    int[] arr3;
    string[] percent;

    int[] total_count;
    int[] arr1;
    int ques1_anscount, ques2_anscount;
    int corp_flag = 0;
    int ques_type_flag1 = 0, ques_type_flag2 = 0;
    string[] total_row_percent;
    Double[] tot_row_percent;
    Hashtable ht = new Hashtable();

    public string Key
    {
        get
        {
            return HttpUtility.UrlEncode(QueryStringHelper.GetString("Key"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {


         var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        //if (loggedInUserInfo.LicenseType == "FREE")
        //{

        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
        //    pnlMain.Enabled = false;
        //    lbn_addnewcrosstab.Enabled = false;
        //    btngenerate.Enabled = false;


        //}
       
            pnlMain.Enabled = true;
            lbn_addnewcrosstab.Enabled = true;
            btngenerate.Enabled = true;
        
        


            lbl_confirm.Visible = false;
            lbl_message.Visible = false;

            if (!IsPostBack)
            {
                SetPremiumPermissions();
            }

            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            Session["TempChart_det"] = null;
            int SurveyID = 0;
            hlCrossTabReport.Attributes.Add("Onclick", "javascript: window.open('help/4_3.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;");
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    SurveyID = Convert.ToInt32(ht[Constants.SURVEYID]);
                    SelectedSurvey = surcore.GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                }
            }
            else
            {
                SurveyID = SurveyBasicInfoView.SURVEY_ID;
                SelectedSurvey = surcore.GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
            }
            if (SurveyID > 0)
            {
                ds_questions = obj_questions.getsurveyquestions(SurveyID);
                ds_cross_saved_reports = obj_questions.getcrossreports(SurveyID);
            }

            if (ds_cross_saved_reports != null && ds_cross_saved_reports.Tables.Count > 0)
            {
                if (ds_cross_saved_reports.Tables[0].Rows.Count != 0)
                {
                    CrossTabSavedReports();
                }
            }
            int vall = 0;
            if (ht != null && ht.Count > 0 && ht.Contains("crep_id"))
            {
                vall = Convert.ToInt32(ht["crep_id"]);
            }

            if (vall != 0 && ds_cross_saved_reports.Tables.Count > 0)
            {
                for (int i = 0; i < ds_cross_saved_reports.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToInt32(ds_cross_saved_reports.Tables[0].Rows[i].ItemArray[0].ToString()) == vall)
                    {
                        idarray[0] = ds_cross_saved_reports.Tables[0].Rows[i].ItemArray[2].ToString();
                        idarray[1] = ds_cross_saved_reports.Tables[0].Rows[i].ItemArray[3].ToString();
                        rnd_questionspanel.HeaderText = " Cross Tab Report Name: " + "  " + ds_cross_saved_reports.Tables[0].Rows[i].ItemArray[5].ToString();
                        txt_crosstabreportname.Text = ds_cross_saved_reports.Tables[0].Rows[i].ItemArray[5].ToString();
                        break;
                    }

                }
                tem1 = Convert.ToInt32(idarray[0]);
                tem2 = Convert.ToInt32(idarray[1]);
               // report_function();
                ReportFunction();

            }
            if (vall != 0)
            {
                 DataRow[] drq1 = ds_questions.Tables[0].Select("QUESTION_ID='" + tem1 + "'");
                 DataRow[] drq2 = ds_questions.Tables[0].Select("QUESTION_ID='" + tem2 + "'");
                HtmlTable Question = new HtmlTable();
                Question.Border = 0;
                Question.CellPadding = 1;
                Question.CellSpacing = 0;
                Question.Width = "600";

                HtmlTableRow trow1 = new HtmlTableRow();
                HtmlTableCell tcell1 = new HtmlTableCell();
                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                lbl.Text = "Cross Tabbed Questions";
                lbl.Font.Bold = true;
                tcell1.Controls.Add(lbl);
                trow1.Cells.Add(tcell1);
                Question.Rows.Add(trow1);
                System.Web.UI.WebControls.Label lbl_rowquestion = new System.Web.UI.WebControls.Label();
                lbl_rowquestion.Text = "Q1." + drq1[0].ItemArray[1].ToString();
                HtmlTableRow trow2 = new HtmlTableRow();
                HtmlTableCell tcell2 = new HtmlTableCell();
                tcell2.Controls.Add(lbl_rowquestion);
                trow2.Cells.Add(tcell2);
                Question.Rows.Add(trow2);
                System.Web.UI.WebControls.Label lbl_columnquestion = new System.Web.UI.WebControls.Label();
                lbl_columnquestion.Text = "Q2." + drq2[0].ItemArray[1].ToString();
                HtmlTableRow trow3 = new HtmlTableRow();
                HtmlTableCell tcell3 = new HtmlTableCell();
                tcell3.Controls.Add(lbl_columnquestion);
                trow3.Cells.Add(tcell3);
                Question.Rows.Add(trow3);
                rnd_questionspanel.Controls.Add(Question);
                btngenerate.Visible = false;
                txt_crosstabreportname.Visible = false;
                ASPxLabel1.Visible = false;
                btnexporttoexcel.Visible = true;

            }

            if (vall == 0)
            {
                int datacount = 0;
                if (ds_questions != null && ds_questions.Tables.Count > 0)
                {
                    datacount = ds_questions.Tables[0].Rows.Count;
                }
                HtmlTable Question = new HtmlTable();
                Question.Width = "600";
                Question.Border = 0;

                Question.CellPadding = 1;
                Question.CellSpacing = 0;
                HtmlTableRow trow1 = new HtmlTableRow();
                HtmlTableCell tcell1 = new HtmlTableCell();
                tcell1.Align = "Center";
                System.Web.UI.WebControls.Label lbl1 = new System.Web.UI.WebControls.Label();
                lbl1.Width = 50;
                lbl1.Text = "Rows";
                lbl1.Font.Bold = true;
                tcell1.Controls.Add(lbl1);
                trow1.Cells.Add(tcell1);
                System.Web.UI.WebControls.Label lbl2 = new System.Web.UI.WebControls.Label();
                lbl2.Width = 70;
                lbl2.Text = "Columns";
                lbl2.Font.Bold = true;
                HtmlTableCell tcell2 = new HtmlTableCell();
                tcell2.Align = "Center";
                tcell2.Controls.Add(lbl2);
                trow1.Cells.Add(tcell2);
                System.Web.UI.WebControls.Label lbl3 = new System.Web.UI.WebControls.Label();
                lbl3.Width = 480;
                lbl3.Text = "     " + " List Of Questions  ";
                lbl3.Font.Bold = true;
                HtmlTableCell tcell3 = new HtmlTableCell();
                tcell3.Controls.Add(lbl3);
                trow1.Height = "25px";
                //trow1.BgColor = "#d0e1eb";
                trow1.Cells.Add(tcell3);
                Question.Rows.Add(trow1);
                rnd_questionspanel.Controls.Add(Question);
                if (ds_questions != null && ds_questions.Tables.Count > 0)
                {
                    if (ds_questions.Tables[0].Rows.Count > 0)
                    {
                        int rw = 0;
                        for (int i = 0; i < ds_questions.Tables[0].Rows.Count; i++)
                        {
                            HtmlTableRow trow = new HtmlTableRow();
                            trow.Height = "25px";
                            HtmlTableCell tcell4 = new HtmlTableCell();
                            tcell4.Attributes.Add("style", "border-top:solid 1px #ccc;");
                            tcell4.Align = "Center";
                            System.Web.UI.WebControls.CheckBox chk = new System.Web.UI.WebControls.CheckBox();
                            chk.ID = "ChkboxRow" + ds_questions.Tables[0].Rows[i].ItemArray[0].ToString();
                            chk.Width = 50;
                            chk.Text = "           ";
                            tcell4.Controls.Add(chk);
                            trow.Cells.Add(tcell4);
                            System.Web.UI.WebControls.CheckBox chk1 = new System.Web.UI.WebControls.CheckBox();
                            chk1.Width = 70;
                            chk1.ID = "ChkboxCol" + ds_questions.Tables[0].Rows[i].ItemArray[0].ToString();
                            chk1.Text = "               ";
                            HtmlTableCell tcell5 = new HtmlTableCell();
                            tcell5.Attributes.Add("style", "border-top:solid 1px #ccc;");
                            tcell5.Align = "Center";
                            tcell5.Controls.Add(chk1);
                            trow.Cells.Add(tcell5);
                            ASPxLabel lbl = new ASPxLabel();
                            lbl.Text = "     " + ds_questions.Tables[0].Rows[i].ItemArray[1].ToString();
                            lbl.EnableDefaultAppearance = true;
                            lbl.EnableTheming = false;
                            lbl.EncodeHtml = false;
                            HtmlTableCell tcell6 = new HtmlTableCell();
                            tcell6.Attributes.Add("style", "border-top:solid 1px #ccc;");
                            tcell6.Controls.Add(lbl);
                            tcell6.Width = "480";
                            trow.Cells.Add(tcell6);
                            if (rw == 0)
                                if (rw == 0)
                                    rw = 1;
                                else
                                    rw = 0;
                            Question.Rows.Add(trow);
                        }

                    }
                }
                rnd_questionspanel.Controls.Add(Question);

                btngenerate.Visible = true;
                txt_crosstabreportname.Visible = false;
                ASPxLabel1.Visible = false;

            }
            CheckUserFeatureLinks();
        
    }


    /// <summary>
    /// Sets the premium permissions.
    /// </summary>
    private void SetPremiumPermissions()
    {
        UserType userType = Utilities.GetUserType();
        bool showIndivisualResponse = Utilities.ShowOrHideFeature(userType, (int)RealTimeReports.Individual_responses);
        bool showCustomCharts = Utilities.ShowOrHideFeature(userType, (int)RealTimeReports.Customization_of_Charts);

        IndividualRes.Visible = showIndivisualResponse;
      //  CustomReports.Visible = showCustomCharts;

        CustomReports.Visible = false;

    }

    #region "Method : SetFeatures"
    /// <summary>
    /// This method returns bool value, if the feature flag is one.
    /// </summary>
    /// <param name="strFeature"></param>
    /// <returns></returns>

    public int SetFeatures(string strFeature)
    {
        int iVal = 0;
        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var sessdet = session.GetLoginUserDetailsSession();
        if (sessdet != null)
        {

            
            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            string strlicensetype;
            DataSet dssurveytype = surcore.getSurveyType(SurveyID);
            if (dssurveytype.Tables[0].Rows[0][1].ToString() != "")
            {
                strlicensetype = dssurveytype.Tables[0].Rows[0][1].ToString();
            }
            else
            {
                strlicensetype = sessdet.LicenseType;
            }



            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(strlicensetype));

            if (sessdet.UserId > 0 && listFeatures != null)
            {
                foreach (var listFeat in listFeatures)
                {

                    if (listFeat.Feature.Contains(strFeature))
                    {
                        iVal = (int)listFeat.Value;
                    }
                }
            }
        }

        else
        {
            Response.Redirect(PathHelper.GetUserLoginPageURL());
        }
        return iVal;
    }

    #endregion

    public void CheckUserFeatureLinks()
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();

        if (userDetails != null && userDetails.UserId > 0)
        {

            if (SetFeatures("CUSTOMCHART") == 0)
            {
                CustomReports.HRef = "#";
                CustomReports.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
            else
            {
                string strCustomTab = EncryptHelper.EncryptQuerystring("CustomizeReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
                CustomReports.HRef = strCustomTab;

            }
            if (SetFeatures("INDIVIDUAL_RESPONSES") == 0)
            {
                IndividualRes.HRef = "#";
                IndividualRes.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
            else
            {
                string strIndividual = EncryptHelper.EncryptQuerystring("IndividualResponse.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
                IndividualRes.HRef = strIndividual;
            }

            if (SetFeatures("EXPORTCROSSTAB_EXCEL") == 0)
            {
                btnexporttoexcel.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
        }
    }

    public void SetSubNaviLinks()
    {
        string strCustomTab = EncryptHelper.EncryptQuerystring("CustomizeReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
        CustomReports.HRef = strCustomTab;
    }

    protected void btngenerate_Click(object sender, EventArgs e)
    {
        IterateControls1(this);
        if (status_flag == 3)
        {
            if (idarray != null)
            {
                if ((idarray[0].Substring(0, 9) == idarray[1].Substring(0, 9)) || (idarray[0].Substring(9) == idarray[1].Substring(9)))
                    status_flag = 1;
            }
        }
        if (status_flag == 1)
        {
            lbl_message.Text = "Please select only one column and only one row -but of different questions.";
            lbl_message.Visible = true;
            rpnl_crosstabbing.Visible = false; rpnl_crosstabbing_tbl.Visible = false;
            divCrosstab.Visible = false;
            btnsavecrosstab.Visible = false;
            txt_crosstabreportname.Visible = false;
            btnexporttoexcel.Visible = false;
        }
        else if (status_flag == 2)
        {
            lbl_message.Text = "Please select only one column and only one row -but of different questions.";
            lbl_message.Visible = true;
            rpnl_crosstabbing.Visible = false; rpnl_crosstabbing_tbl.Visible = false;
            divCrosstab.Visible = false;
            btnsavecrosstab.Visible = false;
            txt_crosstabreportname.Visible = false;
            btnexporttoexcel.Visible = false;
        }
        else if (status_flag == 4)
        {
            lbl_message.Text = "Please select only one column and only one row -but of different questions.";
            lbl_message.Visible = true;
            rpnl_crosstabbing.Visible = false; rpnl_crosstabbing_tbl.Visible = false;
            divCrosstab.Visible = false;
            btnsavecrosstab.Visible = false;
            txt_crosstabreportname.Visible = false;
            btnexporttoexcel.Visible = false;
        }
        else if (status_flag == 3)
        {
            lbl_message.Visible = false;
            rpnl_crosstabbing.Visible = true; rpnl_crosstabbing_tbl.Visible = true;
            int count = ds_questions.Tables[0].Rows.Count;
            str1 = "";
            IterateControls(this);

            string temp1 = "";
            string temp2 = "";
            if (idarray != null)
            {
                temp1 = idarray[0];
                temp2 = idarray[1];
                tem1 = Convert.ToInt32(idarray[0].Substring(9));
                tem2 = Convert.ToInt32(idarray[1].Substring(9));
            }
            //report_function();
            ReportFunction();
            btnsavecrosstab.Visible = true;

            ASPxLabel1.Visible = true;
            txt_crosstabreportname.Visible = true;
            txt_crosstabreportname.Text = "";
            btnexporttoexcel.Visible = true;
            if (ds_cross_saved_reports != null && ds_cross_saved_reports.Tables.Count > 0)
            {
                for (int i = 0; i < ds_cross_saved_reports.Tables[0].Rows.Count; i++)
                {
                    if (tem1 == Convert.ToInt32(ds_cross_saved_reports.Tables[0].Rows[i]["QUESTION_ROW_ID"]) && tem2 == Convert.ToInt32(ds_cross_saved_reports.Tables[0].Rows[i]["QUESTION_COLUMN_ID"]))
                    {
                        txt_crosstabreportname.Text = ds_cross_saved_reports.Tables[0].Rows[i]["CREPORT_NAME"].ToString();
                        break;
                    }
                }
            }
        }
    }


    void IterateControls(Control parent)
    {
        foreach (Control child in parent.Controls)
        {
            if (child.GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
            {
                System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)child;

                if (chk.Checked == true)
                {
                    str1 = chk.ID;
                    if (str1.Substring(0, 9) == "ChkboxRow")
                        idarray[0] = str1;
                    else if (str1.Substring(0, 9) == "ChkboxCol")
                        idarray[1] = str1;
                }
            }
            if (child.Controls.Count > 0)
            {
                IterateControls(child);
            }
        }
    }


    void IterateControls1(Control parent)
    {
        foreach (Control child in parent.Controls)
        {
            if (child.GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
            {
                System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)child;
                if (chk.Checked == true)
                {
                    chkboxcount = chkboxcount + 1;

                    if (chkboxcount > 2)
                    {
                        if (idarray[0].Substring(0, 9) == idarray[1].Substring(0, 9))
                            status_flag = 1;
                        else
                            status_flag = 2;
                        break;
                    }
                    str1 = chk.ID;
                    if (str1.Substring(0, 9) == "ChkboxRow")
                        idarray[chkboxcount - 1] = str1;
                    else if (str1.Substring(0, 9) == "ChkboxCol")
                        idarray[chkboxcount - 1] = str1;
                }
            }
            if (chkboxcount == 2)
                status_flag = 3;
            if (chkboxcount < 2)
                status_flag = 4;

            if (child.Controls.Count > 0)
            {
                IterateControls1(child);
            }
        }
    }

    protected void btnsavecrosstab_Click(object sender, EventArgs e)
    {
        //User usr = AuthenticatedUser;
        IterateControls(this);
        int tem1 = Convert.ToInt32(idarray[0].Substring(9));
        int tem2 = Convert.ToInt32(idarray[1].Substring(9));
        int creport_existsflag = 0;
        for (int i = 0; i < ds_cross_saved_reports.Tables[0].Rows.Count; i++)
        {
            if (tem1 == Convert.ToInt32(ds_cross_saved_reports.Tables[0].Rows[i]["QUESTION_ROW_ID"]) && tem2 == Convert.ToInt32(ds_cross_saved_reports.Tables[0].Rows[i]["QUESTION_COLUMN_ID"]))
            {
                creport_existsflag = 1;
                break;
            }
        }
        int check_flag = 0;

        for (int i = 0; i < ds_cross_saved_reports.Tables[0].Rows.Count; i++)
        {
            if (txt_crosstabreportname.Text == ds_cross_saved_reports.Tables[0].Rows[i]["CREPORT_NAME"].ToString())
            {
                check_flag = 1;
                break;
            }
        }
        if (check_flag == 0)
        {

            int return_id = obj_questions.NewCrossReport(SurveyBasicInfoView.SURVEY_ID, tem1, tem2, DateTime.UtcNow, txt_crosstabreportname.Text, SurveyBasicInfoView.USERID);
            ds_cross_saved_reports = obj_questions.getcrossreports(SurveyBasicInfoView.SURVEY_ID);
            CrossTabSavedReports();
            rpnl_crosstabbing.Visible = false; rpnl_crosstabbing_tbl.Visible = false;
            btnsavecrosstab.Visible = false;
            btnexporttoexcel.Visible = false;
            txt_crosstabreportname.Visible = false;
            ASPxLabel1.Visible = false;
            lbl_confirm.Text = "Cross Tab Report Has been Saved Successfully";
            lbl_confirm.Visible = true;
        }
        else
        {
            lbl_confirm.Text = "Cross Tab report already exists. Please choose another name.";
            lbl_confirm.Visible = true;
            txt_crosstabreportname.Focus();
            btnsavecrosstab.Visible = true;
            btnexporttoexcel.Visible = true;
            txt_crosstabreportname.Visible = true;
            generatebuttonfunction();
        }

    }
    private void AddMtxcolsDatatoTable(int answerid, string answeroption, string answeroptionname, string groupname, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["AnswerID"] = answerid;
            dr["answeroption"] = answeroption;
            dr["answeroptionname"] = answeroptionname;
            dr["groupname"] = groupname;
           
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private System.Data.DataTable createMtxColsDataTable()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
            DataColumn myDataColumn3 = new DataColumn();
          

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "AnswerID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "answeroption";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "answeroptionname";
            myDataTable.Columns.Add(myDataColumn2);

            myDataColumn3.DataType = Type.GetType("System.String");
            myDataColumn3.ColumnName = "groupname";
            myDataTable.Columns.Add(myDataColumn3);


            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void ReportFunction()
    {
        try
        {

            DataRow[] drq1 = ds_questions.Tables[0].Select("QUESTION_ID='" + tem1 + "'");
            DataRow[] drq2 = ds_questions.Tables[0].Select("QUESTION_ID='" + tem2 + "'");

            HtmlTable Repnew1 = new HtmlTable();
            Repnew1.ID = "crosstabsst";
            Repnew1.Width = "100%";
            Repnew1.Border = 1;
            Repnew1.CellPadding = 1;
            Repnew1.CellSpacing = 0;


            HtmlTableRow rowquestion = new HtmlTableRow();
            HtmlTableCell rowquestioncell1 = new HtmlTableCell();
            rowquestioncell1.Width = "100%";

            rowquestion.ID = "r1";
            System.Web.UI.WebControls.Label rowquestionlbl = new System.Web.UI.WebControls.Label();
            System.Web.UI.WebControls.Label rowquestionlblspace = new System.Web.UI.WebControls.Label();
            if (drq1.Length > 0)
            {
                rowquestionlbl.Text = drq1[0].ItemArray[1].ToString();
                rowquestionlbl.Font.Bold = true;
            }
            else
            {
                rowquestionlbl.Text = "";
                rowquestionlbl.Font.Bold = true;
            }


            HtmlTableCell rowquestion2cell1 = new HtmlTableCell();
            rowquestion2cell1.Width = "100%";

            System.Web.UI.WebControls.Label rowquestion2lbl = new System.Web.UI.WebControls.Label();

            if (drq2.Length > 0)
            {
                rowquestion2lbl.Text = drq2[0].ItemArray[1].ToString();
                rowquestion2lbl.Font.Bold = true;
            }
            else
            {
                rowquestion2lbl.Text = "";
                rowquestion2lbl.Font.Bold = true;
            }

            rowquestioncell1.Controls.Add(rowquestionlbl);
            rowquestioncell1.BgColor = "#b1c8e9";
            rowquestion.Cells.Add(rowquestioncell1);

            rowquestion2cell1.Controls.Add(rowquestion2lbl);
            rowquestion2cell1.BgColor = "#b1c8e9";
            rowquestion.Cells.Add(rowquestion2cell1);

            Repnew1.Rows.Add(rowquestion);


            HtmlTableRow colrownew1 = new HtmlTableRow();

            HtmlTableCell cellspace = new HtmlTableCell();

            System.Web.UI.WebControls.Label lblspace = new System.Web.UI.WebControls.Label();
            lblspace.Text = "";
            cellspace.Controls.Add(lblspace);
            colrownew1.Cells.Add(cellspace);

            DataSet dsansoptions = new DataSet("table");
            System.Data.DataTable mtxcolnames = createMtxColsDataTable();

            if (drq2[0].ItemArray[2].ToString() == "10" || drq2[0].ItemArray[2].ToString() == "11")
            {
                DataSet dsansoptionscol = surcore.getMatrixAnsweroptionsQID(Convert.ToInt32(drq2[0].ItemArray[0].ToString()));

                for (int Tcoltxt = 0; Tcoltxt < dsansoptionscol.Tables[1].Rows.Count; Tcoltxt++)
                {
                    string strcategory = dsansoptionscol.Tables[1].Rows[Tcoltxt][3].ToString();
                    if (mtxcolnames.Rows.Count < dsansoptionscol.Tables[1].Rows.Count)
                    {
                        for (int Tcoltxt1 = 0; Tcoltxt1 < dsansoptionscol.Tables[1].Rows.Count; Tcoltxt1++)
                        {
                            if (strcategory == dsansoptionscol.Tables[1].Rows[Tcoltxt1][3].ToString())
                            {

                                AddMtxcolsDatatoTable(Convert.ToInt32(dsansoptionscol.Tables[1].Rows[Tcoltxt1][0].ToString()), dsansoptionscol.Tables[1].Rows[Tcoltxt1][1].ToString(), dsansoptionscol.Tables[1].Rows[Tcoltxt1][2].ToString(), dsansoptionscol.Tables[1].Rows[Tcoltxt1][3].ToString(), mtxcolnames);
                            }
                        }
                    }
                }

                System.Data.DataTable dt0 = dsansoptionscol.Tables[0].Copy();
                dsansoptions.Tables.Add(dt0);


                dsansoptions.Tables.Add(mtxcolnames);

            }
            else
            {
                dsansoptions = surcore.getAnsweroptionsQID(Convert.ToInt32(drq2[0].ItemArray[0].ToString()));

            }



            string createtotalcolumn = "";

            for (int rans = 0; rans < dsansoptions.Tables[1].Rows.Count; rans++)
            {
                HtmlTableCell cellcolrownew1 = new HtmlTableCell();
                cellcolrownew1.BgColor = "#b1c8e9";

                System.Web.UI.WebControls.Label lblcolrownew1 = new System.Web.UI.WebControls.Label();
                string strcolrownew = dsansoptions.Tables[1].Rows[rans][1].ToString().Replace("$--$", "-");
                lblcolrownew1.Text = strcolrownew;

                cellcolrownew1.Controls.Add(lblcolrownew1);
                colrownew1.Cells.Add(cellcolrownew1);

                if (rans < dsansoptions.Tables[1].Rows.Count - 1)
                {
                    createtotalcolumn = dsansoptions.Tables[1].Rows[rans + 1][1].ToString();
                    if (createtotalcolumn != dsansoptions.Tables[1].Rows[rans][1].ToString())
                    {
                        HtmlTableCell cellother = new HtmlTableCell();
                        cellother.BgColor = "#b1c8e9";

                        System.Web.UI.WebControls.Label lblother = new System.Web.UI.WebControls.Label();
                        lblother.Font.Bold = true;
                        lblother.Text = dsansoptions.Tables[1].Rows[rans][1].ToString().Replace("$--$", "-") + " - " + "Total";
                        cellother.Controls.Add(lblother);
                        colrownew1.Cells.Add(cellother);
                    }
                }

            }

            HtmlTableCell cellotherfinal = new HtmlTableCell();
            cellotherfinal.BgColor = "#b1c8e9";
            System.Web.UI.WebControls.Label lblotherfinal = new System.Web.UI.WebControls.Label();
            lblotherfinal.Font.Bold = true;
            // lblotherfinal.Text = ds_reports.Tables[4].Rows[ds_reports.Tables[4].Rows.Count-1][3].ToString() + " - " + "Total";
            lblotherfinal.Text = dsansoptions.Tables[1].Rows[dsansoptions.Tables[1].Rows.Count - 1][1].ToString().Replace("$--$", "-") + " - " + "Total";
            cellotherfinal.Controls.Add(lblotherfinal);
            colrownew1.Cells.Add(cellotherfinal);

            Repnew1.Rows.Add(colrownew1);


            DataSet dsansoptionsrows = surcore.getAnsweroptionsQID(Convert.ToInt32(drq1[0].ItemArray[0].ToString()));
            for (int q1rans = 0; q1rans < dsansoptionsrows.Tables[1].Rows.Count; q1rans++)
            {
                HtmlTableRow colrownewq1 = new HtmlTableRow();
                HtmlTableRow colrownewq1pcntg = new HtmlTableRow();

                HtmlTableCell cellrownewq1 = new HtmlTableCell();
                cellrownewq1.BgColor = "#b1c8e9";
                cellrownewq1.NoWrap = true;
                System.Web.UI.WebControls.Label lblrownew1 = new System.Web.UI.WebControls.Label();
                string strrownew = dsansoptionsrows.Tables[1].Rows[q1rans][1].ToString().Replace("$--$", "-");
                lblrownew1.Text = strrownew;

                cellrownewq1.Controls.Add(lblrownew1);
                colrownewq1.Cells.Add(cellrownewq1);

                int rowansid1;
                int rowansid2;

                if (dsansoptionsrows.Tables[1].Rows[q1rans][0].ToString() == "999999999")
                {
                    rowansid1 = 0;
                }
                else
                {
                    rowansid1 = Convert.ToInt32(dsansoptionsrows.Tables[1].Rows[q1rans][0].ToString());
                }


                int indvcoltotal = 0;
                int indvtotalp1 = 0;
                for (int rans1 = 0; rans1 < dsansoptions.Tables[1].Rows.Count; rans1++)
                {
                    int indvcoltotal1 = 0;
                    HtmlTableCell cellrownewq1d = new HtmlTableCell();
                    cellrownewq1d.BgColor = "#ffffff";
                    cellrownewq1d.NoWrap = true;
                    System.Web.UI.WebControls.Label lblrownew1d = new System.Web.UI.WebControls.Label();

                    if (dsansoptions.Tables[1].Rows[rans1][0].ToString() == "999999999")
                    {
                        rowansid2 = 0;
                    }
                    else
                    {
                        rowansid2 = Convert.ToInt32(dsansoptions.Tables[1].Rows[rans1][0].ToString());
                    }


                    DataSet dscrxcnt = obj_questions.getCrxTbValuesCnt(Convert.ToInt32(drq1[0].ItemArray[0].ToString()), Convert.ToInt32(drq2[0].ItemArray[0].ToString()),
                        rowansid1, rowansid2, SurveyID);

                    string strrownewd = dscrxcnt.Tables[0].Rows.Count.ToString();
                    lblrownew1d.Text = strrownewd;

                    cellrownewq1d.Controls.Add(lblrownew1d);
                    colrownewq1.Cells.Add(cellrownewq1d);

                    string createtotalcolumnd = "";

                    indvcoltotal1 = indvcoltotal1 + Convert.ToInt32(strrownewd);

                    if (rans1 < dsansoptions.Tables[1].Rows.Count - 1)
                    {
                        createtotalcolumnd = dsansoptions.Tables[1].Rows[rans1 + 1][1].ToString();
                        if (createtotalcolumnd != dsansoptions.Tables[1].Rows[rans1][1].ToString())
                        {
                            HtmlTableCell cellotherd = new HtmlTableCell();
                            cellotherd.BgColor = "#b1c8e9";

                            System.Web.UI.WebControls.Label lblotherd = new System.Web.UI.WebControls.Label();
                            lblotherd.Font.Bold = true;
                            lblotherd.Text = indvcoltotal1.ToString();
                            cellotherd.Controls.Add(lblotherd);
                            colrownewq1.Cells.Add(cellotherd);
                        }
                    }

                    indvcoltotal = indvcoltotal1;
                }

                HtmlTableCell cellotherfinald = new HtmlTableCell();
                cellotherfinald.BgColor = "#b1c8e9";
                System.Web.UI.WebControls.Label lblotherfinald = new System.Web.UI.WebControls.Label();
                lblotherfinald.Font.Bold = true;
                lblotherfinald.Text = indvcoltotal.ToString();
                cellotherfinald.Controls.Add(lblotherfinald);
                colrownewq1.Cells.Add(cellotherfinald);


                HtmlTableCell cellrownewq1pcntg = new HtmlTableCell();
                cellrownewq1pcntg.BgColor = "#ffffff";
                cellrownewq1pcntg.NoWrap = true;
                System.Web.UI.WebControls.Label lblrownew1pcntg = new System.Web.UI.WebControls.Label();
                lblrownew1pcntg.Text = "";

                cellrownewq1pcntg.Controls.Add(lblrownew1pcntg);
                colrownewq1pcntg.Cells.Add(cellrownewq1pcntg);

                int rowansid1totalp;
                int rowansid2totalp;

                for (int rans1pcntg = 0; rans1pcntg < dsansoptions.Tables[1].Rows.Count; rans1pcntg++)
                {
                    int sumtotalp = 0;
                    int indvtotalp = 0;
                    if (dsansoptions.Tables[1].Rows[rans1pcntg][0].ToString() == "999999999")
                    {
                        rowansid2totalp = 0;
                    }
                    else
                    {
                        rowansid2totalp = Convert.ToInt32(dsansoptions.Tables[1].Rows[rans1pcntg][0].ToString());
                    }

                    for (int q1ransp = 0; q1ransp < dsansoptionsrows.Tables[1].Rows.Count; q1ransp++)
                    {
                        if (dsansoptionsrows.Tables[1].Rows[q1ransp][0].ToString() == "999999999")
                        {
                            rowansid1totalp = 0;
                        }
                        else
                        {
                            rowansid1totalp = Convert.ToInt32(dsansoptionsrows.Tables[1].Rows[q1ransp][0].ToString());
                        }
                        DataSet dscrxcnttotalp = obj_questions.getCrxTbValuesCnt(Convert.ToInt32(drq1[0].ItemArray[0].ToString()), Convert.ToInt32(drq2[0].ItemArray[0].ToString()),
                        rowansid1totalp, rowansid2totalp, SurveyID);
                        sumtotalp = sumtotalp + dscrxcnttotalp.Tables[0].Rows.Count;
                        if (sumtotalp == 0)
                        {
                            sumtotalp = 1;
                        }
                    }


                    DataSet dscrxcnttotalpd = obj_questions.getCrxTbValuesCnt(Convert.ToInt32(drq1[0].ItemArray[0].ToString()), Convert.ToInt32(drq2[0].ItemArray[0].ToString()),
                       rowansid1, rowansid2totalp, SurveyID);


                    HtmlTableCell cellrownewq1dp = new HtmlTableCell();
                    cellrownewq1dp.BgColor = "#ffffff";
                    cellrownewq1dp.NoWrap = true;
                    System.Web.UI.WebControls.Label lblrownew1dp = new System.Web.UI.WebControls.Label();


                    string percentdisplay = Math.Round(Convert.ToDouble(dscrxcnttotalpd.Tables[0].Rows.Count * 100 / sumtotalp)).ToString();
                    indvtotalp = indvtotalp + Convert.ToInt32(Math.Round(Convert.ToDouble(dscrxcnttotalpd.Tables[0].Rows.Count * 100 / sumtotalp)));
                    indvtotalp1 = indvtotalp;
                    string strrownewdp = percentdisplay + "%";
                    lblrownew1dp.Text = strrownewdp;

                    cellrownewq1dp.Controls.Add(lblrownew1dp);
                    colrownewq1pcntg.Cells.Add(cellrownewq1dp);

                    string createtotalcolumndp = "";

                    if (rans1pcntg < dsansoptions.Tables[1].Rows.Count - 1)
                    {
                        createtotalcolumndp = dsansoptions.Tables[1].Rows[rans1pcntg + 1][1].ToString();
                        if (createtotalcolumndp != dsansoptions.Tables[1].Rows[rans1pcntg][1].ToString())
                        {
                            HtmlTableCell cellotherdp = new HtmlTableCell();
                            cellotherdp.BgColor = "#b1c8e9";

                            System.Web.UI.WebControls.Label lblotherdp = new System.Web.UI.WebControls.Label();
                            lblotherdp.Font.Bold = true;
                            lblotherdp.Text = indvtotalp.ToString() + "%";
                            cellotherdp.Controls.Add(lblotherdp);
                            colrownewq1pcntg.Cells.Add(cellotherdp);
                        }
                    }


                }
                HtmlTableCell cellotherfinaldp = new HtmlTableCell();
                cellotherfinaldp.BgColor = "#b1c8e9";
                System.Web.UI.WebControls.Label lblotherfinaldp = new System.Web.UI.WebControls.Label();
                lblotherfinaldp.Font.Bold = true;
                lblotherfinaldp.Text = indvtotalp1.ToString() + "%";
                cellotherfinaldp.Controls.Add(lblotherfinaldp);
                colrownewq1pcntg.Cells.Add(cellotherfinaldp);




                Repnew1.Rows.Add(colrownewq1);
                Repnew1.Rows.Add(colrownewq1pcntg);
            }
            //display totals
            HtmlTableRow totalrow = new HtmlTableRow();
            HtmlTableCell totalcellcolrownew1 = new HtmlTableCell();
            totalcellcolrownew1.BgColor = "#b1c8e9";

            System.Web.UI.WebControls.Label lblothertotal = new System.Web.UI.WebControls.Label();
            lblothertotal.Font.Bold = true;
            lblothertotal.Text = "";
            totalcellcolrownew1.Controls.Add(lblothertotal);
            totalrow.Cells.Add(totalcellcolrownew1);

            int rowansid1total;
            int rowansid2total;
            int sumtotal1 = 0;
            for (int ranstotal = 0; ranstotal < dsansoptions.Tables[1].Rows.Count; ranstotal++)
            {
                int sumtotal = 0;

                HtmlTableCell totalcolcell = new HtmlTableCell();
                totalcolcell.BgColor = "#b1c8e9";

                if (dsansoptions.Tables[1].Rows[ranstotal][0].ToString() == "999999999")
                {
                    rowansid2total = 0;
                }
                else
                {
                    rowansid2total = Convert.ToInt32(dsansoptions.Tables[1].Rows[ranstotal][0].ToString());
                }

                for (int q1ranstotal = 0; q1ranstotal < dsansoptionsrows.Tables[1].Rows.Count; q1ranstotal++)
                {

                    if (dsansoptionsrows.Tables[1].Rows[q1ranstotal][0].ToString() == "999999999")
                    {
                        rowansid1total = 0;
                    }
                    else
                    {
                        rowansid1total = Convert.ToInt32(dsansoptionsrows.Tables[1].Rows[q1ranstotal][0].ToString());
                    }

                    DataSet dscrxcnttotal = obj_questions.getCrxTbValuesCnt(Convert.ToInt32(drq1[0].ItemArray[0].ToString()), Convert.ToInt32(drq2[0].ItemArray[0].ToString()),
                        rowansid1total, rowansid2total, SurveyID);


                    sumtotal = sumtotal + dscrxcnttotal.Tables[0].Rows.Count;

                }

                System.Web.UI.WebControls.Label lblcolrownew1total = new System.Web.UI.WebControls.Label();
                string strcolrownewtotal = sumtotal.ToString();
                lblcolrownew1total.Text = strcolrownewtotal;

                totalcolcell.Controls.Add(lblcolrownew1total);
                totalrow.Cells.Add(totalcolcell);

                string totalcolumn1 = "";

                if (ranstotal < dsansoptions.Tables[1].Rows.Count - 1)
                {
                    totalcolumn1 = dsansoptions.Tables[1].Rows[ranstotal + 1][1].ToString();
                    if (totalcolumn1 != dsansoptions.Tables[1].Rows[ranstotal][1].ToString())
                    {
                        HtmlTableCell cellothertotal = new HtmlTableCell();
                        cellothertotal.BgColor = "#b1c8e9";

                        System.Web.UI.WebControls.Label lblothertotal1 = new System.Web.UI.WebControls.Label();
                        lblothertotal1.Font.Bold = true;
                        lblothertotal1.Text = sumtotal.ToString();
                        cellothertotal.Controls.Add(lblothertotal1);
                        totalrow.Cells.Add(cellothertotal);
                    }
                }
                sumtotal1 = sumtotal;
            }

            HtmlTableCell cellotherfinaltotal = new HtmlTableCell();
            cellotherfinaltotal.BgColor = "#b1c8e9";
            System.Web.UI.WebControls.Label lblotherfinaltotal = new System.Web.UI.WebControls.Label();
            lblotherfinaltotal.Font.Bold = true;
            lblotherfinaltotal.Text = sumtotal1.ToString();
            cellotherfinaltotal.Controls.Add(lblotherfinaltotal);
            totalrow.Cells.Add(cellotherfinaltotal);


            Repnew1.Rows.Add(totalrow);


            rpnl_crosstabbing.Controls.Add(Repnew1);
            rpnl_crosstabbing.Visible = true; rpnl_crosstabbing_tbl.Visible = true;

            divCrosstab.Visible = true;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void ReportFunction1()
    {
        try
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + SelectedSurvey.SURVEY_NAME + ".xls");
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            DataRow[] drq1 =  ds_questions.Tables[0].Select("QUESTION_ID='" + tem1 + "'");
            DataRow[] drq2 = ds_questions.Tables[0].Select("QUESTION_ID='" + tem2 + "'");

            HtmlTable Repnew1 = new HtmlTable();
            Repnew1.ID = "crosstabsst";
            Repnew1.Width = "100%";
            Repnew1.Border = 1;
            Repnew1.CellPadding = 1;
            Repnew1.CellSpacing = 0;

          
            HtmlTableRow rowquestion = new HtmlTableRow();            
            HtmlTableCell rowquestioncell1 = new HtmlTableCell();
            rowquestioncell1.Width = "100%";

            rowquestion.ID = "r1";
            System.Web.UI.WebControls.Label rowquestionlbl = new System.Web.UI.WebControls.Label();
            System.Web.UI.WebControls.Label rowquestionlblspace = new System.Web.UI.WebControls.Label();
            if (drq1.Length > 0)
            {
                rowquestionlbl.Text = drq1[0].ItemArray[1].ToString();               
                rowquestionlbl.Font.Bold = true;
            }
            else
            {
                rowquestionlbl.Text = "";
                rowquestionlbl.Font.Bold = true;
            }


            HtmlTableCell rowquestion2cell1 = new HtmlTableCell();
            rowquestion2cell1.Width = "100%";

            System.Web.UI.WebControls.Label rowquestion2lbl = new System.Web.UI.WebControls.Label();

            if (drq2.Length > 0)
            {
                rowquestion2lbl.Text = drq2[0].ItemArray[1].ToString();
                rowquestion2lbl.Font.Bold = true;
            }
            else
            {
                rowquestion2lbl.Text = "";
                rowquestion2lbl.Font.Bold = true;
            }
            
            rowquestioncell1.Controls.Add(rowquestionlbl);
            rowquestioncell1.BgColor = "#b1c8e9";            
            rowquestion.Cells.Add(rowquestioncell1);

            rowquestion2cell1.Controls.Add(rowquestion2lbl);
            rowquestion2cell1.BgColor = "#b1c8e9";            
            rowquestion.Cells.Add(rowquestion2cell1);

            Repnew1.Rows.Add(rowquestion);


            HtmlTableRow colrownew1 = new HtmlTableRow();

            HtmlTableCell cellspace = new HtmlTableCell();

            System.Web.UI.WebControls.Label lblspace = new System.Web.UI.WebControls.Label();
            lblspace.Text = "";
            cellspace.Controls.Add(lblspace);
            colrownew1.Cells.Add(cellspace);

            DataSet dsansoptions = new DataSet("table");
            System.Data.DataTable mtxcolnames = createMtxColsDataTable();

            if (drq2[0].ItemArray[2].ToString() == "10" || drq2[0].ItemArray[2].ToString() == "11" )
            {
                DataSet dsansoptionscol = surcore.getMatrixAnsweroptionsQID(Convert.ToInt32(drq2[0].ItemArray[0].ToString()));
                
                for (int Tcoltxt = 0; Tcoltxt < dsansoptionscol.Tables[1].Rows.Count; Tcoltxt++)
                {
                    string strcategory = dsansoptionscol.Tables[1].Rows[Tcoltxt][3].ToString();
                    if (mtxcolnames.Rows.Count < dsansoptionscol.Tables[1].Rows.Count)
                    {
                        for (int Tcoltxt1 = 0; Tcoltxt1 < dsansoptionscol.Tables[1].Rows.Count; Tcoltxt1++)
                        {
                            if (strcategory == dsansoptionscol.Tables[1].Rows[Tcoltxt1][3].ToString())
                            {

                                AddMtxcolsDatatoTable(Convert.ToInt32(dsansoptionscol.Tables[1].Rows[Tcoltxt1][0].ToString()), dsansoptionscol.Tables[1].Rows[Tcoltxt1][1].ToString(), dsansoptionscol.Tables[1].Rows[Tcoltxt1][2].ToString(), dsansoptionscol.Tables[1].Rows[Tcoltxt1][3].ToString(), mtxcolnames);
                            }
                        }
                    }
                }

                System.Data.DataTable dt0 = dsansoptionscol.Tables[0].Copy();
                dsansoptions.Tables.Add(dt0);


                dsansoptions.Tables.Add(mtxcolnames);

            }
            else
            {
                dsansoptions = surcore.getAnsweroptionsQID(Convert.ToInt32(drq2[0].ItemArray[0].ToString()));

            }

            

             string createtotalcolumn = "";

             for (int rans = 0; rans < dsansoptions.Tables[1].Rows.Count; rans++)
             {
                 HtmlTableCell cellcolrownew1 = new HtmlTableCell();
                 cellcolrownew1.BgColor = "#b1c8e9";

                 System.Web.UI.WebControls.Label lblcolrownew1 = new System.Web.UI.WebControls.Label();                 
                 string strcolrownew = dsansoptions.Tables[1].Rows[rans][1].ToString().Replace("$--$","-");
                 lblcolrownew1.Text = strcolrownew;

                 cellcolrownew1.Controls.Add(lblcolrownew1);
                 colrownew1.Cells.Add(cellcolrownew1);

                 if (rans < dsansoptions.Tables[1].Rows.Count - 1)
                 {                     
                     createtotalcolumn = dsansoptions.Tables[1].Rows[rans + 1][1].ToString();                     
                     if (createtotalcolumn != dsansoptions.Tables[1].Rows[rans][1].ToString())
                     {
                         HtmlTableCell cellother = new HtmlTableCell();
                         cellother.BgColor = "#b1c8e9";

                         System.Web.UI.WebControls.Label lblother = new System.Web.UI.WebControls.Label();
                         lblother.Font.Bold = true;                         
                         lblother.Text = dsansoptions.Tables[1].Rows[rans][1].ToString().Replace("$--$","-") + " - " + "Total";
                         cellother.Controls.Add(lblother);
                         colrownew1.Cells.Add(cellother);
                     }
                 }
                 
             }

             HtmlTableCell cellotherfinal = new HtmlTableCell();
             cellotherfinal.BgColor = "#b1c8e9";
             System.Web.UI.WebControls.Label lblotherfinal = new System.Web.UI.WebControls.Label();
             lblotherfinal.Font.Bold = true;
             // lblotherfinal.Text = ds_reports.Tables[4].Rows[ds_reports.Tables[4].Rows.Count-1][3].ToString() + " - " + "Total";
             lblotherfinal.Text = dsansoptions.Tables[1].Rows[dsansoptions.Tables[1].Rows.Count - 1][1].ToString().Replace("$--$","-") + " - " + "Total";
             cellotherfinal.Controls.Add(lblotherfinal);
             colrownew1.Cells.Add(cellotherfinal);

             Repnew1.Rows.Add(colrownew1);


             DataSet dsansoptionsrows = surcore.getAnsweroptionsQID(Convert.ToInt32(drq1[0].ItemArray[0].ToString()));
             for (int q1rans = 0; q1rans < dsansoptionsrows.Tables[1].Rows.Count; q1rans++)
             {
                 HtmlTableRow colrownewq1 = new HtmlTableRow();
                 HtmlTableRow colrownewq1pcntg = new HtmlTableRow();
                 
                  HtmlTableCell cellrownewq1 = new HtmlTableCell();
                 cellrownewq1.BgColor = "#b1c8e9";
                 cellrownewq1.NoWrap = true;
                 System.Web.UI.WebControls.Label lblrownew1 = new System.Web.UI.WebControls.Label();
                 string strrownew = dsansoptionsrows.Tables[1].Rows[q1rans][1].ToString().Replace("$--$","-");
                 lblrownew1.Text = strrownew;

                 cellrownewq1.Controls.Add(lblrownew1);
                 colrownewq1.Cells.Add(cellrownewq1);

                 int rowansid1;
                 int rowansid2;
                
                 if (dsansoptionsrows.Tables[1].Rows[q1rans][0].ToString() == "999999999")
                 {
                     rowansid1 = 0;
                 }
                 else
                 {
                     rowansid1 = Convert.ToInt32(dsansoptionsrows.Tables[1].Rows[q1rans][0].ToString());
                 }


                 int indvcoltotal = 0;
                 int indvtotalp1=0;
                 for (int rans1 = 0; rans1 < dsansoptions.Tables[1].Rows.Count; rans1++)
                 {
                     int indvcoltotal1 = 0;
                     HtmlTableCell cellrownewq1d = new HtmlTableCell();
                     cellrownewq1d.BgColor = "#ffffff";
                     cellrownewq1d.NoWrap = true;
                     System.Web.UI.WebControls.Label lblrownew1d = new System.Web.UI.WebControls.Label();

                     if (dsansoptions.Tables[1].Rows[rans1][0].ToString() == "999999999")
                        {
                            rowansid2 = 0;
                        }
                     else
                        {
                            rowansid2 = Convert.ToInt32(dsansoptions.Tables[1].Rows[rans1][0].ToString());
                        }


                     DataSet dscrxcnt = obj_questions.getCrxTbValuesCnt(Convert.ToInt32(drq1[0].ItemArray[0].ToString()), Convert.ToInt32(drq2[0].ItemArray[0].ToString()),
                         rowansid1, rowansid2, SurveyID);

                     string strrownewd = dscrxcnt.Tables[0].Rows.Count.ToString();                    
                     lblrownew1d.Text = strrownewd;

                     cellrownewq1d.Controls.Add(lblrownew1d);
                     colrownewq1.Cells.Add(cellrownewq1d);

                     string createtotalcolumnd = "";

                     indvcoltotal1 = indvcoltotal1 + Convert.ToInt32(strrownewd);

                     if (rans1 < dsansoptions.Tables[1].Rows.Count - 1)
                     {
                         createtotalcolumnd = dsansoptions.Tables[1].Rows[rans1 + 1][1].ToString();
                         if (createtotalcolumnd != dsansoptions.Tables[1].Rows[rans1][1].ToString())
                         {
                             HtmlTableCell cellotherd = new HtmlTableCell();
                             cellotherd.BgColor = "#b1c8e9";

                             System.Web.UI.WebControls.Label lblotherd = new System.Web.UI.WebControls.Label();
                             lblotherd.Font.Bold = true;
                             lblotherd.Text = indvcoltotal1.ToString();
                             cellotherd.Controls.Add(lblotherd);
                             colrownewq1.Cells.Add(cellotherd);
                         }
                     }

                     indvcoltotal = indvcoltotal1;
                 }

                 HtmlTableCell cellotherfinald = new HtmlTableCell();
                 cellotherfinald.BgColor = "#b1c8e9";
                 System.Web.UI.WebControls.Label lblotherfinald = new System.Web.UI.WebControls.Label();
                 lblotherfinald.Font.Bold = true;
                 lblotherfinald.Text = indvcoltotal.ToString();
                 cellotherfinald.Controls.Add(lblotherfinald);
                 colrownewq1.Cells.Add(cellotherfinald);


                 HtmlTableCell cellrownewq1pcntg = new HtmlTableCell();
                 cellrownewq1pcntg.BgColor = "#ffffff";
                 cellrownewq1pcntg.NoWrap = true;
                 System.Web.UI.WebControls.Label lblrownew1pcntg = new System.Web.UI.WebControls.Label();                 
                 lblrownew1pcntg.Text = "";

                 cellrownewq1pcntg.Controls.Add(lblrownew1pcntg);
                 colrownewq1pcntg.Cells.Add(cellrownewq1pcntg);

                 int rowansid1totalp;
                 int rowansid2totalp;
                 
                 for (int rans1pcntg = 0; rans1pcntg < dsansoptions.Tables[1].Rows.Count; rans1pcntg++)
                 {
                     int sumtotalp = 0;
                     int indvtotalp = 0;
                     if (dsansoptions.Tables[1].Rows[rans1pcntg][0].ToString() == "999999999")
                     {
                         rowansid2totalp = 0;
                     }
                     else
                     {
                         rowansid2totalp = Convert.ToInt32(dsansoptions.Tables[1].Rows[rans1pcntg][0].ToString());
                     }
                     
                     for (int q1ransp = 0; q1ransp < dsansoptionsrows.Tables[1].Rows.Count; q1ransp++)
                     {
                         if (dsansoptionsrows.Tables[1].Rows[q1ransp][0].ToString() == "999999999")
                         {
                             rowansid1totalp = 0;
                         }
                         else
                         {
                             rowansid1totalp = Convert.ToInt32(dsansoptionsrows.Tables[1].Rows[q1ransp][0].ToString());
                         }
                       DataSet   dscrxcnttotalp = obj_questions.getCrxTbValuesCnt(Convert.ToInt32(drq1[0].ItemArray[0].ToString()), Convert.ToInt32(drq2[0].ItemArray[0].ToString()),
                       rowansid1totalp, rowansid2totalp, SurveyID);
                         sumtotalp = sumtotalp + dscrxcnttotalp.Tables[0].Rows.Count;
                         if (sumtotalp == 0)
                         {
                             sumtotalp = 1;
                         }
                     }


                   DataSet  dscrxcnttotalpd = obj_questions.getCrxTbValuesCnt(Convert.ToInt32(drq1[0].ItemArray[0].ToString()), Convert.ToInt32(drq2[0].ItemArray[0].ToString()),
                      rowansid1, rowansid2totalp, SurveyID);


                     HtmlTableCell cellrownewq1dp = new HtmlTableCell();
                     cellrownewq1dp.BgColor = "#ffffff";
                     cellrownewq1dp.NoWrap = true;
                     System.Web.UI.WebControls.Label lblrownew1dp = new System.Web.UI.WebControls.Label();

                 
                        string percentdisplay = Math.Round(Convert.ToDouble(dscrxcnttotalpd.Tables[0].Rows.Count * 100 / sumtotalp)).ToString();
                        indvtotalp = indvtotalp +  Convert.ToInt32(Math.Round(Convert.ToDouble(dscrxcnttotalpd.Tables[0].Rows.Count * 100 / sumtotalp)));
                        indvtotalp1 = indvtotalp;
                     string strrownewdp = percentdisplay+"%";
                     lblrownew1dp.Text = strrownewdp;

                     cellrownewq1dp.Controls.Add(lblrownew1dp);
                     colrownewq1pcntg.Cells.Add(cellrownewq1dp);

                     string createtotalcolumndp = "";

                     if (rans1pcntg < dsansoptions.Tables[1].Rows.Count - 1)
                     {
                         createtotalcolumndp = dsansoptions.Tables[1].Rows[rans1pcntg + 1][1].ToString();
                         if (createtotalcolumndp != dsansoptions.Tables[1].Rows[rans1pcntg][1].ToString())
                         {
                             HtmlTableCell cellotherdp = new HtmlTableCell();
                             cellotherdp.BgColor = "#b1c8e9";

                             System.Web.UI.WebControls.Label lblotherdp = new System.Web.UI.WebControls.Label();
                             lblotherdp.Font.Bold = true;
                             lblotherdp.Text = indvtotalp.ToString()+"%";
                             cellotherdp.Controls.Add(lblotherdp);
                             colrownewq1pcntg.Cells.Add(cellotherdp);
                         }
                     }


                 }
                 HtmlTableCell cellotherfinaldp = new HtmlTableCell();
                 cellotherfinaldp.BgColor = "#b1c8e9";
                 System.Web.UI.WebControls.Label lblotherfinaldp = new System.Web.UI.WebControls.Label();
                 lblotherfinaldp.Font.Bold = true;
                 lblotherfinaldp.Text = indvtotalp1.ToString()+"%";
                 cellotherfinaldp.Controls.Add(lblotherfinaldp);
                 colrownewq1pcntg.Cells.Add(cellotherfinaldp);




                 Repnew1.Rows.Add(colrownewq1);
                 Repnew1.Rows.Add(colrownewq1pcntg);
             }
            //display totals
             HtmlTableRow totalrow = new HtmlTableRow();
             HtmlTableCell totalcellcolrownew1 = new HtmlTableCell();
             totalcellcolrownew1.BgColor = "#b1c8e9";

             System.Web.UI.WebControls.Label lblothertotal = new System.Web.UI.WebControls.Label();
             lblothertotal.Font.Bold = true;
             lblothertotal.Text = "";
             totalcellcolrownew1.Controls.Add(lblothertotal);
             totalrow.Cells.Add(totalcellcolrownew1);

             int rowansid1total;
             int rowansid2total;
             int sumtotal1 = 0;
             for (int ranstotal = 0; ranstotal < dsansoptions.Tables[1].Rows.Count; ranstotal++)
             {
                 int sumtotal = 0; 

                 HtmlTableCell totalcolcell = new HtmlTableCell();
                 totalcolcell.BgColor = "#b1c8e9";

                 if (dsansoptions.Tables[1].Rows[ranstotal][0].ToString() == "999999999")
                 {
                     rowansid2total = 0;
                 }
                 else
                 {
                     rowansid2total = Convert.ToInt32(dsansoptions.Tables[1].Rows[ranstotal][0].ToString());
                 }
                 
                 for (int q1ranstotal = 0; q1ranstotal < dsansoptionsrows.Tables[1].Rows.Count; q1ranstotal++)
                 {
                     
                     if (dsansoptionsrows.Tables[1].Rows[q1ranstotal][0].ToString() == "999999999")
                     {
                         rowansid1total = 0;
                     }
                     else
                     {
                         rowansid1total = Convert.ToInt32(dsansoptionsrows.Tables[1].Rows[q1ranstotal][0].ToString());
                     }

                     DataSet dscrxcnttotal = obj_questions.getCrxTbValuesCnt(Convert.ToInt32(drq1[0].ItemArray[0].ToString()), Convert.ToInt32(drq2[0].ItemArray[0].ToString()),
                         rowansid1total, rowansid2total, SurveyID);


                     sumtotal = sumtotal + dscrxcnttotal.Tables[0].Rows.Count;
                     
                 }
                 
                     System.Web.UI.WebControls.Label lblcolrownew1total = new System.Web.UI.WebControls.Label();
                     string strcolrownewtotal = sumtotal.ToString();
                     lblcolrownew1total.Text = strcolrownewtotal;
                 
                 totalcolcell.Controls.Add(lblcolrownew1total);
                 totalrow.Cells.Add(totalcolcell);

                 string totalcolumn1 = "";

                 if (ranstotal < dsansoptions.Tables[1].Rows.Count - 1)
                 {
                     totalcolumn1 = dsansoptions.Tables[1].Rows[ranstotal + 1][1].ToString();
                     if (totalcolumn1 != dsansoptions.Tables[1].Rows[ranstotal][1].ToString())
                     {
                         HtmlTableCell cellothertotal = new HtmlTableCell();
                         cellothertotal.BgColor = "#b1c8e9";

                         System.Web.UI.WebControls.Label lblothertotal1 = new System.Web.UI.WebControls.Label();
                         lblothertotal1.Font.Bold = true;
                         lblothertotal1.Text = sumtotal.ToString();
                         cellothertotal.Controls.Add(lblothertotal1);
                         totalrow.Cells.Add(cellothertotal);
                     }
                 }
                 sumtotal1 = sumtotal;
             }

             HtmlTableCell cellotherfinaltotal = new HtmlTableCell();
             cellotherfinaltotal.BgColor = "#b1c8e9";
             System.Web.UI.WebControls.Label lblotherfinaltotal = new System.Web.UI.WebControls.Label();
             lblotherfinaltotal.Font.Bold = true;    
             lblotherfinaltotal.Text = sumtotal1.ToString();
             cellotherfinaltotal.Controls.Add(lblotherfinaltotal);
             totalrow.Cells.Add(cellotherfinaltotal);


             Repnew1.Rows.Add(totalrow);


            rpnl_crosstabbing.Controls.Add(Repnew1);

            Repnew1.RenderControl(hw);

            HttpContext.Current.Response.Write(sw.ToString());
            HttpContext.Current.Response.End();


            rpnl_crosstabbing.Visible = true; rpnl_crosstabbing_tbl.Visible = true;

            divCrosstab.Visible = true;



        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void report_function()
    {
        try
        {
            if (SelectedSurvey != null && SelectedSurvey.SURVEY_ID > 0)
            {
                ds_questions = obj_questions.getsurveyquestions(SurveyBasicInfoView.SURVEY_ID);
            }
            if (ds_questions != null && ds_questions.Tables.Count > 0)
            {
                for (int l = 0; l < ds_questions.Tables[0].Rows.Count; l++)
                {
                    if (tem1 == Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[0].ToString()))
                    {
                        ques_array[0] = ds_questions.Tables[0].Rows[l].ItemArray[1].ToString();
                        ques_array[0] = ques_array[0].Replace("<p>", "");
                        ques_array[0] = ques_array[0].Replace("</p>", "");
                        rowquestionid = Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[0].ToString());

                        if (Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[3].ToString()) == 1)
                            ques_type_flag1 = 1;
                    }
                    if (tem2 == Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[0].ToString()))
                    {
                        ques_array[1] = ds_questions.Tables[0].Rows[l].ItemArray[1].ToString();
                        ques_array[1] = ques_array[1].Replace("<p>", "");
                        ques_array[1] = ques_array[1].Replace("</p>", "");
                        colquestionid = Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[0].ToString());
                        colquestiontype = Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[2].ToString());
                        if (Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[3].ToString()) == 1)
                            ques_type_flag2 = 1;
                    }
                }
            }
            if (tem1 != null && tem2 != null)
            {

                if (SurveyBasicInfoView != null && SurveyBasicInfoView.SURVEY_ID > 0)
                {
                    ds_reports = obj_questions.getsurveyanswers(Convert.ToInt32(tem1), Convert.ToInt32(tem2), SurveyBasicInfoView.SURVEY_ID);
                }
            }

            if (ques_type_flag1 == 1)
            {
                if (ds_reports != null && ds_reports.Tables.Count > 0)
                {
                    int rows_count = ds_reports.Tables[0].Rows.Count;
                    ds_reports.Tables[0].Rows.Add();
                    ds_reports.Tables[0].Rows[rows_count]["ANSWER_ID"] = 0;
                    ds_reports.Tables[0].Rows[rows_count]["ANSWER_OPTIONS"] = "Others";
                }

            }
            if (ques_type_flag2 == 1)
            {
                if (ds_reports != null && ds_reports.Tables.Count > 0)
                {
                    int rows_count = ds_reports.Tables[1].Rows.Count;
                    ds_reports.Tables[1].Rows.Add();
                    ds_reports.Tables[1].Rows[rows_count]["ANSWER_ID"] = 0;
                    ds_reports.Tables[1].Rows[rows_count]["ANSWER_OPTIONS"] = "Others";
                }

            }


            int count1 = 0;

            ques1_anscount = ds_reports.Tables[0].Rows.Count;
            ques2_anscount = ds_reports.Tables[1].Rows.Count;
            arr1 = new int[ques1_anscount];
            total_row_percent = new string[ques1_anscount];
            tot_row_percent = new Double[ques1_anscount]; ;
            int[] arr2 = new int[ques2_anscount];
            arr3 = new int[ques1_anscount * ques2_anscount];
            percent = new string[ques1_anscount * ques2_anscount];
            total_count = new int[ques2_anscount];
            int respondents_count = ds_reports.Tables[2].Rows.Count;
            int[] respondents = new int[respondents_count];
            int count3 = 0;

            DataTable colgroupoptions = createDataTable();

            for (int Tcoltxt = 0; Tcoltxt < ds_reports.Tables[4].Rows.Count; Tcoltxt++)
            {
                string strcategory = ds_reports.Tables[4].Rows[Tcoltxt][3].ToString();
                if (colgroupoptions.Rows.Count < ds_reports.Tables[4].Rows.Count)
                {
                    for (int Tcoltxt1 = 0; Tcoltxt1 < ds_reports.Tables[4].Rows.Count; Tcoltxt1++)
                    {
                        if (strcategory == ds_reports.Tables[4].Rows[Tcoltxt1][3].ToString())
                        {
                            AddDatatoTable(Convert.ToInt32(ds_reports.Tables[4].Rows[Tcoltxt1][0].ToString()), ds_reports.Tables[4].Rows[Tcoltxt1][1].ToString(), ds_reports.Tables[4].Rows[Tcoltxt1][2].ToString(), ds_reports.Tables[4].Rows[Tcoltxt1][3].ToString(), colgroupoptions);
                        }
                    }
                }
            }



            if (ds_reports != null && ds_reports.Tables.Count > 0)
            {
                int x = 0;
                int y = 0;

                //code optimization done by satya

                //creating a html table
                HtmlTable Repnew1 = new HtmlTable();
                Repnew1.ID = "crosstabsst";
                Repnew1.Width = "100%";
                Repnew1.Border = 1;
                Repnew1.CellPadding = 1;
                Repnew1.CellSpacing = 0;

                //row to display the row question
                HtmlTableRow rowquestion1 = new HtmlTableRow();

                //row1.BgColor = "#71A2E7";
                HtmlTableCell rowquestioncell1 = new HtmlTableCell();
                rowquestioncell1.Width = "100%";


                rowquestion1.ID = "r1";
                System.Web.UI.WebControls.Label rowquestionlbl = new System.Web.UI.WebControls.Label();
                System.Web.UI.WebControls.Label rowquestionlblspace = new System.Web.UI.WebControls.Label();
                if (ques_array != null && ques_array[0] != null)
                {
                    rowquestionlbl.Text = ques_array[0].ToString();
                    //rowquestionlbl.CssClass = "ques";
                    rowquestionlbl.Font.Bold = true;
                }


                HtmlTableCell rowquestion2cell1 = new HtmlTableCell();
                rowquestion2cell1.Width = "100%";

                System.Web.UI.WebControls.Label rowquestion2lbl = new System.Web.UI.WebControls.Label();

                if (ques_array != null && ques_array[1] != null)
                {
                    rowquestion2lbl.Text = ques_array[1].ToString();
                    //rowquestionlbl.CssClass = "ques";
                    rowquestion2lbl.Font.Bold = true;
                }



                rowquestioncell1.Controls.Add(rowquestionlbl);
                rowquestioncell1.BgColor = "#b1c8e9";
                //rowquestioncell1.NoWrap = true;
                rowquestion1.Cells.Add(rowquestioncell1);


                rowquestion2cell1.Controls.Add(rowquestion2lbl);
                rowquestion2cell1.BgColor = "#b1c8e9";
                //rowquestioncell1.NoWrap = true;
                rowquestion1.Cells.Add(rowquestion2cell1);

                Repnew1.Rows.Add(rowquestion1);


                //row to display the columns of selected question in column options

                HtmlTableRow colrownew1 = new HtmlTableRow();

                HtmlTableCell cellspace = new HtmlTableCell();

                System.Web.UI.WebControls.Label lblspace = new System.Web.UI.WebControls.Label();
                lblspace.Text = "";
                cellspace.Controls.Add(lblspace);
                colrownew1.Cells.Add(cellspace);


                string createtotalcolumn = "";

                //displays the answeriptions of columns option question to display the column headings

                // for (int coltxt = 0; coltxt < ds_reports.Tables[4].Rows.Count; coltxt++)
                for (int coltxt = 0; coltxt < colgroupoptions.Rows.Count; coltxt++)
                {
                    HtmlTableCell cellcolrownew1 = new HtmlTableCell();
                    cellcolrownew1.BgColor = "#b1c8e9";


                    System.Web.UI.WebControls.Label lblcolrownew1 = new System.Web.UI.WebControls.Label();
                    // string strcolrownew = ds_reports.Tables[4].Rows[coltxt][2].ToString();
                    string strcolrownew = colgroupoptions.Rows[coltxt][2].ToString();
                    lblcolrownew1.Text = strcolrownew;

                    cellcolrownew1.Controls.Add(lblcolrownew1);
                    colrownew1.Cells.Add(cellcolrownew1);

                    //  if (coltxt < ds_reports.Tables[4].Rows.Count-1)
                    if (coltxt < colgroupoptions.Rows.Count - 1)
                    {
                        // createtotalcolumn = ds_reports.Tables[4].Rows[coltxt+1][3].ToString();
                        createtotalcolumn = colgroupoptions.Rows[coltxt + 1][3].ToString();
                        // if (createtotalcolumn != ds_reports.Tables[4].Rows[coltxt][3].ToString())
                        if (createtotalcolumn != colgroupoptions.Rows[coltxt][3].ToString())
                        {
                            HtmlTableCell cellother = new HtmlTableCell();
                            cellother.BgColor = "#b1c8e9";

                            System.Web.UI.WebControls.Label lblother = new System.Web.UI.WebControls.Label();
                            lblother.Font.Bold = true;
                            //lblother.Text = ds_reports.Tables[4].Rows[coltxt][3].ToString() + " - " + "Total";
                            lblother.Text = colgroupoptions.Rows[coltxt][3].ToString() + " - " + "Total";
                            cellother.Controls.Add(lblother);
                            colrownew1.Cells.Add(cellother);
                        }
                    }
                }



                HtmlTableCell cellotherfinal = new HtmlTableCell();
                cellotherfinal.BgColor = "#b1c8e9";
                System.Web.UI.WebControls.Label lblotherfinal = new System.Web.UI.WebControls.Label();
                lblotherfinal.Font.Bold = true;
                // lblotherfinal.Text = ds_reports.Tables[4].Rows[ds_reports.Tables[4].Rows.Count-1][3].ToString() + " - " + "Total";
                lblotherfinal.Text = colgroupoptions.Rows[colgroupoptions.Rows.Count - 1][3].ToString() + " - " + "Total";
                cellotherfinal.Controls.Add(lblotherfinal);
                colrownew1.Cells.Add(cellotherfinal);

                Repnew1.Rows.Add(colrownew1);

                //end of displaying the columns headings


                //to display the rows and count value in the cells based on answerids
                //row selected questionnloop
                for (int rowtxt = 0; rowtxt < ds_reports.Tables[0].Rows.Count; rowtxt++)
                {
                    HtmlTableRow rownew1 = new HtmlTableRow();
                    HtmlTableRow perrow1 = new HtmlTableRow();
                    //displaying the answeroption of row selected question
                    HtmlTableCell cellrownew1 = new HtmlTableCell();
                    cellrownew1.BgColor = "#b1c8e9";
                    cellrownew1.NoWrap = true;
                    System.Web.UI.WebControls.Label lblrownew1 = new System.Web.UI.WebControls.Label();
                    string strrownew = ds_reports.Tables[0].Rows[rowtxt]["ANSWER_OPTIONS"].ToString();
                    lblrownew1.Text = strrownew;

                    cellrownew1.Controls.Add(lblrownew1);
                    rownew1.Cells.Add(cellrownew1);

                    HtmlTableCell percell = new HtmlTableCell();
                    System.Web.UI.WebControls.Label lblpercntg = new System.Web.UI.WebControls.Label();
                    //percentage row
                    lblpercntg.Text = "";
                    percell.Controls.Add(lblpercntg);
                    perrow1.Cells.Add(percell);

                    int finalHTSumgrouptotalcnt1 = 0;
                    int sumtotalrowcount = 0;
                    int sumgrouptotalrowcount = 0;

                    int HTSumgrouptotalcnt1 = 0;

                    //column selected question loop colgroupoptions
                    //for (int coltxt = 0; coltxt < ds_reports.Tables[4].Rows.Count; coltxt++)
                    for (int coltxt = 0; coltxt < colgroupoptions.Rows.Count; coltxt++)
                    {

                        HtmlTableCell cellnew1 = new HtmlTableCell();
                        cellnew1.Align = "Center";

                        System.Web.UI.WebControls.Label lblnew1 = new System.Web.UI.WebControls.Label();

                        //gets the count of respondents by passing rowanserid and columnanswerid
                        //ds_rowcolcount = obj_questions.getRespondentscount(Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()),
                        //    Convert.ToInt32(ds_reports.Tables[4].Rows[coltxt]["ANSWER_ID"].ToString()),SurveyBasicInfoView.SURVEY_ID);

                        ds_rowcolcount = obj_questions.getRespondentscount(Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()),
                          Convert.ToInt32(colgroupoptions.Rows[coltxt]["ANSWER_ID"].ToString()), SurveyBasicInfoView.SURVEY_ID);



                        string strcntval;
                        if (ds_rowcolcount.Tables[0].Rows.Count > 0)
                        {

                            strcntval = ds_rowcolcount.Tables[0].Rows[0][2].ToString();
                            lblnew1.Text = strcntval;
                        }
                        else
                        {
                            strcntval = "0";
                            lblnew1.Text = strcntval;
                        }




                        sumtotalrowcount = sumtotalrowcount + Convert.ToInt32(strcntval);


                        //displays the individual count in cell                   

                        cellnew1.Controls.Add(lblnew1);
                        rownew1.Cells.Add(cellnew1);


                        int HTsumtotalrowcountprcntg = 0;



                        for (int rowtxtprcntg = 0; rowtxtprcntg < ds_reports.Tables[0].Rows.Count; rowtxtprcntg++)
                        {


                            ds_rowcolcount = obj_questions.getRespondentscount(Convert.ToInt32(colgroupoptions.Rows[coltxt]["ANSWER_ID"].ToString()),
                                Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxtprcntg]["ANSWER_ID"].ToString()), SurveyBasicInfoView.SURVEY_ID);


                            string HTstrcntvalprcntg;
                            if (ds_rowcolcount.Tables[0].Rows.Count > 0)
                            {

                                HTstrcntvalprcntg = ds_rowcolcount.Tables[0].Rows[0][2].ToString();

                            }
                            else
                            {
                                HTstrcntvalprcntg = "0";

                            }


                            HTsumtotalrowcountprcntg = HTsumtotalrowcountprcntg + Convert.ToInt32(HTstrcntvalprcntg);

                        }


                        HTSumgrouptotalcnt1 = HTSumgrouptotalcnt1 + HTsumtotalrowcountprcntg;

                        finalHTSumgrouptotalcnt1 = HTSumgrouptotalcnt1;

                        HtmlTableCell percell1 = new HtmlTableCell();
                        percell1.BgColor = "LightGrey";
                        percell1.Align = "Center";
                        System.Web.UI.WebControls.Label lblpercntg1 = new System.Web.UI.WebControls.Label();


                        if (colquestiontype == 11)
                        {
                            ds_rowcolcountmultipleselect = obj_questions.getRespondentscountmultipleselect(Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()),
                                colgroupoptions.Rows[coltxt][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);


                            HTsumtotalrowcountprcntg = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

                        }

                        Double z = Convert.ToDouble(Convert.ToInt32(strcntval) * 100);
                        z = Convert.ToDouble(z / HTsumtotalrowcountprcntg);
                        string percentdisplay = Math.Round(Convert.ToDouble(z), 2).ToString();

                        lblpercntg1.Text = percentdisplay + "%";
                        percell1.Controls.Add(lblpercntg1);
                        perrow1.Cells.Add(percell1);





                        //sum of the independent attributes 
                        // if (coltxt < ds_reports.Tables[4].Rows.Count - 1)
                        if (coltxt < colgroupoptions.Rows.Count - 1)
                        {
                            //createtotalcolumn = ds_reports.Tables[4].Rows[coltxt + 1][3].ToString();
                            createtotalcolumn = colgroupoptions.Rows[coltxt + 1][3].ToString();
                            // if (createtotalcolumn != ds_reports.Tables[4].Rows[coltxt][3].ToString())
                            if (createtotalcolumn != colgroupoptions.Rows[coltxt][3].ToString())
                            {


                                if (colquestiontype == 11)
                                {
                                    ds_rowcolcountmultipleselect = obj_questions.getRespondentscountmultipleselect(Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()),
                                        colgroupoptions.Rows[coltxt][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);


                                    sumtotalrowcount = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

                                }


                                HtmlTableCell cellrowtotal = new HtmlTableCell();
                                cellrowtotal.Align = "Center";
                                //  cellrowtotal.BgColor = "lightgreen";                            
                                System.Web.UI.WebControls.Label lblrowtotal = new System.Web.UI.WebControls.Label();
                                lblrowtotal.Font.Bold = true;
                                sumgrouptotalrowcount = sumtotalrowcount;
                                lblrowtotal.Text = sumgrouptotalrowcount.ToString();
                                cellrowtotal.Controls.Add(lblrowtotal);
                                rownew1.Cells.Add(cellrowtotal);


                                HtmlTableCell percell2 = new HtmlTableCell();
                                percell2.Align = "Center";
                                percell2.BgColor = "LightGrey";

                                System.Web.UI.WebControls.Label lblpercntg2 = new System.Web.UI.WebControls.Label();

                                if (colquestiontype == 11)
                                {
                                    ds_rowcolcountmultipleselect = obj_questions.getRespondentsrowcountmultipleselect(rowquestionid,
                                        colgroupoptions.Rows[coltxt][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);


                                    HTSumgrouptotalcnt1 = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

                                }


                                Double z1 = Convert.ToDouble(sumgrouptotalrowcount * 100);
                                z1 = Convert.ToDouble(z1 / HTSumgrouptotalcnt1);
                                string percentdisplaytotal = Math.Round(Convert.ToDouble(z1), 2).ToString();

                                lblpercntg2.Text = percentdisplaytotal + "%";
                                lblpercntg2.Font.Bold = true;
                                percell2.Controls.Add(lblpercntg2);
                                perrow1.Cells.Add(percell2);


                                HTSumgrouptotalcnt1 = 0;
                                sumgrouptotalrowcount = 0;
                                sumtotalrowcount = 0;
                            }
                        }




                    }


                    //last column sum
                    HtmlTableCell cellothernew1 = new HtmlTableCell();
                    cellothernew1.Align = "Center";
                    //cellothernew1.BgColor = "lightgreen";

                    if (colquestiontype == 11)
                    {
                        ds_rowcolcountmultipleselect = obj_questions.getRespondentscountmultipleselect(Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()),
                            colgroupoptions.Rows[colgroupoptions.Rows.Count - 1][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);
                        sumtotalrowcount = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;
                    }

                    System.Web.UI.WebControls.Label lbltotalrowsum = new System.Web.UI.WebControls.Label();
                    lbltotalrowsum.Font.Bold = true;
                    lbltotalrowsum.Text = sumtotalrowcount.ToString();
                    cellothernew1.Controls.Add(lbltotalrowsum);
                    rownew1.Cells.Add(cellothernew1);


                    HtmlTableCell percell3 = new HtmlTableCell();
                    percell3.Align = "Center";
                    percell3.BgColor = "LightGrey";
                    System.Web.UI.WebControls.Label lblpercntg3 = new System.Web.UI.WebControls.Label();
                    lblpercntg3.Font.Bold = true;


                    if (colquestiontype == 11)
                    {
                        ds_rowcolcountmultipleselect = obj_questions.getRespondentsrowcountmultipleselect(rowquestionid,
                            colgroupoptions.Rows[colgroupoptions.Rows.Count - 1][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);

                        finalHTSumgrouptotalcnt1 = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

                    }
                    Double z2 = Convert.ToDouble(sumtotalrowcount * 100);
                    z2 = Convert.ToDouble(z2 / finalHTSumgrouptotalcnt1);
                    string percentdisplaytotal1 = Math.Round(Convert.ToDouble(z2), 2).ToString();

                    lblpercntg3.Text = percentdisplaytotal1 + "%";
                    percell3.Controls.Add(lblpercntg3);
                    perrow1.Cells.Add(percell3);

                    Repnew1.Rows.Add(rownew1);

                    Repnew1.Rows.Add(perrow1);




                }

                //getting column sums
                HtmlTableRow HTRsumofcolumns = new HtmlTableRow();
                HtmlTableCell HTCsumofcolumns = new HtmlTableCell();
                HTCsumofcolumns.Align = "Center";
                HTCsumofcolumns.BgColor = "#b1c8e9";
                //"#b1c8e9";

                System.Web.UI.WebControls.Label HTClblsumofcols = new System.Web.UI.WebControls.Label();
                HTClblsumofcols.Text = "";
                HTCsumofcolumns.Controls.Add(HTClblsumofcols);
                HTRsumofcolumns.Cells.Add(HTCsumofcolumns);

                int HTsumtotalrowcount = 0;
                int HTSumgrouptotalcnt = 0;
                //  for (int coltxt = 0; coltxt < ds_reports.Tables[4].Rows.Count; coltxt++)
                for (int coltxt = 0; coltxt < colgroupoptions.Rows.Count; coltxt++)
                {
                    for (int rowtxt = 0; rowtxt < ds_reports.Tables[0].Rows.Count; rowtxt++)
                    {

                        System.Web.UI.WebControls.Label HTlblnew1 = new System.Web.UI.WebControls.Label();

                        //ds_rowcolcount = obj_questions.getRespondentscount(Convert.ToInt32(ds_reports.Tables[4].Rows[coltxt]["ANSWER_ID"].ToString()),
                        //    Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()), SurveyBasicInfoView.SURVEY_ID);
                        ds_rowcolcount = obj_questions.getRespondentscount(Convert.ToInt32(colgroupoptions.Rows[coltxt]["ANSWER_ID"].ToString()),
                            Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()), SurveyBasicInfoView.SURVEY_ID);



                        string HTstrcntval;
                        if (ds_rowcolcount.Tables[0].Rows.Count > 0)
                        {

                            HTstrcntval = ds_rowcolcount.Tables[0].Rows[0][2].ToString();
                            HTlblnew1.Text = HTstrcntval;
                        }
                        else
                        {
                            HTstrcntval = "0";
                            HTlblnew1.Text = HTstrcntval;
                        }



                        HTsumtotalrowcount = HTsumtotalrowcount + Convert.ToInt32(HTstrcntval);

                    }



                    HtmlTableCell HTCcellrowtotal1 = new HtmlTableCell();
                    HTCcellrowtotal1.Align = "Center";
                    HTCcellrowtotal1.BgColor = "#b1c8e9";
                    System.Web.UI.WebControls.Label HTClblrowtotal1 = new System.Web.UI.WebControls.Label();
                    HTClblrowtotal1.Text = HTsumtotalrowcount.ToString();
                    HTCcellrowtotal1.Controls.Add(HTClblrowtotal1);
                    HTRsumofcolumns.Cells.Add(HTCcellrowtotal1);

                    HTSumgrouptotalcnt = HTSumgrouptotalcnt + HTsumtotalrowcount;

                    HTsumtotalrowcount = 0;

                    // if (coltxt < ds_reports.Tables[4].Rows.Count - 1)
                    if (coltxt < colgroupoptions.Rows.Count - 1)
                    {
                        // createtotalcolumn = ds_reports.Tables[4].Rows[coltxt + 1][3].ToString();
                        createtotalcolumn = colgroupoptions.Rows[coltxt + 1][3].ToString();

                        // if (createtotalcolumn != ds_reports.Tables[4].Rows[coltxt][3].ToString())
                        if (createtotalcolumn != colgroupoptions.Rows[coltxt][3].ToString())
                        {


                            if (colquestiontype == 11)
                            {
                                ds_rowcolcountmultipleselect = obj_questions.getRespondentsrowcountmultipleselect(rowquestionid,
                                    colgroupoptions.Rows[coltxt][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);


                                HTSumgrouptotalcnt = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

                            }


                            HtmlTableCell HTCcellrowtotal2 = new HtmlTableCell();
                            HTCcellrowtotal2.Align = "Center";
                            HTCcellrowtotal2.BgColor = "Red";
                            //#b1c8e9
                            System.Web.UI.WebControls.Label HTClblrowtotal2 = new System.Web.UI.WebControls.Label();
                            HTClblrowtotal2.Font.Bold = true;
                            HTClblrowtotal2.Text = HTSumgrouptotalcnt.ToString();
                            HTCcellrowtotal2.Controls.Add(HTClblrowtotal2);
                            HTRsumofcolumns.Cells.Add(HTCcellrowtotal2);
                            HTSumgrouptotalcnt = 0;
                        }

                    }
                }


                if (colquestiontype == 11)
                {
                    ds_rowcolcountmultipleselect = obj_questions.getRespondentsrowcountmultipleselect(rowquestionid,
                        colgroupoptions.Rows[colgroupoptions.Rows.Count - 1][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);


                    HTSumgrouptotalcnt = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

                }

                HtmlTableCell HTCcellrowtotal3 = new HtmlTableCell();
                HTCcellrowtotal3.Align = "Center";
                HTCcellrowtotal3.BgColor = "Red";
                //#b1c8e9
                System.Web.UI.WebControls.Label HTClblrowtotal3 = new System.Web.UI.WebControls.Label();
                HTClblrowtotal3.Font.Bold = true;
                HTClblrowtotal3.Text = HTSumgrouptotalcnt.ToString();
                HTCcellrowtotal3.Controls.Add(HTClblrowtotal3);
                HTRsumofcolumns.Cells.Add(HTCcellrowtotal3);


                Repnew1.Rows.Add(HTRsumofcolumns);

                //end of sum rows

                rpnl_crosstabbing.Controls.Add(Repnew1);
                rpnl_crosstabbing.Visible = true; rpnl_crosstabbing_tbl.Visible = true;

                divCrosstab.Visible = true;






                //end of code optimization done by satya



            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
       
       

    }
    protected void lbn_addnewcrosstab_Click(object sender, EventArgs e)
    {
        if (SurveyBasicInfoView != null && SurveyBasicInfoView.SURVEY_ID > 0)
        {
            string urlstr = EncryptHelper.EncryptQuerystring("CrossTabReports.aspx", Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(urlstr);
        }
    }
    void CrossTabSavedReports()
    {

        pnl_savedcreports.Visible = true;
        pnl_savedcreports.Controls.Clear();
        HtmlTable ctable = new HtmlTable();
        ctable.ID = "ctablink";
        if (ds_cross_saved_reports != null && ds_cross_saved_reports.Tables.Count > 0)
        {
            for (int i = 0; i < ds_cross_saved_reports.Tables[0].Rows.Count; i++)
            {
                HtmlTableRow crow = new HtmlTableRow();
                HtmlTableCell ccell = new HtmlTableCell();
                ccell.Align = "Center";
                ccell.Width = "200";
                HyperLink hyp = new HyperLink();
                hyp.ID = "hyper" + ds_cross_saved_reports.Tables[0].Rows[i].ItemArray[0].ToString();
                hyp.Text = ds_cross_saved_reports.Tables[0].Rows[i].ItemArray[5].ToString();
                if (SelectedSurvey != null && SelectedSurvey.SURVEY_ID > 0 && SelectedSurvey.USER_ID > 0 && ds_cross_saved_reports.Tables[0].Rows[i].ItemArray[0] != null)
                {
                    string urlstr = EncryptHelper.EncryptQuerystring("CrossTabReports.aspx", "crep_id=" + Convert.ToInt32(ds_cross_saved_reports.Tables[0].Rows[i].ItemArray[0].ToString()) + "&" + Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID + "&SurUserId=" + SurveyBasicInfoView.USERID + "&surveyFlag=" + SurveyFlag);
                    hyp.NavigateUrl = "~/" + urlstr;
                }
                ccell.Controls.Add(hyp);
                ccell.Align = "left";
                crow.Controls.Add(ccell);
                ctable.Rows.Add(crow);
                ctable.Align = "Left";
            }
        }
        pnl_savedcreports.Controls.Add(ctable);
    }

    protected void btnexporttoexcel_Click(object sender, EventArgs e)
    {

        IterateControls1(this);
        if (status_flag == 3)
        {
            if (idarray != null)
            {
                if ((idarray[0].Substring(0, 9) == idarray[1].Substring(0, 9)) || (idarray[0].Substring(9) == idarray[1].Substring(9)))
                    status_flag = 1;
            }
        }
        if (status_flag == 1)
        {
            lbl_message.Text = "Please select only one column and only one row -but of different questions.";
            lbl_message.Visible = true;
            rpnl_crosstabbing.Visible = false; rpnl_crosstabbing_tbl.Visible = false;
            divCrosstab.Visible = false;
            btnsavecrosstab.Visible = false;
            txt_crosstabreportname.Visible = false;
            btnexporttoexcel.Visible = false;
        }
        else if (status_flag == 2)
        {
            lbl_message.Text = "Please select only one column and only one row -but of different questions.";
            lbl_message.Visible = true;
            rpnl_crosstabbing.Visible = false; rpnl_crosstabbing_tbl.Visible = false;
            divCrosstab.Visible = false;
            btnsavecrosstab.Visible = false;
            txt_crosstabreportname.Visible = false;
            btnexporttoexcel.Visible = false;
        }
        else if (status_flag == 4)
        {
            lbl_message.Text = "Please select only one column and only one row -but of different questions.";
            lbl_message.Visible = true;
            rpnl_crosstabbing.Visible = false; rpnl_crosstabbing_tbl.Visible = false;
            divCrosstab.Visible = false;
            btnsavecrosstab.Visible = false;
            txt_crosstabreportname.Visible = false;
            btnexporttoexcel.Visible = false;
        }
        else if (status_flag == 3)
        {
            lbl_message.Visible = false;
            rpnl_crosstabbing.Visible = true; rpnl_crosstabbing_tbl.Visible = true;
            int count = ds_questions.Tables[0].Rows.Count;
            str1 = "";
            IterateControls(this);

            string temp1 = "";
            string temp2 = "";
            if (idarray != null)
            {
                temp1 = idarray[0];
                temp2 = idarray[1];
                tem1 = Convert.ToInt32(idarray[0].Substring(9));
                tem2 = Convert.ToInt32(idarray[1].Substring(9));
            }
        }

        ReportFunction1();

       // if (SelectedSurvey != null && SelectedSurvey.SURVEY_ID > 0)
       // {
       //     ds_questions = obj_questions.getsurveyquestions(SurveyBasicInfoView.SURVEY_ID);
       // }
       // if (ds_questions != null && ds_questions.Tables.Count > 0)
       // {
       //     for (int l = 0; l < ds_questions.Tables[0].Rows.Count; l++)
       //     {
       //         if (tem1 == Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[0].ToString()))
       //         {
       //             ques_array[0] = ds_questions.Tables[0].Rows[l].ItemArray[1].ToString();
       //             ques_array[0] = ques_array[0].Replace("<p>", "");
       //             ques_array[0] = ques_array[0].Replace("</p>", "");
       //             rowquestionid = Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[0].ToString());
       //             if (Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[3].ToString()) == 1)
       //                 ques_type_flag1 = 1;
       //         }
       //         if (tem2 == Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[0].ToString()))
       //         {
       //             ques_array[1] = ds_questions.Tables[0].Rows[l].ItemArray[1].ToString();
       //             ques_array[1] = ques_array[1].Replace("<p>", "");
       //             ques_array[1] = ques_array[1].Replace("</p>", "");
       //             colquestionid = Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[0].ToString());
       //             colquestiontype = Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[2].ToString());
       //             if (Convert.ToInt32(ds_questions.Tables[0].Rows[l].ItemArray[3].ToString()) == 1)
       //                 ques_type_flag2 = 1;
       //         }
       //     }
       // }
       // if (tem1 != null && tem2 != null)
       // {

       //     if (SurveyBasicInfoView != null && SurveyBasicInfoView.SURVEY_ID > 0)
       //     {
       //         ds_reports = obj_questions.getsurveyanswers(Convert.ToInt32(tem1), Convert.ToInt32(tem2), SurveyBasicInfoView.SURVEY_ID);
       //     }
       // }

       // if (ques_type_flag1 == 1)
       // {
       //     if (ds_reports != null && ds_reports.Tables.Count > 0)
       //     {
       //         int rows_count = ds_reports.Tables[0].Rows.Count;
       //         ds_reports.Tables[0].Rows.Add();
       //         ds_reports.Tables[0].Rows[rows_count]["ANSWER_ID"] = 0;
       //         ds_reports.Tables[0].Rows[rows_count]["ANSWER_OPTIONS"] = "Others";
       //     }

       // }
       // if (ques_type_flag2 == 1)
       // {
       //     if (ds_reports != null && ds_reports.Tables.Count > 0)
       //     {
       //         int rows_count = ds_reports.Tables[1].Rows.Count;
       //         ds_reports.Tables[1].Rows.Add();
       //         ds_reports.Tables[1].Rows[rows_count]["ANSWER_ID"] = 0;
       //         ds_reports.Tables[1].Rows[rows_count]["ANSWER_OPTIONS"] = "Others";
       //     }

       // }


       // int count1 = 0;

       // ques1_anscount = ds_reports.Tables[0].Rows.Count;
       // ques2_anscount = ds_reports.Tables[1].Rows.Count;
       // arr1 = new int[ques1_anscount];
       // total_row_percent = new string[ques1_anscount];
       // tot_row_percent = new Double[ques1_anscount]; ;
       // int[] arr2 = new int[ques2_anscount];
       // arr3 = new int[ques1_anscount * ques2_anscount];
       // percent = new string[ques1_anscount * ques2_anscount];
       // total_count = new int[ques2_anscount];
       // int respondents_count = ds_reports.Tables[2].Rows.Count;
       // int[] respondents = new int[respondents_count];
       // int count3 = 0;

       // DataTable colgroupoptions = createDataTable();

       // for (int Tcoltxt = 0; Tcoltxt < ds_reports.Tables[4].Rows.Count; Tcoltxt++)
       // {
       //     string strcategory = ds_reports.Tables[4].Rows[Tcoltxt][3].ToString();
       //     if (colgroupoptions.Rows.Count < ds_reports.Tables[4].Rows.Count)
       //     {
       //         for (int Tcoltxt1 = 0; Tcoltxt1 < ds_reports.Tables[4].Rows.Count; Tcoltxt1++)
       //         {
       //             if (strcategory == ds_reports.Tables[4].Rows[Tcoltxt1][3].ToString())
       //             {
       //                 AddDatatoTable(Convert.ToInt32(ds_reports.Tables[4].Rows[Tcoltxt1][0].ToString()), ds_reports.Tables[4].Rows[Tcoltxt1][1].ToString(), ds_reports.Tables[4].Rows[Tcoltxt1][2].ToString(), ds_reports.Tables[4].Rows[Tcoltxt1][3].ToString(), colgroupoptions);
       //             }
       //         }
       //     }
       // }

       

       // HttpContext.Current.Response.Clear();
       // HttpContext.Current.Response.Buffer = true;
       // HttpContext.Current.Response.ContentType = "application/ms-excel";
       // HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + SelectedSurvey.SURVEY_NAME + ".xls");
       //// HttpContext.Current.Response.Charset = "";
        
       // StringWriter sw = new StringWriter();
       // HtmlTextWriter hw = new HtmlTextWriter(sw);
       // //Table table = new Table();

       


       // if (ds_reports != null && ds_reports.Tables.Count > 0)
       // {
       //     int x = 0;
       //     int y = 0;

       //     //code optimization done by satya

       //     //creating a html table
       //     Table Repnew1 = new Table();
       //     Repnew1.ID = "crosstabsst";
       //     Repnew1.Width = 100;
       //     Repnew1.BorderWidth = 1;            
       //     Repnew1.BorderColor = System.Drawing.Color.Black;
       //     Repnew1.CellPadding = 1;
       //     Repnew1.CellSpacing = 0;

       //     //row to display the row question
       //     TableRow rowquestion1 = new TableRow();
       //     //rowquestion1.BorderWidth = 1;
       //    // rowquestion1.BorderColor = System.Drawing.Color.Blue;
       //     //row1.BgColor = "#71A2E7";
       //     TableCell rowquestioncell1 = new TableCell();
       //     rowquestioncell1.Width = 100;
       //     rowquestioncell1.BorderWidth = 1;
       //     rowquestioncell1.BorderColor = System.Drawing.Color.Black;
       //     //rowquestioncell1.BackColor = System.Drawing.Color.Aqua;

       //     if (ques_array != null && ques_array[0] != null)
       //     {
       //         rowquestioncell1.Text = ques_array[0].ToString();
       //         //rowquestionlbl.CssClass = "ques";
       //         rowquestioncell1.Font.Bold = true;
       //     }

       //     TableCell rowquestion2cell1 = new TableCell();
       //     rowquestion2cell1.Width = 100;
       //     rowquestion2cell1.BackColor = System.Drawing.Color.Aqua;

       //     if (ques_array != null && ques_array[1] != null)
       //     {
       //         rowquestion2cell1.Text = ques_array[1].ToString();
       //         //rowquestionlbl.CssClass = "ques";
       //         rowquestion2cell1.Font.Bold = true;
       //     }

            
       //     //rowquestioncell1.NoWrap = true;
       //     rowquestion1.Cells.Add(rowquestioncell1);
       //     rowquestion1.Cells.Add(rowquestion2cell1);
           
       //     Repnew1.Rows.Add(rowquestion1);



       //     //row to display the columns of selected question in column options

       //     TableRow colrownew1 = new TableRow();

       //     TableCell cellspace = new TableCell();

       //     cellspace.Text = "";
       //     // System.Web.UI.WebControls.Label lblspace = new System.Web.UI.WebControls.Label();
       //     //lblspace.Text = "";
       //     // cellspace.Controls.Add(lblspace);
       //     colrownew1.Cells.Add(cellspace);


       //     string createtotalcolumn = "";

       //     //displays the answeriptions of columns option question to display the column headings

       //     // for (int coltxt = 0; coltxt < ds_reports.Tables[4].Rows.Count; coltxt++)
       //     for (int coltxt = 0; coltxt < colgroupoptions.Rows.Count; coltxt++)
       //     {
       //         TableCell cellcolrownew1 = new TableCell();
       //         // cellcolrownew1.BgColor = "#b1c8e9";
       //         cellcolrownew1.BackColor = System.Drawing.Color.LightSkyBlue;
       //         cellcolrownew1.BorderWidth = 1;
       //         cellcolrownew1.BorderColor = System.Drawing.Color.Black;

       //         // System.Web.UI.WebControls.Label lblcolrownew1 = new System.Web.UI.WebControls.Label();
       //         // string strcolrownew = ds_reports.Tables[4].Rows[coltxt][2].ToString();
       //         string strcolrownew = colgroupoptions.Rows[coltxt][2].ToString();
       //         //lblcolrownew1.Text = strcolrownew;
       //         cellcolrownew1.Text = strcolrownew;


       //         // cellcolrownew1.Controls.Add(lblcolrownew1);
       //         colrownew1.Cells.Add(cellcolrownew1);

       //         //  if (coltxt < ds_reports.Tables[4].Rows.Count-1)
       //         if (coltxt < colgroupoptions.Rows.Count - 1)
       //         {
       //             // createtotalcolumn = ds_reports.Tables[4].Rows[coltxt+1][3].ToString();
       //             createtotalcolumn = colgroupoptions.Rows[coltxt + 1][3].ToString();
       //             // if (createtotalcolumn != ds_reports.Tables[4].Rows[coltxt][3].ToString())
       //             if (createtotalcolumn != colgroupoptions.Rows[coltxt][3].ToString())
       //             {
       //                 TableCell cellother = new TableCell();
       //                 cellother.BackColor = System.Drawing.Color.LightSkyBlue;
       //                 cellother.BorderWidth = 1;
       //                 cellother.BorderColor = System.Drawing.Color.Black;
       //                 cellother.Font.Bold = true;
       //                 //cellother.BgColor = "#b1c8e9";

       //                 // System.Web.UI.WebControls.Label lblother = new System.Web.UI.WebControls.Label();
       //                 //lblother.Font.Bold = true;
       //                 //lblother.Text = ds_reports.Tables[4].Rows[coltxt][3].ToString() + " - " + "Total";
       //                 //lblother.Text = colgroupoptions.Rows[coltxt][3].ToString() + " - " + "Total";
       //                 cellother.Text = colgroupoptions.Rows[coltxt][3].ToString() + " - " + "Total";
       //                 // cellother.Controls.Add(lblother);
       //                 colrownew1.Cells.Add(cellother);
       //             }
       //         }
       //     }



       //     TableCell cellotherfinal = new TableCell();
       //     cellotherfinal.BackColor = System.Drawing.Color.LightSkyBlue;
       //     cellotherfinal.Font.Bold = true;
       //     cellotherfinal.BorderWidth = 1;
       //     cellotherfinal.BorderColor = System.Drawing.Color.Black;
       //     cellotherfinal.Font.Bold = true;
       //     // cellotherfinal.BgColor = "#b1c8e9";
       //     // System.Web.UI.WebControls.Label lblotherfinal = new System.Web.UI.WebControls.Label();
       //     // lblotherfinal.Font.Bold = true;
       //     //// lblotherfinal.Text = ds_reports.Tables[4].Rows[ds_reports.Tables[4].Rows.Count-1][3].ToString() + " - " + "Total";
       //     // lblotherfinal.Text = colgroupoptions.Rows[colgroupoptions.Rows.Count - 1][3].ToString() + " - " + "Total";
       //     cellotherfinal.Text = colgroupoptions.Rows[colgroupoptions.Rows.Count - 1][3].ToString() + " - " + "Total";
       //     // cellotherfinal.Controls.Add(lblotherfinal);
       //     colrownew1.Cells.Add(cellotherfinal);

       //     Repnew1.Rows.Add(colrownew1);

       //     //end of displaying the columns headings


       //     //to display the rows and count value in the cells based on answerids
       //     //row selected questionnloop
       //     for (int rowtxt = 0; rowtxt < ds_reports.Tables[0].Rows.Count; rowtxt++)
       //     {
       //         TableRow rownew1 = new TableRow();
       //         TableRow perrow1 = new TableRow();
       //         //displaying the answeroption of row selected question
       //         TableCell cellrownew1 = new TableCell();
       //         cellrownew1.BackColor = System.Drawing.Color.LightSkyBlue;
       //         cellrownew1.BorderWidth = 1;
       //         cellrownew1.BorderColor = System.Drawing.Color.Black;

       //         // cellrownew1.BgColor = "#b1c8e9";
       //         //  cellrownew1.NoWrap = true;
       //         //  System.Web.UI.WebControls.Label lblrownew1 = new System.Web.UI.WebControls.Label();
       //         string strrownew = ds_reports.Tables[0].Rows[rowtxt]["ANSWER_OPTIONS"].ToString();
       //         // lblrownew1.Text = strrownew;

       //         cellrownew1.Text = strrownew;

       //         // cellrownew1.Controls.Add(lblrownew1);
       //         rownew1.Cells.Add(cellrownew1);

       //         TableCell percell = new TableCell();
       //         percell.BorderWidth = 1;
       //         percell.BorderColor = System.Drawing.Color.Black;
       //         // System.Web.UI.WebControls.Label lblpercntg = new System.Web.UI.WebControls.Label();
       //         // //percentage row
       //         //  lblpercntg.Text = "";
       //         percell.Text = "";
       //         // percell.Controls.Add(lblpercntg);
       //         perrow1.Cells.Add(percell);

       //         int finalHTSumgrouptotalcnt1 = 0;
       //         int sumtotalrowcount = 0;
       //         int sumgrouptotalrowcount = 0;

       //         int HTSumgrouptotalcnt1 = 0;

       //         //column selected question loop colgroupoptions
       //         //for (int coltxt = 0; coltxt < ds_reports.Tables[4].Rows.Count; coltxt++)
       //         for (int coltxt = 0; coltxt < colgroupoptions.Rows.Count; coltxt++)
       //         {

       //             TableCell cellnew1 = new TableCell();
       //             cellnew1.HorizontalAlign = HorizontalAlign.Center;
       //             cellnew1.BorderWidth = 1;
       //             cellnew1.BorderColor = System.Drawing.Color.Black;
       //             // System.Web.UI.WebControls.Label lblnew1 = new System.Web.UI.WebControls.Label();

       //             //gets the count of respondents by passing rowanserid and columnanswerid
       //             //ds_rowcolcount = obj_questions.getRespondentscount(Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()),
       //             //    Convert.ToInt32(ds_reports.Tables[4].Rows[coltxt]["ANSWER_ID"].ToString()),SurveyBasicInfoView.SURVEY_ID);

       //             ds_rowcolcount = obj_questions.getRespondentscount(Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()),
       //               Convert.ToInt32(colgroupoptions.Rows[coltxt]["ANSWER_ID"].ToString()), SurveyBasicInfoView.SURVEY_ID);

       //             string strcntval;
       //             if (ds_rowcolcount.Tables[0].Rows.Count > 0)
       //             {

       //                 strcntval = ds_rowcolcount.Tables[0].Rows[0][2].ToString();
       //                 // lblnew1.Text = strcntval;                        
       //                 cellnew1.Text = strcntval;
       //             }
       //             else
       //             {
       //                 strcntval = "0";
       //                 // lblnew1.Text = strcntval;
       //                 cellnew1.Text = strcntval;
       //             }


       //             sumtotalrowcount = sumtotalrowcount + Convert.ToInt32(strcntval);

       //             //displays the individual count in cell                   

       //             //cellnew1.Controls.Add(lblnew1);
       //             rownew1.Cells.Add(cellnew1);


       //             int HTsumtotalrowcountprcntg = 0;



       //             for (int rowtxtprcntg = 0; rowtxtprcntg < ds_reports.Tables[0].Rows.Count; rowtxtprcntg++)
       //             {


       //                 ds_rowcolcount = obj_questions.getRespondentscount(Convert.ToInt32(colgroupoptions.Rows[coltxt]["ANSWER_ID"].ToString()),
       //                     Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxtprcntg]["ANSWER_ID"].ToString()), SurveyBasicInfoView.SURVEY_ID);

       //                 string HTstrcntvalprcntg;
       //                 if (ds_rowcolcount.Tables[0].Rows.Count > 0)
       //                 {

       //                     HTstrcntvalprcntg = ds_rowcolcount.Tables[0].Rows[0][2].ToString();

       //                 }
       //                 else
       //                 {
       //                     HTstrcntvalprcntg = "0";

       //                 }


       //                 HTsumtotalrowcountprcntg = HTsumtotalrowcountprcntg + Convert.ToInt32(HTstrcntvalprcntg);

       //             }


       //             HTSumgrouptotalcnt1 = HTSumgrouptotalcnt1 + HTsumtotalrowcountprcntg;

       //             finalHTSumgrouptotalcnt1 = HTSumgrouptotalcnt1;

       //             TableCell percell1 = new TableCell();
       //             percell1.HorizontalAlign = HorizontalAlign.Center;
       //             percell1.BackColor = System.Drawing.Color.LightGray;
       //             percell1.BorderWidth = 1;
       //             percell1.BorderColor = System.Drawing.Color.Black;

       //             //percell1.BgColor = "LightGrey";
       //             // System.Web.UI.WebControls.Label lblpercntg1 = new System.Web.UI.WebControls.Label();


       //             if (colquestiontype == 11)
       //             {
       //                 ds_rowcolcountmultipleselect = obj_questions.getRespondentscountmultipleselect(Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()),
       //                     colgroupoptions.Rows[coltxt][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);


       //                 HTsumtotalrowcountprcntg = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

       //             }


       //             Double z = Convert.ToDouble(Convert.ToInt32(strcntval) * 100);
       //             z = Convert.ToDouble(z / HTsumtotalrowcountprcntg);
       //             string percentdisplay = Math.Round(Convert.ToDouble(z), 2).ToString();

       //             // lblpercntg1.Text = percentdisplay + "%";
       //             //percell1.Controls.Add(lblpercntg1);
       //             percell1.Text = percentdisplay + "%";
       //             perrow1.Cells.Add(percell1);





       //             //sum of the independent attributes 
       //             // if (coltxt < ds_reports.Tables[4].Rows.Count - 1)
       //             if (coltxt < colgroupoptions.Rows.Count - 1)
       //             {
       //                 //createtotalcolumn = ds_reports.Tables[4].Rows[coltxt + 1][3].ToString();
       //                 createtotalcolumn = colgroupoptions.Rows[coltxt + 1][3].ToString();
       //                 // if (createtotalcolumn != ds_reports.Tables[4].Rows[coltxt][3].ToString())
       //                 if (createtotalcolumn != colgroupoptions.Rows[coltxt][3].ToString())
       //                 {

       //                     if (colquestiontype == 11)
       //                     {
       //                         ds_rowcolcountmultipleselect = obj_questions.getRespondentscountmultipleselect(Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()),
       //                             colgroupoptions.Rows[coltxt][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);


       //                         sumtotalrowcount = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

       //                     }


       //                     TableCell cellrowtotal = new TableCell();
       //                     cellrowtotal.HorizontalAlign = HorizontalAlign.Center;
       //                     cellrowtotal.Font.Bold = true;
       //                     cellrowtotal.BackColor = System.Drawing.Color.LightSkyBlue;
       //                     cellrowtotal.BorderWidth = 1;
       //                     cellrowtotal.BorderColor = System.Drawing.Color.Black;
       //                     //  cellrowtotal.BgColor = "lightgreen";                            
       //                     // System.Web.UI.WebControls.Label lblrowtotal = new System.Web.UI.WebControls.Label();
       //                     // lblrowtotal.Font.Bold = true;
       //                     sumgrouptotalrowcount = sumtotalrowcount;
       //                     // lblrowtotal.Text = sumgrouptotalrowcount.ToString();    
       //                     cellrowtotal.Text = sumgrouptotalrowcount.ToString();
       //                     //   cellrowtotal.Controls.Add(lblrowtotal);
       //                     rownew1.Cells.Add(cellrowtotal);


       //                     TableCell percell2 = new TableCell();
       //                     percell2.BackColor = System.Drawing.Color.LightGray;
       //                     percell2.BorderWidth = 1;
       //                     percell2.BorderColor = System.Drawing.Color.Black;
       //                     percell2.HorizontalAlign = HorizontalAlign.Center;
       //                     // percell2.BgColor = "LightGrey";

       //                     //System.Web.UI.WebControls.Label lblpercntg2 = new System.Web.UI.WebControls.Label();

       //                     if (colquestiontype == 11)
       //                     {
       //                         ds_rowcolcountmultipleselect = obj_questions.getRespondentsrowcountmultipleselect(rowquestionid,
       //                             colgroupoptions.Rows[coltxt][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);


       //                         HTSumgrouptotalcnt1 = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

       //                     }


       //                     Double z1 = Convert.ToDouble(sumgrouptotalrowcount * 100);
       //                     z1 = Convert.ToDouble(z1 / HTSumgrouptotalcnt1);
       //                     string percentdisplaytotal = Math.Round(Convert.ToDouble(z1), 2).ToString();

       //                     //lblpercntg2.Text = percentdisplaytotal + "%";
       //                     //lblpercntg2.Font.Bold = true;
       //                     //percell2.Controls.Add(lblpercntg2);
       //                     percell2.Text = percentdisplaytotal + "%";
       //                     percell2.Font.Bold = true;
       //                     perrow1.Cells.Add(percell2);


       //                     HTSumgrouptotalcnt1 = 0;
       //                     sumgrouptotalrowcount = 0;
       //                     sumtotalrowcount = 0;
       //                 }
       //             }




       //         }

       //         if (colquestiontype == 11)
       //         {
       //             ds_rowcolcountmultipleselect = obj_questions.getRespondentscountmultipleselect(Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()),
       //                 colgroupoptions.Rows[colgroupoptions.Rows.Count - 1][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);
       //             sumtotalrowcount = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;
       //         }
       //         //last column sum
       //         TableCell cellothernew1 = new TableCell();
       //         cellothernew1.BackColor = System.Drawing.Color.LightSkyBlue;
       //         cellothernew1.BorderWidth = 1;
       //         cellothernew1.BorderColor = System.Drawing.Color.Black;
       //         //cellothernew1.BgColor = "lightgreen";


       //         //System.Web.UI.WebControls.Label lbltotalrowsum = new System.Web.UI.WebControls.Label();
       //         //lbltotalrowsum.Font.Bold = true;
       //         cellothernew1.Font.Bold = true;
       //         cellothernew1.HorizontalAlign = HorizontalAlign.Center;
       //         // lbltotalrowsum.Text = sumtotalrowcount.ToString();
       //         cellothernew1.Text = sumtotalrowcount.ToString();
       //         //cellothernew1.Controls.Add(lbltotalrowsum);
       //         rownew1.Cells.Add(cellothernew1);


       //         TableCell percell3 = new TableCell();
       //         percell3.BackColor = System.Drawing.Color.LightGray;
       //         percell3.BorderWidth = 1;
       //         percell3.HorizontalAlign = HorizontalAlign.Center;
       //         percell3.BorderColor = System.Drawing.Color.Black;
       //         // percell3.BgColor = "LightGrey";
       //         // System.Web.UI.WebControls.Label lblpercntg3 = new System.Web.UI.WebControls.Label();
       //         // lblpercntg3.Font.Bold = true;
       //         percell3.Font.Bold = true;


       //         if (colquestiontype == 11)
       //         {
       //             ds_rowcolcountmultipleselect = obj_questions.getRespondentsrowcountmultipleselect(rowquestionid,
       //                 colgroupoptions.Rows[colgroupoptions.Rows.Count - 1][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);

       //             finalHTSumgrouptotalcnt1 = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

       //         }


       //         Double z2 = Convert.ToDouble(sumtotalrowcount * 100);
       //         z2 = Convert.ToDouble(z2 / finalHTSumgrouptotalcnt1);
       //         string percentdisplaytotal1 = Math.Round(Convert.ToDouble(z2), 2).ToString();

       //         percell3.Text = percentdisplaytotal1 + "%";
       //         //lblpercntg3.Text = percentdisplaytotal1 + "%";
       //         //percell3.Controls.Add(lblpercntg3);
       //         perrow1.Cells.Add(percell3);

       //         Repnew1.Rows.Add(rownew1);

       //         Repnew1.Rows.Add(perrow1);




       //     }

       //     //getting column sums
       //     TableRow HTRsumofcolumns = new TableRow();
       //     TableCell HTCsumofcolumns = new TableCell();
       //     //HTCsumofcolumns.BgColor = "#b1c8e9";
       //     HTCsumofcolumns.BackColor = System.Drawing.Color.LightSkyBlue;

       //     HTCsumofcolumns.BorderWidth = 1;
       //     HTCsumofcolumns.BorderColor = System.Drawing.Color.Black;
       //     HTCsumofcolumns.HorizontalAlign = HorizontalAlign.Center;

       //     //System.Web.UI.WebControls.Label HTClblsumofcols = new System.Web.UI.WebControls.Label();
       //     //HTClblsumofcols.Text = "";
       //     //HTCsumofcolumns.Controls.Add(HTClblsumofcols);
       //     HTCsumofcolumns.Text = "";
       //     HTRsumofcolumns.Cells.Add(HTCsumofcolumns);

       //     int HTsumtotalrowcount = 0;
       //     int HTSumgrouptotalcnt = 0;
       //     //  for (int coltxt = 0; coltxt < ds_reports.Tables[4].Rows.Count; coltxt++)
       //     for (int coltxt = 0; coltxt < colgroupoptions.Rows.Count; coltxt++)
       //     {
       //         for (int rowtxt = 0; rowtxt < ds_reports.Tables[0].Rows.Count; rowtxt++)
       //         {

       //             // System.Web.UI.WebControls.Label HTlblnew1 = new System.Web.UI.WebControls.Label();
       //             string HTlblnew1;

       //             ds_rowcolcount = obj_questions.getRespondentscount(Convert.ToInt32(colgroupoptions.Rows[coltxt]["ANSWER_ID"].ToString()),
       //                 Convert.ToInt32(ds_reports.Tables[0].Rows[rowtxt]["ANSWER_ID"].ToString()), SurveyBasicInfoView.SURVEY_ID);

       //             string HTstrcntval;
       //             if (ds_rowcolcount.Tables[0].Rows.Count > 0)
       //             {

       //                 HTstrcntval = ds_rowcolcount.Tables[0].Rows[0][2].ToString();
       //                 // HTlblnew1.Text = HTstrcntval;
       //                 HTlblnew1 = HTstrcntval;
       //             }
       //             else
       //             {
       //                 HTstrcntval = "0";
       //                 // HTlblnew1.Text = HTstrcntval;
       //                 HTlblnew1 = HTstrcntval;
       //             }


       //             HTsumtotalrowcount = HTsumtotalrowcount + Convert.ToInt32(HTstrcntval);

       //         }
       //         TableCell HTCcellrowtotal1 = new TableCell();
       //         // HTCcellrowtotal1.BgColor = "#b1c8e9";
       //         HTCcellrowtotal1.BackColor = System.Drawing.Color.LightSkyBlue;
       //         HTCcellrowtotal1.BorderWidth = 1;
       //         HTCcellrowtotal1.BorderColor = System.Drawing.Color.Black;
       //         HTCcellrowtotal1.HorizontalAlign = HorizontalAlign.Center;

       //         //System.Web.UI.WebControls.Label HTClblrowtotal1 = new System.Web.UI.WebControls.Label();
       //         //HTClblrowtotal1.Text = HTsumtotalrowcount.ToString();

       //         HTCcellrowtotal1.Text = HTsumtotalrowcount.ToString();
       //         // HTCcellrowtotal1.Controls.Add(HTClblrowtotal1);
       //         HTRsumofcolumns.Cells.Add(HTCcellrowtotal1);

       //         HTSumgrouptotalcnt = HTSumgrouptotalcnt + HTsumtotalrowcount;

       //         HTsumtotalrowcount = 0;

       //         // if (coltxt < ds_reports.Tables[4].Rows.Count - 1)
       //         if (coltxt < colgroupoptions.Rows.Count - 1)
       //         {
       //             // createtotalcolumn = ds_reports.Tables[4].Rows[coltxt + 1][3].ToString();
       //             createtotalcolumn = colgroupoptions.Rows[coltxt + 1][3].ToString();

       //             // if (createtotalcolumn != ds_reports.Tables[4].Rows[coltxt][3].ToString())
       //             if (createtotalcolumn != colgroupoptions.Rows[coltxt][3].ToString())
       //             {
       //                 if (colquestiontype == 11)
       //                 {
       //                     ds_rowcolcountmultipleselect = obj_questions.getRespondentsrowcountmultipleselect(rowquestionid,
       //                         colgroupoptions.Rows[coltxt][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);


       //                     HTSumgrouptotalcnt = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

       //                 }

       //                 TableCell HTCcellrowtotal2 = new TableCell();
       //                 // HTCcellrowtotal2.BgColor = "#b1c8e9";
       //                 HTCcellrowtotal2.BackColor = System.Drawing.Color.Red;
       //                 HTCcellrowtotal2.BorderWidth = 1;
       //                 HTCcellrowtotal2.BorderColor = System.Drawing.Color.Black;
       //                 HTCcellrowtotal2.HorizontalAlign = HorizontalAlign.Center;

       //                 // System.Web.UI.WebControls.Label HTClblrowtotal2 = new System.Web.UI.WebControls.Label();
       //                 //  HTClblrowtotal2.Font.Bold = true;
       //                 HTCcellrowtotal2.Font.Bold = true;
       //                 //HTClblrowtotal2.Text = HTSumgrouptotalcnt.ToString();
       //                 HTCcellrowtotal2.Text = HTSumgrouptotalcnt.ToString();
       //                 //HTCcellrowtotal2.Controls.Add(HTClblrowtotal2);
       //                 HTRsumofcolumns.Cells.Add(HTCcellrowtotal2);
       //                 HTSumgrouptotalcnt = 0;
       //             }

       //         }
       //     }


       //     if (colquestiontype == 11)
       //     {
       //         ds_rowcolcountmultipleselect = obj_questions.getRespondentsrowcountmultipleselect(rowquestionid,
       //             colgroupoptions.Rows[colgroupoptions.Rows.Count - 1][3].ToString(), colquestionid, SurveyBasicInfoView.SURVEY_ID);


       //         HTSumgrouptotalcnt = ds_rowcolcountmultipleselect.Tables[0].Rows.Count;

       //     }
       //     TableCell HTCcellrowtotal3 = new TableCell();
       //     //HTCcellrowtotal3.BgColor = "#b1c8e9";
       //     HTCcellrowtotal3.BackColor = System.Drawing.Color.Red;
       //     HTCcellrowtotal3.BorderWidth = 1;
       //     HTCcellrowtotal3.BorderColor = System.Drawing.Color.Black;
       //     HTCcellrowtotal3.HorizontalAlign = HorizontalAlign.Center;

       //     // System.Web.UI.WebControls.Label HTClblrowtotal3 = new System.Web.UI.WebControls.Label();
       //     // HTClblrowtotal3.Font.Bold = true;
       //     HTCcellrowtotal3.Font.Bold = true;
       //     // HTClblrowtotal3.Text = HTSumgrouptotalcnt.ToString();
       //     HTCcellrowtotal3.Text = HTSumgrouptotalcnt.ToString();
       //     // HTCcellrowtotal3.Controls.Add(HTClblrowtotal3);
       //     HTRsumofcolumns.Cells.Add(HTCcellrowtotal3);


       //     Repnew1.Rows.Add(HTRsumofcolumns);


       //     Repnew1.RenderControl(hw);

        
       // }
       // HttpContext.Current.Response.Write(sw.ToString());
       // HttpContext.Current.Response.End();

        ////var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        ////if (userDetails != null && SurveyBasicInfoView != null)
        ////{
        ////    if (tem1 == 0)
        ////    {
        ////        generatebuttonfunction();

        ////    }
        ////    if (tem1 != 0)
        ////    {
        ////        string qids = "";
        ////        qids = Convert.ToString(tem1) + "_" + Convert.ToString(tem2);
        ////        int exp_id = surcore.InsertExportInfo(SurveyBasicInfoView.SURVEY_ID, 6, qids, userDetails.UserId);
        ////        if (exp_id > 0)
        ////        {
        ////            DataSet ds = new DataSet();
        ////            int flag = 0;
        ////            for (int i = 0; i < 6; i++)
        ////            {
        ////                System.Threading.Thread.Sleep(5000 * 10);
        ////                ds = surcore.GetExportInfo(exp_id);
        ////                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        ////                {
        ////                    if (Convert.ToInt32(ds.Tables[0].Rows[0]["REPORT_STATUS"]) == 2)
        ////                    {
        ////                        string strDirector = ConfigurationManager.AppSettings["RootPath"].ToString();

        ////                        if (!Directory.Exists(strDirector + SurveyBasicInfoView.USERID + @"\" + SurveyBasicInfoView.SURVEY_ID))
        ////                        {
        ////                            Directory.CreateDirectory(strDirector + SurveyBasicInfoView.USERID + @"\" + SurveyBasicInfoView.SURVEY_ID);
        ////                        }

        ////                        string strXLFileName = strDirector + SurveyBasicInfoView.USERID + "\\" + SurveyBasicInfoView.SURVEY_ID + "\\" + Convert.ToString(ds.Tables[0].Rows[0]["REPORT_FILENAME"]);

        ////                       SaveDialogBox(strXLFileName, SelectedSurvey.SURVEY_NAME, ".xls");
        ////                        flag = 1;

        ////                        break;

        ////                    }

        ////                }


        ////            }
        ////        }
        ////    }
        ////}
    }
    public void KillProcess()
    {
        System.Diagnostics.Process[] PROC = System.Diagnostics.Process.GetProcessesByName("EXCEL");
        foreach (System.Diagnostics.Process PK in PROC)
        {
            if (PK.MainWindowTitle.Length == 0)
                PK.Kill();
        }

    }
    public void customizeIntroPage()
    {
    }

    public void CustomizeNonDescPage()
    {
    }



    public void SaveDialogBox(string sourcePath, string SurveyName, string fileExtension)
    {
        if (File.Exists(sourcePath))
        {
            string name_survey = SurveyName.Replace(" ", "");
            name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
            name_survey = name_survey.Replace("/", "");
            Response.ContentType = "application/ms-excel";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + fileExtension);
            Response.TransmitFile(sourcePath);
            Response.End();
        }
        //divSendReport.Visible = true;
        //ErrorSuccessNotifier.AddSuccessMessage(Resources.CommonMessages.SendReportMessage);

    }


    public void generatebuttonfunction()
    {
        IterateControls1(this);
        if (status_flag == 3)
        {
            if (idarray != null)
            {
                if ((idarray[0].Substring(0, 9) == idarray[1].Substring(0, 9)) || (idarray[0].Substring(9) == idarray[1].Substring(9)))
                    status_flag = 1;
            }

        }
        if (status_flag == 1)
        {
            lbl_message.Text = "Please select two different questions in row and column format";
            lbl_message.Visible = true;
            rpnl_crosstabbing.Visible = false; rpnl_crosstabbing_tbl.Visible = false;
            btnsavecrosstab.Visible = false;
            txt_crosstabreportname.Visible = false;
            btnexporttoexcel.Visible = false;

        }
        else if (status_flag == 2)
        {
            lbl_message.Text = "Please select only two questions in row and column format";
            lbl_message.Visible = true;
            rpnl_crosstabbing.Visible = false; rpnl_crosstabbing_tbl.Visible = false;
            btnsavecrosstab.Visible = false;
            txt_crosstabreportname.Visible = false;
            btnexporttoexcel.Visible = false;

        }
        else if (status_flag == 4)
        {
            lbl_message.Text = "Please select only two questions in row and column format";
            lbl_message.Visible = true;
            rpnl_crosstabbing.Visible = false; rpnl_crosstabbing_tbl.Visible = false;
            btnsavecrosstab.Visible = false;
            txt_crosstabreportname.Visible = false;
            btnexporttoexcel.Visible = false;

        }
        else if (status_flag == 3)
        {
            lbl_message.Visible = true;
            rpnl_crosstabbing.Visible = true; rpnl_crosstabbing_tbl.Visible = true;
            int count = 0;
            if (ds_questions != null && ds_questions.Tables.Count > 0)
            {
                count = ds_questions.Tables[0].Rows.Count;
            }
            str1 = "";
            IterateControls(this);

            string temp1 = "";
            string temp2 = "";
            if (idarray != null)
            {
                temp1 = idarray[0];
                temp2 = idarray[1];
                tem1 = Convert.ToInt32(idarray[0].Substring(9));
                tem2 = Convert.ToInt32(idarray[1].Substring(9));
            }
           // report_function();
            ReportFunction();
            btnsavecrosstab.Visible = true;
            ASPxLabel1.Visible = true;
            txt_crosstabreportname.Visible = true;
            btnexporttoexcel.Visible = true;

        }
    }

    private DataTable createDataTable()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
            DataColumn myDataColumn3 = new DataColumn();

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "Answer_ID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "answer_options";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "answeroptionname";
            myDataTable.Columns.Add(myDataColumn2);

            myDataColumn3.DataType = Type.GetType("System.String");
            myDataColumn3.ColumnName = "groupname";
            myDataTable.Columns.Add(myDataColumn3);

            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void AddDatatoTable(int answerID, string answeroption,string answeroptionname,string groupname, DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["Answer_ID"] = answerID;
            dr["answer_options"] = answeroption;
            dr["answeroptionname"] = answeroptionname;
            dr["groupname"] = groupname;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private string GetRowsandColsCount(DataTable dt)
    {
        List<string> rows = new List<string>();
        List<string> cols = new List<string>();
        foreach (DataRow dr in dt.Rows)
        {

            var delimiter = new[] { "$--$" };
            var splitOptions = dr.ItemArray[1].ToString().Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
            if (splitOptions.Length == 2)
            {
                cols.Add(splitOptions[0]);
                rows.Add(splitOptions[1]);

            }
            else if (splitOptions.Length == 3)
            {
                cols.Add(splitOptions[1]);
                rows.Add(splitOptions[2]);
            }

       
        }
        string count = rows.Distinct().ToList().Count.ToString();     
        return count;
    }


    // }
}