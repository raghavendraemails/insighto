<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="new_surveys_Home" MasterPageFile="~/Home.Master" %>
<%@ Register TagPrefix="uc" TagName="SurveysHeader" Src="~/UserControls/SurveysHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc:SurveysHeader ID="SurveysHeader" runat="server" />

<div>
    <!--=== Slider ===-->

<div class="interactive-slider-v1 img-v3">
<div class="container" style=" text-align: center;">
  <div class="group-quote">
  <h2 style="text-transform:none !important; color:#FFF900 !important; font-size:38px !important; text-shadow:2px 2px 0px rgba(80, 80, 80, 0.25) !important;">
  <span>Know Your <u class="slidingVertical-top">
      <div class="slidingVertical">
      <span>Customers</span>
      <span>Employees</span>
      <span>Members</span>
      <span>Partners</span>
      <span>Vendors</span>
    </div>
      </u>  Better </span> </h2>
  <h3 class="hero-h2Line">Simple. Smart. Survey Software to Make Informed Decisions.</h3>

  <div class="hero-banner">
  <a href="registration.aspx" class="btn-u"><b>Create Your Free Account</b></a>
  </div>

</div>
</div>
</div>


  <!--=== End Slider ===-->

  <!--=== Content Part ===-->
  <div class="purchase" id="mantra">
    <div class="container">
      <div class="row">
        <div class="col-md-12 animated fadeInLeft" align="center"> <span>Human Opinions - Views - Responses - Emotions - Suggestions - Feedback</span></div>
          <!--<div>Find your <span class="informal-font">Mantra</span> to Make Informed Decisions. Strengthen your Engagement.</span> </div>-->
      </div>
      <div class="row">
      	<div class="col-md-4"></div>
        <div class="col-md-4">
        	<div class="arrow-down">
        		<a href="#" data-scroll-nav="1"><img class="wow bounce" src="../assets/img/down-arrow1.png" alt="" width="48" height="39"></a>
        	</div>
        </div>
        <div class="col-md-4"></div>
      </div>
    </div>
  </div>

  <!--<div class="bg-blue-light content-sm-new" data-scroll-index='1'>
    <div class="container">
      <div class="row">
        <div class="headline-center margin-bottom-20">
          <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Why Insighto</h2>
          <p class="para-updated">Insighto Surveys enable organisations to listen to key stakeholders and make decisions for success & growth. <br>
            Thus strengthening the relationship and engagement between them</p>
        </div>
      </div>
    </div>
  </div>-->

  <div class="container content-sm" data-scroll-index="1">
    <div class="row">
        <div class="col-md-12">
        <div class="text-center margin-bottom-10">
          <h2 class="title-v2 title-center" style="color:#1035A2 !important;">SIMPLE, EASY, INTUITIVE. INSIGHTO ENABLES YOU TO GET CRITICAL INSIGHTS. </h2>
        </div>
        <center>
        <!--<h4 style="text-transform:capitalize !important;"><i>On your Business. Market. Product. Work. Service.</i></h4>-->
        <p class="para-updated" style="font-size:16px !important;">
        From people who contribute to your success and growth and step up your engagement with them. </p>
        </center>
        </div>
        <div class="row">
        <div class="col-md-5"> <img class="wow rotateIn img-responsive" src="../assets/img/mockup/imac2.png" alt=""> </div>
        <div class="col-md-7">
        <div class="headline-left margin-top-20">
          <img class="img-responsive" src="../assets/img/mockup/cloud-tag.png" alt="">
        </div>
        </div>
        </div>
    </div>
  </div>

  <div class="parallax-counter-v2 parallaxBg1" style="background-position: 50% 32px;">
    <div class="container">
      <ul class="row list-row">
        <li class="col-md-3 col-sm-6 col-xs-12 md-margin-bottom-30">
          <div class="counters rounded">
            <h5 style="color:#1035A2 !important;">USERS FROM</h5>
            <span class="counter">44</span>
            <h4 style="color:#954f9d;">Countries</h4>
          </div>
        </li>
        <li class="col-md-3 col-sm-6 col-xs-12 md-margin-bottom-30">
          <div class="counters rounded">
            <h5 style="color:#1035A2 !important;">USED IN</h5>
            <span class="counter">16</span>
            <h4 style="color:#954f9d;">LANGUAGES</h4>
          </div>
        </li>
        <li class="col-md-3 col-sm-6 col-xs-12 sm-margin-bottom-30">
          <div class="counters rounded">
            <h5 style="color:#1035A2 !important;">GENERATED</h5>
            <span class="counter">5.2</span> Billion
            <h4 style="color:#954f9d;">INSIGHTS</h4>
          </div>
        </li>
        <li class="col-md-3 col-sm-6 col-xs-12">
          <div class="counters rounded">
            <h5 style="color:#1035A2 !important;">FROM</h5>
            <span class="counter">78</span>
            <h4 style="color:#954f9d;">NATIONS</h4>
          </div>
        </li>
      </ul>
    </div>
  </div>

  <div class="">
  <div id="features" class="container content-sm features-content" data-scroll-index="1">
    <div class="text-center margin-bottom-50">
      <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Highlights</h2>
    </div>
    <div class="row category margin-bottom-20">
      <div class="col-md-6 col-sm-6">
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"> <i class="wow wobble icon-ask"></i><!--<i class="wow wobble icon-custom glyphicon glyphwow wobble icon-export"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Simple &amp; Easy ways to Ask Questions - Get Answers</b></h3>
            <p class="para-updated">Build your questionnaire in few minutes<br>
              Without bothering your tech team</p>
          </div>
        </div>
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-question1"></i>
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Several Options to Send Questions</b></h3>
            <p class="para-updated">Post your Questions through multiple ways <br>
              Weblink, Email, Embed, Facebook, Whatsapp </p>
          </div>
        </div>
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-ans"></i> <!--<i class="wow wobble icon-custom wow wobble icon-sm rounded-x wow wobble icon-bg-blue fa fa-bell-o"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Get to see Answers Instantly - in Real-time</b></h3>
            <p class="para-updated">View responses as soon as the respondent fills in <br>
              With built-in basic analytics</p>
          </div>
        </div>


      </div>
      <div class="col-md-6 col-sm-6 md-margin-bottom-40">

        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-chart"></i> <!--<i class="wow wobble icon-custom wow wobble icon-sm rounded-x wow wobble icon-bg-yellow fa fa-thumbs-o-up"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Intuitive and Easy-to-read Charts</b></h3>
            <p class="para-updated">Clearly demonstrate your insights <br>
              With rich animated charts and analytics</p>
          </div>
        </div>

        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-export"></i> <!--<i class="wow wobble icon-custom wow wobble icon-sm rounded-x wow wobble icon-bg-red fa fa-hdd-o"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Export Reports for Further Data Crunching</b></h3>
            <p class="para-updated">Including raw data for further analysis or presentations <br>
              Export to MS Excel, Powerpoint, Word or PDF</p>
          </div>
        </div>
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"> <i class="wow wobble icon-dollar"></i>
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Generate Income while collecting Opinions</b></h3>
            <p class="para-updated"> Monetise your Insight generation efforts <br>
              Add sponsorships and leverage space selling </p>
          </div>
        </div>

      </div>
    </div>

  </div>
  </div>
 <!-- ------------- Multi Language -------------- -->
  <div class="highlights-section content-sm pricing-page-intro">
    <div class="container text-center">
    <div class="row">
    	<div class="col-md-12">
            <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Reach Out to your Multi-Language audience</h2>

            <p class="space-lg-hor para-updated" style="font-size: 15px;">
            Create questions in a language your people are comfortable with. <br> Just key in or copy the language content.
            </p>

        </div>
    </div>
      </div>
  </div>

 <!-- ------------- Multi Language -------------- -->
 <!-- Social icon Section -->
  <div class="content-sm pricing-page-intro">
    <div class="container text-center">
    <div class="text-center margin-bottom-50">
      <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Some of the channels Insighto Surveys can be shared</h2>
    </div>
        <div class="row">
    	<img class="wow bounceInUp img-responsive" src="../assets/img/mockup/social-icons.png" alt="">
    </div>
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <p class="para-updated-box" style="font-size:16px !important; margin-top:10px !important; text-align:center !important;"> Sign up for <a href="registration.aspx"><strong><u>Free plan</u></strong></a> (No Credit cards needed) and Upgrade anytime to any plan of your choice. </p>
      </div>
      <div class="col-md-2"></div>
    </div>
      </div>
  </div>
  <!-- Social icon Section -->

  <div class="bg-grey">
    <div class="content-sm pricing-page-intro" data-scroll-index='3'>
      <div class="container text-center">
        <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Sample Templates</h2>
        <p class="para-updated-lg">Use our sample templates, as your how-to guide, to build surveys like a pro.</p>
        <h5 class="title-v2 title-center" style="color:#1035A2 !important;">Few Samples</h5>
        <br>
        <div class="row category margin-bottom-20" align="left">
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-custom icon-sm rounded-x fa fa-users"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated-link"><a href="https://www.insighto.com/SurveyRespondent.aspx?key=2k9Zm/j8T+TZmrf3FzrPGCFLFsvjqDCrv3rOxFIkygc=" target="_blank">Customer Satisfaction</a></h3>
                <!--<p>&nbsp;</p>-->
              </div>
            </div>
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-custom icon-sm rounded-x fa fa-user"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated-link"><a href="https://www.insighto.com/SurveyRespondent.aspx?key=2k9Zm/j8T+ScFH2zMbMTTzsBLFJ15AU0gubS2ZksZuQ=" target="_blank">Employee Satisfaction</a></h3>
                <!--<p>&nbsp;</p>-->
              </div>
            </div>
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-custom icon-sm rounded-x fa fa-cubes"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated-link"><a href="https://www.insighto.com/SurveyRespondent.aspx?key=2k9Zm/j8T+Ql0Y/UTg7KZJpnt7Svy+HhdLUtXOyJF7s=" target="_blank">Ad Effectiveness</a></h3>
                <!--<p>&nbsp;</p>-->
              </div>
            </div>
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-custom icon-sm rounded-x fa fa-magic"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated-link"><a href="https://www.insighto.com/SurveyRespondent.aspx?key=2k9Zm/j8T+QV5lwxsYGsCm3HuCEhpG9xYKADz66dUWM=" target="_blank">Pre-Event Planning</a></h3>
                <!--<p>&nbsp;</p>-->
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-custom icon-sm rounded-x fa fa-graduation-cap"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated-link"><a href="https://www.insighto.com/SurveyRespondent.aspx?key=2k9Zm/j8T+SPaXuOFSAcl1RuP9qIE1dDgXUd4g67mnc=" target="_blank">Training Program Feedback</a></h3>
                <!--<p>&nbsp;</p>-->
              </div>
            </div>
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-custom icon-sm rounded-x fa fa-times"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated-link"><a href="https://www.insighto.com/SurveyRespondent.aspx?key=2k9Zm/j8T+TV/2y3P+Cusgya5za4dXGr8McSmo09xqk=" target="_blank">Exit Interview</a></h3>
                <!--<p>&nbsp;</p>-->
              </div>
            </div>
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-custom icon-sm rounded-x fa fa-wrench"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated-link"><a href="https://www.insighto.com/SurveyRespondent.aspx?key=2k9Zm/j8T+ToY7BR/XDibm/mbPZl9nymrAPmoC25xU0=" target="_blank">Customer Sat - Tech Support</a></h3>
                <!--<p>&nbsp;</p>-->
              </div>
            </div>
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-custom icon-sm rounded-x fa fa-bullhorn"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated-link"><a href="https://www.insighto.com/SurveyRespondent.aspx?key=2k9Zm/j8T+QoXIO3bucJNz+aQLGwxLimuL0iPAiDp68=" target="_blank">Salesperson Evaluation</a></h3>
                <!--<p>&nbsp;</p>-->
              </div>
            </div>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row category margin-bottom-20" align="left">
          <div class="col-md-4"></div>
          <div class="col-md-4">
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"> <i class="wow wobble icon-custom icon-sm rounded-x fa fa-user"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated-link"><a href="https://www.insighto.com/SurveyRespondent.aspx?key=2k9Zm/j8T+TNqaHaqWESxFzWCH4mqsU+xDefqpYRyco=" target="_blank">Manager's Self-Assessment</a></h3>
                <!--<p>&nbsp;</p>-->
              </div>
            </div>
          </div>
          <div class="col-md-4"></div>
        </div>
      </div>
      <p>&nbsp;</p>
    </div>
  </div>

  <div class="one-page">
    <div class="one-page-inner one-blue-light">
      <div class="container one-blue-light">
        <div class="text-center margin-bottom-50">
          <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Serving Professionals and Business Owners since Five years</h2>
        </div>

        <!-- Service Blcoks -->
        <div class="row service-box-v1 margin-bottom-40">
          <div class="col-md-3 col-sm-12 md-margin-bottom-40">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x icon glyphicon glyphicon-bullhorn"></i>
              <h2 class="heading-sm2">Marketing</h2>
              <p class="para-updated">Marketers use Insighto to generate tremendous customer insights</p>
              <ul class="list-unstyled">
                <li>Voice of Customers</li>
                <li>Customer Needs &amp; Issues</li>
                <li>Product/ Service Quality</li>
                <li>Concept/ Product Testing</li>
                <li>Customer Satisfaction</li>
                <li>Pre &amp; Post Event Feedback</li>
                <li>Campaign Evaluation</li>
                <li>Pre-Advertisement Testing</li>
                <li>Advertisement Effectiveness</li>
                <li>Customer Validation</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 col-sm-12 md-margin-bottom-40">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x icon-line glyphicon glyphicon-briefcase"></i>
              <h2 class="heading-sm2">Sales</h2>
              <p class="para-updated">Insighto assists Sales Professionals to get insights related to </p>
              <ul class="list-unstyled">
                <li>Customer Buying Habits </li>
                <li>Product Strengths</li>
                <li>New Product/Service ideas</li>
                <li>New ways of Product usage</li>
                <li>Competitor Intelligence</li>
                <li>Sales Growth Suggestions</li>
                <li>Lead Farming</li>
                <li>Cross-sell Opportunities</li>
                <li>Up-sell Possibilities</li>
                <li>New Geos Identification</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 col-sm-12">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x icon-line glyphicon glyphicon-user"></i>
              <h2 class="heading-sm2">Human Resources</h2>
              <p class="para-updated">HR Managers use Insighto at every stage of Employee engagement</p>
              <ul class="list-unstyled">
                <li>Employees' Needs </li>
                <li>Hiring Process Evaluation</li>
                <li>New Hire Onboarding </li>
                <li>Employee Satisfaction</li>
                <li>Employee Aspirations</li>
                <li>Employee Happiness</li>
                <li>Self-Assessment Forms </li>
                <li>Employee Training Needs</li>
                <li>Post-Training Effectiveness</li>
                <li>Exit Interviews</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 col-sm-12">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x fa fa-graduation-cap" style="color:#3498db !important;"></i>
              <h2 class="heading-sm2">Academics</h2>
              <p class="para-updated">Administrators & Faculty use Insighto to gauge the students</p>
              <!--<p class="para-updated">Administrators, Faculty and Students use Insighto to succeed in their endeavours</p>-->
              <ul class="list-unstyled">
                <li>Course Content Feedback</li>
                <li>Course Delivery Assessment</li>
                <li>Student Needs Survey</li>
                <li>Student Satisfaction</li>
                <li>Performance Assessment</li>
                <li>Faculty Surveys</li>
                <li>Research Projects</li>
                <li>Parent Satisfaction</li>
                <li>Dissertation Research</li>
                <li>Facilities Survey</li>
              </ul>
            </div>
          </div>
        </div>
        <!-- End Service Blcoks -->
      </div>
    </div>
  </div>
  <div class="bg-grey content-sm pricing-page-intro" data-scroll-index='4' id="pricing">
    <div class="container text-center">
      <h2 class="title-v2 title-center" style="color:#1035A2 !important;">A PRICING PLAN TO SUIT YOUR UNIQUE NEEDS</h2>
      <p class="space-lg-hor para-updated" style="font-size: 15px;">Insighto Surveys are available on demand and as per your budget</p>
      <br>
      <div class="row">
        <div class="col-md-6">
          <div class="service-block service-block-sea service-or">
            <div class="service-bg"></div>
            <h2 class="heading-md pricing-color"><a href="pricing.aspx">Pay Per Survey Plans</a></h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="service-block service-block-blue service-or">
            <div class="service-bg"></div>
            <h2 class="heading-md pricing-color"><a href="pricing.aspx">Premium Annual Plan</a></h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <p class="para-updated-box" style="font-size:16px !important; margin-top:30px !important; text-align:center !important;"> Sign up for <a href="registration.aspx"><strong><u>Free plan</u></strong></a> (No Credit cards needed) and Upgrade anytime to any plan of your choice. </p>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </div>
  <div class="content-sm pricing-page-intro" data-scroll-index="4">
    <div class="container text-center">
      <h2 class="title-v2 title-center" style="color:#1035A2 !important;">SURVEY SERVICES</h2>
      <p class="space-lg-hor para-updated" style="font-size: 15px;">Don’t let your lack of time or skill to create surveys abstain you from useful insights. <br>
        Let Insighto Experts build powerful surveys for you.</p>
      <div> <a href="survey-services.aspx" class="btn-u" target="_blank">Know more on Professional Services</a> </div>
    </div>
  </div>
  <div class="bg-color-light">
    <div class="container content-md">
      <div class="headline-center margin-bottom-60">
        <h2 class="title-v2 title-center" style="color:#1035A2 !important;">CUSTOMER SPEAK</h2>
      </div>
      <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <div class="testimonials-v4 md-margin-bottom-50">
            <div class="testimonials-v4-in">
              <p>We used Insighto.com to research customers for an important business pitch. Their survey engine is very easy to use – my team was not only able to build the questionnaire with ease but also analyze the results in real time. Extracting consumer insights from the data was a breeze with Insighto.com and helped us win high appreciation from our Client.</p>
              <div align="right"><img src="../assets/img/testimonials/testimonial-client1.png" alt="" class="" width="178" height="25"></div>
              <br>
            </div>
            - Vice President, Data Analytics, Research, Tools. MudraMax Media </span> </div>
        </div>
        <div class="col-sm-2"></div>
      </div>
    </div>

    <!-- Testimonial Section -->
  </div>
  <div class="one-page-inner one-blue">
    <div class="container content">
      <div class="row">
        <h2 class="title-v2 title-center" style="color:#1035A2 !important;">THESE FOLKS LOVE US. SO WILL YOU!</h2>
        <div class="client-list-item">
          <ul>
            <li><img src="../assets/img/clients/1.jpg" alt=""></li>
            <li><img src="../assets/img/clients/2.jpg" alt=""></li>
            <li><img src="../assets/img/clients/3.jpg" alt=""></li>
            <li><img src="../assets/img/clients/4.jpg" alt=""></li>
            <li><img src="../assets/img/clients/17.jpg" alt=""></li>
            <li><img src="../assets/img/clients/10.jpg" alt=""></li>
            <li><img src="../assets/img/clients/20.jpg" alt=""></li>
            <li><img src="../assets/img/clients/19.jpg" alt=""></li>
            <li><img src="../assets/img/clients/18.jpg" alt=""></li>
            <li><img src="../assets/img/clients/5.jpg" alt=""></li>
            <li><img src="../assets/img/clients/16.jpg" alt=""></li>
            <li><img src="../assets/img/clients/15.jpg" alt=""></li>
            <li><img src="../assets/img/clients/7.jpg" alt=""></li>
            <li><img src="../assets/img/clients/9.jpg" alt=""></li>
            <li><img src="../assets/img/clients/11.jpg" alt=""></li>
            <li><img src="../assets/img/clients/14.jpg" alt=""></li>
            <li><img src="../assets/img/clients/8.jpg" alt=""></li>
            <li><img src="../assets/img/clients/6.jpg" alt=""></li>
            <li><img src="../assets/img/clients/13.jpg" alt=""></li>
            <li><img src="../assets/img/clients/12.jpg" alt=""></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

 
</div>

</asp:Content>