﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="survey-services.aspx.cs" Inherits="new_surveys_survey_services" MasterPageFile="~/Home.Master"%>
<%@ Register TagPrefix="uc" TagName="SurveysHeader" Src="~/UserControls/SurveysHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc:SurveysHeader ID="SurveysHeader" runat="server" />
<div class="wrapper">

  <div class="breadcrumbs-v3 img-v1 text-center">
    <div class="container">
   <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;">
        <span>Survey Services </span></h2>
    </div>
  </div>
  
  <div class="bg-blue-light-more content-sm-new">
    <div class="container">
      <div class="row">
        <div class="headline-center margin-bottom-20">
          <!--<h2 class="title-v2 title-center" style="color:#1035A2 !important;">It’s a better way to do surveys.</h2>-->
          <p class="para-updated">Insighto offers professional assistance to create and deploy surveys, which get the feedback you need. With years of experience in Industry, our survey professionals have gained technical expertise in survey creation, deployment and reporting. Using best practices, they help you design and develop surveys that will garner maximum responses. Subsequently, our reporting tools will facilitate in-depth analysis resulting in insights that matter.</p>
        </div>
      </div>
    </div>
  </div>
  <br>
  <div class="container">
    <div class="row">
      
      <div class="col-md-7">
        <div class="text-center margin-bottom-50 margin-top-20">
          <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Our Range Of Services</h2>
        </div>
        <div class="row category margin-bottom-20">
          <div class="col-md-12 col-sm-6">
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"> <i class="icon-ask"></i><!--<i class="icon-custom glyphicon glyphicon-export"></i>-->
              <div class="content-boxes-in-v3">
                <h3 class="para-updated" style="color: #1035A2 !important;"><b>Survey Questionnaire Creation</b></h3>
                <p class="para-updated">Send us your questions and let our experts develop a professional survey. Our survey professionals work closely with you to develop, review and test your surveys before deployment.</p>
              </div>
            </div>
            
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="icon-customize"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated" style="color: #1035A2 !important;"><b>Customized Branding</b></h3>
                <p class="para-updated">If you require more than a professional survey, we have a solution for you too. Our design team can work with you to brand your survey pages as per your requirement and integrate them seamlessly so you can leverage on your brand for your survey or vice-versa.</p>
              </div>
            </div>
            
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="icon-language"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated" style="color: #1035A2 !important;"><b>Survey Deployment</b></h3>
                <p class="para-updated">After your survey has been designed and developed, we can assist you in deploying the survey. Our delivery team helps you design email invites with survey links and reach your target database.</p>
              </div>
            </div>
            
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="icon-surveyreport"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated" style="color: #1035A2 !important;"><b>Survey Reports</b></h3>
                <p class="para-updated">Upon data collection, our team can drive the analytics and present specialized reports that will make the survey worth more than you had imagined.</p>
              </div>
            </div>
            
            <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="icon-admin"></i>
              <div class="content-boxes-in-v3">
                <h3 class="para-updated" style="color: #1035A2 !important;"><b>Survey Project Management</b></h3>
                <p class="para-updated">Our team of survey professionals engages in totality to create powerful surveys. They work closely with you from start to finish i.e., from survey creation, deployment, tracking responses, generating real-time reports, to deliver insightful results.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-4 bg-grey1">
        <div class="text-center margin-bottom-50 margin-top-20">
          <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Get in touch with us</h2>
          <p>To get your surveys done quickly and professionally initiate dialogue with our survey experts today. It’s a smarter way to do your survey.</p>
        </div>
        <form action="assets/php/sky-forms-pro/demo-contacts-process.php" method="post" id="sky-form3" class="sky-form contact-style" novalidate>
          <fieldset class="no-padding">
            <label>Name :</label>
            <div class="row sky-space-20">
              <div class="col-md-12 col-md-offset-0">
                <div>
                  <asp:TextBox ID="txtName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" SetFocusOnError="true" Display="Dynamic"
                            ErrorMessage="Please enter name." ControlToValidate="txtName" CssClass="alert-danger"></asp:RequiredFieldValidator>

                </div>
              </div>
            </div>
            <section>
              <label>Do you have subscription to Insighto.com?</label>
              <div class="inline-group">
                      <asp:RadioButtonList ID="rbtnListSubscription" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Yes" Value="Yes" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                        </asp:RadioButtonList>
              </div>
            </section>
            <label>E-mail Id</label>
            <div class="row sky-space-20">
              <div class="col-md-12 col-md-offset-0">
                <div>
                  <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmailId" runat="server" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="Please enter email id." ControlToValidate="txtEmailId"
                            CssClass="alert-danger"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="regEmailId" runat="server"
                            ErrorMessage="Please enter valid email id." ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                            CssClass="alert-danger" ControlToValidate="txtEmailId" Display="Dynamic">
                        </asp:RegularExpressionValidator>
                </div>
              </div>
            </div>
            <label>Phone Number </label>
            <div class="row sky-space-20">
              <div class="col-md-12 col-md-offset-0">
                <div>
                  <asp:TextBox ID="txtPhoneNo" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                </div>
              </div>
            </div>
            <label>Service required :</label>
            <div class="row sky-space-20">
              <div class="col-md-12 col-md-offset-0">
                <div>
                  <label class="select">
                   <asp:DropDownList ID="ddlService" runat="server" CssClass="dropdownService">
                            <asp:ListItem Value="">Choose any service</asp:ListItem>
                            <asp:ListItem>Survey Creation</asp:ListItem>
                            <asp:ListItem>Survey Reports</asp:ListItem>
                            <asp:ListItem>Customized Branding</asp:ListItem>
                            <asp:ListItem>Survey Deployment</asp:ListItem>
                            <asp:ListItem>Survey Project Management</asp:ListItem>
                        </asp:DropDownList>
                    <i></i> </label>
                     <asp:RequiredFieldValidator ID="rfvService" runat="server" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="Please select service." ControlToValidate="ddlService"
                            CssClass="alert-danger"></asp:RequiredFieldValidator>
                </div>
              </div>
            </div>
            <p>
              <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn-u" OnClick="btnSubmit_Click" />
            </p>
            <div class="successPanel" id="divSuccess" style="display: none" runat="server">
                    <div>
                        <%-- <label id="lblMessage" />--%>
                        <asp:Label ID="lblMessage" CssClass="alert-success" runat="server" 
                            Style="display: none;" meta:resourcekey="lblMessageResource1"></asp:Label>
                    </div>
                </div>
            <div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
<p>&nbsp;</p>

  
</div>
<!--/wrapper--> 
</asp:Content>