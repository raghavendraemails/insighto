﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="new_surveys_login" MasterPageFile="~/Home.Master"%>
<%@ Register TagPrefix="uc" TagName="SurveysHeader" Src="~/UserControls/SurveysHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc:SurveysHeader ID="SurveysHeader" runat="server" />
    <div> 
      <div class="breadcrumbs-v3 img-v1 text-center">
        <div class="container">
       <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;">
            <span>Login </span></h2>
        </div>
      </div>
      <div class="container content">
        <div class="row">
          <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <form class="reg-page">
              <div class="reg-header">
                <h2 style="color:#1035A2 !important;">GO TO YOUR ACCOUNT</h2>
              </div>
              <div class="input-group margin-bottom-20"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" ID="txtEmail" runat="server" class="form-control" placeholder="Email" size="40" />
              </div>
              <div class="input-group margin-bottom-20"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" ID="txtpassword1" maxlength="16" Placeholder="Password" runat="server" class="form-control"  size="40" />
              </div>
              <div class="row">
                <div class="col-md-12 checkbox">
                  <p>
                        By signing up, you agree to the <a href="../termsofuse.aspx">Terms & Conditions</a> and the <a href="../privacy-policy.aspx">Privacy Policy</a></p>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12" align="left">
                  <asp:Button ID="BTNSIGNIN" runat="server" Text="Login" CssClass="btn-u" onclick="BTNSIGNIN_Click" />
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" align="left">
             <asp:Label ID="lblInvalidAlert" class="lblRequired alert-danger" runat="server" Visible="False"
                    Text="Invalid User Name or Password" ></asp:Label>
</div></div>

              <hr/>
                  <h4>Forgot your Password ?</h4>
                <%--<p>No worries, <a class="color-green" data-toggle="modal" data-target="#modal" href="/ForgotPassword.aspx">Click here</a> to reset your password.</p>--%>
                <p>No worries, <a class="color-green" href="/ForgotPassword.aspx">Click here</a> to reset your password.</p>
            </form>
          </div>
        </div>
        <!--/row--> 
      </div>
  
  
    </div>
<!--/wrapper--> 

</asp:Content>