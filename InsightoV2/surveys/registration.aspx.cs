﻿using System;
using App_Code;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Text.RegularExpressions;
using System.Data;
using System.Web;

public partial class new_surveys_registration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void signup_free_Click(object sender, EventArgs e)
    {
        lblvalidmsg.Visible = false;
        lblErrorMsg.Visible = false;
        lblvalidmsgemail.Visible = false;
        lblvalidmsgpwd.Visible = false;
        lblvalidmsgrepwd.Visible = false;
        
        //signin_menu.Style.Add("display", "none");

        if (txtEmail.Text.Contains(","))
        {
            lblvalidmsgemail.Text = "Enter valid Email";
            lblvalidmsg.Visible = true;
        }
        else if (txtName.Text == "")
        {
            lblvalidmsg.Text = "Enter Name";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";
            lblvalidmsg.Visible = true;
        }
        else if (!Isvalidname(txtName.Text))
        {
            lblvalidmsg.Text = "Please enter letters a-z and A-Z only";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";
            lblvalidmsg.Visible = true;
        }
        else if (txtEmail.Text == "")
        {

            lblvalidmsgemail.Text = "Enter email";
            lblvalidmsg.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";
            lblvalidmsgemail.Visible = true;
        }
        else if (!IsValidEmailAddress(txtEmail.Text))
        {
            lblvalidmsgemail.Text = "Enter valid Email";
            lblvalidmsg.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";
            lblvalidmsg.Visible = true;
        }
        else if (!IsPasswordStrong(txtpassword1.Text))
        {
            lblvalidmsgpwd.Text = "Password should contain 6-16 characters";
            lblvalidmsgemail.Text = "";
            lblvalidmsg.Text = "";
            lblvalidmsgrepwd.Text = "";
            lblvalidmsgpwd.Visible = true;
        }
        else if (txtrepassword.Text == "")
        {
            lblvalidmsgpwd.Text = "Enter Password";
            lblvalidmsgemail.Text = "";
            lblvalidmsg.Text = "";
            lblvalidmsgrepwd.Text = "";
            lblvalidmsgpwd.Visible = true;
        }
        else if (txtrepassword.Text == "")
        {
            lblvalidmsgrepwd.Text = "Confirm Password";
            lblvalidmsg.Text = "";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Visible = true;
        }
        else if (txtpassword1.Text != txtrepassword.Text)
        {
            lblvalidmsgrepwd.Text = "Confirmed Password not matching Password";
            lblvalidmsg.Text = "";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Visible = true;
        }
        else
        {
            var userService = new UsersService();

            string ipAddress = string.Empty;
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
            var user = userService.RegisterFreeUser(txtName.Text.Trim(), string.Empty, txtEmail.Text.Trim(), string.Empty, 0, ipAddress, txtpassword1.Text.Trim());
            country = Utilities.GetCountryName(ipAddress);
            ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
            ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
            country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();

            if (user == null)
            {

                lblErrorMsg.Text = "You have already signed up! Just login";
                lblErrorMsg.Visible = true;
            }
            else
            {
            }
            //end of subaccount creation

            if (Request.Cookies["Partner"] != null)
            {

                HttpCookie aCookie = Request.Cookies["Partner"];
                strsource = Server.HtmlEncode(aCookie.Values["utm_source"]);
                strterm = "";
                DataSet dsRefby = surcore.updateReferredby(user.USERID, strsource, strterm);

                Response.Cookies["Partner"].Expires = DateTime.Now.AddDays(-730);
            }


            if (user == null)
            {

                lblErrorMsg.Text = "You have already signed up! Just login";
                lblErrorMsg.Visible = true;
                //dvErrMsg.Visible = true;
                //lblErrMsg.Visible = false;
            }
            else
            {
                ServiceFactory.GetService<SessionStateService>().SaveUserToSession(user);

                if (ckbShare.Checked)
                {
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.Getsocialnetwork(), "UserId=" + user.USERID));
                }
                else
                {
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=0"));
                }
            }
        }

    }

    string country;
    string strsource;
    string strterm;
    SurveyCore surcore = new SurveyCore();
    PollCreation Pc = new PollCreation();
    public bool IsPasswordStrong(string p)
    {
        if (string.IsNullOrEmpty(p))
            return false;
        else
        {
            var regex = new Regex("^.{6,16}$");
            return regex.IsMatch(p) && !p.EndsWith(".");
        }
    }

    public bool Isvalidname(string a)
    {
        if (string.IsNullOrEmpty(a))
            return false;
        else
        {
            var regex = new Regex("^[a-zA-Z ]*$");
            return regex.IsMatch(a) && !a.EndsWith(".");
        }

    }

    public bool IsValidEmailAddress(string s)
    {
        if (string.IsNullOrEmpty(s))
            return false;
        else
        {
            var regex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            return regex.IsMatch(s) && !s.EndsWith(".");
        }
    }


}