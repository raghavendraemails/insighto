﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="registration.aspx.cs" Inherits="new_surveys_registration" MasterPageFile="~/Home.Master" %>
<%@ Register TagPrefix="uc" TagName="SurveysHeader" Src="~/UserControls/SurveysHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc:SurveysHeader ID="SurveysHeader" runat="server" />
    <div>

      <div class="breadcrumbs-v3 img-v1 text-center">
        <div class="container">
       <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;">
            <span>SIGN UP </span></h2>
        </div>
      </div>
      <div>
        <div class="container content">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="reg-header">
                  <h2 style="color:#1035A2 !important;">Register a new account</h2>
                  <p>Already Signed Up? Click <a href="login.aspx" class="color-green">Sign In</a> to login your account.</p>
                </div>
                <label>Name</label>
                <%--<input type="text" class="form-control margin-bottom-20">--%>
                <asp:TextBox ID="txtName" Text="" runat="server" CssClass="form-control"/>
                <asp:Label ID="lblvalidmsg" runat="server" CssClass="margin-bottom-20" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/><br />
                <%--<label>Last Name</label>
                <input type="text" class="form-control margin-bottom-20">--%>
                <label>Email Address <span class="color-red">*</span></label>
                <%--<input type="text" class="form-control margin-bottom-20">--%>
                <asp:TextBox ID="txtEmail" Text="" runat="server" CssClass="form-control"/>
                <asp:Label ID="lblvalidmsgemail" runat="server"  CssClass="margin-bottom-20" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/><br />
                <div class="row">
                  <div class="col-sm-6">
                    <label>Password <span class="color-red">*</span></label>
                    <%--<input type="password" class="form-control margin-bottom-20">--%>
                        <!--<asp:TextBox ID="txttempwd" Text="Password" runat="server"  CssClass="form-control margin-bottom-20"/> -->
                        <asp:TextBox ID="txtpassword1" MaxLength="16" TextMode="password" Text="" runat="server" CssClass="form-control"/>
                        <asp:Label ID="lblvalidmsgpwd" runat="server" CssClass="margin-bottom-20" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/>

                  </div>
                  <div class="col-sm-6">
                    <label>Confirm Password <span class="color-red">*</span></label>
                    <%--<input type="password" class="form-control margin-bottom-20">--%>
                        <!--<asp:TextBox ID="txttemrpwd" Text="Re-type Password" runat="server"  CssClass="form-control margin-bottom-20"/> -->
                        <asp:TextBox ID="txtrepassword" TextMode="password" Text="" runat="server"  CssClass="form-control"/>
                            <asp:Label ID="lblvalidmsgrepwd" runat="server"  CssClass="margin-bottom-20" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/>
                  </div>
                </div>
                <div class="row margin-top-20">
            	    <div class="col-md-12">
                    <div>By signing up, you agree to the <a href="../termsofuse.aspx" target="_blank">Terms &amp; Conditions</a> and the <a href="../privacy-policy.aspx" target="_blank">Privacy Policy</a></div>
                  </div>
                </div>
                <br>

                <div class="row">
                  <div class="col-md-12">
                      <asp:CheckBox runat="server" ID="ckbShare" Checked="true"/>
                      <span style="font-weight:normal !important;">Share & take a 30 day Free Trial</span> </label>
                      <br><br>
                      <asp:Button ID="signup_free" runat="server" Text="Go to  Insighto Surveys" Class="btn-u btn btn-lg"  onclick="signup_free_Click"/>
                  </div>
                <div class="row">
            	    <div class="col-md-12">
                    <asp:Label ID="lblErrorMsg" ForeColor="Red" CssClass="lblRequired" Font-Size="12px" runat="server" Visible="false"></asp:Label>
                    
                  </div>
                </div>
                <br>
            </div>
            </div>
        </div>
        <!--/container-->
        <script type="text/javascript">
            $(document).ready(function () { $("#lisignup").attr("class", "active"); });
        </script>
        <!-- CSS Page Style -->
        <link rel="stylesheet" href="../assets/css/pages/page_pricing.css">
      </div>


    </div>
    <!--/wrapper-->
</asp:Content>