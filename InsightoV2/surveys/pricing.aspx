﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pricing.aspx.cs" Inherits="new_surveys_pricing" MasterPageFile="~/Home.Master"%>
<%@ Register TagPrefix="uc" TagName="SurveysHeader" Src="~/UserControls/SurveysHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc:SurveysHeader ID="SurveysHeader" runat="server" />
    <div class="wrapper"> 

     <div class="breadcrumbs-v3 img-v1 text-center">
        <div class="container">
       <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;">
            <span>Survey Plans </span></h2>
        </div>
      </div>
  
  
      <!--=== Content Part ===-->
      <div class="bg-grey content-sm pricing-page-intro">
        <div class="container text-center">
          <p class="space-lg-hor" style="font-size: 15px; margin-top:0px;">
          Your chance to experience Insighto Surveys free, for 30 days. Share with your friends and you will automatically have access to Premium Plan. <br>
          You may select the Plan that suits you best after the free trial.
          </p>
          <a href="registration.aspx" class="btn-u">SIGN UP FOR A 30 DAY FREE TRIAL</a></div>
      </div>
  
      <!--=== Content Part ===-->
      <div id="plans" class="container content">
        <div class="row">
		    <div class="col-md-3"><h3 align="center" style="color:#1035A2 !important;">Free For Life</h3></div>
            <div class="col-md-6"><h3 align="center" style="color:#1035A2 !important;">Pay As You Go</h3></div>
            <div class="col-md-3"><h3 align="center" style="color:#1035A2 !important;">BEST</h3></div>    
        </div>
	    <div class="row">
		    <div class="col-md-3"><div class="dark-box"><h3 style="color:#FFF !important;">Ideal for Explorers</h3></div></div>
            <div class="col-md-6"><div class="dark-box"><h3 style="color:#FFF !important;">Ideal for Occasional Users</h3></div></div>
            <div class="col-md-3"><div class="dark-box"><h3 style="color:#FFF !important;">for Power Users</h3></div></div>    
        </div>
        <div class="row margin-bottom-10 pricing-rounded">
          <div class="col-md-3">
            <div class="pricing hover-effect">
              <div class="pricing-head">
                <h3 style="background:#1E73BE;text-shadow:none; font-size:20px !important; font-weight:normal !important;">Basic Free</h3>
                <h4><span id="spprof" style="font-size:35px;margin-top:22px;"><i class="fa fa-<%=currency %>"></i> 0</span><span style="margin-top:0px;">Lifetime </span></h4>
                <div align="center"><a href="registration.aspx" class="btn-u-blue">SIGN UP</a></div> <br>
                <div style="font-weight:bold; ">&nbsp; <br> &nbsp; <br> &nbsp; </div>
                <hr>
                <div><a href="#"><b>View all features</b></a></div><br>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="pricing hover-effect">
              <div class="pricing-head">
                <h3 style="background:#1E73BE;text-shadow:none; font-size:20px !important; font-weight:normal !important;">Pro-Single</h3>
                <h4><span id="spprof" style="font-size:35px;margin-top:22px;"><i class="fa fa-<%=currency %>"></i> <%=prosingleAmt%></span><span style="margin-top:0px;"> Per Survey </span></h4>
                <div align="center">
                    <asp:Button ID="btnpro1" Text="BUY NOW" runat="server" CssClass="btn-u-blue"  onclick="btnpro1_Click"  />
                </div> <br>
                <div style="font-weight:bold; ">Features of Basic<br> + <br> 500 responses / 1 survey</div>
                <hr>
                <div><a href="#"><b>View all features</b></a></div><br>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="pricing hover-effect">
              <div class="pricing-head">
                <h3 style="background:#1E73BE;text-shadow:none; font-size:20px !important; font-weight:normal !important;">Premium-Single</h3>
                <h4><span id="spBus" style="font-size:35px;margin-top:22px;"><i class="fa fa-<%=currency %>"></i> <%=premsingleAmt%></span><span style="margin-top:0px;">Per Survey  </span></h4>
                <div align="center">
                    <asp:Button ID="btnppsprem" Text="BUY NOW" runat="server" CssClass="btn-u-blue"  onclick="btnppsprem_Click"  />

                </div> <br>
                <div style="font-weight:bold; ">Features of Pro-Single <br> + <br> 1,000 responses / 1 survey</div>
                <hr>
                <div><a href="#"><b>View all features</b></a></div><br>
              </div>
            </div>
        
          </div>
          <div class="col-md-3">
            <div class="pricing hover-effect">
              <div class="pricing-head">
                <h3 style="background:#1E73BE;text-shadow:none; font-size:20px !important; font-weight:normal !important;">Premium Annual</h3>
                <h4><span id="spPub" style="font-size:35px;margin-top:22px;"><i class="fa fa-<%=currency %>"></i> <%=premannualAmt%></span><span style="margin-top:0px;">Per Year </span></h4>
                <div align="center">
                    <asp:Button ID="btnpremium1" Text="BUY NOW" runat="server" CssClass="btn-u-blue"  onclick="btnpremium1_Click" />
                </div>
                <br>
                <div style="font-weight:bold; ">Features of Premium-Single <br> + <br> Unlimited No. of surveys</div>
                <hr>
                <div><a href="#"><b>View all features</b></a></div><br>
              </div>
            </div>
          </div>
        </div>
        <!--/row--> 

      </div>
      <br>
      <div class="container">
          <div class="row">
      	    <div class="row no-space-pricing pricing-mega-v1">
                <div class="col-md-4 col-sm-6 hidden-sm hidden-xs">
                    <div class="pricing hidden-area">
                        <div class="pricing-head ">
                            <h4 class="price">Choose Plan</h4>
                        </div>
                        <ul class="pricing-content list-unstyled">
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Questions Per Survey
                            </li>
                            <li>
                                <i class="fa fa-arrow-right"></i>Number of Responses
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Number of Surveys
                            </li>
                            <li>
                                <i class="fa fa-arrow-right"></i>Number of question types
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Emails Limit
                            </li>
                            <li class="bg-color-purple">
                                <span style="color:#FFF !important;">Survey Basics</span>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Pre-defined  Survey templates
                            </li>
                            <li>
                                <i class="fa fa-arrow-right"></i>Make response mandatory
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Randomize answer options
                            </li>
                            <li>
                                <i class="fa fa-arrow-right"></i>Reorder questions
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Add or Edit questions / answers
                            </li>
                            <li>
                                <i class="fa fa-arrow-right"></i>Survey preview
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Skip logic / Branching
                            </li>
                            <li class="bg-color-purple">
                                <span style="color:#FFF !important;">Question types</span>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>21 Question Types
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Multi-media based Questions
                            </li>
                            <li class="bg-color-purple">
                                <span style="color:#FFF !important;">Design types</span>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Customizable Look & Feel of Survey
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Customize survey end messages
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Remove Branding (Insighto) from Survey
                            </li>
                            <li class="bg-color-purple">
                                <span style="color:#FFF !important;">Launch</span>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Collect responses through Insighto emails
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Collect responses through weblink
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Schedule survey launch time
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Address book
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>CSV file upload
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>MS Excel file upload
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Copy introduction as email invite
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Send reminders to non-respondents
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Redirection of URL
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Password protected survey
                            </li>
                             <li class="bg-color-purple">
                                <span style="color:#FFF !important;">Survey reports</span>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Display data and charts
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Basic Cross tab
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Export to PowerPoint
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Export to Word
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Export to Excel
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Export cross tab report to excel
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Raw data export
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>View individual responses
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Exclude/restore completed responses
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Track / Display respondent email address
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>View partial/incomplete responses
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-arrow-right"></i>Exclude/restore partials to completes
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 block">
                    <div class="pricing">
                        <div class="pricing-head">
                            <h3 style="font-size:20px !important; font-weight:normal !important;">
                                Basic
                            </h3>
                            <h4 class="price">
                                <i class="fa fa-<%=currency %>"></i> <i>0</i>
                                <span class="hidden-md hidden-lg">Per Month</span>
                            </h4>
                        </div>
                        <ul class="pricing-content list-unstyled ">
                            <li class="bg-color">
                                Unlimited
                            </li>
                            <li>
                                100
                            </li>
                            <li class="bg-color">
                                Unlimited
                            </li>
                            <li>
                                21
                            </li>
                            <li class="bg-color">
                                NA
                            </li>
                            <li>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-times"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-times"></i>
                            </li>
                            <li class="bg-color">
                                
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-times"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-times"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-times"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-times"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-times"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                        </ul>
                        <br>
                        <div align="center"><a href="registration.aspx" class="btn-u-blue">SIGN UP</a></div>
                        <br>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 block">
                    <div class="pricing">
                        <div class="pricing-head">
                            <h3 style="font-size:20px !important; font-weight:normal !important;">
                                Pro-Single <!--<span>Officia deserunt mollitia</span>-->
                            </h3>
                            <h4 class="price">
                                 <i class="fa fa-<%=currency %>"></i> <i><%=prosingleAmt %></i>
                            </h4>
                        </div>
                        <ul class="pricing-content list-unstyled ">
                            <li class="bg-color">
                                Unlimited
                            </li>
                            <li>
                                500 / Survey
                            </li>
                            <li class="bg-color">
                                1
                            </li>
                            <li>
                                21
                            </li>
                            <li class="bg-color">
                                5000
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-times"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-times"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                            <li>
                                <i class="fa fa-times"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-times"></i>
                            </li>
                        </ul>
                        
                        <br>
                        <div align="center">
                            <asp:Button ID="Button3" Text="BUY NOW" runat="server" CssClass="btn-u-blue"  onclick="btnpro1_Click" />
                        </div>
                        <br>
                    </div>
                </div>
                 <div class="col-md-2 col-sm-6 block">
                    <div class="pricing">
                        <div class="pricing-head">
                            <h3 style="font-size:20px !important; font-weight:normal !important;">
                                Premium-Single
                            </h3>
                            <h4 class="price">
                                <i class="fa fa-<%=currency %>"></i> <i><%=premsingleAmt %></i>
                            </h4>
                        </div>
                        <ul class="pricing-content list-unstyled ">
                            <li class="bg-color">
                                Unlimited
                            </li>
                            <li>
                                1000 / Survey
                            </li>
                            <li class="bg-color">
                                1
                            </li>
                            <li>
                                21
                            </li>
                            <li class="bg-color">
                                Unlimited
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                        </ul>
                        <br>
                        <div align="center">
                            <asp:Button ID="Button1" Text="BUY NOW" runat="server" CssClass="btn-u-blue"  onclick="btnppsprem1_Click" />
                        </div>
                        <br>
                    </div>
                </div>
                 <div class="col-md-2 col-sm-6 block">
                    <div class="pricing">
                        <div class="pricing-head">
                            <h3 style="font-size:20px !important; font-weight:normal !important;">
                                Annual
                            </h3>
                            <h4 class="price">
                                <i class="fa fa-<%=currency %>"></i> <i><%=premannualAmt %></i>
                                <span class="hidden-md hidden-lg">Per Month</span>
                            </h4>
                        </div>
                        <ul class="pricing-content list-unstyled ">
                            <li class="bg-color">
                                Unlimited
                            </li>
                            <li>
                                Unlimited
                            </li>
                            <li class="bg-color">
                                Unlimited
                            </li>
                            <li>
                                21
                            </li>
                            <li class="bg-color">
                                Unlimited
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li></li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                            </li>
                            <li class="bg-color">
                                <i class="fa fa-check"></i>
                            </li>
                        </ul>
                        
                        <br>
                        <div align="center">
                            <asp:Button ID="Button2" Text="BUY NOW" runat="server" CssClass="btn-u-blue"  onclick="btnpremium1_Click" />
                        </div>
                        <br>
                    </div>
                </div>
            </div>
          </div>
      </div>
      <!--/container--> 
      <!--=== End Content Part ===--> 
  

    </div>
    <!--/wrapper--> 
</asp:Content>