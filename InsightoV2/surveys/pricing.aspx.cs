﻿using System;
using System.Collections;
using System.Configuration;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;

public partial class new_surveys_pricing : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int userId = 0;
    string transType = "New";
    public string surveytype = "";
    public int surveyID;
    string countryName;
    private string licenseType = "";
    //LoggedInUserInfo loggedInUserInfo;
    string strsource;
    string strterm;
    string currurl;
    string querystring;
    private double prodisprice;
    private double premdisprice;

    string strproprice = ConfigurationManager.AppSettings["proprice"];
    string strpremiumprice = ConfigurationManager.AppSettings["premiumprice"];
    private string utmsource;
    private string utmterm;
    string status = "";
    public string BasicAmt;
    public string prosingleAmt;
    public string premsingleAmt;
    public string premannualAmt;
    public string currency = "inr";
    SurveyCore surcoe = new SurveyCore();


    protected void Page_Load(object sender, EventArgs e)
    {


        string ipAddress = "";
        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
        {
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        }
        //ipAddress = "62.210.206.25"; //FR
        countryName = Utilities.GetCountryName(ipAddress);
        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(countryName);

        countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();



        if (countryName.ToUpper() != "INDIA")
        {

            // countryName = "AC";
            BasicAmt = "0";
            prosingleAmt = "9";
            premsingleAmt = "29";
            premannualAmt = "249";
            currency = "dollar";
        }
        else
        {
            // &#x20b9;
            //&#8377;

            BasicAmt = "" + "0";
            prosingleAmt = "" + "399";
            premsingleAmt = "" + "1,299";
            premannualAmt = "" + "9,999";
            currency = "inr";

        }


        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (typeHt.Contains("Type"))
                licenseType = typeHt["Type"].ToString();
            if (typeHt.Contains(Constants.USERID))
                userId = ValidationHelper.GetInteger(typeHt[Constants.USERID].ToString(), 0);
            if (typeHt.Contains("Flag"))
                transType = Convert.ToString(typeHt["Flag"]);
            if (typeHt.Contains("utm_source"))
                utmsource = Convert.ToString(typeHt["utm_source"]);
            if (typeHt.Contains("Survey_type"))
                surveytype = Convert.ToString(typeHt["Survey_type"]);
            if (typeHt.Contains("SurveyId"))
                surveyID = Convert.ToInt32(typeHt["SurveyId"]);
            if (typeHt.Contains("status"))
                status = typeHt["status"].ToString();

        }
    }




    protected void button_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/In/SignUpFree.aspx");
    }

    protected void btnpro1_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        {
            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }

            //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes";
            //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
                //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
                string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&FromPartner=No";
                //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
                string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&FromPartner=No" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }

    protected void btnppsprem_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        {

            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes&surveytype=" + surveytype + "&SurveyId=" + surveyID + "&status=" + status;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes&surveytype=" + surveytype + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&FromPartner=No&surveytype=" + surveytype + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }

    protected void btnpremium1_Click(object sender, EventArgs e)
    {
        if (querystring != "")

        {

            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&FromPartner=No" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }

    protected void button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/In/SignUpFree.aspx");
    }

    protected void button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/In/SignUpFree.aspx");
    }

    protected void btnpro3_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        {
            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }

            //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes";
            //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
                //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
                string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&FromPartner=No";
                //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
                string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&FromPartner=No" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }

    protected void btnpro2_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        {
            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }

            //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes";
            //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
                //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
                string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&FromPartner=No";
                //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
                string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&FromPartner=No" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }

    protected void btnppsprem1_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        {

            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes&surveytype=" + surveytype + "&SurveyId=" + surveyID + "&status=" + status;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes&surveytype=" + surveytype + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&FromPartner=No&surveytype=" + surveytype + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }

    protected void btnppsprem2_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        {

            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes&surveytype=" + surveytype + "&SurveyId=" + surveyID + "&status=" + status;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes&surveytype=" + surveytype + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&FromPartner=No&surveytype=" + surveytype + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }

    protected void btnpremium3_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        {

            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&FromPartner=No" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }

    protected void btnpremium2_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        {

            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&FromPartner=No" + "&SurveyId=" + surveyID + "&status=" + status;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }
}