﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using App_Code;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;
using System.Data;
using Resources;
using Insighto.Business.Helpers;
using System.Collections;
using Insighto.Data;

public partial class new_surveys_login : System.Web.UI.Page
{
    SurveyCore surcore = new SurveyCore();
    int userId = 0;
    public string abovepanel;
    public string withinpanel;
    osm_user user;
    List<osm_user> listuser;
    int activationflag = -1;
    bool ispollcampaign = false;
    bool iseligiblefor60dayfreetrial = false;
    PollCreation Pc;
    bool freepolltrialfor60days = true;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void BTNSIGNIN_Click(object sender, EventArgs e)
    {
        DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        DateTime dt1 = new DateTime(2015, 8, 31);


        if (!IsPostBack)
            return;



        if (Request.QueryString["key"] != null)
        {
            Hashtable ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 && ht.Contains("UserId"))
            {
                int userId = ValidationHelper.GetInteger(ht["UserId"].ToString(), 0);
                UsersService userService = new UsersService();
                var user = userService.FindUserByUserId(userId);
                userService.ProcessUserLogin(user);

                //Timezone is null for first user login. hence redirecting to welcome page
                if (user.ACTIVATION_FLAG == null || user.ACTIVATION_FLAG == 0)
                {
                    userService.ActivateUser(userId);
                    // Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=" + SurveyFlag));
                }
            }
            if (ht != null && ht.Count > 0 && ht.Contains("AlternateId"))
            {
                int Id = ValidationHelper.GetInteger(ht["AlternateId"].ToString(), 0);
                UsersService userService = new UsersService();
                var user = userService.ActivateAlternateUser(Id);
            }
        }


        string alrt = "Please enter";
        this.lblInvalidAlert.Visible = true;
        bool flag = false;
        if (string.IsNullOrEmpty(txtEmail.Value.Trim()) || txtEmail.Value == "Email")
        {
            alrt += " email address.";
            lblInvalidAlert.Text = alrt;
        }
        else
        {
            Regex reLenient = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            bool isEmail = reLenient.IsMatch(txtEmail.Value);
            if (!isEmail)
            {
                alrt += " valid email address.";
                lblInvalidAlert.Text = alrt;
            }
            else if (Convert.ToString(txtpassword1.Value).Length == 0)
            {
                alrt += " password.";
                lblInvalidAlert.Text = alrt;
            }
            flag = true;
        }

        if (flag && Convert.ToString(txtEmail.Value).Length > 0 && Convert.ToString(txtpassword1.Value).Length > 0)
        {
            // To check authenticated user       
            var userService = ServiceFactory.GetService<UsersService>();
            var user = userService.getAuthenticatedUser(txtEmail.Value, txtpassword1.Value);
            if (user == null)
            {
                lblInvalidAlert.Text = "Invalid Email Address or Password.";
                lblInvalidAlert.Visible = true;

                return;
            }
            else if (Utilities.ToEnum<SurveyStatus>(user.STATUS) == SurveyStatus.InActive || ValidationHelper.GetInteger(user.DELETED, 0) == 1)
            {
                lblInvalidAlert.Text = "Your Account is de-activated.";
                lblInvalidAlert.Visible = true;
                return;
            }
            //else if (user.ACTIVATION_FLAG == 0 || user.ACTIVATION_FLAG == null)
            //{
            //    lblInvalidAlert.Text = " Your Account is not activated.";
            //    lblInvalidAlert.Visible = true;
            //    return;



            //}
            else
            {
                userService.ProcessUserLogin(user);
                //if (IsEmailCampaign.Value == "1")
                //{
                ispollcampaign = false;
                //}
                DataSet dsactivity = surcore.getPollRespondentLimit(user.USERID);
                if (dsactivity.Tables[0].Rows[0]["eligiblefor60dayfreepolltrial"].ToString() == "1")
                {
                    iseligiblefor60dayfreetrial = true;
                }

                if (dsactivity.Tables[0].Rows[0]["freepolltrialfor60days"].ToString() != "1")
                {
                    freepolltrialfor60days = false;
                }
                if (iseligiblefor60dayfreetrial == true && freepolltrialfor60days == false && ispollcampaign == true && (dt < dt1))
                {
                    Pc.UpdatePollCampaignUser(user.USERID);
                    if (user.TIME_ZONE == null) //Timezone is null for first user login. hence redirecting to welcome page
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    }
                    else {
                        Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                        Response.Redirect(EncryptHelper.EncryptQuerystring("/Polls/MyPolls.aspx", "surveyFlag=" + user.SURVEY_FLAG));
                    }
                }

                if (user.TIME_ZONE == null) //Timezone is null for first user login. hence redirecting to welcome page
                {
                    if (dsactivity.Tables[3].Rows[0][0].ToString() == "survey")
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    }
                    else
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    }
                }
                else if (user.RESET == 1) // Password reset value is 1 for first login. hence redirectiong to chanepassword page
                {
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserChangePasswordPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                }
                else // on successful login user is redirected to Myaccounts page
                {
                    if (dsactivity.Tables[3].Rows[0][0].ToString() == "survey")
                    {
                        Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    }
                    else
                    {
                        Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                        Response.Redirect(EncryptHelper.EncryptQuerystring("/Polls/MyPolls.aspx", "surveyFlag=" + user.SURVEY_FLAG));
                    }

                }
            }

        }
        else
        {


        }



    }
}