﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using App_Code;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;
using System.Data;
using Resources;
using Insighto.Business.Helpers;

public partial class loginpage : System.Web.UI.Page
{
    SurveyCore surcore = new SurveyCore();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
   
    public bool IsPasswordStrong(string p)
    {
        if (string.IsNullOrEmpty(p))
            return false;
        else
        {
            var regex = new Regex("^.{6,16}$");
            return regex.IsMatch(p) && !p.EndsWith(".");
        }
    }

    public bool Isvalidname(string a)
    {
        if (string.IsNullOrEmpty(a))
            return false;
        else
        {
            var regex = new Regex("^[a-zA-Z ]*$");
            return regex.IsMatch(a) && !a.EndsWith(".");
        }

    }

    public bool IsValidEmailAddress(string s)
    {
        if (string.IsNullOrEmpty(s))
            return false;
        else
        {
            var regex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            return regex.IsMatch(s) && !s.EndsWith(".");
        }
    }
    protected void BTNSIGNIN_Click(object sender, EventArgs e)
    {
        //bool flag = false;
       if (txtEmail.Text.Trim() == "Email")
        {
            lblvalidmsgemail.Text = "Enter email";
            lblvalidmsgpwd.Visible = false;
            lblerrmsg.Visible = false;
            lblvalidmsgemail.Visible = true;
        }
        else if (!IsValidEmailAddress(txtEmail.Text.Trim()))
        {
            lblvalidmsgemail.Text = "Enter valid email";
            lblvalidmsgpwd.Visible = false;
            lblerrmsg.Visible = false;
            lblvalidmsgemail.Visible = true;
        }
       else if (txtpassword1.Text.Trim() == "Password" || txtpassword1.Text.Trim()=="")
       {
           lblvalidmsgpwd.Text = "Enter password";
           lblvalidmsgemail.Visible = false;
           lblerrmsg.Visible = false;
           lblvalidmsgpwd.Visible = true;
       }
       else if (!IsPasswordStrong(txtpassword1.Text.Trim()))
        {
            lblvalidmsgpwd.Text = "Password should contain 6-16 characters";
            lblvalidmsgemail.Visible = false;
            lblerrmsg.Visible = false;
            lblvalidmsgpwd.Visible = true;
        }

       else if ( Convert.ToString(txtEmail.Text).Length > 0 && Convert.ToString(txtpassword1.Text).Length > 0)
            {
                // To check authenticated user       
                var userService = ServiceFactory.GetService<UsersService>();
                var user = userService.getAuthenticatedUser(txtEmail.Text, txtpassword1.Text);
                
                if (user == null)
                {
                    lblerrmsg.Text = "Invalid email address or password.";
                    lblvalidmsgemail.Visible = false;
                    lblvalidmsgpwd.Visible = false;
                    lblerrmsg.Visible = true;
                    return;
                }
                else if (Utilities.ToEnum<SurveyStatus>(user.STATUS) == SurveyStatus.InActive || ValidationHelper.GetInteger(user.DELETED, 0) == 1)
                {
                    lblerrmsg.Text = "Your account is de-activated.";
                    lblvalidmsgemail.Visible = false;
                    lblvalidmsgpwd.Visible = false;
                    lblerrmsg.Visible = true;
                  
                    return;
                }
                else
                {
                    userService.ProcessUserLogin(user);

                    DataSet dsteleuser = surcore.getteleuser(user.USERID);

                    if (dsteleuser.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")                   

                    {
                        Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                      
                    }
                    else
                    {
                        if (user.TIME_ZONE == null) //Timezone is null for first user login. hence redirecting to welcome page
                        {
                            //create folder with userid 
                            //CreateUserFolders(user); this function is moved to register free user .. as every user must have this.
                            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                          //  Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.Getsocialnetwork(), "UserId=" + user.USERID));
                        }
                        else if (user.RESET == 1) // Password reset value is 1 for first login. hence redirectiong to chanepassword page
                        {
                            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserChangePasswordPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                        }
                        else // on successful login user is redirected to Myaccounts page
                        {
                            Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + user.SURVEY_FLAG));

                            
                        }
                    }
                }

            
           
        }




       
    }
}