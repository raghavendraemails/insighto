<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="CreateEmailList.aspx.cs" Inherits="Insighto.Pages.CreateEmailList"%>

<%@ Register Src="UserControls/AddressBookSideNav.ascx" TagName="AddressBookSideNav"
    TagPrefix="uc1" %>
<%@ Register Src="UserControls/CopyContactList.ascx" TagName="CopyContactList" TagPrefix="uc1" %>
<%@ Register Src="UserControls/UploadExcel.ascx" TagName="UploadExcel" TagPrefix="uc2" %>
<%@ Register Src="UserControls/AddNewEmail.ascx" TagName="AddNewEmail" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
<link rel="stylesheet" type="text/css" media="screen" href="../Styles/jquery.alerts.css" />
<script type="text/javascript" src="Scripts/jquery.alerts.js"></script>
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <asp:Label ID="lblPageTitle" runat="server" Text="ADDRESS BOOK" meta:resourcekey="lblPageTitleResource1"></asp:Label>
            <!-- page title -->
            <asp:Label ID="lblEmailListText" runat="server" meta:resourcekey="lblEmailListTextResource1"></asp:Label>
            <!-- //page title -->
        </div>
    </div>
    <div class="contentPanel">
        <div class="surveyCreateTextPanel">
            <!-- survey created content panel -->
            <div class="surveyCreateLeftMenu">
                <!-- survey left menu -->
                <!-- //survey left menu -->
                <div class="surveyCreateLeftMenu">
                    <!-- survey left menu -->
                    <ul class="addrMenu" id="menu">
                        <li><a href="#">Manage Email List</a>
                            <ul id="Ul1" class="innerMenu" runat="server">
                                <li class="activelink">
                                    <asp:HyperLink ID="hlnkManageEmail" runat="server" Text="Create / Manage Email List"
                                        meta:resourcekey="hlnkManageEmailResource1"></asp:HyperLink></li>
                            </ul>
                        </li>
                        <li><a href="#" class="activeLink">Add Email Contacts</a>
                            <ul class="innerMenu" id="innerMenu">
                                <li>
                                    <asp:HyperLink ID="hlnkCreateEmail" runat="server" meta:resourcekey="hlnkCreateEmailResource1"></asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="hlnkCopyLink" runat="server" meta:resourcekey="hlnkCopyLinkResource1"></asp:HyperLink></li>
                                <li id="liUpload" runat="server">
                                    <asp:HyperLink ID="hlnkUploadLink" meta:resourcekey="hlnkUploadLinkResource1" runat="server">
                                        </asp:HyperLink>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- //survey left menu -->
                </div>
            </div>
            <div class="surveyQuestionPanel">
                <!-- survey question panel -->
                <div class="errorPanel" id="recordDeleted" style="display: none;">
                    <span class="successMesg">
                        <asp:Label ID="lblDeleted" runat="server" meta:resourcekey="lblDeletedResource1"></asp:Label></span>
                </div>
                <div id="ManageEmailList">
                    <div class="labelBar">
                        <div class="lblLeftPanel">
                            <asp:TextBox ID="txtEmailList" Visible="False" MaxLength="25" Height="20px" class="textBoxMedium"
                                runat="server" meta:resourcekey="txtEmailListResource1"></asp:TextBox>
                        </div>
                        <div class="informationPanelDefault">
                            <div>
                                <asp:Label ID="lblInfo" runat="server" meta:resourcekey="lblInfoResource1"></asp:Label>
                            </div>
                        </div>
                        <div  style="float: left;margin-top: 1px;">
                        <asp:Button ID="btnBack" CssClass="dynamicButton" ToolTip='Back to Email List' runat="server"
                                            Text="Back to Email List" OnClick="btnBack_Click" meta:resourcekey="btnBackResource1" />
                        </div>
                        <div class="btnRightPanel">
                         <asp:Button ID="btnBacktoLaunchPage" Visible="False" ValidationGroup="ReturnPage"
                                OnClick="btnBacktoLaunchPage_Click" CssClass="prelaunchbackbtn" Style="float: left;
                                margin-right: 5px" runat="server" meta:resourcekey="btnBacktoLaunchPageResource1" />

                       
                        </div>
                        <div class="clear">
                        
                        </div>
                        <div class="mandatoryPanel">
                            <asp:Label ID="lblRequiredFields" runat="server" meta:resourcekey="lblRequiredFieldsResource1"></asp:Label></div>
                        
                        <div class="lblLeftPanel">
                            <asp:Label ID="lblRequired" class="requirelbl" runat="server" Text="Select List"
                                meta:resourcekey="lblRequiredResource1"></asp:Label>
                            &nbsp;<asp:DropDownList ID="drpList" CssClass="dropdownMedium" AutoPostBack="True"
                                runat="server" OnSelectedIndexChanged="drpList_SelectedIndexChanged" meta:resourcekey="drpListResource1">
                            </asp:DropDownList>
                            <span id="divValidators" runat="server"></span>
                        </div>
                        <div class="btnRightPanel">
                            
                            <asp:HyperLink ID="hlnkNewEmail" CssClass="btn_small_81_new" rel="framebox" h="400"
                                w="700" scrolling='yes' title='New Email' onclick='javascript: return ApplyFrameBox(this);'
                                runat="server" Visible="False" meta:resourcekey="hlnkNewEmailResource1"></asp:HyperLink>
                            <asp:HyperLink ID="hlnkCopy" CssClass="btn_small_new" rel="framebox" h="300" w="700"
                                scrolling='yes' title='Copy' onclick='javascript: return ApplyFrameBox(this);'
                                runat="server" Visible="False" meta:resourcekey="hlnkCopyResource1"></asp:HyperLink>
                            <asp:HyperLink ID="hlnkUpload" CssClass="btn_small_81_new" rel="framebox" h="300"
                                w="700" scrolling='yes' onclick='javascript: return ApplyFrameBox(this);' runat="server"
                                Visible="False" meta:resourcekey="hlnkUploadResource1"></asp:HyperLink>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="defaultHeight">
                        </div>
                    </div>
                    <div class="addEmail">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <div id="divAddNewEmailHeader" class="add_menu_header_active">
                                        <b>
                                            <asp:Label ID="lblTitle" runat="server" meta:resourcekey="lblTitleResource1"></asp:Label></b>
                                    </div>
                                    <div class="row_def_height">
                                    </div>
                                    <div id="divAddNewEmail">
                                        <div class="successPanel divSuccessMsg" id="dvSuccessMsg" runat="server" style="display: none;">
                                            <div>
                                                <asp:Label ID="lblSuccessMsg" CssClass="successMsg" runat="server" Style="display: none;"
                                                    Text="Contact added successfully." meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
                                        </div>
                                        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                                            <div>
                                                <asp:Label ID="lblErrMsg" runat="server" Visible="False" Text="<%$ Resources:CommonMessages, EmailExistsMsg %>"
                                                    meta:resourcekey="lblErrMsgResource1"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="clear">
                                            <asp:Panel ID="pnlAddressBook" runat="server" meta:resourcekey="pnlAddressBookResource1">
                                            </asp:Panel>
                                            <%--<asp:HyperLink ID="hlnkAddList" CssClass="btn_small_new" NavigateUrl="ContactList.aspx"
                                                rel="framebox" h="250" w="650" scrolling='yes' title='Copy' Style="float: left;
                                                margin-left: 5px;" onclick='javascript: return ApplyFrameBox(this);' ToolTip="Add List"
                                                runat="server">Name Your List</asp:HyperLink>--%>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="defaultHeight">
                    </div>
                    <div class="CreateEmailPad">
                        <div class="create_btn_aln" id="tblExcel" runat="server" visible="false">
                            <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblEmailList" Font-Bold="True" runat="server" meta:resourcekey="lblEmailListResource1"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clear">
                        </div>
                        <asp:Panel runat="server" ID="divPanelGrid" meta:resourcekey="divPanelGridResource1">
                            <div id="ptoolbar">
                            </div>
                            <div>
                                <asp:TextBox ID="txtSearch" CssClass="textBoxMedium txtSearch" runat="server"></asp:TextBox>
                                <asp:Button ID="btnSearch" CssClass="dynamicButtonSmall btnSearch" runat="server" Text="Search" />
                                <table id="tblJobKits">
                                </table>
                            </div>
                        </asp:Panel>
                        <div class="defaultHeight">
                        </div>
                        <div>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td width="50%">
                                        <table align="right" border='0' width="100%">
                                            <tr>
                                                <td colspan="2">
                                                    <b>
                                                        <asp:Label ID="lblExportEmail" runat="server" Text="Export Email List To" meta:resourcekey="lblExportEmailResource1"></asp:Label></b>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rbtnExcel" GroupName="uploadType" runat="server" Checked="True"
                                                        meta:resourcekey="rbtnExcelResource1" />
                                                </td>
                                                <td>
                                                    <img src="App_Themes/Classic/Images/icon-excel.gif" alt="Excel" title="Excel" />
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rbtnCSV" runat="server" GroupName="uploadType" meta:resourcekey="rbtnCSVResource1" />
                                                </td>
                                                <td>
                                                    <img src="App_Themes/Classic/Images/csv_logo2.png" alt="CSV" title="CSV" />
                                                </td>
                                                <td>
                                                    <div class="top_space_pnl">
                                                        <asp:Button ID="btnGo" runat="server" Text=" Export" ValidationGroup="ExporttoExcel"
                                                            class="ExporttoExcelButton" ToolTip="Export" OnClick="btnGo_Click" meta:resourcekey="btnGoResource1" /></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="50%" align="right">
                                        
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- //survey question panel -->
            </div>
            <div class="clear">
            </div>
            <!-- //survey created content panel -->
        </div>
        <div class="clear">
        </div>
        <!-- //content panel -->
    </div>
    <script type="text/javascript">
    
       
        var serviceUrl = 'AjaxService.aspx';
        var headers = <%= Headers %>;
        var columns = <%= Columns %>
        var listId = <%= ListId %>;
        $(document).ready(function () {
            $("#tblJobKits").jqGrid({
                url: serviceUrl,
                postData: { method: "GetEmailsByList",cid: listId},
                datatype: 'json',
                colNames: headers,
                colModel: columns,
                rowNum: <%=_recordsCount %>,
                 rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'EMAIL_ADDRESS',
                sortorder: "asc",
                rownumbers: true,
                viewrecords: true,
                jsonReader: { repeatitems: false },               
                width: '640',
                caption: '',               
                height: '100%',
               
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
               
               
            });
            $("#tblJobKits").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false,refresh:false });  
             $('.btnSearch').click(function(e)
        {            
         var serviceUrl = 'AjaxService.aspx';
          var headers = <%= Headers %>;
        var columns = <%= Columns %>
        var listId = <%= ListId %>;
        var searchtxt=$('.txtSearch').val();       
             $("#tblJobKits").jqGrid('setGridParam', { url: serviceUrl + "?search=yes&searchtxt="+searchtxt, page: 1 }).trigger("reloadGrid");
              e.preventDefault();           
      
        });
                  
           
        });
     

        
       
       
        function EditLinkFormatter(cellvalue, options, rowObject) {
          
               return "<a href='" + cellvalue + "' ><img src='App_Themes/Classic/Images/icon-pen-active.gif' title='Edit'/></a>";
          
        }        

        function DeleteLinkFormatter(cellvalue, options, rowObject) {
        
                return "<a href='javascript:void(0)' class='button-small-gap' id='lnkDelete' onclick='javascript: return DeleteEmail(" + cellvalue + ");'><img src='App_Themes/Classic/Images/icon-delete-active.gif' title='Delete' /></a>";
           
        }

        function DeleteEmail(EmailId) {
         $('.lblSuccessMsg').html('');
         $('.dvSuccessMsg').hide();
             jConfirm('Are you sure you want to delete ?', 'Delete Confirmation', function (r) {
                if (r == '1') {
                $.post("AjaxService.aspx", { "method": "DeleteEmailById", "Id": EmailId },
                   function (result) {
                 
                       if (result == '1') {                       
                          $('.divSuccessMsg').show();
                          $('.successMsg').show();
                           $('.successMsg').html('Email deleted successfully.');
                           $('.txtFirstName').val('');
                           $('.txtLastName').val('');
                           $('.txtEmail').val('');
                           $('.txtColumn1').val('');       
                           $('.txtColumn2').val('');    
                           $('.btnUpdate').removeAttr('style');                   
                           $('.btnUpdate').hide('');
                           $('.btnSave').show('');
                           $('.btnCancel').show(''); 
                                         
                           gridReload();
                       }
                       else {
                           $('#lblMessage').attr('style', 'disply:none;');
                       }
                   });
                }
            });
        }
        function gridReload() {
        var keyword = $("#txtKeyword").val();
        $("#tblJobKits").jqGrid('setGridParam', { url: serviceUrl + "?keyword=" + keyword, page: 1 }).trigger("reloadGrid");
        }       
    </script>
</asp:Content>
