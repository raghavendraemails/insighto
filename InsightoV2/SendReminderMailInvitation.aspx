﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SendReminderMailInvitation.aspx.cs" Inherits="SendReminderMailInvitation"%>
<%@ Register Src="~/UserControls/SurveyHeader.ascx" TagName="SurveyHeader" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
<script type="text/javascript" src="Scripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">

        tinyMCE.init({
            // General options
            mode: "textareas",
            theme: "advanced",
            plugins: "paste",
            // Theme options
            theme_advanced_buttons1: "bold,italic,underline,fontselect,fontsizeselect,forecolor,pasteword",
            theme_advanced_buttons2: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            //theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing: false,

            // Example content CSS (should be your site CSS)
            content_css: "css/content.css",
            oninit: "postInitWork",
            // Replace values for the template plugin
            template_replace_values: {
                username: "Some User",
                staffid: "991234"
            }
        });

        function postInitWork() {

            var val = document.getElementById("<%=EditorQuesParams.ClientID%>").value;
            var sty_val = val.split("_");
            if (sty_val.length == 3) {
                tinyMCE.getInstanceById('<%=EditorEmailInvitation.ClientID%>').getWin().document.body.style.fontFamily = sty_val[0];
                tinyMCE.getInstanceById('<%=EditorEmailInvitation.ClientID%>').getWin().document.body.style.fontSize = sty_val[1];
                tinyMCE.getInstanceById('<%=EditorEmailInvitation.ClientID%>').getWin().document.body.style.color = sty_val[2];
            }
        }

        function copyIntro() {
            tinyMCE.getInstanceById('<%= EditorEmailInvitation.ClientID %>').setContent(tinyMCE.getInstanceById('<%=hfCopyIntro.ClientID%>').value);
            return false;
        }

    </script>
    <uc:SurveyHeader ID="ucSurveyReportHeader" runat="server" />
    <div class="contentPanel">
        <div class="pageTitle">
            <asp:Label ID="lblReminderMailInvitation" runat="server" 
                Text="Reminder Mail Invitation" 
                meta:resourcekey="lblReminderMailInvitationResource1"></asp:Label>
        </div>
        <div class="clear">
        </div>
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <asp:Label ID="lblSuccMsg" runat="server" CssClass="successMesg" 
                meta:resourcekey="lblSuccMsgResource1"></asp:Label>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <p class="defaultHeight">
        </p>
        <div class="informationPanelDefault">
            <div>
                <asp:Literal ID="lblCreateEmailInvitation" runat="server" 
                    Text="<b>Edit Email Invitation Message:</b> The email reminder will be sent to all the respondents who have not completed the survey from the selected email list." 
                    meta:resourcekey="lblCreateEmailInvitationResource1" />
            </div>
        </div>
        <table id="tblEmailInv" class="fincontrols" cellspacing="0" cellpadding="0" width="100%"
            border="0" runat="server">
            <tbody>
                <tr>
                    <td valign="middle" align="left">
                        <div class="con_login_pnl">
                            <!-- row -->
                            <div class="loginlbl">
                                <asp:Label ID="SendersName" CssClass="requirelbl" runat="server" 
                                    Text="Sender's Name:" meta:resourcekey="SendersNameResource1" />
                            </div>
                            <div class="loginControls">
                                <asp:TextBox ID="txt_sendersname" runat="server" CssClass="textBoxMedium" 
                                    meta:resourcekey="txt_sendersnameResource1" />
                            </div>
                        </div>
                        <div class="con_login_pnl">
                            <!-- validation -->
                            <div class="reqlbl btmpadding">
                                <asp:RequiredFieldValidator SetFocusOnError="True" ID="RequiredFieldValidator1" runat="server"
                                    ErrorMessage="Please Enter Sender's Name" CssClass="lblRequired" Display="Dynamic"
                                    ControlToValidate="txt_sendersname" ValidationGroup="email" 
                                    meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                            </div>
                            <!-- //row -->
                        </div>
                        <div class="con_login_pnl">
                            <!-- row -->
                            <div class="loginlbl">
                                <asp:Label ID="lblFromAddress" CssClass="requirelbl" runat="server" 
                                    Text="Sender's Email Id:" meta:resourcekey="lblFromAddressResource1" />
                            </div>
                            <div class="loginControls">
                                <asp:TextBox ID="txtfromAddress" runat="server" CssClass="textBoxMedium" 
                                    designer:wfdid="w11" meta:resourcekey="txtfromAddressResource1" />
                            </div>
                        </div>
                        <div class="con_login_pnl">
                            <!-- validation -->
                            <div class="reqlbl btmpadding">
                                <asp:RequiredFieldValidator SetFocusOnError="True" ID="RequiredFieldValidator2" runat="server"
                                    ErrorMessage="Please Enter Sender's Email Id" CssClass="lblRequired" Display="Dynamic"
                                    ControlToValidate="txtfromAddress" ValidationGroup="email" 
                                    meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator SetFocusOnError="True" ID="regEmailId" runat="server"
                                    ErrorMessage="Please enter valid Sender's Email Id" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                    CssClass="lblRequired" ControlToValidate="txtfromAddress" 
                                    Display="Dynamic" ValidationGroup="email" 
                                    meta:resourcekey="regEmailIdResource1"></asp:RegularExpressionValidator>
                            </div>
                            <!-- //row -->
                        </div>
                        <div class="con_login_pnl">
                            <!-- row -->
                            <div class="loginlbl">
                                <asp:Label ID="lblMailSubject" CssClass="requirelbl" runat="server" 
                                    Text="Email Subject:" meta:resourcekey="lblMailSubjectResource1" />
                            </div>
                            <div class="loginControls">
                                <asp:TextBox ID="txtEmailSubject" runat="server" CssClass="textBoxMedium" 
                                    meta:resourcekey="txtEmailSubjectResource1" />
                            </div>
                        </div>
                        <div class="con_login_pnl">
                            <!-- validation -->
                            <div class="reqlbl btmpadding">
                                <asp:RequiredFieldValidator SetFocusOnError="True" ID="RequiredFieldValidator3" runat="server"
                                    ErrorMessage="Please Enter Email Subject" CssClass="lblRequired" Display="Dynamic"
                                    ControlToValidate="txtEmailSubject" ValidationGroup="email" 
                                    meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                            </div>
                            <!-- //row -->
                        </div>
                        <div class="con_login_pnl">
                            <!-- row -->
                            <div class="loginlbl">
                                <asp:Label ID="lblReplyto" CssClass="requirelbl" runat="server" 
                                    Text="Reply-To:" meta:resourcekey="lblReplytoResource1" />
                            </div>
                            <div class="loginControls">
                                <asp:TextBox ID="txtreplyto" runat="server" CssClass="textBoxMedium" 
                                    meta:resourcekey="txtreplytoResource1" />
                            </div>
                        </div>
                        <div class="con_login_pnl">
                            <!-- validation -->
                            <div class="reqlbl btmpadding">
                                <asp:RequiredFieldValidator SetFocusOnError="True" ID="RequiredFieldValidator4" runat="server"
                                    ErrorMessage="Please Enter Reply-To" CssClass="lblRequired" Display="Dynamic"
                                    ControlToValidate="txtreplyto" ValidationGroup="email" 
                                    meta:resourcekey="RequiredFieldValidator4Resource1"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator SetFocusOnError="True" ID="RegularExpressionValidator1"
                                    runat="server" ErrorMessage="Please enter valid Reply-To Email Id" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                    CssClass="lblRequired" ControlToValidate="txtreplyto" Display="Dynamic" 
                                    ValidationGroup="email" meta:resourcekey="RegularExpressionValidator1Resource1"></asp:RegularExpressionValidator>
                            </div>
                            <!-- //row -->
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="defaultHeight">
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="wid_98">
                            <div class="informationPanelDefault">
                                <div>
                                    <asp:Literal ID="litPersonalise" runat="server" 
                                        Text="<b>Personalise Email Invitation</b> : Email invites created through Insighto come with the First Name and Last Name of the recipients given in your email lists. You can retain or choose to delete either or both of them." 
                                        meta:resourcekey="litPersonaliseResource1" />
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lbtnCopyIntro" runat="server" Text="Copy Intro as Email Invitation"
                            ToolTip="Copy Intro as Email Invitation" OnClick="lbtnCopyIntro_Click" 
                            meta:resourcekey="lbtnCopyIntroResource1" />
                        <asp:HiddenField ID="hfCopyIntro" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="left">
                        <input type="hidden" runat="server" id="EditorQuesParams" value="0" />
                        <textarea id="EditorEmailInvitation" runat="server" cols="10" rows="8" style="height: 162px;
                            width: 100%;"></textarea>
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="RequiredFieldValidator5" runat="server"
                            ErrorMessage="Please Enter Email Invitation" CssClass="lblRequired" Display="Dynamic"
                            ControlToValidate="EditorEmailInvitation" ValidationGroup="email" 
                            meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="defaultHeight">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnSave" runat="server" Text="Send" ToolTip="Send" ValidationGroup="email"
                            CssClass="btn_small_65" 
                            OnClientClick="javascript:return ShowConfirmation();" 
                            meta:resourcekey="btnSaveResource1" />&nbsp;&nbsp;<asp:Button
                                ID="btnBack" runat="server" Text="Back" ToolTip="Back" CssClass="btn_small_65"
                                OnClick="btnBack_Click" meta:resourcekey="btnBackResource1" />
                        <%--OnClientClick="tinyMCE.triggerSave(false,true);return confirm('Are you sure you want to send reminder email?');"--%>
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="modal" style="border: solid 2px #999; width: 310px; height: 100px; background-color: #fff;
            -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; display: none;">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td class="reminder_title" colspan="2" style="padding-left:5px;">
                        <asp:Label ID="lblReminderConfirm" runat="server" Text="Reminder Confirmation" 
                            meta:resourcekey="lblReminderConfirmResource1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="defaultHeight">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left" style="padding-left:5px;">
                        <asp:Label ID="lblAreyouSure" runat="server" 
                            Text=" Are you sure you want to send reminder email?" 
                            meta:resourcekey="lblAreyouSureResource1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="defaultHeight">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-right:10px;">
                        <table align="right">
                            <tr>
                                <td>
                                    <asp:Button ID="btnYes" Text="Yes" CssClass="reminderbtn" OnClientClick="Popup.hide('modal');return true;"
                                        OnClick="btnSave_Click" ToolTip="Yes" runat="server" 
                                        meta:resourcekey="btnYesResource1" />
                                    <%--<input id="btnYes" type="button" value="Yes" class="dynamicButton" onclick=" />--%>
                                    <asp:HiddenField ID="hfSkipUrl" runat="server" />
                                </td>
                                <td>
                                    <input id="btnNo" type="button" value="No" class="reminderbtn" title="No" onclick="Popup.hide('modal');hidemodel();" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script src="Scripts/popup.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript">
        function ShowConfirmation() {
            //document.getElementById('<%= hfSkipUrl.ClientID %>').value = val;
            Popup.showModal('modal', null, null, { 'screenColor': '#000000', 'screenOpacity': .6 });
            return false;
        }
    </script>    
</asp:Content>
