﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="CopyContactList.aspx.cs" Inherits="Insigto.Pages.CopyContactList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                COPY LIST OF EMAIL ID'S</h3>
        </div>
     <%--   <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" title="Close" onclick="closeModalSelf(false,'');">
                &nbsp;</a>
        </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupContentPanel">
        <!-- popup content panel -->
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccessMsg" runat="server" Visible="False" Text="Emails added Successfully."></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" Visible="False" Text="Contact list already exist."></asp:Label></div>
        </div>
        <div class="formPanel">
            <!-- form -->
            <div class="forgotpanel">
                <asp:Panel ID="pnlMultipleEmails" runat="server">
                    <p class="informationPanel fontSize">
                        <span>Paste or type multiple emails addresses into this field, separated by a comma,
                            in the same line and without any space. Example: <a href="#">Tom@mycompany.com</a>,<a
                                href="#">John@mycompany.com</a></span></p>
                    <table>
                        <tr>
                            <td valign="top" >
                            <asp:TextBox ID="txtMultipleEmails" runat="server" CssClass="textAreaMedium" TextMode="MultiLine"
                        Rows="4" Columns="200" Width="317px"></asp:TextBox>
                         <br />
                    <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqList" ControlToValidate="txtMultipleEmails" CssClass="lblRequired"
                        ErrorMessage="Please enter email id's." runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
         
                            </td>
                            <td valign="top" >
                              <asp:Label ID="lblDuplicateEmails" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                    
                  
                    <div class="clear"></div>
                    <div id="divValidators" runat="server">
                    </div>
                    <div class="clear">
                    </div>
                    <asp:Button ID="btnSave" runat="server" CssClass="btn_small_65" Text="Save" ToolTip="Save"
                        OnClick="btnSave_Click" />
                    <!-- //text here -->
                    <div>
                        <asp:HiddenField ID="hdnError" runat="server" Value="List contains invalid email id's." />
                    </div>
                </asp:Panel>
            </div>
            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>
</asp:Content>
