﻿using System;
using System.Web.UI;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using System.Collections;
using Insighto.Business.Helpers;
using App_Code;


public partial class EmailListPopUp : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string listName = "";
        int ContactId = 0;
        var contactList = new osm_contactlist();    
        
          
        if (!IsPostBack)
        {           
            // insert list if not exist
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var contactListService = ServiceFactory.GetService<ContactListService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var emailService = ServiceFactory.GetService<EmailService>();
           
            if (Request.QueryString["emailId"] != null)
            {
                var email = emailService.GetEmailListByEmailId(Convert.ToInt32(Request.QueryString["emailId"].ToString()));
                contactList = contactListService.FindByContactListID(ValidationHelper.GetInteger(email.CONTACTLIST_ID, 0));
                lblTitle.Text = "Edit Email";
            }
            else if (Request.QueryString["key"] != null)
            {
                Hashtable ht = new Hashtable();

                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);


                if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                {
                    ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                }
                contactList = contactListService.FindByContactListID(ContactId);

            }
            
            BindControls(contactList);
        }

    }
    protected void BindControls(osm_contactlist contactList)
    {
        var emailService = ServiceFactory.GetService<EmailService>();
        var email = new osm_emaillist();
       
        if (contactList != null) 
        {
            lblEmail.Text = "Email";
            lblFirstName.Text = "First Name";
            lblLastName.Text = "Last Name";
          
            if (contactList.CUSTOM_HEADER1 != null && contactList.CUSTOM_HEADER1.Trim() != "")
            {
                txtColumn1.Visible = true;
                lblColumn1.Visible = true;
                lblColumn1.Text = contactList.CUSTOM_HEADER1;
                
            }
            if (contactList.CUSTOM_HEADER2 != null && contactList.CUSTOM_HEADER2.Trim() != "")
            {
                txtColumn2.Visible = true;
                lblColumn2.Visible = true;
                lblColumn2.Text = contactList.CUSTOM_HEADER2;
                
            }
            if (contactList.CUSTOM_HEADER3 != null && contactList.CUSTOM_HEADER3.Trim() != "")
            {
                txtColumn3.Visible = true;
                lblColumn3.Visible = true;
                lblColumn3.Text = contactList.CUSTOM_HEADER3;
                
            }
            if (contactList.CUSTOM_HEADER4 != null && contactList.CUSTOM_HEADER4.Trim() != "")
            {
                txtColumn4.Visible = true;
                lblColumn4.Visible = true;
                lblColumn4.Text = contactList.CUSTOM_HEADER4;
                
            }
            if (contactList.CUSTOM_HEADER5 != null && contactList.CUSTOM_HEADER5.Trim() != "")
            {
                txtColumn5.Visible = true;
                lblColumn5.Visible = true;
                lblColumn5.Text = contactList.CUSTOM_HEADER5;
                
            }
            if (contactList.CUSTOM_HEADER6 != null && contactList.CUSTOM_HEADER6.Trim() != "")
            {
                txtColumn6.Visible = true;
                lblColumn6.Visible = true;
                lblColumn6.Text = contactList.CUSTOM_HEADER6;
               
            }
            if (contactList.CUSTOM_HEADER7 != null && contactList.CUSTOM_HEADER7.Trim() != "")
            {
                txtColumn7.Visible = true;
                lblColumn7.Visible = true;
                lblColumn7.Text = contactList.CUSTOM_HEADER7;
                
            }
            if (contactList.CUSTOM_HEADER8 != null && contactList.CUSTOM_HEADER8.Trim() != "")
            {
                txtColumn8.Visible = true;
                lblColumn8.Visible = true;
                lblColumn8.Text = contactList.CUSTOM_HEADER8;
                
            }

            if (Request.QueryString["emailId"] != null)
            {
                int emailId = Convert.ToInt32(Request.QueryString["emailId"].ToString());
               
                var emailDetails = emailService.GetEmailListByEmailId(emailId);
                if (emailDetails != null)
                {
                    if (emailDetails.FIRST_NAME != null && emailDetails.FIRST_NAME != "")  
                    txtFirstName.Text = emailDetails.FIRST_NAME.Trim();
                    if (emailDetails.LAST_NAME != null && emailDetails.LAST_NAME != "")    
                    txtLastName.Text = emailDetails.LAST_NAME.Trim();
                    if (emailDetails.CUSTOM_VAR1 != null && emailDetails.CUSTOM_VAR1 != "")            
                    txtColumn1.Text = emailDetails.CUSTOM_VAR1.Trim();
                    if (emailDetails.CUSTOM_VAR2 != null && emailDetails.CUSTOM_VAR2 != "") 
                    txtColumn2.Text = emailDetails.CUSTOM_VAR2.Trim();
                    if (emailDetails.CUSTOM_VAR3 != null && emailDetails.CUSTOM_VAR3 != "") 
                    txtColumn3.Text = emailDetails.CUSTOM_VAR3.Trim();
                    if (emailDetails.CUSTOM_VAR4 != null && emailDetails.CUSTOM_VAR4 != "") 
                    txtColumn4.Text = emailDetails.CUSTOM_VAR4.Trim();
                    if (emailDetails.CUSTOM_VAR5 != null && emailDetails.CUSTOM_VAR5 != "") 
                    txtColumn5.Text = emailDetails.CUSTOM_VAR5.Trim();
                    if (emailDetails.CUSTOM_VAR6 != null && emailDetails.CUSTOM_VAR6 != "") 
                    txtColumn6.Text = emailDetails.CUSTOM_VAR6.Trim();
                    if (emailDetails.CUSTOM_VAR7 != null && emailDetails.CUSTOM_VAR7 != "") 
                    txtColumn7.Text = emailDetails.CUSTOM_VAR7.Trim();
                    if (emailDetails.CUSTOM_VAR8 != null && emailDetails.CUSTOM_VAR8 != "") 
                    txtColumn8.Text = emailDetails.CUSTOM_VAR8.Trim();
                    txtEmail.Text = emailDetails.EMAIL_ADDRESS.Trim();
                }
            }
            hdnContactListId.Value = contactList.CONTACTLIST_ID.ToString();

           
        }
    }
    protected void ibtnSave_Click(object sender, EventArgs e)
    {
        SaveContactDetails();
    }

    protected void SaveContactDetails() 
    {
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        var emaillist = new osm_emaillist();        
        emaillist.CONTACTLIST_ID = Convert.ToInt32(hdnContactListId.Value);      
        emaillist.CUSTOM_VAR1 = txtColumn1.Text.Trim();
        emaillist.CUSTOM_VAR2 = txtColumn2.Text.Trim();
        emaillist.CUSTOM_VAR3 = txtColumn3.Text.Trim();
        emaillist.CUSTOM_VAR4 = txtColumn4.Text.Trim();
        emaillist.CUSTOM_VAR5 = txtColumn5.Text.Trim();
        emaillist.CUSTOM_VAR6 = txtColumn6.Text.Trim();
        emaillist.CUSTOM_VAR7 = txtColumn7.Text.Trim();
        emaillist.CUSTOM_VAR8 = txtColumn8.Text.Trim();
        emaillist.FIRST_NAME = txtFirstName.Text.Trim();
        emaillist.LAST_NAME = txtLastName.Text.Trim();
        emaillist.EMAIL_ADDRESS = txtEmail.Text.Trim();
        emaillist.LAST_MODIFIED_BY = userInfo.UserId;
        emaillist.MODIFIED_ON = DateTime.Now;        
        emaillist.DELETED = 0;
        emaillist.STATUS = "Active";
         var emailService = ServiceFactory.GetService<EmailService>();
         if (Request.QueryString["emailId"] != null)
         {
             int emailId = Convert.ToInt32(Request.QueryString["emailId"].ToString());
             lblTitle.Text = Utilities.ResourceMessage("lblEditEmail");  //"Edit Email";
             emaillist.EMAIL_ID = emailId;
             emailService.UpdateEmailsByList(emaillist);
             lblSuccessMsg.Text = Utilities.ResourceMessage("lblSuccessMsg_Update"); //"Contact updated successfully";
             lblSuccessMsg.Visible = true;
             dvSuccessMsg.Visible = true;
             dvErrMsg.Visible = false;
             
         }
         else
         {
              emaillist.CREATED_BY = userInfo.UserId;
              int rtnValue = emailService.SaveEmailsByList(emaillist);
              if (rtnValue > 0)
              {
                  dvSuccessMsg.Visible = true;
                  dvErrMsg.Visible = false;
                  lblSuccessMsg.Visible = true;
                  lblErrMsg.Visible = false;
              }
              else
              {
                  dvSuccessMsg.Visible = false;
                  dvErrMsg.Visible = true;
                  lblErrMsg.Visible = true;
                  lblSuccessMsg.Visible = false;
              }
         }


         // base.CloseModal("CreateEmailList.aspx?contactId=" + emaillist.CONTACTLIST_ID);
       
    }
}