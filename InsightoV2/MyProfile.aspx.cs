﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using Insighto.Exceptions;
using System.Collections;
using Insighto.Data;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;
using System.Data;

namespace Insighto.Pages 
{
public partial class MyProfile : BasePage
{
    LoggedInUserInfo loggedInUserInfo;
    Hashtable ht = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionStateService sessionStateService = new SessionStateService();
        loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();        
        if (!IsPostBack && loggedInUserInfo != null)
        {
            bindDropDownList(Constants.COMPANY_SIZE, drpCompanySize);
            bindDropDownList(Constants.WORK_INDUSTRY, drpWorkIndustry);
            bindDropDownList(Constants.JOB_FUNCTION, drpJobFunction);
            bindDropDownList(Constants.COUNTRY, drpCountry);
           //getTimeZone();
            getUserDetails();

            //if (Request.QueryString["key"] != null)
            //{
            //    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            //    if (ht != null && ht.Count > 0 && ht.Contains("Status"))
            //    {
            //        if (ht["Status"].ToString() == "Success")
            //        {
            //            dvSuccessMsg.Visible = true;
            //            lblSuccessMsg.Text = "Linked account has been created successfully.";
            //            dvErrMsg.Visible = false;
            //        }
            //        else
            //        {
            //            dvSuccessMsg.Visible = false;
            //            lblErrMsg.Text = "Linked account  already exist.";
            //            dvErrMsg.Visible = true;
            //        }

            //    }
               

            //}

            btnBack.Text = String.Format(Utilities.ResourceMessage("btnBackeResource1.Text"), SurveyMode);
            btnBack.ToolTip = String.Format(Utilities.ResourceMessage("btnBackeResource1.Text"), SurveyMode);
        }

        
        
    }
    protected void getUserDetails() {
       
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        PollCreation pc = new PollCreation();
        string polllicencetype = "-";
        var pollsubscriptiondate = "-";
        var pollexpirydate = "-";
        string surveylicencetype = "";
        if (userInfo != null)
        {
                
                var userServices = new UsersService();
                var AUser = userServices.GetUserDetails(userInfo.UserId);
                DataTable dtpoll = pc.getLicencePoll(userInfo.UserId);
                if(dtpoll.Rows.Count > 0){
                    polllicencetype = dtpoll.Rows[0]["polllicencetype"].ToString();
                    pollsubscriptiondate = dtpoll.Rows[0]["pollsubscriptiondate"].ToString();
                    if (dtpoll.Rows[0]["polllicexpirydate"].ToString() != "")
                    {
                        pollexpirydate = dtpoll.Rows[0]["polllicexpirydate"].ToString();
                    }
                }
            
                string name = userInfo.FirstName + " " + userInfo.LastName; 
                //if (name.Length > 30)
                //{
                //    Master.UserValue = name.Substring(0, 30) + "...";
                //}
                //else
                //    Master.UserValue = name;
              
                
                txtFirstName.Text = AUser[0].FIRST_NAME;
                txtLastName.Text = AUser[0].LAST_NAME;
                txtDesignation.Text = AUser[0].USER_DESIGNATION; 
                txtOrganization.Text = AUser[0].USER_COMPANY;
                txtDept.Text = AUser[0].DEPT;               
                
                int stdBias = ValidationHelper.GetInteger(AUser[0].STANDARD_BIAS,0);
                surveylicencetype = (AUser[0].LICENSE_TYPE ?? "");
                if (surveylicencetype == "")
                {
                    txtCurrentSubscription.Text = "-";
                    txtStartDate.Text = "-";
                    txtExpiryDate.Text = "-";
                }
                else if (AUser[0].LICENSE_TYPE == "FREE")
                {
                    txtStartDate.Text = Convert.ToDateTime(AUser[0].CREATED_ON).AddMinutes(stdBias).ToString("dd-MMM-yyyy");
                }
                else
                {
                    var licenseStartdate=ServiceFactory<UsersService>.Instance.GetLicenseStartDateUserId(userInfo.UserId);

                    if (licenseStartdate != null)
                    {

                        if (licenseStartdate.LICENSE_STARTDATE != null)
                        {
                            txtStartDate.Text = Convert.ToDateTime(licenseStartdate.LICENSE_STARTDATE).AddMinutes(stdBias).ToString("dd-MMM-yyyy");
                        }
                    }
                    else
                    {
                        txtStartDate.Text = Convert.ToDateTime(AUser[0].CREATED_ON).AddMinutes(stdBias).ToString("dd-MMM-yyyy");
                    }
                }

                txtCurrentSubscriptionPoll.Text = polllicencetype;
                if (polllicencetype == "")
                {
                    txtStartDatePoll.Text = "-";
                    txtExpiryDatePoll.Text = "-";
                }
                else if (polllicencetype.ToLower()  == "free")
                {
                    txtStartDatePoll.Text = Convert.ToDateTime(pollsubscriptiondate).AddMinutes(stdBias).ToString("dd-MMM-yyyy");
                    txtExpiryDatePoll.Text = Utilities.ResourceMessage("NoExpiryMsg");
                }
                else if (polllicencetype != "-")
                {
                    txtStartDatePoll.Text = Convert.ToDateTime(pollsubscriptiondate).AddMinutes(stdBias).ToString("dd-MMM-yyyy");
                    txtExpiryDatePoll.Text = Convert.ToDateTime(pollexpirydate).AddMinutes(stdBias).ToString("dd-MMM-yyyy");
                }
                else {
                    txtStartDatePoll.Text = "-";
                    txtExpiryDatePoll.Text = "-";
                }
                txtAddressLine1.Text = AUser[0].ADDRESS_LINE1;
                txtAddressLine2.Text = AUser[0].ADDRESS_LINE2;
                txtCity.Text = AUser[0].CITY;
                txtPostalCode.Text = AUser[0].POSTAL_CODE;
                if (AUser[0].COUNTRY != null && AUser[0].COUNTRY!="")
                    drpCountry.SelectedValue = AUser[0].COUNTRY;
                if (AUser[0].JOB_FUNCTION != null && AUser[0].JOB_FUNCTION != "")
                    drpJobFunction.SelectedValue = AUser[0].JOB_FUNCTION;
                if (AUser[0].COMPANY_SIZE != null && AUser[0].COMPANY_SIZE != "")
                    drpCompanySize.SelectedValue = AUser[0].COMPANY_SIZE;
                if (AUser[0].TIME_ZONE != null && AUser[0].TIME_ZONE != "")
                    if (drpTimeZone.Items.FindByText(AUser[0].TIME_ZONE.Trim()) != null)
                    {
                        drpTimeZone.Items.FindByText(AUser[0].TIME_ZONE.Trim()).Selected = true;                       
                    }
                if (AUser[0].WORK_INDUSTRY != null && AUser[0].WORK_INDUSTRY != "")
                    drpWorkIndustry.SelectedValue = AUser[0].WORK_INDUSTRY;
                
                txtState.Text = AUser[0].STATE;
                txtCustomerId.Text = AUser[0].CUSTOMER_ID;
                
                txtCurrentSubscription.Text = AUser[0].LICENSE_TYPE;

                if (surveylicencetype == "")
                {
                    txtStartDate.Text = "-";
                    txtExpiryDate.Text = "-";
                }
                else if (AUser[0].LICENSE_TYPE == "FREE")
                {
                    txtExpiryDate.Text = Utilities.ResourceMessage("NoExpiryMsg");
                }
                else
                {
                    txtExpiryDate.Text = Convert.ToDateTime(AUser[0].LICENSE_EXPIRY_DATE).AddMinutes(stdBias).ToString("dd-MMM-yyyy");
                }

                txtLoginId.Text = AUser[0].LOGIN_NAME;
                if (AUser[0].UPDATES_FLAG == 1)
                    chkInformation.Checked = true;
               
                if (AUser[0].PHONE != null)
                {
                    string phone = AUser[0].PHONE;
                    string coutrycode = "";
                    string regioncode = "";
                    int x = phone.IndexOf(Constants.PHONE_SEPARATER);
                    if (x >= 0)
                    {
                        coutrycode = phone.Substring(0, x);
                        phone = phone.Substring(x + 1);
                        x = phone.IndexOf(Constants.PHONE_SEPARATER);
                        regioncode = phone.Substring(0, x);
                        regioncode = phone.Substring(0, x);
                        phone = phone.Substring(x + 1);
                    }
                    txtPhoneNumber.Text = coutrycode;
                    txtPhoneNumber1.Text = regioncode;
                    txtPhoneNumber2.Text = phone;
                }
                bindLinkedAccounts(userInfo.UserId);
        }
        else {
            //Response.Redirect(PathHelper.GetUserLoginPageURL());        
        }    
    }
    private void bindLinkedAccounts(int userId)
    {
        var userLinkAccountService = ServiceFactory.GetService<UserLinkAccountService>();
        var userLinkAccount = userLinkAccountService.Find(userId, UserLinkAccountType.FaceBook);
        //if (userLinkAccount != null)
        //{
        //    hlnkFacebook.Visible = false;
        //    btnFacebookRemove.Visible = true;
        //    lblFacebookEmail.Text = userLinkAccount.Email;
        //    lblFacebook.Text = UserLinkAccountType.FaceBook.ToString();
        //}
        //else
        //{
        //    btnFacebookRemove.Visible = false;
        //    hlnkFacebook.Visible = true;
        //    lblFacebookEmail.Text = "";
        //    lblFacebook.Text = "";
        //}



        userLinkAccount = userLinkAccountService.Find(userId, UserLinkAccountType.Google);
        //if (userLinkAccount != null)
        //{
        //    hlnkGoogle.Visible = false;
        //    btnGoogleRemove.Visible = true;
        //    lblGoogleEmail.Text = userLinkAccount.Email;
        //    lblGoogle.Text = UserLinkAccountType.Google.ToString();
        //}
        //else
        //{
        //    btnGoogleRemove.Visible = false;
        //    hlnkGoogle.Visible = true;
        //    lblGoogleEmail.Text = "";
        //    lblGoogle.Text = "";
        //}
        
    }

    protected void btnGoogleRemove_Click(object sender,EventArgs e)
    {
       // var sessionService = ServiceFactory.GetService<SessionStateService>();
       // var userInfo = sessionService.GetLoginUserDetailsSession();
       // var userLinkAccountService = ServiceFactory.GetService<UserLinkAccountService>();
       // int rtnValue =  userLinkAccountService.DeleteByUserIdAndAccountType(userInfo.UserId, UserLinkAccountType.Google);
       // if (rtnValue > 0)
       // getUserDetails();
       //lblSuccessMsg.Text = "Google account successfully removed.";
       //lblSuccessMsg.Visible = true; 
       //dvSuccessMsg.Visible = true;
       //dvErrMsg.Visible = false;
         
    }

    protected void btnFacebookRemove_Click(object sender, EventArgs e)
    {
       // var sessionService = ServiceFactory.GetService<SessionStateService>();
       // var userInfo = sessionService.GetLoginUserDetailsSession();
       // var userLinkAccountService = ServiceFactory.GetService<UserLinkAccountService>();
       // int rtnValue  = userLinkAccountService.DeleteByUserIdAndAccountType(userInfo.UserId, UserLinkAccountType.FaceBook);
       // if (rtnValue > 0)
       //     getUserDetails();
       // lblSuccessMsg.Text = "Facebook account successfully removed.";
       //lblSuccessMsg.Visible = true; ;
       //dvSuccessMsg.Visible = true;
       //dvErrMsg.Visible = false;

    }
    protected void bindDropDownList(string CategoryName, DropDownList ddl)
    {
        PickListService pickListService = new PickListService(); 
        osm_picklist osm_picklist = new osm_picklist();
        osm_picklist.PARAM_VALUE = Constants.PARAM_VALUE;
        osm_picklist.PARAMETER = Constants.PARAMETER;
        osm_picklist.CATEGORY = CategoryName;
        var list = new List<osm_picklist>();

        if (ddl.ID == "drpCompanySize")
        {
             list = pickListService.GetPickListFontSize(osm_picklist);
        }
        else
        {
             list = pickListService.GetPickList(osm_picklist);
        }

        if (list.Count > 0)
        {                
            ddl.DataSource = list;
            ddl.DataTextField = Constants.PARAM_VALUE;
            ddl.DataValueField = Constants.PARAMETER;
            
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem(Constants.SELECT, Constants.SELECTVALUE));
            ddl.SelectedIndex = 0;    
        }        
    }




    protected void btnSave_Click(object sender, EventArgs e)
    {
        if(Page.IsValid)
        UpdateProfile();
    }

    protected void UpdateProfile() 
    {
        SessionStateService sessionStateService = new SessionStateService();
        loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        var userServices = new UsersService();
        string phoneNo = "";
        phoneNo = txtPhoneNumber.Text.Trim() + Constants.PHONE_SEPARATER + txtPhoneNumber1.Text.Trim() + Constants.PHONE_SEPARATER + txtPhoneNumber2.Text.Trim();
        if (loggedInUserInfo != null)
        {
            var userList = new List<osm_user>();
            var user = new osm_user();
            user.USERID = loggedInUserInfo.UserId;
            user.FIRST_NAME = txtFirstName.Text.Trim();
            user.LAST_NAME = txtLastName.Text.Trim();
            user.ADDRESS_LINE1 = txtAddressLine1.Text.Trim();
            user.ADDRESS_LINE2 = txtAddressLine2.Text.Trim();
            user.CITY = txtCity.Text.Trim();
            user.STATE = txtState.Text.Trim();
            user.COUNTRY = drpCountry.SelectedValue;
            user.POSTAL_CODE = txtPostalCode.Text.Trim();
            user.USER_COMPANY = txtOrganization.Text.Trim();
            user.DEPT = txtDept.Text.Trim();
            user.USER_DESIGNATION = txtDesignation.Text.Trim();
            user.WORK_INDUSTRY = drpWorkIndustry.SelectedValue;
            user.JOB_FUNCTION = drpJobFunction.SelectedValue;
            user.COMPANY_SIZE = drpCompanySize.SelectedValue;

            string timeZ = drpTimeZone.SelectedValue;
            string[] totalString = timeZ.Split(',');
            string[] tempTime = totalString[0].Split(':');
            string sign = tempTime[0].Substring(0, 1);
            int hours = 0, minutes = 0;
            if (sign != "0")
            {
                hours = ValidationHelper.GetInteger(tempTime[0].Substring(1, (tempTime.Length)), 0) * 60;
                minutes = ValidationHelper.GetInteger(tempTime[1].ToString(), 0);
                minutes = hours + minutes;
                if (sign == "-")
                {
                    minutes = -(minutes);
                }
            }
            user.STANDARD_BIAS = minutes;
            user.TIME_ZONE = drpTimeZone.SelectedItem.Text;
            user.PHONE = phoneNo;
            user.MODIFIED_ON = DateTime.UtcNow;
            if (chkInformation.Checked == true)
                user.UPDATES_FLAG = 1;
            else
                user.UPDATES_FLAG = 0;
            userList.Add(user);
           
            if (userServices.UpdateMyProfile(userList))
            {
                lblSuccessMsg.Text = Utilities.ResourceMessage("MyProfile_Success_Update"); //"User details has been updated successfully.";
                lblSuccessMsg.Visible = true;
                lblErrMsg.Visible = false;
                dvSuccessMsg.Visible = true;
                dvErrMsg.Visible = false;
                var AUser = userServices.GetUserDetails(loggedInUserInfo.UserId);
                sessionStateService.SaveUserToSession(AUser[0]);
                getUserDetails();              
                
                
            }
            else
            {
                dvSuccessMsg.Visible = false;
                dvErrMsg.Visible = true;
                lblErrMsg.Visible = true;
                lblSuccessMsg.Visible = false;
            }
        }
    }

   
    protected void getTimeZone()
    {

        TimeZoneInformation[] m_zones = TimeZoneInformation.EnumZones();
        ArrayList disp_name = new ArrayList();
        ArrayList disp_negative = new ArrayList();
        for (int i = 0; i < m_zones.LongLength; i++)
        {
            if (m_zones[i].StandardBias < 0)
                disp_negative.Add(m_zones[i].DisplayName);
            else
                disp_name.Add(m_zones[i].DisplayName);
        }
        disp_name.Sort();
        disp_negative.Sort();


        int p = 1;
        for (int k = disp_negative.Count - 1; k >= 0; k--)
        {
            for (int i = 0; i < m_zones.LongLength; i++)
            {
                if (Convert.ToString(disp_negative[k]) == m_zones[i].DisplayName)
                {

                    drpTimeZone.Items.Add(m_zones[i].DisplayName);
                    p = p + 1;
                    break;
                }
            }
        }
        for (int k = 0; k < disp_name.Count; k++)
        {
            for (int i = 0; i < m_zones.LongLength; i++)
            {
                if (Convert.ToString(disp_name[k]) == m_zones[i].DisplayName)
                {
                    drpTimeZone.Items.Add(m_zones[i].DisplayName);
                    p = p + 1;
                    break;
                }
            }
        }

        drpTimeZone.Items.Insert(0, new ListItem(Constants.SELECT, Constants.SELECTVALUE));
        drpTimeZone.SelectedIndex = 0;
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + SurveyFlag));
    }
}
}