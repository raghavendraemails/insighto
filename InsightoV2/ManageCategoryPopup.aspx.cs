﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using System.Collections;
using Insighto.Business.Helpers;
using System.Configuration;
using CovalenseUtilities.Helpers;
using App_Code;

public partial class ManageCategoryPopup : BasePage
{
    Hashtable ht = new Hashtable();
    private string _category = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 && ht.Contains("Category"))
            {
                lblCategoryHeading.Text = ht["Category"].ToString();
            }
        }        
        if (!IsPostBack)
        {
            lblMoveSurvey.Text = String.Format(Utilities.ResourceMessage("lblMoveSurvey"), SurveyMode);
            BindCategoryList("SURVEY_CATEGORY", ddlSurvey);        
        }       
        
    }

    public string Category
    {
        get
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("Category"))
                {
                    _category = ht["Category"].ToString();
                }
            }
            return _category;
        }
       
    }
    protected void BindCategoryList(string CategoryName, DropDownList ddl)
    {
       
        PickListService pickListService = new PickListService();
        osm_picklist osm_picklist = new osm_picklist();
        osm_picklist.PARAM_VALUE = Constants.PARAM_VALUE;
        osm_picklist.PARAMETER = Constants.PARAMETER;
        osm_picklist.CATEGORY = CategoryName;
        var AList = pickListService.GetPickList(osm_picklist);
        if (AList.Count > 0)
        {
            ddl.DataSource = AList;
            ddl.DataTextField = Constants.PARAM_VALUE;
            ddl.DataValueField = Constants.PARAMETER;
            ddl.DataBind();
            ddl.Items.Insert(0,new ListItem(Constants.SELECT,"0"));
        }
    }

    protected void ddlSurvey_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hdntxtValue.Value != null || hdntxtValue.Value != "")
        {
            if (ddlSurvey.SelectedIndex < 1)
            {
                dvSuccessMsg.Visible = false;
                dvErrMsg.Visible = true;
                lblErrMsg.Text = String.Format(Utilities.ResourceMessage("errSelectCategory"), SurveyMode.ToLower()); //"Please select a category to move surveys.";
                lblErrMsg.Visible = true;
                lblSuccessMsg.Visible = false;
                hdntxtValue.Value = "";
            }
            else
            {
                string selectedSurveys = hdntxtValue.Value;
                if (selectedSurveys == "" || selectedSurveys == null)
                {                     
                    dvSuccessMsg.Visible = false;
                    dvErrMsg.Visible = true;
                    lblErrMsg.Text = String.Format(Utilities.ResourceMessage("errSelectSurvey"), SurveyMode.ToLower());//"Please select atleast one survey to move to another category.";
                    lblErrMsg.Visible = true;
                    lblSuccessMsg.Visible = false;
                    hdntxtValue.Value = "";
                    ddlSurvey.SelectedIndex = 0;                               
                }
                else if (lblCategoryHeading.Text.ToUpper() != ddlSurvey.SelectedItem.Text.ToUpper())
                {
                    selectedSurveys = hdntxtValue.Value;
                    List<osm_survey> survey = new List<osm_survey>();
                    var sessionService = ServiceFactory.GetService<SessionStateService>();
                    var loginUser = sessionService.GetLoginUserDetailsSession();
                    if (selectedSurveys != "" && loginUser != null)
                    {
                        osm_survey objSurvey = new osm_survey();
                        string[] surveyList = selectedSurveys.Split(',');
                        if (surveyList.Length > 0)
                        {
                            int count = 0;
                            SurveyService surveyService = new SurveyService();
                            for (int i = 0; i < surveyList.Length; i++)
                            {
                                objSurvey.SURVEY_ID =ValidationHelper.GetInteger(surveyList[i],0);
                                objSurvey.USERID = loginUser.UserId;
                                objSurvey.SURVEY_CATEGORY = ddlSurvey.SelectedItem.Text;
                                survey.Add(objSurvey);
                                surveyService.ChangeSurveyCategory(survey);
                                count++;
                            }                           
                           
                           // lblSuccessMsg.Text =  count + " survey(s) moved to " + ddlSurvey.SelectedItem.Text + " category.";
                            lblSuccessMsg.Text = String.Format(Utilities.ResourceMessage("successMsg"), count, ddlSurvey.SelectedItem.Text, SurveyMode.ToLower());
                            lblSuccessMsg.Visible = true;
                            dvSuccessMsg.Visible = true;
                            dvErrMsg.Visible = false;
                            lblErrMsg.Visible = false;
                            ddlSurvey.SelectedIndex = 0;
                            base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("ManageCategory.aspx", "surveyFlag=" + SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                        }
                    }
                }
                else
                {
                    dvSuccessMsg.Visible = false;
                    dvErrMsg.Visible = true;
                    lblErrMsg.Text = String.Format(Utilities.ResourceMessage("errSourceDestination"), SurveyMode.ToLower());//"Source & destination category are same, Please choose another category.";
                    lblErrMsg.Visible = true;
                    lblSuccessMsg.Visible = false;
                    hdntxtValue.Value = "";
                    ddlSurvey.SelectedIndex = 0;
                }
            }            
        }        
    }
}