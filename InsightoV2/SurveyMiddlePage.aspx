﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="SurveyMiddlePage.aspx.cs" Inherits="SurveyMiddlePage"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="marginNewPanel">
        <div class="PageNotFound">
            <div class="shadow">
                <h1>
                    <asp:Label ID="lblTitle" runat="server" Text="Survey Info" 
                        meta:resourcekey="lblTitleResource1"></asp:Label>
                    
                </h1>
                <div class="defaultH">
                </div>
                <p>
                    <asp:Label ID="lblSurveyDeleted" runat="server" 
                        Text="Survey has been deleted! Please wait while you are redirected to my survey's page." 
                        meta:resourcekey="lblSurveyDeletedResource1"></asp:Label>
                    
                </p>
                <div class="defaultH">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
