﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using FeatureTypes;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Configuration;


namespace Insighto.Pages
{
    public partial class SurveyManager : SurveyPageBase
    {
        int surveyId;
        int userId;
        int surveyMode;
        int QuestID = -1, Mode = 2;
        DateTime dt1=Convert.ToDateTime("1/1/0001 12:00:00 AM");
        DateTime dt2=Convert.ToDateTime("1/1/0001 12:00:00 AM");        
        Hashtable ht = new Hashtable();
        SurveyCore surcore = new SurveyCore();
        ValidationCore ValidCore = new ValidationCore();
       // private const string URL_SENDGRID_WITH_CREDENTIALS = @"https://sendgrid.com/api/bounces.get.json?api_user=knowience&api_key=insighto$9999&start_date={0}";
         int i = 0, count = 0;
         string str;

         private MailMessage mailMessage = null;
         private SmtpClient smtpMail = null;
         string strUrl = ConfigurationManager.AppSettings["RootURL"].ToString();
         [Flags]
         enum LaunchMethod
         {
             Emailthr = 1,
             Webthr = 2,
             Embedthr = 4,
             Fbthr = 8,
             Twthr = 16
         }

        protected void Page_Load(object sender, EventArgs e)
        {

      
            var launchDetails = ServiceFactory<LaunchSurveyService>.Instance.GetLaunchBySurveyId(SurveyBasicInfoView.SURVEY_ID);
            if (launchDetails.Count >0)
            {
               DataSet ds1 = bindsurveyEmails(SurveyBasicInfoView.SURVEY_ID);

               count = ds1.Tables[0].Rows.Count;


            }
            //lnkBouncedEmailReport.HRef = EncryptHelper.EncryptQuerystring("~/BouncedEmailReport.aspx", "SurveyId=" + SurveyID);
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    surveyId = Convert.ToInt32(ht[Constants.SURVEYID]);
                }
                if (ht != null && ht.Count > 0 && ht.Contains("surveyFlag"))
                {
                    surveyMode = Convert.ToInt32(ht["surveyFlag"]);
                }
                string strCopyLink = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyBasicsURL(), Constants.SURVEYID + "=" + surveyId + "&Mode=Copy&surveyFlag=" + base.SurveyFlag);
                hlnkCopy.NavigateUrl = strCopyLink;
               

            }
            if (surveyMode == 0)
            {
                lnkEdit.HRef = EncryptHelper.EncryptQuerystring("~/SurveyBasics.aspx", String.Format("SurveyId={0}&surveyFlag={1}&Mode=Edit", surveyId, surveyMode));
                lblHeading.InnerText = "Survey Name:";
            }
            else
            {
                lnkEdit.HRef = EncryptHelper.EncryptQuerystring("~/SurveyBasics.aspx", String.Format("SurveyId={0}&surveyFlag={1}&Mode=Edit", SurveyBasicInfoView.SURVEY_ID, surveyMode));
                lblHeading.InnerText = "Widget Name:";
            }

            CssApplied();
             
            var surveyService = ServiceFactory.GetService<SurveyService>();
            var surveyInfo = surveyService.GetSurveyBySurveyId(surveyId);
            var featureService = ServiceFactory.GetService<FeatureService>();
            var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
            var listFeatures = featureService.Find(GenericHelper.ToEnum<UserType>(userInfo.LicenseType));

            string PreviewUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewURL().Replace("~/", ""), "Mode=" + RespondentDisplayMode.Preview.ToString() + "&SurveyId=" + surveyId);

            if (userInfo.UserId > 0 && listFeatures != null)
            {
                if (featureService.Find(licenceType, FeatureSurveyCreation.PRINT_SURVEY).Value == 0)
                {
                    lBtnPrint.Attributes.Add("Onclick", "javascript: window.open('Upgradelicense.aspx','Upgradelicense','height=300,width=600,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;");
                }
                else
                {
                    string Navurlstr = EncryptHelper.EncryptQuerystring("PrintSurveyQuestions.aspx", "surveyId=" + surveyId + "&Prt=2&surveyFlag=" + base.SurveyFlag);
                    lBtnPrint.Attributes.Add("Onclick", "javascript: window.open('" + Navurlstr + "','SurveyPopup','height=360,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;");
                }
            }
            var ansService = ServiceFactory.GetService<SurveyAnswerService>();
            var surveyDetails = surveyService.GetSuvrveyDetailsBasedOnSurveyId(surveyId);
            var surveyQuesService = ServiceFactory.GetService<QuestionService>();
            var lstQuestions = surveyQuesService.GetSurveyquestionsDet(surveyId);
            var ansDet = ansService.GetSurveyAnswersbySurveyId(lstQuestions);

            if (QuestID < 1 && lstQuestions != null && lstQuestions.Count > 0)
            {

                foreach (var Ques in lstQuestions)
                {

                    if (!(Ques.QUESTION_TYPE_ID == 16 || Ques.QUESTION_TYPE_ID == 17 || Ques.QUESTION_TYPE_ID == 18 || Ques.QUESTION_TYPE_ID == 19 || Ques.QUESTION_TYPE_ID == 5 || Ques.QUESTION_TYPE_ID == 6 || Ques.QUESTION_TYPE_ID == 7 || Ques.QUESTION_TYPE_ID == 8 || Ques.QUESTION_TYPE_ID == 9 || Ques.QUESTION_TYPE_ID == 14 || Ques.QUESTION_TYPE_ID == 20))
                    {
                        QuestID = Ques.QUESTION_ID;
                        break;
                    }
                }
            }

            SelectedSurvey = surcore.GetSurveyWithResponsesCount(surveyId, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
          
            
            //if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0 && SelectedSurvey.STATUS != "Draft")
            //{
            //    int check_count = 0;
            //    int ques_seq = 1;
            //    foreach (SurveyQuestion ques in SelectedSurvey.surveyQues)
            //    {
            //        if (!(ques.QUESTION_TYPE == 16 || ques.QUESTION_TYPE == 17 || ques.QUESTION_TYPE == 18 || ques.QUESTION_TYPE == 19 || ques.QUESTION_TYPE == 5 || ques.QUESTION_TYPE == 6 || ques.QUESTION_TYPE == 7 || ques.QUESTION_TYPE == 8 || ques.QUESTION_TYPE == 9 || ques.QUESTION_TYPE == 14 || ques.QUESTION_TYPE == 20))
            //        {

            //            if (ques.QUESTION_ID == QuestID)
            //            {
            //                HtmlTable tbl = new HtmlTable();
            //                tbl.Width = "100%";
            //                if (ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11)
            //                {
            //                    tbl = BuildQuetions.BuildMatrixChart(ques, Mode, ques_seq);
            //                }
            //                else if (ques.QUESTION_TYPE == 12)
            //                {
            //                    tbl = BuildQuetions.BuildSidebysideMatrixChart(ques, Mode, ques_seq);
            //                }
            //                else if (ques.QUESTION_TYPE == 15)
            //                {
            //                    tbl = BuildQuetions.BuildRankingchartQuestion(ques, Mode, ques_seq,dt1,dt2);
            //                }
            //                else if (ques.QUESTION_TYPE == 13)
            //                {
            //                    tbl = BuildQuetions.BuildConstantSumchart(ques, Mode, "", ques_seq,dt1,dt2);

            //                }
            //                else
            //                {
            //                    tbl = BuildQuetions.BuildChartQuestion(ques, Mode, "", ques_seq, SurveyFlag, null, null);
            //                }
            //                check_count = 1;
            //                WebChartControl wcCtrl = null;
            //                HtmlTableRow tblChartRow = new HtmlTableRow();

            //                HtmlTableCell tblChartCell = new HtmlTableCell();
            //                ques.CHART_TITLE = "";
            //                ques.QUESTION_LABEL = "";
            //                ques.LEGEND_MAX_HORIZONTALALIGN = 10;
            //                ques.LEGEND_MAX_VERTICALALIGN = 10;
            //                ques.LEGEND_HORIZONTALALIGN = "Right";
            //                wcCtrl = BuildQuetions.GetChart(ques, 0,dt1,dt2);
            //                wcCtrl.Width = Unit.Pixel(300);
            //                wcCtrl.Height = Unit.Pixel(200);
            //                ASPxRoundPanelQuestionreports1.Controls.Add(wcCtrl);
            //                ChartExtraFeatures.SetchartDiagramProp(wcCtrl, ques);
            //                tblChartCell.Controls.Add(wcCtrl);
            //                tblChartRow.Cells.Add(tblChartCell);
            //                tbl.Rows.Add(tblChartRow);

            //                PanelReport.Controls.Add(tbl);
            //                ASPxRoundPanelQuestionreports1.Controls.Clear();

            //                break;
            //            }
            //            ques_seq = ques_seq + 1;
            //        }
            //    }
            //    if (check_count == 0)
            //    {
            //        HtmlTable tbl = new HtmlTable();
            //        tbl.Width = "100%";
            //        foreach (SurveyQuestion ques in SelectedSurvey.surveyQues)
            //        {
            //            WebChartControl wcCtrl = null;
            //            HtmlTableRow tblChartRow = new HtmlTableRow();
            //            HtmlTableCell tblChartCell = new HtmlTableCell();
            //            ques.CHART_TITLE = "";
            //            ques.QUESTION_LABEL = "";
            //            ques.LEGEND_MAX_HORIZONTALALIGN = 10;
            //            ques.LEGEND_MAX_VERTICALALIGN = 10;
            //            ques.LEGEND_HORIZONTALALIGN = "Right";
            //            wcCtrl = BuildQuetions.GetChart(ques, 0,dt1,dt2);
            //            wcCtrl.Width = Unit.Pixel(200);
            //            wcCtrl.Height = Unit.Pixel(500);
            //            ChartExtraFeatures.SetchartDiagramProp(wcCtrl, ques);
            //            tblChartCell.Controls.Add(wcCtrl);
            //            tblChartRow.Cells.Add(tblChartCell);
            //            tbl.Rows.Add(tblChartRow);
            //            PanelReport.Controls.Add(tbl);
            //            break;
            //        }

            //    }
            //}


            if (surveyInfo != null)
            {
                lBtnAddEmail.Enabled = (surveyInfo[0].STATUS == "Active") ? true : false;
                if (surveyInfo[0].STATUS != "Draft")
                {
                    lBtnClose.Enabled = false;

                }
                if (surveyInfo[0].STATUS == "Draft" || surveyInfo[0].STATUS == "Closed")
                {
                    lBtnClose.Enabled = false;

                }
                else
                {
                    lBtnClose.Enabled = true;

                }
            }
            if (userInfo != null)
            {

                string Naurlstr = EncryptHelper.EncryptQuerystring("SurveyRespondent.aspx", "Mode=1&SurveyId=" + surveyInfo[0].SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
            }
            if (surveyInfo[0].STATUS == "Closed")
                lBtnActivate.Enabled = true;
            else
                lBtnActivate.Enabled = false;
            if (surveyId != 0)
            {
                if (!IsPostBack)
                {
                    GetDashBoardData(userInfo.UserId, surveyId);

                }
                GetAlertsAndReminders();
            }

            if (hdnactivation.Value == "true")
            {

                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                    {
                        surveyId = Convert.ToInt32(ht[Constants.SURVEYID]);
                        if (surveyId > 0)
                        {
                             surveyService = ServiceFactory.GetService<SurveyService>();
                            surveyService.ChangeSurveyStatus(surveyId, "Closed");

                             sessionService = ServiceFactory.GetService<SessionStateService>();
                             userInfo = sessionService.GetLoginUserDetailsSession();


                            string OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + userInfo.LoginName);

                            string unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";


                            surveyclosedemail("Survey Closed", userInfo.FirstName, userInfo.LoginName, SelectedSurvey.SURVEY_NAME, unsubscribe);


                            lblStatus.Text = "Closed";
                            lBtnActivate.Visible = true;
                            lBtnActivate.Enabled = true;
                            lBtnClose.Enabled = false;
                            lBtnClose.PostBackUrl = "#";
                            lBtnAddEmail.Enabled = false;
                        }
                    }
                }
                lblSuccessMsg.Text = "Survey closed successfully.";
                dvSuccessMsg.Visible = true;
                dvErrMsg.Visible = false;
            } 
        }
        private void GetDashBoardData(int userId, int surveyId)
        {
            var surveyService = ServiceFactory.GetService<SurveyService>();
            var survey = surveyService.FindSuveysByUserIdBySurveyId(userId, surveyId);
            var surveyLaunch = surveyService.FindSurveyLaunchBySurveyId(surveyId);
            var surveyInfo = surveyService.GetSurveyBySurveyId(surveyId);
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();

            lblStatus.Text = survey[0].STATUS;
            lblFolder.Text = survey[0].FOLDER_NAME;
            lblLaunchedBy.Text = survey[0].NAME;
            lblCategory.Text = survey[0].SURVEY_CATEGORY;
            lblLastModified.Text = survey[0].MODIFIED_ON;
            lblInsighto.Text = survey[0].SEND_TO.ToString();
            int totalSent = survey[0].SEND_TO + survey[0].SEND_TO_EXT;
            txtExternal.Text = survey[0].SEND_TO_EXT.ToString();

            lblTotal.Text = totalSent.ToString();

            var responseDetails = surveyService.GetResponseSurveyBySurveyId(surveyId);

            lblScreenOut.Text = survey[0].SCREEN_OUTS.ToString();
            if (responseDetails != null)
            {
               // lblBounced.Text = responseDetails.BOUNCED_ID.ToString();
              //  lblBounced.Text = count.ToString();
                double bouncedPercentage = 0;
                if (totalSent > 0)
                {
                    bouncedPercentage = Math.Round(Convert.ToDouble(100 * count) / totalSent, 2);
                }
              //  lbleBouncedPercent.Text = Convert.ToString(bouncedPercentage) + "%";


                lblScreenOut.Text = survey[0].SCREEN_OUTS.ToString();
                double overquoper = 0;
                if (survey[0].SCREEN_OUTS > 0 && totalSent > 0)
                {
                    overquoper = Math.Round(Convert.ToDouble(100 * Convert.ToInt32(survey[0].SCREEN_OUTS)) / totalSent, 2);
                }
                lblScreenOutPercent.Text = Convert.ToString(overquoper) + "%";


                lblPartials.Text = responseDetails.ResponsePartial.ToString();
                double partialPercent = 0;
                if (responseDetails.ResponsePartial > 0 && totalSent > 0)
                    partialPercent = Math.Round(Convert.ToDouble(100 * Convert.ToInt32(responseDetails.ResponsePartial)) / totalSent, 2);
                lblPartialsPercent.Text = Convert.ToString(partialPercent) + "%";


                double completePercent = 0;
                lblCompletes.Text = responseDetails.ResponseComplete.ToString();
                if (responseDetails.ResponseComplete > 0 && totalSent > 0)
                {
                    completePercent = Math.Round(Convert.ToDouble(100 * Convert.ToInt32(responseDetails.ResponseComplete)) / totalSent, 2);
                }
                lblCompletesPercent.Text = Convert.ToString(completePercent) + "%";

                lblOverQuota.Text = responseDetails.ResponseOverQuota.ToString();

                double overQuotaPercent = 0;
                if (responseDetails.ResponseOverQuota > 0 && totalSent > 0)
                {
                    overQuotaPercent = Math.Round(Convert.ToDouble(100 * Convert.ToInt32(responseDetails.ResponseOverQuota)) / totalSent, 2);
                }
                lblOverQuotaPercent.Text = Convert.ToString(overQuotaPercent) + "%";
            }
            int screenout=survey[0].SCREEN_OUTS;
            lblVisits.Text = Convert.ToString(responseDetails.ResponseVisits + screenout);
            var visits = ValidationHelper.GetInteger(lblVisits.Text, 0);
            double visitPercent = 0;
            if (visits > 0 && totalSent > 0)
            {
                visitPercent = Math.Round(Convert.ToDouble(100 * (visits+screenout)) / totalSent, 2);
            }
            lblVisitPercent.Text = visitPercent.ToString() + "%";
            if (surveyLaunch != null && surveyLaunch.Count > 0)
                lblRecentLaunch.Text = "Launch Date:<br>" + BuildQuetions.GetConvertedDatetime(Convert.ToDateTime(surveyLaunch[0].LAUNCHED_ON), userInfo.StandardBIAS, userInfo.TimeZone, true);
            else
                lblRecentLaunch.Text = "Survey yet to be launched.";

            if (survey[0].EMAILLIST_TYPE == 1)
            {
                //txtSurveyUrl.Text = ConfigurationManager.AppSettings["RootURL"] + EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewURL().Replace("~/", ""), Constants.SURVEYID + "=" + surveyId + "&Mode=" + RespondentDisplayMode.PreviewWithPagination.ToString() + "&surveyFlag=" + base.SurveyFlag + "&Layout=" + survey[0].SURVEY_LAYOUT);

                //IBitlyService s = new BitlyService("satyakolli", "R_8a13c903550fc8f80a8a2efe1e481271");
                //string insightoLong = txtSurveyUrl.Text;
                //string shortened = s.Shorten(insightoLong);
                //if (shortened != null)
                //{
                //    txtSurveyUrl.Text = shortened;
                //}
              
                    txtSurveyUrl.Text = survey[0].SURVEYLAUNCH_URL;

            //    lblSurveyLaunchText.Visible = true;
              //  dvSurveyLunchText.Visible = true; 
                string strRemainderSurvey = EncryptHelper.EncryptQuerystring("SendRemainderEmailList.aspx", "SurveyId=" + base.SurveyID.ToString());
                hlnkEmailList.NavigateUrl = strRemainderSurvey;
            }
            else
            {
                txtSurveyUrl.Text = survey[0].SURVEYLAUNCH_URL;
                hlnkEmailList.CssClass = "top-small-gap icon-Remainder-off";
                hlnkEmailList.NavigateUrl = "javascript:void(0)";
                hlnkEmailList.Attributes.Remove("rel");
               // lblSurveyLaunchText.Visible = false;
              //  dvSurveyLunchText.Visible = false; 
               
            }
            int launchtypenumber = surcore.getLaunchTypeNumber(surveyId);
            LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
            if ((LaunchMethod.Embedthr & launchnew) == LaunchMethod.Embedthr)
            {
                string Embedcode = surcore.getEmbedCode(surveyId);
                txtEmbedCode.Text = Embedcode;
            }
            else
            {
                txtEmbedCode.Text = "";
            }
            lblSurveyName.Text = survey[0].SURVEY_NAME;
            if (txtSurveyUrl.Text != null || txtSurveyUrl.Text == "")
            {
                //hlnkSurveyUrl.NavigateUrl = txtSurveyUrl.Text;
            }

            if (surveyInfo != null && surveyLaunch.Count > 0)
            {
                int k = surveyLaunch.Count;
                if (surveyInfo[0].STATUS != "Draft" && surveyLaunch.Count > 0)
                {
                    if (surveyLaunch[k - 1].LAUNCHED_ON != null)
                    {
                        DateTime dtz = BuildQuetions.GetConvertedDatetime(Convert.ToDateTime(surveyLaunch[k - 1].LAUNCHED_ON), userInfo.StandardBIAS, userInfo.TimeZone, true);
                        lblRecentLaunch.Text = "Launch Date: <br>" + dtz;
                    }
                }

                if (surveyLaunch[k - 1].CREATED_ON != null && Convert.ToString(surveyLaunch[k - 1].CREATED_ON).Length > 0)
                {
                    if (surveyLaunch[k - 1].CREATED_ON != null)
                    {
                        DateTime dt = BuildQuetions.GetConvertedDatetime(Convert.ToDateTime(surveyLaunch[k - 1].CREATED_ON), userInfo.StandardBIAS, userInfo.TimeZone, true);
                    }
                }
            }

        }

        public static DateTime GetConvertedDatetime(DateTime value, int bias, string TimeZone, bool server_flag)
        {
            DateTime ret_value = value;
            if (value != null)
            {
                if (server_flag == true)
                    ret_value = value.AddMinutes(bias);
                else
                    ret_value = DateTime.SpecifyKind(value, DateTimeKind.Utc);

            }
            return ret_value;

        }
        protected void lBtnCopy_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    int surveyId = Convert.ToInt32(ht[Constants.SURVEYID]);
                    if (surveyId > 0)
                    {
                        string Navurlstr = EncryptHelper.EncryptQuerystring("SurveyBasics.aspx", "SurveyId=" + surveyId + "&Mode=Copy&surveyFlag=" + base.SurveyFlag);
                        Response.Redirect(Navurlstr);

                    }
                }
            }

        }
        protected void lBtnDelete_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht.Contains(Constants.SURVEYID))
                {

                    int SurveyId = Convert.ToInt32(ht[Constants.SURVEYID]);
                    var surveyService = ServiceFactory.GetService<SurveyService>();

                    if (SurveyId > 0)
                    {
                        var surveyInfo = surveyService.DeleteSurveyBySurveyId(SurveyId);
                        dvSuccessMsg.Visible = true;
                        dvErrMsg.Visible = false;
                        lblSuccessMsg.Text = "Survey successfully deleted. ";
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + SurveyFlag));
                    }
                }
            }
        }

        public void surveyclosedemail(string templatename, string username, string EmailID, string surveyname, string unsubscribeurl)
        {

            string viewData1;
            JavaScriptSerializer js1 = new JavaScriptSerializer();
            js1.MaxJsonLength = int.MaxValue;
            clsjsonmandrill prmjson1 = new clsjsonmandrill();
            //  var prmjson1 = new jsonmandrill();
            prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
            prmjson1.name = templatename;
            viewData1 = js1.Serialize(prmjson1);

            string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";


            WebClient request1 = new WebClient();
            request1.Encoding = System.Text.Encoding.UTF8;
            request1.Headers["Content-Type"] = "application/json";
            byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));

            string response1 = System.Text.Encoding.ASCII.GetString(resp1);


            clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);

            string bodycontent = jsmandrill11.code;


            string viewData;
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = int.MaxValue;
            var prmjson = new clsjsonmandrill.jsonmandrillmerge();
            prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
            prmjson.template_name = templatename;



            List<clsjsonmandrill.template_content> mtags = new List<clsjsonmandrill.template_content>();
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content00", content = "<div> Dear *|FNAME|*,</div>" });
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div>This is to notify you that your survey *|Survey_Name|* is now closed. You may track and analyze results of your surveys in My Surveys page once you login.</div>" });
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content02", content = "<div><p style='font-size:8pt;font-family:Arial,Sans-Serif'> *|UNSUBSCRIBEINSIGHTO|* </p></div>" });

            List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
            mvars.Add(new clsjsonmandrill.merge_vars { name = "FNAME", content = username });
            mvars.Add(new clsjsonmandrill.merge_vars { name = "Survey_Name", content = surveyname });
            mvars.Add(new clsjsonmandrill.merge_vars { name = "UNSUBSCRIBEINSIGHTO", content = unsubscribeurl });
            prmjson.template_content = mtags;
            prmjson.merge_vars = mvars;


            viewData = js.Serialize(prmjson);


            string url = "https://mandrillapp.com/api/1.0/templates/render.json";


            WebClient request = new WebClient();
            request.Encoding = System.Text.Encoding.UTF8;
            request.Headers["Content-Type"] = "application/json";
            byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

            string response = System.Text.Encoding.ASCII.GetString(resp);

            string unesc = System.Text.RegularExpressions.Regex.Unescape(response);

            string first4 = unesc.Substring(0, 9);

            string last2 = unesc.Substring(unesc.Length - 2, 2);


            string unesc1 = unesc.Replace(first4, "");
            string unescf = unesc1.Replace(last2, "");


            clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);

            //   string bodycontent = jsmandrill1.code;


            mailMessage = new MailMessage();
            mailMessage.To.Clear();
            mailMessage.Sender = new MailAddress("alerts@insighto.com", "Insighto.com");
            // mailMessage.Sender = new MailAddress(Sendmails.FROM_MAILADDRESS, "Insighto.com");
            mailMessage.From = new MailAddress("alerts@insighto.com", "Insighto.com");

            mailMessage.Subject = "[Important –  From Insighto.com] Your survey "+ surveyname + " is now closed";

            mailMessage.To.Add(EmailID);
            //  mailMessage.Headers.Add("survey_id", "6666");
            // mailMessage.Headers.Add("X-MC-Tags", "survey_respondent");
            mailMessage.Body = unescf;
            //if (Sendmails.SENDER_TOCCADDRESSES != null && Convert.ToString(Sendmails.SENDER_TOCCADDRESSES.Trim()).Length > 0)
            //{
            //    mailMessage.CC.Add(Sendmails.SENDER_TOCCADDRESSES.Trim());
            //}
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.Normal;
            smtpMail = new SmtpClient();
            smtpMail.Host = "smtp.mandrillapp.com";
            smtpMail.Port = 587;
            smtpMail.EnableSsl = true;
            smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpMail.Credentials = new System.Net.NetworkCredential("ram@knowience.com", "T74TJc3EZPYaIcvdBg--Dg");

            try
            {

                smtpMail.Send(mailMessage);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        //  Console.WriteLine("Delivery failed - retrying in 5 seconds.");


                        System.Threading.Thread.Sleep(5000);
                        smtpMail.Send(mailMessage);
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            //catch (System.Net.Mail.SmtpException ex)
            //{
            //    throw ex;
            //}
            mailMessage.Dispose();
        }


        protected void lBtnClose_Click(object sender, EventArgs e)
        {

            if (lblStatus.Text == "Active")
            {


                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                    {
                        surveyId = Convert.ToInt32(ht[Constants.SURVEYID]);

                        DataSet dsppstype = surcore.getSurveyType(surveyId);

                        //////                if (surveyId > 0)
                        //////                {
                        //////                    var surveyService = ServiceFactory.GetService<SurveyService>();
                        //////                    surveyService.ChangeSurveyStatus(surveyId, "Closed");

                        //////                    var sessionService = ServiceFactory.GetService<SessionStateService>();
                        //////                    var userInfo = sessionService.GetLoginUserDetailsSession();


                        //////string OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + userInfo.LoginName); 

                        //////string unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";


                        //////surveyclosedemail("Survey Closed", userInfo.FirstName, userInfo.LoginName, SelectedSurvey.SURVEY_NAME, unsubscribe);


                        //////                    lblStatus.Text = "Closed";
                        //////                    lBtnActivate.Visible = true;
                        //////                    lBtnActivate.Enabled = true;
                        //////                    lBtnClose.Enabled = false;
                        //////                    lBtnClose.PostBackUrl = "#";
                        //////                    lBtnAddEmail.Enabled = false;
                        //////                    //  lbn_sendreminders.Enabled = false;


                        if ((dsppstype.Tables[0].Rows[0][0].ToString() == "PPS_PRO") || (dsppstype.Tables[0].Rows[0][0].ToString() == "PPS_PREMIUM"))
                        {
                            Page.RegisterStartupScript("Upgrade", @"<script>Activationalert(); </script>");
                        }
                        else
                        {
                            Page.RegisterStartupScript("Upgrade", @"<script>Activationalertfree(); </script>");
                        }

                    }
                }
                        

                            // lblSuccessMsg.Text = "Selected survey has been closed.";
                            ////lblSuccessMsg.Text = "Survey closed successfully.";

                            ////dvSuccessMsg.Visible = true;
                            ////dvErrMsg.Visible = false;

                ////////        }
                ////////    }
                ////////}
            }
            else
            {

                lblErrMsg.Text = "Only active surveys can be closed.";
                dvSuccessMsg.Visible = false;
                dvErrMsg.Visible = true;

            }
        }
        protected void lBtnActivate_Click(object sender, EventArgs e)
        {

            if (lblStatus.Text == "Closed")
            {
                bool status = true;
                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht.Contains(Constants.SURVEYID))
                    {
                        surveyId = Convert.ToInt32(ht[Constants.SURVEYID]);
                        if (surveyId > 0)
                        {
                            DataSet dsppstype = surcore.getSurveyType(surveyId);
                            bool isNotProFeatureExist;

                            if (dsppstype.Tables[0].Rows[0][0].ToString() != "") 
                            {
                                //lblErrMsg.Text = "Survey has features that come with Paid plans.Please upgrade to activate the survey.";
                                //dvSuccessMsg.Visible = false;
                                //dvErrMsg.Visible = true;
                                isNotProFeatureExist = false;
                            }
                            else
                            {
                               // bool isNotProFeatureExist = ServiceFactory<SurveyService>.Instance.CheckSurveyProFeatures(surveyId);
                                 isNotProFeatureExist = ServiceFactory<SurveyService>.Instance.CheckSurveyProFeatures(surveyId);
                            }
                                if (isNotProFeatureExist)
                                {
                                    var sessionService = ServiceFactory.GetService<SessionStateService>();
                                    var userInfo = sessionService.GetLoginUserDetailsSession();
                                    var featureService = ServiceFactory.GetService<FeatureService>();
                                    var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
                                    var listFeatures = featureService.Find(GenericHelper.ToEnum<UserType>(userInfo.LicenseType));

                                    var surveyService = ServiceFactory.GetService<SurveyService>();
                                    surveyService.ChangeSurveyStatus(surveyId, "Active");


                                    lblStatus.Text = "Active";
                                    lBtnAddEmail.Enabled = true;
                                    lBtnActivate.Enabled = false;
                                    lBtnClose.Enabled = true;
                                    lBtnClose.Visible = true;
                                    // lbn_sendreminders.Enabled = true;been closed.
                                    lblSuccessMsg.Text = "Survey has been activated successfully.";
                                    dvSuccessMsg.Visible = true;
                                    dvErrMsg.Visible = false;
                                }
                                else
                                {
                                    //  lblErrMsg.Text = "Survey has pro features please upgarde your license to activate this survey.";

                                    lblErrMsg.Text = "Survey has features that come with Paid plans.Please upgrade to activate the survey.";
                                    dvSuccessMsg.Visible = false;
                                    dvErrMsg.Visible = true;
                                }
                            
                        }
                    }
                }

            }
            else
            {
                lblErrMsg.Text = "Survey will be activated only for closed surveys";
                dvSuccessMsg.Visible = false;
                dvErrMsg.Visible = true;
            }
        }
        protected void lBtnAddEmail_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    int surveyId = Convert.ToInt32(ht[Constants.SURVEYID]);
                    if (surveyId > 0)
                    {
                        string Navurlstr = EncryptHelper.EncryptQuerystring("AddressBook.aspx", "smSurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag);

                        Response.Redirect(Navurlstr);

                    }
                }
            }
        }
        protected void lBtnPrint_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    int surveyId = Convert.ToInt32(ht[Constants.SURVEYID]);
                    if (surveyId > 0)
                    {
                        string Navurlstr = EncryptHelper.EncryptQuerystring("PrintSurveyQuestions.aspx", "surveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag);
                        Response.Redirect(Navurlstr);
                    }
                }
            }
        }

        #region "Method : SetFeatures"
        /// <summary>
        /// This method returns bool value, if the feature flag is one.
        /// </summary>
        /// <param name="strFeature"></param>
        /// <returns></returns>

        public int SetFeatures(string strFeature)
        {
            int iVal = 0;
            var session = ServiceFactory.GetService<SessionStateService>();
            var userService = ServiceFactory.GetService<UsersService>();
            var sessdet = session.GetLoginUserDetailsSession();
            if (sessdet != null)
            {

                var serviceFeature = ServiceFactory.GetService<FeatureService>();
                var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));

                if (sessdet.UserId > 0 && listFeatures != null)
                {
                    foreach (var listFeat in listFeatures)
                    {

                        if (listFeat.Feature.Contains(strFeature))
                        {
                            iVal = (int)listFeat.Value;
                        }
                    }
                }
            }

            else
            {
                Response.Redirect(PathHelper.GetUserLoginPageURL());
            }
            return iVal;
        }

        #endregion


        protected void txt_external_ValueChanged(object sender, EventArgs e)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            if (userInfo != null)
            {
                int userId = userInfo.UserId;
                int surveyId = 0;
                if (Request.QueryString["key"] != null)
                {
                    dvSuccessMsg.Visible = false;
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                    {
                        surveyId = Convert.ToInt32(ht[Constants.SURVEYID]);
                    }
                    if (txtExternal.Text != "" && txtExternal.Text.Length > 0)
                    {
                        string Str = txtExternal.Text.Trim();
                        int externalCount = ValidationHelper.GetInteger(txtExternal.Text.Trim(), 0);
                        var surveyControlService = ServiceFactory.GetService<SurveyControls>();
                        surveyControlService.UpdateExternalCount(surveyId, externalCount);
                        GetDashBoardData(userId, surveyId);
                    }
                }
            }
        }

        public void GetAlertsAndReminders()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            if (userInfo.LicenseType == UserType.FREE.ToString())
            {
                HtmlTable emptyAlert = new HtmlTable();
               
                HtmlTableRow trow = new HtmlTableRow();
                HtmlTableCell tcell = new HtmlTableCell();
                Label lbl = new Label();
                lbl.Text = "No alerts were defined for this survey!";
                tcell.Controls.Add(lbl);
                trow.Cells.Add(tcell);
                emptyAlert.Rows.Add(trow);
                pnlAlerts.Controls.Add(lbl);
                HtmlTable emptyRemainder = new HtmlTable();
                HtmlTableRow remidnerTrow = new HtmlTableRow();
                HtmlTableCell reminderTcell = new HtmlTableCell();
                Label remidnerlbl = new Label();
                remidnerlbl.Text = "No reminders were defined for this survey!";
                reminderTcell.Controls.Add(remidnerlbl);
                remidnerTrow.Cells.Add(reminderTcell);
                emptyRemainder.Rows.Add(remidnerTrow);
                pnlRemainder.Controls.Add(emptyRemainder);
            }
            else
            {
                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    surveyId = Convert.ToInt32(ht[Constants.SURVEYID]);
                    // var SelectedSurvey = surcore.GetSurveyWithResponsesCount(surveyId, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                }

                SelectedSurvey = surcore.GetSurveyWithResponsesCount(surveyId, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                DataSet ds_surveyalerts = new DataSet();
                int color1 = 0;

                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    int SuryID = Convert.ToInt32(ht[Constants.SURVEYID]);

                    if (SuryID > 0)
                        ds_surveyalerts = surcore.GetSurveyAlerts(SuryID);
                }





                if (SelectedSurvey != null && SelectedSurvey.STATUS != "Draft")
                {
                    if (ds_surveyalerts != null && ds_surveyalerts.Tables.Count > 0)
                    {
                        HtmlTable Alerts = new HtmlTable();
                        HtmlTable Reminder = new HtmlTable();
                        int z = 0;
                       // Alerts.Height = "60";
                        int check_launch_alert = 0;
                        for (int i = 0; i < ds_surveyalerts.Tables[0].Rows.Count; i++)
                        {
                            HtmlTableRow trow = new HtmlTableRow();
                            HtmlTableCell tcell = new HtmlTableCell();
                            Label lbl = new Label();
                            string str = "";
                            // lbl.ForeColor = Color.RED;
                            DateTime dt_display = Convert.ToDateTime("1/1/1800");
                            if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_TYPE"].ToString() == "SurveyLaunchOnDate")
                            {

                                if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"] != null)
                                {
                                    dt_display = Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]);

                                    if (check_launch_alert == 0)
                                    {
                                        if (Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]) <= DateTime.Now)
                                        {
                                            //if (dt_display.AddDays(7) > DateTime.UtcNow)
                                            //{
                                            str = " '" + SelectedSurvey.SURVEY_NAME + "' survey launched successfully on ";
                                            str += BuildQuetions.GetConvertedDatetime(Convert.ToDateTime(dt_display), userInfo.StandardBIAS, userInfo.TimeZone, true);
                                                check_launch_alert += 1;

                                           // }
                                        }
                                        if (Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]) > DateTime.Now)
                                        {
                                            //if (dt_display <= (DateTime.UtcNow.AddDays(7)))
                                            //{

                                                str = "'" + SelectedSurvey.SURVEY_NAME + "' survey is scheduled to be launched on ";
                                                str += BuildQuetions.GetConvertedDatetime(Convert.ToDateTime(dt_display), userInfo.StandardBIAS, userInfo.TimeZone, true);
                                                check_launch_alert += 1;
                                            //}
                                        }
                                    }
                                    else if (check_launch_alert >= 1)
                                    {

                                        if (dt_display <= (DateTime.UtcNow.AddDays(7)))
                                        //str = "-> Your Survey " + SelectedSurvey.SURVEY_NAME + " has Relaunched on ";
                                        {
                                            //str = "'" + SelectedSurvey.SURVEY_NAME + "' launched successfully with new email list on ";
                                            //str += BuildQuetions.GetConvertedDatetime(Convert.ToDateTime(dt_display), userInfo.StandardBIAS, userInfo.TimeZone, true);
                                            check_launch_alert += 1;
                                        }

                                    }



                                }
                            }
                            else if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_TYPE"].ToString() == "SurveyCloseOnDate")
                            {
                                if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"] != null)
                                {
                                    if (Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]) != Convert.ToDateTime("1/1/1900 12:00:00 AM") && Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]) < DateTime.Now)
                                    {

                                        dt_display = Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]);
                                       // if (dt_display.AddDays(7) >= (DateTime.UtcNow))
                                      //  {
                                        if (SelectedSurvey.STATUS == "Closed")
                                        {
                                            str = "'" + SelectedSurvey.SURVEY_NAME + "' is no longer open for input. ";
                                        }
                                       // }

                                    }
                                }
                            }
                            else if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_TYPE"].ToString() == "SurveyCloseOnResponseCount")
                            {

                                if (SelectedSurvey != null && SelectedSurvey.surveyOptns != null)
                                {
                                    foreach (SurveyOptions suropts in SelectedSurvey.surveyOptns)
                                    {
                                        if (ds_surveyalerts.Tables.Count > 1 && ds_surveyalerts.Tables[1].Rows.Count > 0)//&& suropts.CLOSE_ON_DATE.AddDays(7) >= DateTime.UtcNow
                                        {

                                            if (Convert.ToInt32(ds_surveyalerts.Tables[0].Rows[i]["ALERT_INFO"]) > 0)
                                                str = "'" + SelectedSurvey.SURVEY_NAME + "' has crossed your required quota(" + Convert.ToInt32(ds_surveyalerts.Tables[0].Rows[i]["ALERT_INFO"]) + ") of respondents. Your survey is now closed. ";
                                            break;
                                        }

                                    }
                                }
                            }
                            else if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_TYPE"].ToString() == "MinResponse" && ds_surveyalerts.Tables[0].Rows[i]["ALERT_INFO"].ToString() != "0")
                            {
                                if (SelectedSurvey != null && SelectedSurvey.surveyOptns != null)
                                {
                                    foreach (SurveyOptions suropts in SelectedSurvey.surveyOptns)
                                    {
                                        if (ds_surveyalerts.Tables.Count > 1 && ds_surveyalerts.Tables[1].Rows.Count > 0)
                                        {
                                            if (Convert.ToInt32(ds_surveyalerts.Tables[1].Rows[0].ItemArray[0].ToString()) >= suropts.ALERT_ON_MIN_RESPONSES && Convert.ToInt32(ds_surveyalerts.Tables[0].Rows[i]["ALERT_INFO"]) >= 0)
                                            {
                                                bool test = true;
                                                if (ds_surveyalerts.Tables.Count > 2 && ds_surveyalerts.Tables[2].Rows.Count >= suropts.ALERT_ON_MIN_RESPONSES && Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]) != Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                                                {
                                                    dt_display = Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]);
                                                    int res_count = surcore.GetResponseCount(SurveyID);
                                                    if (res_count >= suropts.ALERT_ON_MIN_RESPONSES && DateTime.Now >= dt_display)
                                                    {
                                                        str = "'" + SelectedSurvey.SURVEY_NAME + "' survey has crossed your required minimum (" + suropts.ALERT_ON_MIN_RESPONSES + ") number of respondents successfully.";
                                                        test = false;
                                                        break;
                                                    }
                                                }
                                                if (test && ds_surveyalerts.Tables.Count > 2 && ds_surveyalerts.Tables[2].Rows.Count >= suropts.ALERT_ON_MIN_RESPONSES && Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]) == Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                                                {
                                                    dt_display = Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]);
                                                    //if (dt_display.AddDays(7) >= (DateTime.UtcNow))
                                                    //{
                                                        str = "'" + SelectedSurvey.SURVEY_NAME + "' survey has crossed your required minimum (" + suropts.ALERT_ON_MIN_RESPONSES + ") number of respondents";
                                                        break;
                                                    //}
                                                }

                                            }
                                        }

                                    }
                                }
                            }

                            else if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_TYPE"].ToString() == "MaxResponse" && Utilities.ShowOrHideFeature(Utilities.GetUserType(), (int)Deployment.Alert_on_Min_and_Max_No_of_Respondents) && ds_surveyalerts.Tables[0].Rows[i]["ALERT_INFO"].ToString() != "0") //&& (AuthenticatedUser.FeatureList).ALERT_MINMAXRESP == 1)
                            {
                                if (SelectedSurvey != null && SelectedSurvey.surveyOptns != null)
                                {
                                    foreach (SurveyOptions suropts in SelectedSurvey.surveyOptns)
                                    {
                                        if (ds_surveyalerts.Tables.Count > 1 && ds_surveyalerts.Tables[1].Rows.Count > 0)
                                        {
                                            if (Convert.ToInt32(ds_surveyalerts.Tables[1].Rows[0].ItemArray[0].ToString()) >= suropts.ALERT_ON_MAX_RESPONSES && Convert.ToInt32(ds_surveyalerts.Tables[0].Rows[i]["ALERT_INFO"]) > 0)
                                            {
                                                bool test = true;
                                                if (ds_surveyalerts.Tables.Count > 2 && ds_surveyalerts.Tables[2].Rows.Count >= suropts.ALERT_ON_MAX_RESPONSES && Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]) != Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                                                {
                                                    dt_display = Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]);
                                                    int res_count = surcore.GetResponseCount(SurveyID);
                                                    if (res_count >= suropts.ALERT_ON_MAX_RESPONSES && DateTime.Now >= dt_display)
                                                    {
                                                        str = "'" + SelectedSurvey.SURVEY_NAME + "' survey has crossed your required maximum(" + suropts.ALERT_ON_MAX_RESPONSES + ") number of respondents successfully.";
                                                        test = false;
                                                        break;
                                                    }
                                                }
                                                if (test && ds_surveyalerts.Tables.Count > 2 && ds_surveyalerts.Tables[2].Rows.Count >= suropts.ALERT_ON_MAX_RESPONSES && Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]) == Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                                                {
                                                    dt_display = Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]);
                                                    //if (dt_display.AddDays(7) >= (DateTime.UtcNow))
                                                    //{
                                                        str = "'" + SelectedSurvey.SURVEY_NAME + "' survey has crossed your required maximum(" + suropts.ALERT_ON_MAX_RESPONSES + ") number of respondents";
                                                        break;
                                                    //}
                                                }

                                            }
                                        }

                                    }
                                }

                            }

                            else if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_TYPE"].ToString() == "ReminderText" && SetFeatures("REMINDERS") == 1)// && (AuthenticatedUser.FeatureList).REMINDERS == 1)
                            {
                                if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"] != null)
                                {

                                   

                                    dt_display = Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]);

                                    
                                   // if (dt_display.AddDays(7) >= (DateTime.UtcNow))
                                    //{
                                    string cntstring = "01-jan-1900";
                                    if (Convert.ToDateTime(dt_display).ToString("dd-MMM-yyyy") == Convert.ToDateTime(cntstring).ToString("dd-MMM-yyyy"))
                                    {
                                        str =  Convert.ToString(ds_surveyalerts.Tables[0].Rows[i]["ALERT_INFO"]);
                                    }
                                    else
                                    {

                                        str = Convert.ToDateTime(dt_display).ToString("dd-MMM-yyyy") + ": " + Convert.ToString(ds_surveyalerts.Tables[0].Rows[i]["ALERT_INFO"]);
                                    }
                                        z = 1;
                                   // }
                                    //lbl.ForeColor = Color.BLUE;
                                }
                            }
                            else if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_TYPE"].ToString() == "Manually Closed")
                            {
                                if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"] != null)
                                {
                                    dt_display = Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]);
                                    //if (dt_display.AddDays(7) >= (DateTime.UtcNow))
                                    //{
                                        str = "Your active survey, '" + SelectedSurvey.SURVEY_NAME + "' has been closed on " + Convert.ToDateTime(dt_display).ToString("dd-MMM-yyyy") + " at " + dt_display.ToShortTimeString();

                                   // }
                                }
                            }

                            else if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_TYPE"].ToString() == "Manually Activated")
                            {
                                if (ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"] != null)
                                {
                                    dt_display = Convert.ToDateTime(ds_surveyalerts.Tables[0].Rows[i]["ALERT_DATE"]);
                                    if (dt_display >= (DateTime.Now))
                                    {
                                        str = "Your closed survey, '" + SelectedSurvey.SURVEY_NAME + "' has been activated on " + Convert.ToDateTime(dt_display).ToString("dd-MMM-yyyy") + " at " + dt_display.ToShortTimeString();

                                    }
                                }
                            }
                            if (str.Length > 0)
                            {

                                lbl.Text = str;
                                tcell.Controls.Add(lbl);
                                trow.Cells.Add(tcell);
                                if (z == 1)
                                {
                                    Reminder.Rows.Add(trow);
                                    z = 0;
                                }
                                else
                                {
                                    Alerts.Rows.Add(trow);
                                }
                            }

                        }

                        pnlAlerts.Controls.Add(Alerts);
                        pnlRemainder.Controls.Add(Reminder);
                        if (Alerts.Rows.Count <= 0)
                        {
                            HtmlTableRow trow = new HtmlTableRow();
                            HtmlTableCell tcell = new HtmlTableCell();
                            Label lbl = new Label();
                            lbl.Text = "No alerts were defined for this survey!";
                            tcell.Controls.Add(lbl);
                            trow.Cells.Add(tcell);
                            Alerts.Rows.Add(trow);
                            pnlAlerts.Controls.Add(Alerts);
                        }
                        if (Reminder.Rows.Count <= 0)
                        {
                            HtmlTableRow trow = new HtmlTableRow();
                            HtmlTableCell tcell = new HtmlTableCell();
                            Label lbl = new Label();
                            lbl.Text = "No reminders were defined for this survey!";
                            tcell.Controls.Add(lbl);
                            trow.Cells.Add(tcell);
                            Reminder.Rows.Add(trow);
                            pnlRemainder.Controls.Add(Reminder);
                        }
                    }
                }
                else
                {
                    HtmlTable Alerts = new HtmlTable();
                    HtmlTable Reminder = new HtmlTable();
                    HtmlTableRow trow = new HtmlTableRow();
                    HtmlTableRow trow1 = new HtmlTableRow();
                    HtmlTableCell tcell = new HtmlTableCell();
                    HtmlTableCell tcell1 = new HtmlTableCell();
                    Label lbl = new Label();
                    Label lbl1 = new Label();

                    lbl1.Text = "No reminders were defined for this survey!";
                    tcell1.Controls.Add(lbl1);
                    trow.Cells.Add(tcell1);
                    Alerts.Rows.Add(trow1);
                    pnlAlerts.Controls.Add(Alerts);

                    lbl.Text = "No alerts were defined for this survey!";
                    tcell.Controls.Add(lbl);
                    trow1.Cells.Add(tcell);
                    Reminder.Rows.Add(trow);
                    pnlRemainder.Controls.Add(Reminder);

                }
            }
        }
        //private string FormatRespondentUrl(string navigationUrl)
        //{
        //    navigationUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetRespondentPage(BasePage.Mode), string.Format("SurveyId={0}&PageIndex={1}&Mode={3}&Layout={8}", SurveyId, Utilities.ToInt(hdnpageIndex.Value), hdnRespondentId.Value, BasePage.Mode, 1, BasePage.PasswordVerified, BasePage.LaunchId, BasePage.EmailId, BasePage.ShowTwoColumnLayOut == true ? 2 : 1));
        //    return navigationUrl;
        //}
        protected void lBtnReports_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    int surveyId = Convert.ToInt32(ht[Constants.SURVEYID]);
                    if (surveyId > 0)
                    {
                        string Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag);

                        Response.Redirect(Navurlstr);

                    }
                }
            }
        }

        protected void CssApplied()
        {
            string surveyStatus = base.SurveyBasicInfoView.STATUS;
            if (surveyStatus.ToUpper() == "ACTIVE" || surveyStatus.ToUpper() == "CLOSED")
            {
                lBtnReports.CssClass = "top-small-gap icon-Reports-active";
            }
            else
            {
                lBtnReports.CssClass = "top-small-gap icon-Reports-off";
            }
            if (surveyStatus.ToUpper() == "ACTIVE" || surveyStatus.ToUpper() == "DRAFT")
            {
                lBtnSurveyEdit.CssClass = "top-small-gap icon-Edit-active";
            }
            else
            {
                lBtnSurveyEdit.CssClass = "top-small-gap icon-Edit-off";
            }
            if (surveyStatus.ToUpper() == "ACTIVE")
            {

                hlnkEmailList.CssClass = "top-small-gap icon-Remainder-active";
            }
            else
            {
                hlnkEmailList.CssClass = "top-small-gap icon-Remainder-off";
            }

        }

        protected void lBtnSurveyEdit_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    int surveyId = Convert.ToInt32(ht[Constants.SURVEYID]);
                    if (surveyId > 0)
                    {
                        string Navurlstr = EncryptHelper.EncryptQuerystring("SurveyMiddlePage.aspx", "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag);

                        Response.Redirect(Navurlstr);

                    }
                } 
            }
        }
        private static List<Message> GetBouncedEmails(string targetUrl)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = 999999999;
            var bounceEmails = ProcessSendGridAccount(string.Format("{0}&{1}", targetUrl, "date=1"));
            return serializer.Deserialize<List<Message>>(bounceEmails);
        }
        public static string ProcessSendGridAccount(string newUrl)
        {
            var recieve = string.Empty;
            try
            {
                var url = WebRequest.Create(newUrl);
                //url.Credentials = new NetworkCredential { UserName = "koti@koti.com", Password = "test123" };
                var response = url.GetResponse();
                var input = new StreamReader(response.GetResponseStream());
            
                recieve = input.ReadToEnd();

                //Console.WriteLine(recieve);
                url.Abort();
                input.Close();
                response.Close();
            }
            catch (Exception ee)
            {

                Console.WriteLine(ee.ToString());

            }
            return recieve;

        }
        public SqlConnection strconnectionstring()
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection strconn = new SqlConnection(connStr);

                strconn.Open();
                return strconn;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet bindsurveyEmails(int surveyID)
        {
            SqlConnection con = new SqlConnection();
            try
            {
               
                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GETEmailcomparisonlist", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyID;

                SqlDataAdapter sda = new SqlDataAdapter(scom);

                //SqlDataReader reader = scom.ExecuteReader();
                //while (reader.Read())
                //{
                //    lstdata.Add(new EmailMessage(reader["status"].ToString(), reader["created"].ToString(),reader["reason"].ToString(), reader["email"].ToString()));


                //}
                DataSet dsSurveyQA = new DataSet();
                sda.Fill(dsSurveyQA);
                return dsSurveyQA;
                //return lstdata;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public  DataTable ConvertListToDataTable(List<string> list)
        {
            // New table.
            DataTable table = new DataTable();

            // Get max columns.
            int columns = 1;
            //foreach (var array in list)
            //{
            //    if (array.Length > columns)
            //    {
            //        columns = array.Length;
            //    }
            //}

            // Add columns.
            for (int i = 0; i < columns; i++)
            {
                table.Columns.Add();
            }

            // Add rows.
            foreach (var array in list)
            {
                table.Rows.Add(array);
            }

            return table;
        }

        public DataTable ToDataTable<T>(List<T> items)
        {

            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties

          PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in Props)
            {

                //Setting column names as Property names

                dataTable.Columns.Add(prop.Name);

            }

            foreach (T item in items)
            {

                var values = new object[Props.Length];

                for (int i = 0; i < Props.Length; i++)
                {

                    //inserting property values to datatable rows

                    values[i] = Props[i].GetValue(item, null);

                }

                dataTable.Rows.Add(values);

            }

            //put a breakpoint here and check datatable

            return dataTable;

        }

    }
}
