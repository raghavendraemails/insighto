﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SaveToWordPage.aspx.cs" Inherits="SaveToWordPage" %>

<%@ Reference VirtualPath="~/UserControls/RespondentControls/IntroductionTextControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Free Online Survey and Questionnaire Software for Customer Research and Feedback</title>
    <!-- start - style sheets included for respondent/preview/print -->
    <style type="text/css">
      
        .progresstextPnl
        {
            text-align: center;
        }
        .quationtxtPanel
        {
            color: #333333;
            font-size: 12px;
            text-align: center;
        }
        .mandatorylbl
        {
            color: Red;
            padding: 0 1px;
        }
        .surveyRespondQuestionColumn
        {
            padding-bottom: 8px;
            padding-top: 8px;
        }
        .printLink
        {
            background: url("App_Themes/Images/icon-print.gif") no-repeat scroll left center transparent;
            padding-right: 10px;
        }
        .popupCloseLink
        {
            /*background: url("../images/icon_close1.gif") no-repeat;*/
            background-color: #843D99;
            height: 22px;
            width: 22px;
            border: solid 1px red;
            color: #fff;
        }
        .pageBreakControls
        {
            font-weight: bold;
            text-align: center;
            width: 100%;
        }
        .pageBreaklbl
        {
            color: #843D99;
        }
        .narrationHeader
        {
            font-weight: 700;
            padding: 10px 0 10px 0;
            margin: 10px 0px 10px 10px; /*  margin:0 0 10px 0; */
        }
        .narrationHeader p
        {
            background: none;
            padding-left: 57px; /* padding-left: 48px; */
        }
        /* padding-left:25px; */
        .narrationHeader span
        {
            background: none;
        }
        .narrationHeader span p, .narrationHeader span p span
        {
            color: #333;
        }
    </style>
    <style id="dynamicStyle" runat="server" type="text/css">
        
    </style>
    <style id="currentTheme" runat="server" type="text/css">
        
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="questionViewpanel">
        <div class="surveyHeader">
            <div class="customHeaderText">
                <asp:Label ID="lblSurveyRespondentHeader" runat="server" Text=" "></asp:Label>
            </div>
            <div style="display: block;" id="divLogo" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" id="tblLogo" runat="server">
                </table>
            </div>
            <div class="clear">
            </div>
        </div>
        <p align="right" class="proButton">
            &nbsp;
        </p>
        <div class="clear">
        </div>
        <div class="defaultHeight">
        </div>
        <!-- STARTS Single column lay out container STARTS -->
        <div class="divQuestions qOptionPanel" id="divQuestionsPanel" runat="server">
            <div class="text">
                <asp:Panel ID="pnlQuestions" runat="server" />
            </div>
        </div>
        <!-- END Single column lay out container END -->
        <!-- STARTS Two column lay out container STARTS -->
        <div class="divQuestions" runat="server" id="divTwoColumnPanel">
            <asp:Panel CssClass="twocolumnpanel" runat="server" ID="pnlTwoColumnQuestions">
            </asp:Panel>
            <div class="clear">
            </div>
        </div>
        <!-- END Two column lay out container END -->
        <div class="clear">
        </div>
        <div class="bottomButtonPanel" id="divButtonsPanel" runat="server">
            <asp:Button ID="btnContinue" runat="server" Text="Continue" CssClass="dynamicButton"
                OnClick="btnContinue_Click" />
            <asp:ImageButton ID="imgContinueButton" runat="server" OnClick="btnContinue_Click" />
        </div>
        <div class="customFooterText">
            <asp:Label ID="lblSurveyRespondentFooter" runat="server" Text=" "></asp:Label>
        </div>
        <asp:HiddenField ID="hdnpageIndex" runat="server" />
        <input type="hidden" id="hdnRespondentId" runat="server" class="hdnRespondentId" />
        <asp:HiddenField ID="hdnPasswordVerified" runat="server" ClientIDMode="Static" />
    </div>
    <div class="pwdbylogopading">
        <a href="http://insighto.com">
            <img alt="powered by Insighto" src="http://insighto.debug.covalense.net/App_Themes/Classic/Images/pwdinsighto.jpg"
                border="0" /></a>
    </div>
    </form>
</body>
</html>
