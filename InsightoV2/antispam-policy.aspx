﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="antispam-policy.aspx.cs" Inherits="new_antispam_policy" MasterPageFile="~/Home.Master" %>
<%@ Register TagPrefix="uc" TagName="OtherHeader" Src="~/UserControls/OtherHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc:OtherHeader ID="OtherHeader" runat="server" />
    <div> 
      <!--=== Content Part ===-->
      <div class="breadcrumbs-v3 img-v1 text-center">
        <div class="container">
          <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;"> <span>Anti Spam Policy</span></h2>
        </div>
      </div>
      <p>&nbsp;</p>
      <!--=== Content Part ===-->
      <div class="container">
        <div class="row-fluid privacy">
          <h4>Knowience Anti Spam Policy</h4>
          <h4><strong>1. Spamming defined:</strong></h4>
          <p>Spamming is an activity whereby email messages are directly or indirectly transmitted to any email
        
            address that has not solicited such email and does not consent to such transmission or receipts.</p>
          <p>Knowience also consider spamming to constitute posting advertisements in newsgroups in violation of
        
            the terms of participation in such newsgroup that are off topic, or in newsgroups that do not 
        
            specifically permit advertisements.</p>
          <p>Knowience also consider it spamming when advertisements are placed on message boards or in chat
        
            rooms when they are not permitted by the terms of participation in such message boards and chat 
        
            rooms.</p>
          <h4><strong>2. Anti Spam Policy</strong></h4>
          <p>Any e-mail or message sent, or caused to be sent, to Knowience Users or through the Knowience 
        
            Services may not:</p>
          <ol>
            <li>use or contain invalid or forged headers</li>
            <li>use or contain invalid or non-existent domain names</li>
            <li>employ any technique to otherwise misrepresent, hide or obscure any information in identifying the
              point of origin or the transmission path</li>
            <li>use other means of deceptive addressing</li>
            <li>use a third party's internet domain name, or be relayed from or through a third party's equipment,
              without permission of the third party or</li>
            <li>contain false or misleading information in the subject line or otherwise contain false or misleading
              content.</li>
          </ol>
          <p>Knowience does not authorize the harvesting, mining or collection of e-mail addresses or other 
        
            information from or through the Knowience Services. Knowience does not permit or authorize others 
        
            to use the Knowience Services to collect, compile or obtain any information about its customers or 
        
            subscribers, including but not limited to subscriber e-mail addresses, which are Knowience's 
        
            confidential and proprietary information. Use of the Knowience Services is also subject to the 
        
            Knowience Privacy Statement and the Knowience Website Terms of Use and Notices.</p>
          <p>Knowience does not permit or authorize any attempt to use the Knowience Services in a manner that
        
            could damage, disable, overburden or impair any aspect of any of the Knowience Services, or that 
        
            could interfere with any other party's use and enjoyment of any Knowience Service.</p>
          <p>Knowience forbids the sending of unsolicited mass Emails or unsolicited Emails of any kind in 
        
            connection with use of Insighto.com.</p>
          <p>In the event that Knowience deems you to be in violation of these policies, Knowience shall
        
            immediately revoke your membership rights and close any active account.</p>
          <p>Knowience also reserve the right to suspend your account and participation pending review upon
        
            receipt of any complaint or other evidence that you may be engaged in any spamming activity. 
        
            Knowience may initiate such legal action as it may deem necessary or is advised or shall co-operate 
        
            with any authority and provide all information, material and or such other material and information as 
        
            it may have access to, to protect its rights as also of its clients, customers, users, and others who may 
        
            either directly or indirectly be effected in any way.</p>
          <h4><strong>3. Report</strong></h4>
          <p>If you are “spammed” by anyone regarding our/their products, services, website, or any other matters, 
            please report this activity to <a href="mailto:compliance@insighto.com">compliance@insighto.com</a></p>
        </div>
        <!--/row-fluid--> 
      </div>
      <!--/container-->   
    </div>
<!--/wrapper--> 
</asp:Content>