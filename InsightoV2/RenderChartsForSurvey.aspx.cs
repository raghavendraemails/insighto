﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts.Web;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using sharp = iTextSharp.text;
using FeatureTypes;
using Insighto.Business.ValueObjects;
using System.Text;
using System.Linq;
using System.Threading;
using DevExpress.Web.ASPxClasses;
using System.Diagnostics;
using InfoSoftGlobal;
using System.Security.Cryptography;
using iTextSharp.text.html.simpleparser;
using System.Reflection;
using OfficeOpenXml;
using Office = Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.Runtime.InteropServices;
using Word = Microsoft.Office.Interop.Word;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
//using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Packaging.Extensions;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Threading.Tasks;

public partial class RenderChartsForSurvey : System.Web.UI.Page
{
    #region "Variables"
    /// <summary>
    /// class level variables.
    /// </summary>
    int QuestID = -1, Mode = 0, check_flag = 0, SMode = 0, QType_count = 0;
    public int surveyID = 0;
    int QuesSeq = 0;
    SurveyCore surcore = new SurveyCore();
    SurveyCoreVoice surcorevoice = new SurveyCoreVoice();
    PPTXopenXML pptxml = new PPTXopenXML();

    Hashtable ht = new Hashtable();
    string ViewAllQues = "";
    DateTime dt1 = new DateTime();
    DateTime dt2 = new DateTime();
    bool isDateFilter = false;
    //int pageIndex = 0;
    //int pageSize = 10;

    string strquestionid;
    string tempquestionid = "";
    string firstview = "";
    public string jsstrchartname;
    public string jsstrchartnameppt;
    public string SAVE_PATH;
    public string SAVE_PATHRET;
    public int fcdelaytime;
    public string exportchartfilehandler;
    public string strlicensetype;
    System.Data.DataTable colgroupoptions123413;
    System.Data.DataTable colgroupoptions123413Export;
    System.Data.DataTable dtchart;
    System.Data.DataTable colgroupoptions123413ppt;
    System.Data.DataTable colgroupoptions1011ppt;
    System.Data.DataTable colgroupoptions12ppt;
    System.Data.DataTable colgroupoptions15ppt;
    double maxAllowedResponseLimit = 0;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }

}