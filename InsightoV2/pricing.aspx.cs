﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using Resources;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;
using System.Net;
using System.Web.UI;

public partial class Pricing : System.Web.UI.Page
{
    string countryName = "";
    public string BasicAmt;
    public string prosingleAmt;
    public string premsingleAmt;
    public string premannualAmt;
    SurveyCore surcore = new SurveyCore();
    protected void Page_Load(object sender, EventArgs e)
    {
        string ipAddress = "";
        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
        {
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        }

        countryName = Utilities.GetCountryName(ipAddress);
        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(countryName);

        countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();

        if (countryName.ToUpper() != "INDIA")
        {

            // countryName = "AC";
            BasicAmt = "$0";
            prosingleAmt = "$9";
            premsingleAmt = "$29";
            premannualAmt = "$249";
           
        }
        else
        {
            // &#x20b9;
            //&#8377;

            BasicAmt = "&#8377;" + "0";
            prosingleAmt = "&#8377;" + "399";
            premsingleAmt = "&#8377;" + "1,299";
            premannualAmt = "&#8377;" + "9,999";

           
        }
    }
}