﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Text;
using System.Configuration;
using Insighto.Data;
using System.Data;
namespace Insighto.Pages
{
    public partial class LaunchConfirmation : SurveyPageBase
    {
        string listName = "";
        osm_widget widget = null; //ServiceFactory<WidgetService>.Instance.GetWidgetDetailsBySurveyId(SurveyID);
        osm_WidgetSettings widgetSettings = null;
        Hashtable typeHt = new Hashtable();
        SurveyCore surcore = new SurveyCore();

        public string WidgetCategory
        {
            get
            {
                return ServiceFactory.GetService<PickListService>().GetPickListParemeterName(widget.Category.ToString(), Constants.WIDGETCATEGORY);
            }
        }
        int surveyid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {



            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
                if (typeHt.Contains("SurveyId"))
                    surveyid = Convert.ToInt32(typeHt["SurveyId"].ToString());
            }
            if (!IsPostBack)
            { 
            DataSet dssurveytype = surcore.getSurveyType(surveyid);
            if (dssurveytype.Tables[0].Rows[0][0].ToString() == "PPS_PRO")
            {
                DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", "Active");
            }
            else if (dssurveytype.Tables[0].Rows[0][0].ToString() == "PPS_PREMIUM")
            {
                DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", "Active");
            }
            }

            btnMySurvey.Text = String.Format(Utilities.ResourceMessage("btnMySurveyResource1.Text"), SurveyMode.ToLower());
            btnMySurvey.ToolTip = String.Format(Utilities.ResourceMessage("btnMySurveyResource1.Tooltip"), SurveyMode.ToLower());
            btnDashBoard.Text = String.Format(Utilities.ResourceMessage("btnDashBoardResource1.Text"), SurveyMode);
            btnDashBoard.ToolTip = String.Format(Utilities.ResourceMessage("btnDashBoardResource1.Tooltip"), SurveyMode);
            btnReports.Text = String.Format(Utilities.ResourceMessage("btnReportsResource1.Text"), SurveyMode);
            btnReports.ToolTip = String.Format(Utilities.ResourceMessage("btnReportsResource1.Tooltip"), SurveyMode);
            lblSurveyText.Text = String.Format(Utilities.ResourceMessage("lblSurveyTextResource1.Text"), SurveyMode);
            lblSurveyLive.Text = String.Format(Utilities.ResourceMessage("lblSurveyLiveResource1.Text"), SurveyMode.ToLower());
            lblCopyUrl.Text = String.Format(Utilities.ResourceMessage("lblCopyUrlResource1.Text"), SurveyMode.ToLower());            
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Contains("ContactListName"))
                listName = ht["ContactListName"].ToString();
            widget = ServiceFactory<WidgetService>.Instance.GetWidgetDetailsBySurveyId(SurveyID);
            widgetSettings = ServiceFactory<WidgetSettings>.Instance.GetWidgetSettingssBySurveyId(SurveyID);
            if (base.SurveyBasicInfoView != null)
            {
                lblSurveyName.Text = base.SurveyBasicInfoView.SURVEY_NAME;
            }
            if (base.SurveyFlag == 0)
            {
                if (SurveyBasicInfoView.EMAILLIST_TYPE == 3)
                {
                    string str = "";
                    str += " <div class='informationPanelDefault'><div>" + String.Format(Utilities.ResourceMessage("msgSuccess"), SurveyMode.ToLower(), SurveyBasicInfoView.SURVEY_NAME.ToLower()) + "</div></div>";

                    lblExternal.Text = str;
                    //     txtUrl.Text = SurveyBasicInfoView.SURVEYLAUNCH_URL;


                    txtUrl.Visible = false;
                    lblSurveyLive.Visible = false;
                    lblCopyUrl.Visible = false;


                    divInternalLaunch.Style.Add("display", "none");
                    divExternalMessage.Style.Add("display", "block");
                    divWidget.Style.Add("display", "none");
                    btnCopy.Visible = false;

                }
                if (SurveyBasicInfoView.EMAILLIST_TYPE == 2)
                {
                    string str = "";
                    divInternalLaunch.Style.Add("display", "none");
                    divExternalMessage.Style.Add("display", "block");
                    divWidget.Style.Add("display", "none");
                    str += " <div class='informationPanelDefault'><div>" + String.Format(Utilities.ResourceMessage("msgSuccess"), SurveyMode.ToLower(), SurveyBasicInfoView.SURVEY_NAME.ToLower()) + "</div></div>";
//                  str += " <div class='informationPanelDefault'><div>Your survey (" + SurveyBasicInfoView.SURVEY_NAME.ToLower() + ") has been launched successfully.</div></div>";
                    lblExternal.Text = str;
                    txtUrl.Text = SurveyBasicInfoView.SURVEYLAUNCH_URL;
                    //btnCopy.Attributes.Add("onclick", string.Format("ClipBoard({0})", txtUrl.ClientID));
                }
                else if (SurveyBasicInfoView.EMAILLIST_TYPE == 1)
                {
                    //Hashtable ht = new Hashtable();
                    divExternalMessage.Style.Add("display", "none");
                    divInternalLaunch.Style.Add("display", "block");
                    divWidget.Style.Add("display", "none");
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
                    if (ht != null && ht.Count > 0 && ht.Contains("Flag"))
                    {
                        string str = "";
                        lblSurveyLable.Text = "";
                        //str += " <div class='informationPanelDefault'><div>Your survey (" + SurveyBasicInfoView.SURVEY_NAME.ToLower() + ") has been launched successfully to your email list/s.</div></div>";
                        str += " <div class='informationPanelDefault'><div>" + String.Format(Utilities.ResourceMessage("msgSuccessEmail"), SurveyMode.ToLower(), SurveyBasicInfoView.SURVEY_NAME.ToLower()) + "</div></div>";
                        lblSurveyLink.Text = str;
                    }
                    else
                    {
                        string str = "";//
                        lblSurveyLable.Text = String.Format(Utilities.ResourceMessage("msgSuccessEmailList"),SurveyMode,SurveyBasicInfoView.SURVEY_NAME.ToLower()) + listName;
                        //lblSurveyLable.Text = " Survey " + '"' + SurveyBasicInfoView.SURVEY_NAME.ToLower() + '"' + " has been successfully launched to email list " + listName;
                        str += " <div class='informationPanelDefault'><div>" + String.Format(Utilities.ResourceMessage("msgAuditClearance"), SurveyMode.ToLower()) + "</div></div>";
                        // str += " <div class='informationPanelDefault'><div>Please note that the survey launch is subject to audit clearance for content.</div></div>";
                        lblSurveyLink.Text = str;
                    }
                }
            }
            else if (base.SurveyFlag == 1)
            {
                divInternalLaunch.Style.Add("display", "none");
                divExternalMessage.Style.Add("display", "none");
                divWidget.Style.Add("display", "block");
                string strWidgetText = "";
                lblWigetLabel.Text = String.Format(Utilities.ResourceMessage("msgWidgetSuccess"), SurveyMode, base.SurveyBasicInfoView.SURVEY_NAME.ToLower());
                
                //lblWigetLabel.Text = " Widget " + '"' + base.SurveyBasicInfoView.SURVEY_NAME.ToLower() + '"' + " launched successfully on Insighto platform ";
                txtWigetScript.Text = GenerateScript(base.SurveyID);
                strWidgetText += String.Format(Utilities.ResourceMessage("msgDomain"), widget.DomainName, WidgetCategory);
               // strWidgetText += " Add this script on domain name " + '"' + widget.DomainName + '"' + " and category " + '"' + WidgetCategory + '"' + ".";
               // strWidgetText += "<br/> Please note that the widget launch is subject to audit clearance for content.";
                strWidgetText += "<br/>" + String.Format(Utilities.ResourceMessage("msgAuditClearance"), SurveyMode.ToLower());
                lblWidgetText.Text = strWidgetText;
            }
        }
        protected void btnMySurvey_Click(object sender, EventArgs e)
        {
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + SurveyFlag));
        }
        protected void btnDashBoard_Click(object sender, EventArgs e)
        {
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyManagerURL(), "SurveyId=" + base.SurveyID + "&surveyFlag=" + SurveyFlag));
        }
        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetReportsURL(), "SurveyId=" + base.SurveyID + "&surveyFlag=" + SurveyFlag));
        }
        private string GenerateScript(int SurveyId)
        {
            StringBuilder sbWidgetScript = new StringBuilder();
            string strImageName = "";
            string strStyle = "";
            if (widgetSettings != null)
            {
                strImageName = widgetSettings.ImageName;
                strStyle = "style=font-family:" + widgetSettings.FontType + ";font-size:" + widgetSettings.FontSize + "px;color:" + widgetSettings.FontColor + ";";
            }
            if (widgetSettings.LogoFlag == 1)
            {

                sbWidgetScript.Append("<a href=\"Javascript:var x=window.open('");
                sbWidgetScript.Append(base.SurveyBasicInfoView.SURVEYLAUNCH_URL);
                sbWidgetScript.Append("@' + '&Title=' + document.title + '&DomainName='+document.location.href+'&ItemName=");
                sbWidgetScript.Append(WidgetCategory);
                sbWidgetScript.Append("','mywindow','menubar=0,resizable=1,status=0,scrollbars=1,width=800,height=600')\">");
                sbWidgetScript.Append("<img id='imgFeedBack' alt='Feedback' title='Feedback' src='");
                sbWidgetScript.Append(ConfigurationManager.AppSettings["RootURL"].ToString());
                sbWidgetScript.Append("App_Themes/Classic/Images/");
                sbWidgetScript.Append(strImageName);
                sbWidgetScript.Append("' />");
                sbWidgetScript.Append("</a>");
            }
            else if (widgetSettings.LogoFlag == 4)
            {
                sbWidgetScript.Append("<a href=\"Javascript:var x=window.open('");
                sbWidgetScript.Append(base.SurveyBasicInfoView.SURVEYLAUNCH_URL);
                sbWidgetScript.Append("@' + '&Title=' + document.title + '&DomainName='+document.location.href+'&ItemName=");
                sbWidgetScript.Append(WidgetCategory);
                sbWidgetScript.Append("','mywindow','menubar=0,resizable=1,status=0,scrollbars=1,width=800,height=600')\">");
                sbWidgetScript.Append("<span ");
                sbWidgetScript.Append(strStyle);
                sbWidgetScript.Append(">");
                sbWidgetScript.Append(widget.WidgetTitle);
                sbWidgetScript.Append("</span>");
                sbWidgetScript.Append("</a>");
            }
            else
            {
                sbWidgetScript.Append("<a href=\"Javascript:var x=window.open('");
                sbWidgetScript.Append(base.SurveyBasicInfoView.SURVEYLAUNCH_URL);
                sbWidgetScript.Append("@' + '&Title=' + document.title + '&DomainName='+document.location.href+'&ItemName=");
                sbWidgetScript.Append(WidgetCategory);
                sbWidgetScript.Append("','mywindow','menubar=0,resizable=1,status=0,scrollbars=1,width=800,height=600')\">");
                sbWidgetScript.Append("<img id='imgFeedBack' alt='Feedback' title='Feedback' src='");
                sbWidgetScript.Append(ConfigurationManager.AppSettings["RootURL"].ToString());
                sbWidgetScript.Append("App_Themes/Classic/Images/");
                sbWidgetScript.Append(strImageName);
                sbWidgetScript.Append("' /><span ");
                sbWidgetScript.Append(strStyle);
                sbWidgetScript.Append(">");
                sbWidgetScript.Append(widget.WidgetTitle);
                sbWidgetScript.Append("</span>");
                sbWidgetScript.Append("</a>");
            }
            return Convert.ToString(sbWidgetScript);
        }
    }
}