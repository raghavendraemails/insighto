﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;

namespace Insighto.Pages
{
    public partial class PreLaunchEmailInvitation : SurveyPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dvErrMsg.Visible = false;
            dvSuccessMsg.Visible = false;
            if (base.SurveyBasicInfoView != null)
            {
                if (base.SurveyBasicInfoView.SURVEY_ID > 0)
                {
                    SurveyBasicInfoView surveyBasicInfo = ServiceFactory.GetService<RespondentService>().GetSurveyViewInformation(base.SurveyBasicInfoView.SURVEY_ID);

                    if (surveyBasicInfo.EMAIL_INVITATION != "" || surveyBasicInfo.EMAIL_INVITATION.Length > 0)
                    {
                        tblEmailInvitation.Visible = true;
                       // lblEmailInvitation.Text = surveyBasicInfo.EMAIL_INVITATION;
                        string strEmailInvitation = "";
                        strEmailInvitation += base.SurveyBasicInfoView.EMAIL_INVITATION;
                        //strEmailInvitation += "<p> <a  target='_blank' href='" + ConfigurationManager.AppSettings["RootURL"].ToString() + review_url + "'><span style='font-weight: bold;'>START SURVEY<span></a> here </p>";
                        divEmailContent.InnerHtml = strEmailInvitation;
                        divEmailContent.Style.Add("font-family", base.SurveyBasicInfoView.SURVEY_FONT_TYPE);
                        divEmailContent.Style.Add("font-size", BuildQuestions.GetFontSizeValue(base.SurveyBasicInfoView.SURVEY_FONT_SIZE));
                        divEmailContent.Style.Add("color", BuildQuestions.GetFontColor(base.SurveyBasicInfoView.SURVEY_FONT_COLOR));
                    }
                    else
                    {
                        dvErrMsg.Visible = true;
                        //lblErrMsg.Text = "There will be no email invitation for survey being launched through web link.";
                        tblEmailInvitation.Visible = false;
                    }
                }
            }
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (base.SurveyBasicInfoView.SURVEY_ID > 0)
            {
                string strLaunchUrl = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx", "Page=PEI&" + Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag="+SurveyFlag);
                Response.Redirect(strLaunchUrl);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (base.SurveyBasicInfoView.SURVEY_ID > 0)
            {
                string strLaunchUrl = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx",  Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag);
                Response.Redirect(strLaunchUrl);
            }
        }
    }
}
