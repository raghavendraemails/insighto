﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using System.Web.Script.Serialization;

public partial class paymentwebhooktest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string paymentid = "";
        decimal amount = 0M;
        int orderid = 0;
        string productslug = "";
        string producttitle = "";
        string status = "";
        string customfields = "";

        PollCreation pc;
        pc = new PollCreation();
        if (Request.Form.Count > 0)
        {
            paymentid = Request.Form["payment_id"];
            productslug = Request.Form["offer_slug"];
            producttitle = Request.Form["offer_title"];
            status = Request.Form["status"];
            Decimal.TryParse(Request.Form["amount"], out amount);
            customfields = Request.Form["custom_fields"];
        }
        else if (Request.QueryString.Count > 0)
        {
            paymentid = Request.QueryString["payment_id"];
            productslug = Request.QueryString["offer_slug"];
            producttitle = Request.QueryString["offer_title"];
            status = Request.QueryString["status"];
            Decimal.TryParse(Request.QueryString["amount"], out amount);
            customfields = Request.QueryString["custom_fields"];
        }
        JavaScriptSerializer js = new JavaScriptSerializer();
        if (productslug == "insighto-surveys-premium-annual")
        {
            clscustomfieldsTest str1 = js.Deserialize<clscustomfieldsTest>(customfields);
            Int32.TryParse((str1.field_91429 != null) ? str1.field_91429.value : "0", out orderid);
        }
        else if (productslug == "insighto-surveys-pro-single")
        {
            clscustomfieldsTest str1 = js.Deserialize<clscustomfieldsTest>(customfields);
            Int32.TryParse((str1.field_66631 != null) ? str1.field_66631.value : "0", out orderid);
        }
        else if (productslug == "insighto-surveys-premium-single")
        {
            clscustomfieldsTest str1 = js.Deserialize<clscustomfieldsTest>(customfields);
            Int32.TryParse((str1.field_80275 != null) ? str1.field_80275.value : "0", out orderid);
        }
        lbl1.Text = orderid.ToString();
        pc.SavePollPaymentInformation(paymentid, amount, productslug, producttitle, orderid, status, "Survey");
    }
}

public class clscustomfieldsTest
{
    public clscustomfieldsTest() { }
    public Field_91429 field_91429 { get; set; } //PREM-Y
    public Field_80275 field_80275 { get; set; } //PPS-PREM
    public Field_66631 field_66631 { get; set; } //PPS-PRO
}

public class Field_66631 : FieldsTest { }
public class Field_91429 : FieldsTest { }
public class Field_80275 : FieldsTest { }

public class FieldsTest
{
    public bool required { get; set; }
    public string type { get; set; }
    public string label { get; set; }
    public string value { get; set; }
}
