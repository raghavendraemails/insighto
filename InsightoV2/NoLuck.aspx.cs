﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Business.Helpers;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using Resources;
using System.Collections;

public partial class NoLuck : System.Web.UI.Page
{
    public string myAccountsUrl = string.Empty;
    Hashtable ht = new Hashtable();
    string pageName = "";
    string name;
    protected void Page_Load(object sender, EventArgs e)
    {       
       
        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            if (ht != null)
            {
                pageName=ht["Page"].ToString();
            }
        }
        if (Session["UserInfo"] != null)
        {
            var userInfo = Session[CommonMessages.SurveyFlag] != null ? (int)Session[CommonMessages.SurveyFlag] : 0;
            hdnMyAccount.Value = Utilities.GetAbsoluteUrl(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + userInfo), Page);
            name = "MY SURVEYS";
        }
        else if (Session["AdminUserInfo"] != null)
        {
            hdnMyAccount.Value = "Admin/Login.aspx";
        }
        else
        {
            hdnMyAccount.Value = Utilities.GetAbsoluteUrl("~/", Page);
            name = "HOME";
        }
        PageNotFound.InnerHtml = String.Format(Utilities.ResourceMessage("NoLuck_Error_Raised"), EncryptHelper.EncryptQuerystring("Feedback.aspx", "PageName=" + pageName), name);
    }
}