﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Configuration;
using CovalenseUtilities.Helpers;
using Insighto.Business.Helpers;
using App_Code;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.IO;

namespace Insighto.Pages
{
    public partial class ForgotPassword : BasePage
    {
        private MailMessage mailMessage = null;
        private SmtpClient smtpMail = null;
        string strUrl = ConfigurationManager.AppSettings["RootURL"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblInvaildAlert.Visible = false;
            divalert.Visible = false;
            //dvInforamtionPanelDefault.InnerHtml = Utilities.ResourceMessage("divInforamtionPanelDefault"); 
        }
        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            var userService = ServiceFactory.GetService<UsersService>();
            var userInfo = userService.FindUserByEmailId(txtEmail.Text.Trim());

            if (string.IsNullOrEmpty(txtEmail.Text.Trim()) || txtEmail.Text == "Email")
            {
                divalert.Visible = true;
                lblInvaildAlert.Visible = true;
                lblInvaildAlert.Text = "Please enter an email ID";
            }
            else if (userInfo != null)
            {
                var sendEmailService = ServiceFactory.GetService<SendEmailService>();
                string password = GetRandomPasswordUsingGUID(6);
                password = password.Substring(0, 6);
                string fromEmail = ConfigurationManager.AppSettings["emailActivation"];               
                userService.UpdatePassword(userInfo.USERID, password, 1);
               // sendEmailService.ForgotPassword("Forgot Password", userInfo.FIRST_NAME, fromEmail, password, txtEmail.Text.Trim());               
                string OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + txtEmail.Text.Trim()); 
                string unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";
                forgotpasswordemail("ForgotPassword", userInfo.FIRST_NAME, txtEmail.Text.Trim(), password, unsubscribe);
                lblInvaildAlert.Text = "An email has been sent to you with password reset instructions.";
                divalert.Visible = true;
                lblInvaildAlert.Visible = true;
            }
            else
            {
            }
        }
        public string GetRandomPasswordUsingGUID(int length)
        {
            // Get the GUID
            string guidResult = System.Guid.NewGuid().ToString();
            // Remove the hyphens
            guidResult = guidResult.Replace("-", string.Empty);
            // Make sure length is valid
            if (length <= 0 || length > guidResult.Length)
                throw new ArgumentException("Length must be between 1 and " + guidResult.Length);
            // Return the first length bytes        
            return guidResult.Substring(0, length);
        }
        
        public void forgotpasswordemail(string templatename, string username, string EmailID, string strpassword, string unsubscribeurl)
        {

            string viewData1;
            JavaScriptSerializer js1 = new JavaScriptSerializer();
            js1.MaxJsonLength = int.MaxValue;
            clsjsonmandrill prmjson1 = new clsjsonmandrill();
            //  var prmjson1 = new jsonmandrill();
            prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
            prmjson1.name = templatename;
            viewData1 = js1.Serialize(prmjson1);

            string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";


            WebClient request1 = new WebClient();
            request1.Encoding = System.Text.Encoding.UTF8;
            request1.Headers["Content-Type"] = "application/json";
            byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));

            string response1 = System.Text.Encoding.ASCII.GetString(resp1);


            clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);

            string bodycontent = jsmandrill11.code;


            string viewData;
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = int.MaxValue;
            var prmjson = new clsjsonmandrill.jsonmandrillmerge();
            prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
            prmjson.template_name = templatename;



            List<clsjsonmandrill.template_content> mtags = new List<clsjsonmandrill.template_content>();
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content00", content = "<div> Dear *|FNAME|*,</div>" });
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div><ul><li>&nbsp;User Name: *|Login_Name|*</li><li>&nbsp;Temporary Password:  *|Password|*</li></ul></div>" });            
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content02", content = "<div><p style='font-size:8pt;font-family:Arial,Sans-Serif'> *|UNSUBSCRIBEINSIGHTO|* </p></div>" });

            List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
            mvars.Add(new clsjsonmandrill.merge_vars { name = "FNAME", content = username });
            mvars.Add(new clsjsonmandrill.merge_vars { name = "Login_Name", content = EmailID });
            mvars.Add(new clsjsonmandrill.merge_vars { name = "Password", content = strpassword });
            mvars.Add(new clsjsonmandrill.merge_vars { name = "UNSUBSCRIBEINSIGHTO", content = unsubscribeurl });
            prmjson.template_content = mtags;
            prmjson.merge_vars = mvars;


            viewData = js.Serialize(prmjson);


            string url = "https://mandrillapp.com/api/1.0/templates/render.json";


            WebClient request = new WebClient();
            request.Encoding = System.Text.Encoding.UTF8;
            request.Headers["Content-Type"] = "application/json";
            byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

            string response = System.Text.Encoding.ASCII.GetString(resp);

            string unesc = System.Text.RegularExpressions.Regex.Unescape(response);

            string first4 = unesc.Substring(0, 9);

            string last2 = unesc.Substring(unesc.Length - 2, 2);


            string unesc1 = unesc.Replace(first4, "");
            string unescf = unesc1.Replace(last2, "");


            clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);

            //   string bodycontent = jsmandrill1.code;


            mailMessage = new MailMessage();
            mailMessage.To.Clear();
            mailMessage.Sender = new MailAddress("alerts@insighto.com", "Insighto.com");
            // mailMessage.Sender = new MailAddress(Sendmails.FROM_MAILADDRESS, "Insighto.com");
            mailMessage.From = new MailAddress("alerts@insighto.com", "Insighto.com");

            mailMessage.Subject = "[Important –  From Insighto.com] Your Insighto Password has been reset";

            mailMessage.To.Add(EmailID);
            //  mailMessage.Headers.Add("survey_id", "6666");
            // mailMessage.Headers.Add("X-MC-Tags", "survey_respondent");
            mailMessage.Body = unescf;
            //if (Sendmails.SENDER_TOCCADDRESSES != null && Convert.ToString(Sendmails.SENDER_TOCCADDRESSES.Trim()).Length > 0)
            //{
            //    mailMessage.CC.Add(Sendmails.SENDER_TOCCADDRESSES.Trim());
            //}
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.Normal;
            smtpMail = new SmtpClient();
            smtpMail.Host = "smtp.mandrillapp.com";
            smtpMail.Port = 587;
            smtpMail.EnableSsl = true;
            smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpMail.Credentials = new System.Net.NetworkCredential("ram@knowience.com", "T74TJc3EZPYaIcvdBg--Dg");

            try
            {

                smtpMail.Send(mailMessage);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        //  Console.WriteLine("Delivery failed - retrying in 5 seconds.");


                        System.Threading.Thread.Sleep(5000);
                        smtpMail.Send(mailMessage);
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            //catch (System.Net.Mail.SmtpException ex)
            //{
            //    throw ex;
            //}
            mailMessage.Dispose();
        }

    }
}