﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
public partial class Test : System.Web.UI.Page
{
    private const string URL_SENDGRID_WITH_CREDENTIALS = @"https://sendgrid.com/api/bounces.get.json?api_user=knowience&api_key=insighto$9999&start_date=2012-03-22";
    public int emailId { get; set; }
    public int userId { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnGet_Click(object sender, EventArgs e)
    {
        var messages = GetBouncedEmails(URL_SENDGRID_WITH_CREDENTIALS);
        foreach (var item in messages)
        {
            AddBouncedEmail(item);            
        }
    }
    protected void btnDecode_Click(object sender, EventArgs e)
    {
        txtKey.Text = EncryptHelper.Decrypt(txtKey.Text);
    }
    
    private static List<Message> GetBouncedEmails(string targetUrl)
    {
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        var bounceEmails = ProcessSendGridAccount(string.Format("{0}&{1}", targetUrl, "date=1"));
        return serializer.Deserialize<List<Message>>(bounceEmails);
    }
    public static string ProcessSendGridAccount(string newUrl)
    {
        var recieve = string.Empty;
        try
        {
            var url = WebRequest.Create(newUrl);
            //url.Credentials = new NetworkCredential { UserName = "koti@koti.com", Password = "test123" };
            var response = url.GetResponse();
            var input = new StreamReader(response.GetResponseStream());
            recieve = input.ReadToEnd();

            //Console.WriteLine(recieve);
            url.Abort();
            input.Close();
            response.Close();
        }
        catch (Exception ee)
        {

            Console.WriteLine(ee.ToString());

        }
        return recieve;

    }

    private void AddBouncedEmail(Message msg)
    {
        osm_BouncedMails bounced = new osm_BouncedMails();
        bounced.CODE = msg.status;
        bounced.DATE = Convert.ToDateTime(msg.created);
        bounced.EMAIL_ID = msg.email;
        bounced.REASON=msg.reason;       
        ServiceFactory<LaunchSurveyService>.Instance.InsertBouncedMails(bounced);
        

    }

}
