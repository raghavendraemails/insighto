﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true"
    CodeFile="SurveyDrafts.aspx.cs" Inherits="SurveyDrafts" EnableEventValidation="true"%>

<%@ Register Src="~/UserControls/SurveySideNav.ascx" TagName="SurveySideNav" TagPrefix="uc" %>
<%@ Register Src="~/UserControls/SurveyQuestionnaire.ascx" TagName="Questionnaire"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" runat="Server">
<script type="text/javascript" src="Scripts/jquery.ui.datepicker.js"></script>
 <script type = "text/javascript" src="Scripts/swfobject.js"></script> 
  <script type="text/javascript" src="Scripts/timepicker.js"></script>
    <uc:Questionnaire ID="ucQuestionnaire" runat="server" />
    <div class="bottomButtonPanel">
        <a id="lnkAddNew" runat="server" rel="framebox" h="540" w="950" scrolling="yes" title="Add Questions">
            <div class="hyperlink_add_quetion" id="dvAddQuestion" runat="server">
                <asp:Label ID="lblTitle" runat="server" Text="Add Questions" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>
            </div>
        </a>
        <div>
            <asp:Button ID="lnkNext" runat="server" OnClick="lnkNext_Click" CssClass="dynamicButton btnNext"
                Text="Next Step" ToolTip="Next Step" meta:resourcekey="lnkNextResource1"></asp:Button></div>
    </div>
    <div class="clear">
    </div>
    <div class="defaultH">
    </div>
    <div class="surveypage_lbl">
         <asp:Label ID="lblSurveyPreviewURL" runat="server" 
             Text="Survey Preview URL for review by others" 
             meta:resourcekey="lblSurveyPreviewURLResource1"></asp:Label></div>
    <div class="defaultHeight">
    </div>
    <div class="surveyUrlPanel">
    <asp:TextBox ID="txtUrl" runat="server" CssClass="txtUrl textBoxMedium" ReadOnly="true"
            Width="500px"></asp:TextBox>
            <a id="hlnkPreview" style="padding:0; font-weight:normal;" class="lnkPreview" href="javascript:void(0);">Copy</a>
        
    </div>
    <div class="defaultHeight">
    </div>
    <div class="informationPanelDefault">
    <div id="dvInformation">
           <asp:Label ID="lblInfo2" Text="This is the " meta:resourcekey="lblInfo2" runat="server"></asp:Label><b><asp:Label ID="lblPreview" runat="server" meta:resourcekey="lblPreview" Text="Preview "></asp:Label></b><asp:Label ID="lblOf" meta:resourcekey="lblOf" runat="server"  Text=" of the "></asp:Label> <%= SurveyMode %> <asp:Label ID="lblInfo1"  meta:resourcekey="lblInfo1" Text=" that would be seen by respondent. Copy
            this link and send it to others for review through your email." runat="server" ></asp:Label> <span class="orangeColor">
                <b><asp:Label ID="lblInfo" runat="server" meta:resourcekey="lblInfo"  Text=" Responses to the preview will not be recorded."></asp:Label></b></span>
                </div>
    </div>  
    <script type="text/javascript">
        $(document).ready(function () {
            $('iframe').each(function () {
                var url = $(this).attr("src");
                var result = url.search(/youtube/i);
                if (result != -1) {
                    result = url.indexOf('?');
                    if (result != -1) {
                        $(this).attr("src", url + "&wmode=transparent");
                    } else { $(this).attr("src", url + "?wmode=transparent"); }
                }
            });
            var questionnaire = document.getElementById('pnlQuestionnaire');
            if (questionnaire != null) {
                $('.btnNext').show();
            }
            else {
                $('.btnNext').hide();
            }
        });
    </script>
    <script type="text/javascript" src="Scripts/ZeroClipBoard/jquery.zclip.min.js"></script>
    <script type="text/javascript">
        $(".lnkPreview").zclip({
            path: '/Scripts/ZeroClipBoard/ZeroClipboard.swf',
            copy: $('.txtUrl').val(),
            beforeCopy: function () {
            },
            afterCopy: function () {
            }

        });
       // var questionnaire = document.getElementById('pnlQuestionnaire');
       
    </script>
</asp:Content>
