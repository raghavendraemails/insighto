﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;
using System.Collections;
using App_Code;
using System.Data;

namespace Insighto.Pages
{
    public partial class SurveyAlerts : SurveyPageBase
    {
        int controlId = 0;
        bool addEmail = false;
        bool IsBlockedWordExists = false;
        Hashtable ht = new Hashtable();
        SurveyCore surcore = new SurveyCore();
        protected void Page_Load(object sender, EventArgs e)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
          //  var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            UserType licenceType = 0;

            DataSet dssurveytype = surcore.getSurveyType(base.SurveyBasicInfoView.SURVEY_ID);

            if (dssurveytype.Tables[0].Rows[0][1].ToString() != "")
            {
                licenceType = (UserType)Enum.Parse(typeof(UserType), dssurveytype.Tables[0].Rows[0][1].ToString());
                hdnLicenseType.Value =  dssurveytype.Tables[0].Rows[0][1].ToString(); 
            }
            else
            {
                licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
                //MODIFIED ON 1-JUL-16 AS FOR NEW POLLS CUST LICENSETYPE COULD BE NULL
                hdnLicenseType.Value = licenceType.ToString();// userDetails.LicenseType.ToString(); 
            }

            


            var listFeatures = ServiceFactory.GetService<FeatureService>();
          //  hdnLicenseType.Value = userDetails.LicenseType.ToString();
            if (!IsPostBack)
            {
                txtDate1.Attributes.Add("Readonly", "true");
                txtDate2.Attributes.Add("Readonly", "true");
                txtRemaiderDate.Attributes.Add("Readonly", "true");
                txtDateTime.Attributes.Add("Readonly", "true");
                txtClosingDateTime.Attributes.Add("Readonly", "true");
                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);

                    if (ht != null && ht.Count > 0 && ht.Contains("Page"))
                    {
                        if (ht["Page"].ToString() == "PSC")
                        {
                            btnBack.Visible = true;                           
                        }
                        else
                        {
                            btnBack.Visible = false;
                        }
                    }
                    else
                    {
                        btnBack.Visible = false;
                    }
                }
               
                if (listFeatures.Find(licenceType, FeatureSurveyCreation.RESP_PREVIEW).Value == 0)
                {

                    //lnkPreview.Attributes.Add("Onclick", "javascript: window.open('Upgradelicense.aspx','Upgradelicense','height=300,width=600,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;");
                }
                else
                {
                    string urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "Mode=1&surveyId=" + SurveyID);
                    //  lnkPreview.Attributes.Add("Onclick", "javascript: window.open(" + urlstr + "','SurveyPopup','height=360,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return true;");
                }
                BindAlerts();
                if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0 && (base.SurveyBasicInfoView.STATUS == "Active" || base.SurveyBasicInfoView.STATUS == "Closed"))
                {
                    if (base.SurveyBasicInfoView.STATUS == "Closed")
                    {
                        string alertText = "";
                        if (SelectedSurvey.surveyOptns != null && SelectedSurvey.surveyOptns[0].SURVEYCLOSETYPE == 1)
                            alertText = "The survey is closed because it has crossed the schedule survey close date. If you want to edit the survey, please activate the survey from your closed survey list and select a future close date from your Survey Controls settings.";
                        else if (SelectedSurvey.surveyOptns != null && SelectedSurvey.surveyOptns[0].SURVEYCLOSETYPE == 0)
                            alertText = "The survey is closed due to achievement of response quota. If you want to edit the survey, please activate the survey  from your closed survey list and increase the number of response quota from your Survey Controls settings.";
                       this.ClientScript.RegisterStartupScript(this.GetType(), "Some Title", "<script language=\"javaScript\">" + "alert('" + alertText + "');" + "window.location.href='ClosedSurveys.aspx';" + "<" + "/script>");
                    }
                    else if (base.SurveyBasicInfoView.STATUS == "Active")
                    {
                        rbtnLaunchSchedule.Items.FindByValue("0").Enabled = false;
                        rbtnLaunchSchedule.Items.FindByValue("1").Enabled = false;
                    }

                    //btnSaveActive.Visible = true;
                    //btnSaveEditEmail.Visible = true;
                    //btnSaveContinue.Text = "Save and edit more";


                }
                else
                {
                    //btnSaveActive.Visible = false;
                    //btnSaveEditEmail.Visible = false;
                    //btnSaveContinue.Text = "Save & Go Next";
                }
                BindResourceMessages();
            }
        }
        private void BindResourceMessages()
        {

            rbtnSurveyClose.Items[0].Text = String.Format(Utilities.ResourceMessage("ListItemResource3.Text"), SurveyMode.ToLower());
            lblTitle.Text = String.Format(Utilities.ResourceMessage("lblTitleResource1.Text"), SurveyMode.ToUpper());
            lblSurveyStart.Text = String.Format(Utilities.ResourceMessage("lblSurveyStartResource1.Text"), SurveyMode.ToLower());
            lblQuestionTitle.Text = String.Format(Utilities.ResourceMessage("lblQuestionTitleResource1.Text"), SurveyMode);
            chkAlertOnSurveyLaunch.Text = String.Format(Utilities.ResourceMessage("chkAlertOnSurveyLaunchResource1.Text"), SurveyMode.ToLower());
            chkAlertOnSurveyClose.Text = String.Format(Utilities.ResourceMessage("chkAlertOnSurveyCloseResource1.Text"), SurveyMode.ToLower());
            lblCloseSurvey.Text = String.Format(Utilities.ResourceMessage("lblCloseSurveyResource1.Text"), SurveyMode.ToLower());
            lblAlertsSurveyManager.Text = String.Format(Utilities.ResourceMessage("lblAlertsSurveyManagerResource1.Text"), SurveyMode.ToLower());
            customLaunchDate.ErrorMessage = String.Format(Utilities.ResourceMessage("customLaunchDateResource1.ErrorMessage"), SurveyMode.ToLower());
            customClosingDate.ErrorMessage = String.Format(Utilities.ResourceMessage("customClosingDateResource1.ErrorMessage"),SurveyMode.ToLower());
        }
        protected void btnRestore_Click(object sender, EventArgs e)
        {
            //BindAlerts();
            ClearControls();
            lblSuccMsg.Text = "Default restored successfully.";
            lblSuccMsg.Visible = true;
            dvSuccessMsg.Visible = true;
            dvErrMsg.Visible = false;
           
        }
        private void ClearControls()
        {
            rbtnLaunchSchedule.SelectedIndex = 0;
            chkAlertOnSurveyLaunch.Checked = true;
            txtDateTime.Text = string.Empty;
            launchDateTime.Style.Add("display", "none");
            rbtnSurveyClose.SelectedIndex = 0;
            txtClosingDateTime.Text = string.Empty;
            surveyDateTime.Style.Add("display", "none");
            rbtnNoofRespondents.Checked = false;
            txtNoofRespondents.Text = "0";
            txtNoofRespondents.Enabled = false;
            chkAlertOnSurveyClose.Checked = false;
            txtAlert1.Text = "0";
            txtAlert2.Text = "0";
            txtDate1.Text = string.Empty;
            txtDate2.Text = string.Empty;
            txtRemainder.Text = string.Empty;
            txtRemaiderDate.Text = string.Empty;

        }
        private void BindAlerts()
        {
            if (base.SurveyBasicInfoView != null)
            {
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userDetails = sessionService.GetLoginUserDetailsSession();
                chkAlertOnSurveyLaunch.Checked = Convert.ToBoolean(base.SurveyBasicInfoView.SCHEDULE_ON_ALERT);
                rbtnLaunchSchedule.SelectedValue = base.SurveyBasicInfoView.SCHEDULE_ON.ToString();
                DateTime dtLaunch = Convert.ToDateTime("1/1/1800");
                if (base.SurveyBasicInfoView.LAUNCH_ON_DATE > Convert.ToDateTime("1/1/1800"))
                {
                    dtLaunch = CommonMethods.GetConvertedDatetime(base.SurveyBasicInfoView.LAUNCH_ON_DATE, userDetails.StandardBIAS, true);
                    txtDateTime.Text = dtLaunch.ToString("dd-MMM-yyyy HH:mm");
                    launchDateTime.Style.Add("display", "block");
                    hdnLaunchDate.Value = dtLaunch.ToString();
                }
                else
                {
                    launchDateTime.Style.Add("display", "none");
                }
                if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE.ToString() == "0")
                {
                    rbtnSurveyClose.ClearSelection();
                    rbtnNoofRespondents.Checked = true;
                    txtNoofRespondents.Enabled = true;
                    txtNoofRespondents.Text = base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS.ToString();
                }
                else if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE.ToString() == "1")
                {
                    rbtnNoofRespondents.Checked = false;
                    txtNoofRespondents.Enabled = false;
                    txtNoofRespondents.Text = "0";
                    rbtnSurveyClose.ClearSelection();
                    rbtnSurveyClose.Items.FindByValue("1").Selected = true;
                }
                else if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE.ToString() == "2")
                {
                    rbtnNoofRespondents.Checked = false;
                    txtNoofRespondents.Enabled = false;
                    txtNoofRespondents.Text = "0";
                    rbtnSurveyClose.ClearSelection();
                    rbtnSurveyClose.Items.FindByValue("2").Selected = true;
                }


                DateTime dtClose = Convert.ToDateTime("1/1/1800"); ;
                if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE.ToString() == "1" && base.SurveyBasicInfoView.CLOSE_ON_DATE > Convert.ToDateTime("1/1/1800"))
                {
                    dtClose = CommonMethods.GetConvertedDatetime(base.SurveyBasicInfoView.CLOSE_ON_DATE, userDetails.StandardBIAS, true);
                    txtClosingDateTime.Text = dtClose.ToString("dd-MMM-yyyy HH:mm");
                    surveyDateTime.Style.Add("display", "block");
                    hdnCloseDate.Value = dtClose.ToString();
                }
                else
                {
                    surveyDateTime.Style.Add("display", "none");
                }
                txtAlert1.Text = base.SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES.ToString();
                txtAlert2.Text = base.SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES.ToString();
                DateTime dtMinResp = Convert.ToDateTime("1/1/1800");
                DateTime dtMaxResp = Convert.ToDateTime("1/1/1800");
                DateTime dtRemdate = Convert.ToDateTime("1/1/1800");
                if (base.SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE > Convert.ToDateTime("1/1/1800"))
                {
                    dtMaxResp = base.SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE; // CommonMethods.GetConvertedDatetime(base.SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE, userDetails.StandardBIAS, true);
                    txtDate2.Text = dtMaxResp.ToString("dd-MMM-yyyy HH:mm");
                }
                else
                {
                    txtDate2.Text = "";
                }
                if (base.SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE.Date > Convert.ToDateTime("1/1/1800").Date)
                {
                    dtMinResp = base.SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE; // CommonMethods.GetConvertedDatetime(base.SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE, userDetails.StandardBIAS, true);
                    txtDate1.Text = dtMinResp.ToString("dd-MMM-yyyy HH:mm");
                }
                else
                {
                    txtDate1.Text = "";
                }
                txtRemainder.Text = base.SurveyBasicInfoView.REMINDER_TEXT;
                if (base.SurveyBasicInfoView.REMINDER_ON_DATE > Convert.ToDateTime("1/1/1800"))
                {
                    dtRemdate = base.SurveyBasicInfoView.REMINDER_ON_DATE; //CommonMethods.GetConvertedDatetime(base.SurveyBasicInfoView.REMINDER_ON_DATE, userDetails.StandardBIAS, true);
                    txtRemaiderDate.Text =dtRemdate.ToString("dd-MMM-yyyy");
                }
                else
                {
                    txtRemaiderDate.Text = "";
                }
                chkAlertOnSurveyClose.Checked = Convert.ToBoolean(base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE_ALERT);
                CheckFeaturesRest();
            }
        }
        private void CheckFeaturesRest()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
           // var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);

            UserType licenceType = 0;

            DataSet dssurveytype = surcore.getSurveyType(base.SurveyBasicInfoView.SURVEY_ID);

            if (dssurveytype.Tables[0].Rows[0][1].ToString() != "")
            {
                licenceType = (UserType)Enum.Parse(typeof(UserType), dssurveytype.Tables[0].Rows[0][1].ToString());
            }
            else
            {
                licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            }

            var listFeatures = ServiceFactory.GetService<FeatureService>();



            if (listFeatures.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYLAUNCH).Value == 0)
            {
                feaLaunch.Value = "0";
            }
            if (listFeatures.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYCLOSE).Value == 0)
            {
                feaClose.Value = "0";
            }
            if (listFeatures.Find(licenceType, FeatureDeploymentType.ALERT_MINMAXNO_RESPONDENTS).Value == 0)
            {
                txtAlert1.Enabled = false;
                txtAlert2.Enabled = false;
                txtDate1.Enabled = false;
                txtDate2.Enabled = false;
            }
            if (listFeatures.Find(licenceType, FeatureDeploymentType.SEND_REMAINDERS).Value == 0)
            {
                txtRemaiderDate.Enabled = false;
                txtRemainder.Enabled = false;
            }
        }
        private void SaveSurveyControls()
        {
            DataSet dsppstype = surcore.getSurveyType(base.SurveyBasicInfoView.SURVEY_ID);



            if (chkAlertOnSurveyLaunch.Checked)
            {
                SurveyBasicInfoView.SCHEDULE_ON_ALERT = 1;
            }
            else
            {
                SurveyBasicInfoView.SCHEDULE_ON_ALERT = 0;
            }
            if (chkAlertOnSurveyClose.Checked)
            {
                SurveyBasicInfoView.SCHEDULE_ON_CLOSE_ALERT = 1;
            }
            else
            {
                SurveyBasicInfoView.SCHEDULE_ON_CLOSE_ALERT = 0;
            }
            DateTime defaultDate = Convert.ToDateTime("1/1/1800");
            bool OndateLaunch_fea = true, OnDateClose_fea = true;
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
           // var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            DateTime launchdate;

            if (dsppstype.Tables[1].Rows.Count > 0)
            {
                launchdate = Convert.ToDateTime(dsppstype.Tables[1].Rows[0][0].ToString());
            }
            else
            {
                launchdate = defaultDate;
            }


            UserType licenceType = 0;

            DataSet dssurveytype = surcore.getSurveyType(base.SurveyBasicInfoView.SURVEY_ID);

            if (dssurveytype.Tables[0].Rows[0][1].ToString() != "")
            {
                licenceType = (UserType)Enum.Parse(typeof(UserType), dssurveytype.Tables[0].Rows[0][1].ToString());             
            }
            else
            {
                licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);                
            }

            
            if (userDetails != null && userDetails.UserId > 0 && base.SurveyBasicInfoView != null)
            {
                var listFeatures = ServiceFactory.GetService<FeatureService>();
                bool Alert_fea = true, Remainder_fea = true;
                if (listFeatures.Find(licenceType, FeatureSurveyCreation.ALERTS).Value == 0)
                {
                    if (chkAlertOnSurveyLaunch.Checked || chkAlertOnSurveyClose.Checked)
                    {
                        Alert_fea = false;
                        chkAlertOnSurveyLaunch.Checked = false;
                        chkAlertOnSurveyClose.Checked = false;
                        txtAlert1.Text = "0";
                        txtDate1.Text = "";
                        txtAlert2.Text = "0";
                        txtDate2.Text = "";
                        txtRemainder.Text = "0";
                        txtRemaiderDate.Text = "";
                    }
                }
                if (listFeatures.Find(licenceType, FeatureSurveyCreation.REMINDERS).Value == 0)
                {
                    Remainder_fea = false;
                    txtRemainder.Text = "";
                    txtRemaiderDate.Text = "";
                }
                if (ValidatedateControls())
                {
                    SurveyBasicInfoView.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS = ValidationHelper.GetInteger(txtNoofRespondents.Text.Trim(), 0);
                    SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES = ValidationHelper.GetInteger(txtAlert1.Text.Trim(), 0);
                    SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES = ValidationHelper.GetInteger(txtAlert2.Text.Trim(), 0);
                    if (txtDate1.Text != "")
                    {
                      //  SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE = Convert.ToDateTime(txtDate1.Text.Trim()); // AddDateTime(txtDate1.Text.Trim());

                        if (SurveyBasicInfoView.STATUS == "Draft")
                        {

                            if (AddDateTime(txtDate1.Text.Trim()) <= DateTime.Now.AddMonths(3))
                            {

                                SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE = AddDateTime(txtDate1.Text.Trim()); // AddDateTime(txtDate1.Text.Trim());
                            }
                            else if (AddDateTime(txtDate1.Text.Trim()) > DateTime.Now.AddMonths(3))
                            {
                                lblErrMsg.Text = "Minimum Responses specific date cannot exceed 3 months after launch date.";
                                lblErrMsg.Visible = true;
                                dvErrMsg.Visible = true;

                                lblSuccMsg.Text = "";
                                lblSuccMsg.Visible = false;
                                dvSuccessMsg.Visible = false;

                                return;
                            }

                        }
                        else if (SurveyBasicInfoView.STATUS == "Active")
                        {

                            if (AddDateTime(txtDate1.Text.Trim()) <= SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE)
                            {
                                SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE = AddDateTime(txtDate1.Text.Trim());
                            }
                            else if (AddDateTime(txtDate1.Text.Trim()) > launchdate.AddMonths(3))
                            {
                                lblErrMsg.Text = "Survey closed date is already set.And the new date cannot exceed the date " + launchdate.AddMonths(3);
                                lblErrMsg.Visible = true;
                                dvErrMsg.Visible = true;

                                lblSuccMsg.Text = "";
                                lblSuccMsg.Visible = false;
                                dvSuccessMsg.Visible = false;
                                return;
                            }
                        }

                    }
                    else
                    {
                        SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE = defaultDate;
                    }
                    if (txtDate2.Text != "")
                    {
                       // SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE = Convert.ToDateTime(txtDate2.Text.Trim());  //AddDateTime(txtDate2.Text.Trim());
                        if (SurveyBasicInfoView.STATUS == "Draft")
                        {

                            if (AddDateTime(txtDate2.Text.Trim()) <= DateTime.Now.AddMonths(3))
                            {

                                SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE = AddDateTime(txtDate2.Text.Trim()); // AddDateTime(txtDate1.Text.Trim());
                            }
                            else if (AddDateTime(txtDate2.Text.Trim()) > DateTime.Now.AddMonths(3))
                            {
                                lblErrMsg.Text = "Minimum Responses specific date cannot exceed 3 months after launch date.";
                                lblErrMsg.Visible = true;
                                dvErrMsg.Visible = true;

                                lblSuccMsg.Text = "";
                                lblSuccMsg.Visible = false;
                                dvSuccessMsg.Visible = false;

                                return;
                            }

                        }
                        else if (SurveyBasicInfoView.STATUS == "Active")
                        {

                            if (AddDateTime(txtDate2.Text.Trim()) <= SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE)
                            {
                                SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE = AddDateTime(txtDate2.Text.Trim());
                            }
                            else if (AddDateTime(txtDate2.Text.Trim()) > launchdate.AddMonths(3))
                            {
                                lblErrMsg.Text = "Survey closed date is already set.And the new date cannot exceed the date " + launchdate.AddMonths(3);
                                lblErrMsg.Visible = true;
                                dvErrMsg.Visible = true;

                                lblSuccMsg.Text = "";
                                lblSuccMsg.Visible = false;
                                dvSuccessMsg.Visible = false;
                                return;
                            }
                        }
                    }
                    else
                    {
                        SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE = defaultDate;
                    }
                    if (txtDateTime.Text != "")
                    {
                        if ((dsppstype.Tables[0].Rows[0][0].ToString() == "PPS_PRO") || (dsppstype.Tables[0].Rows[0][0].ToString() == "PPS_PREMIUM"))
                        {

                            if (SurveyBasicInfoView.STATUS == "Draft")
                            {

                                if (AddDateTime(txtDateTime.Text.Trim()) <= DateTime.Now.AddMonths(3))
                                {

                                    SurveyBasicInfoView.LAUNCH_ON_DATE = AddDateTime(txtDateTime.Text.Trim());
                                  
                                }
                                else if (AddDateTime(txtDateTime.Text.Trim()) > DateTime.Now.AddMonths(3))
                                {
                                    lblErrMsg.Text = "Paid survey has to be launced within 3 months of applying survey credit.";
                                    lblErrMsg.Visible = true;
                                    dvErrMsg.Visible = true;

                                    lblSuccMsg.Text = "";
                                    lblSuccMsg.Visible = false;
                                    dvSuccessMsg.Visible = false;

                                    return;
                                }

                            }
                            //else if (SurveyBasicInfoView.STATUS == "Active")
                            //{
                            //    if (AddDateTime(txtDateTime.Text.Trim()) <= launchdate.AddMonths(3))
                            //    {
                            //        SurveyBasicInfoView.LAUNCH_ON_DATE = AddDateTime(txtDateTime.Text.Trim());
                            //    }
                            //    else if (AddDateTime(txtDateTime.Text.Trim()) > launchdate.AddMonths(3))
                            //    {
                            //        lblErrMsg.Text = "Paid survey has to be launced within 3 months of applying survey credit.";
                            //        lblErrMsg.Visible = true;
                            //        dvErrMsg.Visible = true;
                            //        return;
                            //    }
                            //}


                        }
                        else
                        {
                            SurveyBasicInfoView.LAUNCH_ON_DATE = AddDateTime(txtDateTime.Text.Trim());
                        }
                    }
                    if (txtClosingDateTime.Text != "")
                    {
                        

                        if ((dsppstype.Tables[0].Rows[0][0].ToString() == "PPS_PRO") || (dsppstype.Tables[0].Rows[0][0].ToString() == "PPS_PREMIUM"))
                        {

                            if (SurveyBasicInfoView.STATUS == "Draft")
                            {
                                
                                if (AddDateTime(txtClosingDateTime.Text.Trim()) <= DateTime.Now.AddMonths(3))
                                {

                                    SurveyBasicInfoView.CLOSE_ON_DATE = AddDateTime(txtClosingDateTime.Text.Trim());
                                }
                                else if (AddDateTime(txtClosingDateTime.Text.Trim()) > DateTime.Now.AddMonths(3))
                                {
                                    lblErrMsg.Text = "Survey closed date cannot exceed 3 months after launch date.";
                                    lblErrMsg.Visible = true;
                                    dvErrMsg.Visible = true;

                                    lblSuccMsg.Text = "";
                                    lblSuccMsg.Visible = false;
                                    dvSuccessMsg.Visible = false;

                                    return;
                                }
                                
                            }
                            else if (SurveyBasicInfoView.STATUS == "Active")
                            {

                                

                                if (AddDateTime(txtClosingDateTime.Text.Trim()) <= SurveyBasicInfoView.CLOSE_ON_DATE)
                                {
                                    SurveyBasicInfoView.CLOSE_ON_DATE = AddDateTime(txtClosingDateTime.Text.Trim());
                                }
                                else if (AddDateTime(txtClosingDateTime.Text.Trim()) > launchdate.AddMonths(3))
                                {
                                    lblErrMsg.Text = "Survey closed date is already set.And the new date cannot exceed the date " + launchdate.AddMonths(3);
                                    lblErrMsg.Visible = true;
                                    dvErrMsg.Visible = true;

                                    lblSuccMsg.Text = "";
                                    lblSuccMsg.Visible = false;
                                    dvSuccessMsg.Visible = false;
                                    return;
                                }
                            }
                                

                        }
                        else
                        {
                            SurveyBasicInfoView.CLOSE_ON_DATE = AddDateTime(txtClosingDateTime.Text.Trim());
                            
                        }
                    }
                    if (txtRemaiderDate.Text != "")
                    {
                       // SurveyBasicInfoView.REMINDER_ON_DATE = Convert.ToDateTime(txtRemaiderDate.Text.Trim());  //Convert.ToDateTime(txtRemaiderDate.Text.Trim());

                        if (SurveyBasicInfoView.STATUS == "Draft")
                        {

                            if (AddDateTime(txtRemaiderDate.Text.Trim()) <= DateTime.Now.AddMonths(3))
                            {

                                SurveyBasicInfoView.REMINDER_ON_DATE = AddDateTime(txtRemaiderDate.Text.Trim());
                               
                            }
                            else if (AddDateTime(txtRemaiderDate.Text.Trim()) > DateTime.Now.AddMonths(3))
                            {
                                lblErrMsg.Text = "Reminder for paid survey has to be within 3 months of applying survey credit.";
                                lblErrMsg.Visible = true;
                                dvErrMsg.Visible = true;

                                lblSuccMsg.Text = "";
                                lblSuccMsg.Visible = false;
                                dvSuccessMsg.Visible = false;

                                return;
                            }

                        }
                        else if (SurveyBasicInfoView.STATUS == "Active")
                        {
                            if (AddDateTime(txtRemaiderDate.Text.Trim()) <= launchdate.AddMonths(3))
                            {
                                SurveyBasicInfoView.LAUNCH_ON_DATE = AddDateTime(txtRemaiderDate.Text.Trim());
                            }
                            else if (AddDateTime(txtRemaiderDate.Text.Trim()) > launchdate.AddMonths(3))
                            {
                                lblErrMsg.Text = "Reminder for paid survey has to be within the date " + launchdate.AddMonths(3);
                                lblErrMsg.Visible = true;
                                dvErrMsg.Visible = true;

                                lblSuccMsg.Text = "";
                                lblSuccMsg.Visible = false;
                                dvSuccessMsg.Visible = false;

                                return;
                            }
                        }
                    }
                    else
                    {
                        SurveyBasicInfoView.REMINDER_ON_DATE = defaultDate;
                    }
                    if (txtRemainder.Text != "")
                    {
                        SurveyBasicInfoView.REMINDER_TEXT = txtRemainder.Text.Trim();
                    }
                    else
                    {
                        SurveyBasicInfoView.REMINDER_TEXT = string.Empty;
                    }
                    if (rbtnLaunchSchedule.SelectedValue != "1" || (rbtnLaunchSchedule.SelectedValue == "1" && listFeatures.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYLAUNCH).Value == 0))
                        SurveyBasicInfoView.LAUNCH_ON_DATE = defaultDate;
                    if (rbtnSurveyClose.SelectedValue != "1" || (rbtnSurveyClose.SelectedValue == "1" && listFeatures.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYCLOSE).Value == 0))
                        SurveyBasicInfoView.CLOSE_ON_DATE = defaultDate;
                    int launchTyp = 0, surveyClose = 0;

                    launchTyp = ValidationHelper.GetInteger(rbtnLaunchSchedule.SelectedValue, 0);
                    if (rbtnSurveyClose.SelectedValue != null)
                    {
                        surveyClose = ValidationHelper.GetInteger(rbtnSurveyClose.SelectedValue, 0);
                    }

                    SurveyBasicInfoView.SCHEDULE_ON = launchTyp;
                    SurveyBasicInfoView.SCHEDULE_ON_CLOSE = surveyClose;
                    if (rbtnLaunchSchedule.SelectedValue == "1")
                    {
                        if (listFeatures.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYLAUNCH).Value != 1)
                        {
                            OndateLaunch_fea = false;
                        }
                        else
                        {
                            OndateLaunch_fea = true;
                        }
                    }
                    if (rbtnSurveyClose.SelectedValue == "1")
                    {
                        if (listFeatures.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYCLOSE).Value == 1)
                        {
                            OnDateClose_fea = true;
                        }
                        else
                        {
                            OnDateClose_fea = false;
                        }
                    }
                    if (OnDateClose_fea && OndateLaunch_fea && Alert_fea && Remainder_fea)
                    {
                        if (base.SurveyBasicInfoView.SURVEYCONTROL_ID > 0)
                        {
                            int alert_flag = 0;
                            if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0 && base.SurveyBasicInfoView.STATUS == "Active" && base.SurveyBasicInfoView.SURVEY_ID > 0 && base.SurveyBasicInfoView.SCHEDULE_ON == 1 && rbtnLaunchSchedule.SelectedValue == "0")
                            {
                                base.SurveyBasicInfoView.LAUNCH_ON_DATE = defaultDate;
                                controlId = 1;
                            }
                            if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0 && base.SurveyBasicInfoView.STATUS == "Active" && base.SurveyBasicInfoView.SCHEDULE_ON == 0 && rbtnLaunchSchedule.SelectedValue == "0")
                            {
                                SurveyBasicInfoView.LAUNCH_ON_DATE = base.SurveyBasicInfoView.LAUNCH_ON_DATE;
                                alert_flag = 1;
                            }

                            if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0 && base.SurveyBasicInfoView.STATUS == "Active")
                            {
                                var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();
                                IsBlockedWordExists = blockedWords.Where(b => txtRemainder.Text.Contains(b.BLOCKEDWORD_NAME)).Any();
                                if (IsBlockedWordExists)
                                {
                                    if (base.SurveyBasicInfoView != null && (base.SurveyBasicInfoView.SCHEDULE_ON != launchTyp || base.SurveyBasicInfoView.ONLAUNCH_ALERT != Convert.ToInt32(chkAlertOnSurveyLaunch.Checked) ||
                            base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE != surveyClose || base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS != ValidationHelper.GetInteger(txtNoofRespondents.Text.Trim(), 0) || base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE_ALERT != Convert.ToInt32(chkAlertOnSurveyClose.Checked) ||
                             base.SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES != ValidationHelper.GetInteger(txtAlert1.Text, 0) || base.SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES != ValidationHelper.GetInteger(txtAlert2.Text.Trim(), 0) ||
                                base.SurveyBasicInfoView.REMINDER_TEXT != txtRemainder.Text.Trim() || base.SurveyBasicInfoView.LAUNCH_ON_DATE != AddDateTime(txtDateTime.Text) || base.SurveyBasicInfoView.CLOSE_ON_DATE != AddDateTime(txtClosingDateTime.Text.Trim()) ||
                             base.SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE != AddDateTime(txtDate1.Text) || base.SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE != AddDateTime(txtDate2.Text) || base.SurveyBasicInfoView.REMINDER_ON_DATE != AddDateTime(txtRemaiderDate.Text.Trim())))
                                        addEmail = true;
                                }
                            }
                            ServiceFactory.GetService<SurveyControls>().UpdateSurveyControlsFromView(SurveyBasicInfoView);
                            if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0)
                            {
                                 //&& base.SurveyBasicInfoView.STATUS == "Active"
                                ServiceFactory.GetService<SurveyControls>().EditActiveAlertsUpdate(base.SurveyBasicInfoView.SURVEY_ID, alert_flag);
                            }
                        }
                        else
                        {
                            ServiceFactory.GetService<SurveyControls>().InsertSurveyControlsFromView(SurveyBasicInfoView);
                        }
                    }
                    else
                    {
                        IsBlockedWordExists = false;
                        Page.RegisterStartupScript("Upgrade", @"<script> OpenModalPopUP('"+PathHelper.GetUpgradeLicenseURL()+"',690, 515, 'yes');return false; </script>");
                    }

                }
                else
                {
                    IsBlockedWordExists = false;
                }
            }
        }
        protected void btnSaveContinue_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveSurveyControls();
                if (!IsBlockedWordExists)
                {
                    if (btnSaveContinue.Text == "Save and edit more")
                    {
                        string message = AlertMessageText();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "myscript", "alert('" + message + "');window.location.href='SurveyMessages.aspx';", true);
                    }
                    else
                    {
                        if (!dvErrMsg.Visible)
                        {
                            lblSuccMsg.Text = SurveyMode + " alerts saved successfully.";
                            lblSuccMsg.Visible = true;
                            dvSuccessMsg.Visible = true;
                            lblErrMsg.Text = "";
                            lblErrMsg.Visible = false;
                            dvErrMsg.Visible = false;
                        }
                        if (rbtnLaunchSchedule.SelectedValue == "1")
                        {
                            launchDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            launchDateTime.Style.Add("display", "none");
                        }
                        if (rbtnSurveyClose.SelectedValue == "1")
                        {
                            surveyDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            surveyDateTime.Style.Add("display", "none");
                        }
                        if (rbtnNoofRespondents.Checked)
                        {
                            txtNoofRespondents.Enabled = true;
                        }
                        else
                        {
                            txtNoofRespondents.Enabled = false;
                        }
                    }
                }
            }
        }
        protected void btnSaveActive_Click(object sender, EventArgs e)
        {
            SaveSurveyControls();
            if (IsBlockedWordExists)
            {
                string message = AlertMessageText();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "myscript", "alert('" + message + "');", true);
            }

        }
        protected void btnSaveEditEmail_Click(object sender, EventArgs e)
        {

            SaveSurveyControls();
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            if (IsBlockedWordExists)
            {
                if (listFeatures.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYLAUNCH).Value == 1)
                {

                    string message = AlertMessageText();
                    string navURL = EncryptHelper.EncryptQuerystring("EditActiveSurveys.aspx", "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
                    if (addEmail)

                        Page.RegisterStartupScript("myscript", "alert('" + message + "');window.location.href='" + navURL + "';");
                    else
                        Response.Redirect(navURL);
                }
                else
                {
                    Page.RegisterStartupScript("Upgrade", @"<script> OpenModalPopUP('"+PathHelper.GetUpgradeLicenseURL()+"',690, 515, 'yes');return false; </script>");
                }
            }
        }
        private bool ValidatedateControls()
        {
            DateTime dt_laun_validate = Convert.ToDateTime("1/1/1800");
            DateTime dt_clos_validate = Convert.ToDateTime("1/1/1800");

            if (rbtnLaunchSchedule.SelectedIndex == 1)
            {
                dt_laun_validate = AddDateTime(txtDateTime.Text.Trim());
                if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0 && base.SurveyBasicInfoView.STATUS == "Active")
                {
                    if (base.SurveyBasicInfoView.SCHEDULE_ON == 0)
                    {
                        lblErrMsg.Text = SurveyMode + " is already launched, Hence launch date cannot be re-scheduled.";
                        lblErrMsg.Visible = true;
                        dvErrMsg.Visible = true;
                        dvSuccessMsg.Visible = false;                       
                        if (rbtnLaunchSchedule.SelectedValue == "1")
                        {
                            launchDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            launchDateTime.Style.Add("display", "none");
                        }
                        if (rbtnSurveyClose.SelectedValue == "1")
                        {
                            surveyDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            surveyDateTime.Style.Add("display", "none");
                        }
                        return false;
                    }
                    else
                    {
                        lblErrMsg.Visible = false;
                        dvErrMsg.Visible = false;
                    }

                }
            }
            if (rbtnSurveyClose.SelectedValue == "1")
            {
                dt_clos_validate = AddDateTime(txtClosingDateTime.Text.Trim());
            }

            if (rbtnSurveyClose.SelectedValue == "1" && rbtnLaunchSchedule.SelectedIndex == 1 && dt_clos_validate <= dt_laun_validate)
            {
                lblErrMsg.Text = SurveyMode + " close date should be after the launch date.";
                lblErrMsg.Visible = true;
                dvErrMsg.Visible = true;
                dvSuccessMsg.Visible = false;
                if (rbtnLaunchSchedule.SelectedValue == "1")
                {
                    launchDateTime.Style.Add("display", "block");
                }
                else
                {
                    launchDateTime.Style.Add("display", "none");
                }
                if (rbtnSurveyClose.SelectedValue == "1")
                {
                    surveyDateTime.Style.Add("display", "block");
                }
                else
                {
                    surveyDateTime.Style.Add("display", "none");
                }
                return false;
            }
            else
            {
                lblErrMsg.Visible = false;
                dvErrMsg.Visible = false;
            }

            if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0 && base.SurveyBasicInfoView.STATUS == "Active")
            {
                var responseCount = base.SurveyBasicInfoView.ResponseCount;
                //ServiceFactory.GetService<SurveyService>().GetResponseSurveyBySurveyId(base.SurveyBasicInfoView);
                if (rbtnNoofRespondents.Checked)
                {
                    if (ValidationHelper.GetInteger(txtNoofRespondents.Text.Trim(), 0) <= responseCount)
                    {
                        lblErrMsg.Text = "This " + SurveyMode.ToLower()+ " has already met the required number of responses as per quota specified by you. Please input a number greater than the responses already received.";
                        lblErrMsg.Visible = true;
                        dvErrMsg.Visible = true;
                        lblSuccMsg.Visible = false;
                        dvSuccessMsg.Visible = false;
                        dvSuccessMsg.Visible = false;
                        txtNoofRespondents.Enabled = true;
                        if (rbtnLaunchSchedule.SelectedValue == "1")
                        {
                            launchDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            launchDateTime.Style.Add("display", "none");
                        }
                        if (rbtnSurveyClose.SelectedValue == "1")
                        {
                            surveyDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            surveyDateTime.Style.Add("display", "none");
                        }
                        return false;
                    }
                    else
                    {
                        lblErrMsg.Visible = false;
                        dvErrMsg.Visible = false;
                    }
                    if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE == 1)
                    {

                        if (Convert.ToDateTime(hdnCloseDate.Value) <= DateTime.UtcNow)
                        {
                            lblErrMsg.Text = SurveyMode + " is already closed, Hence close date cannot be re-scheduled.";
                            lblErrMsg.Visible = true;
                            dvErrMsg.Visible = true;
                            lblSuccMsg.Visible = false;
                            dvSuccessMsg.Visible = false;
                            dvSuccessMsg.Visible = false;
                            if (rbtnLaunchSchedule.SelectedValue == "1")
                            {
                                launchDateTime.Style.Add("display", "block");
                            }
                            else
                            {
                                launchDateTime.Style.Add("display", "none");
                            }
                            if (rbtnSurveyClose.SelectedValue == "1")
                            {
                                surveyDateTime.Style.Add("display", "block");
                            }
                            else
                            {
                                surveyDateTime.Style.Add("display", "none");
                            }
                            return false;
                        }
                        else
                        {
                            lblErrMsg.Visible = false;
                            dvErrMsg.Visible = false;
                        }
                    }
                }
                if (rbtnSurveyClose.SelectedValue == "1")
                {
                    if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE == 0)
                    {
                        if (hdnCloseDate.Value != "" && Convert.ToDateTime(hdnCloseDate.Value) <= DateTime.UtcNow)
                        {
                            lblErrMsg.Text = SurveyMode + " is already closed, Hence close date cannot be re-scheduled.";
                            lblErrMsg.Visible = true;
                            dvErrMsg.Visible = true;
                            lblSuccMsg.Visible = false;
                            dvSuccessMsg.Visible = false;
                            dvSuccessMsg.Visible = false;
                            if (rbtnLaunchSchedule.SelectedValue == "1")
                            {
                                launchDateTime.Style.Add("display", "block");
                            }
                            else
                            {
                                launchDateTime.Style.Add("display", "none");
                            }
                            if (rbtnSurveyClose.SelectedValue == "1")
                            {
                                surveyDateTime.Style.Add("display", "block");
                            }
                            else
                            {
                                surveyDateTime.Style.Add("display", "none");
                            }
                            return false;
                        }
                        else
                        {
                            lblErrMsg.Visible = false;
                            dvErrMsg.Visible = false;
                        }
                    }

                }
                if (rbtnSurveyClose.SelectedValue == "0")
                {
                    if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE == 0)
                    {
                        if (hdnCloseDate.Value != "" && Convert.ToDateTime(hdnCloseDate) <= DateTime.UtcNow)
                        {
                            lblErrMsg.Text = SurveyMode + " is already closed, Hence close date cannot be re-scheduled.";
                            lblErrMsg.Visible = true;
                            dvErrMsg.Visible = true;
                            lblSuccMsg.Visible = false;
                            dvSuccessMsg.Visible = false;
                            dvSuccessMsg.Visible = false;
                            if (rbtnLaunchSchedule.SelectedValue == "1")
                            {
                                launchDateTime.Style.Add("display", "block");
                            }
                            else
                            {
                                launchDateTime.Style.Add("display", "none");
                            }
                            if (rbtnSurveyClose.SelectedValue == "1")
                            {
                                surveyDateTime.Style.Add("display", "block");
                            }
                            else
                            {
                                surveyDateTime.Style.Add("display", "none");
                            }
                            return false;
                        }
                        else
                        {
                            lblErrMsg.Visible = false;
                            dvErrMsg.Visible = false;
                        }
                    }
                    else if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE == 1)
                    {
                        if (hdnCloseDate.Value != "" && Convert.ToDateTime(hdnCloseDate.Value) <= DateTime.UtcNow)
                        {
                            lblErrMsg.Text = SurveyMode + " is already closed, Hence close date cannot be re-scheduled.";
                            lblErrMsg.Visible = true;
                            dvErrMsg.Visible = true;
                            lblSuccMsg.Visible = false;
                            dvSuccessMsg.Visible = false;
                            dvSuccessMsg.Visible = false;
                            if (rbtnLaunchSchedule.SelectedValue == "1")
                            {
                                launchDateTime.Style.Add("display", "block");
                            }
                            else
                            {
                                launchDateTime.Style.Add("display", "none");
                            }
                            if (rbtnSurveyClose.SelectedValue == "1")
                            {
                                surveyDateTime.Style.Add("display", "block");
                            }
                            else
                            {
                                surveyDateTime.Style.Add("display", "none");
                            }
                            return false;
                        }
                        else
                        {
                            lblErrMsg.Visible = false;
                            dvErrMsg.Visible = false;
                        }
                    }

                }
            }
            DateTime defaultDate = Convert.ToDateTime("1/1/1800");
            if (txtDate1.Text.Trim().Length > 0)
            {
                DateTime ALERT_ON_MIN_RESPONSES = (defaultDate < Convert.ToDateTime(txtDate1.Text.Trim())) ? Convert.ToDateTime(txtDate1.Text.Trim().Split(' ')[0]) : defaultDate;
                if (rbtnLaunchSchedule.SelectedValue == "1")
                {
                    ALERT_ON_MIN_RESPONSES = AddDateTime(txtDate1.Text.Trim());
                    if (ALERT_ON_MIN_RESPONSES < dt_laun_validate)
                    {
                        lblErrMsg.Text = "Please choose valid date for minimum number of responses.";
                        lblErrMsg.Visible = true;
                        dvErrMsg.Visible = true;
                        lblSuccMsg.Visible = false;
                        dvSuccessMsg.Visible = false;
                        dvSuccessMsg.Visible = false;
                        if (rbtnLaunchSchedule.SelectedValue == "1")
                        {
                            launchDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            launchDateTime.Style.Add("display", "none");
                        }
                        if (rbtnSurveyClose.SelectedValue == "1")
                        {
                            surveyDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            surveyDateTime.Style.Add("display", "none");
                        }
                        return false;
                    }
                    else
                    {
                        lblErrMsg.Visible = false;
                        dvErrMsg.Visible = false;
                    }
                }
                if (rbtnSurveyClose.SelectedValue == "1")
                {
                    ALERT_ON_MIN_RESPONSES = AddDateTime(txtDate1.Text.Trim());
                    if (ALERT_ON_MIN_RESPONSES > dt_clos_validate)
                    {
                        lblErrMsg.Text = "Minimum responses date should be before close date.";
                        lblErrMsg.Visible = true;
                        dvErrMsg.Visible = true;
                        lblSuccMsg.Visible = false;
                        dvSuccessMsg.Visible = false;
                        dvSuccessMsg.Visible = false;
                        if (rbtnLaunchSchedule.SelectedValue == "1")
                        {
                            launchDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            launchDateTime.Style.Add("display", "none");
                        }
                        if (rbtnSurveyClose.SelectedValue == "1")
                        {
                            surveyDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            surveyDateTime.Style.Add("display", "none");
                        }
                        return false;
                    }
                    else
                    {
                        lblErrMsg.Visible = false;
                        dvErrMsg.Visible = false;
                    }
                }
                if (ALERT_ON_MIN_RESPONSES < dt_laun_validate)
                {
                    lblErrMsg.Text = "Please choose valid date for minimum number of responses.";
                    lblErrMsg.Visible = true;
                    dvErrMsg.Visible = true;
                    lblSuccMsg.Visible = false;
                    dvSuccessMsg.Visible = false;
                    dvSuccessMsg.Visible = false;
                    if (rbtnLaunchSchedule.SelectedValue == "1")
                    {
                        launchDateTime.Style.Add("display", "block");
                    }
                    else
                    {
                        launchDateTime.Style.Add("display", "none");
                    }
                    if (rbtnSurveyClose.SelectedValue == "1")
                    {
                        surveyDateTime.Style.Add("display", "block");
                    }
                    else
                    {
                        surveyDateTime.Style.Add("display", "none");
                    }
                    return false;
                }
                else
                {
                    lblErrMsg.Visible = false;
                    dvErrMsg.Visible = false;
                }

            }
            if (txtDate2.Text.Trim().Length > 0)
            {
                if (txtDate1.Text.Trim().Length > 0)
                {

                    DateTime ALERT_ON_MIN_RESPONSES = (defaultDate < Convert.ToDateTime(txtDate1.Text.Trim().Split(' ')[0])) ? Convert.ToDateTime(txtDate1.Text.Trim().Split(' ')[0]) : defaultDate;

                    ALERT_ON_MIN_RESPONSES = AddDateTime(txtDate1.Text);
                    DateTime ALERT_ON_MAX_RESPONSES = (defaultDate < Convert.ToDateTime(txtDate2.Text.Trim().Split(' ')[0])) ? Convert.ToDateTime(txtDate2.Text.Trim().Split(' ')[0]) : defaultDate;
                    ALERT_ON_MAX_RESPONSES = AddDateTime(txtDate2.Text.Trim());
                    if (ALERT_ON_MAX_RESPONSES < DateTime.UtcNow)
                    {
                        lblErrMsg.Text = "Please choose valid date for maximum number of responses.";
                        lblErrMsg.Visible = true;
                        dvErrMsg.Visible = true;
                        lblSuccMsg.Visible = false;
                        dvSuccessMsg.Visible = false;
                        dvSuccessMsg.Visible = false;
                        if (rbtnLaunchSchedule.SelectedValue == "1")
                        {
                            launchDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            launchDateTime.Style.Add("display", "none");
                        }
                        if (rbtnSurveyClose.SelectedValue == "1")
                        {
                            surveyDateTime.Style.Add("display", "block");
                        }
                        else
                        {
                            surveyDateTime.Style.Add("display", "none");
                        }
                        return false;
                    }
                    else
                    {
                        lblErrMsg.Visible = false;
                        dvErrMsg.Visible = false;
                    }
                    if (txtDate2.Text.Length > 0)
                    {
                        if (ALERT_ON_MAX_RESPONSES < ALERT_ON_MIN_RESPONSES)
                        {

                            lblErrMsg.Text = "Minimum responses date should be less than maximum responses date.";
                            lblErrMsg.Visible = true;
                            dvErrMsg.Visible = true;
                            lblSuccMsg.Visible = false;
                            dvSuccessMsg.Visible = false;
                            dvSuccessMsg.Visible = false;
                            if (rbtnLaunchSchedule.SelectedValue == "1")
                            {
                                launchDateTime.Style.Add("display", "block");
                            }
                            else
                            {
                                launchDateTime.Style.Add("display", "none");
                            }
                            if (rbtnSurveyClose.SelectedValue == "1")
                            {
                                surveyDateTime.Style.Add("display", "block");
                            }
                            else
                            {
                                surveyDateTime.Style.Add("display", "none");
                            }
                            return false;
                        }
                        else
                        {
                            lblErrMsg.Visible = false;
                            dvErrMsg.Visible = false;
                        }
                    }
                }

            }
            if (txtRemaiderDate.Text.Length > 0)
            {
                if (Convert.ToDateTime(txtRemaiderDate.Text.Trim()) < DateTime.Today)
                {
                    lblErrMsg.Text = "Please choose valid date for reminder.";
                    lblErrMsg.Visible = true;
                    dvErrMsg.Visible = true;
                    lblSuccMsg.Visible = false;
                    dvSuccessMsg.Visible = false;
                    dvSuccessMsg.Visible = false;
                    if (rbtnLaunchSchedule.SelectedValue == "1")
                    {
                        launchDateTime.Style.Add("display", "block");
                    }
                    else
                    {
                        launchDateTime.Style.Add("display", "none");
                    }
                    if (rbtnSurveyClose.SelectedValue == "1")
                    {
                        surveyDateTime.Style.Add("display", "block");
                    }
                    else
                    {
                        surveyDateTime.Style.Add("display", "none");
                    }
                    return false;

                }
                else
                {
                    lblErrMsg.Visible = false;
                    dvErrMsg.Visible = false;
                }
            }

            return true;

        }
        private DateTime AddDateTime(string dateText)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            DateTime returnDate;
            returnDate = Convert.ToDateTime(dateText);
            //returnDate= returnDate.AddHours(Convert.ToDouble(dateText.Split(' ')[1].Split(':')[0]));
            //returnDate = returnDate.AddMinutes(Convert.ToDouble(dateText.Split(' ')[1].Split(':')[1]));
            return CommonMethods.GetConvertedDatetime(returnDate, -(userDetails.StandardBIAS), true);
        }
        public string AlertMessageText()
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            string txtMessage = "";
            txtMessage = "You have saved changes to an active " + SurveyMode.ToLower() + ". These changes are effective immediately and your respondents will now see the revised " + SurveyMode.ToLower();
            if (controlId == 0)
            {
                if (base.SurveyBasicInfoView.EMAILLIST_TYPE != 2)
                {
                    if (base.SurveyBasicInfoView.SCHEDULE_ON == 1 && CommonMethods.GetConvertedDatetime(base.SurveyBasicInfoView.LAUNCH_ON_DATE, userDetails.StandardBIAS, true) > CommonMethods.GetConvertedDatetime(DateTime.UtcNow, userDetails.StandardBIAS, true))
                    {
                        if (base.SurveyBasicInfoView.EMAILLIST_TYPE == 1 && base.SurveyBasicInfoView.CONTACTLIST_ID > 0)
                        {
                            var contactDetails = ServiceFactory.GetService<EmailService>().GetContactListByContactListId(base.SurveyBasicInfoView.CONTACTLIST_ID);
                            if (contactDetails != null)
                            {
                                var contactListName = contactDetails.Select(s => new { s.CONTACTLIST_NAME });
                                txtMessage = "Your changes have been saved. Your respondents will see the revised changes when the " + SurveyMode.ToLower() + " gets launched on " + base.SurveyBasicInfoView.LAUNCH_ON_DATE.AddMinutes(userDetails.StandardBIAS);
                                txtMessage += ". If you wish to change the date of launch, you may do so from the " + SurveyMode + " Controls page. The survey will be sent to " + contactDetails.Count + " email ids that are currently in your email list " + contactListName;
                            }
                        }
                        else
                        {
                            txtMessage = "Your changes have been saved. Your respondents will see the revised changes when the " + SurveyMode.ToLower() + " gets launched on" + base.SurveyBasicInfoView.LAUNCH_ON_DATE.AddMinutes(userDetails.StandardBIAS);

                        }
                    }
                }
                else
                {
                    txtMessage = "Changes have been made in the " + SurveyMode.ToLower() + " and will reflect in the URL that you have used to ";
                    txtMessage += "launch the " + SurveyMode.ToLower() + ". In case you want to send the " + SurveyMode.ToLower() + " to more people, please copy and ";
                    txtMessage += "paste the URL below and send through your own mail system / IM / Social media.                                                                                                                                             ";
                    txtMessage += " URL : " + base.SurveyBasicInfoView.SURVEY_URL;
                }

            }
            return txtMessage;
        }

        #region ValidateLaunchDate_ServerValidate
        protected void ValidateLaunchDate_ServerValidate(Object sender, ServerValidateEventArgs args)
        {
            if (rbtnLaunchSchedule.SelectedValue == "1" && txtDateTime.Text.Length <= 0)
            {
                args.IsValid = false;
                launchDateTime.Style.Add("display", "block");
                txtDateTime.Focus();
            }
            else
            {
                args.IsValid = true;                
            }
        }
        #endregion

        #region ValidateCloseDate_ServerValidate
        protected void ValidateCloseDate_ServerValidate(Object sender, ServerValidateEventArgs args)
        {
            if (rbtnSurveyClose.SelectedValue == "1" && txtClosingDateTime.Text.Length <= 0)
            {
                args.IsValid = false;
                surveyDateTime.Style.Add("display", "block");
                txtClosingDateTime.Focus();
            }
            else
            {
                args.IsValid = true;
            }
        }
        #endregion
        

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetPreLaunchSurveyControlsURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag));
        }
    }
}