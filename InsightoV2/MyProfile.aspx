﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="MyProfile.aspx.cs" Inherits="Insighto.Pages.MyProfile" Culture="auto"
     UICulture="auto" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register Src="UserControls/ProfileRightPanel.ascx" TagName="ProfileRightPanel"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="myProfileLeftPanel">
        <div class="marginNewPanel" style="margin-top:0;">
            <div class="signUpPnl">
                <!-- customer details -->
                <h3>
                    <asp:Label ID="lblCustomerDetails" runat="server" Text="User Details " meta:resourcekey="lblCustomerDetailsResource1"></asp:Label></h3>
               <div class="messagePanelProfile">
                 <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                      <div><asp:Label ID="lblSuccessMsg" runat="server"
                        Text="User details has been updated successfully."></asp:Label></div>
                     </div>
                 <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">   
                    <div><asp:Label ID="lblErrMsg" runat="server" 
                         meta:resourcekey="lblErrMsgResource1"></asp:Label></div>                   
                </div>
                </div>
                <table border="0" width="100%" cellpadding="2">
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblCustomerId" runat="server" Text="Customer ID" meta:resourcekey="lblCustomerIdResource1"></asp:Label>
                        </td>
                        <td class="wid_70">
                            <asp:Label ID="txtCustomerId" runat="server" meta:resourcekey="txtCustomerIdResource1"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblLoginId" Text="Login ID" runat="server" meta:resourcekey="lblLoginIdResource1"></asp:Label>
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="txtLoginId" runat="server" meta:resourcekey="txtLoginIdResource1"></asp:Label></b>
                        </td>
                    </tr>
                </table>
                <!-- //customer details -->
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="marginNewPanel">
            <div class="signUpPnl">
                <!-- subscription details -->
                <h3>
                    <asp:Label ID="lblSubscriptionDetails" runat="server" Text="Subscription Details"
                        meta:resourcekey="lblSubscriptionDetailsResource1"></asp:Label>
                </h3>
                <p>
                    &nbsp;</p>
                <table border="0" width="100%" cellpadding="2">
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <b><asp:Label ID="Label3" Text="Surveys" runat="server"></asp:Label></b>
                        </td>
                        <td>
                            <b><asp:Label ID="Label4" Text="Polls" runat="server"></asp:Label></b>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblCurrentSubscription" Text="Current Subscription" runat="server"
                                meta:resourcekey="lblCurrentSubscriptionResource1"></asp:Label>
                        </td>
                        <td class="wid_70" style="width:35%;">
                            <b>
                                <asp:Label ID="txtCurrentSubscription" runat="server" meta:resourcekey="txtCurrentSubscriptionResource1" />
                            </b>
                        </td>
                        <td class="wid_70" style="width:35%;">
                            <b>
                                <asp:Label ID="txtCurrentSubscriptionPoll" runat="server" meta:resourcekey="txtCurrentSubscriptionResource1" />
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblStartDate" Text="Subscription Start Date" runat="server" meta:resourcekey="lblStartDateResource1"></asp:Label>
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="txtStartDate" runat="server" meta:resourcekey="txtStartDateResource1"></asp:Label>
                            </b>
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="txtStartDatePoll" runat="server" meta:resourcekey="txtStartDateResource1"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblExpiryDate" Text="Subscription Expiry" runat="server" meta:resourcekey="lblExpiryDateResource1"></asp:Label>
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="txtExpiryDate" runat="server" meta:resourcekey="txtExpiryDateResource1"></asp:Label>
                            </b>
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="txtExpiryDatePoll" runat="server" meta:resourcekey="txtExpiryDateResource1"></asp:Label>
                            </b>
                        </td>
                    </tr>
                </table>
                <!-- //subscription details -->
            </div>
        </div>
        <div class="clear">
        </div>

       <%--  <div class="marginNewPanel">
            <div class="signUpPnl">
                <!-- subscription details -->
                <h3>
                    <asp:Label ID="Label3" runat="server" Text="Linked Accounts"
                       ></asp:Label>
                </h3>
                <p>
                    &nbsp;</p>
                <table border="0" width="100%" cellpadding="2">
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblGoogle" Text="" CssClass="myprofile_googleImg" runat="server" 
                               ></asp:Label><asp:HyperLink ID="hlnkGoogle" NavigateUrl="#"  runat="server">Login Google</asp:HyperLink>
                        </td>
                        <%--onclick="javascript: return fnOpenWindow('Google');"
                        onclick="javascript: return fnOpenWindow('Facebook');"
                        <td class="wid_40">
                            <b>
                                <asp:Label ID="lblGoogleEmail" runat="server"  /></b>
                        </td>
                         <td class="wid_30">
                             <asp:Button ID="btnGoogleRemove" ValidationGroup="LinkAccounts" CssClass="btnbg" runat="server" Text="Remove" OnClick="btnGoogleRemove_Click" />
                        </td>

                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblFacebook" CssClass="myprofile_favoriteImg" runat="server"></asp:Label><asp:HyperLink ID="hlnkFacebook" NavigateUrl="#"  runat="server">Login Facebook</asp:HyperLink>
                        </td>
                        <td class="wid_40">
                            <b>
                                <asp:Label ID="lblFacebookEmail" runat="server" /></b>
                        </td>
                         <td class="wid_30">
                         <asp:Button ID="btnFacebookRemove" OnClick="btnFacebookRemove_Click"  CssClass="btnbg" ValidationGroup="LinkAccounts1" runat="server"  Text="Remove" />
                        </td>

                    </tr>
                   
                </table>
                <!-- //subscription details -->
            </div>
        </div>
        <div class="clear">
        </div>--%>
        <div class="marginNewPanel">
            <div class="signUpPnl">
                <!-- manage profile -->
                <h3>
                    <asp:Label ID="lblSubcriberInformation" runat="server" Text="User Information"
                        meta:resourcekey="lblSubcriberInformationResource1"></asp:Label>
                </h3>
                <div class="mandatoryPanel">
                    <asp:Label ID="lblRequiredFields" runat="server" Text="indicates required fields"></asp:Label>
            </div>
                <table border="0" width="100%" cellpadding="2">
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblFirstName" CssClass="requirelbl" Text="First Name" AssociatedControlID="txtFirstName"
                                runat="server" meta:resourcekey="lblFirstNameResource1" />
                        </td>
                        <td class="wid_70">
                            <asp:TextBox ID="txtFirstName" runat="server" CssClass="textBoxMedium" MaxLength="30"
                                meta:resourcekey="txtFirstNameResource1" /><br />
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqFirstName" CssClass="lblRequired" ControlToValidate="txtFirstName"
                                meta:resourcekey="lblReqFirstNameResource1" runat="server" Display="Dynamic" ValidationGroup="valGroup"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regFirstName" CssClass="lblRequired" ControlToValidate="txtFirstName"
                                runat="server" ErrorMessage="Please enter alphabets only." ValidationExpression="^[a-zA-Z ]*$"
                                Display="Dynamic" ValidationGroup="valGroup"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblLastName" CssClass="requirelbl" Text="Last Name" AssociatedControlID="txtLastName" runat="server"
                                meta:resourcekey="lblLastNameResource1" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server" class="textBoxMedium" MaxLength="30"
                                meta:resourcekey="txtLastNameResource1" /><br />
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqLastName" CssClass="lblRequired" ControlToValidate="txtLastName"
                                meta:resourcekey="lblReqLastNameResource1" runat="server" Display="Dynamic" ValidationGroup="valGroup"></asp:RequiredFieldValidator>
                           <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regLastName" CssClass="lblRequired" ControlToValidate="txtLastName"
                                runat="server" ErrorMessage="Please enter alphabets only." ValidationExpression="^[a-zA-Z ]*$"
                                Display="Dynamic" ValidationGroup="valGroup"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblAddressLine1" CssClass="requirelbl" AssociatedControlID="txtAddressLine1" Text="Address Line 1"
                                runat="server" meta:resourcekey="lblAddressLine1Resource1" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddressLine1" runat="server" class="textBoxMedium" MaxLength="100"
                                meta:resourcekey="txtAddressLine1Resource1" /><br />
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqAddressLine1" CssClass="lblRequired" ControlToValidate="txtAddressLine1"
                                meta:resourcekey="lblReqAddressLine1Resource1" runat="server" Display="Dynamic" ValidationGroup="valGroup"></asp:RequiredFieldValidator>
                            <%-- <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regAddressLine1"  CssClass="lblRequired" ControlToValidate="txtAddressLine1"
                  runat="server" meta:resourcekey="lblRegAddressLine1Resource1" 
                  ValidationExpression="^.{4,100}$"  Display="Dynamic"></asp:RegularExpressionValidator>    --%>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblAddressLine2" Text="Address Line 2" AssociatedControlID="txtAddressLine2"
                                runat="server" meta:resourcekey="lblAddressLine2Resource1" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddressLine2" runat="server" class="textBoxMedium" MaxLength="100"
                                meta:resourcekey="txtAddressLine2Resource1" /><br />
                            <%--<asp:RequiredFieldValidator SetFocusOnError="true" ID="reqAddressLine2" CssClass="lblRequired" 
                 ControlToValidate="txtAddressLine2" 
                 meta:resourcekey="lblReqAddressLine2Resource1"  runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>--%>
                           <%-- <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regAddressLine2" CssClass="lblRequired" ControlToValidate="txtAddressLine2"
                                runat="server" meta:resourcekey="lblRegAddressLine2Resource1" ValidationExpression="^.{4,100}$"
                                Display="Dynamic"></asp:RegularExpressionValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblCity" Text="City" CssClass="requirelbl" runat="server" AssociatedControlID="txtCity"
                                meta:resourcekey="lblCityResource1"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCity" runat="server" class="textBoxMedium" MaxLength="50" meta:resourcekey="txtCityResource1" />
                            <br />
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqCity" CssClass="lblRequired" ControlToValidate="txtCity"
                                meta:resourcekey="lblReqCityResource1" runat="server" Display="Dynamic" ValidationGroup="valGroup"></asp:RequiredFieldValidator>
                            <%-- <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regCity"  CssClass="lblRequired" ControlToValidate="txtCity"
                  runat="server" meta:resourcekey="lblRegCityResource1" 
                  ValidationExpression="^.{4,100}$"  Display="Dynamic"></asp:RegularExpressionValidator>     --%>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblState" Text="State" AssociatedControlID="txtState" runat="server"
                                meta:resourcekey="lblStateResource1"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtState" runat="server" class="textBoxMedium" MaxLength="50" meta:resourcekey="txtStateResource1" /><br />
                            <%-- 
                <asp:Label ID="Label8"  AssociatedControlID="regState"  runat="server"  />
                 <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqState" CssClass="lblRequired" 
                 ControlToValidate="txtState" 
                 meta:resourcekey="lblReqStateResource1"  runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regState"  CssClass="lblRequired" ControlToValidate="txtState"
                  runat="server" meta:resourcekey="lblRegStateResource1" 
                  ValidationExpression="^.{4,50}$"  Display="Dynamic"></asp:RegularExpressionValidator> --%>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblPostalCode" Text="State" AssociatedControlID="txtPostalCode" runat="server"
                                meta:resourcekey="lblPostalCodeResource1"></asp:Label>    
                        </td>
                        <td>
                            <asp:TextBox ID="txtPostalCode" runat="server" class="textBoxMedium" MaxLength="10"
                                meta:resourcekey="txtPostalCodeResource1" />
                                
                                <br />
                                   <asp:RegularExpressionValidator SetFocusOnError="true"  ID="RegularExpressionValidator1" CssClass="lblRequired" ControlToValidate="txtPostalCode"
                                runat="server" ValidationExpression="^[0-9]+$" ErrorMessage="Please enter numbers only."
                                Display="Dynamic" ValidationGroup="valGroup"></asp:RegularExpressionValidator>
                               
  <%-- <li class="requiredLi">
                <asp:Label ID="Label9"  AssociatedControlID="regPostalCode"  runat="server"  />
                -- <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqPostalCode" CssClass="lblRequired" 
                 ControlToValidate="txtPostalCode" 
                 meta:resourcekey="lblReqPostalCodeResource1"  runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
                 <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regPostalCode"  CssClass="lblRequired" ControlToValidate="txtPostalCode"
                  runat="server" meta:resourcekey="lblRegPostalCodeResource1" 
                  ValidationExpression="^.{4,10}$"  Display="Dynamic"></asp:RegularExpressionValidator>  
             </li> --%>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblCountry" CssClass="requirelbl" Text="Country" AssociatedControlID="drpCountry" runat="server"
                                meta:resourcekey="lblCountryResource1"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="drpCountry" runat="server" class="dropdownMedium" meta:resourcekey="drpCountryResource1">
                            </asp:DropDownList>
                            <br />
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqCountry" CssClass="lblRequired" InitialValue=" "
                                ControlToValidate="drpCountry" meta:resourcekey="lblReqCountryResource1" runat="server"
                                Display="Dynamic" ValidationGroup="valGroup"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="tdOrganization" runat="server">
                        <td class="wid_30">
                            <asp:Label ID="lblOrganization" Text="Organization" AssociatedControlID="txtOrganization"
                                runat="server" meta:resourcekey="lblOrganizationResource1"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtOrganization" runat="server" class="textBoxMedium" MaxLength="100"
                                />
                        </td>
                    </tr>
                    <tr id="tdDept" runat="server">
                        <td class="wid_30">
                            <asp:Label ID="lblDept" Text="Dept" AssociatedControlID="txtDept" runat="server"
                                meta:resourcekey="lblDeptResource1"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDept" runat="server" class="textBoxMedium" MaxLength="100" meta:resourcekey="txtDeptResource1" />
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblDesignation" Text="Designation" AssociatedControlID="txtDesignation"
                                runat="server" meta:resourcekey="lblDesignationResource1"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDesignation" runat="server" class="textBoxMedium" MaxLength="100"
                                meta:resourcekey="txtDesignationResource1" />
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblWorkIndustry" Text="Work Industry" AssociatedControlID="drpWorkIndustry"
                                runat="server" meta:resourcekey="lblWorkIndustryResource1"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="drpWorkIndustry" runat="server" class="dropdownMedium" meta:resourcekey="drpWorkIndustryResource1">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblJobFunction" Text="Job Function" AssociatedControlID="drpJobFunction"
                                runat="server" meta:resourcekey="lblJobFunctionResource1"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="drpJobFunction" runat="server" class="dropdownMedium" meta:resourcekey="drpJobFunctionResource1">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblCompanySize" Text="Company Size" AssociatedControlID="drpCompanySize"
                                runat="server" meta:resourcekey="lblCompanySizeResource1"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="drpCompanySize" runat="server" class="dropdownMedium" meta:resourcekey="drpCompanySizeResource1">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblPhoneNumber" CssClass="requirelbl" Text="Phone Number" AssociatedControlID="txtPhoneNumber"
                                runat="server" meta:resourcekey="lblPhoneNumberResource1"></asp:Label>
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtPhoneNumber" runat="server" class="phoneCountry"  MaxLength="4" meta:resourcekey="txtPhoneNumberResource1" onkeypress="javascript:return onlyNumbers(event,'Countrycode');" onpaste="return false;" />
                                    </td>
                                    <td class="spacepanel">
                                        &#45;
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPhoneNumber1" runat="server" class="phoneArea"  MaxLength="5" meta:resourcekey="txtPhoneNumber1Resource1" onkeypress="javascript:return onlyNumbers(event,'');" onpaste="return false;"/>
                                    </td>
                                    <td class="spacepanel">
                                        &#45;
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPhoneNumber2" runat="server" class="phoneNumber" MaxLength="10" meta:resourcekey="txtPhoneNumber2Resource1" onkeypress="javascript:return onlyNumbers(event,'');" onpaste="return false;"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                        </td>
                        <td>
                            <asp:Label ID="phoneText" class="note" runat="server" Text="Country Code - Area Code - Phone Number."></asp:Label>
                            <br />
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqPhone" CssClass="lblRequired" ControlToValidate="txtPhoneNumber2"
                                meta:resourcekey="lblReqPhoneResource1" runat="server" Display="Dynamic" ValidationGroup="valGroup"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regPhone" CssClass="lblRequired" ControlToValidate="txtPhoneNumber2"
                                runat="server" ValidationExpression="^.*(?=.{1,10})(?=.*\d).*$" meta:resourcekey="lblregPhoneResource1"
                                Display="Dynamic" ValidationGroup="valGroup"></asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regPhoneArea" CssClass="lblRequired" ControlToValidate="txtPhoneNumber1"
                                runat="server" ValidationExpression="^.*(?=.{0,5})(?=.*\d).*$" meta:resourcekey="lblregAreaResource1"
                                Display="Dynamic" ValidationGroup="valGroup"></asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regCountryCode" CssClass="lblRequired" ControlToValidate="txtPhoneNumber"
                                runat="server" ValidationExpression="^.*(?=.{0,4})(?=.*\d).*$" meta:resourcekey="lblregCountryCodeResource1"
                                Display="Dynamic" ValidationGroup="valGroup"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="lblTimeZone" Text="Select your Time Zone" AssociatedControlID="drpTimeZone"
                                runat="server" meta:resourcekey="lblTimeZoneResource1"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="drpTimeZone"  runat="server" class="dropdownMedium" meta:resourcekey="drpTimeZoneResource1">
                             <asp:ListItem  Value="-12:00,0" Text="(-12:00) International Date Line West"></asp:ListItem>
                             <asp:ListItem  Value="-11:00,0" Text="(-11:00) Midway Island, Samoa"></asp:ListItem>
                             <asp:ListItem  Value="-10:00,0" Text="(-10:00) Hawaii"></asp:ListItem>
                             <asp:ListItem  Value="-09:00,1" Text="(-09:00) Alaska"></asp:ListItem>
                             <asp:ListItem  Value="-08:00,1" Text="(-08:00) Pacific Time (US & Canada)"></asp:ListItem>
                             <asp:ListItem  Value="-07:00,0" Text="(-07:00) Arizona"></asp:ListItem>
                             <asp:ListItem  Value="-07:00,1" Text="(-07:00) Mountain Time (US & Canada)"></asp:ListItem>
                             <asp:ListItem  Value="-06:00,0" Text="(-06:00) Central America, Saskatchewan"></asp:ListItem>
                             <asp:ListItem  Value="-06:00,1" Text="(-06:00) Central Time (US & Canada), Guadalajara, Mexico city"></asp:ListItem>
                             <asp:ListItem  Value="-05:00,0" Text="(-05:00) Indiana, Bogota, Lima, Quito, Rio Branco"></asp:ListItem>
                             <asp:ListItem  Value="-05:00,1" Text="(-05:00) Eastern time (US & Canada)"></asp:ListItem>
                             <asp:ListItem  Value="-04:00,1" Text="(-04:00) Atlantic time (Canada), Manaus, Santiago"></asp:ListItem>
                             <asp:ListItem  Value="-04:00,0" Text="(-04:00) Caracas, La Paz"></asp:ListItem>
                             <asp:ListItem  Value="-03:30,1" Text="(-03:30) Newfoundland"></asp:ListItem>
                             <asp:ListItem  Value="-03:00,1" Text="(-03:00) Greenland, Brasilia, Montevideo"></asp:ListItem>
                             <asp:ListItem  Value="-03:00,0" Text="(-03:00) Buenos Aires, Georgetown"></asp:ListItem>
                             <asp:ListItem  Value="-02:00,1" Text="(-02:00) Mid-Atlantic"></asp:ListItem>
                             <asp:ListItem  Value="-01:00,1" Text="(-01:00) Azores"></asp:ListItem>
                             <asp:ListItem  Value="-01:00,0" Text="(-01:00) Cape Verde Is."></asp:ListItem>
                             <asp:ListItem  Value="00:00,0"  Text="(00:00) Casablanca, Monrovia, Reykjavik"></asp:ListItem>
                             <asp:ListItem  Value="00:00,1"  Text="(00:00) GMT: Dublin, Edinburgh, Lisbon, London"></asp:ListItem>
                             <asp:ListItem  Value="+01:00,1" Text=" (+01:00) Amsterdam, Berlin, Rome, Vienna, Prague, Brussels"></asp:ListItem>
                             <asp:ListItem  Value="+01:00,0" Text="(+01:00) West Central Africa"></asp:ListItem>
                             <asp:ListItem  Value="+02:00,1" Text="(+02:00) Amman, Athens, Istanbul, Beirut, Cairo, Jerusalem"></asp:ListItem>
                             <asp:ListItem  Value="+02:00,0" Text="(+02:00) Harare, Pretoria"></asp:ListItem>
                             <asp:ListItem  Value="+03:00,1" Text="(+03:00) Baghdad, Moscow, St. Petersburg, Volgograd"></asp:ListItem>
                             <asp:ListItem  Value="+03:00,0" Text="(+03:00) Kuwait, Riyadh, Nairobi, Tbilisi"></asp:ListItem>
                             <asp:ListItem  Value="+03:30,0" Text="(+03:30) Tehran"></asp:ListItem>
                             <asp:ListItem  Value="+04:00,0" Text="(+04:00) Abu Dhadi, Muscat"></asp:ListItem>
                             <asp:ListItem  Value="+04:00,1" Text="(+04:00) Baku, Yerevan"></asp:ListItem>
                             <asp:ListItem  Value="+04:30,0" Text="(+04:30) Kabul"></asp:ListItem>
                             <asp:ListItem  Value="+05:00,1" Text="(+05:00) Ekaterinburg"></asp:ListItem>
                             <asp:ListItem  Value="+05:00,0" Text="(+05:00) Islamabad, Karachi, Tashkent"></asp:ListItem>
                             <asp:ListItem  Value="+05:30,0" Text="(+05:30) Chennai, Kolkata, Mumbai, New Delhi, Sri Jayawardenepura"></asp:ListItem>
                             <asp:ListItem  Value="+05:45,0" Text="(+05:45) Kathmandu"></asp:ListItem>
                             <asp:ListItem  Value="+06:00,0" Text="(+06:00) Astana, Dhaka"></asp:ListItem> 
                             <asp:ListItem  Value="+06:00,1" Text="(+06:00) Almaty, Nonosibirsk"></asp:ListItem> 
                             <asp:ListItem  Value="+06:30,0" Text="(+06:30) Yangon (Rangoon)"></asp:ListItem> 
                             <asp:ListItem  Value="+07:00,1" Text="(+07:00) Krasnoyarsk"></asp:ListItem> 
                             <asp:ListItem  Value="+07:00,0" Text="(+07:00) Bangkok, Hanoi, Jakarta"></asp:ListItem> 
                             <asp:ListItem  Value="+08:00,0" Text="(+08:00) Beijing, Hong Kong, Singapore, Taipei"></asp:ListItem> 
                             <asp:ListItem  Value="+08:00,1" Text="(+08:00) Irkutsk, Ulaan Bataar, Perth"></asp:ListItem> 
                             <asp:ListItem  Value="+09:00,1" Text="(+09:00) Yakutsk"></asp:ListItem> 
                             <asp:ListItem  Value="+09:00,0" Text="(+09:00) Seoul, Osaka, Sapporo, Tokyo"></asp:ListItem> 
                             <asp:ListItem  Value="+09:30,0" Text="(+09:30) Darwin"></asp:ListItem> 
                             <asp:ListItem  Value="+09:30,1" Text="(+09:30) Adelaide"></asp:ListItem> 
                             <asp:ListItem  Value="+10:00,0" Text="(+10:00) Brisbane, Guam, Port Moresby"></asp:ListItem> 
                             <asp:ListItem  Value="+10:00,1" Text="(+10:00) Canberra, Melbourne, Sydney, Hobart, Vladivostok"></asp:ListItem> 
                             <asp:ListItem  Value="+11:00,0" Text="(+11:00) Magadan, Solomon Is., New Caledonia"></asp:ListItem> 
                             <asp:ListItem  Value="+12:00,1" Text="(+12:00) Auckland, Wellington"></asp:ListItem> 
                             <asp:ListItem  Value="+12:00,0" Text="(+12:00) Fiji, Kamchatka, Marshall Is."></asp:ListItem> 
                             <asp:ListItem  Value="+13:00,0" Text="(+13:00) Nuku'alofa"></asp:ListItem></asp:DropDownList>
                     
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="Label1" AssociatedControlID="chkInformation" runat="server" meta:resourcekey="Label1Resource1"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkInformation" runat="server" ViewStateMode="Enabled" Text="I
                            would like to receive periodic information about Insighto"  meta:resourcekey="chkInformationResource1" />
                        </td>
                    </tr>
                    <tr>
                        <td class="wid_30">
                            <asp:Label ID="Label2" AssociatedControlID="btnSave" runat="server" meta:resourcekey="Label2Resource1"></asp:Label>
                        </td>
                        <td>
                            <asp:Button ID="btnSave" CssClass="dynamicButton" Text="Save"
                                runat="server" ToolTip="Save" OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" ValidationGroup="valGroup"/>&nbsp;&nbsp;
                                  <!--<asp:Button ID="btnBack" CssClass="dynamicButton"
                                       ToolTip="Back to My Surveys" runat="server" Text="Back to My Surveys"  meta:resourcekey="btnBackeResource1" OnClick="btnBack_Click" ValidationGroup="notValGroup" />-->
                        </td>
                    </tr>
                </table>
                <div class="clear">
                </div>
               
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <uc:ProfileRightPanel ID="ucProfileRightPanel" runat="server" />
    <div class="clear">
    </div>
       <script type="text/javascript">
           function fnOpenWindow(reqType) {
               window.open("LoginWithFavoriteAccount.aspx?key=" + reqType, null,
    "height=480,width=850,status=yes,toolbar=no,menubar=no,location=no");

           }
           function onlyNumbers(e, Type) {
               var key = window.event ? e.keyCode : e.which;
               if (Type == 'Countrycode' && key == 43) {
                   return true;
               }
               else if (key > 31 && (key < 48 || key > 57)) {
                   return false;
               }
               return true;
           }
           function onlyAlphabets(e) {
            var key = window.event ? e.keyCode : e.which;
            if ((key >= 65 && key <= 90) || (key >= 97 && key <= 122) || key == 8 || key == 32) {
                return true;
            }
            return false;
        }
    </script>
</asp:Content>
