﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using App_Code;

public partial class SendRemainderEmailList : SurveyPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (base.SurveyBasicInfoView.USERID > 0)
            {
                BinderReminderSurveys(base.SurveyBasicInfoView.USERID);
                ddlSurvey.SelectedValue = base.SurveyBasicInfoView.SURVEY_ID.ToString();
                BindReminderInvitation(base.SurveyBasicInfoView.SURVEY_ID);
            }
        }
    }

    #region "Method : BinderReminderSurveys"
    /// <summary>
    /// used to bind reminder surveys.
    /// </summary>
    /// <param name="userId"></param>

    private void BinderReminderSurveys(int userId)
    {
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var surveyremainderList = surveyService.GetSendReminderSurveys(userId);

        ddlSurvey.DataSource = surveyremainderList;
        ddlSurvey.DataTextField = "SURVEY_NAME";
        ddlSurvey.DataValueField = "SURVEY_ID";
        ddlSurvey.DataBind();
    }
    #endregion

    #region "Method : BindReminderInvitation"
    /// <summary>
    /// Used to bind Reminder Invitations.
    /// </summary>
    /// <param name="SurveyId"></param>

    private void BindReminderInvitation(int surveyId)
    {
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var surveyremainderInvitationList = surveyService.GetSendReminderSurveysInvitation(surveyId);
        lvSurveyReminders.DataSource = surveyremainderInvitationList;
        lvSurveyReminders.DataBind();
    }
    #endregion

    #region "Event : lvSurveyReminders_ItemDataBound"
    /// <summary>
    /// bind the values on row bound.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lvSurveyReminders_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;

            Label lblUNIQUERESPONDENT = e.Item.FindControl("lblUNIQUERESPONDENT") as Label;
            if (((Insighto.Business.ValueObjects.ReminderEmailEnvitation)(dataItem.DataItem)).UNIQUE_RESPONDENT == 0)
                lblUNIQUERESPONDENT.Text = "Only once"; //Utilities.ResourceMessage("SendReminder_Respondent_OnlyOnce"); //
            else if (((Insighto.Business.ValueObjects.ReminderEmailEnvitation)(dataItem.DataItem)).UNIQUE_RESPONDENT == 1)
                lblUNIQUERESPONDENT.Text ="Multiple times";// Utilities.ResourceMessage("SendReminder_Respondent_MultiTimes");//;
            else if (((Insighto.Business.ValueObjects.ReminderEmailEnvitation)(dataItem.DataItem)).UNIQUE_RESPONDENT == 2)
                lblUNIQUERESPONDENT.Text ="Only once-with password"; //Utilities.ResourceMessage("SendReminder_Respondent_Password");



            //string strReminder = "";
            //if ((((Insighto.Business.ValueObjects.ReminderEmailEnvitation)(dataItem.DataItem)).CONLIST_DELETED.ToString()) == "1")
            //{
            //    strReminder = "javascript: alert('" + Utilities.ResourceMessage("SendReminder_Alert_EmailNotExist") + "');"; //This email list does not exist.
            //}
            //else if ((((Insighto.Business.ValueObjects.ReminderEmailEnvitation)(dataItem.DataItem)).EmailCount) == 0 && (((Insighto.Business.ValueObjects.ReminderEmailEnvitation)(dataItem.DataItem)).DELETED.ToString()) == "0")
            //{
            //    strReminder = "javascript: alert('" + Utilities.ResourceMessage("SendReminder_Alert_NoContactList") + "');";//There are no contacts in this email list. Please add your contacts to this email list to send the reminder mail.
            //}
            //else
            //{
            //    strReminder = EncryptHelper.EncryptQuerystring(PathHelper.GetSendReminderMailInvitationURL(), "launchid=" + (((Insighto.Business.ValueObjects.ReminderEmailEnvitation)(dataItem.DataItem)).LAUNCH_ID).ToString() + "&" + Constants.SURVEYID + "=" + (((Insighto.Business.ValueObjects.ReminderEmailEnvitation)(dataItem.DataItem)).SURVEY_ID).ToString() + "&surveyFlag=" + base.SurveyFlag);
            //}

            //HyperLink hlnkReminder = e.Item.FindControl("hlnkReminder") as HyperLink;
            //hlnkReminder.NavigateUrl = strReminder;

        }
    }
    #endregion

    #region "Event: ddlSurvey_SelectedIndexChanged"
    /// <summary>
    /// selected event change bind email invitation info.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void ddlSurvey_SelectedIndexChanged(object sender, EventArgs e)
    {
        int iSurveyId = int.Parse(ddlSurvey.SelectedValue);
        BindReminderInvitation(iSurveyId);
    }
    #endregion
}