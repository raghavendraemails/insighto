﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true" 
    CodeFile="LaunchSurvey.aspx.cs" Inherits="Insighto.Pages.LaunchSurvey" Culture="auto" UICulture="auto" EnableEventValidation="true" %>
<%@ Register Src="~/UserControls/SurveySideNav.ascx" TagName="SurveyLeftMenu" TagPrefix="uc" %>
<%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>
<%@ Register Src="~/UserControls/HtmlEditor.ascx" TagPrefix="uc" TagName="HtmlControl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" runat="Server">
<style type="text/css">

/* Z-index of #mask must lower than #boxes .window */
#mask {
  position:absolute;
  z-index:9000;
  background-color:rgba(26, 22, 22, 0.22);
  display:none;
  top:0px;
  left:0px;
}
   
#boxes .window {
  position:absolute;
  width:330px;
  height:170px;
  display:none;
  z-index:9999;
  background-color:#ffffff;
}

/* Customize your modal window here, you can add background image too */
#boxes #dialog {
  width:330px;
  height:170px;
}
.divheader
{
   font-size: 14px;
color: #B765C8;
padding: 10px;
background-color: #F4F4F4;
}

.divmsg
{
    margin-top: 25px;
margin-left: 20px;
    line-height:18px;
    margin-bottom: 10px;
}
.twitter-share-button i
{
   background-image:none;
   color:Gray;
}
.yes
{
    margin-top:20px;
   float: left;
   width: 90px;
text-align: center;
padding: 5px;
border: 0px solid #000;
color: #FFF;
font-size: 14px;
font-weight: bold;
cursor: pointer;
background: none repeat scroll 0% 0% #FF7708;
border-radius: 5px;
}

</style>
 

 <style type="text/css">

.rbtnInternakAccessSetting input{margin-top: 1px; border: solid thin blue; float: left;}
.rbtnInternakAccessSetting label{ float: left; text-align: left;}
.rbtnlayout input{margin-top: 1px; border: solid thin blue; float: left;}
.rbtnlayout label{ float: left; text-align: left;}
.ckbhv label
{ 
    float:left;
    margin-top: 2px;
}
.methodbuttonpanel
{
    padding-bottom: 10px;
padding-top: 5px;
margin-bottom: 10px;
font-size:13px;

}
.fontbold
{
    font-weight:bold;
}
.clickhereembed
{
color: #FF9000;
font-size: 10px;
margin-left: 3px;
margin-top: 1px;
float: left;
}
.clickhere
{
color: #FF9000;
font-size: 10px;
float: left;
margin-left: 20px;
margin-top: 2px;
}
.line
{
    float: left;
margin-top: 12px;
border-top: 1px solid #E1E1E1;
width: 100%;
padding-top: 10px;
}
.lineemail
{
margin-top: 40px;
border-top: 1px solid #E1E1E1;
width: 100%;
padding-top: 10px;
}
.widthdiv
{
    width: 100%;
    float: left;
}
.widthdivpad
{
    width: 100%;
    float: left;
    padding-top:15px;
}
.widthdivweb
{
    width: 100%;
    float: left;
    margin-top:10px;
    font-size:13px;
}
.ckbhv input
{ 
    float:left;
}
.ckbblock
{
      margin-top: 9px;
}
.SurveySettings
{
    width:100%; 
    border-bottom:1px solid #E1E1E1;
    padding-bottom: 85px;
    padding-top: 5px;
    margin-bottom: 10px;
}
.floatL 
{
    float:left;
}

.congrats
{
    float: left;
    margin-top: -10px;
}
.generate
{
 margin-top: 8px;
margin-left: 44px;
}
.surveyname
{
    margin-top: 10px;
color: #1747A9;
font-weight: bold;
}
.weblinkcss
{
   margin-top: 15px;
   margin-bottom: 20px;
   border-width: 1px;
   border-color: #BF92CB;
   border-style: solid;
   box-shadow: 1px 1px 1px #BF92CB; 
   background-color: rgb(240, 240, 240);
}
.arrow_box {
position: relative;
background: #F2E5FC;
border: 1px solid #E2C9E9;
border-top-color: #E2C9E9;
border-top-style: solid;
border-top-width: 1px;
border-right-color: #E2C9E9;
border-right-style: solid;
border-right-width: 1px;
border-bottom-color: #E2C9E9;
border-bottom-style: solid;
border-bottom-width: 1px;
border-left-color: #E2C9E9;
border-left-style: solid;
border-left-width: 1px;
width: 590px;
border-radius: 3px;
box-shadow: 1px 1px 5px #CBAFE0;
}
.arrow_box:after, .arrow_box:before {
bottom: 100%;
border: 1px solid #c2e1f5;
content: " ";
height: 0;
width: 0;
position: absolute;
pointer-events: none;
left: 10%;
}

.arrow_box:after {
border-color: rgba(136, 183, 213, 0);
border-bottom-color: #F2E5FC;
border-width: 10px;
margin-left: -34px;
}

.arrow_box:before {
border-color: rgba(194, 225, 245, 0);
border-bottom-color: #E2C9E9;
border-width: 12px;
margin-left: -36px;
}

.heighttxt
{
    font-size:12px;
    width:50px;
    margin-left: 2px;
}
.recom
{
    font-size:10px;
    color: #C424A5;
margin-top: 2px;
}
.embedblockheight
{
   height: 51px;
   padding:9px;
}
.divpadding
{
    padding-top:10px;
}
.survey_access
{
    float:left;
    padding-right:15px;
    border-right:2px solid #BFBCBC;
}
.surveylink
{
    width:100%;
    margin-top: 20px;
}
.embedtextinner
{
    float:left;
    margin-left: 15px;
}
.copyembed
{
    width:100%;
    margin-top: 10px;
}
.generatetop
{
    margin-top:5px;
}
.surveytext
{
 float: left;
margin-left: 5px;
margin-top: -9px;
}
.btncopy
{
    padding:0; 
    font-weight:normal;
float: left;
margin-left: 7px;
margin-top: 7px;
}
.btncopy:hover
{
     padding:0; 
    font-weight:normal;
float: left;
margin-left: 7px;
margin-top: 7px;
}
.surveycopy
{
    float:left; 
    margin-left: 9px;
    margin-top: -6px;
}

.belowurl
{
float: left;
}
.launch_methodstext
{
    font-size:13px;
    font-weight:bold;
}
.lnkurlem
{
    font-size:13px;
    font-weight:normal;

}
.survey_layout
{
    float: left; 
    vertical-align: top;
    padding-left: 28px;
}
.access
{
    margin-left:-9px;
}
.iframemargin
{
    margin-top: 5px;
}
#popup_container h1
{
    text-align:left;
    padding-left:10px;
}

    .hidden
    {  display:none;
        }
        .show
        {
            display:block;
        }

#popup_message
{
   text-align:justify;
    padding-left:5px;
}
#popup_content
{
     width:400px;  
}
</style>
  <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
   <script type="text/javascript" src="Scripts/jquery.alerts.js"></script>


   <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true" />
    <div class="clear">
    </div>
    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1" /></div>
        </div>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblErrMsg" runat="server" Visible="False" 
                meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
    </div>
    <div class="surveyQuestionPanel">
        <!-- survey question panel -->
        <div class="surveyQuestionHeader">
            <div class="surveyQuestionTitle">
               <asp:Label ID="lblTitle" runat="server" Text="SEND SURVEY" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>
            </div>
            <div class="previewPanel">
                <asp:Button ID="btnBack" title='Back' CssClass="backbgbtn" ToolTip="Back to Pre Launch Check"
                    runat="server" OnClick="btnBack_Click" Visible="False" Text="Back" 
                    ValidationGroup="back" meta:resourcekey="btnBackResource1">
                </asp:Button>&nbsp;&nbsp;
                <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
            </div>
        </div>
        <div class="surveyQuestionTabPanel"  style="padding-left:15px;">
            <!-- survey question tabs -->
            <div id="divcredits" runat="server">
              <b>
                <asp:Label ID="Label4" runat="server" 
                    Text="WHICH SURVEY CREDIT WOULD YOU LIKE TO USE?" 
                    meta:resourcekey="lblsurveyplan"></asp:Label>
                 <span> &nbsp<a href="#" class="helpLink1"
                    title="Help" onclick="javascript: window.open('help/2_6_2.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">&nbsp;</a></span></b>

                    <div  class="chooseButtonPanel">

                  <%-- <table id="tblfreesurvey"  runat="server">
                    <tr>
                        <td>You are currently using a Free survey. <asp:HyperLink ID="Upgradelnk" onClick="_gaq.push(['_lblupgradetext1', 'Click', 'hplUpgrade1', '']);" 
                                                                    style="padding:4px 8px 4px 8px; font-size:12px;" Target="_parent"   runat="server" meta:resourcekey="hplUpgradeResource1" >Buy Pro Single</asp:HyperLink> | <asp:HyperLink ID="Upgradelnk1" onClick="_gaq.push(['_lblupgradetext1', 'Click', 'hplUpgrade1', '']);" 
                                                                    style="padding:4px 8px 4px 8px; font-size:12px;" Target="_parent"   runat="server" meta:resourcekey="hplUpgradeResource1" >Buy Premium Single</asp:HyperLink>  
                        </td>
                   </tr>
                    </table>--%>

                    <table id="tblcrediturvey"  runat="server"> 
                    <tr>
                        <td id="tdfreemsg" runat="server">Free survey</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;
                        <asp:HyperLink ID="Upgradelnk" onClick="showimage();" style="padding:4px 8px 4px 8px; font-size:12px;" Target="_parent"   runat="server" meta:resourcekey="hplUpgradeResource1" Text="Apply" ></asp:HyperLink> 
                           <img src="App_Themes/Classic/Images/icon_chk1.png" alt="" runat="server"  id="imgchkfree" style="visibility:hidden" />  
                          
                            </td>
                    </tr>   
                    <tr>
                    <td id="tdprosingle" runat="server">Pro-Single</td>
                        <td><asp:Label runat="server" ID="lblprocredits"></asp:Label></td>
                        <td>&nbsp;
                        <asp:HyperLink ID="applyprolnk" onClick="showimagepro();" style="padding:4px 8px 4px 8px; font-size:12px;" Target="_parent"   runat="server" meta:resourcekey="hplUpgradeResource1" Text="Apply" ></asp:HyperLink> 
                        <asp:HyperLink ID="buyprolnk" onClick="_gaq.push(['_lblupgradetext1', 'Click', 'hplUpgrade1', '']);"  style="padding:4px 8px 4px 8px; font-size:12px;" Target="_parent"   runat="server" meta:resourcekey="hplUpgradeResource1" >Buy - $9/Survey</asp:HyperLink> 
                           <img src="App_Themes/Classic/Images/icon_chk1.png" alt="" runat="server"  id="imgchkpro"  style="visibility:hidden" />  
                           
                            </td>
                    </tr>  
                     <tr>
                    <td id="tdpremsignle" runat="server">Premium-Single</td>
                        <td><asp:Label runat="server" ID="lblpremcredits"></asp:Label></td>
                        <td>&nbsp;
                        <asp:HyperLink ID="applypremlnk" onClick="showimageprem();" style="padding:4px 8px 4px 8px; font-size:12px;" Target="_parent"   runat="server" meta:resourcekey="hplUpgradeResource1" Text="Apply" ></asp:HyperLink> 
                        <asp:HyperLink ID="buypremlnk" onClick="_gaq.push(['_lblupgradetext1', 'Click', 'hplUpgrade1', '']);" style="padding:4px 8px 4px 8px; font-size:12px;" Target="_parent"   runat="server" meta:resourcekey="hplUpgradeResource1" >Buy - $29/Survey</asp:HyperLink> 
                           <img src="App_Themes/Classic/Images/icon_chk1.png" alt="" runat="server"  id="imgchkprem" style="visibility:hidden" />  
                          
                            </td>
                    </tr>                
            
                    </table>
                     <input type="hidden" runat="server" id="hdfreecheck"  />
                    </div>

            </div>  
                <div >
                <div class="survey_access">  
                <div>
                 <b>
                        <asp:Label ID="lblConfirmAccessSettings" runat="server" 
                            Text="Confirm survey access settings" 
                            meta:resourcekey="lblConfirmAccessSettingsResource1"></asp:Label></b>
                    </div>  
                     <div class="access">
                    <asp:Label ID="lblSaveAndCont" runat="server" Visible="False" 
                        meta:resourcekey="lblSaveAndContResource1"></asp:Label>
                    
                    <p>
                        <asp:RadioButtonList ID="rbtnInternakAccessSetting" CssClass="rbtnInternakAccessSetting"
                            runat="server" meta:resourcekey="rbtnInternakAccessSettingResource1" AutoPostBack="true"
                            onselectedindexchanged="rbtnInternakAccessSetting_SelectedIndexChanged">
                            <asp:ListItem Value="0"
                                Text="Allow only one response per computer (Unique respondent)" 
                                meta:resourcekey="ListItemResource6"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Allow multiple responses per computer" 
                                meta:resourcekey="ListItemResource7"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Attach unique password in mail" 
                                meta:resourcekey="ListItemResource8"></asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="reqInternalsettings" 
                            meta:resourcekey="Label1Resource1" />
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqInternalsettings" runat="server"
                            ControlToValidate="rbtnInternakAccessSetting" ValidationGroup="notval" CssClass="lblRequired"
                            Display="Dynamic" meta:resourcekey="rbtnlistAccessSettingsReqResource1"></asp:RequiredFieldValidator>
                    </p>
                </div></div>
                <div class="survey_layout">
             <b>
              <asp:Label ID="lblLayoutSettings" runat="server" Text="Survey layout settings" 
                        meta:resourcekey="lblLayoutSettingsResource1"></asp:Label></b><div id="divLayout" class="access" runat="server" >
              <p><asp:RadioButtonList ID="rbtnLayout" CssClass="rbtnlayout" AutoPostBack="true" runat="server" 
                    meta:resourcekey="rbtnLayoutResource1" 
                      onselectedindexchanged="rbtnLayout_SelectedIndexChanged">
                </asp:RadioButtonList></p>
            </div> </div></div>
          
                       <div class="clear"><asp:hiddenfield runat="server" id="hdnTestValue" />
                       <asp:hiddenfield runat="server" id="hdnFbUrl" />
                           <asp:hiddenfield runat="server" id="hdnSurveyId" />
                           <asp:hiddenfield runat="server" id="hdnlayout" />
                           <asp:hiddenfield runat="server" id="hdnaccess" />
                            <asp:hiddenfield runat="server" id="hdnSurveyName" />
                             <asp:hiddenfield runat="server" id="hdnIntoNew" />
                           </div>
                       <div class="clear"></div>
                       <div class="clear"></div>
                <div class="chooseButtonPanel webLinkSystem" id="webLinkSystem" runat="server">

                

            </div>   
            
            <div>
            <div class="surveypage_lbl" >     
            <h3>
                <asp:Label ID="lblSurveyQ1" runat="server" 
                    Text="HOW WOULD YOU LIKE TO SEND YOUR SURVEY?" 
                    meta:resourcekey="lblSurveyQ1Resource1"></asp:Label>
               <a href="#" class="helpLink1"
                    title="Help" onclick="javascript: window.open('help/2_6_2.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">&nbsp;</a></h3></div>
            <div class="methodbuttonpanel">
            <div class="launch_methodstext">
                         <a href="#dialog" name="modal" runat="server" id="btnLaunchWeb1">Launch through Web link</a>
                         <asp:LinkButton ID="lnkWeblaunchother" Text="Launch through Web link" runat="server" 
                         OnClick="btnLaunchWeb_Click" CausesValidation="false" >Launch through Web link</asp:LinkButton>
                 </div>
                 
                <div id="divWebLaunch" style="width:100%" runat="server"> 
                 <asp:Label ID="lblWebLaunchAlready" runat="server" CssClass="launch_methodstext floatL" ForeColor="#AA77B7" Visible="false" Text="Launch through Web link"></asp:Label>
                  <asp:LinkButton ID="lnkSurveyUrl" Visible="false"  ForeColor="#FF9000" CssClass="clickhere"
                        Text="Click here to view survey link" runat="server" CausesValidation="false" 
                        onclick="lblSurveyUrl_Click" ></asp:LinkButton>
                </div> 

                 
                <div id="web" runat="server" style="display:none" class="widthdivpad">
           <div class="surveyCreateTextPanel arrow_box" id="divExternalMessage" runat="server">
                <div class="congrats"><h1 >
                     <asp:Label ID="lblCongrats" ForeColor="#F023C6" runat="server" Text="Congratulations!" 
                         meta:resourcekey="lblCongratsResource1"></asp:Label></h1>
                </div>
                <div class="clear"></div>
                     <div class="surveyname">Your survey <asp:Label ID="lblSurveyName" runat="server" Text="Label"></asp:Label> has been launched successfully. 
                </div>
                <div class="clear"></div>
               <div class="surveylink">
                    <div class="floatL">
                        <asp:Label ID="lblSurveyLive" runat="server" 
                            Text="Your survey is now live at the link :" 
                            meta:resourcekey="lblSurveyLiveResource1"></asp:Label>
                    </div> 
                 <div class="surveyUrlCopyPanel surveytext">
                       <asp:TextBox ID="txtWebLink" text-align="top" runat="server" Width="200" CssClass="textBoxMedium txtWebLink" ReadOnly="True" meta:resourcekey="txtUrlResource1"></asp:TextBox> 
                       <a id="btnCopy" class="btnCopy" href="javascript:void(0);" style="padding:0px; font-weight:normal" runat="server">Copy</a>
                 
                </div>
            </div> 
            <div class="belowurl"><asp:Label ID="lblCopyUrl" runat="server" 
                        Text="Copy&nbsp;the&nbsp;URL&nbsp;to&nbsp;anywhere&nbsp;you&nbsp;want&nbsp;to&nbsp;show&nbsp;the&nbsp;survey." 
                        meta:resourcekey="lblCopyUrlResource1"></asp:Label></div>
            <div class="clear">
            </div>
        </div>
                </div>
                <div class="clear"></div>
                <div class="widthdivweb">
            <asp:LinkButton ID="btnLaunchEmail" CssClass="fontbold" Text="Launch via Email through Insighto" 
                    runat="server" OnClick="btnLaunchEmail_Click" CausesValidation="false">Launch via Email through Insighto</asp:LinkButton>
            </div>
               <div id="divSurveyLable" class="widthdivpad" runat="server" style="display:none"> 
                     <div class="surveyCreateTextPanel arrow_box">
                       <!-- survey created content panel -->
                          <div class="congrats"><h1 >
                          <asp:Label ID="lblCongrates" runat="server" ForeColor="#F023C6" Text="Congratulations!" ></asp:Label></h1></div>
                          <div class="clear"></div>
                          <div class="surveyname">Your survey <asp:Label ID="lblSurveyName2" runat="server" Text="Label"></asp:Label> has been launched successfully. </div>
                          <div class="clear"> </div>
                          <div class="divpadding"><asp:Label ID="lblSurveyLable" runat="server"
                                meta:resourcekey="lblSurveyLableResource1" /></div>
                    </div>
              </div>
                <div class="chooseButtonPanel internalmailingSystem" id="internalmailingSystem" runat="server">
                <div class="lineemail"></div>
                <div class="stepsPanel">
            <asp:Label ID="lblStep1" CssClass="whiteColor" runat="server" Text="Step 1" 
                        meta:resourcekey="lblStep1Resource1"></asp:Label> 
                    <asp:Label ID="lblChooseEmail" runat="server" Text="Choose email list" 
                        meta:resourcekey="lblChooseEmailResource1"></asp:Label>
                </div>
                
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblSelectEmail" runat="server" Text="Select Email List" 
                            meta:resourcekey="lblSelectEmailResource1"></asp:Label>
                    </div>
                    <div class="loginControls">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlEmailList" runat="server" 
                                        class="dropdownMedium ddlEmailList" meta:resourcekey="ddlEmailListResource1">
                                        <asp:ListItem meta:resourcekey="ListItemResource5">Choose Email List</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="space">
                                </td>
                                <td>
                                    <asp:Button ID="btnAddEmail" runat="server" CssClass="dynamicButton btnAddEmail"
                                        Text="Add / Edit Email List" ToolTip="Add" OnClick="btnAddEmail_Click" 
                                        ValidationGroup="addemail"  />
                                </td>
                                <td class="space">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:Label ID="lblReqEmailList" runat="server" 
                            AssociatedControlID="reqEmaillist" 
                            meta:resourcekey="lblReqEmailListResource2" />
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqEmaillist" CssClass="lblRequired"
                            ControlToValidate="ddlEmailList" meta:resourcekey="lblReqEmailListResource1"
                            runat="server" Display="Dynamic" ValidationGroup="notval"></asp:RequiredFieldValidator></div>
                </div>
                <div class="clear">
                </div>
                <div class="stepsPanel">
                    <asp:Label ID="lblStep2" CssClass="whiteColor" runat="server" Text="Step 1" 
                        meta:resourcekey="lblStep2Resource1"></asp:Label>
                    <asp:Label ID="lblSurveyInvitation" runat="server" 
                        Text="Create survey invitation" meta:resourcekey="lblSurveyInvitationResource1"></asp:Label>
                </div>
                <div>
                    <div class="con_login_pnl">
                        <div class="loginlbl">
                            <asp:Label ID="lblSubject" runat="server" AssociatedControlID="txtSubject" meta:resourcekey="lblSubjectResource1"></asp:Label>
                        </div>
                        <div class="loginControls">
                            <asp:TextBox ID="txtSubject" runat="server" MaxLength="100" 
                                CssClass="textBoxMedium" meta:resourcekey="txtSubjectResource1"></asp:TextBox>
                        </div>
                    </div>
                    <div class="con_login_pnl">
                        <div class="reqlbl btmpadding">
                            <asp:Label ID="lblreqSubject" runat="server" AssociatedControlID="reqSubject" 
                                meta:resourcekey="lblreqSubjectResource1" />
                            <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqSubject" CssClass="lblRequired"
                                ControlToValidate="txtSubject" runat="server"
                                Display="Dynamic" ValidationGroup="notval" 
                                meta:resourcekey="reqSubjectResource2"></asp:RequiredFieldValidator></div>
                    </div>
                    <p>&nbsp;</p>
                    <p>
                        <b>
                        <asp:Label ID="lblInvMail" runat="server" Text="Survey Invitation Email" 
                            meta:resourcekey="lblInvMailResource1"></asp:Label></b></p>
                        <p class="defaultHeight8px"></p>
                    <p style="text-align:justify;">
                        <span class="purpleColor" style="font-weight:bold;">
                            <asp:Label ID="lblEdit" runat="server" Text="Edit / Personalize:" 
                            meta:resourcekey="lblEditResource1"></asp:Label></span>
                        <asp:Label ID="lblEditPersonalText" runat="server" Text=" Given below is a default text
                        of your email invitation. You can edit the same and write your own invite. You can
                        also copy the introduction you created for the survey as your email invite. Insighto
                        will automatically pick up the first and last names from your email list, if you
                        choose to retain the default salutation." 
                            meta:resourcekey="lblEditPersonalTextResource1"></asp:Label>
                    </p>
                    <p class="defaultHeight8px"></p>
                   <p style="text-align:justify;">
                        <span class="purpleColor" style="font-weight:bold;">
                            <asp:Label ID="lblLink" runat="server" Text="Survey Link / URL:" 
                            meta:resourcekey="lblLinkResource1"></asp:Label></span> 
                       <asp:Label ID="lblSurveyLinkURL" runat="server" Text="The link / URL of the survey
                        created would be automatically appended at the end of the invitation. You can decide
                        to change this location, by inserting the link at your place of choice." 
                            meta:resourcekey="lblSurveyLinkURLResource1"></asp:Label>
                    </p>
                    <p class="defaultHeight8px"></p>
                     
                    
                    <div>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left" style="width:50%">
                                            <asp:LinkButton ID="lnkCopyintro" runat="server" OnClick="lnkCopyintro_Click" Text="Copy intro"
                            OnClientClick="Javascript:hidevalidation();" ToolTip="Copy intro" ValidationGroup="notval" 
                                                    meta:resourcekey="lnkCopyintroResource1"></asp:LinkButton>
                                            </td>
                                            <td align="right" style="width:50%">
                                            <asp:LinkButton ID="lnkReset" runat="server" OnClientClick="Javascript:hidevalidation();"
                            OnClick="lnkReset_Click" Text="Restore default." ToolTip="Restore default." 
                                                    meta:resourcekey="lnkResetResource1" ValidationGroup="notval"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                 <p>
                        <input type="hidden" runat="server" id="EditorQuesIntroParams" value="0" />
                        <textarea id="EditorQuestionIntro" runat="server" cols="10" rows="8" class="mceEditor EditorQuestionIntro"
                            style="width: 640px; height: 150px;" ></textarea>
                        <asp:RequiredFieldValidator ID="rqEditorQuestionIntro" runat="server"
                            ErrorMessage="Insert text to preview email invitation" CssClass="lblRequired" Display="Dynamic"
                            ControlToValidate="EditorQuestionIntro" 
                                         ></asp:RequiredFieldValidator>
                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left" style="width:33%">
                                            <asp:HyperLink ID="hlnkInsightoSurveyLink" runat="server" title='Insert survey link'
                            onclick="javascript:return InsertSurveyLink();" ToolTip="Insert survey sink" 
                                                    meta:resourcekey="hlnkInsightoSurveyLinkResource1">Insert survey link</asp:HyperLink>
                                            
                                            </td>
                                            <td align="center" style="width:33%">
                                             
                                                <asp:CheckBox ID="imagechk" runat="server"  Text="Show logo in email invite" style="color:#853C98; font-family:Arial,sans-serif;"
                            ToolTip="Logo should have been uploaded" oncheckedchanged="imagechk_CheckedChanged"  AutoPostBack="true" />
                                            <%--<asp:LinkButton ID="LinkButton1" runat="server" OnClick="lnkPreview_Click" Text="Preview email invitation"
                            ToolTip="Preview email invitation" meta:resourcekey="lnkPreviewResource1"></asp:LinkButton>--%>
                                            </td>
                                            <td align="right" style="width:33%">
                                            <asp:LinkButton ID="lnkPreview" runat="server" OnClick="lnkPreview_Click" Text="Preview email invitation"
                            ToolTip="Preview email invitation" meta:resourcekey="lnkPreviewResource1"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                   
                  <p>&nbsp;</p>
                </div>
                <div class="clear">
                </div>
                <div class="stepsPanel">
                    <asp:Label ID="lblStep3" CssClass="whiteColor" runat="server" Text="Step 3" 
                        meta:resourcekey="lblStep3Resource1"></asp:Label>
                    <asp:Label ID="lblSenderDetails" runat="server" Text="Fill sender details" 
                        meta:resourcekey="lblSenderDetailsResource1"></asp:Label>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblSenderName" runat="server" AssociatedControlID="txtSenderName"
                            meta:resourcekey="lblSenderNameResource1"></asp:Label>
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtSenderName" runat="server" MaxLength="50" 
                            CssClass="textBoxMedium" meta:resourcekey="txtSenderNameResource1"></asp:TextBox>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl con_padding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqName" CssClass="lblRequired"
                            ControlToValidate="txtSenderName" meta:resourcekey="lblReqNameResource1" runat="server"
                            Display="Dynamic" ValidationGroup="notval"></asp:RequiredFieldValidator></div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblSendersEmail" runat="server" AssociatedControlID="txtSenderEmail"
                            meta:resourcekey="lblSenderEmailResource1"></asp:Label></div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtSenderEmail" runat="server" MaxLength="150" 
                            CssClass="textBoxMedium" meta:resourcekey="txtSenderEmailResource1"></asp:TextBox></div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl con_padding">
                        <asp:Label ID="lblReqSenderEmail" runat="server" 
                            AssociatedControlID="reqSenderEmail" 
                            meta:resourcekey="lblReqSenderEmailResource2" />
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqSenderEmail" CssClass="lblRequired"
                            ControlToValidate="txtSenderEmail" meta:resourcekey="lblReqSenderEmailResource1"
                            runat="server" Display="Dynamic" ValidationGroup="notval"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator SetFocusOnError="True" ID="regSenderEmail" CssClass="lblRequired"
                            runat="server" ControlToValidate="txtSenderEmail" meta:resourcekey="lblRegSenderEmailResource1"
                            Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="notval"></asp:RegularExpressionValidator></div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblReplyTo" runat="server" AssociatedControlID="txtReplyTo" meta:resourcekey="lblReplytoResource1"></asp:Label></div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtReplyTo" runat="server" MaxLength="150" 
                            CssClass="textBoxMedium" meta:resourcekey="txtReplyToResource1"></asp:TextBox></div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl con_padding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqReplyTo" CssClass="lblRequired"
                            ControlToValidate="txtReplyTo" meta:resourcekey="lblReqReplytoResource1" runat="server"
                            Display="Dynamic" ValidationGroup="notval"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator SetFocusOnError="True" ID="regReplyto" CssClass="lblRequired"
                            runat="server" ControlToValidate="txtReplyTo" meta:resourcekey="lblregReplytoResource1"
                            Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="notval"></asp:RegularExpressionValidator></div>
                </div >
                <div style="float:right">
                <asp:Button
                            ID="btnEmailLaunch" Text="Launch" runat="server" CssClass="dynamicButton" 
                           ValidationGroup="validate" OnClick="btnEmailLaunch_Click" /></div> 
                <div class="clear">
                </div>
                <div class="informationPanelDefault internalNote" style="display: none;" id="internalNote" runat="server">
                    <div>
                        <asp:Label runat="server" Text="Your selection has activated the <b>'Auto Save'</b> feature which will enable your survey respondents to resume their survey response from the point they left off- even after exiting it midway through."
                            ID="lblInternalNote" meta:resourcekey="lblInternalNoteResource1" /></div>
                </div>
            </div>
                 
                <div class="widthdivweb">
                <div class="floatL launch_methodstext"><asp:LinkButton ID="btnLaunchEmbed" Text="Launch through Embed" runat="server" 
                         OnClick="btnLaunchEmbed_Click" CausesValidation="false" >Launch through Embed</asp:LinkButton></div> 
                         <asp:Label ID="lblEmbedLaunchedAlready" ForeColor="#AA77B7" CssClass="launch_methodstext floatL" Visible="false" runat="server" Text="Launch through Embed"></asp:Label><span> 
                         <a href="#" class="helpLink1 floatL" style="margin-left: 5px;"
                    title="Launch Embed" onclick="javascript: window.open('help/embedhelp.htm','Launch Embed','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">&nbsp;</a></span>  
                 <asp:LinkButton ID="lnkEmbedCode"  CssClass="clickhereembed" ForeColor="#FF9000" Visible="false" 
                        Text="Click here to view embed code" runat="server" CausesValidation="false" 
                        onclick="lblSurveyUrl_Click" ></asp:LinkButton>
               </div>
                 <div id="Embed" runat="server" style="display:none" class="widthdivpad">
                   <div class="arrow_box embedblockheight">
                     <div class="surveyUrlTextPanel generatetop">
                         <div class="floatL">
                            Width <asp:TextBox ID="txtWidthIframe" CssClass="heighttxt" Text="1000" Width="40" runat="server"></asp:TextBox> px
                         </div> 
                         <div class="embedtextinner">
                           Height <asp:TextBox ID="txtHeightIframe" CssClass="heighttxt" Text="900" Width="40" runat="server"></asp:TextBox> px
                         </div>
                         <div class="clear"></div>
                         <div class="recom">
                           * Recommended widthXheight in px: 1000X900
                         </div>
                     </div>
                    <asp:Button ID="btnGenerateIframe" runat="server" CssClass="dynamicButton generate" CausesValidation="false" Text="Generate Code" OnClick="btnGenerateIframe_click"  />
                   </div>
                </div>
                <div id="iframealert" class="widthdivpad" runat="server"> 
                <div class="surveyCreateTextPanel arrow_box">
                        <div class="congrats"><h1><asp:Label ID="lblEmbedLaunch" ForeColor="#F023C6" runat="server" Text="Congratulations!" ></asp:Label></h1></div>
                        <div class="clear"></div>
                        <div  class="surveyname">Your survey <asp:Label ID="lblSurveyName1" runat="server" Text="Label"></asp:Label> has been launched successfully. </div>
                        <div class="clear"></div>
                        <div class="copyembed">Copy the below code and place it in your website</div>
                        <div class="clear"></div>
                        <div class="iframemargin"><asp:TextBox ID="txtIframe" Width="400" Height="80" CssClass="iframe" runat="server" TextMode="MultiLine"></asp:TextBox> 
                        <a id="btncopy1" class="btnCopy1" href="javascript:void(0);" style="padding:0px; font-weight:normal" runat="server">Copy</a>
                 </div>
                </div></div>
                    <div class="clear"></div>
                       <div class="widthdivweb">
                                 <div class="floatL launch_methodstext"><asp:LinkButton ID="lnkFb" Text="Launch through Embed" runat="server" CausesValidation="false" onclick="lnkFb_Click" >Launch through Facebook</asp:LinkButton>
                                 </div> 
                                <asp:Label ID="lblFb" ForeColor="#AA77B7" CssClass="launch_methodstext floatL" Visible="false" runat="server" Text="Launch through Facebook"></asp:Label>  
                       </div>
                       <div id="divfbsuccess" class="widthdivpad" style="display:none" > 
                <div class="surveyCreateTextPanel arrow_box">
                        <div class="congrats"><h1><asp:Label ID="lblCong" ForeColor="#F023C6" runat="server" Text="Congratulations!" ></asp:Label></h1></div>
                        <div class="clear"></div>
                        <div  class="surveyname">Your survey <asp:Label ID="lblSurveyfb" runat="server"></asp:Label> has been launched successfully through Facebook. </div>
                        <div class="clear"></div>
                </div></div>
        
                                   <div class="clear"></div>
                       <div class="widthdivweb " id="TweetBox">
                       <div class="clear"></div>
                                 <div class="floatL launch_methodstext" id="twitteruser"><a onclick="clicked_twitter_button(this);" href="#" runat="server" id="twitter"  data-url="false" data-size="large" data-count="none" data-dnt="true" >Launch through Twitter</a>
                                 </div> 
                       </div>
                       <div class="clear"></div>
                <div id="divtwittersuccess" class="widthdivpad" style="display:none" > 
                <div class="surveyCreateTextPanel arrow_box">
                        <div class="congrats"><h1><asp:Label ID="lblTwitterCon" ForeColor="#F023C6" runat="server" Text="Congratulations!" ></asp:Label></h1></div>
                        <div class="clear"></div>
                        <div  class="surveyname">Your survey <asp:Label ID="lblSurveyTwName" runat="server"></asp:Label> has been launched successfully through Twitter. </div>
                        <div class="clear"></div>
                </div></div>
                <div class="clear"></div>
<%--                <div  class="launch_methodstext widthdivweb" ><a href="#" id="shareAPI">Launch through Linkedin</a>
                <script type="IN/Share" data-url="http://www.insighto.com" data-onsuccess="test" data-onError="test"></script>
                <script type="text/javascript">
                         function test() {
                        alert('shared'); 
                  }</script>
                </div>
                <div class="clear"></div>
                 <div id="divLinkedinsuccess" class="widthdivpad" style="display:none" > 
                <div class="surveyCreateTextPanel arrow_box">
                        <div class="congrats"><h1><asp:Label ID="lblLinkedinCon" ForeColor="#F023C6" runat="server" Text="Congratulations!" ></asp:Label></h1></div>
                        <div class="clear"></div>
                        <div  class="surveyname">Your survey <asp:Label ID="lblSurveyLnName" runat="server"></asp:Label> has been launched successfully through Linkedin. </div>
                        <div class="clear"></div>
                </div></div>--%>
                <asp:RadioButton ID="rbtnWidgetLaunch" runat="server" Checked="True" Text="Create Widget"
                    ToolTip="Create Widget" Visible="False" 
                    meta:resourcekey="rbtnWidgetLaunchResource1" />
            </div>
            </div>
            
            
            <div class="informationPanelDefault externalNote" style="display: none;" id="externalNote"
                runat="server">
                <div>
                    <asp:Label runat="server" Text="Your selection has activated the <b>'Auto Save'</b> feature which will enable your survey respondents to resume their survey response from the point they left off- even after exiting it midway through, if they use the same computer. If they use a different computer to revisit the survey, they would be treated as new respondents."
                        ID="lblExternalNote" meta:resourcekey="lblExternalNoteResource1" /></div>
            </div>

            <div runat="server" id="dvtelesurvey" class="loginControls" visible="false">
            
            <table style="background-color:#B72EA4; width: 100%;" >
            <tr>
                <td align="left">
                    <asp:Label ID="Label2" runat="server" Text="Question and Answer Options" Font-Bold="true" ForeColor="Azure" Font-Size="Large"></asp:Label>
                </td>
            </tr>
          
        </table>
          <asp:Table ID="Table1" runat="server" Width="100%">

        <asp:TableRow Height="50px" >
        <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
        <asp:TableCell Width="90%">

         <table width="100%" >
        <tr>
        <td width="50%" align="left"><asp:Label ID="lblintroduction" Text="Survey Introduction:" runat="server" Font-Bold="true" Font-Size="Medium" ></asp:Label></td>
        <td width="50%">&nbsp;</td>
        </tr>
        
        <tr>
        <td width="50%" align="left"> 
        <asp:Label ID="lblintro" runat="server"  ></asp:Label>
        </td>
        <td width="50%" valign="middle" align="right">
        &nbsp; <asp:FileUpload ID="fileuploadforintro" runat="server"   Height="25px"  />
        <asp:HyperLink ID="lisintroaudio" runat="server"  ></asp:HyperLink>
        
          <%-- <asp:LinkButton ID="lisintroaudio" runat="server"  onclick="lnkaudioIntro_click"  ></asp:LinkButton> --%>
        
        </td>
        </tr>
        <tr>
        <td width="50%">&nbsp;</td>
        <td width="50%" align="right"><asp:Button ID="btnintrosurvey" runat="server" CssClass="dynamicButtonSmall"
                                        Text="Upload" ToolTip="Upload" 
                                        ValidationGroup="addemail" 
                onclick="btnintrosurvey_Click"/>&nbsp;</td>
        </tr>
        </table>
         </asp:TableCell>
         <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
        </asp:TableRow>
        </asp:Table>
        <asp:Table ID="Table2" runat="server" Width="100%">
            <asp:TableRow Height="50px" >
            <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
                <asp:TableCell Width="90%">

                    <asp:GridView AutoGenerateColumns="false" AllowPaging="false" AllowSorting="true"  
                                  HeaderStyle-Height="25px" HeaderStyle-Wrap="true"
                                  runat="server" ID="grdSurveyQA" onrowdatabound="grdSurveyQA_RowDataBound" 
                                  SelectedRowStyle-BackColor="BlanchedAlmond" 
                                  onrowupdating="grdSurveyQA_RowUpdating" 
                                  onselectedindexchanged="grdSurveyQA_SelectedIndexChanged"  
                                  OnRowCommand="grdSurveyQA_RowCommand"
                                  DataKeyNames="QuestionID,Audiofilename" Visible="true" >
           
               
                    <Columns >
                            <asp:BoundField HeaderText="Sno" DataField="RowNumber" />
                            <asp:BoundField HeaderText="QuestionID" DataField="QuestionID" Visible="false" />
                            <asp:BoundField HeaderText="Question" DataField="QuestionLabel" Visible="false" />
                            <asp:BoundField HeaderText="Audiofile" DataField="Audiofilename"  Visible="false"/>

                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="QID" runat="server" Text="<%# Bind('QuestionID') %>"></asp:Label>
                                    <asp:Label ID="AudioID" runat="server" Text="<%# Bind('Audiofilename') %>"></asp:Label>
                                </ItemTemplate> 
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Question & Answers">
                                <ItemTemplate>

                               

                                 
                                    <asp:Label ID="lblquestion" runat="server" Text="<%# Bind('QuestionLabel') %>"  ></asp:Label>&nbsp;
                                    <a href="javascript:SwitchImages('div<%# Eval("QuestionID")%>');">                              
                                    <img id="imgdiv<%# Eval("QuestionID")%>" alt="" border="0" src="../images/plus.png" width="10px" height="10px" />
                                  </a>  
                            
                                <asp:TableRow>
                              
                                    <asp:TableCell>
                                        <div id="div<%# Eval("QuestionID") %>" class="GDiv" style="display:none;" >
                                       <table>
                                       <tr>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>
                                       
                                      
                                        <asp:GridView AutoGenerateColumns="false" AllowPaging="false" AllowSorting="true" runat="server" ID="grdQAnsOption">  
                                            <RowStyle BackColor="#A1BDF0" />
                                            <FooterStyle BackColor="#507CD1"  ForeColor="White" />        
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />        
                                            <HeaderStyle BackColor="#100770"  ForeColor="White" />        
                                            <AlternatingRowStyle BackColor="#E1F0EF" />
                                            <Columns>
                                                <asp:BoundField HeaderText="Sno" DataField="AnsRowNumber" ControlStyle-Width="10px" ControlStyle-Height="50px" />
                                                <asp:BoundField HeaderText="AnswerID" DataField="AnswerID" Visible="false" />
                                                <asp:BoundField HeaderText="Answeroptions" DataField="AnswerOptions" ControlStyle-Width="50px" ControlStyle-Height="50px" />
                                            </Columns>
                                        </asp:GridView>


                                         </td>
                                       </tr>
                                       </table>
                                        </div>
                                    </asp:TableCell>
                                   
                                </asp:TableRow>
                             
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AudioFilePath" >
                                <ItemTemplate>
                                    <asp:FileUpload ID="fileupload1" runat="server" Visible="false" Height="25px" />
                                   <%-- <asp:ImageButton ID="btnupload" runat="server"   CommandName="InsertRecord" ImageUrl="../Images/upload.png" Width="20px" Height="20px" 
                                     onrowcommand="grdSurveyQA_RowCommand" Visible="false" />--%>
                                    <%--<asp:LinkButton ID="lisaudio" runat="server" Visible="false" Text="<%# Bind('Audiofilename') %>"  OnClick="lnkaudio_click"  ></asp:LinkButton>--%>
                                    <asp:HyperLink ID="lisaudio" runat="server" Visible="false"  Text="AudioQuestion" NavigateUrl="<%# Bind('Audiofilename') %>" ></asp:HyperLink>
                                    
                                </ItemTemplate>
                            </asp:TemplateField>
                    </Columns>

       <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#E6CD80" Font-Bold="True" ForeColor="brown" />
            <PagerStyle BackColor="#284775" ForeColor="brown" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F4C0" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                 <AlternatingRowStyle BackColor="#DDC5E6" />

    </asp:GridView>
   
      </asp:TableCell>
       <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
        </asp:TableRow>
         </asp:Table>

         <table width="100%"><tr>
         <td width="5%">&nbsp;</td>         
         <td  width="90%" align="right">
          <asp:Button ID="btnsurveygrid" runat="server" CssClass="dynamicButtonSmall"
                                        Text="UploadAll  " ToolTip="UploadAll" 
                                        ValidationGroup="addemail" 
                 onclick="btnsurveygrid_Click" Width="60px" 
                   />

        
         </td>
         <td width="5%">&nbsp;</td>
         </tr></table>


         <asp:Table runat="server">

        <asp:TableRow Height="50px">
        <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
        <asp:TableCell Width="90%">

        <table width="100%">
        <tr>
        <td width="50%" align="left"><asp:Label ID="lblend" Text="Survey End Message:" runat="server"  Font-Bold="true" Font-Size="Medium"></asp:Label>
         &nbsp;
        </td>
        <td width="50%">&nbsp;</td>
        </tr>
        <tr>
        <td width="50%" align="left">
        <asp:Label ID="lblthanks" runat="server"  ></asp:Label>
        </td>
        <td width="50%" align="right"> 
        
         <asp:FileUpload ID="fileuploadthanks" runat="server" Height="25px" />
      <%--   <asp:ImageButton ID="btnuploadthanks" runat="server"  OnClick="btnuploadthanks_Click" ImageUrl="../Images/upload.png" Width="20px" Height="20px"  />--%>
      <%-- <asp:LinkButton ID="lnkbtnthanks" runat="server"  onclick="lnkaudiothanks_click"  ></asp:LinkButton> --%>
       <asp:HyperLink ID="lnkbtnthanks" runat="server"></asp:HyperLink>
        </td>
        </tr>
        <tr>
        <td width="50%">&nbsp;</td>
        <td width="50%" align="right"> <asp:Button ID="btnThanksSurvey" runat="server" CssClass="dynamicButtonSmall"
                                        Text="Upload" ToolTip="Upload" 
                                        ValidationGroup="addemail" 
                    onclick="btnThanksSurvey_Click" /></td>
        </tr>

        </table>      
                
         </asp:TableCell>
        <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
        </asp:TableRow>

        </asp:Table>


         <table width="100%">
        <tr>
        <td width="5%">&nbsp;</td>
        <td width="90%">
        <table width="100%">
                <tr>
         <td width="50%" align="left">
        <asp:Label ID="lblNoResponse" runat="server"  Text="No Response" ></asp:Label>
        </td>
        <td width="50%" align="right"> 
        
         <asp:FileUpload ID="fileuploadnoresponse" runat="server" Height="25px" />
           <asp:HyperLink ID="lnkbtnnoresponse" runat="server"></asp:HyperLink>
        </td>
        </tr>
        <tr>
        <td width="50%">&nbsp;</td>
        <td width="50%" align="right"> <asp:Button ID="btnnoresponse" runat="server" CssClass="dynamicButtonSmall"
                                        Text="Upload" ToolTip="Upload" 
                                        ValidationGroup="addemail" 
                onclick="btnnoresponse_Click" /></td>
        </tr>
        </table>
        </td>
        <td width="5%">&nbsp;</td>
        </tr>
        </table>

        <table width="100%">
        <tr>
        <td width="5%">&nbsp;</td>
        <td width="90%">
          <table width="100%">
                <tr>
         <td width="50%" align="left">
        <asp:Label ID="lblinvalidentry" runat="server"  Text="Invalid Entry" ></asp:Label>
        </td>
        <td width="50%" align="right"> 
        
         <asp:FileUpload ID="fileuploadinvalidentry" runat="server" Height="25px" />
           <asp:HyperLink ID="lnkbtninvalidentry" runat="server"></asp:HyperLink>
        </td>
        </tr>
        <tr>
        <td width="50%">&nbsp;</td>
        <td width="50%" align="right"> 
            <asp:Button ID="btninvalidentry" runat="server" CssClass="dynamicButtonSmall"
                                        Text="Upload" ToolTip="Upload" 
                                        ValidationGroup="addemail" onclick="btninvalidentry_Click" 
                 /></td>
        </tr>
        </table>
        </td>
        <td width="5%">&nbsp;</td>
        </tr>
        
        </table>

        <table style="background-color:#B72EA4; width: 100%;" >
            <tr>
                <td align="left">
                    <asp:Label ID="Label3" runat="server" Text="Phone Information" Font-Bold="true" ForeColor="Azure" Font-Size="Large"></asp:Label>
                </td>
            </tr>
        </table>

        <table width="100%" align="center">
         <tr>
     <td width="25%">&nbsp;</td>
     <td width="25%" align="right">Voice record duration in seconds:</td>
     <td width="25%" align="left"><asp:TextBox ID="txtmaxduration" runat="server"></asp:TextBox></td>
     <td width="25%">&nbsp;</td>
     </tr>
     <tr>
     <td width="25%">&nbsp;</td>
     <td width="25%" align="right">Toll free number:</td>
     <td width="25%" align="left"><asp:TextBox ID="txttollfreenumber" runat="server"></asp:TextBox></td>
     <td width="25%">&nbsp;</td>
     </tr>
     <tr>
     <td width="25%">&nbsp;</td>
     <td width="25%" align="right">To call numbers:</td>
     <td width="25%" align="left"><asp:TextBox ID="txtkookoonumber" runat="server"></asp:TextBox></td>
     <td width="25%">&nbsp;</td>
     </tr>

     <tr>
     <td width="25%">&nbsp;</td>
     <td width="25%" align="right">Path of wav files</td>
     <td width="25%" align="left"><asp:TextBox ID="txtkookoopath" runat="server"></asp:TextBox></td>
     <td width="25%">&nbsp;</td>
     </tr>


      <tr>
     <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
     <td width="25%" align="left">
     
     <asp:Button ID="SurveyKookooInfo" runat="server"  
             Text="SavePhoneInfo" CssClass="dynamicButtonSmall"
                                      ToolTip="Upload"  ValidationGroup="addemail"                                       
             onclick="SurveyKookooInfo_Click"  /> </td>
     
     </tr>
     </table>
       </div>

     <p><center>
                <b><asp:Label ID="lblReadyLaunch" Visible="false" runat="server" 
                    Text="You are now ready to launch your survey" 
                    meta:resourcekey="lblReadyLaunchResource1"></asp:Label></b>
                    </center></p>
             <div class="line">
             <div class="widthdiv" id="prelabtn" runat="server"><asp:Button ID="btnPreLaunch" runat="server" CssClass="Prelaunch_small" meta:resourcekey="btnPreLaunchResource1"  OnClick="btnPreLaunch_Click" ValidationGroup="validate" />
                &nbsp;&nbsp;<asp:Button ID="btnLaunchLater" runat="server" CssClass="dynamicButton" meta:resourcekey="btnLaunchLaterResource1" OnClick="btnLaunchLater_Click" ValidationGroup="validate" /></div>
             <div class="widthdiv" id="mysurvebtn" runat="server"><asp:Button ID="btnMySurvey" CssClass="dynamicButton" runat="server" CausesValidation="false" Text="My Surveys"
                ToolTip="My Surveys" OnClick="btnMySurvey_Click" 
                meta:resourcekey="btnMySurveyResource1" />&nbsp;&nbsp;<asp:Button ID="btnDashBoard"  CausesValidation="false"  runat="server" CssClass="dynamicButton"
                OnClick="btnDashBoard_Click" Text="Survey Dashboard" 
                ToolTip="Survey Dashboard" meta:resourcekey="btnDashBoardResource1" />&nbsp;&nbsp;<asp:Button ID="btnReports" CausesValidation="false" runat="server" CssClass="dynamicButton" OnClick="btnReports_Click"
                Text="Survey Reports" ToolTip="Survey Reports" 
                meta:resourcekey="btnReportsResource1" /></div>
            </div>
            <div class="clear">
            </div>
            <!-- //survey question tabs -->
            <input type="hidden" id="hdnIntroText" class="hdnIntroText" runat="server" />
               <input type="hidden" runat="server" id="hdnactivation"  />
        </div>
        <div class="clear"> </div>
        
        <div id="boxes">
            <div id="dialog" class="window">
                <div class="divheader">Survey Launch Confirmation <a style="float: right; margin-top:-7px;" href="#" class="close popupCloseLink">&nbsp;</a></div>
                    <div class="divmsg">You are about to launch your survey 
                        <asp:Label ID="lblSurveyNameConfirm" runat="server"></asp:Label>.</div>
         
                    <!-- close button is defined as close class -->
                <div style="margin: auto; width: 100px;">
                    <div class="yes"><asp:LinkButton ID="btnLaunchWeb" ForeColor="#ffffff"  Text="Launch through web link" runat="server" 
                                     OnClick="btnLaunchWeb_Click" CausesValidation="false" >Confirm</asp:LinkButton></div> 
                </div> 
            </div>
        </div>

        <!-- Do not remove div#mask, because you'll need it to fill the whole screen --> 
        <div id="mask"></div>

        <!-- //survey question panel -->
    </div>
    <asp:Panel ID="pnlPreview" runat="server" Style="display: none;" 
        meta:resourcekey="pnlPreviewResource1">
        <asp:Label ID="lblEmailContent" runat="server" 
            meta:resourcekey="lblEmailContentResource1"></asp:Label>
    </asp:Panel>
     <script type="text/jscript" src="http://connect.facebook.net/en_US/all.js"></script>
      <script type="text/javascript">
          function displaySuccess(name, result, msg) {
              if (name == "facebook") {
                  if (result == "success") {
                      document.getElementById('divfbsuccess').style.display = 'block';
                      document.getElementById('<%=prelabtn.ClientID %>').style.display = 'none';
                      document.getElementById('<%=mysurvebtn.ClientID %>').style.display = 'block';
                      document.getElementById('<%=web.ClientID %>').style.display = 'none';
                      document.getElementById('<%=Embed.ClientID %>').style.display = 'none';
                      if (document.getElementById('<%=btnLaunchWeb1.ClientID %>').style.display != 'none') {
                          document.getElementById('<%=lnkWeblaunchother.ClientID %>').style.display = 'block';
                          document.getElementById('<%=btnLaunchWeb1.ClientID %>').style.display = 'none';
                      }
                      else {
                          document.getElementById('<%=lnkWeblaunchother.ClientID %>').style.display = 'none';
                          document.getElementById('<%=btnLaunchWeb1.ClientID %>').style.display = 'none';
                      }
                      document.getElementById('divtwittersuccess').style.display = 'none';
                  }
              }
              if (name == "twitter") {
                  if (result == "success") {
                      document.getElementById('divtwittersuccess').style.display = 'block';
                      document.getElementById('<%=prelabtn.ClientID %>').style.display = 'none';
                      document.getElementById('<%=mysurvebtn.ClientID %>').style.display = 'block';
                      document.getElementById('<%=web.ClientID %>').style.display = 'none';
                      document.getElementById('<%=Embed.ClientID %>').style.display = 'none';
                      document.getElementById('divfbsuccess').style.display = 'none';
                      if (document.getElementById('<%=btnLaunchWeb1.ClientID %>').style.display != 'none') {
                          document.getElementById('<%=lnkWeblaunchother.ClientID %>').style.display = 'block';
                          document.getElementById('<%=btnLaunchWeb1.ClientID %>').style.display = 'none';
                      }
                      else {
                          document.getElementById('<%=lnkWeblaunchother.ClientID %>').style.display = 'none';
                          document.getElementById('<%=btnLaunchWeb1.ClientID %>').style.display = 'none';
                      }
                  }
              }
              if (name == "linkedin") {
                  if (result == "success") {
                      document.getElementById('divlinkedinsuccess').style.display = 'block';
                      document.getElementById('<%=prelabtn.ClientID %>').style.display = 'none';
                      document.getElementById('<%=mysurvebtn.ClientID %>').style.display = 'block';
                      document.getElementById('<%=web.ClientID %>').style.display = 'none';
                      document.getElementById('<%=Embed.ClientID %>').style.display = 'none';
                      document.getElementById('divfbsuccess').style.display = 'none';
                      document.getElementById('divtwittersuccess').style.display = 'none';
                  }
              }
          }
   
          // Login in the current user via Facebook and ask for email permission
          function postToFB() {
              var i = 0;
              var error_no_option = 0, error_no_fb = 0;
              var error_msg;
              // Initialize the Facebook JavaScript SDK
              FB.init({
                  appId: '390001971057863',
                  xfbml: true,
                  status: true,
                  cookie: true
              });

              FB.login(checkLoginStatus, { scope: 'email' });
              return false;
          }

          // Check the result of the user status and display login button if necessary
          function checkLoginStatus(response) {
              var email;
              var shareURL = document.getElementById('<%=hdnFbUrl.ClientID %>').value;
              var surveynamefb = document.getElementById('<%=hdnSurveyName.ClientID %>').value;
              var intro = document.getElementById('<%=hdnIntoNew.ClientID %>').value;
              if (response && response.status == 'connected') {
                  FB.api('/me', function (resp) {
                      email = resp.email;
                  });
                  FB.api('me/friends/', function (resp) {
                      var length = 0;
                      for (var dummy in resp.data) length++;
                      FB.ui(
                {
                    method: 'feed',
                    name: surveynamefb,
                    link: shareURL,
                    picture: 'http://www.insighto.com/App_Themes/Classic/Images/insighto_logo.png',
                    caption: intro,
                    description: shareURL
                },
                function (response) {
                    var shareURL = document.getElementById('<%=hdnTestValue.ClientID %>').value;
                    var surveyid = document.getElementById('<%=hdnSurveyId.ClientID %>').value;
                    var layout = document.getElementById('<%=hdnlayout.ClientID %>').value;
                    var access = document.getElementById('<%=hdnaccess.ClientID %>').value;
                    if (response && response.post_id) {
                        var parms = email + ";" + length;
                        PageMethods.MyMethod(parms, shareURL, layout, access, surveyid, OnSucceeded);
                    }
                }
                );
                      function OnSucceeded(result) {
                          var a = result.split(";")
                          displaySuccess("facebook", a[0], a[1]);
                      }
                  });
              }

          }
    </script>
         <script type="text/javascript">

             function clicked_twitter_button(anchor_instance) {
                 anchor_instance.onclick = null;
                 var surveyid = document.getElementById('<%=hdnSurveyId.ClientID %>').value;
                 var layout = document.getElementById('<%=hdnlayout.ClientID %>').value;
                 var access = document.getElementById('<%=hdnaccess.ClientID %>').value;
                 PageMethods.twitterfun(surveyid,  layout, access, OnSucceededtwitterresult);
                 !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "https://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");
             }
             function OnSucceededtwitterresult(result) {
                 document.getElementById('<%= twitter.ClientID%>').href = "https://twitter.com/intent/tweet?text=I just launched a new survey. Request you to click the link and participate in the survey.&url=" + result;
                 if (result == null) 
                 {
                     Activationalert();
                 }
                 else if (result == "fgh") {
                 noQuestion();
                 }
                 else
                  {
                     document.getElementById('<%=hdnTestValue.ClientID %>').value = result;
                     document.getElementById('<%=twitter.ClientID%>').click();
                 }
             }

             function Confirm() {
                 var confirm_value = document.createElement("INPUT");
                 confirm_value.type = "hidden";
                 confirm_value.name = "confirm_value"; 
                 if (confirm("Do you want to tweet message?")) {
                     confirm_value.value = "Yes";
                     window.open("http://twitter.com/share", 'Twitter', 'height=600,width=600,left=200,top=40,resizable=no,scrollbars=no,toolbar=no,menubar=no,status=no');

                 } else {
                     confirm_value.value = "No";
                 }
                 document.forms[0].appendChild(confirm_value);
             }


             twttr.events.bind('tweet', function (event) {
                 var shareURL = document.getElementById('<%=hdnTestValue.ClientID %>').value;
                 var surveyid = document.getElementById('<%=hdnSurveyId.ClientID %>').value;
                 var layout = document.getElementById('<%=hdnlayout.ClientID %>').value;
                 var access = document.getElementById('<%=hdnaccess.ClientID %>').value;
                 var parms = "TWITTER_USER" + ";" + 50;
                 PageMethods.twittermethod(parms, shareURL, surveyid, layout, access, OnSucceededtwitter);
             });

             function OnSucceededtwitter(result) {
                 var a = result.split(";")
                 displaySuccess("twitter", a[0], a[1]);
             }
    
 </script>
    <script type="text/javascript">

        $(document).ready(function () {

            //select all the a tag with name equal to modal
            $('a[name=modal]').click(function (e) {
                //Cancel the link behavior
                e.preventDefault();
                //Get the A tag
                var id = $(this).attr('href');

                //Get the screen height and width
                var maskHeight = $(window).height();
                var maskWidth = $(window).width();

                //Set height and width to mask to fill up the whole screen
                $('#mask').css({ 'width': maskWidth, 'height': maskHeight });

                //transition effect    
                $('#mask').fadeIn(1000);
                $('#mask').fadeTo("slow", 0.8);

                //Get the window height and width
                var winH = $(window).height();
                var winW = $(window).width();

                //Set the popup window to center
                $(id).css('top', winH / 2 - $(id).height() / 2);
                $(id).css('left', winW / 2 - $(id).width() / 2);

                //transition effect
                $(id).fadeIn(2000);

            });

            //if close button is clicked
            $('.window .close').click(function (e) {
                //Cancel the link behavior
                e.preventDefault();
                $('#mask, .window').hide();
            });

            //if mask is clicked
            $('#mask').click(function () {
                $(this).hide();
                $('.window').hide();
            });

        });
 
</script>
         <script type="text/javascript" src="Scripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">

        $('.rbtnInternakAccessSetting input').change(function () {
            if ((this).value == "0" || (this).value == "2") {
                $('.internalNote').show();
            }
            else {
                $('.internalNote').hide();
            }
        });
        $('.rbtnlistAccessSettings input').change(function () {
            if ((this).value == "0") {
                $('.externalNote').show();
            }
            else {
                $('.externalNote').hide();
            }
        });

        tinyMCE.init({
            // General options
            mode: "specific_textareas",
            editor_selector: "mceEditor",
            theme: "advanced",

            plugins: "paste",
            // Theme options
            theme_advanced_buttons1: "bold,italic,underline,fontselect,fontsizeselect,forecolor,pasteword",
            theme_advanced_buttons2: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            //theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing: false,

            // Example content CSS (should be your site CSS)
            content_css: "css/content.css",
            oninit: "postInitWork",
            // Replace values for the template plugin
            template_replace_values: {
                username: "Some User",
                staffid: "991234"
            }
        });

        function postInitWork() {

            var val = document.getElementById("<%=EditorQuesIntroParams.ClientID%>").value;
            var sty_val = val.split("_");
            if (sty_val.length == 3) {
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.fontFamily = sty_val[0];
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.fontSize = sty_val[1];
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.color = sty_val[2];
            }
        }
        function cleartext() {
            tinyMCE.getInstanceById('<%= EditorQuestionIntro.ClientID %>').setContent('');
            return false;
        }
        function resetText() {
            tinyMCE.getInstanceById('<%= EditorQuestionIntro.ClientID %>').setContent('<p> You are invited to join a short survey being held among the customers of COMPANY/BRAND X. There are only (X No. of questions) and it will take about X minutes of your time. We will keep all your information confidential and will not transmit it to any third parties. If you have any questions about this survey, please contact me directly at my email address provided below. <br> Your Feedback is important!</br>');
        }
        function CopyIntro() {
            var introText = $('.hdnIntroText').val();
            alert($('.EditorQuestionIntro').text());
            $('.EditorQuestionIntro').text("hello");
            return false;
        }
       
        function hidevalidation() {
            document.getElementById('MainContent_SurveyMainContent_EditorQuestionIntro_parent').style.display = "none";
        }
        function restricOptionSelection() {
            OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
            $('.rbtnSurveyType').find("input[value='1']").attr("checked", "checked");
            return false;
        }
        function InsertSurveyLink() {
            tinyMCE.execInstanceCommand('<%= EditorQuestionIntro.ClientID %>', "mceInsertContent", false, "$$Start_Survey$$");
            return false;
        }

        function Activationalert() {
            jAlert('Congratulations!!!You are one step away from launching your first survey. However, you have <b><font color=red>not yet fully activated your Insighto account</font></b>.Another email is now being sent to the email address given during sign up.</br></br>Please have a look at  the email with subject “Please confirm your email address” in your mail inbox and click on the Confirm Email address.You would then be ready to launch your survey, start seeing the responses and make informed decisions!!</br></br>For any issues, please contact support@insighto.com </br></br> Happy Surveying!!!  ', 'Activation Alert', function (r) {
               
                document.getElementById('<%=hdnactivation.ClientID %>').value = r;
               
                var f1 = document.forms['Form1'];
                f1.submit();

                return false;
            });

        }
        function noQuestion() {
            jAlert('There are no questions in the survey. Please add questions and then launch again.');
        }
    </script>
      <script type="text/javascript">
          function SwitchImages(obj) {

              var div = document.getElementById(obj);
              var img = document.getElementById('img' + obj);

              if (div.style.display == "none") {
                  div.style.display = "inline";
                  img.src = "../images/minus.png";

              } else {
                  div.style.display = "none";
                  img.src = "../images/plus.png";

              }
          }

        
 </script>

 <script language="javascript" type="text/javascript">
     function showimage() {

         //  var x = document.getElementById('<%=imgchkfree.ClientID %>');
       //  alert("free");    
         document.getElementById('<%=hdfreecheck.ClientID %>').value = "FREE";
         document.getElementById('<%=imgchkfree.ClientID %>').style.visibility = "visible";
         document.getElementById('<%=imgchkpro.ClientID %>').style.visibility = "hidden";
         document.getElementById('<%=imgchkprem.ClientID %>').style.visibility = "hidden";
         var f1 = document.forms['Form1'];
         f1.submit();
         
     }

     function showimagepro() {

         // var x = document.getElementById('<%=imgchkpro.ClientID %>');
        // alert("pro");
         document.getElementById('<%=hdfreecheck.ClientID %>').value = "PPS_PRO";
         document.getElementById('<%=imgchkpro.ClientID %>').style.visibility = "visible";
         document.getElementById('<%=imgchkfree.ClientID %>').style.visibility = "hidden";
         document.getElementById('<%=imgchkprem.ClientID %>').style.visibility = "hidden";
         var f1 = document.forms['Form1'];       
         f1.submit();
     }

     function showimageprem() {
         document.getElementById('<%=hdfreecheck.ClientID %>').value = "PPS_PREMIUM";
         document.getElementById('<%=imgchkprem.ClientID %>').style.visibility = "visible";
         document.getElementById('<%=imgchkfree.ClientID %>').style.visibility = "hidden";
         document.getElementById('<%=imgchkpro.ClientID %>').style.visibility = "hidden";
         var f1 = document.forms['Form1'];
         f1.submit();
        
     }

     function playaudio(audiofile) {
         window.open("PlayAudio.aspx", "AudioPlay", "toolbar=0,location=0,directories=0,status=0,menubar=0,resizable=0,scorllbars=0,height=100,width=100,left=100,top=100");

     }
    
 </script>

<%-- <script type="text/JavaScript" language="JavaScript">
     var mine = window.open('', '', 'width=1,height=1,left=0,top=0,scrollbars=no');
     
     if (mine)
              var popUpsBlocked = false
     else     
         var popUpsBlocked = true
         
     mine.close()
</script>--%>


 <style type="text/css">
 .GDiv
{
    display:none;
    position:relative;
    font-size:16px;
    color:#434343;
    width:100%;
    font-weight:normal;
    border:0px solid #ccc;
    min-height:0px;
 
}

     </style>
       <script type="text/javascript" src="Scripts/ZeroClipBoard/jquery.zclip.min.js"></script>
    <script type="text/javascript">
        $(".btnCopy").zclip({
            path: 'Scripts/ZeroClipBoard/ZeroClipboard.swf',
            copy: $('.txtWebLink').val(),
            beforeCopy: function () {
                // $('.txtUrl').css('background', 'yellow');
                //$(this).css('color', 'orange');
            },
            afterCopy: function () {
                //$('.txtUrl').css('background', 'green');
                //$('.txtUrl').css('color', 'purple');
                //$(this).next('.check').show();
            }

        });

        $(".btnCopy1").zclip({
            path: 'Scripts/ZeroClipBoard/ZeroClipboard.swf',
            copy: $('.iframe').val(),
            beforeCopy: function () {
                // $('.txtUrl').css('background', 'yellow');
                //$(this).css('color', 'orange');
            },
            afterCopy: function () {
                //$('.txtUrl').css('background', 'green');
                //$('.txtUrl').css('color', 'purple');
                //$(this).next('.check').show();
            }

        });
    </script>
</asp:Content>
