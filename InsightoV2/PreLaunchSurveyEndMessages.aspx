﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PreLaunch.master" AutoEventWireup="true"
    CodeFile="PreLaunchSurveyEndMessages.aspx.cs" Inherits="PreLaunchSurveyEndMessages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PreLaunchMainContent" runat="Server">
    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccMsg" runat="server"></asp:Label></div>
    </div>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblErrMsg" runat="server"></asp:Label></div>
    </div>
    <div class="surveyQuestionHeader">
        <table width="100%">
            <tr>
                <td class="wid_70">
                    <div class="surveyQuestionTitle">
                       <asp:Label ID="lblViewThankYoupage" runat="server" Text="View Thank you Page"></asp:Label></div>
                </td>
                <td class="wid_30">
                    <asp:Button ID="btnEdit" runat="server" Text=" " ToolTip="Edit" CssClass="icon-pen-active"
                        OnClick="btnEdit_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div class="surveyQuestionTabPanel">
        <div>
            <!-- THANK YOU PAGE -->
            <div class="surveyEndTab">
                <!-- thank you page panel -->
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="mesgleftAlign">
                            <div class="surveyEndTabTitle tabThankyou">
                                <span id="iconThankyou" class="iconMinus">Thank You Page</span>
                                <%--<asp:Label ID="lblThankyoupage" runat="server" Text="Thank You Page"></asp:Label>--%>
                            </div>
                        </td>
                        <td class="mesgleftAlign">
                            <%-- <div class="surveyEndTabRadioPanel">  </div>--%>
                            <asp:RadioButtonList ID="rbtnThankyou" CssClass="rbtnThankyou" runat="server" RepeatDirection="Horizontal"
                                Enabled="false">
                                <asp:ListItem Value="0" Text="Default" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Custom Text"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>  <div class="rightAlignselect"><asp:Button ID="btnEdit1" runat="server" Text=" " ToolTip="Edit" CssClass="icon-pen-active"
                        OnClick="btnEdit_Click" /></div></td>
                    </tr>
                </table>
                <!-- //thank you page panel -->
            </div>
            <div class="surveryEdnTabTextPanel pnlThankyou" id="thankyou" style="display: block;"
                runat="server">
                <!-- text here -->
                <%--<div class="sampleLogoHeader">
                    <div class="logo-view">
                        <img src="App_Themes/Classic/Images/logo-insighto-view.jpg" />
                    </div>
                </div>--%>
                <h2 class="surveyMessage divThankyouDefault" id="divThankyouDefault" runat="server">
                    <asp:Label ID="lblDefaultMsg" runat="server" Text="Thank you for participating in the survey. Your feedback is highly appreciated."></asp:Label>
                </h2>
                <div class="divThankyouCustom" id="divThankyouCustom" style="display: none;" runat="server">
                    <asp:TextBox ID="txtThankyouMsg" CssClass="surveyMessageTextBox" runat="server" Text="You can put your custom text here"
                        Enabled="false" />
                </div>
                <div id="ThankyouContent" runat="server">
                </div>
                <!-- //text here -->
            </div>
            <!-- //THANK YOU PAGE -->
        </div>
        <div>
            <!-- SURVEY CLOSE PAGE -->
            <div class="surveyEndTab">
                <!-- survey close page panel -->
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="mesgleftAlign">
                            <div class="surveyEndTabTitle tabClose">
                                <span id="iconClose" class="icon-plus">SURVEY CLOSE PAGE</span>
                                <%--<asp:Label ID="lblSurveyClosePage" runat="server" Text="SURVEY CLOSE PAGE"></asp:Label>--%>
                            </div>
                        </td>
                        <td class="mesgleftAlign">
                            <asp:RadioButtonList ID="rbtnClose" CssClass="rbtnClose" runat="server" RepeatDirection="Horizontal"
                                Enabled="false">
                                <asp:ListItem Value="0" Text="Default" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Custom Text"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                         <td>  <div class="rightAlignselect"><asp:Button ID="btnEdit2" runat="server" Text=" " ToolTip="Edit" CssClass="icon-pen-active"
                        OnClick="btnEdit_Click" /></div></td>
                    </tr>
                </table>
                <!-- //survey close page panel -->
            </div>
            <div class="surveryEdnTabTextPanel pnlClose" id="close" runat="server" style="display: none;">
                <%--<div class="sampleLogoHeader">
                    <div class="logo-view">
                        <img src="App_Themes/Classic/Images/logo-insighto-view.jpg" />
                    </div>
                </div>--%>
                <div>
                    <!-- //////////////////// -->
                    <div id="div2">
                        <h2 class="surveyMessage divCloseDefault" id="divCloseDefault" runat="server">
                            <asp:Label ID="lblCloseDefaultMsg" runat="server" Text="Thank you for participating in the survey. Your feedback is highly appreciated."></asp:Label>
                        </h2>
                        <div class="divCloseCustom divCloseCustom" style="display: none;" id="divCloseCustom"
                            runat="server">
                            <asp:TextBox ID="txtCloseCustom" CssClass="surveyMessageTextBox" runat="server" Text="You can put your custom text here"
                                Enabled="false" />
                        </div>
                        <div id="CloseContent" runat="server">
                        </div>
                    </div>
                    <!--///////////////// -->
                </div>
            </div>
            <!-- //SURVEY CLOSE PAGE -->
        </div>
        <div>
            <!-- SCREEN OUT PAGE -->
            <div class="surveyEndTab">
                <!-- screen out page panel -->
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="mesgleftAlign">
                            <div class="surveyEndTabTitle tabScreenout">
                                <span id="iconOutPage" class="icon-plus">SCREEN OUT PAGE</span>
                                <%--<asp:Label ID="lblScreeOutPage" runat="server" Text="SCREEN OUT PAGE"></asp:Label>--%>
                            </div>
                        </td>
                        <td class="mesgleftAlign">
                            <asp:RadioButtonList ID="rbtnScreenOut" CssClass="rbtnScreenOut" runat="server" RepeatDirection="Horizontal"
                                Enabled="false">
                                <asp:ListItem Value="0" Text="Default" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Custom Text"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>  <div class="rightAlignselect"><asp:Button ID="btnEdit3" runat="server" Text=" " ToolTip="Edit" CssClass="icon-pen-active"
                        OnClick="btnEdit_Click" /></div></td>
                    </tr>
                </table>
                <!-- //screen out page panel -->
            </div>
            <div class="surveryEdnTabTextPanel pnlOutPage" runat="server" id="screenOut" style="display: none;">
               <%-- <div class="sampleLogoHeader">
                    <div class="logo-view">
                        <img src="App_Themes/Classic/Images/logo-insighto-view.jpg" />
                    </div>
                </div>--%>
                <div>
                    <!-- //////////////////// -->
                    <div id="div1">
                        <h2 class="surveyMessage divScreenoutdefault" id="divScreenoutdefault" runat="server">
                            <asp:Label ID="lblScreenoutMsg" runat="server" Text="Thank you for participating in the survey. Your feedback is highly appreciated."></asp:Label>
                        </h2>
                        <div class="divScreenoutcustom" style="display: none;" id="divScreenoutcustom" runat="server">
                            <asp:TextBox ID="txtScreenoutCustom" CssClass="surveyMessageTextBox" runat="server"
                                Text="You can put your custom text here" Enabled="false" />
                        </div>
                        <div id="ScreenoutContent" runat="server">
                        </div>
                    </div>
                    <!--///////////////// -->
                </div>
            </div>
            <!-- //SCREEN OUT PAGE -->
        </div>
        <div>
            <!-- OVER QUOTA PAGE -->
            <div class="surveyEndTab">
                <!-- over quota page panel -->
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="mesgleftAlign">
                            <div class="surveyEndTabTitle tabOverQuota">
                                <span id="iconOverQuota" class="icon-plus">OVER QUOTA PAGE</span>
                                <%--<asp:Label ID="lblOverQuotaPage" runat="server" Text="OVER QUOTA PAGE"></asp:Label> --%>
                            </div>
                        </td>
                        <td class="mesgleftAlign">
                            <asp:RadioButtonList ID="rbtnOverQuota" CssClass="rbtnOverQuota" runat="server" RepeatDirection="Horizontal"
                                Enabled="false">
                                <asp:ListItem Value="0" Text="Default" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Custom Text"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                         <td>  <div class="rightAlignselect"><asp:Button ID="btnEdit4" runat="server" Text=" " ToolTip="Edit" CssClass="icon-pen-active"
                        OnClick="btnEdit_Click" /></div></td>
                    </tr>
                </table>
                <!-- //over quota page panel -->
            </div>
            <div class="surveryEdnTabTextPanel pnlOverQuota" runat="server" id="overQuota" style="display: none;">
               <%-- <div class="sampleLogoHeader">
                    <div class="logo-view">
                        <img src="App_Themes/Classic/Images/logo-insighto-view.jpg" />
                    </div>
                </div>--%>
                <div>
                    <!-- //////////////////// -->
                    <div id="div3">
                        <h2 class="surveyMessage divOverQuotaDefault" id="divOverQuotaDefault" runat="server">
                            <asp:Label ID="lblOverQuotaMsg" runat="server" Text="Thank you for participating in the survey. Your feedback is highly appreciated."></asp:Label>
                        </h2>
                        <div class="divOverQuotaCustom" style="display: none;" id="divOverQuotaCustom" runat="server">
                            <asp:TextBox ID="txtOverQuota" CssClass="surveyMessageTextBox" runat="server" Text="You can put your custom text here"
                                Enabled="false" />
                        </div>
                        <div id="OverQuotaContent" runat="server">
                        </div>
                    </div>
                    <!--///////////////// -->
                </div>
            </div>
            <div class="defaultHeight">
            </div>
            <div class="prelaunchbtn"><asp:Button ID="btnBack" runat="server" Text="Back to Launch Page" ToolTip="Back to Launch Page"
                CssClass="prelaunchbackbtn" OnClick="btnBack_Click" /></div>
            <!-- //OVER QUOTA PAGE -->
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {
            $('.tabThankyou').click(function () {
                var className = $('#iconThankyou').attr('class');
                if (className == 'iconMinus') {

                    $('#iconThankyou').removeClass('iconMinus');
                    $('.pnlThankyou').hide();
                }
                else {
                    $('#iconThankyou').addClass('iconMinus');
                    $('.pnlThankyou').show();
                }
            });

            $('.tabClose').click(function () {
                var className = $('#iconClose').attr('class');
                if (className == 'icon-plus') {
                    $('#iconClose').removeClass('icon-plus');
                    $('#iconClose').addClass('iconMinus');
                    $('.pnlClose').show();
                }
                else {
                    $('#iconClose').removeClass('iconMinus');
                    $('#iconClose').addClass('icon-plus');
                    $('.pnlClose').hide();
                }
            });

            $('.tabScreenout').click(function () {
                var className = $('#iconOutPage').attr('class');
                if (className == 'icon-plus') {
                    $('#iconOutPage').removeClass('icon-plus');
                    $('#iconOutPage').addClass('iconMinus');
                    $('.pnlOutPage').show();
                }
                else {
                    $('#iconOutPage').removeClass('iconMinus');
                    $('#iconOutPage').addClass('icon-plus');
                    $('.pnlOutPage').hide();
                }
            });

            $('.tabOverQuota').click(function () {
                var className = $('#iconOverQuota').attr('class');
                if (className == 'icon-plus') {
                    $('#iconOverQuota').removeClass('icon-plus');
                    $('#iconOverQuota').addClass('iconMinus');
                    $('.pnlOverQuota').show();
                }
                else {
                    $('#iconOverQuota').removeClass('iconMinus');
                    $('#iconOverQuota').addClass('icon-plus');
                    $('.pnlOverQuota').hide();
                }
            });

            $('.rbtnThankyou input').change(function () {
                if ((this).value == "0") {
                    $('.divThankyouDefault').show();
                    $('.divThankyouCustom').hide();
                }
                else {
                    if ($('#MainContent_SurveyMainContent_hdnLicenseType').val() == "FREE") {
                        OpenModalPopUP('UpgradeLicense.aspx', 850, 600, 'yes');
                    }
                    else {
                        $('.divThankyouDefault').hide();
                        $('.divThankyouCustom').show();
                    }
                }
            });

            $('.rbtnClose input').change(function () {
                if ((this).value == "0") {
                    $('.divCloseDefault').show();
                    $('.divCloseCustom').hide();
                }
                else {
                    if ($('#MainContent_SurveyMainContent_hdnLicenseType').val() == "FREE") {
                        OpenModalPopUP('UpgradeLicense.aspx', 850, 600, 'yes');
                    }
                    else {
                        $('.divCloseDefault').hide();
                        $('.divCloseCustom').show();
                    }
                }
            });

            $('.rbtnScreenOut input').change(function () {

                if ((this).value == "0") {
                    $('.divScreenoutdefault').show();
                    $('.divScreenoutcustom').hide();
                }
                else {
                    if ($('#MainContent_SurveyMainContent_hdnLicenseType').val() == "FREE") {
                        OpenModalPopUP('UpgradeLicense.aspx', 850, 600, 'yes');
                    }

                    else {
                        $('.divScreenoutdefault').hide();
                        $('.divScreenoutcustom').show();
                    }
                }
            });

            $('.rbtnOverQuota input').change(function () {

                if ((this).value == "0") {
                    $('.divOverQuotaDefault').show();
                    $('.divOverQuotaCustom').hide();
                }
                else {
                    if ($('#MainContent_SurveyMainContent_hdnLicenseType').val() == "FREE") {
                        OpenModalPopUP('UpgradeLicense.aspx', 850, 600, 'yes');
                    }
                    else {
                        $('.divOverQuotaDefault').hide();
                        $('.divOverQuotaCustom').show();
                    }
                }
            });

        });

       

    </script>
</asp:Content>
