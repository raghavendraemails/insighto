﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using Resources;
using UserControls.RespondentControls;

public partial class SaveToWordPage : SurveyRespondentPageBase
{
    #region Public properties

    /// <summary>
    /// Gets the base page.
    /// </summary>
    public SurveyRespondentPageBase BasePage
    {
        get
        {
            return Page as SurveyRespondentPageBase;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public const string QtypePrefix = "qType";

    /// <summary>
    /// Gets or sets a value indicating whether this instance is quota reached.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this instance is quota reached; otherwise, <c>false</c>.
    /// </value>
    public bool IsQuotaReached { get; set; }

    /// <summary>
    /// Gets or sets the email id.
    /// </summary>
    /// <value>
    /// The email id.
    /// </value>
    public int EmailId { get; set; }

    /// <summary>
    /// Gets or sets the survey id.
    /// </summary>
    /// <value>
    /// The survey id.
    /// </value>
    public int SurveyId { get; set; }

    /// <summary>
    /// Gets or sets the index of the page.
    /// </summary>
    /// <value>
    /// The index of the page.
    /// </value>
    public int PageIndex { get; set; }

    /// <summary>
    /// Gets the name of the cookie.
    /// </summary>
    /// <value>
    /// The name of the cookie.
    /// </value>
    public string CookieName
    {
        get
        {
            return string.Format("{0}_{1}", BasePage.SurveyId, BasePage.LaunchId);
        }
    }

    #endregion

    #region Private Properties

    private List<SurveyQuestionAndAnswerOptions> _surveyoptionsList;
    private List<osm_responsequestions> _lstFilledResponses;
    //private SurveyResponseStatus _fillStatus;

    #endregion

    #region Event handlers

    #region Respondent Handlers

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
    protected override void OnInit(EventArgs e)
    {
        //checks whether survey has required data to process furthur or not
        //if yes, display respondent data; otherwise, redirects to last page.
        if (!BasePage.IsSurveyHasValidKey)
            return;

        SetLocalParamaeters();
        SetSurveyIntroductionText();
        ViewAllQuestions(e);
    }

    /// <summary>
    /// Handles the Click event of the btnContinue control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (BasePage.SurveyBasicInfoView == null || IsPostBack) return;

        SetHeaderDetails();
        SetStyles();
        SetButtons();

        GetSurveyQuestionsAndOptionsList();
        _surveyoptionsList = BasePage.Mode != RespondentDisplayMode.Preview
                                 ? _surveyoptionsList.Where(qa => qa.PageIndex == PageIndex).ToList()
                                 : _surveyoptionsList;

        SetLogo();

        //Bind Css
        LoadCss();

    }

    #endregion
    #endregion

    #region Private Methods

    /// <summary>
    /// View all the questions.
    /// </summary>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    private void ViewAllQuestions(EventArgs e)
    {
        //get basic survey questions and options list.
        GetSurveyQuestionsAndOptionsList();

        _lstFilledResponses = ValidationHelper.GetInteger(hdnRespondentId.Value, 0) > 0 ? SurveyService.GetSurveyResponseQuestionsById(ValidationHelper.GetInteger(hdnRespondentId.Value, 0)) : new List<osm_responsequestions>();
        _surveyoptionsList = BasePage.Mode != RespondentDisplayMode.Preview
                                 ? _surveyoptionsList.Where(qa => qa.PageIndex == PageIndex).ToList()
                                 : _surveyoptionsList;

        DisplayQuestionsInSingleOrTwoColumn();
        base.OnInit(e);
    }

    /// <summary>
    /// Displays the questions in single or two column.
    /// </summary>
    private void DisplayQuestionsInSingleOrTwoColumn()
    {
        int columnCount = 1;
        bool isTwoColumnLayOut = BasePage.ShowTwoColumnLayOut;
        divQuestionsPanel.Visible = !isTwoColumnLayOut;
        divTwoColumnPanel.Visible = isTwoColumnLayOut;
        string videoImagePath = ConfigurationManager.AppSettings["VideoImagepath"];  
        foreach (var surveyoption in _surveyoptionsList.Where(surveyoption => surveyoption.Question != null))
        {
            switch (surveyoption.QuestionType)
            {
                case QuestionType.Choice_YesNo:
                case QuestionType.MultipleChoice_SingleSelect:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.MultipleChoice_SingleSelect), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.MultipleChoice_MultipleSelect:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.MultipleChoice_MultipleSelect), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.Menu_DropDown:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.Menu_DropDown), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.Matrix_SideBySide:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.Matrix_SideBySide), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.Matrix_MultipleSelect:
                case QuestionType.Matrix_SingleSelect:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.Matrix_SingleSelect), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.EmailAddress:
                case QuestionType.Numeric_SingleRowBox:
                case QuestionType.Text_SingleRowBox:
                case QuestionType.Text_CommentBox:
                case QuestionType.Text_MultipleBoxes:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.Text_CommentBox), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.ConstantSum:
                case QuestionType.Ranking:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.ConstantSum), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.QuestionHeader:
                case QuestionType.PageHeader:
                case QuestionType.Narration:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.Narration), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.DateAndTime:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.DateAndTime), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.ContactInformation:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.ContactInformation), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.Image:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.Image), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.IntroductionText:
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                 PathHelper.GetRespondentControlPath(
                                                                     QuestionType.IntroductionText), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;

                case QuestionType.Video:
                    //LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                    //                                            PathHelper.GetRespondentControlPath(
                    //                                                QuestionType.Video), ref columnCount, isTwoColumnLayOut);
                    surveyoption.AnswerOption.FirstOrDefault().ANSWER_OPTIONS =videoImagePath; //Utilities.GetAbsoluteUrl(videoImagePath,Page);
                    surveyoption.Question.QUESTION_TYPE_ID = 21;
                    LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                             PathHelper.GetRespondentControlPath(
                                                                 QuestionType.Image), ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                    break;
            }

            if (BasePage.Mode == RespondentDisplayMode.Preview && surveyoption.Question.PAGE_BREAK == 1)
                LoadUserControl<SurveyRespondentControlBase>(new SurveyQuestionAndAnswerOptions { Question = new osm_surveyquestion { QUESTION_ID = new Random(DateTime.Now.Millisecond).Next(), PAGE_BREAK = 1 }, AnswerOption = new List<osm_answeroptions> { new osm_answeroptions() } },
                                                                PathHelper.GetQuestionControlPath(
                                                                    QuestionType.PageBreak), ref columnCount);
        }
    }

    /// <summary>
    /// Determines whether [is survey active].
    /// </summary>
    /// <returns>
    ///   <c>true</c> if [is survey active]; otherwise, <c>false</c>.
    /// </returns>
    private bool IsSurveyActive()
    {
        //var isSurveyActive = false;
        if (BasePage.SurveyBasicInfoView == null) return false;

        return BasePage.IsSurveyLaunched && BasePage.IsSurveyClosed;
    }


    /// <summary>
    /// Gets the survey questions and options list.
    /// </summary>
    private void GetSurveyQuestionsAndOptionsList()
    {
        _surveyoptionsList = BasePage.SurveyQuesAndAnswerOptionsList;
    }

    /// <summary>
    /// Loads the contr fol.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="surveyoption">The surveyoption.</param>
    /// <param name="path">The path.</param>
    private void LoadUserControl<T>(SurveyQuestionAndAnswerOptions surveyoption, string path, ref  int columnCount, bool isTwoColumnLayOut = false) where T : SurveyRespondentControlBase
    {
        //Load Control
        var control = Page.LoadControl(path) as T;
        if (control != null)
            control.SurveyQuestionAndAnswerOptions = surveyoption;

        var optionsList = _lstFilledResponses != null ? _lstFilledResponses.Where(ao => ao.QUESTION_ID == surveyoption.Question.QUESTION_ID).ToList() : null;
        if (control == null) return;
        control.RespondentAnswerOptions = optionsList;

        if (!isTwoColumnLayOut)
        {
            pnlQuestions.Controls.Add(control);
        }
        else
        {
            //each question will be added to this div.
            var div2ColumnQuestion = new Panel();
            if (columnCount == 1)
                div2ColumnQuestion.CssClass = "firstcolumn";
            else
            {
                div2ColumnQuestion.CssClass = "secondcolumn";
                columnCount = 0;
            }

            if (control is IntroductionTextControl)
            {
                pnlTwoColumnQuestions.Controls.Add(control);
                columnCount = 0;
            }
            else
            {
                div2ColumnQuestion.Controls.Add(control);
                pnlTwoColumnQuestions.Controls.Add(div2ColumnQuestion);
            }
        }
    }

    /// <summary>
    /// Sets the logo.
    /// </summary>
    private void SetLogo()
    {
        divLogo.Visible = Convert.ToBoolean(BasePage.SurveyBasicInfoView.Show_Logo);
        if (BasePage.SurveyBasicInfoView.Show_Logo == false || string.IsNullOrEmpty(BasePage.SurveyBasicInfoView.SURVEY_LOGO))
            return;

        Label ltlCompanyName = new Label();
        Image imgLogo = new Image();
        //set allignments
        HtmlTableRow trLogo = null;
        //HtmlTableCell tdLogo = null;

        //assign survey company name
        ltlCompanyName.Text = !string.IsNullOrEmpty(BasePage.SurveyBasicInfoView.COMPANY_NAME)
                ? BasePage.SurveyBasicInfoView.COMPANY_NAME
                : string.Empty; //@"insighto";           

        //assign survey logo
        var logoSplit = BasePage.SurveyBasicInfoView.SURVEY_LOGO.Split(new[] { '<' }, StringSplitOptions.RemoveEmptyEntries);
        var url = Utilities.GetAbsoluteUrl("~/" + logoSplit[0].Replace(">", string.Empty), Page);
        var alignment = logoSplit.Length > 1 ? logoSplit[2].Replace(">", string.Empty) : string.Empty;
        imgLogo.ImageUrl = url;

        switch (alignment)
        {
            case "left":
                trLogo = new HtmlTableRow();
                GetCompanyNameOrLogo(imgLogo, alignment, trLogo);
                GetCompanyNameOrLogo(ltlCompanyName, alignment, trLogo);

                tblLogo.Rows.Add(trLogo);
                break;

            case "center":
                trLogo = new HtmlTableRow();
                GetCompanyNameOrLogo(imgLogo, alignment, trLogo);
                tblLogo.Rows.Add(trLogo);

                trLogo = new HtmlTableRow();
                GetCompanyNameOrLogo(ltlCompanyName, alignment, trLogo);
                tblLogo.Rows.Add(trLogo);
                break;

            case "right":
                trLogo = new HtmlTableRow();
                GetCompanyNameOrLogo(ltlCompanyName, alignment, trLogo);
                GetCompanyNameOrLogo(imgLogo, alignment, trLogo);

                tblLogo.Rows.Add(trLogo);
                break;

            default:
                trLogo = new HtmlTableRow();
                GetCompanyNameOrLogo(ltlCompanyName, alignment, trLogo);
                GetCompanyNameOrLogo(imgLogo, alignment, trLogo);

                tblLogo.Rows.Add(trLogo);
                break;

        }
        divLogo.Attributes.Add("class", "logo" + alignment.ToLower());
    }

    /// <summary>
    /// Gets the company name or logo.
    /// </summary>
    /// <param name="control">The control.</param>
    /// <param name="alignment">The alignment.</param>
    /// <param name="trLogo">The tr logo.</param>
    private void GetCompanyNameOrLogo(Control control, string alignment, HtmlTableRow trLogo)
    {
        var tdLogo = new HtmlTableCell();
        tdLogo.Style.Add("text-align", alignment);
        tdLogo.Controls.Add(control);
        trLogo.Controls.Add(tdLogo);
    }

    /// <summary>
    /// Sets the styles.
    /// </summary>
    private void SetStyles()
    {
        if (Page.Master == null) return;
        dynamic dynamicStyle = Page.Master.FindControl("dynamicStyle");
        if (dynamicStyle == null)
            dynamicStyle = Page.FindControl("dynamicStyle");

        if (dynamicStyle == null) return;
        const string fontFormat = "font:normal {0}pt {1}, Helvetica, sans-serif; ";
        const string fontColor = "color:{0};";
        var fontStringBuilder = new StringBuilder("body {");
        fontStringBuilder.AppendFormat(fontFormat, BasePage.SurveyBasicInfoView.SURVEY_FONT_SIZE, BasePage.SurveyBasicInfoView.SURVEY_FONT_TYPE);
        fontStringBuilder.AppendFormat(fontColor, ColorCodeHelper.GetColorCodes(BasePage.SurveyBasicInfoView.SURVEY_FONT_COLOR));
        fontStringBuilder.Append("}");
        fontStringBuilder.AppendLine();
        fontStringBuilder.Append(".dynamicButton {");
        fontStringBuilder.AppendFormat("background:{0};", ColorCodeHelper.GetColorCodes(BasePage.SurveyBasicInfoView.BUTTON_BG_COLOR));
        fontStringBuilder.Append("}");
        dynamicStyle.InnerText = fontStringBuilder.ToString();
    }

    /// <summary>
    /// Sets the buttons.
    /// </summary>
    private void SetButtons()
    {
        if (BasePage.Mode == RespondentDisplayMode.Preview)
        {
            btnContinue.Visible = false;
            imgContinueButton.Visible = false;
            return;
        }
        var isArrowLabelBtn = BasePage.SurveyBasicInfoView.SURVEY_BUTTON_TYPE == "Label";
        var isArrowPlainBtn = BasePage.SurveyBasicInfoView.SURVEY_BUTTON_TYPE == "Plain";

        var isArrowBtn = (isArrowLabelBtn || isArrowPlainBtn);

        btnContinue.Visible = !isArrowBtn;
        imgContinueButton.Visible = isArrowBtn;

        var buttonColor = String.IsNullOrEmpty(BasePage.SurveyBasicInfoView.BUTTON_BG_COLOR) ? "purple" : BasePage.SurveyBasicInfoView.BUTTON_BG_COLOR;
        btnContinue.Text = "";

        imgContinueButton.ImageUrl = GetArrowButtonPath(buttonColor);

        if (isArrowLabelBtn)
            imgContinueButton.ImageUrl = GetArrowButtonPath(buttonColor, "continue");
        else if (!isArrowBtn)
            btnContinue.Text = String.IsNullOrEmpty(BasePage.SurveyBasicInfoView.CUSTOM_CONTINUE_TEXT)
                                ? "Continue"
                                : BasePage.SurveyBasicInfoView.CUSTOM_CONTINUE_TEXT;
        // btnContinue.BackColor = System.Drawing.Color.FromName(buttonColor);

        //Note: When user tries to see preview Page_Break will be null.
        if (PageIndex == 1)
        {

            if (isArrowLabelBtn)
                imgContinueButton.ImageUrl = GetArrowButtonPath(buttonColor, "start");
            else if (!isArrowBtn)
                btnContinue.Text = String.IsNullOrEmpty(BasePage.SurveyBasicInfoView.CUSTOMSTART_TEXT)
                                ? "Start"
                                : BasePage.SurveyBasicInfoView.CUSTOMSTART_TEXT;
        }

        if (BasePage.Mode == RespondentDisplayMode.Preview || BasePage.IsLastPage)
        {
            if (isArrowLabelBtn)
                imgContinueButton.ImageUrl = GetArrowButtonPath(buttonColor, "submit");
            else if (!isArrowBtn)
                btnContinue.Text = String.IsNullOrEmpty(BasePage.SurveyBasicInfoView.CUSTOMSUBMITTEXT)
                                       ? "Submit"
                                       : BasePage.SurveyBasicInfoView.CUSTOMSUBMITTEXT;
        }
    }

    /// <summary>
    /// Gets the arrow button path.
    /// </summary>
    /// <param name="color">The color.</param>
    /// <param name="text">The text.</param>
    /// <returns></returns>
    private static string GetArrowButtonPath(string color = "purple", string text = "plain")
    {
        return string.Format("~/Styles/ThemeImages/arrow-{0}-{1}.gif", color.ToLower().Replace(" ", ""), text.ToLower());
    }

    /// <summary>
    /// Sets local propertis form base, generates response id if there isnt one
    /// </summary>
    private void SetLocalParamaeters()
    {
        IsQuotaReached = false;
        SurveyId = BasePage.SurveyId;
        PageIndex = BasePage.PageIndex;
        hdnpageIndex.Value = PageIndex.ToString();
        hdnPasswordVerified.Value = BasePage.PasswordVerified.ToString();
        hdnRespondentId.Value = BasePage.RespondentId.ToString();
    }

    private void SetSurveyIntroductionText()
    {
        //set survey introduction text
        lblSurveyRespondentHeader.Text = BasePage.SurveyBasicInfoView.SURVEY_HEADER;
        lblSurveyRespondentFooter.Text = BasePage.SurveyBasicInfoView.SURVEY_FOOTER;
        Page.Title = RespondentResources.RespondentPageTitle;
    }

    /// <summary>
    /// Sets the header details.
    /// </summary>
    private void SetHeaderDetails()
    {
        if (BasePage.Mode == RespondentDisplayMode.Print || BasePage.Mode == RespondentDisplayMode.Preview || BasePage.Mode == RespondentDisplayMode.PreviewWithPagination)
        {
            divQuestionsPanel.Visible = BasePage.SurveyQuesAndAnswerOptionsList.Count > 0 && !BasePage.ShowTwoColumnLayOut;
            divTwoColumnPanel.Visible = BasePage.SurveyQuesAndAnswerOptionsList.Count > 0 && BasePage.ShowTwoColumnLayOut;
            divButtonsPanel.Visible = (BasePage.SurveyQuesAndAnswerOptionsList.Count == 1 &&
                                       BasePage.SurveyQuesAndAnswerOptionsList[0].QuestionType !=
                                       QuestionType.IntroductionText) ||
                                      BasePage.SurveyQuesAndAnswerOptionsList.Count > 1;

        }
    }

    private void LoadCss()
    {
        dynamic currentTheme = Page.FindControl("currentTheme");
        StringBuilder cssContent = new StringBuilder();

        //bind basic css
        FormatBasicCss(cssContent);

        //bind classic css
        FormatThemeCss(cssContent);

        //set css classes dynamically
        currentTheme.InnerText = cssContent.ToString();


        //bind user styles
        dynamic dynamicStyle = Page.FindControl("dynamicStyle");
        if (dynamicStyle == null) return;

        var fontStringBuilder = FormatUserStyles();
        dynamicStyle.InnerText = fontStringBuilder.ToString();
    }

    private StringBuilder FormatUserStyles()
    {
        const string fontFormat = "font:normal {0}px {1}, Helvetica, sans-serif; ";
        const string fontColor = "color:{0};";
        var fontStringBuilder = new StringBuilder("body {");
        fontStringBuilder.AppendFormat(fontFormat, BasePage.SurveyBasicInfoView.SURVEY_FONT_SIZE, BasePage.SurveyBasicInfoView.SURVEY_FONT_TYPE);
        fontStringBuilder.AppendFormat(fontColor, ColorCodeHelper.GetColorCodes(BasePage.SurveyBasicInfoView.SURVEY_FONT_COLOR));
        fontStringBuilder.Append("}");
        fontStringBuilder.AppendLine();
        fontStringBuilder.Append(".dynamicButton {");
        fontStringBuilder.AppendFormat("background:{0};", ColorCodeHelper.GetColorCodes(BasePage.SurveyBasicInfoView.BUTTON_BG_COLOR));
        fontStringBuilder.Append("}");
        return fontStringBuilder;
    }

    private void FormatThemeCss(StringBuilder cssContent)
    {
        var themeUrl = string.Empty;
        //dynamic theme css bind
        themeUrl = PathHelper.GetThemePath(Utilities.ToEnum<ThemeType>(BasePage.SurveyBasicInfoView.THEME.Replace(" ", "")));
        cssContent.Append(Utilities.GetHtmlString(Utilities.GetAbsoluteUrl(themeUrl, Page)));
        if (BasePage.SurveyBasicInfoView.Show_Progressbar == true)
        {
            themeUrl = string.Format("~/{0}", PathHelper.GetProgressBarPath(Utilities.ToEnum<ProgressBarType>(BasePage.SurveyBasicInfoView.Progressbar_Type.Replace(" ", ""))));
            cssContent.Append(Utilities.GetHtmlString(Utilities.GetAbsoluteUrl(themeUrl, Page)));
        }
    }

    private void FormatBasicCss(StringBuilder cssContent)
    {
        var themeUrl = string.Empty;
        StringBuilder scriptBuilder = new StringBuilder();
        scriptBuilder.Append(".checkList label {");
        scriptBuilder.Append("width:190px;");
        scriptBuilder.Append("display:inline-table;}");
        //basic css for respondent page
        themeUrl = string.Format("~/{0}", "Styles/RespondentBasicStyle.css");
        cssContent.Append(Utilities.GetHtmlString(Utilities.GetAbsoluteUrl(themeUrl, Page)));

        //classic css for respondent page
        themeUrl = string.Format("~/{0}", "App_Themes/Classic/SurveyRespondent.css");
        cssContent.Append(Utilities.GetHtmlString(Utilities.GetAbsoluteUrl(themeUrl, Page)));
        cssContent.Append(scriptBuilder);
    }

    #endregion
}