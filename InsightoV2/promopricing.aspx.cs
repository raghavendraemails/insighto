﻿using System;
using System.Collections;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class promopricing : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int userId = 0;
    string transType = "New";
    string country;
    private string licenseType = "";
    LoggedInUserInfo loggedInUserInfo;
    string strsource;
    string strterm;
    string currurl;
    string querystring;
    private double prodisprice;
    private double premdisprice;
    private double price;
    string strproprice = ConfigurationManager.AppSettings["proprice"];
    string strpremiumprice = ConfigurationManager.AppSettings["premiumprice"];
    private string utmsource;
    private string utmterm;
 

    protected void Page_Load(object sender, EventArgs e)
    {

        string NavurlMonth = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", "LicenseType==PRO_MONTHLY");
        PayMonth.HRef = NavurlMonth;



        string ipAddress = "";

        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
        {
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        }


        var country = Utilities.GetCountryName(ipAddress);
        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);

        country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();


        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (typeHt.Contains("Type"))
                licenseType = typeHt["Type"].ToString();
            if (typeHt.Contains(Constants.USERID))
                userId = ValidationHelper.GetInteger(typeHt[Constants.USERID].ToString(), 0);
            if (typeHt.Contains("Flag"))
                transType = Convert.ToString(typeHt["Flag"]);
            if (typeHt.Contains("utm_source"))
                utmsource = Convert.ToString(typeHt["utm_source"]);
            if (typeHt.Contains("Prem"))
            {
                if (typeHt["Prem"].ToString() == "True")
                {
                    divbasic.Visible = false;
                    divpro.Visible = false;
                    divpremium.Visible = true;
                    lblMonthly.Visible = false;
                }
            }
        }
        SessionStateService sessionStateService = new SessionStateService();
        loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();



        if (loggedInUserInfo != null)
        {
            //lblpro.Text = "Upgrade";
            btnpro1.Text = "Upgrade";
            btnpro2.Text = "Upgrade";
            btnpremium1.Text = "Upgrade";
            btnpremium2.Text = "Upgrade";
            if (loggedInUserInfo.LicenseType == "FREE")
            {

                divbasic.Visible = false;
                td1.Visible = false;
                td2.Visible = true;
                td2.Width = "20%";
                td3.Align = "Center";
                td3.Width = "30%";
                td4.Align = "Center";
                td4.Width = "30%";
                td5.Visible = true;
                td5.Width = "20%";

            }


            else if (loggedInUserInfo.LicenseType == "PRO_MONTHLY")
            {
                // div1.Visible = true;
                divbasic.Visible = false;
                lblMonthly.Visible = false;
                // prodiv.Visible = true;
                td1.Visible = false;
                td2.Visible = true;
                td2.Width = "20%";
                td3.Align = "Center";
                td3.Width = "30%";
                td4.Align = "Center";
                td4.Width = "30%";
                td5.Visible = true;
                td5.Width = "20%";

            }
            else if (loggedInUserInfo.LicenseType == "PRO_QUARTERLY")
            {

                divbasic.Visible = false;
                divpro.Visible = true;
                lblMonthly.Visible = false;
                td1.Visible = false;
                td2.Visible = true;
                td2.Width = "20%";
                td3.Align = "Center";
                td3.Width = "30%";
                td4.Align = "Center";
                td4.Width = "30%";
                td5.Visible = true;
                td5.Width = "20%";
            }
            else if (loggedInUserInfo.LicenseType == "PRO_YEARLY")
            {
                divbasic.Visible = false;
                // prodiv.Visible = true;
                lblMonthly.Visible = false;
                divpro.Visible = false;

                td1.Visible = false;
                td2.Visible = false;
                td2.Width = "15%";
                //  td3.Align = "Center";

                td3.Width = "35%";
                td4.Align = "Center";
                td4.Width = "30%";
                td5.Visible = true;
                td5.Width = "35%";

            }

        }



        SurveyCore surcore = new SurveyCore();


        currurl = HttpContext.Current.Request.RawUrl;
        querystring = HttpContext.Current.Request.QueryString.ToString();

       

        DataSet dspartnerinfo;

        if ((querystring != "") && (!querystring.Contains("key")))
        {
            utmsource = Request.QueryString["utm_source"].ToString();
           // utmterm = Request.QueryString["utm_content"].ToString();
            utmterm = "";
        }
        else
        {
            utmsource = "Insighto";
            utmterm = "Insighto";
        }
       


            string NavUrl = Constants.LicenseType + "=" + "PRO_MONTHLY" + "&UserId=" + userId + "&partner=" + utmsource;
            PayMonth.HRef = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl);

            string NavUrlbasic = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=Yes";
            basicviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlbasic);

            string NavUrlpro = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=Yes";
            proviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlpro);

            string NavUrlprem = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=Yes";
            premviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlprem);

            dspartnerinfo = surcore.getPartnerInfo(utmsource);

            lnkcheckitout.HRef = "http://www.youtube.com/watch?v=7xHGLyowVRk";

                    topleftmsg.Visible = false;
                    toprightmsg.Visible = false;

                    shinebullet.Visible = false;
                    insightobullet.Visible = true;
                    shinesignup.Visible = false;
                    insightosignup.Visible = true;

                    td1.Visible = false;
                    td1.Width = "0%";
                    td2.Visible = true;
                    td2.Align = "Left";
                    td2.Width = "25%";
                    tdpromonth.Visible = false;
                    tdpricefaq.Visible = true;
                    tdonlyaplan.Visible = false;
                    tdfreespace.Visible = false;
                    divbasic.Attributes.Add("class", "price_1_blkpromo");
                    divbasiccont.Attributes.Add("class", "price_1_blk_contpromo");



                    td3.Visible = true;
                    td3.Align = "left";
                    td3.Width = "25%";
                    divpro.Attributes.Add("class", "price_1_blkpromo");
                    divprocont.Attributes.Add("class", "price_1_blk_contpromo");


                    td4.Visible = true;
                    td4.Align = "left";
                    td4.Width = "25%";
                    divpremium.Attributes.Add("class", "price_1_blkpromo");
                    divpremiumcont.Attributes.Add("class", "price_1_blk_contpromo");
                    td5.Visible = true;
                    td5.Align = "right";
                    td5.Width = "25%";

                    dvpricetagfree.Visible = true;
                    divstrikepro.Visible = true;
                    divstrikeprem.Visible = true;
                    dvprempricetagfree.Visible = true;

                   
                    brpremreportindata.Visible = true;
                    brviresp.Visible = true;

                    tdpromonth.Visible = true;
                    brbasicviresp.Visible = true;
                  

                    basicbr1.Visible = true;
                    basicbr2.Visible = true;
                    basicbr3.Visible = true;
                    basicbr4.Visible = true;
                    basicbr5.Visible = true;
                    basicbr6.Visible = true;
                    probr2.Visible = true;
                    probr3.Visible = true;
                    prembr1.Visible = true;
                    int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                    int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());

                    string strproprice1;
                    string strpremiumprice1;

                    if (country.ToUpper() == "INDIA")
                    {
                        int promovalpro = Convert.ToInt32(strproprice.Replace(",", "")) - (prodisval * Convert.ToInt32(strproprice.Replace(",", ""))) / 100;
                        strproprice1 = string.Format("{0:N0}", Convert.ToInt32(promovalpro.ToString()));

                        int promovalprem = Convert.ToInt32(strpremiumprice.Replace(",", "")) - (premdisval * Convert.ToInt32(strpremiumprice.Replace(",", ""))) / 100 - 40;
                        strpremiumprice1 = string.Format("{0:N0}", Convert.ToInt32(promovalprem.ToString()));

                        
                        lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice1);
                        lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice1);
                      //  lblfree.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");
                        lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
                        lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);

                    }

                    else
                    {
                        tdpricefaq.Visible = false;
                        payinrupeesinsighto.Visible = false;
                        payinrupeesshine.Visible = false;
                        phonesupportinsighto.Visible = false;
                        phonesupportshine.Visible = false;
                        pricesUSDinsighto.Visible = true;
                        pricesUSDshine.Visible = true;
                        paymethodinsighto.InnerText = "Convenient payment options available";
                        paymethodshine.InnerText = "Convenient payment options available";

                        string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                        SqlConnection strconn = new SqlConnection(connStr);
                        strconn.Open();
                        SqlCommand scom = new SqlCommand("sp_getPriceLicenseCountry", strconn);
                        scom.CommandType = CommandType.StoredProcedure;
                        scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = country;
                        scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = "";
                        SqlDataAdapter sda = new SqlDataAdapter(scom);
                        DataSet pricedetails = new DataSet();
                        sda.SelectCommand = scom;
                        sda.Fill(pricedetails);

                        string pro_monthlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();


                        string pro_yearlicencetype = pricedetails.Tables[0].Rows[1][1].ToString();


                        string prem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();

                        strconn.Close();

                        DataRow[] dr = pricedetails.Tables[0].Select("License_Type = '" + "PRO_MONTHLY'");

                        string pro_monthprice = "";



                        if (dr.Length > 0)
                        {
                            pro_monthprice = dr[0].ItemArray[4].ToString();
                        }
                        else
                        {
                            pro_monthprice = "";
                        }


                        DataRow[] dr1 = pricedetails.Tables[0].Select("License_Type = '" + "PRO_YEARLY'");

                        string pro_yearprice = "";
                        if (dr1.Length > 0)
                        {
                            double promovalpro = Convert.ToDouble(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToDouble(dr1[0].ItemArray[4].ToString())) / 100;
                            //int promovalpro = Convert.ToInt32(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToInt32(dr1[0].ItemArray[4].ToString())) / 100;
                            pro_yearprice = Math.Round(promovalpro).ToString();



                            lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr1[0].ItemArray[4].ToString());
                            //lblorgpro.Attributes.Add("class", "priceRs");
                        }
                        else
                        {
                            pro_yearprice = "";

                            lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                            //  lblorgpro.Attributes.Add("class", "priceRs");
                        }


                        DataRow[] dr2 = pricedetails.Tables[0].Select("License_Type = '" + "PREMIUM_YEARLY'");

                        string prem_yearprice = "";
                        if (dr2.Length > 0)
                        {
                            double promovalprem = Convert.ToDouble(dr2[0].ItemArray[4].ToString()) - (premdisval * Convert.ToDouble(dr2[0].ItemArray[4].ToString())) / 100;

                            prem_yearprice = Math.Round(promovalprem).ToString();

                            lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr2[0].ItemArray[4].ToString());
                            //  lblorgpremium.Attributes.Add("class", "priceRs");
                        }
                        else
                        {
                            prem_yearprice = "";

                            lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                            //   lblorgpremium.Attributes.Add("class", "priceRs");
                        }

                        lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                        lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                        lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $" + pro_monthprice);
                      //  lblfree.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");

                    }

              


    }



    private string GetCurrencySymbol(string countryName)
    {
        var ci = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.EnglishName.Contains(countryName)).FirstOrDefault();
        return ci.NumberFormat.CurrencySymbol;
    }

    private double GetPriceByCountryName(string countryNAme, string subscription)
    {
        var price = ServiceFactory<OrderInfoService>.Instance.GetPriceByCountryName(countryNAme);
        if (subscription == "MONTHLY")
            return Convert.ToDouble(price.MONTHLY_PRICE);
        else
            return Convert.ToDouble(price.YEARLY_PRICE);
    }
    protected void button_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/In/SignUpFree.aspx");
    }
    protected void button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/In/SignUpFree.aspx");
    }

    protected void btnpro1_Click(object sender, EventArgs e)
    {
        

        string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        
    }
    protected void btnpro2_Click(object sender, EventArgs e)
    {
       
          string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
       
    }
    protected void btnpremium1_Click(object sender, EventArgs e)
    {
        
            string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
       

    }
    protected void btnpremium2_Click(object sender, EventArgs e)
    {
       
            string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
       
    }

    
}