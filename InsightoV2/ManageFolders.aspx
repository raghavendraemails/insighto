﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ManageFolders.aspx.cs" Inherits="ManageFolders"%>

<%@ Register Src="UserControls/ProfileRightPanel.ascx" TagName="ProfileRightPanel"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="myProfileLeftPanel">
        <div class="marginNewPanel" style="margin-top:0;">
            <div class="signUpPnl">
                <!-- customer details -->
                <h3>
                    <asp:Label ID="lblTitle" runat="server" Text=" Manage Folders" 
                        meta:resourcekey="lblTitleResource1"></asp:Label>
                </h3>
                <div class="messagePanelProfile">
                    <div class="informationPanelDefault">
                        <div>
                         <asp:Label ID="lblHelpText" runat="server" 
                                Text=" To move {0}s from one folder to another, click on View icon. To edit folder name, click on Edit. To delete folder, move {0}s to another folder and then click on Delete icon." 
                                meta:resourcekey="lblHelpTextResource1"></asp:Label>
                           </div>
                    </div>
                </div>
                <div class="messagePanelProfile">
                    <div class="successPanel" id="dvSuccessMsg" style="display: none;">
                        <div>
                            <asp:Label ID="lblSuccessMsg" runat="server" 
                                Text="Folder  deleted successfully." meta:resourcekey="lblSuccessMsgResource1"></asp:Label>
                        </div>
                    </div>
                    <div class="errorMessagePanel" id="dvErrMsg" style="display: none;">
                        <div>
                            <asp:Label ID="lblErrMsg" runat="server" 
                                Text="Please move {0}s to another folder before deleting." 
                                meta:resourcekey="lblErrMsgResource1"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="lt_padding">
                    <a id="lnkCreateFolder" runat="server" rel="framebox" h="230" w="650">
                        <img src="App_Themes/Classic/Images/button_create-folder.gif" alt="Create Folder"
                            title="Create Folder" />
                    </a>
                </div>
                <div class="surveysGridPanelPad">
                    <div>
                        <div id="ptoolbar">
                        </div>
                        <table id="tblJobKits">
                        </table>
                    </div>
                    <input type="hidden" runat="server" id="hdnSurveyFlag" class="hdnSurveyFlag" />
                </div>
                <!-- //customer details -->
            </div>
        </div>
    </div>
    <uc:ProfileRightPanel ID="ucProfileRightPanel" runat="server" />
    <div class="clear">
    </div>
    <script type="text/javascript">
        var serviceUrl = 'AjaxService.aspx';
        var displayMode = 'Active';
        var surveyFlag = $(".hdnSurveyFlag").val();
        $(document).ready(function () {
            $("#tblJobKits").jqGrid({
                url: serviceUrl,
                postData: { method: "GetFolders", mode: displayMode,Surveyflag: surveyFlag },
                datatype: 'json',
                colNames: ['FOLDER NAME', ' ', ' ', ' '],
                colModel: [
                { name: 'FOLDER_NAME', index: 'FOLDER_NAME', width: 240, align: 'left', editable: false, resizable: false, sortable: true, size: 20 },
                { name: 'viewUrl', index: 'viewUrl', width: 15, align: 'left', editable: false, sortable: false, resizable: false, size: 10, formatter: ViewLinkFormatter },
                { name: 'editUrl', index: 'editUrl', width: 15, align: 'left', editable: false, sortable: false, resizable: false, size: 10, formatter: EditLinkFormatter },
                { name: 'FOLDER_ID', index: 'FOLDER_ID', width: 15, align: 'left', editable: false, sortable: false, resizable: false, size: 10, formatter: DeleteLinkFormatter }
                 ],
                rowNum: <%=_recordsCount %>,
                 rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'FOLDER_NAME',
                sortorder: "asc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 630,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }

            });
            $("#tblJobKits").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false,refresh:false });
            $("#tblJobKits").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');

        });

        function EditLinkFormatter(cellvalue, options, rowObject) {
            if (cellvalue == "0")
                return "";
            else
                return "<a href='" + cellvalue + "' rel='framebox'   w='650' h='220'><img src='App_Themes/Classic/Images/icon-pen-active.gif' title='Edit'/></a>";

        }

        function ViewLinkFormatter(cellvalue, options, rowObject) {

            return "<a href='" + cellvalue + "' rel='framebox' w='750' h='480' scrolling='yes'><img src='App_Themes/Classic/Images/icon-tv-active.gif' title='View & Move'/></a>";
        }

        function DeleteLinkFormatter(cellvalue, options, rowObject) {
            if (cellvalue > 0)
                return "<a href='#' class='button-small-gap' id='lnkDelete' onclick='javascript: return DeleteFolder(" + cellvalue + ");'><img src='App_Themes/Classic/Images/icon-delete-active.gif' title='Delete' /></a>";
            else
                return "";
        }

        function gridReload() {
            var keyword = $("#txtKeyword").val();
            $("#tblJobKits").jqGrid('setGridParam', { url: serviceUrl + "?keyword=" + keyword, page: 1 }).trigger("reloadGrid");
        }

//        function DeleteFolder(FolderId) {
//            var answer = confirm('Are you sure you want to delete ?');
//            if (answer) {
//                $.post("AjaxService.aspx", { "method": "DeleteFolderByFolderId", "Id": FolderId },
//           function (result) {
//               if (result == '1') {

//                   $('#dvErrMsg').attr('style', 'display:none;');
//                   $('#dvSuccessMsg').attr('style', 'display:visible;');
//                   var status = $('#hdnStatus').val();
//                   gridReload(status);
//               }
//               else {
//                   $('#dvSuccessMsg').attr('style', 'display:none;');
//                   $('#dvErrMsg').attr('style', 'display:visible;');
//               }
//           });
//            }
//        }

function DeleteFolder(FolderId) {
           jConfirm('Are you sure you want to delete this folder?', 'Delete Confirmation', function (r) {
                if (r == '1') {
                $.post("AjaxService.aspx", { "method": "DeleteFolderByFolderId", "Id": FolderId },
           function (result) {
               if (result == '1') {

                   $('#dvErrMsg').attr('style', 'display:none;');
                   $('#dvSuccessMsg').attr('style', 'display:visible;');
                   var status = $('#hdnStatus').val();
                   gridReload(status);
               }
               else {
                   $('#dvSuccessMsg').attr('style', 'display:none;');
                   $('#dvErrMsg').attr('style', 'display:visible;');
               }
           });
           }

            });
        }

    </script>
</asp:Content>
