﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using System.Configuration;
using App_Code;

namespace Insighto.Pages
{
    public partial class AddressBook : SecurePageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            //if (loggedInUserInfo.LicenseType == "FREE")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            //    hlnkAddList.NavigateUrl = EncryptHelper.EncryptQuerystring("UpgradeLicense.aspx", "surveyFlag=" + SurveyFlag);
            //}
            //else
            //{
                hlnkAddList.NavigateUrl = EncryptHelper.EncryptQuerystring("ContactList.aspx", "surveyFlag=" + SurveyFlag);
           // }
                if (Request.QueryString["key"] != null)
                {
                    Hashtable ht = new Hashtable();
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                    {
                        int surID = ValidationHelper.GetInteger(ht["SurveyId"].ToString(), 0);
                        if (surID > 0)
                        {
                            btnBacktoLaunchPage.Visible = true;
                            Session["ReturnSurveyId"] = ht["SurveyId"].ToString();

                            if (ht != null && ht.Count > 0 && ht.Contains("Flag"))
                            {
                                Session["Flag"] = ht["Flag"].ToString();
                            }
                        }
                    }
                    else
                    {
                        Session["ReturnSurveyId"] = null;
                        Session["Flag"] = null;
                        btnBacktoLaunchPage.Visible = false;
                    }
                    if (ht != null && ht.Count > 0 && ht.Contains("smSurveyId"))
                    {
                        int surID = ValidationHelper.GetInteger(ht["smSurveyId"].ToString(), 0);
                        if (surID > 0)
                        {
                            btnBacktoManager.Visible = true;
                        }
                    }
                    else
                        btnBacktoManager.Visible = false;

                }
               

            }
       

        protected void btnBacktoManager_Click(object sender, EventArgs e)
        {
            Hashtable ht = new Hashtable();
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("smSurveyId"))
                {
                    int SurveyID = ValidationHelper.GetInteger(ht["smSurveyId"].ToString(), 0);
                    if (SurveyID > 0)
                    {
                        string urlstr = EncryptHelper.EncryptQuerystring("SurveyManager.aspx", "SurveyId=" + SurveyID+"&surveyFlag="+SurveyFlag);
                        Response.Redirect(urlstr);

                    }
                   
               
                }
            }
        }
        protected void btnBacktoLaunchPage_Click(object sender, EventArgs e)
        {
            Hashtable ht = new Hashtable();
            int SurveyID = 0;          

            if (Session["ReturnSurveyId"] != null)
            {
                SurveyID = ValidationHelper.GetInteger(Session["ReturnSurveyId"].ToString(), 0);
                if (SurveyID > 0)
                {
                    int Contactlist_id = 0;
                    if (Session["ContactId"] != null)
                        Contactlist_id = ValidationHelper.GetInteger(Session["ContactId"].ToString(), 0);
                        string urlstr = "";            
                      if (Session["Flag"] != null)                         
                        urlstr = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx", "SurveyId=" + SurveyID + "&ContID=" + Contactlist_id + "&Flag=Active&surveyFlag="+SurveyFlag);
                      else
                          urlstr = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx", "SurveyId=" + SurveyID + "&ContID=" + Contactlist_id + "&surveyFlag=" + SurveyFlag);
                       Session["ReturnSurveyId"] = null;
                       Session["Flag"] = null;
                       Response.Redirect(urlstr);
                }
            }
           
               
        }
    }
}