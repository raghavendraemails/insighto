﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using App_Code;

public partial class HeaderFooter : SurveyPageBase
{

    #region "Event : Page_Load"
    /// <summary>
    /// page load.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void Page_Load(object sender, EventArgs e)
    {

        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = false;
        
        if (!IsPostBack)
        {
            EditorHeaderParams.Value = base.SurveyBasicInfoView.SURVEY_FONT_TYPE + "_" + BuildQuestions.GetFontSizeValue(base.SurveyBasicInfoView.SURVEY_FONT_SIZE) + "_" + BuildQuestions.GetFontColor(base.SurveyBasicInfoView.SURVEY_FONT_COLOR);
            BindExistsData();

            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            //if (loggedInUserInfo.LicenseType == "FREE")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            //}
        }
    }
    #endregion

    #region "Method : BindExistsData"
    /// <summary>
    /// Used to bind existing data.
    /// </summary>

    public void BindExistsData()
    {
        if (base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            var surveySettingsService = ServiceFactory.GetService<SurveySetting>();
            var surveySettings = surveySettingsService.GetSurveySettings(base.SurveyBasicInfoView.SURVEY_ID);

            if (SetFeatures("HEADER") == 1)
            {
                EditorHeader.Value = surveySettings[0].SURVEY_HEADER;
            }
            else
            {
                EditorHeader.Value = "<img src='In/insighto_logo.gif' align='left'/>";
                //div_header.Visible = true;
            }

            if (SetFeatures("FOOTER") == 1)
            {
                EditorFooter.Value = surveySettings[0].SURVEY_FOOTER;
            }
            else
            {
                //div_footer.Visible = true;
            }
        }
    }
    #endregion

    #region "Event : btnSave_Click"
    /// <summary>
    /// Used to save header and footer.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (SetFeatures("HEADER") == 0)
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Upgrade", "OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');", true);
        }
        bool IsBlockedWordHeaderExists = false;
        bool IsBlockedWordFooterExists = false;
        var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();


        var contentWordsHeader = EditorHeader.Value.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
        var existedBlockedWordsHeader = blockedWords.Where(b => contentWordsHeader.Where(c => c.Trim() == b.BLOCKEDWORD_NAME).Any());
        IsBlockedWordHeaderExists = existedBlockedWordsHeader.Any();

        var contentWordsFooter = EditorFooter.Value.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
        var existedBlockedWordsFooter = blockedWords.Where(b => contentWordsFooter.Where(c => c.Trim() == b.BLOCKEDWORD_NAME).Any());
        IsBlockedWordFooterExists = existedBlockedWordsFooter.Any();

        if (base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            //if (EditorHeader.Value.Trim() != "")
            //{
                if (!IsBlockedWordHeaderExists)
                {
                    //if (EditorFooter.Value.Trim() != "")
                    //{
                        if (!IsBlockedWordFooterExists)
                        {
                            osm_surveysetting osmSurveySetting = new osm_surveysetting();
                            osmSurveySetting.SURVEY_ID = base.SurveyBasicInfoView.SURVEY_ID;
                            if (SetFeatures("HEADER") == 1)
                            {
                                osmSurveySetting.SURVEY_HEADER = EditorHeader.Value;
                            }
                            else
                            {
                                osmSurveySetting.SURVEY_HEADER = "";
                            }

                            if (SetFeatures("FOOTER") == 1)
                            {
                                osmSurveySetting.SURVEY_FOOTER = EditorFooter.Value;
                            }
                            else
                            {
                                osmSurveySetting.SURVEY_FOOTER = "";
                            }

                            var surveySettingsService = ServiceFactory.GetService<SurveySetting>();
                            surveySettingsService.SaveOrUpdateSurveySetting(osmSurveySetting, "H");

                            BindExistsData();

                            if ((SetFeatures("HEADER") == 1) && (SetFeatures("FOOTER") == 1))
                            {
                              
                                dvSuccessMsg.Visible = true;
                            }
                        }
                        else
                        {
                            lblErrMsg.Text = Utilities.ResourceMessage("lblErrMsgFooter"); //"Footer contains blocked words.";
                            dvErrMsg.Visible = false;
                        }
                 
                }
                else
                {
                    lblErrMsg.Text =Utilities.ResourceMessage("lblErrMsgHeader")   ;//"Header contains blocked words.";
                    dvErrMsg.Visible = false;
                }

        }
    }

    #endregion

    #region "Method : SetFeatures"
    /// <summary>
    /// This method returns bool value, if the feature flag is one.
    /// </summary>
    /// <param name="strFeature"></param>
    /// <returns></returns>

    public int SetFeatures(string strFeature)
    {
        int iVal = 0;
        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var sessdet = session.GetLoginUserDetailsSession();
        if (sessdet != null)
        {

            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));

            if (sessdet.UserId > 0 && listFeatures != null)
            {
                foreach (var listFeat in listFeatures)
                {

                    if (listFeat.Feature.Contains(strFeature))
                    {
                        iVal = (int)listFeat.Value;
                    }
                }
            }
        }

        else
        {
            Response.Redirect(PathHelper.GetUserLoginPageURL());
        }
        return iVal;
    }

    #endregion
}