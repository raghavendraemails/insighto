﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Text;
using System.Configuration;
using Insighto.Data;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class ActivateSubType : SecurePageBase
{
    Hashtable typeHt = new Hashtable();
    string tag = "";
    int userid = 0;
    PollCreation Pc = new PollCreation();
    string RedirectUrl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            if (typeHt.Contains("tag"))
                tag = typeHt["tag"].ToString();
            if (typeHt.Contains("userid"))
                userid = Convert.ToInt32(typeHt["userid"].ToString());
            if (tag == "Survey")
            {
                PollActivation.Style.Add("display", "none");
                SurveyActivation.Style.Add("display", "block");
            }
            else if (tag == "Poll")
            {
                PollActivation.Style.Add("display","block");
                SurveyActivation.Style.Add("display", "none");
            }
        }
    }
    protected void btnActivate_Click(object sender, EventArgs e)
    {
        //Pc.UpdateUserSubscriptionType(1,"Survey",userid);
        //RedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.Getsocialnetwork(), "userid=" + userid);
        //Response.Redirect(RedirectUrl);

        Pc.UpdateUserSubscriptionType(1, "survey", userid);

        RedirectUrl = EncryptHelper.EncryptQuerystring("MyAccounts.aspx", "surveyFlag=" + base.SurveyFlag);
     
        //Response.Redirect(RedirectUrl);
        base.CloseModal(RedirectUrl);
    }
    protected void btnActivatePoll_Click(object sender, EventArgs e)
    {
       
        Pc.UpdateUserSubscriptionType(2, "Poll", userid);
      //  RedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyPollPageURL(), "userid=" + userid);  
        RedirectUrl = EncryptHelper.EncryptQuerystring("/Polls/MyPolls.aspx", "userid=" + userid);   
        base.CloseModal(RedirectUrl);
    }
}