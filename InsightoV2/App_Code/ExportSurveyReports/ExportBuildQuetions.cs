﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts.Web;
using System.Text;
using System.Linq;
using System.Drawing;
using System.Web.UI.WebControls;


namespace InsightoV2.ExportSurveyReports
{
    public static class BuildQuetions
    {
        //public BuildQuetions()
        //{
        //    //
        //    // TODO: Add constructor logic here
        //    //
        //} 

        //private static FontUnit Fsize = FontUnit.Small;
        //private static Color FColor = Color.Black;
        private static string Fname = "Trebuchet MS";

        public static string HTMLTextSettings(string text)
        {
            string str = "";
            if (text != null)
            {
                str = "<p>";
                str += text;
                str = str.Replace("</p><p>", "</p>\r\n<p>");
                str = str.Replace("<br />", "</p>\r\n<p>");
                str += "</p>";
                str = str.Replace("<p></p>", "<p>&nbsp;</p>");
                str = str.Replace("<p><p>", "<p>");
                str = str.Replace("</p></p>", "</p>");
            }
            return str;
        }

        //private static HtmlTable GetQuestion(SurveyQuestion ques)
        //{
        //    HtmlTable tablename = new HtmlTable();
        //    HtmlTableRow tableRowQues = new HtmlTableRow();
        //    HtmlTableCell tablecellQues = new HtmlTableCell();
        //    HtmlTableCell tableNocell = new HtmlTableCell();
        //    Label QuestionLabel = new Label();
        //    QuestionLabel.Font.Size = Fsize; QuestionLabel.ForeColor = FColor; QuestionLabel.Font.Name = Fname;
        //    QuestionLabel.ID = "Questionlbl" + ques.QUESTION_ID.ToString();
        //    QuestionLabel.Text = ques.QUESTION_LABEL.Trim();
        //    if (ques.QUESTION_TYPE == 15)
        //    {
        //        QuestionLabel.Text += "<ul><li>Ranks must be between 1 and " + ques.surveyAnswers.Count + ".</li><li>Ranks should not be repeated.</li></ul>";

        //    }
        //    tablecellQues.Controls.Add(QuestionLabel);
        //    tableRowQues.Cells.Add(tableNocell);
        //    tableRowQues.Cells.Add(tablecellQues);
        //    tablename.Rows.Add(tableRowQues);
        //    return tablename;
        //}
        //private static HtmlTable QuestionTypeOne(SurveyQuestion ques)
        //{
        //    //HtmlTable TableQues = GetQuestion(ques);
        //    //HtmlTableRow tableRowAns = new HtmlTableRow();
        //    //HtmlTableCell tablecellAns = new HtmlTableCell();
        //    //HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    //tableRowAns.Cells.Add(tblCellEmpty);
        //    //ASPxRadioButtonList rnd = new ASPxRadioButtonList();
        //    //rnd.ID = "rdlOne" + ques.QUESTION_ID.ToString();
        //    //rnd.Font.Size = Fsize; rnd.ForeColor = FColor; rnd.Font.Name = Fname;
        //    //rnd.RepeatDirection = GetDirection(ques.ANSWER_ALIGNSTYLE);
        //    //rnd.Border.BorderStyle = BorderStyle.None;
        //    //rnd.EncodeHtml = false;
        //    //if (ques != null && ques.surveyAnswers != null)
        //    //{
        //    //    foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //    //    {
        //    //        string answerOpts = AnsOpts.ANSWER_OPTIONS.Replace("<p>", "");
        //    //        answerOpts = answerOpts.Replace("</p>", "");
        //    //        rnd.Items.Add(answerOpts, AnsOpts.ANSWER_ID);
        //    //    }
        //    //}
        //    //tablecellAns.Controls.Add(rnd);
        //    //tableRowAns.Cells.Add(tablecellAns);
        //    //TableQues.Rows.Add(tableRowAns);
        //    //if (ques.OTHER_ANS == 1)
        //    //{
        //    //    HtmlTableRow tableRowOtherAns = new HtmlTableRow();

        //    //    HtmlTableCell tablecellOthEmpty = new HtmlTableCell();
        //    //    HtmlTableCell tablecellOtherAnslbl = new HtmlTableCell();
        //    //    tableRowOtherAns.Align = "Left";
        //    //    tablecellOtherAnslbl.Align = "Right";
        //    //    Label OtherAnslbl = new Label();
        //    //    OtherAnslbl.Font.Size = Fsize;
        //    //    OtherAnslbl.ForeColor = FColor;
        //    //    OtherAnslbl.Font.Name = Fname;
        //    //    OtherAnslbl.ID = "OtherAnslbl" + ques.QUESTION_ID.ToString();
        //    //    OtherAnslbl.Text = "Other";
        //    //    tablecellOtherAnslbl.Controls.Add(OtherAnslbl);
        //    //    HtmlTableCell tablecellOtherAnsDet = new HtmlTableCell();
        //    //    ASPxTextBox txtOthAns = new ASPxTextBox();
        //    //    txtOthAns.Font.Size = Fsize;
        //    //    txtOthAns.ForeColor = Color.Black;  //FColor;
        //    //    txtOthAns.Font.Name = Fname;
        //    //    txtOthAns.ID = "txtOthAns" + ques.QUESTION_ID.ToString();
        //    //    txtOthAns.Width = 150;
        //    //    tablecellOtherAnsDet.Controls.Add(txtOthAns);
        //    //    tableRowOtherAns.Cells.Add(tablecellOthEmpty);
        //    //    tableRowOtherAns.Cells.Add(tablecellOtherAnslbl);
        //    //    tableRowOtherAns.Cells.Add(tablecellOtherAnsDet);
        //    //    TableQues.Rows.Add(tableRowOtherAns);
        //    //}
        //    //return TableQues;

        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableRow tableRowAns1 = new HtmlTableRow();
        //    HtmlTableCell tablecellAns1 = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns1.Cells.Add(tblCellEmpty);
        //    HtmlTable tableAnsOpt = new HtmlTable();

        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        int count = 1;
        //        int status_flag = 0;
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            status_flag = 0;
        //            if (ques.ANSWER_ALIGNSTYLE == "1Column" || ques.ANSWER_ALIGNSTYLE == "Vertical")
        //            {
        //                tableRowAns = new HtmlTableRow();
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "2Columns" && (count - 1) % 2 == 0)
        //            {
        //                //tableRowAns.Cells.Add(tablecellAns);
        //                tableRowAns = new HtmlTableRow();
        //                //TableQues.Rows.Add(tableRowAns);
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "3Columns" && (count - 1) % 3 == 0)
        //            {
        //                tableRowAns = new HtmlTableRow();
        //            }
        //            else if ((ques.ANSWER_ALIGNSTYLE == "4Columns" || ques.ANSWER_ALIGNSTYLE == "Horizontal") && (count - 1) % 4 == 0)
        //            {
        //                tableRowAns = new HtmlTableRow();
        //            }
        //            HtmlTableCell tablecellAns = new HtmlTableCell();
        //            string answerOpts = AnsOpts.ANSWER_OPTIONS.Replace("<p>", "");
        //            answerOpts = answerOpts.Replace("</p>", "");
        //            ASPxRadioButton rnd = new ASPxRadioButton();
        //            rnd.ID = "rdlOne" + count + "Ans" + ques.QUESTION_ID.ToString();
        //            //rnd.ID = "rdlOne" + ques.QUESTION_ID.ToString();
        //            rnd.Font.Size = Fsize;
        //            rnd.ForeColor = FColor;
        //            rnd.Font.Name = Fname;
        //            rnd.Border.BorderStyle = BorderStyle.None;
        //            rnd.GroupName = "rndGrp" + Convert.ToString(ques.QUESTION_ID);
        //            rnd.EncodeHtml = false;
        //            rnd.Value = answerOpts;
        //            rnd.Font.Bold = false;
        //            rnd.Text = answerOpts;
        //            tablecellAns.Controls.Add(rnd);
        //            tableRowAns.Cells.Add(tablecellAns);

        //            HtmlTableCell tablecellempty = new HtmlTableCell();
        //            tablecellempty.Width = "10";
        //            tableRowAns.Cells.Add(tablecellempty);
        //            if (ques.ANSWER_ALIGNSTYLE == "1Column" || ques.ANSWER_ALIGNSTYLE == "Vertical")
        //            {

        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "2Columns" && count % 2 == 0)
        //            {
        //                //tableRowAns.Cells.Add(tablecellAns);
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "3Columns" && count % 3 == 0)
        //            {
        //                //tableRowAns.Cells.Add(tablecellAns);
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }
        //            else if ((ques.ANSWER_ALIGNSTYLE == "4Columns" || ques.ANSWER_ALIGNSTYLE == "Horizontal") && count % 4 == 0)
        //            {
        //                //tableRowAns.Cells.Add(tablecellAns);
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }

        //            count = count + 1;
        //            if (ques.ANSWER_ALIGNSTYLE == "Horizontal" && ques.surveyAnswers.Count == 1)
        //            {
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "Horizontal" && ques.surveyAnswers.Count == 2 && count == 2)
        //            {
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "Horizontal" && ques.surveyAnswers.Count == 3 && count == 3)
        //            {
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }
        //        }
        //        if (status_flag == 0)
        //            tableAnsOpt.Rows.Add(tableRowAns);
        //    }


        //    HtmlTable tableother = new HtmlTable();
        //    if (ques.OTHER_ANS == 1)
        //    {

        //        HtmlTableRow tableRowOtherAns = new HtmlTableRow();
        //        HtmlTableCell tablecellOthEmpty = new HtmlTableCell();
        //        HtmlTableCell tablecellOtherAnslbl = new HtmlTableCell();
        //        tableRowOtherAns.Align = "Left";
        //        tablecellOtherAnslbl.Align = "Right";
        //        Label OtherAnslbl = new Label();
        //        OtherAnslbl.Font.Size = Fsize;
        //        OtherAnslbl.ForeColor = FColor;
        //        OtherAnslbl.Font.Name = Fname;
        //        OtherAnslbl.ID = "OtherAnslbl" + ques.QUESTION_ID.ToString();
        //        OtherAnslbl.Text = "Other";
        //        tablecellOtherAnslbl.Controls.Add(OtherAnslbl);
        //        HtmlTableCell tablecellOtherAnsDet = new HtmlTableCell();
        //        ASPxTextBox txtOthAns = new ASPxTextBox();
        //        txtOthAns.Font.Size = Fsize;
        //        txtOthAns.ForeColor = Color.Black;  //FColor;
        //        txtOthAns.Font.Name = Fname;
        //        txtOthAns.ID = "txtOthAns" + ques.QUESTION_ID.ToString();
        //        txtOthAns.Width = 150;
        //        tablecellOtherAnsDet.Controls.Add(txtOthAns);
        //        tablecellOtherAnsDet.ColSpan = 3;
        //        //tableRowOtherAns.Cells.Add(tablecellOthEmpty);
        //        tableRowOtherAns.Cells.Add(tablecellOtherAnslbl);
        //        tableRowOtherAns.Cells.Add(tablecellOtherAnsDet);
        //        tableother.Rows.Add(tableRowOtherAns);
        //        //tableAnsOpt.Rows.Add(tableRowOtherAns);
        //    }
        //    if (tableAnsOpt != null)
        //    {
        //        tablecellAns1.Controls.Add(tableAnsOpt);
        //        tableRowAns1.Cells.Add(tablecellAns1);
        //        TableQues.Rows.Add(tableRowAns1);
        //        if (ques.OTHER_ANS == 1)
        //        {
        //            HtmlTableRow tableothermainrow = new HtmlTableRow();
        //            HtmlTableCell tableothermaincell = new HtmlTableCell();
        //            tableothermaincell.ColSpan = 3;
        //            tableothermaincell.Controls.Add(tableother);
        //            tableothermainrow.Cells.Add(tableothermaincell);
        //            TableQues.Rows.Add(tableothermainrow);
        //        }

        //    }
        //    return TableQues;

        //}
        //private static HtmlTable QuestionTypeTwo(SurveyQuestion ques)
        //{
        //    //    HtmlTable TableQues = GetQuestion(ques);
        //    //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    //    tableRowAns.Cells.Add(tblCellEmpty);
        //    //    CheckBoxList chk = new CheckBoxList();
        //    //    chk.Font.Size = Fsize;
        //    //    chk.ForeColor = FColor;
        //    //    chk.Font.Name = Fname;
        //    //    chk.ID = "chkTwo" + ques.QUESTION_ID.ToString();
        //    //    chk.RepeatDirection = GetDirection(ques.ANSWER_ALIGNSTYLE);
        //    //    if (ques != null && ques.surveyAnswers != null)
        //    //    {
        //    //        //if (ques.RANDOM_ANSWERS == 1 && RandomReq == 1)
        //    //        //{
        //    //        //    ques.surveyAnswers = SortRandom(ques.surveyAnswers);
        //    //        //}
        //    //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //    //        {
        //    //            string answerOpt = AnsOpts.ANSWER_OPTIONS.Replace("<p>", "");
        //    //            answerOpt = answerOpt.Replace("</p>", "");
        //    //            ListItem itm = new ListItem(answerOpt, AnsOpts.ANSWER_ID.ToString());
        //    //            chk.Items.Add(itm);
        //    //        }
        //    //    }
        //    //    tablecellAns.Controls.Add(chk);
        //    //    tableRowAns.Cells.Add(tablecellAns);
        //    //    TableQues.Rows.Add(tableRowAns);
        //    //    if (ques.OTHER_ANS == 1)
        //    //    {
        //    //        HtmlTableRow tableRowOtherAns = new HtmlTableRow();
        //    //        HtmlTableCell tablecellothEmpty = new HtmlTableCell();
        //    //        HtmlTableCell tablecellOtherAnslbl = new HtmlTableCell();
        //    //        tableRowOtherAns.Align = "Left";
        //    //        tablecellOtherAnslbl.Align = "Right";
        //    //        Label OtherAnslbl = new Label();
        //    //        OtherAnslbl.Font.Size = Fsize;
        //    //        OtherAnslbl.ForeColor = FColor;
        //    //        OtherAnslbl.Font.Name = Fname;
        //    //        OtherAnslbl.ID = "OtherAnslbl" + ques.QUESTION_ID.ToString();
        //    //        OtherAnslbl.Text = "Other";
        //    //        tablecellOtherAnslbl.Controls.Add(OtherAnslbl);
        //    //        HtmlTableCell tablecellOtherAnsDet = new HtmlTableCell();
        //    //        ASPxTextBox txtOthAns = new ASPxTextBox();
        //    //        txtOthAns.Font.Size = Fsize;
        //    //        txtOthAns.ForeColor = Color.Black;  //FColor;
        //    //        txtOthAns.Font.Name = Fname;
        //    //        txtOthAns.ID = "txtOthAns" + ques.QUESTION_ID.ToString();
        //    //        txtOthAns.Width = 150;
        //    //        tablecellOtherAnsDet.Controls.Add(txtOthAns);
        //    //        tableRowOtherAns.Cells.Add(tablecellothEmpty);
        //    //        tableRowOtherAns.Cells.Add(tablecellOtherAnslbl);
        //    //        tableRowOtherAns.Cells.Add(tablecellOtherAnsDet);
        //    //        TableQues.Rows.Add(tableRowOtherAns);
        //    //    }

        //    //    return TableQues;


        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableRow tableRowAns1 = new HtmlTableRow();
        //    HtmlTableCell tablecellAns1 = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns1.Cells.Add(tblCellEmpty);
        //    HtmlTable tableAnsOpt = new HtmlTable();

        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        int count = 1;
        //        int status_flag = 0;
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            status_flag = 0;
        //            if (ques.ANSWER_ALIGNSTYLE == "1Column" || ques.ANSWER_ALIGNSTYLE == "Vertical")
        //            {
        //                tableRowAns = new HtmlTableRow();
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "2Columns" && (count - 1) % 2 == 0)
        //            {
        //                tableRowAns = new HtmlTableRow();
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "3Columns" && (count - 1) % 3 == 0)
        //            {
        //                tableRowAns = new HtmlTableRow();
        //            }
        //            else if ((ques.ANSWER_ALIGNSTYLE == "4Columns" || ques.ANSWER_ALIGNSTYLE == "Horizontal") && (count - 1) % 4 == 0)
        //            {
        //                tableRowAns = new HtmlTableRow();
        //            }
        //            HtmlTableCell tablecellAns = new HtmlTableCell();
        //            string answerOpts = AnsOpts.ANSWER_OPTIONS.Replace("<p>", "");
        //            answerOpts = answerOpts.Replace("</p>", "");
        //            ASPxCheckBox chk = new ASPxCheckBox();
        //            chk.ID = "rdlOne" + count + "Ans" + ques.QUESTION_ID.ToString();
        //            chk.Font.Size = Fsize;
        //            chk.ForeColor = FColor;
        //            chk.Font.Name = Fname;
        //            chk.Border.BorderStyle = BorderStyle.None;
        //            //chk.GroupName = "rndGrp" + Convert.ToString(ques.QUESTION_ID);
        //            chk.EncodeHtml = false;
        //            chk.Value = answerOpts;
        //            chk.Font.Bold = false;
        //            chk.Text = answerOpts;
        //            tablecellAns.Controls.Add(chk);
        //            tableRowAns.Cells.Add(tablecellAns);

        //            HtmlTableCell tablecellempty = new HtmlTableCell();
        //            tablecellempty.Width = "10";
        //            tableRowAns.Cells.Add(tablecellempty);
        //            if (ques.ANSWER_ALIGNSTYLE == "1Column" || ques.ANSWER_ALIGNSTYLE == "Vertical")
        //            {
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "2Columns" && count % 2 == 0)
        //            {
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "3Columns" && count % 3 == 0)
        //            {
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }
        //            else if ((ques.ANSWER_ALIGNSTYLE == "4Columns" || ques.ANSWER_ALIGNSTYLE == "Horizontal") && count % 4 == 0)
        //            {
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //            }
        //            count = count + 1;
        //            if (ques.ANSWER_ALIGNSTYLE == "Horizontal" && ques.surveyAnswers.Count == 1)
        //            {
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "Horizontal" && ques.surveyAnswers.Count == 2 && count == 2)
        //            {
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }
        //            else if (ques.ANSWER_ALIGNSTYLE == "Horizontal" && ques.surveyAnswers.Count == 3 && count == 3)
        //            {
        //                tableAnsOpt.Rows.Add(tableRowAns);
        //                status_flag = 1;
        //            }

        //        }
        //        if (status_flag == 0)
        //            tableAnsOpt.Rows.Add(tableRowAns);
        //    }
        //    HtmlTable tableother = new HtmlTable();
        //    if (ques.OTHER_ANS == 1)
        //    {
        //        HtmlTableRow tableRowOtherAns = new HtmlTableRow();
        //        HtmlTableCell tablecellOthEmpty = new HtmlTableCell();
        //        HtmlTableCell tablecellOtherAnslbl = new HtmlTableCell();
        //        tableRowOtherAns.Align = "Left";
        //        tablecellOtherAnslbl.Align = "Right";
        //        Label OtherAnslbl = new Label();
        //        //OtherAnslbl.Width = 50;
        //        OtherAnslbl.Font.Size = Fsize;
        //        OtherAnslbl.ForeColor = FColor;
        //        OtherAnslbl.Font.Name = Fname;
        //        OtherAnslbl.ID = "OtherAnslbl" + ques.QUESTION_ID.ToString();
        //        OtherAnslbl.Text = "Other";
        //        tablecellOtherAnslbl.Controls.Add(OtherAnslbl);
        //        HtmlTableCell tablecellOtherAnsDet = new HtmlTableCell();
        //        ASPxTextBox txtOthAns = new ASPxTextBox();
        //        txtOthAns.Font.Size = Fsize;
        //        txtOthAns.ForeColor = Color.Black;  //FColor;
        //        txtOthAns.Font.Name = Fname;
        //        txtOthAns.ID = "txtOthAns" + ques.QUESTION_ID.ToString();
        //        txtOthAns.Width = 150;
        //        tablecellOtherAnsDet.Controls.Add(txtOthAns);
        //        tablecellOtherAnsDet.ColSpan = 3;
        //        //tableRowOtherAns.Cells.Add(tablecellOthEmpty);
        //        tableRowOtherAns.Cells.Add(tablecellOtherAnslbl);
        //        tableRowOtherAns.Cells.Add(tablecellOtherAnsDet);
        //        tableother.Rows.Add(tableRowOtherAns);
        //        //tableAnsOpt.Rows.Add(tableRowOtherAns);
        //    }

        //    if (tableAnsOpt != null)
        //    {
        //        tablecellAns1.Controls.Add(tableAnsOpt);
        //        tableRowAns1.Cells.Add(tablecellAns1);
        //        TableQues.Rows.Add(tableRowAns1);
        //        if (ques.OTHER_ANS == 1)
        //        {
        //            HtmlTableRow tableothermainrow = new HtmlTableRow();
        //            HtmlTableCell tableothermaincell = new HtmlTableCell();
        //            tableothermaincell.ColSpan = 3;
        //            tableothermaincell.Controls.Add(tableother);
        //            tableothermainrow.Cells.Add(tableothermaincell);
        //            TableQues.Rows.Add(tableothermainrow);
        //        }

        //    }
        //    return TableQues;

        //}
        //private static HtmlTable QuestionTypeThree(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    ASPxComboBox cbo = new ASPxComboBox();
        //    cbo.Font.Size = Fsize;
        //    //cbo.ForeColor = //FColor;
        //    //cbo.Font.Name = Fname;
        //    cbo.ID = "cbThree" + ques.QUESTION_ID.ToString();
        //    cbo.EncodeHtml = false;
        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        //if (ques.RANDOM_ANSWERS == 1 && RandomReq == 1)
        //        //{
        //        //    ques.surveyAnswers = SortRandom(ques.surveyAnswers);
        //        //}
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            string answers = AnsOpts.ANSWER_OPTIONS.Replace("<p>", "");
        //            answers = answers.Replace("</p>", "");
        //            cbo.Items.Add(answers, AnsOpts.ANSWER_ID);
        //        }
        //    }
        //    tablecellAns.Controls.Add(cbo);
        //    tableRowAns.Cells.Add(tablecellAns);
        //    if (ques.OTHER_ANS == 1)
        //    {
        //        HtmlTableCell tablecellOtherAnslbl = new HtmlTableCell();
        //        HtmlTableCell tablecellothEmpty = new HtmlTableCell();
        //        Label OtherAnslbl = new Label();
        //        OtherAnslbl.Font.Size = Fsize;
        //        OtherAnslbl.ForeColor = FColor;
        //        OtherAnslbl.Font.Name = Fname;
        //        OtherAnslbl.ID = "OtherAnslbl" + ques.QUESTION_ID.ToString();
        //        OtherAnslbl.Text = "Other";
        //        tablecellOtherAnslbl.Controls.Add(OtherAnslbl);
        //        HtmlTableCell tablecellOtherAnsDet = new HtmlTableCell();
        //        tablecellOtherAnsDet.Align = "Left";
        //        ASPxTextBox txtOthAns = new ASPxTextBox();
        //        txtOthAns.Font.Size = Fsize;
        //        txtOthAns.ForeColor = Color.Black; //FColor;
        //        txtOthAns.Font.Name = Fname;
        //        txtOthAns.ID = "txtOthAns" + ques.QUESTION_ID.ToString();
        //        txtOthAns.Width = 150;
        //        tablecellOtherAnsDet.Controls.Add(txtOthAns);
        //        tableRowAns.Cells.Add(tablecellothEmpty);
        //        tableRowAns.Cells.Add(tablecellOtherAnslbl);
        //        tableRowAns.Cells.Add(tablecellOtherAnsDet);
        //    }
        //    TableQues.Rows.Add(tableRowAns);
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypefour(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    ASPxRadioButtonList rbl = new ASPxRadioButtonList();
        //    rbl.Font.Size = Fsize;
        //    rbl.ForeColor = FColor;
        //    rbl.Font.Name = Fname;
        //    rbl.ID = "rblFour" + ques.QUESTION_ID.ToString();
        //    rbl.RepeatDirection = GetDirection(ques.ANSWER_ALIGNSTYLE);
        //    rbl.Border.BorderStyle = BorderStyle.None;
        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            rbl.Items.Add(AnsOpts.ANSWER_OPTIONS, AnsOpts.ANSWER_ID);
        //        }
        //    }
        //    //rbl.Items.Add("Yes", "Yes");
        //    //rbl.Items.Add("No", "No");
        //    rbl.ItemSpacing = Unit.Pixel(10);
        //    rbl.EncodeHtml = false;
        //    tablecellAns.Controls.Add(rbl);
        //    tableRowAns.Cells.Add(tablecellAns);
        //    TableQues.Rows.Add(tableRowAns);
        //    if (ques.OTHER_ANS == 1)
        //    {
        //        HtmlTable tableother = new HtmlTable();
        //        HtmlTableRow tableRowOtherAns = new HtmlTableRow();
        //        HtmlTableCell tablecellothEmpty = new HtmlTableCell();
        //        HtmlTableCell tablecellOtherAnslbl = new HtmlTableCell();
        //        tablecellOtherAnslbl.Align = "Right";
        //        Label OtherAnslbl = new Label();
        //        OtherAnslbl.Font.Size = Fsize;
        //        OtherAnslbl.ForeColor = FColor;
        //        OtherAnslbl.Font.Name = Fname;
        //        OtherAnslbl.ID = "OtherAnslbl" + ques.QUESTION_ID.ToString();
        //        OtherAnslbl.Text = "Other";
        //        tablecellOtherAnslbl.Controls.Add(OtherAnslbl);
        //        HtmlTableCell tablecellOtherAnsDet = new HtmlTableCell();
        //        ASPxTextBox txtOthAns = new ASPxTextBox();
        //        txtOthAns.Font.Size = Fsize;
        //        //txtOthAns.ForeColor = FColor;
        //        txtOthAns.Font.Name = Fname;
        //        txtOthAns.ID = "txtOthAns" + ques.QUESTION_ID.ToString();
        //        txtOthAns.Width = 150;
        //        tablecellOtherAnsDet.Controls.Add(txtOthAns);
        //        tableRowOtherAns.Cells.Add(tablecellothEmpty);
        //        tableRowOtherAns.Cells.Add(tablecellOtherAnslbl);
        //        tableRowOtherAns.Cells.Add(tablecellOtherAnsDet);
        //        tableother.Rows.Add(tableRowOtherAns);
        //        //TableQues.Rows.Add(tableRowOtherAns);


        //        HtmlTableRow tableothermainrow = new HtmlTableRow();
        //        HtmlTableCell tableothermaincell = new HtmlTableCell();
        //        tableothermaincell.ColSpan = 3;
        //        tableothermaincell.Controls.Add(tableother);
        //        tableothermainrow.Cells.Add(tableothermaincell);
        //        TableQues.Rows.Add(tableothermainrow);


        //    }
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypefive(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    ASPxMemo mem = new ASPxMemo();
        //    mem.Font.Size = Fsize;
        //    mem.ForeColor = Color.Black; // FColor;
        //    mem.Font.Name = Fname;
        //    mem.ID = "memFive" + ques.QUESTION_ID.ToString();
        //    //mem.Width = 350;
        //    mem.Width = 530;
        //    mem.Height = 120;
        //    HtmlTableCell tblcellValidator = new HtmlTableCell();
        //    //Security sec = new Security();
        //    ValidationCore validcore = new ValidationCore();
        //    validcore.ValidateQuestion(Convert.ToString(mem.ID), Convert.ToInt16(ques.QUESTION_TYPE), tblcellValidator, 0, ques.CHAR_LIMIT);
        //    tablecellAns.Controls.Add(mem);
        //    tableRowAns.Cells.Add(tablecellAns);
        //    tableRowAns.Cells.Add(tblcellValidator);
        //    TableQues.Rows.Add(tableRowAns);
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypesix(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    ASPxTextBox txt = new ASPxTextBox();
        //    txt.ID = "txtSix" + ques.QUESTION_ID.ToString();
        //    //txt.Width = 200;
        //    txt.Width = 600;
        //    txt.Height = 25;
        //    txt.Font.Size = Fsize;
        //    txt.ForeColor = Color.Black;   //FColor;
        //    txt.Font.Name = Fname;
        //    HtmlTableCell tblcellValidator = new HtmlTableCell();
        //    ValidationCore validcore = new ValidationCore();
        //    validcore.ValidateQuestion(Convert.ToString(txt.ID), Convert.ToInt16(ques.QUESTION_TYPE), tblcellValidator, 0, ques.CHAR_LIMIT);

        //    tablecellAns.Controls.Add(txt);
        //    tableRowAns.Cells.Add(tablecellAns);
        //    tableRowAns.Cells.Add(tblcellValidator);
        //    TableQues.Rows.Add(tableRowAns);
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypeseven(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    ASPxTextBox txt = new ASPxTextBox();
        //    txt.ID = "txtSeven" + ques.QUESTION_ID.ToString();
        //    //txt.Width = 200;
        //    txt.Width = 150;
        //    txt.Height = 25;
        //    txt.Font.Size = Fsize;
        //    txt.ForeColor = Color.Black; //FColor;
        //    txt.Font.Name = Fname;
        //    HtmlTableCell tblcellValidator = new HtmlTableCell();
        //    ValidationCore validcore = new ValidationCore();
        //    validcore.ValidateQuestion(Convert.ToString(txt.ID), Convert.ToInt16(ques.QUESTION_TYPE), tblcellValidator, 0, 0);
        //    tablecellAns.Controls.Add(txt);
        //    tableRowAns.Cells.Add(tablecellAns);
        //    tableRowAns.Cells.Add(tblcellValidator);
        //    TableQues.Rows.Add(tableRowAns);
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypeeight(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    ASPxTextBox txt = new ASPxTextBox();
        //    txt.ID = "txtEight" + ques.QUESTION_ID.ToString();
        //    //txt.Width = 200;
        //    txt.Width = 300;
        //    txt.Height = 25;
        //    txt.Font.Size = Fsize;
        //    txt.ForeColor = Color.Black; //FColor;
        //    txt.Font.Name = Fname;
        //    HtmlTableCell tblcellValidator = new HtmlTableCell();
        //    ValidationCore validcore = new ValidationCore();
        //    validcore.ValidateQuestion(Convert.ToString(txt.ID), Convert.ToInt16(ques.QUESTION_TYPE), tblcellValidator, 0, 0);
        //    tablecellAns.Controls.Add(txt);
        //    tableRowAns.Cells.Add(tablecellAns);
        //    tableRowAns.Cells.Add(tblcellValidator);
        //    TableQues.Rows.Add(tableRowAns);
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypenine(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    HtmlTable tableAns = new HtmlTable();
        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            HtmlTableRow tableAnsRow = new HtmlTableRow();
        //            HtmlTableCell tableAnscell = new HtmlTableCell();
        //            HtmlTableCell tableAnstxtcell = new HtmlTableCell();
        //            string answers = AnsOpts.ANSWER_OPTIONS.Replace("<p>", "");
        //            answers = answers.Replace("</p>", "");
        //            Label AnswerLabel = new Label();
        //            AnswerLabel.Font.Size = Fsize;
        //            AnswerLabel.ForeColor = FColor;
        //            AnswerLabel.Font.Name = Fname;
        //            AnswerLabel.ID = "lblAnswer" + AnsOpts.ANSWER_ID.ToString();
        //            //AnswerLabel.Text = AnsOpts.ANSWER_OPTIONS;
        //            AnswerLabel.Text = answers;
        //            tableAnscell.Controls.Add(AnswerLabel);
        //            tableAnsRow.Cells.Add(tableAnscell);
        //            ASPxTextBox txt = new ASPxTextBox();
        //            txt.Font.Size = Fsize;
        //            txt.ForeColor = Color.Black;  //FColor;
        //            txt.Font.Name = Fname;
        //            txt.ID = "txtNine" + AnsOpts.ANSWER_ID.ToString();
        //            txt.Width = 600;
        //            txt.Height = 25;
        //            //txt.Size = 25;
        //            tableAnstxtcell.Controls.Add(txt);

        //            //Lines Added by sandeep for validation of Comment Box with One or More lines with prompt
        //            HtmlTableCell tblcellValidator = new HtmlTableCell();
        //            ValidationCore validcore = new ValidationCore();
        //            validcore.ValidateQuestion(Convert.ToString(txt.ID), Convert.ToInt16(ques.QUESTION_TYPE), tblcellValidator, 0, ques.CHAR_LIMIT);
        //            tableAnsRow.Cells.Add(tableAnstxtcell);
        //            //line added by Sandeep
        //            tableAnsRow.Cells.Add(tblcellValidator);
        //            //line ended
        //            tableAns.Rows.Add(tableAnsRow);
        //        }
        //    }
        //    if (tableAns != null)
        //    {
        //        tablecellAns.Controls.Add(tableAns);
        //        tableRowAns.Cells.Add(tablecellAns);
        //        TableQues.Rows.Add(tableRowAns);
        //    }
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypeTen(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    HtmlTable tableAnsOp = GenrateMatrixQues(ques, 10);
        //    if (tableAnsOp != null)
        //    {
        //        tablecellAns.Controls.Add(tableAnsOp);
        //        tableRowAns.Cells.Add(tablecellAns);
        //        TableQues.Rows.Add(tableRowAns);
        //    }
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypeEleven(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    HtmlTable tableAnsOpt = GenrateMatrixQues(ques, 11);
        //    if (tableAnsOpt != null)
        //    {
        //        tablecellAns.Controls.Add(tableAnsOpt);
        //        tableRowAns.Cells.Add(tablecellAns);
        //        TableQues.Rows.Add(tableRowAns);
        //    }
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypeTwelve(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    HtmlTable tableAns = new HtmlTable();
        //    ArrayList colHead = new ArrayList();
        //    ArrayList ColSubHead = new ArrayList();
        //    ArrayList rowHead = new ArrayList();
        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            string ansOpt = AnsOpts.ANSWER_OPTIONS;
        //            int pos = ansOpt.IndexOf("</p>");
        //            if (pos > 1)
        //            {
        //                colHead.Add(ansOpt.Substring(0, pos + 4));
        //                ansOpt = ansOpt.Substring(pos + 4);
        //                int AnsOptStrPos = ansOpt.IndexOf("<p>");
        //                int AnsOptEndPos = ansOpt.IndexOf("</p>");
        //                if (AnsOptStrPos > 0 && AnsOptEndPos > 4)
        //                {
        //                    rowHead.Add(ansOpt.Substring(AnsOptStrPos, AnsOptEndPos - AnsOptStrPos + 4));
        //                    ansOpt = ansOpt.Substring(AnsOptEndPos - AnsOptStrPos + 4);
        //                }
        //                int AnsExtOptStrPos = ansOpt.IndexOf("<p>");
        //                int AnsEXtOptEndPos = ansOpt.IndexOf("</p>");
        //                if (AnsExtOptStrPos > 0 && AnsEXtOptEndPos > 4)
        //                {
        //                    ColSubHead.Add(ansOpt.Substring(AnsExtOptStrPos, AnsEXtOptEndPos - AnsExtOptStrPos + 4));
        //                }
        //            }
        //        }
        //    }
        //    if (colHead.Count > 0)
        //        colHead = RemoveDuplicateItems(colHead);
        //    if (ColSubHead.Count > 0)
        //        ColSubHead = RemoveDuplicateItems(ColSubHead);
        //    if (rowHead.Count > 0)
        //        rowHead = RemoveDuplicateItems(rowHead);
        //    HtmlTableRow tableRowHead = new HtmlTableRow();
        //    HtmlTableCell tableRowHeadCor = new HtmlTableCell();
        //    tableRowHead.Cells.Add(tableRowHeadCor);
        //    foreach (object obj in colHead)
        //    {
        //        HtmlTableCell tableColHeadcell = new HtmlTableCell();
        //        tableColHeadcell.ColSpan = ColSubHead.Count + 1;
        //        tableColHeadcell.Align = "Center";
        //        tableColHeadcell.Attributes["Style"] = "padding:8px";
        //        Label lbl = new Label();
        //        lbl.Font.Size = Fsize;
        //        lbl.ForeColor = FColor;
        //        lbl.Font.Name = Fname;
        //        lbl.Text = obj.ToString();
        //        tableColHeadcell.Controls.Add(lbl);
        //        tableRowHead.Cells.Add(tableColHeadcell);
        //    }
        //    if (colHead.Count > 0)
        //        tableAns.Rows.Add(tableRowHead);
        //    if (ColSubHead.Count > 0)
        //    {
        //        HtmlTableRow tablesubHeadRow = new HtmlTableRow();
        //        HtmlTableCell tableColSubHeadcellCor = new HtmlTableCell();
        //        tablesubHeadRow.Cells.Add(tableColSubHeadcellCor);
        //        foreach (object ob in colHead)
        //        {
        //            HtmlTableCell tableColSubHeadGrpGapCell = new HtmlTableCell();
        //            tablesubHeadRow.Cells.Add(tableColSubHeadGrpGapCell);
        //            foreach (object obj in ColSubHead)
        //            {
        //                HtmlTableCell tableColSubHeadcell = new HtmlTableCell();
        //                tableColSubHeadcell.Align = "Center";
        //                tableColSubHeadcell.Attributes["Style"] = "padding:4px";
        //                Label lbl = new Label();
        //                lbl.Font.Size = Fsize;
        //                lbl.ForeColor = FColor;
        //                lbl.Font.Name = Fname;
        //                lbl.Text = obj.ToString();
        //                tableColSubHeadcell.Controls.Add(lbl);
        //                tablesubHeadRow.Cells.Add(tableColSubHeadcell);
        //            }

        //        }
        //        tableAns.Rows.Add(tablesubHeadRow);
        //    }
        //    int RowCell = 0, RowHed = 0, CellGrp = 0, Cellcunt = 0;
        //    HtmlTableRow tableAnsRowop = new HtmlTableRow();
        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        foreach (SurveyAnswers AnsOp in ques.surveyAnswers)
        //        {
        //            if (RowCell == 0)
        //            {
        //                tableAnsRowop = new HtmlTableRow();
        //                HtmlTableCell tblrowHeadCell = new HtmlTableCell();
        //                Label lbl = new Label();
        //                lbl.Font.Size = Fsize;
        //                lbl.ForeColor = FColor;
        //                lbl.Font.Name = Fname;
        //                lbl.Text = rowHead[RowHed].ToString();
        //                tblrowHeadCell.Controls.Add(lbl);
        //                tableAnsRowop.Cells.Add(tblrowHeadCell);
        //                RowHed++;
        //            }
        //            if (Cellcunt == 0)
        //            {
        //                HtmlTableCell GrpGapcell = new HtmlTableCell();
        //                tableAnsRowop.Cells.Add(GrpGapcell);
        //            }
        //            HtmlTableCell tableRowcell = new HtmlTableCell();
        //            tableRowcell.Align = "Center";
        //            ASPxRadioButton rnd = new ASPxRadioButton();
        //            rnd.Font.Size = Fsize;
        //            rnd.ForeColor = FColor;
        //            rnd.Font.Name = Fname;
        //            rnd.ID = "rbTwelve" + AnsOp.ANSWER_ID.ToString();
        //            rnd.Value = AnsOp.ANSWER_ID;
        //            rnd.GroupName = "rbGroupTwelve" + Convert.ToString(ques.QUESTION_ID) + CellGrp.ToString();
        //            tableRowcell.Controls.Add(rnd);
        //            tableAnsRowop.Cells.Add(tableRowcell);
        //            RowCell++;
        //            Cellcunt++;
        //            if (RowCell == ColSubHead.Count * colHead.Count)
        //            {
        //                RowCell = 0;
        //                tableAns.Rows.Add(tableAnsRowop);
        //            }
        //            if (Cellcunt == ColSubHead.Count)
        //            {
        //                Cellcunt = 0;
        //                CellGrp++;
        //            }
        //        }
        //    }
        //    if (tableAns != null)
        //    {
        //        tablecellAns.Controls.Add(tableAns);
        //        tableRowAns.Cells.Add(tablecellAns);
        //        TableQues.Rows.Add(tableRowAns);
        //    }
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypethirteen(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    HtmlTable tableAns = new HtmlTable();
        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            HtmlTableRow tableAnsRow = new HtmlTableRow();
        //            HtmlTableCell tableAnscell = new HtmlTableCell();
        //            HtmlTableCell tableAnstxtcell = new HtmlTableCell();
        //            string answers = AnsOpts.ANSWER_OPTIONS.Replace("<p>", "");
        //            answers = answers.Replace("</p>", "");
        //            Label AnswerLabel = new Label();
        //            AnswerLabel.Font.Size = Fsize;
        //            AnswerLabel.ForeColor = FColor;
        //            AnswerLabel.Font.Name = Fname;
        //            AnswerLabel.ID = "lblAnswer" + AnsOpts.ANSWER_ID.ToString();
        //            AnswerLabel.Text = AnsOpts.ANSWER_OPTIONS;
        //            tableAnscell.Controls.Add(AnswerLabel);
        //            tableAnsRow.Cells.Add(tableAnscell);
        //            ASPxTextBox txt = new ASPxTextBox();
        //            txt.Font.Size = Fsize;
        //            txt.ForeColor = Color.Black;  //FColor;
        //            txt.Font.Name = Fname;
        //            txt.ID = "txtThirteen" + AnsOpts.ANSWER_ID.ToString();
        //            txt.Width = 50;
        //            txt.Height = 25;
        //            HtmlTableCell tblcellValidator = new HtmlTableCell();
        //            ValidationCore validcore = new ValidationCore();
        //            validcore.ValidateQuestion(Convert.ToString(txt.ID), Convert.ToInt16(ques.QUESTION_TYPE), tblcellValidator, 0, ques.CHAR_LIMIT);
        //            tableAnstxtcell.Controls.Add(txt);
        //            tableAnsRow.Cells.Add(tableAnstxtcell);
        //            tableAnsRow.Cells.Add(tblcellValidator);
        //            tableAns.Rows.Add(tableAnsRow);

        //        }
        //    }
        //    if (tableAns != null)
        //    {
        //        tablecellAns.Controls.Add(tableAns);
        //        tableRowAns.Cells.Add(tablecellAns);
        //        TableQues.Rows.Add(tableRowAns);
        //    }
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypefourteen(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    HtmlTable datetbl = new HtmlTable();
        //    HtmlTableRow datetableRow = new HtmlTableRow();
        //    HtmlTableCell datetabledatecell = new HtmlTableCell();
        //    ASPxDateEdit ded = new ASPxDateEdit();
        //    ded.Font.Size = Fsize;
        //    ded.ForeColor = Color.Black; //FColor;
        //    ded.Font.Name = Fname;
        //    ded.ID = "dedFourteen" + ques.QUESTION_ID.ToString();
        //    datetabledatecell.Controls.Add(ded);
        //    datetableRow.Cells.Add(datetabledatecell);
        //    HtmlTableCell datetableHourscell = new HtmlTableCell();
        //    ASPxComboBox cboHours = new ASPxComboBox();
        //    cboHours.Width = Unit.Pixel(40);
        //    cboHours.ID = "cboHour" + ques.QUESTION_ID.ToString();
        //    cboHours.SelectedIndex = 0;
        //    for (int i = 0; i < 12; i++)
        //    {
        //        cboHours.Items.Add(Convert.ToString(i), i);
        //    }
        //    datetableHourscell.Controls.Add(cboHours);
        //    datetableRow.Cells.Add(datetableHourscell);

        //    HtmlTableCell datetableMinscell = new HtmlTableCell();
        //    ASPxComboBox cboMins = new ASPxComboBox();
        //    cboMins.Width = Unit.Pixel(40);
        //    cboMins.ID = "cboMin" + ques.QUESTION_ID.ToString();
        //    cboMins.SelectedIndex = 0;
        //    for (int i = 0; i < 60; i++)
        //    {
        //        cboMins.Items.Add(Convert.ToString(i), i);
        //    }
        //    datetableMinscell.Controls.Add(cboMins);
        //    datetableRow.Cells.Add(datetableMinscell);

        //    //HtmlTableCell datetableSeccell = new HtmlTableCell();
        //    //ASPxComboBox cboSecs = new ASPxComboBox();
        //    //cboSecs.Width = Unit.Pixel(40);
        //    //cboSecs.ID = "cboSec" + ques.QUESTION_ID.ToString();
        //    //cboSecs.SelectedIndex = 0;
        //    //for (int i = 0; i < 60; i++)
        //    //{
        //    //    cboSecs.Items.Add(Convert.ToString(i), i);
        //    //}
        //    //datetableSeccell.Controls.Add(cboSecs);
        //    //datetableRow.Cells.Add(datetableSeccell);

        //    HtmlTableCell datetableapcell = new HtmlTableCell();
        //    ASPxComboBox cboaps = new ASPxComboBox();
        //    cboaps.Width = Unit.Pixel(50);
        //    cboaps.SelectedIndex = 0;
        //    cboaps.ID = "cboaps" + ques.QUESTION_ID.ToString();
        //    cboaps.Items.Add("AM", "AM");
        //    cboaps.Items.Add("PM", "PM");
        //    datetableapcell.Controls.Add(cboaps);
        //    datetableRow.Cells.Add(datetableapcell);

        //    datetbl.Rows.Add(datetableRow);
        //    tablecellAns.Controls.Add(datetbl);
        //    tableRowAns.Cells.Add(tablecellAns);
        //    TableQues.Rows.Add(tableRowAns);
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypefifteen(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    HtmlTable tableAns = new HtmlTable();
        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            HtmlTableRow tableAnsRow = new HtmlTableRow();
        //            HtmlTableCell tableAnscell = new HtmlTableCell();
        //            HtmlTableCell tableAnstxtcell = new HtmlTableCell();
        //            string answers = AnsOpts.ANSWER_OPTIONS.Replace("<p>", "");
        //            answers = answers.Replace("</p>", "");
        //            Label AnswerLabel = new Label();
        //            AnswerLabel.Font.Size = Fsize;
        //            AnswerLabel.ForeColor = FColor;
        //            AnswerLabel.Font.Name = Fname;
        //            AnswerLabel.ID = "lblAnswer" + AnsOpts.ANSWER_ID.ToString();
        //            AnswerLabel.Text = AnsOpts.ANSWER_OPTIONS;
        //            tableAnscell.Controls.Add(AnswerLabel);
        //            tableAnsRow.Cells.Add(tableAnscell);
        //            ASPxTextBox txt = new ASPxTextBox();
        //            txt.Font.Size = Fsize;
        //            txt.ForeColor = Color.Black;   //FColor;
        //            txt.Font.Name = Fname;
        //            txt.ID = "txtFifteen" + AnsOpts.ANSWER_ID.ToString();
        //            txt.Width = 50;
        //            txt.Height = 25;
        //            HtmlTableCell tblcellValidator = new HtmlTableCell();
        //            ValidationCore validcore = new ValidationCore();
        //            validcore.ValidateQuestion(Convert.ToString(txt.ID), Convert.ToInt16(ques.QUESTION_TYPE), tblcellValidator, ques.surveyAnswers.Count, 0);

        //            tableAnstxtcell.Controls.Add(txt);
        //            tableAnsRow.Cells.Add(tableAnstxtcell);
        //            tableAnsRow.Cells.Add(tblcellValidator);
        //            tableAns.Rows.Add(tableAnsRow);
        //        }
        //    }
        //    if (tableAns != null)
        //    {
        //        tablecellAns.Controls.Add(tableAns);
        //        tableRowAns.Cells.Add(tablecellAns);
        //        TableQues.Rows.Add(tableRowAns);
        //    }
        //    return TableQues;
        //}
        //private static HtmlTable QuestionTypeninteen(SurveyQuestion ques)
        //{
        //    HtmlTable tablename = new HtmlTable();
        //    HtmlTableRow tableRowQues = new HtmlTableRow();
        //    HtmlTableCell tablecellQues = new HtmlTableCell();
        //    HtmlTableCell tableNocell = new HtmlTableCell();
        //    ASPxImage img = new ASPxImage();
        //    img.ID = "ImgV" + ques.QUESTION_ID.ToString();
        //    string url = "~/" + ques.QUESTION_LABEL.Substring(ques.QUESTION_LABEL.IndexOf("OSM") + 4);
        //    img.ImageUrl = url;
        //    img.Height = 100;
        //    img.Width = 100;
        //    tablecellQues.Controls.Add(img);
        //    tableRowQues.Cells.Add(tableNocell);
        //    tableRowQues.Cells.Add(tablecellQues);
        //    tablename.Rows.Add(tableRowQues);
        //    return tablename;
        //}
        //private static HtmlTable GenrateMatrixQues(SurveyQuestion ques, int QuestionType)
        //{
        //    HtmlTable tableAns = new HtmlTable();
        //    tableAns.CellPadding = 5;
        //    HtmlTableCell tblAnsCorCell = new HtmlTableCell();
        //    HtmlTableRow tableAnsRow = new HtmlTableRow();
        //    tableAnsRow.Cells.Add(tblAnsCorCell);
        //    ArrayList colHeads = new ArrayList();
        //    ArrayList rowHeads = new ArrayList();
        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            string ansOpt = AnsOpts.ANSWER_OPTIONS;
        //            int pos = ansOpt.IndexOf("</p>");
        //            if (pos > 1)
        //            {
        //                string clHead = ansOpt.Substring(0, pos + 4);
        //                rowHeads.Add(clHead);
        //                ansOpt = ansOpt.Substring(pos + 4);
        //                int AnsOptStrPos = ansOpt.IndexOf("<p>");
        //                int AnsOptEndPos = ansOpt.IndexOf("</p>");
        //                if (AnsOptStrPos > 0 && AnsOptEndPos > 4)
        //                {
        //                    colHeads.Add(ansOpt.Substring(AnsOptStrPos, AnsOptEndPos - AnsOptStrPos + 4));
        //                }
        //            }
        //        }
        //    }
        //    if (colHeads.Count > 0)
        //        colHeads = RemoveDuplicateItems(colHeads);
        //    if (rowHeads.Count > 0)
        //        rowHeads = RemoveDuplicateItems(rowHeads);
        //    foreach (object obj in colHeads)
        //    {
        //        HtmlTableCell tableColHeadcell = new HtmlTableCell();
        //        //tableColHeadcell.Style = "padding:10";
        //        tableColHeadcell.Align = "Center";
        //        tableColHeadcell.Attributes["Style"] = "padding:5px";
        //        Label lbl = new Label();
        //        lbl.Font.Size = Fsize;
        //        lbl.ForeColor = FColor;
        //        lbl.Font.Name = Fname;
        //        lbl.Text = obj.ToString();
        //        tableColHeadcell.Controls.Add(lbl);
        //        tableAnsRow.Cells.Add(tableColHeadcell);
        //    }
        //    tableAns.Rows.Add(tableAnsRow);
        //    int l = 0, j = 0;
        //    HtmlTableRow tableAnsRowop = new HtmlTableRow();
        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        foreach (SurveyAnswers AnsOp in ques.surveyAnswers)
        //        {

        //            if (l == 0)
        //            {
        //                tableAnsRowop = new HtmlTableRow();
        //                HtmlTableCell tblrowHeadCell = new HtmlTableCell();
        //                Label lbl = new Label();
        //                lbl.Font.Size = Fsize;
        //                lbl.ForeColor = FColor;
        //                lbl.Font.Name = Fname;
        //                lbl.Text = rowHeads[j].ToString();
        //                tblrowHeadCell.Controls.Add(lbl);
        //                tableAnsRowop.Cells.Add(tblrowHeadCell);
        //                j++;
        //            }
        //            HtmlTableCell tableRowcell = new HtmlTableCell();
        //            tableRowcell.Align = "Center";
        //            if (QuestionType == 10)
        //            {
        //                ASPxRadioButton rnd = new ASPxRadioButton();
        //                rnd.Font.Size = Fsize;
        //                rnd.ForeColor = FColor;
        //                rnd.Font.Name = Fname;
        //                rnd.ID = "rb" + AnsOp.ANSWER_ID.ToString();
        //                rnd.GroupName = "rndGrp" + Convert.ToString(ques.QUESTION_ID) + j.ToString();
        //                rnd.Value = AnsOp.ANSWER_ID;
        //                tableRowcell.Controls.Add(rnd);
        //            }
        //            else if (QuestionType == 11)
        //            {
        //                ASPxCheckBox chk = new ASPxCheckBox();
        //                chk.Font.Size = Fsize;
        //                chk.ForeColor = FColor;
        //                chk.Font.Name = Fname;
        //                chk.ID = "Chk" + AnsOp.ANSWER_ID.ToString();
        //                chk.Value = AnsOp.ANSWER_ID;
        //                tableRowcell.Controls.Add(chk);
        //            }
        //            tableAnsRowop.Cells.Add(tableRowcell);
        //            l++;
        //            if (l == colHeads.Count)
        //            {
        //                l = 0;
        //                tableAns.Rows.Add(tableAnsRowop);
        //            }
        //        }
        //    }
        //    return tableAns;
        //}

        //private static HtmlTable questionTypetwenty(SurveyQuestion ques)
        //{
        //    HtmlTable TableQues = GetQuestion(ques);
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    HtmlTable tableAns = new HtmlTable();
        //    if (ques != null && ques.surveyAnswers != null)
        //    {
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            HtmlTableRow tableAnsRow = new HtmlTableRow();
        //            HtmlTableCell tableAnscell = new HtmlTableCell();
        //            HtmlTableCell tableAnstxtcell = new HtmlTableCell();
        //            //string answers = AnsOpts.ANSWER_OPTIONS.Replace("<p>", "");
        //            //answers = answers.Replace("</p>", "");
        //            Label AnswerLabel = new Label();
        //            AnswerLabel.Font.Size = Fsize;
        //            AnswerLabel.ForeColor = FColor;
        //            AnswerLabel.Font.Name = Fname;
        //            string answers = AnsOpts.ANSWER_OPTIONS;
        //            // answers = answers.Replace("</p>", "");
        //            if (AnsOpts.RESPONSE_REQUIRED == 1)
        //            {
        //                answers = "*" + answers;
        //            }
        //            else
        //            {
        //                answers = " " + answers;
        //            }
        //            AnswerLabel.ID = "lblAnswer" + AnsOpts.ANSWER_ID.ToString();
        //            // AnswerLabel.Text = AnsOpts.ANSWER_OPTIONS;
        //            AnswerLabel.Text = answers;
        //            tableAnscell.Controls.Add(AnswerLabel);
        //            tableAnsRow.Cells.Add(tableAnscell);
        //            ASPxTextBox txt = new ASPxTextBox();
        //            txt.Font.Size = Fsize;
        //            txt.ForeColor = Color.Black; //FColor;
        //            txt.Font.Name = Fname;
        //            txt.ID = "txttwenty" + AnsOpts.ANSWER_ID.ToString();
        //            //txt.Width = 150;
        //            txt.Width = 300;
        //            txt.Height = 25;
        //            HtmlTableCell tblcellValidator = new HtmlTableCell();
        //            if (AnsOpts.DEMOGRAPIC_BLOCKID == 10)
        //            {
        //                ValidationCore validcore = new ValidationCore();
        //                validcore.ValidateQuestion(Convert.ToString(txt.ID), 8, tblcellValidator, 0, 0);
        //            }
        //            tableAnstxtcell.Controls.Add(txt);
        //            tableAnsRow.Cells.Add(tableAnstxtcell);
        //            tableAnsRow.Cells.Add(tblcellValidator);
        //            tableAns.Rows.Add(tableAnsRow);
        //        }
        //    }
        //    if (tableAns != null)
        //    {
        //        tablecellAns.Controls.Add(tableAns);
        //        tableRowAns.Cells.Add(tablecellAns);
        //        TableQues.Rows.Add(tableRowAns);
        //    }
        //    return TableQues;
        //}
        //public static Label SetHTMLTextForSurveyIntro(Label lbl, string FontName, string FontSize, string FontColor)
        //{
        //    try
        //    {
        //        Fsize = FontUnit.Small;
        //        FColor = Color.Black;
        //        Fname = "Trebuchet MS";
        //        SetFontName(FontName);
        //        SetFontSize(FontSize);
        //        SetFontColor(FontColor);
        //        lbl.Font.Size = SetFontSize(FontSize);
        //        lbl.ForeColor = SetFontColor(FontColor);
        //        lbl.Font.Name = SetFontName(FontName);
        //        return lbl;
        //    }
        //    catch (Exception ex)
        //    {

        //        return lbl;
        //    }

        //}
        //public static ASPxLabel SetHTMLTextForSurveyIntro1(ASPxLabel lbl, string FontName, string FontSize, string FontColor)
        //{
        //    try
        //    {
        //        Fsize = FontUnit.Small;
        //        FColor = Color.Black;
        //        Fname = "Trebuchet MS";
        //        lbl.Font.Size = SetFontSize(FontSize);
        //        lbl.ForeColor = SetFontColor(FontColor);
        //        lbl.Font.Name = SetFontName(FontName);
        //        return lbl;
        //    }
        //    catch (Exception ex)
        //    {

        //        return lbl;
        //    }

        //}
        //public static HtmlTable HTMLText(SurveyQuestion ques, string FontName, string FontSize, string FontColor)
        //{
        //    try
        //    {
        //        Fsize = FontUnit.Small;
        //        FColor = Color.Black;
        //        Fname = "Trebuchet MS";
        //        SetFontName(FontName);
        //        SetFontSize(FontSize);
        //        SetFontColor(FontColor);
        //        HtmlTable tblQues = new HtmlTable();
        //        switch (ques.QUESTION_TYPE)
        //        {
        //            case 1:
        //                tblQues = QuestionTypeOne(ques);
        //                break;
        //            case 2:
        //                tblQues = QuestionTypeTwo(ques);
        //                break;
        //            case 3:
        //                tblQues = QuestionTypeThree(ques);
        //                break;
        //            case 4:
        //                tblQues = QuestionTypefour(ques);
        //                break;
        //            case 5:
        //                tblQues = QuestionTypefive(ques);
        //                break;
        //            case 6:
        //                tblQues = QuestionTypesix(ques);
        //                break;
        //            case 7:
        //                tblQues = QuestionTypeseven(ques);
        //                break;
        //            case 8:
        //                tblQues = QuestionTypeeight(ques);
        //                break;
        //            case 9:
        //                tblQues = QuestionTypenine(ques);
        //                break;
        //            case 10:
        //                tblQues = QuestionTypeTen(ques);
        //                break;
        //            case 11:
        //                tblQues = QuestionTypeEleven(ques);
        //                break;
        //            case 12:
        //                tblQues = QuestionTypeTwelve(ques);
        //                break;
        //            case 13:
        //                tblQues = QuestionTypethirteen(ques);
        //                break;
        //            case 14:
        //                tblQues = QuestionTypefourteen(ques);
        //                break;
        //            case 15:
        //                tblQues = QuestionTypefifteen(ques);
        //                break;
        //            case 16:
        //                tblQues = GetQuestion(ques);
        //                break;
        //            case 17:
        //                tblQues = GetQuestion(ques);
        //                break;
        //            case 18:
        //                tblQues = GetQuestion(ques);
        //                break;
        //            case 19:
        //                tblQues = QuestionTypeninteen(ques);
        //                break;
        //            case 20:
        //                tblQues = questionTypetwenty(ques);
        //                break;
        //            default:
        //                break;
        //        }
        //        return tblQues;
        //    }
        //    catch (Exception ex)
        //    {
        //        HtmlTable tbl = new HtmlTable();
        //        return tbl;
        //    }
        //}

        public static List<T> SortRandom<T>(List<T> list) where T : class, new()
        {
            List<T> returnObj = new List<T>();

            Random rn = new Random();
            int randomIndex;
            List<int> indexAdded = new List<int>();

            while (indexAdded.Count < list.Count)
            {
                randomIndex = rn.Next(0, list.Count);

                if (!indexAdded.Exists(delegate(int index) { return index == randomIndex; }))
                {
                    indexAdded.Add(randomIndex);
                    returnObj.Add(list[randomIndex]);
                }
            }
            return returnObj;
        }
        public static ArrayList RemoveDuplicateItems(ArrayList list)
        {
            for (int i = 0, j; i < list.Count - 1; ++i)
            {
                j = i + 1;

                while (j < list.Count)
                {
                    if (list[j].ToString().Trim() == list[i].ToString().Trim())
                    {
                        list.RemoveAt(j);
                    }
                    else
                    {
                        ++j;
                    }
                }
            }
            return list;
        }
        //public static HtmlTable BuildRankingchartQuestion(SurveyQuestion SurQues, int Mode, int ques_seq)
        //{
        //    ExportSurveyCore surCor = new ExportSurveyCore();
        //    HtmlTable tableAns = new HtmlTable();
        //    tableAns.BgColor = "White";
        //    tableAns.BorderColor = "Black";
        //    tableAns.CellSpacing = 0;
        //    tableAns.Width = "100%";
        //    HtmlTableRow tblQuesRow = new HtmlTableRow();
        //    tblQuesRow.BgColor = "#d6e7ff";
        //    HtmlTableCell tblQuesCell = new HtmlTableCell();
        //    tblQuesCell.ColSpan = SurQues.surveyAnswers.Count + 1;
        //    Label lblQues = new Label();
        //    lblQues.ID = "lblQues" + SurQues.QUESTION_ID.ToString();
        //    string str = ques_seq + ". " + SurQues.QUESTION_LABEL;
        //    str = str.Replace("<p>", " ");
        //    str = str.Replace("</p>", " ");
        //    lblQues.Text = str;
        //    tblQuesCell.Controls.Add(lblQues);
        //    tblQuesRow.Cells.Add(tblQuesCell);
        //    tableAns.Rows.Add(tblQuesRow);
        //    ArrayList ans_ids = new ArrayList();
        //    if (SurQues.surveyAnswers != null && SurQues.surveyAnswers.Count > 0)
        //    {
        //        foreach (SurveyAnswers ans in SurQues.surveyAnswers)
        //        {
        //            ans_ids.Add(ans.ANSWER_ID);

        //        }
        //    }
        //    DataSet ds_ranking = new DataSet();
        //    ds_ranking = surCor.GetRankingConstanSum(SurQues.QUESTION_ID, ans_ids);
        //    if (SurQues.RESPONSE_COUNT > 0)
        //    {
        //        if (Mode != 2)
        //        {
        //            HtmlTableCell tblAnsCorCell = new HtmlTableCell();
        //            tblAnsCorCell.InnerText = " .";
        //            HtmlTableRow tableAnsRow = new HtmlTableRow();
        //            tableAnsRow.BgColor = "#EBEBEB";
        //            tableAnsRow.Cells.Add(tblAnsCorCell);
        //            int k = 1;
        //            foreach (SurveyAnswers ans in SurQues.surveyAnswers)
        //            {
        //                HtmlTableCell tableColHeadcell = new HtmlTableCell();
        //                tableColHeadcell.Align = "Center";
        //                Label lbl = new Label();
        //                lbl.Text = k.ToString();
        //                tableColHeadcell.Controls.Add(lbl);
        //                tableAnsRow.Cells.Add(tableColHeadcell);
        //                k += 1;
        //            }
        //            tableAns.Rows.Add(tableAnsRow);
        //            int l = 0, j = 0; bool rowColor = false;
        //            HtmlTableRow tableAnsRowop = new HtmlTableRow();
        //            //Line added on 12th may
        //            int row_count = 0, g = 0;
        //            foreach (SurveyAnswers AnsOp in SurQues.surveyAnswers)
        //            {

        //                if (l == 0)
        //                {
        //                    tableAnsRowop = new HtmlTableRow();
        //                    tableAnsRowop.BgColor = (rowColor) ? "#F6F6F6" : "White";
        //                    HtmlTableCell tblrowHeadCell = new HtmlTableCell();
        //                    Label lbl = new Label();
        //                    lbl.Text = AnsOp.ANSWER_OPTIONS.ToString();
        //                    tblrowHeadCell.Controls.Add(lbl);
        //                    tableAnsRowop.Cells.Add(tblrowHeadCell);
        //                    j++;
        //                }
        //                int ansoptions_count = SurQues.surveyAnswers.Count;
        //                int total_count = Convert.ToInt32(ds_ranking.Tables[0].Rows.Count);
        //                for (int c = 0; c < ansoptions_count; c++)
        //                {
        //                    int temp_count = 0;
        //                    for (int i = 0; i < ds_ranking.Tables[0].Rows.Count; i++)
        //                    {

        //                        if (Convert.ToInt32(ds_ranking.Tables[0].Rows[i]["ANSWER_ID"]) == AnsOp.ANSWER_ID && Convert.ToString(ds_ranking.Tables[0].Rows[i]["ANSWER_TEXT"]) == Convert.ToString(c + 1))
        //                        {

        //                            temp_count += 1;

        //                        }

        //                    }
        //                    double ResPercentrank = 0;
        //                    if (temp_count > 0 && AnsOp.RESPONSE_COUNT > 0)
        //                        ResPercentrank = Math.Round(Convert.ToDouble(temp_count * 100) / SurQues.RESPONSE_COUNT, 2);
        //                    //ans_with_Rep_rank_count.Add(temp_count);
        //                    //ans_with_Rep_rank_percent.Add(ResPercentrank);
        //                    HtmlTableCell tableRowcell = new HtmlTableCell();
        //                    tableRowcell.Align = "Center";
        //                    HtmlTable TblResponse = new HtmlTable();
        //                    TblResponse.CellSpacing = 0;
        //                    TblResponse.Border = 0;
        //                    HtmlTableRow rowResponse = new HtmlTableRow();
        //                    HtmlTableCell cellresCount = new HtmlTableCell();
        //                    HtmlTableRow rowResponsePer = new HtmlTableRow();
        //                    HtmlTableCell cellresPer = new HtmlTableCell();
        //                    ASPxLabel lblrescut = new ASPxLabel();
        //                    lblrescut.Value = temp_count;
        //                    cellresCount.Controls.Add(lblrescut);
        //                    rowResponse.Cells.Add(cellresCount);
        //                    ASPxLabel lblresPer = new ASPxLabel();
        //                    lblresPer.Value = ResPercentrank + "%";
        //                    cellresPer.Controls.Add(lblresPer);
        //                    rowResponsePer.Cells.Add(cellresPer);
        //                    TblResponse.Rows.Add(rowResponse);
        //                    TblResponse.Rows.Add(rowResponsePer);
        //                    tableRowcell.Controls.Add(TblResponse);
        //                    tableAnsRowop.Cells.Add(tableRowcell);
        //                    l++;
        //                    if (l == SurQues.surveyAnswers.Count)
        //                    {
        //                        l = 0;
        //                        tableAns.Rows.Add(tableAnsRowop);
        //                        rowColor = (rowColor) ? false : true;
        //                        g = 0;
        //                    }

        //                }
        //            }
        //        }

        //    }
        //    else
        //    {
        //        HtmlTableRow tblNoResRow = new HtmlTableRow();
        //        HtmlTableCell tblNoResCell = new HtmlTableCell();
        //        tblNoResCell.Align = "Center";
        //        tblNoResCell.InnerText = " No Responses for this Question.";
        //        tblNoResRow.Cells.Add(tblNoResCell);
        //        tableAns.Rows.Add(tblNoResRow);
        //    }
        //    return tableAns;

        //}

        //public static HtmlTable BuildConstantSumchart(SurveyQuestion SurQues, int Mode, string ViewTyp, int ques_seq)
        //{

        //    ExportSurveyCore surCor = new ExportSurveyCore();
        //    HtmlTable tbl = new HtmlTable();
        //    tbl.BgColor = "White";
        //    tbl.Width = "100%";
        //    tbl.CellSpacing = 0;
        //    HtmlTableRow tblQuesRow = new HtmlTableRow();
        //    //tblQuesRow.BgColor = "#71A2E7";
        //    tblQuesRow.BgColor = "#d6e7ff";
        //    HtmlTableCell tblQuesCell = new HtmlTableCell();
        //    tblQuesCell.ColSpan = 2;
        //    Label lblQues = new Label();
        //    lblQues.ID = "lblQues" + SurQues.QUESTION_ID.ToString();
        //    string str = ques_seq + ". " + SurQues.QUESTION_LABEL;
        //    str = str.Replace("<p>", " ");
        //    str = str.Replace("</p>", " ");
        //    lblQues.Text = str;
        //    //lblQues.Text = SurQues.QUESTION_LABEL;
        //    //lblQues.Font.Name = "#49649a"; 
        //    tblQuesCell.Controls.Add(lblQues);
        //    tblQuesRow.Cells.Add(tblQuesCell);
        //    tbl.Rows.Add(tblQuesRow);

        //    if (SurQues.surveyAnswers != null && SurQues.surveyAnswers.Count > 0)
        //    {
        //        int TotalResponsCount = 0, YesResponsecount = 0, NoresponseCount = 0;
        //        ArrayList ans_options = new ArrayList();
        //        ArrayList ans_option_sum = new ArrayList();
        //        foreach (SurveyAnswers ans in SurQues.surveyAnswers)
        //        {
        //            TotalResponsCount += ans.RESPONSE_COUNT;
        //            ans_options.Add(ans.ANSWER_ID);
        //        }
        //        DataSet ds_constantsum = new DataSet();
        //        ds_constantsum = surCor.GetConstantSumForReports(SurQues.QUESTION_ID, ans_options);
        //        if (ds_constantsum != null && ds_constantsum.Tables.Count > 0)
        //        {
        //            foreach (SurveyAnswers ans in SurQues.surveyAnswers)
        //            {
        //                int temp_total = 0;
        //                for (int m = 0; m < ds_constantsum.Tables[0].Rows.Count; m++)
        //                {

        //                    if (Convert.ToString(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) != null && ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"] != null && Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) == ans.ANSWER_ID)
        //                    {
        //                        temp_total += Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"]);

        //                    }
        //                }
        //                ans_option_sum.Add(temp_total);
        //            }

        //        }

        //        if (TotalResponsCount > 0)
        //        {
        //            if (Mode != 2)
        //            {
        //                HtmlTable tblAns = new HtmlTable();
        //                tblAns.CellSpacing = 0;
        //                tblAns.Border = 0;
        //                tblAns.BorderColor = "Black";
        //                tblAns.Width = "100%";
        //                bool rowColor = false;
        //                int y = 0;
        //                double ResPercentdividedby = 0;
        //                string RespondentIDS = surCor.GetRespondentsForReports(SurQues.QUESTION_ID);
        //                string[] arr = RespondentIDS.Split(',');
        //                double z = Convert.ToDouble(SurQues.CHAR_LIMIT);
        //                if (z == 0.0)
        //                {
        //                    z = 100;

        //                }
        //                ResPercentdividedby = Math.Round(Convert.ToDouble(z) / 100, 2);
        //                double totalsum = 0;
        //                //ResPercentdividedby = Math.Round(Convert.ToDouble(SurQues.CHAR_LIMIT) / 100, 2); 
        //                foreach (SurveyAnswers ans in SurQues.surveyAnswers)
        //                {
        //                    double ResPercent = 0;
        //                    if (Convert.ToInt32(ans_option_sum[y]) > 0 && ResPercentdividedby > 0 && arr.Length != null && arr.Length > 0)
        //                        ResPercent = Math.Round(Convert.ToDouble(ans_option_sum[y]) / (ResPercentdividedby * arr.Length), 2);
        //                    HtmlTableRow tblAnsRow = new HtmlTableRow();
        //                    tblAnsRow.BgColor = (rowColor) ? "#F6F6F6" : "White";
        //                    HtmlTableCell tblAnsCell = new HtmlTableCell();
        //                    HtmlTableCell tblBarCell = new HtmlTableCell();
        //                    tblBarCell.Width = "250";
        //                    HtmlTableCell tblResCntCell = new HtmlTableCell();
        //                    tblResCntCell.Width = "70";
        //                    tblResCntCell.Align = "Center";
        //                    HtmlTableCell tblPercentCell = new HtmlTableCell();
        //                    tblPercentCell.Width = "70";
        //                    tblPercentCell.Align = "Center";
        //                    Label lblAns = new Label();
        //                    lblAns.ID = "lblQues" + ans.ANSWER_ID;
        //                    lblAns.Text = ans.ANSWER_OPTIONS;
        //                    tblAnsCell.Controls.Add(lblAns);
        //                    tblAnsRow.Cells.Add(tblAnsCell);

        //                    Table tblBar = new Table();
        //                    TableRow tblbarrow = new TableRow();
        //                    TableCell tblbarcrtcell = new TableCell();
        //                    tblbarcrtcell.HorizontalAlign = HorizontalAlign.Center;
        //                    tblbarcrtcell.Text = Convert.ToString(ResPercent) + "%";
        //                    tblbarcrtcell.Style.Add(HtmlTextWriterStyle.Padding, "0px");
        //                    tblbarcrtcell.Width = Convert.ToInt32(ResPercent * 2.5);
        //                    tblbarcrtcell.Height = Unit.Pixel(15);
        //                    if (ResPercent > 0)
        //                        tblbarcrtcell.BackColor = Color.SandyBrown;
        //                    tblbarrow.Cells.Add(tblbarcrtcell);
        //                    tblBar.Rows.Add(tblbarrow);
        //                    tblBarCell.Controls.Add(tblBar);

        //                    tblAnsRow.Cells.Add(tblBarCell);
        //                    double avg = 0;
        //                    avg = Math.Round(Convert.ToDouble(ans_option_sum[y]) / (arr.Length), 2);
        //                    totalsum = totalsum + avg;
        //                    tblResCntCell.InnerText = Convert.ToString(avg);
        //                    tblAnsRow.Cells.Add(tblResCntCell);
        //                    // tblPercentCell.InnerText = Convert.ToString(ResPercent) + "%";
        //                    //tblAnsRow.Cells.Add(tblPercentCell);
        //                    tblAns.Rows.Add(tblAnsRow);
        //                    rowColor = (rowColor) ? false : true;
        //                    y = y + 1;
        //                }
        //                HtmlTableRow tblAnsLastButOneRow = new HtmlTableRow();
        //                tblAnsLastButOneRow.BgColor = "#EBEBEB";
        //                HtmlTableCell tblBarButOneLastCell = new HtmlTableCell();
        //                HtmlTableCell tblResCntButOneLastCell = new HtmlTableCell();
        //                tblBarButOneLastCell.Width = "70";
        //                tblResCntButOneLastCell.Align = "Center";
        //                HtmlTableCell tblPercentButoneLastCell = new HtmlTableCell();
        //                tblBarButOneLastCell.ColSpan = 2;
        //                tblBarButOneLastCell.Align = "Right";
        //                tblBarButOneLastCell.InnerText = "Total:";
        //                tblAnsLastButOneRow.Cells.Add(tblBarButOneLastCell);
        //                //tblResCntLastCell.InnerText = Convert.ToString(TotalResponsCount);
        //                tblResCntButOneLastCell.InnerText = Convert.ToString(totalsum);
        //                tblAnsLastButOneRow.Cells.Add(tblResCntButOneLastCell);
        //                tblAns.Rows.Add(tblAnsLastButOneRow);

        //                HtmlTableRow tblAnsLastRow = new HtmlTableRow();
        //                tblAnsLastRow.BgColor = "#EBEBEB";
        //                HtmlTableCell tblBarLastCell = new HtmlTableCell();
        //                HtmlTableCell tblResCntLastCell = new HtmlTableCell();
        //                tblResCntLastCell.Width = "70";
        //                tblResCntLastCell.Align = "Center";
        //                HtmlTableCell tblPercentLastCell = new HtmlTableCell();
        //                tblBarLastCell.ColSpan = 2;
        //                tblBarLastCell.Align = "Right";
        //                tblBarLastCell.InnerText = "Total Response Count:";
        //                tblAnsLastRow.Cells.Add(tblBarLastCell);
        //                //tblResCntLastCell.InnerText = Convert.ToString(TotalResponsCount);
        //                tblResCntLastCell.InnerText = Convert.ToString(arr.Length);
        //                tblAnsLastRow.Cells.Add(tblResCntLastCell);
        //                tblAns.Rows.Add(tblAnsLastRow);
        //                HtmlTableRow ansrow = new HtmlTableRow();
        //                HtmlTableCell anscell = new HtmlTableCell();
        //                anscell.ColSpan = 2;
        //                anscell.Controls.Add(tblAns);
        //                ansrow.Cells.Add(anscell);
        //                tbl.Rows.Add(ansrow);
        //            }

        //        }
        //        else
        //        {
        //            HtmlTableRow tblNoResRow = new HtmlTableRow();
        //            HtmlTableCell tblNoResCell = new HtmlTableCell();
        //            tblNoResCell.Align = "Center";
        //            tblNoResCell.InnerText = " No Responses for this Question.";

        //            tblNoResRow.Cells.Add(tblNoResCell);
        //            tbl.Rows.Add(tblNoResRow);
        //        }
        //    }

        //    return tbl;

        //}
        //public static HtmlTable BuildChartQuestion(SurveyQuestion SurQues, int Mode, string ViewTyp, int ques_seq)
        //{

        //    ExportSurveyCore surCor = new ExportSurveyCore();
        //    HtmlTable tbl = new HtmlTable();
        //    tbl.BgColor = "White";
        //    tbl.Width = "100%";
        //    tbl.CellSpacing = 0;
        //    HtmlTableRow tblQuesRow = new HtmlTableRow();
        //    //tblQuesRow.BgColor = "#71A2E7";
        //    tblQuesRow.BgColor = "#d6e7ff";
        //    HtmlTableCell tblQuesCell = new HtmlTableCell();
        //    tblQuesCell.ColSpan = 2;
        //    Label lblQues = new Label();
        //    lblQues.ID = "lblQues" + SurQues.QUESTION_ID.ToString();
        //    string str = ques_seq + ". " + SurQues.QUESTION_LABEL;
        //    str = str.Replace("<p>", " ");
        //    str = str.Replace("</p>", " ");
        //    lblQues.Text = str;
        //    //lblQues.Text = SurQues.QUESTION_LABEL;
        //    //lblQues.Font.Name = "#49649a"; 
        //    tblQuesCell.Controls.Add(lblQues);
        //    tblQuesRow.Cells.Add(tblQuesCell);
        //    tbl.Rows.Add(tblQuesRow);

        //    if (SurQues.QUESTION_TYPE == 1 || SurQues.QUESTION_TYPE == 2 || SurQues.QUESTION_TYPE == 3 || SurQues.QUESTION_TYPE == 4 || SurQues.QUESTION_TYPE == 10 || SurQues.QUESTION_TYPE == 11 || SurQues.QUESTION_TYPE == 12)
        //    {
        //        if (SurQues.surveyAnswers != null && SurQues.surveyAnswers.Count > 0)
        //        {
        //            int TotalResponsCount = 0, YesResponsecount = 0, NoresponseCount = 0;
        //            //if (SurQues.QUESTION_TYPE == 4)
        //            //{
        //            //    TotalResponsCount = SurQues.RESPONSE_COUNT;
        //            //    for (int i = 0; i < 2; i++)
        //            //    {
        //            //        string typ = (i == 0) ? "Yes" : "No";
        //            //        if (i == 0)
        //            //            YesResponsecount = surCor.GetQuestionResponseCount(typ, SurQues.QUESTION_ID);
        //            //        else
        //            //            NoresponseCount = surCor.GetQuestionResponseCount(typ, SurQues.QUESTION_ID);
        //            //    }
        //            //}
        //            //else
        //            //{}
        //            foreach (SurveyAnswers ans in SurQues.surveyAnswers)
        //            {
        //                TotalResponsCount += ans.RESPONSE_COUNT;
        //            }
        //            //Block added to include others option
        //            if (SurQues.QUESTION_TYPE == 1 || SurQues.QUESTION_TYPE == 2 || SurQues.QUESTION_TYPE == 3 || SurQues.QUESTION_TYPE == 4)
        //            {
        //                TotalResponsCount += SurQues.OTHER_RESPCOUNT;

        //            }
        //            //Block ended

        //            if (TotalResponsCount > 0)
        //            {
        //                if (Mode != 2)
        //                {
        //                    HtmlTable tblAns = new HtmlTable();
        //                    tblAns.CellSpacing = 0;
        //                    tblAns.Border = 0;
        //                    tblAns.BorderColor = "Black";
        //                    tblAns.Width = "100%";
        //                    bool rowColor = false;
        //                    //if (SurQues.QUESTION_TYPE == 4)
        //                    //{
        //                    //   tblAns= BuildYesNoQuesChart(tblAns, YesResponsecount, NoresponseCount, TotalResponsCount);
        //                    //}
        //                    //else
        //                    //{
        //                    foreach (SurveyAnswers ans in SurQues.surveyAnswers)
        //                    {
        //                        double ResPercent = 0;
        //                        if (TotalResponsCount > 0)
        //                            ResPercent = Math.Round(Convert.ToDouble(ans.RESPONSE_COUNT * 100) / TotalResponsCount, 2);

        //                        HtmlTableRow tblAnsRow = new HtmlTableRow();
        //                        tblAnsRow.BgColor = (rowColor) ? "#F6F6F6" : "White";
        //                        HtmlTableCell tblAnsCell = new HtmlTableCell();
        //                        HtmlTableCell tblBarCell = new HtmlTableCell();
        //                        tblBarCell.Width = "250";
        //                        HtmlTableCell tblResCntCell = new HtmlTableCell();
        //                        tblResCntCell.Width = "70";
        //                        tblResCntCell.Align = "Center";
        //                        HtmlTableCell tblPercentCell = new HtmlTableCell();
        //                        tblPercentCell.Width = "70";
        //                        tblPercentCell.Align = "Center";
        //                        Label lblAns = new Label();
        //                        lblAns.ID = "lblQues" + ans.ANSWER_ID;
        //                        lblAns.Text = ans.ANSWER_OPTIONS;
        //                        tblAnsCell.Controls.Add(lblAns);
        //                        tblAnsRow.Cells.Add(tblAnsCell);

        //                        Table tblBar = new Table();
        //                        TableRow tblbarrow = new TableRow();
        //                        TableCell tblbarcrtcell = new TableCell();
        //                        tblbarcrtcell.HorizontalAlign = HorizontalAlign.Center;
        //                        tblbarcrtcell.Text = Convert.ToString(ResPercent) + "%";
        //                        tblbarcrtcell.Style.Add(HtmlTextWriterStyle.Padding, "0px");
        //                        tblbarcrtcell.Width = Convert.ToInt32(ResPercent * 2.5);
        //                        tblbarcrtcell.Height = Unit.Pixel(15);
        //                        if (ResPercent > 0)
        //                            tblbarcrtcell.BackColor = Color.SandyBrown;
        //                        tblbarrow.Cells.Add(tblbarcrtcell);
        //                        tblBar.Rows.Add(tblbarrow);
        //                        tblBarCell.Controls.Add(tblBar);

        //                        tblAnsRow.Cells.Add(tblBarCell);
        //                        tblResCntCell.InnerText = Convert.ToString(ans.RESPONSE_COUNT);
        //                        tblAnsRow.Cells.Add(tblResCntCell);
        //                        // tblPercentCell.InnerText = Convert.ToString(ResPercent) + "%";
        //                        //tblAnsRow.Cells.Add(tblPercentCell);
        //                        tblAns.Rows.Add(tblAnsRow);
        //                        rowColor = (rowColor) ? false : true;
        //                    }
        //                    //Block for other answer options
        //                    if (SurQues.OTHER_ANS == 1)
        //                    {
        //                        if (SurQues.QUESTION_TYPE == 1 || SurQues.QUESTION_TYPE == 2 || SurQues.QUESTION_TYPE == 3 || SurQues.QUESTION_TYPE == 4)
        //                        {
        //                            double ResPercentothers = 0;
        //                            if (TotalResponsCount > 0)
        //                                ResPercentothers = Math.Round(Convert.ToDouble(SurQues.OTHER_RESPCOUNT * 100) / TotalResponsCount, 2);

        //                            HtmlTableRow tblAnsRowothers = new HtmlTableRow();
        //                            tblAnsRowothers.BgColor = (rowColor) ? "#F6F6F6" : "White";
        //                            HtmlTableCell tblAnsCellothers = new HtmlTableCell();
        //                            HtmlTableCell tblBarCellothers = new HtmlTableCell();
        //                            tblBarCellothers.Width = "250";
        //                            HtmlTableCell tblResCntCellothers = new HtmlTableCell();
        //                            tblResCntCellothers.Width = "70";
        //                            tblResCntCellothers.Align = "Center";
        //                            HtmlTableCell tblPercentCellothers = new HtmlTableCell();
        //                            tblPercentCellothers.Width = "70";
        //                            tblPercentCellothers.Align = "Center";
        //                            Label lblAnsothers = new Label();
        //                            lblAnsothers.ID = "lblQuesothers";
        //                            //lblAnsothers.ID = "lblQuesothers" + ans.ANSWER_ID;
        //                            string NavUrl = "QuesID=" + SurQues.QUESTION_ID + "&Mode=" + Mode;
        //                            if (ViewTyp == "All")
        //                                NavUrl += "&Veiw=" + ViewTyp;
        //                            //hlResponses.NavigateUrl = URLSecurity.EncryptQuerystring("DescriptiveResponses.aspx", NavUrl);

        //                            lblAnsothers.Text = "Others <br/>";

        //                            HyperLink hyp = new HyperLink();
        //                            hyp.Text = "View Responses ";
        //                            hyp.NavigateUrl = URLSecurity.EncryptQuerystring("DescriptiveResponses.aspx", NavUrl);
        //                            tblAnsCellothers.Controls.Add(lblAnsothers);
        //                            tblAnsCellothers.Controls.Add(hyp);
        //                            tblAnsRowothers.Cells.Add(tblAnsCellothers);

        //                            Table tblBarothers = new Table();
        //                            TableRow tblbarrowothers = new TableRow();
        //                            TableCell tblbarcrtcellothers = new TableCell();
        //                            tblbarcrtcellothers.HorizontalAlign = HorizontalAlign.Center;
        //                            tblbarcrtcellothers.Text = Convert.ToString(ResPercentothers) + "%";
        //                            tblbarcrtcellothers.Style.Add(HtmlTextWriterStyle.Padding, "0px");
        //                            tblbarcrtcellothers.Width = Convert.ToInt32(ResPercentothers * 2.5);
        //                            tblbarcrtcellothers.Height = Unit.Pixel(15);
        //                            if (ResPercentothers > 0)
        //                                tblbarcrtcellothers.BackColor = Color.SandyBrown;
        //                            tblbarrowothers.Cells.Add(tblbarcrtcellothers);
        //                            tblBarothers.Rows.Add(tblbarrowothers);
        //                            tblBarCellothers.Controls.Add(tblBarothers);

        //                            tblAnsRowothers.Cells.Add(tblBarCellothers);
        //                            tblResCntCellothers.InnerText = Convert.ToString(SurQues.OTHER_RESPCOUNT);
        //                            tblAnsRowothers.Cells.Add(tblResCntCellothers);
        //                            // tblPercentCell.InnerText = Convert.ToString(ResPercent) + "%";
        //                            //tblAnsRow.Cells.Add(tblPercentCell);
        //                            tblAns.Rows.Add(tblAnsRowothers);
        //                            rowColor = (rowColor) ? false : true;

        //                        }
        //                    }

        //                    //Block ended
        //                    HtmlTableRow tblAnsLastRow = new HtmlTableRow();
        //                    tblAnsLastRow.BgColor = "#EBEBEB";
        //                    HtmlTableCell tblBarLastCell = new HtmlTableCell();
        //                    HtmlTableCell tblResCntLastCell = new HtmlTableCell();
        //                    tblResCntLastCell.Width = "70";
        //                    tblResCntLastCell.Align = "Center";
        //                    HtmlTableCell tblPercentLastCell = new HtmlTableCell();
        //                    tblBarLastCell.ColSpan = 2;
        //                    tblBarLastCell.Align = "Right";
        //                    tblBarLastCell.InnerText = "Total Response Count:";
        //                    tblAnsLastRow.Cells.Add(tblBarLastCell);
        //                    tblResCntLastCell.InnerText = Convert.ToString(TotalResponsCount);
        //                    tblAnsLastRow.Cells.Add(tblResCntLastCell);
        //                    //tblPercentLastCell.InnerText = " .";
        //                    //tblAnsLastRow.Cells.Add(tblPercentLastCell);
        //                    tblAns.Rows.Add(tblAnsLastRow);
        //                    HtmlTableRow ansrow = new HtmlTableRow();
        //                    HtmlTableCell anscell = new HtmlTableCell();
        //                    anscell.ColSpan = 2;
        //                    anscell.Controls.Add(tblAns);
        //                    ansrow.Cells.Add(anscell);
        //                    tbl.Rows.Add(ansrow);
        //                }
        //                # region    Charts
        //                //if (Mode != 1)
        //                //{
        //                //    HtmlTableRow tblChartRow = new HtmlTableRow();
        //                //    HtmlTableCell tblChartCell = new HtmlTableCell();
        //                //    tblChartCell.Width = "100%";
        //                //    tblChartCell.Align = "Center";
        //                //    Panel PnlChart = new Panel();
        //                //    PnlChart.BorderStyle = BorderStyle.None;
        //                //    WebChartControl wcCtrl = new WebChartControl();
        //                //    wcCtrl.ID = "webChrt" + Convert.ToString(SurQues.QUESTION_ID);
        //                //    wcCtrl = GetChart(SurQues);
        //                //    PnlChart.Controls.Add(wcCtrl);
        //                //    tblChartCell.Controls.Add(PnlChart);
        //                //    tblChartRow.Cells.Add(tblChartCell);
        //                //    tbl.Rows.Add(tblChartRow);
        //                //}
        //                #endregion
        //            }
        //            else
        //            {
        //                HtmlTableRow tblNoResRow = new HtmlTableRow();
        //                HtmlTableCell tblNoResCell = new HtmlTableCell();
        //                tblNoResCell.Align = "Center";
        //                tblNoResCell.InnerText = " No Responses for this Question.";
        //                tblNoResRow.Cells.Add(tblNoResCell);
        //                tbl.Rows.Add(tblNoResRow);
        //            }
        //        }
        //    }

        //    else if (SurQues.QUESTION_TYPE == 20 || SurQues.QUESTION_TYPE == 15 || SurQues.QUESTION_TYPE == 13 || SurQues.QUESTION_TYPE == 9 || SurQues.QUESTION_TYPE == 5 || SurQues.QUESTION_TYPE == 6 || SurQues.QUESTION_TYPE == 7 || SurQues.QUESTION_TYPE == 8 || SurQues.QUESTION_TYPE == 14)
        //    {
        //        if (Mode != 2)
        //        {
        //            HtmlTableRow tblhlAnsRow = new HtmlTableRow();
        //            HtmlTableCell tblhlAnsCell = new HtmlTableCell();
        //            ASPxHyperLink hlResponses = new ASPxHyperLink();
        //            hlResponses.EncodeHtml = false;
        //            hlResponses.Text = "You have " + SurQues.RESPONSE_COUNT + " Responses for this Question.";
        //            string NavUrl = "QuesID=" + SurQues.QUESTION_ID + "&Mode=" + Mode;
        //            if (ViewTyp == "All")
        //                NavUrl += "&Veiw=" + ViewTyp;
        //            hlResponses.NavigateUrl = URLSecurity.EncryptQuerystring("DescriptiveResponses.aspx", NavUrl);
        //            hlResponses.Font.Bold = true;
        //            hlResponses.Font.Underline = true;
        //            tblhlAnsCell.Controls.Add(hlResponses);
        //            tblhlAnsRow.Cells.Add(tblhlAnsCell);
        //            tbl.Rows.Add(tblhlAnsRow);
        //        }
        //    }
        //    return tbl;

        //}
        //private static HtmlTable BuildYesNoQuesChart(HtmlTable tblAns, int YesResponsecount, int NoresponseCount, int QuesResponseCount)
        //{
        //    int i = 0; bool rowColor = false;
        //    while (i < 2)
        //    {
        //        int ResPercent = 0;
        //        if (QuesResponseCount > 0 && i == 0)
        //            ResPercent = (YesResponsecount * 100 / QuesResponseCount);
        //        else if (QuesResponseCount > 0 && i == 1)
        //            ResPercent = (NoresponseCount * 100 / QuesResponseCount);
        //        HtmlTableRow tblAnsRow = new HtmlTableRow();
        //        tblAnsRow.BgColor = (rowColor) ? "#F6F6F6" : "White";
        //        HtmlTableCell tblAnsCell = new HtmlTableCell();
        //        HtmlTableCell tblBarCell = new HtmlTableCell();
        //        tblBarCell.Width = "250";
        //        HtmlTableCell tblResCntCell = new HtmlTableCell();
        //        tblResCntCell.Width = "70";
        //        tblResCntCell.Align = "Center";
        //        HtmlTableCell tblPercentCell = new HtmlTableCell();
        //        tblPercentCell.Width = "70";
        //        tblPercentCell.Align = "Center";
        //        Label lblAns = new Label();
        //        lblAns.ID = "lblQues" + i;
        //        if (i == 0)
        //            lblAns.Text = "Yes";
        //        else
        //            lblAns.Text = "No";
        //        tblAnsCell.Controls.Add(lblAns);
        //        tblAnsRow.Cells.Add(tblAnsCell);
        //        Table tblBar = new Table();
        //        TableRow tblbarrow = new TableRow();
        //        TableCell tblbarcrtcell = new TableCell();
        //        tblbarcrtcell.Style.Add(HtmlTextWriterStyle.Padding, "0px");
        //        tblbarcrtcell.Width = ResPercent * 2;
        //        tblbarcrtcell.Height = Unit.Pixel(15);
        //        tblbarcrtcell.BackColor = Color.Sienna;
        //        tblbarrow.Cells.Add(tblbarcrtcell);
        //        tblBar.Rows.Add(tblbarrow);
        //        tblBarCell.Controls.Add(tblBar);

        //        tblAnsRow.Cells.Add(tblBarCell);
        //        if (i == 0)
        //            tblResCntCell.InnerText = Convert.ToString(YesResponsecount);
        //        else
        //            tblResCntCell.InnerText = Convert.ToString(NoresponseCount);
        //        tblAnsRow.Cells.Add(tblResCntCell);
        //        tblPercentCell.InnerText = Convert.ToString(ResPercent) + "%";
        //        tblAnsRow.Cells.Add(tblPercentCell);
        //        tblAns.Rows.Add(tblAnsRow);
        //        rowColor = (rowColor) ? false : true;
        //        i++;
        //    }
        //    return tblAns;

        //}
        //public static HtmlTable BuildMatrixChart(SurveyQuestion ques, int Mode, int ques_seq)
        //{
        //    HtmlTable tableAns = new HtmlTable();
        //    tableAns.BgColor = "White";
        //    tableAns.BorderColor = "Black";
        //    tableAns.CellSpacing = 0;
        //    tableAns.Width = "100%";
        //    ArrayList colHeads = new ArrayList();
        //    ArrayList rowHeads = new ArrayList();
        //    Hashtable headerList = GetAnsItemsList(ques);
        //    if (headerList.Contains("ColHead"))
        //        colHeads = (ArrayList)headerList["ColHead"];
        //    if (headerList.Contains("RowHead"))
        //        rowHeads = (ArrayList)headerList["RowHead"];
        //    //Block on 12 may commented for checking
        //    ArrayList column_values = new ArrayList();
        //    int m = 0, b = 0;
        //    int TotalResponsCount = 0;
        //    foreach (SurveyAnswers ans in ques.surveyAnswers)
        //    {
        //        m += 1; ;
        //        b = b + ans.RESPONSE_COUNT;
        //        if (m == colHeads.Count)
        //        {
        //            column_values.Add(b);
        //            b = 0;
        //            m = 0;
        //        }
        //        TotalResponsCount += ans.RESPONSE_COUNT;
        //    }
        //    //Block ended
        //    HtmlTableRow tblQuesRow = new HtmlTableRow();
        //    ////tblQuesRow.BgColor = "#71A2E7";
        //    tblQuesRow.BgColor = "#d6e7ff";
        //    HtmlTableCell tblQuesCell = new HtmlTableCell();
        //    tblQuesCell.ColSpan = colHeads.Count + 1;
        //    Label lblQues = new Label();
        //    lblQues.ID = "lblQues" + ques.QUESTION_ID.ToString();
        //    string str = ques_seq + ". " + ques.QUESTION_LABEL;
        //    str = str.Replace("<p>", " ");
        //    str = str.Replace("</p>", " ");
        //    lblQues.Text = str;
        //    //lblQues.Text = ques.QUESTION_LABEL;
        //    tblQuesCell.Controls.Add(lblQues);
        //    tblQuesRow.Cells.Add(tblQuesCell);
        //    tableAns.Rows.Add(tblQuesRow);
        //    //int TotalResponsCount = 0;
        //    //foreach (SurveyAnswers ans in ques.surveyAnswers)
        //    //{
        //    //    TotalResponsCount += ans.RESPONSE_COUNT;
        //    //}
        //    if (TotalResponsCount > 0)
        //    {
        //        if (Mode != 2)
        //        {
        //            HtmlTableCell tblAnsCorCell = new HtmlTableCell();
        //            tblAnsCorCell.InnerText = " .";
        //            HtmlTableRow tableAnsRow = new HtmlTableRow();
        //            tableAnsRow.BgColor = "#EBEBEB";
        //            tableAnsRow.Cells.Add(tblAnsCorCell);

        //            foreach (object obj in colHeads)
        //            {
        //                HtmlTableCell tableColHeadcell = new HtmlTableCell();
        //                tableColHeadcell.Align = "Center";
        //                Label lbl = new Label();
        //                lbl.Text = obj.ToString();
        //                tableColHeadcell.Controls.Add(lbl);
        //                tableAnsRow.Cells.Add(tableColHeadcell);
        //            }
        //            tableAns.Rows.Add(tableAnsRow);
        //            int l = 0, j = 0; bool rowColor = false;
        //            HtmlTableRow tableAnsRowop = new HtmlTableRow();
        //            //Line added on 12th may
        //            int row_count = 0, g = 0;
        //            foreach (SurveyAnswers AnsOp in ques.surveyAnswers)
        //            {
        //                if (l == 0)
        //                {
        //                    tableAnsRowop = new HtmlTableRow();
        //                    tableAnsRowop.BgColor = (rowColor) ? "#F6F6F6" : "White";
        //                    HtmlTableCell tblrowHeadCell = new HtmlTableCell();
        //                    Label lbl = new Label();
        //                    lbl.Text = rowHeads[j].ToString();
        //                    tblrowHeadCell.Controls.Add(lbl);
        //                    tableAnsRowop.Cells.Add(tblrowHeadCell);
        //                    j++;
        //                }

        //                //row_count += 1;

        //                HtmlTableCell tableRowcell = new HtmlTableCell();
        //                tableRowcell.Align = "Center";
        //                HtmlTable TblResponse = new HtmlTable();
        //                TblResponse.CellSpacing = 0;
        //                TblResponse.Border = 0;
        //                HtmlTableRow rowResponse = new HtmlTableRow();
        //                HtmlTableCell cellresCount = new HtmlTableCell();
        //                HtmlTableRow rowResponsePer = new HtmlTableRow();
        //                HtmlTableCell cellresPer = new HtmlTableCell();
        //                ASPxLabel lblrescut = new ASPxLabel();
        //                lblrescut.Value = AnsOp.RESPONSE_COUNT;
        //                cellresCount.Controls.Add(lblrescut);
        //                rowResponse.Cells.Add(cellresCount);
        //                ASPxLabel lblresPer = new ASPxLabel();
        //                double ResPercent = 0;
        //                //Block Updated By Sandeep
        //                if (column_values[row_count] != null && Convert.ToInt32(column_values[row_count]) > 0)
        //                    ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT * 100) / Convert.ToInt32(column_values[row_count]), 2);

        //                g = g + 1;
        //                if (g == colHeads.Count)
        //                {
        //                    row_count += 1;
        //                }
        //                //Block Commented
        //                //if (TotalResponsCount > 0)
        //                //ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT*100)/ TotalResponsCount,2);
        //                lblresPer.Value = ResPercent + "%";
        //                cellresPer.Controls.Add(lblresPer);
        //                rowResponsePer.Cells.Add(cellresPer);
        //                TblResponse.Rows.Add(rowResponse);
        //                TblResponse.Rows.Add(rowResponsePer);
        //                tableRowcell.Controls.Add(TblResponse);
        //                tableAnsRowop.Cells.Add(tableRowcell);
        //                l++;
        //                if (l == colHeads.Count)
        //                {
        //                    l = 0;
        //                    tableAns.Rows.Add(tableAnsRowop);
        //                    rowColor = (rowColor) ? false : true;
        //                    g = 0;
        //                    //row_count = 0;
        //                }
        //            }
        //        }
        //        # region    Charts
        //        //if (Mode != 1)
        //        //{
        //        //    HtmlTableRow tblChartRow = new HtmlTableRow();
        //        //    HtmlTableCell tblChartCell = new HtmlTableCell();
        //        //    tblChartCell.ColSpan = colHeads.Count + 1;
        //        //    tblChartCell.Width = "100%";
        //        //    tblChartCell.Align = "Center";
        //        //    Panel PnlChart = new Panel();
        //        //    PnlChart.ID = "pnlchrt" + Convert.ToString(ques.QUESTION_ID);
        //        //    PnlChart.BorderStyle = BorderStyle.None;
        //        //    WebChartControl wcCtrl = new WebChartControl();
        //        //    wcCtrl.ID = "webChrt" + Convert.ToString(ques.QUESTION_ID);
        //        //    wcCtrl = GetChart(ques);
        //        //    PnlChart.Controls.Add(wcCtrl);
        //        //    tblChartCell.Controls.Add(PnlChart);
        //        //    tblChartRow.Cells.Add(tblChartCell);
        //        //    tableAns.Rows.Add(tblChartRow);
        //        //}
        //        #endregion
        //    }
        //    else
        //    {
        //        HtmlTableRow tblNoResRow = new HtmlTableRow();
        //        HtmlTableCell tblNoResCell = new HtmlTableCell();
        //        tblNoResCell.Align = "Center";
        //        tblNoResCell.InnerText = " No Responses for this Question.";
        //        tblNoResRow.Cells.Add(tblNoResCell);
        //        tableAns.Rows.Add(tblNoResRow);
        //    }
        //    return tableAns;
        //}
        //public static HtmlTable BuildSidebysideMatrixChart(SurveyQuestion ques, int Mode, int ques_seq)
        //{
        //    HtmlTable TableQues = new HtmlTable();
        //    TableQues.BgColor = "White";
        //    TableQues.CellSpacing = 0;
        //    TableQues.Width = "100%";
        //    int TotalResponsCount = 0;
        //    HtmlTableRow tableRowAns = new HtmlTableRow();
        //    HtmlTableCell tablecellAns = new HtmlTableCell();
        //    HtmlTableCell tblCellEmpty = new HtmlTableCell();
        //    tableRowAns.Cells.Add(tblCellEmpty);
        //    HtmlTable tableAns = new HtmlTable();
        //    tableAns.BgColor = "White";
        //    tableAns.CellSpacing = 0;
        //    tableAns.Width = "100%";
        //    ArrayList colHead = new ArrayList();
        //    ArrayList ColSubHead = new ArrayList();
        //    ArrayList rowHead = new ArrayList();
        //    Hashtable headerList = GetAnsItemsList(ques);
        //    if (headerList.Contains("ColHead"))
        //        colHead = (ArrayList)headerList["ColHead"];
        //    if (headerList.Contains("ColSubHead"))
        //        ColSubHead = (ArrayList)headerList["ColSubHead"];
        //    if (headerList.Contains("RowHead"))
        //        rowHead = (ArrayList)headerList["RowHead"];

        //    //Block on 12 may
        //    ArrayList column_values = new ArrayList();
        //    int m = 0, b = 0;
        //    foreach (SurveyAnswers ans in ques.surveyAnswers)
        //    {
        //        m += 1; ;
        //        b = b + ans.RESPONSE_COUNT;
        //        if (m == ColSubHead.Count)
        //        {
        //            column_values.Add(b);
        //            b = 0;
        //            m = 0;
        //        }
        //        TotalResponsCount += ans.RESPONSE_COUNT;
        //    }
        //    //Block ended


        //    HtmlTableRow tblQuesRow = new HtmlTableRow();

        //    tblQuesRow.BgColor = "#d6e7ff";
        //    //tblQuesRow.BgColor = "#71A2E7";
        //    HtmlTableCell tblQuesCell = new HtmlTableCell();
        //    tblQuesCell.ColSpan = colHead.Count + 1;
        //    Label lblQues = new Label();
        //    lblQues.ID = "lblQues" + ques.QUESTION_ID.ToString();
        //    string str = ques_seq + ". " + ques.QUESTION_LABEL;
        //    str = str.Replace("<p>", " ");
        //    str = str.Replace("</p>", " ");
        //    lblQues.Text = str;

        //    //lblQues.Text = ques.QUESTION_LABEL;
        //    tblQuesCell.Controls.Add(lblQues);
        //    tblQuesRow.Cells.Add(tblQuesCell);
        //    TableQues.Rows.Add(tblQuesRow);
        //    //int TotalResponsCount = 0;
        //    foreach (SurveyAnswers ans in ques.surveyAnswers)
        //    {
        //        TotalResponsCount += ans.RESPONSE_COUNT;
        //    }
        //    if (TotalResponsCount > 0)
        //    {
        //        if (Mode != 2)
        //        {
        //            HtmlTableRow tableRowHead = new HtmlTableRow();
        //            tableRowHead.BgColor = "#EBEBEB";
        //            HtmlTableCell tableRowHeadCor = new HtmlTableCell();
        //            tableRowHead.Cells.Add(tableRowHeadCor);
        //            foreach (object obj in colHead)
        //            {
        //                HtmlTableCell tableColHeadcell = new HtmlTableCell();
        //                tableColHeadcell.ColSpan = ColSubHead.Count + 1;
        //                tableColHeadcell.Align = "Center";
        //                Label lbl = new Label();
        //                lbl.Text = obj.ToString();
        //                tableColHeadcell.Controls.Add(lbl);
        //                tableRowHead.Cells.Add(tableColHeadcell);
        //            }
        //            if (colHead.Count > 0)
        //                tableAns.Rows.Add(tableRowHead);
        //            if (ColSubHead.Count > 0)
        //            {
        //                HtmlTableRow tablesubHeadRow = new HtmlTableRow();
        //                tablesubHeadRow.BgColor = "#F6F6F6";
        //                HtmlTableCell tableColSubHeadcellCor = new HtmlTableCell();
        //                tablesubHeadRow.Cells.Add(tableColSubHeadcellCor);
        //                foreach (object ob in colHead)
        //                {
        //                    HtmlTableCell tableColSubHeadGrpGapCell = new HtmlTableCell();
        //                    tablesubHeadRow.Cells.Add(tableColSubHeadGrpGapCell);
        //                    foreach (object obj in ColSubHead)
        //                    {
        //                        HtmlTableCell tableColSubHeadcell = new HtmlTableCell();
        //                        tableColSubHeadcell.Align = "Center";
        //                        Label lbl = new Label();
        //                        lbl.Text = obj.ToString();
        //                        tableColSubHeadcell.Controls.Add(lbl);
        //                        tablesubHeadRow.Cells.Add(tableColSubHeadcell);
        //                    }

        //                }
        //                tableAns.Rows.Add(tablesubHeadRow);
        //            }
        //            int RowCell = 0, RowHed = 0, CellGrp = 0, Cellcunt = 0;
        //            HtmlTableRow tableAnsRowop = new HtmlTableRow();
        //            bool rowColor = false;
        //            //////Line added on 12th may
        //            int row_count = 0, g = 0;
        //            foreach (SurveyAnswers AnsOp in ques.surveyAnswers)
        //            {
        //                if (RowCell == 0)
        //                {
        //                    tableAnsRowop = new HtmlTableRow();
        //                    tableAnsRowop.BgColor = (rowColor) ? "#F6F6F6" : "White";
        //                    HtmlTableCell tblrowHeadCell = new HtmlTableCell();
        //                    Label lbl = new Label();
        //                    lbl.Text = rowHead[RowHed].ToString();
        //                    tblrowHeadCell.Controls.Add(lbl);
        //                    tableAnsRowop.Cells.Add(tblrowHeadCell);
        //                    RowHed++;
        //                }
        //                if (Cellcunt == 0)
        //                {
        //                    HtmlTableCell GrpGapcell = new HtmlTableCell();
        //                    tableAnsRowop.Cells.Add(GrpGapcell);
        //                }
        //                HtmlTableCell tableRowcell = new HtmlTableCell();
        //                tableRowcell.Align = "Center";
        //                HtmlTable TblResponse = new HtmlTable();
        //                TblResponse.CellSpacing = 0;
        //                HtmlTableRow rowResponse = new HtmlTableRow();
        //                HtmlTableCell cellresCount = new HtmlTableCell();
        //                HtmlTableRow rowResponsePer = new HtmlTableRow();
        //                HtmlTableCell cellresPer = new HtmlTableCell();
        //                ASPxLabel lblrescut = new ASPxLabel();
        //                lblrescut.Value = AnsOp.RESPONSE_COUNT;
        //                cellresCount.Controls.Add(lblrescut);
        //                rowResponse.Cells.Add(cellresCount);
        //                ASPxLabel lblresPer = new ASPxLabel();
        //                double ResPercent = 0;
        //                //Block Updated By Sandeep
        //                if (column_values[row_count] != null && Convert.ToInt32(column_values[row_count]) > 0)
        //                    ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT * 100) / Convert.ToInt32(column_values[row_count]), 2);

        //                g = g + 1;
        //                if (g == ColSubHead.Count)
        //                {
        //                    row_count += 1;
        //                    g = 0;
        //                }
        //                //Block Commented
        //                //if (TotalResponsCount > 0)
        //                //    ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT * 100) / TotalResponsCount, 2);
        //                ////if (TotalResponsCount > 0)
        //                ////ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT * 100) / TotalResponsCount,2);
        //                lblresPer.Value = ResPercent + "%";
        //                cellresPer.Controls.Add(lblresPer);
        //                rowResponsePer.Cells.Add(cellresPer);
        //                TblResponse.Rows.Add(rowResponse);
        //                TblResponse.Rows.Add(rowResponsePer);
        //                tableRowcell.Controls.Add(TblResponse);
        //                tableAnsRowop.Cells.Add(tableRowcell);
        //                RowCell++;
        //                Cellcunt++;
        //                if (RowCell == ColSubHead.Count * colHead.Count)
        //                {
        //                    RowCell = 0;
        //                    tableAns.Rows.Add(tableAnsRowop);
        //                    rowColor = (rowColor) ? false : true;
        //                }
        //                if (Cellcunt == ColSubHead.Count)
        //                {
        //                    Cellcunt = 0;
        //                    CellGrp++;
        //                }
        //            }
        //            if (tableAns != null)
        //            {
        //                tablecellAns.Controls.Add(tableAns);
        //                tableRowAns.Cells.Add(tablecellAns);
        //                TableQues.Rows.Add(tableRowAns);
        //            }
        //        }
        //        # region    Charts
        //        if (Mode != 1)
        //        {
        //            //HtmlTableRow tblChartRow = new HtmlTableRow();
        //            //HtmlTableCell tblChartCell = new HtmlTableCell();
        //            //tblChartCell.ColSpan = colHead.Count + 1;
        //            //tblChartCell.Width = "100%";
        //            //tblChartCell.Align = "Center";
        //            //Panel PnlChart = new Panel();
        //            //PnlChart.BorderStyle = BorderStyle.None;
        //            //WebChartControl wcCtrl = new WebChartControl();
        //            //wcCtrl.ID = "webChrt" + Convert.ToString(ques.QUESTION_ID);
        //            //wcCtrl = GetChart(ques);
        //            //PnlChart.Controls.Add(wcCtrl);
        //            //tblChartCell.Controls.Add(PnlChart);
        //            //tblChartRow.Cells.Add(tblChartCell);
        //            //TableQues.Rows.Add(tblChartRow);
        //        }
        //        #endregion
        //    }
        //    else
        //    {
        //        HtmlTableRow tblNoResRow = new HtmlTableRow();
        //        HtmlTableCell tblNoResCell = new HtmlTableCell();
        //        tblNoResCell.Align = "Center";
        //        tblNoResCell.InnerText = " No Responses for this Question.";
        //        tblNoResRow.Cells.Add(tblNoResCell);
        //        TableQues.Rows.Add(tblNoResRow);
        //    }
        //    return TableQues;
        //}
        public static Hashtable GetAnsIds(SurveyQuestion ques)
        {
            Hashtable hsTbl = new Hashtable();
            ArrayList colHead = new ArrayList();
            ArrayList ColSubHead = new ArrayList();
            ArrayList rowHead = new ArrayList();

            if (ques.QUESTION_TYPE == 12)
            {
                //foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
                //{
                //    string ansOpt = AnsOpts.ANSWER_OPTIONS;
                //    int pos = ansOpt.IndexOf("</p>");
                //    if (pos > 1)
                //    {
                //        colHead.Add(ansOpt.Substring(0, pos + 4));
                //        ansOpt = ansOpt.Substring(pos + 4);
                //        int AnsOptStrPos = ansOpt.IndexOf("<p>");
                //        int AnsOptEndPos = ansOpt.IndexOf("</p>");
                //        if (AnsOptStrPos > 0 && AnsOptEndPos > 4)
                //        {
                //            rowHead.Add(ansOpt.Substring(AnsOptStrPos, AnsOptEndPos - AnsOptStrPos + 4));
                //            ansOpt = ansOpt.Substring(AnsOptEndPos - AnsOptStrPos + 4);
                //        }
                //        int AnsExtOptStrPos = ansOpt.IndexOf("<p>");
                //        int AnsEXtOptEndPos = ansOpt.IndexOf("</p>");
                //        if (AnsExtOptStrPos > 0 && AnsEXtOptEndPos > 4)
                //        {
                //            ColSubHead.Add(ansOpt.Substring(AnsExtOptStrPos, AnsEXtOptEndPos - AnsExtOptStrPos + 4));
                //        }
                //    }
                //}


                foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
                {
                    string ansOpt = AnsOpts.ANSWER_OPTIONS;
                    //int pos = ansOpt.IndexOf("</p>");
                    int pos = ansOpt.IndexOf("$--$");
                    if (pos > 1)
                    {
                        //colHead.Add(ansOpt.Substring(0, pos + 4));
                        colHead.Add(ansOpt.Substring(0, pos));
                        ansOpt = ansOpt.Substring(pos + 4);
                        //int AnsOptStrPos = ansOpt.IndexOf("<p>");
                        //int AnsOptEndPos = ansOpt.IndexOf("</p>");
                        //if (AnsOptStrPos > 0 && AnsOptEndPos > 4)
                        int AnsOptEndPos = ansOpt.IndexOf("$--$");
                        if (AnsOptEndPos >= 0)
                        {
                            //rowHead.Add(ansOpt.Substring(AnsOptStrPos, AnsOptEndPos - AnsOptStrPos + 4));
                            //ansOpt = ansOpt.Substring(AnsOptEndPos - AnsOptStrPos + 4);
                            ColSubHead.Add(ansOpt.Substring(0, AnsOptEndPos).Trim());
                            ansOpt = ansOpt.Substring(AnsOptEndPos + 4);
                        }
                        //int AnsExtOptStrPos = ansOpt.IndexOf("<p>");
                        //int AnsEXtOptEndPos = ansOpt.IndexOf("</p>");
                        //if (AnsExtOptStrPos > 0 && AnsEXtOptEndPos > 4)
                        if (ansOpt.Length > 0)
                        {
                            //ColSubHead.Add(ansOpt.Substring(AnsExtOptStrPos, AnsEXtOptEndPos - AnsExtOptStrPos + 4));
                            rowHead.Add(ansOpt.Trim());
                        }
                    }
                }
            }
            else if (ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11)
            {
                //foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
                //{
                //    string ansOpt = AnsOpts.ANSWER_OPTIONS;
                //    int pos = ansOpt.IndexOf("</p>");
                //    if (pos > 1)
                //    {
                //        rowHead.Add(ansOpt.Substring(0, pos + 4));
                //        ansOpt = ansOpt.Substring(pos + 4);
                //        int AnsOptStrPos = ansOpt.IndexOf("<p>");
                //        int AnsOptEndPos = ansOpt.IndexOf("</p>");
                //        if (AnsOptStrPos > 0 && AnsOptEndPos > 4)
                //        {
                //            colHead.Add(ansOpt.Substring(AnsOptStrPos, AnsOptEndPos - AnsOptStrPos + 4));
                //        }
                //    }
                //}
                foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
                {
                    string ansOpt = AnsOpts.ANSWER_OPTIONS;
                    //int pos = ansOpt.IndexOf("</p>");
                    int pos = ansOpt.IndexOf("$--$");
                   // if (pos > 1)
                   // {
                        //rowHead.Add(ansOpt.Substring(0, pos + 4));
                        colHead.Add(ansOpt.Substring(0, pos));
                        ansOpt = ansOpt.Substring(pos + 4);
                        //int AnsOptStrPos = ansOpt.IndexOf("<p>");
                        //int AnsOptEndPos = ansOpt.IndexOf("</p>");
                        //if (AnsOptStrPos > 0 && AnsOptEndPos > 4)
                        if (ansOpt.Length > 0)
                        {
                            //colHead.Add(ansOpt.Substring(AnsOptStrPos, AnsOptEndPos - AnsOptStrPos + 4));
                            rowHead.Add(ansOpt.Trim());
                        }
                    //}
                }

            }
            if (colHead.Count > 0)
            {
                colHead = RemoveDuplicateItems(colHead);
                //hsTbl.Add("ColHead", colHead);
            }
            if (ColSubHead.Count > 0)
            {
                ColSubHead = RemoveDuplicateItems(ColSubHead);
                //hsTbl.Add("ColSubHead", ColSubHead);
            }
            if (rowHead.Count > 0)
            {
                rowHead = RemoveDuplicateItems(rowHead);
                //hsTbl.Add("RowHead", rowHead);
            }
            if (ques.QUESTION_TYPE == 10)
            {
                int i = 0, index = 1,col=0,row=0;
                string str = "";
                foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
                {
                    string optionValue = string.Format("{0}$--${1}", colHead[col], rowHead[row]);
                    var anserOption = ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS == optionValue).FirstOrDefault();
                    str += "_" + anserOption.ANSWER_ID;
                    i++;
                    if (i == colHead.Count)
                    {
                        i = 0;
                        hsTbl.Add(index, str.Substring(1));
                        str = "";
                        index++;
                    }
                    col++;
                    if (col == colHead.Count)
                    {
                       row++;
                        col = 0;
                    }
                }

            }
            if (ques.QUESTION_TYPE == 12)
            {

                int i = 0, index = 1, dim = 0, j = 1, topHeader = 0, col=0,row=0;
                string str = "";
                foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
                {
                    string optionValue = string.Format("{0}$--${1}$--${2}", colHead[topHeader], ColSubHead[col], rowHead[row]);
                    var anserOption = ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS == optionValue).FirstOrDefault();


                    str += "_" + anserOption.ANSWER_ID;
                    i++;

                    if (i == ColSubHead.Count)
                    {
                        i = 0;
                        hsTbl.Add(index, str.Substring(1));
                        str = "";
                       // index = index + rowHead.Count;
                        index++;
                        dim++;

                    }
                    //if (dim == colHead.Count)
                    //{
                    //    index = j + 1;
                    //    dim = 0;
                    //    j++;
                    //}
                    col++;
                    if (col == ColSubHead.Count)
                    {
                        row++;
                        col = 0;
                    }
                    if (row == rowHead.Count)
                    {
                        topHeader++;
                        col = 0;
                        row = 0;
                    }
                }
            }
            return hsTbl;
        }

        public static WebChartControl GetChart(SurveyQuestion SurQues, int posside)
        {
            ValidationCore cor = new ValidationCore();
            ChartTitle cTitle = new ChartTitle();
            string titleTxt = "", commenttxt = "";
            if (SurQues.CHART_TITLE.Trim().Length > 0)
            {
                titleTxt = cor.clearHTML(Convert.ToString(SurQues.CHART_TITLE));
            }
            else
            {
                //titleTxt = cor.clearHTML(SurQues.QUESTION_LABEL);
                titleTxt = cor.clearHTML(Convert.ToString(SurQues.QUESTION_LABEL));
                titleTxt = titleTxt.Replace("&nbsp;", "");
            }
            cTitle.Font = new Font("Trebuchet MS", 10.0F, FontStyle.Regular);
            cTitle.Dock = GetTltleDock(SurQues.CHART_DOCK);
            cTitle.Alignment = GetTltleAlignment(SurQues.CHART_ALIGNMENT);
            cTitle.Antialiasing = true;
            if (titleTxt.Length > 80)
                cTitle.Lines = GetTitleMultiLines(titleTxt);
            else
                cTitle.Text = titleTxt;
            ChartTitle CommentTitle = new ChartTitle();
            commenttxt = SurQues.COMMENTS;
            if (commenttxt.Length > 80)
                CommentTitle.Lines = GetTitleMultiLines(commenttxt);
            else
                CommentTitle.Text = commenttxt;
            CommentTitle.Alignment = StringAlignment.Center;
            CommentTitle.Dock = ChartTitleDockStyle.Bottom;
            CommentTitle.Font = new Font("Trebuchet MS", 10.0F, FontStyle.Regular);
            WebChartControl wcCtrl = new WebChartControl();
            wcCtrl.Titles.Add(cTitle);
            wcCtrl.Titles.Add(CommentTitle);
            //wcCtrl.PaletteName = (SurQues.CHART_PALETTE.Trim().Length > 0) ? Convert.ToString(SurQues.CHART_PALETTE) : "Chameleon";
            wcCtrl.PaletteName = (SurQues.CHART_PALETTE.Trim().Length > 0) ? Convert.ToString(SurQues.CHART_PALETTE) : "Nature Colors";
            wcCtrl.AppearanceName = (SurQues.CHART_APPEARANCE.Trim().Length > 0) ? Convert.ToString(SurQues.CHART_APPEARANCE) : "Pastel Kit";
            wcCtrl.BorderOptions.Visible = false;
            wcCtrl.Height = Unit.Pixel(300);
            wcCtrl.Width = Unit.Pixel(500);
            wcCtrl.ID = "wcc" + Convert.ToString(SurQues.QUESTION_ID);
            ViewType viewType = GetChartViewType(SurQues.CHART_TYPE, SurQues.QUESTION_TYPE);
            //ViewType viewType = GetChartViewType(SurQues.CHART_TYPE);
            if (!(viewType == ViewType.Bar || viewType == ViewType.Bar3D || viewType == ViewType.ManhattanBar || viewType == ViewType.Point || viewType == ViewType.RadarPoint || viewType == ViewType.StackedBar))//|| viewType == ViewType.Bar
            {
                Series S = new Series("ser" + Convert.ToString(SurQues.QUESTION_ID), viewType);
                S.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                if (SurQues.LABEL_POSITION != null)
                {
                    if (viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D)
                    {
                        ((Pie3DSeriesLabel)S.Label).Position = GetLabelPostion(SurQues.LABEL_POSITION);
                    }
                    else if (viewType == ViewType.Pie || viewType == ViewType.Doughnut)
                    {
                        ((PieSeriesLabel)S.Label).Position = GetLabelPostion(SurQues.LABEL_POSITION);
                    }
                    else if (viewType == ViewType.Bar)//|| viewType == "Bubble"
                    {
                        ((SideBySideBarSeriesLabel)S.Label).Position = (SurQues.LABEL_POSITION == "Center") ? BarSeriesLabelPosition.Center : BarSeriesLabelPosition.Top;
                    }
                }
                //if ((SurQues.QUESTION_TYPE == 10 ||SurQues.QUESTION_TYPE == 11) && (viewType==ViewType.Area || viewType==ViewType.StackedArea  || viewType ==ViewType.FullStackedArea ||  viewType ==ViewType.SplineArea  ||  viewType ==ViewType.StackedSplineArea ||  viewType ==ViewType.FullStackedSplineArea || viewType ==ViewType.Area3D || viewType ==ViewType.SplineArea3D))
                if (SurQues.QUESTION_TYPE == 10 || SurQues.QUESTION_TYPE == 11)
                {

                    ArrayList colHeads = new ArrayList();
                    ArrayList rowHeads = new ArrayList();
                    Hashtable headerList = GetAnsItemsList(SurQues);
                    if (headerList.Contains("ColHead"))
                        colHeads = (ArrayList)headerList["ColHead"];
                    if (headerList.Contains("RowHead"))
                        rowHeads = (ArrayList)headerList["RowHead"];
                    ArrayList column_values = new ArrayList();
                    ArrayList cell_values = new ArrayList();
                    int m = 0, c = 0, TotalResponsCount = 0;
                    foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                    {
                        m += 1; ;
                        c = c + ans.RESPONSE_COUNT;
                        if (m == colHeads.Count)
                        {
                            column_values.Add(c);
                            c = 0;
                            m = 0;
                        }
                        cell_values.Add(ans.RESPONSE_COUNT);
                        TotalResponsCount += ans.RESPONSE_COUNT;
                    }
                    int cell_index = 0;

                    for (int k = 0; k < colHeads.Count; k++)
                    {
                        //Series S1 = new Series(cor.clearHTML(rowHeads[k].ToString()), viewType);
                        Series S1 = new Series(cor.clearHTML(colHeads[k].ToString()), viewType);
                        S1.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                        int x = 0;
                        for (int y = cell_index; y < cell_index + rowHeads.Count; y++)
                        {

                            SeriesPoint srpnt = new SeriesPoint(Convert.ToString(rowHeads[x]), cell_values[cell_index]);
                            srpnt.Argument = cor.clearHTML(Convert.ToString(rowHeads[x]));

                            if (viewType != ViewType.Pie3D && viewType != ViewType.Line3D && viewType != ViewType.Doughnut3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut)
                                S1.Label.OverlappingOptions.ResolveOverlapping = true;
                            // S1.Label.OverlappingOptions.ResolveOverlapping = true;

                            if (srpnt.Argument.Length >= 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument.Substring(0, 14)) + "-" + Convert.ToString(x + 1) + ".";

                            }
                            else if (srpnt.Argument.Length < 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                            {

                                srpnt.Argument = Convert.ToString(srpnt.Argument) + "-" + Convert.ToString(x + 1);
                            }

                            S1.Points.Add(srpnt);
                            //S1.Name = cor.clearHTML(Convert.ToString(colHeads[x]));
                            S1.Name = cor.clearHTML(Convert.ToString(colHeads[k].ToString()));
                            cell_index += colHeads.Count;
                            //cell_index += 1;
                            x += 1;
                            if (x == rowHeads.Count)
                            {
                                cell_index = k + 1;
                                break;
                            }
                            if (viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D || viewType == ViewType.Pie || viewType == ViewType.Doughnut)
                                S1.PointOptions.PointView = PointView.Values;
                            else
                                S1.PointOptions.PointView = PointView.Values;


                        }

                        wcCtrl.Series.Add(S1);


                    }


                }
                else if (SurQues.QUESTION_TYPE == 12)
                {
                    ArrayList colHead = new ArrayList();
                    ArrayList ColSubHead = new ArrayList();
                    ArrayList rowHeads = new ArrayList();
                    Hashtable headerList = GetAnsItemsList(SurQues);
                    if (headerList.Contains("ColHead"))
                        colHead = (ArrayList)headerList["ColHead"];
                    if (headerList.Contains("ColSubHead"))
                        ColSubHead = (ArrayList)headerList["ColSubHead"];
                    if (headerList.Contains("RowHead"))
                        rowHeads = (ArrayList)headerList["RowHead"];
                    ArrayList column_values = new ArrayList();
                    ArrayList cell_values = new ArrayList();
                    int m = 0, b = 0;
                    int TotalResponsCount = 0;
                    foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                    {
                        m += 1; ;
                        b = b + ans.RESPONSE_COUNT;
                        if (m == ColSubHead.Count)
                        {
                            column_values.Add(b);
                            b = 0;
                            m = 0;
                        }
                        TotalResponsCount += ans.RESPONSE_COUNT;
                        cell_values.Add(ans.RESPONSE_COUNT);
                    }
                    int cell_index = posside * ColSubHead.Count;
                    for (int k = 0; k < rowHeads.Count; k++)
                    {
                        Series S1 = new Series(cor.clearHTML(rowHeads[k].ToString()), viewType);
                        S1.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                        int x = 0;
                        for (int y = cell_index; y < cell_index + ColSubHead.Count; y++)
                        {
                            SeriesPoint srpnt = new SeriesPoint(Convert.ToString(cell_index), cell_values[y]);
                            srpnt.Argument = cor.clearHTML(Convert.ToString(rowHeads[x]));

                            if (viewType != ViewType.Pie3D && viewType != ViewType.Line3D && viewType != ViewType.Doughnut3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut)
                                S1.Label.OverlappingOptions.ResolveOverlapping = true;
                            if (srpnt.Argument.Length >= 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument.Substring(0, 14)) + "-" + Convert.ToString(x + 1) + ".";

                            }
                            else if (srpnt.Argument.Length < 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument) + "-" + Convert.ToString(x + 1);
                            }
                            S1.Points.Add(srpnt);
                            S1.Name = cor.clearHTML(Convert.ToString(rowHeads[k]));
                            cell_index += 1;
                            x += 1;
                            if (viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D || viewType == ViewType.Pie || viewType == ViewType.Doughnut)
                                S1.PointOptions.PointView = PointView.Values;
                            else
                                S1.PointOptions.PointView = PointView.Values;
                            //S1.PointOptions.PointView = PointView.ArgumentAndValues;
                            if (x == ColSubHead.Count)
                            {
                                cell_index += (colHead.Count - 1) * (ColSubHead.Count);
                                break;
                            }

                        }
                        wcCtrl.Series.Add(S1);
                    }

                    wcCtrl.Titles.Clear();
                    ChartTitle cTitle1 = new ChartTitle();
                    string titleTxt1 = "";
                    if (SurQues.CHART_TITLE.Trim().Length > 0)
                    {
                        titleTxt1 = cor.clearHTML(Convert.ToString(SurQues.CHART_TITLE));

                    }
                    else
                    {
                        titleTxt1 = cor.clearHTML(Convert.ToString(SurQues.QUESTION_LABEL));
                        titleTxt1 = titleTxt1.Replace("&nbsp;", "");
                    }
                    ValidationCore vcore = new ValidationCore();
                    titleTxt1 += "(" + vcore.clearHTML(Convert.ToString(colHead[posside])) + ")";
                    cTitle1.Font = new Font("Trebuchet MS", 10.0F, FontStyle.Regular);
                    cTitle1.Dock = GetTltleDock(SurQues.CHART_DOCK);
                    cTitle1.Alignment = GetTltleAlignment(SurQues.CHART_ALIGNMENT);
                    if (titleTxt1.Length > 90)
                        cTitle1.Lines = GetTitleMultiLines(titleTxt1);
                    else
                        cTitle1.Text = titleTxt1;
                    wcCtrl.Titles.Add(cTitle1);
                    string commenttxt1 = "";
                    ChartTitle CommentTitle1 = new ChartTitle();
                    commenttxt1 = SurQues.COMMENTS;
                    if (commenttxt.Length > 80)
                        CommentTitle1.Lines = GetTitleMultiLines(commenttxt1);
                    else
                        CommentTitle1.Text = commenttxt1;
                    CommentTitle1.Alignment = StringAlignment.Center;
                    CommentTitle1.Dock = ChartTitleDockStyle.Bottom;
                    CommentTitle1.Font = new Font("Trebuchet MS", 10.0F, FontStyle.Regular);
                    wcCtrl.Titles.Add(CommentTitle1);

                }

                else if (SurQues.QUESTION_TYPE == 15)
                {
                    ArrayList ans_ids = new ArrayList();
                    ArrayList ans_text = new ArrayList();
                    ExportSurveyCore surCor = new ExportSurveyCore();
                    if (SurQues.surveyAnswers != null && SurQues.surveyAnswers.Count > 0)
                    {
                        foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                        {
                            ans_ids.Add(ans.ANSWER_ID);
                            ans_text.Add(cor.clearHTML(ans.ANSWER_OPTIONS));
                        }
                    }
                    DataSet ds_ranking = new DataSet();
                    ds_ranking = surCor.GetRankingConstanSum(SurQues.QUESTION_ID, ans_ids,SurQues.SURVEY_ID);
                    ArrayList ans_with_Rep_rank_count = new ArrayList();
                    ArrayList ans_with_Rep_rank_percent = new ArrayList();
                    int ansoptions_count = SurQues.surveyAnswers.Count;
                    int z = 1;
                    DataSet ds_ans = new DataSet();
                    foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                    {
                        Series S1 = new Series(Convert.ToString(z), viewType);
                        S1.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                        int x = 0, r = 0;

                        for (int c = 0; c < ansoptions_count; c++)
                        {
                            int temp_count = 0;
                            if (ds_ranking != null && ds_ranking.Tables.Count > 0)
                            {
                                for (int i = 0; i < ds_ranking.Tables[0].Rows.Count; i++)
                                {
                                    if (Convert.ToInt32(ds_ranking.Tables[0].Rows[i]["ANSWER_ID"]) == Convert.ToInt32(ans_ids[c]) && Convert.ToString(ds_ranking.Tables[0].Rows[i]["ANSWER_TEXT"]) == Convert.ToString(z))
                                    {
                                        temp_count += 1;


                                    }

                                }

                                double ResPercentrank = 0;
                                if (temp_count > 0 && ans.RESPONSE_COUNT > 0)
                                    ResPercentrank = Math.Round(Convert.ToDouble(temp_count * 100) / ans.RESPONSE_COUNT, 2);
                                ans_with_Rep_rank_count.Add(temp_count);
                                ans_with_Rep_rank_percent.Add(ResPercentrank);
                            }
                            SeriesPoint srpnt = new SeriesPoint(Convert.ToString(ans_text[c]), temp_count);
                            srpnt.Argument = Convert.ToString(ans_text[c]).Replace("\r\n", "");
                            if (viewType != ViewType.Pie3D && viewType != ViewType.Line3D && viewType != ViewType.Doughnut3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut)
                                S1.Label.OverlappingOptions.ResolveOverlapping = true;
                            //S1.Label.OverlappingOptions.ResolveOverlapping = true;
                            if (srpnt.Argument.Length >= 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument.Substring(0, 14)) + "-" + Convert.ToString(c + 1) + ".";
                            }
                            else if (srpnt.Argument.Length < 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                            {

                                srpnt.Argument = Convert.ToString(srpnt.Argument) + "-" + Convert.ToString(c + 1);
                            }

                            S1.Points.Add(srpnt);
                            S1.Name = Convert.ToString(z);

                            //if (viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D || viewType == ViewType.Pie || viewType == ViewType.Doughnut)
                            //{
                            //    //S1.PointOptions.PointView = PointView.Argument;
                            //    S1.PointOptions.PointView = PointView.ArgumentAndValues;
                            //}
                            ////else
                            S1.PointOptions.PointView = PointView.Values;
                            x += 1;
                            if (x == ansoptions_count)
                                break;
                            r = r + 1;
                        }

                        wcCtrl.Series.Add(S1);
                        z = z + 1;
                    }
                }
                else if (SurQues.QUESTION_TYPE == 13)
                {

                    if (SurQues.surveyAnswers != null && SurQues.surveyAnswers.Count > 0)
                    {
                        ArrayList ans_options = new ArrayList();
                        ArrayList ans_option_sum = new ArrayList();
                        int TotalResponsCount = 0;
                        ExportSurveyCore score = new ExportSurveyCore();
                        foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                        {
                            TotalResponsCount += ans.RESPONSE_COUNT;
                            ans_options.Add(ans.ANSWER_ID);
                        }
                        DataSet ds_constantsum = new DataSet();
                        ds_constantsum = score.GetConstantSumForReports(SurQues.QUESTION_ID, ans_options,SurQues.SURVEY_ID);


                        foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                        {
                            int temp_total = 0;
                            if (ds_constantsum != null && ds_constantsum.Tables.Count > 0)
                            {
                                for (int m = 0; m < ds_constantsum.Tables[0].Rows.Count; m++)
                                {
                                    if (Convert.ToString(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) != null && ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"] != null && Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) == ans.ANSWER_ID)
                                    {
                                        temp_total += Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"]);
                                    }
                                }
                            }
                            ans_option_sum.Add(temp_total);
                        }



                        int y = 0;
                        double ResPercentdividedby = 0;
                        string RespondentIDS = score.GetRespondentsForReports(SurQues.QUESTION_ID,SurQues.SURVEY_ID);
                        string[] arr = RespondentIDS.Split(',');
                        double z = Convert.ToDouble(SurQues.CHAR_LIMIT);
                        if (z == 0.0)
                        {
                            z = 100;

                        }
                        ResPercentdividedby = Math.Round(Convert.ToDouble(z) / 100, 2);

                        S.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                        foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                        {
                            double avg = 0;
                            avg = Math.Round(Convert.ToDouble(ans_option_sum[y]) / (arr.Length), 2);
                            SeriesPoint srpnt = new SeriesPoint(Convert.ToString(cor.clearHTML(ans.ANSWER_OPTIONS)), avg);
                            string str = cor.clearHTML(ans.ANSWER_OPTIONS);
                            str = str.Replace("\r\n", "");
                            srpnt.Argument = str;
                            if (srpnt.Argument.Length >= 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument.Substring(0, 14)) + "-" + Convert.ToString(y + 1) + ".";

                            }
                            else if (srpnt.Argument.Length < 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                            {

                                srpnt.Argument = Convert.ToString(srpnt.Argument) + "-" + Convert.ToString(y + 1);
                            }
                            S.Points.Add(srpnt);
                            if (viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D || viewType == ViewType.Pie || viewType == ViewType.Doughnut)
                            {
                                S.PointOptions.PointView = PointView.Values;

                            }
                            else
                                S.PointOptions.PointView = PointView.Values;

                            NumericFormat format;
                            format = NumericFormat.FixedPoint;
                            S.PointOptions.ValueNumericOptions.Format = format;
                            S.PointOptions.ValueNumericOptions.Precision = 0;
                            if (viewType != ViewType.Pie3D && viewType != ViewType.Line3D && viewType != ViewType.Doughnut3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut)
                                S.Label.OverlappingOptions.ResolveOverlapping = true;
                            S.LegendText = " ";
                            y = y + 1;
                        }
                    }
                    wcCtrl.Series.Add(S);
                }
                else
                {

                    if (SurQues.surveyAnswers != null && SurQues.surveyAnswers.Count > 0)
                    {
                        S.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                        int count = 0;
                        foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                        {
                            SeriesPoint srpnt = new SeriesPoint(cor.clearHTML(Convert.ToString(ans.ANSWER_OPTIONS)), Convert.ToInt32(ans.RESPONSE_COUNT));
                            string str = cor.clearHTML(ans.ANSWER_OPTIONS);
                            str = str.Replace("\r\n", "");
                            srpnt.Argument = str.Trim();
                            if (srpnt.Argument.Length >= 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument.Substring(0, 14)) + "-" + Convert.ToString(count + 1) + ".";
                            }
                            else if (srpnt.Argument.Length < 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument) + "-" + Convert.ToString(count + 1);
                            }
                            S.Points.Add(srpnt);
                            if (viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D || viewType == ViewType.Pie || viewType == ViewType.Doughnut)
                            {
                                S.PointOptions.PointView = PointView.Values;

                            }
                            else
                            {
                                S.PointOptions.PointView = PointView.Values;

                            }
                            NumericFormat format;
                            format = NumericFormat.FixedPoint;
                            S.PointOptions.ValueNumericOptions.Format = format;
                            S.PointOptions.ValueNumericOptions.Precision = 0;
                            if (viewType != ViewType.Pie3D && viewType != ViewType.Line3D && viewType != ViewType.Doughnut3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut)
                                S.Label.OverlappingOptions.ResolveOverlapping = true;
                            count += 1;

                        }


                        if (SurQues.OTHER_ANS == 1)
                        {
                            if (SurQues.QUESTION_TYPE == 1 || SurQues.QUESTION_TYPE == 2 || SurQues.QUESTION_TYPE == 3 || SurQues.QUESTION_TYPE == 4)
                            {
                                SeriesPoint srpntothers = new SeriesPoint("Others", Convert.ToInt32(SurQues.OTHER_RESPCOUNT));
                                srpntothers.Argument = "Others";
                                if (srpntothers.Argument.Length >= 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                                {
                                    srpntothers.Argument = Convert.ToString(srpntothers.Argument.Substring(0, 14)) + "-" + Convert.ToString(count + 1) + ".";
                                }
                                else if (srpntothers.Argument.Length < 15 && (viewType != ViewType.Pie3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut && viewType != ViewType.Doughnut3D))
                                {
                                    srpntothers.Argument = Convert.ToString(srpntothers.Argument) + "-" + Convert.ToString(count + 1);
                                }
                                S.Points.Add(srpntothers);
                                if (viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D || viewType == ViewType.Pie || viewType == ViewType.Doughnut)
                                {
                                    S.PointOptions.PointView = PointView.Values;
                                }
                                else
                                {
                                    S.PointOptions.PointView = PointView.Values;
                                }
                                NumericFormat format;
                                format = NumericFormat.FixedPoint;
                                S.PointOptions.ValueNumericOptions.Format = format;
                                S.PointOptions.ValueNumericOptions.Precision = 0;
                                if (viewType != ViewType.Pie3D && viewType != ViewType.Line3D && viewType != ViewType.Doughnut3D && viewType != ViewType.Pie && viewType != ViewType.Doughnut)
                                    S.Label.OverlappingOptions.ResolveOverlapping = true;
                                S.LegendText = " ";

                            }
                        }


                    }
                    wcCtrl.Series.Add(S);


                }

            }
            else
            {


                if (SurQues.surveyAnswers != null && SurQues.surveyAnswers.Count > 0 && SurQues.QUESTION_TYPE != 15 && SurQues.QUESTION_TYPE != 10 && SurQues.QUESTION_TYPE != 11 && SurQues.QUESTION_TYPE != 13 && SurQues.QUESTION_TYPE != 12)
                {

                    int count = 0;
                    foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                    {
                        Series S = new Series(cor.clearHTML(ans.ANSWER_OPTIONS), viewType);
                        S.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                        SeriesPoint srpnt = new SeriesPoint(Convert.ToString(ans.ANSWER_ID), ans.RESPONSE_COUNT);
                        string str = cor.clearHTML(ans.ANSWER_OPTIONS);
                        str = str.Replace("\r\n", "");
                        //str = str.Replace("&nbsp;", "");
                        srpnt.Argument = str.Trim();
                        //srpnt.Argument = cor.clearHTML(ans.ANSWER_OPTIONS);
                        S.Label.OverlappingOptions.ResolveOverlapping = true;
                        srpnt.Tag = cor.clearHTML(ans.ANSWER_OPTIONS);
                        //SeriesPoint srpnnt1 = new SeriesPoint();
                        if (srpnt.Argument.Length >= 15)
                        {
                            srpnt.Argument = Convert.ToString(srpnt.Argument.Substring(0, 14)) + "-" + Convert.ToString(count + 1) + ".";

                        }
                        else if (srpnt.Argument.Length < 15)
                        {
                            srpnt.Argument = Convert.ToString(srpnt.Argument) + "-" + Convert.ToString(count + 1);
                        }
                        S.Points.Add(srpnt);
                        S.Name = cor.clearHTML(ans.ANSWER_OPTIONS);
                        count += 1;
                        wcCtrl.Series.Add(S);
                    }
                    if (SurQues.OTHER_ANS == 1)
                    {
                        if (SurQues.QUESTION_TYPE == 1 || SurQues.QUESTION_TYPE == 2 || SurQues.QUESTION_TYPE == 3 || SurQues.QUESTION_TYPE == 4)
                        {
                            Series S = new Series("Others", viewType);
                            S.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                            SeriesPoint srpntothers1 = new SeriesPoint("Others", SurQues.OTHER_RESPCOUNT);
                            srpntothers1.Argument = "Others";
                            S.Label.OverlappingOptions.ResolveOverlapping = true;
                            if (srpntothers1.Argument.Length >= 15)
                            {
                                srpntothers1.Argument = Convert.ToString(srpntothers1.Argument.Substring(0, 14)) + "-" + Convert.ToString(count + 1) + ".";
                            }
                            else if (srpntothers1.Argument.Length < 15)
                            {
                                srpntothers1.Argument = Convert.ToString(srpntothers1.Argument) + "-" + Convert.ToString(count + 1);
                            }
                            S.Points.Add(srpntothers1);
                            S.Name = "Others";
                            wcCtrl.Series.Add(S);
                        }
                    }
                }
                else if (SurQues.QUESTION_TYPE == 15)
                {
                    ArrayList ans_ids = new ArrayList();
                    ArrayList ans_text = new ArrayList();
                    ExportSurveyCore surCor = new ExportSurveyCore();
                    if (SurQues.surveyAnswers != null && SurQues.surveyAnswers.Count > 0)
                    {
                        foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                        {
                            ans_ids.Add(ans.ANSWER_ID);
                            ans_text.Add(cor.clearHTML(ans.ANSWER_OPTIONS));
                        }
                    }
                    DataSet ds_ranking = new DataSet();
                    ds_ranking = surCor.GetRankingConstanSum(SurQues.QUESTION_ID, ans_ids,SurQues.SURVEY_ID);
                    ArrayList ans_with_Rep_rank_count = new ArrayList();
                    ArrayList ans_with_Rep_rank_percent = new ArrayList();
                    int ansoptions_count = SurQues.surveyAnswers.Count;
                    int z = 1;
                    DataSet ds_ans = new DataSet();
                    foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                    {
                        Series S1 = new Series(Convert.ToString(z), viewType);
                        S1.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                        int x = 0, r = 0;
                        for (int c = 0; c < ansoptions_count; c++)
                        {
                            int temp_count = 0;
                            if (ds_ranking != null && ds_ranking.Tables.Count > 0)
                            {
                                for (int i = 0; i < ds_ranking.Tables[0].Rows.Count; i++)
                                {
                                    if (Convert.ToInt32(ds_ranking.Tables[0].Rows[i]["ANSWER_ID"]) == Convert.ToInt32(ans_ids[c]) && Convert.ToString(ds_ranking.Tables[0].Rows[i]["ANSWER_TEXT"]) == Convert.ToString(z))
                                    {
                                        temp_count += 1;


                                    }
                                }
                                double ResPercentrank = 0;
                                if (temp_count > 0 && ans.RESPONSE_COUNT > 0)
                                    ResPercentrank = Math.Round(Convert.ToDouble(temp_count * 100) / ans.RESPONSE_COUNT, 2);
                                ans_with_Rep_rank_count.Add(temp_count);
                                ans_with_Rep_rank_percent.Add(ResPercentrank);
                            }


                            SeriesPoint srpnt = new SeriesPoint(Convert.ToString(ans_text[c]), temp_count);
                            srpnt.Argument = Convert.ToString(ans_text[c]).Replace("\r\n", "");
                            S1.Label.OverlappingOptions.ResolveOverlapping = true;
                            if (srpnt.Argument.Length >= 15)
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument.Substring(0, 14)) + "-" + Convert.ToString(c + 1) + ".";

                            }
                            else if (srpnt.Argument.Length < 15)
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument) + "-" + Convert.ToString(c + 1);
                            }

                            S1.Points.Add(srpnt);
                            S1.Name = Convert.ToString(z);
                            x += 1;
                            if (x == ansoptions_count)
                                break;
                            r = r + 1;
                        }

                        wcCtrl.Series.Add(S1);
                        z = z + 1;
                    }
                }

                else if (SurQues.QUESTION_TYPE == 13)
                {

                    if (SurQues.surveyAnswers != null && SurQues.surveyAnswers.Count > 0)
                    {
                        ArrayList ans_options = new ArrayList();
                        ArrayList ans_option_sum = new ArrayList();
                        int TotalResponsCount = 0;
                        ExportSurveyCore score = new ExportSurveyCore();
                        foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                        {
                            TotalResponsCount += ans.RESPONSE_COUNT;
                            ans_options.Add(ans.ANSWER_ID);
                        }
                        DataSet ds_constantsum = new DataSet();
                        ds_constantsum = score.GetConstantSumForReports(SurQues.QUESTION_ID, ans_options,SurQues.SURVEY_ID);

                        foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                        {
                            int temp_total = 0;
                            if (ds_constantsum != null && ds_constantsum.Tables.Count > 0)
                            {
                                for (int m = 0; m < ds_constantsum.Tables[0].Rows.Count; m++)
                                {
                                    if (Convert.ToString(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) != null && ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"] != null && Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) == ans.ANSWER_ID)
                                    {
                                        temp_total += Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"]);
                                    }
                                }
                            }
                            ans_option_sum.Add(temp_total);
                        }

                        int y = 0;
                        double ResPercentdividedby = 0;
                        string RespondentIDS = score.GetRespondentsForReports(SurQues.QUESTION_ID,SurQues.SURVEY_ID);
                        string[] arr = RespondentIDS.Split(',');
                        double z = Convert.ToDouble(SurQues.CHAR_LIMIT);
                        if (z == 0.0)
                        {
                            z = 100;

                        }
                        ResPercentdividedby = Math.Round(Convert.ToDouble(z) / 100, 2);

                        foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                        {
                            double avg = 0;
                            avg = Math.Round(Convert.ToDouble(ans_option_sum[y]) / (arr.Length), 2);
                            Series S = new Series(cor.clearHTML(ans.ANSWER_OPTIONS), viewType);
                            S.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                            SeriesPoint srpnt = new SeriesPoint(Convert.ToString(ans.ANSWER_ID), avg);

                            srpnt.Argument = cor.clearHTML(ans.ANSWER_OPTIONS);
                            if (srpnt.Argument.Length >= 15)
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument.Substring(0, 14)) + "-" + Convert.ToString(y + 1) + ".";

                            }
                            else if (srpnt.Argument.Length < 15)
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument) + "-" + Convert.ToString(y + 1);
                            }

                            S.Label.OverlappingOptions.ResolveOverlapping = true;
                            S.Points.Add(srpnt);
                            S.Name = cor.clearHTML(ans.ANSWER_OPTIONS);
                            wcCtrl.Series.Add(S);
                            y = y + 1;
                        }

                    }
                }
                else if (SurQues.QUESTION_TYPE == 12)
                {

                    ArrayList colHead = new ArrayList();
                    ArrayList ColSubHead = new ArrayList();
                    ArrayList rowHeads = new ArrayList();
                    Hashtable headerList = GetAnsItemsList(SurQues);
                    if (headerList.Contains("ColHead"))
                        colHead = (ArrayList)headerList["ColHead"];
                    if (headerList.Contains("ColSubHead"))
                        ColSubHead = (ArrayList)headerList["ColSubHead"];
                    if (headerList.Contains("RowHead"))
                        rowHeads = (ArrayList)headerList["RowHead"];
                    ArrayList column_values = new ArrayList();
                    ArrayList cell_values = new ArrayList();               
                    int row = 0, col = 0, topHeader = 0;
                    foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                    {                        
                        string optionValue = string.Format("{0}$--${1}$--${2}", colHead[topHeader], ColSubHead[col], rowHeads[row]);
                        var anserOption = SurQues.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS.Replace("\r\n", "").Replace("\r", "").Replace("\n", "") == optionValue.Replace("\r\n", "").Replace("\r", "").Replace("\n", "")).FirstOrDefault();
                        cell_values.Add(anserOption.RESPONSE_COUNT);
                        col++;
                        if (col == ColSubHead.Count)
                        {
                            topHeader++;
                            col = 0;                         
                        }
                        if (topHeader == colHead.Count)
                        {
                            row++;
                            col = 0;
                            topHeader = 0;
                        }
                    }
                    int cell_index = posside * ColSubHead.Count;
                    int z = 0;
                    //for (int k = 0; k < rowHeads.Count; k++)
                    //{
                    //    Series S1 = new Series(cor.clearHTML(rowHeads[k].ToString()), viewType);
                    //    S1.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                    //    int x = 0;
                    //    for (int y = cell_index; y < cell_index + ColSubHead.Count; y++)
                    //    {
                    //        SeriesPoint srpnt = new SeriesPoint(Convert.ToString(cell_index), cell_values[cell_index]);
                    //        srpnt.Argument = cor.clearHTML(Convert.ToString(rowHeads[x]));
                    //        S1.Label.OverlappingOptions.ResolveOverlapping = true;
                    //        S1.Points.Add(srpnt);
                    //        S1.Name = cor.clearHTML(Convert.ToString(ColSubHead[k]));
                    //        cell_index += colHead.Count * (ColSubHead.Count);
                    //        x += 1;
                    //        if (x == rowHeads.Count)
                    //        {
                    //            cell_index = (posside * ColSubHead.Count)+(k+1);
                    //            break;
                    //        }
                    //    }
                    //   
                    //}

                    for (int k = 0; k < ColSubHead.Count; k++)
                    {
                        Series S1 = new Series(cor.clearHTML(ColSubHead[k].ToString()), viewType);
                        S1.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                        int x = 0;
                        for (int y = cell_index; y < cell_index + rowHeads.Count; y++)
                        {
                            //SeriesPoint srpnt = new SeriesPoint(Convert.ToString(cell_index), cell_values[cell_index]);
                            SeriesPoint srpnt = new SeriesPoint(Convert.ToString(rowHeads[x]), cell_values[cell_index]);
                            srpnt.Argument = cor.clearHTML(Convert.ToString(rowHeads[x]));

                            S1.Label.OverlappingOptions.ResolveOverlapping = true;
                            if (srpnt.Argument.Length >= 15)
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument.Substring(0, 15)) + "-" + Convert.ToString(x + 1) + ".";

                            }
                            else if (srpnt.Argument.Length < 15)
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument) + "-" + Convert.ToString(x + 1);
                            }

                            S1.Points.Add(srpnt);
                            //S1.Name = cor.clearHTML(Convert.ToString(colHeads[x]));
                            S1.Name = cor.clearHTML(Convert.ToString(ColSubHead[k].ToString()));
                            cell_index += colHead.Count * (ColSubHead.Count);
                            //cell_index += 1;
                            x += 1;
                            if (x == rowHeads.Count)
                            {
                                cell_index = (posside * ColSubHead.Count) + (k + 1);
                                break;
                            }
                        }
                        wcCtrl.Series.Add(S1);
                    }
                    wcCtrl.Titles.Clear();
                    ChartTitle cTitle1 = new ChartTitle();
                    string titleTxt1 = "";
                    if (SurQues.CHART_TITLE.Trim().Length > 0)
                        titleTxt1 = cor.clearHTML(Convert.ToString(SurQues.CHART_TITLE));
                    else
                    {
                        titleTxt1 = cor.clearHTML(Convert.ToString(SurQues.QUESTION_LABEL));
                        titleTxt1 = titleTxt1.Replace("&nbsp;", "");
                    }
                    ValidationCore vcore = new ValidationCore();
                    titleTxt1 += "(" + vcore.clearHTML(Convert.ToString(colHead[posside])) + ")";
                    cTitle1.Font = new Font("Trebuchet MS", 10.0F, FontStyle.Regular);
                    cTitle1.Dock = GetTltleDock(SurQues.CHART_DOCK);
                    cTitle1.Alignment = GetTltleAlignment(SurQues.CHART_ALIGNMENT);
                    if (titleTxt1.Length > 80)
                        cTitle1.Lines = GetTitleMultiLines(titleTxt1);
                    else
                        cTitle1.Text = titleTxt1;
                    wcCtrl.Titles.Add(cTitle1);

                    string commenttxt1 = "";
                    ChartTitle CommentTitle1 = new ChartTitle();
                    commenttxt1 = SurQues.COMMENTS;
                    if (commenttxt.Length > 80)
                        CommentTitle1.Lines = GetTitleMultiLines(commenttxt1);
                    else
                        CommentTitle1.Text = commenttxt1;
                    CommentTitle1.Alignment = StringAlignment.Center;
                    CommentTitle1.Dock = ChartTitleDockStyle.Bottom;
                    CommentTitle1.Font = new Font("Trebuchet MS", 10.0F, FontStyle.Regular);
                    wcCtrl.Titles.Add(CommentTitle1);



                }
                else if (SurQues.QUESTION_TYPE == 10 || SurQues.QUESTION_TYPE == 11)
                {
                    ArrayList colHeads = new ArrayList();
                    ArrayList rowHeads = new ArrayList();
                    Hashtable headerList = GetAnsItemsList(SurQues);
                    if (headerList.Contains("ColHead"))
                        colHeads = (ArrayList)headerList["ColHead"];
                    if (headerList.Contains("RowHead"))
                        rowHeads = (ArrayList)headerList["RowHead"];
                    ArrayList column_values = new ArrayList();
                    ArrayList cell_values = new ArrayList();
                    int col = 0, row = 0;
                    foreach (SurveyAnswers ans in SurQues.surveyAnswers)
                    {
                        //m += 1; ;
                        //c = c + ans.RESPONSE_COUNT;
                        //if (m == colHeads.Count)
                        //{
                        //    column_values.Add(c);
                        //    c = 0;
                        //    m = 0;
                        //}
                        //cell_values.Add(ans.RESPONSE_COUNT);
                        //TotalResponsCount += ans.RESPONSE_COUNT;
                        string optionValue = string.Format("{0}$--${1}", colHeads[col], rowHeads[row]);
                        var anserOption = SurQues.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Trim() == optionValue.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Trim()).FirstOrDefault();
                        //totalCount = anserOption.RESPONSE_COUNT;
                        cell_values.Add(anserOption.RESPONSE_COUNT);
                        col++;
                        if (col == colHeads.Count)
                        {
                            row++;
                            col = 0;                           
                        }

                    }
                    int cell_index = 0;

                    for (int k = 0; k < colHeads.Count; k++)
                    {
                        ////Series S1 = new Series(cor.clearHTML(rowHeads[k].ToString()), viewType);
                        ////S1.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                        ////int x = 0;
                        ////for (int y = cell_index; y < cell_index + colHeads.Count; y++)
                        ////{
                        ////    SeriesPoint srpnt = new SeriesPoint(Convert.ToString(cell_index), cell_values[cell_index]);
                        ////    srpnt.Argument = cor.clearHTML(Convert.ToString(rowHeads[x]));
                        ////    S1.Label.OverlappingOptions.ResolveOverlapping = true;
                        ////    S1.Points.Add(srpnt);
                        ////    S1.Name = cor.clearHTML(Convert.ToString(colHeads[k]));
                        ////    //cell_index += colHeads.Count;
                        ////    cell_index += rowHeads.Count;
                        ////    x += 1;
                        ////    if (x == colHeads.Count)
                        ////    {
                        ////        cell_index = k + 1;
                        ////        break;
                        ////    }
                        ////}
                        ////wcCtrl.Series.Add(S1);


                        //Series S1 = new Series(cor.clearHTML(rowHeads[k].ToString()), viewType);
                        Series S1 = new Series(cor.clearHTML(colHeads[k].ToString()), viewType);
                        S1.Label.Visible = Convert.ToBoolean(SurQues.SHOW_LABEL);
                        int x = 0;
                        for (int y = cell_index; y < cell_index + rowHeads.Count; y++)
                        {
                            //SeriesPoint srpnt = new SeriesPoint(Convert.ToString(cell_index), cell_values[cell_index]);
                            SeriesPoint srpnt = new SeriesPoint(Convert.ToString(rowHeads[x]), cell_values[cell_index]);
                            srpnt.Argument = cor.clearHTML(Convert.ToString(rowHeads[x]));

                            S1.Label.OverlappingOptions.ResolveOverlapping = true;
                            if (srpnt.Argument.Length >= 15)
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument.Substring(0, 14)) + "-" + Convert.ToString(x + 1) + ".";

                            }
                            else if (srpnt.Argument.Length < 15)
                            {
                                srpnt.Argument = Convert.ToString(srpnt.Argument) + "-" + Convert.ToString(x + 1);
                            }

                            S1.Points.Add(srpnt);
                            S1.Name = cor.clearHTML(Convert.ToString(colHeads[k].ToString()));
                            cell_index += colHeads.Count;
                            x += 1;
                            if (x == rowHeads.Count)
                            {
                                cell_index = k + 1;
                                break;
                            }
                        }
                        wcCtrl.Series.Add(S1);


                    }

                }
            }
            if (viewType == ViewType.Area || viewType == ViewType.SplineArea || viewType == ViewType.Point)
            {
                for (int i = 0; i < wcCtrl.Series.Count; i++)
                {
                    ((PointSeriesLabel)wcCtrl.Series[i].Label).Angle = SurQues.LABEL_ANGLE;
                }
            }

            if (viewType == ViewType.Bar)
            {

                for (int i = 0; i < wcCtrl.Series.Count; i++)
                {
                    ((SideBySideBarSeriesLabel)wcCtrl.Series[i].Label).Position = (SurQues.LABEL_POSITION == "Center") ? BarSeriesLabelPosition.Center : BarSeriesLabelPosition.Top;
                }
                //if (viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D)
                //{
                //    ((Pie3DSeriesLabel)S.Label).Position = GetLabelPostion(SurQues.LABEL_POSITION);
                //}
                //else if (viewType == ViewType.Pie || viewType == ViewType.Doughnut)
                //{
                //    ((PieSeriesLabel)S.Label).Position = GetLabelPostion(SurQues.LABEL_POSITION);
                //}
                //else if (viewType == ViewType.Bar)//|| viewType == "Bubble"
                //{
                //    ((SideBySideBarSeriesLabel)S.Label).Position = (SurQues.LABEL_POSITION == "Center") ? BarSeriesLabelPosition.Center : BarSeriesLabelPosition.Top;
                //}
            }
            if (viewType == ViewType.FullStackedBar || viewType == ViewType.FullStackedBar3D || viewType == ViewType.FullStackedArea || viewType == ViewType.FullStackedSplineArea || viewType == ViewType.Pie || viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D || viewType == ViewType.Doughnut)
            {
                foreach (Series series in wcCtrl.Series)
                {
                    bool state = Convert.ToBoolean(SurQues.VALUEAS_PERCENT);
                    if (viewType == ViewType.FullStackedBar || viewType == ViewType.FullStackedBar3D)
                    {
                        ((FullStackedBarPointOptions)series.PointOptions).PercentOptions.ValueAsPercent = state;
                    }
                    else if (viewType == ViewType.FullStackedArea || viewType == ViewType.FullStackedSplineArea)
                    {
                        ((FullStackedAreaPointOptions)series.PointOptions).PercentOptions.ValueAsPercent = state;
                    }
                    else if (viewType == ViewType.Pie || viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D || viewType == ViewType.Doughnut)
                    {
                        if (state)
                        {
                            ((PiePointOptions)series.PointOptions).ValueNumericOptions.Format = NumericFormat.Percent;
                            ((PiePointOptions)series.PointOptions).ValueNumericOptions.Precision = 0;
                        }
                        else
                        {
                            ((PiePointOptions)series.PointOptions).ValueNumericOptions.Format = NumericFormat.FixedPoint;
                            ((PiePointOptions)series.PointOptions).ValueNumericOptions.Precision = 0;
                        }
                        ((PiePointOptions)series.PointOptions).PercentOptions.ValueAsPercent = state;
                        //((PiePointOptions)series.PointOptions).PointView = PointView.Argument;
                        ((PiePointOptions)series.LegendPointOptions).PointView = PointView.Argument;

                    }
                }
            }

            if (viewType == ViewType.Bar3D || viewType == ViewType.StackedBar3D || viewType == ViewType.FullStackedBar3D || viewType == ViewType.Area || viewType == ViewType.SplineArea || viewType == ViewType.Area3D || viewType == ViewType.StackedArea3D || viewType == ViewType.FullStackedArea3D || viewType == ViewType.SplineArea3D || viewType == ViewType.StackedSplineArea3D || viewType == ViewType.FullStackedSplineArea3D)
            {
                byte transparency = Convert.ToByte(SurQues.TRANSPARENCY);
                foreach (Series series in wcCtrl.Series)
                {
                    if (viewType == ViewType.Bar3D || viewType == ViewType.StackedBar3D || viewType == ViewType.FullStackedBar3D)
                        ((Bar3DSeriesView)series.View).Transparency = transparency;
                    if (viewType == ViewType.Area)
                        ((AreaSeriesView)series.View).Transparency = transparency;
                    else if (viewType == ViewType.SplineArea)
                        ((SplineAreaSeriesView)series.View).Transparency = transparency;
                    else if (viewType == ViewType.Area3D || viewType == ViewType.StackedArea3D || viewType == ViewType.FullStackedArea3D)
                        ((Area3DSeriesView)series.View).Transparency = transparency;
                    else if (viewType == ViewType.SplineArea3D)
                        ((SplineArea3DSeriesView)series.View).Transparency = transparency;
                    else if (viewType == ViewType.StackedSplineArea3D)
                        ((StackedSplineArea3DSeriesView)series.View).Transparency = transparency;
                    else if (viewType == ViewType.FullStackedSplineArea3D)
                        ((FullStackedSplineArea3DSeriesView)series.View).Transparency = transparency;
                }
            }

            if (viewType == ViewType.Point || viewType == ViewType.Line || viewType == ViewType.StepLine || viewType == ViewType.Area || viewType == ViewType.SplineArea || viewType == ViewType.PolarArea || viewType == ViewType.PolarLine || viewType == ViewType.PolarPoint || viewType == ViewType.RadarArea || viewType == ViewType.RadarLine || viewType == ViewType.RadarPoint)
            {
                MarkerKind MarKind = GetMarkerKing(SurQues.MARKER_KIND);
                foreach (Series series in wcCtrl.Series)
                {
                    if (viewType == ViewType.Point || viewType == ViewType.Line || viewType == ViewType.StepLine || viewType == ViewType.Area || viewType == ViewType.SplineArea)
                    {
                        ((PointSeriesView)series.View).PointMarkerOptions.Kind = MarKind;
                        if (SurQues.MARKER_SIZE > 1)
                        {
                            if (viewType != ViewType.StepLine)
                                ((PointSeriesView)series.View).PointMarkerOptions.Size = SurQues.MARKER_SIZE;
                            else
                                ((StepLineSeriesView)series.View).PointMarkerOptions.Size = SurQues.MARKER_SIZE;
                        }
                    }
                    else if (viewType == ViewType.PolarArea || viewType == ViewType.PolarLine || viewType == ViewType.PolarPoint || viewType == ViewType.RadarArea || viewType == ViewType.RadarLine || viewType == ViewType.RadarPoint)
                    {
                        ((RadarPointSeriesView)series.View).PointMarkerOptions.Kind = MarKind;
                        if (SurQues.MARKER_SIZE > 1)
                        {
                            if (viewType == ViewType.RadarPoint || viewType == ViewType.RadarLine || viewType == ViewType.RadarArea || viewType == ViewType.PolarPoint)
                                ((RadarPointSeriesView)series.View).PointMarkerOptions.Size = SurQues.MARKER_SIZE;
                            else if (viewType == ViewType.PolarArea || viewType == ViewType.PolarLine)
                                ((RadarLineSeriesView)series.View).LineMarkerOptions.Size = SurQues.MARKER_SIZE;
                        }
                        if (viewType == ViewType.RadarArea)
                            ((RadarAreaSeriesView)series.View).MarkerOptions.Visible = Convert.ToBoolean(SurQues.SHOW_MARKER);
                        else if (viewType == ViewType.PolarArea || viewType == ViewType.PolarLine || viewType == ViewType.RadarLine)
                            ((RadarLineSeriesView)series.View).LineMarkerOptions.Visible = Convert.ToBoolean(SurQues.SHOW_MARKER);
                    }
                }
            }
            if (viewType == ViewType.StepLine)
                ((StepLineSeriesView)wcCtrl.Series[0].View).InvertedStep = Convert.ToBoolean(SurQues.INVERTED);
            else if (viewType == ViewType.StepLine3D)
                ((StepLine3DSeriesView)wcCtrl.Series[0].View).InvertedStep = Convert.ToBoolean(SurQues.INVERTED);
            if (SurQues.EXPLODED_POINT != null && SurQues.EXPLODED_POINT.Trim().Length > 0)
            {
                if (viewType == ViewType.Pie || viewType == ViewType.Doughnut || viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D)
                    ApplyMode((PieSeriesViewBase)wcCtrl.Series[0].View, SurQues.EXPLODED_POINT);
            }
            if (SurQues.HOLE_RADIUS != null && SurQues.HOLE_RADIUS.Trim().Length > 0)
            {
                if (viewType == ViewType.Doughnut)
                    ((DoughnutSeriesView)wcCtrl.Series[0].View).HoleRadiusPercent = Convert.ToInt32(SurQues.HOLE_RADIUS);
                else if (viewType == ViewType.Doughnut3D)
                    ((Doughnut3DSeriesView)wcCtrl.Series[0].View).HoleRadiusPercent = Convert.ToInt32(SurQues.HOLE_RADIUS);
            }

            wcCtrl.Legend.AlignmentHorizontal = GetLegendHoriAlign(SurQues.LEGEND_HORIZONTALALIGN);
            wcCtrl.Legend.AlignmentVertical = GetLegendVeriAlign(SurQues.LEGEND_VERTICALALIGN);
            if (SurQues.LEGEND_MAX_HORIZONTALALIGN > 0)
                wcCtrl.Legend.MaxHorizontalPercentage = Convert.ToDouble(SurQues.LEGEND_MAX_HORIZONTALALIGN);
            else
                wcCtrl.Legend.MaxHorizontalPercentage = 100;
            //wcCtrl.Legend.MaxHorizontalPercentage = 25;
            if (SurQues.LEGEND_MAX_VERTICALALIGN > 0)
                wcCtrl.Legend.MaxVerticalPercentage = Convert.ToDouble(SurQues.LEGEND_MAX_VERTICALALIGN);
            else
                wcCtrl.Legend.MaxVerticalPercentage = 50;
            //wcCtrl.Legend.MaxVerticalPercentage = 100;
            wcCtrl.Legend.Direction = GetLegendDirection(SurQues.LEGEND_DIRECTION);
            if (Convert.ToString(wcCtrl.Legend.Direction) != "TopToBottom")
                wcCtrl.Legend.EquallySpacedItems = Convert.ToBoolean(SurQues.LEGEND_EQUALLYSPACED_ITEMS);
            wcCtrl.Legend.Visible = Convert.ToBoolean(SurQues.HIDE_LEGAND);

            //if (viewType == ViewType.Area || viewType == ViewType.Area3D || viewType == ViewType.Line || viewType == ViewType.Line3D || viewType == ViewType.RadarArea || viewType == ViewType.Spline || viewType == ViewType.RadarLine || viewType == ViewType.Spline3D || viewType == ViewType.SplineArea || viewType == ViewType.SplineArea3D || viewType == ViewType.StackedArea || viewType == ViewType.StackedArea3D || viewType == ViewType.StackedSplineArea || viewType == ViewType.StackedSplineArea3D || viewType == ViewType.StepLine || viewType == ViewType.StepLine3D)
            //{
            //    wcCtrl.Legend.Visible = false;
            //}

            return (wcCtrl);
        }



        public static LegendAlignmentHorizontal GetLegendHoriAlign(string HoriAlign)
        {
            //LegendAlignmentHorizontal AlignTyp = LegendAlignmentHorizontal.RightOutside;
            LegendAlignmentHorizontal AlignTyp = LegendAlignmentHorizontal.Left;
            switch (HoriAlign)
            {
                case "Center":
                    AlignTyp = LegendAlignmentHorizontal.Center;
                    break;
                case "Left":
                    AlignTyp = LegendAlignmentHorizontal.Left;
                    break;
                case "LeftOutside":
                    AlignTyp = LegendAlignmentHorizontal.LeftOutside;
                    break;
                case "Right":
                    AlignTyp = LegendAlignmentHorizontal.Right;
                    break;
                case "RightOutside":
                    AlignTyp = LegendAlignmentHorizontal.RightOutside;
                    break;
            }
            return AlignTyp;
        }
        public static LegendAlignmentVertical GetLegendVeriAlign(string VertAlign)
        {
            //LegendAlignmentVertical AlignTyp = LegendAlignmentVertical.Top;
            LegendAlignmentVertical AlignTyp = LegendAlignmentVertical.TopOutside;
            switch (VertAlign)
            {
                case "Bottom":
                    AlignTyp = LegendAlignmentVertical.Bottom;
                    break;
                case "BottomOutside":
                    AlignTyp = LegendAlignmentVertical.BottomOutside;
                    break;
                case "Center":
                    AlignTyp = LegendAlignmentVertical.Center;
                    break;
                case "Top":
                    AlignTyp = LegendAlignmentVertical.Top;
                    break;
                case "TopOutside":
                    AlignTyp = LegendAlignmentVertical.TopOutside;
                    break;
            }
            return AlignTyp;
        }
        public static LegendDirection GetLegendDirection(string Dire)
        {
            LegendDirection DireTyp = LegendDirection.LeftToRight;
            switch (Dire)
            {
                case "BottomToTop":
                    DireTyp = LegendDirection.BottomToTop;
                    break;
                case "LeftToRight":
                    DireTyp = LegendDirection.LeftToRight;
                    break;
                case "RightToLeft":
                    DireTyp = LegendDirection.RightToLeft;
                    break;
                case "TopToBottom":
                    DireTyp = LegendDirection.TopToBottom;
                    break;
            }
            return DireTyp;
        }

        private static double GetLineFactor(string index)
        {
            switch (index)
            {
                case "Circles":
                    return 1;
                case "Cardioid":
                    return 0.5;
                case "Lemniscate":
                    return 2;
                default:
                    return 2;
            }
        }
        private static double ToRadian(double angle)
        {
            return angle * Math.PI / 180.0;
        }
        private static double Function(double m, double angle)
        {
            double cos = Math.Cos(m * ToRadian(90.0 + angle));
            return Math.Pow(Math.Abs(cos), m);
        }
        private static SeriesPoint[] GenerateFunctionPoints(string index, int pointCount)
        {
            double m = GetLineFactor(index);
            int step = 360 / pointCount;
            SeriesPoint[] points = new SeriesPoint[pointCount];
            for (int i = 0; i < pointCount; i++)
                points[i] = new SeriesPoint(step * i, new double[] { Function(m, step * i) });
            return points;
        }

        private static void ApplyMode(PieSeriesViewBase view, string mode)
        {
            switch (mode)
            {
                case "Custom":
                    view.ExplodeMode = PieExplodeMode.UsePoints;
                    break;
                case "None":
                    view.ExplodeMode = PieExplodeMode.None;
                    break;
                case "All":
                    view.ExplodeMode = PieExplodeMode.All;
                    break;
                case "Min Value":
                    view.ExplodeMode = PieExplodeMode.MinValue;
                    break;
                case "Max Value":
                    view.ExplodeMode = PieExplodeMode.MaxValue;
                    break;
                default:
                    view.ExplodedPointsFilters.Clear();
                    view.ExplodedPointsFilters.Add(new SeriesPointFilter(SeriesPointKey.Argument, DataFilterCondition.Equal, mode));
                    view.ExplodeMode = PieExplodeMode.UseFilters;
                    break;
            }
        }
        public static ChartTitleDockStyle GetTltleDock(string Dock)
        {

            ChartTitleDockStyle DockTyp = ChartTitleDockStyle.Top;
            switch (Dock)
            {
                case "Bottom":
                    DockTyp = ChartTitleDockStyle.Bottom;
                    break;
                case "Left":
                    DockTyp = ChartTitleDockStyle.Left;
                    break;
                case "Right":
                    DockTyp = ChartTitleDockStyle.Right;
                    break;
                case "Top":
                    DockTyp = ChartTitleDockStyle.Top;
                    break;
            }
            return DockTyp;
        }
        public static StringAlignment GetTltleAlignment(string Align)
        {
            StringAlignment AlignTyp = StringAlignment.Center;
            switch (Align)
            {
                case "Center":
                    AlignTyp = StringAlignment.Center;
                    break;
                case "Far":
                    AlignTyp = StringAlignment.Far;
                    break;
                case "Near":
                    AlignTyp = StringAlignment.Near;
                    break;
            }
            return AlignTyp;
        }



        public static ViewType GetChartViewType(string VieTyp, int ques_type)
        {
            ViewType viewType = ViewType.Pie;
            if (ques_type > 0)
            {
                if (ques_type == 1)
                {
                    viewType = ViewType.Bar;
                }
                else if (ques_type == 2)
                {
                    viewType = ViewType.Bar3D;
                }
                else if (ques_type == 3)
                {
                    viewType = ViewType.Doughnut;
                }
                else if (ques_type == 4)
                {
                    viewType = ViewType.Doughnut3D;
                }
                else if (ques_type == 10 || ques_type == 11 || ques_type == 12)
                {
                    viewType = ViewType.StackedBar;
                }
                else if (ques_type == 15)
                {
                    viewType = ViewType.StackedArea;
                }
            }

            //ViewType viewType = ViewType.Pie;
            switch (VieTyp)
            {
                case "Area":
                    viewType = ViewType.Area;
                    break;
                case "Area3D":
                    viewType = ViewType.Area3D;
                    break;
                case "Bar":
                    viewType = ViewType.Bar;
                    break;
                case "Bar3D":
                    viewType = ViewType.Bar3D;
                    break;
                case "CandleStick":
                    viewType = ViewType.CandleStick;
                    break;
                case "Doughnut":
                    viewType = ViewType.Doughnut;
                    break;
                case "Doughnut3D":
                    viewType = ViewType.Doughnut3D;
                    break;
                case "FullStackedArea":
                    viewType = ViewType.FullStackedArea;
                    break;
                case "FullStackedArea3D":
                    viewType = ViewType.FullStackedArea3D;
                    break;
                case "FullStackedBar":
                    viewType = ViewType.FullStackedBar;
                    break;
                case "FullStackedBar3D":
                    viewType = ViewType.FullStackedBar3D;
                    break;
                case "FullStackedSplineArea":
                    viewType = ViewType.FullStackedSplineArea;
                    break;
                case "FullStackedSplineArea3D":
                    viewType = ViewType.FullStackedSplineArea3D;
                    break;
                case "Gantt":
                    viewType = ViewType.Gantt;
                    break;
                case "Line":
                    viewType = ViewType.Line;
                    break;
                case "Line3D":
                    viewType = ViewType.Line3D;
                    break;
                case "ManhattanBar":
                    viewType = ViewType.ManhattanBar;
                    break;
                case "Pie":
                    viewType = ViewType.Pie;
                    break;
                case "Pie3D":
                    viewType = ViewType.Pie3D;
                    break;
                case "Point":
                    viewType = ViewType.Point;
                    break;
                case "PolarArea":
                    viewType = ViewType.PolarArea;
                    break;
                case "PolarLine":
                    viewType = ViewType.PolarLine;
                    break;
                case "PolarPoint":
                    viewType = ViewType.PolarPoint;
                    break;
                case "RadarArea":
                    viewType = ViewType.RadarArea;
                    break;
                case "RadarLine":
                    viewType = ViewType.RadarLine;
                    break;
                case "RadarPoint":
                    viewType = ViewType.RadarPoint;
                    break;
                case "RangeBar":
                    viewType = ViewType.RangeBar;
                    break;
                case "SideBySideGantt":
                    viewType = ViewType.SideBySideGantt;
                    break;
                case "SideBySideRangeBar":
                    viewType = ViewType.SideBySideRangeBar;
                    break;
                case "Spline":
                    viewType = ViewType.Spline;
                    break;
                case "Spline3D":
                    viewType = ViewType.Spline3D;
                    break;
                case "SplineArea":
                    viewType = ViewType.SplineArea;
                    break;
                case "SplineArea3D":
                    viewType = ViewType.SplineArea3D;
                    break;
                case "StackedArea":
                    viewType = ViewType.StackedArea;
                    break;
                case "StackedArea3D":
                    viewType = ViewType.StackedArea3D;
                    break;
                case "StackedBar":
                    viewType = ViewType.StackedBar;
                    break;
                case "StackedSplineArea":
                    viewType = ViewType.StackedSplineArea;
                    break;
                case "StackedSplineArea3D":
                    viewType = ViewType.StackedSplineArea3D;
                    break;
                case "StepLine":
                    viewType = ViewType.StepLine;
                    break;
                case "StepLine3D":
                    viewType = ViewType.StepLine3D;
                    break;
                case "Stock":
                    viewType = ViewType.Stock;
                    break;
                case "StackedBar3D":
                    viewType = ViewType.StackedBar3D;
                    break;
            }
            return viewType;
        }
        public static PieSeriesLabelPosition GetLabelPostion(string Postion)
        {

            PieSeriesLabelPosition Pos = PieSeriesLabelPosition.Inside;
            switch (Postion)
            {
                case "Inside":
                    Pos = PieSeriesLabelPosition.Inside;
                    break;
                case "Outside":
                    Pos = PieSeriesLabelPosition.Outside;
                    break;
                case "Radial":
                    Pos = PieSeriesLabelPosition.Radial;
                    break;
                case "Tangent":
                    Pos = PieSeriesLabelPosition.Tangent;
                    break;
                case "TwoColumns":
                    Pos = PieSeriesLabelPosition.TwoColumns;
                    break;
                default:
                    break;
            }
            return Pos;
        }
        private static MarkerKind GetMarkerKing(string Kind)
        {

            MarkerKind MarKind = MarkerKind.Circle;
            switch (Kind)
            {
                case "Circle":
                    MarKind = MarkerKind.Circle;
                    break;
                case "Cross":
                    MarKind = MarkerKind.Cross;
                    break;
                case "Diamond":
                    MarKind = MarkerKind.Diamond;
                    break;
                case "Hexagon":
                    MarKind = MarkerKind.Hexagon;
                    break;
                case "InvertedTriangle":
                    MarKind = MarkerKind.InvertedTriangle;
                    break;
                case "Pentagon":
                    MarKind = MarkerKind.Pentagon;
                    break;
                case "Plus":
                    MarKind = MarkerKind.Plus;
                    break;
                case "Square":
                    MarKind = MarkerKind.Square;
                    break;
                case "Star":
                    MarKind = MarkerKind.Star;
                    break;
                case "Triangle":
                    MarKind = MarkerKind.Triangle;
                    break;
                default:
                    break;
            }
            return MarKind;
        }

        //private static RepeatDirection GetDirection(string Align)
        //{
        //    RepeatDirection AlignTyp = RepeatDirection.Vertical;
        //    switch (Align)
        //    {
        //        case "Vertical":
        //            AlignTyp = RepeatDirection.Vertical;
        //            break;
        //        case "Horizontal":
        //            AlignTyp = RepeatDirection.Horizontal;
        //            break;
        //    }
        //    return AlignTyp;
        //}
        //private static FontUnit SetFontSize(string FontSiz)
        //{
        //    if (FontSiz != null && FontSiz.Length > 0)
        //    {
        //        switch (FontSiz.Trim())
        //        {
        //            case "7":
        //                Fsize = FontUnit.XXSmall;
        //                break;
        //            case "8":
        //                Fsize = FontUnit.XSmall;
        //                break;
        //            case "9":
        //                Fsize = FontUnit.Smaller;
        //                break;
        //            case "10":
        //                Fsize = FontUnit.Small;
        //                break;
        //            case "11":
        //                Fsize = FontUnit.Medium;
        //                break;
        //            case "12":
        //                Fsize = FontUnit.Large;
        //                break;
        //            case "13":
        //                Fsize = FontUnit.Larger;
        //                break;
        //            case "14":
        //                Fsize = FontUnit.XLarge;
        //                break;
        //            case "15":
        //                Fsize = FontUnit.XXLarge;
        //                break;

        //        }
        //    }
        //    return Fsize;
        //}
        //private static Color SetFontColor(string FontColor)
        //{
        //    if (FontColor != null && FontColor.Length > 0)
        //    {
        //        switch (FontColor)
        //        {
        //            case "AliceBlue":
        //                FColor = Color.AliceBlue;
        //                break;
        //            case "AntiqueWhite":
        //                FColor = Color.AntiqueWhite;
        //                break;
        //            case "Aqua":
        //                FColor = Color.Aqua;
        //                break;
        //            case "Aquamarine":
        //                FColor = Color.Aquamarine;
        //                break;
        //            case "Azure":
        //                FColor = Color.Azure;
        //                break;
        //            case "Beige":
        //                FColor = Color.Beige;
        //                break;
        //            case "Bisque":
        //                FColor = Color.Bisque;
        //                break;
        //            case "Black":
        //                FColor = Color.Black;
        //                break;
        //            case "BlanchedAlmond":
        //                FColor = Color.BlanchedAlmond;
        //                break;
        //            case "Blue":
        //                FColor = Color.Blue;
        //                break;
        //            case "BlueViolet":
        //                FColor = Color.BlueViolet;
        //                break;
        //            case "Brown":
        //                FColor = Color.Brown;
        //                break;
        //            case "BurlyWood":
        //                FColor = Color.BurlyWood;
        //                break;
        //        }
        //    }
        //    return FColor;
        //}
        private static string SetFontName(string FonName)
        {
            if (FonName != null && FonName.Trim().Length > 0)
                Fname = FonName;
            return Fname;
        }

        public static DateTime GetConvertedDatetime(DateTime value, int bias, string TimeZone, bool server_flag)
        {
            DateTime ret_value = value;
            if (value != null)
            {
                if (server_flag == true)
                    ret_value = value.AddMinutes(bias);
                else
                    ret_value = DateTime.SpecifyKind(value, DateTimeKind.Utc);

            }
            return ret_value;

        }

        public static void SetChartPreview(WebChartControl wcCtrl, SurveyQuestion SurQues, string ChartViewType)
        {
            ValidationCore cor = new ValidationCore();
            //ViewType viewType = GetChartViewType(ChartViewType);
            ViewType viewType = GetChartViewType(ChartViewType, SurQues.QUESTION_TYPE);


            if (viewType == ViewType.Area || viewType == ViewType.SplineArea || viewType == ViewType.Point)
            {
                for (int i = 0; i < wcCtrl.Series.Count; i++)
                {
                    ((PointSeriesLabel)wcCtrl.Series[i].Label).Angle = SurQues.LABEL_ANGLE;
                }
            }

            if (viewType == ViewType.FullStackedBar || viewType == ViewType.FullStackedBar3D || viewType == ViewType.FullStackedArea || viewType == ViewType.FullStackedSplineArea || viewType == ViewType.Pie || viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D || viewType == ViewType.Doughnut)
            {
                foreach (Series series in wcCtrl.Series)
                {
                    bool state = Convert.ToBoolean(SurQues.VALUEAS_PERCENT);
                    if (viewType == ViewType.FullStackedBar || viewType == ViewType.FullStackedBar3D)
                    {
                        ((FullStackedBarPointOptions)series.PointOptions).PercentOptions.ValueAsPercent = state;
                    }
                    else if (viewType == ViewType.FullStackedArea || viewType == ViewType.FullStackedSplineArea)
                    {
                        ((FullStackedAreaPointOptions)series.PointOptions).PercentOptions.ValueAsPercent = state;
                    }
                    else if (viewType == ViewType.Pie || viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D || viewType == ViewType.Doughnut)
                    {
                        if (state)
                        {
                            ((PiePointOptions)series.PointOptions).ValueNumericOptions.Format = NumericFormat.Percent;
                            ((PiePointOptions)series.PointOptions).ValueNumericOptions.Precision = 0;
                            ((PiePointOptions)series.PointOptions).PercentOptions.ValueAsPercent = state;
                        }
                        else
                        {
                            //((PiePointOptions)series.PointOptions).ValueNumericOptions.Format = NumericFormat.FixedPoint;
                            //((PiePointOptions)series.PointOptions).ValueNumericOptions.Precision = 1;

                            (series.PointOptions).ValueNumericOptions.Format = NumericFormat.FixedPoint;
                            (series.PointOptions).ValueNumericOptions.Precision = 1;
                            //(series.PointOptions).PercentOptions.ValueAsPercent = state;
                        }
                        //((PiePointOptions)series.PointOptions).PercentOptions.ValueAsPercent = state;
                    }
                }
            }

            if (viewType == ViewType.Bar3D || viewType == ViewType.StackedBar3D || viewType == ViewType.FullStackedBar3D || viewType == ViewType.Area || viewType == ViewType.SplineArea || viewType == ViewType.Area3D || viewType == ViewType.StackedArea3D || viewType == ViewType.FullStackedArea3D || viewType == ViewType.SplineArea3D || viewType == ViewType.StackedSplineArea3D || viewType == ViewType.FullStackedSplineArea3D)
            {
                byte transparency = Convert.ToByte(SurQues.TRANSPARENCY);
                foreach (Series series in wcCtrl.Series)
                {
                    if (viewType == ViewType.Bar3D || viewType == ViewType.StackedBar3D || viewType == ViewType.FullStackedBar3D)
                        ((Bar3DSeriesView)series.View).Transparency = transparency;
                    if (viewType == ViewType.Area)
                        ((AreaSeriesView)series.View).Transparency = transparency;
                    else if (viewType == ViewType.SplineArea)
                        ((SplineAreaSeriesView)series.View).Transparency = transparency;
                    else if (viewType == ViewType.Area3D || viewType == ViewType.StackedArea3D || viewType == ViewType.FullStackedArea3D)
                        ((Area3DSeriesView)series.View).Transparency = transparency;
                    else if (viewType == ViewType.SplineArea3D)
                        ((SplineArea3DSeriesView)series.View).Transparency = transparency;
                    else if (viewType == ViewType.StackedSplineArea3D)
                        ((StackedSplineArea3DSeriesView)series.View).Transparency = transparency;
                    else if (viewType == ViewType.FullStackedSplineArea3D)
                        ((FullStackedSplineArea3DSeriesView)series.View).Transparency = transparency;
                }
            }

            if (viewType == ViewType.Point || viewType == ViewType.Line || viewType == ViewType.StepLine || viewType == ViewType.Area || viewType == ViewType.SplineArea || viewType == ViewType.PolarArea || viewType == ViewType.PolarLine || viewType == ViewType.PolarPoint || viewType == ViewType.RadarArea || viewType == ViewType.RadarLine || viewType == ViewType.RadarPoint)
            {
                MarkerKind MarKind = GetMarkerKing(SurQues.MARKER_KIND);
                foreach (Series series in wcCtrl.Series)
                {
                    if (viewType == ViewType.Point || viewType == ViewType.Line || viewType == ViewType.StepLine || viewType == ViewType.Area || viewType == ViewType.SplineArea)
                    {
                        ((PointSeriesView)series.View).PointMarkerOptions.Kind = MarKind;
                        if (SurQues.MARKER_SIZE > 1)
                        {
                            if (viewType != ViewType.StepLine)
                                ((PointSeriesView)series.View).PointMarkerOptions.Size = SurQues.MARKER_SIZE;
                            else
                                ((StepLineSeriesView)series.View).PointMarkerOptions.Size = SurQues.MARKER_SIZE;
                        }
                    }
                    else if (viewType == ViewType.PolarArea || viewType == ViewType.PolarLine || viewType == ViewType.PolarPoint || viewType == ViewType.RadarArea || viewType == ViewType.RadarLine || viewType == ViewType.RadarPoint)
                    {
                        ((RadarPointSeriesView)series.View).PointMarkerOptions.Kind = MarKind;
                        if (SurQues.MARKER_SIZE > 1)
                        {
                            if (viewType == ViewType.RadarPoint || viewType == ViewType.RadarLine || viewType == ViewType.RadarArea || viewType == ViewType.PolarPoint)
                                ((RadarPointSeriesView)series.View).PointMarkerOptions.Size = SurQues.MARKER_SIZE;
                            else if (viewType == ViewType.PolarArea || viewType == ViewType.PolarLine)
                                ((RadarLineSeriesView)series.View).LineMarkerOptions.Size = SurQues.MARKER_SIZE;
                        }
                        if (viewType == ViewType.RadarArea)
                            ((RadarAreaSeriesView)series.View).MarkerOptions.Visible = Convert.ToBoolean(SurQues.SHOW_MARKER);
                        else if (viewType == ViewType.PolarArea || viewType == ViewType.PolarLine || viewType == ViewType.RadarLine)
                            ((RadarLineSeriesView)series.View).LineMarkerOptions.Visible = Convert.ToBoolean(SurQues.SHOW_MARKER);
                    }
                }
            }
            if (viewType == ViewType.StepLine)
                ((StepLineSeriesView)wcCtrl.Series[0].View).InvertedStep = Convert.ToBoolean(SurQues.INVERTED);
            else if (viewType == ViewType.StepLine3D)
                ((StepLine3DSeriesView)wcCtrl.Series[0].View).InvertedStep = Convert.ToBoolean(SurQues.INVERTED);
            if (SurQues.EXPLODED_POINT != null && SurQues.EXPLODED_POINT.Trim().Length > 0)
            {
                if (viewType == ViewType.Pie || viewType == ViewType.Doughnut || viewType == ViewType.Pie3D || viewType == ViewType.Doughnut3D)
                    ApplyMode((PieSeriesViewBase)wcCtrl.Series[0].View, SurQues.EXPLODED_POINT);
            }
            if (SurQues.HOLE_RADIUS != null && SurQues.HOLE_RADIUS.Trim().Length > 0)
            {
                if (viewType == ViewType.Doughnut)
                    ((DoughnutSeriesView)wcCtrl.Series[0].View).HoleRadiusPercent = Convert.ToInt32(SurQues.HOLE_RADIUS);
                else if (viewType == ViewType.Doughnut3D)
                    ((Doughnut3DSeriesView)wcCtrl.Series[0].View).HoleRadiusPercent = Convert.ToInt32(SurQues.HOLE_RADIUS);
            }
        }

        public static void AddChartTitles(WebChartControl webchrtcntrl, string titleTxt, string ComntTitle, string DockSty, string Alignment)
        {
            webchrtcntrl.Titles.Clear();
            ChartTitle cTitle = new ChartTitle();
            cTitle.Font = new Font("Trebuchet MS", 10.0F, FontStyle.Regular);
            cTitle.Dock = BuildQuetions.GetTltleDock(DockSty);
            cTitle.Alignment = BuildQuetions.GetTltleAlignment(Alignment);
            if (titleTxt.Length > 80)
                cTitle.Lines = GetTitleMultiLines(titleTxt);
            else
                cTitle.Text = titleTxt;

            ChartTitle CommentTitle = new ChartTitle();
            if (ComntTitle != null && ComntTitle.Length > 80)
            {
                CommentTitle.Lines = GetTitleMultiLines(ComntTitle);
            }
            else
            {
                CommentTitle.Text = ComntTitle;
            }

            CommentTitle.Font = new Font("Trebuchet MS", 10.0F, FontStyle.Regular);
            CommentTitle.Alignment = StringAlignment.Center;
            CommentTitle.Dock = ChartTitleDockStyle.Bottom;

            webchrtcntrl.Titles.Add(cTitle);
            webchrtcntrl.Titles.Add(CommentTitle);
        }
        private static string[] GetTitleMultiLines(string titleTxt)
        {
            List<string> al = new List<string>();
            for (int i = 0; titleTxt.Length > 0; i++)
            {
                if (titleTxt.Length > 80)
                {
                    al.Add(titleTxt.Substring(0, 80));
                    titleTxt = titleTxt.Substring(80);
                }
                else
                {
                    al.Add(titleTxt);
                    titleTxt = "";
                }
            }
            return al.ToArray();
        }


        //public static bool CheckQuestionType(int Ques, FeaturesRestriction fea)
        //{
        //    bool chk = false;

        //    try
        //    {
        //        switch (Ques)
        //        {
        //            case 1:
        //                chk = Convert.ToBoolean(fea.Q_SINGLESELECT);
        //                break;
        //            case 2:
        //                chk = Convert.ToBoolean(fea.Q_MULTISELECT);
        //                break;
        //            case 3:
        //                chk = Convert.ToBoolean(fea.Q_DROPDOWN);
        //                break;
        //            case 4:
        //                chk = Convert.ToBoolean(fea.Q_YESNO);
        //                break;
        //            case 5:
        //                chk = Convert.ToBoolean(fea.QT_COMMENT);
        //                break;
        //            case 6:
        //                chk = Convert.ToBoolean(fea.QT_SINGLEROW);
        //                break;
        //            case 7:
        //                chk = Convert.ToBoolean(fea.Q_NUMERIC);
        //                break;
        //            case 8:
        //                chk = Convert.ToBoolean(fea.Q_EMAIL);
        //                break;
        //            case 9:
        //                chk = Convert.ToBoolean(fea.QT_MULTIROW);
        //                break;
        //            case 10:
        //                chk = Convert.ToBoolean(fea.QM_SINGLESELECT);
        //                break;
        //            case 11:
        //                chk = Convert.ToBoolean(fea.QM_MULTISELECT);
        //                break;
        //            case 12:
        //                chk = Convert.ToBoolean(fea.QM_SIDEBYSIDE);
        //                break;
        //            case 13:
        //                chk = Convert.ToBoolean(fea.Q_CONSTANTSUM);
        //                break;
        //            case 14:
        //                chk = Convert.ToBoolean(fea.Q_DATETIME);
        //                break;
        //            case 15:
        //                chk = Convert.ToBoolean(fea.Q_RANKING);
        //                break;
        //            case 16:
        //                chk = Convert.ToBoolean(fea.Q_NARRATION);
        //                break;
        //            case 17:
        //                chk = Convert.ToBoolean(fea.Q_PAGEHEADER);
        //                break;
        //            case 18:
        //                chk = Convert.ToBoolean(fea.Q_QUESTIONHEADER);
        //                break;
        //            case 19:
        //                chk = Convert.ToBoolean(fea.Q_IMAGE);
        //                break;
        //            case 20:
        //                chk = Convert.ToBoolean(fea.Q_CONTACTINFO);
        //                break;
        //            default:
        //                break;
        //        }
        //    }
        //    catch { }

        //    return chk;
        //}


        //public static List<ArrayList> RawDataFormatted(Survey Sur,SurveyRespondents Respond,int bias)
        //{
        //    ArrayList Arr_RawFormat = new ArrayList();
        //    Arr_RawFormat = GetHeaderRowForRawExport(Sur);
        //    List<ArrayList> RawdataRows = new List<ArrayList>();
        //    RawdataRows.Add(Arr_RawFormat);
        //    if (Respond != null && Respond.SurveyResp.Count > 0)
        //    {
        //        //List<SurveyQuestion> lst = Sur.surveyQues();
        //        Hashtable ht_itemsText = GetHashedAnsOptions(Sur);
        //        int seq = 1;
        //        foreach (SurveyResponse Res in Respond.SurveyResp)
        //        {
        //            Arr_RawFormat = GetRespondentAnswers(Sur, Res,ht_itemsText,bias,seq);
        //            RawdataRows.Add(Arr_RawFormat);
        //            seq++;
        //        }

        //    }

        //    return RawdataRows;
        //}

        public static List<ArrayList> RawDataFormatted(Survey Sur, SurveyRespondents Respond, int bias)
        {
            ArrayList Arr_RawFormat = new ArrayList();
            Arr_RawFormat = GetHeaderRowForRawExport(Sur);
            List<ArrayList> RawdataRows = new List<ArrayList>();
            RawdataRows.Add(Arr_RawFormat);
            if (Respond != null && Respond.SurveyResp.Count > 0)
            {
                //List<SurveyQuestion> lst = Sur.surveyQues();
                Hashtable ht_itemsText = GetHashedAnsOptions(Sur);
                int seq = 1;
             
              foreach (SurveyResponse Res in Respond.SurveyResp)
               
                {
                    Arr_RawFormat = GetRespondentAnswers(Sur, Res, ht_itemsText, bias, seq);
                    RawdataRows.Add(Arr_RawFormat);
                    seq++;
                }

            }

            return RawdataRows;
        }
        //private static Hashtable GetHashedAnsOptions(Survey Sur)
        //{
        //    Hashtable ht_Ansitems=new Hashtable();
        //    ValidationCore vcore = new ValidationCore(); 
        //    foreach(SurveyQuestion Ques in Sur.surveyQues)
        //    {
        //       if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20)
        //       {
        //           foreach (SurveyAnswers Ans in Ques.surveyAnswers)
        //           {
        //               ht_Ansitems.Add(Ans.ANSWER_ID, vcore.clearHTML(Ans.ANSWER_OPTIONS).Replace("\r\n", ""));
        //           }
        //           //if(Ques.OTHER_ANS==1 && (Ques.QUESTION_TYPE==1 || Ques.QUESTION_TYPE==2 || Ques.QUESTION_TYPE==3 || Ques.QUESTION_TYPE==4))
        //           //{


        //           //}
        //       }
        //       else if (Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12 || Ques.QUESTION_TYPE == 13)
        //       {
        //           Hashtable headerList = GetAnsItemsList(Ques); 
        //           ArrayList colHead = new ArrayList();
        //           ArrayList ColSubHead = new ArrayList();
        //           ArrayList rowHead = new ArrayList();
        //            if (headerList.Contains("ColHead"))
        //                colHead = (ArrayList)headerList["ColHead"];
        //            if (headerList.Contains("ColSubHead"))
        //                ColSubHead = (ArrayList)headerList["ColSubHead"];
        //            if (headerList.Contains("RowHead"))
        //                rowHead = (ArrayList)headerList["RowHead"];
        //           if(Ques.QUESTION_TYPE==10)
        //           {
        //               int i=0;
        //               foreach (SurveyAnswers Ans in Ques.surveyAnswers)
        //               {
        //                   ht_Ansitems.Add(Ans.ANSWER_ID,vcore.clearHTML(colHead[i].ToString()).Replace("\r\n", ""));
        //                   i++;
        //                   if(i==colHead.Count)
        //                       i=0;
        //               }
        //           }
        //           else if(Ques.QUESTION_TYPE==11)
        //           {
        //               foreach (SurveyAnswers Ans in Ques.surveyAnswers)
        //               {
        //                   ht_Ansitems.Add(Ans.ANSWER_ID, vcore.clearHTML(Ans.ANSWER_OPTIONS).Replace("\r\n", ""));

        //               }

        //           }
        //           else if(Ques.QUESTION_TYPE==12)
        //           {
        //               int i=0,m=0;
        //               int count=colHead.Count *ColSubHead.Count;
        //               foreach (SurveyAnswers Ans in Ques.surveyAnswers)
        //               {
        //                   ht_Ansitems.Add(Ans.ANSWER_ID,vcore.clearHTML(ColSubHead[m].ToString()).Replace("\r\n", ""));
        //                   i++;
        //                   m++;
        //                   if(m==ColSubHead.Count)
        //                       m=0;
        //                   if(i==count)
        //                   {
        //                       i=0;
        //                       m=0;
        //                   }

        //               }
        //           }
        //       }

        //    }
        //    return ht_Ansitems;
        //}

        private static Hashtable GetHashedAnsOptions(Survey Sur)
        {
            Hashtable ht_Ansitems = new Hashtable();
            ValidationCore vcore = new ValidationCore();
            foreach (SurveyQuestion Ques in Sur.surveyQues)
            {
                if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20)
                {
                    foreach (SurveyAnswers Ans in Ques.surveyAnswers)
                    {
                        ht_Ansitems.Add(Ans.ANSWER_ID, vcore.clearHTML(Ans.ANSWER_OPTIONS).Replace("\r\n", ""));
                    }
                    //if(Ques.OTHER_ANS==1 && (Ques.QUESTION_TYPE==1 || Ques.QUESTION_TYPE==2 || Ques.QUESTION_TYPE==3 || Ques.QUESTION_TYPE==4))
                    //{


                    //}
                }
                else if (Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12 || Ques.QUESTION_TYPE == 13)
                {
                    Hashtable headerList = GetAnsItemsList(Ques);
                    ArrayList colHead = new ArrayList();
                    ArrayList ColSubHead = new ArrayList();
                    ArrayList rowHead = new ArrayList();
                    if (headerList.Contains("ColHead"))
                        colHead = (ArrayList)headerList["ColHead"];
                    if (headerList.Contains("ColSubHead"))
                        ColSubHead = (ArrayList)headerList["ColSubHead"];
                    if (headerList.Contains("RowHead"))
                        rowHead = (ArrayList)headerList["RowHead"];
                    if (Ques.QUESTION_TYPE == 10)
                    {
                        int col = 0, row=0;
                        foreach (SurveyAnswers Ans in Ques.surveyAnswers)
                        {
                            string optionValue = string.Format("{0}$--${1}", colHead[col], rowHead[row]);
                            var anserOption = Ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS == optionValue).FirstOrDefault();
                            ht_Ansitems.Add(anserOption.ANSWER_ID, vcore.clearHTML(colHead[col].ToString()).Replace("\r\n", ""));
                            row++;

                            if (row == rowHead.Count)
                            {                               
                                col++;
                                row = 0;                              
                            }                           
                            //i++;
                            //if (i == colHead.Count)
                            //    i = 0;
                        }
                    }
                    else if (Ques.QUESTION_TYPE == 11)
                    {
                        foreach (SurveyAnswers Ans in Ques.surveyAnswers)
                        {
                            ht_Ansitems.Add(Ans.ANSWER_ID, vcore.clearHTML(Ans.ANSWER_OPTIONS).Replace("\r\n", ""));

                        }

                    }
                    else if (Ques.QUESTION_TYPE == 12)
                    {
                        int i = 0, m = 0, index = 0, topHeader = 0, col=0,row=0;
                        int count = colHead.Count * ColSubHead.Count;
                        foreach (SurveyAnswers Ans in Ques.surveyAnswers)
                        {
                            string optionValue = string.Format("{0}$--${1}$--${2}", colHead[topHeader], ColSubHead[col], rowHead[row]);
                            var anserOption = Ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS == optionValue).FirstOrDefault();

                            ht_Ansitems.Add(anserOption.ANSWER_ID, vcore.clearHTML(ColSubHead[col].ToString()).Replace("\r\n", ""));
                            col++;
                            if (col == ColSubHead.Count)
                            {
                                row++;
                                col = 0;
                            }
                            if (row == rowHead.Count)
                            {
                                topHeader++;
                                col = 0;
                                row = 0;
                            }
                            //i++;
                            //m++;
                            //if (m == rowHead.Count)
                            //{
                            //    m = 0;
                            //    index++;
                            //}
                            //if (i == count)
                            //{
                            //    i = 0;
                            //    m = 0;
                            //    index=0;
                            //}

                        }
                    }
                    //else if (Ques.QUESTION_TYPE == 12)
                    //{
                    //    int i = 0, m = 0;
                    //    int count = colHead.Count * ColSubHead.Count;
                    //    foreach (SurveyAnswers Ans in Ques.surveyAnswers)
                    //    {
                    //        ht_Ansitems.Add(Ans.ANSWER_ID, vcore.clearHTML(ColSubHead[m].ToString()).Replace("\r\n", ""));
                    //        i++;
                    //        m++;
                    //        if (m == ColSubHead.Count)
                    //            m = 0;
                    //        if (i == count)
                    //        {
                    //            i = 0;
                    //            m = 0;
                    //        }

                    //    }
                    //}
                }

            }
            return ht_Ansitems;
        }

        //private static ArrayList GetHeaderRowForRawExport(Survey Sur)
        //{
        //    ValidationCore vcore=new ValidationCore();
        //    ArrayList HeaderRow=new ArrayList();
        //    HeaderRow.Add("SessionID");
        //    HeaderRow.Add("Email");
        //    HeaderRow.Add("Submit Time");
        //    HeaderRow.Add("Serial number");
        //    if (Sur != null && Sur.surveyQues != null && Sur.surveyQues.Count > 0)
        //    {
        //        Sur.surveyQues.Sort();
        //        int i = 1;
        //        foreach (SurveyQuestion Ques in Sur.surveyQues)
        //        {

        //            string str = "";
        //            if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 14 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20)
        //            {
        //                str = "Question " + i.ToString() + ": " + vcore.clearHTML(Ques.QUESTION_LABEL).Replace("\r\n","");
        //                HeaderRow.Add(str);
        //                if (Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE==15 || Ques.QUESTION_TYPE==20)
        //                {
        //                    foreach (SurveyAnswers Ans in Ques.surveyAnswers)
        //                    {
        //                        str = "Question " + i.ToString() + ": " + vcore.clearHTML(Ans.ANSWER_OPTIONS).Replace("\r\n", "");
        //                        HeaderRow.Add(str);
        //                    }

        //                }
        //                else if (Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12)
        //                {
        //                    ArrayList colHead = new ArrayList();
        //                    ArrayList ColSubHead = new ArrayList();
        //                    ArrayList rowHead = new ArrayList();
        //                    Hashtable headerList = GetAnsItemsList(Ques);
        //                    if (headerList.Contains("ColHead"))
        //                        colHead = (ArrayList)headerList["ColHead"];
        //                    if (headerList.Contains("ColSubHead"))
        //                        ColSubHead = (ArrayList)headerList["ColSubHead"];
        //                    if (headerList.Contains("RowHead"))
        //                        rowHead = (ArrayList)headerList["RowHead"];
        //                    if (Ques.QUESTION_TYPE == 10)
        //                    {
        //                        foreach (object obj in rowHead)
        //                        {
        //                            str = "Question " + i.ToString() + ": " + vcore.clearHTML(obj.ToString()).Replace("\r\n","");
        //                            HeaderRow.Add(str);
        //                        }
        //                    }
        //                    else if (Ques.QUESTION_TYPE == 11)
        //                    {
        //                        foreach (object obj in rowHead)
        //                        {
        //                            foreach (object objcol in colHead)
        //                            {
        //                                str = "Question " + i.ToString() + ": " + vcore.clearHTML(obj.ToString()).Replace("\r\n", "") + "-" + vcore.clearHTML(objcol.ToString()).Replace("\r\n", "");
        //                                HeaderRow.Add(str);
        //                            }
        //                        }
        //                    }
        //                    else if (Ques.QUESTION_TYPE == 12)
        //                    {
        //                        foreach (object obj in colHead)
        //                        {
        //                            foreach (object objrow in rowHead)
        //                            {
        //                                str = "Question " + i.ToString() + ": " + vcore.clearHTML(obj.ToString()).Replace("\r\n", "") + "-" + vcore.clearHTML(objrow.ToString()).Replace("\r\n", "");
        //                                HeaderRow.Add(str);
        //                            }
        //                        }
        //                    }
        //                }

        //                if (Ques.OTHER_ANS == 1)
        //                {
        //                    str = "Question " + i.ToString() + ": Other, please specify ";
        //                    HeaderRow.Add(str);

        //                }
        //                i++;
        //            }

        //        }
        //    }
        //    return HeaderRow;
        //}

        private static ArrayList GetHeaderRowForRawExport(Survey Sur)
        {
            ValidationCore vcore = new ValidationCore();
            ArrayList HeaderRow = new ArrayList();
           // HeaderRow.Add("SessionID");
            HeaderRow.Add("Email");
            HeaderRow.Add("Submit Time");
            HeaderRow.Add("Serial number");
            if (Sur != null && Sur.surveyQues != null && Sur.surveyQues.Count > 0)
            {
                Sur.surveyQues.Sort();
                int i = 1;
                foreach (SurveyQuestion Ques in Sur.surveyQues)
                {

                    string str = "";
                    //commented by krishna reddy
                    if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 5 || Ques.QUESTION_TYPE == 6 || Ques.QUESTION_TYPE == 7 || Ques.QUESTION_TYPE == 8 || Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 14 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20 || Ques.QUESTION_TYPE == 9)
                   // if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 5 || Ques.QUESTION_TYPE == 6 || Ques.QUESTION_TYPE == 7 || Ques.QUESTION_TYPE == 8 || Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 14 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20 || Ques.QUESTION_TYPE == 9)
                    {
                        str = "Question " + i.ToString() + ": " + vcore.clearHTML(Ques.QUESTION_LABEL).Replace("\r\n", "");
                        HeaderRow.Add(str);
                        if (Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20 || Ques.QUESTION_TYPE == 9)
                        {
                            foreach (SurveyAnswers Ans in Ques.surveyAnswers)
                            {
                                str = "Question " + i.ToString() + ": " + vcore.clearHTML(Ans.ANSWER_OPTIONS).Replace("\r\n", "");
                                HeaderRow.Add(str);
                            }

                        }
                            //commented by krishna reddy
                       // else if (Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12)
                        else if (Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12)
                        {
                            ArrayList colHead = new ArrayList();
                            ArrayList ColSubHead = new ArrayList();
                            ArrayList rowHead = new ArrayList();
                            Hashtable headerList = GetAnsItemsList(Ques);
                            if (headerList.Contains("ColHead"))
                                colHead = (ArrayList)headerList["ColHead"];
                            if (headerList.Contains("ColSubHead"))
                                ColSubHead = (ArrayList)headerList["ColSubHead"];
                            if (headerList.Contains("RowHead"))
                                rowHead = (ArrayList)headerList["RowHead"];
                            if (Ques.QUESTION_TYPE == 10)
                            {
                                foreach (object obj in rowHead)
                                {
                                    str = "Question " + i.ToString() + ": " + vcore.clearHTML(obj.ToString()).Replace("\r\n", "");
                                    HeaderRow.Add(str);
                                }
                            }
                            else if (Ques.QUESTION_TYPE == 11)
                            {
                                foreach (object obj in colHead)
                                {
                                    foreach (object objcol in rowHead)
                                    {
                                        str = "Question " + i.ToString() + ": " + vcore.clearHTML(obj.ToString()).Replace("\r\n", "") + "-" + vcore.clearHTML(objcol.ToString()).Replace("\r\n", "");
                                        HeaderRow.Add(str);
                                    }
                                }
                            }
                            else if (Ques.QUESTION_TYPE == 12)
                            {
                                foreach (object obj in colHead)
                                {
                                    foreach (object objrow in rowHead)
                                    {
                                        str = "Question " + i.ToString() + ": " + vcore.clearHTML(obj.ToString()).Replace("\r\n", "") + "-" + vcore.clearHTML(objrow.ToString()).Replace("\r\n", "");
                                        HeaderRow.Add(str);
                                    }
                                }
                            }
                        }

                        if (Ques.OTHER_ANS == 1)
                        {
                            str = "Question " + i.ToString() + ": Other, please specify ";
                            HeaderRow.Add(str);

                        }
                        i++;
                    }

                }
            }
            return HeaderRow;
        }
        //public static Hashtable GetAnsItemsList(SurveyQuestion ques)
        //{
        //    Hashtable hsTbl = new Hashtable();
        //    ArrayList colHead = new ArrayList();
        //    ArrayList ColSubHead = new ArrayList();
        //    ArrayList rowHead = new ArrayList();
        //    if (ques.QUESTION_TYPE == 12)
        //    {
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            string ansOpt = AnsOpts.ANSWER_OPTIONS;
        //            int pos = ansOpt.IndexOf("</p>");
        //            if (pos > 1)
        //            {
        //                colHead.Add(ansOpt.Substring(0, pos + 4));
        //                ansOpt = ansOpt.Substring(pos + 4);
        //                int AnsOptStrPos = ansOpt.IndexOf("<p>");
        //                int AnsOptEndPos = ansOpt.IndexOf("</p>");
        //                if (AnsOptStrPos > 0 && AnsOptEndPos > 4)
        //                {
        //                    rowHead.Add(ansOpt.Substring(AnsOptStrPos, AnsOptEndPos - AnsOptStrPos + 4));
        //                    ansOpt = ansOpt.Substring(AnsOptEndPos - AnsOptStrPos + 4);
        //                }
        //                int AnsExtOptStrPos = ansOpt.IndexOf("<p>");
        //                int AnsEXtOptEndPos = ansOpt.IndexOf("</p>");
        //                if (AnsExtOptStrPos > 0 && AnsEXtOptEndPos > 4)
        //                {
        //                    ColSubHead.Add(ansOpt.Substring(AnsExtOptStrPos, AnsEXtOptEndPos - AnsExtOptStrPos + 4));
        //                }
        //            }
        //        }
        //    }
        //    else if (ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11)
        //    {
        //        foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
        //        {
        //            string ansOpt = AnsOpts.ANSWER_OPTIONS;
        //            int pos = ansOpt.IndexOf("</p>");
        //            if (pos > 1)
        //            {
        //                rowHead.Add(ansOpt.Substring(0, pos + 4));
        //                ansOpt = ansOpt.Substring(pos + 4);
        //                int AnsOptStrPos = ansOpt.IndexOf("<p>");
        //                int AnsOptEndPos = ansOpt.IndexOf("</p>");
        //                if (AnsOptStrPos > 0 && AnsOptEndPos > 4)
        //                {
        //                    colHead.Add(ansOpt.Substring(AnsOptStrPos, AnsOptEndPos - AnsOptStrPos + 4));
        //                }
        //            }
        //        }
        //    }
        //    if (colHead.Count > 0)
        //    {
        //        colHead = RemoveDuplicateItems(colHead);
        //        hsTbl.Add("ColHead", colHead);
        //    }
        //    if (ColSubHead.Count > 0)
        //    {
        //        ColSubHead = RemoveDuplicateItems(ColSubHead);
        //        hsTbl.Add("ColSubHead", ColSubHead);
        //    }
        //    if (rowHead.Count > 0)
        //    {
        //        rowHead = RemoveDuplicateItems(rowHead);
        //        hsTbl.Add("RowHead", rowHead);
        //    }
        //    return hsTbl;
        //}



        public static Hashtable GetAnsItemsList(SurveyQuestion ques)
        {
            Hashtable hsTbl = new Hashtable();
            ArrayList colHead = new ArrayList();
            ArrayList ColSubHead = new ArrayList();
            ArrayList rowHead = new ArrayList();
            if (ques.QUESTION_TYPE == 12)
            {
                foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
                {
                    string ansOpt = AnsOpts.ANSWER_OPTIONS;
                    //int pos = ansOpt.IndexOf("</p>");
                    int pos = ansOpt.IndexOf("$--$");
                    //if (pos > 1)
                    //{
                        //colHead.Add(ansOpt.Substring(0, pos + 4));
                        colHead.Add(ansOpt.Substring(0, pos));
                        ansOpt = ansOpt.Substring(pos + 4);
                        //int AnsOptStrPos = ansOpt.IndexOf("<p>");
                        //int AnsOptEndPos = ansOpt.IndexOf("</p>");
                        int AnsOptEndPos = ansOpt.IndexOf("$--$");
                        //if (AnsOptStrPos > 0 && AnsOptEndPos > 4)
                        if (AnsOptEndPos >= 0)
                        {
                            //rowHead.Add(ansOpt.Substring(AnsOptStrPos, AnsOptEndPos - AnsOptStrPos + 4));
                            //ansOpt = ansOpt.Substring(AnsOptEndPos - AnsOptStrPos + 4);
                            ColSubHead.Add(ansOpt.Substring(0, AnsOptEndPos).Trim());
                            ansOpt = ansOpt.Substring(AnsOptEndPos + 4);
                        }
                        //int AnsExtOptStrPos = ansOpt.IndexOf("<p>");
                        //int AnsEXtOptEndPos = ansOpt.IndexOf("</p>");
                        //if (AnsExtOptStrPos > 0 && AnsEXtOptEndPos > 4)
                        if (ansOpt.Length > 0)
                        {
                            //ColSubHead.Add(ansOpt.Substring(AnsExtOptStrPos, AnsEXtOptEndPos - AnsExtOptStrPos + 4));
                            rowHead.Add(ansOpt.Trim());
                        }
                   // }
                }
            }
            else if (ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11)
            {
                foreach (SurveyAnswers AnsOpts in ques.surveyAnswers)
                {
                    string ansOpt = AnsOpts.ANSWER_OPTIONS;
                    //int pos = ansOpt.IndexOf("</p>");
                    int pos = ansOpt.IndexOf("$--$");
                    //if (pos > 1)
                    //{
                        //rowHead.Add(ansOpt.Substring(0, pos + 4));
                        colHead.Add(ansOpt.Substring(0, pos));
                        ansOpt = ansOpt.Substring(pos + 4);
                        //int AnsOptStrPos = ansOpt.IndexOf("<p>");
                        //int AnsOptEndPos = ansOpt.IndexOf("</p>");
                        //if (AnsOptStrPos > 0 && AnsOptEndPos > 4)
                        if (ansOpt.Length > 0)
                        {
                            //colHead.Add(ansOpt.Substring(AnsOptStrPos, AnsOptEndPos - AnsOptStrPos + 4));
                            rowHead.Add(ansOpt.Trim());
                        }
                   // }
                }
            }
            if (colHead.Count > 0)
            {
                colHead = RemoveDuplicateItems(colHead);
                hsTbl.Add("ColHead", colHead);
            }
            if (ColSubHead.Count > 0)
            {
                ColSubHead = RemoveDuplicateItems(ColSubHead);
                hsTbl.Add("ColSubHead", ColSubHead);
            }
            if (rowHead.Count > 0)
            {
                rowHead = RemoveDuplicateItems(rowHead);
                hsTbl.Add("RowHead", rowHead);
            }
            return hsTbl;
        }
        //private static ArrayList GetRespondentAnswers(Survey Sur, SurveyResponse Response,Hashtable ht_Ansitems,int bias,int seq)
        //{
        //    ArrayList HeaderRow = new ArrayList();
        //    //HeaderRow.Add(0);
        //    HeaderRow.Add(Response.RANDOM_SESSIONID);
        //    HeaderRow.Add(Response.EMAIL_ADDRESS);
        //    DateTime dt = GetConvertedDatetime(Response.CREATED_ON,bias,"", true);
        //    string Date_str = Convert.ToDateTime(dt).ToString("dd-MMM-yyyy") + " at " + dt.ToShortTimeString();
        //    HeaderRow.Add(Date_str);
        //    HeaderRow.Add(seq);
        //    List<ResponseQuestion> RespQues = Response.ResponseQuestion;
        //    if (RespQues!= null && RespQues.Count > 0)
        //    {
        //        foreach (SurveyQuestion Ques in Sur.surveyQues)
        //        {
        //            int i = 1;
        //            string str = "";
        //            if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 14 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20)
        //            {
        //                //str = "Question: "+i.ToString()+Ques.QUESTION_LABEL;
        //                //HeaderRow.Add(str);
        //                if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4)
        //                {
        //                    bool test1 = false;
        //                    bool check = false;
        //                    for (int j = 0; j < RespQues.Count; j++)
        //                    {
        //                        if (Ques.QUESTION_ID == RespQues[j].QUESTION_ID)
        //                        {
        //                            if (RespQues[j].ANSWER_ID > 0)
        //                            {
        //                                HeaderRow.Add(ht_Ansitems[RespQues[j].ANSWER_ID]);
        //                                check = true;
        //                                if (Ques.OTHER_ANS == 0)
        //                                    break;
        //                            }
        //                            else
        //                            {
        //                                HeaderRow.Add("");
        //                                check = true;
        //                                HeaderRow.Add(RespQues[j].ANSWER_TEXT);
        //                                test1 = true;
        //                            }
        //                        }
        //                    }
        //                    if(!check)
        //                        HeaderRow.Add("");
        //                    if (Ques.OTHER_ANS == 1 && test1 == false)
        //                        HeaderRow.Add("");
        //                }
        //                else if (Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 11)
        //                {
        //                    HeaderRow.Add("");
        //                    bool test1 = false;
        //                    foreach (SurveyAnswers Ans in Ques.surveyAnswers)
        //                    {
        //                        bool test = false;
        //                        for (int j = 0; j < RespQues.Count; j++)
        //                        {
        //                            if (Ques.QUESTION_ID == RespQues[j].QUESTION_ID && RespQues[j].ANSWER_ID > 0 && Ans.ANSWER_ID == RespQues[j].ANSWER_ID)
        //                            {
        //                                HeaderRow.Add("Yes");
        //                                test = true;
        //                                break;
        //                            }
        //                            else if (Ques.QUESTION_TYPE==2 && Ques.QUESTION_ID == RespQues[j].QUESTION_ID && RespQues[j].ANSWER_TEXT!=null && RespQues[j].ANSWER_TEXT.Length>0)
        //                            {
        //                                HeaderRow.Add(RespQues[j].ANSWER_TEXT);
        //                                test1 = true;

        //                            }
        //                        }
        //                        if (!test)
        //                            HeaderRow.Add("No");
        //                    }
        //                    if (Ques.QUESTION_TYPE==2 && Ques.OTHER_ANS == 1 && test1 == false)
        //                        HeaderRow.Add("");
        //                }
        //                else if (Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20)
        //                {
        //                    HeaderRow.Add("");
        //                    foreach (SurveyAnswers Ans in Ques.surveyAnswers)
        //                    {
        //                        bool test = false;
        //                        for (int j = 0; j < RespQues.Count; j++)
        //                        {
        //                            if (Ques.QUESTION_ID == RespQues[j].QUESTION_ID && RespQues[j].ANSWER_ID > 0 && Ans.ANSWER_ID == RespQues[j].ANSWER_ID)
        //                            {
        //                                HeaderRow.Add(RespQues[j].ANSWER_TEXT);
        //                                test = true;
        //                                break;
        //                            }
        //                        }
        //                        if (!test)
        //                            HeaderRow.Add("");
        //                    }

        //                }
        //                else if (Ques.QUESTION_TYPE == 14)
        //                {
        //                    bool test = false;
        //                    for (int j = 0; j < RespQues.Count; j++)
        //                    {
        //                        if (Ques.QUESTION_ID == RespQues[j].QUESTION_ID)
        //                        {
        //                            HeaderRow.Add(RespQues[j].ANSWER_TEXT);
        //                            test = true;
        //                        }
        //                    }
        //                    if (!test)
        //                        HeaderRow.Add("");
        //                }
        //                else if (Ques.QUESTION_TYPE == 10)
        //                {
        //                    HeaderRow.Add("");
        //                    Hashtable headerList = GetAnsItemsList(Ques);
        //                    ArrayList rowHead = new ArrayList();
        //                    if (headerList.Contains("RowHead"))
        //                        rowHead = (ArrayList)headerList["RowHead"];
        //                    Hashtable Ansids = GetAnsIds(Ques);
        //                    int y = 1;
        //                    foreach (object objRow in rowHead)
        //                    {
        //                        string temp = "";
        //                        temp = (string)Ansids[y];
        //                        string[] arr = temp.Split('_');
        //                        bool test = false;
        //                        for (int l = 0; l < arr.Length; l++)
        //                        {
        //                            for (int j = 0; j < RespQues.Count; j++)
        //                            {
        //                                if (Convert.ToInt32(arr[l]) == RespQues[j].ANSWER_ID)
        //                                {
        //                                    HeaderRow.Add(ht_Ansitems[RespQues[j].ANSWER_ID]);
        //                                    test = true;
        //                                }
        //                            }
        //                        }
        //                        if (!test)
        //                            HeaderRow.Add("");
        //                        y++;
        //                    }
        //                }
        //                else if (Ques.QUESTION_TYPE == 12)
        //                {
        //                    HeaderRow.Add("");
        //                    Hashtable headerList = GetAnsItemsList(Ques);
        //                    ArrayList rowHead = new ArrayList();
        //                    ArrayList colHead = new ArrayList();
        //                    if (headerList.Contains("RowHead"))
        //                        rowHead = (ArrayList)headerList["RowHead"];
        //                    if (headerList.Contains("RowHead"))
        //                        colHead = (ArrayList)headerList["ColHead"];
        //                    Hashtable Ansids = GetAnsIds(Ques);
        //                    int count = rowHead.Count * colHead.Count;
        //                    int y = 1;
        //                    //foreach (object objRow in rowHead)
        //                    for (int z = 0; z < count; z++)
        //                    {
        //                        string temp = "";
        //                        temp = (string)Ansids[y];
        //                        string[] arr = temp.Split('_');
        //                        bool test = false;
        //                        for (int l = 0; l < arr.Length;l++)
        //                        {
        //                            for (int j = 0; j < RespQues.Count; j++)
        //                            {
        //                                if (Convert.ToInt32(arr[l]) == RespQues[j].ANSWER_ID)
        //                                {
        //                                    HeaderRow.Add(ht_Ansitems[RespQues[j].ANSWER_ID]);
        //                                    test = true;
        //                                }
        //                            }
        //                        }
        //                        if (!test)
        //                            HeaderRow.Add("");
        //                        y++;
        //                    }
        //                }

        //                //if (Ques.OTHER_ANS == 1)
        //                //{
        //                //    str = "Question " + i.ToString() + ": Other, please specify ";

        //                //}
        //            }
        //        }
        //    }
        //    return HeaderRow;
        //}


        private static ArrayList GetRespondentAnswers(Survey Sur, SurveyResponse Response, Hashtable ht_Ansitems, int bias, int seq)
        {
            ArrayList HeaderRow = new ArrayList();
            //HeaderRow.Add(0);
           // HeaderRow.Add(Response.RANDOM_SESSIONID);
            HeaderRow.Add(Response.EMAIL_ADDRESS);
            DateTime dt = GetConvertedDatetime(Response.CREATED_ON, bias, "", true);
            string Date_str = Convert.ToDateTime(dt).ToString("dd-MMM-yyyy") + " at " + dt.ToShortTimeString();
            HeaderRow.Add(Date_str);
            HeaderRow.Add(seq);
            List<ResponseQuestion> RespQues = Response.ResponseQuestion;
            if (RespQues != null && RespQues.Count > 0)
            {
                foreach (SurveyQuestion Ques in Sur.surveyQues)
                {
                    int i = 1;
                    string str = "";
                    if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 5 || Ques.QUESTION_TYPE == 6 || Ques.QUESTION_TYPE == 7 || Ques.QUESTION_TYPE == 8 || Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 14 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20 || Ques.QUESTION_TYPE == 9)
                    {
                        //str = "Question: "+i.ToString()+Ques.QUESTION_LABEL;
                        //HeaderRow.Add(str);
                        if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4)
                        {
                            bool test1 = false;
                            bool check = false;
                            bool emptyAns = true;
                            for (int j = 0; j < RespQues.Count; j++)
                            {
                                if (Ques.QUESTION_ID == RespQues[j].QUESTION_ID)
                                {
                                    if (RespQues[j].ANSWER_ID > 0)
                                    {
                                        HeaderRow.Add(ht_Ansitems[RespQues[j].ANSWER_ID]);
                                        check = true;
                                        emptyAns = false;
                                        if (Ques.OTHER_ANS == 0)
                                            break;
                                    }
                                    else
                                    {
                                        if(emptyAns)
                                            HeaderRow.Add("");
                                        check = true;
                                        HeaderRow.Add(RespQues[j].ANSWER_TEXT);
                                        test1 = true;
                                    }
                                }
                            }
                            if (!check)
                                HeaderRow.Add("");
                            if (Ques.OTHER_ANS == 1 && test1 == false)
                                HeaderRow.Add("");
                        }
                        else if (Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 11)
                        {
                            HeaderRow.Add("");
                            bool test1 = false;
                            foreach (SurveyAnswers Ans in Ques.surveyAnswers)
                            {
                                bool test = false;
                                for (int j = 0; j < RespQues.Count; j++)
                                {
                                    if (Ques.QUESTION_ID == RespQues[j].QUESTION_ID && RespQues[j].ANSWER_ID > 0 && Ans.ANSWER_ID == RespQues[j].ANSWER_ID)
                                    {
                                        HeaderRow.Add("Yes");
                                        test = true;
                                        break;
                                    }
                                    //else if (Ques.QUESTION_TYPE == 2 && Ques.QUESTION_ID == RespQues[j].QUESTION_ID && RespQues[j].ANSWER_TEXT != null && RespQues[j].ANSWER_TEXT.Length > 0)
                                    //{
                                    //    HeaderRow.Add(RespQues[j].ANSWER_TEXT);
                                    //    test1 = true;

                                    //}
                                }
                                if (!test)
                                    HeaderRow.Add("No");
                            }
                            if (Ques.QUESTION_TYPE == 2 && Ques.OTHER_ANS == 1 && test1 == false)
                            {
                                for (int j = 0; j < RespQues.Count; j++)
                                {
                                    if (Ques.QUESTION_TYPE == 2 && Ques.QUESTION_ID == RespQues[j].QUESTION_ID && RespQues[j].ANSWER_TEXT != null && RespQues[j].ANSWER_TEXT.Length > 0 && RespQues[j].ANSWER_ID == 0)
                                    {
                                        HeaderRow.Add(RespQues[j].ANSWER_TEXT);
                                        test1 = true;

                                    }

                                }
                                if (!test1)
                                    HeaderRow.Add("");
                            }
                            
                            //if (Ques.QUESTION_TYPE == 2 && Ques.OTHER_ANS == 1 && test1 == false)
                            //    HeaderRow.Add("");
                        }
                        else if (Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20 || Ques.QUESTION_TYPE == 9)
                        {
                            HeaderRow.Add("");
                            foreach (SurveyAnswers Ans in Ques.surveyAnswers)
                            {
                                bool test = false;
                                for (int j = 0; j < RespQues.Count; j++)
                                {
                                    if (Ques.QUESTION_ID == RespQues[j].QUESTION_ID && RespQues[j].ANSWER_ID > 0 && Ans.ANSWER_ID == RespQues[j].ANSWER_ID)
                                    {
                                        HeaderRow.Add(RespQues[j].ANSWER_TEXT);
                                        test = true;
                                        break;
                                    }
                                }
                                if (!test)
                                    HeaderRow.Add("");
                            }

                        }
                        else if (Ques.QUESTION_TYPE == 14 || Ques.QUESTION_TYPE == 5 || Ques.QUESTION_TYPE == 6 || Ques.QUESTION_TYPE == 7 || Ques.QUESTION_TYPE == 8)
                        {
                            bool test = false;
                            for (int j = 0; j < RespQues.Count; j++)
                            {
                                if (Ques.QUESTION_ID == RespQues[j].QUESTION_ID)
                                {
                                    HeaderRow.Add(RespQues[j].ANSWER_TEXT);
                                    test = true;
                                }
                            }
                            if (!test)
                                HeaderRow.Add("");
                        }
                        else if (Ques.QUESTION_TYPE == 10)
                        {
                            HeaderRow.Add("");
                            Hashtable headerList = GetAnsItemsList(Ques);
                            ArrayList rowHead = new ArrayList();
                            if (headerList.Contains("RowHead"))
                                rowHead = (ArrayList)headerList["RowHead"];
                            Hashtable Ansids = GetAnsIds(Ques);
                            int y = 1;
                            foreach (object objRow in rowHead)
                            {
                                string temp = "";
                                temp = (string)Ansids[y];
                                string[] arr = temp.Split('_');
                                bool test = false;
                                for (int l = 0; l < arr.Length; l++)
                                {
                                    for (int j = 0; j < RespQues.Count; j++)
                                    {
                                        if (Convert.ToInt32(arr[l]) == RespQues[j].ANSWER_ID)
                                        {
                                            HeaderRow.Add(ht_Ansitems[RespQues[j].ANSWER_ID]);
                                            test = true;
                                        }
                                    }
                                }
                                if (!test)
                                    HeaderRow.Add("");
                                y++;
                            }
                        }
                        else if (Ques.QUESTION_TYPE == 12)
                        {
                            try
                            {                              
                                HeaderRow.Add("");
                                Hashtable headerList = GetAnsItemsList(Ques);
                                ArrayList rowHead = new ArrayList();
                                ArrayList colHead = new ArrayList();
                                if (headerList.Contains("RowHead"))
                                    rowHead = (ArrayList)headerList["RowHead"];
                                if (headerList.Contains("ColHead"))
                                    colHead = (ArrayList)headerList["ColHead"];
                                Hashtable Ansids = GetAnsIds(Ques);
                                int count = rowHead.Count * colHead.Count;
                                int y = 1;
                                //foreach (object objRow in rowHead)
                                for (int z = 0; z < count; z++)
                                {
                                    string temp = "";
                                    temp = (string)Ansids[y];
                                    string[] arr = temp.Split('_');
                                    bool test = false;
                                    for (int l = 0; l < arr.Length; l++)
                                    {
                                        for (int j = 0; j < RespQues.Count; j++)
                                        {
                                            if (Convert.ToInt32(arr[l]) == RespQues[j].ANSWER_ID)
                                            {
                                                HeaderRow.Add(ht_Ansitems[RespQues[j].ANSWER_ID]);
                                                test = true;
                                            }
                                        }
                                    }
                                    if (!test)
                                        HeaderRow.Add("");
                                    y++;                                  
                                }
                            }

                            //if (Ques.OTHER_ANS == 1)
                            //{
                            //    str = "Question " + i.ToString() + ": Other, please specify ";

                            //}

                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }
                }
            }
            return HeaderRow;
        }

        //public static ArrayList GetBordercounts(Survey Sur)
        //{

        //    ArrayList borders = new ArrayList();
        //    if (Sur != null && Sur.surveyQues != null && Sur.surveyQues.Count > 0)
        //    {
        //        List<SurveyQuestion> Queslist = Sur.surveyQues;
        //        Queslist.Sort();
        //        Sur.surveyQues.Sort();
        //        int i = 1;
        //        borders.Add(1);
        //        borders.Add(1);
        //        borders.Add(1);
        //        borders.Add(1);
        //        foreach (SurveyQuestion Ques in Queslist)
        //        {

        //            string str = "";
        //            if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 14 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20)
        //            {
        //                int count=1;
        //                if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 14)
        //                {

        //                    if(Ques.OTHER_ANS==1)
        //                        count++;

        //                    borders.Add(count);

        //                }
        //                else if (Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 15)
        //                {

        //                    count = count+Ques.surveyAnswers.Count;
        //                    if (Ques.QUESTION_TYPE == 2 && Ques.OTHER_ANS == 1)
        //                        count++;
        //                    borders.Add(count);
        //                }

        //                else if (Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE==12)
        //                {

        //                    ArrayList rowHead = new ArrayList();
        //                    Hashtable headerList = GetAnsItemsList(Ques);
        //                    if (headerList.Contains("RowHead"))
        //                        rowHead = (ArrayList)headerList["RowHead"];
        //                    if (Ques.QUESTION_TYPE == 10)
        //                    {
        //                        count = count + rowHead.Count;
        //                    }
        //                    else if (Ques.QUESTION_TYPE == 12)
        //                    {
        //                        ArrayList colHead = new ArrayList();
        //                        if (headerList.Contains("ColHead"))
        //                            colHead = (ArrayList)headerList["ColHead"];

        //                        count = count + (rowHead.Count*colHead.Count);
        //                    }
        //                    borders.Add(count);
        //                }

        //            }


        //        }


        //    }
        //    return borders;

        //}

        public static ArrayList GetBordercounts(Survey Sur)
        {

            ArrayList borders = new ArrayList();
            if (Sur != null && Sur.surveyQues != null && Sur.surveyQues.Count > 0)
            {
                List<SurveyQuestion> Queslist = Sur.surveyQues;
                Queslist.Sort();
                Sur.surveyQues.Sort();
                int i = 1;
                borders.Add(1);
                borders.Add(1);
                borders.Add(1);
                borders.Add(1);
                foreach (SurveyQuestion Ques in Queslist)
                {

                    string str = "";
                    if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 14 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20 || Ques.QUESTION_TYPE == 5 || Ques.QUESTION_TYPE == 6 || Ques.QUESTION_TYPE == 7 || Ques.QUESTION_TYPE == 8 || Ques.QUESTION_TYPE == 9)
                    {
                        int count = 1;
                        if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 14 || Ques.QUESTION_TYPE == 5 || Ques.QUESTION_TYPE == 6 || Ques.QUESTION_TYPE == 7 || Ques.QUESTION_TYPE == 8)
                        {

                            if (Ques.OTHER_ANS == 1)
                                count++;

                            borders.Add(count);

                        }
                        else if (Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 15 || Ques.QUESTION_TYPE == 20 || Ques.QUESTION_TYPE == 9)
                        {

                            count = count + Ques.surveyAnswers.Count;
                            if (Ques.QUESTION_TYPE == 2 && Ques.OTHER_ANS == 1)
                                count++;
                            borders.Add(count);
                        }

                        else if (Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 12)
                        {

                            ArrayList rowHead = new ArrayList();
                            Hashtable headerList = GetAnsItemsList(Ques);
                            if (headerList.Contains("RowHead"))
                                rowHead = (ArrayList)headerList["RowHead"];
                            if (Ques.QUESTION_TYPE == 10)
                            {
                                count = count + rowHead.Count;
                            }
                            else if (Ques.QUESTION_TYPE == 12)
                            {
                                ArrayList colHead = new ArrayList();
                                if (headerList.Contains("ColHead"))
                                    colHead = (ArrayList)headerList["ColHead"];

                                count = count + (rowHead.Count * colHead.Count);
                            }
                            borders.Add(count);
                        }

                    }


                }


            }
            return borders;

        }



    }
}
