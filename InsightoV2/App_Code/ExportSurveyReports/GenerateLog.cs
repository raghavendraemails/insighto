﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
namespace InsightoV2.ExportSurveyReports
{
    public class GenerateLog
    {
        private static Object locker = new Object();


        public static void GenrateSendmailLog(string Content, string path)
        {
            lock (locker)
            {
                StreamWriter sw = null;
                try
                {
                    if (File.Exists(path))
                    {
                        FileStream file = new FileStream(path, FileMode.Append, FileAccess.Write);
                        sw = new StreamWriter(file);
                    }
                    else
                    {
                        sw = File.CreateText(path);
                    }
                    sw.WriteLine(DateTime.Now.ToString() + " ***** " + Content.Trim());
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (sw != null) sw.Close();
                }
            }

        }
    }
}
