﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.XtraCharts.Web;
using DevExpress.XtraCharts;


namespace InsightoV2.ExportSurveyReports
{
    public static class ChartExtraFeatures
    {
        //public ChartExtraFeatures()
        //{
        //    //
        //    // TODO: Add constructor logic here
        //    //
        //}
        public static void SetchartDiagramProp(WebChartControl wcCtrl, SurveyQuestion SurQues)
        {
            if (SurQues != null)
            {
                string ChartType = SurQues.CHART_TYPE;
                if (ChartType == "")
                {
                    ChartType = GetChartViewType("", SurQues.QUESTION_TYPE);
                    //ViewType viewType = GetChartViewType(ChartViewType, SurQues.QUESTION_TYPE);

                }
                if (ChartType == ViewType.Bar3D.ToString() || ChartType == ViewType.FullStackedBar3D.ToString() || ChartType == ViewType.StackedBar3D.ToString() || ChartType == ViewType.Line3D.ToString() || ChartType == ViewType.StepLine3D.ToString() || ChartType == ViewType.Spline3D.ToString() || ChartType == ViewType.Area3D.ToString() || ChartType == ViewType.StackedArea3D.ToString() || ChartType == ViewType.FullStackedArea3D.ToString() || ChartType == ViewType.SplineArea3D.ToString() || ChartType == ViewType.StackedSplineArea3D.ToString() || ChartType == ViewType.FullStackedSplineArea3D.ToString())
                {
                    if (wcCtrl.Diagram != null)
                    {
                        if (SurQues.ZOOMPERCENT >= 0)
                            ((Diagram3D)wcCtrl.Diagram).ZoomPercent = SurQues.ZOOMPERCENT;
                        if (SurQues.PERSPECTIVE_ANGLE >= 0)
                            ((Diagram3D)wcCtrl.Diagram).PerspectiveAngle = SurQues.PERSPECTIVE_ANGLE;
                        //((Diagram3D)wcCtrl.Diagram).PerspectiveAngle = 0;
                    }
                }

                if (ChartType == ViewType.PolarArea.ToString() || ChartType == ViewType.PolarLine.ToString() || ChartType == ViewType.PolarPoint.ToString() || ChartType == ViewType.RadarArea.ToString() || ChartType == ViewType.RadarLine.ToString() || ChartType == ViewType.RadarPoint.ToString())
                {
                    if (wcCtrl.Diagram != null && SurQues.DIAGRAM_TYPE.Trim().Length > 0)
                        ((RadarDiagram)wcCtrl.Diagram).DrawingStyle = (RadarDiagramDrawingStyle)((Enum)Enum.Parse(typeof(RadarDiagramDrawingStyle), SurQues.DIAGRAM_TYPE));
                    if (ChartType == ViewType.RadarArea.ToString() || ChartType == ViewType.RadarLine.ToString() || ChartType == ViewType.RadarPoint.ToString())
                    {
                        RadarAxisXLabelTextDirection textDirection = RadarAxisXLabelTextDirection.LeftToRight;
                        if (SurQues.TEXT_DIRECTION.Trim().Length > 0)
                        {
                            textDirection = (RadarAxisXLabelTextDirection)((Enum)Enum.Parse(typeof(RadarAxisXLabelTextDirection), SurQues.TEXT_DIRECTION));
                        }
                        if (wcCtrl.Diagram != null)
                        {
                            if (textDirection == RadarAxisXLabelTextDirection.Radial || textDirection == RadarAxisXLabelTextDirection.Tangent)
                                ((RadarDiagram)wcCtrl.Diagram).AxisX.Label.Antialiasing = true;
                            else
                                ((RadarDiagram)wcCtrl.Diagram).AxisX.Label.Antialiasing = false;
                            ((RadarDiagram)wcCtrl.Diagram).AxisX.Label.TextDirection = textDirection;
                        }
                    }
                    else if (ChartType == ViewType.PolarArea.ToString() || ChartType == ViewType.PolarLine.ToString() || ChartType == ViewType.PolarPoint.ToString())
                    {
                        wcCtrl.Series[0].Points.Clear();
                        wcCtrl.Series[0].Points.AddRange(GenerateFunctionPoints(SurQues.FUNCTION_TYPE, SurQues.surveyAnswers.Count));
                    }
                }
                else if (ChartType == ViewType.Area.ToString() || ChartType == ViewType.Bar.ToString() || ChartType == ViewType.Line.ToString() || ChartType == ViewType.Point.ToString() || ChartType == ViewType.Spline.ToString() || ChartType == ViewType.SplineArea.ToString() || ChartType == ViewType.StackedArea.ToString() || ChartType == ViewType.StackedBar.ToString() || ChartType == ViewType.StackedSplineArea.ToString() || ChartType == ViewType.StepLine.ToString())
                {
                    if (wcCtrl.Diagram != null && wcCtrl.Diagram.ToString() == "(XYDiagram)")
                    {
                        //((XYDiagram)wcCtrl.Diagram).Rotated = true;
                        ((XYDiagram)wcCtrl.Diagram).AxisX.Label.Visible = Convert.ToBoolean(SurQues.HIDE_XAXIS);
                        ((XYDiagram)wcCtrl.Diagram).AxisX.Label.Staggered = Convert.ToBoolean(SurQues.STAGGERED);
                        //((XYDiagram)wcCtrl.Diagram).AxisX.GridSpacingAuto = true;
                        ((XYDiagram)wcCtrl.Diagram).AxisX.Label.Angle = -30;
                        // ((XYDiagram)wcCtrl.Diagram).AxisX.Label.Angle = -25;
                        //((XYDiagram)wcCtrl.Diagram).AxisX.GridSpacing = 4;
                        //axisX.Tickmarks.MinorVisible = true;
                        //axisX.Label.Angle = -45;
                        //axisX.Label.Antialiasing = true;

                        ((XYDiagram)wcCtrl.Diagram).AxisX.Label.Antialiasing = true;
                    }
                }
                else if (ChartType == ViewType.Area3D.ToString() || ChartType == ViewType.Bar3D.ToString() || ChartType == ViewType.Line3D.ToString() || ChartType == ViewType.ManhattanBar.ToString() || ChartType == ViewType.Spline3D.ToString() || ChartType == ViewType.SplineArea3D.ToString() || ChartType == ViewType.StackedSplineArea3D.ToString() || ChartType == ViewType.StepLine3D.ToString() || ChartType == ViewType.StackedArea3D.ToString())
                {
                    if (wcCtrl.Diagram != null && wcCtrl.Diagram.ToString() == "(XYDiagram3D)")
                    {
                        ((XYDiagram3D)wcCtrl.Diagram).AxisX.Label.Visible = Convert.ToBoolean(SurQues.HIDE_XAXIS);
                        ((XYDiagram3D)wcCtrl.Diagram).AxisX.Label.Staggered = Convert.ToBoolean(SurQues.STAGGERED);
                        ((XYDiagram3D)wcCtrl.Diagram).AxisX.Label.Angle = -30;
                        ((XYDiagram3D)wcCtrl.Diagram).AxisX.Label.Antialiasing = true;
                        //((XYDiagram3D)wcCtrl.Diagram).Rotated = true;


                    }
                }
            }
        }
        public static string GetChartViewType(string VieTyp, int ques_type)
        {
            string viewType = "";
            if (ques_type > 0)
            {
                if (ques_type == 1)
                {
                    viewType = "Bar";
                }
                else if (ques_type == 2)
                {
                    viewType = "Bar3D";
                }
                else if (ques_type == 3)
                {
                    viewType = "Doughnut";
                }
                else if (ques_type == 4)
                {
                    viewType = "Doughnut3D";
                }
                else if (ques_type == 10 || ques_type == 11 || ques_type == 12)
                {
                    viewType = "StackedBar";
                }
                else if (ques_type == 15)
                {
                    viewType = "StackedArea";
                }

            }
            return viewType;
            //ViewType viewType = ViewType.Pie;
            //switch (VieTyp)
            //{
            //    case "Area":
            //        viewType = ViewType.Area;
            //        break;
            //    case "Area3D":
            //        viewType = ViewType.Area3D;
            //        break;
            //    case "Bar":
            //        viewType = ViewType.Bar;
            //        break;
            //    case "Bar3D":
            //        viewType = ViewType.Bar3D;
            //        break;
            //    case "CandleStick":
            //        viewType = ViewType.CandleStick;
            //        break;
            //    case "Doughnut":
            //        viewType = ViewType.Doughnut;
            //        break;
            //    case "Doughnut3D":
            //        viewType = ViewType.Doughnut3D;
            //        break;
            //    case "FullStackedArea":
            //        viewType = ViewType.FullStackedArea;
            //        break;
            //    case "FullStackedArea3D":
            //        viewType = ViewType.FullStackedArea3D;
            //        break;
            //    case "FullStackedBar":
            //        viewType = ViewType.FullStackedBar;
            //        break;
            //    case "FullStackedBar3D":
            //        viewType = ViewType.FullStackedBar3D;
            //        break;
            //    case "FullStackedSplineArea":
            //        viewType = ViewType.FullStackedSplineArea;
            //        break;
            //    case "FullStackedSplineArea3D":
            //        viewType = ViewType.FullStackedSplineArea3D;
            //        break;
            //    case "Gantt":
            //        viewType = ViewType.Gantt;
            //        break;
            //    case "Line":
            //        viewType = ViewType.Line;
            //        break;
            //    case "Line3D":
            //        viewType = ViewType.Line3D;
            //        break;
            //    case "ManhattanBar":
            //        viewType = ViewType.ManhattanBar;
            //        break;
            //    case "Pie":
            //        viewType = ViewType.Pie;
            //        break;
            //    case "Pie3D":
            //        viewType = ViewType.Pie3D;
            //        break;
            //    case "Point":
            //        viewType = ViewType.Point;
            //        break;
            //    case "PolarArea":
            //        viewType = ViewType.PolarArea;
            //        break;
            //    case "PolarLine":
            //        viewType = ViewType.PolarLine;
            //        break;
            //    case "PolarPoint":
            //        viewType = ViewType.PolarPoint;
            //        break;
            //    case "RadarArea":
            //        viewType = ViewType.RadarArea;
            //        break;
            //    case "RadarLine":
            //        viewType = ViewType.RadarLine;
            //        break;
            //    case "RadarPoint":
            //        viewType = ViewType.RadarPoint;
            //        break;
            //    case "RangeBar":
            //        viewType = ViewType.RangeBar;
            //        break;
            //    case "SideBySideGantt":
            //        viewType = ViewType.SideBySideGantt;
            //        break;
            //    case "SideBySideRangeBar":
            //        viewType = ViewType.SideBySideRangeBar;
            //        break;
            //    case "Spline":
            //        viewType = ViewType.Spline;
            //        break;
            //    case "Spline3D":
            //        viewType = ViewType.Spline3D;
            //        break;
            //    case "SplineArea":
            //        viewType = ViewType.SplineArea;
            //        break;
            //    case "SplineArea3D":
            //        viewType = ViewType.SplineArea3D;
            //        break;
            //    case "StackedArea":
            //        viewType = ViewType.StackedArea;
            //        break;
            //    case "StackedArea3D":
            //        viewType = ViewType.StackedArea3D;
            //        break;
            //    case "StackedBar":
            //        viewType = ViewType.StackedBar;
            //        break;
            //    case "StackedSplineArea":
            //        viewType = ViewType.StackedSplineArea;
            //        break;
            //    case "StackedSplineArea3D":
            //        viewType = ViewType.StackedSplineArea3D;
            //        break;
            //    case "StepLine":
            //        viewType = ViewType.StepLine;
            //        break;
            //    case "StepLine3D":
            //        viewType = ViewType.StepLine3D;
            //        break;
            //    case "Stock":
            //        viewType = ViewType.Stock;
            //        break;
            //    case "StackedBar3D":
            //        viewType = ViewType.StackedBar3D;
            //        break;
            //}
            //return viewType;
        }

        private static double GetLineFactor(string index)
        {
            switch (index)
            {
                case "Circles":
                    return 1;
                case "Cardioid":
                    return 0.5;
                case "Lemniscate":
                    return 2;
                default:
                    return 2;
            }
        }
        private static double ToRadian(double angle)
        {
            return angle * Math.PI / 180.0;
        }
        private static double Function(double m, double angle)
        {
            double cos = Math.Cos(m * ToRadian(90.0 + angle));
            return Math.Pow(Math.Abs(cos), m);
        }
        private static SeriesPoint[] GenerateFunctionPoints(string index, int pointCount)
        {
            double m = GetLineFactor(index);
            int step = 360 / pointCount;
            SeriesPoint[] points = new SeriesPoint[pointCount];
            for (int i = 0; i < pointCount; i++)
                points[i] = new SeriesPoint(step * i, new double[] { Function(m, step * i) });
            return points;
        }
    }
}
