﻿using System;
using System.Collections.Generic;

using System.Text;

namespace InsightoV2.ExportSurveyReports
{
    public class SurveyResponse
    {
        int RespondentID, surveyID, EmailID, launchid, LastSeqNo;
        string Status, SessionID, EmailAddress, RandomSessionID;
        DateTime CreatedOn;
        private List<ResponseQuestion> ResponseQues;

        public int EMAIL_ID
        {
            get
            {
                return EmailID;
            }
            set
            {
                EmailID = value;
            }
        }
        public int SURVEY_ID
        {
            get
            {
                return surveyID;
            }
            set
            {
                surveyID = value;
            }
        }

        public int RESPONDENT_ID
        {
            get
            {
                return RespondentID;
            }
            set
            {
                RespondentID = value;
            }
        }
        public string STATUS
        {
            get
            {
                return Status;
            }
            set
            {
                Status = value;
            }
        }
        public DateTime CREATED_ON
        {
            get
            {
                return CreatedOn;
            }
            set
            {
                CreatedOn = value;
            }
        }

        public int LAUNCH_ID
        {
            get
            {
                return launchid;
            }
            set
            {
                launchid = value;
            }
        }
        public string SESSION_ID
        {
            get
            {
                return SessionID;
            }
            set
            {
                SessionID = value;
            }
        }
        public string EMAIL_ADDRESS
        {
            get
            {
                return EmailAddress;
            }
            set
            {
                EmailAddress = value;
            }
        }

        public string RANDOM_SESSIONID
        {
            get
            {
                return RandomSessionID;
            }
            set
            {
                RandomSessionID = value;
            }
        }

        public int LAST_QUES_SEQNO
        {
            get
            {
                return LastSeqNo;
            }
            set
            {
                LastSeqNo = value;
            }
        }
        public List<ResponseQuestion> ResponseQuestion
        {
            get
            {
                return ResponseQues;
            }
            set
            {
                ResponseQues = value;
            }
        }

    }
}
