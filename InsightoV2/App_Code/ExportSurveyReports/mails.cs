using System;
using System.Collections.Generic;
using System.Text;

namespace InsightoV2.ExportSurveyReports
{
    public class mails
    {
        int groupID,portnNo;
        string host, senderMailAddress, SenderPassword, FromMailAddress, mailSubject,mailBody,mailToaddress,logPath,mailccaddress,attachments;

        //private List<mailToList> mails_tolist;


        public int Group_ID
        {
            get
            {
                return groupID;
            }
            set
            {
                groupID = value;
            }
        }
        
        public string HOST
        {
            get
            {
                return host;
            }
            set
            {
                host = value;
            }
        }

        public string SENDER_MAILADDRESS
        {
            get
            {
                return senderMailAddress;
            }
            set
            {
                senderMailAddress = value;
            }
        }

        public string SENDER_PASSWORD
        {
            get
            {
                return SenderPassword;
            }
            set
            {
                SenderPassword = value;
            }
        }

        public string FROM_MAILADDRESS
        {
            get
            {
                return FromMailAddress;
            }
            set
            {
                FromMailAddress = value;
            }
        }

        public string MAIL_SUBJECT
        {
            get
            {
                return mailSubject;
            }
            set
            {
                mailSubject = value;
            }
        }

        public string MAIL_BODY
        {
            get
            {
                return mailBody;
            }
            set
            {
                mailBody = value;
            }
        }

        public string MAIL_TOADDRESS
        {
            get
            {
                return mailToaddress;
            }
            set
            {
                mailToaddress = value;
            }
        }

        public string LOG_PATH
        {
            get
            {
                return logPath;
            }
            set
            {
                logPath = value;
            }
        }

       

        public string SENDER_TOCCADDRESSES
        {
            get
            {
                return mailccaddress ;
            }
            set
            {
                mailccaddress = value;
            }
        }
        //public List<mailToList> mail_tolist
        //{
        //    get
        //    {
        //        return mails_tolist;
        //    }
        //    set
        //    {
        //        mails_tolist = value;
        //    }
        //}

        public string MAIL_ATTACHMENTS
        {
            get
            {
                return attachments;
            }
            set
            {
                attachments = value;
            }
        }

        public int PORTNO
        {
            get
            {
                return portnNo;
            }
            set
            {
                portnNo = value;
            }
        }
        public string UserName { get; set; }
        public string AttachmentFileName { get; set; }
    }
}
