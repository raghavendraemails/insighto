﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightoV2.ExportSurveyReports
{
    public enum DataProvider
    {
        Oracle, SqlServer, OleDb, Odbc, MySql
    }
}
