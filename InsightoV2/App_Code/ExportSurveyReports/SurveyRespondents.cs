﻿using System;
using System.Collections.Generic;

using System.Text;

namespace InsightoV2.ExportSurveyReports
{
    public class SurveyRespondents
    {
        private List<SurveyResponse> SurveyRespond;
        public List<SurveyResponse> SurveyResp
        {
            get
            {
                return SurveyRespond;
            }
            set
            {
                SurveyRespond = value;
            }
        }
    }
}
