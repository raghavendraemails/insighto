﻿using System;
using System.Collections.Generic;

using System.Text;

namespace InsightoV2.ExportSurveyReports
{
    public class SurveyOptions
    {

        int surveyID, surveycontrolID, thanksPageType, closePageType, screenoutPageType, overquotaPageType, EmailListType, SurveyLaunchType, SurveyCloseType, uniquRespondent,totalResponse;
        string unsubscribed, surveyPassword, thanksPage, closePage, screenoutPage, overquotaPage, URLredirection, emailInvitation, emailFromAddress, emailSubject, emailsendername, emailreplytoaddress, surveyURL;
        DateTime launchDate;

        public int SURVEY_ID
        {
            get
            {
                return surveyID;
            }
            set
            {
                surveyID = value;
            }
        }
        public int SURVEYCONTROL_ID
        {
            get
            {
                return surveycontrolID;
            }
            set
            {
                surveycontrolID = value;
            }
        }
        public int SURVEYLAUNCHTYPE
        {
            get
            {
                return SurveyLaunchType;
            }
            set
            {
                SurveyLaunchType = value;
            }
        }
         public int TOTAL_RESPONSES
        {
            get
            {
                return totalResponse;
            }
            set
            {
                totalResponse = value;
            }
        }
         public string SURVEY_URL
        {
            get
            {
                return surveyURL;
            }
            set
            {
                surveyURL = value;
            }
        }
        public int SURVEYCLOSETYPE
        {
            get
            {
                return SurveyCloseType;
            }
            set
            {
                SurveyCloseType = value;
            }
        }
        public int UNIQUE_RESPONDENT
        {
            get
            {
                return uniquRespondent;
            }
            set
            {
                uniquRespondent = value;
            }
        }
        public string UNSUBSCRIBED
        {
            get
            {
                return unsubscribed;
            }
            set
            {
                unsubscribed = value;
            }
        }
        public string SURVEY_PASSWORD
        {
            get
            {
                return surveyPassword;
            }
            set
            {
                surveyPassword = value;
            }
        }
        public int THANKU_PAGETYPE
        {
            get
            {
                return thanksPageType;
            }
            set
            {
                thanksPageType = value;
            }
        }
        public int CLOSE_PAGETYPE
        {
            get
            {
                return closePageType;
            }
            set
            {
                closePageType = value;
            }
        }
        public int SCREENOUT_PAGETYPE
        {
            get
            {
                return screenoutPageType;
            }
            set
            {
                screenoutPageType = value;
            }
        }
        public int OVERQUOTA_PAGETYPE
        {
            get
            {
                return overquotaPageType;
            }
            set
            {
                overquotaPageType = value;
            }
        }
        public int EMAILLIST_TYPE
        {
            get
            {
                return EmailListType;
            }
            set
            {
                EmailListType = value;
            }
        }
        public string THANKYOU_PAGE
        {
            get
            {
                return thanksPage;
            }
            set
            {
                thanksPage = value;
            }
        }
        public string CLOSE_PAGE
        {
            get
            {
                return closePage;
            }
            set
            {
                closePage = value;
            }
        }
        public string SCREENOUT_PAGE
        {
            get
            {
                return screenoutPage;
            }
            set
            {
                screenoutPage = value;
            }
        }
        public string OVERQUOTA_PAGE
        {
            get
            {
                return overquotaPage;
            }
            set
            {
                overquotaPage = value;
            }
        }
        public string URL_REDIRECTION
        {
            get
            {
                return URLredirection;
            }
            set
            {
                URLredirection = value;
            }
        }
        public string EMAIL_INVITATION
        {
            get
            {
                return emailInvitation;
            }
            set
            {
                emailInvitation = value;
            }
        }

        public string EMAIL_FROMADDRESS
        {
            get
            {
                return emailFromAddress;
            }
            set
            {
                emailFromAddress = value;
            }
        }
        public string EMAIL_SUBJECT
        {
            get
            {
                return emailSubject;
            }
            set
            {
                emailSubject = value;
            }
        }

        public string EMAIL_SENDERNAME
        {
            get
            {
                return emailsendername;
            }
            set
            {
                emailsendername = value;
            }
        }
        public string EMAIL_REPLYTOADDRESS
        {
            get
            {
                return emailreplytoaddress;
            }
            set
            {
                emailreplytoaddress = value;
            }
        }
        public DateTime LAUNCH_ON_DATE
        {
            get
            {
                return launchDate;
            }
            set
            {
                launchDate = value;
            }
        }



    }
}
