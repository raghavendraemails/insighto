﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;

using System.Configuration;
namespace InsightoV2.ExportSurveyReports
{
       
    public class JobExecuter
    {
        DBManager dbm = new DBManager();
        CultureInfo ci = new CultureInfo("en-us");
       
        string Logpath = "";
        string strMailSettingsPath = ConfigurationSettings.AppSettings["MailSettings"].ToString();
        public string GetConfigVal(string Value)
        {
            string val = "";
            string Qry = "Select CONFIG_VALUE from oms_config where CONFIG_KEY='" + Value + "' and DELETED=0";
            object obj = dbm.ExecuteScalar(CommandType.Text, Qry);
            try
            {
                if (obj != null)
                    val = Convert.ToString(obj);
            }
            catch { }
            return val;
        }
        public void CreateExportFiles()
        {
             DataSet dsq = new DataSet();
            //dsq.ReadXml("C:\\MailSending.xml");
             dsq.ReadXml(strMailSettingsPath);  
            try
            {
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                string qry1 = "select REPORT_ID, REPORT_TYPE,REPORT_MODE,USERID,SURVEY_ID from osm_surveyreports where REPORT_STATUS=0 and DELETED=0";
                qry1 += " Select ADM_REPORTID,REPORT_PARAMETERS,REPORT_STATUS from osm_adminreports where REPORT_STATUS=0 and DELETED=0 ";
                DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qry1);
                SurveyReportsExport exp = new SurveyReportsExport();
                string filename = "";
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            string qry2 = "Update osm_surveyreports SET REPORT_STATUS=1 where REPORT_ID=" + Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_ID"]);
                            if (dbm.Connection.State == ConnectionState.Closed)
                                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                            dbm.ExecuteNonQuery(CommandType.Text, qry2);

                            filename = "";
                            int flag = 0;
                            if (Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_TYPE"]) == 2)
                            {
                                filename = exp.CreateMSExcelSheet(Convert.ToInt32(ds.Tables[0].Rows[i]["SURVEY_ID"]), Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_TYPE"]), Convert.ToString(ds.Tables[0].Rows[i]["REPORT_MODE"]));
                                flag = 1;
                            }
                            else if (Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_TYPE"]) == 0)
                            {
                                filename = exp.CreatePPTExport(Convert.ToInt32(ds.Tables[0].Rows[i]["SURVEY_ID"]), Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_TYPE"]), Convert.ToString(ds.Tables[0].Rows[i]["REPORT_MODE"]));
                                flag = 1;
                            }
                            else if (Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_TYPE"]) == 3)
                            {
                                filename = exp.CreateWordExport(Convert.ToInt32(ds.Tables[0].Rows[i]["SURVEY_ID"]), Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_TYPE"]), Convert.ToString(ds.Tables[0].Rows[i]["REPORT_MODE"]));
                                flag = 1;
                            }
                            else if (Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_TYPE"]) == 4 || Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_TYPE"]) == 5)
                            {
                                filename = exp.CreateRawDataExpot(Convert.ToInt32(ds.Tables[0].Rows[i]["SURVEY_ID"]), Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_TYPE"]));
                                flag = 1;
                            }
                            else if (Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_TYPE"]) == 6)
                            {

                                filename = exp.CreateCrosstabReport(Convert.ToInt32(ds.Tables[0].Rows[i]["SURVEY_ID"]), Convert.ToString(ds.Tables[0].Rows[i]["REPORT_MODE"]));
                                 flag = 1;
                            }

                            if (flag == 1)
                            {
                                int status_resp = 2;
                                if (filename == "")
                                {
                                    status_resp = 3;

                                }
                                if (filename == "No Responses")
                                {
                                    status_resp = 4;
                                }

                                if (filename != "")
                                {
                                    string qrystr = "Update osm_surveyreports SET REPORT_STATUS=" + status_resp + ", REPORT_FILENAME='" + filename + "' where REPORT_ID=" + Convert.ToInt32(ds.Tables[0].Rows[i]["REPORT_ID"]);
                                    if (dbm.Connection.State == ConnectionState.Closed)
                                        dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                                    dbm.ExecuteNonQuery(CommandType.Text, qrystr);
                                }


                            }


                        }
                    }
                
                    if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                    {
                       
                        for (int j= 0; j < ds.Tables[1].Rows.Count; j++)
                        {
                                
                                filename = "";
                                string qry2 = "Update osm_adminreports SET REPORT_STATUS=1 where ADM_REPORTID=" + Convert.ToInt32(ds.Tables[1].Rows[j]["ADM_REPORTID"]);
                                if (dbm.Connection.State == ConnectionState.Closed)
                                    dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                                dbm.ExecuteNonQuery(CommandType.Text, qry2);
                                filename = exp.ExportAdminUserslist(Convert.ToString(ds.Tables[1].Rows[j]["REPORT_PARAMETERS"]));
                                int status_resp = 2;
                                if (filename == "")
                                {
                                    status_resp = 3;

                                }
                                string qrystr = "Update osm_adminreports SET REPORT_STATUS=" + status_resp + ", REPORT_FILENAME='" + filename + "' where ADM_REPORTID=" + Convert.ToInt32(ds.Tables[1].Rows[j]["ADM_REPORTID"]);
                                if (dbm.Connection.State == ConnectionState.Closed)
                                    dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                                dbm.ExecuteNonQuery(CommandType.Text, qrystr);
                        }


                    }

                }

            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt");
            }
            finally
            {
                dbm.Close();
            }



        }
   
    }
}
