﻿using System;
using System.Collections.Generic;

using System.Text;

namespace InsightoV2.ExportSurveyReports
{
    public class Survey
    {
        int surveyID, folderID, userID, PageBreak, ResponseRequired, CheckBlockWords, SurveyIntroPageBreak, BrowserbackButton,standardbias;
        string surveyName, surveyCategory, active, status, SurveyINTRO, SurveyHeader, SurveyFooter, surveyFontType, SurveyFontSize, SurveyFontColor, surveyButtonType, CustomStarttext, CustomSubmittext, Theme, Logo;
        string LoginName, FirstName, LastName, time_zone;
        private List<SurveyOptions> surveyOpt;
        private List<SurveyQuestion> surveyQue;
        // = new List<SurveyOptions>(); 
        //int SURVEY_ID, FOLDER_ID, USER_ID, CONTACTLIST_ID, PENDING_VERFICATION, BOUNCED, DUPLICATED, THEME_ID, STATUS, ONLAUNCH_ALERT, CLOSE_ON_NO_RESPONDENTS, CLOSE_ON_DATE, CONTINUOUS_SURVEY, UNIQUE_RESPONDENT, SAVE_AND_CONTINUE, AUTOMATIC_THANKYOU_EMAIL, ALERT_ON_MIN_RESPONSES, ALERT_ON_MAX_RESPONSES, SEND_TO, TOTAL_RESPONSES, PARTIAL_RESPONSES, SCREEN_OUTS;
        //string SURVEY_NAME, SURVEY_CATEGORY, ACTIVE, UNSUBSCRIBED, SURVEY_PASSWORD, SURVEY_URL, REMINDER_TEXT, THANKYOU_PAGE, CLOSE_PAGE, SCREENOUT_PAGE, OVERQUOTA_PAGE, URL_REDIRECTION, TIME_FOR_URL_REDIRECTION, EMAIL_INVITATION;
        //DateTime LAUNCH_ON_DATE, ALERT_ON_MIN_RESPONSES_BY_DATE, ALERT_ON_MAX_RESPONSES_BY_DATE, REMINDER_ON_DATE, LAUNCHED_ON;

        public int SURVEY_ID
        {
            get
            {
                return surveyID;
            }
            set
            {
                surveyID = value;
            }
        }
        public int FOLDER_ID
        {
            get
            {
                return folderID;
            }
            set
            {
                folderID = value;
            }
        }
        public string TIME_ZONE
        {
            get
            {
                return time_zone;
            }
            set
            {
                time_zone = value;
            }
        }
        public string LOGIN_NAME
        {
            get
            {
                return LoginName;
            }
            set
            {
                LoginName = value;
            }
        }

       
        public string FIRST_NAME
        {
            get
            {
                return FirstName;
            }
            set
            {
                FirstName = value;
            }
        }

        public string LAST_NAME
        {
            get
            {
                return LastName;
            }
            set
            {
                LastName = value;
            }
        }
        public int STANDARD_BIAS
        {
            get
            {
                return standardbias;
            }
            set
            {
                standardbias = value;
            }
        }
        public int USER_ID
        {
            get
            {
                return userID;
            }
            set
            {
                userID = value;
            }
        }
        public int PAGE_BREAK
        {
            get
            {
                return PageBreak;
            }
            set
            {
                PageBreak = value;
            }
        }

        public int SURVEYINTRO_PAGEBREAK
        {
            get
            {
                return SurveyIntroPageBreak;
            }
            set
            {
                SurveyIntroPageBreak = value;
            }
        }
        public int RESPONSE_REQUIRED
        {
            get
            {
                return ResponseRequired;
            }
            set
            {
                ResponseRequired = value;
            }
        }
        public int CHECK_BLOCKWORDS
        {
            get
            {
                return CheckBlockWords;
            }
            set
            {
                CheckBlockWords = value;
            }
        }
        public string SURVEY_INTRO
        {
            get
            {
                return SurveyINTRO;
            }
            set
            {
                SurveyINTRO = value;
            }
        }
        public string SURVEY_NAME
        {
            get
            {
                return surveyName;
            }
            set
            {
                surveyName = value;
            }
        }
        public string SURVEY_CATEGORY
        {
            get
            {
                return surveyCategory;
            }
            set
            {
                surveyCategory = value;
            }
        }
        public string ACTIVE
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }
        public string STATUS
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public string SURVEY_HEADER
        {
            get
            {
                return SurveyHeader;
            }
            set
            {
                SurveyHeader = value;
            }
        }

        public string SURVEY_FOOTER
        {
            get
            {
                return SurveyFooter;
            }
            set
            {
                SurveyFooter = value;
            }
        }
        public string SURVEY_FONT_TYPE
        {
            get
            {
                return surveyFontType;
            }
            set
            {
                surveyFontType = value;
            }
        }
        public string SURVEY_FONT_SIZE
        {
            get
            {
                return SurveyFontSize;
            }
            set
            {
                SurveyFontSize = value;
            }
        }
        public string SURVEY_FONT_COLOR
        {
            get
            {
                return SurveyFontColor;
            }
            set
            {
                SurveyFontColor = value;
            }
        }


        public string SURVEY_BUTTON_TYPE
        {
            get
            {
                return surveyButtonType;
            }
            set
            {
                surveyButtonType = value;
            }
        }
        public string CUSTOMSTART_TEXT
        {
            get
            {
                return CustomStarttext;
            }
            set
            {
                CustomStarttext = value;
            }
        }
        public string CUSTOMSUBMITTEXT
        {
            get
            {
                return CustomSubmittext;
            }
            set
            {
                CustomSubmittext = value;
            }
        }
        public string THEME
        {
            get
            {
                return Theme;
            }
            set
            {
                Theme = value;
            }
        }
        public int BROWSER_BACKBUTTON_STATUS
        {
            get
            {
                return BrowserbackButton;
            }
            set
            {
                BrowserbackButton = value;
            }
        }

        public string SURVEY_LOGO
        {
            get
            {
                return Logo;
            }
            set
            {
                Logo = value;
            }
        }
        public List<SurveyOptions> surveyOptns
        {
            get
            {
                return surveyOpt;
            }
            set
            {
                surveyOpt = value;
            }
        }
        public List<SurveyQuestion> surveyQues
        {
            get
            {
                return surveyQue;
            }
            set
            {
                surveyQue = value;
            }
        }

    }
}
