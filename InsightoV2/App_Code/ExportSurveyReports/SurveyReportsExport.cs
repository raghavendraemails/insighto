﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts.Web;
using DevExpress.Web.ASPxRoundPanel;
using Microsoft.Office.Interop;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Core;
using Microsoft.Office;
using Microsoft.Office.Interop.PowerPoint;
using Word = Microsoft.Office.Interop.Word;
using System.Web.UI.WebControls;
//using Missing = System.Reflection.Missing;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using Range = Microsoft.Office.Interop.Excel.Range;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Linq;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;


namespace InsightoV2.ExportSurveyReports
{
    public class SurveyReportsExport
    {
        string Path = ConfigurationManager.AppSettings["RootPath"].ToString();
        DBManager dbm = new DBManager();
        string Logpath = "";
        Excel.Application oXL;
        Excel._Workbook oWB;
        Excel._Worksheet IntroSheet, RespSheet, NonDesc, DescSheet;
        Excel.Range oRng;
        Excel.ChartObject myChart;
        Excel.ChartObjects xlCharts;
        Excel.Chart chartPage;
        Excel.Range chartRange;
        int QType_count = 0;
        int SMode = 0;
        DataSet dsq = new DataSet();
        string strMailSettingsPath = ConfigurationSettings.AppSettings["MailSettings"].ToString();

        public string GetConfigVal(string Value)
        {
            string val = "";
            dsq.ReadXml(strMailSettingsPath);
            dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
            string Qry = "Select CONFIG_VALUE from oms_config where CONFIG_KEY='" + Value + "' and DELETED=0";
            object obj = dbm.ExecuteScalar(CommandType.Text, Qry);
            try
            {
                if (obj != null)
                    val = Convert.ToString(obj);
            }
            catch { }
            return val;
        }
        public Survey GetSurveyDet(int SurveyID)
        {
            try
            {
                Survey sur = new Survey();
                if (SurveyID > 0)
                {
                    string str = " select sur.SURVEY_NAME,sur.USERID,SurIntro.SURVEY_INTRO from osm_survey sur";
                    str += " left outer join osm_surveyIntro SurIntro on SurIntro.SURVEY_ID=sur.SURVEY_ID ";
                    str += " where sur.SURVEY_ID= " + SurveyID;

                    string surSet = " select SURVEY_HEADER,SURVEY_FOOTER,CUSTOMSTART_TEXT,CUSTOMSUBMITTEXT,SURVEY_LOGO,SURVEY_BUTTON_TYPE,SURVEY_FONT_TYPE,SURVEY_FONT_SIZE,SURVEY_FONT_COLOR from osm_surveysetting where DELETED=0 and SURVEY_ID= " + SurveyID;
                    string Queqry = " select QUESTION_ID,QUSETION_LABEL,QUESTION_SEQ,QUESTION_TYPE_ID,PAGE_BREAK,RESPONSE_REQUIRED,OTHER_ANS,CHAR_LIMIT,ANSWER_ALIGNSTYLE,SKIP_LOGIC from osm_surveyquestion where DELETED=0 and SURVEY_ID= " + SurveyID;
                    string Conqry = " select CLOSE_PAGE,EMAIL_INVITATION,FROM_MAILADDRESS,MAIL_SUBJECT,LAUNCH_ON_DATE,OVERQUOTA_PAGE,SCREENOUT_PAGE,SURVEY_ID,SURVEYCONTROL_ID,THANKYOU_PAGE,UNSUBSCRIBED,URL_REDIRECTION,SENDER_NAME,REPLY_TO_EMAILADDRESS,SCHEDULE_ON,SCHEDULE_ON_CLOSE,THANKUPAGE_TYPE,CLOSEPAGE_TYPE,SCREENOUTPAGE_TYPE,OVERQUOTAPAGE_TYPE,UNIQUE_RESPONDENT, from osm_surveycontrols where DELETED=0 and SURVEY_ID= " + SurveyID;

                    DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        sur.USER_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["USERID"]);
                        sur.SURVEY_ID = SurveyID;
                        sur.SURVEY_INTRO = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_INTRO"]);
                        sur.SURVEY_NAME = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_NAME"]);

                        DataSet surSetds = dbm.ExecuteDataSet(CommandType.Text, surSet);
                        if (surSetds != null && surSetds.Tables.Count > 0 && surSetds.Tables[0].Rows.Count > 0)
                        {
                            sur.SURVEY_HEADER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_HEADER"]);
                            sur.SURVEY_FOOTER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FOOTER"]);
                            sur.CUSTOMSTART_TEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSTART_TEXT"]);
                            sur.CUSTOMSUBMITTEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSUBMITTEXT"]);
                            sur.SURVEY_FONT_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_TYPE"]);
                            sur.SURVEY_FONT_SIZE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_SIZE"]);
                            sur.SURVEY_FONT_COLOR = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_COLOR"]);
                            sur.SURVEY_BUTTON_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_BUTTON_TYPE"]);
                            sur.SURVEY_LOGO = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_LOGO"]);

                        }

                        DataSet Quesds = dbm.ExecuteDataSet(CommandType.Text, Queqry);
                        if (Quesds != null && Quesds.Tables.Count > 0 && Quesds.Tables[0].Rows.Count > 0)
                        {
                            sur.surveyQues = new List<SurveyQuestion>();
                            for (int i = 0; i < Quesds.Tables[0].Rows.Count; i++)
                            {
                                SurveyQuestion ques = new SurveyQuestion();
                                ques.QUESTION_ID = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_ID"]);
                                ques.QUESTION_LABEL = Convert.ToString(Quesds.Tables[0].Rows[i]["QUSETION_LABEL"]);
                                ques.QUESTION_SEQ = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_SEQ"]);
                                ques.QUESTION_TYPE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_TYPE_ID"]);
                                ques.RESPONSE_REQUIRED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RESPONSE_REQUIRED"]);
                                ques.RANDOM_ANSWERS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RANDOMIZE_ANSWERS"]);
                                ques.OTHER_ANS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["OTHER_ANS"]);
                                ques.SKIP_LOGIC = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SKIP_LOGIC"]);
                                ques.ANSWER_ALIGNSTYLE = Convert.ToString(Quesds.Tables[0].Rows[i]["ANSWER_ALIGNSTYLE"]);
                                ques.PAGE_BREAK = Convert.ToInt32(Quesds.Tables[0].Rows[i]["PAGE_BREAK"]);
                                ques.CHAR_LIMIT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["CHAR_LIMIT"]);
                                string Ansqry = " select ANSWER_ID,ANSWER_OPTIONS,QUESTION_ID,PIPING_TEXT from osm_answeroptions where DELETED=0 and QUESTION_ID= " + ques.QUESTION_ID + " order by ANSWER_ID"; ;
                                DataSet Ansds = dbm.ExecuteDataSet(CommandType.Text, Ansqry);
                                if (Ansds != null && Ansds.Tables.Count > 0 && Ansds.Tables[0].Rows.Count > 0)
                                {
                                    ques.surveyAnswers = new List<SurveyAnswers>();
                                    for (int y = 0; y < Ansds.Tables[0].Rows.Count; y++)
                                    {
                                        SurveyAnswers Ansr = new SurveyAnswers();
                                        Ansr.ANSWER_ID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["ANSWER_ID"]);
                                        Ansr.ANSWER_OPTIONS = Convert.ToString(Ansds.Tables[0].Rows[y]["ANSWER_OPTIONS"]);
                                        Ansr.QUESTION_ID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["QUESTION_ID"]);
                                        Ansr.PIPING_TEXT = Convert.ToString(Ansds.Tables[0].Rows[y]["PIPING_TEXT"]);
                                        ques.surveyAnswers.Add(Ansr);
                                    }
                                }
                                sur.surveyQues.Add(ques);
                            }
                        }

                        DataSet Controlsds = dbm.ExecuteDataSet(CommandType.Text, Conqry);
                        if (Controlsds != null && Controlsds.Tables.Count > 0 && Controlsds.Tables[0].Rows.Count > 0)
                        {
                            sur.surveyOptns = new List<SurveyOptions>();
                            for (int j = 0; j < Controlsds.Tables[0].Rows.Count; j++)
                            {
                                SurveyOptions opt = new SurveyOptions();
                                opt.CLOSE_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["CLOSE_PAGE"]);
                                opt.EMAIL_INVITATION = Convert.ToString(Controlsds.Tables[0].Rows[j]["EMAIL_INVITATION"]);
                                opt.EMAIL_FROMADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["FROM_MAILADDRESS"]);
                                opt.EMAIL_SUBJECT = Convert.ToString(Controlsds.Tables[0].Rows[j]["MAIL_SUBJECT"]);
                                opt.LAUNCH_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["LAUNCH_ON_DATE"]);
                                opt.OVERQUOTA_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["OVERQUOTA_PAGE"]);
                                opt.SCREENOUT_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["SCREENOUT_PAGE"]);
                                opt.SURVEY_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEY_ID"]);
                                opt.SURVEYCONTROL_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEYCONTROL_ID"]);
                                opt.THANKYOU_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["THANKYOU_PAGE"]);
                                opt.UNSUBSCRIBED = Convert.ToString(Controlsds.Tables[0].Rows[j]["UNSUBSCRIBED"]);
                                opt.URL_REDIRECTION = Convert.ToString(Controlsds.Tables[0].Rows[j]["URL_REDIRECTION"]);
                                opt.EMAIL_SENDERNAME = Convert.ToString(Controlsds.Tables[0].Rows[j]["SENDER_NAME"]);
                                opt.EMAIL_REPLYTOADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["REPLY_TO_EMAILADDRESS"]);
                                opt.THANKU_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["THANKUPAGE_TYPE"]);
                                opt.CLOSE_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["CLOSEPAGE_TYPE"]);
                                opt.SCREENOUT_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCREENOUTPAGE_TYPE"]);
                                opt.OVERQUOTA_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["OVERQUOTAPAGE_TYPE"]);
                                opt.SURVEYLAUNCHTYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON"]);
                                opt.UNIQUE_RESPONDENT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["UNIQUE_RESPONDENT"]);
                                opt.EMAILLIST_TYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["EMAILLIST_TYPE"]);
                                opt.SURVEYCLOSETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE"]);
                                sur.surveyOptns.Add(opt);
                            }
                        }
                    }
                }
                return sur;
            }
            catch { return null; }

        }
        #region  Export To ExcelSheet
        //For Excel sheet 
        public string CreateMSExcelSheet(int SurveyID, int resp_type, string ExportMode)
        {
            string strXLFileName = "";
            string toEmailID = "";
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            try
            {
                ValidationCore Vcore = new ValidationCore();
                ExportSurveyCore scor = new ExportSurveyCore();
                Survey sur = new Survey();

                //sur = scor.GetSurveyDet(SurveyID);
                sur = scor.GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                toEmailID = scor.GetEmailId(SurveyID);
                double toppos = 500;
                float Top = 0;
                Top = (float)toppos;
                //int SurveyID = sur.SURVEY_ID;
                string name_survey = sur.SURVEY_NAME.Replace(" ", "");
                name_survey = name_survey.Replace("/", "");
                name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                //string strXLFileName = MapPath("~/") + "In\\UserImages\\" + SelectedSurvey.USER_ID + "\\" + sur.SURVEY_ID + "\\" + name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".xls";
                // "C:\\Admin\\" 
                string filename = name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".xls";
                string sXLFileName = Path + sur.USER_ID + "\\" + sur.SURVEY_ID;

                if (!Directory.Exists(sXLFileName))
                {
                    Directory.CreateDirectory(sXLFileName);
                }
                strXLFileName = Path + sur.USER_ID + "\\" + sur.SURVEY_ID + "\\" + filename;

                string surName = sur.SURVEY_NAME;
                GC.Collect();
                oXL = new Excel.Application();
                oXL.Visible = false;
                oWB = (Excel._Workbook)(oXL.Workbooks.Add(Missing.Value));
                GetIntroPage(surName, SurveyID, sur);
                int count = 0;
                int quesCnt = 0;
                int qcount = 0;
                int ref_index = 0;
                if (sur != null && sur.surveyQues != null && sur.surveyQues.Count > 0)
                {
                    sur.surveyQues.Sort();
                    quesCnt = sur.surveyQues.Count;
                    qcount = quesCnt;
                }
                int max_col_count = 0;
                int[] quesID = new int[quesCnt];
                int temp_max_col_count = 0;
                if (sur.surveyQues != null && sur.surveyQues.Count > 0)
                {

                    foreach (SurveyQuestion sq in sur.surveyQues)
                    {
                        int QID = Convert.ToInt32(sq.QUESTION_ID);
                        quesID[--quesCnt] = QID;
                        if (sq.QUESTION_TYPE == 10 || sq.QUESTION_TYPE == 11 || sq.QUESTION_TYPE == 12 || sq.QUESTION_TYPE == 15)
                        {
                            if (sq.surveyAnswers != null)
                            {
                                Hashtable headerList1 = BuildQuetions.GetAnsItemsList(sq);
                                ArrayList colHeads1 = new ArrayList();
                                ArrayList ColSubHead1 = new ArrayList();
                                if (sq.QUESTION_TYPE == 10 || sq.QUESTION_TYPE == 11)
                                {
                                    if (headerList1.Contains("ColHead"))
                                    {
                                        colHeads1 = (ArrayList)headerList1["ColHead"];
                                        max_col_count = colHeads1.Count;
                                    }

                                }
                                if (sq.QUESTION_TYPE == 12)
                                {
                                    if (headerList1.Contains("ColHead"))
                                        colHeads1 = (ArrayList)headerList1["ColHead"];
                                    if (headerList1.Contains("ColSubHead"))
                                        ColSubHead1 = (ArrayList)headerList1["ColSubHead"];
                                    max_col_count = colHeads1.Count + ColSubHead1.Count + 1;
                                }
                                if (sq.QUESTION_TYPE == 15)
                                {

                                    max_col_count = sq.surveyAnswers.Count;

                                }
                                if (max_col_count > temp_max_col_count)
                                    temp_max_col_count = max_col_count;
                            }

                        }
                    }
                }
                max_col_count = temp_max_col_count + 2;
                if (max_col_count == 0 || max_col_count <= 3)
                    max_col_count = 4;
                Excel._Worksheet[] Sheet = new Excel._Worksheet[qcount];
                int jj = 1, ij = 3;
                int sheetno = 1;

                if (sur.surveyQues != null && sur.surveyQues.Count > 0)
                {
                    foreach (SurveyQuestion Ques in sur.surveyQues)
                    {
                        jj = 1;
                        if (Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11)
                        {
                            ref_index = 1;
                            if (Ques.QUESTION_TYPE == 10)
                                NonDesc.Cells[ij++, jj] = "Matrix - Single Select (Rating Scale)";
                            else if (Ques.QUESTION_TYPE == 11)
                                NonDesc.Cells[ij++, jj] = "Matrix - Multiple Select";

                            ArrayList colHeads = new ArrayList();
                            ArrayList rowHeads = new ArrayList();
                            Hashtable headerList = BuildQuetions.GetAnsItemsList(Ques);
                            if (headerList.Contains("ColHead"))
                                colHeads = (ArrayList)headerList["ColHead"];
                            if (headerList.Contains("RowHead"))
                                rowHeads = (ArrayList)headerList["RowHead"];
                            System.Web.UI.WebControls.Label lblNonDescQueslbl = new System.Web.UI.WebControls.Label();
                            lblNonDescQueslbl.Text = Ques.QUESTION_LABEL;
                            string Question = Vcore.clearHTML(Convert.ToString(Ques.QUESTION_LABEL));
                            Question = Question.Replace("\r", " ");
                            Question = Question.Replace("\n", " ");
                            string NonDescQno = "Q" + Ques.QUESTION_SEQ + " : " + Question;
                            int QuesID = Ques.QUESTION_ID;
                            int cellForPic = ij;
                            Excel.Range range = NonDesc.get_Range(NonDesc.Cells[ij, jj], NonDesc.Cells[ij, colHeads.Count + 1]);
                            range.Merge(true);
                            range.HorizontalAlignment = Excel.Constants.xlCenter;
                            range.Interior.ColorIndex = 24;
                            range.WrapText = true;

                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            int bord_value = ij;
                            NonDesc.Cells[ij++, jj] = NonDescQno;
                            int pos1 = ij++;
                            int pos2 = jj;
                            string str1 = "";
                            NonDesc.Cells[pos1, 1] = "Answer Options";
                            for (int c = 0; c < colHeads.Count; c++)
                            {
                                str1 = Vcore.clearHTML(colHeads[c].ToString());
                                NonDesc.Cells[pos1, ++jj] = str1;
                            }

                            range = NonDesc.get_Range(NonDesc.Cells[pos1, 1], NonDesc.Cells[pos1, colHeads.Count + 1]);
                            range.HorizontalAlignment = Excel.Constants.xlCenter;
                            range.Interior.ColorIndex = 44;
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            str1 = "";
                            for (int r = 0; r < rowHeads.Count; r++)
                            {
                                str1 = Vcore.clearHTML(rowHeads[r].ToString());
                                str1 = str1.Replace("\r", " ");
                                str1 = str1.Replace("\n", " ");
                                NonDesc.Cells[ij++, 1] = str1;
                            }
                            ArrayList column_values = new ArrayList();
                            int m = 0, b = 0;
                            int TotalResponsCount = 0;

                            foreach (SurveyAnswers ans in Ques.surveyAnswers)
                            {
                                m += 1; ;
                                b = b + ans.RESPONSE_COUNT;
                                if (m == colHeads.Count)
                                {
                                    column_values.Add(b);
                                    b = 0;
                                    m = 0;
                                }
                                TotalResponsCount += ans.RESPONSE_COUNT;
                            }
                            int row_count = 0, g = 0, l = 0;
                            if (TotalResponsCount > 0)
                            {
                                ArrayList ansrescount = new ArrayList();
                                ArrayList ansrespercent = new ArrayList();

                                foreach (SurveyAnswers AnsOp in Ques.surveyAnswers)
                                {
                                    ansrescount.Add(AnsOp.RESPONSE_COUNT);
                                    double ResPercent = 0;
                                    if (column_values[row_count] != null && Convert.ToInt32(column_values[row_count]) > 0)
                                        ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT * 100) / Convert.ToInt32(column_values[row_count]), 1);

                                    g = g + 1;
                                    if (g == colHeads.Count)
                                    {
                                        row_count += 1;
                                    }
                                    l++;
                                    if (l == colHeads.Count)
                                    {
                                        l = 0;
                                        g = 0;
                                    }
                                    ansrespercent.Add(ResPercent);

                                }
                                int colheadscount = 0;
                                pos1 += 1;
                                int pos3 = pos2;
                                for (int cell_values = 0; cell_values < ansrescount.Count; cell_values++)
                                {
                                    if (colheadscount == colHeads.Count)
                                    {
                                        pos1 += 1;
                                        pos2 = pos3;
                                        colheadscount = 0;
                                    }

                                    NonDesc.Cells[pos1, ++pos2] = ansrescount[cell_values] + "(" + ansrespercent[cell_values] + "%)";
                                    Excel.Range range1 = NonDesc.get_Range(NonDesc.Cells[pos1, pos2], NonDesc.Cells[ij, jj + max_col_count]);
                                    range1.HorizontalAlignment = Excel.Constants.xlRight;
                                    colheadscount += 1;
                                }

                            }

                            for (int r = bord_value + 3; r < ij - 1; r++)
                            {
                                range = NonDesc.get_Range(NonDesc.Cells[r, 1], NonDesc.Cells[r, colHeads.Count + 1]);
                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            }
                            for (int r = 2; r <= colHeads.Count + 1; r++)
                            {
                                range = NonDesc.get_Range(NonDesc.Cells[bord_value + 1, r], NonDesc.Cells[ij - 1, r]);
                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            }


                            range = NonDesc.get_Range(NonDesc.Cells[bord_value, 1], NonDesc.Cells[pos1, colHeads.Count + 1]);
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            ij += 1;
                        }


                        else if (Ques.QUESTION_TYPE == 12)
                        {
                            ref_index = 1;
                            NonDesc.get_Range("A1", "A1").Font.Bold = true;
                            if (Ques.QUESTION_TYPE == 12)
                                NonDesc.Cells[ij++, jj] = "Matrix - Side by Side ";
                            ArrayList colHead = new ArrayList();
                            ArrayList ColSubHead = new ArrayList();
                            ArrayList rowHead = new ArrayList();
                            Hashtable headerList = BuildQuetions.GetAnsItemsList(Ques);
                            if (headerList.Contains("ColHead"))
                                colHead = (ArrayList)headerList["ColHead"];
                            if (headerList.Contains("ColSubHead"))
                                ColSubHead = (ArrayList)headerList["ColSubHead"];
                            if (headerList.Contains("RowHead"))
                                rowHead = (ArrayList)headerList["RowHead"];
                            System.Web.UI.WebControls.Label lblNonDescQueslbl = new System.Web.UI.WebControls.Label();
                            lblNonDescQueslbl.Text = Ques.QUESTION_LABEL;
                            string Question = Vcore.clearHTML(Convert.ToString(Ques.QUESTION_LABEL));
                            string NonDescQno = "Q" + Ques.QUESTION_SEQ + " : " + Question;
                            int QuesID = Ques.QUESTION_ID;
                            int cellForPic = ij;
                            int x = (ColSubHead.Count + 1) * colHead.Count;
                            Excel.Range range = NonDesc.get_Range(NonDesc.Cells[ij, jj], NonDesc.Cells[ij, x]);
                            range.HorizontalAlignment = Excel.Constants.xlCenter;
                            range.Merge(true);
                            range.Interior.ColorIndex = 24;
                            range.WrapText = true;
                            range.VerticalAlignment = Excel.Constants.xlTopToBottom;
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            int bord_value = ij;
                            NonDesc.Cells[ij++, jj] = NonDescQno;
                            int posmain = ij;
                            jj = 2;
                            for (int main_col_heads = 0; main_col_heads < colHead.Count; main_col_heads++)
                            {
                                Excel.Range range1 = NonDesc.get_Range(NonDesc.Cells[ij, jj], NonDesc.Cells[ij, jj + ColSubHead.Count - 1]);
                                range1.Merge(true);
                                range1.Interior.ColorIndex = 44;
                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                                range1.Cells.HorizontalAlignment = Excel.Constants.xlCenter;
                                NonDesc.Cells[ij, jj] = Vcore.clearHTML(colHead[main_col_heads].ToString());
                                jj = jj + ColSubHead.Count + 1;

                            }
                            int possubmain = ij + 1;
                            NonDesc.Cells[possubmain, 1] = "Answer Options";
                            range = NonDesc.get_Range(NonDesc.Cells[possubmain, 1], NonDesc.Cells[possubmain, x]);
                            range.Interior.ColorIndex = 44;

                            range = NonDesc.get_Range(NonDesc.Cells[possubmain - 1, 1], NonDesc.Cells[possubmain, x]);
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

                            jj = 2;
                            for (int main_col_heads = 0; main_col_heads < colHead.Count; main_col_heads++)
                            {
                                for (int sub_col_heads = 0; sub_col_heads < ColSubHead.Count; sub_col_heads++)
                                {
                                    NonDesc.Cells[possubmain, jj++] = Vcore.clearHTML(ColSubHead[sub_col_heads].ToString());
                                }
                                jj = jj + 1;

                            }
                            jj = 1;
                            int row_point = ++ij + 1;
                            ij = ij + 1;
                            for (int row_ans_opts = 0; row_ans_opts < rowHead.Count; row_ans_opts++)
                            {
                                string st = Vcore.clearHTML(rowHead[row_ans_opts].ToString());
                                st = st.Replace("\r", " ");
                                st = st.Replace("\n", " ");
                                NonDesc.Cells[ij++, jj] = st;

                            }
                            jj = 2;
                            int TotalResponsCount = 0;
                            ArrayList column_values = new ArrayList();
                            int m = 0, b = 0;
                            foreach (SurveyAnswers ans in Ques.surveyAnswers)
                            {
                                m += 1; ;
                                b = b + ans.RESPONSE_COUNT;
                                if (m == ColSubHead.Count)
                                {
                                    column_values.Add(b);
                                    b = 0;
                                    m = 0;
                                }
                                TotalResponsCount += ans.RESPONSE_COUNT;
                            }
                            if (TotalResponsCount > 0)
                            {
                                ArrayList ansrescount = new ArrayList();
                                ArrayList ansrespercent = new ArrayList();
                                int row_count = 0, g = 0;
                                foreach (SurveyAnswers AnsOp in Ques.surveyAnswers)
                                {
                                    ansrescount.Add(AnsOp.RESPONSE_COUNT);
                                    double ResPercent = 0;
                                    //if (TotalResponsCount > 0)
                                    //    ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT * 100) / TotalResponsCount, 1);
                                    if (column_values[row_count] != null && Convert.ToInt32(column_values[row_count]) > 0)
                                        ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT * 100) / Convert.ToInt32(column_values[row_count]), 2);
                                    ansrespercent.Add(ResPercent);
                                    g = g + 1;
                                    if (g == ColSubHead.Count)
                                    {
                                        row_count += 1;
                                        g = 0;
                                    }
                                }

                                int columnvalues_count = colHead.Count * ColSubHead.Count;
                                int sub_matrix_count = 0;
                                int row_cells_count = 0;
                                for (int cell_values = 0; cell_values < ansrescount.Count; cell_values++)
                                {

                                    NonDesc.Cells[row_point, jj] = ansrescount[cell_values] + "(" + ansrespercent[cell_values] + "%)";
                                    Excel.Range range2 = NonDesc.get_Range(NonDesc.Cells[row_point, jj], NonDesc.Cells[row_point, jj]);
                                    range2.HorizontalAlignment = Excel.Constants.xlRight;
                                    sub_matrix_count += 1;
                                    row_cells_count += 1;
                                    jj += 1;

                                    if (sub_matrix_count == ColSubHead.Count)
                                    {
                                        sub_matrix_count = 0;
                                        jj += 1;
                                    }
                                    if (row_cells_count == columnvalues_count)
                                    {
                                        row_point += 1;
                                        row_cells_count = 0;
                                        jj = 2;
                                    }

                                }

                            }
                            for (int r = bord_value + 3; r < ij - 1; r++)
                            {
                                range = NonDesc.get_Range(NonDesc.Cells[r, 1], NonDesc.Cells[r, x]);
                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            }
                            for (int r = 2; r <= x; r++)
                            {
                                range = NonDesc.get_Range(NonDesc.Cells[bord_value + 1, r], NonDesc.Cells[ij - 1, r]);
                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            }

                            range = NonDesc.get_Range(NonDesc.Cells[bord_value + 1, 1], NonDesc.Cells[bord_value + 1, x]);
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

                            range = NonDesc.get_Range(NonDesc.Cells[bord_value, 1], NonDesc.Cells[ij - 1, x]);
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

                            ij += 1;

                        }

                        else if ((Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 13))
                        {
                            int temp = 0;
                            ref_index = 1;
                            if (count < qcount && NonDesc != null)
                            {
                                if (Ques.QUESTION_TYPE == 1)
                                    NonDesc.Cells[ij++, jj] = "Multiple Choice - Single Select";
                                else if (Ques.QUESTION_TYPE == 2)
                                    NonDesc.Cells[ij++, jj] = "Multiple Choice - Multiple Select";
                                else if (Ques.QUESTION_TYPE == 3)
                                    NonDesc.Cells[ij++, jj] = "Menu - Drop Down";
                                else if (Ques.QUESTION_TYPE == 4)
                                    NonDesc.Cells[ij++, jj] = "Choice - Yes / No";
                                else if (Ques.QUESTION_TYPE == 13)
                                    NonDesc.Cells[ij++, jj] = "Constant Sum";

                                System.Web.UI.WebControls.Label lblNonDescQueslbl = new System.Web.UI.WebControls.Label();
                                lblNonDescQueslbl.Text = Ques.QUESTION_LABEL;
                                string Question = Vcore.clearHTML(Convert.ToString(Ques.QUESTION_LABEL));
                                Question = Question.Replace("\r", " ");
                                Question = Question.Replace("\n", " ");
                                string NonDescQno = "Q" + Ques.QUESTION_SEQ + " : " + Question;
                                int QuesID = Ques.QUESTION_ID;
                                int cellForPic = ij;
                                Excel.Range range = NonDesc.get_Range(NonDesc.Cells[ij, jj], NonDesc.Cells[ij, jj + 2]);
                                range.Merge(true);
                                range.Interior.ColorIndex = 24;
                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                                range.WrapText = true;
                                int bord_value = ij;
                                NonDesc.Cells[ij++, jj] = NonDescQno;
                                range = NonDesc.get_Range(NonDesc.Cells[ij, 1], NonDesc.Cells[ij, 3]);
                                range.Interior.ColorIndex = 44;
                                NonDesc.Cells[ij, 1] = "Answer Options";
                                NonDesc.Cells[ij, 2] = "In %";
                                NonDesc.Cells[ij++, 3] = "In nos.";
                                range.Cells.HorizontalAlignment = Excel.Constants.xlCenter;
                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                                int picminrng = ij;
                                int picMaxRng = 0;
                                int ansOpt = 0;
                                if (ExportMode == "DC" || ExportMode == "Data" || ExportMode == "Charts")
                                {
                                    int totalresponses_count = 0;
                                    if (Ques.QUESTION_TYPE != 13)
                                    {
                                        if (Ques.surveyAnswers != null && Ques.surveyAnswers.Count > 0)
                                        {
                                            foreach (SurveyAnswers ans in Ques.surveyAnswers)
                                            {
                                                int totcnt = 0;
                                                int ansopt = 0;
                                                if (Ques.QUESTION_TYPE == 2)
                                                {
                                                    totcnt = Ques.RESPONSE_COUNT;
                                                }
                                                else
                                                {
                                                    foreach (SurveyAnswers ans1 in Ques.surveyAnswers)
                                                    {
                                                        totcnt += ans1.RESPONSE_COUNT;
                                                        ansopt++;
                                                    }
                                                    totcnt += Ques.OTHER_RESPCOUNT;
                                                }
                                                totalresponses_count = totcnt;
                                                string str = Vcore.clearHTML(Convert.ToString(ans.ANSWER_OPTIONS));
                                                str = str.Replace("\r", " ");
                                                str = str.Replace("\n", " ");
                                                NonDesc.Cells[ij, jj] = str;

                                                int rescnt = ans.RESPONSE_COUNT;
                                                if (!(rescnt == 0))
                                                {
                                                    double percent = 0;
                                                    if (totcnt > 0)
                                                        percent = Math.Round(Convert.ToDouble(rescnt * 100) / totcnt, 1);
                                                    NonDesc.Cells[ij, jj + 1] = percent + "%";
                                                }
                                                else
                                                {
                                                    NonDesc.Cells[ij, jj + 1] = rescnt + "%";
                                                }
                                                Excel.Range range6 = NonDesc.get_Range(NonDesc.Cells[ij, jj + 1], NonDesc.Cells[ij, jj + 1]);
                                                range6.HorizontalAlignment = Excel.Constants.xlRight;
                                                NonDesc.Cells[ij, jj + 2] = rescnt;
                                                Excel.Range range3 = NonDesc.get_Range(NonDesc.Cells[ij, jj + 2], NonDesc.Cells[ij, jj + 2]);
                                                range3.HorizontalAlignment = Excel.Constants.xlRight;
                                                ij++;
                                                temp++;
                                                ansOpt += 1;
                                            }

                                            if (Ques.OTHER_ANS == 1)
                                            {

                                                NonDesc.Cells[ij, jj] = "Others";
                                                int rescntothers = Ques.OTHER_RESPCOUNT;
                                                if (!(rescntothers == 0) && totalresponses_count > 0)
                                                {
                                                    double percent = 0;
                                                    if (totalresponses_count > 0)
                                                        percent = Math.Round(Convert.ToDouble(rescntothers * 100) / totalresponses_count, 1);
                                                    NonDesc.Cells[ij, jj + 1] = percent + "%";

                                                }
                                                else
                                                {
                                                    NonDesc.Cells[ij, jj + 1] = rescntothers + "%";
                                                }
                                                Excel.Range range4 = NonDesc.get_Range(NonDesc.Cells[ij, jj + 1], NonDesc.Cells[ij, jj + 1]);
                                                range4.HorizontalAlignment = Excel.Constants.xlRight;
                                                NonDesc.Cells[ij, jj + 2] = rescntothers;
                                                Excel.Range range5 = NonDesc.get_Range(NonDesc.Cells[ij, jj + 2], NonDesc.Cells[ij, jj + 2]);
                                                range5.HorizontalAlignment = Excel.Constants.xlRight;
                                                ij++;


                                            }
                                            NonDesc.Cells[ij, jj] = "TOTAL :";
                                            NonDesc.Cells[ij++, jj + 2] = totalresponses_count;

                                            for (int r = bord_value + 3; r < ij - 1; r++)
                                            {
                                                range = NonDesc.get_Range(NonDesc.Cells[r, 1], NonDesc.Cells[r, 3]);
                                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                                            }

                                            range = NonDesc.get_Range(NonDesc.Cells[bord_value, 2], NonDesc.Cells[ij - 1, 2]);
                                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                                            range = NonDesc.get_Range(NonDesc.Cells[bord_value, 3], NonDesc.Cells[ij - 1, 3]);
                                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);


                                            range = NonDesc.get_Range(NonDesc.Cells[bord_value, 1], NonDesc.Cells[ij - 1, 3]);
                                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                                        }
                                    }
                                    else if (Ques.QUESTION_TYPE == 13)
                                    {

                                        int ansoptcnt = 0;
                                        int YesResponsecount = 0, NoresponseCount = 0;
                                        int TotalResponsCount = 0;
                                        if (Ques.QUESTION_TYPE == 10)
                                            NonDesc.Cells[ij++, jj] = "Ranking";
                                        ArrayList ans_options = new ArrayList();
                                        ArrayList ans_option_sum = new ArrayList();
                                        if (Ques.surveyAnswers != null)
                                        {
                                            foreach (SurveyAnswers ans in Ques.surveyAnswers)
                                            {
                                                TotalResponsCount += ans.RESPONSE_COUNT;
                                                ans_options.Add(ans.ANSWER_ID);
                                            }
                                            DataSet ds_constantsum = new DataSet();
                                            ds_constantsum = scor.GetConstantSumForReports(Ques.QUESTION_ID, ans_options, Ques.SURVEY_ID);
                                            if (ds_constantsum != null && ds_constantsum.Tables.Count > 0)
                                            {
                                                double ResPercentdividedby = 0;
                                                string RespondentIDS = scor.GetRespondentsForReports(Ques.QUESTION_ID, Ques.SURVEY_ID);
                                                string[] arr = RespondentIDS.Split(',');
                                                double z = Convert.ToDouble(Ques.CHAR_LIMIT);
                                                if (z == 0.0)
                                                {
                                                    z = 100;

                                                }
                                                ResPercentdividedby = Math.Round(Convert.ToDouble(z) / 100, 2);
                                                double totalsum = 0;
                                                int y = 0;
                                                foreach (SurveyAnswers ans in Ques.surveyAnswers)
                                                {
                                                    int temp_total = 0;
                                                    string str1 = Vcore.clearHTML(Convert.ToString(ans.ANSWER_OPTIONS));
                                                    str1 = str1.Replace("\r", " ");
                                                    str1 = str1.Replace("\n", " ");
                                                    for (int m = 0; m < ds_constantsum.Tables[0].Rows.Count; m++)
                                                    {
                                                        if (Convert.ToString(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) != null && ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"] != null && Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) == ans.ANSWER_ID)
                                                        {
                                                            temp_total += Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"]);

                                                        }
                                                    }
                                                    double avg = 0;
                                                    avg = Math.Round(Convert.ToDouble(temp_total) / (arr.Length), 2);
                                                    double ResPercent = 0;
                                                    if (Convert.ToInt32(temp_total) > 0 && ResPercentdividedby > 0 && arr.Length != null && arr.Length > 0)
                                                        ResPercent = Math.Round(Convert.ToDouble(temp_total) / (ResPercentdividedby * arr.Length), 1);
                                                    NonDesc.Cells[ij, jj] = str1;
                                                    NonDesc.Cells[ij, jj + 1] = ResPercent + "%";
                                                    Excel.Range range6 = NonDesc.get_Range(NonDesc.Cells[ij, jj + 1], NonDesc.Cells[ij, jj + 1]);
                                                    range6.HorizontalAlignment = Excel.Constants.xlRight;
                                                    NonDesc.Cells[ij, jj + 2] = avg;
                                                    Excel.Range range3 = NonDesc.get_Range(NonDesc.Cells[ij, jj + 2], NonDesc.Cells[ij, jj + 2]);
                                                    range3.HorizontalAlignment = Excel.Constants.xlRight;
                                                    ij++;
                                                    temp++;
                                                    ansOpt += 1;
                                                    y = y + 1;
                                                }
                                                NonDesc.Cells[ij, jj] = "TOTAL :";
                                                NonDesc.Cells[ij++, jj + 2] = Convert.ToString(z);

                                                for (int r = bord_value + 3; r < ij - 1; r++)
                                                {
                                                    range = NonDesc.get_Range(NonDesc.Cells[r, 1], NonDesc.Cells[r, 3]);
                                                    range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                                                }

                                                range = NonDesc.get_Range(NonDesc.Cells[bord_value, 2], NonDesc.Cells[ij - 1, 2]);
                                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                                                range = NonDesc.get_Range(NonDesc.Cells[bord_value, 3], NonDesc.Cells[ij - 1, 3]);
                                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);


                                                range = NonDesc.get_Range(NonDesc.Cells[bord_value, 1], NonDesc.Cells[ij - 1, 3]);
                                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                                            }

                                        }

                                    }
                                }

                            }
                            ij += 1;
                        }
                        else if (Ques.QUESTION_TYPE == 15)
                        {
                            ref_index = 1;
                            NonDesc.Cells[ij++, jj] = "Ranking";
                            NonDesc.get_Range("A1", "A1").Font.Bold = true;
                            System.Web.UI.WebControls.Label lblNonDescQueslbl = new System.Web.UI.WebControls.Label();
                            lblNonDescQueslbl.Text = Ques.QUESTION_LABEL;
                            string Question = Vcore.clearHTML(Convert.ToString(Ques.QUESTION_LABEL));
                            Question = Question.Replace("\r", " ");
                            Question = Question.Replace("\n", " ");
                            string NonDescQno = "Q" + Ques.QUESTION_SEQ + " : " + Question;
                            int QuesID = Ques.QUESTION_ID;
                            int cellForPic = ij;
                            Excel.Range range = NonDesc.get_Range(NonDesc.Cells[ij, jj], NonDesc.Cells[ij, jj + Ques.surveyAnswers.Count]);
                            range.Merge(true);
                            range.HorizontalAlignment = Excel.Constants.xlCenter;
                            range.Interior.ColorIndex = 24;
                            range.WrapText = true;
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            int bord_value = ij;
                            NonDesc.Cells[ij++, jj] = NonDescQno;
                            NonDesc.Cells[ij, 1] = "Answer Options";
                            range = NonDesc.get_Range(NonDesc.Cells[ij, 1], NonDesc.Cells[ij, 1 + Ques.surveyAnswers.Count]);
                            range.Interior.ColorIndex = 44;
                            range.Cells.HorizontalAlignment = Excel.Constants.xlCenter;
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            int pos1 = ij++;
                            int pos2 = jj;
                            string str1 = "";
                            ArrayList rowHeads = new ArrayList();
                            int TotalResponsCount = 0;
                            if (Ques.surveyAnswers != null && Ques.surveyAnswers.Count > 0)
                            {
                                ArrayList ans_ids = new ArrayList();
                                foreach (SurveyAnswers ans in Ques.surveyAnswers)
                                {
                                    TotalResponsCount += ans.RESPONSE_COUNT;
                                    rowHeads.Add(ans.ANSWER_OPTIONS);
                                    ans_ids.Add(ans.ANSWER_ID);
                                }
                                for (int c = 0; c < Ques.surveyAnswers.Count; c++)
                                {
                                    str1 = Convert.ToString(c + 1);
                                    NonDesc.Cells[pos1, ++jj] = str1;
                                }
                                str1 = "";
                                for (int r = 0; r < rowHeads.Count; r++)
                                {
                                    str1 = Vcore.clearHTML(rowHeads[r].ToString());
                                    str1 = str1.Replace("\r", " ");
                                    str1 = str1.Replace("\n", " ");
                                    NonDesc.Cells[ij++, 1] = str1;
                                }

                                DataSet ds_ranking = new DataSet();
                                ds_ranking = scor.GetRankingConstanSum(Ques.QUESTION_ID, ans_ids, Ques.SURVEY_ID);
                                if (ds_ranking != null && ds_ranking.Tables.Count > 0)
                                {
                                    ArrayList ansrescount = new ArrayList();
                                    ArrayList ansrespercent = new ArrayList();
                                    foreach (SurveyAnswers AnsOp in Ques.surveyAnswers)
                                    {
                                        int ansoptions_count = Ques.surveyAnswers.Count;
                                        int total_count = Convert.ToInt32(ds_ranking.Tables[0].Rows.Count);
                                        for (int c = 0; c < ansoptions_count; c++)
                                        {
                                            int temp_count = 0;
                                            for (int b = 0; b < ds_ranking.Tables[0].Rows.Count; b++)
                                            {
                                                if (Convert.ToInt32(ds_ranking.Tables[0].Rows[b]["ANSWER_ID"]) == AnsOp.ANSWER_ID && Convert.ToString(ds_ranking.Tables[0].Rows[b]["ANSWER_TEXT"]) == Convert.ToString(c + 1))
                                                {
                                                    temp_count += 1;
                                                }
                                            }
                                            double ResPercentrank = 0;
                                            if (temp_count > 0 && AnsOp.RESPONSE_COUNT > 0)
                                                ResPercentrank = Math.Round(Convert.ToDouble(temp_count * 100) / Ques.RESPONSE_COUNT, 1);
                                            ansrescount.Add(temp_count);
                                            ansrespercent.Add(ResPercentrank);

                                        }
                                    }
                                    int colheadscount = 0;
                                    pos1 += 1;
                                    int pos3 = pos2;
                                    for (int cell_values = 0; cell_values < ansrescount.Count; cell_values++)
                                    {
                                        if (colheadscount == rowHeads.Count)
                                        {
                                            pos1 += 1;
                                            pos2 = pos3;
                                            colheadscount = 0;
                                        }

                                        NonDesc.Cells[pos1, ++pos2] = ansrescount[cell_values] + "(" + ansrespercent[cell_values] + "%)";
                                        Excel.Range range1 = NonDesc.get_Range(NonDesc.Cells[pos1, pos2], NonDesc.Cells[ij, jj + max_col_count]);
                                        range1.HorizontalAlignment = Excel.Constants.xlRight;
                                        colheadscount += 1;

                                    }
                                }
                            }
                            for (int r = bord_value + 3; r < ij - 1; r++)
                            {
                                range = NonDesc.get_Range(NonDesc.Cells[r, 1], NonDesc.Cells[r, Ques.surveyAnswers.Count + 1]);
                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            }
                            for (int r = 2; r <= Ques.surveyAnswers.Count + 1; r++)
                            {
                                range = NonDesc.get_Range(NonDesc.Cells[bord_value + 1, r], NonDesc.Cells[ij - 1, r]);
                                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            }
                            range = NonDesc.get_Range(NonDesc.Cells[bord_value, 1], NonDesc.Cells[ij - 1, Ques.surveyAnswers.Count + 1]);
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            ij += 1;
                        }

                    }
                    if (QType_count == 1)
                    {
                        CustomizeNonDescPage(surName);
                        Excel.Range range12 = NonDesc.get_Range(NonDesc.Cells[1, 1], NonDesc.Cells[ij, 1]);
                        range12.ColumnWidth = 50;

                    }
                }
                qcount -= 1;
                //ExportSurveyCore ccore = new ExportSurveyCore();
                DataSet dsesponses = new DataSet();

                int index = 0;
                if (sur.surveyQues != null)
                {
                    foreach (SurveyQuestion Ques in sur.surveyQues)
                    {
                        ArrayList ans_ids = new ArrayList();
                        ArrayList ans_opt = new ArrayList();
                        int ii = 1, ji = 4;

                        if (Ques.QUESTION_TYPE == 5 || Ques.QUESTION_TYPE == 6 || Ques.QUESTION_TYPE == 7 || Ques.QUESTION_TYPE == 8 || Ques.QUESTION_TYPE == 14)
                        {
                            index = index + 1;
                            //dsesponses = ccore.GetTextResponses(Ques.QUESTION_ID, 0);
                            dsesponses = scor.GetTextResponses(Ques.QUESTION_ID, 0, sur.SURVEY_ID);
                        }
                        else if (Ques.QUESTION_TYPE == 9 || Ques.QUESTION_TYPE == 20)
                        {

                            if (Ques.surveyAnswers != null)
                            {
                                foreach (SurveyAnswers surans in Ques.surveyAnswers)
                                {
                                    ans_ids.Add(surans.ANSWER_ID);
                                    ans_opt.Add(surans.ANSWER_OPTIONS);
                                }
                            }
                            dsesponses = scor.GetRankingConstanSumCommentBoxTexResponses(sur.SURVEY_ID, Ques.QUESTION_ID, ans_ids);
                            index = index + 1;
                        }
                        else
                        {

                            continue;
                        }
                        if (QType_count == 1)
                            Sheet[count] = (Excel._Worksheet)oWB.Sheets.Add(NonDesc, Missing.Value, Missing.Value, Missing.Value);
                        else
                            Sheet[count] = (Excel._Worksheet)oWB.Sheets.Add(RespSheet, Missing.Value, Missing.Value, Missing.Value);
                        if (index == 1)
                        {
                            Sheet[count].Name = "Text Response-" + index.ToString();
                        }
                        else
                        {
                            Sheet[count].Name = "TR-" + index.ToString();
                        }
                        Sheet[count].Move(Missing.Value, Sheet[count]);

                        Excel.Range range = Sheet[count].get_Range(Sheet[count].Cells[1, 1], Sheet[count].Cells[1, 6]);
                        range.Merge(true);
                        range.Font.Bold = true;
                        range.Font.Size = 12;
                        range.Interior.ColorIndex = 24;
                        range.HorizontalAlignment = Excel.Constants.xlCenter;
                        range.VerticalAlignment = Excel.Constants.xlBottom;
                        range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                        Sheet[count].Cells[1, 1] = sur.SURVEY_NAME;
                        range = Sheet[count].get_Range(Sheet[count].Cells[3, 1], Sheet[count].Cells[3, 6]);
                        range.Font.Size = 10;
                        range.Merge(true);
                        range.Font.Bold = true;
                        range.VerticalAlignment = Excel.Constants.xlJustify;
                        range.HorizontalAlignment = Excel.Constants.xlLeft;
                        string Resp = "You have  " + Ques.RESPONSE_COUNT + " Responses for this Queston as Listed Below:";
                        Sheet[count].Cells[3, ii] = Resp;
                        ji += 1;

                        System.Web.UI.WebControls.Label lblDescQueslbl = new System.Web.UI.WebControls.Label();
                        lblDescQueslbl.Text = Vcore.clearHTML(Convert.ToString(Ques.QUESTION_LABEL));
                        string Question = Convert.ToString(lblDescQueslbl.Text);
                        Question = Question.Replace("\r", " ");
                        Question = Question.Replace("\n", " ");

                        string DescQno = "Q" + Ques.QUESTION_SEQ + ":  " + Question;
                        if (Ques.QUESTION_TYPE == 20 || Ques.QUESTION_TYPE == 9)
                            range = Sheet[count].get_Range(Sheet[count].Cells[ji, ii], Sheet[count].Cells[ji, 6]);
                        else
                            range = Sheet[count].get_Range(Sheet[count].Cells[ji, ii], Sheet[count].Cells[ji, 6]);
                        range.HorizontalAlignment = Excel.Constants.xlCenter;
                        range.VerticalAlignment = Excel.Constants.xlBottom;
                        range.Merge(true);
                        range.Interior.ColorIndex = 24;
                        range.WrapText = true;
                        range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

                        Sheet[count].Cells[ji++, ii] = DescQno;
                        ji += 1;
                        int resp = 0;
                        if (Ques.QUESTION_TYPE == 5 || Ques.QUESTION_TYPE == 6 || Ques.QUESTION_TYPE == 7 || Ques.QUESTION_TYPE == 8 || Ques.QUESTION_TYPE == 14)
                        {
                            for (int c = 0; c < dsesponses.Tables[0].Rows.Count; c++)
                            {
                                if (dsesponses.Tables[0].Rows[c]["answer_text"] != null)
                                {
                                    string AnsOpt = "";
                                    if (Ques.QUESTION_TYPE != 14)
                                    {
                                        AnsOpt = Convert.ToString(dsesponses.Tables[0].Rows[c]["answer_text"]);
                                    }
                                    else
                                    {

                                        //DateTime dt1 = Convert.ToDateTime(dsesponses.Tables[0].Rows[c]["answer_text"]);
                                        //AnsOpt = Convert.ToDateTime(dt1).ToString("dd-MMM-yyyy") + " " + dt1.ToShortTimeString();
                                        //AnsOpt = Convert.ToString(dsesponses.Tables[0].Rows[c]["answer_text"]);
                                        // DateTime dt1;
                                        //if (!DateTime.TryParse(AnsOpt, out dt1))
                                        //{
                                        //    AnsOpt = Convert.ToString(dsesponses.Tables[0].Rows[c]["answer_text"]);
                                        //}
                                        //else
                                        //{
                                        if (Convert.ToString(dsesponses.Tables[0].Rows[c]["answer_text"]).Trim() != "" && Convert.ToString(dsesponses.Tables[0].Rows[c]["answer_text"]).Trim().Length > 0)
                                        {
                                            DateTime dt = new DateTime();
                                            //DateTime dt1 =  Convert.ToDateTime(dsesponses.Tables[0].Rows[c]["answer_text"]);
                                            if (DateTime.TryParse(Convert.ToString(dsesponses.Tables[0].Rows[c]["answer_text"]), out dt))
                                            {
                                                AnsOpt = Convert.ToDateTime(dt).ToString("dd-MMM-yyyy") + " " + dt.ToShortTimeString();
                                            }

                                        }

                                        //}
                                    }
                                    int x = ji;
                                    Sheet[count].Cells[ji++, 1] = (resp + 1);
                                    Sheet[count].Cells[x, 2] = AnsOpt;
                                    resp += 1;
                                }
                            }
                        }
                        if (Ques.QUESTION_TYPE == 9 || Ques.QUESTION_TYPE == 20)
                        {
                            if (ans_opt != null)
                            {
                                int m = 2;
                                for (int k = 0; k < ans_opt.Count; k++)
                                {
                                    string str = Vcore.clearHTML(Convert.ToString(ans_opt[k]));
                                    str = str.Replace("\r\n", "");
                                    Sheet[count].Cells[ji, m++] = str;

                                }
                                range = Sheet[count].get_Range(Sheet[count].Cells[ji, 1], Sheet[count].Cells[ji, m - 1]);
                                range.Font.Bold = true;
                                range.Font.Size = 10;
                                range.HorizontalAlignment = Excel.Constants.xlCenter;
                            }
                            ji = ji + 1;
                            for (int y = 0; y < dsesponses.Tables[0].Rows.Count; y++)
                            {
                                int n = 1;
                                Sheet[count].Cells[ji, n++] = (y + 1);
                                for (int z = 0; z < ans_opt.Count; z++)
                                {
                                    if (dsesponses.Tables[0].Rows[y][z + 4] != null)
                                    {
                                        //Sheet[count].Cells[ji, n++] = Convert.ToString(dsesponses.Tables[0].Rows[y][z + 4]);
                                        Sheet[count].Cells[ji, n++] = Convert.ToString(dsesponses.Tables[0].Rows[y][z + 5]);

                                    }
                                    else
                                    {
                                        Sheet[count].Cells[ji, n++] = "";

                                    }

                                }
                                ji = ji + 1;
                            }

                            if (Ques.QUESTION_TYPE == 9 || Ques.QUESTION_TYPE == 20)
                            {

                                for (int z = 0; z < Ques.surveyAnswers.Count; z++)
                                {

                                    range = Sheet[count].get_Range(Sheet[count].Cells[1, z + 2], Sheet[count].Cells[ji, z + 2]);
                                    if (Ques.QUESTION_TYPE == 9)
                                    {
                                        range.ColumnWidth = 32;

                                    }
                                    else if (Ques.QUESTION_TYPE == 20)
                                    {
                                        int ch = Ques.surveyAnswers[z].DEMOGRAPIC_BLOCKID;
                                        //string temp_text = "";
                                        if (ch == 1)
                                            range.ColumnWidth = 16;
                                        else if (ch == 2)
                                            range.ColumnWidth = 16;
                                        else if (ch == 3)
                                            range.ColumnWidth = 20;
                                        else if (ch == 4)
                                            range.ColumnWidth = 20;
                                        else if (ch == 5)
                                            range.ColumnWidth = 16;
                                        else if (ch == 6)
                                            range.ColumnWidth = 14;
                                        else if (ch == 7)
                                            range.ColumnWidth = 12;
                                        else if (ch == 8)
                                            range.ColumnWidth = 12;
                                        else if (ch == 9)
                                            range.ColumnWidth = 12;
                                        else if (ch == 10)
                                            range.ColumnWidth = 42;
                                    }
                                    range.WrapText = true;
                                }
                            }
                        }
                        range = Sheet[count].get_Range(Sheet[count].Cells[1, 1], Sheet[count].Cells[ji, 1]);
                        range.ColumnWidth = 7;

                        if (Ques.QUESTION_TYPE != 9 && Ques.QUESTION_TYPE != 20)
                        {
                            range = Sheet[count].get_Range(Sheet[count].Cells[2, 2], Sheet[count].Cells[ji, 2]);
                            if (Ques.QUESTION_TYPE == 5 || Ques.QUESTION_TYPE == 6)
                            {
                                range.ColumnWidth = 100;
                            }
                            else if (Ques.QUESTION_TYPE == 8)
                            {
                                range.ColumnWidth = 42;
                            }
                            else if (Ques.QUESTION_TYPE == 7)
                            {
                                range.ColumnWidth = 32;
                            }
                            else if (Ques.QUESTION_TYPE == 14)
                            {
                                range.ColumnWidth = 32;
                            }
                            range.WrapText = true;
                        }
                        Range excelRange1 = Sheet[count].UsedRange;
                        excelRange1.WrapText = true;

                        Range rg = Sheet[count].UsedRange;
                        rg.Font.Name = "Trebuchet MS";
                        rg.Font.Size = 10;
                        rg.VerticalAlignment = Excel.Constants.xlJustify;
                        rg.Cells.WrapText = true;
                        Excel.Range range8 = Sheet[count].get_Range(Sheet[count].Cells[1, 2], Sheet[count].Cells[ji, 2]);
                        sheetno++;
                        count++;
                        qcount--;

                    }

                }
                if (ref_index > 0 && NonDesc != null)
                    NonDesc.Move(Missing.Value, RespSheet);

                else
                    RespSheet.Move(Sheet[0], Missing.Value);

                RespSheet.Activate();
                try
                {
                    oWB.SaveAs(strXLFileName, Excel.XlFileFormat.xlWorkbookNormal,
                    null, null, false, false, Excel.XlSaveAsAccessMode.xlNoChange, false, false, null, null, null);
                    //Need all following code to clean up and extingush all references!!!
                    oWB.Close(true, Missing.Value, Missing.Value);
                    oXL.Workbooks.Close();
                    oXL.Quit();

                    //System.Runtime.InteropServices.Marshal.ReleaseComObject(oRng);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oXL);
                    //System.Runtime.InteropServices.Marshal.ReleaseComObject(IntroSheet);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oWB);


               


                    //KillProcess();  //killing Excel.exe processes 
                    ////IntroSheet = null;
                    //oWB = null;
                    //oXL = null;
                    //GC.Collect();
                                       
                    System.Diagnostics.Process[] PROC = System.Diagnostics.Process.GetProcessesByName("EXCEL");
                    foreach (System.Diagnostics.Process PK in PROC)
                    {
                        if (PK.MainWindowTitle.Length == 0)
                            PK.Kill();
                    }
                    oWB = null;
                    oXL = null;
                    GC.Collect();

                }
                catch (Exception ex)
                {
                    GenerateLog.GenrateSendmailLog(string.Format("Message: {0} \r\n StackTrace: {1}", ex.Message, ex.StackTrace), @"c:\SurveyExportLog.txt");
                    return "";
                }

                try
                {
                    // SendMailAttachment(strXLFileName, toEmailID);
                }
                catch (Exception ex)
                {
                    GenerateLog.GenrateSendmailLog("Failed Sending Attachments", @"c:\V2\SurveyExportLog.txt");
                }

                return filename;
                //string surname = sur.SURVEY_NAME;
                //string Extn = ".xls";
                //SaveDialogBox(strXLFileName, surname, Extn);
            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(string.Format("Message: {0} \r\n StackTrace: {1}", ex.Message, ex.StackTrace), @"c:\SurveyExportLog.txt");
                return "";
            }

        }

        public void GetIntroPage(string surName, int SurID, Survey sur)
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            try
            {
                RespSheet = (Excel._Worksheet)oWB.Worksheets[1];
                RespSheet.Name = "Survey Profile and Statistics";
                Excel.Range range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[1, 2]);
                range.Merge(true);
                range.Font.Bold = true;
                range.Font.Size = 12;
                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                range.Interior.ColorIndex = 24;
                range.HorizontalAlignment = Excel.Constants.xlCenter;
                range.VerticalAlignment = Excel.Constants.xlBottom;
                range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[20, 1]);
                range.ColumnWidth = 18;
                range.Font.Bold = true;

                range = RespSheet.get_Range(RespSheet.Cells[3, 1], RespSheet.Cells[7, 2]);
                range.Font.Size = 10;

                range = RespSheet.get_Range(RespSheet.Cells[1, 2], RespSheet.Cells[20, 2]);
                range.ColumnWidth = 30;

                range = RespSheet.get_Range(RespSheet.Cells[1, 3], RespSheet.Cells[20, 3]);
                range.ColumnWidth = 18;

                range = RespSheet.get_Range(RespSheet.Cells[1, 4], RespSheet.Cells[20, 4]);
                range.ColumnWidth = 18;

                range = RespSheet.get_Range(RespSheet.Cells[1, 5], RespSheet.Cells[20, 5]);
                range.ColumnWidth = 15;

                range = RespSheet.get_Range(RespSheet.Cells[1, 6], RespSheet.Cells[20, 6]);
                range.ColumnWidth = 15;

                RespSheet.Cells[1, 1] = "Survey Fact Profile";
                RespSheet.Cells[3, 1] = "Survey Title";
                RespSheet.Cells[3, 2] = sur.SURVEY_NAME;
                RespSheet.Cells[4, 1] = "UserName";
                RespSheet.Cells[4, 2] = sur.LOGIN_NAME;
                //RespSheet.Cells[4, 2] = "";
                RespSheet.Cells[5, 1] = "Launch Date";

                RespSheet.Cells[6, 1] = "Date of Export";
                //RespSheet.Cells[6, 2] =  Convert.ToString(Convert.ToDateTime(DateTime.Now).ToString("dd-MMM-yyyy") + " at " + DateTime.Now.ToShortTimeString());
                RespSheet.Cells[6, 2] =  Convert.ToString(Convert.ToDateTime(CommonMethods.GetConvertedDatetime(DateTime.Now, loggedInUserInfo.StandardBIAS, true)).ToString("dd-MMM-yyyy") + " at " + DateTime.Now.ToShortTimeString());
                RespSheet.Cells[11, 1] = "Survey URL";

                range = RespSheet.get_Range(RespSheet.Cells[1, 4], RespSheet.Cells[1, 6]);
                range.Merge(true);
                range.Font.Size = 12;
                range.Interior.ColorIndex = 24;
                range.Font.Bold = true;
                range.HorizontalAlignment = Excel.Constants.xlCenter;
                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                RespSheet.Cells[1, 4] = "Survey Name: " + sur.SURVEY_NAME;

                for (int bord_count = 3; bord_count <= 9; bord_count++)
                {

                    range = RespSheet.get_Range(RespSheet.Cells[bord_count, 4], RespSheet.Cells[bord_count, 6]);
                    range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                }

                range = RespSheet.get_Range(RespSheet.Cells[3, 4], RespSheet.Cells[9, 4]);
                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);


                range = RespSheet.get_Range(RespSheet.Cells[3, 5], RespSheet.Cells[9, 5]);
                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

                range = RespSheet.get_Range(RespSheet.Cells[2, 4], RespSheet.Cells[2, 6]);
                range.Merge(true);
                range.Font.Size = 12;
                range.Interior.ColorIndex = 44;
                range.Font.Bold = true;
                range.HorizontalAlignment = Excel.Constants.xlCenter;
                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                RespSheet.Cells[2, 4] = "Survey Statistics Report";
                range = RespSheet.get_Range(RespSheet.Cells[4, 4], RespSheet.Cells[9, 4]);
                range.Font.Size = 10;
                range.Font.Bold = true;
                range.VerticalAlignment = Excel.Constants.xlJustify;

                range = RespSheet.get_Range(RespSheet.Cells[3, 5], RespSheet.Cells[3, 6]);
                range.Font.Size = 10;
                range.Font.Bold = true;
                range.HorizontalAlignment = Excel.Constants.xlCenter;
                range.VerticalAlignment = Excel.Constants.xlBottom;

                RespSheet.Cells[3, 5] = "Count";
                RespSheet.Cells[3, 6] = "Response in %";
                RespSheet.Cells[4, 4] = "Response Received";
                RespSheet.Cells[5, 4] = "Completes";
                RespSheet.Cells[6, 4] = "Partials";
                RespSheet.Cells[7, 4] = "Bounced";
                RespSheet.Cells[8, 4] = "Screened out";
                RespSheet.Cells[9, 4] = "Over-quota";

                ExportSurveyCore surcore = new ExportSurveyCore();
                DataSet ds = new DataSet();

                ds = surcore.GetSurveyDashBoardDetails(sur.SURVEY_ID, sur.USER_ID);
                string launch_date = "", Completes = "", Partials = "", Bounced = "", Overquota = "", url = "", screened_out = "", resp_received = "";
                string comp_percent = "", part_percent = "", boun_percent = "", visits_perecent = "", overqu_percent = "";
                double resp_received_percent = 0.0, screen_percent = 0.0;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    url = Convert.ToString(ds.Tables[0].Rows[0]["SURVEYLAUNCH_URL"]);
                    if (sur != null && ds.Tables[4].Rows.Count > 0)
                    {
                        int k = ds.Tables[4].Rows.Count;
                        if (sur.STATUS != "Draft" && ds.Tables[4].Rows.Count > 0)
                        {
                            if (ds.Tables[4].Rows[k - 1]["LAUNCHED_ON"] != null)
                            {
                                DateTime dtz = BuildQuetions.GetConvertedDatetime(Convert.ToDateTime(ds.Tables[4].Rows[k - 1]["LAUNCHED_ON"]), sur.STANDARD_BIAS, sur.TIME_ZONE, true);
                                launch_date = Convert.ToDateTime(dtz).ToString("dd-MMM-yyyy") + " at " + dtz.ToShortTimeString();
                            }
                        }

                    }
                    int ovrsent = Convert.ToInt32(ds.Tables[0].Rows[0]["SEND_TO"]) + Convert.ToInt32(ds.Tables[0].Rows[0]["SEND_TO_EXT"]);
                    if (ds.Tables[5].Rows.Count > 0)
                        Completes = Convert.ToString(ds.Tables[5].Rows[0].ItemArray[0].ToString());
                    if (ds.Tables[6].Rows.Count > 0)
                        Partials = Convert.ToString(ds.Tables[6].Rows[0].ItemArray[0].ToString());
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        Bounced = Convert.ToString(ds.Tables[3].Rows[0]["COUNT_BOUNCEDMAILS"]);
                        double bouncedper = 0;
                        if (Convert.ToInt32(ds.Tables[3].Rows[0]["COUNT_BOUNCEDMAILS"]) > 0)
                        {
                            bouncedper += Math.Round(Convert.ToDouble(100 * Convert.ToInt32(ds.Tables[3].Rows[0]["COUNT_BOUNCEDMAILS"])) / Convert.ToInt32(resp_received), 1);

                        }
                        boun_percent = Convert.ToString(bouncedper);
                    }
                    resp_received = Convert.ToString(ds.Tables[0].Rows[0]["TOTAL_RESPONSES"]);
                    screened_out = Convert.ToString(ds.Tables[0].Rows[0]["SCREEN_OUTS"]);
                    if (ds.Tables.Count > 7 && ds.Tables[7].Rows.Count > 0)
                    {
                        Overquota = Convert.ToString(ds.Tables[7].Rows[0].ItemArray[0].ToString());
                        double overquoper = 0;
                        //if (Convert.ToInt32(ds.Tables[7].Rows[0].ItemArray[0].ToString()) > 0 && ovrsent > 0)
                        if (Convert.ToInt32(ds.Tables[7].Rows[0].ItemArray[0].ToString()) > 0)
                        {
                            overquoper = Math.Round(Convert.ToDouble(100 * Convert.ToInt32(ds.Tables[7].Rows[0].ItemArray[0].ToString())) / Convert.ToInt32(resp_received), 1);
                        }
                        overqu_percent = Convert.ToString(overquoper);

                    }
                    // resp_received_percent = Convert.ToString(ds.Tables[0].Rows[0]["RESPONSE_RATE"]);
                    resp_received_percent = Convert.ToInt32(resp_received) > 0 ? Math.Round(Convert.ToDouble(100 * (Convert.ToInt32(resp_received) / Convert.ToInt32(resp_received))), 1) : 0.0;
                    double completepercent = 0;
                    //if (ds.Tables[5].Rows.Count > 0 && ovrsent > 0)
                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        completepercent = Math.Round(Convert.ToDouble(100 * Convert.ToInt32(ds.Tables[5].Rows[0].ItemArray[0])) / Convert.ToInt32(resp_received), 1);
                    }
                    comp_percent = Convert.ToString(completepercent);
                    double partialpercent = 0;
                    // if (ds.Tables[6].Rows.Count > 0 && ovrsent > 0)
                    if (ds.Tables[6].Rows.Count > 0)
                    {
                        partialpercent = Math.Round(Convert.ToDouble(100 * Convert.ToInt32(ds.Tables[6].Rows[0].ItemArray[0])) / Convert.ToInt32(resp_received), 1);
                    }
                    part_percent = Convert.ToString(partialpercent);
                    //if (Convert.ToString(ds.Tables[0].Rows[0]["SCREENOUT_RATE"]) != "0.00")
                    //    screen_percent = Convert.ToString(ds.Tables[0].Rows[0]["SCREENOUT_RATE"]);
                    //else
                    //    screen_percent = "0";
                    screen_percent = Math.Round(Convert.ToDouble(100 * Convert.ToInt32(screened_out)) / Convert.ToInt32(resp_received), 1);

                }
                RespSheet.Cells[4, 5] = resp_received;
                RespSheet.Cells[5, 5] = Completes;
                RespSheet.Cells[6, 5] = Partials;
                RespSheet.Cells[7, 5] = Bounced;
                RespSheet.Cells[8, 5] = screened_out;
                RespSheet.Cells[9, 5] = Overquota;

                RespSheet.Cells[4, 6] = resp_received_percent;
                RespSheet.Cells[5, 6] = comp_percent;
                RespSheet.Cells[6, 6] = part_percent;
                RespSheet.Cells[7, 6] = boun_percent;
                RespSheet.Cells[8, 6] = screen_percent;
                RespSheet.Cells[9, 6] = overqu_percent;

                RespSheet.Cells[5, 2] = launch_date;
                range = RespSheet.get_Range(RespSheet.Cells[7, 2], RespSheet.Cells[7, 2]);
                range.WrapText = true;
                //range.Merge = true;
                range.Font.Size = 8;
                range = RespSheet.get_Range(RespSheet.Cells[11, 2], RespSheet.Cells[11, 6]);
                range.Merge(true);
                range.Font.Size = 10;
                range.Hyperlinks.Add(range, url, url, url, url);

                range = RespSheet.get_Range(RespSheet.Cells[11, 1], RespSheet.Cells[11, 8]);
                range.Font.Size = 10;

                range = RespSheet.get_Range(RespSheet.Cells[4, 5], RespSheet.Cells[9, 5]);
                range.Merge(true);
                range.Font.Size = 10;
                range.HorizontalAlignment = Excel.Constants.xlRight;
                range.VerticalAlignment = Excel.Constants.xlJustify;

                range = RespSheet.get_Range(RespSheet.Cells[4, 6], RespSheet.Cells[9, 6]);
                range.Merge(true);
                range.Font.Size = 10;
                range.HorizontalAlignment = Excel.Constants.xlRight;
                range.VerticalAlignment = Excel.Constants.xlJustify;

                range = RespSheet.get_Range(RespSheet.Cells[1, 4], RespSheet.Cells[9, 6]);
                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);


                range = RespSheet.UsedRange;
                range.Font.Name = "Trebuchet MS";

                if (sur != null && sur.SURVEY_ID > 0 && sur.surveyQues != null)
                {
                    foreach (SurveyQuestion surques in sur.surveyQues)
                    {
                        if (surques.QUESTION_TYPE == 1 || surques.QUESTION_TYPE == 2 || surques.QUESTION_TYPE == 3 || surques.QUESTION_TYPE == 4 || surques.QUESTION_TYPE == 10 || surques.QUESTION_TYPE == 11 || surques.QUESTION_TYPE == 12 || surques.QUESTION_TYPE == 13 || surques.QUESTION_TYPE == 15)
                        {
                            QType_count = 1;
                            break;
                        }
                    }
                }
                if (QType_count == 1)
                {
                    NonDesc = (Excel._Worksheet)oWB.Sheets[2];
                    NonDesc.Name = "Close-ended Responses";
                }
            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(string.Format("Message: {0} \r\n StackTrace: {1}", ex.Message, ex.StackTrace), @"c:\SurveyExportLog.txt");
            }
        }
        public void CustomizeNonDescPage(string surName)
        {
            try
            {
                Range excelRange = NonDesc.UsedRange;
                excelRange.WrapText = true;

                Range rg = NonDesc.UsedRange;
                rg.Font.Name = "Trebuchet MS";
                rg.Font.Size = 10;
                rg.VerticalAlignment = Excel.Constants.xlJustify;
                rg.Cells.ColumnWidth = 11;
                rg.Cells.WrapText = true;
                rg = NonDesc.get_Range(NonDesc.Cells[1, 1], NonDesc.Cells[1, 3]);
                rg.Font.Size = 12;
                rg.Font.Bold = true;
                rg.Interior.ColorIndex = 24;
                rg.Merge(true);
                rg.HorizontalAlignment = Excel.Constants.xlCenter;
                rg.VerticalAlignment = Excel.Constants.xlBottom;
                rg.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                NonDesc.Cells[1, 1] = "Survey Name: " + surName;
            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(string.Format("Message: {0} \r\n StackTrace: {1}", ex.Message, ex.StackTrace), @"c:\SurveyExportLog.txt");
            }
        }
        public void customizeIntroPage()
        {
            IntroSheet.Activate();
            oRng = IntroSheet.get_Range("A1", "Z1");
            oRng.Font.Name = "Trebuchet MS";
            oRng.Font.Bold = true;
            oRng.Font.Size = 15;
            oRng.Interior.ColorIndex = 37;
            //oRng.EntireColumn.AutoFit();
            oXL.Visible = false;
            oXL.UserControl = false;

        }
        public void KillProcess()
        {
            System.Diagnostics.Process[] PROC = System.Diagnostics.Process.GetProcessesByName("EXCEL");
            foreach (System.Diagnostics.Process PK in PROC)
            {//User excel process always have window name
                if (PK.MainWindowTitle.Length == 0)
                    PK.Kill();
            }

        }
        //For Excel Sheet
        #endregion


        //public string CreateRawDataExpot(int SurveyID,int resp_type)
        //{
        //    try
        //    {
        //        string filename = "", filename1 = "";
        //        if (SurveyID != null && SurveyID > 0 && (resp_type == 4 || resp_type == 5))
        //        {
        //            SurveyRespondents Resp;
        //            ExportSurveyCore score = new ExportSurveyCore();
        //            string resp_mode = "";
        //            if (resp_type == 4)
        //                resp_mode = "Complete";
        //            else if (resp_type == 5)
        //                resp_mode = "Partial";

        //            Survey Sur = new Survey();
        //            Sur = score.GetSurveyDet(SurveyID);
        //            Resp = score.GetRawDataExportResponses(SurveyID, resp_mode);
        //            string name_survey = "";
        //            if (Resp != null && Resp.SurveyResp != null && Resp.SurveyResp.Count > 0)
        //            {

        //                List<ArrayList> lst = BuildQuetions.RawDataFormatted(Sur, Resp, Sur.STANDARD_BIAS);
        //                name_survey = Sur.SURVEY_NAME.Replace(" ", "");
        //                name_survey = name_survey.Replace("/", "");
        //                name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
        //                if (resp_type == 4)
        //                    name_survey = "RawDataExportComplete_" + name_survey;
        //                else if (resp_type == 5)
        //                    name_survey = "RawDataExportPartial_" + name_survey;
        //                filename = name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".xls";

        //                //string strXLFileName = "F:\\Knowience\\Source\\OSM\\In\\UserImages\\" + Sur.USER_ID + "\\" + Sur.SURVEY_ID + "\\" + filename;
        //                string strXLFileName = "C:\\Inetpub\\wwwroot\\Insighto\\OSM\\In\\UserImages\\" + Sur.USER_ID + "\\" + Sur.SURVEY_ID + "\\" + filename;
        //                string surName = name_survey;


        //                int qcount = 0;

        //                int jj = 1, ij = 4;


        //                int r_indx = 1;
        //                int c_indx = 1;
        //                int cell_count = 0;

        //                DataSet ds = new DataSet();
        //                ds.Tables.Add();
        //                for (int i = 0; i < lst.Count; i++)
        //                {
        //                    ArrayList arr_rawdata = new ArrayList();
        //                    arr_rawdata = (ArrayList)lst[i];
        //                    cell_count = arr_rawdata.Count;
        //                    if(i>0)
        //                        ds.Tables[0].Rows.Add();
        //                    for (int r = 0; r < arr_rawdata.Count; r++)
        //                    {
        //                        if(i==0)
        //                            ds.Tables[0].Columns.Add(Convert.ToString(arr_rawdata[r]));
        //                        else
        //                            ds.Tables[0].Rows[i - 1][r] = Convert.ToString(arr_rawdata[r]);

        //                    }


        //                }
        //                System.Web.UI.WebControls.DataGrid grid = new System.Web.UI.WebControls.DataGrid();
        //                grid.HeaderStyle.Font.Bold = true;
        //                grid.DataSource = ds;
        //                grid.DataBind();

        //                using (StreamWriter sw = new StreamWriter(strXLFileName))
        //                {
        //                    using (System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(sw))
        //                    {
        //                        grid.RenderControl(hw);

        //                    }
        //                }
        //                oXL = new Microsoft.Office.Interop.Excel.Application();
        //                Excel.Workbook oWB = oXL.Workbooks.Open(strXLFileName, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false,
        //                                             false, 0, true, 1, 0);
        //                //Excel.Workbook oWB = oXL.Workbooks.Open(strXLFileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
        //                //                             Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

        //                Excel.Worksheet RespSheet = (Excel.Worksheet)oWB.ActiveSheet;
        //                RespSheet.Name = "Raw Data Export";

        //                //for (int i = 0; i < lst.Count; i++)
        //                //{
        //                //    ArrayList arr_rawdata = new ArrayList();
        //                //    arr_rawdata = (ArrayList)lst[i];
        //                //    cell_count = arr_rawdata.Count;
        //                //    for (int r = 0; r < arr_rawdata.Count; r++)
        //                //    {

        //                //        RespSheet.Cells[r_indx, c_indx] = arr_rawdata[r];

        //                //        c_indx++;

        //                //    }
        //                //    c_indx = 1;
        //                //    r_indx++;
        //                //}
        //                if (lst.Count != null && lst.Count > 0)
        //                {
        //                    ArrayList arr_rawdata = new ArrayList();
        //                    arr_rawdata = (ArrayList)lst[0];
        //                    cell_count = arr_rawdata.Count;

        //                }
        //                Range range = RespSheet.UsedRange;
        //                Range range1 = RespSheet.UsedRange;
        //                range.Font.Name = "Trebuchet MS";
        //                range.VerticalAlignment = Excel.Constants.xlTop;
        //                range.HorizontalAlignment = Excel.Constants.xlLeft;
        //                range.Cells.ColumnWidth = 20;
        //                range.WrapText = true;


        //                range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[1, cell_count]);
        //                range.Cells.RowHeight = 90;
        //                range.Font.Size = 10;
        //                range.Font.Bold = true;
        //                range.Interior.ColorIndex = 37;
        //                for (int i = 0; i < lst.Count; i++)
        //                {
        //                    range = RespSheet.get_Range(RespSheet.Cells[i + 2, 1], RespSheet.Cells[i + 2, cell_count]);
        //                    range.Cells.RowHeight = 15;
        //                    range.Font.Size = 10;
        //                }

        //                range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[lst.Count, cell_count]);
        //                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

        //                range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[1, cell_count]);
        //                range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

        //                ArrayList bord = BuildQuetions.GetBordercounts(Sur);
        //                r_indx = 1;
        //                c_indx = 0;
        //                int start_index = 1;
        //                for (int b = 0; b < bord.Count; b++)
        //                {

        //                    int no_cells = Convert.ToInt32(bord[b]);
        //                    //c_indx = no_cells + c_indx;
        //                    range = RespSheet.get_Range(RespSheet.Cells[1, start_index], RespSheet.Cells[lst.Count, c_indx + no_cells]);
        //                    range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
        //                    c_indx = no_cells + c_indx;
        //                    start_index = c_indx + 1;
        //                }

        //                r_indx = 1;
        //                c_indx = 0;
        //                start_index = 1;
        //                for (int b = 0; b < bord.Count; b++)
        //                {

        //                    int no_cells = Convert.ToInt32(bord[b]);
        //                    //c_indx = no_cells + c_indx;
        //                    range = RespSheet.get_Range(RespSheet.Cells[1, start_index], RespSheet.Cells[1, c_indx + no_cells]);
        //                    range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
        //                    c_indx = no_cells + c_indx;
        //                    start_index = c_indx + 1;
        //                }

        //                range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[lst.Count, 1]);
        //                range.ColumnWidth = 25;
        //                filename1 = name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".xls";
        //                //string strXLFileName1 = "F:\\Knowience\\Source\\OSM\\In\\UserImages\\" + Sur.USER_ID + "\\" + Sur.SURVEY_ID + "\\" + filename1;
        //                string strXLFileName1 = "C:\\Inetpub\\wwwroot\\Insighto\\OSM\\In\\UserImages\\" + Sur.USER_ID + "\\" + Sur.SURVEY_ID + "\\" + filename;
        //                oWB.SaveAs(strXLFileName1, Excel.XlFileFormat.xlWorkbookNormal,
        //                   null, null, false, false, Excel.XlSaveAsAccessMode.xlNoChange, false, false, null, null, null);
        //                oWB.Close(true, Missing.Value, Missing.Value);
        //                oXL.Workbooks.Close();
        //                oXL.Quit();
        //                System.Runtime.InteropServices.Marshal.ReleaseComObject(oXL);
        //                System.Runtime.InteropServices.Marshal.ReleaseComObject(RespSheet);
        //                System.Runtime.InteropServices.Marshal.ReleaseComObject(oWB);
        //                System.Diagnostics.Process[] PROC = System.Diagnostics.Process.GetProcessesByName("EXCEL");
        //                foreach (System.Diagnostics.Process PK in PROC)
        //                {
        //                    if (PK.MainWindowTitle.Length == 0)
        //                        PK.Kill();
        //                }
        //                oWB = null;
        //                oXL = null;
        //                GC.Collect();

        //                FileInfo TheFile = new FileInfo(strXLFileName);
        //                if (TheFile.Exists)
        //                {
        //                    File.Delete(strXLFileName);
        //                }

        //            }
        //            else
        //            {
        //                filename1 = "No Responses";

        //            }

        //        }

        //        return filename1;


        //    }
        //    catch (Exception ex)
        //    {
        //       GenerateLog.GenrateSendmailLog(string.Format("Message: {0} \r\n StackTrace: {1}", ex.Message, ex.StackTrace), @"c:\SurveyExportLog.txt");
        //       return "";

        //    }

        //}
        public string CreateRawDataExpot(int SurveyID, int resp_type)
        {

            string strXLFileName = "";
            string filename = "";
            string toEmailId = "";
            try
            {
                
                if (SurveyID != null && SurveyID > 0 && (resp_type == 4 || resp_type == 5))
                {
                    SurveyRespondents Resp;
                    ExportSurveyCore score = new ExportSurveyCore();
                    string resp_mode = "";
                    if (resp_type == 4)
                        resp_mode = "Complete";
                    else if (resp_type == 5)
                        resp_mode = "Partial";

                    Survey Sur = new Survey();
                    Sur = score.GetSurveyDet(SurveyID);
                    toEmailId = score.GetEmailId(SurveyID);
                    Resp = score.GetRawDataExportResponses(SurveyID, resp_mode);
                    string name_survey = "";
                    if (Resp != null && Resp.SurveyResp != null && Resp.SurveyResp.Count > 0)
                    {

                        List<ArrayList> lst = BuildQuetions.RawDataFormatted(Sur, Resp, Sur.STANDARD_BIAS);
                        name_survey = Sur.SURVEY_NAME.Replace(" ", "");
                        name_survey = name_survey.Replace("/", "");
                        name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                        if (resp_type == 4)
                            name_survey = "RawDataExportComplete_" + name_survey;
                        else if (resp_type == 5)
                            name_survey = "RawDataExportPartial_" + name_survey;
                        filename = name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".xls";

                        //string strXLFileName = "F:\\Knowience\\Source\\OSM\\In\\UserImages\\" + Sur.USER_ID + "\\" + Sur.SURVEY_ID + "\\" + filename;
                        string sXLFileName = Path + Sur.USER_ID + "\\" + Sur.SURVEY_ID;
                        if (!Directory.Exists(sXLFileName))
                        {
                            Directory.CreateDirectory(sXLFileName);
                        }
                        strXLFileName = Path + Sur.USER_ID + "\\" + Sur.SURVEY_ID + "\\" + filename;

                        string surName = name_survey;
                        GC.Collect();

                        oXL = new Excel.Application();
                        oXL.Visible = false;

                        
                        oWB = (Excel._Workbook)(oXL.Workbooks.Add(Missing.Value));


                        Excel._Worksheet[] Sheet = new Excel._Worksheet[2];
                        int jj = 1, ij = 4;
                        RespSheet = (Excel._Worksheet)oWB.Sheets[1];
                        RespSheet.Name = "Raw Data Export";
                        int r_indx = 1;
                        int c_indx = 1;
                        int cell_count = 0;
                        //for (int i = 0; i < lst.Count; i++)
                        //{
                        //    ArrayList arr_rawdata = new ArrayList();
                        //    arr_rawdata = (ArrayList)lst[i];
                        //    cell_count = arr_rawdata.Count;
                        //    for (int r = 0; r < arr_rawdata.Count; r++)
                        //    {

                        //        RespSheet.Cells[r_indx, c_indx] = arr_rawdata[r];

                        //        c_indx++;
                        //        //cell_count++;
                        //    }
                        //    c_indx = 1;
                        //    r_indx++;
                        //}

                        DataSet ds = new DataSet();
                        System.Data.DataTable dt = new System.Data.DataTable();
                        ds.Tables.Add();
                        for (int i = 0; i < lst.Count; i++)
                        {
                            ArrayList arr_rawdata = new ArrayList();
                            arr_rawdata = (ArrayList)lst[i];
                            cell_count = arr_rawdata.Count;
                            if (i > 0)
                                ds.Tables[0].Rows.Add();
                            for (int r = 0; r < arr_rawdata.Count; r++)
                            {
                                if (i == 0)
                                    // if(!ds.Tables[0].Columns.Contains(Convert.ToString(arr_rawdata[r])))
                                    ds.Tables[0].Columns.Add(Convert.ToString(arr_rawdata[r]));
                                else
                                    ds.Tables[0].Rows[i - 1][r] = Convert.ToString(arr_rawdata[r]);

                            }

                        }
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            dt = ds.Tables[0];
                            object[,] rawData = new object[dt.Rows.Count + 1, dt.Columns.Count];

                            // Copy the column names to the first row of the object array
                            for (int col = 0; col < dt.Columns.Count; col++)
                            {
                                rawData[0, col] = dt.Columns[col].ColumnName;
                            }

                            // Copy the values to the object array
                            for (int col = 0; col < dt.Columns.Count; col++)
                            {
                                for (int row = 0; row < dt.Rows.Count; row++)
                                {
                                    rawData[row + 1, col] = dt.Rows[row].ItemArray[col];
                                }
                            }
                            // Calculate the final column letter
                            string finalColLetter = string.Empty;
                            string colCharset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            int colCharsetLen = colCharset.Length;

                            if (dt.Columns.Count > colCharsetLen)
                            {
                                finalColLetter = colCharset.Substring(
                                    (dt.Columns.Count - 1) / colCharsetLen - 1, 1);
                            }

                            finalColLetter += colCharset.Substring(
                                    (dt.Columns.Count - 1) % colCharsetLen, 1);
                            string excelRange = string.Format("A1:{0}{1}",
                                finalColLetter, dt.Rows.Count + 1);
                            RespSheet.get_Range(excelRange, Type.Missing).Value2 = rawData;
                        }

                        if (lst.Count != null && lst.Count > 0)
                        {
                            ArrayList arr_rawdata = new ArrayList();
                            arr_rawdata = (ArrayList)lst[0];
                            cell_count = arr_rawdata.Count;

                        }
                        Range range = RespSheet.UsedRange;
                        Range range1 = RespSheet.UsedRange;
                        range.Font.Name = "Trebuchet MS";
                        range.VerticalAlignment = Excel.Constants.xlTop;
                        range.HorizontalAlignment = Excel.Constants.xlLeft;
                        range.Cells.ColumnWidth = 20;
                        range.WrapText = true;


                        range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[1, cell_count]);
                        range.Cells.RowHeight = 90;
                        range.Font.Size = 10;
                        range.Font.Bold = true;
                        range.Interior.ColorIndex = 37;
                        for (int i = 0; i < lst.Count; i++)
                        {
                            range = RespSheet.get_Range(RespSheet.Cells[i + 2, 1], RespSheet.Cells[i + 2, cell_count]);
                            range.Cells.RowHeight = 15;
                            range.Font.Size = 10;
                        }

                        range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[lst.Count, cell_count]);
                        range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

                        range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[1, cell_count]);
                        range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

                        ArrayList bord = BuildQuetions.GetBordercounts(Sur);
                        r_indx = 1;
                        c_indx = 0;
                        int start_index = 1;
                        for (int b = 0; b < bord.Count; b++)
                        {
                            int no_cells = Convert.ToInt32(bord[b]);
                            //c_indx = no_cells + c_indx;
                            range = RespSheet.get_Range(RespSheet.Cells[1, start_index], RespSheet.Cells[lst.Count, c_indx + no_cells]);
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            c_indx = no_cells + c_indx;
                            start_index = c_indx + 1;
                        }

                        r_indx = 1;
                        c_indx = 0;
                        start_index = 1;
                        for (int b = 0; b < bord.Count; b++)
                        {
                            int no_cells = Convert.ToInt32(bord[b]);
                            //c_indx = no_cells + c_indx;
                            range = RespSheet.get_Range(RespSheet.Cells[1, start_index], RespSheet.Cells[1, c_indx + no_cells]);
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            c_indx = no_cells + c_indx;
                            start_index = c_indx + 1;
                        }

                        range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[lst.Count, 1]);
                        range.ColumnWidth = 25;
                        

                        //oWB.SaveAs(strXLFileName, Excel.XlFileFormat.xlWorkbookNormal,
                        //   Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, false, false, Missing.Value, Missing.Value, Missing.Value);

                        oWB.SaveCopyAs(strXLFileName); 

                        oWB.Close(true, Missing.Value, Missing.Value);
                        oXL.Workbooks.Close();
                        oXL.Quit();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oXL);
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(RespSheet);
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oWB);
                        System.Diagnostics.Process[] PROC = System.Diagnostics.Process.GetProcessesByName("EXCEL");
                        foreach (System.Diagnostics.Process PK in PROC)
                        {
                            if (PK.MainWindowTitle.Length == 0)
                                PK.Kill();
                        }
                        oWB = null;
                        oXL = null;
                        GC.Collect();
                    }
                    else
                    {
                        filename = "No Responses";

                    }

                }
                try
                {
                    // SendMailAttachment(strXLFileName, toEmailId);
                }
                catch (Exception ex)
                {
                   
                    GenerateLog.GenrateSendmailLog("Failed Sending Attachments to surveyId:" + SurveyID, @"c:\V2\SurveyExportLog.txt");
                }

                return filename;


            }
            catch (Exception ex)
            {
                
                GenerateLog.GenrateSendmailLog(string.Format("Message: {0} \r\n StackTrace: {1}", ex.Message, ex.StackTrace), @"c:\SurveyExportLog.txt");
                //return "";
                return filename;
            }

        }
        public string CreateCrosstabReport(int SurveyID, string qids)
        {
            DataSet ds_reports = new DataSet();
            ExportSurveyCore score = new ExportSurveyCore();
            ValidationCore vcore = new ValidationCore();
            string filename = "";
            string strXLFileName = "";
            string toEmailId = "";
            try
            {
                if (qids != null && qids.Length > 0)
                {
                    string[] QuesIds = qids.Split('_');
                    int tem1 = 0, tem2 = 0;
                    int[] total_count;
                    int[] arr1;
                    int[] arr3;
                    Double[] percent;
                    string[] total_row_percent;
                    Double[] tot_row_percent;

                    if (QuesIds != null && QuesIds.Length > 0)
                    {
                        tem1 = Convert.ToInt32(QuesIds.GetValue(0));
                        tem2 = Convert.ToInt32(QuesIds.GetValue(1));
                    }
                    ds_reports = score.getsurveyanswers(tem1, tem2, SurveyID);
                    string[] ques_array = new string[2];
                    if (ds_reports != null && ds_reports.Tables.Count > 4)
                    {
                        for (int m = 0; m < 2; m++)
                        {
                            if (Convert.ToInt32(ds_reports.Tables[4].Rows[m]["QUESTION_ID"]) == tem1)
                            {
                                ques_array[0] = Convert.ToString(ds_reports.Tables[4].Rows[m]["QUSETION_LABEL"]);
                                if (Convert.ToInt32(ds_reports.Tables[4].Rows[m]["OTHER_ANS"]) == 1)
                                {
                                    int rows_count = ds_reports.Tables[0].Rows.Count;
                                    ds_reports.Tables[0].Rows.Add();
                                    ds_reports.Tables[0].Rows[rows_count]["ANSWER_ID"] = 0;
                                    ds_reports.Tables[0].Rows[rows_count]["ANSWER_OPTIONS"] = "Others";
                                }

                            }
                            else if (Convert.ToInt32(ds_reports.Tables[4].Rows[m]["QUESTION_ID"]) == tem2)
                            {
                                ques_array[1] = Convert.ToString(ds_reports.Tables[4].Rows[m]["QUSETION_LABEL"]);
                                if (Convert.ToInt32(ds_reports.Tables[4].Rows[m]["OTHER_ANS"]) == 1)
                                {
                                    int rows_count = ds_reports.Tables[1].Rows.Count;
                                    ds_reports.Tables[1].Rows.Add();
                                    ds_reports.Tables[1].Rows[rows_count]["ANSWER_ID"] = 0;
                                    ds_reports.Tables[1].Rows[rows_count]["ANSWER_OPTIONS"] = "Others";
                                }

                            }

                        }



                    }
                    int count1 = 0;
                    int ques1_anscount = ds_reports.Tables[0].Rows.Count;
                    int ques2_anscount = ds_reports.Tables[1].Rows.Count;
                    total_count = new int[ques2_anscount];
                    arr1 = new int[ques1_anscount];
                    arr3 = new int[ques1_anscount * ques2_anscount];
                    total_row_percent = new string[ques1_anscount];
                    tot_row_percent = new Double[ques1_anscount]; ;
                    percent = new Double[ques1_anscount * ques2_anscount];
                    int respondents_count = ds_reports.Tables[2].Rows.Count;
                    int[] respondents = new int[respondents_count];
                    int count3 = 0;

                    if (ds_reports != null && ds_reports.Tables.Count > 0)
                    {
                        for (int i = 0; i < ds_reports.Tables[0].Rows.Count; i++)
                        {
                            int temp_id = Convert.ToInt32(ds_reports.Tables[0].Rows[i].ItemArray[0].ToString());

                            count1 = 0;
                            int row_selected_count = 0;
                            for (int k = 0; k < ds_reports.Tables[2].Rows.Count; k++)
                            {
                                if (Convert.ToInt32(ds_reports.Tables[2].Rows[k].ItemArray[2].ToString()) == temp_id)
                                {
                                    for (int a = 0; a < ds_reports.Tables[3].Rows.Count; a++)
                                    {
                                        if (Convert.ToInt32(ds_reports.Tables[3].Rows[a].ItemArray[1].ToString()) == Convert.ToInt32(ds_reports.Tables[2].Rows[k].ItemArray[1].ToString()))
                                        {
                                            respondents[count1] = Convert.ToInt32(ds_reports.Tables[2].Rows[k].ItemArray[1].ToString());
                                            count1 = count1 + 1;
                                            row_selected_count = row_selected_count + 1;
                                            break;

                                        }
                                    }

                                }
                            }
                            int count2 = 0;
                            for (int p = 0; p < ds_reports.Tables[1].Rows.Count; p++)
                            {
                                int temp1_id = Convert.ToInt32(ds_reports.Tables[1].Rows[p].ItemArray[0]);
                                for (int res_i = 0; res_i < respondents.Length; res_i++)
                                {
                                    int temp_resp_id = Convert.ToInt32(respondents[res_i]);

                                    for (int k = 0; k < ds_reports.Tables[3].Rows.Count; k++)
                                    {
                                        if (temp_resp_id == Convert.ToInt32(ds_reports.Tables[3].Rows[k].ItemArray[1].ToString()))
                                        {
                                            if (temp1_id == Convert.ToInt32(ds_reports.Tables[3].Rows[k].ItemArray[2].ToString()))
                                                count2 = count2 + 1;
                                            //break;
                                            //row_selected_count = row_selected_count + 1;
                                        }

                                    }

                                }
                                arr3[count3] = count2;
                                count2 = 0;
                                count3 = count3 + 1;

                            }
                            arr1[i] = row_selected_count;
                            row_selected_count = 0;
                            for (int l = 0; l < respondents.Length; l++)
                            {
                                respondents[l] = 0;
                            }

                        }
                    }
                    int get_count = 0;
                    if (total_count != null)
                    {
                        for (int i = 0; i < total_count.Length; i++)
                        {
                            get_count = i;
                            total_count[i] = 0;
                            if (ques1_anscount != null && arr3 != null)
                            {
                                for (int j = 0; j < ques1_anscount; j++)
                                {
                                    total_count[i] = total_count[i] + arr3[get_count];
                                    get_count = get_count + ques2_anscount;
                                }
                            }

                        }
                    }
                    int length = 0;
                    if (total_count != null)
                    {
                        length = total_count.Length;
                    }
                    get_count = 0;
                    int var1;
                    var1 = 0;
                    if (arr3 != null)
                    {
                        for (int i = 0; i < arr3.Length; i++)
                        {
                            if (total_count[var1] != 0)
                            {

                                Double z = Convert.ToDouble(Convert.ToInt32(arr3[i]) * 100);
                                z = Convert.ToDouble(Convert.ToInt32(z) / Convert.ToInt32(total_count[var1]));
                                percent[i] = Convert.ToDouble(z);
                            }
                            else
                            {
                                percent[i] = 0;
                            }
                            var1 = var1 + 1;

                            if (var1 == length)
                            {

                                var1 = 0;
                            }

                        }
                    }

                    int totalrowvalue = 0;
                    if (arr1 != null)
                    {
                        for (int k = 0; k < arr1.Length; k++)
                        {
                            totalrowvalue = totalrowvalue + arr1[k];
                        }
                        for (int k = 0; k < arr1.Length; k++)
                        {
                            Double y = Convert.ToDouble(Convert.ToInt32(arr1[k]) * 100);
                            if (totalrowvalue != 0)
                            {
                                y = y / totalrowvalue;
                                total_row_percent[k] = "(" + Math.Round(y, 2) + "%)";
                            }
                            else
                            {
                                total_row_percent[k] = "(" + 0 + "%)";
                            }
                            //tot_row_percent[k] =Convert.ToDouble((Convert.ToInt32(arr1[k]) / Convert.ToInt32(totalrowvalue)));
                            //total_row_percent[k] = "(" +Convert.ToDouble(tot_row_percent[k] * 100) + "%)";  
                        }
                    }

                    string name_survey = Convert.ToString(ds_reports.Tables[5].Rows[0]["SURVEY_NAME"]);
                    int USER_ID = Convert.ToInt32(ds_reports.Tables[5].Rows[0]["USERID"]);

                    toEmailId = ds_reports.Tables[5].Rows[0]["Login_Name"].ToString() + "," + name_survey;

                    name_survey = name_survey.Replace(" ", "");
                    name_survey = name_survey.Replace("/", "");
                    name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                    filename = name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".xls";
                    //string strXLFileName = MapPath("~/") + "In\\UserImages\\" + Sur.USER_ID + "\\" + Sur.SURVEY_ID + "\\" + name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".xls";
                    string sXLFileName = Path + USER_ID + "\\" + SurveyID;
                    if (!Directory.Exists(sXLFileName))
                    {
                        Directory.CreateDirectory(sXLFileName);
                    }
                    strXLFileName = Path + USER_ID + "\\" + SurveyID + "\\" + filename;

                    //string strXLFileName = "F:\\Knowience\\Source\\OSM\\In\\UserImages\\" + USER_ID + "\\" + SurveyID + "\\" + filename;
                    //string strXLFileName = "F:\\Knowience\\Source\\OSM\\In\\UserImages\\" + USER_ID + "\\" + SurveyID + "\\" + filename;
                    GC.Collect();
                    oXL = new Excel.Application();
                    oXL.Visible = false;
                    oWB = (Excel._Workbook)(oXL.Workbooks.Add(Missing.Value));
                    // string strCurrentDir = Server.MapPath(".") + "\\";
                    int qcount = 0;
                    Excel._Worksheet[] Sheet = new Excel._Worksheet[2];
                    int jj = 1, ij = 4;
                    RespSheet = (Excel._Worksheet)oWB.Sheets[1];
                    RespSheet.Name = "Cross Tab Report";



                    Excel.Range range = RespSheet.get_Range(RespSheet.Cells[3, 1], RespSheet.Cells[3, 6]);
                    range.Merge(true);
                    range.HorizontalAlignment = Excel.Constants.xlLeft;
                    range.Font.Bold = true;
                    range.Font.Size = 10;
                    range.WrapText = true;
                    RespSheet.Cells[3, 1] = "Cross Tab Report Name: " + name_survey;


                    int r_indx = 0;
                    int c_indx = 0;
                    if (ques2_anscount != null)
                    {
                        range = RespSheet.get_Range(RespSheet.Cells[5, 1], RespSheet.Cells[5, ques2_anscount + 2]);
                        range.Merge(true);
                        range.HorizontalAlignment = Excel.Constants.xlLeft;
                        range.Interior.ColorIndex = 37;
                        range.WrapText = true;
                        range.Font.Bold = true;
                        range.Font.Size = 10;
                        range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                    }

                    if (ques1_anscount != null)
                    {
                        range = RespSheet.get_Range(RespSheet.Cells[6, 3], RespSheet.Cells[6, ques2_anscount + 2]);
                        range.Merge(true);
                        range.HorizontalAlignment = Excel.Constants.xlCenter;
                        range.Interior.ColorIndex = 37;

                        range.Font.Size = 10;
                        range.WrapText = true;
                    }

                    //RespSheet.Cells[5, 1] = Convert.ToString(vcore.clearHTML(ques_array[0].ToString()));
                    //RespSheet.Cells[6, 3] = Convert.ToString(vcore.clearHTML(ques_array[1].ToString()));

                    RespSheet.Cells[5, 1] = Convert.ToString(vcore.clearHTML(ques_array[0].ToString()));
                    RespSheet.Cells[6, 3] = Convert.ToString(vcore.clearHTML(ques_array[1].ToString()));

                    r_indx = 7;
                    c_indx = 3;

                    RespSheet.Cells[6, 2] = "Total";

                    if (ques2_anscount != null && total_count != null && ds_reports != null && ds_reports.Tables.Count > 0)
                    {
                        for (int m = 0; m < ques2_anscount; m++)
                        {
                            r_indx = 7;
                            string temp_cell = "";
                            if (ds_reports.Tables[1].Rows[m].ItemArray[1] != null)
                                temp_cell = ds_reports.Tables[1].Rows[m].ItemArray[1].ToString();
                            //temp_cell = temp_cell.Replace("<p>", "");
                            //temp_cell = temp_cell.Replace("</p>", "");

                            temp_cell = temp_cell.Replace("\r\n", "");
                            temp_cell = temp_cell.Replace("$--$", "\r\n");
                            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                            lbl.Text = Convert.ToString(vcore.clearHTML(temp_cell));
                            lbl.Font.Bold = true;
                            RespSheet.Cells[r_indx, c_indx] = lbl.Text;

                            r_indx++;
                            if (total_count[m] != null)
                                RespSheet.Cells[r_indx, c_indx] = Convert.ToInt32(total_count[m]);
                            c_indx++;

                        }
                    }

                    r_indx = 9;
                    c_indx = 1;

                    if (ques1_anscount != null)
                    {
                        range = RespSheet.get_Range(RespSheet.Cells[9, 1], RespSheet.Cells[9 + ques1_anscount - 1, 1]);
                        range.Merge(true);
                        range.HorizontalAlignment = Excel.Constants.xlLeft;
                        //range.Interior.PatternColorIndex= 
                        range.Interior.ColorIndex = 37;
                        range.WrapText = true;
                        //range.Font.Bold = true;
                    }


                    int row_total_count = 0;
                    if (ques1_anscount != null && arr1 != null && ds_reports != null && ds_reports.Tables.Count > 0)
                    {
                        for (int m = 0; m < ques1_anscount; m++)
                        {
                            c_indx = 1;
                            string temp_cell = "";
                            if (ds_reports.Tables[0].Rows[m].ItemArray[1] != null)
                                temp_cell = ds_reports.Tables[0].Rows[m].ItemArray[1].ToString();                           
                            temp_cell = temp_cell.Replace("\r\n", "");
                            temp_cell = temp_cell.Replace("$--$", "\r\n");
                            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                            lbl.Text = Convert.ToString(vcore.clearHTML(temp_cell));
                            //lbl.Font.Bold = true;
                            RespSheet.Cells[r_indx, c_indx] = lbl.Text;

                            c_indx++;
                            if (arr1[m] != null)
                            {
                                RespSheet.Cells[r_indx, c_indx] = Convert.ToInt32(arr1[m]);
                                row_total_count = row_total_count + Convert.ToInt32(arr1[m]);
                                r_indx++;
                            }

                        }
                    }
                    range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[r_indx, 1]);
                    range.ColumnWidth = 50;
                    r_indx = 9;
                    int r = 0;
                    if (ques1_anscount != null)
                    {
                        for (int m = 0; m < ques1_anscount; m++)
                        {
                            c_indx = 3;
                            if (ques2_anscount != null && arr3 != null && percent != null)
                            {
                                for (int l = 0; l < ques2_anscount; l++)
                                {
                                    RespSheet.Cells[r_indx, c_indx] = Convert.ToInt32(arr3[r]) + "(" + Convert.ToInt32(percent[r]) + "%)";
                                    Excel.Range range5 = RespSheet.get_Range(RespSheet.Cells[r_indx, c_indx], RespSheet.Cells[r_indx, c_indx]);
                                    range5.HorizontalAlignment = Excel.Constants.xlRight;
                                    c_indx++;
                                    r++;

                                }
                            }
                            r_indx++;
                        }
                    }
                    //RespSheet.Cells[15, 3] = row_total_count;
                    RespSheet.Cells[8, 2] = row_total_count;

                    range = RespSheet.get_Range(RespSheet.Cells[6, 1], RespSheet.Cells[8, 1]);
                    range.Merge(true);
                    range.WrapText = true;
                    range.HorizontalAlignment = Excel.Constants.xlCenter;
                    range.Interior.ColorIndex = 2;
                    range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

                    range = RespSheet.get_Range(RespSheet.Cells[6, 2], RespSheet.Cells[8, 2]);
                    range.Merge(true);
                    range.WrapText = true;
                    range.HorizontalAlignment = Excel.Constants.xlCenter;
                    range.Interior.ColorIndex = 2;
                    range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

                    if (ques1_anscount != null && ques2_anscount != null)
                    {


                        r_indx = 6;
                        for (int i = 0; i < ques1_anscount + 3; i++)
                        {
                            range = RespSheet.get_Range(RespSheet.Cells[r_indx, 3], RespSheet.Cells[r_indx, 3 + ques2_anscount - 1]);
                            r_indx++;
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                        }


                        range = RespSheet.get_Range(RespSheet.Cells[6, 2], RespSheet.Cells[ques1_anscount + 8, 2]);
                        range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

                        c_indx = 3;
                        r_indx = 7;

                        for (int i = 0; i < ques2_anscount; i++)
                        {

                            range = RespSheet.get_Range(RespSheet.Cells[r_indx, c_indx], RespSheet.Cells[r_indx + ques1_anscount + 1, c_indx]);
                            c_indx++;
                            range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                            range.ColumnWidth = 11;
                        }

                        range = RespSheet.get_Range(RespSheet.Cells[5, 1], RespSheet.Cells[ques1_anscount + 8, ques2_anscount + 2]);
                        range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);

                        range = RespSheet.get_Range(RespSheet.Cells[6, 1], RespSheet.Cells[8, ques2_anscount + 2]);
                        range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                        range.WrapText = true;
                        range.Font.Bold = true;
                        range.HorizontalAlignment = Excel.Constants.xlCenter;
                        range.VerticalAlignment = Excel.Constants.xlCenter;
                    }



                    string strFile = "Crosstab1" + DateTime.UtcNow.Ticks.ToString() + ".xls";
                    CustomizeCrossTabNonDescPage();


                    range = RespSheet.get_Range(RespSheet.Cells[1, 1], RespSheet.Cells[1, 6]);
                    range.Merge(true);
                    range.HorizontalAlignment = Excel.Constants.xlLeft;
                    range.Font.Bold = true;
                    range.Font.Size = 12;
                    range.Interior.ColorIndex = 37;


                    //rng2.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red)
                    //  range.Interior.Color=System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(197,217,241));          
                    //long lngcolor;
                    //lngcolor = RGB(197, 217, 241);
                    //range.Interior.Color = lngcolor;
                    //range.Interior.Color = System.Drawing.Color.FromArgb(0, 0, 255);

                    range.WrapText = true;
                    range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, 1);
                    RespSheet.Cells[1, 1] = "Survey Name: " + name_survey;

                    oWB.SaveAs(strXLFileName, Excel.XlFileFormat.xlWorkbookNormal,
                     Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, false, false, Missing.Value, Missing.Value, Missing.Value);
                    oWB.Close(true, Missing.Value, Missing.Value);
                    oXL.Workbooks.Close();
                    oXL.Quit();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oXL);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(RespSheet);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oWB);
                    System.Diagnostics.Process[] PROC = System.Diagnostics.Process.GetProcessesByName("EXCEL");
                    foreach (System.Diagnostics.Process PK in PROC)
                    {
                        if (PK.MainWindowTitle.Length == 0)
                            PK.Kill();
                    }
                    oWB = null;
                    oXL = null;
                    GC.Collect();

                }
                try
                {
                    // SendMailAttachment(strXLFileName, toEmailId);
                }
                catch (Exception exe)
                {
                    GenerateLog.GenrateSendmailLog("Failed Sending Attachments to surveyId:" + SurveyID, @"c:\V2\SurveyExportLog.txt");
                }
                return filename;
            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(string.Format("Message: {0} \r\n StackTrace: {1}", ex.Message, ex.StackTrace), @"c:\SurveyExportLog.txt");
                return "";

            }

        }

        public string CreatePPTExport(int SurveyID, int resp_type, string ExportMode)
        {
            string toEmailId = "";
            try
            {
                Survey sur = new Survey();
                ExportSurveyCore core = new ExportSurveyCore();
                //sur = core.GetSurveyDet(SurveyID);
                sur = core.GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                toEmailId = core.GetEmailId(SurveyID);
                string name_survey = sur.SURVEY_NAME.Replace(" ", "");
                name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                name_survey = name_survey.Replace("/", "");
                ValidationCore ValidCore = new ValidationCore();
                //string strXLFileName = "E:\\Knowience\\source\\osm\\In\\UserImages\\" + sur.USER_ID + "\\" + sur.SURVEY_ID + "\\" + filename;
                // string strFileName = MapPath("~/") + "In\\UserImages\\" + SelectedSurvey.USER_ID + "\\" + sur.SURVEY_ID + "\\" + name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".ppt";
                //string strTemplate = MapPath("~/") + "In\\UserImages\\" + SelectedSurvey.USER_ID + "\\" + "sample.ppt";
                //string path_toimageppt = MapPath("~/") + "images\\Pictures\\only_logo10025.jpg";
                string filename = name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".ppt";
                //string sFileName = "C:\\Inetpub\\wwwroot\\Insighto\\OSM\\In\\UserImages\\" + sur.USER_ID + "\\" + sur.SURVEY_ID;
                string sFileName = Path + sur.USER_ID + "\\" + sur.SURVEY_ID;
                if (!Directory.Exists(sFileName))
                {
                    Directory.CreateDirectory(sFileName);
                }
                string strFileName = Path + sur.USER_ID + "\\" + sur.SURVEY_ID + "\\" + filename;
                string strTemplate = Path + sur.USER_ID + "\\" + "sample.ppt";
                string path_toimageppt = ConfigurationManager.AppSettings["PPTImagePath"].ToString();
                if (!File.Exists(strTemplate))
                {
                    File.Create(strTemplate);
                }
                PowerPoint.Application objApp;
                PowerPoint.Presentations objPresSet;
                PowerPoint._Presentation objPres;
                PowerPoint.Slides objSlides;
                PowerPoint._Slide objSlide = null;
                PowerPoint._Master sadasd;
                PowerPoint._Slide objSlide1 = null;
                PowerPoint.TextRange objTextRng, objTextRng1;
                PowerPoint.Table tbl;
                PowerPoint.Shapes objShapes;
                PowerPoint.Shape objShape;
                //Graph.Chart objChart;
                objApp = new PowerPoint.Application();
                //objApp.Visible = MsoTriState.msoCTrue;
                objPresSet = objApp.Presentations;
                objPres = objPresSet.Open(strTemplate, MsoTriState.msoFalse, MsoTriState.msoTrue, MsoTriState.msoFalse);
                objSlides = objPres.Slides;
                int i = 1;
                if (sur.surveyQues != null && sur.surveyQues.Count > 0)
                {
                    sur.surveyQues.Sort();
                    objSlide1 = objSlides.Add(i, PowerPoint.PpSlideLayout.ppLayoutTitleOnly);
                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 400, 400, 200);
                    objSlide1.Shapes[1].TextFrame.TextRange.Text = sur.SURVEY_NAME;
                    objSlide1.Shapes[1].Top = 210;
                    objSlide1.Shapes[1].Left = 30;
                    objSlide1.Shapes[1].Height = 80;
                    objSlide1.Shapes[1].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                    objSlide1.Shapes[1].Line.Weight = 2;
                    objSlide1.Shapes[1].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[1].TextFrame.TextRange.Font.Size = 36;

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 450, 560, 200);
                    objSlide1.Shapes[2].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                    objSlide1.Shapes[2].TextFrame.TextRange.Text = "-A Report";
                    objSlide1.Shapes[2].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[2].TextFrame.TextRange.Font.Color.RGB = System.Drawing.Color.FromArgb(190, 190, 190).ToArgb();
                    objSlide1.Shapes[2].TextFrame.TextRange.Font.Size = 32;
                    objSlide1.Shapes[2].Top = 300;
                    objSlide1.Shapes[2].Left = 140;

                    //objSlide1.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 570, 2, 140, 25);
                    objSlide1.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 660, 2, 50, 25);
                    objSlide1.Shapes[4].ActionSettings[PpMouseActivation.ppMouseClick].Hyperlink.Address = "http://www.insighto.com";
                    i = i + 1;

                    DataSet ds = new DataSet();
                    if (sur != null)
                    {
                        ds = core.GetSurveyDashBoardDetails(sur.SURVEY_ID, sur.USER_ID);
                    }

                    string launch_date = "", Completes = "", Partials = "", Bounced = "", Overquota = "", url = "", screened_out = "", resp_received = "";
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {

                        url = Convert.ToString(ds.Tables[0].Rows[0]["SURVEYLAUNCH_URL"]);
                        if (sur != null && ds.Tables[4].Rows.Count > 0)
                        {
                            int k = ds.Tables[4].Rows.Count;
                            if (sur.STATUS != "Draft" && ds.Tables[4].Rows.Count > 0)
                            {
                                if (ds.Tables[4].Rows[k - 1]["LAUNCHED_ON"] != null)
                                {
                                    DateTime dtz = BuildQuetions.GetConvertedDatetime(Convert.ToDateTime(ds.Tables[4].Rows[k - 1]["LAUNCHED_ON"]), sur.STANDARD_BIAS, sur.TIME_ZONE, true);
                                    launch_date = Convert.ToDateTime(dtz).ToString("dd-MMM-yyyy") + " at " + dtz.ToShortTimeString();
                                }
                            }

                        }
                        int ovrsent = Convert.ToInt32(ds.Tables[0].Rows[0]["SEND_TO"]) + Convert.ToInt32(ds.Tables[0].Rows[0]["SEND_TO_EXT"]);
                        string totalResponses = Convert.ToString(ds.Tables[0].Rows[0]["TOTAL_RESPONSES"]);
                        resp_received = Convert.ToString(ds.Tables[0].Rows[0]["TOTAL_RESPONSES"]);
                        if (ds.Tables[5].Rows.Count > 0)
                            Completes = Convert.ToString(ds.Tables[5].Rows[0].ItemArray[0].ToString());
                        if (ds.Tables[6].Rows.Count > 0)
                            Partials = Convert.ToString(ds.Tables[6].Rows[0].ItemArray[0].ToString());
                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            Bounced = Convert.ToString(ds.Tables[3].Rows[0]["COUNT_BOUNCEDMAILS"]);
                            double bouncedper = 0;
                            if (Convert.ToInt32(ds.Tables[3].Rows[0]["COUNT_BOUNCEDMAILS"]) > 0)
                            {
                                bouncedper += Math.Round(Convert.ToDouble(100 * Convert.ToInt32(ds.Tables[3].Rows[0]["COUNT_BOUNCEDMAILS"])) / Convert.ToInt32(totalResponses), 1);

                            }
                            Bounced += "(" + Convert.ToString(bouncedper) + "%)";
                        }
                        screened_out = Convert.ToString(ds.Tables[0].Rows[0]["SCREEN_OUTS"]);
                        if (ds.Tables.Count > 7 && ds.Tables[7].Rows.Count > 0)
                        {
                            Overquota = Convert.ToString(ds.Tables[7].Rows[0].ItemArray[0].ToString());
                            double overquoper = 0;
                            if (Convert.ToInt32(ds.Tables[7].Rows[0].ItemArray[0].ToString()) > 0)
                            {
                                overquoper = Math.Round(Convert.ToDouble(100 * Convert.ToInt32(ds.Tables[7].Rows[0].ItemArray[0].ToString())) / Convert.ToInt32(totalResponses), 1);
                            }
                            Overquota += "(" + Convert.ToString(overquoper) + "%)";

                        }
                        resp_received += Convert.ToInt32(totalResponses) > 0 ? "(" + Math.Round(Convert.ToDouble(100 * Convert.ToInt32(totalResponses) / Convert.ToInt32(totalResponses)), 1) + "%)" : "0" + "%)";
                        double completepercent = 0;
                        if (ds.Tables[5].Rows.Count > 0)
                        {
                            completepercent += Math.Round(Convert.ToDouble(100 * Convert.ToInt32(ds.Tables[5].Rows[0].ItemArray[0])) / Convert.ToInt32(totalResponses), 1);
                        }
                        Completes += "(" + Convert.ToString(completepercent) + "%)";
                        double partialpercent = 0;
                        if (ds.Tables[6].Rows.Count > 0)
                        {
                            partialpercent = Math.Round(Convert.ToDouble(100 * Convert.ToInt32(ds.Tables[6].Rows[0].ItemArray[0])) / Convert.ToInt32(totalResponses), 1);
                        }
                        Partials += "(" + Convert.ToString(partialpercent) + "%)";
                        double screenedoutpercent = 0;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            screenedoutpercent = Math.Round(Convert.ToDouble(100 * Convert.ToInt32(screened_out)) / Convert.ToInt32(totalResponses), 1);
                        }
                        screened_out += "(" + Convert.ToString(screenedoutpercent) + "%)";


                    }

                    objSlide1 = objSlides.Add(i, PowerPoint.PpSlideLayout.ppLayoutBlank);
                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 38, 40, 653, 80);
                    objSlide1.Shapes[1].TextFrame.TextRange.Text = "Survey Summary";
                    objSlide1.Shapes[1].Top = 30;
                    objSlide1.Shapes[1].Height = 80;
                    objSlide1.Shapes[1].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                    objSlide1.Shapes[1].Line.Weight = 1;
                    objSlide1.Shapes[1].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[1].TextFrame.TextRange.Font.Size = 44;

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 38, 110, 653, 400);
                    objSlide1.Shapes[2].Line.Weight = 1;
                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 150, 250, 30);
                    objSlide1.Shapes[3].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[3].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[3].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[3].TextFrame.TextRange.Text = "Survey Launch Date";

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 300, 150, 380, 30);
                    objSlide1.Shapes[4].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[4].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[4].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[4].TextFrame.TextRange.Text = "  -      " + launch_date;

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 180, 250, 30);
                    objSlide1.Shapes[5].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[5].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[5].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[5].TextFrame.TextRange.Text = "Response Received";

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 300, 180, 380, 30);
                    objSlide1.Shapes[6].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[6].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[6].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[6].TextFrame.TextRange.Text = "  -      " + resp_received;


                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 210, 250, 30);
                    objSlide1.Shapes[7].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[7].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[7].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[7].TextFrame.TextRange.Text = "Completes";

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 300, 210, 380, 30);
                    objSlide1.Shapes[8].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[8].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[8].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[8].TextFrame.TextRange.Text = "  -      " + Completes;

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 240, 250, 30);
                    objSlide1.Shapes[9].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[9].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[9].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[9].TextFrame.TextRange.Text = "Partials";

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 300, 240, 380, 30);
                    objSlide1.Shapes[10].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[10].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[10].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[10].TextFrame.TextRange.Text = "  -      " + Partials;


                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 270, 250, 30);
                    objSlide1.Shapes[11].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[11].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[11].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[11].TextFrame.TextRange.Text = "Bounced";

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 300, 270, 380, 30);
                    objSlide1.Shapes[12].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[12].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[12].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[12].TextFrame.TextRange.Text = "  -      " + Bounced;


                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 300, 250, 30);
                    objSlide1.Shapes[13].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[13].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[13].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[13].TextFrame.TextRange.Text = "Screened Out";

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 300, 300, 380, 30);
                    objSlide1.Shapes[14].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[14].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[14].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[14].TextFrame.TextRange.Text = "  -      " + screened_out;

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 330, 250, 30);
                    objSlide1.Shapes[15].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[15].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[15].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[15].TextFrame.TextRange.Text = "Over quota";

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 300, 330, 380, 30);
                    objSlide1.Shapes[16].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[16].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[16].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[16].TextFrame.TextRange.Text = "  -      " + Overquota;

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 370, 250, 30);
                    objSlide1.Shapes[17].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[17].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[17].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[17].TextFrame.TextRange.Text = "Survey URL";

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 300, 370, 70, 30);
                    objSlide1.Shapes[18].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[18].TextFrame.TextRange.Font.Size = 22;
                    objSlide1.Shapes[18].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[18].TextFrame.TextRange.Text = "  -";

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 360, 370, 330, 30);
                    objSlide1.Shapes[19].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[19].TextFrame.TextRange.Font.Size = 14;
                    objSlide1.Shapes[19].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                    objSlide1.Shapes[19].TextFrame.TextRange.Text = url;
                    objSlide1.Shapes[19].TextFrame.TextRange.ActionSettings[PpMouseActivation.ppMouseClick].Hyperlink.Address = url;

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 1, 2, 200, 200);
                    objSlide1.Shapes[20].TextFrame.TextRange.Text = "Survey title: " + sur.SURVEY_NAME;
                    objSlide1.Shapes[20].Top = 2;
                    objSlide1.Shapes[20].Left = 1;
                    objSlide1.Shapes[20].TextFrame.TextRange.Font.Size = 9;
                    objSlide1.Shapes[20].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[20].TextFrame.TextRange.Font.Bold = MsoTriState.msoTrue;

                    objSlide1.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 660, 2, 50, 25);


                    //objSlide1.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 570, 2, 140, 25);
                    objSlide1.Shapes[21].ActionSettings[PpMouseActivation.ppMouseClick].Hyperlink.Address = "http://www.insighto.com";
                    i = i + 1;
                    foreach (SurveyQuestion ques in sur.surveyQues)
                    {
                        try
                        {
                            string QUES_MODE = GetQuesType(Convert.ToInt32(ques.QUESTION_TYPE));
                            if (QUES_MODE == "Desc")
                            {
                                ViewType viewType1 = BuildQuetions.GetChartViewType(ques.CHART_TYPE, ques.QUESTION_TYPE);
                                if (ques.QUESTION_TYPE != 4 || (viewType1 != ViewType.Pie && viewType1 != ViewType.Doughnut && viewType1 != ViewType.Doughnut3D && viewType1 != ViewType.Pie3D) || ExportMode != "DC")
                                {
                                    objSlide = objSlides.Add(i, PowerPoint.PpSlideLayout.ppLayoutTable);
                                    objSlide.Shapes[1].Width = 653;
                                    objSlide.Shapes[1].Height = 80;
                                    objSlide.Shapes[1].Top = 30;
                                    objTextRng = objSlide.Shapes[1].TextFrame.TextRange;
                                    objSlide.Shapes[1].Line.Weight = 1;
                                    objSlide.Shapes[2].Left = 10;
                                    objSlide.Shapes[2].Line.Weight = 5;

                                    string temp_str = ValidCore.clearHTML(Convert.ToString(ques.QUESTION_LABEL));
                                    temp_str = temp_str.Replace("\r", " ");
                                    temp_str = temp_str.Replace("\n", " ");
                                    objTextRng.Text = temp_str;
                                    string ques_text = ValidCore.clearHTML(Convert.ToString(ques.QUESTION_LABEL));
                                    ques_text = ques_text.Replace("&nbsp;", "");
                                    if (ques_text != null)
                                    {
                                        if (ques_text.Length >= 80 && ques_text.Length < 100)
                                        {
                                            objTextRng.Font.Size = 26;
                                        }
                                        else if (ques_text.Length >= 60 && ques_text.Length < 80)
                                        {
                                            objTextRng.Font.Size = 28;
                                        }
                                        else if (ques_text.Length >= 40 && ques_text.Length < 60)
                                        {
                                            objTextRng.Font.Size = 30;
                                        }
                                        else if (ques_text.Length >= 20 && ques_text.Length < 40)
                                        {
                                            objTextRng.Font.Size = 32;
                                        }
                                        else if (ques_text.Length >= 0 && ques_text.Length < 20)
                                        {
                                            objTextRng.Font.Size = 36;
                                        }
                                        else if (ques_text.Length >= 100 && ques_text.Length < 120)
                                        {
                                            objTextRng.Font.Size = 24;
                                        }
                                        else if (ques_text.Length >= 120 && ques_text.Length < 140)
                                        {
                                            objTextRng.Font.Size = 22;
                                        }
                                        else if (ques_text.Length >= 140 && ques_text.Length < 160)
                                        {
                                            objTextRng.Font.Size = 20;
                                        }
                                        else if (ques_text.Length >= 160 && ques_text.Length < 180)
                                        {
                                            objTextRng.Font.Size = 18;
                                        }
                                        else if (ques_text.Length >= 180 && ques_text.Length < 200)
                                        {
                                            objTextRng.Font.Size = 16;
                                        }
                                        else if (ques_text.Length >= 200 && ques_text.Length < 280)
                                        {
                                            objTextRng.Font.Size = 14;
                                        }
                                        else if (ques_text.Length >= 280)
                                        {
                                            objTextRng.Font.Size = 12;
                                        }
                                    }
                                    objTextRng.Font.Name = "Trebuchet MS";
                                    objTextRng.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignLeft;
                                }
                                if (ExportMode == "DC" || ExportMode == "Data")
                                {
                                    DataSet dsDataReport = new DataSet();
                                    if (ques.QUESTION_TYPE != 12 && ques.QUESTION_TYPE != 15)
                                        dsDataReport = GetDataReport(ques);



                                    if (dsDataReport != null || ques.QUESTION_TYPE == 12 || ques.QUESTION_TYPE == 15)
                                    {
                                        if (!(ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11 || ques.QUESTION_TYPE == 12 || ques.QUESTION_TYPE == 15))
                                        {
                                            if (ques.QUESTION_TYPE != 4 || (viewType1 != ViewType.Pie && viewType1 != ViewType.Doughnut && viewType1 != ViewType.Doughnut3D && viewType1 != ViewType.Pie3D) || ExportMode != "DC")
                                            {
                                                objTextRng = objSlide.Shapes[1].TextFrame.TextRange;
                                                objTextRng.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                                                Microsoft.Office.Interop.PowerPoint.Shape table = (Microsoft.Office.Interop.PowerPoint.Shape)objSlide.Shapes.AddTable(dsDataReport.Tables[0].Rows.Count + 1, 3, 38, 120, 650, 10);
                                                table.Visible = MsoTriState.msoTrue;
                                                table.Table.Cell(1, 1).Shape.TextFrame.TextRange.Text = "Answer Options";
                                                table.Table.Cell(1, 1).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                table.Table.Cell(1, 1).Shape.TextFrame.TextRange.Font.Size = 15;
                                                table.Table.Cell(1, 2).Shape.TextFrame.TextRange.Text = "In Count";
                                                table.Table.Cell(1, 2).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                table.Table.Cell(1, 2).Shape.TextFrame.TextRange.Font.Size = 15;
                                                table.Table.Cell(1, 3).Shape.TextFrame.TextRange.Text = " In %";
                                                table.Table.Cell(1, 3).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                table.Table.Cell(1, 3).Shape.TextFrame.TextRange.Font.Size = 15;

                                                for (int r = 2; r <= dsDataReport.Tables[0].Rows.Count + 1; r++)
                                                {
                                                    for (int col = 1; col <= 3; col++)
                                                    {
                                                        if (col == 1)
                                                        {
                                                            table.Table.Cell(r, col).Shape.TextFrame.TextRange.Text = Convert.ToString(dsDataReport.Tables[0].Rows[r - 2][0]);
                                                            table.Table.Cell(r, col).Shape.TextFrame.TextRange.Font.Size = 15;
                                                            table.Table.Cell(r, col).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                        }
                                                        else if (col == 2)
                                                        {
                                                            table.Table.Cell(r, col).Shape.TextFrame.TextRange.Text = Convert.ToString(dsDataReport.Tables[0].Rows[r - 2][1]);
                                                            table.Table.Cell(r, col).Shape.TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignRight;
                                                            table.Table.Cell(r, col).Shape.TextFrame.TextRange.Font.Size = 15;
                                                            table.Table.Cell(r, col).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                        }
                                                        else if (col == 3)
                                                        {
                                                            table.Table.Cell(r, col).Shape.TextFrame.TextRange.Text = Convert.ToString(dsDataReport.Tables[0].Rows[r - 2][2]);
                                                            table.Table.Cell(r, col).Shape.TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignRight;
                                                            table.Table.Cell(r, col).Shape.TextFrame.TextRange.Font.Size = 15;
                                                            table.Table.Cell(r, col).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                        }
                                                    }
                                                }
                                                table.Table.Columns[1].Width = 450;
                                                table.Table.Columns[2].Width = 100;
                                                table.Table.Columns[3].Width = 100;

                                                objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 1, 2, 220, 200);
                                                objSlide.Shapes[3].TextFrame.TextRange.Text = "Survey title: " + sur.SURVEY_NAME;
                                                objSlide.Shapes[3].Top = 2;
                                                objSlide.Shapes[3].Left = 1;
                                                objSlide.Shapes[3].TextFrame.TextRange.Font.Size = 9;
                                                objSlide.Shapes[3].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                objSlide.Shapes[3].TextFrame.TextRange.Font.Bold = MsoTriState.msoTrue;


                                                objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 660, 2, 50, 25);
                                                //objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 570, 2, 140, 25);
                                                objSlide.Shapes[4].ActionSettings[PpMouseActivation.ppMouseClick].Hyperlink.Address = "http://www.insighto.com";
                                                if (ExportMode == "DC")
                                                {
                                                    objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 560, 140, 200);
                                                    objSlide.Shapes[5].TextFrame.TextRange.Text = "Related graph on next slide";
                                                    objSlide.Shapes[5].Top = 515;
                                                    objSlide.Shapes[5].Left = 560;
                                                    objSlide.Shapes[5].TextFrame.TextRange.Font.Size = 9;
                                                    objSlide.Shapes[5].TextFrame.TextRange.Font.Bold = MsoTriState.msoTrue;
                                                    objSlide.Shapes[5].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                    objSlide.Shapes[5].TextFrame.TextRange.Font.Underline = MsoTriState.msoTrue;
                                                }
                                            }
                                        }
                                        else if (ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11)
                                        {
                                            objTextRng = objSlide.Shapes[1].TextFrame.TextRange;
                                            objTextRng.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                                            int Rowcnt = Convert.ToInt32(dsDataReport.Tables[0].Rows[0][4]);
                                            int ColCnt = Convert.ToInt32(dsDataReport.Tables[0].Rows[0][5]);
                                            int iCnt = 0;
                                            int iDynPercentcnt = Convert.ToInt32(dsDataReport.Tables[0].Rows[0][6]);
                                            Microsoft.Office.Interop.PowerPoint.Shape table = (Microsoft.Office.Interop.PowerPoint.Shape)objSlide.Shapes.AddTable(Rowcnt+1, ColCnt+1, 38, 120, 650, 10);
                                            table.Visible = MsoTriState.msoTrue;
                                            table.Table.Columns[1].Width = 200;
                                            int wid = 80;
                                            int x = 450;
                                            int index = 0;
                                            wid = Convert.ToInt32(x / (ColCnt));
                                            for (int col = 2; col <= ColCnt; col++)
                                            {
                                                table.Table.Columns[col].Width = wid;
                                            }

                                            for (int row = 1; row <= Rowcnt+1; row++)
                                            {
                                                for (int col = 1; col <= ColCnt+1; col++)
                                                {
                                                    if (col == 1 && row == 1)
                                                    {
                                                        table.Table.Cell(1, 1).Shape.TextFrame.TextRange.Text = " ";
                                                        table.Table.Cell(1, 1).Shape.TextFrame.TextRange.Font.Size = 15;
                                                        table.Table.Cell(1, 1).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                    }
                                                    if (row == 1 && col != 1)
                                                    {
                                                        string coltxt = Convert.ToString(dsDataReport.Tables[0].Rows[col - 2][0]);
                                                        table.Table.Cell(1, col).Shape.TextFrame.TextRange.Text = Convert.ToString(dsDataReport.Tables[0].Rows[col - 2][0]);
                                                        table.Table.Cell(1, col).Shape.TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignRight;
                                                        table.Table.Cell(1, col).Shape.TextFrame.TextRange.Font.Size = 15;
                                                        table.Table.Cell(1, col).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                    }
                                                    if (row != 1 && col == 1)
                                                    {
                                                        //string rotxt = Convert.ToString(dsDataReport.Tables[0].Rows[col - 2][1]);
                                                        table.Table.Cell(row, 1).Shape.TextFrame.TextRange.Text = Convert.ToString(dsDataReport.Tables[0].Rows[index][1]);
                                                        table.Table.Cell(row, 1).Shape.TextFrame.TextRange.Font.Size = 15;
                                                        table.Table.Cell(row, 1).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                    }

                                                    if (row != 1 && col != 1)// && iCnt < iDynPercentcnt
                                                    {
                                                        //string datatxt = Convert.ToString(dsDataReport.Tables[0].Rows[iCnt][2]) + "(" + Convert.ToString(dsDataReport.Tables[0].Rows[iCnt][3]) + ")";
                                                        table.Table.Cell(row, col).Shape.TextFrame.TextRange.Text = Convert.ToString(dsDataReport.Tables[0].Rows[iCnt][2]) + "(" + Convert.ToString(dsDataReport.Tables[0].Rows[iCnt][3]) + "%)";
                                                        table.Table.Cell(row, col).Shape.TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignRight;
                                                        table.Table.Cell(row, col).Shape.TextFrame.TextRange.Font.Size = 15;
                                                        table.Table.Cell(row, col).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                        iCnt++;
                                                    }
                                                }
                                                if (row > 1)
                                                {
                                                    index += ColCnt;
                                                }
                                            }

                                            objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 1, 2, 220, 200);
                                            objSlide.Shapes[3].TextFrame.TextRange.Text = "Survey title: " + sur.SURVEY_NAME;
                                            objSlide.Shapes[3].Top = 2;
                                            objSlide.Shapes[3].Left = 1;
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Size = 9;
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Bold = MsoTriState.msoTrue;


                                            objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 660, 2, 50, 25);
                                            //objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 570, 2, 140, 25);
                                            objSlide.Shapes[4].ActionSettings[PpMouseActivation.ppMouseClick].Hyperlink.Address = "http://www.insighto.com";
                                            if (ExportMode == "DC")
                                            {
                                                objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 560, 140, 200);
                                                objSlide.Shapes[5].TextFrame.TextRange.Text = "Related graph on next slide";
                                                objSlide.Shapes[5].Top = 515;
                                                objSlide.Shapes[5].Left = 560;
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Size = 9;
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Bold = MsoTriState.msoTrue;
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Underline = MsoTriState.msoTrue;
                                            }
                                        }
                                        else if (ques.QUESTION_TYPE == 15)
                                        {
                                            int Rowcnt = 0;
                                            int ColCnt = 0;
                                            objTextRng = objSlide.Shapes[1].TextFrame.TextRange;
                                            objTextRng.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                                            ArrayList ans_text = new ArrayList();
                                            ArrayList percent_values = new ArrayList();
                                            if (ques.surveyAnswers != null)
                                            {

                                                Rowcnt = ques.surveyAnswers.Count + 1;
                                                ColCnt = ques.surveyAnswers.Count + 1;
                                                int iCnt = 0;
                                                int iDynPercentcnt = Convert.ToInt32(ques.surveyAnswers.Count * ques.surveyAnswers.Count);
                                                ArrayList ans_ids = new ArrayList();

                                                if (ques.surveyAnswers != null && ques.surveyAnswers.Count > 0)
                                                {
                                                    foreach (SurveyAnswers ans in ques.surveyAnswers)
                                                    {
                                                        ans_ids.Add(ans.ANSWER_ID);
                                                        ans_text.Add(ans.ANSWER_OPTIONS);

                                                    }
                                                }
                                                DataSet ds_ranking = new DataSet();
                                                ds_ranking = core.GetRankingConstanSum(ques.QUESTION_ID, ans_ids, ques.SURVEY_ID);
                                                foreach (SurveyAnswers AnsOp in ques.surveyAnswers)
                                                {
                                                    int ansoptions_count = ques.surveyAnswers.Count;
                                                    int total_count = Convert.ToInt32(ds_ranking.Tables[0].Rows.Count);
                                                    ArrayList values = new ArrayList();
                                                    for (int c = 0; c < ansoptions_count; c++)
                                                    {
                                                        int temp_count = 0;
                                                        for (int b = 0; b < ds_ranking.Tables[0].Rows.Count; b++)
                                                        {

                                                            if (Convert.ToInt32(ds_ranking.Tables[0].Rows[b]["ANSWER_ID"]) == AnsOp.ANSWER_ID && Convert.ToString(ds_ranking.Tables[0].Rows[b]["ANSWER_TEXT"]) == Convert.ToString(c + 1))
                                                            {
                                                                temp_count += 1;
                                                            }

                                                        }
                                                        double ResPercentrank = 0;
                                                        if (temp_count > 0 && AnsOp.RESPONSE_COUNT > 0)
                                                            ResPercentrank = Math.Round(Convert.ToDouble(temp_count * 100) / ques.RESPONSE_COUNT, 2);

                                                        string temp_value = temp_count + "( " + ResPercentrank + "%)";
                                                        percent_values.Add(temp_value);

                                                    }
                                                }
                                                Microsoft.Office.Interop.PowerPoint.Shape table = (Microsoft.Office.Interop.PowerPoint.Shape)objSlide.Shapes.AddTable(Rowcnt, ColCnt, 38, 120, 650, 10);
                                                table.Visible = MsoTriState.msoTrue;
                                                for (int row = 1; row <= Rowcnt; row++)
                                                {
                                                    for (int col = 1; col <= ColCnt; col++)
                                                    {
                                                        if (col == 1 && row == 1)
                                                        {
                                                            table.Table.Cell(1, 1).Shape.TextFrame.TextRange.Text = " ";
                                                            table.Table.Cell(1, 1).Shape.TextFrame.TextRange.Font.Size = 15;
                                                            table.Table.Cell(1, 1).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                        }
                                                        if (row != 1 && col == 1)
                                                        {
                                                            table.Table.Cell(row, 1).Shape.TextFrame.TextRange.Text = ValidCore.clearHTML(Convert.ToString(ans_text[row - 2]));
                                                            table.Table.Cell(row, 1).Shape.TextFrame.TextRange.Font.Size = 15;
                                                            table.Table.Cell(row, 1).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                        }
                                                        if (row == 1 && col != 1)
                                                        {
                                                            table.Table.Cell(1, col).Shape.TextFrame.TextRange.Text = Convert.ToString(col - 1);
                                                            table.Table.Cell(1, col).Shape.TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignRight;
                                                            table.Table.Cell(1, col).Shape.TextFrame.TextRange.Font.Size = 15;
                                                            table.Table.Cell(1, col).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";

                                                        }
                                                        if (row != 1 && col != 1 && iCnt < iDynPercentcnt)
                                                        {
                                                            table.Table.Cell(row, col).Shape.TextFrame.TextRange.Text = Convert.ToString(percent_values[iCnt]);
                                                            table.Table.Cell(row, col).Shape.TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignRight;
                                                            table.Table.Cell(row, col).Shape.TextFrame.TextRange.Font.Size = 15;
                                                            table.Table.Cell(row, col).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                            iCnt++;

                                                        }
                                                    }
                                                }
                                                table.Table.Columns[1].Width = 200;
                                                int wid = 80;
                                                int x = 450;
                                                wid = Convert.ToInt32(x / (ColCnt - 1));
                                                for (int col = 2; col <= ColCnt; col++)
                                                {
                                                    table.Table.Columns[col].Width = wid;
                                                }

                                            }
                                            objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 1, 2, 220, 200);
                                            objSlide.Shapes[3].TextFrame.TextRange.Text = "Survey title: " + sur.SURVEY_NAME;
                                            objSlide.Shapes[3].Top = 2;
                                            objSlide.Shapes[3].Left = 1;
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Size = 9;
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Bold = MsoTriState.msoTrue;


                                            objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 660, 2, 50, 25);
                                            //objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 570, 2, 140, 25);
                                            objSlide.Shapes[4].ActionSettings[PpMouseActivation.ppMouseClick].Hyperlink.Address = "http://www.insighto.com";
                                            if (ExportMode == "DC")
                                            {
                                                objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 560, 140, 200);
                                                objSlide.Shapes[5].TextFrame.TextRange.Text = "Related graph on next slide";
                                                objSlide.Shapes[5].Top = 515;
                                                objSlide.Shapes[5].Left = 560;
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Size = 9;
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Bold = MsoTriState.msoTrue;
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Underline = MsoTriState.msoTrue;
                                            }
                                        }


                                        else if (ques.QUESTION_TYPE == 12)
                                        {
                                            objTextRng = objSlide.Shapes[1].TextFrame.TextRange;
                                            objTextRng.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                                            ValidationCore Vcore = new ValidationCore();
                                            ArrayList colHead = new ArrayList();
                                            ArrayList ColSubHead = new ArrayList();
                                            ArrayList rowHead = new ArrayList();
                                            Hashtable headerList = BuildQuetions.GetAnsItemsList(ques);
                                            if (headerList.Contains("ColHead"))
                                                colHead = (ArrayList)headerList["ColHead"];
                                            if (headerList.Contains("ColSubHead"))
                                                ColSubHead = (ArrayList)headerList["ColSubHead"];
                                            if (headerList.Contains("RowHead"))
                                                rowHead = (ArrayList)headerList["RowHead"];
                                            int count = (colHead.Count * ColSubHead.Count) + 1;
                                            Microsoft.Office.Interop.PowerPoint.Shape table = (Microsoft.Office.Interop.PowerPoint.Shape)objSlide.Shapes.AddTable(rowHead.Count + 2, count, 38, 120, 650, 10);
                                            table.Visible = MsoTriState.msoTrue;
                                            int row_value = 1;
                                            int col_value = 2;
                                            int temp_count = (ColSubHead.Count * colHead.Count) + 1;
                                            temp_count = temp_count - 1;
                                            int k = 0;

                                            for (int main_col_heads = 1; main_col_heads < temp_count + 1; main_col_heads++)
                                            {
                                                if (Convert.ToInt32((main_col_heads - 1) % (ColSubHead.Count)) == 0)
                                                {
                                                    table.Table.Cell(row_value, col_value).Shape.TextFrame.TextRange.Text = Vcore.clearHTML(colHead[k].ToString());
                                                    table.Table.Cell(row_value, col_value).Shape.TextFrame.TextRange.Font.Size = 15;
                                                    table.Table.Cell(row_value, col_value).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                    k = k + 1;
                                                }
                                                else
                                                {
                                                    table.Table.Cell(row_value, col_value).Shape.TextFrame.TextRange.Text = "";
                                                }
                                                col_value += 1;
                                            }
                                            int mer_index = 2;
                                            for (int mer = 0; mer < colHead.Count; mer++)
                                            {
                                                table.Table.Cell(1, mer_index).Merge(table.Table.Cell(1, mer_index + ColSubHead.Count - 1));
                                                table.Table.Cell(1, mer_index).Shape.TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                                                mer_index = mer_index + ColSubHead.Count;
                                            }
                                            row_value = 2;
                                            col_value = 2;
                                            for (int main_col_heads = 1; main_col_heads < colHead.Count + 1; main_col_heads++)
                                            {
                                                for (int sub_heads = 1; sub_heads < ColSubHead.Count + 1; sub_heads++)
                                                {
                                                    table.Table.Cell(row_value, col_value).Shape.TextFrame.TextRange.Text = Vcore.clearHTML(ColSubHead[sub_heads - 1].ToString());
                                                    table.Table.Cell(row_value, col_value).Shape.TextFrame.TextRange.Font.Size = 15;
                                                    table.Table.Cell(row_value, col_value).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                    col_value += 1;
                                                }
                                            }
                                            row_value = 3;
                                            col_value = 1;
                                            for (int row_ans_opts = 1; row_ans_opts < rowHead.Count + 1; row_ans_opts++)
                                            {
                                                table.Table.Cell(row_value, col_value).Shape.TextFrame.TextRange.Text = Vcore.clearHTML(rowHead[row_ans_opts - 1].ToString());
                                                table.Table.Cell(row_value, col_value).Shape.TextFrame.TextRange.Font.Size = 15;
                                                table.Table.Cell(row_value, col_value).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                row_value += 1;

                                            }
                                            row_value = 3;
                                            int TotalResponsCount = 0;
                                            //foreach (SurveyAnswers ans in ques.surveyAnswers)
                                            //{
                                            //    TotalResponsCount += ans.RESPONSE_COUNT;
                                            //}
                                            ArrayList column_values = new ArrayList();
                                          //  int m = 0, b = 0;
                                            int topHeader = 0, col = 0, row = 0;
                                            int totalCount = 0;
                                            foreach (SurveyAnswers ans in ques.surveyAnswers)
                                            {
                                                //m += 1; ;
                                                //b = b + ans.RESPONSE_COUNT;
                                                //if (m == ColSubHead.Count)
                                                //{
                                                //    column_values.Add(b);
                                                //    b = 0;
                                                //    m = 0;
                                                //}
                                                //TotalResponsCount += ans.RESPONSE_COUNT;
                                                string optionValue = string.Format("{0}$--${1}$--${2}", colHead[topHeader], ColSubHead[col], rowHead[row]);
                                                var anserOption = ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS.Replace("\r\n", "").Replace("\r", "").Replace("\n", "") == optionValue.Replace("\r\n", "").Replace("\r", "").Replace("\n", "")).FirstOrDefault();
                                                totalCount += anserOption.RESPONSE_COUNT;
                                                col++;
                                                if (col == ColSubHead.Count)
                                                {
                                                   
                                                    topHeader++;
                                                    col = 0;
                                                    column_values.Add(totalCount);
                                                    totalCount = 0;                                                   
                                                }
                                                if (topHeader == colHead.Count)
                                                {
                                                    row++;
                                                    col = 0;
                                                    topHeader = 0;
                                                }
                                            }
                                            //if (TotalResponsCount > 0)
                                            //{
                                                ArrayList ansrescount = new ArrayList();
                                                ArrayList ansrespercent = new ArrayList();
                                                int row_count = 0, g = 0;
                                                topHeader = 0;
                                                col = 0;
                                                row = 0;
                                                foreach (SurveyAnswers AnsOp in ques.surveyAnswers)
                                                {
                                                    string optionValue = string.Format("{0}$--${1}$--${2}", colHead[topHeader], ColSubHead[col], rowHead[row]);
                                                    var anserOption = ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS.Replace("\r\n", "").Replace("\r", "").Replace("\n", "") == optionValue.Replace("\r\n", "").Replace("\r", "").Replace("\n", "")).FirstOrDefault();
                                                    ansrescount.Add(anserOption.RESPONSE_COUNT);
                                                    double ResPercent = 0;                                                    
                                                    if (column_values[row_count] != null && Convert.ToInt32(column_values[row_count]) > 0)
                                                        ResPercent = Math.Round(Convert.ToDouble(anserOption.RESPONSE_COUNT * 100) / Convert.ToInt32(column_values[row_count]), 2);
                                                    ansrespercent.Add(ResPercent);
                                                    //g = g + 1;
                                                    //if (g == ColSubHead.Count)
                                                    //{
                                                    //    row_count += 1;
                                                    //    g = 0;
                                                    //}
                                                    col++;
                                                    if (col == ColSubHead.Count)
                                                    {
                                                        row_count++;
                                                        topHeader++;
                                                        col = 0;
                                                        // column_values.Add(totalCount);
                                                        //totalCount = 0;
                                                        // row = 0;
                                                    }
                                                    if (topHeader == colHead.Count)
                                                    {
                                                        row++;
                                                        col = 0;
                                                        topHeader = 0;
                                                    }


                                                }
                                                int columnvalues_count = colHead.Count * ColSubHead.Count;
                                                int sub_matrix_count = 1;
                                                int row_cells_count = 1;
                                                int jj = 2;
                                                for (int cell_values = 1; cell_values < ansrescount.Count + 1; cell_values++)
                                                {

                                                    table.Table.Cell(row_value, jj).Shape.TextFrame.TextRange.Text = ansrescount[cell_values - 1] + "(" + ansrespercent[cell_values - 1] + "%)";
                                                    table.Table.Cell(row_value, jj).Shape.TextFrame.TextRange.Font.Size = 15;
                                                    table.Table.Cell(row_value, jj).Shape.TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                    table.Table.Cell(row_value, jj).Shape.TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignRight;
                                                    sub_matrix_count += 1;
                                                    row_cells_count += 1;
                                                    jj += 1;
                                                    if (row_cells_count == columnvalues_count + 1)
                                                    {
                                                        row_value += 1;
                                                        row_cells_count = 1;
                                                        jj = 2;
                                                    }

                                                }

                                           // }
                                            objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 1, 2, 220, 200);
                                            objSlide.Shapes[3].TextFrame.TextRange.Text = "Survey title: " + sur.SURVEY_NAME;
                                            objSlide.Shapes[3].Top = 2;
                                            objSlide.Shapes[3].Left = 1;
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Size = 9;
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Bold = MsoTriState.msoTrue;

                                            objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 660, 2, 50, 25);
                                            //objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 570, 2, 140, 25);
                                            objSlide.Shapes[4].ActionSettings[PpMouseActivation.ppMouseClick].Hyperlink.Address = "http://www.insighto.com";
                                            if (ExportMode == "DC")
                                            {
                                                objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 560, 140, 200);
                                                objSlide.Shapes[5].TextFrame.TextRange.Text = "Related graph on next slide";
                                                objSlide.Shapes[5].Top = 515;
                                                objSlide.Shapes[5].Left = 560;
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Size = 9;
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Bold = MsoTriState.msoTrue;
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                                objSlide.Shapes[5].TextFrame.TextRange.Font.Underline = MsoTriState.msoTrue;
                                            }
                                        }
                                    }
                                    if (ques.QUESTION_TYPE != 4 || (viewType1 != ViewType.Pie && viewType1 != ViewType.Doughnut && viewType1 != ViewType.Doughnut3D && viewType1 != ViewType.Pie3D) || ExportMode != "DC")
                                    {
                                        objSlide.Shapes[1].Line.Weight = 1;
                                        i++;
                                    }
                                }
                                if (ExportMode == "DC" || ExportMode == "Charts")
                                {
                                    if (ques.QUESTION_TYPE != 12)
                                    {
                                        if (ExportMode == "DC")
                                        {
                                            objSlide = objSlides.Add(i++, PowerPoint.PpSlideLayout.ppLayoutTable);
                                        }
                                        string PicPath = SaveChartToImage(ques, sur.USER_ID, resp_type);
                                        objTextRng = objSlide.Shapes[1].TextFrame.TextRange;
                                        objTextRng.Font.Name = "Trebuchet MS";
                                        string titleTxt = "";
                                        if (ques.CHART_TITLE.Trim().Length > 0)
                                            titleTxt = ques.CHART_TITLE;
                                        else
                                        {
                                            titleTxt = ValidCore.clearHTML(Convert.ToString(ques.QUESTION_LABEL));
                                            titleTxt = titleTxt.Replace("&nbsp;", "");
                                        }
                                        titleTxt = titleTxt.Replace("\r", " ");
                                        titleTxt = titleTxt.Replace("\n", " ");

                                        if (titleTxt != null)
                                        {
                                            if (titleTxt.Length >= 80 && titleTxt.Length < 100)
                                            {
                                                objTextRng.Font.Size = 26;
                                            }
                                            else if (titleTxt.Length >= 60 && titleTxt.Length < 80)
                                            {
                                                objTextRng.Font.Size = 28;
                                            }
                                            else if (titleTxt.Length >= 40 && titleTxt.Length < 60)
                                            {
                                                objTextRng.Font.Size = 30;
                                            }
                                            else if (titleTxt.Length >= 20 && titleTxt.Length < 40)
                                            {
                                                objTextRng.Font.Size = 32;
                                            }
                                            else if (titleTxt.Length >= 0 && titleTxt.Length < 20)
                                            {
                                                objTextRng.Font.Size = 36;
                                            }
                                            else if (titleTxt.Length >= 100 && titleTxt.Length < 120)
                                            {
                                                objTextRng.Font.Size = 24;
                                            }
                                            else if (titleTxt.Length >= 120 && titleTxt.Length < 140)
                                            {
                                                objTextRng.Font.Size = 22;
                                            }
                                            else if (titleTxt.Length >= 140 && titleTxt.Length < 160)
                                            {
                                                objTextRng.Font.Size = 20;
                                            }
                                            else if (titleTxt.Length >= 160 && titleTxt.Length < 180)
                                            {
                                                objTextRng.Font.Size = 18;
                                            }
                                            else if (titleTxt.Length >= 180 && titleTxt.Length < 200)
                                            {
                                                objTextRng.Font.Size = 16;
                                            }
                                            else if (titleTxt.Length >= 200 && titleTxt.Length < 280)
                                            {
                                                objTextRng.Font.Size = 14;
                                            }
                                            else if (titleTxt.Length >= 280)
                                            {
                                                objTextRng.Font.Size = 12;
                                            }
                                        }
                                        objTextRng.Text = titleTxt;
                                        objSlide.Shapes[1].Width = 653;
                                        objSlide.Shapes[1].Height = 80;
                                        objSlide.Shapes[1].Top = 30;
                                        objSlide.Shapes[1].Line.Weight = 1;
                                        objSlide.Shapes.AddPicture(PicPath, MsoTriState.msoFalse, MsoTriState.msoTrue, 38, 120, 650, 350);
                                        objSlide.Shapes[2].Line.Weight = 1;

                                        objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 1, 2, 200, 200);
                                        objSlide.Shapes[3].TextFrame.TextRange.Text = "Survey title: " + sur.SURVEY_NAME;
                                        objSlide.Shapes[3].Top = 2;
                                        objSlide.Shapes[3].Left = 1;
                                        objSlide.Shapes[3].TextFrame.TextRange.Font.Size = 9;
                                        objSlide.Shapes[3].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                        objSlide.Shapes[3].TextFrame.TextRange.Font.Bold = MsoTriState.msoTrue;
                                        objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 38, 485, 650, 40);
                                        objTextRng = objSlide.Shapes[4].TextFrame.TextRange;
                                        objTextRng.Text = ques.COMMENTS;
                                        objTextRng.Font.Name = "Trebuchet MS";
                                        objTextRng.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;

                                        if (ques.COMMENTS != null && ques.COMMENTS.Length > 0)
                                        {
                                            if (ques.COMMENTS.Length >= 80 && ques.COMMENTS.Length < 100)
                                            {
                                                objTextRng.Font.Size = 14;
                                            }
                                            else if (ques.COMMENTS.Length >= 60 && ques.COMMENTS.Length < 80)
                                            {
                                                objTextRng.Font.Size = 16;
                                            }
                                            else if (ques.COMMENTS.Length >= 40 && ques.COMMENTS.Length < 60)
                                            {
                                                objTextRng.Font.Size = 18;
                                            }
                                            else if (ques.COMMENTS.Length >= 20 && ques.COMMENTS.Length < 40)
                                            {
                                                objTextRng.Font.Size = 22;
                                            }
                                            else if (ques.COMMENTS.Length >= 0 && ques.COMMENTS.Length < 20)
                                            {
                                                objTextRng.Font.Size = 24;
                                            }
                                            else if (ques.COMMENTS.Length >= 100 && ques.COMMENTS.Length < 120)
                                            {
                                                objTextRng.Font.Size = 14;
                                            }
                                            else if (ques.COMMENTS.Length >= 120 && ques.COMMENTS.Length < 140)
                                            {
                                                objTextRng.Font.Size = 12;
                                            }
                                            else if (ques.COMMENTS.Length >= 140 && ques.COMMENTS.Length < 160)
                                            {
                                                objTextRng.Font.Size = 12;
                                            }
                                            else if (ques.COMMENTS.Length >= 160 && ques.COMMENTS.Length < 180)
                                            {
                                                objTextRng.Font.Size = 12;
                                            }
                                            else if (ques.COMMENTS.Length >= 180 && ques.COMMENTS.Length < 200)
                                            {
                                                objTextRng.Font.Size = 12;
                                            }
                                            else if (ques.COMMENTS.Length >= 200 && ques.COMMENTS.Length < 280)
                                            {
                                                objTextRng.Font.Size = 10;
                                            }
                                            else if (ques.COMMENTS.Length >= 280)
                                            {
                                                objTextRng.Font.Size = 8;
                                            }
                                        }
                                        objSlide.Shapes[4].Height = 40;
                                        if (ques.COMMENTS != null && ques.COMMENTS.Length > 0)
                                        {
                                            objSlide.Shapes[4].Line.Weight = 1;
                                        }
                                        objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 660, 2, 50, 25);
                                        //objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 570, 2, 140, 25);
                                        objSlide.Shapes[5].ActionSettings[PpMouseActivation.ppMouseClick].Hyperlink.Address = "http://www.insighto.com";
                                    }
                                    else if (ques.QUESTION_TYPE == 12)
                                    {
                                        ArrayList colHead = new ArrayList();
                                        Hashtable headerList = BuildQuetions.GetAnsItemsList(ques);
                                        if (headerList.Contains("ColHead"))
                                            colHead = (ArrayList)headerList["ColHead"];
                                        for (int mat_count = 0; mat_count < colHead.Count; mat_count++)
                                        {
                                            if (ExportMode == "DC")
                                            {
                                                objSlide = objSlides.Add(i++, PowerPoint.PpSlideLayout.ppLayoutTable);
                                                objSlide.HeadersFooters.Clear();
                                                objSlide.DisplayMasterShapes = MsoTriState.msoTrue;
                                            }
                                            SMode = mat_count;
                                            string PicPath = SaveChartToImage(ques, sur.USER_ID, resp_type);
                                            objTextRng = objSlide.Shapes[1].TextFrame.TextRange;
                                            objTextRng.Font.Name = "Trebuchet MS";
                                            string titleTxt = "";
                                            if (ques.CHART_TITLE.Trim().Length > 0)
                                                titleTxt = ques.CHART_TITLE;
                                            else
                                            {
                                                titleTxt = ValidCore.clearHTML(ques.QUESTION_LABEL) + "(" + ValidCore.clearHTML(Convert.ToString(colHead[mat_count])) + ")";
                                                titleTxt = titleTxt.Replace("&nbsp;", "");
                                            }
                                            titleTxt = titleTxt.Replace("\r", " ");
                                            titleTxt = titleTxt.Replace("\n", " ");
                                            if (titleTxt != null)
                                            {
                                                if (titleTxt.Length >= 80 && titleTxt.Length < 100)
                                                {
                                                    objTextRng.Font.Size = 26;
                                                }
                                                else if (titleTxt.Length >= 60 && titleTxt.Length < 80)
                                                {
                                                    objTextRng.Font.Size = 28;
                                                }
                                                else if (titleTxt.Length >= 40 && titleTxt.Length < 60)
                                                {
                                                    objTextRng.Font.Size = 30;
                                                }
                                                else if (titleTxt.Length >= 20 && titleTxt.Length < 40)
                                                {
                                                    objTextRng.Font.Size = 32;
                                                }
                                                else if (titleTxt.Length >= 0 && titleTxt.Length < 20)
                                                {
                                                    objTextRng.Font.Size = 36;
                                                }
                                                else if (titleTxt.Length >= 100 && titleTxt.Length < 120)
                                                {
                                                    objTextRng.Font.Size = 24;
                                                }
                                                else if (titleTxt.Length >= 120 && titleTxt.Length < 140)
                                                {
                                                    objTextRng.Font.Size = 22;
                                                }
                                                else if (titleTxt.Length >= 140 && titleTxt.Length < 160)
                                                {
                                                    objTextRng.Font.Size = 20;
                                                }
                                                else if (titleTxt.Length >= 160 && titleTxt.Length < 180)
                                                {
                                                    objTextRng.Font.Size = 18;
                                                }
                                                else if (titleTxt.Length >= 180 && titleTxt.Length < 200)
                                                {
                                                    objTextRng.Font.Size = 16;
                                                }
                                                else if (titleTxt.Length >= 200 && titleTxt.Length < 280)
                                                {
                                                    objTextRng.Font.Size = 14;
                                                }
                                                else if (titleTxt.Length >= 280)
                                                {
                                                    objTextRng.Font.Size = 12;
                                                }
                                            }
                                            titleTxt +=
                                            objTextRng.Text = titleTxt;
                                            objSlide.Shapes[1].Width = 653;
                                            objSlide.Shapes[1].Height = 80;
                                            objSlide.Shapes[1].Top = 30;
                                            objSlide.Shapes[1].Line.Weight = 1;
                                            objSlide.Shapes.AddPicture(PicPath, MsoTriState.msoFalse, MsoTriState.msoTrue, 38, 120, 650, 350);
                                            objSlide.Shapes[2].Line.Weight = 1;

                                            objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 1, 2, 200, 200);
                                            objSlide.Shapes[3].TextFrame.TextRange.Text = "Survey title: " + sur.SURVEY_NAME;
                                            objSlide.Shapes[3].Top = 2;
                                            objSlide.Shapes[3].Left = 1;
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Size = 9;
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                                            objSlide.Shapes[3].TextFrame.TextRange.Font.Bold = MsoTriState.msoTrue;

                                            objSlide.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 38, 485, 650, 40);
                                            objTextRng = objSlide.Shapes[4].TextFrame.TextRange;
                                            objTextRng.Text = ques.COMMENTS;
                                            objTextRng.Font.Name = "Trebuchet MS";
                                            objTextRng.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                                            if (ques.COMMENTS != null && ques.COMMENTS.Length > 0)
                                            {
                                                if (ques.COMMENTS.Length >= 80 && ques.COMMENTS.Length < 100)
                                                {
                                                    objTextRng.Font.Size = 14;
                                                }
                                                else if (ques.COMMENTS.Length >= 60 && ques.COMMENTS.Length < 80)
                                                {
                                                    objTextRng.Font.Size = 16;
                                                }
                                                else if (ques.COMMENTS.Length >= 40 && ques.COMMENTS.Length < 60)
                                                {
                                                    objTextRng.Font.Size = 18;
                                                }
                                                else if (ques.COMMENTS.Length >= 20 && ques.COMMENTS.Length < 40)
                                                {
                                                    objTextRng.Font.Size = 22;
                                                }
                                                else if (ques.COMMENTS.Length >= 0 && ques.COMMENTS.Length < 20)
                                                {
                                                    objTextRng.Font.Size = 24;
                                                }
                                                else if (ques.COMMENTS.Length >= 100 && ques.COMMENTS.Length < 120)
                                                {
                                                    objTextRng.Font.Size = 14;
                                                }
                                                else if (ques.COMMENTS.Length >= 120 && ques.COMMENTS.Length < 140)
                                                {
                                                    objTextRng.Font.Size = 12;
                                                }
                                                else if (ques.COMMENTS.Length >= 140 && ques.COMMENTS.Length < 160)
                                                {
                                                    objTextRng.Font.Size = 12;
                                                }
                                                else if (ques.COMMENTS.Length >= 160 && ques.COMMENTS.Length < 180)
                                                {
                                                    objTextRng.Font.Size = 12;
                                                }
                                                else if (ques.COMMENTS.Length >= 180 && ques.COMMENTS.Length < 200)
                                                {
                                                    objTextRng.Font.Size = 12;
                                                }
                                                else if (ques.COMMENTS.Length >= 200 && ques.COMMENTS.Length < 280)
                                                {
                                                    objTextRng.Font.Size = 10;
                                                }
                                                else if (ques.COMMENTS.Length >= 280)
                                                {
                                                    objTextRng.Font.Size = 8;
                                                }



                                            }
                                            objSlide.Shapes[4].Height = 40;
                                            if (ques.COMMENTS != null && ques.COMMENTS.Length > 0)
                                            {
                                                objSlide.Shapes[4].Line.Weight = 1;
                                            }
                                            objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 660, 2, 50, 25);
                                            //objSlide.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 570, 2, 140, 25);
                                            objSlide.Shapes[5].ActionSettings[PpMouseActivation.ppMouseClick].Hyperlink.Address = "http://www.insighto.com";
                                        }


                                    }
                                    if (ExportMode == "Charts")
                                    { i++; }
                                }
                            }
                        }
                        catch { }
                    }

                    objSlide1 = objSlides.Add(i, PowerPoint.PpSlideLayout.ppLayoutBlank);
                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 350, 600, 200);
                    objSlide1.Shapes[1].TextFrame.TextRange.Text = "Thank You";
                    objSlide1.Shapes[1].Top = 200;
                    objSlide1.Shapes[1].Left = 50;
                    objSlide1.Shapes[1].Height = 80;
                    objSlide1.Shapes[1].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                    objSlide1.Shapes[1].Line.Weight = 2;
                    objSlide1.Shapes[1].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[1].TextFrame.TextRange.Font.Size = 40;

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 520, 600, 80);
                    objSlide1.Shapes[2].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                    objSlide1.Shapes[2].TextFrame.TextRange.Text = sur.SURVEY_NAME.ToUpper();
                    objSlide1.Shapes[2].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[2].Height = 80;
                    objSlide1.Shapes[2].TextFrame.TextRange.Font.Size = 32;
                    objSlide1.Shapes[2].TextFrame.TextRange.Font.Color.RGB = System.Drawing.Color.FromArgb(190, 190, 190).ToArgb();
                    objSlide1.Shapes[2].Top = 260;
                    objSlide1.Shapes[2].Left = 50;

                    objSlide1.Shapes.AddTextbox(MsoTextOrientation.msoTextOrientationHorizontal, 50, 550, 600, 200);
                    objSlide1.Shapes[3].TextFrame.TextRange.ParagraphFormat.Alignment = PpParagraphAlignment.ppAlignCenter;
                    objSlide1.Shapes[3].TextFrame.TextRange.Text = "End of Report";
                    objSlide1.Shapes[3].TextFrame.TextRange.Font.Name = "Trebuchet MS";
                    objSlide1.Shapes[3].TextFrame.TextRange.Font.Size = 32;
                    objSlide1.Shapes[3].TextFrame.TextRange.Font.Color.RGB = System.Drawing.Color.FromArgb(190, 190, 190).ToArgb();
                    objSlide1.Shapes[3].Top = 300;
                    objSlide1.Shapes[3].Left = 50;

                    objSlide1.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 660, 2, 50, 25);
                    //objSlide1.Shapes.AddPicture(path_toimageppt, MsoTriState.msoFalse, MsoTriState.msoTrue, 570, 2, 140, 25);
                    objSlide1.Shapes[4].ActionSettings[PpMouseActivation.ppMouseClick].Hyperlink.Address = "http://www.insighto.com";
                }

                objPres.SaveAs(strFileName, PpSaveAsFileType.ppSaveAsPresentation, Microsoft.Office.Core.MsoTriState.msoFalse);
                objApp.Quit();
                GC.Collect();
                try
                {
                    SendMailAttachment(strFileName, toEmailId);
                }
                catch (Exception ex)
                {
                    GenerateLog.GenrateSendmailLog("Failed Sending Attachments to surveyId: " + SurveyID, @"c:\V2\SurveyExportLog.txt");
                }
                return filename;

            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(string.Format("Message: {0} \r\n StackTrace: {1}", ex.Message, ex.StackTrace), @"c:\SurveyExportLog.txt");
                return "";

            }

        }
        public string CreateWordExport(int SurveyID, int resp_type, string ExportMode)
        {
            string toEmailId = "";
            try
            {
                CCWordApp test;
                Survey sur = new Survey();
                ExportSurveyCore score = new ExportSurveyCore();
                ValidationCore ValidCore = new ValidationCore();
                //sur = score.GetSurveyDet(SurveyID);
                sur = score.GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                toEmailId = score.GetEmailId(SurveyID);
                test = new CCWordApp();
                test.SaveNewDoc();
                test.SetFontSize(12);
                test.SetFont("Trebuchet MS");
                test.SetAlignment("Center");
                System.Web.UI.WebControls.Label surName = new System.Web.UI.WebControls.Label();
                surName.Text = sur.SURVEY_NAME;
                surName.CssClass = "Label";
                string surveyname = "Survey Name :  " + surName.Text;
                test.InsertText(surveyname);
                test.InsertLineBreak(1);
                string username = "User Name :   " + sur.LOGIN_NAME;
                test.InsertText(username);
                test.InsertPagebreak();
                test.SetFontSize(12);
                test.SetFontName("Trebuchet MS");
                test.SetAlignment("Left");
                test.InsertLineBreak(1);

                if (sur.surveyQues != null && sur.surveyQues.Count > 0)
                {
                    sur.surveyQues.Sort();
                    foreach (SurveyQuestion ques in sur.surveyQues)
                    {
                        string QUES_MODE = GetQuesType(Convert.ToInt32(ques.QUESTION_TYPE));
                        if (QUES_MODE == "Desc")
                        {
                            test.SetFontSize(12);
                            System.Web.UI.WebControls.Label Qlabel = new System.Web.UI.WebControls.Label();
                            Qlabel.Text = "Question Text: " + ValidCore.clearHTML(Convert.ToString(ques.QUESTION_LABEL));
                            Qlabel.Font.Bold = true;
                            test.InsertText(Qlabel.Text);
                            test.InsertLineBreak(1);
                            if (ExportMode == "Data" || ExportMode == "DC")
                            {
                                DataSet dsDataReport = new DataSet();
                                if (ques.QUESTION_TYPE != 12)
                                    dsDataReport = GetDataReport(ques);
                                if (dsDataReport.Tables.Count > 0 && dsDataReport.Tables[0].Rows.Count > 0) //condition added by anji
                                {
                                    if (dsDataReport != null || ques.QUESTION_TYPE == 12 || ques.QUESTION_TYPE == 15)
                                    {
                                        test.SetFontSize(10);

                                        if (!(ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11 || ques.QUESTION_TYPE == 12 || ques.QUESTION_TYPE == 15))
                                        {

                                            Word.Table NewTable = test.CreateTable(dsDataReport.Tables[0].Rows.Count, 3);
                                            NewTable.TopPadding = 10;
                                            //table.BackgroundColor = new iTextSharp.text.Color(System.Drawing.Color.AliceBlue);
                                            //NewTable.Shading.BackgroundPatternColor = (WdColor)System.Drawing.Color.AliceBlue;     
                                            for (int i = 1; i <= dsDataReport.Tables[0].Rows.Count; i++)
                                            {
                                                for (int j = 1; j <= 3; j++)
                                                {
                                                    if (j == 1)
                                                    {
                                                        NewTable.Cell(i, j).Range.Text = ValidCore.clearHTML(Convert.ToString(dsDataReport.Tables[0].Rows[i - 1][0]));

                                                    }
                                                    else if (j == 2)
                                                    {
                                                        NewTable.Cell(i, j).Range.Text = Convert.ToString(dsDataReport.Tables[0].Rows[i - 1][1]);
                                                        NewTable.Cell(i, j).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalBottom;
                                                        NewTable.Cell(i, j).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                                                        NewTable.Cell(i, j).HeightRule = WdRowHeightRule.wdRowHeightAuto;
                                                        NewTable.Cell(i, j).Width = 50;
                                                    }
                                                    else if (j == 3)
                                                    {
                                                        NewTable.Cell(i, j).Range.Text = Convert.ToString(dsDataReport.Tables[0].Rows[i - 1][2]);
                                                        NewTable.Cell(i, j).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                                        NewTable.Cell(i, j).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                                                        NewTable.Cell(i, j).HeightRule = WdRowHeightRule.wdRowHeightAuto;
                                                        NewTable.Cell(i, j).Width = 50;
                                                    }
                                                }
                                            }

                                        }

                                        else if (ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11)
                                        {
                                            int Rowcnt = Convert.ToInt32(dsDataReport.Tables[0].Rows[0][4]);
                                            int ColCnt = Convert.ToInt32(dsDataReport.Tables[0].Rows[0][5]);
                                            int iDynPercentcnt = Convert.ToInt32(dsDataReport.Tables[0].Rows[0][6]);
                                            int iCnt = 0;
                                            int index = 0;
                                            //Word.Table NewTable = test.CreateTable(ColCnt, Rowcnt);
                                            Word.Table NewTable = test.CreateTable(Rowcnt+1, ColCnt+1);
                                            NewTable.TopPadding = 5;
                                            object start = System.Type.Missing;
                                            int iCol = NewTable.Columns.Count;
                                            if (iCol < ColCnt)
                                            {
                                                NewTable.Columns.Add(ref start);
                                            }
                                            iCol = NewTable.Columns.Count;
                                            for (int row = 1; row <= Rowcnt+1; row++)
                                            {
                                                for (int col = 1; col <= ColCnt+1; col++)
                                                {
                                                    if (col == 1 && row == 1)
                                                    {
                                                        NewTable.Cell(row, col).Range.Text = " ";
                                                        NewTable.Cell(row, col).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                                        NewTable.Cell(row, col).HeightRule = WdRowHeightRule.wdRowHeightAuto;
                                                        NewTable.Cell(row, col).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                                                    }
                                                    if (col == 1 && row != 1)
                                                    {
                                                       // string rowtxt = Convert.ToString(dsDataReport.Tables[0].Rows[row - 2][1]);
                                                        NewTable.Cell(row, 1).Range.Text = Convert.ToString(dsDataReport.Tables[0].Rows[index][1]);
                                                        NewTable.Cell(row, 1).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                                        NewTable.Cell(row, 1).HeightRule = WdRowHeightRule.wdRowHeightAuto;
                                                        NewTable.Cell(row, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                                                        NewTable.Cell(row, 1).WordWrap = true;
                                                    }
                                                    if (col != 1 && row == 1)
                                                    {
                                                        string coltxt = Convert.ToString(dsDataReport.Tables[0].Rows[col - 2][0]);
                                                        coltxt = ValidCore.clearHTML(coltxt);
                                                        NewTable.Cell(1, col).Range.Text = coltxt.Trim();
                                                        //NewTable.Cell(1, col).Range.Text = Convert.ToString(dsDataReport.Tables[0].Rows[col - 2][0]);
                                                        NewTable.Cell(1, col).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                                        NewTable.Cell(1, col).HeightRule = WdRowHeightRule.wdRowHeightAuto;
                                                        NewTable.Cell(1, col).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                                                    }
                                                    else if (row != 1 && col != 1)// && iCnt < iDynPercentcnt
                                                    {
                                                       // string per = Convert.ToString(dsDataReport.Tables[0].Rows[iCnt][2]) + "(" + Convert.ToString(dsDataReport.Tables[0].Rows[iCnt][3]) + ")";
                                                        NewTable.Cell(row, col).Range.Text = Convert.ToString(dsDataReport.Tables[0].Rows[iCnt][2]) + "(" + Convert.ToString(dsDataReport.Tables[0].Rows[iCnt][3]) + "%)";
                                                        NewTable.Cell(row, col).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                                        NewTable.Cell(row, col).HeightRule = WdRowHeightRule.wdRowHeightAuto;
                                                        NewTable.Cell(row, col).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                                                       iCnt++;

                                                    }

                                                }
                                                if (row > 1)
                                                {
                                                    index += ColCnt;
                                                }

                                            }

                                        }

                                        else if (ques.QUESTION_TYPE == 15)
                                        {
                                            int Rowcnt = 0;
                                            int ColCnt = 0;

                                            ArrayList ans_text = new ArrayList();
                                            ArrayList percent_values = new ArrayList();
                                            if (ques.surveyAnswers != null)
                                            {
                                                Rowcnt = ques.surveyAnswers.Count + 1;
                                                ColCnt = ques.surveyAnswers.Count + 1;
                                                int iCnt = 0;
                                                int iDynPercentcnt = Convert.ToInt32(ques.surveyAnswers.Count * ques.surveyAnswers.Count);

                                                ArrayList ans_ids = new ArrayList();

                                                if (ques.surveyAnswers != null && ques.surveyAnswers.Count > 0)
                                                {
                                                    foreach (SurveyAnswers ans in ques.surveyAnswers)
                                                    {
                                                        ans_ids.Add(ans.ANSWER_ID);
                                                        ans_text.Add(ans.ANSWER_OPTIONS);

                                                    }
                                                }
                                                DataSet ds_ranking = new DataSet();
                                                ds_ranking = score.GetRankingConstanSum(ques.QUESTION_ID, ans_ids, ques.SURVEY_ID);
                                                foreach (SurveyAnswers AnsOp in ques.surveyAnswers)
                                                {
                                                    int ansoptions_count = ques.surveyAnswers.Count;
                                                    int total_count = Convert.ToInt32(ds_ranking.Tables[0].Rows.Count);
                                                    ArrayList values = new ArrayList();
                                                    for (int c = 0; c < ansoptions_count; c++)
                                                    {
                                                        int temp_count = 0;
                                                        for (int b = 0; b < ds_ranking.Tables[0].Rows.Count; b++)
                                                        {

                                                            if (Convert.ToInt32(ds_ranking.Tables[0].Rows[b]["ANSWER_ID"]) == AnsOp.ANSWER_ID && Convert.ToString(ds_ranking.Tables[0].Rows[b]["ANSWER_TEXT"]) == Convert.ToString(c + 1))
                                                            {
                                                                temp_count += 1;
                                                            }

                                                        }
                                                        double ResPercentrank = 0;
                                                        if (temp_count > 0 && AnsOp.RESPONSE_COUNT > 0)
                                                            ResPercentrank = Math.Round(Convert.ToDouble(temp_count * 100) / ques.RESPONSE_COUNT, 2);

                                                        string temp_value = temp_count + "( " + ResPercentrank + "%)";
                                                        percent_values.Add(temp_value);

                                                    }
                                                }
                                                Word.Table NewTable = test.CreateTable(Rowcnt, ColCnt);
                                                NewTable.TopPadding = 5;
                                                object start = System.Type.Missing;
                                                int iCol = NewTable.Columns.Count;
                                                if (iCol < ColCnt)
                                                {
                                                    NewTable.Columns.Add(ref start);
                                                }
                                                iCol = NewTable.Columns.Count;
                                                for (int row = 1; row <= Rowcnt; row++)
                                                {
                                                    for (int col = 1; col <= ColCnt; col++)
                                                    {
                                                        if (col == 1 && row == 1)
                                                        {
                                                            NewTable.Cell(row, col).Range.Text = " ";
                                                        }
                                                        else if (col == 1 && row != 1)
                                                        {
                                                            NewTable.Cell(row, 1).Range.Text = ValidCore.clearHTML(Convert.ToString(ans_text[row - 2]));
                                                        }
                                                        else if (col != 1 && row == 1)
                                                        {
                                                            NewTable.Cell(1, col).Range.Text = Convert.ToString(col - 1);
                                                            NewTable.Cell(1, col).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                                            NewTable.Cell(1, col).HeightRule = WdRowHeightRule.wdRowHeightAuto;
                                                            NewTable.Cell(1, col).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                                                            NewTable.Cell(1, col).Width = 50;
                                                        }
                                                        else if (row != 1 && col != 1 && iCnt < iDynPercentcnt)
                                                        {
                                                            NewTable.Cell(row, col).Range.Text = Convert.ToString(percent_values[iCnt]);
                                                            NewTable.Cell(row, col).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                                            NewTable.Cell(row, col).HeightRule = WdRowHeightRule.wdRowHeightAuto;
                                                            NewTable.Cell(row, col).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                                                            NewTable.Cell(row, col).Width = 50;
                                                            iCnt++;

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //else if (ques.QUESTION_TYPE == 12)
                                        //{
                                        //    ValidationCore Vcore = new ValidationCore();
                                        //    ArrayList colHead = new ArrayList();
                                        //    ArrayList ColSubHead = new ArrayList();
                                        //    ArrayList rowHead = new ArrayList();
                                        //    Hashtable headerList = BuildQuetions.GetAnsItemsList(ques);
                                        //    if (headerList.Contains("ColHead"))
                                        //        colHead = (ArrayList)headerList["ColHead"];
                                        //    if (headerList.Contains("ColSubHead"))
                                        //        ColSubHead = (ArrayList)headerList["ColSubHead"];
                                        //    if (headerList.Contains("RowHead"))
                                        //        rowHead = (ArrayList)headerList["RowHead"];
                                        //    int count = (colHead.Count * ColSubHead.Count) + 1;
                                        //    Word.Table NewTable = test.CreateTable(rowHead.Count + 2, count);
                                        //    NewTable.TopPadding = 2;

                                        //    object start = System.Type.Missing;
                                        //    int iCol = NewTable.Columns.Count;
                                        //    if (iCol < count)
                                        //    {
                                        //        NewTable.Columns.Add(ref start);
                                        //    }

                                        //    int row_value = 1;
                                        //    int col_value = 2;
                                        //    int temp_count = (ColSubHead.Count * colHead.Count) + 1;
                                        //    temp_count = temp_count - 1;
                                        //    int k = 0;

                                        //    for (int main_col_heads = 1; main_col_heads < temp_count + 1; main_col_heads++)
                                        //    {
                                        //        if (Convert.ToInt32((main_col_heads - 1) % (ColSubHead.Count)) == 0)
                                        //        {
                                        //            NewTable.Cell(row_value, col_value).HeightRule = WdRowHeightRule.wdRowHeightAuto;
                                        //            NewTable.Cell(row_value, col_value).Range.Text = Vcore.clearHTML(colHead[k].ToString());
                                        //            NewTable.Cell(row_value, col_value).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                        //            NewTable.Cell(row_value, col_value).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                                        //            k = k + 1;
                                        //        }
                                        //        else
                                        //        {
                                        //            NewTable.Cell(row_value, col_value).Range.Text = " ";
                                        //            NewTable.Cell(row_value, col_value).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                        //        }
                                        //        col_value += 1;
                                        //    }
                                        //    int mer_index = 2;
                                        //    for (int mer = 0; mer < colHead.Count; mer++)
                                        //    {

                                        //        NewTable.Cell(1, mer_index).Merge(NewTable.Cell(1, mer_index + ColSubHead.Count - 1));

                                        //        NewTable.Cell(1, mer_index).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalBottom;
                                        //        NewTable.Cell(1, mer_index).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                                        //        mer_index = mer_index + 1;
                                        //    }
                                        //    NewTable.Rows[1].Alignment = WdRowAlignment.wdAlignRowCenter;
                                        //    row_value = 2;
                                        //    col_value = 2;
                                        //    for (int main_col_heads = 1; main_col_heads < colHead.Count + 1; main_col_heads++)
                                        //    {
                                        //        for (int sub_heads = 1; sub_heads < ColSubHead.Count + 1; sub_heads++)
                                        //        {
                                        //            NewTable.Cell(row_value, col_value).Range.Text = Vcore.clearHTML(ColSubHead[sub_heads - 1].ToString());
                                        //            NewTable.Cell(row_value, col_value).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                        //            col_value += 1;
                                        //        }

                                        //    }
                                        //    row_value = 3;
                                        //    col_value = 1;
                                        //    for (int row_ans_opts = 1; row_ans_opts < rowHead.Count + 1; row_ans_opts++)
                                        //    {

                                        //        NewTable.Cell(row_value, col_value).Range.Text = Vcore.clearHTML(rowHead[row_ans_opts - 1].ToString());
                                        //        NewTable.Cell(row_value, col_value).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                        //        row_value += 1;

                                        //    }
                                        //    row_value = 3;
                                        //    int TotalResponsCount = 0;
                                        //    //foreach (SurveyAnswers ans in ques.surveyAnswers)
                                        //    //{
                                        //    //    TotalResponsCount += ans.RESPONSE_COUNT;
                                        //    //}
                                        //    ArrayList column_values = new ArrayList();
                                        //    int m = 0, b = 0;
                                        //    foreach (SurveyAnswers ans in ques.surveyAnswers)
                                        //    {
                                        //        m += 1; ;
                                        //        b = b + ans.RESPONSE_COUNT;
                                        //        if (m == ColSubHead.Count)
                                        //        {
                                        //            column_values.Add(b);
                                        //            b = 0;
                                        //            m = 0;
                                        //        }
                                        //        TotalResponsCount += ans.RESPONSE_COUNT;
                                        //    }
                                        //    if (TotalResponsCount > 0)
                                        //    {
                                        //        ArrayList ansrescount = new ArrayList();
                                        //        ArrayList ansrespercent = new ArrayList();
                                        //        int row_count = 0, g = 0;
                                        //        foreach (SurveyAnswers AnsOp in ques.surveyAnswers)
                                        //        {
                                        //            ansrescount.Add(AnsOp.RESPONSE_COUNT);
                                        //            double ResPercent = 0;
                                        //            //if (TotalResponsCount > 0)
                                        //            //    ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT * 100) / TotalResponsCount, 2);
                                        //            if (column_values[row_count] != null && Convert.ToInt32(column_values[row_count]) > 0)
                                        //                ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT * 100) / Convert.ToInt32(column_values[row_count]), 2);
                                        //            ansrespercent.Add(ResPercent);
                                        //            g = g + 1;
                                        //            if (g == ColSubHead.Count)
                                        //            {
                                        //                row_count += 1;
                                        //                g = 0;
                                        //            }
                                        //        }
                                        //        int columnvalues_count = colHead.Count * ColSubHead.Count;
                                        //        int sub_matrix_count = 1;
                                        //        int row_cells_count = 1;
                                        //        int jj = 2;
                                        //        for (int cell_values = 1; cell_values < ansrescount.Count + 1; cell_values++)
                                        //        {


                                        //            string str = ansrescount[cell_values - 1] + "(" + ansrespercent[cell_values - 1] + "%)";
                                        //            NewTable.Cell(row_value, jj).Range.Text = str;
                                        //            NewTable.Cell(row_value, jj).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                                        //            NewTable.Cell(row_value, jj).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                        //            sub_matrix_count += 1;
                                        //            row_cells_count += 1;
                                        //            jj += 1;
                                        //            if (row_cells_count == columnvalues_count + 1)
                                        //            {
                                        //                row_value += 1;
                                        //                row_cells_count = 1;
                                        //                jj = 2;
                                        //            }

                                        //        }

                                        //    }

                                        //}
                                    }
                                }
                                else if (ques.QUESTION_TYPE == 12)
                                {
                                    ValidationCore Vcore = new ValidationCore();
                                    ArrayList colHead = new ArrayList();
                                    ArrayList ColSubHead = new ArrayList();
                                    ArrayList rowHead = new ArrayList();
                                    Hashtable headerList = BuildQuetions.GetAnsItemsList(ques);
                                    if (headerList.Contains("ColHead"))
                                        colHead = (ArrayList)headerList["ColHead"];
                                    if (headerList.Contains("ColSubHead"))
                                        ColSubHead = (ArrayList)headerList["ColSubHead"];
                                    if (headerList.Contains("RowHead"))
                                        rowHead = (ArrayList)headerList["RowHead"];
                                    int count = (colHead.Count * ColSubHead.Count) + 1;
                                    Word.Table NewTable = test.CreateTable(rowHead.Count + 2, count);
                                    NewTable.TopPadding = 2;

                                    object start = System.Type.Missing;
                                    int iCol = NewTable.Columns.Count;
                                    if (iCol < count)
                                    {
                                        NewTable.Columns.Add(ref start);
                                    }

                                    int row_value = 1;
                                    int col_value = 2;
                                    int temp_count = (ColSubHead.Count * colHead.Count) + 1;
                                    temp_count = temp_count - 1;
                                    int k = 0;

                                    for (int main_col_heads = 1; main_col_heads < temp_count + 1; main_col_heads++)
                                    {
                                        if (Convert.ToInt32((main_col_heads - 1) % (ColSubHead.Count)) == 0)
                                        {
                                            NewTable.Cell(row_value, col_value).HeightRule = WdRowHeightRule.wdRowHeightAuto;
                                            NewTable.Cell(row_value, col_value).Range.Text = Vcore.clearHTML(colHead[k].ToString());
                                            NewTable.Cell(row_value, col_value).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                            NewTable.Cell(row_value, col_value).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                                            k = k + 1;
                                        }
                                        else
                                        {
                                            NewTable.Cell(row_value, col_value).Range.Text = " ";
                                            NewTable.Cell(row_value, col_value).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                        }
                                        col_value += 1;
                                    }
                                    int mer_index = 2;
                                    for (int mer = 0; mer < colHead.Count; mer++)
                                    {

                                        NewTable.Cell(1, mer_index).Merge(NewTable.Cell(1, mer_index + ColSubHead.Count - 1));

                                        NewTable.Cell(1, mer_index).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalBottom;
                                        NewTable.Cell(1, mer_index).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                                        mer_index = mer_index + 1;
                                    }
                                    NewTable.Rows[1].Alignment = WdRowAlignment.wdAlignRowCenter;
                                    row_value = 2;
                                    col_value = 2;
                                    for (int main_col_heads = 1; main_col_heads < colHead.Count + 1; main_col_heads++)
                                    {
                                        for (int sub_heads = 1; sub_heads < ColSubHead.Count + 1; sub_heads++)
                                        {
                                            NewTable.Cell(row_value, col_value).Range.Text = Vcore.clearHTML(ColSubHead[sub_heads - 1].ToString());
                                            NewTable.Cell(row_value, col_value).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                            col_value += 1;
                                        }

                                    }
                                    row_value = 3;
                                    col_value = 1;
                                    for (int row_ans_opts = 1; row_ans_opts < rowHead.Count + 1; row_ans_opts++)
                                    {

                                        NewTable.Cell(row_value, col_value).Range.Text = Vcore.clearHTML(rowHead[row_ans_opts - 1].ToString());
                                        NewTable.Cell(row_value, col_value).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                        row_value += 1;

                                    }
                                    row_value = 3;
                                    int TotalResponsCount = 0;
                                    //foreach (SurveyAnswers ans in ques.surveyAnswers)
                                    //{
                                    //    TotalResponsCount += ans.RESPONSE_COUNT;
                                    //}
                                    ArrayList column_values = new ArrayList();
                                    //int m = 0, b = 0;
                                    int topHeader = 0, col = 0, row = 0;
                                    int totalCount = 0;
                                    foreach (SurveyAnswers ans in ques.surveyAnswers)
                                    {
                                        //m += 1; ;
                                        //b = b + ans.RESPONSE_COUNT;
                                        //if (m == ColSubHead.Count)
                                        //{
                                        //    column_values.Add(b);
                                        //    b = 0;
                                        //    m = 0;
                                        //}
                                        //TotalResponsCount += ans.RESPONSE_COUNT;
                                        string optionValue = string.Format("{0}$--${1}$--${2}", colHead[topHeader], ColSubHead[col], rowHead[row]);
                                        var anserOption = ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS.Replace("\r\n", "").Replace("\r", "").Replace("\n", "") == optionValue.Replace("\r\n", "").Replace("\r", "").Replace("\n", "")).FirstOrDefault();
                                        totalCount += anserOption.RESPONSE_COUNT;
                                        col++;
                                        if (col == ColSubHead.Count)
                                        {

                                            topHeader++;
                                            col = 0;
                                            column_values.Add(totalCount);
                                            totalCount = 0;
                                        }
                                        if (topHeader == colHead.Count)
                                        {
                                            row++;
                                            col = 0;
                                            topHeader = 0;
                                        }
                                    }
                                  //  if (TotalResponsCount > 0)
                                   // {
                                        ArrayList ansrescount = new ArrayList();
                                        ArrayList ansrespercent = new ArrayList();
                                        int row_count = 0;
                                        topHeader = 0;
                                        col = 0;
                                        row = 0;    
                                        foreach (SurveyAnswers AnsOp in ques.surveyAnswers)
                                        {
                                            //ansrescount.Add(AnsOp.RESPONSE_COUNT);
                                            //double ResPercent = 0;
                                            ////if (TotalResponsCount > 0)
                                            ////    ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT * 100) / TotalResponsCount, 2);
                                            //if (column_values[row_count] != null && Convert.ToInt32(column_values[row_count]) > 0)
                                            //    ResPercent = Math.Round(Convert.ToDouble(AnsOp.RESPONSE_COUNT * 100) / Convert.ToInt32(column_values[row_count]), 2);
                                            //ansrespercent.Add(ResPercent);
                                            //g = g + 1;
                                            //if (g == ColSubHead.Count)
                                            //{
                                            //    row_count += 1;
                                            //    g = 0;
                                            //}
                                            string optionValue = string.Format("{0}$--${1}$--${2}", colHead[topHeader], ColSubHead[col], rowHead[row]);
                                            var anserOption = ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS.Replace("\r\n", "").Replace("\r", "").Replace("\n", "") == optionValue.Replace("\r\n", "").Replace("\r", "").Replace("\n", "")).FirstOrDefault();
                                            ansrescount.Add(anserOption.RESPONSE_COUNT);
                                            double ResPercent = 0;
                                            if (column_values[row_count] != null && Convert.ToInt32(column_values[row_count]) > 0)
                                                ResPercent = Math.Round(Convert.ToDouble(anserOption.RESPONSE_COUNT * 100) / Convert.ToInt32(column_values[row_count]), 2);
                                            ansrespercent.Add(ResPercent);                                         
                                            col++;
                                            if (col == ColSubHead.Count)
                                            {
                                                row_count++;
                                                topHeader++;
                                                col = 0;                                               
                                            }
                                            if (topHeader == colHead.Count)
                                            {
                                                row++;
                                                col = 0;
                                                topHeader = 0;
                                            }
                                        }
                                        int columnvalues_count = colHead.Count * ColSubHead.Count;
                                        int sub_matrix_count = 1;
                                        int row_cells_count = 1;
                                        int jj = 2;
                                        for (int cell_values = 1; cell_values < ansrescount.Count + 1; cell_values++)
                                        {


                                            string str = ansrescount[cell_values - 1] + "(" + ansrespercent[cell_values - 1] + "%)";
                                            NewTable.Cell(row_value, jj).Range.Text = str;
                                            NewTable.Cell(row_value, jj).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                                            NewTable.Cell(row_value, jj).VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                                            sub_matrix_count += 1;
                                            row_cells_count += 1;
                                            jj += 1;
                                            if (row_cells_count == columnvalues_count + 1)
                                            {
                                                row_value += 1;
                                                row_cells_count = 1;
                                                jj = 2;
                                            }

                                        }

                                   // }

                                } 
                                test.GoToTheEnd();
                                if (ExportMode == "Data")
                                    test.InsertPagebreak();
                                else
                                    test.InsertLineBreak(1);

                            }
                            if (ExportMode == "Charts" || ExportMode == "DC")
                            {
                                if (ques.QUESTION_TYPE != 12)
                                {
                                    string titleTxt = "";
                                    if (ques.CHART_TITLE.Trim().Length > 0)
                                        titleTxt = ques.CHART_TITLE;
                                    else
                                    {
                                        titleTxt = ValidCore.clearHTML(ques.QUESTION_LABEL);
                                        titleTxt = titleTxt.Replace("&nbsp;", "");
                                    }
                                    test.InsertText(titleTxt);
                                    test.InsertLineBreak(1);
                                    string PicPath = SaveChartToImage(ques, sur.USER_ID, resp_type);
                                    test.InsertPicture(PicPath);
                                    test.InsertPagebreak();
                                }
                                else
                                {
                                    ArrayList colHead = new ArrayList();
                                    Hashtable headerList = BuildQuetions.GetAnsItemsList(ques);
                                    if (headerList.Contains("ColHead"))
                                        colHead = (ArrayList)headerList["ColHead"];
                                    for (int mat_count = 0; mat_count < colHead.Count; mat_count++)
                                    {
                                        SMode = mat_count;
                                        string titleTxt = "";
                                        if (ques.CHART_TITLE.Trim().Length > 0)
                                            titleTxt = ques.CHART_TITLE;
                                        else
                                        {
                                            titleTxt = ValidCore.clearHTML(ques.QUESTION_LABEL) + "(" + ValidCore.clearHTML(Convert.ToString(colHead[mat_count])) + ")";
                                            titleTxt = titleTxt.Replace("&nbsp;", "");
                                        }
                                        test.InsertText(titleTxt);
                                        test.InsertLineBreak(1);
                                        string PicPath = SaveChartToImage(ques, sur.USER_ID, resp_type);
                                        test.InsertPicture(PicPath);
                                        test.InsertPagebreak();
                                    }

                                }
                            }
                        }
                    }



                }

                string name_survey = sur.SURVEY_NAME.Replace(" ", "");
                name_survey = name_survey.Replace("/", "");
                name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                string filename = name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".doc";
                //string sourcePath = MapPath("~/") + "In\\UserImages\\" + sur.USER_ID + "\\" + sur.SURVEY_ID + "\\" + filename;

                string sPath = Path + sur.USER_ID + "\\" + sur.SURVEY_ID;
                if (!Directory.Exists(sPath))
                {
                    Directory.CreateDirectory(sPath);
                }
                string sourcePath = Path + sur.USER_ID + "\\" + sur.SURVEY_ID + "\\" + filename;

                string surname = sur.SURVEY_NAME;
                test.SaveAs(sourcePath);
                string Extn = ".doc";
                test.Quit();
                FileInfo FI = new FileInfo(sourcePath);
                if (FI.Exists)
                {
                    System.Threading.Thread.Sleep(500);
                }
                try
                {
                    // SendMailAttachment(sourcePath, toEmailId);
                }
                catch (Exception ex)
                {
                    GenerateLog.GenrateSendmailLog("Failed Sending Attachments to surveyId:" + SurveyID, @"c:\V2\SurveyExportLog.txt");
                }

                return filename;
                // SaveDialogBox(sourcePath, surname, Extn);

            }

            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(string.Format("Message: {0} \r\n StackTrace: {1}", ex.Message, ex.StackTrace), @"c:\SurveyExportLog.txt");
                return "";

            }

        }
        public class CCWordApp
        {
            object start = 0;
            public Word.ApplicationClass oWordApplic;	// a reference to Word application
            public Word.Document oDoc;					// a reference to the document


            public CCWordApp()
            {
                // activate the interface with the COM object of Microsoft Word
                oWordApplic = new Word.ApplicationClass();
            }
            public void Activate()
            {
                oDoc.Activate();

            }

            public Word.Table CreateTable(int ColumnCnt, int RowCnt)
            {
                object Index = "\\endofdoc";
                object oMissing = Missing.Value;
                object oTableBehavior = Word.WdDefaultTableBehavior.wdWord9TableBehavior;
                object oFitBehavior = Word.WdAutoFitBehavior.wdAutoFitFixed;
                object start = oWordApplic.ActiveDocument.Content.End - 1;
                //Word.Table NewTable = oDoc.Tables.Add(oDoc.Bookmarks.get_Item(ref Index).Range, ColumnCnt, RowCnt, ref oTableBehavior, ref oFitBehavior);
                //NewWordDoc1.Tables.Add(NewWordDoc1.Bookmarks.get_Item(ref Index).Range, TotalResponsCount, 3, ref oMissing, ref oMissing, ref oMissing);
                Word.Range rng = oDoc.Range(ref start, ref oMissing);
                Word.Table NewTable = oDoc.Tables.Add(rng, ColumnCnt, RowCnt, ref oTableBehavior, ref oFitBehavior);
                return NewTable;
            }
            public Microsoft.Office.Interop.Word.Document CreateNewDoc()
            {
                object missing = System.Reflection.Missing.Value;
                Word.Document Newdoc;
                Newdoc = oWordApplic.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                Newdoc.Activate();
                return Newdoc;

            }
            // Open a file (the file must exists) and activate it
            public void Open(string strFileName)
            {
                object fileName = strFileName;
                object readOnly = false;
                object isVisible = true;
                object missing = System.Reflection.Missing.Value;


                oDoc = oWordApplic.Documents.Open(ref fileName, ref missing, ref readOnly,
                    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref isVisible, ref missing, ref missing, ref missing, ref missing);

                oDoc.Activate();
            }
            // Open a new document
            public void Open()
            {
                object missing = System.Reflection.Missing.Value;
                oDoc = oWordApplic.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                oDoc.Activate();
            }
            public void Quit()
            {
                object missing = System.Reflection.Missing.Value;
                oWordApplic.Application.Quit(ref missing, ref missing, ref missing);
            }
            public void Save()
            {
                oDoc.Save();
            }
            public void SaveAs(string strFileName)
            {
                object missing = System.Reflection.Missing.Value;
                object fileName = strFileName;
                oDoc.SaveAs(ref fileName, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
            }
            public void SaveNewDoc()
            {
                object missing = System.Reflection.Missing.Value;
                oDoc = oWordApplic.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                oDoc.Activate();
            }
            // Save the document in HTML format
            public void SaveAsHtml(string strFileName)
            {
                object missing = System.Reflection.Missing.Value;
                object fileName = strFileName;
                object Format = (int)Word.WdSaveFormat.wdFormatHTML;
                oDoc.SaveAs(ref fileName, ref Format, ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
            }
            public void InsertText(string strText)
            {
                oWordApplic.Selection.TypeText(strText);
            }
            public void InsertLineBreak()
            {
                oWordApplic.Selection.TypeParagraph();

            }
            public void ConvertTotable()
            {
                object missing = System.Reflection.Missing.Value;
                object separator = WdSeparatorType.wdSeparatorEnDash;
                object objTrue = true;
                object NumCol = 3;
                object start1 = 0;
                object objend = oWordApplic.ActiveDocument.Content.End - 1;
                oWordApplic.DefaultTableSeparator = "@#&*";
                oWordApplic.Selection.ConvertToTable(ref missing, ref missing, ref NumCol, ref missing, ref missing, ref objTrue, ref objTrue, ref objTrue, ref objTrue, ref objTrue, ref objTrue, ref objTrue, ref objTrue, ref objTrue, ref missing, ref missing);
                start1 = oWordApplic.ActiveDocument.Content.End - 1;
                //oWordApplic.Selection.InsertNewPage();
                //oDoc.Activate();

            }
            public void InsertLineBreak(int nline)
            {
                for (int i = 0; i < nline; i++)
                    oWordApplic.Selection.TypeParagraph();
            }
            // Change the paragraph alignement
            public void SetAlignment(string strType)
            {
                switch (strType)
                {
                    case "Center":
                        oWordApplic.Selection.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                        break;
                    case "Left":
                        oWordApplic.Selection.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                        break;
                    case "Right":
                        oWordApplic.Selection.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
                        break;
                    case "Justify":
                        oWordApplic.Selection.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphJustify;
                        break;
                }

            }
            // if you use thif function to change the font you should call it again with 
            // no parameter in order to set the font without a particular format
            public void SetFont(string strType)
            {
                switch (strType)
                {
                    case "Bold":
                        oWordApplic.Selection.Font.Bold = 1;
                        break;
                    case "Italic":
                        oWordApplic.Selection.Font.Italic = 1;
                        break;
                    case "Underlined":
                        oWordApplic.Selection.Font.Subscript = 0;
                        break;
                }
            }
            public void SetFont()
            {
                oWordApplic.Selection.Font.Bold = 0;
                oWordApplic.Selection.Font.Italic = 0;
                oWordApplic.Selection.Font.Subscript = 0;

            }
            public void SetFontName(string strType)
            {
                oWordApplic.Selection.Font.Name = strType;
            }
            public void SetFontSize(int nSize)
            {
                oWordApplic.Selection.Font.Size = nSize;
            }
            public void InsertPagebreak()
            {
                object pBreak = (int)Word.WdBreakType.wdPageBreak;
                oWordApplic.Selection.InsertBreak(ref pBreak);
            }
            public void GotoBookMark(string strBookMarkName)
            {
                object missing = System.Reflection.Missing.Value;
                object Bookmark = (int)Word.WdGoToItem.wdGoToBookmark;
                object NameBookMark = strBookMarkName;
                oWordApplic.Selection.GoTo(ref Bookmark, ref missing, ref missing, ref NameBookMark);
            }
            public void GoToTheEnd()
            {
                object missing = System.Reflection.Missing.Value;
                object unit;
                unit = Word.WdUnits.wdStory;
                oWordApplic.Selection.EndKey(ref unit, ref missing);

            }
            public void GoToTheBeginning()
            {
                object missing = System.Reflection.Missing.Value;
                object unit;
                unit = Word.WdUnits.wdStory;
                oWordApplic.Selection.HomeKey(ref unit, ref missing);

            }
            public void GoToTheTable(int ntable)
            {
                object missing = System.Reflection.Missing.Value;
                object what;
                what = Word.WdUnits.wdTable;
                object which;
                which = Word.WdGoToDirection.wdGoToFirst;
                object count;
                count = 1;
                oWordApplic.Selection.GoTo(ref what, ref which, ref count, ref missing);
                oWordApplic.Selection.Find.ClearFormatting();
                oWordApplic.Selection.Text = "";
            }
            public void GoToRightCell()
            {
                object missing = System.Reflection.Missing.Value;
                object direction;
                direction = Word.WdUnits.wdCell;
                oWordApplic.Selection.MoveRight(ref direction, ref missing, ref missing);
            }
            public void GoToLeftCell()
            {
                object missing = System.Reflection.Missing.Value;
                object direction;
                direction = Word.WdUnits.wdCell;
                oWordApplic.Selection.MoveLeft(ref direction, ref missing, ref missing);
            }
            public void GoToDownCell()
            {
                object missing = System.Reflection.Missing.Value;
                object direction;
                direction = Word.WdUnits.wdLine;
                oWordApplic.Selection.MoveDown(ref direction, ref missing, ref missing);
            }
            public void GoToUpCell()
            {
                object missing = System.Reflection.Missing.Value;
                object direction;
                direction = Word.WdUnits.wdLine;
                oWordApplic.Selection.MoveUp(ref direction, ref missing, ref missing);
            }
            public void InsertPicture(string PicPath)
            {

                object TemplateFileName = PicPath;//"C:\\Water.jpg";
                object missing = System.Reflection.Missing.Value;
                oDoc.Application.Selection.InlineShapes.AddPicture(PicPath, ref missing, ref missing, ref missing);
            }

        }

        public void CustomizeCrossTabNonDescPage()
        {
            Range excelRange = RespSheet.UsedRange;
            excelRange.WrapText = true;

            Range rg = RespSheet.UsedRange;
            rg.Font.Name = "Trebuchet MS";
            rg.Font.Size = 10;
            rg.Cells.WrapText = true;
        }

        public string SaveChartToImage(SurveyQuestion ques, int USERID, int resp_type)
        {
            string PicPath = Path + USERID + "\\sample.png";
            if (ques.RESPONSE_COUNT != null && ques.RESPONSE_COUNT > 0)
            {
                WebChartControl wbchrt = null;
                wbchrt = BuildQuetions.GetChart(ques, SMode);
                ViewType viewType = BuildQuetions.GetChartViewType(ques.CHART_TYPE, ques.QUESTION_TYPE);
                if (ques.QUESTION_TYPE == 4 && (viewType == ViewType.Pie || viewType == ViewType.Doughnut || viewType == ViewType.Doughnut3D || viewType == ViewType.Pie3D) && resp_type == 0)
                {

                    foreach (DevExpress.XtraCharts.Series series in wbchrt.Series)
                    {
                        //((PiePointOptions)series.PointOptions).PointView = PointView.ArgumentAndValues;
                        series.PointOptions.PointView = PointView.ArgumentAndValues;
                    }
                    wbchrt.Legend.Visible = false;
                }
                wbchrt.Height = (Unit)600;
                wbchrt.Width = (Unit)500;
                ASPxRoundPanel ASPxRoundPanelQuestionreports1 = new ASPxRoundPanel();
                ASPxRoundPanelQuestionreports1.Controls.Add(wbchrt);

                if (ques != null && wbchrt != null)
                    ChartExtraFeatures.SetchartDiagramProp(wbchrt, ques);
                wbchrt.Titles.Clear();
                string Id = "WbchrtPdf" + Convert.ToInt32(ques.QUESTION_ID);
                wbchrt.ID = Id;

                ChartControl piechartControl = new ChartControl();
                ((DevExpress.XtraCharts.Native.IChartContainer)piechartControl).Chart.Assign(((IChartContainer)wbchrt).Chart);
                piechartControl.Height = 500;
                piechartControl.Width = 700;
                piechartControl.ExportToImage(PicPath, ImageFormat.Png);
                ASPxRoundPanelQuestionreports1.Controls.Clear();
            }
            else
            {
                Bitmap objBmpImage = new Bitmap(1, 1);
                objBmpImage = savetextToImage();
                objBmpImage.Save(PicPath);
            }

            return (PicPath);
        }
        public System.Drawing.Bitmap savetextToImage()
        {
            string sImageText = " No Responses for this question";
            Bitmap objBmpImage = new Bitmap(1, 1);
            int intWidth = 0;
            int intHeight = 0;
            System.Drawing.Font objFont = new System.Drawing.Font("Arial", 20, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            Graphics objGraphics = Graphics.FromImage(objBmpImage);
            intWidth = (int)objGraphics.MeasureString(sImageText, objFont).Width;
            intHeight = 75;
            objBmpImage = new Bitmap(objBmpImage, new Size(intWidth, intHeight));
            objGraphics = Graphics.FromImage(objBmpImage);
            objGraphics.Clear(System.Drawing.Color.White);
            objGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            objGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            objGraphics.DrawString(sImageText, objFont, new SolidBrush(System.Drawing.Color.FromArgb(102, 102, 102)), 0, 0);
            objGraphics.Flush();
            return (objBmpImage);
        }
        public string GetQuesType(int QUESTION_TYPE)
        {
            string QUES_MODE = "";
            if (QUESTION_TYPE == 1 || QUESTION_TYPE == 2 || QUESTION_TYPE == 3 || QUESTION_TYPE == 4 || QUESTION_TYPE == 10 || QUESTION_TYPE == 11 || QUESTION_TYPE == 12 || QUESTION_TYPE == 13 || QUESTION_TYPE == 15)
            { QUES_MODE = "Desc"; }
            else if (!(QUESTION_TYPE == 16 || QUESTION_TYPE == 17 || QUESTION_TYPE == 18 || QUESTION_TYPE == 19))
            { QUES_MODE = "NonDesc"; }
            return (QUES_MODE);
        }
        public DataSet GetDataReport(SurveyQuestion ques)
        {
            int qid = Convert.ToInt32(ques.QUESTION_ID);
            ValidationCore Vcore = new ValidationCore();
            ExportSurveyCore surcore = new ExportSurveyCore();
            DataSet ds = new DataSet();
            System.Data.DataTable tab = new System.Data.DataTable();
            ds.Tables.Add(tab);
            int i = 0;
            if (ques.surveyAnswers != null && ques.surveyAnswers.Count > 0)
            {
                if (!(ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11 || ques.QUESTION_TYPE == 12 || ques.QUESTION_TYPE == 13))
                {
                    //int ansoptcnt = 0;
                    //int YesResponsecount = 0, NoresponseCount = 0;
                    int TotalResponsCount = 0;
                    if (ques.QUESTION_TYPE == 2)
                    {
                        TotalResponsCount += ques.RESPONSE_COUNT;
                    }
                    else
                    {
                        foreach (SurveyAnswers answ in ques.surveyAnswers)
                        {
                            TotalResponsCount += answ.RESPONSE_COUNT;
                        }
                    }
                    if (ques.QUESTION_TYPE == 1 || ques.QUESTION_TYPE == 3 || ques.QUESTION_TYPE == 4)
                    {
                        TotalResponsCount += ques.OTHER_RESPCOUNT;
                    }
                    ds.Tables[0].Columns.Add("AnswerOptions");
                    ds.Tables[0].Columns.Add("ResponseCount");
                    ds.Tables[0].Columns.Add("ResponsePercent");
                    foreach (SurveyAnswers answe in ques.surveyAnswers)
                    {
                        ds.Tables[0].Rows.Add();
                        string str1 = Vcore.clearHTML(Convert.ToString(answe.ANSWER_OPTIONS));
                        str1 = str1.Replace("\r", " ");
                        str1 = str1.Replace("\n", " ");
                        ds.Tables[0].Rows[i]["AnswerOptions"] = str1;
                        ds.Tables[0].Rows[i]["ResponseCount"] = Convert.ToString(answe.RESPONSE_COUNT);
                        if (!(TotalResponsCount == 0))
                        {
                            Double z = Convert.ToDouble(Convert.ToInt32(answe.RESPONSE_COUNT * 100));
                            z = z / TotalResponsCount;
                            ds.Tables[0].Rows[i]["ResponsePercent"] = Math.Round(z, 2) + " %";
                        }
                        else
                        {
                            ds.Tables[0].Rows[i]["ResponsePercent"] = Convert.ToString(0) + " %";

                        }

                        i++;
                    }
                    if (ques.OTHER_ANS == 1)
                    {
                        if (ques.QUESTION_TYPE == 1 || ques.QUESTION_TYPE == 2 || ques.QUESTION_TYPE == 3 || ques.QUESTION_TYPE == 4)
                        {
                            ds.Tables[0].Rows.Add();
                            ds.Tables[0].Rows[i]["AnswerOptions"] = "Others";
                            ds.Tables[0].Rows[i]["ResponseCount"] = Convert.ToString(ques.OTHER_RESPCOUNT);
                            if (!(TotalResponsCount == 0))
                            {
                                // ds.Tables[0].Rows[i]["ResponsePercent"] = Math.Round(Convert.ToDouble((ques.OTHER_RESPCOUNT * 100) / TotalResponsCount),2) + " %";
                                Double z = Convert.ToDouble(Convert.ToInt32(ques.OTHER_RESPCOUNT * 100));
                                z = z / TotalResponsCount;
                                ds.Tables[0].Rows[i]["ResponsePercent"] = Math.Round(z, 2) + " %";
                            }
                            else
                            {
                                ds.Tables[0].Rows[i]["ResponsePercent"] = Convert.ToString(0) + " %";

                            }
                        }
                    }
                }
                else if (ques.QUESTION_TYPE == 13)
                {

                    //int ansoptcnt = 0;
                    //int YesResponsecount = 0, NoresponseCount = 0;
                    int TotalResponsCount = 0;
                    ArrayList ans_options = new ArrayList();
                    ArrayList ans_option_sum = new ArrayList();
                    foreach (SurveyAnswers ans in ques.surveyAnswers)
                    {
                        TotalResponsCount += ans.RESPONSE_COUNT;
                        ans_options.Add(ans.ANSWER_ID);
                    }
                    DataSet ds_constantsum = new DataSet();
                    ds_constantsum = surcore.GetConstantSumForReports(ques.QUESTION_ID, ans_options, ques.SURVEY_ID);
                    ds.Tables[0].Columns.Add("AnswerOptions");
                    ds.Tables[0].Columns.Add("ResponseCount");
                    ds.Tables[0].Columns.Add("ResponsePercent");
                    if (ds_constantsum != null && ds_constantsum.Tables.Count > 0)
                    {
                        double ResPercentdividedby = 0;
                        string RespondentIDS = surcore.GetRespondentsForReports(ques.QUESTION_ID, ques.SURVEY_ID);
                        string[] arr = RespondentIDS.Split(',');
                        double z = Convert.ToDouble(ques.CHAR_LIMIT);
                        if (z == 0.0)
                        {
                            z = 100;

                        }

                        ResPercentdividedby = Math.Round(Convert.ToDouble(z) / 100, 2);
                        double totalsum = 0;
                        int y = 0;
                        foreach (SurveyAnswers ans in ques.surveyAnswers)
                        {
                            int temp_total = 0;
                            ds.Tables[0].Rows.Add();
                            string str1 = Vcore.clearHTML(Convert.ToString(ans.ANSWER_OPTIONS));
                            str1 = str1.Replace("\r", " ");
                            str1 = str1.Replace("\n", " ");
                            ds.Tables[0].Rows[i]["AnswerOptions"] = str1;

                            for (int m = 0; m < ds_constantsum.Tables[0].Rows.Count; m++)
                            {

                                if (Convert.ToString(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) != null && ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"] != null && Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) == ans.ANSWER_ID)
                                {
                                    temp_total += Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"]);

                                }
                            }
                            double avg = 0;
                            avg = Math.Round(Convert.ToDouble(temp_total) / (arr.Length), 2);
                            ds.Tables[0].Rows[i]["ResponseCount"] = Convert.ToString(avg);
                            double ResPercent = 0;
                            if (Convert.ToInt32(temp_total) > 0 && ResPercentdividedby > 0 && arr.Length != null && arr.Length > 0)
                                ResPercent = Math.Round(Convert.ToDouble(temp_total) / (ResPercentdividedby * arr.Length), 1);
                            ds.Tables[0].Rows[i]["ResponsePercent"] = Convert.ToString(ResPercent) + " %";
                            y = y + 1;
                            i++;
                        }

                    }

                }
                else
                {

                    ds.Clear();
                    ds.Tables[0].Columns.Add("ColumnOptions");
                    ds.Tables[0].Columns.Add("RowOptions");
                    ds.Tables[0].Columns.Add("ResponseCount");
                    ds.Tables[0].Columns.Add("ResponsePercent");
                    ds.Tables[0].Columns.Add("Rowcount");
                    ds.Tables[0].Columns.Add("ColCount");
                    ds.Tables[0].Columns.Add("DynamicPercentCnt");
                    ds.Tables[0].Columns.Add("ColumnSubOptions");
                    ds.Tables[0].Columns.Add("ColumnSubCount");
                    ArrayList colHead = new ArrayList();
                    ArrayList ColSubHead = new ArrayList();
                    ArrayList rowHead = new ArrayList();
                    Hashtable headerList = BuildQuetions.GetAnsItemsList(ques);
                    if (headerList.Contains("ColHead"))
                        colHead = (ArrayList)headerList["ColHead"];
                    if (headerList.Contains("ColSubHead"))
                        ColSubHead = (ArrayList)headerList["ColSubHead"];
                    if (headerList.Contains("RowHead"))
                        rowHead = (ArrayList)headerList["RowHead"];
                    ArrayList column_values = new ArrayList();
                    //int m = 0, b = 0;
                    //int TotalResponsCount = 0;
                    int col = 0, row = 0, totalResponse = 0;
                    foreach (SurveyAnswers ans in ques.surveyAnswers)
                    {
                        //m += 1; ;
                        //b = b + ans.RESPONSE_COUNT;
                        //if (m == colHead.Count)
                        //{
                        //    column_values.Add(b);
                        //    b = 0;
                        //    m = 0;
                        //}
                        //TotalResponsCount += ans.RESPONSE_COUNT;
                        string optionValue = string.Format("{0}$--${1}", colHead[col], rowHead[row]);
                        var anserOption = ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Trim() == optionValue.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Trim()).FirstOrDefault();
                        totalResponse += anserOption.RESPONSE_COUNT;
                        col++;
                        if (col == colHead.Count)
                        {
                            column_values.Add(totalResponse);
                            row++;
                            col = 0;
                            totalResponse = 0;
                        }                  
                    }
                    int row_count = 0;
                    //    , g = 0;
                    //int l = 0;
                    col = 0;
                    row = 0;
                    foreach (SurveyAnswers answe in ques.surveyAnswers)
                    {
                        string optionValue = string.Format("{0}$--${1}", colHead[col], rowHead[row]);
                        var anserOption = ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Trim() == optionValue.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Trim()).FirstOrDefault();
                        ds.Tables[0].Rows.Add();
                        string str2 = "";
                        if (colHead.Count > col)
                        {
                            str2 = Vcore.clearHTML(Convert.ToString(colHead[col]));
                            str2 = str2.Replace("\r", " ");
                            str2 = str2.Replace("\n", " ");
                            ds.Tables[0].Rows[i]["ColumnOptions"] = str2;
                           // ds.Tables[0].Rows[i]["ColumnOptions"] = Vcore.clearHTML(Convert.ToString(colHead[i]));
                           // ds.Tables[0].Rows[0]["ColCount"] = i + 2;
                        }
                        if (rowHead.Count > row)
                        {
                            str2 = Vcore.clearHTML(Convert.ToString(rowHead[row]));
                            str2 = str2.Replace("\r", " ");
                            str2 = str2.Replace("\n", " ");
                            ds.Tables[0].Rows[i]["RowOptions"] = str2;
                           // ds.Tables[0].Rows[0]["Rowcount"] = i + 2;
                        }
                        //if (ColSubHead.Count > i)
                        //{
                        //    str2 = Vcore.clearHTML(Convert.ToString(ColSubHead[i]));
                        //    str2 = str2.Replace("\r", " ");
                        //    str2 = str2.Replace("\n", " ");
                        //    ds.Tables[0].Rows[i]["ColumnSubOptions"] = str2;
                        //    ds.Tables[0].Rows[0]["ColumnSubCount"] = i + 1;
                        //}
                        ds.Tables[0].Rows[i]["ResponseCount"] = Convert.ToString(anserOption.RESPONSE_COUNT);
                        if (column_values[row_count] != null && Convert.ToInt32(column_values[row_count]) > 0)
                            ds.Tables[0].Rows[i]["ResponsePercent"] = Math.Round(Convert.ToDouble(anserOption.RESPONSE_COUNT * 100) / Convert.ToInt32(column_values[row_count]), 2);
                        else
                            ds.Tables[0].Rows[i]["ResponsePercent"] = Convert.ToString(0) + " %";
                        //g = g + 1;
                        //if (g == colHead.Count)
                        //{
                        //    row_count += 1;
                        //}
                        //l++;
                        //if (l == colHead.Count)
                        //{
                        //    l = 0;
                        //    g = 0;

                        //}
                        //ds.Tables[0].Rows[0]["DynamicPercentCnt"] = i + 1;
                        //i++;
                         col++;
                    if (col == colHead.Count)
                    {
                        row_count++;
                        row++;
                        col = 0;
                    }        
                    ds.Tables[0].Rows[0]["DynamicPercentCnt"] = i + 1;
                    i++;
                }
                ds.Tables[0].Rows[0]["Rowcount"] = rowHead.Count;
                ds.Tables[0].Rows[0]["ColCount"] = colHead.Count;                    
                    //if (ques.QUESTION_TYPE == 12)
                    //{
                    //    int cn = 0;
                    //    for (int isub = 0; isub < Convert.ToInt32(ds.Tables[0].Rows[0]["ColCount"]) - 1; isub++)
                    //    {
                    //        for (int j = 0; j <= colHead.Count; j++)
                    //        {
                    //            string str3 = Vcore.clearHTML(Convert.ToString(ColSubHead[j]));
                    //            str3 = str3.Replace("\r", " ");
                    //            str3 = str3.Replace("\n", " ");
                    //            ds.Tables[0].Rows[cn++]["ColumnSubOptions"] = str3;

                    //        }

                    //    }
                    //}
                }


            }
            return (ds);
        }


        public string ExportAdminUserslist(string rep_params)
        {
            try
            {
                string filename = "";
                if (rep_params != "" && rep_params.Length > 0)
                {
                    DataSet ds = new DataSet();
                    ExportSurveyCore core = new ExportSurveyCore();
                    string[] det = rep_params.Split('$');
                    Hashtable ht = new Hashtable();

                    if (det != null)
                    {
                        int x = 1;
                        for (int i = 1; i < det.Length; i++)
                        {
                            if (det.GetValue(i) != null && Convert.ToString(det.GetValue(i)).Substring(0, 4) == "##@@")
                            {
                                //if (Convert.ToString(det.GetValue(i)).Length > 4)
                                // {
                                ht.Add(x, Convert.ToString(det.GetValue(i)).Substring(4));
                                //  }
                                // else
                                //{
                                //ht.Add(x, Convert.ToString(det.GetValue(i)));

                                // }
                                x++;
                            }

                        }
                    }
                    if (ht != null && ht.Count > 0 && ht.Count == 16)
                    {
                        ds = core.GetUserAccntsDet(Convert.ToString(ht[1]), Convert.ToString(ht[2]), Convert.ToString(ht[3]), Convert.ToString(ht[4]), Convert.ToString(ht[5]), Convert.ToString(ht[6]), Convert.ToString(ht[7]), Convert.ToString(ht[8]), Convert.ToString(ht[9]), Convert.ToString(ht[10]), Convert.ToString(ht[11]), Convert.ToString(ht[12]), Convert.ToString(ht[13]), Convert.ToString(ht[14]), Convert.ToInt32(ht[15]), Convert.ToString(ht[16]));
                        if (ds != null && ds.Tables.Count > 0)
                        {

                            filename = "Userslist" + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".xls";
                            string strXLFileName = Path + filename;

                            Excel.Application oXL;
                            Excel._Workbook oWB;
                            Excel._Worksheet IntroSheet, RespSheet, NonDescXLsheet, BlankXLsheet;
                            Excel.Range oRng;

                            GC.Collect();
                            oXL = new Excel.Application();
                            oXL.Visible = false;
                            //string strCurrentDir = Server.MapPath(".") + "\\";
                            oWB = (Excel._Workbook)(oXL.Workbooks.Add(Missing.Value));
                            int qcount = 0;
                            Excel._Worksheet[] Sheet = new Excel._Worksheet[qcount];
                            int jj = 1, ij = 4;
                            RespSheet = (Excel._Worksheet)oWB.Worksheets[1];
                            RespSheet.Name = "User Account Details";

                            RespSheet.get_Range("A1", "A1").Font.Bold = true;
                            RespSheet.get_Range("A1", "A1").Font.Size = 10;
                            RespSheet.get_Range("A1", "A1").Value2 = "List of Registered Users";

                            RespSheet.get_Range("B3", "B3").ColumnWidth = 10;
                            RespSheet.get_Range("B3", "B3").Value2 = "S.No";

                            RespSheet.get_Range("C3", "C3").ColumnWidth = 30;
                            RespSheet.get_Range("C3", "C3").Value2 = "Login Name";

                            RespSheet.get_Range("D3", "D3").ColumnWidth = 30;
                            RespSheet.get_Range("D3", "D3").Value2 = "First Name";

                            RespSheet.get_Range("E3", "E3").ColumnWidth = 30;
                            RespSheet.get_Range("E3", "E3").Value2 = "Last Name";

                            RespSheet.get_Range("F3", "F3").ColumnWidth = 30;
                            RespSheet.get_Range("F3", "F3").Value2 = "Telephone";

                            RespSheet.get_Range("G3", "G3").ColumnWidth = 30;
                            RespSheet.get_Range("G3", "G3").Value2 = "Created Date";

                            RespSheet.get_Range("H3", "H3").ColumnWidth = 30;
                            RespSheet.get_Range("H3", "H3").Value2 = "License Expiry Date";

                            RespSheet.get_Range("I3", "I3").ColumnWidth = 30;
                            RespSheet.get_Range("I3", "I3").Value2 = "Account Type";

                            RespSheet.get_Range("J3", "J3").ColumnWidth = 30;
                            RespSheet.get_Range("J3", "J3").Value2 = "Status";

                            oRng = RespSheet.get_Range(RespSheet.Cells[3, 2], RespSheet.Cells[3, 10]);
                            oRng.HorizontalAlignment = Excel.Constants.xlCenter;
                            oRng.Interior.ColorIndex = 44;

                            int r_indx = 4;
                            int c_indx = 2;
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {

                                r_indx++;
                                c_indx = 2;
                                RespSheet.Cells[r_indx, c_indx] = (i + 1).ToString();
                                c_indx++;
                                RespSheet.Cells[r_indx, c_indx] = ds.Tables[0].Rows[i]["LOGIN_NAME"].ToString();
                                c_indx++;
                                RespSheet.Cells[r_indx, c_indx] = ds.Tables[0].Rows[i]["FIRST_NAME"].ToString();
                                c_indx++;
                                RespSheet.Cells[r_indx, c_indx] = ds.Tables[0].Rows[i]["LAST_NAME"].ToString();
                                c_indx++;
                                RespSheet.Cells[r_indx, c_indx] = ds.Tables[0].Rows[i]["PHONE"].ToString();
                                c_indx++;
                                RespSheet.Cells[r_indx, c_indx] = ds.Tables[0].Rows[i]["CREATED_ON"].ToString();
                                c_indx++;
                                RespSheet.Cells[r_indx, c_indx] = ds.Tables[0].Rows[i]["LICENSE_EXPIRY_DATE"].ToString();
                                c_indx++;
                                RespSheet.Cells[r_indx, c_indx] = ds.Tables[0].Rows[i]["ACCOUNT_TYPE"].ToString();
                                c_indx++;
                                RespSheet.Cells[r_indx, c_indx] = ds.Tables[0].Rows[i]["STATUS"].ToString();

                            }

                            oWB.SaveAs(strXLFileName, Excel.XlFileFormat.xlWorkbookNormal,
                                null, null, false, false, Excel.XlSaveAsAccessMode.xlNoChange, false, false, null, null, null);
                            oWB.Close(true, Missing.Value, Missing.Value);
                            oXL.Workbooks.Close();
                            oXL.Quit();
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(oXL);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(RespSheet);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(oWB);
                            System.Diagnostics.Process[] PROC = System.Diagnostics.Process.GetProcessesByName("EXCEL");
                            foreach (System.Diagnostics.Process PK in PROC)
                            {
                                if (PK.MainWindowTitle.Length == 0)
                                    PK.Kill();
                            }
                            oWB = null;
                            oXL = null;
                            GC.Collect();
                        }


                    }

                }

                return filename;
            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(string.Format("Message: {0} \r\n StackTrace: {1}", ex.Message, ex.StackTrace), @"c:\SurveyExportLog.txt");

                return "";
            }
        }

        public void SendMailAttachment(string atachmentFile, string toEmailId)
        {
            string[] eparts = toEmailId.Split(new string[] { "," }, StringSplitOptions.None);
            mails umail = new mails();
            umail.HOST = GetConfigVal("MAIL_HOST");
            umail.PORTNO = Convert.ToInt32(GetConfigVal("MAIL_PORT"));
            umail.SENDER_MAILADDRESS = GetConfigVal("MAIL_SENDERADDRESS_AUTH");
            umail.SENDER_PASSWORD = GetConfigVal("MAIL_PASSWORD_AUTH");
            umail.MAIL_SUBJECT = eparts[1] + "-" + " Report";
            umail.FROM_MAILADDRESS = GetConfigVal("MAIL_FROMADDRESS_AUTH");
            umail.MAIL_TOADDRESS = eparts[0];
            umail.UserName = GetConfigVal("UserName");

            //Sending mail with attachment
            umail.AttachmentFileName = atachmentFile;
            EmailSending emailSend = new EmailSending();
            emailSend.SendMailing(umail);
        }

        private string GetRowsandColsCount(System.Data.DataTable dt)
        {
            List<string> rows = new List<string>();
            List<string> cols = new List<string>();
            foreach (DataRow dr in dt.Rows)
            {

                var delimiter = new[] { "$--$" };
                var splitOptions = dr.ItemArray[1].ToString().Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                if (splitOptions.Length == 2)
                {
                    cols.Add(splitOptions[0]);
                    rows.Add(splitOptions[1]);

                }
                else if (splitOptions.Length == 3)
                {
                    cols.Add(splitOptions[1]);
                    rows.Add(splitOptions[2]);
                }

            }
            string count = rows.Distinct().ToList().Count.ToString();          
            return count;
        }



    }
}
