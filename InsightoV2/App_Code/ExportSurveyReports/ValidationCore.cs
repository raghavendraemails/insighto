﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Sql;
using Microsoft.Win32;
using System.Data;
using System.Web;
using System.Text.RegularExpressions;


namespace InsightoV2.ExportSurveyReports
{
    public class ValidationCore
    {
        public ValidationCore()
        {
            
        }
       
        //public void ValidateQuestion(string controlID, int QuesType, HtmlTableCell cntrlCell, int AnsOptCnt, int CharLimit)
        //{
        //    RegularExpressionValidator Regex = new RegularExpressionValidator();
        //    Regex.ID = "ctrlRegex" + controlID;

        //    if (QuesType == 5)//Comments and suggeations
        //    {
        //        if (CharLimit == 0 || CharLimit < 0)
        //            CharLimit = 2000;
        //        Regex.ControlToValidate = controlID;
        //        //Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s]{1," + CharLimit + "}";
        //        Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s!@$%&()-_+=;""<>?/.]{1," + CharLimit + "}";
        //        //Regex.ErrorMessage = "Allowed Alphenumerics  and maximum  char limit" + CharLimit + "only";

        //        Regex.ErrorMessage = "You have exceeded the maximum character limit of " + CharLimit + ". Please revise your answer.";
        //        //below actual latest messge
        //        //Regex.ErrorMessage = "Maximum character limit is" + CharLimit + "only";
        //        Regex.Display = ValidatorDisplay.Dynamic;

        //    }
        //    if (QuesType == 6)//Single row Question
        //    {
        //        if (CharLimit == 0 || CharLimit < 0)
        //            CharLimit = 500;
        //        Regex.ControlToValidate = controlID;
        //        //Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s]{1," + CharLimit + "}";
        //        Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s!@$%&()-_+=;""<>?/.]{1," + CharLimit + "}";
        //        //Regex.ErrorMessage = "Allowed Alphenumerics  and maximum  char limit " + CharLimit + " only";
        //        Regex.ErrorMessage = "You have exceeded the maximum character limit of " + CharLimit + ". Please revise your answer.";
        //        //Regex.ErrorMessage = "Maximum character limit is" + CharLimit + "only";
        //        Regex.Display = ValidatorDisplay.Dynamic;
        //    }
        //    if (QuesType == 7)//Numeric Input
        //    {
        //        Regex.ControlToValidate = controlID;
        //        Regex.ValidationExpression = @"[0-9]{1,20}";
        //        Regex.ErrorMessage = " Only numeric value (numbers only) up to 20 digits allowed";
        //        //Regex.ErrorMessage = "Allowed numerics upto 20 digits only";
        //        Regex.Display = ValidatorDisplay.Dynamic;

        //    }
        //    if (QuesType == 20)
        //    {
        //        Regex.ControlToValidate = controlID;
        //        Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s!@$%&()-_+=;""<>?/.]{1}";
        //        Regex.ErrorMessage = "You have exceeded the maximum character limit of 500. Please revise your answer.";
        //        //Regex.ErrorMessage = "Allowed numerics upto 20 digits only";
        //        Regex.Display = ValidatorDisplay.Dynamic;

        //    }
        //    if (QuesType == 8)//Email address
        //    {
        //        Regex.ControlToValidate = controlID;
        //        Regex.ValidationExpression = @"[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}";
        //        //Regex.ErrorMessage = "Invalid email address";
        //        Regex.ErrorMessage = "Invalid email address format";
        //        Regex.Display = ValidatorDisplay.Dynamic;

        //    }
        //    //Lines for validating QuestionTypeNine(Open ended One or more lines with prompt)
        //    if (QuesType == 9)
        //    {
        //        if (CharLimit == 0 || CharLimit < 0)
        //            CharLimit = 500;
        //        Regex.ControlToValidate = controlID;
        //        //Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s]{1," + CharLimit + "}";
        //        Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s!@$%&()-_+=;""<>?/.]{1," + CharLimit + "}";
        //        //Regex.ErrorMessage = "Allowed Alphenumerics  and maximum  char limit " + CharLimit + " only";
        //        Regex.ErrorMessage = "You have exceeded the maximum character limit of " + CharLimit + ". Please revise your answer.";
        //        //Regex.ErrorMessage = "Maximum character limit is" + CharLimit + "only";

        //        Regex.Display = ValidatorDisplay.Dynamic;

        //    }
        //    //Block Ended
        //    if (QuesType == 13)//Constant Sum
        //    {
        //        if (CharLimit == 0 || CharLimit < 0)
        //            CharLimit = 100;
        //        Regex.ControlToValidate = controlID;
        //        Regex.ValidationExpression = @"[0-9]{1,20}";
        //        //Regex.ErrorMessage = "Allowed numbers only and sum must be" + CharLimit;
        //        Regex.ErrorMessage = "Only numeric value (numbers only) allowed – values must add up to " + CharLimit;
        //        //Only numeric value (numbers only) allowed – values must add up to X.
        //        Regex.Display = ValidatorDisplay.Dynamic;
        //    }
        //    if (QuesType == 15)//Ranking
        //    {
        //        Regex.ControlToValidate = controlID;
        //        Regex.ValidationExpression = @"[0-9]{1,20}";
        //        //Regex.ErrorMessage = "Allowed numbers only and rankings must in between 1 and " + AnsOptCnt;
        //        Regex.ErrorMessage = "Only numeric value (numbers only) allowed – ranking must be between 1 and " + AnsOptCnt;
        //        //Only numeric value (numbers only) allowed – ranking must be between 1 and X.
        //        Regex.Display = ValidatorDisplay.Dynamic;
        //    }
        //    if (QuesType == 14)//Date
        //    {
        //        Regex.ControlToValidate = controlID;
        //        //="(0?[1-9]|[12][0-9]|3[01])[- /.](0?[1-9]|1[012])[- /.](19|20)?\d\d"
        //        //DateTime dt; bool isValid = DateTime.TryParseExact("08/30/2009", "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);

        //        Regex.ValidationExpression = @"([0-9]{1,2})/([0-9]{1,2})/([0-9]{4,4})";
        //        //Regex.ErrorMessage = "Invalid Date format";
        //        Regex.ErrorMessage = "Incorrect Date format";
        //        Regex.Display = ValidatorDisplay.Dynamic;
        //    }
        //    //cntrlCell.Controls.Add(ReqFld);
        //    cntrlCell.Controls.Add(Regex);
        //}
        //public void ValidateContactInfoQuestion(string controlID, int QuesType, HtmlTableCell cntrlCell, int CharLimit, string errortext)
        //{
        //    //RequiredFieldValidator ReqFld = new RequiredFieldValidator();
        //    //ReqFld.ID = "cntrlReqfldvld" + controlID;
        //    //ReqFld.ControlToValidate = controlID;
        //    //ReqFld.ErrorMessage = "Please Enter Valid MailID";
        //    //ReqFld.Display = ValidatorDisplay.Dynamic;

        //    RegularExpressionValidator Regex = new RegularExpressionValidator();
        //    Regex.ID = "ctrlRegex" + controlID;
        //    if (QuesType == 20)//Date
        //    {
        //        Regex.ControlToValidate = controlID;
        //        Regex.ValidationExpression = @"[0-9a-zA-Z''-'\s!@$%&()-_+=;""<>?/.]{1,500}";
        //        Regex.ErrorMessage = errortext + " is Mandatory";
        //        //Regex.ErrorMessage = "Allowed numerics upto 20 digits only";
        //        Regex.Display = ValidatorDisplay.Dynamic;
        //    }
        //    if (QuesType == 7)//Numeric Input
        //    {
        //        Regex.ControlToValidate = controlID;
        //        Regex.ValidationExpression = @"[0-9]{1,20}";
        //        Regex.ErrorMessage = " Only numeric value (numbers only) up to 20 digits allowed";
        //        //Regex.ErrorMessage = "Allowed numerics upto 20 digits only";
        //        Regex.Display = ValidatorDisplay.Dynamic;

        //    }
        //    cntrlCell.Controls.Add(Regex);

        //}

        //public string clearHTML1(string HTMLcontent)
        //{
        //         Dim objRegExp, strOutput
        //Set objRegExp = New Regexp

        //objRegExp.IgnoreCase = True
        //objRegExp.Global = True
        //objRegExp.Pattern = "<(.|\n)+?>"

        //'Replace all HTML tag matches with the empty string
        //strOutput = objRegExp.Replace(strHTML, "")

        //'Replace all < and > with &lt; and &gt;
        //strOutput = Replace(strOutput, "<", "&lt;")
        //strOutput = Replace(strOutput, ">", "&gt;")

        //stripHTML = strOutput    'Return the value of strOutput

        //Set objRegExp = Nothing
        //Regex objRegExp = new Regex();


        //  }
        //return Regex.Replace(text, @”<(.|\n)*?>”, string.Empty);
        //   Private Function ConvertHtmlToPlainText(ByVal htmlText As String) As String 
        //    Return Regex.Replace(htmlText, "<[^>]*>", String.Empty) 
        //End Function 

        public string stripfonttags(string HTMLcontent)
        {
            string pattern1html = "</?font[^>]*>";
            string plainHTML = "";
            plainHTML = Regex.Replace(HTMLcontent, pattern1html, string.Empty);
            return pattern1html;
        }
        public string clearHTML(string HTMLcontent)
        {
            //System.Text.RegularExpressions.Regex.Replace(HTMLcontent, "<span[^>]*>", string.Empty);
            //return Regex.Replace(HTMLcontent, @"<[/]?(form)[^>]*?>", "", RegexOptions.IgnoreCase);
            //return Regex.Replace(HTMLcontent, @"^<[/]?[.*]>", "",RegexOptions.IgnoreCase);
            //return (HTMLcontent);
            string plainHTML = "";
            if (HTMLcontent != null)
            {
                //HTMLcontent = HTMLcontent.Replace("&lt;","<");
                //HTMLcontent = HTMLcontent.Replace("&gt;",">");
                string pattern1html = "<(.|\n)+?>";
                plainHTML = Regex.Replace(HTMLcontent, pattern1html, string.Empty);
                string patternHtml = "<.*?>";
                string patternNewline = "\".";
                string patternNbsp = "&.*";
                //HTMLcontent = HTMLcontent.Replace("&lt;", "<");
                //HTMLcontent = HTMLcontent.Replace("&gt;", ">");
                plainHTML = Regex.Replace(plainHTML, patternHtml, string.Empty);

                //string patternHtml = "</?(?i:script|embed|object|frameset|frame|iframe|meta|link|style)(.|\n)*?>";
                //plainHTML = Regex.Replace(HTMLcontent, patternHtml, string.Empty);
                plainHTML = plainHTML.Replace("&lt;", "<");
                plainHTML = plainHTML.Replace("&gt;", ">");
                plainHTML = plainHTML.Replace("&quot;", "\"");
                plainHTML = plainHTML.Replace("&apos;", "'");
                plainHTML = plainHTML.Replace("&amp;", "&");
                plainHTML = plainHTML.Replace("&nbsp;", " ");
                plainHTML = plainHTML.Replace("&copy;", "\u00a9");
                //plainHTML = plainHTML.Replace("<p>", "");
                //plainHTML = plainHTML.Replace("</p>", "");


                //plainHTML = Regex.Replace(HTMLcontent, patternHtml, string.Empty);
                ////plainHTML = plainHTML.Replace("&lt;", "<");
                //plainHTML = Regex.Replace(plainHTML, patternNewline, string.Empty);
                //plainHTML = Regex.Replace(plainHTML, patternNbsp, string.Empty);
            }
            return (plainHTML);
        }
        //public HtmlTable GenerateProgressBar(int PresentQuesNoCnt, int TotQuesNoCnt)
        //{
        //    float result = (PresentQuesNoCnt * 100 / TotQuesNoCnt);//calculations 
        //    float spent = 100 - result;          //result holders
        //    StringBuilder s = new StringBuilder();
        //    HtmlTextWriter writer = new HtmlTextWriter(new System.IO.StringWriter(s, System.Globalization.CultureInfo.InvariantCulture));
        //    s.Append("<table border=\"0\" width=\"300\">");
        //    s.Append("<tr>");
        //    s.Append("<td width=\"");
        //    s.Append(result);
        //    if (result == 100)
        //        s.Append("%\" bgcolor=LimeGreen height=\"12\"></td>");
        //    else
        //        s.Append("%\" bgcolor=Orange  height=\"12\"></td>");
        //    s.Append("<td width=\"");
        //    s.Append(spent);
        //    s.Append("%\" bgcolor=LightGoldenrodYellow >");
        //    s.Append("</tr>");
        //    s.Append("</table>");

        //    s.Append("<table border=\"0\" width=\"300\">" +
        //          "<tr><td width=\"100%\">");
        //    s.Append("<p dir=\"ltr\" align=\"center\">");
        //    s.Append("</font><font size=\"3\" face=\"Impact\" " +
        //           "color=\"#FF9933\">");
        //    s.Append(" </font><font color=\"#FF6600\" size=\"4\">");
        //    s.Append(result.ToString());// percentage
        //    s.Append("<font size=\"3\" color=\"#FF6600\">");
        //    s.Append(" % </font><font size=\"3\"  color=\"#8585AD\" " +
        //         ">                Viewed ");
        //    s.Append(" (" + PresentQuesNoCnt + " / " + TotQuesNoCnt + ")");// acutally data
        //    s.Append("</font></td></tr></table>");
        //    s.Append("</center>");
        //    HtmlTable tableProgressBar = new HtmlTable();
        //    HtmlTableRow RowProgressBar = new HtmlTableRow();
        //    HtmlTableCell CellProgressBar = new HtmlTableCell();
        //    CellProgressBar.InnerHtml = Convert.ToString(s);
        //    RowProgressBar.Cells.Add(CellProgressBar);
        //    tableProgressBar.Rows.Add(RowProgressBar);
        //    return tableProgressBar;
        //}

       

    }
}
