﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;

using System.Configuration;  

namespace InsightoV2.ExportSurveyReports
{
    public class ExportSurveyCore
    {
        DBManager dbm = new DBManager();
        CultureInfo ci = new CultureInfo("en-us");
        string Logpath = "";
        DataSet dsq = new DataSet();
        string strMailSettingsPath = ConfigurationSettings.AppSettings["MailSettings"].ToString();
        public Survey GetSurveyDet(int SurveyID)
        {
            //str += " SurQue.QUESTION_ID,SurQue.QUESTION_TYPE_ID,SurQue.QUESTION_SEQ,SurQue.QUSETION_LABEL,SurQue.SKIP_LOGIC,SurQue.RESPONSE_REQUIRED,SurQue.PAGE_BREAK,";
            //string st = " select SurCon.SURVEYCONTROL_ID,SurCon.SCHEDULE_ON,SurCon.SCHEDULE_ON_ALERT,SurCon.SCHEDULE_ON_CLOSE,";
            //st += " SurCon.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS,SurCon.SCHEDULE_ON_CLOSE_ALERT,SurCon.TIME_FOR_URL_REDIRECTION,";
            //st += " SurCon.SCREEN_OUTS,SurCon.PARTIAL_RESPONSES,SurCon.TOTAL_RESPONSES,SurCon.SEND_TO,SurCon.ONLAUNCH_ALERT,";
            //st += " SurCon.CLOSE_ON_NO_RESPONDENTS,SurCon.CONTINUOUS_SURVEY,SurCon.UNIQUE_RESPONDENT,SurCon.SAVE_AND_CONTINUE,";
            //st += " SurCon.AUTOMATIC_THANKYOU_EMAIL,SurCon.ALERT_ON_MIN_RESPONSES,SurCon.ALERT_ON_MAX_RESPONSES,";
            //st += " SurCon.THANKYOU_PAGE,SurCon.SCREENOUT_PAGE,SurCon.CLOSE_PAGE,SurCon.OVERQUOTA_PAGE,SurCon.UNSUBSCRIBED,";
            //st += " SurCon.EMAIL_INVITATION,SurCon.SURVEY_PASSWORD,SurCon.SURVEY_URL,SurCon.REMINDER_TEXT,SurCon.LAUNCH_ON_DATE,";
            //st += " SurCon.CLOSE_ON_DATE,SurCon.LAUNCHED_ON,SurCon.ALERT_ON_MIN_RESPONSES_BY_DATE,SurCon.ALERT_ON_MAX_RESPONSES_BY_DATE,";
            //st += " SurCon.REMINDER_ON_DATE from osm_surveycontrols SurCon";
            //str += " left outer join osm_surveyquestion SurQue on SurQue.SURVEY_ID=sur.SURVEY_ID ";
            //str += " left outer join osm_surveycontrols SurCon on SurCon.SURVEY_ID=sur.SURVEY_ID ";
            try
            {
                // dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                Survey sur = new Survey();
                if (SurveyID > 0)
                {
                    //string str = " select sur.SURVEY_NAME,sur.SURVEY_CATEGORY,sur.THEME,sur.FOLDER_ID,sur.USERID,sur.CREATED_ON,sur.STATUS,sur.CHECK_BLOCKWORDS,SurIntro.SURVEY_INTRO ";
                    //str += " from osm_survey sur ";
                    string str = " select sur.SURVEY_NAME,sur.SURVEY_CATEGORY,sur.THEME,sur.FOLDER_ID,sur.USERID,sur.CREATED_ON,sur.STATUS,sur.CHECK_BLOCKWORDS,usr.LOGIN_NAME,usr.FIRST_NAME,usr.LAST_NAME,STANDARD_BIAS,TIME_ZONE ";
                    str += " from osm_survey sur left outer join osm_user usr on sur.USERID=usr.USERID  ";
                    str += " where sur.SURVEY_ID= " + SurveyID;
                    str += " select * from osm_surveyIntro where DELETED=0 and SURVEY_ID= " + SurveyID;
                    //string surIntro = " select * from osm_surveyIntro where DELETED=0 and SURVEY_ID= " + SurveyID;
                    string surSet = " select * from osm_surveysetting where DELETED=0 and SURVEY_ID= " + SurveyID;
                    //order by is written by sandeep
                    string Queqry = " select * from osm_surveyquestion where DELETED=0 and SURVEY_ID= " + SurveyID;
                    string Conqry = " select * from osm_surveycontrols where DELETED=0 and SURVEY_ID= " + SurveyID;

                    DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        sur.USER_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["USERID"]);
                        sur.SURVEY_ID = SurveyID;
                        sur.FOLDER_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["FOLDER_ID"]);
                        sur.STATUS = Convert.ToString(ds.Tables[0].Rows[0]["STATUS"]);
                        sur.SURVEY_CATEGORY = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_CATEGORY"]);
                        sur.THEME = Convert.ToString(ds.Tables[0].Rows[0]["THEME"]);
                        sur.LOGIN_NAME = Convert.ToString(ds.Tables[0].Rows[0]["LOGIN_NAME"]);
                        sur.FIRST_NAME = Convert.ToString(ds.Tables[0].Rows[0]["FIRST_NAME"]);
                        sur.LAST_NAME = Convert.ToString(ds.Tables[0].Rows[0]["LAST_NAME"]);
                        sur.TIME_ZONE = Convert.ToString(ds.Tables[0].Rows[0]["TIME_ZONE"]);
                        sur.STANDARD_BIAS = Convert.ToInt32(ds.Tables[0].Rows[0]["STANDARD_BIAS"]);
                        if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                        {
                            sur.SURVEY_INTRO = Convert.ToString(ds.Tables[1].Rows[0]["SURVEY_INTRO"]);
                            if (ds.Tables[1].Rows[0]["PAGE_BREAK"] != null)
                            {
                                sur.SURVEYINTRO_PAGEBREAK = Convert.ToInt32(ds.Tables[1].Rows[0]["PAGE_BREAK"]);
                            }
                        }
                        sur.SURVEY_NAME = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_NAME"]);
                        sur.CHECK_BLOCKWORDS = Convert.ToInt32(ds.Tables[0].Rows[0]["CHECK_BLOCKWORDS"]);

                        DataSet surSetds = dbm.ExecuteDataSet(CommandType.Text, surSet);
                        if (surSetds != null && surSetds.Tables.Count > 0 && surSetds.Tables[0].Rows.Count > 0)
                        {
                            sur.SURVEY_HEADER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_HEADER"]);
                            sur.SURVEY_FOOTER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FOOTER"]);
                            sur.SURVEY_FONT_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_TYPE"]);
                            sur.SURVEY_FONT_SIZE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_SIZE"]);
                            sur.SURVEY_FONT_COLOR = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_COLOR"]);
                            sur.SURVEY_BUTTON_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_BUTTON_TYPE"]);
                            sur.CUSTOMSTART_TEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSTART_TEXT"]);
                            sur.CUSTOMSUBMITTEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSUBMITTEXT"]);
                            sur.PAGE_BREAK = Convert.ToInt32(surSetds.Tables[0].Rows[0]["PAGE_BREAK"]);
                            sur.RESPONSE_REQUIRED = Convert.ToInt32(surSetds.Tables[0].Rows[0]["RESPONSE_REQUIRED"]);
                            sur.BROWSER_BACKBUTTON_STATUS = Convert.ToInt32(surSetds.Tables[0].Rows[0]["BROWSER_BACKBUTTON"]);
                            sur.SURVEY_LOGO = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_LOGO"]);
                        }
                        else
                        {
                            sur.BROWSER_BACKBUTTON_STATUS = 1;
                        }
                        DataSet Quesds = dbm.ExecuteDataSet(CommandType.Text, Queqry);
                        if (Quesds != null && Quesds.Tables.Count > 0 && Quesds.Tables[0].Rows.Count > 0)
                        {
                            sur.surveyQues = new List<SurveyQuestion>();
                            for (int i = 0; i < Quesds.Tables[0].Rows.Count; i++)
                            {
                                SurveyQuestion ques = new SurveyQuestion();
                                ques.CREATED_ON = Convert.ToDateTime(Quesds.Tables[0].Rows[i]["CREATED_ON"]);
                                ques.LASTMODIFIED_ON = Convert.ToDateTime(Quesds.Tables[0].Rows[i]["LASTMODIFIED_ON"]);
                                ques.PAGE_BREAK = Convert.ToInt32(Quesds.Tables[0].Rows[i]["PAGE_BREAK"]);
                                ques.QUESTION_ID = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_ID"]);
                                ques.QUESTION_LABEL = Convert.ToString(Quesds.Tables[0].Rows[i]["QUSETION_LABEL"]);
                                ques.QUESTION_SEQ = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_SEQ"]);
                                ques.QUESTION_TYPE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_TYPE_ID"]);
                                ques.RESPONSE_REQUIRED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RESPONSE_REQUIRED"]);
                                ques.RANDOM_ANSWERS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RANDOMIZE_ANSWERS"]);
                                ques.OTHER_ANS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["OTHER_ANS"]);
                                ques.SKIP_LOGIC = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SKIP_LOGIC"]);
                                ques.SURVEY_ID = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SURVEY_ID"]);
                                ques.CHART_ALIGNMENT = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_ALIGNMENT"]);
                                ques.CHART_APPEARANCE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_APPEARANCE"]);
                                ques.CHART_DOCK = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_DOCK"]);
                                ques.CHART_PALETTE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_PALETTE"]);
                                ques.CHART_TITLE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_TITLE"]);
                                ques.CHART_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_TYPE"]);
                                ques.COMMENTS = Convert.ToString(Quesds.Tables[0].Rows[i]["COMMENTS"]);
                                ques.CHAR_LIMIT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["CHAR_LIMIT"]);
                                ques.ANSWER_ALIGNSTYLE = Convert.ToString(Quesds.Tables[0].Rows[i]["ANSWER_ALIGNSTYLE"]);
                                ques.HIDE_LEGAND = Convert.ToInt32(Quesds.Tables[0].Rows[i]["HIDE_LEGAND"]);
                                ques.HIDE_XAXIS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["HIDE_XAXIS"]);
                                ques.STAGGERED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["STAGGERED"]);

                                ques.SHOW_LABEL = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SHOW_LABEL"]);
                                ques.LABEL_ANGLE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LABEL_ANGLE"]);
                                ques.VALUEAS_PERCENT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["VALUEAS_PERCENT"]);
                                ques.ZOOMPERCENT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["ZOOMPERCENT"]);
                                ques.TRANSPARENCY = Convert.ToInt32(Quesds.Tables[0].Rows[i]["TRANSPARENCY"]);
                                ques.PERSPECTIVE_ANGLE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["PERSPECTIVE_ANGLE"]);
                                ques.MARKER_SIZE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["MARKER_SIZE"]);
                                ques.SHOW_MARKER = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SHOW_MARKER"]);
                                ques.INVERTED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["INVERTED"]);
                                ques.LABEL_POSITION = Convert.ToString(Quesds.Tables[0].Rows[i]["LABEL_POSITION"]);
                                ques.MARKER_KIND = Convert.ToString(Quesds.Tables[0].Rows[i]["MARKER_KIND"]);
                                ques.HOLE_RADIUS = Convert.ToString(Quesds.Tables[0].Rows[i]["HOLE_RADIUS"]);
                                ques.DIAGRAM_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["DIAGRAM_TYPE"]);
                                ques.TEXT_DIRECTION = Convert.ToString(Quesds.Tables[0].Rows[i]["TEXT_DIRECTION"]);
                                ques.FUNCTION_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["FUNCTION_TYPE"]);
                                ques.EXPLODED_POINT = Convert.ToString(Quesds.Tables[0].Rows[i]["EXPLODED_POINT"]);
                                ques.LEGEND_DIRECTION = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_DIRECTION"]);
                                ques.LEGEND_EQUALLYSPACED_ITEMS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_EQUALLYSPACED_ITEMS"]);
                                ques.LEGEND_HORIZONTALALIGN = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_HORIZONTALALIGN"]);
                                ques.LEGEND_MAX_HORIZONTALALIGN = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_MAX_HORIZONTALALIGN"]);
                                ques.LEGEND_MAX_VERTICALALIGN = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_MAX_VERTICALALIGN"]);
                                ques.LEGEND_VERTICALALIGN = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_VERTICALALIGN"]);

                                string Ansqry = " select * from osm_answeroptions where DELETED=0 and QUESTION_ID= " + ques.QUESTION_ID + " order by ANSWER_ID asc";
                                //string Ansqry = " select * from osm_answeroptions where DELETED=0 and QUESTION_ID= " + ques.QUESTION_ID;
                                DataSet Ansds = dbm.ExecuteDataSet(CommandType.Text, Ansqry);
                                if (Ansds != null && Ansds.Tables.Count > 0 && Ansds.Tables[0].Rows.Count > 0)
                                {
                                    ques.surveyAnswers = new List<SurveyAnswers>();
                                    for (int y = 0; y < Ansds.Tables[0].Rows.Count; y++)
                                    {
                                        SurveyAnswers Ansr = new SurveyAnswers();
                                        Ansr.ANSWER_ID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["ANSWER_ID"]);
                                        Ansr.ANSWER_OPTIONS = Convert.ToString(Ansds.Tables[0].Rows[y]["ANSWER_OPTIONS"]);
                                        Ansr.CREATED_ON = Convert.ToDateTime(Ansds.Tables[0].Rows[y]["CREATED_ON"]);
                                        Ansr.LAST_MODIFIED_ON = Convert.ToDateTime(Ansds.Tables[0].Rows[y]["LAST_MODIFIED_ON"]);
                                        Ansr.QUESTION_ID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["QUESTION_ID"]);
                                        Ansr.SKIP_QUESTION_ID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["SKIP_QUESTION_ID"]);
                                        Ansr.DEMOGRAPIC_BLOCKID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["DEMOGRAPIC_BLOCKID"]);
                                        Ansr.PIPING_TEXT = Convert.ToString(Ansds.Tables[0].Rows[y]["PIPING_TEXT"]);
                                        Ansr.RESPONSE_REQUIRED = Convert.ToInt32(Ansds.Tables[0].Rows[y]["RESPONSE_REQUIRED"]);
                                        ques.surveyAnswers.Add(Ansr);
                                    }
                                }
                                sur.surveyQues.Add(ques);
                            }
                        }

                        DataSet Controlsds = dbm.ExecuteDataSet(CommandType.Text, Conqry);
                        if (Controlsds != null && Controlsds.Tables.Count > 0 && Controlsds.Tables[0].Rows.Count > 0)
                        {
                            sur.surveyOptns = new List<SurveyOptions>();
                            for (int j = 0; j < Controlsds.Tables[0].Rows.Count; j++)
                            {
                                SurveyOptions opt = new SurveyOptions();
                                //opt.ALERT_ON_MAX_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ALERT_ON_MAX_RESPONSES"]);
                                //opt.ALERT_ON_MAX_RESPONSES_BY_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["ALERT_ON_MAX_RESPONSES_BY_DATE"]);
                                //opt.ALERT_ON_MIN_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ALERT_ON_MIN_RESPONSES"]);
                                //opt.ALERT_ON_MIN_RESPONSES_BY_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["ALERT_ON_MIN_RESPONSES_BY_DATE"]);
                                //opt.AUTOMATIC_THANKYOU_EMAIL = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["AUTOMATIC_THANKYOU_EMAIL"]);
                                //opt.CLOSE_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["CLOSE_ON_DATE"]);
                                opt.CLOSE_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["CLOSE_PAGE"]);
                                opt.EMAIL_INVITATION = Convert.ToString(Controlsds.Tables[0].Rows[j]["EMAIL_INVITATION"]);

                                opt.EMAIL_FROMADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["FROM_MAILADDRESS"]);
                                opt.EMAIL_SUBJECT = Convert.ToString(Controlsds.Tables[0].Rows[j]["MAIL_SUBJECT"]);

                                opt.LAUNCH_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["LAUNCH_ON_DATE"]);

                                // opt.LAUNCHED_ON = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["LAUNCHED_ON"]);

                                //opt.NOOFRESPONSESTOCLOSEE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS"]);
                                //opt.ONCLOSEALERT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE_ALERT"]);
                                //opt.ONLAUNCH_ALERT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ONLAUNCH_ALERT"]);
                                //above line to as the field updating on surveycontrolspage is SCHEDULE_ON_ALERT
                                //opt.ONLAUNCH_ALERT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_ALERT"]);
                                //opt.OVERQUOTA_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["OVERQUOTA_PAGE"]);
                                //opt.PARTIAL_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["PARTIAL_RESPONSES"]);
                                //opt.REMINDER_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["REMINDER_ON_DATE"]);
                                //opt.REMINDER_TEXT = Convert.ToString(Controlsds.Tables[0].Rows[j]["REMINDER_TEXT"]);
                                //opt.SAVE_AND_CONTINUE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SAVE_AND_CONTINUE"]);
                                //opt.SCREEN_OUTS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCREEN_OUTS"]);
                                opt.SCREENOUT_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["SCREENOUT_PAGE"]);
                                //opt.SEND_TO = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SEND_TO"]);
                                //opt.STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["STATUS"]);
                                opt.SURVEY_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEY_ID"]);
                                opt.SURVEY_PASSWORD = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEY_PASSWORD"]);
                                opt.SURVEY_URL = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEY_URL"]);
                                opt.SURVEYCLOSETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE"]);
                                opt.SURVEYCONTROL_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEYCONTROL_ID"]);
                                opt.SURVEYLAUNCHTYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON"]);
                                opt.THANKYOU_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["THANKYOU_PAGE"]);
                                //opt.TIME_FOR_URL_REDIRECTION = Convert.ToString(Controlsds.Tables[0].Rows[j]["TIME_FOR_URL_REDIRECTION"]);
                                opt.TOTAL_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["TOTAL_RESPONSES"]);
                                opt.UNIQUE_RESPONDENT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["UNIQUE_RESPONDENT"]);
                                opt.UNSUBSCRIBED = Convert.ToString(Controlsds.Tables[0].Rows[j]["UNSUBSCRIBED"]);
                                opt.URL_REDIRECTION = Convert.ToString(Controlsds.Tables[0].Rows[j]["URL_REDIRECTION"]);
                                opt.THANKU_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["THANKUPAGE_TYPE"]);
                                opt.CLOSE_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["CLOSEPAGE_TYPE"]);
                                opt.SCREENOUT_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCREENOUTPAGE_TYPE"]);
                                opt.OVERQUOTA_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["OVERQUOTAPAGE_TYPE"]);
                                //opt.URLREDIRECTION_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["URLREDIRECTION_STATUS"]);
                                //opt.FINISHOPTIONS_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["FINISHOPTIONS_STATUS"]);
                                //opt.CONTACTLIST_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["CONTACTLIST_ID"]);
                                //opt.SURVEYLAUNCH_URL = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEYLAUNCH_URL"]);
                                opt.EMAILLIST_TYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["EMAILLIST_TYPE"]);
                                //opt.SURVEYURL_TYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEYURL_TYPE"]);
                                //opt.LAUNCHOPTIONS_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["LAUNCHOPTIONS_STATUS"]);

                                opt.EMAIL_SENDERNAME = Convert.ToString(Controlsds.Tables[0].Rows[j]["SENDER_NAME"]);
                                opt.EMAIL_REPLYTOADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["REPLY_TO_EMAILADDRESS"]);

                                //  opt.BOUNCED = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["BOUNCED"]);
                                //  opt.DUPLICATED = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["DUPLICATED"]);
                                //  opt.ENABLEPASSWORD = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ENABLEPASSWORD"]);
                                //  opt.PENDING_VERFICATION = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["PENDING_VERFICATION"]);
                                //  opt.THEME_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["THEME_ID"]);
                                sur.surveyOptns.Add(opt);
                            }
                        }
                    }
                }
                return sur;
            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt");
                return null;
            }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }
        }


        public Survey GetSurveyWithResponsesCount(int SurveyID, DateTime Startdate, DateTime Enddate)
        {
            try
            {

                Survey Sur = GetSurveyDet(SurveyID);
                string RespondentIDS = GetActiveRespondent(SurveyID);
                //dbm.OpenConnection(connect);
                //dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                if (Sur != null && Sur.SURVEY_ID > 0 && Sur.surveyQues != null && Sur.surveyQues.Count > 0 && RespondentIDS != null && RespondentIDS.Length > 0)
                {
                    foreach (SurveyQuestion surQues in Sur.surveyQues)
                    {
                        int QuesResCount = 0, AnsResCount = 0;
                        string qryQuesResp="";
                        if(surQues.QUESTION_TYPE ==2)
                        {
                            qryQuesResp = "select count(Distinct RESPONDENT_ID) from osm_responsequestions where QUESTION_ID=" + surQues.QUESTION_ID + " and deleted=0";
                            if (RespondentIDS != null && RespondentIDS.Length > 0)
                                qryQuesResp += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                        }
                        else
                        {
                         qryQuesResp = "select count(Distinct RESPONDENT_ID) as RESPONSE_COUNT from osm_responsequestions where DELETED=0 and QUESTION_ID=" + surQues.QUESTION_ID;
                        if (RespondentIDS != null && RespondentIDS.Length > 0)
                            qryQuesResp += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                        if (Startdate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM") && Enddate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                        {
                            qryQuesResp += " and RESPONSE_RECEIVED_ON>='" + Startdate + "' and RESPONSE_RECEIVED_ON<='" + Enddate + "'";
                        }
                        }
                        object QCount = dbm.ExecuteScalar(CommandType.Text, qryQuesResp);
                        try
                        {
                            if (QCount != null)
                            {
                                QuesResCount = Convert.ToInt32(QCount);
                            }
                        }
                        catch
                        {
                        }
                        surQues.RESPONSE_COUNT = QuesResCount;
                        if (surQues.surveyAnswers != null && surQues.surveyAnswers.Count > 0)
                        {
                            foreach (SurveyAnswers ans in surQues.surveyAnswers)
                            {
                                //string qryAnsResp = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where ANSWER_ID=" + ans.ANSWER_ID + " and RESPONDENT_ID in(" + RespondentIDS + ")";
                                string qryAnsResp = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where DELETED=0 and ANSWER_ID=" + ans.ANSWER_ID;
                                if (RespondentIDS != null && RespondentIDS.Length > 0)
                                    qryAnsResp += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                                if (Startdate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM") && Enddate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                                {
                                    qryAnsResp += " and RESPONSE_RECEIVED_ON >='" + Startdate + "' and RESPONSE_RECEIVED_ON <='" + Enddate + "'";
                                }
                                object ACount = dbm.ExecuteScalar(CommandType.Text, qryAnsResp);
                                try
                                {
                                    if (ACount != null)
                                    {
                                        AnsResCount = Convert.ToInt32(ACount);
                                    }
                                }
                                catch { }
                                ans.RESPONSE_COUNT = AnsResCount;
                            }
                            if (surQues.OTHER_ANS == 1)
                            {
                                if (surQues.QUESTION_TYPE == 1 || surQues.QUESTION_TYPE == 2 || surQues.QUESTION_TYPE == 3 || surQues.QUESTION_TYPE == 4)
                                {
                                    //string qryAnsRespother = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where ANSWER_ID=0 and QUESTION_ID=" + surQues.QUESTION_ID + " and RESPONDENT_ID in(" + RespondentIDS + ")";
                                    string qryAnsRespother = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where ANSWER_ID=0 and QUESTION_ID=" + surQues.QUESTION_ID;
                                    if (RespondentIDS != null && RespondentIDS.Length > 0)
                                        qryAnsRespother += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                                    if (Startdate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM") && Enddate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                                    {
                                        qryAnsRespother += " and RESPONSE_RECEIVED_ON >='" + Startdate + "' and RESPONSE_RECEIVED_ON <='" + Enddate + "'";
                                    }
                                    object ACountothers = dbm.ExecuteScalar(CommandType.Text, qryAnsRespother);
                                    try
                                    {
                                        if (ACountothers != null)
                                        {
                                            surQues.OTHER_RESPCOUNT = Convert.ToInt32(ACountothers);
                                        }
                                    }
                                    catch { }

                                }
                            }
                        }
                    }
                }
                return Sur;
            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt");
                return null;
            }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }
        }
        public SurveyRespondents GetRawDataExportResponses(int SurveyID, string Resptype)
        {
            try
            {
                //dbm.OpenConnection(connect);
                // dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                string str_respondents = "Select res.RESPONDENT_ID,res.EMAIL_ID,res.STATUS,res.CREATED_ON,res.SESSIONID,eml.EMAIL_ADDRESS,res.RANDOMSESSIONID from osm_responsesurvey res left outer join osm_emaillist eml on res.EMAIL_ID=eml.EMAIL_ID where SURVEY_ID=" + SurveyID + " and res.DELETED=0 and res.STATUS='" + Resptype + "'";
                str_respondents += " Select QUESTION_ID,QUESTION_TYPE_ID from osm_surveyquestion where SURVEY_ID=" + SurveyID + " and QUESTION_TYPE_ID IN('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','20') and DELETED=0 order by QUESTION_SEQ ";
                DataSet ds_respondents = dbm.ExecuteDataSet(CommandType.Text, str_respondents);
                string Ques_ids = "";
                SurveyRespondents Respond = new SurveyRespondents();

                DataSet ds_respques = new DataSet();
                if (ds_respondents != null && ds_respondents.Tables.Count > 1 && ds_respondents.Tables[1].Rows.Count > 0)
                {
                    for (int i = 0; i < ds_respondents.Tables[1].Rows.Count; i++)
                    {
                        Ques_ids += ",'" + ds_respondents.Tables[1].Rows[i]["QUESTION_ID"] + "'";
                    }

                    if (Ques_ids.Length > 0 && ds_respondents != null && ds_respondents.Tables.Count > 0 && ds_respondents.Tables[0].Rows.Count > 0)
                    {
                        string Resp_ids = "";
                        for (int i = 0; i < ds_respondents.Tables[0].Rows.Count; i++)
                        {
                            Resp_ids += ",'" + ds_respondents.Tables[0].Rows[i]["RESPONDENT_ID"] + "'";
                        }
                        if (Resp_ids != null && Resp_ids.Length > 0)
                        {
                            Resp_ids = Resp_ids.Substring(1);
                            string str_resques = "Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where RESPONDENT_ID IN(" + Resp_ids + ") and QUESTION_ID IN(" + Ques_ids.Substring(1) + ") and DELETED=0 order by RESPONDENT_ID,QUESTION_ID";
                            //string str_resques = "Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where RESPONDENT_ID IN(" + Resp_ids + ")  order by RESPONDENT_ID";
                            ds_respques = dbm.ExecuteDataSet(CommandType.Text, str_resques);
                        }
                        if (ds_respques != null && ds_respques.Tables.Count > 0 && ds_respques.Tables[0].Rows.Count > 0)
                        {
                            int k = 0;
                            Respond.SurveyResp = new List<SurveyResponse>();
                            //sur.surveyQues = new List<SurveyQuestion>();
                            for (int i = 0; i < ds_respondents.Tables[0].Rows.Count; i++)
                            {
                                SurveyResponse res = new SurveyResponse();
                                res.RESPONDENT_ID = Convert.ToInt32(ds_respondents.Tables[0].Rows[i]["RESPONDENT_ID"]);
                                res.SURVEY_ID = SurveyID;
                                // res.SESSION_ID = Convert.ToString(ds_respondents.Tables[0].Rows[i]["SESSIONID"]);
                                // res.RANDOM_SESSIONID = Convert.ToString(ds_respondents.Tables[0].Rows[i]["RANDOMSESSIONID"]);
                                res.EMAIL_ID = Convert.ToInt32(ds_respondents.Tables[0].Rows[i]["EMAIL_ID"]);
                                res.EMAIL_ADDRESS = Convert.ToString(ds_respondents.Tables[0].Rows[i]["EMAIL_ADDRESS"]);
                                res.STATUS = Convert.ToString(ds_respondents.Tables[0].Rows[i]["STATUS"]);
                                res.CREATED_ON = Convert.ToDateTime(ds_respondents.Tables[0].Rows[i]["CREATED_ON"]);
                                //res.RESPONDENT_ID = Convert.ToInt32(ds_respondents.Tables[0].Rows[i][""]);
                                res.ResponseQuestion = new List<ResponseQuestion>();
                                for (int m = k; m < ds_respques.Tables[0].Rows.Count; m++)
                                {
                                    if (Convert.ToInt32(ds_respques.Tables[0].Rows[m]["RESPONDENT_ID"]) != res.RESPONDENT_ID)
                                    {
                                        break;
                                    }
                                    ResponseQuestion ques_res = new ResponseQuestion();
                                    ques_res.RESPONSE_ID = Convert.ToInt32(ds_respques.Tables[0].Rows[m]["RESPONSE_ID"]);
                                    ques_res.RESPONDENT_ID = Convert.ToInt32(ds_respques.Tables[0].Rows[m]["RESPONDENT_ID"]);
                                    ques_res.QUESTION_ID = Convert.ToInt32(ds_respques.Tables[0].Rows[m]["QUESTION_ID"]);
                                    ques_res.ANSWER_ID = Convert.ToInt32(ds_respques.Tables[0].Rows[m]["ANSWER_ID"]);
                                    ques_res.ANSWER_TEXT = Convert.ToString(ds_respques.Tables[0].Rows[m]["ANSWER_TEXT"]);
                                    res.ResponseQuestion.Add(ques_res);
                                    k++;
                                }
                                Respond.SurveyResp.Add(res);
                            }

                        }
                    }
                }

                return Respond;
            }
            catch (Exception ex) { GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt"); return null; }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }
        }
        public DataSet GetConstantSumForReports(int QuestionID, ArrayList ans_options, int SurveyID)
        {
            try
            {
                string RespondentIDS = GetRespondentsForReports(QuestionID, SurveyID);
                // dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                // dbm.OpenConnection(connect);
                DataSet ds = new DataSet();
                //string qry = "Select RESPONDENT_ID,EMAIL_ID,STATUS,CREATED_ON from osm_responsesurvey where SURVEY_ID=" + SurveyID + "and STATUS='Complete'";
                if (RespondentIDS != null && RespondentIDS.Length > 0)
                {
                    string qry = " Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where QUESTION_ID=" + QuestionID + " and RESPONDENT_ID IN(" + RespondentIDS + ") and DELETED=0 order by RESPONDENT_ID";

                    ds = dbm.ExecuteDataSet(CommandType.Text, qry);

                }
                return ds;

            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt");
                return null;

            }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }

        }

        public string GetRespondentsForReports(int QuestionID, int SurveyID)
        {
            int cont = 0;
            string usrLmitDet = GetUserLimit(SurveyID, "");
            int usrLmit = 0; string LienTyp = "";
            string[] det = usrLmitDet.Split('$');
            if (det != null && det.Length > 0)
            {
                LienTyp = Convert.ToString(det.GetValue(1));
                usrLmit = Convert.ToInt32(det.GetValue(0));
            }
            string RespondentIDs = "";
            string qry = "";
            if (usrLmit != -1 && usrLmit > 0)
            {

                //qry = "select TOP "+usrLmit +" RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and VISABLE_LIMIT=1 and SURVEY_ID=" + SurveyID;
                qry = "select TOP " + usrLmit + " RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=(Select SURVEY_ID from osm_surveyquestion where QUESTION_ID=" + QuestionID + ")";
            }
            else if (usrLmit == -1)
            {
                qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=(Select SURVEY_ID from osm_surveyquestion where QUESTION_ID=" + QuestionID + ")";

            }



            //string RespondentIDs = "";
            //string qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and VISABLE_LIMIT=1 and SURVEY_ID=" + SurveyID;
            //string qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and VISABLE_LIMIT=1 and SURVEY_ID=(Select SURVEY_ID from osm_surveyquestion where QUESTION_ID=" + QuestionID + ")";
            try
            {
                //dbm.OpenConnection(connect);
                // dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qry);
                if (ds != null && ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        RespondentIDs += "," + Convert.ToString(ds.Tables[0].Rows[i]["RESPONDENT_ID"]);
                    }
                    if (RespondentIDs != null && RespondentIDs.Trim().Length > 0)
                        RespondentIDs = RespondentIDs.Substring(1);
                }
                return RespondentIDs;
            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt");
                return RespondentIDs;
            }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }
        }
        public DataSet GetRankingConstanSum(int QuestionID, ArrayList ans_options, int SurveyID)
        {
            try
            {
                string RespondentIDS = GetRespondentsForReports(QuestionID, SurveyID);
                // dbm.OpenConnection(connect);
                // dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                DataSet ds = new DataSet();
                //string qry = "Select RESPONDENT_ID,EMAIL_ID,STATUS,CREATED_ON from osm_responsesurvey where SURVEY_ID=" + SurveyID + "and STATUS='Complete'";
                if (RespondentIDS != null && RespondentIDS.Length > 0)
                {
                    string qry = " Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where QUESTION_ID=" + QuestionID + " and RESPONDENT_ID IN(" + RespondentIDS + ") and DELETED=0 order by RESPONDENT_ID";
                    ds = dbm.ExecuteDataSet(CommandType.Text, qry);

                }
                return ds;
                //ds = dbm.ExecuteDataSet(CommandType.Text, qry);
                //DataSet ds_ansidtabbles = new DataSet();
                //string qry1 = "";
                //for (int i = 0; i < ans_options.Count; i++)
                //{
                //    qry1 = " Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where QUESTION_ID=" + QuestionID + " and ANSWER_ID=" + ans_options[i] + " order by RESPONDENT_ID,ANSWER_ID";
                //    ds_ansidtabbles = dbm.ExecuteDataSet(CommandType.Text, qry1);
                //    ds.Tables[0].Columns.Add();
                //    ds.Tables[0].Columns.Add();
                //    for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                //    {
                //        for (int l = 0; l < ds_ansidtabbles.Tables[0].Rows.Count; l++)
                //        {

                //            if (Convert.ToInt32(ds.Tables[0].Rows[k]["RESPONDENT_ID"].ToString()) == Convert.ToInt32(ds_ansidtabbles.Tables[0].Rows[l]["RESPONDENT_ID"].ToString()))
                //            {
                //                ds.Tables[0].Rows[k][i + 4] = Convert.ToString(ds_ansidtabbles.Tables[0].Rows[l]["ANSWER_TEXT"].ToString());
                //                ds.Tables[0].Rows[k][i + 5] = Convert.ToInt32(ds_ansidtabbles.Tables[0].Rows[l]["ANSWER_ID"].ToString());
                //                break;

                //            }

                //        }

                //    }

                //}

            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt");
                return null;
            }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }

        }
        public DataSet GetSurveyDashBoardDetails(int Survey_Id, int Userid)
        {
            try
            {
                //dbm.OpenConnection(connect);
                // dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                //string qrygetsurvmgedata = "SET ARITHABORT OFF SET ANSI_WARNINGS OFF select  S1.SURVEY_NAME,convert(varchar,S1.CREATED_ON,103)as CREATED_ON,S1.STATUS, ";
                //qrygetsurvmgedata += " S1.SURVEY_ID,S1.USERID,convert(varchar,S2.LAUNCHED_ON,103) as LAUNCHED_ON,convert(varchar,S2.LAUNCH_ON_DATE,103)as LAUNCHDATE_ON, ";
                //qrygetsurvmgedata += " (select count(*) from osm_responsesurvey where SURVEY_ID=S1.SURVEY_ID) as TOTAL_RESPONSES ,isnull(S2.PARTIAL_RESPONSES,0) as PARTIAL_RESPONSES,isnull(S2.SCREEN_OUTS,0) as SCREEN_OUTS,";
                //qrygetsurvmgedata += " S2.SURVEY_ID,isnull(S2.SEND_TO,0) as SEND_TO,isnull(convert(decimal(5,0), 100.00 *S2.TOTAL_RESPONSES/(S2.SEND_TO+S2.SEND_TO_EXT)),0) as RESPONSE_RATE, ";
                //qrygetsurvmgedata += " isnull(convert(decimal(5,0), 100.00 *S2.PARTIAL_RESPONSES/S2.SEND_TO),0) as PASRTIAL_RESPONSE_RATE, ";
                //qrygetsurvmgedata += " isnull(convert(decimal(5,0), 100.00 *S2.SCREEN_OUTS/S2.SEND_TO),0) as SCREENOUT_RATE,isnull(S2.SEND_TO_EXT,0) as SEND_TO_EXT, ";
                //qrygetsurvmgedata += " isnull(S2.TOTAL_RESPONSES_EXT,0) as TOTAL_RESPONSES_EXT,isnull(S2.PARTIAL_RESPONSES_EXT,0) as PARTIAL_RESPONSES_EXT,isnull(SCREEN_OUTS_EXT,0) as SCREEN_OUTS_EXT, ";
                //qrygetsurvmgedata += " isnull(convert(decimal(5,0), 100.00 *S2.TOTAL_RESPONSES_EXT/S2.SEND_TO),0) as TOTAL_RESPONSES_RATE_EXT,isnull(convert(decimal(5,0), 100.00 *S2.PARTIAL_RESPONSES_EXT/S2.SEND_TO_EXT),0) as PARTIAL_RATE_EXT, ";
                //qrygetsurvmgedata += " isnull(convert(decimal(5,0), 100.00 *S2.SCREEN_OUTS_EXT/S2.SEND_TO_EXT),0) as SCREENOUT_RATE_EXT,S2.SURVEYLAUNCH_URL FROM osm_survey S1 left outer join osm_surveycontrols S2 ";
                //qrygetsurvmgedata += " on S1.SURVEY_ID=S2.SURVEY_ID WHERE S1.DELETED=0 AND S1.SURVEY_ID=" + Survey_Id;
                //qrygetsurvmgedata += " Select (Usr.FIRST_NAME+' '+Usr.LAST_NAME) as survey_created_by_name from osm_user Usr where USERID=" + Userid;
                //qrygetsurvmgedata += " Select fl.FOLDER_NAME from osm_survey S1 left outer join osm_cl_folders fl on S1.FOLDER_ID=fl.FOLDER_ID where S1.SURVEY_ID=" + Survey_Id;
                //qrygetsurvmgedata += " Select count(BOUNCED_ID) as COUNT_BOUNCEDMAILS from osm_bouncedmaillist where SURVEY_ID=" + Survey_Id;
                //qrygetsurvmgedata += " Select * from osm_surveylaunch where SURVEY_ID=" + Survey_Id;
                //qrygetsurvmgedata += " Select count(RESPONDENT_ID) from osm_responsesurvey where STATUS='Complete' and DELETED=0 and SURVEY_ID=" + Survey_Id;
                //qrygetsurvmgedata += " Select count(RESPONDENT_ID) from osm_responsesurvey where STATUS='Partial' and DELETED=0 and SURVEY_ID=" + Survey_Id;
                //qrygetsurvmgedata += " Select count(RESPONDENT_ID) from osm_responsesurvey where STATUS='Overquota' and DELETED=0 and SURVEY_ID=" + Survey_Id;
                string qrygetsurvmgedata = "SET ARITHABORT OFF SET ANSI_WARNINGS OFF select  S1.SURVEY_NAME,convert(varchar,S1.CREATED_ON,103)as CREATED_ON,S1.STATUS, ";
                qrygetsurvmgedata += " S1.SURVEY_ID,S1.USERID,convert(varchar,S2.LAUNCHED_ON,103) as LAUNCHED_ON,convert(varchar,S2.LAUNCH_ON_DATE,103)as LAUNCHDATE_ON, ";
                qrygetsurvmgedata += " (select count(*) from osm_responsesurvey where SURVEY_ID=S1.SURVEY_ID) as TOTAL_RESPONSES ,(select count(*) from osm_responsesurvey where SURVEY_ID=S1.SURVEY_ID and STATUS = 'Partial') as PARTIAL_RESPONSES,(select count(*) from osm_responsesurvey where SURVEY_ID=S1.SURVEY_ID and STATUS = 'ScreenOut') as SCREEN_OUTS,";
                qrygetsurvmgedata += " S2.SURVEY_ID,isnull(S2.SEND_TO,0) as SEND_TO,isnull(convert(decimal(5,0), 100.00 *((select count(*) from osm_responsesurvey where SURVEY_ID=S1.SURVEY_ID)/(S2.SEND_TO+S2.SEND_TO_EXT))),0) as RESPONSE_RATE, ";
                qrygetsurvmgedata += " isnull(convert(decimal(5,0), 100.00 *((select count(*) from osm_responsesurvey where SURVEY_ID=S1.SURVEY_ID and STATUS = 'Partial')/S2.SEND_TO)),0) as PASRTIAL_RESPONSE_RATE, ";
                qrygetsurvmgedata += " isnull(convert(decimal(5,0), 100.00 *((select count(*) from osm_responsesurvey where SURVEY_ID=S1.SURVEY_ID and STATUS = 'ScreenOut')/S2.SEND_TO)),0) as SCREENOUT_RATE,isnull(S2.SEND_TO_EXT,0) as SEND_TO_EXT, ";
                qrygetsurvmgedata += " isnull(S2.TOTAL_RESPONSES_EXT,0) as TOTAL_RESPONSES_EXT,isnull(S2.PARTIAL_RESPONSES_EXT,0) as PARTIAL_RESPONSES_EXT,isnull(SCREEN_OUTS_EXT,0) as SCREEN_OUTS_EXT, ";
                qrygetsurvmgedata += " isnull(convert(decimal(5,0), 100.00 *S2.TOTAL_RESPONSES_EXT/S2.SEND_TO),0) as TOTAL_RESPONSES_RATE_EXT,isnull(convert(decimal(5,0), 100.00 *S2.PARTIAL_RESPONSES_EXT/S2.SEND_TO_EXT),0) as PARTIAL_RATE_EXT, ";
                qrygetsurvmgedata += " isnull(convert(decimal(5,0), 100.00 *S2.SCREEN_OUTS_EXT/S2.SEND_TO_EXT),0) as SCREENOUT_RATE_EXT,S2.SURVEYLAUNCH_URL FROM osm_survey S1 left outer join osm_surveycontrols S2 ";
                qrygetsurvmgedata += " on S1.SURVEY_ID=S2.SURVEY_ID WHERE S1.DELETED=0 AND S1.SURVEY_ID=" + Survey_Id;
                qrygetsurvmgedata += " Select (Usr.FIRST_NAME+' '+Usr.LAST_NAME) as survey_created_by_name from osm_user Usr where USERID=" + Userid;
                qrygetsurvmgedata += " Select fl.FOLDER_NAME from osm_survey S1 left outer join osm_cl_folders fl on S1.FOLDER_ID=fl.FOLDER_ID where S1.SURVEY_ID=" + Survey_Id;
                qrygetsurvmgedata += " Select count(BOUNCED_ID) as COUNT_BOUNCEDMAILS from osm_bouncedmaillist where SURVEY_ID=" + Survey_Id;
                qrygetsurvmgedata += " Select * from osm_surveylaunch where SURVEY_ID=" + Survey_Id;
                qrygetsurvmgedata += " Select count(RESPONDENT_ID) from osm_responsesurvey where STATUS='Complete' and DELETED=0 and SURVEY_ID=" + Survey_Id;
                qrygetsurvmgedata += " Select count(RESPONDENT_ID) from osm_responsesurvey where STATUS='Partial' and DELETED=0 and SURVEY_ID=" + Survey_Id;
                qrygetsurvmgedata += " Select count(RESPONDENT_ID) from osm_responsesurvey where STATUS='Overquota' and DELETED=0 and SURVEY_ID=" + Survey_Id;
                // qrygetsurvmgedata += " select count(*) as total from osm_responsesurvey where survey_id=" + Survey_Id;


                DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qrygetsurvmgedata);
                return ds;

            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt");
                return null;
            }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }

        }
        public static DateTime GetConvertedDatetime(DateTime value, int bias, string TimeZone, bool server_flag)
        {
            DateTime ret_value = value;
            if (value != null)
            {
                if (server_flag == true)
                    ret_value = value.AddMinutes(bias);
                else
                    ret_value = DateTime.SpecifyKind(value, DateTimeKind.Utc);

            }
            return ret_value;

        }
        public DataSet GetTextResponses(int QuesID, int others, int SurveyID)
        {
            try
            {

                //string RespondentIDS = GetActiveRespondent(SurveyID);
                ////dbm.OpenConnection(connect);
                //if (SurveyID > 0 && RespondentIDS != null && RespondentIDS.Length > 0)
                //    dbm.OpenConnection(connect);
                //string qry = "";
                //if (others == 1)
                //    qry = " select (ROW_NUMBER() OVER (ORDER BY RESPONDENT_ID)) as SNO,answer_text from osm_responsequestions where question_id=" + QuesID + " and ANSWER_ID=0 and RESPONDENT_ID IN(" + RespondentIDS + ") and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";
                //else
                //    qry = " select (ROW_NUMBER() OVER (ORDER BY RESPONDENT_ID)) as SNO,answer_text from osm_responsequestions where question_id=" + QuesID + " and RESPONDENT_ID IN(" + RespondentIDS + ") and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";
                //return (dbm.ExecuteDataSet(CommandType.Text, qry));



                //string RespondentIDS = GetActiveRespondent(SurveyID);
                // dbm.OpenConnection(connect);
                string qry = "";
                string usrLmitDet = GetUserLimit(SurveyID, "");
                int usrLmit = 0; string LienTyp = "";
                string[] det = usrLmitDet.Split('$');
                if (det != null && det.Length > 0)
                {
                    LienTyp = Convert.ToString(det.GetValue(1));
                    usrLmit = Convert.ToInt32(det.GetValue(0));
                }
                //dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                if (usrLmit != -1 && usrLmit > 0)
                {
                    qry = "select TOP " + usrLmit + " RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
                }
                else if (usrLmit == -1)
                {
                    qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;

                }
                //string qry = "Select RESPONDENT_ID from osm_responsesurvey where SURVEY_ID=" + SurveyID + "and STATUS='Complete' and DELETED=0 and VISABLE_LIMIT=1 ";
                ds = dbm.ExecuteDataSet(CommandType.Text, qry);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataSet ds_ansidtabbles = new DataSet();
                    string qry1 = "";
                    if (others == 1)
                        qry1 = " select (ROW_NUMBER() OVER (ORDER BY RESPONDENT_ID)) as SNO,answer_text,RESPONDENT_ID from osm_responsequestions where question_id=" + QuesID + " and ANSWER_ID=0 and RESPONDENT_ID IN(select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID + ") and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";
                    else
                        qry1 = " select (ROW_NUMBER() OVER (ORDER BY RESPONDENT_ID)) as SNO,answer_text,RESPONDENT_ID from osm_responsequestions where question_id=" + QuesID + " and RESPONDENT_ID IN(select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID + ") and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";
                    //qry1 = " Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where QUESTION_ID=" + QuestionID + " and ANSWER_ID=" + ans_options[i] + " and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";
                    ds_ansidtabbles = dbm.ExecuteDataSet(CommandType.Text, qry1);
                    ds1.Tables.Add();
                    ds1.Tables[0].Columns.Add("SNO");
                    ds1.Tables[0].Columns.Add("answer_text");
                    for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                    {
                        int flag = 0;
                        ds1.Tables[0].Rows.Add();
                        for (int l = 0; l < ds_ansidtabbles.Tables[0].Rows.Count; l++)
                        {

                            if (Convert.ToInt32(ds.Tables[0].Rows[k]["RESPONDENT_ID"].ToString()) == Convert.ToInt32(ds_ansidtabbles.Tables[0].Rows[l]["RESPONDENT_ID"].ToString()))
                            {
                                ds1.Tables[0].Rows[k][0] = Convert.ToString(k + 1);
                                ds1.Tables[0].Rows[k][1] = Convert.ToString(ds_ansidtabbles.Tables[0].Rows[l]["ANSWER_TEXT"].ToString()).Trim();
                                flag = 1;
                                break;

                            }

                        }
                        if (flag == 0)
                        {
                            ds1.Tables[0].Rows[k][0] = Convert.ToString(k + 1);
                            ds1.Tables[0].Rows[k][1] = "";
                        }

                    }


                }

                return ds1;
            }
            catch (Exception ex) { GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt"); return null; }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }
        }
        public DataSet GetRankingConstanSumCommentBoxTexResponses(int SurveyID, int QuestionID, ArrayList ans_options)
        {
            try
            {
                //dbm.OpenConnection(connect);
                string usrLmitDet = GetUserLimit(SurveyID, "");
                int usrLmit = 0; string LienTyp = "";
                string[] det = usrLmitDet.Split('$');
                if (det != null && det.Length > 0)
                {
                    LienTyp = Convert.ToString(det.GetValue(1));
                    usrLmit = Convert.ToInt32(det.GetValue(0));
                }
                string RespondentIDs = "";
                string qry = "";

                //dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                if (usrLmit != -1 && usrLmit > 0)
                {

                    qry = "select TOP " + usrLmit + " RESPONDENT_ID,EMAIL_ID,STATUS,CREATED_ON  from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
                }
                else if (usrLmit == -1)
                {
                    qry = "select RESPONDENT_ID,EMAIL_ID,STATUS,CREATED_ON  from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;

                }
                DataSet ds = new DataSet();

                // string qry = "Select RESPONDENT_ID,EMAIL_ID,STATUS,CREATED_ON from osm_responsesurvey where SURVEY_ID=" + SurveyID + "and STATUS='Complete' and DELETED=0 and VISABLE_LIMIT=1";
                ds = dbm.ExecuteDataSet(CommandType.Text, qry);
                DataSet ds_ansidtabbles = new DataSet();
                string qry1 = "";
                for (int i = 0; i < ans_options.Count; i++)
                {
                    qry1 = " Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where QUESTION_ID=" + QuestionID + " and ANSWER_ID=" + ans_options[i] + " and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";
                    ds_ansidtabbles = dbm.ExecuteDataSet(CommandType.Text, qry1);
                    if (i == 0)
                        ds.Tables[0].Columns.Add("SNO");
                    ds.Tables[0].Columns.Add();

                    for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                    {
                        int flag = 0;
                        for (int l = 0; l < ds_ansidtabbles.Tables[0].Rows.Count; l++)
                        {

                            if (Convert.ToInt32(ds.Tables[0].Rows[k]["RESPONDENT_ID"].ToString()) == Convert.ToInt32(ds_ansidtabbles.Tables[0].Rows[l]["RESPONDENT_ID"].ToString()))
                            {
                                if (i == 0)
                                    ds.Tables[0].Rows[k][i + 4] = Convert.ToString(k + 1);
                                ds.Tables[0].Rows[k][i + 5] = Convert.ToString(ds_ansidtabbles.Tables[0].Rows[l]["ANSWER_TEXT"].ToString());
                                flag = 1;
                                break;

                            }

                        }
                        if (flag == 0 && i == 0)
                            ds.Tables[0].Rows[k][i + 4] = Convert.ToString(k + 1);
                    }

                }
                return ds;
            }
            catch (Exception ex) { GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt"); return null; }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }

        }
        public string GetActiveRespondent(int SurveyID)
        {
            int cont = 0;
            string usrLmitDet = GetUserLimit(SurveyID, "");
            int usrLmit = 0; string LienTyp = "";
            string[] det = usrLmitDet.Split('$');
            if (det != null && det.Length > 0)
            {
                LienTyp = Convert.ToString(det.GetValue(1));
                usrLmit = Convert.ToInt32(det.GetValue(0));
            }
            string RespondentIDs = "";
            string qry = "";
            if (usrLmit != -1 && usrLmit > 0)
            {

                //qry = "select TOP "+usrLmit +" RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and VISABLE_LIMIT=1 and SURVEY_ID=" + SurveyID;
                qry = "select TOP " + usrLmit + " RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
            }
            else if (usrLmit == -1)
            {
                qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and  SURVEY_ID=" + SurveyID;

            }



            //string RespondentIDs = "";
            // string qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and VISABLE_LIMIT=1 and SURVEY_ID=" + SurveyID;
            try
            {
                // dbm.OpenConnection(connect);
                // dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qry);
                if (ds != null && ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        RespondentIDs += "," + Convert.ToString(ds.Tables[0].Rows[i]["RESPONDENT_ID"]);
                    }
                    if (RespondentIDs != null && RespondentIDs.Trim().Length > 0)
                        RespondentIDs = RespondentIDs.Substring(1);
                }
                return RespondentIDs;
            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt");
                return RespondentIDs;
            }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }
        }


        public string GetUserLimit(int SurveyID, string LicenseType)
        {
            string ret = ""; string fea_nam = "";
            string Qry = "select LICENSE_TYPE from osm_user where deleted=0 and USERID =(select USERID from osm_survey where deleted =0 and SURVEY_ID='" + SurveyID + "')";
            try
            {
                //dbm.OpenConnection(connect);
                //dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                if (LicenseType != null && LicenseType.Trim().Length > 0)
                {
                    fea_nam = LicenseType;
                }
                else
                {
                    object obj = dbm.ExecuteScalar(CommandType.Text, Qry);
                    if (obj != null)
                    {
                        fea_nam = Convert.ToString(obj);
                    }
                }
                if (fea_nam != null && fea_nam.Length > 0)
                {
                    string QRtext = "select  " + fea_nam + "  from osm_features where FEATURE='NO_OF_RESPONSES' and deleted=0 ";
                    object obj1 = dbm.ExecuteScalar(CommandType.Text, QRtext);
                    if (obj1 != null)
                    {
                        ret = Convert.ToString(obj1);
                    }
                }
                ret = ret + "$" + fea_nam;
                return ret;
            }
            catch
            {
                return "";
            }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }
        }
        public DataSet getsurveyanswers(int q1_id, int q2_id, int SurveyID)
        {
            try
            {
                // dbm.OpenConnection(connect);
                string Respondents = GetActiveRespondent(SurveyID);
                //dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                string str = " select ANSWER_ID,ANSWER_OPTIONS from osm_answeroptions where QUESTION_ID=" + q1_id;
                str += " select ANSWER_ID,ANSWER_OPTIONS from osm_answeroptions where QUESTION_ID=" + q2_id;
                //str += " select RESPONSE_ID,RQ1.RESPONDENT_ID,ANSWER_ID,ANSWER_TEXT from osm_responsequestions RQ1 left outer join osm_responsesurvey R1 on RQ1.RESPONDENT_ID=R1.RESPONDENT_ID where RQ1.DELETED=0 and VISABLE_LIMIT=1 and QUESTION_ID=" + q1_id;
                //str += " select RESPONSE_ID,RQ1.RESPONDENT_ID,ANSWER_ID,ANSWER_TEXT from osm_responsequestions RQ1 left outer join osm_responsesurvey R1 on RQ1.RESPONDENT_ID=R1.RESPONDENT_ID where RQ1.DELETED=0 and VISABLE_LIMIT=1 and QUESTION_ID=" + q2_id;
                str += " select RESPONSE_ID,RQ1.RESPONDENT_ID,ANSWER_ID,ANSWER_TEXT from osm_responsequestions RQ1 inner join osm_responsesurvey R1 on RQ1.RESPONDENT_ID=R1.RESPONDENT_ID where RQ1.DELETED=0 and QUESTION_ID=" + q1_id + "  and RQ1.RESPONDENT_ID IN(" + Respondents + ")";
                str += " select RESPONSE_ID,RQ1.RESPONDENT_ID,ANSWER_ID,ANSWER_TEXT from osm_responsequestions RQ1 inner join osm_responsesurvey R1 on RQ1.RESPONDENT_ID=R1.RESPONDENT_ID where RQ1.DELETED=0 and QUESTION_ID=" + q2_id + " and RQ1.RESPONDENT_ID IN(" + Respondents + ")";
                str += " Select QUESTION_ID,QUSETION_LABEL,OTHER_ANS from osm_surveyquestion where QUESTION_ID=" + q1_id + "or QUESTION_ID=" + q2_id;
                str += " Select SURVEY_ID, SURVEY_NAME,ou.USERID,ou.Login_Name from osm_survey os inner join osm_user ou on os.userid=ou.userid where SURVEY_ID=" + SurveyID;
                //string QryGetSurveyQuestions = "";
                //int del_value = 0;
                //string QRtext = "select count(*) as count from osm_user where STATUS ='Active' and LOGIN_NAME= " + "'" + Name + "'";
                //QryGetSurveyQuestions = "Select QUESTION_ID,QUSETION_LABEL,QUESTION_TYPE_ID from osm_surveyquestion  where SURVEY_ID=" + "'" + surid + "'";
                DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
                return ds;
            }
            catch (Exception ex) { GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt"); return null; }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }
        }
        public DataSet GetUserAccntsDet(string EMAIL, string FIRST_NAME, string LAST_NAME, string CITY, string STATE, string COMPANY, string WORK_INDUSTRY, string JOB_FUNCTION, string LICENSE_TYPE, string STATUS, string CREATED_ON, string end_CREATED_ON, string lic_expiry_date, string end_expirydate_ON, int noofdays, string cust_id)
        {
            try
            {
                //dbm.OpenConnection(connect);
                //dsq.ReadXml("C:\\MailSending.xml");
                dsq.ReadXml(strMailSettingsPath);
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                string noofdays_date = "";
                string GetDet = "Select USERID,LOGIN_NAME,FIRST_NAME,LAST_NAME,PHONE,convert(varchar,CREATED_ON,103)as CREATED_ON,convert(varchar,LICENSE_EXPIRY_DATE,103) as LICENSE_EXPIRY_DATE,ACCOUNT_TYPE,STATUS,CUSTOMER_ID,LICENSE_TYPE,LICENSE_OPTED,ACTIVATION_FLAG,EMAIL_FLAG from osm_user where (EMAIL like '%" + EMAIL + "%' and FIRST_NAME like '%" + FIRST_NAME + "%' and LAST_NAME like '%" + LAST_NAME + "%' and CITY like '%" + CITY + "%' and STATE like '%" + STATE + "%' and COMPANY like '%" + COMPANY + "%' and  WORK_INDUSTRY like '%" + WORK_INDUSTRY + "%'  and  JOB_FUNCTION like '%" + JOB_FUNCTION + "%' and CUSTOMER_ID like '%" + cust_id + "%')";
                if (CREATED_ON != "" && end_CREATED_ON != "")
                {
                    GetDet += " and CREATED_ON   between '" + CREATED_ON + "' and '" + end_CREATED_ON + "'";
                }
                else if (CREATED_ON != "" && end_CREATED_ON == "")
                {
                    GetDet += " and CREATED_ON >= '" + CREATED_ON + "' ";
                }
                else if (CREATED_ON == "" && end_CREATED_ON != "")
                {
                    GetDet += " and  CREATED_ON <= '" + end_CREATED_ON + "' ";
                }
                if (noofdays != 0)
                {
                    noofdays_date = Convert.ToString(DateTime.Now.AddDays(noofdays));
                    GetDet += " and  (LICENSE_EXPIRY_DATE between '" + DateTime.Now + "' and '" + noofdays_date + "')";

                }
                if (lic_expiry_date != "" && end_expirydate_ON != "")
                {
                    GetDet += " and LICENSE_EXPIRY_DATE between '" + lic_expiry_date + "' and '" + end_expirydate_ON + "'";
                }
                else if (lic_expiry_date != "" && end_expirydate_ON == "")
                {
                    GetDet += " and LICENSE_EXPIRY_DATE >= '" + CREATED_ON + "' ";
                }
                else if (lic_expiry_date == "" && end_expirydate_ON != "")
                {
                    GetDet += " and LICENSE_EXPIRY_DATE <= '" + end_expirydate_ON + "' ";
                }
                if (LICENSE_TYPE != "")
                {
                    GetDet += "and   LICENSE_TYPE like '%" + LICENSE_TYPE + "%'";
                }
                if (STATUS != "")
                {
                    GetDet += "and  STATUS = '" + STATUS + "' ";
                }

                GetDet += " and APPROVAL_FLAG=0";

                DataSet ds = dbm.ExecuteDataSet(CommandType.Text, GetDet);
                if (ds != null && ds.Tables.Count > 0)
                {
                    ds.Tables[0].Columns.Add("url");
                    ds.Tables[0].Columns.Add("User_status");
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        int sid = Convert.ToInt32(ds.Tables[0].Rows[i]["USERID"]);
                        string name;
                        name = ds.Tables[0].Rows[i]["FIRST_NAME"] + "  " + ds.Tables[0].Rows[i]["LAST_NAME"];
                        ds.Tables[0].Rows[i]["url"] = "ViewUserProfile.aspx?UId=" + sid + "&Uname=" + name;
                        if (Convert.ToInt32(ds.Tables[0].Rows[i]["EMAIL_FLAG"]) == 0 && Convert.ToInt32(ds.Tables[0].Rows[i]["ACTIVATION_FLAG"]) == 0)
                        {
                            ds.Tables[0].Rows[i]["User_status"] = "Pending_Auth.Email";
                        }
                        else if (Convert.ToInt32(ds.Tables[0].Rows[i]["EMAIL_FLAG"]) == 1 && Convert.ToInt32(ds.Tables[0].Rows[i]["ACTIVATION_FLAG"]) == 0)
                        {
                            ds.Tables[0].Rows[i]["User_status"] = "Pending Authentication";
                        }
                        else if ((Convert.ToInt32(ds.Tables[0].Rows[i]["EMAIL_FLAG"]) == 1) || (Convert.ToInt32(ds.Tables[0].Rows[i]["EMAIL_FLAG"]) == 2) && Convert.ToInt32(ds.Tables[0].Rows[i]["ACTIVATION_FLAG"]) == 1)
                        {
                            ds.Tables[0].Rows[i]["User_status"] = "Active";
                        }

                    }
                }
                return ds;
            }
            catch (Exception ex)
            {
                GenerateLog.GenrateSendmailLog(ex.Message, @"c:\SurveyExportLog.txt");
                return null;
            }
            finally
            {
                dbm.Connection.Dispose();
                dbm.Connection.Close();
            }
        }
        public string GetEmailId(int SurveyID)
        {
            string toEmailId = "";
            string qry = "";
            qry = " Select SURVEY_ID, SURVEY_NAME,ou.USERID,ou.Login_Name from osm_survey os inner join osm_user ou on os.userid=ou.userid where SURVEY_ID=" + SurveyID;
                
          
            try
            {
             
                dsq.ReadXml(strMailSettingsPath);  
                dbm.OpenConnection(Convert.ToString(dsq.Tables["DB_CONNECTION"].Rows[0]["VALUE"]));
                DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qry);
                
                 if (ds != null && ds.Tables.Count > 0)
                {
                    toEmailId = ds.Tables[0].Rows[0]["Login_Name"].ToString() + "," + ds.Tables[0].Rows[0]["SURVEY_NAME"].ToString(); 
                }
            }
            catch(Exception ex)
            {

            }
            return toEmailId;
 

    }
    }
}
