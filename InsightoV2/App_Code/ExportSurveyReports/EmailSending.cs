using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Net.Mail;
using System.Collections;

namespace InsightoV2.ExportSurveyReports
{
    public class EmailSending
    {
        private string path = "";
        private MailMessage mailMessage = null;
        private SmtpClient smtpMail = null;

        public void SendMailing(mails Sendmails) 
        {
            path = Sendmails.LOG_PATH;
            mailMessage = new MailMessage();
            mailMessage.To.Clear();
            mailMessage.Sender = new MailAddress(Sendmails.SENDER_MAILADDRESS,"Insighto.com");
            // mailMessage.Sender = new MailAddress(Sendmails.FROM_MAILADDRESS, "Insighto.com");
            mailMessage.From = new MailAddress(Sendmails.FROM_MAILADDRESS,"Insighto.com");
            mailMessage.Subject = Sendmails.MAIL_SUBJECT;
            mailMessage.To.Add(Sendmails.MAIL_TOADDRESS.Trim());
            mailMessage.Body = Sendmails.MAIL_BODY;
            
            if (!string.IsNullOrEmpty(Sendmails.AttachmentFileName))
                mailMessage.Attachments.Add(new Attachment(Sendmails.AttachmentFileName));
    
            if (Sendmails.SENDER_TOCCADDRESSES!=null && Convert.ToString(Sendmails.SENDER_TOCCADDRESSES.Trim()).Length > 0)
            {
                mailMessage.CC.Add(Sendmails.SENDER_TOCCADDRESSES.Trim());
            }
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.Normal;
            smtpMail = new SmtpClient();
            smtpMail.Host = Sendmails.HOST;
            smtpMail.Port = Sendmails.PORTNO;
            smtpMail.EnableSsl = true;
            smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpMail.Credentials = new System.Net.NetworkCredential(Sendmails.UserName,Sendmails.SENDER_PASSWORD);
            try
            {
                smtpMail.Send(mailMessage);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        //  Console.WriteLine("Delivery failed - retrying in 5 seconds.");


                        System.Threading.Thread.Sleep(5000);
                        smtpMail.Send(mailMessage);
                    }
                    else
                    {
                        throw ex;
                    }
                }
                GenerateLog.GenrateSendmailLog(ex.Message, path);
                GenerateLog.GenrateSendmailLog(ex.InnerException.Message, path);
                GenerateLog.GenrateSendmailLog(ex.InnerException.InnerException.Message, path);
            }
            //catch (System.Net.Mail.SmtpException ex)
            //{
            //    GenerateLog.GenrateSendmailLog(ex.Message, path);
            //    GenerateLog.GenrateSendmailLog(ex.InnerException.Message, path);
            //    GenerateLog.GenrateSendmailLog(ex.InnerException.InnerException.Message, path);
            //}
            mailMessage.Dispose();
        }




        public void MailSendingWithAttachment(mails Sendmails)
        {
            path = Sendmails.LOG_PATH;
            mailMessage = new MailMessage();
            
            mailMessage.To.Clear();
            mailMessage.Sender = new MailAddress(Sendmails.SENDER_MAILADDRESS, "Insighto.com");
            mailMessage.From = new MailAddress(Sendmails.FROM_MAILADDRESS, "Insighto.com");
            mailMessage.Subject = Sendmails.MAIL_SUBJECT;
            mailMessage.To.Add(Sendmails.MAIL_TOADDRESS.Trim());
            mailMessage.Body = Sendmails.MAIL_BODY;
            if (Sendmails.MAIL_ATTACHMENTS != null && Convert.ToString(Sendmails.MAIL_ATTACHMENTS.Trim()).Length > 0)
            {
                Attachment att = new Attachment(Sendmails.MAIL_ATTACHMENTS);
                mailMessage.Attachments.Add(att);
            }
            if (Sendmails.SENDER_TOCCADDRESSES != null && Convert.ToString(Sendmails.SENDER_TOCCADDRESSES.Trim()).Length > 0)
            {
                mailMessage.CC.Add(Sendmails.SENDER_TOCCADDRESSES.Trim());
            }
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.Normal;
            smtpMail = new SmtpClient();
            smtpMail.Host = Sendmails.HOST;
            smtpMail.Port = Sendmails.PORTNO;
            smtpMail.EnableSsl =true;
            smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpMail.Credentials = new System.Net.NetworkCredential(Sendmails.SENDER_MAILADDRESS, Sendmails.SENDER_PASSWORD);
            try
            {
                smtpMail.Send(mailMessage);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        //  Console.WriteLine("Delivery failed - retrying in 5 seconds.");


                        System.Threading.Thread.Sleep(5000);
                        smtpMail.Send(mailMessage);
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            //catch (System.Net.Mail.SmtpException ex)
            //{
            //    GenerateLog.GenrateSendmailLog(ex.Message, path);
            //    GenerateLog.GenrateSendmailLog(ex.InnerException.Message, path);
            //    GenerateLog.GenrateSendmailLog(ex.InnerException.InnerException.Message, path);
            //}
            mailMessage.Dispose();
        }

        
    }
}
