﻿using System;
using CovalenseUtilities.Helpers;
using Insighto.Business.Helpers;
using System.Collections;
using Insighto.Business.Enumerations;

/// <summary>
/// Summary description for UserControlBase
/// </summary>
public class UserControlBase : System.Web.UI.UserControl
{
    public UserControlBase()
    {
        Load += OnLoad;
        //
        // TODO: Add constructor logic here
        //
    }

    public virtual void OnLoad(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Whether the field is required
    /// </summary>
    public virtual bool IsRequired { get; set; }
    

    /// <summary>
    /// The Group (Validation Group)
    /// </summary>
    public virtual string Group
    {
        get
        {
            throw new NotImplementedException("UserControlBase.Group not implemented");
        }
        set
        {
            throw new NotImplementedException("UserControlBase.Group not implemented");
        }
    }

    public virtual void Clear()
    {
        throw new NotImplementedException("UserControlBase.Clear not implemented");
    }

    public virtual void Initialise()
    {
        throw new NotImplementedException("UserControlBase.Initialise not implemented");
    }

    public virtual void Display()
    {
        throw new NotImplementedException("UserControlBase.Display not implemented");
    }

    public virtual bool GetStatusOfMandatory
    {
        get
        {
            throw new NotImplementedException("UserControlBase.GetStatusOfMandatory not implemented");
        }

    }

    public virtual bool GetStatusOfIncludedFields
    {
        get
        {
            throw new NotImplementedException("UserControlBase.GetStatusOfIncludedFields not implemented");
        }
    }
    public int SurveyFlag
    {
        get
        {
            var key = QueryStringHelper.GetString("key");
            if (String.IsNullOrEmpty(key))
                return 0;

            Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
            if (ht != null && ht.Contains("surveyFlag"))
            {
                return ValidationHelper.GetInteger(ht["surveyFlag"], 0);
            }
            return 0;
        }

    }
    public string SurveyMode
    {
        get
        {
            var key = QueryStringHelper.GetString("key");
            if (String.IsNullOrEmpty(key))
                return ValidationHelper.GetString(UserSelectedMode.Survey);

            Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
            if (ht != null && ht.Contains("surveyFlag"))
            {
                int flag = ValidationHelper.GetInteger(ht["surveyFlag"], 0);
                if (flag > 0)
                    return ValidationHelper.GetString(UserSelectedMode.Widget);
                else
                    return ValidationHelper.GetString(UserSelectedMode.Survey);
            }
            return ValidationHelper.GetString(UserSelectedMode.Survey);
        }
    }
    public void CloseModelForSpecificTime(string pageToBeRefreshedURL, string timeValue)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "closeModelAfterFive", "closeModelAfterFive(true,'" + pageToBeRefreshedURL + "','" + timeValue + "');", true);
    }
}