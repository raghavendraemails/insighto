﻿using System;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Web;
using Insighto.Business.Enumerations;


/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : Page
{
    public BasePage()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    static string cultureName = "en-IN";
    public static string CultureName
    {
        get { return cultureName; }
        set { cultureName = value; }
    }

    public int SurveyFlag
    {
        get
        {
            var key = QueryStringHelper.GetString("key");
            if (String.IsNullOrEmpty(key))
                return 0;

            Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
            if (ht != null && ht.Contains("surveyFlag"))
            {
                return ValidationHelper.GetInteger(ht["surveyFlag"], 0);
            }
            return 0;
        }

    }


    public string SurveyMode
    {
        get
        {
            var key = QueryStringHelper.GetString("key");
            if (String.IsNullOrEmpty(key))
                return ValidationHelper.GetString(UserSelectedMode.Survey);

            Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
            if (ht != null && ht.Contains("surveyFlag"))
            {
                int flag = ValidationHelper.GetInteger(ht["surveyFlag"], 0);
                if (flag > 0)
                return ValidationHelper.GetString(UserSelectedMode.Widget);
                else
                    return ValidationHelper.GetString(UserSelectedMode.Survey);
            }
            return ValidationHelper.GetString(UserSelectedMode.Survey);
        }

    }

    protected override void InitializeCulture()
    {
        //SetUserLocale(true);
        base.InitializeCulture();
    }

    public void CloseModal()
    {
        CloseModal(string.Empty);
    }

    public void CloseModal(string pageToBeRefreshedURL)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "closeModal", "closeModalSelf(true,'" + pageToBeRefreshedURL + "');", true);
    }

    public void CloseModelForSpecificTime(string pageToBeRefreshedURL, string timeValue)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "closeModelAfterFive", "closeModelAfterFive(true,'" + pageToBeRefreshedURL + "','" + timeValue + "');", true);
    }
    public string _recordsCount
    {
        get
        {
            return ServiceFactory.GetService<SessionStateService>().GetRecordsCountFromSession();
        }
    }

    /// <summary>
    /// Sets a user's Locale based on the browser's Locale setting. If no setting
    /// is provided the default Locale is used.
    /// </summary>
    public static void SetUserLocale(bool setUiCulture, string currencySymbol = null)
    {
        try
        {
            CultureInfo culture = new CultureInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name);
            RegionInfo rInfo = new RegionInfo(culture.Name);
            if (rInfo.EnglishName == "India")
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol = "&#8377;";
            }
            else
            {
                culture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol = "$";
            }

            if (!string.IsNullOrEmpty(currencySymbol))
                Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol = currencySymbol;


            if (setUiCulture)
                Thread.CurrentThread.CurrentUICulture = culture;
        }
        catch
        { ;}
    }
}