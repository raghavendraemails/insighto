﻿using System;
using System.Collections;
using System.Collections.Generic;
using CovalenseUtilities.Helpers;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System.Linq;

namespace App_Code
{
    /// <summary>
    /// Summary description for SurveyRespondentControlBase
    /// </summary>
    public class QuestionControlBase : UserControlBase
    {
        /// <summary>
        /// Gets the survey flag.
        /// </summary>
        public int SurveyFlag
        {
            get
            {
                var key = QueryStringHelper.GetString("key");
                if (String.IsNullOrEmpty(key))
                    return 0;

                Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
                if (ht != null && ht.Contains("surveyFlag"))
                {
                    return ValidationHelper.GetInteger(ht["surveyFlag"], 0);
                }
                return 0;
            }
        }

        /// <summary>
        /// Gets the survey page base.
        /// </summary>
        public SurveyPageBase SurveyPageBase
        {
            get
            {
                return Page as SurveyPageBase;
            }
        }

        public int SurveyID
        {
            get
            {
                int surveyId = 0;
                var key = QueryStringHelper.GetString("key");
                if (String.IsNullOrEmpty(key))
                    return 0;

                Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
                if (ht != null)
                {
                    if (ht.Contains("SurveyId"))
                    {
                        surveyId = ValidationHelper.GetInteger(ht["SurveyId"].ToString(), 0);
                    }
                }
                return surveyId;          
            }
        }
                
        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        private SurveyQuestionAndAnswerOptions _questionAndAnswerOptions = new SurveyQuestionAndAnswerOptions();
        public virtual SurveyQuestionAndAnswerOptions QuestionAndAnswerOptions
        {
            get
            {
                return _questionAndAnswerOptions;
            }
            set
            {
                _questionAndAnswerOptions = value;
            }
        }


        private List<SurveyQuestionAndAnswerOptions> _matrixquestionAndAnswerOptions = new List<SurveyQuestionAndAnswerOptions>();
        public virtual List<SurveyQuestionAndAnswerOptions> MatrixQuestionAndAnswerOptions
        {
            get
            {
                return _matrixquestionAndAnswerOptions;
            }
            set
            {
                _matrixquestionAndAnswerOptions = value;
                QuestionAndAnswerOptions = _matrixquestionAndAnswerOptions.FirstOrDefault();
            }
        }

        /// <summary>
        /// Gets the question.
        /// </summary>
        public osm_surveyquestion Question
        {
            get
            {
                if (QuestionAndAnswerOptions == null)
                    throw new InvalidOperationException("QuestionAndAnswerOptions is null, Assign SurveyQuestionAndAnswerOptions before accessing this property");

                return QuestionAndAnswerOptions.Question;
            }
        }
        
        /// <summary>
        /// Gets the type of the question.
        /// </summary>
        /// <value>
        /// The type of the question.
        /// </value>
        public QuestionType QuestionType
        {
            get
            {
                if (Question == null)
                    throw new InvalidOperationException("Question is null, Assign QuestionAndAnswerOptions before accessing this property");

                return QuestionAndAnswerOptions.QuestionType;
            }
        }

        /// <summary>
        /// Gets or sets the selected question.
        /// </summary>
        /// <value>
        /// The selected question.
        /// </value>
        public virtual osm_surveyquestion SelectedQuestion { get; set; }
                
        /// <summary>
        /// Gets or sets the question settings.
        /// </summary>
        /// <value>
        /// The question settings.
        /// </value>
        List<string> _lstSettings = null;
        public virtual List<string> QuestionSettings
        {
            get
            {
                if (_lstSettings == null)
                    throw new InvalidOperationException("QuestionSettingValues is null, Assign QuestionSettingValues before accessing this property");

                return _lstSettings;
            }

            set
            {
                _lstSettings = value;
            }
        }

        /// <summary>
        /// Gets or sets the selected answer options.
        /// </summary>
        /// <value>
        /// The selected answer options.
        /// </value>
        public virtual List<osm_answeroptions> SelectedAnswerOptions
        {
            get
            {
                throw new NotImplementedException("QuestionControlBase.SelectedAnswerOptions not implemented");
            }
            set
            {
                throw new NotImplementedException("QuestionControlBase.SelectedAnswerOptions not implemented");
            }
        }


        /// <summary>
        /// Gets or sets the selected survey question and answer options.
        /// </summary>
        /// <value>
        /// The selected survey question and answer options.
        /// </value>
        private List<SurveyQuestionAndAnswerOptions> _selectedQuestionAndAnswerOptions = new List<SurveyQuestionAndAnswerOptions>();
        public virtual List<SurveyQuestionAndAnswerOptions> SelectedSurveyQuestionAndAnswerOptions
        {
            get
            {
                return _selectedQuestionAndAnswerOptions;
            }
            set
            {
                _selectedQuestionAndAnswerOptions = value;
            }
        }

        /// <summary>
        /// Gets or sets the selected anwer value.
        /// </summary>
        /// <value>
        /// The selected anwer value.
        /// </value>
        public virtual int SelectedAnwerValue { get; set; }        
    }
}