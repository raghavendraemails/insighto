﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsjsonmandrill
/// </summary>
public class clsjsonmandrill
{
	public clsjsonmandrill()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string key;
    public string id;

    public string name;
    public string code;
    public string tag;
    public string template_name;

    public string content;

    

    public class jsonmandrillmerge
    {
        public string key;
        public string template_name;
        public List<template_content> template_content;
        public List<merge_vars> merge_vars;

    }
    public class template_content
    {

        public string name { get; set; }

        public string content { get; set; }
    }
    public class merge_vars
    {

        public string name { get; set; }

        public string content { get; set; }
    }
}