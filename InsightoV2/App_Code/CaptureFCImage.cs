﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using InfoSoftGlobal;
using System.Text;
using System.Drawing;
using System.Configuration;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
/// <summary>
/// Summary description for CaptureFCImage
/// </summary>
public class CaptureFCImage
{
	public CaptureFCImage()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataTable dtQuestion;
    public string QuestionType;
    public string Questionseq;
    public string srvyQuestion;
    public DataRow[] drQT1234;
    public string jsstrchartname;
    public string jsstrchartnameppt;
    public string SAVE_PATH;
    public string cpath;
    public string SAVE_PATHRET;
    public int fcdelaytime;
    public string exportchartfilehandler;
    public string strlicensetype;
    public string SurveyID;

    public DataRow[] drQT10;
    public DataTable dt10;
    public DataTable dtmtxrows10;
    public DataTable dtmtxcols10;



    public DataRow[] drQT11;
    public DataTable dt11;
    public DataTable dtmtxrows11;
    public DataTable dtmtxcols11;

    public DataRow[] drQT12;
    public DataTable dt12;
    public DataTable dtmtxrows12;
    public DataTable dtmtxcols12;
    public DataTable dtmtxsubcols12;


    public DataRow[] drQT15;
    public DataTable dt15;
    public DataTable dtmtxrows15;
    public DataTable dtmtxcols15;
  

    public static string StripTagsRegex(string source)
    {
        return Regex.Replace(source, "<.*?>", string.Empty);
    }
    public void generateQt1234Img()
    {

        string strunique1 = GetUniqueKey();
        string uniquestr = SurveyID + Questionseq;

        jsstrchartname = uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");

       

        StringBuilder strXML = new StringBuilder();
        string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(srvyQuestion)).Replace("\r", "").Replace("\n", "");
       // string strchartname = strchartname1.Replace("&amp;nbsp;", "").Trim();
        string strchartname = "";

        if ((QuestionType == "1") || (QuestionType == "2") || (QuestionType == "3"))
        {
            strXML.Append("<chart caption='" + strchartname + "'  animate3D='1' xAxisName='' showValues='1' labelStep='0'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1'  showBorder='1'  borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a' numberSuffix='%'  exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif' >");
        }
        if ((QuestionType == "4"))
        {
            strXML.Append("<chart caption='" + strchartname + "'  pieSliceDepth='30'  animated='1' interactiveLegend='1' formatNumberScale='0'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1'  borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif' >");
        
        }
      
        for (int irg1 = 0; irg1 < drQT1234.Length; irg1++)
        {

            strXML.AppendFormat("<set label='{0}' value='{1}' />", System.Web.HttpUtility.HtmlEncode(drQT1234[irg1][2].ToString()).Replace("\r", "").Replace("\n", ""), drQT1234[irg1][6].ToString());
        }
            //Close <chart> element
            strXML.Append("</chart>");

            string charttypename = "";
            if (QuestionType == "1")
            {
                charttypename = "/Charts/Column3D.swf";
            }
            else if (QuestionType == "2")
            {
                charttypename = "/Charts/Bar2D.swf";
            }
            else if (QuestionType == "3")
            {
                charttypename = "/Charts/Bar2D.swf";
            }
            else if (QuestionType == "4")
            {
                charttypename = "/Charts/Pie3D.swf";
            }
           

       //  string   LiteralQTInitial = InfoSoftGlobal.FusionCharts.RenderChart(charttypename, "", strXML.ToString(),  uniquestr, "598", "300", false, true, false);

         SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();
         cpath = ConfigurationSettings.AppSettings["Fchartpath"].ToString();
        // string ChartPath = System.Web.HttpContext.Current.Server.MapPath(charttypename);  
         string ChartPath = cpath + charttypename;
         string ChartXMLURL = strXML.ToString();       
         string fileNameppt = SAVE_PATH + "pptimg" + Questionseq + ".png";


         OnlyCapture(ChartPath, ChartXMLURL, fileNameppt);       


    }




    public void generateQt13Img()
    {

        string strunique1 = GetUniqueKey();
        string uniquestr = SurveyID + Questionseq;

        jsstrchartname = uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");



        StringBuilder strXML = new StringBuilder();
        string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(srvyQuestion)).Replace("\r", "").Replace("\n", "");
       // string strchartname = strchartname1.Replace("&amp;nbsp;", "").Trim();
        string strchartname = "";

        strXML.Append("<chart caption='" + strchartname + "'  pieSliceDepth='30'  animated='1' interactiveLegend='1' formatNumberScale='0'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif' >");

        for (int irg1 = 0; irg1 < drQT1234.Length; irg1++)
        {

            strXML.AppendFormat("<set label='{0}' value='{1}' />", System.Web.HttpUtility.HtmlEncode(drQT1234[irg1][2].ToString()).Replace("\r", "").Replace("\n", ""), drQT1234[irg1][6].ToString());

        }
        //Close <chart> element
        strXML.Append("</chart>");

        string charttypename = "/Charts/Doughnut3D.swf";
       

        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();
        cpath = ConfigurationSettings.AppSettings["Fchartpath"].ToString();
        // string ChartPath = System.Web.HttpContext.Current.Server.MapPath(charttypename);  
        string ChartPath =cpath + charttypename;
        string ChartXMLURL = strXML.ToString();
        string fileNameppt = SAVE_PATH + "pptimg" + Questionseq + ".png";


        OnlyCapture(ChartPath, ChartXMLURL, fileNameppt);


    }

    public void generateQt10Img()
    {
        string strunique11011 = GetUniqueKey();
        string uniquestr1011 = SurveyID + Questionseq; 

        jsstrchartname = uniquestr1011 + DateTime.Now.ToString("ddMMyyyyhhMMss");


        StringBuilder strXML = new StringBuilder();

        string strchartnamematrix1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(srvyQuestion)).Replace("\r", "").Replace("\n", "");
       // string strchartnamematrix = strchartnamematrix1.Replace("&amp;nbsp;", "").Trim();
        string strchartnamematrix = "";
        ////////Generate the chart element string

        strXML.Append("<chart  caption='" + strchartnamematrix + "' animate3D='1' xAxisName='' showValues='1' labelStep='0' rotateXAxisName ='0'   canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1'  borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a' numberSuffix='%'  exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif' >");
        strXML.AppendFormat("<categories>");

        foreach (DataRow dr in dtmtxrows10.Rows)
        {

            strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r", "").Replace("\n", ""));
        }
        strXML.AppendFormat("</categories>");

        for (int mcans1chart = 0; mcans1chart < dtmtxcols10.Rows.Count; mcans1chart++)
        {

            strXML.AppendFormat("<dataset seriesName='{0}'>", dtmtxcols10.Rows[mcans1chart][0].ToString().Replace("\r", "").Replace("\n", ""));

            for (int mcansrowschart = 0; mcansrowschart < dtmtxrows10.Rows.Count; mcansrowschart++)
            {

                DataRow[] dr1 = dt10.Select("QuestionID='" + Questionseq + "'" + "and rowansweroption='" + dtmtxrows10.Rows[mcansrowschart][0].ToString() + "'" + "and columnansweroption ='" + dtmtxcols10.Rows[mcans1chart][0].ToString() + "'");

                if (dr1.Length > 0)
                {
                    strXML.AppendFormat("<set value='{0}'/>", dr1[0].ItemArray[3].ToString());
                }
                else
                {
                    strXML.AppendFormat("<set value='{0}'/>", "0");
                }

            }
            strXML.AppendFormat("</dataset>");
        }
        strXML.Append("</chart>");

        string charttypename = "/Charts/MSLine.swf";

        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();
        cpath = ConfigurationSettings.AppSettings["Fchartpath"].ToString();
        // string ChartPath = System.Web.HttpContext.Current.Server.MapPath(charttypename);  
        string ChartPath = cpath + charttypename;
        string ChartXMLURL = strXML.ToString();
        string fileNameppt = SAVE_PATH + "pptimg" + Questionseq + ".png";


        OnlyCapture(ChartPath, ChartXMLURL, fileNameppt);

        
    }


    public void generateQt11Img()
    {
        string strunique11011 = GetUniqueKey();
        string uniquestr1011 = SurveyID + Questionseq;

        jsstrchartname = uniquestr1011 + DateTime.Now.ToString("ddMMyyyyhhMMss");


        StringBuilder strXML = new StringBuilder();

        string strchartnamematrix1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(srvyQuestion)).Replace("\r", "").Replace("\n", "");
       // string strchartnamematrix = strchartnamematrix1.Replace("&amp;nbsp;", "").Trim();
        string strchartnamematrix = "";

        ////////Generate the chart element string

        strXML.Append("<chart  caption='" + strchartnamematrix + "' animate3D='1' xAxisName='' showValues='1' labelStep='0' rotateXAxisName ='0'   canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1'  borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a' numberSuffix='%'  exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif' >");
        strXML.AppendFormat("<categories>");

        foreach (DataRow dr in dtmtxrows11.Rows)
        {

            strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r", "").Replace("\n", ""));
        }
        strXML.AppendFormat("</categories>");

        for (int mcans1chart = 0; mcans1chart < dtmtxcols11.Rows.Count; mcans1chart++)
        {

            strXML.AppendFormat("<dataset seriesName='{0}'>", dtmtxcols11.Rows[mcans1chart][0].ToString().Replace("\r", "").Replace("\n", ""));

            for (int mcansrowschart = 0; mcansrowschart < dtmtxrows11.Rows.Count; mcansrowschart++)
            {

                DataRow[] dr1 = dt11.Select("QuestionID='" + Questionseq + "'" + "and rowansweroption='" + dtmtxrows11.Rows[mcansrowschart][0].ToString() + "'" + "and columnansweroption ='" + dtmtxcols11.Rows[mcans1chart][0].ToString() + "'");

                if (dr1.Length > 0)
                {
                    strXML.AppendFormat("<set value='{0}'/>", dr1[0].ItemArray[3].ToString());
                }
                else
                {
                    strXML.AppendFormat("<set value='{0}'/>", "0");
                }

            }
            strXML.AppendFormat("</dataset>");
        }
        strXML.Append("</chart>");

        string charttypename = "/Charts/MSLine.swf";

        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();
        // string ChartPath = System.Web.HttpContext.Current.Server.MapPath(charttypename);  
        cpath = ConfigurationSettings.AppSettings["Fchartpath"].ToString();
        string ChartPath = cpath + charttypename;
        string ChartXMLURL = strXML.ToString();
        string fileNameppt = SAVE_PATH + "pptimg" + Questionseq + ".png";


        OnlyCapture(ChartPath, ChartXMLURL, fileNameppt);


    }

    public void generateQt12Img()
    {
        string strunique112 = GetUniqueKey();
        string uniquestr12 = SurveyID + Questionseq;

        jsstrchartname = uniquestr12 + DateTime.Now.ToString("ddMMyyyyhhMMss");
        StringBuilder strXML = new StringBuilder();

        string strchartnamematrixsbs1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(srvyQuestion)).Replace("\r", "").Replace("\n", "");
       // string strchartnamematrixsbs = strchartnamematrixsbs1.Replace("&amp;nbsp;", "").Trim();
        string strchartnamematrixsbs = "";

        
        strXML.Append("<chart  caption='" + strchartnamematrixsbs + "' animate3D='1' xAxisName='' showValues='1' labelStep='0' rotateXAxisName ='0' labelDisplay='Rotate' slantLabels='1'   canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1'  font='Arial, Helvetica, sans-serif' >");
        strXML.AppendFormat("<categories>");

        foreach (DataRow dr in dtmtxrows12.Rows)
        {
            strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r", "").Replace("\n", "") + "-" + dtmtxcols12.Rows[0][0].ToString());

            for (int i = 1; i <= dtmtxcols12.Rows.Count - 1; i++)
            {

                strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r", "").Replace("\n", "") + "-" + dtmtxcols12.Rows[i][0].ToString());

            }

        }
        strXML.AppendFormat("</categories>");

        for (int colval = 0; colval < dtmtxsubcols12.Rows.Count; colval++)
        {
            strXML.AppendFormat("<dataset seriesName='{0}'>", dtmtxsubcols12.Rows[colval][0].ToString().Replace("\r", "").Replace("\n", ""));

            for (int colvalx = 0; colvalx < dtmtxrows12.Rows.Count; colvalx++)
            {

                string rowtot;


                for (int rot = 0; rot < dtmtxcols12.Rows.Count; rot++)
                {
                    DataRow[] dr1 = null;

                    dr1 = dt12.Select("QuestionID='" + Questionseq + "'" + "and columnoption='" + System.Web.HttpUtility.HtmlEncode(dtmtxcols12.Rows[rot][0].ToString()) + "'" + "and subcolumnoption ='" + dtmtxsubcols12.Rows[colval][0].ToString() + "'" + "and rowoption='" + System.Web.HttpUtility.HtmlEncode(dtmtxrows12.Rows[colvalx][0].ToString()) + "'");

                    if (dr1.Length > 0)
                    {
                        strXML.AppendFormat("<set value='{0}'/>", dr1[0].ItemArray[4].ToString());
                    }
                    else
                    {
                        strXML.AppendFormat("<set value='{0}'/>", "0");
                    }

                }
            }

            strXML.AppendFormat("</dataset>");
        }

        strXML.Append("</chart>");

        string charttypename = "/Charts/MSColumn3D.swf";

        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();
        // string ChartPath = System.Web.HttpContext.Current.Server.MapPath(charttypename);  
        string cpath = ConfigurationSettings.AppSettings["Fchartpath"].ToString();
        string ChartPath = cpath + charttypename;
        string ChartXMLURL = strXML.ToString();
        string fileNameppt = SAVE_PATH + "pptimg" + Questionseq + ".png";


        OnlyCapture(ChartPath, ChartXMLURL, fileNameppt);

    }

    public void generateQt15Img()
    {
        string strunique115 = GetUniqueKey();
        string uniquestr15 = SurveyID + Questionseq; ;


        jsstrchartname = uniquestr15 + DateTime.Now.ToString("ddMMyyyyhhMMss");


        StringBuilder strXML = new StringBuilder();

        string strchartnamematrixsbs1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(srvyQuestion)).Replace("\r", "").Replace("\n", "");
       // string strchartnamematrixsbs = strchartnamematrixsbs1.Replace("&amp;nbsp;", "").Trim();
        string strchartnamematrixsbs = "";

        strXML.Append("<chart  caption='" + strchartnamematrixsbs + "' animate3D='1' xAxisName='' showValues='1' labelStep='0' rotateXAxisName ='0' labelDisplay='Rotate' slantLabels='1'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif' >");

        strXML.AppendFormat("<categories>");

        foreach (DataRow dr in dtmtxrows15.Rows)
        {

            strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r", "").Replace("\n", ""));

        }
        strXML.AppendFormat("</categories>");

        for (int mcansprankchart = 0; mcansprankchart <  dtmtxcols15.Rows.Count; mcansprankchart++)
        {

            strXML.AppendFormat("<dataset seriesName='{0}'>", dtmtxcols15.Rows[mcansprankchart][0].ToString().Replace("\r", "").Replace("\n", ""));
            for (int mcansrowsrankchart = 0; mcansrowsrankchart < dtmtxrows15.Rows.Count; mcansrowsrankchart++)
            {

                DataRow[] dr15cht = null;

                dr15cht = dt15.Select("QuestionID='" + Questionseq + "'" + "and rowansweroption='" + System.Web.HttpUtility.HtmlEncode(dtmtxrows15.Rows[mcansrowsrankchart][0].ToString()) + "'" + "and columnansweroption ='" + System.Web.HttpUtility.HtmlEncode(dtmtxcols15.Rows[mcansprankchart][0].ToString()) + "'");
                if (dr15cht.Length > 0)
                {
                    strXML.AppendFormat("<set value='{0}'/>", dr15cht[0].ItemArray[3].ToString());
                }
                else
                {
                    strXML.AppendFormat("<set value='{0}'/>", "0");
                }

            }
            strXML.AppendFormat("</dataset>");
        }
        strXML.Append("</chart>");

        string charttypename = "/Charts/MSColumn3D.swf";

        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();
        cpath = ConfigurationSettings.AppSettings["Fchartpath"].ToString();
        // string ChartPath = System.Web.HttpContext.Current.Server.MapPath(charttypename);  
        string ChartPath = cpath + charttypename;
        string ChartXMLURL = strXML.ToString();
        string fileNameppt = SAVE_PATH + "pptimg" + Questionseq + ".png";


        OnlyCapture(ChartPath, ChartXMLURL, fileNameppt);


    }

    public void OnlyCapture(string ChartPath, string ChartXMLURL, string FileName)
    {
        try
        {
            string ReturnValue = "";
            FusionCharts.ServerSideImageHandler ssh = new FusionCharts.ServerSideImageHandler(ChartPath, 850, 500, ChartXMLURL, "", FusionCharts.ServerSideImageHandler.ImageType.PNG, 96.0f);
            // Setting chart SWF version
            ssh.SetChartVersion("3.2.2.0");
            // ssh.SetChartVersion("3.2.1.0");
            // ssh.SetCapturingDelayTime(fcdelaytime);
            // Begins the capture process.
            // An image will be created at the specified location after execution of this statement.
            ReturnValue = ssh.BeginCapture();
            if (!ReturnValue.ToLower().Equals("success"))
            {
                //  System.Web.HttpContext.Current.Response.Write("<b>Error:&nbsp;&nbsp;</b>" + ReturnValue.Replace(Environment.NewLine, "<br/>"));
            }
            else
            {
                Bitmap bmp = ssh.GetImage();
                bmp.Save(FileName);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    private string GetUniqueKey()
    {
        int maxSize = 8;
        int minSize = 5;
        char[] chars = new char[62];
        string a;
        a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        chars = a.ToCharArray();
        int size = maxSize;
        byte[] data = new byte[1];
        RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
        crypto.GetNonZeroBytes(data);
        size = maxSize;
        data = new byte[size];
        crypto.GetNonZeroBytes(data);
        StringBuilder result = new StringBuilder(size);
        foreach (byte b in data)
        {
            result.Append(chars[b % (chars.Length - 1)]);
        }
        return result.ToString();
    }
}