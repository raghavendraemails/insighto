﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Insighto.Business.Charts;

/// <summary>
/// Summary description for PickListManager
/// </summary>
public class PickListManager
{
    
    private class PickListDetails
    {
        public string Name;
        public object toUse;
        public string Category;
        public bool InGrid = false;
    }
    
    private Hashtable controls = new Hashtable();
    public DBManager dbm;
    private DataTable mPickList = null;
    public PickListManager()
    {
        DataSet ds = new DataSet();
        dbm = new DBManager();
        
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["connection"].ToString());
        SqlDataAdapter da = new SqlDataAdapter("select * from osm_picklist where DELETED=0", cn);
        da.Fill(ds, "PickList");
        mPickList = ds.Tables["PickList"];


    }

    public void AddControl(string Name, System.Web.UI.Control ctrl, string Category)
    {
        PickListDetails pld = new PickListDetails();
        pld.Name = Name;
        pld.Category = Category;
        pld.InGrid = false;
        pld.toUse = ctrl;
        try
        {
            controls.Add(Name, pld);
        }
        catch
        {
            controls.Remove(Name);
            controls.Add(Name, pld);
            return;
        }
    }

   
    public DataRow[] GetPickList(string Category)
    {
        DataRow[] toRet = null;
        if (mPickList != null)
        {
            toRet = mPickList.Select("CATEGORY = '" + Category + "'");
        }
        return toRet;
    }
    public void LoadCombo(string Name)
    {
        PickListDetails pld = (PickListDetails)controls[Name];

        DataRow[] dr = GetPickList(pld.Category);
        if (dr != null)
        {
            if (pld.InGrid == false)
            {
                DevExpress.Web.ASPxEditors.ASPxComboBox cbo = (DevExpress.Web.ASPxEditors.ASPxComboBox)pld.toUse;
                
                for (int i = 0; i < dr.Length; i++)
                {
                    cbo.Items.Add(dr[i]["PARAM_VALUE"].ToString());
                }
            }
        }
    }
    public void LoadCombo1(string Name)
    {
        PickListDetails pld = (PickListDetails)controls[Name];

        DataRow[] dr = GetPickList(pld.Category);
        if (dr != null)
        {
            if (pld.InGrid == false)
            {
                DropDownList cbo = (System.Web.UI.WebControls.DropDownList)pld.toUse;
                for (int i = 0; i < dr.Length; i++)
                {
                    cbo.Items.Add(dr[i]["PARAM_VALUE"].ToString());
                }
            }
        }
    }

    public void LoadCombo2(string Name)
    {
        PickListDetails pld = (PickListDetails)controls[Name];

        DataRow[] dr = GetPickList(pld.Category);
        if (dr != null)
        {
            if (pld.InGrid == false)
            {
                HtmlSelect cbo = (System.Web.UI.HtmlControls.HtmlSelect)pld.toUse;
                for (int i = 0; i < dr.Length; i++)
                {
                    cbo.Items.Add(dr[i]["PARAM_VALUE"].ToString());
                }
            }
        }
    }

    public void Loadlistbox(string Name)
    {
        PickListDetails pld = (PickListDetails)controls[Name];

        DataRow[] dr = GetPickList(pld.Category);
        if (dr != null)
        {
            if (pld.InGrid == false)
            {
                DevExpress.Web.ASPxEditors.ASPxListBox list = (DevExpress.Web.ASPxEditors.ASPxListBox)pld.toUse;

                for (int i = 0; i < dr.Length; i++)
                {
                    list.Items.Add(dr[i]["PARAM_VALUE"].ToString());
                }
            }
        }
    }


}

