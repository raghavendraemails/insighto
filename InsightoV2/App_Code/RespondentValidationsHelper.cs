﻿using System.Web.UI.WebControls;
using Insighto.Business.Enumerations;
using Insighto.Data;
using Resources;

namespace App_Code
{
    /// <summary>
    /// Summary description for RespondentValidationsHelper
    /// </summary>
    public class RespondentValidationsHelper
    {
        /// <summary>
        /// Gets the expression validator.
        /// </summary>
        /// <param name="demographicId">The demographic id.</param>
        /// <param name="question">The question.</param>
        /// <param name="questionType">Type of the question.</param>
        /// <param name="answerId">The answer id.</param>
        /// <returns></returns>
        public static RegularExpressionValidator GetExpressionValidator(int demographicId, osm_surveyquestion question, QuestionType questionType, int answerId = 0)
        {
            var revTemp = new RegularExpressionValidator();
            switch (demographicId)
            {
                case 10:
                    revTemp.ID = GetFormattedId("revEml", question.QUESTION_ID, question.QUESTION_TYPE_ID, answerId);
                    revTemp.ValidationExpression = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    revTemp.ErrorMessage = @"Please enter a valid email";
                    break;
            }

            revTemp.CssClass = RespondentResources.ErrorMessageCssClass;
            revTemp.Display = ValidatorDisplay.Dynamic;
            revTemp.Visible = questionType == QuestionType.EmailAddress || questionType == QuestionType.Numeric_SingleRowBox;
            return revTemp;
        }

        /// <summary>
        /// Gets the required field validator.
        /// </summary>
        /// <param name="question">The question.</param>
        /// <param name="questionType">Type of the question.</param>
        /// <param name="answerId">The answer id.</param>
        /// <returns></returns>
        public static RequiredFieldValidator GetRequiredFieldValidator(osm_surveyquestion question, QuestionType questionType, int answerId = 0)
        {
            var rfvTemp = new RequiredFieldValidator
                              {
                                  ID = GetFormattedId("rfv", question.QUESTION_ID, question.QUESTION_TYPE_ID, answerId),
                                  ErrorMessage = CommonMessages.Respondent_Mandatory,
                                  Display = ValidatorDisplay.Dynamic,
                                  CssClass = RespondentResources.ErrorMessageCssClass,
                                  Visible = false
                              };
            return rfvTemp;
        }


        /// <summary>
        /// Gets the formatted id.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <param name="answerId">The answer id.</param>
        /// <param name="questionId">The question id.</param>
        /// <param name="questionTypeId">The question type id.</param>
        /// <returns></returns>
        public static string GetFormattedId(string prefix, int questionId, int? questionTypeId, int answerId = 0)
        {
            return string.Format("{0}{1}_{2}_{3}", prefix, questionTypeId, questionId, answerId);
        }
    }
}