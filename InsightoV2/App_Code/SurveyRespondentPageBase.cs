﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace App_Code
{
    /// <summary>
    /// Summary description for BasePage
    /// </summary>
    public class SurveyRespondentPageBase : BasePage
    {
        private Hashtable _queryStringData = new Hashtable();
        /// <summary>
        /// Gets the query string data.
        /// </summary>
        public Hashtable QueryStringData
        {
            get
            {
                //if (_queryStringData.Count == 0)
                _queryStringData = EncryptHelper.DecryptQuerystringParam(Request.QueryString["Key"].Replace(' ', '+'));
                return _queryStringData;
               
            }
        }

        
        /// <summary>
        /// Determines whether [is survey has valid key].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is survey has valid key]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsSurveyHasValidKey
        {
            get
            {
                return (QueryStringData != null) && (SurveyBasicInfoView != null);
            }

        }
        
        

                   

        private int _surveyId;
        /// <summary>
        /// Gets the survey id.
        /// </summary>
        public int SurveyId
        {
            get
            {
                if (_surveyId == 0)
                    _surveyId = ValidationHelper.GetInteger(QueryStringData["SurveyId"], 0);

                return _surveyId;

            }
        }

        private int _passwordVerified;
        /// <summary>
        /// Gets or sets the password verified.
        /// </summary>
        /// <value>
        /// The password verified.
        /// </value>
        public int PasswordVerified
        {
            get
            {
                var hdnPasswordVerified = Utilities.FindControl<HiddenField>(Page, "hdnPasswordVerified");
                _passwordVerified = hdnPasswordVerified != null ? ValidationHelper.GetInteger(hdnPasswordVerified.Value, 0) : 0;
                if (_passwordVerified == 0)
                    _passwordVerified = ValidationHelper.GetInteger(QueryStringData["Pass_Verf"], 0);

                return _passwordVerified;
            }
            set
            {
                _passwordVerified = value;
                var hdnPasswordVerified = Utilities.FindControl<HiddenField>(Page, "hdnPasswordVerified");
                if (hdnPasswordVerified != null)
                    hdnPasswordVerified.Value = _passwordVerified.ToString();
            }
        }

        private int _respondentId;
        /// <summary>
        /// Gets or sets the respondent id.
        /// </summary>
        /// <value>
        /// The respondent id.
        /// </value>
        public virtual int RespondentId
        {
            get
            {
                var hdnRespondentId = Utilities.FindControl<HtmlInputHidden>(Page, "hdnRespondentId");
                _respondentId = hdnRespondentId != null ? ValidationHelper.GetInteger(hdnRespondentId.Value, 0) : 0;
                if (_respondentId == 0)
                    _respondentId = ValidationHelper.GetInteger(QueryStringData["RespondentId"], 0);

                return _respondentId;
            }
            set
            {
                _respondentId = value;
                var hdnRespondentId = Utilities.FindControl<HtmlInputHidden>(Page, "hdnRespondentId");
                if (hdnRespondentId != null)
                    hdnRespondentId.Value = _respondentId.ToString();
            }

        }

        //ToDo: Refactor for performance
        private SurveyBasicInfoView _surveyBasicInfoView;
        /// <summary>
        /// Gets the survey basic info view.
        /// </summary>
        public SurveyBasicInfoView SurveyBasicInfoView
        {
            get
            {
                if (SurveyId == 0)
                    return null;

                return _surveyBasicInfoView ??
                       (_surveyBasicInfoView =
                        ServiceFactory.GetService<RespondentService>().GetSurveyViewInformation(SurveyId));
            }
        }

        private List<SurveyQuestionAndAnswerOptions> _surveyQuesAndAnswerOptionsList = new List<SurveyQuestionAndAnswerOptions>();
        /// <summary>
        /// Gets the survey ques and answer options list.
        /// </summary>
        public List<SurveyQuestionAndAnswerOptions> SurveyQuesAndAnswerOptionsList
        {
            get
            {

                if (!_surveyQuesAndAnswerOptionsList.Any())
                    _surveyQuesAndAnswerOptionsList = ServiceFactory.GetService<RespondentService>().GetSurveyQuestionAndAnswerOptionsList(SurveyId, true, true, true);

                return _surveyQuesAndAnswerOptionsList;
            }
        }

        private int _pageIndex;
        /// <summary>
        /// Gets or sets the index of the page.
        /// </summary>
        /// <value>
        /// The index of the page.
        /// </value>
        public int PageIndex
        {
            get
            {
                if (_pageIndex == 0)
                    _pageIndex = ValidationHelper.GetInteger(QueryStringData["PageIndex"], 1);

                return _pageIndex;
            }

            set
            {
                _pageIndex = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is last page.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is last page; otherwise, <c>false</c>.
        /// </value>
        public bool IsLastPage
        {
            get
            {
                var lastPage = SurveyQuesAndAnswerOptionsList.OrderByDescending(p => p.PageIndex).Select(p => p.PageIndex).FirstOrDefault();

                return Mode == RespondentDisplayMode.Preview ? true : (lastPage == PageIndex);
            }

        }

        private RespondentDisplayMode _mode;
        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        /// <value>
        /// The mode.
        /// </value>
        public RespondentDisplayMode Mode
        {
            get
            {
                _mode = Utilities.ToEnum<RespondentDisplayMode>(Utilities.ToString(QueryStringData["Mode"]));
                return _mode;
            }
            set
            {
                _mode = value;
            }
        }

        private int _emailId;
        /// <summary>
        /// Gets or sets the email id.
        /// </summary>
        /// <value>
        /// The email id.
        /// </value>
        public int EmailId
        {
            get
            {
                if (_emailId == 0)
                  
                return _emailId = ValidationHelper.GetInteger(QueryStringData["emID"], 0);
                return _emailId;

            }
            set { _emailId = value; }

        }
        private string _embed;
        /// <summary>
        /// Gets or sets the email id.
        /// </summary>
        /// <value>
        /// The email id.
        /// </value>
        public string Embed
        {
            get
            {
                if (_embed == null)
                    return _embed = ValidationHelper.GetString(QueryStringData["Embed"]);

                return _embed;

            }
            set { _embed = value; }

        }

        private int _launchId;
        /// <summary>
        /// Gets or sets the launch id.
        /// </summary>
        /// <value>
        /// The launch id.
        /// </value>
        public int LaunchId
        {
            get
            {
                if (_launchId == 0)
                    return _launchId = ValidationHelper.GetInteger(QueryStringData["LauId"], 0);

                return _launchId;

            }
            set { _launchId = value; }

        }

        /// <summary>
        /// Gets the last rank.
        /// </summary>
        public int LastRank
        {
            get
            {
                var rankPage = SurveyQuesAndAnswerOptionsList.OrderByDescending(p => p.RankIndex).FirstOrDefault();
                return rankPage != null ? rankPage.RankIndex : 0;
            }
        }

        /// <summary>
        /// Gets the response count.
        /// </summary>
        public int ResponseCount
        {
            get
            {
                return ServiceFactory.GetService<RespondentService>().GetResponseCount(SurveyId);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is survey launched.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is survey launched; otherwise, <c>false</c>.
        /// </value>
        public bool IsSurveyLaunched
        {
            get
            {
                switch (SurveyBasicInfoView.SCHEDULE_ON)
                {
                    case 0:
                        return Utilities.ToEnum<SurveyStatus>(SurveyBasicInfoView.STATUS) == SurveyStatus.Active;

                    case 1:
                        return SurveyBasicInfoView.LAUNCH_ON_DATE <= DateTime.UtcNow;

                    default:
                        return false;
                }
            }
        }

        public bool IsSurveyClosed
        {
            get
            {
                switch (SurveyBasicInfoView.SCHEDULE_ON_CLOSE)
                {
                    case 2:
                        return Utilities.ToEnum<SurveyStatus>(SurveyBasicInfoView.STATUS) == SurveyStatus.Active;

                    case 1:
                        return DateTime.UtcNow <= SurveyBasicInfoView.CLOSE_ON_DATE;

                    case 0:
                        if (Utilities.ToEnum<SurveyStatus>(SurveyBasicInfoView.STATUS) == SurveyStatus.Draft)
                            return Utilities.ToEnum<SurveyStatus>(SurveyBasicInfoView.STATUS) == SurveyStatus.Active;
                        else
                            return ResponseCount < SurveyBasicInfoView.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS;

                    default:
                        return false;
                }
            }
        }

        /// <summary>
        /// Gets the type of the current respondent.
        /// </summary>
        /// <value>
        /// The type of the current respondent.
        /// </value>
        private List<osm_surveylaunch> _osmLaunchInfo = new List<osm_surveylaunch>();
        public RespondentType CurrentRespondentType
        {
            get
            {
                if (!_osmLaunchInfo.Any())
                    _osmLaunchInfo = ServiceFactory<RespondentService>.Instance.GetSurveyLaunchDetails(SurveyId);

                if (LaunchId > 0)
                    return (Utilities.ToEnum<RespondentType>(_osmLaunchInfo.Where(li => li.LAUNCH_ID == LaunchId).FirstOrDefault().UNIQUE_RESPONDENT));
                else
                    return (Utilities.ToEnum<RespondentType>(SurveyBasicInfoView.UNIQUE_RESPONDENT));
            }
        }

        /// <summary>
        /// Gets a value indicating whether [show two column lay out].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [show two column lay out]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowTwoColumnLayOut
        {
            get
            {
                return QueryStringData["Layout"] != null && Utilities.ToEnum<RespondentLayOutTypes>(Convert.ToInt32(QueryStringData["Layout"].ToString())) == RespondentLayOutTypes.TwoColumn;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is password user.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is password user; otherwise, <c>false</c>.
        /// </value>
        public bool IsPasswordUser
        {
            get
            {
                if (SurveyBasicInfoView == null)
                    return false;

                return CurrentRespondentType == RespondentType.OnlyOnceWithPassword;
            }

        }

        public bool IsSurveyLaunchedByDate
        {
            get
            {
                return SurveyBasicInfoView.LAUNCH_ON_DATE <= DateTime.UtcNow;
            }
        }

        public bool IsOncePerComputerUserCompletedResponses
        {
            get
            {
                return ServiceFactory<RespondentService>.Instance.IsUserFilledSurveyComplete(SurveyId, EmailId, LaunchId);
            }

        }
    }
}