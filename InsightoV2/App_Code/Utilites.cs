﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Business.geoip;
using Insighto.Data;
using System.Text;


namespace App_Code
{
    /// <summary>
    /// Summary description for Utilites
    /// </summary>
    public static class Utilities
    {
        public static string ResourceMessage(string resourceExpr)
        {
            string virtualpath = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;
            return (String)HttpContext.GetLocalResourceObject(virtualpath, resourceExpr);
        }

        /// <summary>
        /// Finds the control.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="startingControl">The starting control.</param>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public static T FindControl<T>(Control startingControl, string id) where T : Control
        {
            T found = null;

            foreach (Control activeControl in startingControl.Controls)
            {
                found = activeControl as T;

                if (found == null)
                {
                    found = FindControl<T>(activeControl, id);
                }
                else if (string.Compare(id, found.ID, true) != 0)
                {
                    found = null;
                }

                if (found != null)
                {
                    break;
                }
            }
            return found;
        }

        /// <summary>
        /// Convert to String
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static string ToString(object source)
        {
            try
            {
                return source != null ? Convert.ToString(source) : string.Empty;
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// Strings to enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumString">The enum string.</param>
        /// <returns></returns>
        public static T ToEnum<T>(string enumString)
        {
            if (!string.IsNullOrEmpty(enumString) && Enum.IsDefined(typeof(T), enumString))
                return (T)Enum.Parse(typeof(T), enumString);
            return default(T);
        }

        public static T ToEnum<T>(int enumValue)
        {
            if (Enum.IsDefined(typeof(T), enumValue))
                return (T)Enum.ToObject(typeof(T), enumValue);
            return default(T);
        }

        public static List<T> EnumToList<T>()
        {

            Type enumType = typeof(T);

            // Can't use type constraints on value types, so have to do check like this
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            // ReSharper disable AssignNullToNotNullAttribute
            return new List<T>(Enum.GetValues(enumType) as IEnumerable<T>);
            // ReSharper restore AssignNullToNotNullAttribute
        }



        public static string Getchecksum(string merchantId, string orderId, string amount, string redirectUrl, string workingKey)
        {
            string str = merchantId + "|" + orderId + "|" + amount + "|" + redirectUrl + "|" + workingKey;
            const int adler = 1;
            return Adler32(adler, str);
        }

        public static string Verifychecksum(string merchantId, string orderId, string amount, string authDesc, string workingKey, string checksum)
        {
            var str = merchantId + "|" + orderId + "|" + amount + "|" + authDesc + "|" + workingKey;
            const long adler = 1;
            var adlerResult = Adler32(adler, str);

            string retval = string.Compare(adlerResult, checksum, true) == 0 ? "true" : "false";
            return retval;
        }


        private static string Adler32(long adler, string strPattern)
        {
            // ReSharper disable InconsistentNaming
            const long BASE = 65521;
            // ReSharper restore InconsistentNaming
            var s1 = Andop(adler, 65535);
            var s2 = Andop(Cdec(Rightshift(Cbin(adler), 16)), 65535);

            for (var n = 0; n < strPattern.Length; n++)
            {

                var testchar = (strPattern.Substring(n, 1)).ToCharArray();
                var intTest = (long)testchar[0];
                s1 = (s1 + intTest) % BASE;
                s2 = (s2 + s1) % BASE;
            }
            return (Cdec(Leftshift(Cbin(s2), 16)) + s1).ToString();
        }


        private static long Power(long num)
        {
            long result = 1;
            for (var i = 1; i <= num; i++)
            {
                result = result * 2;
            }
            return result;
        }


        private static long Andop(long op1, long op2)
        {
            var op = "";
            var op3 = Cbin(op1);
            var op4 = Cbin(op2);
            for (var i = 0; i < 32; i++)
            {
                op = op + "" + ((long.Parse(op3.Substring(i, 1))) & (long.Parse(op4.Substring(i, 1))));
            }
            return Cdec(op);
        }

        private static string Cbin(long num)
        {
            var bin = "";
            do
            {
                bin = (((num % 2)) + bin);
                num = (long)Math.Floor(Convert.ToDouble(num) / 2);
            } while (num != 0);

            long tempCount = 32 - bin.Length;

            for (var i = 1; i <= tempCount; i++)
            {
                bin = "0" + bin;
            }
            return bin;
        }


        private static string Leftshift(string str, long num)
        {
            long tempCount = 32 - str.Length;

            for (var i = 1; i <= tempCount; i++)
            {

                str = "0" + str;
            }

            for (var i = 1; i <= num; i++)
            {
                str = str + "0";
                str = str.Substring(1, str.Length - 1);
            }
            return str;
        }


        private static string Rightshift(string str, long num)
        {

            for (var i = 1; i <= num; i++)
            {
                str = "0" + str;
                str = str.Substring(0, str.Length - 1);
            }
            return str;
        }

        private static long Cdec(string strNum)
        {
            long dec = 0;
            for (var n = 0; n < strNum.Length; n++)
            {
                dec = dec + long.Parse(strNum.Substring(n, 1)) * Power(strNum.Length - (n + 1));
            }
            return dec;
        }

        /// <summary>
        /// To the int.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        public static int ToInt(string source, out int target)
        {
            return int.TryParse(source, out target) ? target : 0;
        }

        /// <summary>
        /// To the int.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static int ToInt(string source)
        {
            int target;
            return int.TryParse(source, out target) ? target : 0;
        }

        /// <summary>
        /// Determines whether [is date between] [the specified my date time].
        /// </summary>
        /// <param name="myDateTime">My date time.</param>
        /// <param name="startDateTime">The start date time.</param>
        /// <param name="endDateTime">The end date time.</param>
        /// <returns>
        ///   <c>true</c> if [is date between] [the specified my date time]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsDateBetween(DateTime myDateTime, DateTime startDateTime, DateTime endDateTime)
        {
            return (myDateTime >= startDateTime && myDateTime <= endDateTime) ? true : false;
        }

        /// <summary>
        /// Sets the size of the font.
        /// </summary>
        /// <param name="fontSiz">The font siz.</param>
        /// <returns></returns>
        public static FontUnit SetFontSize(string fontSiz)
        {
            var fsize = FontUnit.Small;
            if (!string.IsNullOrEmpty(fontSiz))
            {
                switch (fontSiz.Trim())
                {
                    case "7":
                        fsize = FontUnit.XXSmall;
                        break;
                    case "8":
                        fsize = FontUnit.XSmall;
                        break;
                    case "9":
                        fsize = FontUnit.Small;
                        break;
                    case "10":
                        fsize = FontUnit.Medium;
                        break;
                    case "11":
                        fsize = FontUnit.Large;
                        break;
                    case "12":
                        fsize = FontUnit.XLarge;
                        break;
                    case "13":
                        fsize = FontUnit.XXLarge;
                        break;
                }
            }
            return fsize;
        }

        /// <summary>
        /// Gets the font size value.
        /// </summary>
        /// <param name="fontSiz">The font siz.</param>
        /// <returns></returns>
        public static string GetFontSizeValue(string fontSiz)
        {
            string fSize = "";
            if (!string.IsNullOrEmpty(fontSiz))
            {

                switch (fontSiz.Trim())
                {
                    case "7":
                        fSize = "XX-Small";
                        break;
                    case "8":
                        fSize = "X-Small";
                        break;
                    case "9":
                        fSize = "Small";
                        break;
                    case "10":
                        fSize = "Medium";
                        break;
                    case "11":
                        fSize = "Large";
                        break;
                    case "12":
                        fSize = "X-Large";
                        break;
                    case "13":
                        fSize = "XX-Large";
                        break;

                }
            }
            return fSize;
        }

        public static string GetFontColor(string fontColor)
        {
            var clr = "Black";
            if (!string.IsNullOrEmpty(fontColor))
            {
                switch (fontColor)
                {
                    case "AliceBlue":
                        clr = "AliceBlue";
                        break;
                    case "Amber":
                        clr = "#FF9900";
                        break;
                    case "AntiqueWhite":
                        clr = "AntiqueWhite";
                        break;
                    case "Aqua":
                        clr = "Aqua";
                        break;
                    case "Aquamarine":
                        clr = "Aquamarine";
                        break;
                    case "Azure":
                        clr = "Azure";
                        break;
                    case "Beige":
                        clr = "Beige";
                        break;
                    case "Bisque":
                        clr = "Bisque";
                        break;
                    case "Black":
                        clr = "Black";
                        break;
                    case "BlanchedAlmond":
                        clr = "BlanchedAlmond";
                        break;
                    case "Blue":
                        clr = "Blue";
                        break;
                    case "BlueViolet":
                        clr = "BlueViolet";
                        break;
                    case "Brown":
                        clr = "Brown";
                        break;
                    case "BurlyWood":
                        clr = "BurlyWood";
                        break;
                    case "Burnt Orange":
                        clr = "#993300";
                        break;
                    case "Dark azure":
                        clr = "#003366";
                        break;
                    case "Dark green":
                        clr = "DarkGreen";
                        break;
                    case "Dark olive":
                        clr = "#333300";
                        break;
                    case "Gold":
                        clr = "Gold";
                        break;
                    case "Gray":
                        clr = "Gray";
                        break;
                    case "Grayish blue":
                        clr = "#666699";
                        break;
                    case "Green":
                        clr = "Green";
                        break;
                    case "Indigo":
                        clr = "Indigo";
                        break;
                    case "Light sky blue":
                        clr = "SkyBlue";
                        break;
                    case "Light yellow":
                        clr = "LightYellow";
                        break;
                    case "Lime":
                        clr = "Lime";
                        break;
                    case "Magneta":
                        clr = "Magenta";
                        break;
                    case "Maroon":
                        clr = "Maroon";
                        break;
                    case "Medium gray":
                        clr = "#999999";
                        break;
                    case "Navy Blue":
                        clr = "#000080";
                        break;
                    case "Olive":
                        clr = "Olive";
                        break;
                    case "Pale cyan":
                        clr = "#CCFFFF";
                        break;
                    case "Pale green":
                        clr = "PaleGreen";
                        break;
                    case "Plum":
                        clr = "Plum";
                        break;
                    case "Peach":
                        clr = "#FFCC99";
                        break;
                    case "Pink":
                        clr = "Pink";
                        break;
                    case "Purple":
                        clr = "Purple";
                        break;
                    case "Red":
                        clr = "Red";
                        break;
                    case "Royal blue":
                        clr = "RoyalBlue";
                        break;
                    case "Sea green":
                        clr = "SeaGreen";
                        break;
                    case "Silver":
                        clr = "Silver";
                        break;
                    case "Sky blue":
                        clr = "SkyBlue";
                        break;
                    case "Teal":
                        clr = "Teal";
                        break;
                    case "Turquoise":
                        clr = "Turquoise";
                        break;
                    case "Very dark gray":
                        clr = "#333333";
                        break;
                    case "White":
                        clr = "White";
                        break;
                    case "Yellow":
                        clr = "Yellow";
                        break;
                    case "Yellow green":
                        clr = "YellowGreen";
                        break;

                }
            }
            return clr;
        }

        /// <summary>
        /// Toes the int.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static int ToInt(object source)
        {
            int target;
            if (source is int)
                return (int)source;
            else
                return int.TryParse(source as string, out target) ? target : 0;
        }

        /// <summary>
        /// Fills the drop down list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dropdown">The dropdown.</param>
        /// <param name="listCollection">The list collection.</param>
        /// <param name="textField">The text field.</param>
        /// <param name="valueField">The value field.</param>
        /// <returns></returns>
        public static void FillDropDownList<T>(DropDownList dropdown, IList<T> listCollection, string textField,
                                               string valueField) where T : class
        {
            FillDropDownList(dropdown, listCollection, textField, valueField, true);
        }

        /// <summary>
        /// Fills the drop down list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dropdown">The dropdown.</param>
        /// <param name="listCollection">The list collection.</param>
        /// <param name="textField">The text field.</param>
        /// <param name="valueField">The value field.</param>
        /// <param name="isAdditionalItemRequired">if set to <c>true</c> [is additional item required].</param>
        private static void FillDropDownList<T>(ListControl dropdown, ICollection<T> listCollection, string textField,
                                                string valueField, bool isAdditionalItemRequired) where T : class
        {
            dropdown.Items.Clear();
            if (listCollection.Count > 0)
            {
                dropdown.DataSource = listCollection;
                dropdown.DataTextField = textField;
                dropdown.DataValueField = valueField;
                dropdown.DataBind();
            }
            if (!isAdditionalItemRequired) return;
            dropdown.Items.Insert(0, "-- Select One --");
            dropdown.Items[0].Value = @"0";
        }

        /// <summary>
        /// Fills the answer options.
        /// </summary>
        /// <param name="lstControl">The LST control.</param>
        /// <param name="source">The source.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="answerIds">The answer ids.</param>
        /// <param name="txtOther"></param>
        /// <param name="isRandomizeOptions"></param>
        /// <param name="isAdditionalItemRequired">if set to <c>true</c> [is additional item required].</param>
        /// <param name="textField">The text field.</param>
        /// <param name="valueField">The value field.</param>
        public static void FillAnswerOptions(ListControl lstControl, SurveyQuestionAndAnswerOptions source, string prefix, List<osm_responsequestions> answerIds, TextBox txtOther, bool isRandomizeOptions = false, bool isAdditionalItemRequired = false, string textField = null, string valueField = null)
        {
            if (lstControl == null) return;
            lstControl.Items.Clear();
            var radioButtonList = new RadioButtonList();
            var checkBoxList = new CheckBoxList();
            if (lstControl is RadioButtonList)
            {
                radioButtonList = lstControl as RadioButtonList;
                if (source.Question.ANSWER_ALIGNSTYLE == RepeatDirection.Horizontal.ToString())
                    radioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                else
                    radioButtonList.RepeatColumns = ValidationHelper.GetInteger(source.Question.ANSWER_ALIGNSTYLE, 1);
            }
            else if (lstControl is CheckBoxList)
            {
                checkBoxList = lstControl as CheckBoxList;
                checkBoxList.RepeatColumns = ValidationHelper.GetInteger(source.Question.ANSWER_ALIGNSTYLE, 1);
                if (source.Question.ANSWER_ALIGNSTYLE == RepeatDirection.Horizontal.ToString())
                    checkBoxList.RepeatDirection = RepeatDirection.Horizontal;
                else
                    checkBoxList.RepeatColumns = ValidationHelper.GetInteger(source.Question.ANSWER_ALIGNSTYLE, 1);
            }

            lstControl.ValidationGroup = ToString(source.Question.QUESTION_ID);
            ListItem lstAnswerOption;
            var lstAnswerOptions = isRandomizeOptions == false
                                       ? source.AnswerOption
                                       : Shuffle(source.AnswerOption);
            foreach (var item in lstAnswerOptions)
            {
                lstAnswerOption = new ListItem
                                      {
                                          Text = item.ANSWER_OPTIONS,
                                          Value = SurveyRespondentHelper.FormatControlId(prefix,
                                                                                         source.Question.
                                                                                             QUESTION_TYPE_ID,
                                                                                         item.QUESTION_ID,
                                                                                         item.ANSWER_ID)
                                      };

                //check whether the response filling from halfwaythrough
                var response = GetResponseQuestion(answerIds, item.ANSWER_ID);
                lstAnswerOption.Selected = response != null;

                if (lstControl is RadioButtonList)
                    radioButtonList.Items.Add(lstAnswerOption);
                else if (lstControl is CheckBoxList)
                    checkBoxList.Items.Add(lstAnswerOption);
                else if (lstControl is DropDownList)
                    lstControl.Items.Add(lstAnswerOption);

                txtOther.Visible = source.Question.OTHER_ANS > 0;
                txtOther.Text = response != null ? response.ANSWER_TEXT : string.Empty;
            }

            if (!isAdditionalItemRequired || !(lstControl is DropDownList)) return;
            lstControl.Items.Insert(0, "-- Select --");
            lstControl.Items[0].Value = @"0";
        }

        /// <summary>
        /// Fills the question.
        /// </summary>
        /// <param name="lblQuestion">The LBL question.</param>
        /// <param name="source">The source.</param>
        /// <param name="ltrlMandatory">The LTRL mandatory.</param>
        /// <param name="rfvControl">The RFV control.</param>
        /// <param name="txtOther">The TXT other.</param>
        public static void FillQuestion(Label lblQuestion, SurveyQuestionAndAnswerOptions source, Literal ltrlMandatory, RequiredFieldValidator rfvControl, TextBox txtOther)
        {
            lblQuestion.Text = source.Question.QUSETION_LABEL;
            lblQuestion.Visible = true;
            ltrlMandatory.Visible = source.Question.RESPONSE_REQUIRED > 0;
            rfvControl.Visible = source.Question.RESPONSE_REQUIRED > 0;

            txtOther.Visible = source.Question.OTHER_ANS > 0;
        }

        public static T ToEnum<T>(int? questiontype)
        {
            if (questiontype == null)
                return default(T);

            return ToEnum<T>(ToInt(questiontype));
        }

        /// <summary>
        /// Gets the response question.
        /// </summary>
        /// <param name="answerIds">The answer ids.</param>
        /// <param name="target">The target.</param>
        /// <param name="isQuestionId">if set to <c>true</c> [is question id].</param>
        /// <returns></returns>
        public static osm_responsequestions GetResponseQuestion(List<osm_responsequestions> answerIds, int target, bool isQuestionId = false)
        {
            if (isQuestionId)
                return answerIds != null ? answerIds.Where(p => p.QUESTION_ID == target).FirstOrDefault() : null;
            return answerIds != null ? answerIds.Where(p => p.ANSWER_ID == target).FirstOrDefault() : null;
        }


        public static osm_answeroptions GetQuestioAnswerOption(List<osm_answeroptions> answerIds, int target, bool isQuestionId = false)
        {
            if (isQuestionId)
                return answerIds != null ? answerIds.Where(p => p.QUESTION_ID == target).FirstOrDefault() : null;
            return answerIds != null ? answerIds.Where(p => p.ANSWER_ID == target).FirstOrDefault() : null;
        }


        /// <summary>
        /// Gets the password.
        /// </summary>
        /// <param name="surveyId"></param>
        /// <param name="launchId">The launch id.</param>
        /// <param name="emailno">The emailno.</param>
        /// <returns></returns>
        public static string GetPassword(string surveyId, int launchId, int emailno)
        {
            if (launchId <= 0 || emailno <= 0 || ValidationHelper.GetInteger(surveyId, 0) <= 0)
                return string.Empty;

            string password = "";
            var surLength = surveyId.Length;
            var emailId = Convert.ToString(emailno);
            var length = emailId.Length;
            var value = emailId.Aggregate(0, (current, t) => Convert.ToInt32(t) + current) + launchId;

            if (length < 4)
            {
                if (length == 3)
                    emailId = surveyId.Substring(0, 1) + emailId;
                else if (length == 2)
                    emailId = surveyId.Substring(0, 1) + Convert.ToString(launchId).Substring(0, 1);
                else if (length == 1)
                    emailId = string.Format("{0}{1}2", surveyId.Substring(0, 1), Convert.ToString(launchId).Substring(0, 1));

            }

            var surLength1 = surLength + emailno;
            surLength = Convert.ToString(surLength1).Length;

            var totalLen = emailId.Length + surLength1 + launchId;
            totalLen = totalLen * (length - 2);

            var totLen1 = Convert.ToString(totalLen).Length;
            password = password + GetRelativeCharacter(emailId.Substring(length - 1, 1)) + Convert.ToString(surLength1).Substring(surLength - 1, 1);

            password = password + GetRelativeCharacter(emailId.Substring(length - 3, 1)) + Convert.ToString(totalLen).Substring(totLen1 - 1, 1);

            password = surLength < 2
                           ? password + GetRelativeCharacter(Convert.ToString(surLength1).Substring(surLength - 1, 1))
                           : password + GetRelativeCharacter(Convert.ToString(surLength1).Substring(surLength - 2, 1));

            password = password + GetRelativeCharacter(Convert.ToString(totalLen).Substring(totLen1 - 1, 1));

            var valLen = Convert.ToString(value).Length;
            return password + Convert.ToString(value).Substring(valLen - 1, 1) + GetRelativeCharacter(Convert.ToString(value).Substring(valLen - 1, 1));
        }

        /// <summary>
        /// Gets the relative character.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns></returns>
        public static string GetRelativeCharacter(string number)
        {
            switch (Convert.ToChar(number))
            {
                case '0':
                    return "z";
                case '1':
                    return "B";
                case '2':
                    return "X";
                case '3':
                    return "d";
                case '4':
                    return "v";
                case '5':
                    return "F";
                case '6':
                    return "T";
                case '7':
                    return "h";
                case '8':
                    return "r";
                case '9':
                    return "J";
                default:
                    return number;
            }
        }

        /// <summary>
        /// Shuffles the specified list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        public static List<T> Shuffle<T>(List<T> list)
        {
            var randomizedList = new List<T>();
            var rnd = new Random();
            while (list.Count > 0)
            {
                var index = rnd.Next(0, list.Count); //pick a random item from the master list
                randomizedList.Add(list[index]); //place it at the end of the randomized list
                list.RemoveAt(index);
            }
            return randomizedList;
        }

        /// <summary>
        /// Determines whether the specified list control has value.
        /// </summary>
        /// <param name="listControl">The list control.</param>
        /// <param name="txtOther">The TXT other.</param>
        /// <returns>
        ///   <c>true</c> if the specified list control has value; otherwise, <c>false</c>.
        /// </returns>
        private static bool HasValue(ListControl listControl, TextBox txtOther)
        {
            var hasValue = false;
            if (listControl != null)
            {
                hasValue = listControl.SelectedItem != null && listControl.SelectedItem.Text != "-- Select --";

                hasValue = hasValue == true ? hasValue : txtOther != null && !string.IsNullOrEmpty(txtOther.Text);
            }
            else
                hasValue = hasValue == false ? txtOther != null && !string.IsNullOrEmpty(txtOther.Text) : hasValue;

            return hasValue;
        }

        /// <summary>
        /// Determines whether [is empty record] [the specified list control].
        /// </summary>
        /// <param name="listControl">The list control.</param>
        /// <param name="txtOther">The TXT other.</param>
        /// <returns>
        ///   <c>true</c> if [is empty record] [the specified list control]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEmptyRecord(ListControl listControl, TextBox txtOther)
        {
            return !Utilities.HasValue(listControl, txtOther);
        }

        /// <summary>
        /// Determines whether [is empty record] [the specified list control].
        /// </summary>
        /// <param name="listControl">The list control.</param>
        /// <returns>
        ///   <c>true</c> if [is empty record] [the specified list control]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEmptyRecord(ListControl listControl)
        {
            var hasValue = false;
            if (listControl != null)
            {
                hasValue = listControl.SelectedItem != null && listControl.SelectedItem.Text != "-- Select --";
            }
            return !hasValue;
        }

        /// <summary>
        /// Determines whether [is empty record] [the specified TXT other].
        /// </summary>
        /// <param name="txtOther">The TXT other.</param>
        /// <returns>
        ///   <c>true</c> if [is empty record] [the specified TXT other]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEmptyRecord(TextBox txtOther)
        {
            if (string.IsNullOrEmpty(txtOther.Text))
                return true;

            return false;
        }

        /// <summary>
        /// Mails the settings.
        /// </summary>
        /// <returns></returns>
        public static SmtpSection MailSettings()
        {
            Configuration configurationFile = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            MailSettingsSectionGroup mailSettings = configurationFile.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
            return mailSettings.Smtp;
        }

        /// <summary>
        /// Clears the HTML.
        /// </summary>
        /// <param name="htmlContent">Content of the HTML.</param>
        /// <returns></returns>
        public static string ClearHTML(string htmlContent)
        {
            string plainHTML = "";
            if (htmlContent != null)
            {
                string pattern1html = "<(.|\n)+?>";
                plainHTML = Regex.Replace(htmlContent, pattern1html, string.Empty);
                string patternHtml = "<.*?>";
                string patternNewline = "\".";
                string patternNbsp = "&.*";
                plainHTML = Regex.Replace(plainHTML, patternHtml, string.Empty);
                plainHTML = plainHTML.Replace("&lt;", "<");
                plainHTML = plainHTML.Replace("&gt;", ">");
                plainHTML = plainHTML.Replace("&quot;", "\"");
                plainHTML = plainHTML.Replace("&apos;", "'");
                plainHTML = plainHTML.Replace("&amp;", "&");
                plainHTML = plainHTML.Replace("&nbsp;", " ");
                plainHTML = plainHTML.Replace("&copy;", "\u00a9");

            }
            return (plainHTML);
        }

        /// <summary>
        /// MIMEs the type.
        /// </summary>
        /// <param name="Extension">The extension.</param>
        /// <returns></returns>
        public static string MimeType(string Extension)
        {
            string mime = "application/octetstream";
            if (string.IsNullOrEmpty(Extension))
                return mime;

            string ext = Extension.ToLower();
            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (rk != null && rk.GetValue("Content Type") != null)
                mime = rk.GetValue("Content Type").ToString();
            return mime;
        }

        /// <summary>
        /// Toes the data table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(
                    prop.Name,
                    (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        ? Nullable.GetUnderlyingType(prop.PropertyType)
                        : prop.PropertyType
                );
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static string GetHtmlString(string requestUrl)
        {
            var content = string.Empty;
            try
            {
                var url = WebRequest.Create(requestUrl);
                var response = url.GetResponse();
                var input = new StreamReader(response.GetResponseStream());
                content = input.ReadToEnd();
            }
            catch { }
            return content;
        }

        public static string GetAbsoluteUrl(string relativeUrl, Page page)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return string.Empty;

            if (HttpContext.Current.Request.IsSecureConnection)
                return string.Format("https://{0}{1}", HttpContext.Current.Request.Url.Host, page.ResolveUrl(relativeUrl));
            else
                return string.Format("http://{0}{1}", HttpContext.Current.Request.Url.Host, page.ResolveUrl(relativeUrl));
        }

        public static CustomErrorsSection GetCustomErrorsSectionFromConfig()
        {
            Configuration configuration;
            configuration = WebConfigurationManager.OpenWebConfiguration("~");
            return (CustomErrorsSection)configuration.GetSection("system.web/customErrors");
        }

        public static ConfigurationSection GetConfigSection(string sectionName)
        {
            Configuration configuration;
            configuration = WebConfigurationManager.OpenWebConfiguration("~");
            return configuration.GetSection(sectionName);
        }

        public static bool WriteCustomErrorsSectionInConfig(string requestUrl)
        {
            try
            {
                Configuration configuration;
                configuration = WebConfigurationManager.OpenWebConfiguration("~");
                CustomErrorsSection customErrorSection = (CustomErrorsSection)configuration.GetSection("system.web/customErrors");
                customErrorSection.DefaultRedirect = requestUrl;
                configuration.Save();
                return true;
            }
            catch
            {
                return false;
            }

        }

        /// <summary>
        /// Resizes the image.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="originalFilename">The original filename.</param>
        /// <param name="canvasWidth">Width of the canvas.</param>
        /// <param name="canvasHeight">Height of the canvas.</param>
        /// <param name="originalWidth">Width of the original.</param>
        /// <param name="originalHeight">Height of the original.</param>
        /// <returns></returns>
        public static System.Drawing.Image ResizeImage(string path, string originalFilename, int canvasWidth, int canvasHeight, int originalWidth, int originalHeight)
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(path + originalFilename);

            System.Drawing.Image thumbnail = new Bitmap(canvasWidth, canvasHeight); // changed parm names
            System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(thumbnail);

            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphic.SmoothingMode = SmoothingMode.HighQuality;
            graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphic.CompositingQuality = CompositingQuality.HighQuality;

            /* ------------------ new code --------------- */

            // Figure out the ratio
            double ratioX = (double)canvasWidth / (double)originalWidth;
            double ratioY = (double)canvasHeight / (double)originalHeight;
            double ratio = ratioX < ratioY ? ratioX : ratioY; // use whichever multiplier is smaller

            // now we can get the new height and width
            int newHeight = Convert.ToInt32(originalHeight * ratio);
            int newWidth = Convert.ToInt32(originalWidth * ratio);

            // Now calculate the X,Y position of the upper-left corner 
            // (one of these will always be zero)
            int posX = Convert.ToInt32((canvasWidth - (image.Width * ratio)) / 2);
            int posY = Convert.ToInt32((canvasHeight - (image.Height * ratio)) / 2);

            graphic.Clear(Color.White); // white padding
            graphic.DrawImage(image, posX, posY, newWidth, newHeight);

            /* ------------- end new code ---------------- */

            System.Drawing.Imaging.ImageCodecInfo[] info = ImageCodecInfo.GetImageEncoders();
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            //thumbnail.Save(path + width + "." + originalFilename, info[1], encoderParameters);

            return thumbnail;
        }

        /// <summary>
        /// Resizes the image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="size">The size.</param>
        /// <param name="preserveAspectRatio">if set to <c>true</c> [preserve aspect ratio].</param>
        /// <returns></returns>
        public static System.Drawing.Image ResizeImage(System.Drawing.Image image, Size size, bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            System.Drawing.Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }

        /// <summary>
        /// Formats the currency.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="showISOCurrencySymbol">if set to <c>true</c> [show ISO currency symbol].</param>
        /// <returns></returns>
        public static string FormatCurrency(double amount, bool showBothSymbols, bool showISOCurrencySymbol = false)
        {
            var regionInfo = new RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name);
            if (showBothSymbols)
                return String.Format("{0} {1} {2}", regionInfo.ISOCurrencySymbol, System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol, amount);
            else
                return String.Format("{0}",amount.ToString("#0.00")); // return String.Format("{0} {1}", regionInfo.ISOCurrencySymbol, amount);
        }

        /// <summary>
        /// Shows the or hide feature.
        /// </summary>
        /// <param name="userType">Type of the user.</param>
        /// <param name="realTimeReports">The real time reports.</param>
        /// <returns></returns>
        public static bool ShowOrHideFeature(UserType userType, int featureId)
        {
            List<FeatureInfo> featuresList = HttpContext.Current.Cache[userType.ToString()] as List<FeatureInfo>;
            if (featuresList == null)
            {
                featuresList = ServiceFactory<FeatureService>.Instance.Find(userType).ToList();
                HttpContext.Current.Cache[userType.ToString()] = featuresList;
            }
            FeatureInfo feature = featuresList.Where(f => f.PK_FEATURE_ID == featureId).FirstOrDefault();
            return feature != null && Convert.ToInt32(feature.Value) > 0;
        }

        /// <summary>
        /// Gets the feature value.
        /// </summary>
        /// <param name="userType">Type of the user.</param>
        /// <param name="featureId">The feature id.</param>
        /// <returns></returns>
        public static int? GetFeatureValue(UserType userType, int featureId)
        {
            List<FeatureInfo> featuresList = HttpContext.Current.Cache[userType.ToString()] as List<FeatureInfo>;
            if (featuresList == null)
            {
                featuresList = ServiceFactory<FeatureService>.Instance.Find(userType).ToList();
                HttpContext.Current.Cache[userType.ToString()] = featuresList;
            }
            FeatureInfo feature = featuresList.Where(f => f.PK_FEATURE_ID == featureId).FirstOrDefault();
            return feature != null ? feature.Value : null;
        }

        /// <summary>
        /// Gets the type of the user.
        /// </summary>
        /// <returns></returns>
        public static UserType GetUserType()
        {
            LoggedInUserInfo userDetails = HttpContext.Current.Session["UserInfo"] as LoggedInUserInfo;
            if (userDetails != null)
                return GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            else
                return UserType.None;
        }

        /// <summary>
        /// Fills the radio button list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="radioButtonList">The radio button list.</param>
        /// <param name="listCollection">The list collection.</param>
        /// <param name="textField">The text field.</param>
        /// <param name="valueField">The value field.</param>
        public static void FillRadioButtonList<T>(RadioButtonList radioButtonList, IList<T> listCollection, string textField,
                                               string valueField) where T : class
        {
            FillDropDownList(radioButtonList, listCollection, textField, valueField, false);
        }

        public static bool IsNegative(this int number)
        {
            return number < 0;
        }

        /// <summary>
        /// Trims the specified target value.
        /// </summary>
        /// <param name="targetValue">The target value.</param>
        /// <returns></returns>
        public static string Trim(string targetValue)
        {
            return !string.IsNullOrEmpty(targetValue) ? targetValue.Trim() : string.Empty;
        }
         
        static Random random = new Random((int)DateTime.Now.Ticks);
        public static int GetRandomNumber(int min, int max)
        {
            return random.Next(min, max);
        }

        public static string GetCountryName(string ipAddress)
        {
            string a = "INDIA";
            //string filepath = "C://FTP DATA//Publishedfiles_7june2012//GeoLiteCity.dat";
            string filepath = ConfigurationManager.AppSettings["geolitepath"].ToString();
            try
            {
               // WebClient wClient = new WebClient();
               // string ipApiURL = ConfigurationManager.AppSettings["IpapiURL"].ToString();

               // byte[] c = wClient.DownloadData(string.Format("{0}{1}{2}", ipApiURL, "?ip=", "67.228.221.19")); //US
               // byte[] c = wClient.DownloadData(string.Format("{0}{1}{2}", ipApiURL, "?h=", "300.166.19.400"));  //India
               // byte[] c = wClient.DownloadData(string.Format("{0}{1}{2}", ipApiURL, "?h=", "124.125.101.26"));  //India
               //  byte[] c = wClient.DownloadData(string.Format("{0}{1}{2}", ipApiURL, "?h=", ipAddress));      

               //   c = Encoding.Convert(Encoding.GetEncoding("iso-8859-1"), Encoding.UTF8, c);
               //     string tempString = Encoding.UTF8.GetString(c, 0, c.Length);
               //     string[] strDetails = tempString.Split('\n');
               //     string[] str = strDetails[0].Split(',');
               //     a = str[3].Substring(1, str[3].Length - 2);

               // srini, 26/7/2012, unplug ipaddressapi and plug maxmind Geolite database
               // Doesn't work for localhost
                if (ipAddress == "127.0.0.1" || ipAddress == "localhost")
                    a = "INDIA";
                else
                {
                    LookupService ls = new LookupService(filepath);
                    Country country = null;
                    country = ls.getCountry(ipAddress);
                    //for localhost
                    if ((a = country.getName()) == "N/A")
                    {
                        a = "INDIA";
                    }
                }
            }
            catch (Exception ex)
            {
                a = "INDIA";
            }
           return a;
        }
        public static string GetCityName(string ipAddress)
        {
            string c = "Hyderabad";
            try
            {
                // srini, 27/5/2014, Get region
                // Doesn't work for localhost
                if (ipAddress == "127.0.0.1" || ipAddress == "localhost")
                    c = "Hyderabad";
                else
                {
                    LookupService ls = new LookupService(PathHelper.GetGeoliteDBFileWithPath());
                    c = ls.getLocation(ipAddress).city;
                    //for localhost
                    if (c == "N/A")
                    {
                        c = "Hyderabad";
                    }
                }
            }
            catch (Exception ex)
            {
                c = "Hyderabad";
            }
            return c;
        }
    }
}