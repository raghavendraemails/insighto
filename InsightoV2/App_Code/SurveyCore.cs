using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Insighto.Business.Charts;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using System.Data;
using System.Data.SqlClient;

public class SurveyCore
{
    SqlConnection objsql = new SqlConnection();
    public static System.Web.SessionState.HttpSessionState Sstate = null;
    DBManager dbm = new DBManager();
    string connect = ConfigurationManager.ConnectionStrings["InsightoConnString"].ToString();

    public SurveyCore()
    {
        //dbm.OpenConnection(connect);
    }

public void updateembedcode(string embedcode, int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updateembedcode", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@embedcode", SqlDbType.VarChar)).Value = embedcode;
            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.VarChar)).Value = surveyid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesurveylaunch = new DataSet();
            sda.Fill(dsupdatesurveylaunch);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
public int QuestionCount(int surveyid)
{
    try
    {
        SqlConnection con = new SqlConnection();
        con = strconnectionstring();
        SqlCommand scom = new SqlCommand("sp_getQuestionCount", con);
        scom.CommandType = CommandType.StoredProcedure;

        scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.VarChar)).Value = surveyid;
        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet dsquestions = new DataSet();
        sda.Fill(dsquestions);
        con.Close();
        int QuestionCount = Convert.ToInt32(dsquestions.Tables[0].Rows[0][0].ToString());
        return QuestionCount;
    }
    catch (Exception ex)
    {
        throw ex;
    }
}
    public Survey GetSurveyWithResponsesOnlineReport(int SurveyID, DateTime Startdate, DateTime Enddate)
    {
        try
        {
            Survey Sur = GetSurveyDetOnlineReport(SurveyID);
            return Sur;
        }
        catch
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public Survey GetSurveyWithResponsesCountFusion(int SurveyID, DateTime Startdate, DateTime Enddate)
    {
        try
        {
            Survey Sur = GetSurveyDetFusion(SurveyID);            
            return Sur;
        }
        catch
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    public Survey GetSurveyWithResponsesCount(int SurveyID, DateTime Startdate, DateTime Enddate)
    {
        try
        {
            Survey Sur = GetSurveyDet(SurveyID);
            string RespondentIDS = GetActiveRespondent(SurveyID);
            dbm.OpenConnection(connect);
            if (Sur != null && Sur.SURVEY_ID > 0 && Sur.surveyQues != null && Sur.surveyQues.Count > 0 && RespondentIDS != null && RespondentIDS.Length > 0)
            {
                foreach (SurveyQuestion surQues in Sur.surveyQues)
                {
                    int QuesResCount = 0, AnsResCount = 0;
                    string qryQuesResp = "";
                    if (surQues.QUESTION_TYPE == 2)
                    {
                        qryQuesResp = "select count(Distinct RESPONDENT_ID) from osm_responsequestions where QUESTION_ID=" + surQues.QUESTION_ID + " and deleted=0";
                        if (RespondentIDS != null && RespondentIDS.Length > 0)
                            qryQuesResp += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                    }
                    else
                    {
                        qryQuesResp = "select count(Distinct RESPONDENT_ID) as RESPONSE_COUNT from osm_responsequestions where DELETED=0 and QUESTION_ID=" + surQues.QUESTION_ID;
                        if (RespondentIDS != null && RespondentIDS.Length > 0)
                            qryQuesResp += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                        if (Startdate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM") && Enddate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                        {
                            qryQuesResp += " and RESPONSE_RECEIVED_ON>='" + Startdate + "' and RESPONSE_RECEIVED_ON<='" + Enddate + "'";
                        }
                    }
                    object QCount = dbm.ExecuteScalar(CommandType.Text, qryQuesResp);
                    try
                    {
                        if (QCount != null)
                        {
                            QuesResCount = Convert.ToInt32(QCount);
                        }
                    }
                    catch
                    {
                    }
                    surQues.RESPONSE_COUNT = QuesResCount;
                    if (surQues.surveyAnswers != null && surQues.surveyAnswers.Count > 0)
                    {
                        foreach (SurveyAnswers ans in surQues.surveyAnswers)
                        {
                            //string qryAnsResp = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where ANSWER_ID=" + ans.ANSWER_ID + " and RESPONDENT_ID in(" + RespondentIDS + ")";
                            string qryAnsResp = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where DELETED=0 and ANSWER_ID=" + ans.ANSWER_ID;
                            if (RespondentIDS != null && RespondentIDS.Length > 0)
                                qryAnsResp += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                            if (Startdate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM") && Enddate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                            {
                                qryAnsResp += " and RESPONSE_RECEIVED_ON >='" + Startdate + "' and RESPONSE_RECEIVED_ON <='" + Enddate + "'";
                            }
                            object ACount = dbm.ExecuteScalar(CommandType.Text, qryAnsResp);
                            try
                            {
                                if (ACount != null)
                                {
                                    AnsResCount = Convert.ToInt32(ACount);
                                }
                            }
                            catch { }
                            ans.RESPONSE_COUNT = AnsResCount;
                        }
                        if (surQues.OTHER_ANS == 1)
                        {
                            if (surQues.QUESTION_TYPE == 1 || surQues.QUESTION_TYPE == 2 || surQues.QUESTION_TYPE == 3 || surQues.QUESTION_TYPE == 4)
                            {
                                //string qryAnsRespother = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where ANSWER_ID=0 and QUESTION_ID=" + surQues.QUESTION_ID + " and RESPONDENT_ID in(" + RespondentIDS + ")";
                                string qryAnsRespother = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where ANSWER_ID=0 and QUESTION_ID=" + surQues.QUESTION_ID;
                                if (RespondentIDS != null && RespondentIDS.Length > 0)
                                    qryAnsRespother += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                                if (Startdate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM") && Enddate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                                {
                                    qryAnsRespother += " and RESPONSE_RECEIVED_ON >='" + Startdate + "' and RESPONSE_RECEIVED_ON <='" + Enddate + "'";
                                }
                                object ACountothers = dbm.ExecuteScalar(CommandType.Text, qryAnsRespother);
                                try
                                {
                                    if (ACountothers != null)
                                    {
                                        surQues.OTHER_RESPCOUNT = Convert.ToInt32(ACountothers);
                                    }
                                }
                                catch { }

                            }
                        }
                    }
                }
            }
            return Sur;
        }
        catch
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    public void Updatesurveyques_other(int surveyId,int quesVal,int questionSeq,string other_txt )
    {
        
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);
            strconn.Open();
            SqlCommand scom = new SqlCommand("question_othertext", strconn);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@SURVEY_ID", SqlDbType.Int)).Value = surveyId;
            scom.Parameters.Add(new SqlParameter("@QUESTION_TYPE_ID", SqlDbType.Int)).Value = quesVal;
            scom.Parameters.Add(new SqlParameter("@QUESTION_SEQ", SqlDbType.Int)).Value =questionSeq ;
            scom.Parameters.Add(new SqlParameter("@Otner_text", SqlDbType.NVarChar)).Value = other_txt;
            int ans = scom.ExecuteNonQuery();
            strconn.Close();
    }
    public string Selectsurveyques_other(int QuestionId)
    {

        string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
        SqlConnection strconn = new SqlConnection(connStr);
        strconn.Open();
        SqlCommand scom = new SqlCommand("Selectquestion_othertext", strconn);
        scom.CommandType = CommandType.StoredProcedure;
        scom.Parameters.Add(new SqlParameter("@QUESTION_ID", SqlDbType.Int)).Value = QuestionId;
        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet ds = new DataSet();
        sda.SelectCommand = scom;
        sda.Fill(ds, "Other_txt");
        //DataTable dt= ds.Tables["Other_txt"];
        string othertext = ds.Tables[0].Rows[0][0].ToString();
        return othertext;       
    }
    public string GetActiveRespondent(int SurveyID)
    {
        int cont = 0;
        string usrLmitDet = GetUserLimit(SurveyID, "");
        int usrLmit = 0; string LienTyp = "";
        string[] det = usrLmitDet.Split('$');
        if (det != null && det.Length > 0)
        {
            LienTyp = Convert.ToString(det.GetValue(1));
            usrLmit = Convert.ToInt32(det.GetValue(0));
        }
        string RespondentIDs = "";
        string qry = "";
        if (usrLmit != -1 && usrLmit > 0)
        {

            //qry = "select TOP "+usrLmit +" RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and VISABLE_LIMIT=1 and SURVEY_ID=" + SurveyID;
            qry = "select TOP " + usrLmit + " RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
        }
        else if (usrLmit == -1)
        {
            qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;

        }
        //qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and VISABLE_LIMIT=1 and SURVEY_ID=" + SurveyID;
        try
        {
            dbm.OpenConnection(connect);
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qry);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    RespondentIDs += "," + Convert.ToString(ds.Tables[0].Rows[i]["RESPONDENT_ID"]);
                }
                if (RespondentIDs != null && RespondentIDs.Trim().Length > 0)
                    RespondentIDs = RespondentIDs.Substring(1);
            }
            return RespondentIDs;
        }
        catch
        {
            return RespondentIDs;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    public string CreateSurveyFromTemplate(int copySurveyID, int newSurveyID, string SURVEY_NAME, int SurveyFlag)
    {

        var surveyService = ServiceFactory.GetService<SurveyService>();
        surveyService.CopySurveyDet(copySurveyID, newSurveyID);
        var surveDet = surveyService.GetSurveyBySurveyId(newSurveyID);
        string url = EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + newSurveyID + "&surveyName=" + surveDet[0].SURVEY_NAME + "&surveyFlag=" + SurveyFlag);
        return url;
    }



    public string GetUserLimit(int SurveyID, string LicenseType)
    {
        string ret = ""; string fea_nam = "";
        string Qry = "select LICENSE_TYPE from osm_user where deleted=0 and USERID =(select USERID from osm_survey where deleted =0 and SURVEY_ID='" + SurveyID + "')";
        try
        {
            dbm.OpenConnection(connect);
            if (LicenseType != null && LicenseType.Trim().Length > 0)
            {
                fea_nam = LicenseType;
            }
            else
            {
                object obj = dbm.ExecuteScalar(CommandType.Text, Qry);
                if (obj != null)
                {
                    fea_nam = Convert.ToString(obj);
                }
            }
            if (fea_nam != null && fea_nam.Length > 0)
            {
                string QRtext = "select  " + fea_nam + "  from osm_features where FEATURE='NO_OF_RESPONSES' and deleted=0 ";
                object obj1 = dbm.ExecuteScalar(CommandType.Text, QRtext);
                if (obj1 != null)
                {
                    ret = Convert.ToString(obj1);
                }
            }
            ret = ret + "$" + fea_nam;
            return ret;
        }
        catch
        {
            return "";
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    public Survey GetSurveyDetOnlineReport(int SurveyID)
    {
        try
        {
            dbm.OpenConnection(connect);
            Survey sur = new Survey();
            if (SurveyID > 0)
            {
                string str = " select sur.SURVEY_NAME,sur.SURVEY_CATEGORY,sur.THEME,sur.FOLDER_ID,sur.USERID,sur.CREATED_ON,sur.STATUS,sur.CHECK_BLOCKWORDS ";
                str += " from osm_survey sur ";
                str += " where sur.SURVEY_ID= " + SurveyID;
                str += " select * from osm_surveyIntro where DELETED=0 and SURVEY_ID= " + SurveyID;
                string surSet = " select * from osm_surveysetting where DELETED=0 and SURVEY_ID= " + SurveyID;
                //order by is written by sandeep
                //string Queqry = " select * from osm_surveyquestion where DELETED=0 and SURVEY_ID= " + SurveyID + "  order by question_seq";
                string Queqry = "select QUESTION_ID,SURVEY_ID,QUESTION_TYPE_ID,ROW_NUMBER() OVER (ORDER BY question_seq ASC) AS QUESTION_SEQ,QUSETION_LABEL,SKIP_LOGIC, RESPONSE_REQUIRED,PAGE_BREAK,CREATED_ON,LASTMODIFIED_ON,DELETED,OTHER_ANS,RANDOMIZE_ANSWERS,CHART_ALIGNMENT,CHART_APPEARANCE,CHART_DOCK,CHART_PALETTE,CHART_TITLE,CHART_TYPE,COMMENTS,CHAR_LIMIT,ANSWER_ALIGNSTYLE,SHOW_LABEL,LABEL_ANGLE,VALUEAS_PERCENT,ZOOMPERCENT,TRANSPARENCY,PERSPECTIVE_ANGLE,MARKER_SIZE,SHOW_MARKER,INVERTED,LABEL_POSITION,MARKER_KIND,HOLE_RADIUS,DIAGRAM_TYPE,TEXT_DIRECTION,FUNCTION_TYPE,EXPLODED_POINT,LEGEND_DIRECTION,LEGEND_EQUALLYSPACED_ITEMS,LEGEND_HORIZONTALALIGN,LEGEND_VERTICALALIGN,LEGEND_MAX_HORIZONTALALIGN,LEGEND_MAX_VERTICALALIGN,HIDE_LEGAND,HIDE_XAXIS,STAGGERED,AudioFileName from osm_surveyquestion where DELETED=0 and SURVEY_ID= " + SurveyID + " and QUESTION_TYPE_ID not in(5,6,9,7,20,8,14,18,17,16,19,21) order by question_seq";

                string Conqry = " select * from osm_surveycontrols where DELETED=0 and SURVEY_ID= " + SurveyID;

                DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    sur.USER_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["USERID"]);
                    sur.SURVEY_ID = SurveyID;
                    sur.FOLDER_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["FOLDER_ID"]);
                    sur.STATUS = Convert.ToString(ds.Tables[0].Rows[0]["STATUS"]);
                    sur.SURVEY_CATEGORY = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_CATEGORY"]);
                    sur.THEME = Convert.ToString(ds.Tables[0].Rows[0]["THEME"]);
                    if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                    {
                        sur.SURVEY_INTRO = Convert.ToString(ds.Tables[1].Rows[0]["SURVEY_INTRO"]);
                        if (ds.Tables[1].Rows[0]["PAGE_BREAK"] != null)
                        {
                            sur.SURVEYINTRO_PAGEBREAK = Convert.ToInt32(ds.Tables[1].Rows[0]["PAGE_BREAK"]);
                        }
                    }
                    sur.SURVEY_NAME = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_NAME"]);
                    sur.CHECK_BLOCKWORDS = Convert.ToInt32(ds.Tables[0].Rows[0]["CHECK_BLOCKWORDS"]);

                    DataSet surSetds = dbm.ExecuteDataSet(CommandType.Text, surSet);
                    if (surSetds != null && surSetds.Tables.Count > 0 && surSetds.Tables[0].Rows.Count > 0)
                    {
                        sur.SURVEY_HEADER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_HEADER"]);
                        sur.SURVEY_FOOTER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FOOTER"]);
                        sur.SURVEY_FONT_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_TYPE"]);
                        sur.SURVEY_FONT_SIZE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_SIZE"]);
                        sur.SURVEY_FONT_COLOR = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_COLOR"]);
                        sur.SURVEY_BUTTON_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_BUTTON_TYPE"]);
                        sur.CUSTOMSTART_TEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSTART_TEXT"]);
                        sur.CUSTOMSUBMITTEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSUBMITTEXT"]);
                        sur.PAGE_BREAK = Convert.ToInt32(surSetds.Tables[0].Rows[0]["PAGE_BREAK"]);
                        sur.RESPONSE_REQUIRED = Convert.ToInt32(surSetds.Tables[0].Rows[0]["RESPONSE_REQUIRED"]);
                        sur.BROWSER_BACKBUTTON_STATUS = Convert.ToInt32(surSetds.Tables[0].Rows[0]["BROWSER_BACKBUTTON"]);
                        sur.SURVEY_LOGO = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_LOGO"]);
                    }
                    else
                    {
                        sur.BROWSER_BACKBUTTON_STATUS = 1;
                    }
                    DataSet Quesds = dbm.ExecuteDataSet(CommandType.Text, Queqry);
                    if (Quesds != null && Quesds.Tables.Count > 0 && Quesds.Tables[0].Rows.Count > 0)
                    {
                        sur.surveyQues = new List<SurveyQuestion>();
                        for (int i = 0; i < Quesds.Tables[0].Rows.Count; i++)
                        {
                            SurveyQuestion ques = new SurveyQuestion();
                            ques.CREATED_ON = Convert.ToDateTime(Quesds.Tables[0].Rows[i]["CREATED_ON"]);
                            ques.LASTMODIFIED_ON = Convert.ToDateTime(Quesds.Tables[0].Rows[i]["LASTMODIFIED_ON"]);
                            ques.PAGE_BREAK = Convert.ToInt32(Quesds.Tables[0].Rows[i]["PAGE_BREAK"]);
                            ques.QUESTION_ID = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_ID"]);
                            ques.QUESTION_LABEL = Convert.ToString(Quesds.Tables[0].Rows[i]["QUSETION_LABEL"]);
                            ques.QUESTION_SEQ = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_SEQ"]);
                            ques.QUESTION_TYPE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_TYPE_ID"]);
                            ques.RESPONSE_REQUIRED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RESPONSE_REQUIRED"]);
                            ques.RANDOM_ANSWERS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RANDOMIZE_ANSWERS"]);
                            ques.OTHER_ANS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["OTHER_ANS"]);
                            ques.SKIP_LOGIC = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SKIP_LOGIC"]);
                            ques.SURVEY_ID = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SURVEY_ID"]);
                            ques.CHART_ALIGNMENT = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_ALIGNMENT"]);
                            ques.CHART_APPEARANCE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_APPEARANCE"]);
                            ques.CHART_DOCK = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_DOCK"]);
                            ques.CHART_PALETTE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_PALETTE"]);
                            ques.CHART_TITLE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_TITLE"]);
                            ques.CHART_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_TYPE"]);
                            ques.COMMENTS = Convert.ToString(Quesds.Tables[0].Rows[i]["COMMENTS"]);
                            ques.CHAR_LIMIT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["CHAR_LIMIT"]);
                            ques.ANSWER_ALIGNSTYLE = Convert.ToString(Quesds.Tables[0].Rows[i]["ANSWER_ALIGNSTYLE"]);
                            ques.HIDE_LEGAND = Convert.ToInt32(Quesds.Tables[0].Rows[i]["HIDE_LEGAND"]);
                            ques.HIDE_XAXIS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["HIDE_XAXIS"]);
                            ques.STAGGERED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["STAGGERED"]);

                            ques.SHOW_LABEL = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SHOW_LABEL"]);
                            ques.LABEL_ANGLE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LABEL_ANGLE"]);
                            ques.VALUEAS_PERCENT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["VALUEAS_PERCENT"]);
                            ques.ZOOMPERCENT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["ZOOMPERCENT"]);
                            ques.TRANSPARENCY = Convert.ToInt32(Quesds.Tables[0].Rows[i]["TRANSPARENCY"]);
                            ques.PERSPECTIVE_ANGLE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["PERSPECTIVE_ANGLE"]);
                            ques.MARKER_SIZE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["MARKER_SIZE"]);
                            ques.SHOW_MARKER = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SHOW_MARKER"]);
                            ques.INVERTED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["INVERTED"]);
                            ques.LABEL_POSITION = Convert.ToString(Quesds.Tables[0].Rows[i]["LABEL_POSITION"]);
                            ques.MARKER_KIND = Convert.ToString(Quesds.Tables[0].Rows[i]["MARKER_KIND"]);
                            ques.HOLE_RADIUS = Convert.ToString(Quesds.Tables[0].Rows[i]["HOLE_RADIUS"]);
                            ques.DIAGRAM_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["DIAGRAM_TYPE"]);
                            ques.TEXT_DIRECTION = Convert.ToString(Quesds.Tables[0].Rows[i]["TEXT_DIRECTION"]);
                            ques.FUNCTION_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["FUNCTION_TYPE"]);
                            ques.EXPLODED_POINT = Convert.ToString(Quesds.Tables[0].Rows[i]["EXPLODED_POINT"]);
                            ques.LEGEND_DIRECTION = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_DIRECTION"]);
                            ques.LEGEND_EQUALLYSPACED_ITEMS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_EQUALLYSPACED_ITEMS"]);
                            ques.LEGEND_HORIZONTALALIGN = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_HORIZONTALALIGN"]);
                            ques.LEGEND_MAX_HORIZONTALALIGN = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_MAX_HORIZONTALALIGN"]);
                            ques.LEGEND_MAX_VERTICALALIGN = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_MAX_VERTICALALIGN"]);
                            ques.LEGEND_VERTICALALIGN = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_VERTICALALIGN"]);


                            sur.surveyQues.Add(ques);
                        }
                    }
                    DataSet Controlsds = dbm.ExecuteDataSet(CommandType.Text, Conqry);
                    if (Controlsds != null && Controlsds.Tables.Count > 0 && Controlsds.Tables[0].Rows.Count > 0)
                    {
                        sur.surveyOptns = new List<SurveyOptions>();
                        for (int j = 0; j < Controlsds.Tables[0].Rows.Count; j++)
                        {
                            SurveyOptions opt = new SurveyOptions();
                            opt.ALERT_ON_MAX_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ALERT_ON_MAX_RESPONSES"]);
                            opt.ALERT_ON_MAX_RESPONSES_BY_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["ALERT_ON_MAX_RESPONSES_BY_DATE"]);
                            opt.ALERT_ON_MIN_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ALERT_ON_MIN_RESPONSES"]);
                            opt.ALERT_ON_MIN_RESPONSES_BY_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["ALERT_ON_MIN_RESPONSES_BY_DATE"]);
                            opt.AUTOMATIC_THANKYOU_EMAIL = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["AUTOMATIC_THANKYOU_EMAIL"]);
                            opt.CLOSE_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["CLOSE_ON_DATE"]);
                            opt.CLOSE_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["CLOSE_PAGE"]);
                            opt.EMAIL_INVITATION = Convert.ToString(Controlsds.Tables[0].Rows[j]["EMAIL_INVITATION"]);
                            opt.EMAIL_FROMADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["FROM_MAILADDRESS"]);
                            opt.EMAIL_SUBJECT = Convert.ToString(Controlsds.Tables[0].Rows[j]["MAIL_SUBJECT"]);
                            opt.LAUNCH_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["LAUNCH_ON_DATE"]);
                            opt.NOOFRESPONSESTOCLOSEE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS"]);
                            opt.ONCLOSEALERT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE_ALERT"]);
                            opt.ONLAUNCH_ALERT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_ALERT"]);
                            opt.OVERQUOTA_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["OVERQUOTA_PAGE"]);
                            opt.PARTIAL_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["PARTIAL_RESPONSES"]);
                            opt.REMINDER_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["REMINDER_ON_DATE"]);
                            opt.REMINDER_TEXT = Convert.ToString(Controlsds.Tables[0].Rows[j]["REMINDER_TEXT"]);
                            opt.SAVE_AND_CONTINUE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SAVE_AND_CONTINUE"]);
                            opt.SCREEN_OUTS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCREEN_OUTS"]);
                            opt.SCREENOUT_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["SCREENOUT_PAGE"]);
                            opt.SEND_TO = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SEND_TO"]);
                            opt.STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["STATUS"]);
                            opt.SURVEY_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEY_ID"]);
                            opt.SURVEY_PASSWORD = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEY_PASSWORD"]);
                            opt.SURVEY_URL = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEY_URL"]);
                            opt.SURVEYCLOSETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE"]);
                            opt.SURVEYCONTROL_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEYCONTROL_ID"]);
                            opt.SURVEYLAUNCHTYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON"]);
                            opt.THANKYOU_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["THANKYOU_PAGE"]);
                            opt.TIME_FOR_URL_REDIRECTION = Convert.ToString(Controlsds.Tables[0].Rows[j]["TIME_FOR_URL_REDIRECTION"]);
                            opt.TOTAL_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["TOTAL_RESPONSES"]);
                            opt.UNIQUE_RESPONDENT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["UNIQUE_RESPONDENT"]);
                            opt.UNSUBSCRIBED = Convert.ToString(Controlsds.Tables[0].Rows[j]["UNSUBSCRIBED"]);
                            opt.URL_REDIRECTION = Convert.ToString(Controlsds.Tables[0].Rows[j]["URL_REDIRECTION"]);
                            opt.THANKU_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["THANKUPAGE_TYPE"]);
                            opt.CLOSE_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["CLOSEPAGE_TYPE"]);
                            opt.SCREENOUT_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCREENOUTPAGE_TYPE"]);
                            opt.OVERQUOTA_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["OVERQUOTAPAGE_TYPE"]);
                            opt.URLREDIRECTION_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["URLREDIRECTION_STATUS"]);
                            opt.FINISHOPTIONS_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["FINISHOPTIONS_STATUS"]);
                            opt.CONTACTLIST_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["CONTACTLIST_ID"]);
                            opt.SURVEYLAUNCH_URL = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEYLAUNCH_URL"]);
                            opt.EMAILLIST_TYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["EMAILLIST_TYPE"]);
                            opt.SURVEYURL_TYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEYURL_TYPE"]);
                            opt.LAUNCHOPTIONS_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["LAUNCHOPTIONS_STATUS"]);

                            opt.EMAIL_SENDERNAME = Convert.ToString(Controlsds.Tables[0].Rows[j]["SENDER_NAME"]);
                            opt.EMAIL_REPLYTOADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["REPLY_TO_EMAILADDRESS"]);
                            sur.surveyOptns.Add(opt);
                        }
                    }
                }
            }
            return sur;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    public Survey GetSurveyDetFusion(int SurveyID)
    {
        try
        {
            dbm.OpenConnection(connect);
            Survey sur = new Survey();
            if (SurveyID > 0)
            {
                string str = " select sur.SURVEY_NAME,sur.SURVEY_CATEGORY,sur.THEME,sur.FOLDER_ID,sur.USERID,sur.CREATED_ON,sur.STATUS,sur.CHECK_BLOCKWORDS ";
                str += " from osm_survey sur ";
                str += " where sur.SURVEY_ID= " + SurveyID;
                str += " select * from osm_surveyIntro where DELETED=0 and SURVEY_ID= " + SurveyID;
                string surSet = " select * from osm_surveysetting where DELETED=0 and SURVEY_ID= " + SurveyID;
                //order by is written by sandeep
               string Queqry = " select * from osm_surveyquestion where DELETED=0 and SURVEY_ID= " + SurveyID + " order by question_seq";

               

                string Conqry = " select * from osm_surveycontrols where DELETED=0 and SURVEY_ID= " + SurveyID;

                DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    sur.USER_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["USERID"]);
                    sur.SURVEY_ID = SurveyID;
                    sur.FOLDER_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["FOLDER_ID"]);
                    sur.STATUS = Convert.ToString(ds.Tables[0].Rows[0]["STATUS"]);
                    sur.SURVEY_CATEGORY = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_CATEGORY"]);
                    sur.THEME = Convert.ToString(ds.Tables[0].Rows[0]["THEME"]);
                    if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                    {
                        sur.SURVEY_INTRO = Convert.ToString(ds.Tables[1].Rows[0]["SURVEY_INTRO"]);
                        if (ds.Tables[1].Rows[0]["PAGE_BREAK"] != null)
                        {
                            sur.SURVEYINTRO_PAGEBREAK = Convert.ToInt32(ds.Tables[1].Rows[0]["PAGE_BREAK"]);
                        }
                    }
                    sur.SURVEY_NAME = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_NAME"]);
                    sur.CHECK_BLOCKWORDS = Convert.ToInt32(ds.Tables[0].Rows[0]["CHECK_BLOCKWORDS"]);

                    DataSet surSetds = dbm.ExecuteDataSet(CommandType.Text, surSet);
                    if (surSetds != null && surSetds.Tables.Count > 0 && surSetds.Tables[0].Rows.Count > 0)
                    {
                        sur.SURVEY_HEADER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_HEADER"]);
                        sur.SURVEY_FOOTER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FOOTER"]);
                        sur.SURVEY_FONT_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_TYPE"]);
                        sur.SURVEY_FONT_SIZE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_SIZE"]);
                        sur.SURVEY_FONT_COLOR = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_COLOR"]);
                        sur.SURVEY_BUTTON_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_BUTTON_TYPE"]);
                        sur.CUSTOMSTART_TEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSTART_TEXT"]);
                        sur.CUSTOMSUBMITTEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSUBMITTEXT"]);
                        sur.PAGE_BREAK = Convert.ToInt32(surSetds.Tables[0].Rows[0]["PAGE_BREAK"]);
                        sur.RESPONSE_REQUIRED = Convert.ToInt32(surSetds.Tables[0].Rows[0]["RESPONSE_REQUIRED"]);
                        sur.BROWSER_BACKBUTTON_STATUS = Convert.ToInt32(surSetds.Tables[0].Rows[0]["BROWSER_BACKBUTTON"]);
                        sur.SURVEY_LOGO = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_LOGO"]);
                    }
                    else
                    {
                        sur.BROWSER_BACKBUTTON_STATUS = 1;
                    }
                    DataSet Quesds = dbm.ExecuteDataSet(CommandType.Text, Queqry);
                    if (Quesds != null && Quesds.Tables.Count > 0 && Quesds.Tables[0].Rows.Count > 0)
                    {
                        sur.surveyQues = new List<SurveyQuestion>();
                        for (int i = 0; i < Quesds.Tables[0].Rows.Count; i++)
                        {
                            SurveyQuestion ques = new SurveyQuestion();
                            ques.CREATED_ON = Convert.ToDateTime(Quesds.Tables[0].Rows[i]["CREATED_ON"]);
                            ques.LASTMODIFIED_ON = Convert.ToDateTime(Quesds.Tables[0].Rows[i]["LASTMODIFIED_ON"]);
                            ques.PAGE_BREAK = Convert.ToInt32(Quesds.Tables[0].Rows[i]["PAGE_BREAK"]);
                            ques.QUESTION_ID = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_ID"]);
                            ques.QUESTION_LABEL = Convert.ToString(Quesds.Tables[0].Rows[i]["QUSETION_LABEL"]);
                            ques.QUESTION_SEQ = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_SEQ"]);
                            ques.QUESTION_TYPE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_TYPE_ID"]);
                            ques.RESPONSE_REQUIRED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RESPONSE_REQUIRED"]);
                            ques.RANDOM_ANSWERS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RANDOMIZE_ANSWERS"]);
                            ques.OTHER_ANS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["OTHER_ANS"]);
                            ques.SKIP_LOGIC = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SKIP_LOGIC"]);
                            ques.SURVEY_ID = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SURVEY_ID"]);
                            ques.CHART_ALIGNMENT = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_ALIGNMENT"]);
                            ques.CHART_APPEARANCE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_APPEARANCE"]);
                            ques.CHART_DOCK = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_DOCK"]);
                            ques.CHART_PALETTE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_PALETTE"]);
                            ques.CHART_TITLE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_TITLE"]);
                            ques.CHART_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_TYPE"]);
                            ques.COMMENTS = Convert.ToString(Quesds.Tables[0].Rows[i]["COMMENTS"]);
                            ques.CHAR_LIMIT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["CHAR_LIMIT"]);
                            ques.ANSWER_ALIGNSTYLE = Convert.ToString(Quesds.Tables[0].Rows[i]["ANSWER_ALIGNSTYLE"]);
                            ques.HIDE_LEGAND = Convert.ToInt32(Quesds.Tables[0].Rows[i]["HIDE_LEGAND"]);
                            ques.HIDE_XAXIS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["HIDE_XAXIS"]);
                            ques.STAGGERED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["STAGGERED"]);

                            ques.SHOW_LABEL = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SHOW_LABEL"]);
                            ques.LABEL_ANGLE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LABEL_ANGLE"]);
                            ques.VALUEAS_PERCENT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["VALUEAS_PERCENT"]);
                            ques.ZOOMPERCENT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["ZOOMPERCENT"]);
                            ques.TRANSPARENCY = Convert.ToInt32(Quesds.Tables[0].Rows[i]["TRANSPARENCY"]);
                            ques.PERSPECTIVE_ANGLE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["PERSPECTIVE_ANGLE"]);
                            ques.MARKER_SIZE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["MARKER_SIZE"]);
                            ques.SHOW_MARKER = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SHOW_MARKER"]);
                            ques.INVERTED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["INVERTED"]);
                            ques.LABEL_POSITION = Convert.ToString(Quesds.Tables[0].Rows[i]["LABEL_POSITION"]);
                            ques.MARKER_KIND = Convert.ToString(Quesds.Tables[0].Rows[i]["MARKER_KIND"]);
                            ques.HOLE_RADIUS = Convert.ToString(Quesds.Tables[0].Rows[i]["HOLE_RADIUS"]);
                            ques.DIAGRAM_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["DIAGRAM_TYPE"]);
                            ques.TEXT_DIRECTION = Convert.ToString(Quesds.Tables[0].Rows[i]["TEXT_DIRECTION"]);
                            ques.FUNCTION_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["FUNCTION_TYPE"]);
                            ques.EXPLODED_POINT = Convert.ToString(Quesds.Tables[0].Rows[i]["EXPLODED_POINT"]);
                            ques.LEGEND_DIRECTION = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_DIRECTION"]);
                            ques.LEGEND_EQUALLYSPACED_ITEMS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_EQUALLYSPACED_ITEMS"]);
                            ques.LEGEND_HORIZONTALALIGN = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_HORIZONTALALIGN"]);
                            ques.LEGEND_MAX_HORIZONTALALIGN = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_MAX_HORIZONTALALIGN"]);
                            ques.LEGEND_MAX_VERTICALALIGN = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_MAX_VERTICALALIGN"]);
                            ques.LEGEND_VERTICALALIGN = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_VERTICALALIGN"]);


                            sur.surveyQues.Add(ques);
                        }
                    }
                    DataSet Controlsds = dbm.ExecuteDataSet(CommandType.Text, Conqry);
                    if (Controlsds != null && Controlsds.Tables.Count > 0 && Controlsds.Tables[0].Rows.Count > 0)
                    {
                        sur.surveyOptns = new List<SurveyOptions>();
                        for (int j = 0; j < Controlsds.Tables[0].Rows.Count; j++)
                        {
                            SurveyOptions opt = new SurveyOptions();
                            opt.ALERT_ON_MAX_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ALERT_ON_MAX_RESPONSES"]);
                            opt.ALERT_ON_MAX_RESPONSES_BY_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["ALERT_ON_MAX_RESPONSES_BY_DATE"]);
                            opt.ALERT_ON_MIN_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ALERT_ON_MIN_RESPONSES"]);
                            opt.ALERT_ON_MIN_RESPONSES_BY_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["ALERT_ON_MIN_RESPONSES_BY_DATE"]);
                            opt.AUTOMATIC_THANKYOU_EMAIL = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["AUTOMATIC_THANKYOU_EMAIL"]);
                            opt.CLOSE_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["CLOSE_ON_DATE"]);
                            opt.CLOSE_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["CLOSE_PAGE"]);
                            opt.EMAIL_INVITATION = Convert.ToString(Controlsds.Tables[0].Rows[j]["EMAIL_INVITATION"]);
                            opt.EMAIL_FROMADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["FROM_MAILADDRESS"]);
                            opt.EMAIL_SUBJECT = Convert.ToString(Controlsds.Tables[0].Rows[j]["MAIL_SUBJECT"]);
                            opt.LAUNCH_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["LAUNCH_ON_DATE"]);
                            opt.NOOFRESPONSESTOCLOSEE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS"]);
                            opt.ONCLOSEALERT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE_ALERT"]);
                            opt.ONLAUNCH_ALERT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_ALERT"]);
                            opt.OVERQUOTA_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["OVERQUOTA_PAGE"]);
                            opt.PARTIAL_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["PARTIAL_RESPONSES"]);
                            opt.REMINDER_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["REMINDER_ON_DATE"]);
                            opt.REMINDER_TEXT = Convert.ToString(Controlsds.Tables[0].Rows[j]["REMINDER_TEXT"]);
                            opt.SAVE_AND_CONTINUE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SAVE_AND_CONTINUE"]);
                            opt.SCREEN_OUTS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCREEN_OUTS"]);
                            opt.SCREENOUT_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["SCREENOUT_PAGE"]);
                            opt.SEND_TO = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SEND_TO"]);
                            opt.STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["STATUS"]);
                            opt.SURVEY_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEY_ID"]);
                            opt.SURVEY_PASSWORD = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEY_PASSWORD"]);
                            opt.SURVEY_URL = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEY_URL"]);
                            opt.SURVEYCLOSETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE"]);
                            opt.SURVEYCONTROL_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEYCONTROL_ID"]);
                            opt.SURVEYLAUNCHTYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON"]);
                            opt.THANKYOU_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["THANKYOU_PAGE"]);
                            opt.TIME_FOR_URL_REDIRECTION = Convert.ToString(Controlsds.Tables[0].Rows[j]["TIME_FOR_URL_REDIRECTION"]);
                            opt.TOTAL_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["TOTAL_RESPONSES"]);
                            opt.UNIQUE_RESPONDENT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["UNIQUE_RESPONDENT"]);
                            opt.UNSUBSCRIBED = Convert.ToString(Controlsds.Tables[0].Rows[j]["UNSUBSCRIBED"]);
                            opt.URL_REDIRECTION = Convert.ToString(Controlsds.Tables[0].Rows[j]["URL_REDIRECTION"]);
                            opt.THANKU_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["THANKUPAGE_TYPE"]);
                            opt.CLOSE_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["CLOSEPAGE_TYPE"]);
                            opt.SCREENOUT_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCREENOUTPAGE_TYPE"]);
                            opt.OVERQUOTA_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["OVERQUOTAPAGE_TYPE"]);
                            opt.URLREDIRECTION_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["URLREDIRECTION_STATUS"]);
                            opt.FINISHOPTIONS_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["FINISHOPTIONS_STATUS"]);
                            opt.CONTACTLIST_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["CONTACTLIST_ID"]);
                            opt.SURVEYLAUNCH_URL = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEYLAUNCH_URL"]);
                            opt.EMAILLIST_TYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["EMAILLIST_TYPE"]);
                            opt.SURVEYURL_TYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEYURL_TYPE"]);
                            opt.LAUNCHOPTIONS_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["LAUNCHOPTIONS_STATUS"]);

                            opt.EMAIL_SENDERNAME = Convert.ToString(Controlsds.Tables[0].Rows[j]["SENDER_NAME"]);
                            opt.EMAIL_REPLYTOADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["REPLY_TO_EMAILADDRESS"]);
                            sur.surveyOptns.Add(opt);
                        }
                    }
                }
            }
            return sur;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public Survey GetSurveyDet(int SurveyID)
    {
        try
        {
            dbm.OpenConnection(connect);
            Survey sur = new Survey();
            if (SurveyID > 0)
            {
                string str = " select sur.SURVEY_NAME,sur.SURVEY_CATEGORY,sur.THEME,sur.FOLDER_ID,sur.USERID,sur.CREATED_ON,sur.STATUS,sur.CHECK_BLOCKWORDS ";
                str += " from osm_survey sur ";
                str += " where sur.SURVEY_ID= " + SurveyID;
                str += " select * from osm_surveyIntro where DELETED=0 and SURVEY_ID= " + SurveyID;
                string surSet = " select * from osm_surveysetting where DELETED=0 and SURVEY_ID= " + SurveyID;
                //order by is written by sandeep
                string Queqry = " select * from osm_surveyquestion where DELETED=0 and SURVEY_ID= " + SurveyID + "order by question_seq";
                string Conqry = " select * from osm_surveycontrols where DELETED=0 and SURVEY_ID= " + SurveyID;

                DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    sur.USER_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["USERID"]);
                    sur.SURVEY_ID = SurveyID;
                    sur.FOLDER_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["FOLDER_ID"]);
                    sur.STATUS = Convert.ToString(ds.Tables[0].Rows[0]["STATUS"]);
                    sur.SURVEY_CATEGORY = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_CATEGORY"]);
                    sur.THEME = Convert.ToString(ds.Tables[0].Rows[0]["THEME"]);
                    if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                    {
                        sur.SURVEY_INTRO = Convert.ToString(ds.Tables[1].Rows[0]["SURVEY_INTRO"]);
                        if (ds.Tables[1].Rows[0]["PAGE_BREAK"] != null)
                        {
                            sur.SURVEYINTRO_PAGEBREAK = Convert.ToInt32(ds.Tables[1].Rows[0]["PAGE_BREAK"]);
                        }
                    }
                    sur.SURVEY_NAME = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_NAME"]);
                    sur.CHECK_BLOCKWORDS = Convert.ToInt32(ds.Tables[0].Rows[0]["CHECK_BLOCKWORDS"]);

                    DataSet surSetds = dbm.ExecuteDataSet(CommandType.Text, surSet);
                    if (surSetds != null && surSetds.Tables.Count > 0 && surSetds.Tables[0].Rows.Count > 0)
                    {
                        sur.SURVEY_HEADER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_HEADER"]);
                        sur.SURVEY_FOOTER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FOOTER"]);
                        sur.SURVEY_FONT_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_TYPE"]);
                        sur.SURVEY_FONT_SIZE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_SIZE"]);
                        sur.SURVEY_FONT_COLOR = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_COLOR"]);
                        sur.SURVEY_BUTTON_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_BUTTON_TYPE"]);
                        sur.CUSTOMSTART_TEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSTART_TEXT"]);
                        sur.CUSTOMSUBMITTEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSUBMITTEXT"]);
                        sur.PAGE_BREAK = Convert.ToInt32(surSetds.Tables[0].Rows[0]["PAGE_BREAK"]);
                        sur.RESPONSE_REQUIRED = Convert.ToInt32(surSetds.Tables[0].Rows[0]["RESPONSE_REQUIRED"]);
                        sur.BROWSER_BACKBUTTON_STATUS = Convert.ToInt32(surSetds.Tables[0].Rows[0]["BROWSER_BACKBUTTON"]);
                        sur.SURVEY_LOGO = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_LOGO"]);
                    }
                    else
                    {
                        sur.BROWSER_BACKBUTTON_STATUS = 1;
                    }
                    DataSet Quesds = dbm.ExecuteDataSet(CommandType.Text, Queqry);
                    if (Quesds != null && Quesds.Tables.Count > 0 && Quesds.Tables[0].Rows.Count > 0)
                    {
                        sur.surveyQues = new List<SurveyQuestion>();
                        for (int i = 0; i < Quesds.Tables[0].Rows.Count; i++)
                        {
                            SurveyQuestion ques = new SurveyQuestion();
                            ques.CREATED_ON = Convert.ToDateTime(Quesds.Tables[0].Rows[i]["CREATED_ON"]);
                            ques.LASTMODIFIED_ON = Convert.ToDateTime(Quesds.Tables[0].Rows[i]["LASTMODIFIED_ON"]);
                            ques.PAGE_BREAK = Convert.ToInt32(Quesds.Tables[0].Rows[i]["PAGE_BREAK"]);
                            ques.QUESTION_ID = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_ID"]);
                            ques.QUESTION_LABEL = Convert.ToString(Quesds.Tables[0].Rows[i]["QUSETION_LABEL"]);
                            ques.QUESTION_SEQ = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_SEQ"]);
                            ques.QUESTION_TYPE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_TYPE_ID"]);
                            ques.RESPONSE_REQUIRED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RESPONSE_REQUIRED"]);
                            ques.RANDOM_ANSWERS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RANDOMIZE_ANSWERS"]);
                            ques.OTHER_ANS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["OTHER_ANS"]);
                            ques.SKIP_LOGIC = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SKIP_LOGIC"]);
                            ques.SURVEY_ID = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SURVEY_ID"]);
                            ques.CHART_ALIGNMENT = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_ALIGNMENT"]);
                            ques.CHART_APPEARANCE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_APPEARANCE"]);
                            ques.CHART_DOCK = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_DOCK"]);
                            ques.CHART_PALETTE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_PALETTE"]);
                            ques.CHART_TITLE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_TITLE"]);
                            ques.CHART_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_TYPE"]);
                            ques.COMMENTS = Convert.ToString(Quesds.Tables[0].Rows[i]["COMMENTS"]);
                            ques.CHAR_LIMIT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["CHAR_LIMIT"]);
                            ques.ANSWER_ALIGNSTYLE = Convert.ToString(Quesds.Tables[0].Rows[i]["ANSWER_ALIGNSTYLE"]);
                            ques.HIDE_LEGAND = Convert.ToInt32(Quesds.Tables[0].Rows[i]["HIDE_LEGAND"]);
                            ques.HIDE_XAXIS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["HIDE_XAXIS"]);
                            ques.STAGGERED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["STAGGERED"]);

                            ques.SHOW_LABEL = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SHOW_LABEL"]);
                            ques.LABEL_ANGLE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LABEL_ANGLE"]);
                            ques.VALUEAS_PERCENT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["VALUEAS_PERCENT"]);
                            ques.ZOOMPERCENT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["ZOOMPERCENT"]);
                            ques.TRANSPARENCY = Convert.ToInt32(Quesds.Tables[0].Rows[i]["TRANSPARENCY"]);
                            ques.PERSPECTIVE_ANGLE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["PERSPECTIVE_ANGLE"]);
                            ques.MARKER_SIZE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["MARKER_SIZE"]);
                            ques.SHOW_MARKER = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SHOW_MARKER"]);
                            ques.INVERTED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["INVERTED"]);
                            ques.LABEL_POSITION = Convert.ToString(Quesds.Tables[0].Rows[i]["LABEL_POSITION"]);
                            ques.MARKER_KIND = Convert.ToString(Quesds.Tables[0].Rows[i]["MARKER_KIND"]);
                            ques.HOLE_RADIUS = Convert.ToString(Quesds.Tables[0].Rows[i]["HOLE_RADIUS"]);
                            ques.DIAGRAM_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["DIAGRAM_TYPE"]);
                            ques.TEXT_DIRECTION = Convert.ToString(Quesds.Tables[0].Rows[i]["TEXT_DIRECTION"]);
                            ques.FUNCTION_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["FUNCTION_TYPE"]);
                            ques.EXPLODED_POINT = Convert.ToString(Quesds.Tables[0].Rows[i]["EXPLODED_POINT"]);
                            ques.LEGEND_DIRECTION = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_DIRECTION"]);
                            ques.LEGEND_EQUALLYSPACED_ITEMS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_EQUALLYSPACED_ITEMS"]);
                            ques.LEGEND_HORIZONTALALIGN = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_HORIZONTALALIGN"]);
                            ques.LEGEND_MAX_HORIZONTALALIGN = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_MAX_HORIZONTALALIGN"]);
                            ques.LEGEND_MAX_VERTICALALIGN = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_MAX_VERTICALALIGN"]);
                            ques.LEGEND_VERTICALALIGN = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_VERTICALALIGN"]);

                            string Ansqry = " select * from osm_answeroptions where DELETED=0 and QUESTION_ID= " + ques.QUESTION_ID + " order by ANSWER_ID";
                            DataSet Ansds = dbm.ExecuteDataSet(CommandType.Text, Ansqry);
                            if (Ansds != null && Ansds.Tables.Count > 0 && Ansds.Tables[0].Rows.Count > 0)
                            {
                                ques.surveyAnswers = new List<SurveyAnswers>();
                                for (int y = 0; y < Ansds.Tables[0].Rows.Count; y++)
                                {
                                    SurveyAnswers Ansr = new SurveyAnswers();
                                    Ansr.ANSWER_ID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["ANSWER_ID"]);
                                    Ansr.ANSWER_OPTIONS = Convert.ToString(Ansds.Tables[0].Rows[y]["ANSWER_OPTIONS"]);
                                    Ansr.CREATED_ON = Convert.ToDateTime(Ansds.Tables[0].Rows[y]["CREATED_ON"]);
                                    Ansr.LAST_MODIFIED_ON = Convert.ToDateTime(Ansds.Tables[0].Rows[y]["LAST_MODIFIED_ON"]);
                                    Ansr.QUESTION_ID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["QUESTION_ID"]);
                                    Ansr.SKIP_QUESTION_ID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["SKIP_QUESTION_ID"]);
                                    Ansr.DEMOGRAPIC_BLOCKID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["DEMOGRAPIC_BLOCKID"]);
                                    Ansr.PIPING_TEXT = Convert.ToString(Ansds.Tables[0].Rows[y]["PIPING_TEXT"]);
                                    Ansr.RESPONSE_REQUIRED = Convert.ToInt32(Ansds.Tables[0].Rows[y]["RESPONSE_REQUIRED"]);
                                    ques.surveyAnswers.Add(Ansr);
                                }
                            }
                            sur.surveyQues.Add(ques);
                        }
                    }
                    DataSet Controlsds = dbm.ExecuteDataSet(CommandType.Text, Conqry);
                    if (Controlsds != null && Controlsds.Tables.Count > 0 && Controlsds.Tables[0].Rows.Count > 0)
                    {
                        sur.surveyOptns = new List<SurveyOptions>();
                        for (int j = 0; j < Controlsds.Tables[0].Rows.Count; j++)
                        {
                            SurveyOptions opt = new SurveyOptions();
                            opt.ALERT_ON_MAX_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ALERT_ON_MAX_RESPONSES"]);
                            opt.ALERT_ON_MAX_RESPONSES_BY_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["ALERT_ON_MAX_RESPONSES_BY_DATE"]);
                            opt.ALERT_ON_MIN_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ALERT_ON_MIN_RESPONSES"]);
                            opt.ALERT_ON_MIN_RESPONSES_BY_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["ALERT_ON_MIN_RESPONSES_BY_DATE"]);
                            opt.AUTOMATIC_THANKYOU_EMAIL = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["AUTOMATIC_THANKYOU_EMAIL"]);
                            opt.CLOSE_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["CLOSE_ON_DATE"]);
                            opt.CLOSE_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["CLOSE_PAGE"]);
                            opt.EMAIL_INVITATION = Convert.ToString(Controlsds.Tables[0].Rows[j]["EMAIL_INVITATION"]);
                            opt.EMAIL_FROMADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["FROM_MAILADDRESS"]);
                            opt.EMAIL_SUBJECT = Convert.ToString(Controlsds.Tables[0].Rows[j]["MAIL_SUBJECT"]);
                            opt.LAUNCH_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["LAUNCH_ON_DATE"]);
                            opt.NOOFRESPONSESTOCLOSEE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS"]);
                            opt.ONCLOSEALERT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE_ALERT"]);
                            opt.ONLAUNCH_ALERT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_ALERT"]);
                            opt.OVERQUOTA_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["OVERQUOTA_PAGE"]);
                            opt.PARTIAL_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["PARTIAL_RESPONSES"]);
                            opt.REMINDER_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["REMINDER_ON_DATE"]);
                            opt.REMINDER_TEXT = Convert.ToString(Controlsds.Tables[0].Rows[j]["REMINDER_TEXT"]);
                            opt.SAVE_AND_CONTINUE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SAVE_AND_CONTINUE"]);
                            opt.SCREEN_OUTS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCREEN_OUTS"]);
                            opt.SCREENOUT_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["SCREENOUT_PAGE"]);
                            opt.SEND_TO = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SEND_TO"]);
                            opt.STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["STATUS"]);
                            opt.SURVEY_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEY_ID"]);
                            opt.SURVEY_PASSWORD = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEY_PASSWORD"]);
                            opt.SURVEY_URL = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEY_URL"]);
                            opt.SURVEYCLOSETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE"]);
                            opt.SURVEYCONTROL_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEYCONTROL_ID"]);
                            opt.SURVEYLAUNCHTYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON"]);
                            opt.THANKYOU_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["THANKYOU_PAGE"]);
                            opt.TIME_FOR_URL_REDIRECTION = Convert.ToString(Controlsds.Tables[0].Rows[j]["TIME_FOR_URL_REDIRECTION"]);
                            opt.TOTAL_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["TOTAL_RESPONSES"]);
                            opt.UNIQUE_RESPONDENT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["UNIQUE_RESPONDENT"]);
                            opt.UNSUBSCRIBED = Convert.ToString(Controlsds.Tables[0].Rows[j]["UNSUBSCRIBED"]);
                            opt.URL_REDIRECTION = Convert.ToString(Controlsds.Tables[0].Rows[j]["URL_REDIRECTION"]);
                            opt.THANKU_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["THANKUPAGE_TYPE"]);
                            opt.CLOSE_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["CLOSEPAGE_TYPE"]);
                            opt.SCREENOUT_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCREENOUTPAGE_TYPE"]);
                            opt.OVERQUOTA_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["OVERQUOTAPAGE_TYPE"]);
                            opt.URLREDIRECTION_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["URLREDIRECTION_STATUS"]);
                            opt.FINISHOPTIONS_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["FINISHOPTIONS_STATUS"]);
                            opt.CONTACTLIST_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["CONTACTLIST_ID"]);
                            opt.SURVEYLAUNCH_URL = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEYLAUNCH_URL"]);
                            opt.EMAILLIST_TYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["EMAILLIST_TYPE"]);
                            opt.SURVEYURL_TYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEYURL_TYPE"]);
                            opt.LAUNCHOPTIONS_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["LAUNCHOPTIONS_STATUS"]);

                            opt.EMAIL_SENDERNAME = Convert.ToString(Controlsds.Tables[0].Rows[j]["SENDER_NAME"]);
                            opt.EMAIL_REPLYTOADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["REPLY_TO_EMAILADDRESS"]);
                            sur.surveyOptns.Add(opt);
                        }
                    }
                }
            }
            return sur;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public string GetRespondentsForReports(int QuestionID)
    {
        int SurveyID = GetSurveyIDofQuestion(QuestionID);
        string usrLmitDet = GetUserLimit(SurveyID, "");
        int usrLmit = 0; string LienTyp = "";
        string[] det = usrLmitDet.Split('$');
        if (det != null && det.Length > 0)
        {
            LienTyp = Convert.ToString(det.GetValue(1));
            usrLmit = Convert.ToInt32(det.GetValue(0));
        }
        string RespondentIDs = "";
        string qry = "";
        if (usrLmit != -1 && usrLmit > 0)
        {
            qry = "select TOP " + usrLmit + " RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=(Select SURVEY_ID from osm_surveyquestion where QUESTION_ID=" + QuestionID + ")";
        }
        else if (usrLmit == -1)
        {
            qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=(Select SURVEY_ID from osm_surveyquestion where QUESTION_ID=" + QuestionID + ")";
        }
        try
        {
            dbm.OpenConnection(connect);
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qry);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    RespondentIDs += "," + Convert.ToString(ds.Tables[0].Rows[i]["RESPONDENT_ID"]);
                }
                if (RespondentIDs != null && RespondentIDs.Trim().Length > 0)
                    RespondentIDs = RespondentIDs.Substring(1);
            }
            return RespondentIDs;
        }
        catch
        {
            return RespondentIDs;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public int GetSurveyIDofQuestion(int QuestionID)
    {
        try
        {
            dbm.OpenConnection(connect);
            string qry;
            qry = "Select SURVEY_ID from osm_surveyquestion where QUESTION_ID=" + QuestionID;
            object obj = dbm.ExecuteScalar(CommandType.Text, qry);
            int ID = 0;
            if (obj != null)
                ID = Convert.ToInt32(obj);
            return (ID);
        }
        catch { return 0; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetRankingConstanSumCommentBoxTexResponses(int SurveyID, int QuestionID, ArrayList ans_options)
    {
        try
        {
            string usrLmitDet = GetUserLimit(SurveyID, "");
            int usrLmit = 0; string LienTyp = "";
            string[] det = usrLmitDet.Split('$');
            if (det != null && det.Length > 0)
            {
                LienTyp = Convert.ToString(det.GetValue(1));
                usrLmit = Convert.ToInt32(det.GetValue(0));
            }
            string RespondentIDs = "";
            string qry = "";
            if (usrLmit != -1 && usrLmit > 0)
            {

                qry = "select TOP " + usrLmit + " RESPONDENT_ID,EMAIL_ID,STATUS,CREATED_ON  from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
            }
            else if (usrLmit == -1)
            {
                qry = "select RESPONDENT_ID,EMAIL_ID,STATUS,CREATED_ON  from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;

            }
            dbm.OpenConnection(connect);
            DataSet ds = new DataSet();
            ds = dbm.ExecuteDataSet(CommandType.Text, qry);
            DataSet ds_ansidtabbles = new DataSet();
            string qry1 = "";
            for (int i = 0; i < ans_options.Count; i++)
            {
                qry1 = " Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where QUESTION_ID=" + QuestionID + " and ANSWER_ID=" + ans_options[i] + " and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";
                ds_ansidtabbles = dbm.ExecuteDataSet(CommandType.Text, qry1);
                if (i == 0)
                    ds.Tables[0].Columns.Add("SNO");
                ds.Tables[0].Columns.Add();

                for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                {
                    int flag = 0;
                    for (int l = 0; l < ds_ansidtabbles.Tables[0].Rows.Count; l++)
                    {

                        if (Convert.ToInt32(ds.Tables[0].Rows[k]["RESPONDENT_ID"].ToString()) == Convert.ToInt32(ds_ansidtabbles.Tables[0].Rows[l]["RESPONDENT_ID"].ToString()))
                        {
                            if (i == 0)
                                ds.Tables[0].Rows[k][i + 4] = Convert.ToString(k + 1);
                            ds.Tables[0].Rows[k][i + 5] = Convert.ToString(ds_ansidtabbles.Tables[0].Rows[l]["ANSWER_TEXT"].ToString());
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 0 && i == 0)
                        ds.Tables[0].Rows[k][i + 4] = Convert.ToString(k + 1);
                }

            }
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }

    }
    public DataSet GetExportInfo(int ExportID)
    {
        try
        {
            dbm.OpenConnection(connect);
            string qryinfo = " Select * from osm_surveyreports where DELETED=0 and REPORT_ID=" + ExportID;
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qryinfo);
            return ds;
        }
        catch
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public bool GetRawDataExportResponses(int SurveyID, string Resptype)
    {
        try
        {
            dbm.OpenConnection(connect);
            bool flag = false;
            string str_respondents = "Select res.RESPONDENT_ID from osm_responsesurvey res left outer join osm_emaillist eml on res.EMAIL_ID=eml.EMAIL_ID where SURVEY_ID=" + SurveyID + " and res.DELETED=0 and res.STATUS='" + Resptype + "'";
            str_respondents += " Select QUESTION_ID from osm_surveyquestion where SURVEY_ID=" + SurveyID + " and QUESTION_TYPE_ID IN('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','20') and DELETED=0 order by QUESTION_SEQ ";
            DataSet ds_respondents = dbm.ExecuteDataSet(CommandType.Text, str_respondents);

            if (ds_respondents != null && ds_respondents.Tables.Count > 1 && ds_respondents.Tables[0].Rows.Count > 0 && ds_respondents.Tables[1].Rows.Count > 0)
            {
                flag = true;
            }
            return flag;
        }
        catch { return false; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public int InsertExportInfo(int SurveyID, int rep_type, string rep_mode, int Usr_id)
    {
        SqlCommand cmd = new SqlCommand();
        try
        {
            dbm.OpenConnection(connect);
            SqlParameter objsqlParameter;
            objsqlParameter = cmd.Parameters.Add("@SurveyID", SqlDbType.Int);
            cmd.Parameters["@SurveyID"].Value = SurveyID;
            cmd.Parameters["@SurveyID"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Report_Type", SqlDbType.Int);
            cmd.Parameters["@Report_Type"].Value = rep_type;
            cmd.Parameters["@Report_Type"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Report_Mode", SqlDbType.NVarChar);
            cmd.Parameters["@Report_Mode"].Value = rep_mode;
            cmd.Parameters["@Report_Mode"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@CreatedOn", SqlDbType.NVarChar);
            cmd.Parameters["@CreatedOn"].Value = DateTime.UtcNow;
            cmd.Parameters["@CreatedOn"].Direction = ParameterDirection.Input;

            objsqlParameter = cmd.Parameters.Add("@USERID", SqlDbType.Int);
            cmd.Parameters["@USERID"].Value = Usr_id;
            cmd.Parameters["@USERID"].Direction = ParameterDirection.Input;

            objsqlParameter = cmd.Parameters.Add("@EXPORT_REP_ID", SqlDbType.Int);
            cmd.Parameters["@EXPORT_REP_ID"].Direction = ParameterDirection.Output;
            dbm.SPExecuteNonQuery(CommandType.StoredProcedure, "sp_ExportSurveyRep", cmd);
            int val = Convert.ToInt32(cmd.Parameters["@EXPORT_REP_ID"].Value);
            return val;
        }
        catch (Exception ex)
        {
            return 0;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetConstantSumForReports(int QuestionID, ArrayList ans_options,DateTime dt1,DateTime dt2)
    {
        try
        {
            string RespondentIDS = GetRespondentsForReports(QuestionID);
            dbm.OpenConnection(connect);
            DataSet ds = new DataSet();
            if (RespondentIDS != null && RespondentIDS.Length > 0)
            {
                string qry = " Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where QUESTION_ID=" + QuestionID + " and RESPONDENT_ID IN(" + RespondentIDS + ") and DELETED=0 ";
                if (dt1 > Convert.ToDateTime("1/1/0001 12:00:00 AM") && dt2 > Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                {
                    qry += " and RESPONSE_RECEIVED_ON between '" + dt1 + "' and '" + dt2 + "'";
                }
                qry += " order by RESPONDENT_ID";
                ds = dbm.ExecuteDataSet(CommandType.Text, qry);

            }
            return ds;

        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetRankingConstanSum(int QuestionID, ArrayList ans_options,DateTime dt1,DateTime dt2)
    {
        try
        {
            string RespondentIDS = GetRespondentsForReports(QuestionID);
            dbm.OpenConnection(connect);
            DataSet ds = new DataSet();
            if (RespondentIDS != null && RespondentIDS.Length > 0)
            {
                string qry = " Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where QUESTION_ID=" + QuestionID + " and RESPONDENT_ID IN(" + RespondentIDS + ") and DELETED=0";
                if (dt1 > Convert.ToDateTime("1/1/0001 12:00:00 AM") && dt2 > Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                {
                    qry += " and RESPONSE_RECEIVED_ON between '" + dt1 + "' and '" + dt2+"'"; 
                }
                qry += " order by RESPONDENT_ID";

                ds = dbm.ExecuteDataSet(CommandType.Text, qry);

            }
            return ds;

        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetTextResponses(int QuesID, int others, int SurveyID)
    {
        try
        {
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            string qry = "";
            string usrLmitDet = GetUserLimit(SurveyID, "");
            int usrLmit = 0; string LienTyp = "";
            string[] det = usrLmitDet.Split('$');
            if (det != null && det.Length > 0)
            {
                LienTyp = Convert.ToString(det.GetValue(1));
                usrLmit = Convert.ToInt32(det.GetValue(0));
            }
            dbm.OpenConnection(connect);
            if (usrLmit != -1 && usrLmit > 0)
            {
                qry = "select TOP " + usrLmit + " RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
            }
            else if (usrLmit == -1)
            {
                qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;

            }
            ds = dbm.ExecuteDataSet(CommandType.Text, qry);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataSet ds_ansidtabbles = new DataSet();
                string qry1 = "";
                if (others == 1)
                    qry1 = " select (ROW_NUMBER() OVER (ORDER BY RESPONDENT_ID)) as SNO,answer_text,RESPONDENT_ID from osm_responsequestions where question_id=" + QuesID + " and ANSWER_ID=0 and RESPONDENT_ID IN(" + qry + ") and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";
                else
                    qry1 = " select (ROW_NUMBER() OVER (ORDER BY RESPONDENT_ID)) as SNO,answer_text,RESPONDENT_ID from osm_responsequestions where question_id=" + QuesID + " and RESPONDENT_ID IN(" + qry + ") and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";

                ds_ansidtabbles = dbm.ExecuteDataSet(CommandType.Text, qry1);
                ds1.Tables.Add();
                ds1.Tables[0].Columns.Add("SNO");
                ds1.Tables[0].Columns.Add("answer_text");
                for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                {
                    int flag = 0;
                    ds1.Tables[0].Rows.Add();
                    for (int l = 0; l < ds_ansidtabbles.Tables[0].Rows.Count; l++)
                    {

                        if (Convert.ToInt32(ds.Tables[0].Rows[k]["RESPONDENT_ID"].ToString()) == Convert.ToInt32(ds_ansidtabbles.Tables[0].Rows[l]["RESPONDENT_ID"].ToString()))
                        {
                            ds1.Tables[0].Rows[k][0] = Convert.ToString(k + 1);
                            ds1.Tables[0].Rows[k][1] = Convert.ToString(ds_ansidtabbles.Tables[0].Rows[l]["ANSWER_TEXT"].ToString()).Trim();
                            flag = 1;
                            break;

                        }
                    }
                    if (flag == 0)
                    {
                        ds1.Tables[0].Rows[k][0] = Convert.ToString(k + 1);
                        ds1.Tables[0].Rows[k][1] = "";
                    }
                }
            }
            return ds1;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet getsurveyquestions(int surid)
    {
        try
        {
            dbm.OpenConnection(connect);
            string QryGetSurveyQuestions = "";
            QryGetSurveyQuestions = "Select QUESTION_ID,QUSETION_LABEL,QUESTION_TYPE_ID,OTHER_ANS from osm_surveyquestion  where SURVEY_ID=" + "'" + surid + "' and (QUESTION_TYPE_ID='1' OR QUESTION_TYPE_ID='2' OR ";
            QryGetSurveyQuestions = QryGetSurveyQuestions + "QUESTION_TYPE_ID='3' OR QUESTION_TYPE_ID='4' OR QUESTION_TYPE_ID='10' OR QUESTION_TYPE_ID='11' OR QUESTION_TYPE_ID='12') and DELETED=0 order by QUESTION_SEQ";
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, QryGetSurveyQuestions);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet getcrossreports(int survey_id)
    {
        try
        {
            dbm.OpenConnection(connect);
            string QryGetSurveyCrossReports = "Select CROSS_REPORT_ID,SURVEY_ID,QUESTION_ROW_ID,QUESTION_COLUMN_ID,CREATED_ON,CREPORT_NAME from osm_crossreports  where SURVEY_ID=" + survey_id;
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, QryGetSurveyCrossReports);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public int NewCrossReport(int survey_id, int question_row_id, int question_column_id, DateTime CreatedOn, String creport_name, int Created_by)
    {
        SqlCommand cmd = new SqlCommand();
        try
        {
            dbm.OpenConnection(connect);
            SqlParameter objsqlParameter;
            objsqlParameter = cmd.Parameters.Add("@survey_id", SqlDbType.Int);
            cmd.Parameters["@survey_id"].Value = survey_id;
            cmd.Parameters["@survey_id"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@question_row_id", SqlDbType.Int);
            cmd.Parameters["@question_row_id"].Value = question_row_id;
            cmd.Parameters["@question_row_id"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@question_column_id", SqlDbType.Int);
            cmd.Parameters["@question_column_id"].Value = question_column_id;
            cmd.Parameters["@question_column_id"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ModifiedOn", SqlDbType.DateTime);
            cmd.Parameters["@ModifiedOn"].Value = DateTime.UtcNow;
            cmd.Parameters["@ModifiedOn"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@creport_name", SqlDbType.VarChar);
            cmd.Parameters["@creport_name"].Value = creport_name;
            cmd.Parameters["@creport_name"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Created_by", SqlDbType.Int);
            cmd.Parameters["@Created_by"].Value = Created_by;
            cmd.Parameters["@Created_by"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@REPORT_ID", SqlDbType.Int);
            cmd.Parameters["@REPORT_ID"].Direction = ParameterDirection.Output;
            dbm.SPExecuteNonQuery(CommandType.StoredProcedure, "sp_crossreports", cmd);
            int val = Convert.ToInt16(cmd.Parameters["@REPORT_ID"].Value);
            string val1 = cmd.Parameters["@returnmessage"].Value.ToString();
            return val;
        }
        catch (Exception ex)
        {
            return 0;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    public int GetQuestionType(int QuestionID)
    {
        try
        {
            dbm.OpenConnection(connect);
            string qry;
            qry = "Select Question_Type_ID from osm_surveyquestion where QUESTION_ID=" + QuestionID;
            object obj = dbm.ExecuteScalar(CommandType.Text, qry);
            int ID = 0;
            if (obj != null)
                ID = Convert.ToInt32(obj);
            return (ID);
        }
        catch { return 0; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet getsurveyanswers(int q1_id, int q2_id, int SurveyID)
    {
        try
        {
            string Respondents = GetActiveRespondent(SurveyID);
            int questiontypeid = GetQuestionType(q2_id);

            dbm.OpenConnection(connect);
            string str = " select ANSWER_ID,ANSWER_OPTIONS from osm_answeroptions where QUESTION_ID=" + q1_id;
            str += " select ANSWER_ID,ANSWER_OPTIONS from osm_answeroptions where QUESTION_ID=" + q2_id;
            str += " select RESPONSE_ID,RQ1.RESPONDENT_ID,ANSWER_ID,ANSWER_TEXT from osm_responsequestions RQ1 inner join osm_responsesurvey R1 on RQ1.RESPONDENT_ID=R1.RESPONDENT_ID where RQ1.DELETED=0 and QUESTION_ID=" + q1_id + "  and RQ1.RESPONDENT_ID IN(" + Respondents + ")";
            str += " select RESPONSE_ID,RQ1.RESPONDENT_ID,ANSWER_ID,ANSWER_TEXT from osm_responsequestions RQ1 inner join osm_responsesurvey R1 on RQ1.RESPONDENT_ID=R1.RESPONDENT_ID where RQ1.DELETED=0 and QUESTION_ID=" + q2_id + " and RQ1.RESPONDENT_ID IN(" + Respondents + ")";
            if ((questiontypeid == 10) || (questiontypeid == 11) || (questiontypeid == 12))
            {
                str += "select ANSWER_ID,answer_options ,replace(answer_options, substring(answer_options,0, CHARINDEX('$--$',ANSWER_OPTIONS)+4),'') + '  -  ' + substring(answer_options,0, CHARINDEX('$--$',ANSWER_OPTIONS)) as answeroptionname,replace(answer_options, substring(answer_options,0, CHARINDEX('$--$',ANSWER_OPTIONS)+4),'') as groupname from osm_answeroptions where QUESTION_ID= " + q2_id;
            }
            else
            {
                str += "select ANSWER_ID,answer_options , ANSWER_OPTIONS as answeroptionname,ANSWER_OPTIONS as groupname from osm_answeroptions where QUESTION_ID= " + q2_id;
            }
           
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet getRespondentscount(int ans1_id, int ans2_id, int surveyid)
    {
        try
        {
           
            dbm.OpenConnection(connect);

            string str = "select orq.answer_id,oao.ANSWER_OPTIONS, COUNT(respondent_id) as countrespondent  from osm_responsequestions orq ,osm_answeroptions oao  where orq.ANSWER_ID =" + ans1_id + " and orq.ANSWER_ID = oao.ANSWER_ID and orq.DELETED=0 and oao.DELETED=0 and RESPONDENT_ID in (select orq1.respondent_id from osm_responsequestions orq1, osm_responsesurvey sur1  where ANSWER_ID =" + ans2_id + " and orq1.DELETED=0 and orq1.RESPONDENT_ID = sur1.RESPONDENT_ID and sur1.STATUS = 'Complete' and sur1.DELETED=0 and sur1.SURVEY_ID=" + surveyid + ")  group by orq.ANSWER_ID,oao.ANSWER_OPTIONS";

            


          
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    public DataSet getRespondentscountmultipleselect(int ans1_id, string groupname,int colquestionid, int surveyid)
    {
        try
        {
            
            dbm.OpenConnection(connect);

            string str = "select orq.answer_id,oao.ANSWER_OPTIONS, respondent_id as countrespondent  from osm_responsequestions orq ,osm_answeroptions oao  where orq.ANSWER_ID=" + ans1_id + " and orq.ANSWER_ID = oao.ANSWER_ID and orq.DELETED=0 and oao.DELETED=0 and RESPONDENT_ID in (select orq1.respondent_id from osm_responsequestions orq1,osm_responsesurvey sur1  where ANSWER_ID in  ( select answer_id from osm_answeroptions where replace(answer_options, substring(answer_options,0, CHARINDEX('$--$',ANSWER_OPTIONS)+4),'') = '" + groupname + "' and QUESTION_ID = " + colquestionid + ")  and orq1.DELETED=0 and orq1.RESPONDENT_ID = sur1.RESPONDENT_ID and sur1.STATUS = 'Complete' and sur1.DELETED=0 and sur1.SURVEY_ID= " + surveyid + ")  group by orq.ANSWER_ID,oao.ANSWER_OPTIONS,RESPONDENT_ID ";


            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }


    public DataSet getRespondentsrowcountmultipleselect(int rowquestionid, string groupname, int colquestionid, int surveyid)
    {
        try
        {

            dbm.OpenConnection(connect);

            string str = "select orq.answer_id,oao.ANSWER_OPTIONS, respondent_id as countrespondent  from osm_responsequestions orq ,osm_answeroptions oao  where orq.ANSWER_ID in (select answer_id from osm_answeroptions  where QUESTION_ID= " + rowquestionid + " )   and orq.ANSWER_ID = oao.ANSWER_ID and orq.DELETED=0 and oao.DELETED=0 and RESPONDENT_ID in (select orq1.respondent_id from osm_responsequestions orq1,osm_responsesurvey sur1  where ANSWER_ID in  ( select answer_id from osm_answeroptions where replace(answer_options, substring(answer_options,0, CHARINDEX('$--$',ANSWER_OPTIONS)+4),'') = '" + groupname + "' and QUESTION_ID = " + colquestionid + ")  and orq1.DELETED=0 and orq1.RESPONDENT_ID = sur1.RESPONDENT_ID and sur1.STATUS = 'Complete' and sur1.DELETED=0 and sur1.SURVEY_ID= " + surveyid + ")  group by orq.ANSWER_ID,oao.ANSWER_OPTIONS,RESPONDENT_ID ";


            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }


    public Survey UpdateQuestionChart(int SurveyID, int QuestionID, string ChartTitle, string Comments, string ChartType, string ChartAppearance, string ChartPalette, string ChartAligment, string ChartDock, int ShowLabel, int LabelAngle, int ValueAspercent, int ZoomPercent, int Transparency, int PerspectiveAngle, int MarkerSize, int ShowMarker, int Inverted, string LabelPosition, string MarkerKind, string HoleRadius, string DiagramType, string TextDirection, string FunctionType, string ExplodedPoint, string LegendHorizontalAlignment, string LegendVerticalAlignment, int LegendMaxHorizontalPercentage, int LegendMaxVerticalPercentage, string LegendDirection, int LegendEquallySpacedItems, int HideLegand, int HideXaixs, int Staggered)
    {
        SqlCommand cmd = new SqlCommand();
        try
        {
            dbm.OpenConnection(connect);
            SqlParameter objsqlParameter;
            objsqlParameter = cmd.Parameters.Add("@ChartTitle", SqlDbType.VarChar);
            cmd.Parameters["@ChartTitle"].Value = ChartTitle;
            cmd.Parameters["@ChartTitle"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Comments", SqlDbType.VarChar);
            cmd.Parameters["@Comments"].Value = Comments;
            cmd.Parameters["@Comments"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ChartType", SqlDbType.VarChar);
            cmd.Parameters["@ChartType"].Value = ChartType;
            cmd.Parameters["@ChartType"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ChartAppearance", SqlDbType.VarChar);
            cmd.Parameters["@ChartAppearance"].Value = ChartAppearance;
            cmd.Parameters["@ChartAppearance"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ChartPalette", SqlDbType.VarChar);
            cmd.Parameters["@ChartPalette"].Value = ChartPalette;
            cmd.Parameters["@ChartPalette"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ChartAligment", SqlDbType.VarChar);
            cmd.Parameters["@ChartAligment"].Value = ChartAligment;
            cmd.Parameters["@ChartAligment"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ChartDock", SqlDbType.VarChar);
            cmd.Parameters["@ChartDock"].Value = ChartDock;
            cmd.Parameters["@ChartDock"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@QuestionID", SqlDbType.Int);
            cmd.Parameters["@QuestionID"].Value = QuestionID;
            cmd.Parameters["@QuestionID"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ShowLabel", SqlDbType.Int);
            cmd.Parameters["@ShowLabel"].Value = ShowLabel;
            cmd.Parameters["@ShowLabel"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LabelAngle", SqlDbType.Int);
            cmd.Parameters["@LabelAngle"].Value = LabelAngle;
            cmd.Parameters["@LabelAngle"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ValueAspercent", SqlDbType.Int);
            cmd.Parameters["@ValueAspercent"].Value = ValueAspercent;
            cmd.Parameters["@ValueAspercent"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ZoomPercent", SqlDbType.Int);
            cmd.Parameters["@ZoomPercent"].Value = ZoomPercent;
            cmd.Parameters["@ZoomPercent"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Transparency", SqlDbType.Int);
            cmd.Parameters["@Transparency"].Value = Transparency;
            cmd.Parameters["@Transparency"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@PerspectiveAngle", SqlDbType.Int);
            cmd.Parameters["@PerspectiveAngle"].Value = PerspectiveAngle;
            cmd.Parameters["@PerspectiveAngle"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@MarkerSize", SqlDbType.Int);
            cmd.Parameters["@MarkerSize"].Value = MarkerSize;
            cmd.Parameters["@MarkerSize"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ShowMarker", SqlDbType.Int);
            cmd.Parameters["@ShowMarker"].Value = ShowMarker;
            cmd.Parameters["@ShowMarker"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Inverted", SqlDbType.Int);
            cmd.Parameters["@Inverted"].Value = Inverted;
            cmd.Parameters["@Inverted"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LabelPosition", SqlDbType.VarChar);
            cmd.Parameters["@LabelPosition"].Value = LabelPosition;
            cmd.Parameters["@LabelPosition"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@MarkerKind", SqlDbType.VarChar);
            cmd.Parameters["@MarkerKind"].Value = MarkerKind;
            cmd.Parameters["@MarkerKind"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@HoleRadius", SqlDbType.VarChar);
            cmd.Parameters["@HoleRadius"].Value = HoleRadius;
            cmd.Parameters["@HoleRadius"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@DiagramType", SqlDbType.VarChar);
            cmd.Parameters["@DiagramType"].Value = DiagramType;
            cmd.Parameters["@DiagramType"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@TextDirection", SqlDbType.VarChar);
            cmd.Parameters["@TextDirection"].Value = TextDirection;
            cmd.Parameters["@TextDirection"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@FunctionType", SqlDbType.VarChar);
            cmd.Parameters["@FunctionType"].Value = FunctionType;
            cmd.Parameters["@FunctionType"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ExplodedPoint", SqlDbType.VarChar);
            cmd.Parameters["@ExplodedPoint"].Value = ExplodedPoint;
            cmd.Parameters["@ExplodedPoint"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendHorizontalAlignment", SqlDbType.VarChar);
            cmd.Parameters["@LegendHorizontalAlignment"].Value = LegendHorizontalAlignment;
            cmd.Parameters["@LegendHorizontalAlignment"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendVerticalAlignment", SqlDbType.VarChar);
            cmd.Parameters["@LegendVerticalAlignment"].Value = LegendVerticalAlignment;
            cmd.Parameters["@LegendVerticalAlignment"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendDirection", SqlDbType.VarChar);
            cmd.Parameters["@LegendDirection"].Value = LegendDirection;
            cmd.Parameters["@LegendDirection"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendMaxHorizontalPercentage", SqlDbType.Int);
            cmd.Parameters["@LegendMaxHorizontalPercentage"].Value = LegendMaxHorizontalPercentage;
            cmd.Parameters["@LegendMaxHorizontalPercentage"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendMaxVerticalPercentage", SqlDbType.Int);
            cmd.Parameters["@LegendMaxVerticalPercentage"].Value = LegendMaxVerticalPercentage;
            cmd.Parameters["@LegendMaxVerticalPercentage"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendEquallySpacedItems", SqlDbType.Int);
            cmd.Parameters["@LegendEquallySpacedItems"].Value = LegendEquallySpacedItems;
            cmd.Parameters["@LegendEquallySpacedItems"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@HideLegand", SqlDbType.Int);
            cmd.Parameters["@HideLegand"].Value = HideLegand;
            cmd.Parameters["@HideLegand"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@HideXaixs", SqlDbType.Int);
            cmd.Parameters["@HideXaixs"].Value = HideXaixs;
            cmd.Parameters["@HideXaixs"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Staggered", SqlDbType.Int);
            cmd.Parameters["@Staggered"].Value = Staggered;
            cmd.Parameters["@Staggered"].Direction = ParameterDirection.Input;
            dbm.SPExecuteNonQuery(CommandType.StoredProcedure, "sp_UpdateQuestionChart", cmd);
            return GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetSurveyAlerts(int Survey_Id)
    {
        try
        {
            dbm.OpenConnection(connect);
            string qrygetsurveyalerts = " Select ALERT_ID,ALERT_TYPE,ALERT_INFO,ALERT_DATE from osm_alertmanagement where ALERT_ID in(select max(alert_id) from osm_alertmanagement where DELETED=0 and SURVEY_ID=" + Survey_Id+"group by alert_type )";
            qrygetsurveyalerts += " Select count(RESPONDENT_ID) from osm_responsesurvey where SURVEY_ID=" + Survey_Id;
            qrygetsurveyalerts += " Select * from osm_responsesurvey where STATUS='Complete' and SURVEY_ID=" + Survey_Id + " order by RESPONDENT_ID ";
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qrygetsurveyalerts);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetIndiviudalRespIds(int Survey_ID, bool responstype, bool validresp, bool excludedresp, bool partialresp, bool partialtocompresp)
    {
        try
        {
            string RespondentIDS = GetActiveRespondent(Survey_ID);

            dbm.OpenConnection(connect);
            string QryGetResponseDetails = "Select res.RESPONDENT_ID,res.EMAIL_ID,res.STATUS,res.CREATED_ON,eml.EMAIL_ADDRESS,res.FLAG_COMPLETESTATUS,res.DELETED from osm_responsesurvey res left outer join osm_emaillist eml on res.EMAIL_ID=eml.EMAIL_ID where SURVEY_ID=" + Survey_ID;
            if (validresp && excludedresp && partialresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') ";
            }
            else if (validresp && excludedresp && partialresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') ";
            }
            else if (validresp && partialresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') and res.DELETED=0 ";
            }
            else if (partialresp && excludedresp && partialtocompresp)
            {
                QryGetResponseDetails += " and ((res.status='Complete' and res.FLAG_COMPLETESTATUS=1) or (res.STATUS='Partial' and res.FLAG_COMPLETESTATUS=0) or (res.STATUS='Complete' and res.DELETED=1))";
            }
            else if (validresp && excludedresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status='Complete'";
            }
            else if (validresp && partialresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') and res.DELETED=0";
            }
            else if (validresp && excludedresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete')";
            }
            else if (validresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete') and res.DELETED=0";
            }
            else if (partialresp && excludedresp)
            {
                QryGetResponseDetails += " and ((res.status='Complete' and res.DELETED=1) or (res.STATUS='Partial'))";
            }
            else if (excludedresp && partialtocompresp)
            {
                QryGetResponseDetails += " and ((res.status='Complete' and res.FLAG_COMPLETESTATUS=1) or (res.STATUS='Complete' and res.DELETED=1))";
            }
            else if (partialresp && partialtocompresp)
            {
                QryGetResponseDetails += " and ((res.status='Partial') or (res.STATUS='Complete' and res.FLAG_COMPLETESTATUS=1))";
            }
            else if (validresp)
            {
                QryGetResponseDetails += " and res.status='Complete' and res.DELETED=0 ";
            }
            else if (partialresp)
            {
                QryGetResponseDetails += " and res.status='Partial' and res.DELETED=0 ";
            }
            else if (excludedresp)
            {
                QryGetResponseDetails += " and res.status='Complete' and res.DELETED=1 ";
            }
            else if (partialtocompresp)
            {
                QryGetResponseDetails += " and res.status='Complete' and res.DELETED=0 and res.FLAG_COMPLETESTATUS=1 ";
            }
            if (responstype)
            {
                QryGetResponseDetails += " and res.RESPONDENT_ID IN(Select DISTINCT ques.RESPONDENT_ID ";
                QryGetResponseDetails += " from osm_responsequestions ques inner join osm_responsesurvey res on  ";
                QryGetResponseDetails += " res.RESPONDENT_ID=ques.RESPONDENT_ID where ques.DELETED=0 ";
                QryGetResponseDetails += " and res.SURVEY_ID=" + Survey_ID + ")";
            }
            QryGetResponseDetails += " order by res.STATUS";
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, QryGetResponseDetails);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    public DataSet GetIndiviudalRespIds1000(int Survey_ID, bool responstype, bool validresp, bool excludedresp, bool partialresp, bool partialtocompresp)
    {
        try
        {
            string RespondentIDS = GetActiveRespondent(Survey_ID);

            dbm.OpenConnection(connect);
            string QryGetResponseDetails = "Select top 1000 res.RESPONDENT_ID,res.EMAIL_ID,res.STATUS,res.CREATED_ON,eml.EMAIL_ADDRESS,res.FLAG_COMPLETESTATUS,res.DELETED from osm_responsesurvey res left outer join osm_emaillist eml on res.EMAIL_ID=eml.EMAIL_ID where SURVEY_ID=" + Survey_ID;
            if (validresp && excludedresp && partialresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') ";
            }
            else if (validresp && excludedresp && partialresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') ";
            }
            else if (validresp && partialresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') and res.DELETED=0 ";
            }
            else if (partialresp && excludedresp && partialtocompresp)
            {
                QryGetResponseDetails += " and ((res.status='Complete' and res.FLAG_COMPLETESTATUS=1) or (res.STATUS='Partial' and res.FLAG_COMPLETESTATUS=0) or (res.STATUS='Complete' and res.DELETED=1))";
            }
            else if (validresp && excludedresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status='Complete'";
            }
            else if (validresp && partialresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') and res.DELETED=0";
            }
            else if (validresp && excludedresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete')";
            }
            else if (validresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete') and res.DELETED=0";
            }
            else if (partialresp && excludedresp)
            {
                QryGetResponseDetails += " and ((res.status='Complete' and res.DELETED=1) or (res.STATUS='Partial'))";
            }
            else if (excludedresp && partialtocompresp)
            {
                QryGetResponseDetails += " and ((res.status='Complete' and res.FLAG_COMPLETESTATUS=1) or (res.STATUS='Complete' and res.DELETED=1))";
            }
            else if (partialresp && partialtocompresp)
            {
                QryGetResponseDetails += " and ((res.status='Partial') or (res.STATUS='Complete' and res.FLAG_COMPLETESTATUS=1))";
            }
            else if (validresp)
            {
                QryGetResponseDetails += " and res.status='Complete' and res.DELETED=0 ";
            }
            else if (partialresp)
            {
                QryGetResponseDetails += " and res.status='Partial' and res.DELETED=0 ";
            }
            else if (excludedresp)
            {
                QryGetResponseDetails += " and res.status='Complete' and res.DELETED=1 ";
            }
            else if (partialtocompresp)
            {
                QryGetResponseDetails += " and res.status='Complete' and res.DELETED=0 and res.FLAG_COMPLETESTATUS=1 ";
            }
            if (responstype)
            {
                QryGetResponseDetails += " and res.RESPONDENT_ID IN(Select DISTINCT ques.RESPONDENT_ID ";
                QryGetResponseDetails += " from osm_responsequestions ques inner join osm_responsesurvey res on  ";
                QryGetResponseDetails += " res.RESPONDENT_ID=ques.RESPONDENT_ID where ques.DELETED=0 ";
                QryGetResponseDetails += " and res.SURVEY_ID=" + Survey_ID + ")";
            }
            QryGetResponseDetails += " order by res.STATUS";
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, QryGetResponseDetails);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    public SurveyResponse GetSurveyResponses(int SurveyID, int Respondent_ID)
    {
        try
        {
            dbm.OpenConnection(connect);
            string str1 = "select * from osm_responsequestions where RESPONDENT_ID=" + Respondent_ID + " and DELETED=0 order by QUESTION_ID";
            DataSet ds_resp_ques = dbm.ExecuteDataSet(CommandType.Text, str1);
            SurveyResponse res = new SurveyResponse();
            res.ResponseQuestion = new List<ResponseQuestion>();
            for (int k = 0; k < ds_resp_ques.Tables[0].Rows.Count; k++)
            {
                ResponseQuestion ques_res = new ResponseQuestion();
                ques_res.RESPONSE_ID = Convert.ToInt32(ds_resp_ques.Tables[0].Rows[k]["RESPONSE_ID"]);
                ques_res.RESPONDENT_ID = Convert.ToInt32(ds_resp_ques.Tables[0].Rows[k]["RESPONDENT_ID"]);
                ques_res.QUESTION_ID = Convert.ToInt32(ds_resp_ques.Tables[0].Rows[k]["QUESTION_ID"]);
                ques_res.ANSWER_ID = Convert.ToInt32(ds_resp_ques.Tables[0].Rows[k]["ANSWER_ID"]);
                ques_res.ANSWER_TEXT = Convert.ToString(ds_resp_ques.Tables[0].Rows[k]["ANSWER_TEXT"]);
                res.ResponseQuestion.Add(ques_res);
            }
            return res;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public void ExcludeIndividualResp(int respondent_id, int SurveyID, int restore_type)
    {
        try
        {
            dbm.OpenConnection(connect);
            string qryQues = "";
            if (restore_type == 0)
                qryQues = "update osm_responsesurvey set DELETED=1 where RESPONDENT_ID=" + respondent_id;
            else if (restore_type == 1)
                qryQues = "update osm_responsesurvey set DELETED=0, FLAG_COMPLETESTATUS=0,VISABLE_LIMIT=0,STATUS='Partial' where RESPONDENT_ID=" + respondent_id;
            dbm.ExecuteNonQuery(CommandType.Text, qryQues);

        }
        catch { }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public bool IncludeResponsetoComplete(int respondent_id, int SurveyID, int restore_value)
    {
        try
        {
            bool ret_value = false;
            int cont = 0;
            string usrLmitDet = GetUserLimit(SurveyID, "");
            int usrLmit = 0; string LienTyp = "";
            string[] det = usrLmitDet.Split('$');
            if (det != null && det.Length > 0)
            {
                LienTyp = Convert.ToString(det.GetValue(1));
                usrLmit = Convert.ToInt32(det.GetValue(0));
            }
            if (LienTyp.ToLower() == "free")
                cont = GetResponseSurveyCount(SurveyID);
            else
                cont = GetUserSurveysResponseCount(SurveyID, 0);
            string qry = "";
            if (usrLmit == -1 || (usrLmit > 0 && usrLmit > cont))
            {
                dbm.OpenConnection(connect);
                if (restore_value == 0)
                    qry += " update osm_responsesurvey set STATUS='Complete', VISABLE_LIMIT=1, FLAG_COMPLETESTATUS=1,DELETED=0 where RESPONDENT_ID=" + respondent_id;
                else if (restore_value == 1)
                    qry += " update osm_responsesurvey set STATUS='Complete', VISABLE_LIMIT=1,  FLAG_COMPLETESTATUS=0,DELETED=0 where RESPONDENT_ID=" + respondent_id;
                dbm.ExecuteNonQuery(CommandType.Text, qry);
                ret_value = true;
            }
            return ret_value;
        }
        catch
        {
            return false;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public int GetResponseSurveyCount(int SurveyID)
    {
        int cnt = 0;
        string qry = "select count(RESPONDENT_ID) from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
        try
        {
            dbm.OpenConnection(connect);
            object obj = dbm.ExecuteScalar(CommandType.Text, qry);
            if (obj != null)
            {
                cnt = Convert.ToInt32(obj);
            }
            return cnt;
        }
        catch
        {
            return cnt;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public int GetUserSurveysResponseCount(int SurveyID, int UserID)
    {
        int cnt = 0;
        string qry = "select SURVEY_ID from osm_survey where userid= ";
        if (UserID > 0)
            qry += "'" + UserID + "'";
        else
            qry += "(select UserID from osm_survey where SURVEY_ID=" + SurveyID + ")";
        try
        {
            dbm.OpenConnection(connect);
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qry);
            if (ds != null && ds.Tables.Count > 0)
            {
                string Surveys = "";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Surveys += "," + ds.Tables[0].Rows[i]["SURVEY_ID"];
                }
                if (Surveys != null && Surveys.Trim().Length > 1)
                {
                    Surveys = Surveys.Substring(1);
                    string qry1 = "select count(RESPONDENT_ID) from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID in (" + Surveys + ")";
                    object obj = dbm.ExecuteScalar(CommandType.Text, qry1);
                    if (obj != null)
                    {
                        cnt = Convert.ToInt32(obj);
                    }
                }
            }
            return cnt;
        }
        catch
        {
            return cnt;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    /// <summary>
    /// Gets the response count.
    /// </summary>
    public int GetResponseCount(int surveyId)
    {
        return ServiceFactory.GetService<RespondentService>().GetResponseCount(surveyId);
    }
    public  DataSet voicedataset()
    {
        try
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetTeleUser", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userInfo.UserId;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsteleuser = new DataSet();
            sda.Fill(dsteleuser);
            con.Close();
            return dsteleuser;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public  SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getSurveyProfileStatistics(int surveyID)
    {
        try
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetSurveyprofileandstatistics", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet getSurveyQIDQType(int surveyID)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getsurveyQIDQType", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getAnsweroptionsQID(int questionID)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getAnsweroptionsQID", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@questionID", SqlDbType.Int)).Value = questionID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getAnsweroptionsQIDOpt(int questionID)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_Individual_Question_Report", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@question_id", SqlDbType.Int)).Value = questionID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getAnsweroptionsQIDySurveOpt(int surveyid)
    {
        try
        {
            
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_REPORT_GET_ALL_v2", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@survey_id", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getPollRespondentLimit(int userid)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_RespondentLimit", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollinfo = new DataSet();
            sda.Fill(dspollinfo);
            con.Close();
            return dspollinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

   

    public DataSet getpollinfo(int pollid,int responselmt)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_pollchartRL", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@responselmt", SqlDbType.Int)).Value = responselmt;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollinfo = new DataSet();
            sda.Fill(dspollinfo);
            con.Close();
            return dspollinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getpolldevice(int pollid,int responselmt)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_polldeviceRL", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@responselmt", SqlDbType.Int)).Value = responselmt;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollinfo = new DataSet();
            sda.Fill(dspollinfo);
            con.Close();
            return dspollinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getpollPage(int pollid, int responselmt)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_pollpageRL", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@responselmt", SqlDbType.Int)).Value = responselmt;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollinfo = new DataSet();
            sda.Fill(dspollinfo);
            con.Close();
            return dspollinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getpollcountryinfo(int pollid,string countryname,int responselmt)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_pollcountrychartRL", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@country", SqlDbType.VarChar)).Value = countryname;
            scom.Parameters.Add(new SqlParameter("@responselmt", SqlDbType.Int)).Value = responselmt;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollinfo = new DataSet();
            sda.Fill(dspollinfo);

            //DataRow dr;
            //dr = dspollinfo.Tables[5].NewRow();
            //if (countryname == "" || countryname == "All Locations")
            //{
            //    dr["country"] = countryname;
            //    dspollinfo.Tables[5].Rows.Add(dr);
            //}
            con.Close();
            return dspollinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getpollpageinfo(int pollid, string pageurl, int responselmt)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_pollpagechartRL", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@pageurl", SqlDbType.VarChar)).Value = pageurl;
            scom.Parameters.Add(new SqlParameter("@responselmt", SqlDbType.Int)).Value = responselmt;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollinfo = new DataSet();
            sda.Fill(dspollinfo);
            con.Close();
            return dspollinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getpolldeviceinfo(int pollid, string devicename, int responselmt)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_pollDevicechartRL", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@device", SqlDbType.VarChar)).Value = devicename;
            scom.Parameters.Add(new SqlParameter("@responselmt", SqlDbType.Int)).Value = responselmt;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollinfo = new DataSet();
            sda.Fill(dspollinfo);
            con.Close();
            return dspollinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getpollmobiledeviceinfo(int pollid, string devicename,int responselmt)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_pollMobileDevicechartRL", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@device", SqlDbType.VarChar)).Value = devicename;
            scom.Parameters.Add(new SqlParameter("@responselmt", SqlDbType.Int)).Value = responselmt;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollinfo = new DataSet();
            sda.Fill(dspollinfo);
            con.Close();
            return dspollinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getpollmobileosinfo(int pollid, string devicename, int responselmt)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_pollMobileOPchartRL", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@device", SqlDbType.VarChar)).Value = devicename;
            scom.Parameters.Add(new SqlParameter("@responselmt", SqlDbType.Int)).Value = responselmt;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollinfo = new DataSet();
            sda.Fill(dspollinfo);
            con.Close();
            return dspollinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getIndvQuestAnsweroptions(int surveyid,int questionid)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_REPORT_GET_ALL_v2_IndQuest", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@survey_id", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@IndvQuestion_id", SqlDbType.Int)).Value = questionid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getFreeIndvQuestAnsweroptions(int surveyid, int questionid)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_REPORT_GET_ALL_v2_FREEIndvQuest", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@survey_id", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@IndvQuestion_id", SqlDbType.Int)).Value = questionid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet get500IndvQuestAnsweroptions(int surveyid, int questionid)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_REPORT_GET_ALL_v2_500IndvQuest", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@survey_id", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@IndvQuestion_id", SqlDbType.Int)).Value = questionid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet get1000IndvQuestAnsweroptions(int surveyid, int questionid)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_REPORT_GET_ALL_v2_1000IndvQuest", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@survey_id", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@IndvQuestion_id", SqlDbType.Int)).Value = questionid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getReportquestions(int surveyid)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_REPORT_GET_ReportQuestions", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@survey_id", SqlDbType.Int)).Value = surveyid;
       
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet getAnsweroptionsQIDySurveOptPPTX(int surveyid)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_REPORT_GET_ALL_v2PPTx", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@survey_id", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getAnsweroptionsQIDySurveOptDateRange(int surveyid,DateTime stdate,DateTime enddate)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_REPORT_GET_ALL_v2_DateRange", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@survey_id", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@startdate", SqlDbType.DateTime)).Value = stdate;
            scom.Parameters.Add(new SqlParameter("@enddate", SqlDbType.DateTime)).Value = enddate;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getAnsweroptionsQIDySurveOptFree(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_REPORT_GET_ALL_v2_FREE", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@survey_id", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getAnsweroptionsQIDySurveOpt500(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_REPORT_GET_ALL_v2_500", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@survey_id", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet getAnsweroptionsQIDySurveOpt1000(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("SP_REPORT_GET_ALL_v2_1000", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@survey_id", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getMatrixAnsweroptionsQID(int questionID)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getMatxAnsweroptionsQID", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@questionID", SqlDbType.Int)).Value = questionID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsmtxansoptions = new DataSet();
            sda.Fill(dsmtxansoptions);
            con.Close();
            return dsmtxansoptions;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getSurveyAnalytics(int surveyID)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_surveyAnalytics", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@SURVEY_ID", SqlDbType.Int)).Value = surveyID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet getAnsweroptionsCSQID(int questionID)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getAnsweroptionsCSQID", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@questionID", SqlDbType.Int)).Value = questionID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getMatrixrowscolsQID(int questionID)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getMatrixrowscolsQID", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@questionID", SqlDbType.Int)).Value = questionID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveymatxques = new DataSet();
            sda.Fill(dssurveymatxques);
            con.Close();
            return dssurveymatxques;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getMatrixansrowscolsCount(int questionID,string rowname,string colname)
    {
        try
        {
           

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getMatrixansQID", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@questionID", SqlDbType.Int)).Value = questionID;
            scom.Parameters.Add(new SqlParameter("@rowname", SqlDbType.VarChar)).Value = rowname;
            scom.Parameters.Add(new SqlParameter("@colname", SqlDbType.VarChar)).Value = colname;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveymatxques = new DataSet();
            sda.Fill(dssurveymatxques);
            con.Close();
            return dssurveymatxques;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getMatrixsidebysideQID(int questionID)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getMatrixsidebyside", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@questionID", SqlDbType.Int)).Value = questionID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveymatxques = new DataSet();
            sda.Fill(dssurveymatxques);
            con.Close();
            return dssurveymatxques;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getMatrixsidebysiderowscolsCount(int questionID, string colname,string subcolname,string rowdata)
    {
        try
        {


            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getMatrixSidebySideansQID", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@questionID", SqlDbType.Int)).Value = questionID;
            scom.Parameters.Add(new SqlParameter("@column", SqlDbType.VarChar)).Value = colname;
            scom.Parameters.Add(new SqlParameter("@subcolumn", SqlDbType.VarChar)).Value = subcolname;
            scom.Parameters.Add(new SqlParameter("@rowdata", SqlDbType.VarChar)).Value = rowdata;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveymatxques = new DataSet();
            sda.Fill(dssurveymatxques);
            con.Close();
            return dssurveymatxques;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getCrxTbValuesCnt(int rowquestionID1, int colquestionID2, int rowanswerid1, int colanswerid2,int surveyID)
    {
        try
        {
            
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getCrxTbValues", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@rowquestionID1", SqlDbType.Int)).Value = rowquestionID1;
            scom.Parameters.Add(new SqlParameter("@colquestionID2", SqlDbType.Int)).Value = colquestionID2;
            scom.Parameters.Add(new SqlParameter("@rowanswerid1", SqlDbType.Int)).Value = rowanswerid1;
            scom.Parameters.Add(new SqlParameter("@colanswerid2", SqlDbType.Int)).Value = colanswerid2;
            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dscrxtb = new DataSet();
            sda.Fill(dscrxtb);
            con.Close();
            return dscrxtb;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet getRankingrowscolsQID(int questionID)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getRanking", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@questionID", SqlDbType.Int)).Value = questionID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveymatxques = new DataSet();
            sda.Fill(dssurveymatxques);
            con.Close();
            return dssurveymatxques;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getRankingrowscolsCount(int questionID, string rowname, string colname)
    {
        try
        {


            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getRankingQID", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@questionID", SqlDbType.Int)).Value = questionID;
            scom.Parameters.Add(new SqlParameter("@rowdata", SqlDbType.VarChar)).Value = rowname;
            scom.Parameters.Add(new SqlParameter("@answertext", SqlDbType.VarChar)).Value = colname;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveymatxques = new DataSet();
            sda.Fill(dssurveymatxques);
            con.Close();
            return dssurveymatxques;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getTextResponsesQID(int questionID)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getTextAnswersQID", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@questionID", SqlDbType.Int)).Value = questionID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveymatxques = new DataSet();
            sda.Fill(dssurveymatxques);
            con.Close();
            return dssurveymatxques;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getOtherResponses(int questionID)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getOtherAnswers", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@questionID", SqlDbType.Int)).Value = questionID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveymatxques = new DataSet();
            sda.Fill(dssurveymatxques);
            con.Close();
            return dssurveymatxques;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getPriceLicenseCountry( string countryname, string licensetype)
    {
        try
        {
            
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getPriceLicenseCountry", con);
            scom.CommandType = CommandType.StoredProcedure;


            scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = countryname;
            scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = licensetype;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetprice = new DataSet();
            sda.Fill(dsgetprice);
            con.Close();
            return dsgetprice;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet getCreditExpirydate(int orderid)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getCreditExpiryDate", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@orderid", SqlDbType.Int)).Value = orderid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssassyorderno = new DataSet();
            sda.Fill(dssassyorderno);
            con.Close();
            return dssassyorderno;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet updateSaasyOrderRef(string InsOrderID, string SaasyOrderRef, string TransStatus, string strAddressCountry, string strCustomerPhone, string strAddressStreet1, string strAddressStreet2, string strAddressCity, string strAddressRegion, string strCustomerCompany, string strAddressPostalCode, string strCustomerLastName,string strOrderItemSubscriptionReference)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updateSaasyorderRef", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@InsOrderID", SqlDbType.VarChar)).Value = InsOrderID;
            scom.Parameters.Add(new SqlParameter("@SaasyOrderRef", SqlDbType.VarChar)).Value = SaasyOrderRef;
            scom.Parameters.Add(new SqlParameter("@TransStatus", SqlDbType.VarChar)).Value = TransStatus;

            scom.Parameters.Add(new SqlParameter("@strAddressCountry", SqlDbType.VarChar)).Value = strAddressCountry;
            scom.Parameters.Add(new SqlParameter("@strCustomerPhone", SqlDbType.VarChar)).Value = strCustomerPhone;
            scom.Parameters.Add(new SqlParameter("@strAddressStreet1", SqlDbType.VarChar)).Value = strAddressStreet1;
            scom.Parameters.Add(new SqlParameter("@strAddressStreet2", SqlDbType.VarChar)).Value = strAddressStreet2;
            scom.Parameters.Add(new SqlParameter("@strAddressCity", SqlDbType.VarChar)).Value = strAddressCity;
            scom.Parameters.Add(new SqlParameter("@strAddressRegion", SqlDbType.VarChar)).Value = strAddressRegion;
            scom.Parameters.Add(new SqlParameter("@strCustomerCompany", SqlDbType.VarChar)).Value = strCustomerCompany;
            scom.Parameters.Add(new SqlParameter("@strAddressPostalCode", SqlDbType.VarChar)).Value = strAddressPostalCode;
            scom.Parameters.Add(new SqlParameter("@strCustomerLastName", SqlDbType.VarChar)).Value = strCustomerLastName;
            scom.Parameters.Add(new SqlParameter("@strOrderItemSubscriptionReference", SqlDbType.VarChar)).Value = strOrderItemSubscriptionReference;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdateorderef = new DataSet();
            sda.Fill(dsupdateorderef);
            con.Close();
            return dsupdateorderef;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet getSaasyorderno(int userid)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getsaasyorderinfo", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssassyorderno = new DataSet();
            sda.Fill(dssassyorderno);
            con.Close();
            return dssassyorderno;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet getSaasypaymentorderid(int orderid)
    {
        try
        {
            //var sessionService = ServiceFactory.GetService<SessionStateService>();
            //var userInfo = sessionService.GetLoginUserDetailsSession();

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetSaasyPaymentOrderID", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@orderid", SqlDbType.Int)).Value = orderid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssassyorderno = new DataSet();
            sda.Fill(dssassyorderno);
            con.Close();
            return dssassyorderno;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet updateReferredby(int pkuserid,string Referredby,string Referredbycat)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updateReferredby", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@USERID", SqlDbType.Int)).Value = pkuserid;
            scom.Parameters.Add(new SqlParameter("@Referredby", SqlDbType.NVarChar)).Value = Referredby;
            scom.Parameters.Add(new SqlParameter("@Referredbycat", SqlDbType.NVarChar)).Value = Referredbycat;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdaterferredby = new DataSet();
            sda.Fill(dsupdaterferredby);
            con.Close();
            return dsupdaterferredby;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet updateSubaccount(int pkuserid,string mandrillsubaccount)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updateSubAccount", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@USERID", SqlDbType.Int)).Value = pkuserid;
            scom.Parameters.Add(new SqlParameter("@mandrillsubaccount", SqlDbType.VarChar)).Value = mandrillsubaccount;
           

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesubaccount = new DataSet();
            sda.Fill(dsupdatesubaccount);
            con.Close();
            return dsupdatesubaccount;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet updateSaasyDefaultstatus(int pkorderid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updateSaasyDefaultStatus", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pkOrderID", SqlDbType.Int)).Value = pkorderid;
          
            
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatedefaultstatus = new DataSet();
            sda.Fill(dsupdatedefaultstatus);
            con.Close();
            return dsupdatedefaultstatus;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet updatePollSaasyDefaultstatus(int pkorderid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updatePollSaasyDefaultStatus", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pkOrderID", SqlDbType.Int)).Value = pkorderid;


            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatedefaultstatus = new DataSet();
            sda.Fill(dsupdatedefaultstatus);
            con.Close();
            return dsupdatedefaultstatus;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet updatePollOrderinfo(int pkorderid,string orderno)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updatePollOrderInfo", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pkOrderID", SqlDbType.Int)).Value = pkorderid;
            scom.Parameters.Add(new SqlParameter("@Orderno", SqlDbType.VarChar)).Value = orderno;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatedefaultstatus = new DataSet();
            sda.Fill(dsupdatedefaultstatus);
            con.Close();
            return dsupdatedefaultstatus;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet InsertPolluserhistory(int pkorderid, int userid, string licenseopt)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_insertpolluseraccounthistory", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@orderid", SqlDbType.Int)).Value = pkorderid;
            scom.Parameters.Add(new SqlParameter("@userID", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@licensetypeopt", SqlDbType.VarChar)).Value = licenseopt;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsPolluserhistory = new DataSet();
            sda.Fill(dsPolluserhistory);
            con.Close();
            return dsPolluserhistory;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet Insertdeclinedorderinfo(int pkorderid, int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_PollDeclinedOrder", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@orderid", SqlDbType.Int)).Value = pkorderid;
            scom.Parameters.Add(new SqlParameter("@userID", SqlDbType.Int)).Value = userid;
          

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsPolldecorder = new DataSet();
            sda.Fill(dsPolldecorder);
            con.Close();
            return dsPolldecorder;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet updatePollLicense(int userid, string polllicensetype, DateTime licsubdate)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updatepollLicense", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = polllicensetype;
            scom.Parameters.Add(new SqlParameter("@licensesubdate", SqlDbType.DateTime)).Value = licsubdate;      

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdaterferredby = new DataSet();
            sda.Fill(dsupdaterferredby);
            con.Close();
            return dsupdaterferredby;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public DataSet getpollorderinfo(int userid, int modeofpayment)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getpollOrderInfo", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@modeofpayment", SqlDbType.Int)).Value = modeofpayment;
       
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollorderinfo = new DataSet();
            sda.Fill(dspollorderinfo);
            con.Close();
            return dspollorderinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet insertpollinfo(int userid,string orderid, int modeofpayment,DateTime createddate,double productprice,double taxprice,double totalprice,DateTime paiddate,int transactionstatus,double taxpercent,string ORDERTYPE,string CurrencyType,double ExchangeRate,DateTime ModifiedDate)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_insertpollOrderInfo", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@ORDERID", SqlDbType.VarChar)).Value = orderid;
            scom.Parameters.Add(new SqlParameter("@MODEOFPAYMENT", SqlDbType.Int)).Value = modeofpayment;
            scom.Parameters.Add(new SqlParameter("@CREATEDDATE", SqlDbType.DateTime)).Value = createddate;
            scom.Parameters.Add(new SqlParameter("@PRODUCTPRICE", SqlDbType.Float)).Value = productprice;
            scom.Parameters.Add(new SqlParameter("@TAXPRICE", SqlDbType.Float)).Value = taxprice;
            scom.Parameters.Add(new SqlParameter("@TOTALPRICE", SqlDbType.Float)).Value = totalprice;
            scom.Parameters.Add(new SqlParameter("@PAIDDATE", SqlDbType.Float)).Value = totalprice;
            scom.Parameters.Add(new SqlParameter("@TRANSACTIONSTATUS", SqlDbType.Int)).Value = transactionstatus;
            scom.Parameters.Add(new SqlParameter("@TAXPERCENT", SqlDbType.Int)).Value = taxpercent;
            scom.Parameters.Add(new SqlParameter("@ORDERTYPE", SqlDbType.VarChar)).Value = ORDERTYPE;
            scom.Parameters.Add(new SqlParameter("@CurrencyType", SqlDbType.VarChar)).Value = CurrencyType;
            scom.Parameters.Add(new SqlParameter("@ExchangeRate", SqlDbType.Float)).Value = ExchangeRate;
            scom.Parameters.Add(new SqlParameter("@ModifiedDate", SqlDbType.DateTime)).Value = ModifiedDate;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollorderinfo = new DataSet();
            sda.Fill(dspollorderinfo);
            con.Close();
            return dspollorderinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet insertCreditTransaction(string surveytype,int units,int pkuserid,float amount,DateTime creditissueddate,DateTime creditexpirydate,string txnstatus,int orderno,string thirdpartyrefno,int emailstatus,string isupgrade,string creditused)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_InsertCreditTransactions", con);
            scom.CommandType = CommandType.StoredProcedure;
            
             scom.Parameters.Add(new SqlParameter("@surveytype", SqlDbType.NVarChar)).Value = surveytype;
             scom.Parameters.Add(new SqlParameter("@Units", SqlDbType.Int)).Value = units;
            scom.Parameters.Add(new SqlParameter("@USERID", SqlDbType.Int)).Value = pkuserid;
            scom.Parameters.Add(new SqlParameter("@Amount", SqlDbType.Float)).Value = amount;         
          scom.Parameters.Add(new SqlParameter("@Credit_issued_date", SqlDbType.DateTime)).Value = creditissueddate;
            scom.Parameters.Add(new SqlParameter("@Credit_expiry_date", SqlDbType.DateTime)).Value = creditexpirydate;
            scom.Parameters.Add(new SqlParameter("@Txn_Status", SqlDbType.NVarChar)).Value = txnstatus;
            scom.Parameters.Add(new SqlParameter("@Order_no", SqlDbType.Int)).Value = orderno;
             scom.Parameters.Add(new SqlParameter("@tp_ref_no", SqlDbType.NVarChar)).Value = thirdpartyrefno;
             scom.Parameters.Add(new SqlParameter("@email_status", SqlDbType.Int)).Value = emailstatus;
            scom.Parameters.Add(new SqlParameter("@IsUpgrade", SqlDbType.NVarChar)).Value = isupgrade;
            scom.Parameters.Add(new SqlParameter("@Credit_used", SqlDbType.NVarChar)).Value = creditused;
            
            
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdaterferredby = new DataSet();
            sda.Fill(dsupdaterferredby);
            con.Close();
            return dsupdaterferredby;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet UpdateCCTxnStatus(string txnstatus,int orderid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updatecctxnstatus", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@TransStatus", SqlDbType.VarChar)).Value = txnstatus;
            scom.Parameters.Add(new SqlParameter("@orderid", SqlDbType.Int)).Value = orderid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dscctxnstatus = new DataSet();
            sda.Fill(dscctxnstatus);
            con.Close();
            return dscctxnstatus;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public DataSet getteleuser(int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetTeleUser", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsteleuser = new DataSet();
            sda.Fill(dsteleuser);
            con.Close();
            return dsteleuser;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public DataSet gettxnstatususer(int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetOrderTranStatus", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dstxnstatususer = new DataSet();
            sda.Fill(dstxnstatususer);
            con.Close();
            return dstxnstatususer;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet updatelaunchactivationemail(int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatelaunchActivStatus", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dslnchstatususer = new DataSet();
            sda.Fill(dslnchstatususer);
            con.Close();
            return dslnchstatususer;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet getemailAuthentication()
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetEmailAuthentication", con);
            scom.CommandType = CommandType.StoredProcedure;           

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsemailAuth = new DataSet();
            sda.Fill(dsemailAuth);
            con.Close();
            return dsemailAuth;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet getRawdataexportquestions(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getRawdataexportquestions", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetRawdataexportquestions = new DataSet();
            sda.Fill(dsgetRawdataexportquestions);
            con.Close();
            return dsgetRawdataexportquestions;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getRawdataexportquestions1000(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getRawdataexportquestions1000", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetRawdataexportquestions = new DataSet();
            sda.Fill(dsgetRawdataexportquestions);
            con.Close();
            return dsgetRawdataexportquestions;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getPartialdataexportquestions(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getPartialdataexportquestions", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetRawdataexportquestions = new DataSet();
            sda.Fill(dsgetRawdataexportquestions);
            con.Close();
            return dsgetRawdataexportquestions;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getRawdataexportansweroptions(int questionid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getRawdataexportansweroptions", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@Questionid", SqlDbType.Int)).Value = questionid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetRawdataexportansweroptions = new DataSet();
            sda.Fill(dsgetRawdataexportansweroptions);
            con.Close();
            return dsgetRawdataexportansweroptions;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    // Srini, Get all answers as raw data for a given question
    public DataSet getRawdataAnswersForQues(int questionid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getRawdataAnswersForQues", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@Questionid", SqlDbType.Int)).Value = questionid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetRawdataexportansweroptions = new DataSet();
            sda.Fill(dsgetRawdataexportansweroptions);
            con.Close();
            return dsgetRawdataexportansweroptions;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public void Logochecking(Boolean str,int surveyid)
    {
        SqlConnection con = new SqlConnection();

        try
        {
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_logochkd", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@checked", SqlDbType.Bit)).Value = Convert.ToBoolean(str);
            scom.Parameters.Add(new SqlParameter("@SURVEY_ID", SqlDbType.Int)).Value = Convert.ToInt32(surveyid);

            scom.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
       
                
    }


    public DataSet getRawdataexportrespondentdata(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getRawdataexportrespondentdata", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetRawdataexportrespondentdata = new DataSet();
            sda.Fill(dsgetRawdataexportrespondentdata);
            con.Close();
            return dsgetRawdataexportrespondentdata;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getPollRawdatarespondents(int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getPollRawdatarespondents", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;           

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetRawdataexportrespondentdata = new DataSet();
            sda.Fill(dsgetRawdataexportrespondentdata);
            con.Close();
            return dsgetRawdataexportrespondentdata;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getRawdatarespondents(int surveyid,int questionid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getRawdatarespondents", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@questionid", SqlDbType.Int)).Value = questionid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetRawdataexportrespondentdata = new DataSet();
            sda.Fill(dsgetRawdataexportrespondentdata);
            con.Close();
            return dsgetRawdataexportrespondentdata;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getRawdatarespondents1000(int surveyid, int questionid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getRawdatarespondents1000", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@questionid", SqlDbType.Int)).Value = questionid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetRawdataexportrespondentdata = new DataSet();
            sda.Fill(dsgetRawdataexportrespondentdata);
            con.Close();
            return dsgetRawdataexportrespondentdata;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getPartialdatarespondents(int surveyid, int questionid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getPartialdatarespondents", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@questionid", SqlDbType.Int)).Value = questionid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetRawdataexportrespondentdata = new DataSet();
            sda.Fill(dsgetRawdataexportrespondentdata);
            con.Close();
            return dsgetRawdataexportrespondentdata;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getRawdataexportcontactinfo(int questionid,int respondentid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getRawdataexportcontactinfo", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@Questionid", SqlDbType.Int)).Value = questionid;
            scom.Parameters.Add(new SqlParameter("@Respondentid", SqlDbType.Int)).Value = respondentid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetRawdataexportcontactinfo = new DataSet();
            sda.Fill(dsgetRawdataexportcontactinfo);
            con.Close();
            return dsgetRawdataexportcontactinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet getSurveyAnalyticsData(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_surveyAnalyticsReport", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@SURVEY_ID", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetSurveyAnalyticsData = new DataSet();
            sda.Fill(dsgetSurveyAnalyticsData);
            con.Close();
            return dsgetSurveyAnalyticsData;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getpollSurveyAnalyticsData(int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_pollsurveyAnalyticsReport", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollID", SqlDbType.Int)).Value = pollid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetSurveyAnalyticsData = new DataSet();
            sda.Fill(dsgetSurveyAnalyticsData);
            con.Close();
            return dsgetSurveyAnalyticsData;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getMatrixSSMS(int questionid,int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getMatrixSSMS", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@Questionid", SqlDbType.Int)).Value = questionid;
            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetMatrixssms = new DataSet();
            sda.Fill(dsgetMatrixssms);
            con.Close();
            return dsgetMatrixssms;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getMatrixSSMS1011(int questionid, int surveyid,string mtxrowname)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getMatrixSSMS1011", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@Questionid", SqlDbType.Int)).Value = questionid;
            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@mtxrowname", SqlDbType.VarChar)).Value = mtxrowname;
            

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetMatrixssms1011 = new DataSet();
            sda.Fill(dsgetMatrixssms1011);
            con.Close();
            return dsgetMatrixssms1011;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet getMatrixSSMS12(int questionid, int surveyid, string mtxrowname,string mtxcolname)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getMatrixSSMS12", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@Questionid", SqlDbType.Int)).Value = questionid;
            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@mtxrowname", SqlDbType.VarChar)).Value = mtxrowname;
            scom.Parameters.Add(new SqlParameter("@mtxcolname", SqlDbType.VarChar)).Value = mtxcolname;


            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetMatrixssms1011 = new DataSet();
            sda.Fill(dsgetMatrixssms1011);
            con.Close();
            return dsgetMatrixssms1011;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getimageppt(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_imageppt", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@SURVEY_ID", SqlDbType.Int)).Value = surveyid;
          


            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsimageppt = new DataSet();
            sda.Fill(dsimageppt);
            con.Close();
            return dsimageppt;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getPartnerInfo(string strpartnername)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getpartnerinfo", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@partnername", SqlDbType.VarChar)).Value = strpartnername;          

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsgetpartnerinfo = new DataSet();
            sda.Fill(dsgetpartnerinfo);
            con.Close();
            return dsgetpartnerinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet getmaxseqcntsurvey(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getMaxSeqSurvey", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;



            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsmaxseq = new DataSet();
            sda.Fill(dsmaxseq);
            con.Close();
            return dsmaxseq;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public DataSet getRespondentStatus(int RespondentId,int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getRespondentStatus", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@respondentid", SqlDbType.Int)).Value = RespondentId;
            scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;



            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsmaxseq = new DataSet();
            sda.Fill(dsmaxseq);
            con.Close();
            return dsmaxseq;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public NetworkShareCodesLinkedin AddNetworkTrial(int insighto_user_ID, string network_name, string social_login_id,
                                                int social_count, int premium_days)
    {
        // Check if an entry exists with shared status
        //SqlConnection sqlconn;

        SqlConnection sqlconn = new SqlConnection();

        sqlconn = strconnectionstring();


       // SqlCommand sqlcomm;
     //   string connect = ConfigurationManager.ConnectionStrings["InsightoConnString"].ToString();
        int result = 0, count = 0;
        String errMessage = "OK";

        NetworkShareCodesLinkedin SC = NetworkShareCodesLinkedin.Shared;

      //  sqlconn = new SqlConnection(connect);
      //  sqlconn.Open();
        //sqlcomm = new SqlCommand();
        //sqlcomm.Connection = sqlconn;
        //sqlcomm.CommandType = CommandType.StoredProcedure;
        //sqlcomm.CommandText = "SP_GET_NETWORK_SHARE_STATUS";

        SqlCommand sqlcomm = new SqlCommand("SP_GET_NETWORK_SHARE_STATUS", sqlconn);
        sqlcomm.CommandType = CommandType.StoredProcedure;

        // Return value is count
        SqlParameter returnValue = new SqlParameter("@cnt", SqlDbType.Int);
        returnValue.Direction = ParameterDirection.ReturnValue;
        sqlcomm.Parameters.Add(returnValue);
        sqlcomm.Parameters.Add(new SqlParameter("@insighto_user_ID", SqlDbType.Int)).Value = insighto_user_ID;
        sqlcomm.Parameters.Add(new SqlParameter("@network_name", SqlDbType.NVarChar)).Value = network_name;
        sqlcomm.Parameters.Add(new SqlParameter("@social_login_id", SqlDbType.NVarChar)).Value = social_login_id;
        sqlcomm.Parameters.Add(new SqlParameter("@share_status", SqlDbType.SmallInt)).Value = NetworkShareCodesLinkedin.Shared;

        try
        {
            result = sqlcomm.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            errMessage = ex.Message;
       //     sqlcomm.Dispose();
          //  sqlconn.Close();
            return NetworkShareCodesLinkedin.Error;
        }

      //  sqlcomm.Dispose();

        // Check if record exisits
        count = Convert.ToInt32(returnValue.Value);
        if (count == 1)
            SC = NetworkShareCodesLinkedin.Reshared;

        //sqlcomm = new SqlCommand();
        //sqlcomm.Connection = sqlconn;
        //sqlcomm.CommandType = CommandType.StoredProcedure;
        //sqlcomm.CommandText = "SP_INSERT_NETWORK_SHARE";

        SqlCommand sqlcomminsert = new SqlCommand("SP_INSERT_NETWORK_SHARE", sqlconn);
        sqlcomminsert.CommandType = CommandType.StoredProcedure;


        sqlcomminsert.Parameters.Add(new SqlParameter("@insighto_user_ID", SqlDbType.Int)).Value = insighto_user_ID;
        sqlcomminsert.Parameters.Add(new SqlParameter("@network_name", SqlDbType.NVarChar)).Value = network_name;
        sqlcomminsert.Parameters.Add(new SqlParameter("@social_login_id", SqlDbType.NVarChar)).Value = social_login_id;
        sqlcomminsert.Parameters.Add(new SqlParameter("@social_count", SqlDbType.Int)).Value = social_count;
        sqlcomminsert.Parameters.Add(new SqlParameter("@premium_days", SqlDbType.SmallInt)).Value = premium_days;
        sqlcomminsert.Parameters.Add(new SqlParameter("@share_status", SqlDbType.SmallInt)).Value = SC;

        try
        {
            result = sqlcomminsert.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            errMessage = ex.Message;
           // sqlcomm.Dispose();
            sqlconn.Close();
            return NetworkShareCodesLinkedin.Error;
        }

      //  sqlcomm.Dispose();

        if (SC == NetworkShareCodesLinkedin.Reshared)
        {
            sqlconn.Close();
            return NetworkShareCodesLinkedin.Reshared; // can't offer premium
        }

        // Update user table for premium
        //sqlcomm = new SqlCommand();
        //sqlcomm.Connection = sqlconn;
        //sqlcomm.CommandType = CommandType.StoredProcedure;
        //sqlcomm.CommandText = "SP_NETWORK_UPDATE_OSM_USER";

        SqlCommand sqlcommuser = new SqlCommand("SP_NETWORK_UPDATE_OSM_USER", sqlconn);
        sqlcommuser.CommandType = CommandType.StoredProcedure;


        sqlcommuser.Parameters.Add(new SqlParameter("@userID", SqlDbType.Int)).Value = insighto_user_ID;
        sqlcommuser.Parameters.Add(new SqlParameter("@premium_days", SqlDbType.SmallInt)).Value = premium_days;

        try
        {
            result = sqlcommuser.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            errMessage = ex.Message;
           // sqlcomm.Dispose();
            sqlconn.Close();
            return NetworkShareCodesLinkedin.Error;
        }

       // sqlcomm.Dispose();
        sqlconn.Close();
        return NetworkShareCodesLinkedin.Shared;
    }
    public DataSet getCreditsCount(int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetCreditsCount", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dscreditscount = new DataSet();
            sda.Fill(dscreditscount);
            con.Close();
            return dscreditscount;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet getSurveyType(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getSurveyType", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveytype = new DataSet();
            sda.Fill(dssurveytype);
            con.Close();
            return dssurveytype;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public DataSet getSurveystatus(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getSurveyStatus", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveytype = new DataSet();
            sda.Fill(dssurveytype);
            con.Close();
            return dssurveytype;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public DataSet emaillaunchtypeinfo(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getEmailLaunchTypeInfo", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveytype = new DataSet();
            sda.Fill(dssurveytype);
            con.Close();
            return dssurveytype;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public DataSet updatesurveyType(int userid,string surveytype,int surveyid, string licensetype,string status)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updatesurveytype", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@surveytype", SqlDbType.VarChar)).Value = surveytype;
            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;
             scom.Parameters.Add(new SqlParameter("@license_type", SqlDbType.VarChar)).Value = licensetype;
             scom.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar)).Value = status;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesurveytype = new DataSet();
            sda.Fill(dsupdatesurveytype);
            con.Close();
            return dsupdatesurveytype;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet updatesurveylaunch(int surveyid, string launchtype)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updatesurveylaunchtype", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@launchsurveytype", SqlDbType.VarChar)).Value = launchtype;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesurveylaunch = new DataSet();
            sda.Fill(dsupdatesurveylaunch);
            con.Close();
            return dsupdatesurveylaunch;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public int getLaunchTypeNumber(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getLaunchTypeNumber", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesurveylaunch = new DataSet();
            sda.Fill(dsupdatesurveylaunch);
            con.Close();
            int number = Convert.ToInt32(dsupdatesurveylaunch.Tables[0].Rows[0]["launchsurveytype"]);
            return number;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public string getEmbedCode(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getEmbedCode", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesurveylaunch = new DataSet();
            sda.Fill(dsupdatesurveylaunch);
            con.Close();
            String embedcode = dsupdatesurveylaunch.Tables[0].Rows[0]["EMBEDCODE"].ToString();
            return embedcode;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public string getSurveyUrl(int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getSurevyUrl", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesurveylaunch = new DataSet();
            sda.Fill(dsupdatesurveylaunch);
            con.Close();
            string url = dsupdatesurveylaunch.Tables[0].Rows[0]["SURVEYLAUNCH_URL"].ToString();
            return url;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


   

    public DataSet updatesurveyEmailType(int userid, string surveytype, int surveyid, string licensetype, string status,string emailstatus,string prevsurveytype)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updatesurveyEmailtype", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@surveytype", SqlDbType.VarChar)).Value = surveytype;
            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;
            scom.Parameters.Add(new SqlParameter("@license_type", SqlDbType.VarChar)).Value = licensetype;
            scom.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar)).Value = status;
            scom.Parameters.Add(new SqlParameter("@emailstatus", SqlDbType.VarChar)).Value = emailstatus;
            scom.Parameters.Add(new SqlParameter("@prevsurveytype", SqlDbType.VarChar)).Value = prevsurveytype;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesurveytype = new DataSet();
            sda.Fill(dsupdatesurveytype);
            con.Close();
            return dsupdatesurveytype;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet updateSurveyID(int surveyid,  int orderid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("updateSurveyId", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@SURVEY_ID", SqlDbType.Int)).Value = surveyid;  
            scom.Parameters.Add(new SqlParameter("@orderID", SqlDbType.Int)).Value = orderid;          
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesurveytype = new DataSet();
            sda.Fill(dsupdatesurveytype);
            con.Close();
            return dsupdatesurveytype;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet getBizcode(int orderid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("getBizCode", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@orderID", SqlDbType.Int)).Value = orderid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesurveytype = new DataSet();
            sda.Fill(dsupdatesurveytype);
            con.Close();
            return dsupdatesurveytype;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public DataSet getSaasyBizcode(string orderid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("getSaasyBizCode", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@InsOrderID", SqlDbType.VarChar)).Value = orderid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatesurveytype = new DataSet();
            sda.Fill(dsupdatesurveytype);
            con.Close();
            return dsupdatesurveytype;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet applycreditssurvey(int userid, string surveytype, int surveyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_ApplycreditSurveys", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@surveytype", SqlDbType.VarChar)).Value = surveytype;
            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyid;
          
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsapplycreditssurvey = new DataSet();
            sda.Fill(dsapplycreditssurvey);
            con.Close();
            return dsapplycreditssurvey;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet updateSurveySettings(string customexittext, string customlatertext, int surveyid)
    {
        SqlConnection con = new SqlConnection();

        con = strconnectionstring();
        SqlCommand scom = new SqlCommand("updateSurveysettings", con);
        scom.CommandType = CommandType.StoredProcedure;

        scom.Parameters.Add(new SqlParameter("@CUSTOMEXIT_TEXT", SqlDbType.NVarChar)).Value = customexittext;
        scom.Parameters.Add(new SqlParameter("@CUSTOMSAVELATER_TEXT", SqlDbType.NVarChar)).Value = customlatertext;
        scom.Parameters.Add(new SqlParameter("@surveyId", SqlDbType.Int)).Value = surveyid;

        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet dsupdatesrvysettings = new DataSet();
        sda.Fill(dsupdatesrvysettings);
        con.Close();
        return dsupdatesrvysettings;
    }


    public DataSet updateQMSurveySettings(int QMbit,  int surveyid)
    {
        SqlConnection con = new SqlConnection();

        con = strconnectionstring();
        SqlCommand scom = new SqlCommand("updateQMSurveysettings", con);
        scom.CommandType = CommandType.StoredProcedure;

        scom.Parameters.Add(new SqlParameter("@QMCheck", SqlDbType.Bit)).Value = QMbit;      
        scom.Parameters.Add(new SqlParameter("@surveyId", SqlDbType.Int)).Value = surveyid;

        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet dsupdatesrvysettings = new DataSet();
        sda.Fill(dsupdatesrvysettings);
        con.Close();
        return dsupdatesrvysettings;
    }


    public DataSet getSurveySettings(int surveyid)
    {
        SqlConnection con = new SqlConnection();

        con = strconnectionstring();
        SqlCommand scom = new SqlCommand("getSurveysettings", con);
        scom.CommandType = CommandType.StoredProcedure;

        scom.Parameters.Add(new SqlParameter("@surveyId", SqlDbType.Int)).Value = surveyid;

        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet dsgetsrvysettings = new DataSet();
        sda.Fill(dsgetsrvysettings);
        con.Close();
        return dsgetsrvysettings;
    }

    public DataTable getUsersForAWSEmail()
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetAllUsersForAWSEmail", con);
            scom.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsusers = new DataSet();
            sda.Fill(dsusers);
            con.Close();
            DataTable dt = new DataTable();
            dt = dsusers.Tables[0];
            return dt;

        }
        catch (Exception)
        {

            throw;
        }
    }

    //public bool IsPremiumCreditApplied(int surverid) {
    //    try
    //    {
    //        SqlConnection con = new SqlConnection();
    //        con = strconnectionstring();
    //        SqlCommand scom = new SqlCommand("sp_CheckCreditApplied", con);
    //        scom.CommandType = CommandType.StoredProcedure;
    //        SqlDataAdapter sda = new SqlDataAdapter(scom);
    //        DataSet dsusers = new DataSet();
    //        sda.Fill(dsusers);
    //        con.Close();
    //        DataTable dt = new DataTable();
    //        dt = dsusers.Tables[0];
    //        return dt;

    //    }
    //    catch (Exception)
    //    {

    //        throw;
    //    }
    
    //}

    public string AddSurveyCredit(int userid, string licensetype)
    {
        string retval = "";
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_AddSurveyCredit", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@LicenseType", SqlDbType.NVarChar)).Value = licensetype;
            //scom.ExecuteNonQuery();
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsCredits = new DataSet();
            sda.Fill(dsCredits);
            con.Close();
            if (dsCredits.Tables[0].Rows[0]["ErrNumber"].ToString() == "0")
            {
                retval = "Success";
            }
            else
            {
                retval = "Error";
            }
        }
        catch (Exception)
        {
            retval = "Error";
        }
        return retval;
    }
}


