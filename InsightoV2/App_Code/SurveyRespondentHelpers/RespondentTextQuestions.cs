﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System.Collections.Generic;

namespace App_Code.SurveyRespondentHelpers
{
    /// <summary>
    /// Summary description for RespondentTextTypeQuestions
    /// </summary>
    public class RespondentTextQuestions : BusinessServiceBase
    {
        /// <summary>
        /// Creates the text comment box question.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="divMultipleTextControls">The div multiple text controls.</param>
        /// <param name="source">The source.</param>
        /// <param name="enableValidations"></param>
        public void CreateTextCommentBoxQuestion(Page page, HtmlGenericControl divMultipleTextControls, SurveyQuestionAndAnswerOptions source, List<osm_responsequestions> responseList = null, bool enableValidations = false)
        {
            LoadUserControl<SurveyRespondentControlBase>(page, divMultipleTextControls, source, "UserControls/RespondentControls/TextControl.ascx", responseList, enableValidations);
        }

        /// <summary>
        /// Creates the text single row box question.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="divMultipleTextControls">The div multiple text controls.</param>
        /// <param name="source">The source.</param>
        /// <param name="enableValidations"></param>
        public void CreateTextSingleRowBoxQuestion(Page page, HtmlGenericControl divMultipleTextControls, SurveyQuestionAndAnswerOptions source, List<osm_responsequestions> responseList = null, bool enableValidations = false)
        {
            LoadUserControl<SurveyRespondentControlBase>(page, divMultipleTextControls, source, "UserControls/RespondentControls/TextControl.ascx", responseList, enableValidations);
        }

        /// <summary>
        /// Creates the numeric single row box question.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="divMultipleTextControls">The div multiple text controls.</param>
        /// <param name="source">The source.</param>
        /// <param name="enableValidations"></param>
        public void CreateNumericSingleRowBoxQuestion(Page page, HtmlGenericControl divMultipleTextControls, SurveyQuestionAndAnswerOptions source, List<osm_responsequestions> responseList = null, bool enableValidations = false)
        {
            LoadUserControl<SurveyRespondentControlBase>(page, divMultipleTextControls, source, "UserControls/RespondentControls/TextControl.ascx", responseList, enableValidations);
        }

        /// <summary>
        /// Creates the email address question.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="divMultipleTextControls">The div multiple text controls.</param>
        /// <param name="source">The source.</param>
        /// <param name="enableValidations"></param>
        public void CreateEmailAddressQuestion(Page page, HtmlGenericControl divMultipleTextControls, SurveyQuestionAndAnswerOptions source, List<osm_responsequestions> responseList = null, bool enableValidations = false)
        {
            LoadUserControl<SurveyRespondentControlBase>(page, divMultipleTextControls, source, "UserControls/RespondentControls/TextControl.ascx", responseList, enableValidations);
        }

        /// <summary>
        /// Creates the text multiple boxes question.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="divMultipleTextControls">The div multiple text controls.</param>
        /// <param name="source">The source.</param>
        /// <param name="enableValidations"></param>
        public void CreateTextMultipleBoxesQuestion(Page page, HtmlGenericControl divMultipleTextControls, SurveyQuestionAndAnswerOptions source, List<osm_responsequestions> responseList = null, bool enableValidations = false)
        {
            LoadUserControl<SurveyRespondentControlBase>(page, divMultipleTextControls, source, "UserControls/RespondentControls/TextControl.ascx", responseList, enableValidations);
        }

        /// <summary>
        /// Creates the text question.
        /// </summary>
        /// <param name="tblText">The TBL text.</param>
        /// <param name="source">The source.</param>
        /// <param name="enableValidations">if set to <c>true</c> [enable validations].</param>
        public void CreateTextQuestion(HtmlTable tblText, SurveyQuestionAndAnswerOptions source, bool enableValidations = false)
        {
            IsRequired = source.Question.RESPONSE_REQUIRED > 0;
            AddAnswerOptionControl(tblText, null, source.QuestionType, source.Question, true, enableValidations);
            foreach (var item in source.AnswerOption)
            {
                AddAnswerOptionControl(tblText, item, source.QuestionType, source.Question, false, enableValidations);
            }
        }

        #region Auxillary methods

        /// <summary>
        /// Loads the user control.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="page"></param>
        /// <param name="divMultipleTextControls"></param>
        /// <param name="surveyoption">The surveyoption.</param>
        /// <param name="path">The path.</param>
        private static void LoadUserControl<T>(Page page, HtmlGenericControl divMultipleTextControls, SurveyQuestionAndAnswerOptions surveyoption, string path, List<osm_responsequestions> responseList, bool enableValidations) where T : SurveyRespondentControlBase
        {
            //Load Control
            var control = page.LoadControl(path) as T;

            if (surveyoption.QuestionType != QuestionType.Text_MultipleBoxes)
                surveyoption.AnswerOption.Add(new osm_answeroptions());


            if (control == null) return;
            control.ID = SurveyRespondentHelper.FormatControlId("txt", surveyoption.Question.QUESTION_TYPE_ID, surveyoption.Question.QUESTION_ID, 0);
            control.EnableValidation = enableValidations;
            control.SurveyQuestionAndAnswerOptions = surveyoption;
            control.RespondentAnswerOptions = responseList;
            divMultipleTextControls.Controls.Add(control);
        }

        private void AddAnswerOptionControl(HtmlTable tblText, osm_answeroptions item, QuestionType questionType, osm_surveyquestion question, bool isValidation = false, bool enableValidations = false)
        {
            HtmlTableRow trOption;
            var tdOption = new HtmlTableCell();
            if (!isValidation)
            {
                trOption = new HtmlTableRow();
                tdOption.ColSpan = 2;
                //add expression validator if IsRequired = true and not multiple text boxes
                if (enableValidations && IsRequired && questionType != QuestionType.Text_MultipleBoxes)
                {
                    var revTemp = RespondentValidationsHelper.GetExpressionValidator(question, questionType, item.ANSWER_ID);
                    if (revTemp != null)
                        tdOption.Controls.Add(revTemp);
                    trOption.Controls.Add(tdOption);
                    tblText.Controls.Add(trOption);
                }

                //add expression validator if IsRequired = true
                if (enableValidations && !IsRequired && questionType != QuestionType.Text_MultipleBoxes)
                {
                    var revTemp = RespondentValidationsHelper.GetExpressionValidator(question, questionType, item.ANSWER_ID);
                    if (revTemp != null)
                        tdOption.Controls.Add(revTemp);

                    trOption.Controls.Add(tdOption);
                    tblText.Controls.Add(trOption);
                }

                trOption = new HtmlTableRow();
                tdOption = new HtmlTableCell();
                var lblOption = new Label { ID = RespondentValidationsHelper.GetFormattedId("lbl", question.QUESTION_ID, question.QUESTION_TYPE_ID, item.ANSWER_ID), Text = item.ANSWER_OPTIONS };
                tdOption.Controls.Add(lblOption);
                trOption.Controls.Add(tdOption);

                tdOption = new HtmlTableCell();
                string cssText = questionType == QuestionType.Text_CommentBox ? "textAreaCommentBox" : "textBoxMedium";
                var txtOption = new TextBox { ID = RespondentValidationsHelper.GetFormattedId("txt", question.QUESTION_ID, question.QUESTION_TYPE_ID, item.ANSWER_ID), CssClass = cssText };
                tdOption.Controls.Add(txtOption);
                trOption.Controls.Add(tdOption);
                tblText.Controls.Add(trOption);
            }
        }

        #endregion

        public bool IsRequired { get; set; }
    }
}