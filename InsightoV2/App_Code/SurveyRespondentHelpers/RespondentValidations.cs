﻿using System.Web.UI.WebControls;
using Insighto.Business.Enumerations;
using Insighto.Data;
using Resources;

namespace App_Code.SurveyRespondentHelpers
{
    /// <summary>
    /// Summary description for RespondentValidationsHelper
    /// </summary>
    public class RespondentValidationsHelper
    {
        /// <summary>
        /// Gets the expression validator.
        /// </summary>
        /// <param name="demographicId">The demographic id.</param>
        /// <param name="question">The question.</param>
        /// <param name="questionType">Type of the question.</param>
        /// <param name="answerId">The answer id.</param>
        /// <returns></returns>
        public static RegularExpressionValidator GetExpressionValidator(osm_surveyquestion question, QuestionType questionType, int answerId = 0, int demographicId = 0)
        {
            RegularExpressionValidator revTemp = null;
            if (demographicId > 0)
            {
                switch (demographicId)
                {
                    case 10:
                        revTemp = new RegularExpressionValidator
                                      {
                                          ID =
                                              GetFormattedId("revEml", question.QUESTION_ID, question.QUESTION_TYPE_ID,
                                                             answerId),
                                          ValidationExpression = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
                                          ErrorMessage = @"Please enter a valid email",
                                          CssClass = RespondentResources.ErrorMessageCssClass,
                                          Display = ValidatorDisplay.Dynamic,
                                          Visible = questionType == QuestionType.EmailAddress ||
                                                    questionType == QuestionType.Numeric_SingleRowBox
                                      };
                        break;
                }

                return revTemp;
            }


            switch (questionType)
            {
                case QuestionType.EmailAddress:
                    revTemp = new RegularExpressionValidator
                                  {
                                      ID =
                                          GetFormattedId("revEml", question.QUESTION_ID, question.QUESTION_TYPE_ID,
                                                         answerId),
                                      ValidationExpression = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
                                      ErrorMessage = @"Please enter a valid email."
                                  };
                    break;

                case QuestionType.Numeric_SingleRowBox:
                    revTemp = new RegularExpressionValidator
                                  {
                                      ID =
                                          GetFormattedId("revNum", question.QUESTION_ID, question.QUESTION_TYPE_ID,
                                                         answerId),
                                      ValidationExpression = @"^\d+$",
                                      ErrorMessage = @"Please enter numerics only."
                                  };
                    break;

                case QuestionType.Text_SingleRowBox:
                case QuestionType.Text_MultipleBoxes:
                case QuestionType.Text_CommentBox:
                    revTemp = new RegularExpressionValidator
                                  {
                                      ID =
                                          GetFormattedId("revLmt", question.QUESTION_ID, question.QUESTION_TYPE_ID,
                                                         answerId),
                                      ValidationExpression = @"[0-9a-zA-Z''-'\s!@$%&()-_+=;""<>?/.]{1," + question.CHAR_LIMIT + "}",
                                      ErrorMessage = @"You have exceeded the maximum character limit of " +
                                                     question.CHAR_LIMIT + @". Please revise your answer."
                                  };
                    break;
            }

            if (revTemp != null)
            {
                revTemp.CssClass = RespondentResources.ErrorMessageCssClass;
                revTemp.Display = ValidatorDisplay.Dynamic;
                revTemp.Visible = questionType == QuestionType.EmailAddress ||
                                  questionType == QuestionType.Numeric_SingleRowBox;
            }
            return revTemp;
        }

        /// <summary>
        /// Gets the required field validator.
        /// </summary>
        /// <param name="question">The question.</param>
        /// <param name="questionType">Type of the question.</param>
        /// <param name="answerId">The answer id.</param>
        /// <returns></returns>
        public static RequiredFieldValidator GetRequiredFieldValidator(osm_surveyquestion question, QuestionType questionType, int answerId = 0)
        {
            var rfvTemp = new RequiredFieldValidator
                              {
                                  ID = GetFormattedId("rfv", question.QUESTION_ID, question.QUESTION_TYPE_ID, answerId),
                                  ErrorMessage = CommonMessages.Respondent_Mandatory,
                                  Display = ValidatorDisplay.Dynamic,
                                  CssClass = RespondentResources.ErrorMessageCssClass,
                                  Visible = false
                              };
            return rfvTemp;
        }


        /// <summary>
        /// Gets the formatted id.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <param name="answerId">The answer id.</param>
        /// <param name="questionId">The question id.</param>
        /// <param name="questionTypeId">The question type id.</param>
        /// <returns></returns>
        public static string GetFormattedId(string prefix, int questionId = 0, int? questionTypeId = 0, int answerId = 0)
        {
            return string.Format("{0}{1}_{2}_{3}", prefix, questionTypeId, questionId, answerId);
        }
    }
}