﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using Insighto.Business.Enumerations;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;

namespace App_Code.SurveyRespondentHelpers
{
    /// <summary>
    /// Summary description for RespondentMatrixQuestions
    /// </summary>
    public class RespondentMatrixQuestions : BusinessServiceBase
    {
        #region Matrix Side By Side Control

        #region Private properties

        List<string> _lstDimensions = new List<string>();
        List<string> _lstDefinitions = new List<string>();
        List<string> _lstRowAttributes = new List<string>();
        List<MatrixRowData> _lstContent = new List<MatrixRowData>();

        #endregion

        #region Public Properties

        public osm_surveyquestion QuestionInfo { get; set; }

        #endregion

        /// <summary>
        /// Creates the matrix side by side question.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="tblMatrixSideBySide">The TBL matrix side by side.</param>
        /// <param name="respondentAnswerOptions">The respondent answer options.</param>
        public void CreateMatrixSideBySideQuestion(SurveyQuestionAndAnswerOptions source, HtmlTable tblMatrixSideBySide, List<osm_responsequestions> respondentAnswerOptions = null)
        {
            QuestionInfo = source.Question;
            SplitData(source);
            BuildMatrixSideBySide(tblMatrixSideBySide, respondentAnswerOptions);
        }

        #region Auxillary methods

        /// <summary>
        /// Splits the data.
        /// </summary>
        public void SplitData(IMutableAnswerOptions<osm_answeroptions> source)
        {
            if (source == null)
                return;

            var matrixRowList = new List<MatrixRowData>();
            MatrixRowData rowData;
            foreach (var item in source.AnswerOption)
            {
                var delimiter = new[] { "$--$" };
                var splitOptions = item.ANSWER_OPTIONS.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                var is3Dimension = (splitOptions.Length == 3);
                rowData = new MatrixRowData { RowId = item.ANSWER_ID };
                if (is3Dimension)
                {
                    rowData.TopHeaderAttribute = RespondentQuestionsHelper.Trim(splitOptions[0]);
                    rowData.RowAttribute = RespondentQuestionsHelper.Trim(splitOptions[2]);
                    rowData.ColumnAttribute = RespondentQuestionsHelper.Trim(splitOptions[1]);
                }
                else
                {
                    rowData.RowAttribute = RespondentQuestionsHelper.Trim(splitOptions[0]);
                    rowData.ColumnAttribute = RespondentQuestionsHelper.Trim(splitOptions[1]);
                }

                matrixRowList.Add(rowData);
            }


            _lstContent = matrixRowList;
            _lstDimensions = matrixRowList.Select(m => m.TopHeaderAttribute).Distinct().ToList();
            _lstDefinitions = matrixRowList.Select(m => m.ColumnAttribute).Distinct().ToList();
            _lstRowAttributes = matrixRowList.Select(m => m.RowAttribute).Distinct().ToList();
        }

        /// <summary>
        /// Builds the matrix side by side.
        /// </summary>
        /// <param name="tblMatrixSideBySide"></param>
        /// <param name="respondentAnswerOptions"></param>
        private void BuildMatrixSideBySide(HtmlTable tblMatrixSideBySide, List<osm_responsequestions> respondentAnswerOptions)
        {
            if (_lstContent == null || _lstContent.Count <= 0)
                return;

            tblMatrixSideBySide.Visible = true;
            CreateDimensionsHeader(tblMatrixSideBySide);
            CreateDefinitionsHeader(tblMatrixSideBySide);
            AddAttributesAndContent(tblMatrixSideBySide, respondentAnswerOptions);

        }

        /// <summary>
        /// Creates the dimensions header.
        /// </summary>
        private void CreateDimensionsHeader(HtmlTable tblMatrixSideBySide)
        {
            if (_lstRowAttributes.Count <= 0 || _lstDimensions.Count <= 0)
                return;

            var rowDimension = new HtmlTableRow();
            var cellDimension = new HtmlTableCell();
            rowDimension.Cells.Add(cellDimension);

            //sets the dimensions
            foreach (var obj in _lstDimensions)
            {
                cellDimension = new HtmlTableCell { ColSpan = _lstDefinitions.Count + 1, Align = "Center" };
                cellDimension.Attributes["Style"] = "padding:8px";
                cellDimension.InnerHtml = obj;
                rowDimension.Cells.Add(cellDimension);
            }
            tblMatrixSideBySide.Rows.Add(rowDimension);
        }

        /// <summary>
        /// Creates the definitions header.
        /// </summary>
        /// <param name="tblMatrixSideBySide">The TBL matrix side by side.</param>
        private void CreateDefinitionsHeader(HtmlTable tblMatrixSideBySide)
        {
            if (_lstDefinitions.Count > 0)
            {
                var rowDefinition = new HtmlTableRow();
                var cellDefinition = new HtmlTableCell();
                rowDefinition.Cells.Add(cellDefinition);

#pragma warning disable 168
                foreach (var ob in _lstDimensions)
#pragma warning restore 168
                {
                    //this sets gap for group
                    cellDefinition = new HtmlTableCell();
                    rowDefinition.Cells.Add(cellDefinition);

                    //sets column definitions fro each dimension.
                    foreach (var obj in _lstDefinitions)
                    {
                        cellDefinition = new HtmlTableCell { Align = "Center" };
                        cellDefinition.Attributes["Style"] = "padding:8px";
                        cellDefinition.InnerHtml = obj;
                        rowDefinition.Cells.Add(cellDefinition);
                    }
                }
                tblMatrixSideBySide.Rows.Add(rowDefinition);
            }
        }

        /// <summary>
        /// Adds the content of the attributes and.
        /// </summary>
        /// <param name="tblMatrixSideBySide">The TBL matrix side by side.</param>
        /// <param name="respondentAnswerOptions">The respondent answer options.</param>
        private void AddAttributesAndContent(HtmlTable tblMatrixSideBySide, List<osm_responsequestions> respondentAnswerOptions)
        {
            if (_lstContent == null) return;

            int rowCell = 0, rowHead = 0, rbCellGroup = 0, rbCellCntr = 0;
            HtmlTableRow rowAtrributeAndOptions = null;
            HtmlTableCell cellAtrributeAndOptions;
            MatrixRowData matrixRow = null;
            int headerIndex = 0;
            int rowIndex = 0;
            foreach (var option in _lstContent)
            {
                matrixRow = _lstContent.Where(ao => ao.ColumnAttribute == _lstDefinitions[rbCellCntr] && ao.RowAttribute == _lstRowAttributes[rowIndex] && ao.TopHeaderAttribute == _lstDimensions[headerIndex]).FirstOrDefault();
                if (rowCell == 0)
                {
                    //add row attribute ..will be dispalyed in 1st column, generally.
                    rowAtrributeAndOptions = new HtmlTableRow();
                    cellAtrributeAndOptions = new HtmlTableCell
                    {
                        InnerHtml = _lstRowAttributes[rowHead],
                        NoWrap = true
                    };
                    rowAtrributeAndOptions.Cells.Add(cellAtrributeAndOptions);
                    rowHead++;
                }
                if (rbCellCntr == 0)
                {
                    //this set the one column gap b/w row groups
                    cellAtrributeAndOptions = new HtmlTableCell();
                    if (rowAtrributeAndOptions != null) rowAtrributeAndOptions.Cells.Add(cellAtrributeAndOptions);
                }

                //Add options and values...
                cellAtrributeAndOptions = new HtmlTableCell { Align = "Center" };
                var rb = new HtmlInputRadioButton
                {
                    ID = matrixRow.RowId.ToString(),
                    Value = SurveyRespondentHelper.FormatControlId(MATRIX_PREFIX, QuestionInfo.QUESTION_TYPE_ID,
                                                                   QuestionInfo.QUESTION_ID,
                                                                   matrixRow.RowId),
                    Name = "rbTwelve" + rbCellGroup
                };

                //if user is halfway through
                var response = Utilities.GetResponseQuestion(respondentAnswerOptions, matrixRow.RowId);
                rb.Checked = response != null;

                cellAtrributeAndOptions.Controls.Add(rb);
                if (rowAtrributeAndOptions != null) rowAtrributeAndOptions.Cells.Add(cellAtrributeAndOptions);
                rowCell++;
                rbCellCntr++;

                if (rbCellCntr == _lstDefinitions.Count)
                {
                    rbCellCntr = 0;
                    rbCellGroup++;
                    headerIndex++;
                }
                if (rowCell == _lstDimensions.Count * _lstDefinitions.Count)
                {
                    rowCell = 0;
                    headerIndex = 0;
                    rowIndex++;
                    tblMatrixSideBySide.Rows.Add(rowAtrributeAndOptions);
                }
            }
        }

        #endregion

        #endregion

        #region Matrix Single/Multiple Select Control

        #region Private properties

        private List<MatrixRowData> _matrixRowList = new List<MatrixRowData>();
        private List<string> _topHeaderList;
        private List<string> _columnHeaderList;
        private List<string> _rowHeaderList;

        #endregion

        #region Public properties

        public List<MatrixRowData> MatrixRowList
        {
            get
            {
                return _matrixRowList;
            }
            set
            {
                _matrixRowList = value;
            }
        }

        public List<string> TopHeaderList
        {
            get
            {
                if (!_topHeaderList.Any())
                    _topHeaderList = MatrixRowList.Select(m => m.TopHeaderAttribute).Distinct().ToList();

                return _topHeaderList;
            }
        }

        public List<string> RowHeaderList
        {
            get
            {
                if (!_rowHeaderList.Any())
                    _rowHeaderList = MatrixRowList.Select(m => m.RowAttribute).Distinct().ToList();

                return _rowHeaderList;
            }
        }

        public List<string> ColumnHeaderList
        {
            get
            {
                if (!_columnHeaderList.Any())
                    _columnHeaderList = MatrixRowList.Select(m => m.ColumnAttribute).Distinct().ToList();

                return _columnHeaderList;
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Creates the matrix single select question.
        /// </summary>
        /// <param name="tblMatrix2D">The table matrix2 D.</param>
        /// <param name="source">The source.</param>
        /// <param name="respondentAnswerOptions">The respondent answer options.</param>
        public void CreateMatrixSingleSelectQuestion(HtmlTable tblMatrix2D, SurveyQuestionAndAnswerOptions source, List<osm_responsequestions> respondentAnswerOptions = null)
        {
            _topHeaderList = new List<string>();
            _columnHeaderList = new List<string>();
            _rowHeaderList = new List<string>();
            QuestionInfo = source.Question;

            _matrixRowList = GenerateMatrixData(source);
            GenrateMatrixQues(tblMatrix2D, QuestionType.Matrix_SingleSelect, respondentAnswerOptions);
        }

        /// <summary>
        /// Creates the matrix multiple select question.
        /// </summary>
        /// <param name="tblMatrix2D">The table matrix2 D.</param>
        /// <param name="source">The source.</param>
        /// <param name="respondentAnswerOptions">The respondent answer options.</param>
        public void CreateMatrixMultipleSelectQuestion(HtmlTable tblMatrix2D, SurveyQuestionAndAnswerOptions source, List<osm_responsequestions> respondentAnswerOptions = null)
        {
            _topHeaderList = new List<string>();
            _columnHeaderList = new List<string>();
            _rowHeaderList = new List<string>();
            QuestionInfo = source.Question;

            _matrixRowList = GenerateMatrixData(source);
            GenrateMatrixQues(tblMatrix2D, QuestionType.Matrix_MultipleSelect, respondentAnswerOptions);
        }

        #endregion

        #region Auxillary methods

        /// <summary>
        /// Splits the data.
        /// </summary>
        public static List<MatrixRowData> GenerateMatrixData(SurveyQuestionAndAnswerOptions source)
        {
            var matrixRowList = new List<MatrixRowData>();
            if (source == null)
                return matrixRowList;

            foreach (var item in source.AnswerOption)
            {
                var delimiter = new[] { "$--$" };
                var splitOptions = item.ANSWER_OPTIONS.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                var is3Dimension = (splitOptions.Length == 3);
                var rowData = new MatrixRowData { RowId = item.ANSWER_ID };
                if (is3Dimension)
                {
                    rowData.TopHeaderAttribute = RespondentQuestionsHelper.Trim(splitOptions[0]);
                    rowData.RowAttribute = RespondentQuestionsHelper.Trim(splitOptions[1]);
                    rowData.ColumnAttribute = RespondentQuestionsHelper.Trim(splitOptions[2]);
                }
                else
                {
                    rowData.RowAttribute = RespondentQuestionsHelper.Trim(splitOptions[1]);
                    rowData.ColumnAttribute = RespondentQuestionsHelper.Trim(splitOptions[0]);
                }
                matrixRowList.Add(rowData);
            }

            return matrixRowList;
        }

        private const string MATRIX_PREFIX = "mx";
        private void GenrateMatrixQues(HtmlTable tblMatrix2D, QuestionType questionType, List<osm_responsequestions> respondentAnswerOptions)
        {
            var tableAnsRow = new HtmlTableRow();
            tableAnsRow.Cells.Add(new HtmlTableCell());
            MatrixRowData matrixRow = null;
            int rowIndex = 0;
            foreach (var tableColHeadcell in ColumnHeaderList.Select(obj => new HtmlTableCell { InnerHtml = obj }))
            {
                int tdcnt = ColumnHeaderList.Count + 1;
                int tdwidth = 60 / tdcnt;
                tableColHeadcell.Width = tdwidth.ToString() + "%";               
              //  tableColHeadcell.Align = "Center";
               tableColHeadcell.Attributes.Add("class", "tdMatrixColumnSelect");
                tableAnsRow.Cells.Add(tableColHeadcell);
            }
            tblMatrix2D.Rows.Add(tableAnsRow);

            int l = 0, j = 0;
            var tableAnsRowop = new HtmlTableRow();
            foreach (var row in MatrixRowList)
            {
                matrixRow = MatrixRowList.Where(ao => ao.ColumnAttribute == ColumnHeaderList[l] && ao.RowAttribute == RowHeaderList[rowIndex]).FirstOrDefault();
                if (l == 0)
                {
                    tableAnsRowop = new HtmlTableRow();
                    var tblrowHeadCell = new HtmlTableCell { InnerHtml = RowHeaderList[j] };
                    tblrowHeadCell.Attributes.Add("class", "tdMatrixRowSelect");
                    tableAnsRowop.Cells.Add(tblrowHeadCell);
                    j++;
                }
                var tableRowcell = new HtmlTableCell();
                int tdcnt1 = ColumnHeaderList.Count + 1;
                int tdwidth1 = 60 / tdcnt1;
                tableRowcell.Width = tdwidth1.ToString() + "%";
               // tableRowcell.Width = "10%";
                tableRowcell.Align = "Center";

                switch (questionType)
                {
                    case QuestionType.Matrix_SingleSelect:
                        {
                            var rb = new HtmlInputRadioButton
                            {
                                ID = matrixRow.RowId.ToString(),
                                Value = SurveyRespondentHelper.FormatControlId(MATRIX_PREFIX, QuestionInfo.QUESTION_TYPE_ID,
                                                                   QuestionInfo.QUESTION_ID,
                                                                   matrixRow.RowId),
                                Name = "rndGrp" + j
                            };

                            //if user is halfway through
                            var response = Utilities.GetResponseQuestion(respondentAnswerOptions, matrixRow.RowId);
                            rb.Checked = response != null;

                            tableRowcell.Controls.Add(rb);
                        }
                        break;
                    case QuestionType.Matrix_MultipleSelect:
                        {
                            var chk = new HtmlInputCheckBox
                            {
                                ID = matrixRow.RowId.ToString(),
                                Value = SurveyRespondentHelper.FormatControlId(MATRIX_PREFIX, QuestionInfo.QUESTION_TYPE_ID,
                                                                   QuestionInfo.QUESTION_ID,
                                                                   matrixRow.RowId),
                                Name = row.RowId.ToString()
                            };

                            //if user is halfway through
                            var response = Utilities.GetResponseQuestion(respondentAnswerOptions, matrixRow.RowId);
                            chk.Checked = response != null;

                            tableRowcell.Controls.Add(chk);
                        }
                        break;
                }
                tableAnsRowop.Cells.Add(tableRowcell);
                l++;
                if (l != ColumnHeaderList.Count) continue;
                l = 0;
                rowIndex++;

                tblMatrix2D.Rows.Add(tableAnsRowop);
            }

            tblMatrix2D.Visible = true;
        }

        #endregion

        #endregion
    }
}