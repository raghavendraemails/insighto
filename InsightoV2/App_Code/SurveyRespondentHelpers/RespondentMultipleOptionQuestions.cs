﻿using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using Insighto.Business.Enumerations;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System.Collections.Generic;
using System;
using Insighto.Business.Helpers;
using System.Linq;
using CovalenseUtilities.Services;

namespace App_Code.SurveyRespondentHelpers
{
    public class RespondentMultipleQuestions : BusinessServiceBase
    {
        #region Contact info question type

        /// <summary>
        /// Creates the contact info question.
        /// </summary>
        /// <param name="tblContactInfo">The table contact info.</param>
        /// <param name="surveyQuestionAndAnswerOptions">The survey question and answer options.</param>
        /// <param name="enableValidations">if set to <c>true</c> [enable validations].</param>
        /// <returns></returns>
        public HtmlTable CreateContactInfoQuestion(HtmlTable tblContactInfo, SurveyQuestionAndAnswerOptions surveyQuestionAndAnswerOptions, bool enableValidations = false)
        {
            foreach (var item in surveyQuestionAndAnswerOptions.AnswerOption)
            {
                AddAnswerOptionControl(tblContactInfo, item, surveyQuestionAndAnswerOptions.Question, surveyQuestionAndAnswerOptions.QuestionType, enableValidations);
            }
            return tblContactInfo;
        }

        #endregion

        #region MOMS question type

        /// <summary>
        /// Creates the MOMS question.
        /// </summary>
        /// <param name="lstControl">The LST control.</param>
        /// <param name="surveyQuestionAndAnswerOptions">The survey question and answer options.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="answerIds">The answer ids.</param>
        /// <param name="txtOther">The TXT other.</param>
        /// <param name="isRandomizeOptions">if set to <c>true</c> [is randomize options].</param>
        /// <param name="isAdditionalItemRequired">if set to <c>true</c> [is additional item required].</param>
        /// <param name="textField">The text field.</param>
        /// <param name="valueField">The value field.</param>
        public void CreateMOMSQuestion(ListControl lstControl, SurveyQuestionAndAnswerOptions surveyQuestionAndAnswerOptions, string prefix, List<osm_responsequestions> answerIds, TextBox txtOther, bool isRandomizeOptions = false, bool isAdditionalItemRequired = false, string textField = null, string valueField = null)
        {
            if (lstControl == null) return;
            lstControl.Items.Clear();
            var checkBoxList = new CheckBoxList();
            if (lstControl is CheckBoxList)
            {
                checkBoxList = lstControl as CheckBoxList;
                checkBoxList.RepeatColumns = ValidationHelper.GetInteger(surveyQuestionAndAnswerOptions.Question.ANSWER_ALIGNSTYLE, 1);
                if (surveyQuestionAndAnswerOptions.Question.ANSWER_ALIGNSTYLE == RepeatDirection.Horizontal.ToString())
                    checkBoxList.RepeatDirection = RepeatDirection.Horizontal;
                else
                    checkBoxList.RepeatColumns = ValidationHelper.GetInteger(surveyQuestionAndAnswerOptions.Question.ANSWER_ALIGNSTYLE, 1);
            }
            lstControl.ValidationGroup = ToString(surveyQuestionAndAnswerOptions.Question.QUESTION_ID);
            ListItem lstAnswerOption;
            var lstAnswerOptions = isRandomizeOptions == false
                                       ? surveyQuestionAndAnswerOptions.AnswerOption
                                       : Shuffle(surveyQuestionAndAnswerOptions.AnswerOption);
            foreach (var item in lstAnswerOptions)
            {
                lstAnswerOption = new ListItem
                {
                    Text = item.ANSWER_OPTIONS,
                    Value = SurveyRespondentHelper.FormatControlId(prefix,
                                                                   surveyQuestionAndAnswerOptions.Question.
                                                                       QUESTION_TYPE_ID,
                                                                   item.QUESTION_ID,
                                                                   item.ANSWER_ID)
                };

                //check whether the response filling from halfwaythrough
                var response = GetResponseQuestion(answerIds, item.ANSWER_ID);
                lstAnswerOption.Selected = response != null;
                if (lstControl is CheckBoxList)
                    checkBoxList.Items.Add(lstAnswerOption);

               // txtOther.Visible = surveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0;
                txtOther.Text = response != null ? response.ANSWER_TEXT : string.Empty;
                
            }
            if (!isAdditionalItemRequired) return;
            lstControl.Items.Insert(0, "-- Select --");
            lstControl.Items[0].Value = @"-1";
        }
        #endregion

        #region Dropdown menu question type
        /// <summary>
        /// Creates the dropdown menu question.
        /// </summary>
        /// <param name="lstControl">The LST control.</param>
        /// <param name="surveyQuestionAndAnswerOptions">The survey question and answer options.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="answerIds">The answer ids.</param>
        /// <param name="txtOther">The TXT other.</param>
        /// <param name="isRandomizeOptions">if set to <c>true</c> [is randomize options].</param>
        /// <param name="isAdditionalItemRequired">if set to <c>true</c> [is additional item required].</param>
        /// <param name="textField">The text field.</param>
        /// <param name="valueField">The value field.</param>
        public void CreateDropdownMenuQuestion(ListControl lstControl, SurveyQuestionAndAnswerOptions surveyQuestionAndAnswerOptions, string prefix, List<osm_responsequestions> answerIds, TextBox txtOther, bool isRandomizeOptions = false, bool isAdditionalItemRequired = false, string textField = null, string valueField = null)
        {
            if (lstControl == null) return;
            lstControl.Items.Clear();
            lstControl.ValidationGroup = ToString(surveyQuestionAndAnswerOptions.Question.QUESTION_ID);
            ListItem lstAnswerOption;
            var lstAnswerOptions = isRandomizeOptions == false
                                       ? surveyQuestionAndAnswerOptions.AnswerOption
                                       : Shuffle(surveyQuestionAndAnswerOptions.AnswerOption);
            foreach (var item in lstAnswerOptions)
            {
                lstAnswerOption = new ListItem
                {
                    Text = item.ANSWER_OPTIONS,
                    Value = SurveyRespondentHelper.FormatControlId(prefix,
                                                                   surveyQuestionAndAnswerOptions.Question.
                                                                       QUESTION_TYPE_ID,
                                                                   item.QUESTION_ID,
                                                                   item.ANSWER_ID)
                };

                //check whether the response filling from halfwaythrough
                var response = GetResponseQuestion(answerIds, item.ANSWER_ID);                
                if (lstControl is DropDownList)
                    lstControl.Items.Add(lstAnswerOption);

                lstAnswerOption.Selected = response != null;
                txtOther.Visible = surveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0;
                txtOther.Text = response != null ? response.ANSWER_TEXT : string.Empty;
            }

            if (!isAdditionalItemRequired || !(lstControl is DropDownList)) return;
            lstControl.Items.Insert(0, "-- Select --");
            lstControl.Items[0].Value = @"0";
            lstControl.SelectedIndex = 0;
        }
        #endregion

        #region Yes No option question type
        /// <summary>
        /// Creates the yes no option question.
        /// </summary>
        /// <param name="lstControl">The LST control.</param>
        /// <param name="surveyQuestionAndAnswerOptions">The survey question and answer options.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="answerIds">The answer ids.</param>
        /// <param name="txtOther">The TXT other.</param>
        /// <param name="isRandomizeOptions">if set to <c>true</c> [is randomize options].</param>
        /// <param name="isAdditionalItemRequired">if set to <c>true</c> [is additional item required].</param>
        /// <param name="textField">The text field.</param>
        /// <param name="valueField">The value field.</param>
        public void CreateYesNoOptionQuestion(ListControl lstControl, SurveyQuestionAndAnswerOptions surveyQuestionAndAnswerOptions, string prefix, List<osm_responsequestions> answerIds, TextBox txtOther, bool isRandomizeOptions = false, bool isAdditionalItemRequired = false, string textField = null, string valueField = null)
        {
            GetMossQuestionTypeControl(lstControl, surveyQuestionAndAnswerOptions, prefix, answerIds, txtOther, isRandomizeOptions, isAdditionalItemRequired);
        }
        #endregion

        #region MOSS question type
        /// <summary>
        /// Creates the MOss question.
        /// </summary>
        /// <param name="lstControl">The LST control.</param>
        /// <param name="surveyQuestionAndAnswerOptions">The survey question and answer options.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="answerIds">The answer ids.</param>
        /// <param name="txtOther">The TXT other.</param>
        /// <param name="isRandomizeOptions">if set to <c>true</c> [is randomize options].</param>
        /// <param name="isAdditionalItemRequired">if set to <c>true</c> [is additional item required].</param>
        /// <param name="textField">The text field.</param>
        /// <param name="valueField">The value field.</param>
        public void CreateMossQuestiontable(ListControl lstControl, SurveyQuestionAndAnswerOptions surveyQuestionAndAnswerOptions, string prefix, List<osm_responsequestions> answerIds, TextBox txtOther,HtmlTable tblmoss, bool isRandomizeOptions = false, bool isAdditionalItemRequired = false, string textField = null, string valueField = null)
        {
            GetMossQuestionTypeControltable(lstControl, surveyQuestionAndAnswerOptions, prefix, answerIds, txtOther,tblmoss, isRandomizeOptions, isAdditionalItemRequired);
        }

        public void CreateMossQuestion(ListControl lstControl, SurveyQuestionAndAnswerOptions surveyQuestionAndAnswerOptions, string prefix, List<osm_responsequestions> answerIds, TextBox txtOther,  bool isRandomizeOptions = false, bool isAdditionalItemRequired = false, string textField = null, string valueField = null)
        {
            GetMossQuestionTypeControl(lstControl, surveyQuestionAndAnswerOptions, prefix, answerIds, txtOther,  isRandomizeOptions, isAdditionalItemRequired);
        }
        #endregion

        #region Auxillary methods

        /// <summary>
        /// Adds the answer option control.
        /// </summary>
        /// <param name="tblContactInfo">The TBL contact info.</param>
        /// <param name="answeroption">The answeroption.</param>
        /// <param name="question">The question.</param>
        /// <param name="questionType">Type of the question.</param>
        /// <param name="enableValidations">if set to <c>true</c> [enable validations].</param>
        private static void AddAnswerOptionControl(HtmlTable tblContactInfo, osm_answeroptions answeroption, osm_surveyquestion question, QuestionType questionType, bool enableValidations)
        {
            var trOption = new HtmlTableRow();
            var tdOption = new HtmlTableCell();

            var lblOption = new Label { ID = "lbl" + answeroption.ANSWER_ID, Text = answeroption.ANSWER_OPTIONS };
            tdOption.Controls.Add(lblOption);
            trOption.Controls.Add(tdOption);

            var txtOption = new TextBox();
            tdOption = new HtmlTableCell();
            txtOption.ID = RespondentValidationsHelper.GetFormattedId("txt", ValidationHelper.GetInteger(question.QUESTION_ID, 0), ValidationHelper.GetInteger(question.QUESTION_TYPE_ID, 0), ValidationHelper.GetInteger(answeroption.ANSWER_ID, 0));
            txtOption.CssClass = "textBoxMedium";
            tdOption.Controls.Add(txtOption);
            trOption.Controls.Add(tdOption);
            tblContactInfo.Controls.Add(trOption);

            trOption = new HtmlTableRow();
            tdOption = new HtmlTableCell { ColSpan = 2 };
            if (enableValidations && answeroption.RESPONSE_REQUIRED > 0)
                tdOption.Controls.Add(RespondentValidationsHelper.GetRequiredFieldValidator(question, questionType,
                                                                                            ValidationHelper.GetInteger(
                                                                                                answeroption.ANSWER_ID,
                                                                                                0)));
            if (enableValidations)
            {
                var revEmail = RespondentValidationsHelper.GetExpressionValidator(question, questionType,
                                                                             answeroption.ANSWER_ID,
                                                                             answeroption.DEMOGRAPIC_BLOCKID);
                if (revEmail != null)
                tdOption.Controls.Add(revEmail);
            }
            trOption.Controls.Add(tdOption);
            tblContactInfo.Controls.Add(trOption);
        }


        /// <summary>
        /// Shuffles the specified list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        public static List<T> Shuffle<T>(List<T> list)
        {
            var randomizedList = new List<T>();
            var rnd = new Random();
            while (list.Count > 0)
            {
                var index = rnd.Next(0, list.Count); //pick a random item from the master list
                randomizedList.Add(list[index]); //place it at the end of the randomized list
                list.RemoveAt(index);
            }
            return randomizedList;
        }

        /// <summary>
        /// Gets the response question.
        /// </summary>
        /// <param name="answerIds">The answer ids.</param>
        /// <param name="target">The target.</param>
        /// <param name="isQuestionId">if set to <c>true</c> [is question id].</param>
        /// <returns></returns>
        public static osm_responsequestions GetResponseQuestion(List<osm_responsequestions> answerIds, int target, bool isQuestionId = false)
        {
            if (isQuestionId)
                return answerIds != null ? answerIds.Where(p => p.QUESTION_ID == target).FirstOrDefault() : null;
            return answerIds != null ? answerIds.Where(p => p.ANSWER_ID == target).FirstOrDefault() : null;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public static string ToString(object source)
        {
            try
            {
                return source != null ? Convert.ToString(source) : string.Empty;
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// Gets the moss question type control.
        /// </summary>
        /// <param name="lstControl">The LST control.</param>
        /// <param name="surveyQuestionAndAnswerOptions">The survey question and answer options.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="answerIds">The answer ids.</param>
        /// <param name="txtOther">The TXT other.</param>
        /// <param name="isRandomizeOptions">if set to <c>true</c> [is randomize options].</param>
        /// <param name="isAdditionalItemRequired">if set to <c>true</c> [is additional item required].</param>
        private static void GetMossQuestionTypeControl(ListControl lstControl, SurveyQuestionAndAnswerOptions surveyQuestionAndAnswerOptions, string prefix, List<osm_responsequestions> answerIds, TextBox txtOther, bool isRandomizeOptions = false, bool isAdditionalItemRequired = false)
        {
            bool OtherHasText = false;
            if (lstControl == null) return;
            lstControl.Items.Clear();
            var radioButtonList = new RadioButtonList();
            if (lstControl is RadioButtonList)
            {
                radioButtonList = lstControl as RadioButtonList;
                              
                if (surveyQuestionAndAnswerOptions.Question.ANSWER_ALIGNSTYLE == RepeatDirection.Horizontal.ToString())
                    radioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                else
                    radioButtonList.RepeatColumns = ValidationHelper.GetInteger(surveyQuestionAndAnswerOptions.Question.ANSWER_ALIGNSTYLE, 1);
            }
            lstControl.ValidationGroup = ToString(surveyQuestionAndAnswerOptions.Question.QUESTION_ID);
            ListItem lstAnswerOption;
            var lstAnswerOptions = isRandomizeOptions == false
                                       ? surveyQuestionAndAnswerOptions.AnswerOption
                                       : Shuffle(surveyQuestionAndAnswerOptions.AnswerOption);

            //double dcl = Convert.ToDouble(radioButtonList.RepeatColumns);
            //double dcl1 = Convert.ToDouble(lstAnswerOptions.Count);

            //int rowcnt = Convert.ToInt32(Math.Round(dcl1 / dcl));

           
            //int tdwidth = 100 / radioButtonList.RepeatColumns;
            


            foreach (var item in lstAnswerOptions)
            {
                

                lstAnswerOption = new ListItem
                {
                
                    Text = item.ANSWER_OPTIONS,
                    Value = SurveyRespondentHelper.FormatControlId(prefix,
                                                                   surveyQuestionAndAnswerOptions.Question.
                                                                       QUESTION_TYPE_ID,
                                                                   item.QUESTION_ID,
                                                                   item.ANSWER_ID)
                };

                var response = GetResponseQuestion(answerIds, item.ANSWER_ID);
                lstAnswerOption.Selected = response != null;



                if (lstControl is RadioButtonList)
                  //  lstControl.Width = 100;
                    radioButtonList.Items.Add( lstAnswerOption);
              
                 //  txtOther.Visible = surveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0;
                if (response != null && OtherHasText == false)
                {                    
                    txtOther.Text = response != null ? response.ANSWER_TEXT : string.Empty;
                    OtherHasText = true;
                }

          
            }
        

            if (!isAdditionalItemRequired) return;
            lstControl.Items.Insert(0, "-- Select --");
            lstControl.Items[0].Value = @"0";
        }


        private static void GetMossQuestionTypeControltable(ListControl lstControl, SurveyQuestionAndAnswerOptions surveyQuestionAndAnswerOptions, string prefix, List<osm_responsequestions> answerIds, TextBox txtOther,HtmlTable tblmoss, bool isRandomizeOptions = false, bool isAdditionalItemRequired = false)
        {
            bool OtherHasText = false;
            if (lstControl == null) return;
            lstControl.Items.Clear();
            var radioButtonList = new RadioButtonList();
            if (lstControl is RadioButtonList)
            {
                radioButtonList = lstControl as RadioButtonList;

                if (surveyQuestionAndAnswerOptions.Question.ANSWER_ALIGNSTYLE == RepeatDirection.Horizontal.ToString())
                    radioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                else
                    radioButtonList.RepeatColumns = ValidationHelper.GetInteger(surveyQuestionAndAnswerOptions.Question.ANSWER_ALIGNSTYLE, 1);
            }
            lstControl.ValidationGroup = ToString(surveyQuestionAndAnswerOptions.Question.QUESTION_ID);
            ListItem lstAnswerOption;
            var lstAnswerOptions = isRandomizeOptions == false
                                       ? surveyQuestionAndAnswerOptions.AnswerOption
                                       : Shuffle(surveyQuestionAndAnswerOptions.AnswerOption);

            double dcl = Convert.ToDouble(radioButtonList.RepeatColumns);
            double dcl1 = Convert.ToDouble(lstAnswerOptions.Count);

            int rowcnt = Convert.ToInt32(Math.Round(dcl1 / dcl));


            int tdwidth = 100 / radioButtonList.RepeatColumns;


            int stcount = 0;
            for (int ir = 0; ir < rowcnt; ir++)
            {
                HtmlTableRow htr = new HtmlTableRow();
               
                int colcount1 = stcount +  radioButtonList.RepeatColumns;

                int colcount = 0;

                if (colcount1 < lstAnswerOptions.Count)
                {
                    colcount = colcount1;
                }
                else
                {
                    colcount = lstAnswerOptions.Count;
                }

                for (int ic = stcount; ic < colcount; ic++)

                
                //foreach (var item in lstAnswerOptions)
                {
                    var radioButtonList1 = new RadioButtonList();
                    
                        HtmlTableCell htc = new HtmlTableCell();
                        htc.Width = tdwidth.ToString() + "%";
                        lstAnswerOption = new ListItem
                        {

                            Text = lstAnswerOptions[ic].ANSWER_OPTIONS,
                            Value = SurveyRespondentHelper.FormatControlId(prefix,
                                                                           surveyQuestionAndAnswerOptions.Question.
                                                                               QUESTION_TYPE_ID,
                                                                           lstAnswerOptions[ic].QUESTION_ID,
                                                                           lstAnswerOptions[ic].ANSWER_ID)
                        };

                        var response = GetResponseQuestion(answerIds, lstAnswerOptions[ic].ANSWER_ID);
                        lstAnswerOption.Selected = response != null;

                        
                        if (lstControl is RadioButtonList)
                            radioButtonList1.Items.Add(lstAnswerOption);

                        
                        htc.Controls.Add(radioButtonList1);

                        //  txtOther.Visible = surveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0;
                        if (response != null && OtherHasText == false)
                        {
                            txtOther.Text = response != null ? response.ANSWER_TEXT : string.Empty;
                            OtherHasText = true;
                        }
                        htr.Cells.Add(htc);
                
                }
                stcount = colcount;
                tblmoss.Rows.Add(htr);
            }   
                
            
            tblmoss.Visible = true;
                //foreach (var item in lstAnswerOptions)
                //{


                //    lstAnswerOption = new ListItem
                //    {

                //        Text = item.ANSWER_OPTIONS,
                //        Value = SurveyRespondentHelper.FormatControlId(prefix,
                //                                                       surveyQuestionAndAnswerOptions.Question.
                //                                                           QUESTION_TYPE_ID,
                //                                                       item.QUESTION_ID,
                //                                                       item.ANSWER_ID)
                //    };

                //    var response = GetResponseQuestion(answerIds, item.ANSWER_ID);
                //    lstAnswerOption.Selected = response != null;



                //    if (lstControl is RadioButtonList)
                //        lstControl.Width = 100;
                //    radioButtonList.Items.Add(lstAnswerOption);

                //    //  txtOther.Visible = surveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0;
                //    if (response != null && OtherHasText == false)
                //    {
                //        txtOther.Text = response != null ? response.ANSWER_TEXT : string.Empty;
                //        OtherHasText = true;
                //    }


                //}
            

            if (!isAdditionalItemRequired) return;
            lstControl.Items.Insert(0, "-- Select --");
            lstControl.Items[0].Value = @"0";
        }


        #endregion
    }
}
