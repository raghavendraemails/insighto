﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;
using System.Web.UI;

namespace App_Code.SurveyRespondentHelpers
{
    public class RespondentOtherTypeQuestions : BusinessServiceBase
    {
        #region Introduction question type

        /// <summary>
        /// Creates the introduction question.
        /// </summary>
        /// <param name="introText">The intro text.</param>
        public string CreateIntroductionQuestion(string introText)
        {
            return introText;
        }

        #endregion

        #region Image question type
        /// <summary>
        /// Creates the introduction question.
        /// </summary>
        /// <param name="imageUrl">The image URL.</param>
        /// <returns></returns>
        public string CreateImageQuestion(string imageUrl)
        {
            return imageUrl;
        }

        #endregion

        #region Video question type
        /// <summary>
        /// Creates the video question.
        /// </summary>
        /// <param name="videoUrl">The video URL.</param>
        /// <returns></returns>
        public string CreateVideoQuestion(string videoUrl)
        {
            var pathurl = string.Empty;
            if (!string.IsNullOrEmpty(videoUrl))
            {
                string[] videoParams = videoUrl.Split(new[] { "$--$" }, StringSplitOptions.RemoveEmptyEntries);
                if (videoParams.Length > 1)
                    pathurl = videoParams[1];
            }
            return pathurl;
        }

        /// <summary>
        /// Gets the type of the video dispaly.
        /// </summary>
        /// <param name="videoUrl">The video URL.</param>
        /// <returns></returns>
        public VideoTypes GetVideoDispalyType(string videoUrl)
        {
            if (!string.IsNullOrEmpty(videoUrl))
            {
                string[] videoParams = videoUrl.Split(new[] { "$--$" }, StringSplitOptions.RemoveEmptyEntries);
                return Utilities.ToEnum<VideoTypes>(ValidationHelper.GetInteger(videoParams[0], 0));
            }
            return default(VideoTypes);
        }

        /// <summary>
        /// Displays the video question.
        /// </summary>
        /// <param name="VideoPath">The video path.</param>
        /// <returns></returns>
        public HtmlGenericControl DisplayVideoQuestion(string VideoPath, string playerId, Page page = null)
        {
            var displayVideo = new HtmlGenericControl();
            var VideoUrl = page != null ? Utilities.GetAbsoluteUrl(ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateVideoQuestion(VideoPath), page) : 
                ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateVideoQuestion(VideoPath);
            var displayMode = ServiceFactory<RespondentOtherTypeQuestions>.Instance.GetVideoDispalyType(VideoPath);
            //if (displayMode == VideoTypes.Upload_Video)
            //{
            //    //displayVideo.InnerHtml = "<div id=\"divCustomVideo\" runat=\"server\" >"
            //    //                        + "<p id='" + playerId + "'>"
            //    //                        + " <a href='http://www.macromedia.com/go/getflashplayer'>Get the Flash Player</a> to"
            //    //                        + " see this Video.</p>"
            //    //                        + "<script type=\"text/javascript\">"
            //    //                        + "var s1 = new SWFObject(\"flvplayer.swf\", \"single\", \"250\", \"200\", \"7\");"
            //    //                        + "s1.addParam(\"allowfullscreen\", \"true\");"
            //    //                        + "s1.addVariable(\"file\",'" + VideoUrl + "');"
            //    //                        + "s1.addVariable('showdigits', 'false');"
            //    //                        + "s1.addVariable('bufferlength', '3');"
            //    //                        + "s1.addVariable('type', 'flv');"
            //    //                        + "s1.addVariable('usekeys', 'false');"
            //    //                        + "s1.addVariable('autostart', 'false');"
            //    //                        + "s1.write('" + playerId + "');"
            //    //                        + "</script>"
            //    //                        + "</div>";


              
               
            //       displayVideo.InnerHtml = VideoUrl;

                
             
            //}
            //else 
            if (displayMode == VideoTypes.Upload_YouTube_Video)
            {
                displayVideo.InnerHtml = VideoUrl;
            }
            return displayVideo;
        }

        #endregion

        #region Narration question type

        public string CreateNarrationQuestion(string narrationText)
        {
            return CreateQuestionHeader(narrationText);
        }
        #endregion

        #region Page header question type
        /// <summary>
        /// Creates the page header question.
        /// </summary>
        /// <param name="pHeaderText">The p header text.</param>
        /// <returns></returns>
        public string CreatePageHeaderQuestion(string pHeaderText)
        {
            return CreateQuestionHeader(pHeaderText);
        }
        #endregion

        #region Question header question type

        /// <summary>
        /// Creates the question header question.
        /// </summary>
        /// <param name="qHeaderText">The q header text.</param>
        /// <returns></returns>
        public string CreateQuestionHeaderQuestion(string qHeaderText)
        {
            return CreateQuestionHeader(qHeaderText);
        }
        #endregion

        #region Date time question type

        public void CreateDateTimeQuestion(HtmlTable tblDateTime, SurveyQuestionAndAnswerOptions question)
        {
            var trOption = new HtmlTableRow();
            var tdOption = new HtmlTableCell();
            var lblOption = new Label { ID = "lbl" + question.Question.QUESTION_ID, Text = @"Date" };
            tdOption.Controls.Add(lblOption);
            trOption.Controls.Add(tdOption);

            tdOption = new HtmlTableCell();
            var txtOption = new TextBox { ID = RespondentValidationsHelper.GetFormattedId("txt", question.Question.QUESTION_ID, question.Question.QUESTION_TYPE_ID), CssClass = "textBoxDateTime txtDate" };
            tdOption.Controls.Add(txtOption);
            trOption.Controls.Add(tdOption);
            tblDateTime.Rows.Add(trOption);

            //var option = surveyQuestionAndAnswerOptions.AnswerOption.Take(1).FirstOrDefault();
            //return RespondentMultipleQuestions.GetResponseQuestion(respondentAnswerOptions, option != null ? option.ANSWER_ID : 0);

        }
        #endregion

        #region Auxillary methods
        /// <summary>
        /// Creates the question header.
        /// </summary>
        /// <param name="headerText">The header text.</param>
        /// <returns></returns>
        private static string CreateQuestionHeader(string headerText)
        {
            return headerText;
        }
        #endregion
    }
}