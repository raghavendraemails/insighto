﻿using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;

namespace App_Code.SurveyRespondentHelpers
{
    /// <summary>
    /// Summary description for RespondentNumericQuestions
    /// </summary>
    public class RespondentNumericQuestions : BusinessServiceBase
    {
        /// <summary>
        /// Creates the ranking question.
        /// </summary>
        /// <param name="trRankSum">The tr rank sum.</param>
        /// <param name="source">The source.</param>
        /// <param name="enableValidations">if set to <c>true</c> [enable validations].</param>
        public void CreateRankingQuestion(HtmlTable trRankSum, SurveyQuestionAndAnswerOptions source, bool enableValidations = false)
        {
            foreach (var item in source.AnswerOption)
            {
                AddAnswerOptionControl(trRankSum, item, source.Question);
            }
        }

        /// <summary>
        /// Creates the constant sum question.
        /// </summary>
        /// <param name="trRankSum">The tr rank sum.</param>
        /// <param name="source">The source.</param>
        /// <param name="enableValidations">if set to <c>true</c> [enable validations].</param>
        public void CreateConstantSumQuestion(HtmlTable trRankSum, SurveyQuestionAndAnswerOptions source, bool enableValidations = false)
        {
            foreach (var item in source.AnswerOption)
            {
                AddAnswerOptionControl(trRankSum, item, source.Question);
            }
        }

        /// <summary>
        /// Adds the answer option control.
        /// </summary>
        /// <param name="trRankSum">The tr rank sum.</param>
        /// <param name="item">The item.</param>
        /// <param name="question">The question.</param>
        private static void AddAnswerOptionControl(HtmlTable trRankSum, osm_answeroptions item, osm_surveyquestion question)
        {
            var trOption = new HtmlTableRow();
            var tdOption = new HtmlTableCell();
            var lblOption = new Label { ID = "lbl" + item.ANSWER_ID, Text = item.ANSWER_OPTIONS };
            tdOption.Controls.Add(lblOption);
            trOption.Controls.Add(tdOption);

            tdOption = new HtmlTableCell();
            var txtOption = new TextBox { ID = RespondentValidationsHelper.GetFormattedId("txt", question.QUESTION_ID, question.QUESTION_TYPE_ID, item.ANSWER_ID), CssClass = "textBoxRanking", MaxLength=10 };
            tdOption.Controls.Add(txtOption);
            trOption.Controls.Add(tdOption);
            trRankSum.Rows.Add(trOption);
        }
    }
}