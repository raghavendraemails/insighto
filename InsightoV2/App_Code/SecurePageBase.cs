﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Threading;
using System.Globalization;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;


/// <summary>
/// Summary description for BasePage
/// </summary>
public class SecurePageBase :BasePage
{
    public SecurePageBase()
    {
    }

    public bool IsModal
    {
        get
        {
            return this.MasterPageFile.ToLower().Contains("model");
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (userDetails == null)
        {
            if (IsModal)
            {
                Response.Redirect(PathHelper.GetLogoutURL());
                return;
            }
            Response.Redirect(PathHelper.GetUserLoginPageURL());
        }
    }
    
}