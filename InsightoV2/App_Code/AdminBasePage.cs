﻿using System.Web.UI;

/// <summary>
/// Summary description for AdminBasePage
/// </summary>
public class AdminBasePage:Page
{
	public AdminBasePage()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public void CloseModelForSpecificTime(string pageToBeRefreshedURL, string timeValue)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "closeModelAfterFive", "closeModelAfterFive(true,'" + pageToBeRefreshedURL + "','" + timeValue + "');", true);
    }
}