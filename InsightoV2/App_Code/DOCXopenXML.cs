﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts.Web;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using sharp = iTextSharp.text;
using FeatureTypes;
using Insighto.Business.ValueObjects;
using System.Text;
using System.Linq;
using System.Threading;
using DevExpress.Web.ASPxClasses;
using System.Diagnostics;
using InfoSoftGlobal;
using System.Security.Cryptography;
using iTextSharp.text.html.simpleparser;
using System.Reflection;
using OfficeOpenXml;
using Office = Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.Runtime.InteropServices;
using Word = Microsoft.Office.Interop.Word;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Configuration;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;

/// <summary>

/// <summary>
/// Summary description for DOCXopenXML
/// </summary>
public class DOCXopenXML
{
	public DOCXopenXML()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void CreateFirstTable(string fileName, WordprocessingDocument doc, string surveyname)
    {
      
        Run run = new Run();
        DocumentFormat.OpenXml.Wordprocessing.Text text = new DocumentFormat.OpenXml.Wordprocessing.Text() { Text = "Survey Title : " + surveyname };
        run.Append(text);
        
        RunProperties runProp = new RunProperties(); // Create run properties.
        RunFonts runFont = new RunFonts();           // Create font
        runFont.Ascii = "Arial";                     // Specify font family

        DocumentFormat.OpenXml.Wordprocessing.FontSize size = new DocumentFormat.OpenXml.Wordprocessing.FontSize();
      //  DocumentFormat.OpenXml.Wordprocessing.Color color1 = new DocumentFormat.OpenXml.Wordprocessing.Color() { Val = "#FFFF00" };
        size.Val = new StringValue("60");  // 48 half-point font size
        runProp.Append(runFont);
      //  runProp.Append(color1);
        runProp.Append(size);
        run.PrependChild<RunProperties>(runProp);
        //////doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(run)));

        DocumentFormat.OpenXml.Wordprocessing.Table table = new DocumentFormat.OpenXml.Wordprocessing.Table();

        TableProperties props = new TableProperties(
          new TableBorders(
            new TopBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new BottomBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new LeftBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new RightBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideHorizontalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideVerticalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            }));
        table.AppendChild<TableProperties>(props);
        var tr = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
        var tc = new DocumentFormat.OpenXml.Wordprocessing.TableCell();
        tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(run));
        tc.Append(new TableCellProperties(
                      new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "9500" }));
        tr.Append(tc);
        table.Append(tr);

        // Append the table to the document.
        doc.MainDocumentPart.Document.Body.AppendChild(table);
    }

    public void CreatePollFirstTable(string fileName, WordprocessingDocument doc, string surveyname)
    {

        Run run = new Run();
        DocumentFormat.OpenXml.Wordprocessing.Text text = new DocumentFormat.OpenXml.Wordprocessing.Text() { Text = "Poll Title : " + surveyname };
        run.Append(text);

        RunProperties runProp = new RunProperties(); // Create run properties.
        RunFonts runFont = new RunFonts();           // Create font
        runFont.Ascii = "Arial";                     // Specify font family

        DocumentFormat.OpenXml.Wordprocessing.FontSize size = new DocumentFormat.OpenXml.Wordprocessing.FontSize();
        //  DocumentFormat.OpenXml.Wordprocessing.Color color1 = new DocumentFormat.OpenXml.Wordprocessing.Color() { Val = "#FFFF00" };
        size.Val = new StringValue("60");  // 48 half-point font size
        runProp.Append(runFont);
        //  runProp.Append(color1);
        runProp.Append(size);
        run.PrependChild<RunProperties>(runProp);
        //////doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(run)));

        DocumentFormat.OpenXml.Wordprocessing.Table table = new DocumentFormat.OpenXml.Wordprocessing.Table();

        TableProperties props = new TableProperties(
          new TableBorders(
            new TopBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new BottomBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new LeftBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new RightBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideHorizontalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideVerticalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            }));
        table.AppendChild<TableProperties>(props);
        var tr = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
        var tc = new DocumentFormat.OpenXml.Wordprocessing.TableCell();
        tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(run));
        tc.Append(new TableCellProperties(
                      new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "9500" }));
        tr.Append(tc);
        table.Append(tr);

        // Append the table to the document.
        doc.MainDocumentPart.Document.Body.AppendChild(table);
    }


    public void CreateTable(string fileName, WordprocessingDocument doc, string seqtype, DataRow[] drrows, string noofresps,string questionlabel)
{
    // Use the file name and path passed in as an argument 
    // to open an existing Word 2007 document.
    DocumentFormat.OpenXml.Wordprocessing.Table tableq = new DocumentFormat.OpenXml.Wordprocessing.Table();

    TableProperties propsq = new TableProperties(
      new TableBorders(
        new TopBorder
        {
            Val = new EnumValue<BorderValues>(BorderValues.Single),
            Size = 12
        },
        new BottomBorder
        {
            Val = new EnumValue<BorderValues>(BorderValues.Single),
            Size = 12
        },
        new LeftBorder
        {
            Val = new EnumValue<BorderValues>(BorderValues.Single),
            Size = 12
        },
        new RightBorder
        {
            Val = new EnumValue<BorderValues>(BorderValues.Single),
            Size = 12
        },
        new InsideHorizontalBorder
        {
            Val = new EnumValue<BorderValues>(BorderValues.Single),
            Size = 12
        },
        new InsideVerticalBorder
        {
            Val = new EnumValue<BorderValues>(BorderValues.Single),
            Size = 12
        }));
    tableq.AppendChild<TableProperties>(propsq);
    var trq = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
    var tcq = new DocumentFormat.OpenXml.Wordprocessing.TableCell();

    tcq.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
               new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "24" }),
               new DocumentFormat.OpenXml.Wordprocessing.Text(questionlabel))));
    tcq.Append(new TableCellProperties(
             new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "9600" }));
    trq.Append(tcq);
    tableq.Append(trq);
           doc.MainDocumentPart.Document.Body.AppendChild(tableq);

        //   Run run = new Run();
          // Spacing sp1 = new Spacing();
          // SpacingBetweenLines sp1 = new SpacingBetweenLines() { After = "0" };
          // run.Append(sp1);
           //DocumentFormat.OpenXml.Wordprocessing.Text text = new DocumentFormat.OpenXml.Wordprocessing.Text() { Text = "" };

           //RunProperties runProp = new RunProperties();
           //RunFonts runFont = new RunFonts();
           //runFont.Ascii = "Arial";
           //DocumentFormat.OpenXml.Wordprocessing.FontSize size = new DocumentFormat.OpenXml.Wordprocessing.FontSize();
           //size.Val = new StringValue("24");
           //runProp.Append(runFont);
           //runProp.Append(size);
           //run.PrependChild<RunProperties>(runProp);

           //run.Append(text);

        


    string[,] tableSources = null;

    if ((seqtype == "1") || (seqtype == "2") || (seqtype == "3") || (seqtype == "4") || (seqtype == "13"))
    {
        tableSources = new string[drrows.Length + 2, 3];

        tableSources[0, 0] = "Answeroptions";
        tableSources[0, 1] = "In%";
        tableSources[0, 2] = "In nos.";

        for (int irg1 = 0; irg1 < drrows.Length; irg1++)
        {
            tableSources[irg1 + 1, 0] = drrows[irg1][2].ToString();
            tableSources[irg1 + 1, 1] = drrows[irg1][6].ToString() + "%";
            tableSources[irg1 + 1, 2] = drrows[irg1][5].ToString();
        }

        tableSources[drrows.Length + 1, 0] = "# of Respondents";
        tableSources[drrows.Length + 1, 1] = " ";
        tableSources[drrows.Length + 1, 2] = noofresps;
    }

       
          DocumentFormat.OpenXml.Wordprocessing.Table table = new DocumentFormat.OpenXml.Wordprocessing.Table();
        
    TableProperties props = new TableProperties(
      new TableBorders(
        new TopBorder
        {
          Val = new EnumValue<BorderValues>(BorderValues.Single),
          Size = 12
        },
        new BottomBorder
        {
          Val = new EnumValue<BorderValues>(BorderValues.Single),
          Size = 12
        },
        new LeftBorder
        {
          Val = new EnumValue<BorderValues>(BorderValues.Single),
          Size = 12
        },
        new RightBorder
        {
          Val = new EnumValue<BorderValues>(BorderValues.Single),
          Size = 12
        },
        new InsideHorizontalBorder
        {
          Val = new EnumValue<BorderValues>(BorderValues.Single),
          Size = 12
        },
        new InsideVerticalBorder
        {
          Val = new EnumValue<BorderValues>(BorderValues.Single),
          Size = 12
        }));
    table.AppendChild<TableProperties>(props);

         
    for (var i = 0; i <= tableSources.GetUpperBound(0); i++)
    {
        var tr = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
          
        for (var j = 0; j <= tableSources.GetUpperBound(1); j++)
        {
            var tc = new DocumentFormat.OpenXml.Wordprocessing.TableCell();

            if (i == 0)
            {
                tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                    new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "20" }, 
                        new DocumentFormat.OpenXml.Wordprocessing.Color { Val = "#FFFFFF" },
                        new DocumentFormat.OpenXml.Wordprocessing.Bold()),
                        new DocumentFormat.OpenXml.Wordprocessing.Text(tableSources[i, j]))));
            }
            else
            {
                tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" }, 
                    new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "20" }),
                    new DocumentFormat.OpenXml.Wordprocessing.Text(tableSources[i, j]))));
            }

          
            // Assume you want columns that are automatically sized.
            if (j == 0)
            {
                tc.Append(new TableCellProperties(
                  new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4800" }));

            }
            else
            {
                tc.Append(new TableCellProperties(
                  new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
            }

            if (i == 0)
            {
                TableCellProperties trp = new TableCellProperties();
                trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = " #4F81BD" ,});
               
                tc.Append(trp);
            }
            else if (i == 1)
            {
                TableCellProperties trp = new TableCellProperties();
                trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#E9EDF4" });

                tc.Append(trp);
            }
            else
            {
                if ((i % 2) == 1)
                {
                    TableCellProperties trp = new TableCellProperties();
                    trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#E9EDF4" });
                    tc.Append(trp);
                }
                else
                {
                    TableCellProperties trp = new TableCellProperties();
                    trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#D0D8E8" });
                    tc.Append(trp);
                }
            }
            tr.Append(tc);
        }
        table.Append(tr);

    }

   // doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(run)));
    doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new Spacing())));
          // Append the table to the document.
        doc.MainDocumentPart.Document.Body.AppendChild(table);
    
}


    public void CreatePollTable(string fileName, WordprocessingDocument doc,  DataRow[] dtrows, string noofresps, string questionlabel)
    {
        // Use the file name and path passed in as an argument 
        // to open an existing Word 2007 document.
        DocumentFormat.OpenXml.Wordprocessing.Table tableq = new DocumentFormat.OpenXml.Wordprocessing.Table();

        TableProperties propsq = new TableProperties(
          new TableBorders(
            new TopBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new BottomBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new LeftBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new RightBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideHorizontalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideVerticalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            }));
        tableq.AppendChild<TableProperties>(propsq);
        var trq = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
        var tcq = new DocumentFormat.OpenXml.Wordprocessing.TableCell();

        tcq.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                   new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "24" }),
                   new DocumentFormat.OpenXml.Wordprocessing.Text(questionlabel))));
        tcq.Append(new TableCellProperties(
                 new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "9600" }));
        trq.Append(tcq);
        tableq.Append(trq);
        doc.MainDocumentPart.Document.Body.AppendChild(tableq);

       
        string[,] tableSources = null;

        
            tableSources = new string[dtrows.Length + 2, 3];

            tableSources[0, 0] = "Answeroptions";
            tableSources[0, 1] = "In%";
            tableSources[0, 2] = "In nos.";

            for (int irg1 = 0; irg1 < dtrows.Length; irg1++)
            {
                tableSources[irg1 + 1, 0] = dtrows[irg1][1].ToString();
                tableSources[irg1 + 1, 1] = dtrows[irg1][3].ToString() + "%";
                tableSources[irg1 + 1, 2] = dtrows[irg1][2].ToString();
            }

            tableSources[dtrows.Length + 1, 0] = "# of Respondents";
            tableSources[dtrows.Length + 1, 1] = " ";
            tableSources[dtrows.Length + 1, 2] = noofresps;
        


        DocumentFormat.OpenXml.Wordprocessing.Table table = new DocumentFormat.OpenXml.Wordprocessing.Table();

        TableProperties props = new TableProperties(
          new TableBorders(
            new TopBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new BottomBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new LeftBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new RightBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideHorizontalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideVerticalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            }));
        table.AppendChild<TableProperties>(props);


        for (var i = 0; i <= tableSources.GetUpperBound(0); i++)
        {
            var tr = new DocumentFormat.OpenXml.Wordprocessing.TableRow();

            for (var j = 0; j <= tableSources.GetUpperBound(1); j++)
            {
                var tc = new DocumentFormat.OpenXml.Wordprocessing.TableCell();

                if (i == 0)
                {
                    tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                        new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "20" },
                            new DocumentFormat.OpenXml.Wordprocessing.Color { Val = "#FFFFFF" },
                            new DocumentFormat.OpenXml.Wordprocessing.Bold()),
                            new DocumentFormat.OpenXml.Wordprocessing.Text(tableSources[i, j]))));
                }
                else
                {
                    tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                        new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "20" }),
                        new DocumentFormat.OpenXml.Wordprocessing.Text(tableSources[i, j]))));
                }


                // Assume you want columns that are automatically sized.
                if (j == 0)
                {
                    tc.Append(new TableCellProperties(
                      new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4800" }));

                }
                else
                {
                    tc.Append(new TableCellProperties(
                      new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                }

                if (i == 0)
                {
                    TableCellProperties trp = new TableCellProperties();
                    trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = " #4F81BD", });

                    tc.Append(trp);
                }
                else if (i == 1)
                {
                    TableCellProperties trp = new TableCellProperties();
                    trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#E9EDF4" });

                    tc.Append(trp);
                }
                else
                {
                    if ((i % 2) == 1)
                    {
                        TableCellProperties trp = new TableCellProperties();
                        trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#E9EDF4" });
                        tc.Append(trp);
                    }
                    else
                    {
                        TableCellProperties trp = new TableCellProperties();
                        trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#D0D8E8" });
                        tc.Append(trp);
                    }
                }
                tr.Append(tc);
            }
            table.Append(tr);

        }

        // doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(run)));
        doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new Spacing())));
        // Append the table to the document.
        doc.MainDocumentPart.Document.Body.AppendChild(table);

    }


    public void CreateTable(string fileName, WordprocessingDocument doc, string seqtype, DataRow[] drrows, string noofresps, DataTable mtxrownames,DataTable mtxcolnames,DataTable dt1011,string seqnum,string questionlabel)
    {
        // Use the file name and path passed in as an argument 
        // to open an existing Word 2007 document.
        DocumentFormat.OpenXml.Wordprocessing.Table tableq = new DocumentFormat.OpenXml.Wordprocessing.Table();

        TableProperties propsq = new TableProperties(
          new TableBorders(
            new TopBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new BottomBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new LeftBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new RightBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideHorizontalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideVerticalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            }));
        tableq.AppendChild<TableProperties>(propsq);
        var trq = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
        var tcq = new DocumentFormat.OpenXml.Wordprocessing.TableCell();

        tcq.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                   new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "24" }),
                   new DocumentFormat.OpenXml.Wordprocessing.Text(questionlabel))));
        tcq.Append(new TableCellProperties(
                 new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "9600" }));
        trq.Append(tcq);
        tableq.Append(trq);
        doc.MainDocumentPart.Document.Body.AppendChild(tableq);

        //Run run = new Run();
        //DocumentFormat.OpenXml.Wordprocessing.Text text = new DocumentFormat.OpenXml.Wordprocessing.Text() { Text = questionlabel };

        //RunProperties runProp = new RunProperties();
        //RunFonts runFont = new RunFonts();
        //runFont.Ascii = "Arial";
        //DocumentFormat.OpenXml.Wordprocessing.FontSize size = new DocumentFormat.OpenXml.Wordprocessing.FontSize();
        //size.Val = new StringValue("24");
        //runProp.Append(runFont);
        //runProp.Append(size);
        //run.PrependChild<RunProperties>(runProp);

        //run.Append(text);


        string[,] tableSources = null;

        if ((seqtype == "10") || (seqtype == "11"))
        {
            tableSources = new string[mtxrownames.Rows.Count * 2 + 2, mtxcolnames.Rows.Count + 1];

            

            tableSources[0, 0] = "";
            for (int irg = 0; irg < mtxcolnames.Rows.Count; irg++)
            {
                tableSources[0, irg + 1] = System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[irg][0].ToString());
            }


            int rowcnt = 0;
            for (int irg1 = 0; irg1 < mtxrownames.Rows.Count; irg1++)
            {

                DataRow[] dr1011f = dt1011.Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + seqnum + "'");

                tableSources[rowcnt + 1, 0] = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString());
                tableSources[rowcnt + 2, 0] = " ";

                for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                {

                    tableSources[rowcnt + 1, cgv + 1] = dr1011f[cgv][5].ToString();
                    tableSources[rowcnt + 2, cgv + 1] = dr1011f[cgv][6].ToString() + "%";

                }
                rowcnt = rowcnt + 2;

            }

            tableSources[mtxrownames.Rows.Count * 2 + 1, 0] = "# of Respondents";
            for (int irg1 = 0; irg1 < mtxcolnames.Rows.Count; irg1++)
            {
                tableSources[mtxrownames.Rows.Count * 2 + 1, irg1 + 1] = " ";
            }
            tableSources[mtxrownames.Rows.Count * 2 + 1, mtxcolnames.Rows.Count] = noofresps;
        }

        DocumentFormat.OpenXml.Wordprocessing.Table table = new DocumentFormat.OpenXml.Wordprocessing.Table();

        TableProperties props = new TableProperties(
          new TableBorders(
            new TopBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new BottomBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new LeftBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new RightBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideHorizontalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideVerticalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            }));
        table.AppendChild<TableProperties>(props);

        for (var i = 0; i <= tableSources.GetUpperBound(0); i++)
        {
            var tr = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
            
            for (var j = 0; j <= tableSources.GetUpperBound(1); j++)
            {
                var tc = new DocumentFormat.OpenXml.Wordprocessing.TableCell();


                if (i == 0)
                {
                    tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                        new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "20" },
                            new DocumentFormat.OpenXml.Wordprocessing.Color { Val = "#FFFFFF" },
                            new DocumentFormat.OpenXml.Wordprocessing.Bold()),
                            new DocumentFormat.OpenXml.Wordprocessing.Text(tableSources[i, j]))));
                }
                else
                {
                    tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                        new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "20" }),
                        new DocumentFormat.OpenXml.Wordprocessing.Text(tableSources[i, j]))));
                }

               // tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new Text(tableSources[i, j]))));

                if (j == 0)
                {
                    tc.Append(new TableCellProperties(
                      new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4800" }));
                }
                else
                {
                    int wdtcol = 4800 / tableSources.GetUpperBound(1);
                    tc.Append(new TableCellProperties(
                      new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = wdtcol.ToString() }));
                }
                if (i == 0)
                {
                    TableCellProperties trp = new TableCellProperties();
                    trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = " #4F81BD" });
                    tc.Append(trp);
                }
                else if (i == 1)
                {
                    TableCellProperties trp = new TableCellProperties();
                    trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#E9EDF4" });
                    tc.Append(trp);
                }
                else
                {
                    if ((i % 2) == 1)
                    {
                        TableCellProperties trp = new TableCellProperties();
                        trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#E9EDF4" });
                        tc.Append(trp);
                    }
                    else
                    {
                        TableCellProperties trp = new TableCellProperties();
                        trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#D0D8E8" });
                        tc.Append(trp);
                    }
                }
                tr.Append(tc);
            }
            table.Append(tr);

        }

     //   doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(run)));
        doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new Spacing())));
        // Append the table to the document.
        doc.MainDocumentPart.Document.Body.AppendChild(table);

    }

    public void CreateTable(string fileName, WordprocessingDocument doc, string seqtype, DataRow[] drrows, string noofresps, DataTable mtxrownames, DataTable mtxcolnames, DataTable mtxsubcolnames, DataTable dt12, string seqnum, string questionlabel)
    {
        DocumentFormat.OpenXml.Wordprocessing.Table tableq = new DocumentFormat.OpenXml.Wordprocessing.Table();

        TableProperties propsq = new TableProperties(
          new TableBorders(
            new TopBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new BottomBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new LeftBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new RightBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideHorizontalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideVerticalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            }));
        tableq.AppendChild<TableProperties>(propsq);
        var trq = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
        var tcq = new DocumentFormat.OpenXml.Wordprocessing.TableCell();

        tcq.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                   new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "24" }),
                   new DocumentFormat.OpenXml.Wordprocessing.Text(questionlabel))));
        tcq.Append(new TableCellProperties(
                 new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "9600" }));
        trq.Append(tcq);
        tableq.Append(trq);
        doc.MainDocumentPart.Document.Body.AppendChild(tableq);


        //Run run = new Run();
        //DocumentFormat.OpenXml.Wordprocessing.Text text = new DocumentFormat.OpenXml.Wordprocessing.Text() { Text = questionlabel };
        //RunProperties runProp = new RunProperties();
        //RunFonts runFont = new RunFonts();
        //runFont.Ascii = "Arial";
        //DocumentFormat.OpenXml.Wordprocessing.FontSize size = new DocumentFormat.OpenXml.Wordprocessing.FontSize();
        //size.Val = new StringValue("24");
        //runProp.Append(runFont);
        //runProp.Append(size);
        //run.PrependChild<RunProperties>(runProp);
        //run.Append(text);


        string[,] tableSources = null;

        if ((seqtype == "12"))
        {
            tableSources = new string[mtxrownames.Rows.Count * 2 + 2, mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count + 1];

            tableSources[0, 0] = "";
            int cval = 0;
            for (int irg12 = 0; irg12 < mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count; irg12++)
            {
                if ((irg12 == 0) || (irg12 == mtxsubcolnames.Rows.Count))
                {
                    tableSources[0, irg12 + 1] = System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[cval][0].ToString());
                    cval = cval + 1;
                }
                else
                {
                    tableSources[0, irg12 + 1] = "";
                }

            }
            tableSources[1, 0] = "";
            for (int irg112 = 0; irg112 < mtxrownames.Rows.Count; irg112++)
            {

                DataRow[] dr12sub = dt12.Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg112][0].ToString()).Replace("'", "''") + "' and seq_num = '" + seqnum + "'");
                for (int cgv12 = 0; cgv12 < dr12sub.Length; cgv12++)
                {
                    tableSources[1, cgv12 + 1] = System.Web.HttpUtility.HtmlDecode(dr12sub[cgv12][4].ToString());
                }

            }

            int rowcnt = 1;
            for (int irg12data = 0; irg12data < mtxrownames.Rows.Count; irg12data++)
            {

                DataRow[] dr12f = dt12.Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg12data][0].ToString()).Replace("'", "''") + "' and seq_num = '" + seqnum + "'");

                tableSources[rowcnt + 1, 0] = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg12data][0].ToString());
                tableSources[rowcnt + 2, 0] = " ";

                for (int cgv12 = 0; cgv12 < dr12f.Length; cgv12++)
                {
                    tableSources[rowcnt + 1, cgv12 + 1] = dr12f[cgv12][5].ToString();
                    tableSources[rowcnt + 2, cgv12 + 1] = dr12f[cgv12][6].ToString() + "%";

                }
                rowcnt = rowcnt + 2;

            }


            tableSources[mtxrownames.Rows.Count * 2 + 1, 0] = "# of Respondents";
            for (int irg1 = 0; irg1 < mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count; irg1++)
            {
                tableSources[mtxrownames.Rows.Count * 2 + 1, irg1 + 1] = " ";
            }
            tableSources[mtxrownames.Rows.Count * 2 + 1, mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count] = noofresps;
        }

        DocumentFormat.OpenXml.Wordprocessing.Table table = new DocumentFormat.OpenXml.Wordprocessing.Table();

        TableProperties props = new TableProperties(
          new TableBorders(
            new TopBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new BottomBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new LeftBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new RightBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideHorizontalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideVerticalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            }));
        table.AppendChild<TableProperties>(props);

        for (var i = 0; i <= tableSources.GetUpperBound(0); i++)
        {
            var tr = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
            for (var j = 0; j <= tableSources.GetUpperBound(1); j++)
            {
                var tc = new DocumentFormat.OpenXml.Wordprocessing.TableCell();

                if (i == 0)
                {
                    tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                        new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "20" },
                            new DocumentFormat.OpenXml.Wordprocessing.Color { Val = "#FFFFFF" },
                            new DocumentFormat.OpenXml.Wordprocessing.Bold()),
                            new DocumentFormat.OpenXml.Wordprocessing.Text(tableSources[i, j]))));
                }
                else
                {
                    tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                        new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "20" }),
                        new DocumentFormat.OpenXml.Wordprocessing.Text(tableSources[i, j]))));
                }

               // tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new Text(tableSources[i, j]))));
                // Assume you want columns that are automatically sized.
                if (j == 0)
                {
                    tc.Append(new TableCellProperties(
                      new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4800" }));
                }
                else
                {
                    int wdtcol = 4800 / tableSources.GetUpperBound(1);
                    tc.Append(new TableCellProperties(
                      new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = wdtcol.ToString() }));
                }
                if (i == 0)
                {
                    TableCellProperties trp = new TableCellProperties();
                    trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = " #4F81BD" });
                    tc.Append(trp);
                }
                else if (i == 1)
                {
                    TableCellProperties trp = new TableCellProperties();
                    trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#E9EDF4" });
                    tc.Append(trp);
                }
                else
                {
                    if ((i % 2) == 1)
                    {
                        TableCellProperties trp = new TableCellProperties();
                        trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#E9EDF4" });
                        tc.Append(trp);
                    }
                    else
                    {
                        TableCellProperties trp = new TableCellProperties();
                        trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#D0D8E8" });
                        tc.Append(trp);
                    }
                }
                tr.Append(tc);
            }
            table.Append(tr);

        }

     //   doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(run)));
        doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new Spacing())));

        // Append the table to the document.
        doc.MainDocumentPart.Document.Body.AppendChild(table);


    }


    public void CreateTable15(string fileName, WordprocessingDocument doc, string seqtype, DataRow[] drrows, string noofresps, DataTable mtxrownames, DataTable mtxcolnames, DataTable dt15, string seqnum, string questionlabel)
    {
        // Use the file name and path passed in as an argument 
        // to open an existing Word 2007 document.
        DocumentFormat.OpenXml.Wordprocessing.Table tableq = new DocumentFormat.OpenXml.Wordprocessing.Table();

        TableProperties propsq = new TableProperties(
          new TableBorders(
            new TopBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new BottomBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new LeftBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new RightBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideHorizontalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideVerticalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            }));
        tableq.AppendChild<TableProperties>(propsq);
        var trq = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
        var tcq = new DocumentFormat.OpenXml.Wordprocessing.TableCell();

        tcq.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                   new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "24" }),
                   new DocumentFormat.OpenXml.Wordprocessing.Text(questionlabel))));
        tcq.Append(new TableCellProperties(
                 new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "9600" }));
        trq.Append(tcq);
        tableq.Append(trq);
        doc.MainDocumentPart.Document.Body.AppendChild(tableq);

        //Run run = new Run();
        //DocumentFormat.OpenXml.Wordprocessing.Text text = new DocumentFormat.OpenXml.Wordprocessing.Text() { Text = questionlabel };
        //RunProperties runProp = new RunProperties();
        //RunFonts runFont = new RunFonts();
        //runFont.Ascii = "Arial";
        //DocumentFormat.OpenXml.Wordprocessing.FontSize size = new DocumentFormat.OpenXml.Wordprocessing.FontSize();
        //size.Val = new StringValue("24");
        //runProp.Append(runFont);
        //runProp.Append(size);
        //run.PrependChild<RunProperties>(runProp);
        //run.Append(text);


        string[,] tableSources = null;

        if (seqtype == "15")
        {
            tableSources = new string[mtxrownames.Rows.Count * 2 + 2, mtxcolnames.Rows.Count + 1];


            tableSources[0, 0] = "";
            for (int irg = 0; irg < mtxcolnames.Rows.Count; irg++)
            {
                tableSources[0, irg + 1] = System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[irg][0].ToString());
            }



            int rowcnt = 0;
            for (int irg1 = 0; irg1 < mtxrownames.Rows.Count; irg1++)
            {

                DataRow[] dr1011f = dt15.Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + seqnum + "'");


                tableSources[rowcnt + 1, 0] = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString());
                tableSources[rowcnt + 2, 0] = " ";



                for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                {

                    tableSources[rowcnt + 1, cgv + 1] = dr1011f[cgv][5].ToString();
                    tableSources[rowcnt + 2, cgv + 1] = dr1011f[cgv][6].ToString() + "%";

                }
                rowcnt = rowcnt + 2;

            }


            tableSources[mtxrownames.Rows.Count * 2 + 1, 0] = "# of Respondents";
            for (int irg1 = 0; irg1 < mtxcolnames.Rows.Count; irg1++)
            {
                tableSources[mtxrownames.Rows.Count * 2 + 1, irg1 + 1] = " ";
            }
            tableSources[mtxrownames.Rows.Count * 2 + 1, mtxcolnames.Rows.Count] = noofresps;
        }


        DocumentFormat.OpenXml.Wordprocessing.Table table = new DocumentFormat.OpenXml.Wordprocessing.Table();

        TableProperties props = new TableProperties(
          new TableBorders(
            new TopBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new BottomBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new LeftBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new RightBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideHorizontalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideVerticalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            }));
        table.AppendChild<TableProperties>(props);

        for (var i = 0; i <= tableSources.GetUpperBound(0); i++)
        {
            var tr = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
            for (var j = 0; j <= tableSources.GetUpperBound(1); j++)
            {
                var tc = new DocumentFormat.OpenXml.Wordprocessing.TableCell();
               // tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new Text(tableSources[i, j]))));
                if (i == 0)
                {
                    tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                        new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "20" },
                            new DocumentFormat.OpenXml.Wordprocessing.Color { Val = "#FFFFFF" },
                            new DocumentFormat.OpenXml.Wordprocessing.Bold()),
                            new DocumentFormat.OpenXml.Wordprocessing.Text(tableSources[i, j]))));
                }
                else
                {
                    tc.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                        new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "20" }),
                        new DocumentFormat.OpenXml.Wordprocessing.Text(tableSources[i, j]))));
                }
                // Assume you want columns that are automatically sized.
                if (j == 0)
                {
                    tc.Append(new TableCellProperties(
                      new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4800" }));
                }
                else
                {
                    int wdtcol = 4800 / tableSources.GetUpperBound(1);
                    tc.Append(new TableCellProperties(
                      new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = wdtcol.ToString() }));
                }
                if (i == 0)
                {
                    TableCellProperties trp = new TableCellProperties();
                    trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = " #4F81BD" });
                    tc.Append(trp);
                }
                else if (i == 1)
                {
                    TableCellProperties trp = new TableCellProperties();
                    trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#E9EDF4" });
                    tc.Append(trp);
                }
                else
                {
                    if ((i % 2) == 1)
                    {
                        TableCellProperties trp = new TableCellProperties();
                        trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#E9EDF4" });
                        tc.Append(trp);
                    }
                    else
                    {
                        TableCellProperties trp = new TableCellProperties();
                        trp.Append(new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = "#D0D8E8" });
                        tc.Append(trp);
                    }
                }
                tr.Append(tc);
            }
            table.Append(tr);

        }

       // doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(run)));
        doc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new Spacing())));

        // Append the table to the document.
        doc.MainDocumentPart.Document.Body.AppendChild(table);

    }


    public void InsertAPicture(WordprocessingDocument wordprocessingDocument, string fileName,string questionlabel)
    {
       
            MainDocumentPart mainPart = wordprocessingDocument.MainDocumentPart;

            ImagePart imagePart = mainPart.AddImagePart(ImagePartType.Jpeg);
       
            using (FileStream stream = new FileStream(fileName, FileMode.Open))
            {
                imagePart.FeedData(stream);

            }

            AddImageToBody(wordprocessingDocument, mainPart.GetIdOfPart(imagePart), questionlabel);
        
    }

    private static void AddImageToBody(WordprocessingDocument wordDoc, string relationshipId,string questionlabel)
    {
        //Run run = new Run();
        //DocumentFormat.OpenXml.Wordprocessing.Text text = new DocumentFormat.OpenXml.Wordprocessing.Text() { Text = questionlabel };
        //RunProperties runProp = new RunProperties();
        //RunFonts runFont = new RunFonts();
        //runFont.Ascii = "Arial";
        //DocumentFormat.OpenXml.Wordprocessing.FontSize size = new DocumentFormat.OpenXml.Wordprocessing.FontSize();
        //size.Val = new StringValue("24");
        //runProp.Append(runFont);
        //runProp.Append(size);
        //run.PrependChild<RunProperties>(runProp);
        //run.Append(text);


        DocumentFormat.OpenXml.Wordprocessing.Table tableq = new DocumentFormat.OpenXml.Wordprocessing.Table();

        TableProperties propsq = new TableProperties(
          new TableBorders(
            new TopBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new BottomBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new LeftBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new RightBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideHorizontalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            },
            new InsideVerticalBorder
            {
                Val = new EnumValue<BorderValues>(BorderValues.Single),
                Size = 12
            }));
        tableq.AppendChild<TableProperties>(propsq);
        var trq = new DocumentFormat.OpenXml.Wordprocessing.TableRow();
        var tcq = new DocumentFormat.OpenXml.Wordprocessing.TableCell();

        tcq.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new RunFonts { Ascii = "Arial" },
                   new DocumentFormat.OpenXml.Wordprocessing.RunProperties(new DocumentFormat.OpenXml.Wordprocessing.FontSize { Val = "24" }),
                   new DocumentFormat.OpenXml.Wordprocessing.Text(questionlabel))));
        tcq.Append(new TableCellProperties(
                 new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "9600" }));
        trq.Append(tcq);
        tableq.Append(trq);
        wordDoc.MainDocumentPart.Document.Body.AppendChild(tableq);


        // Define the reference of the image.
        var element =
             new Drawing(
                 new DW.Inline(
                     new DW.Extent() { Cx = 5990000L, Cy = 3792000L },
                     new DW.EffectExtent()
                     {
                         LeftEdge = 0L,
                         TopEdge = 0L,
                         RightEdge = 0L,
                         BottomEdge = 0L
                     },
                     new DW.DocProperties()
                     {
                         Id = (UInt32Value)1U,
                         Name = "Picture 1"
                     },
                     new DW.NonVisualGraphicFrameDrawingProperties(
                         new A.GraphicFrameLocks() { NoChangeAspect = true }),
                     new A.Graphic(
                         new A.GraphicData(
                             new PIC.Picture(
                                 new PIC.NonVisualPictureProperties(
                                     new PIC.NonVisualDrawingProperties()
                                     {
                                         Id = (UInt32Value)1U,
                                         Name = "New Bitmap Image.jpg"
                                     },
                                     new PIC.NonVisualPictureDrawingProperties()),
                                 new PIC.BlipFill(
                                     new A.Blip(
                                         new A.BlipExtensionList(
                                             new A.BlipExtension()
                                             {
                                                 Uri =
                                                   "{28A0092B-C50C-407E-A947-70E740481C1C}"
                                             })
                                     )
                                     {
                                         Embed = relationshipId,
                                         CompressionState =
                                         A.BlipCompressionValues.Print
                                     },
                                     new A.Stretch(
                                         new A.FillRectangle())),
                                 new PIC.ShapeProperties(
                                     new A.Transform2D(
                                         new A.Offset() { X = 0L, Y = 0L },
                                         new A.Extents() { Cx = 5990000L, Cy = 3792000L }),
                                     new A.PresetGeometry(
                                         new A.AdjustValueList()
                                     ) { Preset = A.ShapeTypeValues.Rectangle }))
                         ) { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                 )
                 {
                     DistanceFromTop = (UInt32Value)0U,
                     DistanceFromBottom = (UInt32Value)0U,
                     DistanceFromLeft = (UInt32Value)0U,
                     DistanceFromRight = (UInt32Value)0U,
                     EditId = "50D07946"
                 });


       // wordDoc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(run)));

        // Append the reference to body, the element should be in a Run.
        wordDoc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(element)));

  

               
    }
    
}