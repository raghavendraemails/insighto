﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;
using Insighto.Business.Helpers;
using System.Collections;
using Insighto.Data;

/// <summary>
/// Summary description for SurveyAnswerControlBase
/// </summary>
public class SurveyAnswerControlBase : UserControlBase
{
	public SurveyAnswerControlBase()
	{
	}

    public int SurveyFlag
    {
        get
        {
            var key = QueryStringHelper.GetString("key");
            if (String.IsNullOrEmpty(key))
                return 0;

            Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
            if (ht != null && ht.Contains("surveyFlag"))
            {
                return ValidationHelper.GetInteger(ht["surveyFlag"], 0);
            }
            return 0;
        }

    }

    public QuestionType QuestionType
    {
        get
        {
            int questID = 0;

            Hashtable ht1 = new Hashtable();
            ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
            if (ht1.Contains("QuestionType"))
            {
                 questID = ValidationHelper.GetInteger(ht1["QuestionType"].ToString(), 0);
           
            }

            return GenericHelper.ToEnum<QuestionType>(questID);
        }
    }

    public SurveyPageBase SurveyPageBase
    {
        get
        {
            return Page as SurveyPageBase;
        }
    }
    
    public int SurveyID
    {
        
        get
        {
            int surveyId = 0;
            var key = QueryStringHelper.GetString("key");
            if (String.IsNullOrEmpty(key))
                return 0;

            Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
            if (ht != null)
            {
                if (ht.Contains("SurveyId"))
                {
                    surveyId = ValidationHelper.GetInteger(ht["SurveyId"].ToString(), 0);
                }
               
            }

            return surveyId;
        }
    }

    public virtual List<string> Value
    {
        get
        {
            throw new NotImplementedException("SurveyAnswerControlBase.Value not implemented");
        }
        set
        {
            throw new NotImplementedException("SurveyAnswerControlBase.Value not implemented");
        }
    }

    public virtual List<osm_answeroptions> AnswerOptions
    {
        get
        {
            throw new NotImplementedException("SurveyAnswerControlBase.AnswerOptions not implemented");
        }
        set
        {
            throw new NotImplementedException("SurveyAnswerControlBase.AnswerOptions not implemented");
        }
    }

    public virtual void Save()
    {
        throw new NotImplementedException("SurveyAnswerControlBase.Save not implemented");
    }
}