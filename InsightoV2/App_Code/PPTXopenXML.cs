﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts.Web;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using sharp = iTextSharp.text;
using FeatureTypes;
using Insighto.Business.ValueObjects;
using System.Text;
using System.Linq;
using System.Threading;
using DevExpress.Web.ASPxClasses;
using System.Diagnostics;
using InfoSoftGlobal;
using System.Security.Cryptography;
using iTextSharp.text.html.simpleparser;
using System.Reflection;
using OfficeOpenXml;
using Office = Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.Runtime.InteropServices;
using Word = Microsoft.Office.Interop.Word;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Presentation;

using P = DocumentFormat.OpenXml.Presentation;
using D = DocumentFormat.OpenXml.Drawing;
using A = DocumentFormat.OpenXml.Drawing;
using Drawing = DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Drawing;
using P14 = DocumentFormat.OpenXml.Office2010.PowerPoint;
using DocumentFormat.OpenXml.Office2010.Drawing;
using DocumentFormat.OpenXml.Packaging.Extensions;
using System.Configuration;
/// <summary>
/// Summary description for PPTXopenXML
/// </summary>
public class PPTXopenXML
{
   
   public PresentationDocument presentationDocument1;
   public int position1;
   public string slideTitle1;
   public int Questionseq;
   public string QuestionType;
   public DataRow[] drQT1234;
    public string QuestionID;
    public int rowcnt;
  public string SAVE_PATH;
	public PPTXopenXML()
	{
		//
		// TODO: Add constructor logic here
		//
	}

      
    public void InsertNewfirstSlide(PresentationDocument presentationDocument, int position, string slideTitle,string surveyname)
    {

        if (presentationDocument == null)
        {
            throw new ArgumentNullException("presentationDocument");
        }

        if (slideTitle == null)
        {
            throw new ArgumentNullException("slideTitle");
        }

        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Verify that the presentation is not empty.
        if (presentationPart == null)
        {
            throw new InvalidOperationException("The presentation document is empty.");
        }

        Slide slide = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");
        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");

        DocumentFormat.OpenXml.Presentation.Shape shape = slide.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));

        shape.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
                  new Drawing.ListStyle(),
                  new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = "Survey Title : " + surveyname })));

        
     //   CreateTableInfirstSlide(presentationDocument, slide.SlidePart,surveyname);

     //   slide.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape);
        slide.Save();
        presentationDocument.PresentationPart.Presentation.Save();

    }

    public void InsertNewPollfirstSlide(PresentationDocument presentationDocument, int position, string slideTitle, string surveyname)
    {

        if (presentationDocument == null)
        {
            throw new ArgumentNullException("presentationDocument");
        }

        if (slideTitle == null)
        {
            throw new ArgumentNullException("slideTitle");
        }

        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Verify that the presentation is not empty.
        if (presentationPart == null)
        {
            throw new InvalidOperationException("The presentation document is empty.");
        }

        Slide slide = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");
        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");

        DocumentFormat.OpenXml.Presentation.Shape shape = slide.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));

        shape.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
                  new Drawing.ListStyle(),
                  new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = "Poll Title : " + surveyname })));


        //   CreateTableInfirstSlide(presentationDocument, slide.SlidePart,surveyname);

        //   slide.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape);
        slide.Save();
        presentationDocument.PresentationPart.Presentation.Save();

    }

    // Insert the specified slide into the presentation at the specified position.
    public void InsertNewSlide(PresentationDocument presentationDocument, int position, string slideTitle, int iseq, string seqtype, DataRow[] drrows,string questionid,string noofresps,string questionlabel)
    {

        if (presentationDocument == null)
        {
            throw new ArgumentNullException("presentationDocument");
        }

        if (slideTitle == null)
        {
            throw new ArgumentNullException("slideTitle");
        }

        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Verify that the presentation is not empty.
        if (presentationPart == null)
        {
            throw new InvalidOperationException("The presentation document is empty.");
        }


       
        Slide slide = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");
        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");

        DocumentFormat.OpenXml.Presentation.Shape shape = slide.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));

       
        CreateTableInLastSlide(presentationDocument, slide.SlidePart, iseq, seqtype, drrows, questionid, noofresps);

        shape.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
                  new Drawing.ListStyle(),
                  new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = questionlabel })));

      //  slide.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape);
        slide.Save();
        presentationDocument.PresentationPart.Presentation.Save();

        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();

        Slide slide1 = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");


        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");
        DocumentFormat.OpenXml.Presentation.Shape shape1 = slide1.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));
        //  Picture pic = slide.AddPicture(shape, @"D:\InsightoDevCode\InsightoV2\images\ExportImages\pptimg43428.png");

        
        shape1.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
                new Drawing.ListStyle(),
                new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = questionlabel })));


     
        DocumentFormat.OpenXml.Presentation.Picture pic = PowerpointExtensions.AddPicture(slide1, shape1, SAVE_PATH + "pptimg" + questionid + ".png");
      

      //  slide1.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape1);
        slide1.Save();
        presentationDocument.PresentationPart.Presentation.Save();

    }

    public void InsertNewPollSlide(PresentationDocument presentationDocument, int position, string slideTitle, DataRow[] dtpoll, string pollid, string noofresps, string questionlabel)
    {

        if (presentationDocument == null)
        {
            throw new ArgumentNullException("presentationDocument");
        }

        if (slideTitle == null)
        {
            throw new ArgumentNullException("slideTitle");
        }

        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Verify that the presentation is not empty.
        if (presentationPart == null)
        {
            throw new InvalidOperationException("The presentation document is empty.");
        }



        Slide slide = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");
        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");

        DocumentFormat.OpenXml.Presentation.Shape shape = slide.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));


        CreatePollTableInLastSlide(presentationDocument, slide.SlidePart, dtpoll, pollid, noofresps);

        shape.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
                  new Drawing.ListStyle(),
                  new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = questionlabel })));

        //  slide.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape);
        slide.Save();
        presentationDocument.PresentationPart.Presentation.Save();

        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();

        Slide slide1 = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");


        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");
        DocumentFormat.OpenXml.Presentation.Shape shape1 = slide1.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));
        //  Picture pic = slide.AddPicture(shape, @"D:\InsightoDevCode\InsightoV2\images\ExportImages\pptimg43428.png");


        shape1.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
                new Drawing.ListStyle(),
                new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = questionlabel })));



        DocumentFormat.OpenXml.Presentation.Picture pic = PowerpointExtensions.AddPicture(slide1, shape1, SAVE_PATH + "pptimg" + pollid + ".jpg");


        //  slide1.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape1);
        slide1.Save();
        presentationDocument.PresentationPart.Presentation.Save();

    }

    public void InsertNewSlide(PresentationDocument presentationDocument, int position, string slideTitle, int iseq, string seqtype, DataRow[] drrows, string questionid, string noofresps,DataTable mtxrownames,DataTable mtxcolnames,DataTable dt1011,string seqnum,string questionlabel)
    {

        if (presentationDocument == null)
        {
            throw new ArgumentNullException("presentationDocument");
        }

        if (slideTitle == null)
        {
            throw new ArgumentNullException("slideTitle");
        }

        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Verify that the presentation is not empty.
        if (presentationPart == null)
        {
            throw new InvalidOperationException("The presentation document is empty.");
        }
               

        Slide slide = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");
        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");

        DocumentFormat.OpenXml.Presentation.Shape shape = slide.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));

        shape.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
                 new Drawing.ListStyle(),
                 new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = questionlabel })));

        CreateTableInLastSlide(presentationDocument, slide.SlidePart, iseq, seqtype, drrows, questionid, noofresps,mtxrownames,mtxcolnames,dt1011,seqnum);

       // slide.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape);
        slide.Save();
        presentationDocument.PresentationPart.Presentation.Save();

        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();

        Slide slide1 = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");
        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");
        DocumentFormat.OpenXml.Presentation.Shape shape1 = slide1.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));
        //  Picture pic = slide.AddPicture(shape, @"D:\InsightoDevCode\InsightoV2\images\ExportImages\pptimg43428.png");

        shape1.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
               new Drawing.ListStyle(),
               new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = questionlabel })));


        DocumentFormat.OpenXml.Presentation.Picture pic = PowerpointExtensions.AddPicture(slide1, shape1, SAVE_PATH +"pptimg" + questionid + ".png");
       // slide1.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape1);
        slide1.Save();
        presentationDocument.PresentationPart.Presentation.Save();

    }

    public void InsertNewSlide(PresentationDocument presentationDocument, int position, string slideTitle, int iseq, string seqtype, DataRow[] drrows, string questionid, string noofresps, DataTable mtxrownames, DataTable mtxcolnames, DataTable mtxsubcolnames,DataTable dt12, string seqnum,string questionlabel)
    {

        if (presentationDocument == null)
        {
            throw new ArgumentNullException("presentationDocument");
        }

        if (slideTitle == null)
        {
            throw new ArgumentNullException("slideTitle");
        }

        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Verify that the presentation is not empty.
        if (presentationPart == null)
        {
            throw new InvalidOperationException("The presentation document is empty.");
        }


        Slide slide = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");
        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");

        DocumentFormat.OpenXml.Presentation.Shape shape = slide.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));

        shape.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
                 new Drawing.ListStyle(),
                 new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = questionlabel })));

        CreateTableInLastSlide(presentationDocument, slide.SlidePart, iseq, seqtype, drrows, questionid, noofresps, mtxrownames, mtxcolnames, mtxsubcolnames, dt12, seqnum);

     //   slide.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape);
        slide.Save();
        presentationDocument.PresentationPart.Presentation.Save();

        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();

        Slide slide1 = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");
        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");
        DocumentFormat.OpenXml.Presentation.Shape shape1 = slide1.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));

        shape1.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
               new Drawing.ListStyle(),
               new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = questionlabel })));

        //  Picture pic = slide.AddPicture(shape, @"D:\InsightoDevCode\InsightoV2\images\ExportImages\pptimg43428.png");
        DocumentFormat.OpenXml.Presentation.Picture pic = PowerpointExtensions.AddPicture(slide1, shape1, SAVE_PATH + "pptimg" + questionid + ".png");
       // slide1.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape1);
        slide1.Save();
        presentationDocument.PresentationPart.Presentation.Save();

    }

    public void InsertNewSlide15(PresentationDocument presentationDocument, int position, string slideTitle, int iseq, string seqtype, DataRow[] drrows, string questionid, string noofresps, DataTable mtxrownames, DataTable mtxcolnames, DataTable dt15, string seqnum,string questionlabel)
    {

        if (presentationDocument == null)
        {
            throw new ArgumentNullException("presentationDocument");
        }

        if (slideTitle == null)
        {
            throw new ArgumentNullException("slideTitle");
        }

        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Verify that the presentation is not empty.
        if (presentationPart == null)
        {
            throw new InvalidOperationException("The presentation document is empty.");
        }


        Slide slide = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");
        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");

        DocumentFormat.OpenXml.Presentation.Shape shape = slide.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));

        shape.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
                 new Drawing.ListStyle(),
                 new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = questionlabel })));

        CreateTableInLastSlide15(presentationDocument, slide.SlidePart, iseq, seqtype, drrows, questionid, noofresps, mtxrownames, mtxcolnames, dt15, seqnum);

      //  slide.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape);
        slide.Save();
        presentationDocument.PresentationPart.Presentation.Save();


        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();

        Slide slide1 = PowerpointExtensions.InsertSlide(presentationDocument.PresentationPart, "Title and Content");
        //Slide slide = presentationDocument.PresentationPart.InsertSlide("Title and Content");
        DocumentFormat.OpenXml.Presentation.Shape shape1 = slide1.CommonSlideData.ShapeTree.Elements<DocumentFormat.OpenXml.Presentation.Shape>().FirstOrDefault(
            sh => sh.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value.ToLower().Equals("Content Placeholder 2".ToLower()));

        shape1.TextBody = new DocumentFormat.OpenXml.Presentation.TextBody(new Drawing.BodyProperties(),
               new Drawing.ListStyle(),
               new Drawing.Paragraph(new Drawing.Run(new Drawing.Text() { Text = questionlabel })));

        //  Picture pic = slide.AddPicture(shape, @"D:\InsightoDevCode\InsightoV2\images\ExportImages\pptimg43428.png");
        DocumentFormat.OpenXml.Presentation.Picture pic = PowerpointExtensions.AddPicture(slide1, shape1,SAVE_PATH +  "pptimg" + questionid + ".png");
       // slide1.CommonSlideData.ShapeTree.RemoveChild<DocumentFormat.OpenXml.Presentation.Shape>(shape1);
        slide1.Save();
        presentationDocument.PresentationPart.Presentation.Save();

    }


    // Count the slides in the presentation.
    public  void GetSlideIdAndText(out string sldText, string docName, int index)
    {
        using (PresentationDocument ppt = PresentationDocument.Open(docName, false))
        {
            // Get the relationship ID of the first slide.
            PresentationPart part = ppt.PresentationPart;
            OpenXmlElementList slideIds = part.Presentation.SlideIdList.ChildElements;

            string relId = (slideIds[index] as SlideId).RelationshipId;

            // Get the slide part from the relationship ID.
            SlidePart slide = (SlidePart)part.GetPartById(relId);

            // Build a StringBuilder object.
            StringBuilder paragraphText = new StringBuilder();

            // Get the inner text of the slide:
            IEnumerable<A.Text> texts = slide.Slide.Descendants<A.Text>();
            foreach (A.Text text in texts)
            {
                paragraphText.Append(text.Text);
            }
            sldText = paragraphText.ToString();
        }
    }

    public  void CreatePresentation(string filepath)
    {
        // Create a presentation at a specified file path. The presentation document type is pptx, by default.
        PresentationDocument presentationDoc = PresentationDocument.Create(filepath, PresentationDocumentType.Presentation);
        PresentationPart presentationPart = presentationDoc.AddPresentationPart();
        presentationPart.Presentation = new Presentation();

        CreatePresentationParts(presentationPart);

        //Close the presentation handle
        presentationDoc.Close();
    }

    private  void CreatePresentationParts(PresentationPart presentationPart)
    {
        SlideMasterIdList slideMasterIdList1 = new SlideMasterIdList(new SlideMasterId() { Id = (UInt32Value)2147483648U, RelationshipId = "rId1" });
        SlideIdList slideIdList1 = new SlideIdList(new SlideId() { Id = (UInt32Value)256U, RelationshipId = "rId2" });
        SlideSize slideSize1 = new SlideSize() { Cx = 9144000, Cy = 6858000, Type = SlideSizeValues.Screen4x3 };
        NotesSize notesSize1 = new NotesSize() { Cx = 6858000, Cy = 9144000 };
        DefaultTextStyle defaultTextStyle1 = new DefaultTextStyle();

        presentationPart.Presentation.Append(slideMasterIdList1, slideIdList1, slideSize1, notesSize1, defaultTextStyle1);

        SlidePart slidePart1;
        SlideLayoutPart slideLayoutPart1;
        SlideMasterPart slideMasterPart1;
        ThemePart themePart1;


        slidePart1 = CreateSlidePart(presentationPart);
        slideLayoutPart1 = CreateSlideLayoutPart(slidePart1);
        slideMasterPart1 = CreateSlideMasterPart(slideLayoutPart1);
        themePart1 = CreateTheme(slideMasterPart1);

        slideMasterPart1.AddPart(slideLayoutPart1, "rId1");
        presentationPart.AddPart(slideMasterPart1, "rId1");
        presentationPart.AddPart(themePart1, "rId5");
    }

    private  SlidePart CreateSlidePart(PresentationPart presentationPart)
    {
        SlidePart slidePart1 = presentationPart.AddNewPart<SlidePart>("rId2");
        slidePart1.Slide = new Slide(
                new CommonSlideData(
                    new ShapeTree(
                        new P.NonVisualGroupShapeProperties(
                            new P.NonVisualDrawingProperties() { Id = (UInt32Value)1U, Name = "" },
                            new P.NonVisualGroupShapeDrawingProperties(),
                            new ApplicationNonVisualDrawingProperties()),
                        new GroupShapeProperties(new TransformGroup()),
                        new P.Shape(
                            new P.NonVisualShapeProperties(
                                new P.NonVisualDrawingProperties() { Id = (UInt32Value)2U, Name = "Title 1" },
                                new P.NonVisualShapeDrawingProperties(new ShapeLocks() { NoGrouping = true }),
                                new ApplicationNonVisualDrawingProperties(new PlaceholderShape())),
                            new P.ShapeProperties(),
                            new P.TextBody(
                                new BodyProperties(),
                                new ListStyle(),
                                new DocumentFormat.OpenXml.Drawing.Paragraph(new EndParagraphRunProperties() { Language = "en-US" }))))),
                new ColorMapOverride(new MasterColorMapping()));

        return slidePart1;
    }

    private  SlideLayoutPart CreateSlideLayoutPart(SlidePart slidePart1)
    {
        SlideLayoutPart slideLayoutPart1 = slidePart1.AddNewPart<SlideLayoutPart>("rId1");
        SlideLayout slideLayout = new SlideLayout(
        new CommonSlideData(new ShapeTree(
          new P.NonVisualGroupShapeProperties(
          new P.NonVisualDrawingProperties() { Id = (UInt32Value)1U, Name = "" },
          new P.NonVisualGroupShapeDrawingProperties(),
          new ApplicationNonVisualDrawingProperties()),
          new GroupShapeProperties(new TransformGroup()),
          new P.Shape(
          new P.NonVisualShapeProperties(
            new P.NonVisualDrawingProperties() { Id = (UInt32Value)2U, Name = "" },
            new P.NonVisualShapeDrawingProperties(new ShapeLocks() { NoGrouping = true }),
            new ApplicationNonVisualDrawingProperties(new PlaceholderShape())),
          new P.ShapeProperties(),
          new P.TextBody(
            new BodyProperties(),
            new ListStyle(),
            new DocumentFormat.OpenXml.Drawing.Paragraph(new EndParagraphRunProperties()))))),
        new ColorMapOverride(new MasterColorMapping()));
        slideLayoutPart1.SlideLayout = slideLayout;
        return slideLayoutPart1;
    }

    private  SlideMasterPart CreateSlideMasterPart(SlideLayoutPart slideLayoutPart1)
    {
        SlideMasterPart slideMasterPart1 = slideLayoutPart1.AddNewPart<SlideMasterPart>("rId1");
        SlideMaster slideMaster = new SlideMaster(
        new CommonSlideData(new ShapeTree(
          new P.NonVisualGroupShapeProperties(
          new P.NonVisualDrawingProperties() { Id = (UInt32Value)1U, Name = "" },
          new P.NonVisualGroupShapeDrawingProperties(),
          new ApplicationNonVisualDrawingProperties()),
          new GroupShapeProperties(new TransformGroup()),
          new P.Shape(
          new P.NonVisualShapeProperties(
            new P.NonVisualDrawingProperties() { Id = (UInt32Value)2U, Name = "Title Placeholder 1" },
            new P.NonVisualShapeDrawingProperties(new ShapeLocks() { NoGrouping = true }),
            new ApplicationNonVisualDrawingProperties(new PlaceholderShape() { Type = PlaceholderValues.Title })),
          new P.ShapeProperties(),
          new P.TextBody(
            new BodyProperties(),
            new ListStyle(),
            new DocumentFormat.OpenXml.Drawing.Paragraph())))),
        new P.ColorMap() { Background1 = D.ColorSchemeIndexValues.Light1, Text1 = D.ColorSchemeIndexValues.Dark1, Background2 = D.ColorSchemeIndexValues.Light2, Text2 = D.ColorSchemeIndexValues.Dark2, Accent1 = D.ColorSchemeIndexValues.Accent1, Accent2 = D.ColorSchemeIndexValues.Accent2, Accent3 = D.ColorSchemeIndexValues.Accent3, Accent4 = D.ColorSchemeIndexValues.Accent4, Accent5 = D.ColorSchemeIndexValues.Accent5, Accent6 = D.ColorSchemeIndexValues.Accent6, Hyperlink = D.ColorSchemeIndexValues.Hyperlink, FollowedHyperlink = D.ColorSchemeIndexValues.FollowedHyperlink },
        new SlideLayoutIdList(new SlideLayoutId() { Id = (UInt32Value)2147483649U, RelationshipId = "rId1" }),
        new TextStyles(new TitleStyle(), new BodyStyle(), new OtherStyle()));
        slideMasterPart1.SlideMaster = slideMaster;

        return slideMasterPart1;
    }

    private  ThemePart CreateTheme(SlideMasterPart slideMasterPart1)
    {
        ThemePart themePart1 = slideMasterPart1.AddNewPart<ThemePart>("rId5");
        D.Theme theme1 = new D.Theme() { Name = "Office Theme" };

        D.ThemeElements themeElements1 = new D.ThemeElements(
        new D.ColorScheme(
          new D.Dark1Color(new D.SystemColor() { Val = D.SystemColorValues.WindowText, LastColor = "000000" }),
          new D.Light1Color(new D.SystemColor() { Val = D.SystemColorValues.Window, LastColor = "FFFFFF" }),
          new D.Dark2Color(new D.RgbColorModelHex() { Val = "1F497D" }),
          new D.Light2Color(new D.RgbColorModelHex() { Val = "EEECE1" }),
          new D.Accent1Color(new D.RgbColorModelHex() { Val = "4F81BD" }),
          new D.Accent2Color(new D.RgbColorModelHex() { Val = "C0504D" }),
          new D.Accent3Color(new D.RgbColorModelHex() { Val = "9BBB59" }),
          new D.Accent4Color(new D.RgbColorModelHex() { Val = "8064A2" }),
          new D.Accent5Color(new D.RgbColorModelHex() { Val = "4BACC6" }),
          new D.Accent6Color(new D.RgbColorModelHex() { Val = "F79646" }),
          new D.Hyperlink(new D.RgbColorModelHex() { Val = "0000FF" }),
          new D.FollowedHyperlinkColor(new D.RgbColorModelHex() { Val = "800080" })) { Name = "Office" },
          new D.FontScheme(
          new D.MajorFont(
          new D.LatinFont() { Typeface = "Calibri" },
          new D.EastAsianFont() { Typeface = "" },
          new D.ComplexScriptFont() { Typeface = "" }),
          new D.MinorFont(
          new D.LatinFont() { Typeface = "Calibri" },
          new D.EastAsianFont() { Typeface = "" },
          new D.ComplexScriptFont() { Typeface = "" })) { Name = "Office" },
          new D.FormatScheme(
          new D.FillStyleList(
          new D.SolidFill(new D.SchemeColor() { Val = D.SchemeColorValues.PhColor }),
          new D.GradientFill(
            new D.GradientStopList(
            new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 50000 },
              new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 },
            new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 37000 },
             new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 35000 },
            new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 15000 },
             new D.SaturationModulation() { Val = 350000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 100000 }
            ),
            new D.LinearGradientFill() { Angle = 16200000, Scaled = true }),
          new D.NoFill(),
          new D.PatternFill(),
          new D.GroupFill()),
          new D.LineStyleList(
          new D.Outline(
            new D.SolidFill(
            new D.SchemeColor(
              new D.Shade() { Val = 95000 },
              new D.SaturationModulation() { Val = 105000 }) { Val = D.SchemeColorValues.PhColor }),
            new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
          {
              Width = 9525,
              CapType = D.LineCapValues.Flat,
              CompoundLineType = D.CompoundLineValues.Single,
              Alignment = D.PenAlignmentValues.Center
          },
          new D.Outline(
            new D.SolidFill(
            new D.SchemeColor(
              new D.Shade() { Val = 95000 },
              new D.SaturationModulation() { Val = 105000 }) { Val = D.SchemeColorValues.PhColor }),
            new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
          {
              Width = 9525,
              CapType = D.LineCapValues.Flat,
              CompoundLineType = D.CompoundLineValues.Single,
              Alignment = D.PenAlignmentValues.Center
          },
          new D.Outline(
            new D.SolidFill(
            new D.SchemeColor(
              new D.Shade() { Val = 95000 },
              new D.SaturationModulation() { Val = 105000 }) { Val = D.SchemeColorValues.PhColor }),
            new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
          {
              Width = 9525,
              CapType = D.LineCapValues.Flat,
              CompoundLineType = D.CompoundLineValues.Single,
              Alignment = D.PenAlignmentValues.Center
          }),
          new D.EffectStyleList(
          new D.EffectStyle(
            new D.EffectList(
            new D.OuterShadow(
              new D.RgbColorModelHex(
              new D.Alpha() { Val = 38000 }) { Val = "000000" }) { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false })),
          new D.EffectStyle(
            new D.EffectList(
            new D.OuterShadow(
              new D.RgbColorModelHex(
              new D.Alpha() { Val = 38000 }) { Val = "000000" }) { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false })),
          new D.EffectStyle(
            new D.EffectList(
            new D.OuterShadow(
              new D.RgbColorModelHex(
              new D.Alpha() { Val = 38000 }) { Val = "000000" }) { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false }))),
          new D.BackgroundFillStyleList(
          new D.SolidFill(new D.SchemeColor() { Val = D.SchemeColorValues.PhColor }),
          new D.GradientFill(
            new D.GradientStopList(
            new D.GradientStop(
              new D.SchemeColor(new D.Tint() { Val = 50000 },
                new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 },
            new D.GradientStop(
              new D.SchemeColor(new D.Tint() { Val = 50000 },
                new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 },
            new D.GradientStop(
              new D.SchemeColor(new D.Tint() { Val = 50000 },
                new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 }),
            new D.LinearGradientFill() { Angle = 16200000, Scaled = true }),
          new D.GradientFill(
            new D.GradientStopList(
            new D.GradientStop(
              new D.SchemeColor(new D.Tint() { Val = 50000 },
                new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 },
            new D.GradientStop(
              new D.SchemeColor(new D.Tint() { Val = 50000 },
                new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 }),
            new D.LinearGradientFill() { Angle = 16200000, Scaled = true }))) { Name = "Office" });

        theme1.Append(themeElements1);
        theme1.Append(new D.ObjectDefaults());
        theme1.Append(new D.ExtraColorSchemeList());

        themePart1.Theme = theme1;
        return themePart1;

    }

    public static string StripTagsRegex(string source)
    {
        return Regex.Replace(source, "<.*?>", string.Empty);
    }

    public void OnlyCapture(string ChartPath, string ChartXMLURL, string FileName)
    {
        string ReturnValue = "";
        FusionCharts.ServerSideImageHandler ssh = new FusionCharts.ServerSideImageHandler(ChartPath, 850, 500, ChartXMLURL, "", FusionCharts.ServerSideImageHandler.ImageType.PNG, 96.0f);
        // Setting chart SWF version
        ssh.SetChartVersion("3.2.2.0");
        // ssh.SetChartVersion("3.2.1.0");
        // ssh.SetCapturingDelayTime(fcdelaytime);
        // Begins the capture process.
        // An image will be created at the specified location after execution of this statement.
        ReturnValue = ssh.BeginCapture();
        if (!ReturnValue.ToLower().Equals("success"))
        {
           // this.Response.Write("<b>Error:&nbsp;&nbsp;</b>" + ReturnValue.Replace(Environment.NewLine, "<br/>"));
        }
        else
        {
            Bitmap bmp = ssh.GetImage();
            bmp.Save(FileName);
        }
       
    }

    /// <summary>
    /// Generate Table as below order:
    /// a:tbl(Table) ->a:tr(TableRow)->a:tc(TableCell)
    /// We can return TableCell object with CreateTextCell method
    /// and Append the TableCell object to TableRow 
    /// </summary>
    /// <returns>Table Object</returns>
    /// 

    private static A.Table GenerateTable(int iseq, string seqtype, DataRow[] drrows, string noofresps,DataTable mtxrownames,DataTable mtxcolnames,DataTable dt1011,string seqnum)
    {             
        string[,] tableSources = null;

        if ( (seqtype == "10") || (seqtype == "11") )
        {
            tableSources = new string[mtxrownames.Rows.Count*2+2, mtxcolnames.Rows.Count+1];
            
            tableSources[0, 0] = "";
            for (int irg = 0; irg < mtxcolnames.Rows.Count; irg++)
            {
                tableSources[0, irg+1] = System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[irg][0].ToString());                
            }

           
            int rowcnt = 0;
            for (int irg1 = 0; irg1 < mtxrownames.Rows.Count; irg1++)
            {
              
                DataRow[] dr1011f = dt1011.Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + seqnum + "'");
                
                tableSources[rowcnt + 1, 0] = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString());
                tableSources[rowcnt + 2, 0] = " ";
                                             
                for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                {

                    tableSources[rowcnt + 1, cgv + 1] = dr1011f[cgv][5].ToString();
                    tableSources[rowcnt + 2, cgv + 1] = dr1011f[cgv][6].ToString() + "%";              
                 
                }
                rowcnt = rowcnt + 2;

            }
            
            tableSources[mtxrownames.Rows.Count * 2+1, 0] = "# of Respondents";
            for (int irg1 = 0; irg1 < mtxcolnames.Rows.Count; irg1++)
            {
                tableSources[mtxrownames.Rows.Count * 2+1, irg1+1] =  " ";
            }
            tableSources[mtxrownames.Rows.Count * 2 + 1, mtxcolnames.Rows.Count] = noofresps;
        }
        
        // Declare and instantiate table 
        A.Table table = new A.Table();
       

        // Specify the required table properties for the table
        A.TableProperties tableProperties = new A.TableProperties() { FirstRow = true, BandRow = true };
        A.TableStyleId tableStyleId = new A.TableStyleId();
        tableStyleId.Text = "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}";

        tableProperties.Append(tableStyleId);
        
        // Declare and instantiate tablegrid and colums
        A.TableGrid tableGrid1 = new A.TableGrid();
        
        //A.GridColumn gridColumn1 = new A.GridColumn() { Width = 3048000L };
        //A.GridColumn gridColumn2 = new A.GridColumn() { Width = 3048000L };
        //A.GridColumn gridColumn3 = new A.GridColumn() { Width = 3048000L };

        //tableGrid1.Append(gridColumn1);
        //tableGrid1.Append(gridColumn2);
        //tableGrid1.Append(gridColumn3);

        A.GridColumn[] gridColumn = new A.GridColumn[mtxcolnames.Rows.Count+1];
        

        for (int irg = 0; irg < mtxcolnames.Rows.Count+1; irg++)
        {
            if (irg == 0)
            {
                gridColumn[irg] = new A.GridColumn() { Width = 2548000L };
            }
            else
            {
                gridColumn[irg] = new A.GridColumn() { Width = 6044000L / mtxcolnames.Rows.Count };
                //if (mtxcolnames.Rows.Count <= 3)
                //{
                   // gridColumn[irg] = new A.GridColumn() { Width = 1848000L };
                //}
                //else
                //{
                //    gridColumn[irg] = new A.GridColumn() { Width = 1048000L };
                //}
            }
           
            tableGrid1.Append(gridColumn[irg]);
        }

        table.Append(tableProperties);
        table.Append(tableGrid1);
        for (int row = 0; row < tableSources.GetLength(0); row++)
        {
            // Instantiate the table row
            A.TableRow tableRow = new A.TableRow() { Height = 370840L };
            for (int column = 0; column < tableSources.GetLength(1); column++)
            {
                tableRow.Append(CreateTextCell(tableSources.GetValue(row, column).ToString()));
            }

            table.Append(tableRow);
        }

        return table;

    }


    private static A.Table GenerateTable(int iseq, string seqtype, DataRow[] drrows, string noofresps, DataTable mtxrownames, DataTable mtxcolnames,DataTable mtxsubcolnames, DataTable dt12, string seqnum)
    {
        

        string[,] tableSources = null;

        if ((seqtype == "12"))
        {
            tableSources = new string[mtxrownames.Rows.Count * 2 + 2, mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count + 1];

            tableSources[0, 0] = "";
            int cval = 0;
            for (int irg12 = 0; irg12 < mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count; irg12++)
            {
                if ((irg12 == 0) || (irg12 == mtxsubcolnames.Rows.Count))
                {
                    tableSources[0, irg12 + 1] = System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[cval][0].ToString());
                    cval = cval + 1;
                }
                else
                {
                    tableSources[0, irg12 + 1] = "";
                }

            }
            tableSources[1, 0] = "";
            for (int irg112 = 0; irg112 < mtxrownames.Rows.Count; irg112++)
            {

                DataRow[] dr12sub = dt12.Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg112][0].ToString()).Replace("'", "''") + "' and seq_num = '" + seqnum + "'");
                for (int cgv12 = 0; cgv12 < dr12sub.Length; cgv12++)
                {
                    tableSources[1, cgv12 + 1] = System.Web.HttpUtility.HtmlDecode(dr12sub[cgv12][4].ToString());
                }

            }

            int rowcnt = 1;
            for (int irg12data = 0; irg12data < mtxrownames.Rows.Count; irg12data++)
            {
               
                DataRow[] dr12f = dt12.Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg12data][0].ToString()).Replace("'", "''") + "' and seq_num = '" + seqnum + "'");

                tableSources[rowcnt + 1, 0] = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg12data][0].ToString());
                tableSources[rowcnt + 2, 0] = " ";

                for (int cgv12 = 0; cgv12 < dr12f.Length; cgv12++)
                {
                    tableSources[rowcnt + 1, cgv12 + 1] = dr12f[cgv12][5].ToString();
                    tableSources[rowcnt + 2, cgv12 + 1] = dr12f[cgv12][6].ToString() + "%";

                }
                rowcnt = rowcnt + 2;
               
            }


            tableSources[mtxrownames.Rows.Count * 2 + 1, 0] = "# of Respondents";
            for (int irg1 = 0; irg1 < mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count ; irg1++)
            {
                tableSources[mtxrownames.Rows.Count * 2 + 1, irg1 + 1] = " ";
            }
            tableSources[mtxrownames.Rows.Count * 2 + 1, mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count] = noofresps;
        }



        // Declare and instantiate table 
        A.Table table = new A.Table();

        // Specify the required table properties for the table
        A.TableProperties tableProperties = new A.TableProperties() { FirstRow = true, BandRow = true };
        A.TableStyleId tableStyleId = new A.TableStyleId();
        tableStyleId.Text = "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}";

        tableProperties.Append(tableStyleId);


        // Declare and instantiate tablegrid and colums
        A.TableGrid tableGrid1 = new A.TableGrid();
        //A.GridColumn gridColumn1 = new A.GridColumn() { Width = 3048000L };
        //A.GridColumn gridColumn2 = new A.GridColumn() { Width = 3048000L };
        //A.GridColumn gridColumn3 = new A.GridColumn() { Width = 3048000L };

        //tableGrid1.Append(gridColumn1);
        //tableGrid1.Append(gridColumn2);
        //tableGrid1.Append(gridColumn3);

        A.GridColumn[] gridColumn = new A.GridColumn[mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count + 1];

        int colsubcolcnt = mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count;

        for (int irg = 0; irg < mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count + 1; irg++)
        {
            //if (irg == 0)
            //{
            //    gridColumn[irg] = new A.GridColumn() { Width = 3048000L };
            //}
            //else
            //{
            //    gridColumn[irg] = new A.GridColumn() { Width = 1048000L };
            //}
            if (irg == 0)
            {
                gridColumn[irg] = new A.GridColumn() { Width = 2548000L };
            }
            else
            {
                gridColumn[irg] = new A.GridColumn() { Width = 6044000L / colsubcolcnt };               
            }
            tableGrid1.Append(gridColumn[irg]);
        }

        table.Append(tableProperties);
        table.Append(tableGrid1);
        for (int row = 0; row < tableSources.GetLength(0); row++)
        {
            // Instantiate the table row
            A.TableRow tableRow = new A.TableRow() { Height = 370840L };
            for (int column = 0; column < tableSources.GetLength(1); column++)
            {
                tableRow.Append(CreateTextCell(tableSources.GetValue(row, column).ToString()));
            }

            table.Append(tableRow);
        }

        return table;

    }

    private static A.Table GeneratePollTable(DataRow[] dtpoll, string noofresps)
    {

        //string[,] tableSources1 = new string[,] { { "Answeroptions", "In%", "In nos." }, { "E Satsified", "33%", "3" } };


        string[,] tableSources = null;

     
           tableSources = new string[dtpoll.Length+2, 3];

            tableSources[0, 0] = "Answeroptions";
            tableSources[0, 1] = "In%";
            tableSources[0, 2] = "In nos.";



            for (int irg1 = 0; irg1 < dtpoll.Length; irg1++)
            {
                tableSources[irg1 + 1, 0] = dtpoll[irg1][1].ToString();
                tableSources[irg1 + 1, 1] = dtpoll[irg1][3].ToString() + "%";
                tableSources[irg1 + 1, 2] = dtpoll[irg1][2].ToString();
            }

            tableSources[dtpoll.Length + 1, 0] = "# of Respondents";
            tableSources[dtpoll.Length + 1, 1] = " ";
            tableSources[dtpoll.Length + 1, 2] = noofresps;
        



        // Declare and instantiate table 
        A.Table table = new A.Table();


        // Specify the required table properties for the table
        A.TableProperties tableProperties = new A.TableProperties() { FirstRow = true, BandRow = true };
        A.TableStyleId tableStyleId = new A.TableStyleId();
        tableStyleId.Text = "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}";

        tableProperties.Append(tableStyleId);


        // Declare and instantiate tablegrid and colums
        A.TableGrid tableGrid1 = new A.TableGrid();

        A.GridColumn gridColumn1 = new A.GridColumn() { Width = 4548000L };
        A.GridColumn gridColumn2 = new A.GridColumn() { Width = 2008000L };
        A.GridColumn gridColumn3 = new A.GridColumn() { Width = 2008000L };

        tableGrid1.Append(gridColumn1);
        tableGrid1.Append(gridColumn2);
        tableGrid1.Append(gridColumn3);
        table.Append(tableProperties);
        table.Append(tableGrid1);



        for (int row = 0; row < tableSources.GetLength(0); row++)
        {
            A.TableRow tableRow = new A.TableRow() { Height = 370840L };

            // Instantiate the table row

            for (int column = 0; column < tableSources.GetLength(1); column++)
            {


                tableRow.Append(CreateTextCell(tableSources.GetValue(row, column).ToString()));
            }


            table.Append(tableRow);
        }


        return table;

    }

    
    private static A.Table GenerateTable(int iseq, string seqtype, DataRow[] drrows,string noofresps)
    {
      
        //string[,] tableSources1 = new string[,] { { "Answeroptions", "In%", "In nos." }, { "E Satsified", "33%", "3" } };

     
        string[,] tableSources=null;

        if ((seqtype == "1") || (seqtype == "2") || (seqtype == "3") || (seqtype == "4") || (seqtype == "13") )
                    {
                        tableSources = new string[drrows.Length + 2, 3];
                        
                        tableSources[0, 0] = "Answeroptions";
                        tableSources[0, 1] = "In%";
                        tableSources[0, 2] = "In nos.";

                        for (int irg1 = 0; irg1 < drrows.Length; irg1++)
                        {
                            tableSources[irg1 + 1, 0] = drrows[irg1][2].ToString();
                            tableSources[irg1 + 1, 1] = drrows[irg1][6].ToString() + "%";
                            tableSources[irg1 + 1, 2] = drrows[irg1][5].ToString();
                         }

                        tableSources[drrows.Length + 1, 0] = "# of Respondents";
                        tableSources[drrows.Length + 1, 1] = " ";
                        tableSources[drrows.Length + 1, 2] = noofresps;
                    }

       


                    // Declare and instantiate table 
                    A.Table table = new A.Table();

        
                    // Specify the required table properties for the table
                    A.TableProperties tableProperties = new A.TableProperties() { FirstRow = true, BandRow = true };
                    A.TableStyleId tableStyleId = new A.TableStyleId();
                    tableStyleId.Text = "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}";

                    tableProperties.Append(tableStyleId);


                    // Declare and instantiate tablegrid and colums
                    A.TableGrid tableGrid1 = new A.TableGrid();        

                    A.GridColumn gridColumn1 = new A.GridColumn() { Width = 4548000L };
                    A.GridColumn gridColumn2 = new A.GridColumn() { Width = 2008000L };
                    A.GridColumn gridColumn3 = new A.GridColumn() { Width = 2008000L };

                    tableGrid1.Append(gridColumn1);
                    tableGrid1.Append(gridColumn2);
                    tableGrid1.Append(gridColumn3);
                    table.Append(tableProperties);
                    table.Append(tableGrid1);



                    for (int row = 0; row < tableSources.GetLength(0); row++)
                    {
                        A.TableRow tableRow = new A.TableRow() { Height = 370840L };
                       
                        // Instantiate the table row
                                           
                        for (int column = 0; column < tableSources.GetLength(1); column++)
                        {
                            

                            tableRow.Append(CreateTextCell(tableSources.GetValue(row, column).ToString()));
                        }
                        

                        table.Append(tableRow);
                    }

                   
                    return table;
                            
    }


    private static A.Table GenerateTable15(int iseq, string seqtype, DataRow[] drrows, string noofresps, DataTable mtxrownames, DataTable mtxcolnames, DataTable dt15, string seqnum)
    {
       

        string[,] tableSources = null;

        if (seqtype == "15")
        {
            tableSources = new string[mtxrownames.Rows.Count * 2 + 2, mtxcolnames.Rows.Count + 1];


            tableSources[0, 0] = "";
            for (int irg = 0; irg < mtxcolnames.Rows.Count; irg++)
            {
                tableSources[0, irg + 1] = System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[irg][0].ToString());
            }



            int rowcnt = 0;
            for (int irg1 = 0; irg1 < mtxrownames.Rows.Count; irg1++)
            {

                DataRow[] dr1011f = dt15.Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + seqnum + "'");


                tableSources[rowcnt + 1, 0] = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString());
                tableSources[rowcnt + 2, 0] = " ";



                for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                {

                    tableSources[rowcnt + 1, cgv + 1] = dr1011f[cgv][5].ToString();
                    tableSources[rowcnt + 2, cgv + 1] = dr1011f[cgv][6].ToString() + "%";

                }
                rowcnt = rowcnt + 2;

            }


            tableSources[mtxrownames.Rows.Count * 2 + 1, 0] = "# of Respondents";
            for (int irg1 = 0; irg1 < mtxcolnames.Rows.Count; irg1++)
            {
                tableSources[mtxrownames.Rows.Count * 2 + 1, irg1 + 1] = " ";
            }
            tableSources[mtxrownames.Rows.Count * 2 + 1, mtxcolnames.Rows.Count] = noofresps;
        }



        // Declare and instantiate table 
        A.Table table = new A.Table();

        // Specify the required table properties for the table
        A.TableProperties tableProperties = new A.TableProperties() { FirstRow = true, BandRow = true };
        A.TableStyleId tableStyleId = new A.TableStyleId();
        tableStyleId.Text = "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}";

        tableProperties.Append(tableStyleId);


        // Declare and instantiate tablegrid and colums
        A.TableGrid tableGrid1 = new A.TableGrid();
        //A.GridColumn gridColumn1 = new A.GridColumn() { Width = 3048000L };
        //A.GridColumn gridColumn2 = new A.GridColumn() { Width = 3048000L };
        //A.GridColumn gridColumn3 = new A.GridColumn() { Width = 3048000L };

        //tableGrid1.Append(gridColumn1);
        //tableGrid1.Append(gridColumn2);
        //tableGrid1.Append(gridColumn3);

        A.GridColumn[] gridColumn = new A.GridColumn[mtxcolnames.Rows.Count + 1];

        for (int irg = 0; irg < mtxcolnames.Rows.Count + 1; irg++)
        {
         //   gridColumn[irg] = new A.GridColumn() { Width = 2548000L };
            gridColumn[irg] = new A.GridColumn() { Width = 6844000L / mtxcolnames.Rows.Count };            

            tableGrid1.Append(gridColumn[irg]);
        }

        table.Append(tableProperties);
        table.Append(tableGrid1);
        for (int row = 0; row < tableSources.GetLength(0); row++)
        {
            // Instantiate the table row
            A.TableRow tableRow = new A.TableRow() { Height = 370840L };
            for (int column = 0; column < tableSources.GetLength(1); column++)
            {
                tableRow.Append(CreateTextCell(tableSources.GetValue(row, column).ToString()));
            }

            table.Append(tableRow);
        }

        return table;

    }

    private static A.Table GenerateTableF(string surveyname)
    {
        
        //  string[,] tableSources = new string[,] { { "Answeroptions", "In%", "In nos." }, { "E Satsified", "33%", "3" } };


        string[,] tableSources = null;

        
            tableSources = new string[2, 1];

            tableSources[0, 0] = surveyname;
            tableSources[1, 0] = "- A Report";
                       

        // Declare and instantiate table 
        A.Table table = new A.Table();

        // Specify the required table properties for the table
        A.TableProperties tableProperties = new A.TableProperties() { FirstRow = true, BandRow = true };

        //A.TableStyleId tableStyleId = new A.TableStyleId();
        //tableStyleId.Text = "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}";

        //tableProperties.Append(tableStyleId);

        // Declare and instantiate tablegrid and colums
        A.TableGrid tableGrid1 = new A.TableGrid();
        A.GridColumn gridColumn1 = new A.GridColumn() { Width = 3048000L };
      
        tableGrid1.Append(gridColumn1);
      
        table.Append(tableProperties);
        table.Append(tableGrid1);
        for (int row = 0; row < tableSources.GetLength(0); row++)
        {
            // Instantiate the table row
            A.TableRow tableRow = new A.TableRow() { Height = 470840L };
            for (int column = 0; column < tableSources.GetLength(1); column++)
            {
                tableRow.Append(CreateTextCell(tableSources.GetValue(row, column).ToString()));
            }

            table.Append(tableRow);
        }

        return table;

    }

    /// <summary>
    /// Create table cell with the below order:
    /// a:tc(TableCell)->a:txbody(TextBody)->a:p(Paragraph)->a:r(Run)->a:t(Text)
    /// </summary>
    /// <param name="text">Inserted Text in Cell</param>
    /// <returns>Return TableCell object</returns>
    private static A.TableCell CreateTextCell(string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            text = string.Empty;
        }

        // Declare and instantiate the table cell 
        // Create table cell with the below order:
        // a:tc(TableCell)->a:txbody(TextBody)->a:p(Paragraph)->a:r(Run)->a:t(Text)
        A.TableCell tableCell = new A.TableCell();

        //  Declare and instantiate the text body
        A.TextBody textBody = new A.TextBody();
        A.BodyProperties bodyProperties = new A.BodyProperties();
        A.ListStyle listStyle = new A.ListStyle();

        A.Paragraph paragraph = new A.Paragraph();
        A.Run run = new A.Run();
        A.RunProperties runProperties = new A.RunProperties() { Language = "en-US", Dirty = false };
        A.Text text2 = new A.Text();
        text2.Text = text;
        run.Append(runProperties);
        run.Append(text2);
        A.EndParagraphRunProperties endParagraphRunProperties = new A.EndParagraphRunProperties() { Language = "en-US", Dirty = false };

        paragraph.Append(run);
        paragraph.Append(endParagraphRunProperties);
        textBody.Append(bodyProperties);
        textBody.Append(listStyle);
        textBody.Append(paragraph);

        A.TableCellProperties tableCellProperties = new A.TableCellProperties();
        tableCell.Append(textBody);
        tableCell.Append(tableCellProperties);

        return tableCell;
    }


    public  string[] GetAllTextInSlide(PresentationDocument presentationDocument, int slideIndex)
    {
        // Verify that the presentation document exists.
        if (presentationDocument == null)
        {
            throw new ArgumentNullException("presentationDocument");
        }

        // Verify that the slide index is not out of range.
        if (slideIndex < 0)
        {
            throw new ArgumentOutOfRangeException("slideIndex");
        }

        // Get the presentation part of the presentation document.
        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Verify that the presentation part and presentation exist.
        if (presentationPart != null && presentationPart.Presentation != null)
        {
            // Get the Presentation object from the presentation part.
            Presentation presentation = presentationPart.Presentation;

            // Verify that the slide ID list exists.
            if (presentation.SlideIdList != null)
            {
                // Get the collection of slide IDs from the slide ID list.
                DocumentFormat.OpenXml.OpenXmlElementList slideIds =
                    presentation.SlideIdList.ChildElements;

                // If the slide ID is in range...
                if (slideIndex < slideIds.Count)
                {
                    // Get the relationship ID of the slide.
                    string slidePartRelationshipId = (slideIds[slideIndex] as SlideId).RelationshipId;

                    // Get the specified slide part from the relationship ID.
                    SlidePart slidePart =
                        (SlidePart)presentationPart.GetPartById(slidePartRelationshipId);

                    // Pass the slide part to the next method, and
                    // then return the array of strings that method
                    // returns to the previous method.
                    return GetAllTextInSlide(slidePart);
                }
            }
        }

        // Else, return null.
        return null;
    }

    public static string[] GetAllTextInSlide(SlidePart slidePart)
    {
        // Verify that the slide part exists.
        if (slidePart == null)
        {
            throw new ArgumentNullException("slidePart");
        }

        // Create a new linked list of strings.
        LinkedList<string> texts = new LinkedList<string>();

        // If the slide exists...
        if (slidePart.Slide != null)
        {
            // Iterate through all the paragraphs in the slide.
            foreach (DocumentFormat.OpenXml.Drawing.Paragraph paragraph in
                slidePart.Slide.Descendants<DocumentFormat.OpenXml.Drawing.Paragraph>())
            {
                // Create a new string builder.                    
                StringBuilder paragraphText = new StringBuilder();

                // Iterate through the lines of the paragraph.
                foreach (DocumentFormat.OpenXml.Drawing.Text text in
                    paragraph.Descendants<DocumentFormat.OpenXml.Drawing.Text>())
                {
                    // Append each line to the previous lines.
                    paragraphText.Append(text.Text);
                }

                if (paragraphText.Length > 0)
                {
                    // Add each paragraph to the linked list.
                    texts.AddLast(paragraphText.ToString());
                }
            }
        }

        if (texts.Count > 0)
        {
            // Return an array of strings.
            return texts.ToArray();
        }
        else
        {
            return null;
        }
    }

    public void CreateTableInfirstSlide(PresentationDocument presentationDocument, SlidePart lastSlidePart,string surveyname)
    {
        // Get the presentation Part of the presentation document
        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Get the Slide Id collection of the presentation document
        var slideIdList = presentationPart.Presentation.SlideIdList;


        if (slideIdList == null)
        {
            throw new NullReferenceException("The number of slide is empty, please select a ppt with a slide at least again");
        }


        // Get all Slide Part of the presentation document
        var list = slideIdList.ChildElements
                    .Cast<SlideId>()
                    .Select(x => presentationPart.GetPartById(x.RelationshipId))
                    .Cast<SlidePart>();

        // Get the last Slide Part of the presentation document
        //  var tableSlidePart = (SlidePart)list.Last();
        // int slidecnt = position;

        //   var tableSlidePart = (SlidePart)presentationPart.GetPartById(((SlideId)(slideIdList.ChildElements[slidecnt])).RelationshipId);

        var tableSlidePart = lastSlidePart;

        // Declare and instantiate the graphic Frame of the new slide
        DocumentFormat.OpenXml.Presentation.GraphicFrame graphicFrame = tableSlidePart.Slide.CommonSlideData.ShapeTree.AppendChild(new DocumentFormat.OpenXml.Presentation.GraphicFrame());

        // Specify the required Frame properties of the graphicFrame
        ApplicationNonVisualDrawingPropertiesExtension applicationNonVisualDrawingPropertiesExtension = new ApplicationNonVisualDrawingPropertiesExtension() { Uri = "{D42A27DB-BD31-4B8C-83A1-F6EECF244321}" };
        P14.ModificationId modificationId1 = new P14.ModificationId() { Val = 3229994563U };
        modificationId1.AddNamespaceDeclaration("p14", "http://schemas.microsoft.com/office/powerpoint/2010/main");
        applicationNonVisualDrawingPropertiesExtension.Append(modificationId1);

        //  dsansoptionsall = surcore.getAnsweroptionsQIDySurveOpt(SelectedSurvey.SURVEY_ID);

        graphicFrame.NonVisualGraphicFrameProperties = new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameProperties
        (new DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() { Id = 5, Name = "table 1" },
        new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameDrawingProperties(new A.GraphicFrameLocks() { NoGrouping = true }),
        new ApplicationNonVisualDrawingProperties(new ApplicationNonVisualDrawingPropertiesExtensionList(applicationNonVisualDrawingPropertiesExtension)));

        graphicFrame.Transform = new Transform(new A.Offset() { X = 1650609L, Y = 1343400L }, new A.Extents() { Cx = 6096000L, Cy = 741680L });

        // Specify the Griaphic of the graphic Frame
        graphicFrame.Graphic = new A.Graphic(new A.GraphicData(GenerateTableF(surveyname)) { Uri = "http://schemas.openxmlformats.org/drawingml/2006/table" });



        //  presentationPart.Presentation.Save();


    }

    public void CreatePollTableInLastSlide(PresentationDocument presentationDocument, SlidePart lastSlidePart,  DataRow[] dtpoll, string questionid, string noofresps)
    {
        // Get the presentation Part of the presentation document
        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Get the Slide Id collection of the presentation document
        var slideIdList = presentationPart.Presentation.SlideIdList;


        if (slideIdList == null)
        {
            throw new NullReferenceException("The number of slide is empty, please select a ppt with a slide at least again");
        }


        // Get all Slide Part of the presentation document
        var list = slideIdList.ChildElements
                    .Cast<SlideId>()
                    .Select(x => presentationPart.GetPartById(x.RelationshipId))
                    .Cast<SlidePart>();

        // Get the last Slide Part of the presentation document
        //  var tableSlidePart = (SlidePart)list.Last();
        // int slidecnt = position;

        //   var tableSlidePart = (SlidePart)presentationPart.GetPartById(((SlideId)(slideIdList.ChildElements[slidecnt])).RelationshipId);

        var tableSlidePart = lastSlidePart;

        // Declare and instantiate the graphic Frame of the new slide
        DocumentFormat.OpenXml.Presentation.GraphicFrame graphicFrame = tableSlidePart.Slide.CommonSlideData.ShapeTree.AppendChild(new DocumentFormat.OpenXml.Presentation.GraphicFrame());

        // Specify the required Frame properties of the graphicFrame
        ApplicationNonVisualDrawingPropertiesExtension applicationNonVisualDrawingPropertiesExtension = new ApplicationNonVisualDrawingPropertiesExtension() { Uri = "{D42A27DB-BD31-4B8C-83A1-F6EECF244321}" };
        P14.ModificationId modificationId1 = new P14.ModificationId() { Val = 3229994563U };
        modificationId1.AddNamespaceDeclaration("p14", "http://schemas.microsoft.com/office/powerpoint/2010/main");
        applicationNonVisualDrawingPropertiesExtension.Append(modificationId1);

        //  dsansoptionsall = surcore.getAnsweroptionsQIDySurveOpt(SelectedSurvey.SURVEY_ID);

        graphicFrame.NonVisualGraphicFrameProperties = new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameProperties
        (new DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() { Id = 5, Name = "table 1" },
        new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameDrawingProperties(new A.GraphicFrameLocks() { NoGrouping = true }),
        new ApplicationNonVisualDrawingProperties(new ApplicationNonVisualDrawingPropertiesExtensionList(applicationNonVisualDrawingPropertiesExtension)));

        graphicFrame.Transform = new Transform(new A.Offset() { X = 300609L, Y = 1643400L }, new A.Extents() { Cx = 6096000L, Cy = 741680L });

        // Specify the Griaphic of the graphic Frame
        graphicFrame.Graphic = new A.Graphic(new A.GraphicData(GeneratePollTable( dtpoll, noofresps)) { Uri = "http://schemas.openxmlformats.org/drawingml/2006/table" });


        //  presentationPart.Presentation.Save();


    }


    public void CreateTableInLastSlide(PresentationDocument presentationDocument, SlidePart lastSlidePart, int iseq, string seqtype, DataRow[] drrows, string questionid,string noofresps)
    {
        // Get the presentation Part of the presentation document
        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Get the Slide Id collection of the presentation document
        var slideIdList = presentationPart.Presentation.SlideIdList;

        
        if (slideIdList == null)
        {
            throw new NullReferenceException("The number of slide is empty, please select a ppt with a slide at least again");
        }

        
        // Get all Slide Part of the presentation document
        var list = slideIdList.ChildElements
                    .Cast<SlideId>()
                    .Select(x => presentationPart.GetPartById(x.RelationshipId))
                    .Cast<SlidePart>();

        // Get the last Slide Part of the presentation document
      //  var tableSlidePart = (SlidePart)list.Last();
       // int slidecnt = position;

     //   var tableSlidePart = (SlidePart)presentationPart.GetPartById(((SlideId)(slideIdList.ChildElements[slidecnt])).RelationshipId);

        var tableSlidePart = lastSlidePart;
      
        // Declare and instantiate the graphic Frame of the new slide
        DocumentFormat.OpenXml.Presentation.GraphicFrame graphicFrame = tableSlidePart.Slide.CommonSlideData.ShapeTree.AppendChild(new DocumentFormat.OpenXml.Presentation.GraphicFrame());

        // Specify the required Frame properties of the graphicFrame
        ApplicationNonVisualDrawingPropertiesExtension applicationNonVisualDrawingPropertiesExtension = new ApplicationNonVisualDrawingPropertiesExtension() { Uri = "{D42A27DB-BD31-4B8C-83A1-F6EECF244321}" };
        P14.ModificationId modificationId1 = new P14.ModificationId() { Val = 3229994563U };
        modificationId1.AddNamespaceDeclaration("p14", "http://schemas.microsoft.com/office/powerpoint/2010/main");
        applicationNonVisualDrawingPropertiesExtension.Append(modificationId1);

      //  dsansoptionsall = surcore.getAnsweroptionsQIDySurveOpt(SelectedSurvey.SURVEY_ID);

        graphicFrame.NonVisualGraphicFrameProperties = new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameProperties
        (new DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() { Id = 5, Name = "table 1" },
        new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameDrawingProperties(new A.GraphicFrameLocks() { NoGrouping = true }),
        new ApplicationNonVisualDrawingProperties(new ApplicationNonVisualDrawingPropertiesExtensionList(applicationNonVisualDrawingPropertiesExtension)));

        graphicFrame.Transform = new Transform(new A.Offset() { X = 300609L, Y = 1643400L }, new A.Extents() { Cx = 6096000L, Cy = 741680L });
       
        // Specify the Griaphic of the graphic Frame
        graphicFrame.Graphic = new A.Graphic(new A.GraphicData(GenerateTable(iseq, seqtype, drrows,noofresps)) { Uri = "http://schemas.openxmlformats.org/drawingml/2006/table" });
            

      //  presentationPart.Presentation.Save();

      
    }

    public void CreateTableInLastSlide(PresentationDocument presentationDocument, SlidePart lastSlidePart, int iseq, string seqtype, DataRow[] drrows, string questionid, string noofresps,DataTable mtxrownames,DataTable mtxcolnames,DataTable dt1011,string seqnum)
    {
        // Get the presentation Part of the presentation document
        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Get the Slide Id collection of the presentation document
        var slideIdList = presentationPart.Presentation.SlideIdList;


        if (slideIdList == null)
        {
            throw new NullReferenceException("The number of slide is empty, please select a ppt with a slide at least again");
        }


        // Get all Slide Part of the presentation document
        var list = slideIdList.ChildElements
                    .Cast<SlideId>()
                    .Select(x => presentationPart.GetPartById(x.RelationshipId))
                    .Cast<SlidePart>();

        // Get the last Slide Part of the presentation document
        //  var tableSlidePart = (SlidePart)list.Last();
        // int slidecnt = position;

        //   var tableSlidePart = (SlidePart)presentationPart.GetPartById(((SlideId)(slideIdList.ChildElements[slidecnt])).RelationshipId);

        var tableSlidePart = lastSlidePart;

        // Declare and instantiate the graphic Frame of the new slide
        DocumentFormat.OpenXml.Presentation.GraphicFrame graphicFrame = tableSlidePart.Slide.CommonSlideData.ShapeTree.AppendChild(new DocumentFormat.OpenXml.Presentation.GraphicFrame());

        // Specify the required Frame properties of the graphicFrame
        ApplicationNonVisualDrawingPropertiesExtension applicationNonVisualDrawingPropertiesExtension = new ApplicationNonVisualDrawingPropertiesExtension() { Uri = "{D42A27DB-BD31-4B8C-83A1-F6EECF244321}" };
        P14.ModificationId modificationId1 = new P14.ModificationId() { Val = 3229994563U };
        modificationId1.AddNamespaceDeclaration("p14", "http://schemas.microsoft.com/office/powerpoint/2010/main");
        applicationNonVisualDrawingPropertiesExtension.Append(modificationId1);

        //  dsansoptionsall = surcore.getAnsweroptionsQIDySurveOpt(SelectedSurvey.SURVEY_ID);

        graphicFrame.NonVisualGraphicFrameProperties = new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameProperties
        (new DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() { Id = 5, Name = "table 1" },
        new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameDrawingProperties(new A.GraphicFrameLocks() { NoGrouping = true }),
        new ApplicationNonVisualDrawingProperties(new ApplicationNonVisualDrawingPropertiesExtensionList(applicationNonVisualDrawingPropertiesExtension)));

        graphicFrame.Transform = new Transform(new A.Offset() { X = 300609L, Y = 1643400L }, new A.Extents() { Cx = 6096000L, Cy = 741680L });

        // Specify the Griaphic of the graphic Frame
        graphicFrame.Graphic = new A.Graphic(new A.GraphicData(GenerateTable(iseq, seqtype, drrows, noofresps,mtxrownames,mtxcolnames,dt1011,seqnum)) { Uri = "http://schemas.openxmlformats.org/drawingml/2006/table" });



        //  presentationPart.Presentation.Save();


    }


    public void CreateTableInLastSlide(PresentationDocument presentationDocument, SlidePart lastSlidePart, int iseq, string seqtype, DataRow[] drrows, string questionid, string noofresps, DataTable mtxrownames, DataTable mtxcolnames,DataTable mtxsubcolnames, DataTable dt12, string seqnum)
    {
        // Get the presentation Part of the presentation document
        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Get the Slide Id collection of the presentation document
        var slideIdList = presentationPart.Presentation.SlideIdList;


        if (slideIdList == null)
        {
            throw new NullReferenceException("The number of slide is empty, please select a ppt with a slide at least again");
        }


        // Get all Slide Part of the presentation document
        var list = slideIdList.ChildElements
                    .Cast<SlideId>()
                    .Select(x => presentationPart.GetPartById(x.RelationshipId))
                    .Cast<SlidePart>();

        // Get the last Slide Part of the presentation document
        //  var tableSlidePart = (SlidePart)list.Last();
        // int slidecnt = position;

        //   var tableSlidePart = (SlidePart)presentationPart.GetPartById(((SlideId)(slideIdList.ChildElements[slidecnt])).RelationshipId);

        var tableSlidePart = lastSlidePart;

        // Declare and instantiate the graphic Frame of the new slide
        DocumentFormat.OpenXml.Presentation.GraphicFrame graphicFrame = tableSlidePart.Slide.CommonSlideData.ShapeTree.AppendChild(new DocumentFormat.OpenXml.Presentation.GraphicFrame());

        // Specify the required Frame properties of the graphicFrame
        ApplicationNonVisualDrawingPropertiesExtension applicationNonVisualDrawingPropertiesExtension = new ApplicationNonVisualDrawingPropertiesExtension() { Uri = "{D42A27DB-BD31-4B8C-83A1-F6EECF244321}" };
        P14.ModificationId modificationId1 = new P14.ModificationId() { Val = 3229994563U };
        modificationId1.AddNamespaceDeclaration("p14", "http://schemas.microsoft.com/office/powerpoint/2010/main");
        applicationNonVisualDrawingPropertiesExtension.Append(modificationId1);

        //  dsansoptionsall = surcore.getAnsweroptionsQIDySurveOpt(SelectedSurvey.SURVEY_ID);

        graphicFrame.NonVisualGraphicFrameProperties = new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameProperties
        (new DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() { Id = 5, Name = "table 1" },
        new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameDrawingProperties(new A.GraphicFrameLocks() { NoGrouping = true }),
        new ApplicationNonVisualDrawingProperties(new ApplicationNonVisualDrawingPropertiesExtensionList(applicationNonVisualDrawingPropertiesExtension)));

        graphicFrame.Transform = new Transform(new A.Offset() { X = 300609L, Y = 1643400L }, new A.Extents() { Cx = 6096000L, Cy = 741680L });

        // Specify the Griaphic of the graphic Frame
        graphicFrame.Graphic = new A.Graphic(new A.GraphicData(GenerateTable(iseq, seqtype, drrows, noofresps, mtxrownames, mtxcolnames,mtxsubcolnames, dt12, seqnum)) { Uri = "http://schemas.openxmlformats.org/drawingml/2006/table" });



        //  presentationPart.Presentation.Save();


    }


    public void CreateTableInLastSlide15(PresentationDocument presentationDocument, SlidePart lastSlidePart, int iseq, string seqtype, DataRow[] drrows, string questionid, string noofresps, DataTable mtxrownames, DataTable mtxcolnames, DataTable dt15, string seqnum)
    {
        // Get the presentation Part of the presentation document
        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Get the Slide Id collection of the presentation document
        var slideIdList = presentationPart.Presentation.SlideIdList;


        if (slideIdList == null)
        {
            throw new NullReferenceException("The number of slide is empty, please select a ppt with a slide at least again");
        }


        // Get all Slide Part of the presentation document
        var list = slideIdList.ChildElements
                    .Cast<SlideId>()
                    .Select(x => presentationPart.GetPartById(x.RelationshipId))
                    .Cast<SlidePart>();

        // Get the last Slide Part of the presentation document
        //  var tableSlidePart = (SlidePart)list.Last();
        // int slidecnt = position;

        //   var tableSlidePart = (SlidePart)presentationPart.GetPartById(((SlideId)(slideIdList.ChildElements[slidecnt])).RelationshipId);

        var tableSlidePart = lastSlidePart;

        // Declare and instantiate the graphic Frame of the new slide
        DocumentFormat.OpenXml.Presentation.GraphicFrame graphicFrame = tableSlidePart.Slide.CommonSlideData.ShapeTree.AppendChild(new DocumentFormat.OpenXml.Presentation.GraphicFrame());

        // Specify the required Frame properties of the graphicFrame
        ApplicationNonVisualDrawingPropertiesExtension applicationNonVisualDrawingPropertiesExtension = new ApplicationNonVisualDrawingPropertiesExtension() { Uri = "{D42A27DB-BD31-4B8C-83A1-F6EECF244321}" };
        P14.ModificationId modificationId1 = new P14.ModificationId() { Val = 3229994563U };
        modificationId1.AddNamespaceDeclaration("p14", "http://schemas.microsoft.com/office/powerpoint/2010/main");
        applicationNonVisualDrawingPropertiesExtension.Append(modificationId1);

        //  dsansoptionsall = surcore.getAnsweroptionsQIDySurveOpt(SelectedSurvey.SURVEY_ID);

        graphicFrame.NonVisualGraphicFrameProperties = new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameProperties
        (new DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() { Id = 5, Name = "table 1" },
        new DocumentFormat.OpenXml.Presentation.NonVisualGraphicFrameDrawingProperties(new A.GraphicFrameLocks() { NoGrouping = true }),
        new ApplicationNonVisualDrawingProperties(new ApplicationNonVisualDrawingPropertiesExtensionList(applicationNonVisualDrawingPropertiesExtension)));

        graphicFrame.Transform = new Transform(new A.Offset() { X = 300609L, Y = 1643400L }, new A.Extents() { Cx = 6096000L, Cy = 741680L });

        // Specify the Griaphic of the graphic Frame
        graphicFrame.Graphic = new A.Graphic(new A.GraphicData(GenerateTable15(iseq, seqtype, drrows, noofresps, mtxrownames, mtxcolnames, dt15, seqnum)) { Uri = "http://schemas.openxmlformats.org/drawingml/2006/table" });



        //  presentationPart.Presentation.Save();


    }

    // Delete the specified slide from the presentation.
    public  void DeleteSlide(PresentationDocument presentationDocument, int slideIndex)
    {
        if (presentationDocument == null)
        {
            throw new ArgumentNullException("presentationDocument");
        }

        // Use the CountSlides sample to get the number of slides in the presentation.
        int slidesCount = CountSlides(presentationDocument);

        if (slideIndex < 0 || slideIndex >= slidesCount)
        {
            throw new ArgumentOutOfRangeException("slideIndex");
        }

        // Get the presentation part from the presentation document. 
        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Get the presentation from the presentation part.
        Presentation presentation = presentationPart.Presentation;

        // Get the list of slide IDs in the presentation.
        SlideIdList slideIdList = presentation.SlideIdList;

        // Get the slide ID of the specified slide
        SlideId slideId = slideIdList.ChildElements[slideIndex] as SlideId;

        // Get the relationship ID of the slide.
        string slideRelId = slideId.RelationshipId;

        // Remove the slide from the slide list.
        slideIdList.RemoveChild(slideId);

        //
        // Remove references to the slide from all custom shows.
        if (presentation.CustomShowList != null)
        {
            // Iterate through the list of custom shows.
            foreach (var customShow in presentation.CustomShowList.Elements<CustomShow>())
            {
                if (customShow.SlideList != null)
                {
                    // Declare a link list of slide list entries.
                    LinkedList<SlideListEntry> slideListEntries = new LinkedList<SlideListEntry>();
                    foreach (SlideListEntry slideListEntry in customShow.SlideList.Elements())
                    {
                        // Find the slide reference to remove from the custom show.
                        if (slideListEntry.Id != null && slideListEntry.Id == slideRelId)
                        {
                            slideListEntries.AddLast(slideListEntry);
                        }
                    }

                    // Remove all references to the slide from the custom show.
                    foreach (SlideListEntry slideListEntry in slideListEntries)
                    {
                        customShow.SlideList.RemoveChild(slideListEntry);
                    }
                }
            }
        }

        // Save the modified presentation.
        presentation.Save();

        // Get the slide part for the specified slide.
        SlidePart slidePart = presentationPart.GetPartById(slideRelId) as SlidePart;

        // Remove the slide part.
        presentationPart.DeletePart(slidePart);
    }

    // Count the slides in the presentation.
    public  int CountSlides(PresentationDocument presentationDocument)
    {
        // Check for a null document object.
        if (presentationDocument == null)
        {
            throw new ArgumentNullException("presentationDocument");
        }

        int slidesCount = 0;

        // Get the presentation part of document.
        PresentationPart presentationPart = presentationDocument.PresentationPart;

        // Get the slide count from the SlideParts.
        if (presentationPart != null)
        {
            slidesCount = presentationPart.SlideParts.Count();
        }

        // Return the slide count to the previous method.
        return slidesCount;
    }
}