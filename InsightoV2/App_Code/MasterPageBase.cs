﻿using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Configuration;


/// <summary>
/// Summary description for BasePage
/// </summary>
public class MasterPageBase : System.Web.UI.MasterPage
{
    public MasterPageBase()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string VersionNumber
    {
        get
        {
            return ConfigurationManager.AppSettings["versionNumber"];
        }
    }

    public string ReleaseMode
    {
        get
        {
            return ConfigurationManager.AppSettings["releaseMode"];
        }
    }
    
}