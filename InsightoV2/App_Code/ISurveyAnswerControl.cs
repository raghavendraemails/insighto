﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Insighto.Business.Enumerations;

/// <summary>
/// Summary description for ISurveyAnswerControl
/// </summary>
public interface ISurveyAnswerControl
{
    QuestionType QuestionType { get; set; }
    List<string> Value { get; set; }
    void Initialise();
    void Display();
    void Clear();
}