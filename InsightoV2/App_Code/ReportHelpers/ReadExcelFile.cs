﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using System.IO;

/// <summary>
/// Summary description for ReadExcelFile
/// </summary>
public class ReadExcelFile
{
    /// <summary>
    /// Gets the excel data.
    /// </summary>
    /// <param name="absoluteFilePath">The absolute file path.</param>
    /// <returns></returns>
    public static DataTable GetExcelData(string absoluteFilePath)
    {
        DataTable dt = new DataTable();
        using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(absoluteFilePath, false))
        {

            WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
            IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
            string relationshipId = sheets.First().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
            Worksheet workSheet = worksheetPart.Worksheet;
            SheetData sheetData = workSheet.GetFirstChild<SheetData>();
            IEnumerable<Row> rows = sheetData.Descendants<Row>();

            foreach (Cell cell in rows.ElementAt(0))
            {
                dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
            }

            foreach (Row row in rows) //this will also include your header row...
            {
                DataRow tempRow = dt.NewRow();
                for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                {
                    tempRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                }
                dt.Rows.Add(tempRow);
            }
        }
        //dt.Rows.RemoveAt(0); //...so i'm taking it out here.
        return dt;
    }

    /// <summary>
    /// Gets the cell value.
    /// </summary>
    /// <param name="document">The document.</param>
    /// <param name="cell">The cell.</param>
    /// <returns></returns>
    public static string GetCellValue(SpreadsheetDocument document, Cell cell)
    {
        SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
        string value = cell.CellValue != null ? cell.CellValue.InnerXml : string.Empty;

        if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
        {
            return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
        }
        else
        {
            return value;
        }
    }

    /// <summary>
    /// CSVs to data table.
    /// </summary>
    /// <param name="file">The file.</param>
    /// <param name="isRowOneHeader">if set to <c>true</c> [is row one header].</param>
    /// <returns></returns>
    public static DataTable CsvToDataTable(string file, bool isRowOneHeader)
    {

        DataTable csvDataTable = new DataTable();

        //no try/catch - add these in yourselfs or let exception happen
        String[] csvData = File.ReadAllLines(file);

        //if no data in file ‘manually' throw an exception
        if (csvData.Length == 0)
        {
            throw new Exception("CSV File Appears to be Empty");
        }

        String[] headings = csvData[0].Split(',');
        int index = 0; //will be zero or one depending on isRowOneHeader

        if (isRowOneHeader) //if first record lists headers
        {
            index = 1; //so we won't take headings as data

            //for each heading
            for (int i = 0; i < headings.Length; i++)
            {
                //add a column for each heading
                csvDataTable.Columns.Add(headings[i], typeof(string));
            }
        }
        else //if no headers just go for col1, col2 etc.
        {
            for (int i = 0; i < headings.Length; i++)
            {
                //create arbitary column names
                csvDataTable.Columns.Add("col" + (i + 1).ToString(), typeof(string));
            }
        }

        //populate the DataTable
        for (int i = index; i < csvData.Length; i++)
        {
            //create new rows
            DataRow row = csvDataTable.NewRow();

            for (int j = 0; j < headings.Length; j++)
            {
                //fill them
                row[j] = csvData[i].Split(',')[j];
            }

            //add rows to over DataTable
            csvDataTable.Rows.Add(row);
        }

        //return the CSV DataTable
        return csvDataTable;

    }
}