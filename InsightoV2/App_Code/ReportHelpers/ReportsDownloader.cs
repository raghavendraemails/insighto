﻿using System;
using System.Web;
using Insighto.Business.Services;
using CovalenseUtilities.Services;
using System.Data;
using System.Collections.Generic;
using Insighto.Data;
using ExportToExcel;
using System.Linq;
using System.Text.RegularExpressions;
using App_Code;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using Insighto.Business.Enumerations;
using System.IO;

public class ReportsDownloader : IHttpHandler
{
    private string reportName;
    private string fileName;
    private string filePath;

    public bool IsReusable { get { return false; } }

    public void ProcessRequest(HttpContext context)
    {
        var htQueryStrings = EncryptHelper.DecryptQuerystringParam(context.Request.QueryString["Key"]);
        SurveyBasicInfoView surveyBasicView = null;
         var surveyId = 0;
        if (htQueryStrings != null)
        {
            surveyId = ValidationHelper.GetInteger(htQueryStrings["SurveyId"], 0);
            if (surveyId <= 0) return;
            surveyBasicView = ServiceFactory.GetService<RespondentService>().GetSurveyViewInformation(surveyId);
        }
        if (surveyBasicView == null) return;

        if (htQueryStrings["ReportType"] != null && htQueryStrings["ReportType"] == "RawData")
        {
            CreateRawDataReport(context);
        }
        else if (htQueryStrings["ReportType"] != null && Convert.ToString(htQueryStrings["ReportType"]) == "SaveToWord")
        {
            //basic css for respondent page
            var themeUrl = string.Empty;            
            themeUrl = EncryptHelper.EncryptQuerystring("/SaveToWordPage.aspx", Constants.SURVEYID + "=" + surveyId + "&Mode=" + RespondentDisplayMode.Preview.ToString() + "&surveyFlag=" + 0);
            var content = Utilities.GetHtmlString(string.Format("http://{0}{1}", "insighto.com", themeUrl));

            //render document
            string fileName = surveyBasicView.SURVEY_NAME + ".doc";
            RenderDocument(context, content, fileName);
        }
        else if (context.Request.QueryString["ReportType"] != null && Convert.ToString(context.Request.QueryString["ReportType"]) == "PDF")
        {
            //basic css for respondent page
            var themeUrl = string.Empty;
            themeUrl = EncryptHelper.EncryptQuerystring("/SaveToWordPage.aspx", Constants.SURVEYID + "=" + surveyId + "&Mode=" + RespondentDisplayMode.Preview.ToString() + "&surveyFlag=" + 0);
            var content = Utilities.GetHtmlString(string.Format("http://{0}{1}", context.Request.Url.Host, themeUrl));

            //render document
            string fileName = surveyBasicView.SURVEY_NAME + ".pdf";
            RenderDocument(context, content, fileName.Replace(" ", ""));
        }

    }

    private void CreateRawDataReport(HttpContext context)
    {
        var questions = ServiceFactory<SurveyReportsService>.Instance.GetQuestionsBySurveyId(4337);
        var answers = ServiceFactory<SurveyReportsService>.Instance.GetResponseQuestionsAnswers(4337);

        DataSet dsReports = new DataSet();
        DataTable dtShemaAndData = GetSchema(questions);
        dtShemaAndData = FillData(dtShemaAndData, answers);
        dsReports.Tables.Add(dtShemaAndData);
        CreateExcelFile.CreateExcelDocument(dsReports, context.Server.MapPath(@"TemplateFolder/Reports/" + "testPage.xls"));
    }

    private void RenderDocument(HttpContext context, string strHTMLContent, string fileName)
    {
        context.Response.Clear();
        context.Response.Charset = string.Empty;        
        context.Response.ContentType = Utilities.MimeType(Path.GetExtension(fileName));
        context.Response.AddHeader("Content-Disposition", "filename=" + fileName.Replace(" ",""));
        context.Response.Write(strHTMLContent);
        context.Response.End();
        context.Response.Flush();
    }

    private DataTable FillData(DataTable dtShemaAndData, List<ResponseQuestionsInfo> answers)
    {
        //get unique respondents
        var respondentIds = answers.Select(an => an.RESPONDENT_ID).Distinct().ToArray();
        var email = answers.Select(an => an.EMAIL_ADDRESS).Distinct().FirstOrDefault();
        var responseRecievedTime = answers.Select(an => an.RESPONSE_RECEIVED_ON).Distinct().FirstOrDefault();
        //filter answers by respndondent id
        var respondentFilteredAnswers = new List<ResponseQuestionsInfo>();
        foreach (var resId in respondentIds)
        {
            DataRow dr = dtShemaAndData.NewRow();
            GetDefaultValues(dtShemaAndData, email, responseRecievedTime, dr);

            int columnIndex = 3;
            respondentFilteredAnswers = answers.Where(an => an.RESPONDENT_ID == resId).ToList();
            foreach (var item in respondentFilteredAnswers)
            {
                dr[columnIndex] = string.IsNullOrEmpty(item.ANSWER_OPTIONS) ? item.ANSWER_TEXT : item.ANSWER_OPTIONS;
                columnIndex++;
            }
            dtShemaAndData.Rows.Add(dr);
            if (columnIndex == (respondentFilteredAnswers.Count + 3))
                columnIndex = 3;
        }
        dtShemaAndData.AcceptChanges();
        return dtShemaAndData;
    }

    /// <summary>
    /// Gets the default values.
    /// </summary>
    /// <param name="dtShemaAndData">The dt shema and data.</param>
    /// <param name="email">The email.</param>
    /// <param name="responseRecievedTime">The response recieved time.</param>
    /// <param name="dr">The dr.</param>
    private static void GetDefaultValues(DataTable dtShemaAndData, string email, DateTime responseRecievedTime, DataRow dr)
    {
        //dr[0] = (dtShemaAndData.Rows.Count - 1) + 1;
        dr[1] = email;
        dr[2] = responseRecievedTime.ToString();
    }

    private DataTable GetSchema(List<osm_surveyquestion> questions)
    {
        DataTable dt = new DataTable();
        DataColumn dc = new DataColumn();
        dc.ColumnName = "Serial Number";
        dc.AutoIncrement = true;
        dc.DataType = typeof(int);
        dt.Columns.Add(dc);

        dc = new DataColumn();
        dc.ColumnName = "Email";
        dc.DataType = typeof(String);
        dt.Columns.Add(dc);

        dc = new DataColumn();
        dc.ColumnName = "Submit Time";
        dc.DataType = typeof(String);
        dt.Columns.Add(dc);

        foreach (var item in questions)
        {
            dc = new DataColumn();
            dc.ColumnName = ClearHTML(string.Format("Question {0} : {1}", item.QUESTION_SEQ, item.QUSETION_LABEL));
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);
        }

        return dt;
    }

    public string ClearHTML(string HTMLcontent)
    {
        System.Text.RegularExpressions.Regex.Replace(HTMLcontent, "<span[^>]*>", string.Empty);
        Regex.Replace(HTMLcontent, @"<[/]?(form)[^>]*?>", "", RegexOptions.IgnoreCase);
        Regex.Replace(HTMLcontent, @"^<[/]?[.*]>", "", RegexOptions.IgnoreCase);

        string plainHTML = "";
        if (HTMLcontent != null)
        {
            //HTMLcontent = HTMLcontent.Replace("&lt;","<");
            //HTMLcontent = HTMLcontent.Replace("&gt;",">");
            string pattern1html = "<(.|\n)+?>";
            plainHTML = Regex.Replace(HTMLcontent, pattern1html, string.Empty);
            string patternHtml = "<.*?>";
            string patternNewline = "\".";
            string patternNbsp = "&.*";
            //HTMLcontent = HTMLcontent.Replace("&lt;", "<");
            //HTMLcontent = HTMLcontent.Replace("&gt;", ">");
            plainHTML = Regex.Replace(plainHTML, patternHtml, string.Empty);

            //string patternHtml = "</?(?i:script|embed|object|frameset|frame|iframe|meta|link|style)(.|\n)*?>";
            //plainHTML = Regex.Replace(HTMLcontent, patternHtml, string.Empty);
            plainHTML = plainHTML.Replace("&lt;", "<");
            plainHTML = plainHTML.Replace("&gt;", ">");
            plainHTML = plainHTML.Replace("&quot;", "\"");
            plainHTML = plainHTML.Replace("&apos;", "'");
            plainHTML = plainHTML.Replace("&amp;", "&");
            plainHTML = plainHTML.Replace("&nbsp;", " ");
            plainHTML = plainHTML.Replace("&copy;", "\u00a9");
            //plainHTML = plainHTML.Replace("<p>", "");
            //plainHTML = plainHTML.Replace("</p>", "");


            //plainHTML = Regex.Replace(HTMLcontent, patternHtml, string.Empty);
            ////plainHTML = plainHTML.Replace("&lt;", "<");
            //plainHTML = Regex.Replace(plainHTML, patternNewline, string.Empty);
            //plainHTML = Regex.Replace(plainHTML, patternNbsp, string.Empty);
        }
        return (plainHTML);
    }
}