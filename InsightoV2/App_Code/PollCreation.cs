﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Insighto.Business.Charts;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for PollCreation
/// </summary>
public class PollCreation : IPollCreation
{
    SqlConnection objsql = new SqlConnection();
    public static System.Web.SessionState.HttpSessionState Sstate = null;
    DBManager dbm = new DBManager();
    string connect = ConfigurationManager.ConnectionStrings["InsightoConnString"].ToString();
	
    public PollCreation()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    
    public SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    
    public int AddNameandType(string name, string type, int userid, int parentid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_CreatePoll", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollname", SqlDbType.NVarChar)).Value = name;
            scom.Parameters.Add(new SqlParameter("@polltype", SqlDbType.VarChar)).Value = type;
            scom.Parameters.Add(new SqlParameter("@USERID", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@parentpollId", SqlDbType.Int)).Value = parentid;
            scom.Parameters.Add(new SqlParameter("@PollId", SqlDbType.Int)).Value = "0";
            scom.Parameters["@PollId"].Direction = ParameterDirection.Output;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            int pollId = Convert.ToInt32(scom.Parameters["@PollId"].Value);
            return pollId;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //public int AddNameandType(string name, string type, int userid)
    //{
    //    try
    //    {
    //        SqlConnection con = new SqlConnection();
    //        con = strconnectionstring();
    //        SqlCommand scom = new SqlCommand("sp_CreatePoll", con);
    //        scom.CommandType = CommandType.StoredProcedure;
    //        scom.Parameters.Add(new SqlParameter("@pollname", SqlDbType.NVarChar)).Value = name;
    //        scom.Parameters.Add(new SqlParameter("@polltype", SqlDbType.VarChar)).Value = type;
    //        scom.Parameters.Add(new SqlParameter("@USERID", SqlDbType.Int)).Value = userid;
    //        scom.Parameters.Add(new SqlParameter("@PollId", SqlDbType.Int)).Value = "0";
    //        scom.Parameters["@PollId"].Direction = ParameterDirection.Output;
    //        SqlDataAdapter sda = new SqlDataAdapter(scom);
    //        DataSet dspoll = new DataSet();
    //        sda.Fill(dspoll);
    //        con.Close();
    //        int pollId = Convert.ToInt32(scom.Parameters["@PollId"].Value);
    //        return pollId;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    public void UpdatePollName(string name, string type, int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollNameandTheme", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollname", SqlDbType.VarChar)).Value = name;
            scom.Parameters.Add(new SqlParameter("@polltype", SqlDbType.VarChar)).Value = type;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public bool GetSettingsInfo(int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetSettingsInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            bool Pn;
            if (dspoll.Tables[0].Rows.Count > 0)
            {
                Pn = true;
            }
            else
            {
                Pn = false;
            }
            return Pn;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public bool getPollName(int userId,string name)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetPollName", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userId", SqlDbType.Int)).Value = userId;
            scom.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar)).Value = name;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            bool Pn;
            if (dspoll.Tables[0].Rows.Count > 0)
            {
                Pn = true;
            }
            else
            {
                Pn = false;
            }
            return Pn;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataSet getPollInfo(int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetPollInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataSet getPollCountries()
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetPollCountries", con);
            scom.CommandType = CommandType.StoredProcedure;        
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollcountries = new DataSet();
            sda.Fill(dspollcountries);
            con.Close();
            return dspollcountries;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet getPollCities(string countryname)
    {
        DataSet dspoll = new DataSet();
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetPollCities", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollcountry", SqlDbType.VarChar)).Value = countryname;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return dspoll;
    }
    
    public DataSet getPollInfofromparent(int parentid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getPollIdfromparent", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@parentpollId", SqlDbType.Int)).Value = parentid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataSet getResInfo(int respondentId)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getSrcUrl", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@respondentId", SqlDbType.Int)).Value = respondentId;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataSet getVotesInfo(int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetPollVisitsInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataSet getPollLicenceInfo(int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetPollLicInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet updateDomainname(int userid,string domainname)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateDomainName", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@domain", SqlDbType.NVarChar)).Value = domainname;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataTable getAnswerOptions(int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetPollAnswerOptions", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            dt = dspoll.Tables[0];
            return dt;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
    }
    
    public void SaveAnswerOptions(string AnswerOption, int PollId, string InvitationThankYouMessage)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_SaveAnswerOptions", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@AnswerOption", SqlDbType.NVarChar)).Value = AnswerOption;
            scom.Parameters.Add(new SqlParameter("@InvitationThankYouMessage", SqlDbType.NVarChar)).Value = InvitationThankYouMessage;
            scom.Parameters.Add(new SqlParameter("@PollId", SqlDbType.Int)).Value = PollId;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UpdateAnswerOptions(int AnswerId, string AnswerOption, string InvitationThankYouMessage)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateAnswerOptions", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@AnswerOption", SqlDbType.NVarChar)).Value = AnswerOption;
            scom.Parameters.Add(new SqlParameter("@InvitationThankYouMessage", SqlDbType.NVarChar)).Value = InvitationThankYouMessage;
            scom.Parameters.Add(new SqlParameter("@AnswerId", SqlDbType.Int)).Value = AnswerId;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void RemoveAnswerOption(int AnswerId, string AnswerOption)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_RemoveAnswerOption", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@AnswerOption", SqlDbType.VarChar)).Value = AnswerOption;
            scom.Parameters.Add(new SqlParameter("@AnswerId", SqlDbType.Int)).Value = AnswerId;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void InsertintoPollInfo(int PollId, string Question, string VideoCode, string Imagelink, int Questiontype, int Commentreq, int uniqueRespondent, string votebuttontext
        , string invitationmessage, int isemailrequired, int isphonerequired, string redirecturl, int showsubmitbutton, string submitbuttontext, int issamethankyoumessageused)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_PollUpdate", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@Question", SqlDbType.NVarChar)).Value = Question;
            scom.Parameters.Add(new SqlParameter("@VideoCode", SqlDbType.VarChar)).Value = VideoCode;
            scom.Parameters.Add(new SqlParameter("@Imagelink", SqlDbType.VarChar)).Value = Imagelink;
            scom.Parameters.Add(new SqlParameter("@Questiontype", SqlDbType.VarChar)).Value = Questiontype;
            scom.Parameters.Add(new SqlParameter("@Commentreq", SqlDbType.Int)).Value = Commentreq;
            scom.Parameters.Add(new SqlParameter("@uniquerespondent", SqlDbType.VarChar)).Value = uniqueRespondent.ToString();
            scom.Parameters.Add(new SqlParameter("@votebuttontext", SqlDbType.VarChar)).Value = votebuttontext.ToString();
            scom.Parameters.Add(new SqlParameter("@invitationmessage", SqlDbType.VarChar)).Value = invitationmessage.ToString();
            scom.Parameters.Add(new SqlParameter("@redirecturl", SqlDbType.VarChar)).Value = redirecturl.ToString();
            scom.Parameters.Add(new SqlParameter("@submitbuttontext", SqlDbType.VarChar)).Value = submitbuttontext.ToString();
            scom.Parameters.Add(new SqlParameter("@isemailrequired", SqlDbType.Int)).Value = isemailrequired;
            scom.Parameters.Add(new SqlParameter("@isphonerequired", SqlDbType.Int)).Value = isphonerequired;
            scom.Parameters.Add(new SqlParameter("@showsubmitbutton", SqlDbType.Int)).Value = showsubmitbutton;
            scom.Parameters.Add(new SqlParameter("@issamethankyoumessageused", SqlDbType.Int)).Value = issamethankyoumessageused;
            
            scom.Parameters.Add(new SqlParameter("@PollId", SqlDbType.Int)).Value = PollId;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void InsertPollSettingsFromExistingPoll(int copyFromPollId, int newPollId)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_CopyPollSettings", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@copyfrompollid", SqlDbType.Int)).Value = copyFromPollId;
            scom.Parameters.Add(new SqlParameter("@newpollid", SqlDbType.Int)).Value = newPollId;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void InsertintoPollSettings(int pollid, string pollbgcolor, string questionfont, string questionbgcolor, string questioncolor, string questionstyle, 
        string questionfw, string questionuline, string questionalign, string questionfs, string answerfont, string answercolor, string answerfw, string answeruline, 
        string answerstyle, string answeralign, string answerfs, string votebgcolor, string votefont, string votefs, string votestyle, string voteul, string votecolor,
        string votefw, string votealign, string sponsorlogo, string sponsortext,string logolink, string messagebgcolor, string messagecolor, string messagefont, string messagestyle, string messageul,
        string messagealign, string messagefs, string messagefw, string googletext, string twittertext, string fbtext, int votecount, string sharetext, string votetext, int chartwidth, int chartheight, string weblinkbgcolor)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_InsertPollSettingsInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollbgcolor", SqlDbType.VarChar)).Value = pollbgcolor;
            scom.Parameters.Add(new SqlParameter("@questionfont", SqlDbType.VarChar)).Value = questionfont;
            scom.Parameters.Add(new SqlParameter("@questioncolor", SqlDbType.VarChar)).Value = questioncolor;
            scom.Parameters.Add(new SqlParameter("@questionbgcolor", SqlDbType.VarChar)).Value = questionbgcolor;
            scom.Parameters.Add(new SqlParameter("@questionstyle", SqlDbType.VarChar)).Value = questionstyle;
            scom.Parameters.Add(new SqlParameter("@questionfw", SqlDbType.VarChar)).Value = questionfw;
            scom.Parameters.Add(new SqlParameter("@questionuline", SqlDbType.VarChar)).Value = questionuline;
            scom.Parameters.Add(new SqlParameter("@questionalign", SqlDbType.VarChar)).Value = questionalign;
            scom.Parameters.Add(new SqlParameter("@questionfs", SqlDbType.VarChar)).Value = questionfs;
            scom.Parameters.Add(new SqlParameter("@answerfont", SqlDbType.VarChar)).Value = answerfont;
            scom.Parameters.Add(new SqlParameter("@answercolor", SqlDbType.VarChar)).Value = answercolor;
            scom.Parameters.Add(new SqlParameter("@answerfw", SqlDbType.VarChar)).Value = answerfw;
            scom.Parameters.Add(new SqlParameter("@answeruline", SqlDbType.VarChar)).Value = answeruline;
            scom.Parameters.Add(new SqlParameter("@answerstyle", SqlDbType.VarChar)).Value = answerstyle;
            scom.Parameters.Add(new SqlParameter("@answeralign", SqlDbType.VarChar)).Value = answeralign;
            scom.Parameters.Add(new SqlParameter("@answerfs", SqlDbType.VarChar)).Value = answerfs;
            scom.Parameters.Add(new SqlParameter("@votebgcolor", SqlDbType.VarChar)).Value = votebgcolor;
            scom.Parameters.Add(new SqlParameter("@votefont", SqlDbType.VarChar)).Value = votefont;
            scom.Parameters.Add(new SqlParameter("@votefs", SqlDbType.VarChar)).Value = votefs;
            scom.Parameters.Add(new SqlParameter("@votestyle", SqlDbType.VarChar)).Value = votestyle;
            scom.Parameters.Add(new SqlParameter("@voteul", SqlDbType.VarChar)).Value = voteul;
            scom.Parameters.Add(new SqlParameter("@votecolor", SqlDbType.VarChar)).Value = votecolor;
            scom.Parameters.Add(new SqlParameter("@votefw", SqlDbType.VarChar)).Value = votefw;
            scom.Parameters.Add(new SqlParameter("@votealign", SqlDbType.VarChar)).Value = votealign;
            scom.Parameters.Add(new SqlParameter("@sponsorlogo", SqlDbType.VarChar)).Value = sponsorlogo;
            scom.Parameters.Add(new SqlParameter("@sponsortext", SqlDbType.VarChar)).Value = sponsortext;
            scom.Parameters.Add(new SqlParameter("@logolink", SqlDbType.VarChar)).Value = logolink;
            scom.Parameters.Add(new SqlParameter("@messagecolor", SqlDbType.VarChar)).Value = messagecolor;
            scom.Parameters.Add(new SqlParameter("@messagebgcolor", SqlDbType.VarChar)).Value = messagebgcolor;
            scom.Parameters.Add(new SqlParameter("@messagefont", SqlDbType.VarChar)).Value = messagefont;
            scom.Parameters.Add(new SqlParameter("@messagestyle", SqlDbType.VarChar)).Value = messagestyle;
            scom.Parameters.Add(new SqlParameter("@messageul", SqlDbType.VarChar)).Value = messageul;
            scom.Parameters.Add(new SqlParameter("@messagealign", SqlDbType.VarChar)).Value = messagealign;
            scom.Parameters.Add(new SqlParameter("@messagefs", SqlDbType.VarChar)).Value = messagefs;
            scom.Parameters.Add(new SqlParameter("@messagefw", SqlDbType.VarChar)).Value = messagefw;
            scom.Parameters.Add(new SqlParameter("@googletext", SqlDbType.VarChar)).Value = googletext;
            scom.Parameters.Add(new SqlParameter("@twittertext", SqlDbType.VarChar)).Value = twittertext;
            scom.Parameters.Add(new SqlParameter("@fbtext", SqlDbType.VarChar)).Value = fbtext;
            scom.Parameters.Add(new SqlParameter("@votecount", SqlDbType.Int)).Value = votecount;
            scom.Parameters.Add(new SqlParameter("@sharetext", SqlDbType.NVarChar)).Value = sharetext;
            scom.Parameters.Add(new SqlParameter("@votetext", SqlDbType.NVarChar)).Value = votetext;
            scom.Parameters.Add(new SqlParameter("@chartwidth", SqlDbType.Int)).Value = chartwidth;
            scom.Parameters.Add(new SqlParameter("@chartheight", SqlDbType.Int)).Value = chartheight;
            scom.Parameters.Add(new SqlParameter("@weblinkbgcolor", SqlDbType.NVarChar)).Value = weblinkbgcolor;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataTable getPollSettingsInfo(int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetPollSettingsInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            dt = dspoll.Tables[0];
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void UpdatePollinfo(int pollid, string resultaccess, string resulttext, string poweredby)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@resultaccess", SqlDbType.VarChar)).Value = resultaccess;
            scom.Parameters.Add(new SqlParameter("@resulttext", SqlDbType.VarChar)).Value = resulttext;
            scom.Parameters.Add(new SqlParameter("@poweredby", SqlDbType.VarChar)).Value = poweredby;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void UpdateSliderPollsettings(string position, string sliderdisplay, int freqofvisit,int pollid,string countryname,string cityname,string devicename)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateSettingsSliderInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@position", SqlDbType.VarChar)).Value = position;
            scom.Parameters.Add(new SqlParameter("@sliderdisplay", SqlDbType.VarChar)).Value = sliderdisplay;
            scom.Parameters.Add(new SqlParameter("@freqofvisit", SqlDbType.Int)).Value = freqofvisit;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = countryname;
            scom.Parameters.Add(new SqlParameter("@cityname", SqlDbType.VarChar)).Value = cityname;
            scom.Parameters.Add(new SqlParameter("@device", SqlDbType.VarChar)).Value = devicename;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void UpdatePollSettings(int pollid, string pollbgcolor, string questionfont, string questionbgcolor, string questioncolor, string questionstyle,
       string questionfw, string questionuline, string questionalign, string questionfs, string answerfont, string answercolor, string answerfw, string answeruline,
       string answerstyle, string answeralign, string answerfs, string votebgcolor, string votefont, string votefs, string votestyle, string voteul, string votecolor,
       string votefw, string votealign, string sponsorlogo, string sponsortext, string logolink, string messagebgcolor, string messagecolor, string messagefont, string messagestyle, string messageul,
       string messagealign, string messagefs, string messagefw, string googletext, string twittertext, string fbtext, int votecount, string sharetext, string votetext, int chartwidth, int chartheight, int autoadjustdimensions, string weblinkbgcolor)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateSettingsInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollbgcolor", SqlDbType.VarChar)).Value = pollbgcolor;
            scom.Parameters.Add(new SqlParameter("@questionfont", SqlDbType.VarChar)).Value = questionfont;
            scom.Parameters.Add(new SqlParameter("@questioncolor", SqlDbType.VarChar)).Value = questioncolor;
            scom.Parameters.Add(new SqlParameter("@questionbgcolor", SqlDbType.VarChar)).Value = questionbgcolor;
            scom.Parameters.Add(new SqlParameter("@questionstyle", SqlDbType.VarChar)).Value = questionstyle;
            scom.Parameters.Add(new SqlParameter("@questionfw", SqlDbType.VarChar)).Value = questionfw;
            scom.Parameters.Add(new SqlParameter("@questionuline", SqlDbType.VarChar)).Value = questionuline;
            scom.Parameters.Add(new SqlParameter("@questionalign", SqlDbType.VarChar)).Value = questionalign;
            scom.Parameters.Add(new SqlParameter("@questionfs", SqlDbType.VarChar)).Value = questionfs;
            scom.Parameters.Add(new SqlParameter("@answerfont", SqlDbType.VarChar)).Value = answerfont;
            scom.Parameters.Add(new SqlParameter("@answercolor", SqlDbType.VarChar)).Value = answercolor;
            scom.Parameters.Add(new SqlParameter("@answerfw", SqlDbType.VarChar)).Value = answerfw;
            scom.Parameters.Add(new SqlParameter("@answeruline", SqlDbType.VarChar)).Value = answeruline;
            scom.Parameters.Add(new SqlParameter("@answerstyle", SqlDbType.VarChar)).Value = answerstyle;
            scom.Parameters.Add(new SqlParameter("@answeralign", SqlDbType.VarChar)).Value = answeralign;
            scom.Parameters.Add(new SqlParameter("@answerfs", SqlDbType.VarChar)).Value = answerfs;
            scom.Parameters.Add(new SqlParameter("@votebgcolor", SqlDbType.VarChar)).Value = votebgcolor;
            scom.Parameters.Add(new SqlParameter("@votefont", SqlDbType.VarChar)).Value = votefont;
            scom.Parameters.Add(new SqlParameter("@votefs", SqlDbType.VarChar)).Value = votefs;
            scom.Parameters.Add(new SqlParameter("@votestyle", SqlDbType.VarChar)).Value = votestyle;
            scom.Parameters.Add(new SqlParameter("@voteul", SqlDbType.VarChar)).Value = voteul;
            scom.Parameters.Add(new SqlParameter("@votecolor", SqlDbType.VarChar)).Value = votecolor;
            scom.Parameters.Add(new SqlParameter("@votefw", SqlDbType.VarChar)).Value = votefw;
            scom.Parameters.Add(new SqlParameter("@votealign", SqlDbType.VarChar)).Value = votealign;
            scom.Parameters.Add(new SqlParameter("@sponsorlogo", SqlDbType.VarChar)).Value = sponsorlogo;
            scom.Parameters.Add(new SqlParameter("@sponsortext", SqlDbType.VarChar)).Value = sponsortext;
            scom.Parameters.Add(new SqlParameter("@logolink", SqlDbType.VarChar)).Value = logolink;
            scom.Parameters.Add(new SqlParameter("@messagecolor", SqlDbType.VarChar)).Value = messagecolor;
            scom.Parameters.Add(new SqlParameter("@messagebgcolor", SqlDbType.VarChar)).Value = messagebgcolor;
            scom.Parameters.Add(new SqlParameter("@messagefont", SqlDbType.VarChar)).Value = messagefont;
            scom.Parameters.Add(new SqlParameter("@messagestyle", SqlDbType.VarChar)).Value = messagestyle;
            scom.Parameters.Add(new SqlParameter("@messageul", SqlDbType.VarChar)).Value = messageul;
            scom.Parameters.Add(new SqlParameter("@messagealign", SqlDbType.VarChar)).Value = messagealign;
            scom.Parameters.Add(new SqlParameter("@messagefs", SqlDbType.VarChar)).Value = messagefs;
            scom.Parameters.Add(new SqlParameter("@messagefw", SqlDbType.VarChar)).Value = messagefw;
            scom.Parameters.Add(new SqlParameter("@googletext", SqlDbType.VarChar)).Value = googletext;
            scom.Parameters.Add(new SqlParameter("@twittertext", SqlDbType.VarChar)).Value = twittertext;
            scom.Parameters.Add(new SqlParameter("@fbtext", SqlDbType.VarChar)).Value = fbtext;
            scom.Parameters.Add(new SqlParameter("@votecount", SqlDbType.Int)).Value = votecount;
            scom.Parameters.Add(new SqlParameter("@sharetext", SqlDbType.NVarChar)).Value = sharetext;
            scom.Parameters.Add(new SqlParameter("@votetext", SqlDbType.NVarChar)).Value = votetext;
            scom.Parameters.Add(new SqlParameter("@chartwidth", SqlDbType.Int)).Value = chartwidth;
            scom.Parameters.Add(new SqlParameter("@chartheight", SqlDbType.Int)).Value = chartheight;
            scom.Parameters.Add(new SqlParameter("@autoadjustdimensions", SqlDbType.Int)).Value = autoadjustdimensions;
            scom.Parameters.Add(new SqlParameter("@weblinkbgcolor", SqlDbType.NVarChar)).Value = weblinkbgcolor;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UpdateSponsorDetails(int pollid, string sponsorlogo, string sponsortext, string logolink)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateSponsorDetails", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@sponsorlogo", SqlDbType.VarChar)).Value = sponsorlogo;
            scom.Parameters.Add(new SqlParameter("@sponsortext", SqlDbType.VarChar)).Value = sponsortext;
            scom.Parameters.Add(new SqlParameter("@logolink", SqlDbType.VarChar)).Value = logolink;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SaveLaunchInfo(int pollid, int unique, string polllaunchurl, string embedcode, int launchtype)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateLaunchPollInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@polllaunchurl", SqlDbType.VarChar)).Value = polllaunchurl;
            scom.Parameters.Add(new SqlParameter("@unique", SqlDbType.Int)).Value = unique;
            scom.Parameters.Add(new SqlParameter("@embedcode", SqlDbType.VarChar)).Value = embedcode;
            scom.Parameters.Add(new SqlParameter("@launchtype", SqlDbType.VarChar)).Value = launchtype;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SaveIframeInfo(int pollid, int width, int height, int recwidth, int recheight)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateIframeInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@width", SqlDbType.Int)).Value = width;
            scom.Parameters.Add(new SqlParameter("@height", SqlDbType.Int)).Value = height;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@recwidth", SqlDbType.Int)).Value = recwidth;
            scom.Parameters.Add(new SqlParameter("@recheight", SqlDbType.Int)).Value = recheight;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SaveImageSpec(int pollid, int width, int height)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateImageSpec", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@width", SqlDbType.Int)).Value = width;
            scom.Parameters.Add(new SqlParameter("@height", SqlDbType.Int)).Value = height;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void SaveModiyIframeSpec(int pollid,  int height)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateModifyIframeSpec", con);
            scom.CommandType = CommandType.StoredProcedure;           
            scom.Parameters.Add(new SqlParameter("@recheight", SqlDbType.Int)).Value = height;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void InsertintoQuestion(int PollId, string questiontext)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_InsertIntoQuestionInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@questiontext", SqlDbType.NVarChar)).Value = questiontext;
            scom.Parameters.Add(new SqlParameter("@PollId", SqlDbType.Int)).Value = PollId;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataTable GetQuestionInfo(int PollId)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetQuestionInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@PollId", SqlDbType.Int)).Value = PollId;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            DataTable dt = new DataTable();
            dt = dspoll.Tables[0];
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void UpdateQuestion(int PollId, string questiontext)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateQuestionInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@questiontext", SqlDbType.NVarChar)).Value = questiontext;
            scom.Parameters.Add(new SqlParameter("@PollId", SqlDbType.Int)).Value = PollId;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public int InsertIntoRespondent(int pollid, string status,string country,string state,string city,string ipaddress,string longitude,string latitude, string device,string platform, string brand,string mobplatform,string srcurl)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_InsertIntoRespondent", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar)).Value = status;
            scom.Parameters.Add(new SqlParameter("@country", SqlDbType.VarChar)).Value = country;
            scom.Parameters.Add(new SqlParameter("@state", SqlDbType.VarChar)).Value = state;
            scom.Parameters.Add(new SqlParameter("@city", SqlDbType.VarChar)).Value = city;
            scom.Parameters.Add(new SqlParameter("@ipaddress", SqlDbType.VarChar)).Value = ipaddress;
            scom.Parameters.Add(new SqlParameter("@longitude", SqlDbType.VarChar)).Value = longitude;
            scom.Parameters.Add(new SqlParameter("@latitude", SqlDbType.VarChar)).Value = latitude;
            scom.Parameters.Add(new SqlParameter("@device", SqlDbType.VarChar)).Value = device;
            scom.Parameters.Add(new SqlParameter("@platform", SqlDbType.VarChar)).Value = platform;
            scom.Parameters.Add(new SqlParameter("@brand", SqlDbType.VarChar)).Value = brand;
            scom.Parameters.Add(new SqlParameter("@mobplatform", SqlDbType.VarChar)).Value = mobplatform;
            scom.Parameters.Add(new SqlParameter("@srcurl", SqlDbType.VarChar)).Value = srcurl;
            scom.Parameters.Add(new SqlParameter("@respondentid", SqlDbType.Int)).Value = "0";
            scom.Parameters["@respondentid"].Direction = ParameterDirection.Output;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            int respondentid = Convert.ToInt32(scom.Parameters["@respondentid"].Value);
            return respondentid;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void UpdateIntoRespondent(int respondentid,string status, string comments)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateIntoRespondent", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar)).Value = status;
            scom.Parameters.Add(new SqlParameter("@comments", SqlDbType.VarChar)).Value = comments;
            scom.Parameters.Add(new SqlParameter("@respondentid", SqlDbType.Int)).Value = respondentid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void InsertIntoResponse(int respondentid, int answerid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_InsertIntoResponses", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@answerid", SqlDbType.Int)).Value = answerid;
            scom.Parameters.Add(new SqlParameter("@respondentid", SqlDbType.Int)).Value = respondentid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet DeletePoll(int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_DeletePoll", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public int DeletePollByUserId(int userid, int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_DeletePollByUser", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.ExecuteNonQuery();
            con.Close();
            return 0;
        }
        catch (Exception)
        {
            return 1;
        }
    }
    
    public void UpdateStatus(int pollid, string status)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateStatus", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar)).Value = status;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
   
    public int UpdateStatusByUser(int userid, int pollid, string status)
    {

        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateStatusByUser", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar)).Value = status;
            scom.ExecuteNonQuery();
            con.Close();
            return 0;
        }
        catch (Exception)
        {
            return 1;
        }
    }

    public void UpdateSuperpollStatus(int superpollpollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("closeActivePolls", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@parentPollId", SqlDbType.Int)).Value = superpollpollid;           
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataTable getPollThemes(int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetPollUserSettingsInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            DataTable dt = new DataTable();
            sda.Fill(dspoll);
            con.Close();
            dt = dspoll.Tables[0];
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataTable getsuperPollThemes(int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetsuperPollUserSettingsInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            DataTable dt = new DataTable();
            sda.Fill(dspoll);
            con.Close();
            dt = dspoll.Tables[0];
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataTable getsuperPollpageurls()
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetsuperPollpageURLs", con);
            scom.CommandType = CommandType.StoredProcedure;          
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            DataTable dt = new DataTable();
            sda.Fill(dspoll);
            con.Close();
            dt = dspoll.Tables[0];
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataSet getpollchartinfo(int pollid)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_pollchart", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspollinfo = new DataSet();
            sda.Fill(dspollinfo);
            con.Close();
            return dspollinfo;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UpdatePollLicenseType(int userid, string polllicensetype, int subscriptiontype)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollLicenseType", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@polllicensetype", SqlDbType.VarChar)).Value = polllicensetype;
            scom.Parameters.Add(new SqlParameter("@subscriptiontype", SqlDbType.Int)).Value = subscriptiontype;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UpdatePollLicenseTypeOnSignUp(int userid, string polllicensetype, int subscriptiontype)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollLicenseTypeOnSignUp", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@polllicensetype", SqlDbType.VarChar)).Value = polllicensetype;
            scom.Parameters.Add(new SqlParameter("@subscriptiontype", SqlDbType.Int)).Value = subscriptiontype;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UpdatePollPaymentType(int userid, string polllicensetype, int orderid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollPaymentInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@polllicensetype", SqlDbType.VarChar)).Value = polllicensetype;
            scom.Parameters.Add(new SqlParameter("@ordrId", SqlDbType.Int)).Value = orderid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet getPollOrder(string orderid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getpollOrder", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@Orderid", SqlDbType.VarChar)).Value = orderid;         
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet getPollOrderInfo(int orderid,int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetPollPaymentOrderID", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@Orderid", SqlDbType.Int)).Value = orderid;
            scom.Parameters.Add(new SqlParameter("@userID", SqlDbType.Int)).Value = userid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UpdateOnlySubscriptionType(int type, string activity, int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateOnlyUserSubType", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@activity", SqlDbType.VarChar)).Value = activity;
            scom.Parameters.Add(new SqlParameter("@type", SqlDbType.Int)).Value = type;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void UpdateUserSubscriptionType(int type,string activity, int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateUserSubType", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@activity", SqlDbType.VarChar)).Value = activity;
            scom.Parameters.Add(new SqlParameter("@type", SqlDbType.Int)).Value = type;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void UpdateUserLastActivity(string activity, int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateLastActivity", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@activity", SqlDbType.VarChar)).Value = activity;  
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UpdateEmailtoSurveyUser(int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateEmailtoSurveyUser", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;            
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public int getSubType(int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetUserSubType", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            int subtype = Convert.ToInt32(dspoll.Tables[0].Rows[0][0].ToString());
            return subtype;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet GetCCPollInvoiceInfo(int userid,int orderid,int historyid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetCCPollInvoiceInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@orderID", SqlDbType.Int)).Value = orderid;
            scom.Parameters.Add(new SqlParameter("@historyID", SqlDbType.Int)).Value = historyid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public DataSet updateImage(int pollid, string imagetext)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollImage", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@imagetext", SqlDbType.VarChar)).Value = imagetext;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void updatePollNameOnly(int pollid, string pollname)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollNameOnly", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@pollname", SqlDbType.VarChar)).Value = pollname;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void updateReplacedPollNameOnlyForThisUser(int userid, int pollid, string pollname)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdateReplacedPollNameOnlyForThisUser", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@pollname", SqlDbType.VarChar)).Value = pollname;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataTable getImageNames()
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetImageNames", con);
            scom.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            DataTable dt = new DataTable();
            dt = dspoll.Tables[0];
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataTable getLicencePoll(int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetPollLicence", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            DataTable dt = new DataTable();
            dt = dspoll.Tables[0];
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void deleteAnswerOptions(int pollid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_DeletePollAnswerOptions", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataTable getUsersForPollsPromo()
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetUsersForPollsPromo", con);
            scom.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            DataTable dt = new DataTable();
            dt = dspoll.Tables[0];
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UpdatePollCampaignUser(int userid)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollCampaignUser", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public int GetSurveyCreditCount(int userid)
    {
        int count = 0;
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetTotalCreditsCount", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            if (dspoll.Tables.Count > 0) {
                if (dspoll.Tables[0].Rows.Count > 0)
                {
                    Int32.TryParse(dspoll.Tables[0].Rows[0][0].ToString(), out count);
                }
            }
            con.Close();
        }
        catch (Exception ex)
        {
        }
        return count;
    }

    public DataTable getAllUsersPollLicences()
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetAllUsersPollLicences", con);
            scom.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            DataTable dt = new DataTable();
            dt = dspoll.Tables[0];
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SavePollPaymentInformation(string paymentid, decimal amount, string productslug, string producttitle, int orderid, string status, string type)
    {

        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_InsertInstaMojoPaymentInfo", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@paymentid", SqlDbType.VarChar)).Value = paymentid;
            scom.Parameters.Add(new SqlParameter("@amount", SqlDbType.Decimal)).Value = amount;
            scom.Parameters.Add(new SqlParameter("@producttitle", SqlDbType.VarChar)).Value = producttitle;
            scom.Parameters.Add(new SqlParameter("@productslug", SqlDbType.VarChar)).Value = productslug;
            scom.Parameters.Add(new SqlParameter("@orderid", SqlDbType.Int)).Value = orderid;
            scom.Parameters.Add(new SqlParameter("@status", SqlDbType.VarChar)).Value = status;
            scom.Parameters.Add(new SqlParameter("@Type", SqlDbType.VarChar)).Value = type;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataTable getPollPaymentInformation(string paymentid, string transactiontype) //USE FOR POLLS AND SURVEYS 
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetInstaMojoPaymentInformation", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@paymentid", SqlDbType.VarChar)).Value = paymentid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            DataTable dt = new DataTable();
            if (transactiontype == "Poll")
            {
                dt = dspoll.Tables[0];
            }
            else {//FOR SURVEYS
                dt = dspoll.Tables[1];
            }
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet getInstaMojoForPolls(string licensetype)//USE THIS FUNCTION FOR BOTH SURVEYS AND POLL PRODUCTS
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_getInstaMojoProducts", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = licensetype;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
            return dspoll;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UpdatePollDetails(int pollid, int userid, string resultaccess, string resulttext, string poweredby, int questiontype, string questiontext, int commentreq, string videolink, string imagelink, string uniquerespondent) {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollDetails", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@resultaccess", SqlDbType.VarChar)).Value = resultaccess;
            scom.Parameters.Add(new SqlParameter("@resulttext", SqlDbType.VarChar)).Value = resulttext;
            scom.Parameters.Add(new SqlParameter("@poweredby", SqlDbType.VarChar)).Value = poweredby;
            scom.Parameters.Add(new SqlParameter("@questiontype", SqlDbType.Int)).Value = questiontype;
            scom.Parameters.Add(new SqlParameter("@questiontext", SqlDbType.VarChar)).Value = questiontext;
            scom.Parameters.Add(new SqlParameter("@commentreq", SqlDbType.Bit)).Value = commentreq;
            scom.Parameters.Add(new SqlParameter("@videolink", SqlDbType.VarChar)).Value = videolink;
            scom.Parameters.Add(new SqlParameter("@imagelink", SqlDbType.VarChar)).Value = imagelink;
            scom.Parameters.Add(new SqlParameter("@imagelink", SqlDbType.VarChar)).Value = uniquerespondent;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public void UpdatePollDimensions(int pollid, int userid, int iframeheight, int iframewidth, int recmheight, int recmwidth)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollDimensions", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@userid", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@iframeheight", SqlDbType.Int)).Value = iframeheight;
            scom.Parameters.Add(new SqlParameter("@iframewidth", SqlDbType.Int)).Value = iframewidth;
            scom.Parameters.Add(new SqlParameter("@recmheight", SqlDbType.Int)).Value = recmheight;
            scom.Parameters.Add(new SqlParameter("@recmwidth", SqlDbType.Int)).Value = recmwidth;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public void UpdatePollLeadGen(int pollid, int respondentid, string phone, string email, string redirects)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollLeadGen", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@pollid", SqlDbType.Int)).Value = pollid;
            scom.Parameters.Add(new SqlParameter("@respondentid", SqlDbType.Int)).Value = respondentid;
            scom.Parameters.Add(new SqlParameter("@phone", SqlDbType.VarChar)).Value = phone;
            scom.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar)).Value = email;
            scom.Parameters.Add(new SqlParameter("@redirects", SqlDbType.VarChar)).Value = redirects;
            
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public void UpdatePollWebLinkBgColor(int pollid, int userid, string weblinkbgcolor)
    {
        try
        {
            SqlConnection con = new SqlConnection();
            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_UpdatePollWebLinkBgColor", con);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@weblinkbgcolor", SqlDbType.NVarChar)).Value = weblinkbgcolor;
            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;
            scom.Parameters.Add(new SqlParameter("@PollId", SqlDbType.Int)).Value = pollid;
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dspoll = new DataSet();
            sda.Fill(dspoll);
            con.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}