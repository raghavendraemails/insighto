﻿using System;
using System.Collections.Generic;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;

namespace App_Code
{
    /// <summary>
    /// Summary description for SurveyRespondentControlBase
    /// </summary>
    public class SurveyRespondentControlBase : UserControlBase
    {
        private SurveyQuestionAndAnswerOptions _surveyQuestionAndAnswerOptions = new SurveyQuestionAndAnswerOptions();
        public SurveyRespondentPageBase RespondentPageBase
        {
            get
            {
                return Page as SurveyRespondentPageBase;
            }
        }


        public virtual SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions
        {
            get
            {
                return _surveyQuestionAndAnswerOptions;
            }
            set
            {
                _surveyQuestionAndAnswerOptions = value;
            }
        }

        public osm_surveyquestion Question
        {
            get
            {
                if (SurveyQuestionAndAnswerOptions == null)
                    throw new InvalidOperationException("SurveyQuestionAndAnswerOptions is null, Assign SurveyQuestionAndAnswerOptions before accessing this property");

                return SurveyQuestionAndAnswerOptions.Question;
            }
        }

        public QuestionType QuestionType
        {
            get
            {
                if (Question == null)
                    throw new InvalidOperationException("Question is null, Assign SurveyQuestionAndAnswerOptions before accessing this property");

                return SurveyQuestionAndAnswerOptions.QuestionType;
            }
        }

        public bool IsSkipLogicEnabled
        {
            get
            {
                if (Question == null)
                    throw new InvalidOperationException("Question is null, Assign SurveyQuestionAndAnswerOptions before accessing this property");

                return Question.SKIP_LOGIC > 0;
            }
// ReSharper disable ValueParameterNotUsed
            set { }
// ReSharper restore ValueParameterNotUsed
        }

        public bool IsOtherOptionEnabled
        {
            get
            {
                if (Question == null)
                    throw new InvalidOperationException("Question is null, Assign SurveyQuestionAndAnswerOptions before accessing this property");

                return Question.OTHER_ANS > 0;
            }
// ReSharper disable ValueParameterNotUsed
            set { }
// ReSharper restore ValueParameterNotUsed
        }

        public virtual List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                throw new NotImplementedException("SurveyRespondentControlBase.SelectedAnswerOptions not implemented");
            }
            set
            {
                throw new NotImplementedException("SurveyRespondentControlBase.SelectedAnswerOptions not implemented");
            }
        }

        private List<osm_responsequestions> _respondentAnswerOptions = new List<osm_responsequestions>();
        public virtual List<osm_responsequestions> RespondentAnswerOptions
        {
            get
            {
                return _respondentAnswerOptions;
            }
            set {
                _respondentAnswerOptions = value;
            }
        }

        public virtual bool IsReadOnly { get; set; }

        public virtual string ControlIdPrefix { get; set; }

        public virtual int SelectedAnwerValue { get; set; }

        public virtual int LastQuestionSeqNo
        {
            get
            {
                if (Question == null)
                    throw new InvalidOperationException("Question is null, Assign SurveyQuestionAndAnswerOptions before accessing this property");

                return ValidationHelper.GetInteger(Question.QUESTION_SEQ, 0);
            }
// ReSharper disable ValueParameterNotUsed
            set { }
// ReSharper restore ValueParameterNotUsed
        }

        public virtual int AnswerId { get; set;}
        
        public bool EnableValidation { get; set; }
    }
}