﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NetworkShareStatus
/// </summary>
public enum NetworkShareCodesLinkedin
{
	
        Shared = 0,
        Expired = 1,
        Reshared = 2,
        Renewed = 3,
        Error = -1
    
}