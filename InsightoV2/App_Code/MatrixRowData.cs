﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MatrixRowData
/// </summary>
public class MatrixRowData
{
    /// <summary>
    /// Gets or sets the row id.
    /// </summary>
    /// <value>
    /// The row id.
    /// </value>
    public int RowId { get; set; }
    /// <summary>
    /// Gets or sets the Top Header dimension.
    /// </summary>
    /// <value>
    /// The Top Header dimension.3D matrix only contains this data
    /// </value>
    public string TopHeaderAttribute { get; set; }
    /// <summary>
    /// Gets or sets the Row attribute.
    /// </summary>
    /// <value>
    /// The Row attribute.
    /// </value>
    public string RowAttribute { get; set; }
    /// <summary>
    /// Gets or sets the Column Attribute.
    /// </summary>
    /// <value>
    /// The Column Attribute.
    /// </value>
    public string ColumnAttribute { get; set; }

    /// <summary>
    /// Gets or sets the Selected state.
    /// </summary>
    public bool Selected { get; set; }
}