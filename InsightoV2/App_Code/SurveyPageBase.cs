﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System.Data;

/// <summary>
/// Summary description for BasePage
/// </summary>
public class SurveyPageBase : SecurePageBase
{
    SurveyCore surcore = new SurveyCore();
    public SurveyPageBase()
    {
    }
   
    public int SurveyID
    {
        get
        {

            var key = QueryStringHelper.GetString("key");
            if (String.IsNullOrEmpty(key))
                return 0;

            Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
            return ValidationHelper.GetInteger(ht["SurveyId"], 0);

        }
    }
    
    /// <summary>
    /// Gets the survey ques and answer options list.
    /// </summary>
    private List<SurveyQuestionAndAnswerOptions> _surveyQuesAndAnswerOptionsList = new List<SurveyQuestionAndAnswerOptions>();
    public List<SurveyQuestionAndAnswerOptions> SurveyQuesAndAnswerOptionsList
    {
        get
        {
            if (!_surveyQuesAndAnswerOptionsList.Any())
                _surveyQuesAndAnswerOptionsList = ServiceFactory.GetService<RespondentService>().GetSurveyQuestionAndAnswerOptionsList(SurveyID, true, true);

            return _surveyQuesAndAnswerOptionsList;
        }
    }
    [Flags]
    enum LaunchMethod
    {
        Emailthr = 1,
        Webthr = 2,
        Embedthr = 4,
        Fbthr = 8,
        Twthr = 16
    }
    //ToDo: Refactor for performance
    private SurveyBasicInfoView _surveyBasicInfoViewWithId;
    public SurveyBasicInfoView SurveyBasicInfoViewWithId(int sid = 0)
    {

            DataSet dsppstype = surcore.getSurveyType(sid);

            string launchcnt = dsppstype.Tables[2].Rows[0][0].ToString();
            int launchcountval = Convert.ToInt32(launchcnt);
            int launchtypenumber = surcore.getLaunchTypeNumber(sid);
            LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
            if (_surveyBasicInfoViewWithId == null)
            {
            if ((LaunchMethod.Emailthr & launchnew) == LaunchMethod.Emailthr)
            {
                DataSet dsemailinfo = surcore.emaillaunchtypeinfo(sid);
                DataTable dtemailinfo = dsemailinfo.Tables[0];

                foreach (DataRow row in dtemailinfo.Rows)
                {
                     _surveyBasicInfoViewWithId = new SurveyBasicInfoView();
                     _surveyBasicInfoViewWithId.SURVEY_ID = Convert.ToInt32(row["SURVEY_ID"]);
                     _surveyBasicInfoViewWithId.SURVEY_NAME = row["SURVEY_NAME"].ToString();
                     _surveyBasicInfoViewWithId.SURVEY_CATEGORY = row["SURVEY_CATEGORY"].ToString();
                     _surveyBasicInfoViewWithId.FOLDER_ID = Convert.ToInt32(row["FOLDER_ID"]);
                     _surveyBasicInfoViewWithId.USERID = Convert.ToInt32(row["USERID"]);
                     _surveyBasicInfoViewWithId.CREATED_ON = Convert.ToDateTime(row["CREATED_ON"]);
                     _surveyBasicInfoViewWithId.DELETED = Convert.ToInt32(row["DELETED"]);
                     _surveyBasicInfoViewWithId.STATUS = row["STATUS"].ToString();
                     _surveyBasicInfoViewWithId.TEMPLATE_CATEGORY = row["TEMPLATE_CATEGORY"].ToString();
                     _surveyBasicInfoViewWithId.TEMPLATE_SUBCATEGORY = row["TEMPLATE_SUBCATEGORY"].ToString();
                     _surveyBasicInfoViewWithId.THEME = row["THEME"].ToString();
                     _surveyBasicInfoViewWithId.MODIFIED_ON = Convert.ToDateTime(row["MODIFIED_ON"]);
                     if (row["SURVEY_CLOSE_DATE"].ToString() != "")
                     {
                         _surveyBasicInfoViewWithId.SURVEY_CLOSE_DATE = Convert.ToDateTime(row["SURVEY_CLOSE_DATE"]);
                     }

                     _surveyBasicInfoViewWithId.CHECK_BLOCKWORDS = Convert.ToInt32(row["CHECK_BLOCKWORDS"]);
                     _surveyBasicInfoViewWithId.SurveyWidgetFlag = Convert.ToInt32(row["SurveyWidgetFlag"]);
                     _surveyBasicInfoViewWithId.SCHEDULE_ON = Convert.ToInt32(row["SCHEDULE_ON"]);
                     _surveyBasicInfoViewWithId.SCHEDULE_ON_ALERT = Convert.ToInt32(row["SCHEDULE_ON_ALERT"]);
                     _surveyBasicInfoViewWithId.SCHEDULE_ON_CLOSE = Convert.ToInt32(row["SCHEDULE_ON_CLOSE"]);
                     _surveyBasicInfoViewWithId.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS = Convert.ToInt32(row["SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS"]);
                     _surveyBasicInfoViewWithId.SCHEDULE_ON_CLOSE_ALERT = Convert.ToInt32(row["SCHEDULE_ON_CLOSE_ALERT"]);
                     _surveyBasicInfoViewWithId.SurveyControlStatus = Convert.ToInt32(row["SurveyControlStatus"]);
                     _surveyBasicInfoViewWithId.TIME_FOR_URL_REDIRECTION = row["TIME_FOR_URL_REDIRECTION"].ToString();
                     _surveyBasicInfoViewWithId.SCREEN_OUTS = Convert.ToInt32(row["SCREEN_OUTS"]);
                     _surveyBasicInfoViewWithId.PARTIAL_RESPONSES = Convert.ToInt32(row["PARTIAL_RESPONSES"]);
                     _surveyBasicInfoViewWithId.TOTAL_RESPONSES = Convert.ToInt32(row["TOTAL_RESPONSES"]);
                     _surveyBasicInfoViewWithId.SEND_TO = Convert.ToInt32(row["SEND_TO"]);
                     _surveyBasicInfoViewWithId.ONLAUNCH_ALERT = Convert.ToInt32(row["ONLAUNCH_ALERT"]);
                     _surveyBasicInfoViewWithId.CLOSE_ON_NO_RESPONDENTS = Convert.ToInt32(row["CLOSE_ON_NO_RESPONDENTS"]);
                     _surveyBasicInfoViewWithId.CONTINUOUS_SURVEY = Convert.ToInt32(row["CONTINUOUS_SURVEY"]);
                     _surveyBasicInfoViewWithId.UNIQUE_RESPONDENT = Convert.ToInt32(row["UNIQUE_RESPONDENT"]);
                     _surveyBasicInfoViewWithId.SAVE_AND_CONTINUE = Convert.ToInt32(row["SAVE_AND_CONTINUE"]);
                     _surveyBasicInfoViewWithId.AUTOMATIC_THANKYOU_EMAIL = Convert.ToInt32(row["AUTOMATIC_THANKYOU_EMAIL"]);
                     _surveyBasicInfoViewWithId.ALERT_ON_MIN_RESPONSES = Convert.ToInt32(row["ALERT_ON_MIN_RESPONSES"]);
                     _surveyBasicInfoViewWithId.ALERT_ON_MAX_RESPONSES = Convert.ToInt32(row["ALERT_ON_MAX_RESPONSES"]);
                     _surveyBasicInfoViewWithId.THANKYOU_PAGE = row["THANKYOU_PAGE"].ToString();
                     _surveyBasicInfoViewWithId.SCREENOUT_PAGE = row["SCREENOUT_PAGE"].ToString();
                     _surveyBasicInfoViewWithId.CLOSE_PAGE = row["CLOSE_PAGE"].ToString();
                     _surveyBasicInfoViewWithId.OVERQUOTA_PAGE = row["OVERQUOTA_PAGE"].ToString();
                     _surveyBasicInfoViewWithId.UNSUBSCRIBED = row["UNSUBSCRIBED"].ToString();
                     _surveyBasicInfoViewWithId.EMAIL_INVITATION = row["EMAIL_INVITATION"].ToString();
                     _surveyBasicInfoViewWithId.SURVEY_PASSWORD = row["SURVEY_PASSWORD"].ToString();
                     _surveyBasicInfoViewWithId.SURVEY_URL = row["SURVEY_URL"].ToString();
                     _surveyBasicInfoViewWithId.REMINDER_TEXT = row["REMINDER_TEXT"].ToString();
                     _surveyBasicInfoViewWithId.LAUNCH_ON_DATE = Convert.ToDateTime(row["LAUNCH_ON_DATE"]);
                     _surveyBasicInfoViewWithId.CLOSE_ON_DATE = Convert.ToDateTime(row["CLOSE_ON_DATE"]);
                     _surveyBasicInfoViewWithId.LAUNCHED_ON = Convert.ToDateTime(row["LAUNCHED_ON"]);
                     _surveyBasicInfoViewWithId.ALERT_ON_MIN_RESPONSES_BY_DATE = Convert.ToDateTime(row["ALERT_ON_MIN_RESPONSES_BY_DATE"]);
                     _surveyBasicInfoViewWithId.ALERT_ON_MAX_RESPONSES_BY_DATE = Convert.ToDateTime(row["ALERT_ON_MAX_RESPONSES_BY_DATE"]);
                    _surveyBasicInfoViewWithId.REMINDER_ON_DATE = Convert.ToDateTime(row["REMINDER_ON_DATE"]);
                    _surveyBasicInfoViewWithId.SurveyControlDeleted = Convert.ToInt32(row["SurveyControlDeleted"]);
                    _surveyBasicInfoViewWithId.NO_OF_RESPONSES = Convert.ToInt32(row["NO_OF_RESPONSES"]);
                    _surveyBasicInfoViewWithId.SEND_TO_EXT = Convert.ToInt32(row["SEND_TO_EXT"]);
                    _surveyBasicInfoViewWithId.TOTAL_RESPONSES_EXT = Convert.ToInt32(row["TOTAL_RESPONSES_EXT"]);
                    _surveyBasicInfoViewWithId.PARTIAL_RESPONSES_EXT = Convert.ToInt32(row["PARTIAL_RESPONSES_EXT"]);
                    _surveyBasicInfoViewWithId.SCREEN_OUTS_EXT = Convert.ToInt32(row["SCREEN_OUTS_EXT"]);
                    _surveyBasicInfoViewWithId.URL_REDIRECTION = row["URL_REDIRECTION"].ToString();
                    _surveyBasicInfoViewWithId.THANKUPAGE_TYPE = Convert.ToInt32(row["THANKUPAGE_TYPE"]);
                    _surveyBasicInfoViewWithId.CLOSEPAGE_TYPE = Convert.ToInt32(row["CLOSEPAGE_TYPE"]);
                    _surveyBasicInfoViewWithId.SCREENOUTPAGE_TYPE = Convert.ToInt32(row["SCREENOUTPAGE_TYPE"]);
                    _surveyBasicInfoViewWithId.OVERQUOTAPAGE_TYPE = Convert.ToInt32(row["OVERQUOTAPAGE_TYPE"]);
                    _surveyBasicInfoViewWithId.URLREDIRECTION_STATUS = Convert.ToInt32(row["URLREDIRECTION_STATUS"]);
                    _surveyBasicInfoViewWithId.FINISHOPTIONS_STATUS = Convert.ToInt32(row["FINISHOPTIONS_STATUS"]);
                    _surveyBasicInfoViewWithId.SURVEYLAUNCH_URL = row["SURVEYLAUNCH_URL"].ToString();
                    _surveyBasicInfoViewWithId.CONTACTLIST_ID = Convert.ToInt32(row["CONTACTLIST_ID"]);
                    _surveyBasicInfoViewWithId.EMAILLIST_TYPE = Convert.ToInt32(row["EMAILLIST_TYPE"]);
                    _surveyBasicInfoViewWithId.SURVEYURL_TYPE = Convert.ToInt32(row["SURVEYURL_TYPE"]);
                    _surveyBasicInfoViewWithId.LAUNCHOPTIONS_STATUS = Convert.ToInt32(row["LAUNCHOPTIONS_STATUS"]);
                    _surveyBasicInfoViewWithId.SURVEY_STATUS = Convert.ToInt32(row["SURVEY_STATUS"]);
                    _surveyBasicInfoViewWithId.FROM_MAILADDRESS = row["FROM_MAILADDRESS"].ToString();
                    _surveyBasicInfoViewWithId.MAIL_SUBJECT = row["MAIL_SUBJECT"].ToString();
                    _surveyBasicInfoViewWithId.SENDER_NAME = row["SENDER_NAME"].ToString();
                    _surveyBasicInfoViewWithId.REPLY_TO_EMAILADDRESS = row["REPLY_TO_EMAILADDRESS"].ToString();
                    _surveyBasicInfoViewWithId.SURVEY_INTRO = row["SURVEY_INTRO"].ToString();
                    _surveyBasicInfoViewWithId.SurveyIntroStatus = row["SurveyIntroStatus"].ToString();
                    _surveyBasicInfoViewWithId.PAGE_BREAK = Convert.ToInt32(row["PAGE_BREAK"]);
                    _surveyBasicInfoViewWithId.SURVEY_HEADER = row["SURVEY_HEADER"].ToString();
                    _surveyBasicInfoViewWithId.SURVEY_FOOTER = row["SURVEY_FOOTER"].ToString();
                    _surveyBasicInfoViewWithId.SURVEY_FONT_TYPE = row["SURVEY_FONT_TYPE"].ToString();
                    _surveyBasicInfoViewWithId.SURVEY_FONT_SIZE = row["SURVEY_FONT_SIZE"].ToString();
                    _surveyBasicInfoViewWithId.SURVEY_FONT_COLOR = row["SURVEY_FONT_COLOR"].ToString();
                    _surveyBasicInfoViewWithId.SURVEY_BUTTON_TYPE = row["SURVEY_BUTTON_TYPE"].ToString();
                    _surveyBasicInfoViewWithId.CUSTOMSTART_TEXT = row["CUSTOMSTART_TEXT"].ToString();
                    _surveyBasicInfoViewWithId.CUSTOMSUBMITTEXT = row["CUSTOMSUBMITTEXT"].ToString();
                    _surveyBasicInfoViewWithId.SurveySettingDeleted = Convert.ToInt32(row["SurveySettingDeleted"]);
                    _surveyBasicInfoViewWithId.SurveySettingRespReq = Convert.ToInt32(row["SurveySettingRespReq"]);
                    _surveyBasicInfoViewWithId.BROWSER_BACKBUTTON = Convert.ToInt32(row["BROWSER_BACKBUTTON"]);
                    _surveyBasicInfoViewWithId.SURVEY_LOGO = row["SURVEY_LOGO"].ToString();
                    _surveyBasicInfoViewWithId.USERTYPE_ID = Convert.ToInt32(row["USERTYPE_ID"]);
                    _surveyBasicInfoViewWithId.LOGIN_NAME = row["LOGIN_NAME"].ToString();
                    _surveyBasicInfoViewWithId.EMAIL = row["EMAIL"].ToString();
                    _surveyBasicInfoViewWithId.ACCOUNT_TYPE = row["ACCOUNT_TYPE"].ToString();
                    _surveyBasicInfoViewWithId.LICENSE_TYPE = row["LICENSE_TYPE"].ToString();
                    _surveyBasicInfoViewWithId.LICENSE_EXPIRY_DATE = Convert.ToDateTime(row["LICENSE_EXPIRY_DATE"]);
                    _surveyBasicInfoViewWithId.UserStatus = row["UserStatus"].ToString();
                    _surveyBasicInfoViewWithId.UserDeleted = Convert.ToInt32(row["UserDeleted"]);
                    _surveyBasicInfoViewWithId.LICENSE_UPGRADE = Convert.ToInt32(row["LICENSE_UPGRADE"]);
                    _surveyBasicInfoViewWithId.SURVEYMAIL_STATUS = Convert.ToInt32(row["SURVEYMAIL_STATUS"]);
                    _surveyBasicInfoViewWithId.TIME_ZONE = row["TIME_ZONE"].ToString();
                    _surveyBasicInfoViewWithId.QuestionCount = Convert.ToInt32(row["QuestionCount"]);
                    _surveyBasicInfoViewWithId.ResponseCount = Convert.ToInt32(row["ResponseCount"]);
                    _surveyBasicInfoViewWithId.SURVEYCONTROL_ID = Convert.ToInt32(row["SURVEYCONTROL_ID"]);
                    _surveyBasicInfoViewWithId.EMAIL_COUNT = Convert.ToInt32(row["EMAIL_COUNT"]);
                    _surveyBasicInfoViewWithId.COMPANY_NAME = row["COMPANY_NAME"].ToString();
                    _surveyBasicInfoViewWithId.BUTTON_BG_COLOR = row["BUTTON_BG_COLOR"].ToString();
                    _surveyBasicInfoViewWithId.CUSTOM_CONTINUE_TEXT = row["CUSTOM_CONTINUE_TEXT"].ToString();
                    _surveyBasicInfoViewWithId.Show_Logo = Convert.ToBoolean(row["Show_Logo"]);
                    _surveyBasicInfoViewWithId.Show_Progressbar = Convert.ToBoolean(row["Show_Progressbar"]);
                    _surveyBasicInfoViewWithId.Progressbar_Type = row["Progressbar_Type"].ToString();
                    _surveyBasicInfoViewWithId.ProgressbarText = row["ProgressbarText"].ToString();
                    _surveyBasicInfoViewWithId.Show_Progressbartext = Convert.ToBoolean(row["Show_Progressbartext"]);
                    _surveyBasicInfoViewWithId.SURVEY_LAYOUT = Convert.ToInt32(row["SURVEY_LAYOUT"]);

                } 
            }
            else
            {
                _surveyBasicInfoViewWithId = ServiceFactory.GetService<RespondentService>().GetSurveyViewInformation(sid);
            }
            }
            return _surveyBasicInfoViewWithId;
    }



    private SurveyBasicInfoView _surveyBasicInfoView;
    public SurveyBasicInfoView SurveyBasicInfoView
    {
        get
        {
            if (SurveyID == 0)
                return null;

            DataSet dsppstype = surcore.getSurveyType(SurveyID);

            string launchcnt = dsppstype.Tables[2].Rows[0][0].ToString();
            int launchcountval = Convert.ToInt32(launchcnt);
            int launchtypenumber = surcore.getLaunchTypeNumber(SurveyID);
            LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
            if (_surveyBasicInfoView == null)
            {
                if ((LaunchMethod.Emailthr & launchnew) == LaunchMethod.Emailthr)
                {
                    DataSet dsemailinfo = surcore.emaillaunchtypeinfo(SurveyID);
                    DataTable dtemailinfo = dsemailinfo.Tables[0];

                    foreach (DataRow row in dtemailinfo.Rows)
                    {
                        _surveyBasicInfoView = new SurveyBasicInfoView();
                        _surveyBasicInfoView.SURVEY_ID = Convert.ToInt32(row["SURVEY_ID"]);
                        _surveyBasicInfoView.SURVEY_NAME = row["SURVEY_NAME"].ToString();
                        _surveyBasicInfoView.SURVEY_CATEGORY = row["SURVEY_CATEGORY"].ToString();
                        _surveyBasicInfoView.FOLDER_ID = Convert.ToInt32(row["FOLDER_ID"]);
                        _surveyBasicInfoView.USERID = Convert.ToInt32(row["USERID"]);
                        _surveyBasicInfoView.CREATED_ON = Convert.ToDateTime(row["CREATED_ON"]);
                        _surveyBasicInfoView.DELETED = Convert.ToInt32(row["DELETED"]);
                        _surveyBasicInfoView.STATUS = row["STATUS"].ToString();
                        _surveyBasicInfoView.TEMPLATE_CATEGORY = row["TEMPLATE_CATEGORY"].ToString();
                        _surveyBasicInfoView.TEMPLATE_SUBCATEGORY = row["TEMPLATE_SUBCATEGORY"].ToString();
                        _surveyBasicInfoView.THEME = row["THEME"].ToString();
                        _surveyBasicInfoView.MODIFIED_ON = Convert.ToDateTime(row["MODIFIED_ON"]);
                        if (row["SURVEY_CLOSE_DATE"].ToString() != "")
                        {
                            _surveyBasicInfoView.SURVEY_CLOSE_DATE = Convert.ToDateTime(row["SURVEY_CLOSE_DATE"]);
                        }

                        _surveyBasicInfoView.CHECK_BLOCKWORDS = Convert.ToInt32(row["CHECK_BLOCKWORDS"]);
                        _surveyBasicInfoView.SurveyWidgetFlag = Convert.ToInt32(row["SurveyWidgetFlag"]);
                        _surveyBasicInfoView.SCHEDULE_ON = Convert.ToInt32(row["SCHEDULE_ON"]);
                        _surveyBasicInfoView.SCHEDULE_ON_ALERT = Convert.ToInt32(row["SCHEDULE_ON_ALERT"]);
                        _surveyBasicInfoView.SCHEDULE_ON_CLOSE = Convert.ToInt32(row["SCHEDULE_ON_CLOSE"]);
                        _surveyBasicInfoView.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS = Convert.ToInt32(row["SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS"]);
                        _surveyBasicInfoView.SCHEDULE_ON_CLOSE_ALERT = Convert.ToInt32(row["SCHEDULE_ON_CLOSE_ALERT"]);
                        _surveyBasicInfoView.SurveyControlStatus = Convert.ToInt32(row["SurveyControlStatus"]);
                        _surveyBasicInfoView.TIME_FOR_URL_REDIRECTION = row["TIME_FOR_URL_REDIRECTION"].ToString();
                        _surveyBasicInfoView.SCREEN_OUTS = Convert.ToInt32(row["SCREEN_OUTS"]);
                        _surveyBasicInfoView.PARTIAL_RESPONSES = Convert.ToInt32(row["PARTIAL_RESPONSES"]);
                        _surveyBasicInfoView.TOTAL_RESPONSES = Convert.ToInt32(row["TOTAL_RESPONSES"]);
                        _surveyBasicInfoView.SEND_TO = Convert.ToInt32(row["SEND_TO"]);
                        _surveyBasicInfoView.ONLAUNCH_ALERT = Convert.ToInt32(row["ONLAUNCH_ALERT"]);
                        _surveyBasicInfoView.CLOSE_ON_NO_RESPONDENTS = Convert.ToInt32(row["CLOSE_ON_NO_RESPONDENTS"]);
                        _surveyBasicInfoView.CONTINUOUS_SURVEY = Convert.ToInt32(row["CONTINUOUS_SURVEY"]);
                        _surveyBasicInfoView.UNIQUE_RESPONDENT = Convert.ToInt32(row["UNIQUE_RESPONDENT"]);
                        _surveyBasicInfoView.SAVE_AND_CONTINUE = Convert.ToInt32(row["SAVE_AND_CONTINUE"]);
                        _surveyBasicInfoView.AUTOMATIC_THANKYOU_EMAIL = Convert.ToInt32(row["AUTOMATIC_THANKYOU_EMAIL"]);
                        _surveyBasicInfoView.ALERT_ON_MIN_RESPONSES = Convert.ToInt32(row["ALERT_ON_MIN_RESPONSES"]);
                        _surveyBasicInfoView.ALERT_ON_MAX_RESPONSES = Convert.ToInt32(row["ALERT_ON_MAX_RESPONSES"]);
                        _surveyBasicInfoView.THANKYOU_PAGE = row["THANKYOU_PAGE"].ToString();
                        _surveyBasicInfoView.SCREENOUT_PAGE = row["SCREENOUT_PAGE"].ToString();
                        _surveyBasicInfoView.CLOSE_PAGE = row["CLOSE_PAGE"].ToString();
                        _surveyBasicInfoView.OVERQUOTA_PAGE = row["OVERQUOTA_PAGE"].ToString();
                        _surveyBasicInfoView.UNSUBSCRIBED = row["UNSUBSCRIBED"].ToString();
                        _surveyBasicInfoView.EMAIL_INVITATION = row["EMAIL_INVITATION"].ToString();
                        _surveyBasicInfoView.SURVEY_PASSWORD = row["SURVEY_PASSWORD"].ToString();
                        _surveyBasicInfoView.SURVEY_URL = row["SURVEY_URL"].ToString();
                        _surveyBasicInfoView.REMINDER_TEXT = row["REMINDER_TEXT"].ToString();
                        _surveyBasicInfoView.LAUNCH_ON_DATE = Convert.ToDateTime(row["LAUNCH_ON_DATE"]);
                        _surveyBasicInfoView.CLOSE_ON_DATE = Convert.ToDateTime(row["CLOSE_ON_DATE"]);
                        _surveyBasicInfoView.LAUNCHED_ON = Convert.ToDateTime(row["LAUNCHED_ON"]);
                        _surveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE = Convert.ToDateTime(row["ALERT_ON_MIN_RESPONSES_BY_DATE"]);
                        _surveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE = Convert.ToDateTime(row["ALERT_ON_MAX_RESPONSES_BY_DATE"]);
                        _surveyBasicInfoView.REMINDER_ON_DATE = Convert.ToDateTime(row["REMINDER_ON_DATE"]);
                        _surveyBasicInfoView.SurveyControlDeleted = Convert.ToInt32(row["SurveyControlDeleted"]);
                        _surveyBasicInfoView.NO_OF_RESPONSES = Convert.ToInt32(row["NO_OF_RESPONSES"]);
                        _surveyBasicInfoView.SEND_TO_EXT = Convert.ToInt32(row["SEND_TO_EXT"]);
                        _surveyBasicInfoView.TOTAL_RESPONSES_EXT = Convert.ToInt32(row["TOTAL_RESPONSES_EXT"]);
                        _surveyBasicInfoView.PARTIAL_RESPONSES_EXT = Convert.ToInt32(row["PARTIAL_RESPONSES_EXT"]);
                        _surveyBasicInfoView.SCREEN_OUTS_EXT = Convert.ToInt32(row["SCREEN_OUTS_EXT"]);
                        _surveyBasicInfoView.URL_REDIRECTION = row["URL_REDIRECTION"].ToString();
                        _surveyBasicInfoView.THANKUPAGE_TYPE = Convert.ToInt32(row["THANKUPAGE_TYPE"]);
                        _surveyBasicInfoView.CLOSEPAGE_TYPE = Convert.ToInt32(row["CLOSEPAGE_TYPE"]);
                        _surveyBasicInfoView.SCREENOUTPAGE_TYPE = Convert.ToInt32(row["SCREENOUTPAGE_TYPE"]);
                        _surveyBasicInfoView.OVERQUOTAPAGE_TYPE = Convert.ToInt32(row["OVERQUOTAPAGE_TYPE"]);
                        _surveyBasicInfoView.URLREDIRECTION_STATUS = Convert.ToInt32(row["URLREDIRECTION_STATUS"]);
                        _surveyBasicInfoView.FINISHOPTIONS_STATUS = Convert.ToInt32(row["FINISHOPTIONS_STATUS"]);
                        _surveyBasicInfoView.SURVEYLAUNCH_URL = row["SURVEYLAUNCH_URL"].ToString();
                        _surveyBasicInfoView.CONTACTLIST_ID = Convert.ToInt32(row["CONTACTLIST_ID"]);
                        _surveyBasicInfoView.EMAILLIST_TYPE = Convert.ToInt32(row["EMAILLIST_TYPE"]);
                        _surveyBasicInfoView.SURVEYURL_TYPE = Convert.ToInt32(row["SURVEYURL_TYPE"]);
                        _surveyBasicInfoView.LAUNCHOPTIONS_STATUS = Convert.ToInt32(row["LAUNCHOPTIONS_STATUS"]);
                        _surveyBasicInfoView.SURVEY_STATUS = Convert.ToInt32(row["SURVEY_STATUS"]);
                        _surveyBasicInfoView.FROM_MAILADDRESS = row["FROM_MAILADDRESS"].ToString();
                        _surveyBasicInfoView.MAIL_SUBJECT = row["MAIL_SUBJECT"].ToString();
                        _surveyBasicInfoView.SENDER_NAME = row["SENDER_NAME"].ToString();
                        _surveyBasicInfoView.REPLY_TO_EMAILADDRESS = row["REPLY_TO_EMAILADDRESS"].ToString();
                        _surveyBasicInfoView.SURVEY_INTRO = row["SURVEY_INTRO"].ToString();
                        _surveyBasicInfoView.SurveyIntroStatus = row["SurveyIntroStatus"].ToString();
                        _surveyBasicInfoView.PAGE_BREAK = Convert.ToInt32(row["PAGE_BREAK"]);
                        _surveyBasicInfoView.SURVEY_HEADER = row["SURVEY_HEADER"].ToString();
                        _surveyBasicInfoView.SURVEY_FOOTER = row["SURVEY_FOOTER"].ToString();
                        _surveyBasicInfoView.SURVEY_FONT_TYPE = row["SURVEY_FONT_TYPE"].ToString();
                        _surveyBasicInfoView.SURVEY_FONT_SIZE = row["SURVEY_FONT_SIZE"].ToString();
                        _surveyBasicInfoView.SURVEY_FONT_COLOR = row["SURVEY_FONT_COLOR"].ToString();
                        _surveyBasicInfoView.SURVEY_BUTTON_TYPE = row["SURVEY_BUTTON_TYPE"].ToString();
                        _surveyBasicInfoView.CUSTOMSTART_TEXT = row["CUSTOMSTART_TEXT"].ToString();
                        _surveyBasicInfoView.CUSTOMSUBMITTEXT = row["CUSTOMSUBMITTEXT"].ToString();
                        _surveyBasicInfoView.SurveySettingDeleted = Convert.ToInt32(row["SurveySettingDeleted"]);
                        _surveyBasicInfoView.SurveySettingRespReq = Convert.ToInt32(row["SurveySettingRespReq"]);
                        _surveyBasicInfoView.BROWSER_BACKBUTTON = Convert.ToInt32(row["BROWSER_BACKBUTTON"]);
                        _surveyBasicInfoView.SURVEY_LOGO = row["SURVEY_LOGO"].ToString();
                        _surveyBasicInfoView.USERTYPE_ID = Convert.ToInt32(row["USERTYPE_ID"]);
                        _surveyBasicInfoView.LOGIN_NAME = row["LOGIN_NAME"].ToString();
                        _surveyBasicInfoView.EMAIL = row["EMAIL"].ToString();
                        _surveyBasicInfoView.ACCOUNT_TYPE = row["ACCOUNT_TYPE"].ToString();
                        _surveyBasicInfoView.LICENSE_TYPE = row["LICENSE_TYPE"].ToString();
                        _surveyBasicInfoView.LICENSE_EXPIRY_DATE = Convert.ToDateTime(row["LICENSE_EXPIRY_DATE"]);
                        _surveyBasicInfoView.UserStatus = row["UserStatus"].ToString();
                        _surveyBasicInfoView.UserDeleted = Convert.ToInt32(row["UserDeleted"]);
                        _surveyBasicInfoView.LICENSE_UPGRADE = Convert.ToInt32(row["LICENSE_UPGRADE"]);
                        _surveyBasicInfoView.SURVEYMAIL_STATUS = Convert.ToInt32(row["SURVEYMAIL_STATUS"]);
                        _surveyBasicInfoView.TIME_ZONE = row["TIME_ZONE"].ToString();
                        _surveyBasicInfoView.QuestionCount = Convert.ToInt32(row["QuestionCount"]);
                        _surveyBasicInfoView.ResponseCount = Convert.ToInt32(row["ResponseCount"]);
                        _surveyBasicInfoView.SURVEYCONTROL_ID = Convert.ToInt32(row["SURVEYCONTROL_ID"]);
                        _surveyBasicInfoView.EMAIL_COUNT = Convert.ToInt32(row["EMAIL_COUNT"]);
                        _surveyBasicInfoView.COMPANY_NAME = row["COMPANY_NAME"].ToString();
                        _surveyBasicInfoView.BUTTON_BG_COLOR = row["BUTTON_BG_COLOR"].ToString();
                        _surveyBasicInfoView.CUSTOM_CONTINUE_TEXT = row["CUSTOM_CONTINUE_TEXT"].ToString();
                        _surveyBasicInfoView.Show_Logo = Convert.ToBoolean(row["Show_Logo"]);
                        _surveyBasicInfoView.Show_Progressbar = Convert.ToBoolean(row["Show_Progressbar"]);
                        _surveyBasicInfoView.Progressbar_Type = row["Progressbar_Type"].ToString();
                        _surveyBasicInfoView.ProgressbarText = row["ProgressbarText"].ToString();
                        _surveyBasicInfoView.Show_Progressbartext = Convert.ToBoolean(row["Show_Progressbartext"]);
                        _surveyBasicInfoView.SURVEY_LAYOUT = Convert.ToInt32(row["SURVEY_LAYOUT"]);

                    }


                }
                else
                {
                    _surveyBasicInfoView = ServiceFactory.GetService<RespondentService>().GetSurveyViewInformation(SurveyID);
                }
            }
            return _surveyBasicInfoView;
        }
    }

    public bool IsSurveyActive
    {
        get
        {
            if (SurveyBasicInfoView != null && (SurveyBasicInfoView.STATUS == SurveyStatus.Active.ToString()))
                return true;

            return false;
        }
    }

    public bool IsSurveyActiveFb(int surveyid=0)
    {

        if (SurveyBasicInfoViewWithId(surveyid) != null && (SurveyBasicInfoViewWithId(surveyid).STATUS == SurveyStatus.Active.ToString()))
            return true;
        else
        {
            return false;
        }
    }
    protected const string SurveySessionName = "SURVEY";
    //protected Survey SelectedSurvey { get; set; }
    public Survey SelectedSurvey { get; set; }

    int questID = 0;
    public int EditingQuestionId
    {
        get
        {
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
            if (Mode.Equals("Edit"))
            {
                if (ht.Contains("QuestionId"))
                {
                    questID = ValidationHelper.GetInteger(ht["QuestionId"].ToString(), 0);
                }
                else
                {
                    questID = 0;
                }
            }

            return questID;
        }
    }

    public string Mode
    {
        get
        {
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
            if (ht.Contains("Mode"))
            {
                return Convert.ToString(ht["Mode"]);

            }
            return string.Empty;
        }
    }


    public SurveyActionTypes ActionType
    {
        get
        {
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
            if (ht.Contains("Mode"))
            {
                return Utilities.ToEnum<SurveyActionTypes>(string.IsNullOrEmpty(Convert.ToString(ht["Mode"])) ? "Add" : Convert.ToString(ht["Mode"]));
            }
            return default(SurveyActionTypes);
        }
    }

    public QuestionType QuestionType
    {
        get
        {
            int questID = 0;

            Hashtable ht1 = new Hashtable();
            ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
            if (ht1.Contains("QuestionType"))
            {
                questID = ValidationHelper.GetInteger(ht1["QuestionType"].ToString(), 0);

            }
            return GenericHelper.ToEnum<QuestionType>(questID);
        }
    }

    public SurveyStatus CurrentSurveyStatus
    {
        get
        {
            return Utilities.ToEnum<SurveyStatus>(SurveyBasicInfoView.SURVEY_STATUS);
        }
    }

    int questionSeq;
    public int QuestionSequenceId
    {
        get
        {
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
            if (ht.Contains("QuestionSeq"))
            {
                questionSeq = ValidationHelper.GetInteger(ht["QuestionSeq"].ToString(), 0);
            }
            return questionSeq;
        }
    }

}