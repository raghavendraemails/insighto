﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Insighto.Business.Charts;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for SurveyCoreVoice
/// </summary>
public class SurveyCoreVoice
{
    SqlConnection objsql = new SqlConnection();
    public static System.Web.SessionState.HttpSessionState Sstate = null;
    DBManager dbm = new DBManager();
    string connect = ConfigurationManager.ConnectionStrings["InsightoConnString"].ToString();
	public SurveyCoreVoice()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public Survey GetSurveyWithResponsesCount(int SurveyID, DateTime Startdate, DateTime Enddate)
    {
        try
        {
            Survey Sur = GetSurveyDet(SurveyID);
            string RespondentIDS = GetActiveRespondent(SurveyID);
            string callerIDs = GetActiveCallerID(SurveyID);

            dbm.OpenConnection(connect);
            if (Sur != null && Sur.SURVEY_ID > 0 && Sur.surveyQues != null && Sur.surveyQues.Count > 0 && RespondentIDS != null && RespondentIDS.Length > 0)
            {
                foreach (SurveyQuestion surQues in Sur.surveyQues)
                {
                    int QuesResCount = 0, AnsResCount = 0;
                    string qryQuesResp = "";
                    if (surQues.QUESTION_TYPE == 2)
                    {
                        // qryQuesResp = "select count(Distinct RESPONDENT_ID) from osm_responsequestions where QUESTION_ID=" + surQues.QUESTION_ID + " and deleted=0";
                        qryQuesResp = "select count( RESPONDENT_ID) from osm_responsequestions where QUESTION_ID=" + surQues.QUESTION_ID + " and deleted=0";
                        if (RespondentIDS != null && RespondentIDS.Length > 0)
                            qryQuesResp += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                    }
                    else
                    {
                        // qryQuesResp = "select count(Distinct RESPONDENT_ID) as RESPONSE_COUNT from osm_responsequestions where DELETED=0 and QUESTION_ID=" + surQues.QUESTION_ID;
                        qryQuesResp = "select count( RESPONDENT_ID) as RESPONSE_COUNT from osm_responsequestions where DELETED=0 and QUESTION_ID=" + surQues.QUESTION_ID;
                        if (RespondentIDS != null && RespondentIDS.Length > 0)
                            qryQuesResp += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                        if (Startdate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM") && Enddate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                        {
                            qryQuesResp += " and RESPONSE_RECEIVED_ON>='" + Startdate + "' and RESPONSE_RECEIVED_ON<='" + Enddate + "'";
                        }
                    }
                    object QCount = dbm.ExecuteScalar(CommandType.Text, qryQuesResp);
                    try
                    {
                        if (QCount != null)
                        {
                            QuesResCount = Convert.ToInt32(QCount);
                        }
                    }
                    catch
                    {
                    }
                    surQues.RESPONSE_COUNT = QuesResCount;
                    if (surQues.surveyAnswers != null && surQues.surveyAnswers.Count > 0)
                    {
                        foreach (SurveyAnswers ans in surQues.surveyAnswers)
                        {
                            //string qryAnsResp = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where ANSWER_ID=" + ans.ANSWER_ID + " and RESPONDENT_ID in(" + RespondentIDS + ")";
                            string qryAnsResp = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where DELETED=0 and ANSWER_ID=" + ans.ANSWER_ID;
                            if (RespondentIDS != null && RespondentIDS.Length > 0)
                                qryAnsResp += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                            if (Startdate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM") && Enddate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                            {
                                qryAnsResp += " and RESPONSE_RECEIVED_ON >='" + Startdate + "' and RESPONSE_RECEIVED_ON <='" + Enddate + "'";
                            }
                            object ACount = dbm.ExecuteScalar(CommandType.Text, qryAnsResp);
                            try
                            {
                                if (ACount != null)
                                {
                                    AnsResCount = Convert.ToInt32(ACount);
                                }
                            }
                            catch { }
                            ans.RESPONSE_COUNT = AnsResCount;
                        }
                        if (surQues.OTHER_ANS == 1)
                        {
                            if (surQues.QUESTION_TYPE == 1 || surQues.QUESTION_TYPE == 2 || surQues.QUESTION_TYPE == 3 || surQues.QUESTION_TYPE == 4)
                            {
                                //string qryAnsRespother = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where ANSWER_ID=0 and QUESTION_ID=" + surQues.QUESTION_ID + " and RESPONDENT_ID in(" + RespondentIDS + ")";
                                string qryAnsRespother = "select count(ANSWER_ID) as RESPONSE_COUNT from osm_responsequestions where ANSWER_ID=0 and QUESTION_ID=" + surQues.QUESTION_ID;
                                if (RespondentIDS != null && RespondentIDS.Length > 0)
                                    qryAnsRespother += " and RESPONDENT_ID in(" + RespondentIDS + ")";
                                if (Startdate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM") && Enddate != null && Startdate > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                                {
                                    qryAnsRespother += " and RESPONSE_RECEIVED_ON >='" + Startdate + "' and RESPONSE_RECEIVED_ON <='" + Enddate + "'";
                                }
                                object ACountothers = dbm.ExecuteScalar(CommandType.Text, qryAnsRespother);
                                try
                                {
                                    if (ACountothers != null)
                                    {
                                        surQues.OTHER_RESPCOUNT = Convert.ToInt32(ACountothers);
                                    }
                                }
                                catch { }

                            }
                        }
                    }
                }
            }
            return Sur;
        }
        catch
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }


    public string GetActiveRespondent(int SurveyID)
    {
        int cont = 0;
        string usrLmitDet = GetUserLimit(SurveyID, "");
        int usrLmit = 0; string LienTyp = "";
        string[] det = usrLmitDet.Split('$');
        if (det != null && det.Length > 0)
        {
            LienTyp = Convert.ToString(det.GetValue(1));
            usrLmit = Convert.ToInt32(det.GetValue(0));
        }
        string RespondentIDs = "";
        string qry = "";
        if (usrLmit != -1 && usrLmit > 0)
        {

            //qry = "select TOP "+usrLmit +" RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and VISABLE_LIMIT=1 and SURVEY_ID=" + SurveyID;
            qry = "select TOP " + usrLmit + " RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
        }
        else if (usrLmit == -1)
        {
            qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;

        }
        //qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and VISABLE_LIMIT=1 and SURVEY_ID=" + SurveyID;
        try
        {
            dbm.OpenConnection(connect);
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qry);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    RespondentIDs += "," + Convert.ToString(ds.Tables[0].Rows[i]["RESPONDENT_ID"]);
                }
                if (RespondentIDs != null && RespondentIDs.Trim().Length > 0)
                    RespondentIDs = RespondentIDs.Substring(1);
            }
            return RespondentIDs;
        }
        catch
        {
            return RespondentIDs;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public string GetActiveCallerID(int SurveyID)
    {
        int cont = 0;
        string usrLmitDet = GetUserLimit(SurveyID, "");
        int usrLmit = 0; string LienTyp = "";
        string[] det = usrLmitDet.Split('$');
        if (det != null && det.Length > 0)
        {
            LienTyp = Convert.ToString(det.GetValue(1));
            usrLmit = Convert.ToInt32(det.GetValue(0));
        }
        string RespondentIDs = "";
        string qry = "";
        if (usrLmit != -1 && usrLmit > 0)
        {

            //qry = "select TOP "+usrLmit +" RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and VISABLE_LIMIT=1 and SURVEY_ID=" + SurveyID;
            qry = "select TOP " + usrLmit + " RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
        }
        else if (usrLmit == -1)
        {
            qry = "select SessionID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;

        }
        //qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and VISABLE_LIMIT=1 and SURVEY_ID=" + SurveyID;
        try
        {
            dbm.OpenConnection(connect);
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qry);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    RespondentIDs += "," + Convert.ToString(ds.Tables[0].Rows[i]["RESPONDENT_ID"]);
                }
                if (RespondentIDs != null && RespondentIDs.Trim().Length > 0)
                    RespondentIDs = RespondentIDs.Substring(1);
            }
            return RespondentIDs;
        }
        catch
        {
            return RespondentIDs;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }


    public string GetUserLimit(int SurveyID, string LicenseType)
    {
        string ret = ""; string fea_nam = "";
        string Qry = "select LICENSE_TYPE from osm_user where deleted=0 and USERID =(select USERID from osm_survey where deleted =0 and SURVEY_ID='" + SurveyID + "')";
        try
        {
            dbm.OpenConnection(connect);
            if (LicenseType != null && LicenseType.Trim().Length > 0)
            {
                fea_nam = LicenseType;
            }
            else
            {
                object obj = dbm.ExecuteScalar(CommandType.Text, Qry);
                if (obj != null)
                {
                    fea_nam = Convert.ToString(obj);
                }
            }
            if (fea_nam != null && fea_nam.Length > 0)
            {
                string QRtext = "select  " + fea_nam + "  from osm_features where FEATURE='NO_OF_RESPONSES' and deleted=0 ";
                object obj1 = dbm.ExecuteScalar(CommandType.Text, QRtext);
                if (obj1 != null)
                {
                    ret = Convert.ToString(obj1);
                }
            }
            ret = ret + "$" + fea_nam;
            return ret;
        }
        catch
        {
            return "";
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    public Survey GetSurveyDet(int SurveyID)
    {
        try
        {
            dbm.OpenConnection(connect);
            Survey sur = new Survey();
            if (SurveyID > 0)
            {
                string str = " select sur.SURVEY_NAME,sur.SURVEY_CATEGORY,sur.THEME,sur.FOLDER_ID,sur.USERID,sur.CREATED_ON,sur.STATUS,sur.CHECK_BLOCKWORDS ";
                str += " from osm_survey sur ";
                str += " where sur.SURVEY_ID= " + SurveyID;
                str += " select * from osm_surveyIntro where DELETED=0 and SURVEY_ID= " + SurveyID;
                string surSet = " select * from osm_surveysetting where DELETED=0 and SURVEY_ID= " + SurveyID;
                //order by is written by sandeep
                string Queqry = " select * from osm_surveyquestion where DELETED=0 and SURVEY_ID= " + SurveyID;
                string Conqry = " select * from osm_surveycontrols where DELETED=0 and SURVEY_ID= " + SurveyID;

                DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    sur.USER_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["USERID"]);
                    sur.SURVEY_ID = SurveyID;
                    sur.FOLDER_ID = Convert.ToInt32(ds.Tables[0].Rows[0]["FOLDER_ID"]);
                    sur.STATUS = Convert.ToString(ds.Tables[0].Rows[0]["STATUS"]);
                    sur.SURVEY_CATEGORY = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_CATEGORY"]);
                    sur.THEME = Convert.ToString(ds.Tables[0].Rows[0]["THEME"]);
                    if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                    {
                        sur.SURVEY_INTRO = Convert.ToString(ds.Tables[1].Rows[0]["SURVEY_INTRO"]);
                        if (ds.Tables[1].Rows[0]["PAGE_BREAK"] != null)
                        {
                            sur.SURVEYINTRO_PAGEBREAK = Convert.ToInt32(ds.Tables[1].Rows[0]["PAGE_BREAK"]);
                        }
                    }
                    sur.SURVEY_NAME = Convert.ToString(ds.Tables[0].Rows[0]["SURVEY_NAME"]);
                    sur.CHECK_BLOCKWORDS = Convert.ToInt32(ds.Tables[0].Rows[0]["CHECK_BLOCKWORDS"]);

                    DataSet surSetds = dbm.ExecuteDataSet(CommandType.Text, surSet);
                    if (surSetds != null && surSetds.Tables.Count > 0 && surSetds.Tables[0].Rows.Count > 0)
                    {
                        sur.SURVEY_HEADER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_HEADER"]);
                        sur.SURVEY_FOOTER = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FOOTER"]);
                        sur.SURVEY_FONT_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_TYPE"]);
                        sur.SURVEY_FONT_SIZE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_SIZE"]);
                        sur.SURVEY_FONT_COLOR = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_FONT_COLOR"]);
                        sur.SURVEY_BUTTON_TYPE = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_BUTTON_TYPE"]);
                        sur.CUSTOMSTART_TEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSTART_TEXT"]);
                        sur.CUSTOMSUBMITTEXT = Convert.ToString(surSetds.Tables[0].Rows[0]["CUSTOMSUBMITTEXT"]);
                        sur.PAGE_BREAK = Convert.ToInt32(surSetds.Tables[0].Rows[0]["PAGE_BREAK"]);
                        sur.RESPONSE_REQUIRED = Convert.ToInt32(surSetds.Tables[0].Rows[0]["RESPONSE_REQUIRED"]);
                        sur.BROWSER_BACKBUTTON_STATUS = Convert.ToInt32(surSetds.Tables[0].Rows[0]["BROWSER_BACKBUTTON"]);
                        sur.SURVEY_LOGO = Convert.ToString(surSetds.Tables[0].Rows[0]["SURVEY_LOGO"]);
                    }
                    else
                    {
                        sur.BROWSER_BACKBUTTON_STATUS = 1;
                    }
                    DataSet Quesds = dbm.ExecuteDataSet(CommandType.Text, Queqry);
                    if (Quesds != null && Quesds.Tables.Count > 0 && Quesds.Tables[0].Rows.Count > 0)
                    {
                        sur.surveyQues = new List<SurveyQuestion>();
                        for (int i = 0; i < Quesds.Tables[0].Rows.Count; i++)
                        {
                            SurveyQuestion ques = new SurveyQuestion();
                            ques.CREATED_ON = Convert.ToDateTime(Quesds.Tables[0].Rows[i]["CREATED_ON"]);
                            ques.LASTMODIFIED_ON = Convert.ToDateTime(Quesds.Tables[0].Rows[i]["LASTMODIFIED_ON"]);
                            ques.PAGE_BREAK = Convert.ToInt32(Quesds.Tables[0].Rows[i]["PAGE_BREAK"]);
                            ques.QUESTION_ID = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_ID"]);
                            ques.QUESTION_LABEL = Convert.ToString(Quesds.Tables[0].Rows[i]["QUSETION_LABEL"]);
                            ques.QUESTION_SEQ = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_SEQ"]);
                            ques.QUESTION_TYPE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["QUESTION_TYPE_ID"]);
                            ques.RESPONSE_REQUIRED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RESPONSE_REQUIRED"]);
                            ques.RANDOM_ANSWERS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["RANDOMIZE_ANSWERS"]);
                            ques.OTHER_ANS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["OTHER_ANS"]);
                            ques.SKIP_LOGIC = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SKIP_LOGIC"]);
                            ques.SURVEY_ID = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SURVEY_ID"]);
                            ques.CHART_ALIGNMENT = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_ALIGNMENT"]);
                            ques.CHART_APPEARANCE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_APPEARANCE"]);
                            ques.CHART_DOCK = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_DOCK"]);
                            ques.CHART_PALETTE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_PALETTE"]);
                            ques.CHART_TITLE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_TITLE"]);
                            ques.CHART_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["CHART_TYPE"]);
                            ques.COMMENTS = Convert.ToString(Quesds.Tables[0].Rows[i]["COMMENTS"]);
                            ques.CHAR_LIMIT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["CHAR_LIMIT"]);
                            ques.ANSWER_ALIGNSTYLE = Convert.ToString(Quesds.Tables[0].Rows[i]["ANSWER_ALIGNSTYLE"]);
                            ques.HIDE_LEGAND = Convert.ToInt32(Quesds.Tables[0].Rows[i]["HIDE_LEGAND"]);
                            ques.HIDE_XAXIS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["HIDE_XAXIS"]);
                            ques.STAGGERED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["STAGGERED"]);

                            ques.SHOW_LABEL = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SHOW_LABEL"]);
                            ques.LABEL_ANGLE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LABEL_ANGLE"]);
                            ques.VALUEAS_PERCENT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["VALUEAS_PERCENT"]);
                            ques.ZOOMPERCENT = Convert.ToInt32(Quesds.Tables[0].Rows[i]["ZOOMPERCENT"]);
                            ques.TRANSPARENCY = Convert.ToInt32(Quesds.Tables[0].Rows[i]["TRANSPARENCY"]);
                            ques.PERSPECTIVE_ANGLE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["PERSPECTIVE_ANGLE"]);
                            ques.MARKER_SIZE = Convert.ToInt32(Quesds.Tables[0].Rows[i]["MARKER_SIZE"]);
                            ques.SHOW_MARKER = Convert.ToInt32(Quesds.Tables[0].Rows[i]["SHOW_MARKER"]);
                            ques.INVERTED = Convert.ToInt32(Quesds.Tables[0].Rows[i]["INVERTED"]);
                            ques.LABEL_POSITION = Convert.ToString(Quesds.Tables[0].Rows[i]["LABEL_POSITION"]);
                            ques.MARKER_KIND = Convert.ToString(Quesds.Tables[0].Rows[i]["MARKER_KIND"]);
                            ques.HOLE_RADIUS = Convert.ToString(Quesds.Tables[0].Rows[i]["HOLE_RADIUS"]);
                            ques.DIAGRAM_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["DIAGRAM_TYPE"]);
                            ques.TEXT_DIRECTION = Convert.ToString(Quesds.Tables[0].Rows[i]["TEXT_DIRECTION"]);
                            ques.FUNCTION_TYPE = Convert.ToString(Quesds.Tables[0].Rows[i]["FUNCTION_TYPE"]);
                            ques.EXPLODED_POINT = Convert.ToString(Quesds.Tables[0].Rows[i]["EXPLODED_POINT"]);
                            ques.LEGEND_DIRECTION = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_DIRECTION"]);
                            ques.LEGEND_EQUALLYSPACED_ITEMS = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_EQUALLYSPACED_ITEMS"]);
                            ques.LEGEND_HORIZONTALALIGN = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_HORIZONTALALIGN"]);
                            ques.LEGEND_MAX_HORIZONTALALIGN = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_MAX_HORIZONTALALIGN"]);
                            ques.LEGEND_MAX_VERTICALALIGN = Convert.ToInt32(Quesds.Tables[0].Rows[i]["LEGEND_MAX_VERTICALALIGN"]);
                            ques.LEGEND_VERTICALALIGN = Convert.ToString(Quesds.Tables[0].Rows[i]["LEGEND_VERTICALALIGN"]);

                            string Ansqry = " select * from osm_answeroptions where DELETED=0 and QUESTION_ID= " + ques.QUESTION_ID + " order by ANSWER_ID";
                            DataSet Ansds = dbm.ExecuteDataSet(CommandType.Text, Ansqry);
                            if (Ansds != null && Ansds.Tables.Count > 0 && Ansds.Tables[0].Rows.Count > 0)
                            {
                                ques.surveyAnswers = new List<SurveyAnswers>();
                                for (int y = 0; y < Ansds.Tables[0].Rows.Count; y++)
                                {
                                    SurveyAnswers Ansr = new SurveyAnswers();
                                    Ansr.ANSWER_ID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["ANSWER_ID"]);
                                    Ansr.ANSWER_OPTIONS = Convert.ToString(Ansds.Tables[0].Rows[y]["ANSWER_OPTIONS"]);
                                    Ansr.CREATED_ON = Convert.ToDateTime(Ansds.Tables[0].Rows[y]["CREATED_ON"]);
                                    Ansr.LAST_MODIFIED_ON = Convert.ToDateTime(Ansds.Tables[0].Rows[y]["LAST_MODIFIED_ON"]);
                                    Ansr.QUESTION_ID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["QUESTION_ID"]);
                                    Ansr.SKIP_QUESTION_ID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["SKIP_QUESTION_ID"]);
                                    Ansr.DEMOGRAPIC_BLOCKID = Convert.ToInt32(Ansds.Tables[0].Rows[y]["DEMOGRAPIC_BLOCKID"]);
                                    Ansr.PIPING_TEXT = Convert.ToString(Ansds.Tables[0].Rows[y]["PIPING_TEXT"]);
                                    Ansr.RESPONSE_REQUIRED = Convert.ToInt32(Ansds.Tables[0].Rows[y]["RESPONSE_REQUIRED"]);
                                    ques.surveyAnswers.Add(Ansr);
                                }
                            }
                            sur.surveyQues.Add(ques);
                        }
                    }
                    DataSet Controlsds = dbm.ExecuteDataSet(CommandType.Text, Conqry);
                    if (Controlsds != null && Controlsds.Tables.Count > 0 && Controlsds.Tables[0].Rows.Count > 0)
                    {
                        sur.surveyOptns = new List<SurveyOptions>();
                        for (int j = 0; j < Controlsds.Tables[0].Rows.Count; j++)
                        {
                            SurveyOptions opt = new SurveyOptions();
                            opt.ALERT_ON_MAX_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ALERT_ON_MAX_RESPONSES"]);
                            opt.ALERT_ON_MAX_RESPONSES_BY_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["ALERT_ON_MAX_RESPONSES_BY_DATE"]);
                            opt.ALERT_ON_MIN_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["ALERT_ON_MIN_RESPONSES"]);
                            opt.ALERT_ON_MIN_RESPONSES_BY_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["ALERT_ON_MIN_RESPONSES_BY_DATE"]);
                            opt.AUTOMATIC_THANKYOU_EMAIL = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["AUTOMATIC_THANKYOU_EMAIL"]);
                            opt.CLOSE_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["CLOSE_ON_DATE"]);
                            opt.CLOSE_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["CLOSE_PAGE"]);
                            opt.EMAIL_INVITATION = Convert.ToString(Controlsds.Tables[0].Rows[j]["EMAIL_INVITATION"]);
                            opt.EMAIL_FROMADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["FROM_MAILADDRESS"]);
                            opt.EMAIL_SUBJECT = Convert.ToString(Controlsds.Tables[0].Rows[j]["MAIL_SUBJECT"]);
                            opt.LAUNCH_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["LAUNCH_ON_DATE"]);
                            opt.NOOFRESPONSESTOCLOSEE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS"]);
                            opt.ONCLOSEALERT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE_ALERT"]);
                            opt.ONLAUNCH_ALERT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_ALERT"]);
                            opt.OVERQUOTA_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["OVERQUOTA_PAGE"]);
                            opt.PARTIAL_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["PARTIAL_RESPONSES"]);
                            opt.REMINDER_ON_DATE = Convert.ToDateTime(Controlsds.Tables[0].Rows[j]["REMINDER_ON_DATE"]);
                            opt.REMINDER_TEXT = Convert.ToString(Controlsds.Tables[0].Rows[j]["REMINDER_TEXT"]);
                            opt.SAVE_AND_CONTINUE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SAVE_AND_CONTINUE"]);
                            opt.SCREEN_OUTS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCREEN_OUTS"]);
                            opt.SCREENOUT_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["SCREENOUT_PAGE"]);
                            opt.SEND_TO = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SEND_TO"]);
                            opt.STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["STATUS"]);
                            opt.SURVEY_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEY_ID"]);
                            opt.SURVEY_PASSWORD = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEY_PASSWORD"]);
                            opt.SURVEY_URL = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEY_URL"]);
                            opt.SURVEYCLOSETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON_CLOSE"]);
                            opt.SURVEYCONTROL_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEYCONTROL_ID"]);
                            opt.SURVEYLAUNCHTYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCHEDULE_ON"]);
                            opt.THANKYOU_PAGE = Convert.ToString(Controlsds.Tables[0].Rows[j]["THANKYOU_PAGE"]);
                            opt.TIME_FOR_URL_REDIRECTION = Convert.ToString(Controlsds.Tables[0].Rows[j]["TIME_FOR_URL_REDIRECTION"]);
                            opt.TOTAL_RESPONSES = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["TOTAL_RESPONSES"]);
                            opt.UNIQUE_RESPONDENT = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["UNIQUE_RESPONDENT"]);
                            opt.UNSUBSCRIBED = Convert.ToString(Controlsds.Tables[0].Rows[j]["UNSUBSCRIBED"]);
                            opt.URL_REDIRECTION = Convert.ToString(Controlsds.Tables[0].Rows[j]["URL_REDIRECTION"]);
                            opt.THANKU_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["THANKUPAGE_TYPE"]);
                            opt.CLOSE_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["CLOSEPAGE_TYPE"]);
                            opt.SCREENOUT_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SCREENOUTPAGE_TYPE"]);
                            opt.OVERQUOTA_PAGETYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["OVERQUOTAPAGE_TYPE"]);
                            opt.URLREDIRECTION_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["URLREDIRECTION_STATUS"]);
                            opt.FINISHOPTIONS_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["FINISHOPTIONS_STATUS"]);
                            opt.CONTACTLIST_ID = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["CONTACTLIST_ID"]);
                            opt.SURVEYLAUNCH_URL = Convert.ToString(Controlsds.Tables[0].Rows[j]["SURVEYLAUNCH_URL"]);
                            opt.EMAILLIST_TYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["EMAILLIST_TYPE"]);
                            opt.SURVEYURL_TYPE = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["SURVEYURL_TYPE"]);
                            opt.LAUNCHOPTIONS_STATUS = Convert.ToInt32(Controlsds.Tables[0].Rows[j]["LAUNCHOPTIONS_STATUS"]);

                            opt.EMAIL_SENDERNAME = Convert.ToString(Controlsds.Tables[0].Rows[j]["SENDER_NAME"]);
                            opt.EMAIL_REPLYTOADDRESS = Convert.ToString(Controlsds.Tables[0].Rows[j]["REPLY_TO_EMAILADDRESS"]);
                            sur.surveyOptns.Add(opt);
                        }
                    }
                }
            }
            return sur;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public string GetRespondentsForReports(int QuestionID)
    {
        int SurveyID = GetSurveyIDofQuestion(QuestionID);
        string usrLmitDet = GetUserLimit(SurveyID, "");
        int usrLmit = 0; string LienTyp = "";
        string[] det = usrLmitDet.Split('$');
        if (det != null && det.Length > 0)
        {
            LienTyp = Convert.ToString(det.GetValue(1));
            usrLmit = Convert.ToInt32(det.GetValue(0));
        }
        string RespondentIDs = "";
        string qry = "";
        if (usrLmit != -1 && usrLmit > 0)
        {
            qry = "select TOP " + usrLmit + " RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=(Select SURVEY_ID from osm_surveyquestion where QUESTION_ID=" + QuestionID + ")";
        }
        else if (usrLmit == -1)
        {
            qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=(Select SURVEY_ID from osm_surveyquestion where QUESTION_ID=" + QuestionID + ")";
        }
        try
        {
            dbm.OpenConnection(connect);
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qry);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    RespondentIDs += "," + Convert.ToString(ds.Tables[0].Rows[i]["RESPONDENT_ID"]);
                }
                if (RespondentIDs != null && RespondentIDs.Trim().Length > 0)
                    RespondentIDs = RespondentIDs.Substring(1);
            }
            return RespondentIDs;
        }
        catch
        {
            return RespondentIDs;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public int GetSurveyIDofQuestion(int QuestionID)
    {
        try
        {
            dbm.OpenConnection(connect);
            string qry;
            qry = "Select SURVEY_ID from osm_surveyquestion where QUESTION_ID=" + QuestionID;
            object obj = dbm.ExecuteScalar(CommandType.Text, qry);
            int ID = 0;
            if (obj != null)
                ID = Convert.ToInt32(obj);
            return (ID);
        }
        catch { return 0; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetRankingConstanSumCommentBoxTexResponses(int SurveyID, int QuestionID, ArrayList ans_options)
    {
        try
        {
            string usrLmitDet = GetUserLimit(SurveyID, "");
            int usrLmit = 0; string LienTyp = "";
            string[] det = usrLmitDet.Split('$');
            if (det != null && det.Length > 0)
            {
                LienTyp = Convert.ToString(det.GetValue(1));
                usrLmit = Convert.ToInt32(det.GetValue(0));
            }
            string RespondentIDs = "";
            string qry = "";
            if (usrLmit != -1 && usrLmit > 0)
            {

                //qry = "select TOP " + usrLmit + " RESPONDENT_ID,EMAIL_ID,STATUS,CREATED_ON  from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
                qry = "select TOP " + usrLmit + " RESPONDENT_ID,Sno = ROW_NUMBER() OVER (ORDER BY RESPONDENT_ID ASC),ors.EMAIL_ID,ors.STATUS,CREATED_ON,SESSIONID,oel.EMAIL_ADDRESS   from osm_responsesurvey ors, osm_emaillist oel where ors.deleted=0 and ors.STATUS='Complete' and oel.DELETED = 0  and ors.EMAIL_ID = oel.EMAIL_ID and SURVEY_ID=" + SurveyID;

            }
            else if (usrLmit == -1)
            {
                //  qry = "select RESPONDENT_ID,EMAIL_ID,STATUS,CREATED_ON  from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
                qry = "select RESPONDENT_ID,Sno = ROW_NUMBER() OVER (ORDER BY RESPONDENT_ID ASC),ors.EMAIL_ID,ors.STATUS,CREATED_ON,SESSIONID,oel.EMAIL_ADDRESS  from osm_responsesurvey ors, osm_emaillist oel where ors.deleted=0 and ors.STATUS='Complete' and oel.DELETED = 0  and ors.EMAIL_ID = oel.EMAIL_ID and SURVEY_ID=" + SurveyID;


            }
            dbm.OpenConnection(connect);
            DataSet ds = new DataSet();
            ds = dbm.ExecuteDataSet(CommandType.Text, qry);
            DataSet ds_ansidtabbles = new DataSet();
            string qry1 = "";
            for (int i = 0; i < ans_options.Count; i++)
            {
                qry1 = " Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where QUESTION_ID=" + QuestionID + " and ANSWER_ID=" + ans_options[i] + " and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";
                ds_ansidtabbles = dbm.ExecuteDataSet(CommandType.Text, qry1);
                if (i == 0)
                    ds.Tables[0].Columns.Add("SNO");
                ds.Tables[0].Columns.Add();

                for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                {
                    int flag = 0;
                    for (int l = 0; l < ds_ansidtabbles.Tables[0].Rows.Count; l++)
                    {

                        if (Convert.ToInt32(ds.Tables[0].Rows[k]["RESPONDENT_ID"].ToString()) == Convert.ToInt32(ds_ansidtabbles.Tables[0].Rows[l]["RESPONDENT_ID"].ToString()))
                        {
                            if (i == 0)
                                //    ds.Tables[0].Rows[k][i + 4] = Convert.ToString(k + 1);
                                //ds.Tables[0].Rows[k][i + 5] = Convert.ToString(ds_ansidtabbles.Tables[0].Rows[l]["ANSWER_TEXT"].ToString());
                                if (ds.Tables[0].Rows[k][5].ToString() == "")
                                {
                                    ds.Tables[0].Rows[k][5] = ds.Tables[0].Rows[k][6].ToString();
                                }

                            ds.Tables[0].Rows[k][i + 7] = Convert.ToString(k + 1);
                            ds.Tables[0].Rows[k][i + 8] = Convert.ToString(ds_ansidtabbles.Tables[0].Rows[l]["ANSWER_TEXT"].ToString());
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 0 && i == 0)
                        // ds.Tables[0].Rows[k][i + 4] = Convert.ToString(k + 1);
                        ds.Tables[0].Rows[k][i + 7] = Convert.ToString(k + 1);
                }

            }
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }

    }
    public DataSet GetExportInfo(int ExportID)
    {
        try
        {
            dbm.OpenConnection(connect);
            string qryinfo = " Select * from osm_surveyreports where DELETED=0 and REPORT_ID=" + ExportID;
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qryinfo);
            return ds;
        }
        catch
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public bool GetRawDataExportResponses(int SurveyID, string Resptype)
    {
        try
        {
            dbm.OpenConnection(connect);
            bool flag = false;
            string str_respondents = "Select res.RESPONDENT_ID from osm_responsesurvey res left outer join osm_emaillist eml on res.EMAIL_ID=eml.EMAIL_ID where SURVEY_ID=" + SurveyID + " and res.DELETED=0 and res.STATUS='" + Resptype + "'";
            str_respondents += " Select QUESTION_ID from osm_surveyquestion where SURVEY_ID=" + SurveyID + " and QUESTION_TYPE_ID IN('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','20') and DELETED=0 order by QUESTION_SEQ ";
            DataSet ds_respondents = dbm.ExecuteDataSet(CommandType.Text, str_respondents);

            if (ds_respondents != null && ds_respondents.Tables.Count > 1 && ds_respondents.Tables[0].Rows.Count > 0 && ds_respondents.Tables[1].Rows.Count > 0)
            {
                flag = true;
            }
            return flag;
        }
        catch { return false; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public int InsertExportInfo(int SurveyID, int rep_type, string rep_mode, int Usr_id)
    {
        SqlCommand cmd = new SqlCommand();
        try
        {
            dbm.OpenConnection(connect);
            SqlParameter objsqlParameter;
            objsqlParameter = cmd.Parameters.Add("@SurveyID", SqlDbType.Int);
            cmd.Parameters["@SurveyID"].Value = SurveyID;
            cmd.Parameters["@SurveyID"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Report_Type", SqlDbType.Int);
            cmd.Parameters["@Report_Type"].Value = rep_type;
            cmd.Parameters["@Report_Type"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Report_Mode", SqlDbType.NVarChar);
            cmd.Parameters["@Report_Mode"].Value = rep_mode;
            cmd.Parameters["@Report_Mode"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@CreatedOn", SqlDbType.NVarChar);
            cmd.Parameters["@CreatedOn"].Value = DateTime.UtcNow;
            cmd.Parameters["@CreatedOn"].Direction = ParameterDirection.Input;

            objsqlParameter = cmd.Parameters.Add("@USERID", SqlDbType.Int);
            cmd.Parameters["@USERID"].Value = Usr_id;
            cmd.Parameters["@USERID"].Direction = ParameterDirection.Input;

            objsqlParameter = cmd.Parameters.Add("@EXPORT_REP_ID", SqlDbType.Int);
            cmd.Parameters["@EXPORT_REP_ID"].Direction = ParameterDirection.Output;
            dbm.SPExecuteNonQuery(CommandType.StoredProcedure, "sp_ExportSurveyRep", cmd);
            int val = Convert.ToInt32(cmd.Parameters["@EXPORT_REP_ID"].Value);
            return val;
        }
        catch (Exception ex)
        {
            return 0;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetConstantSumForReports(int QuestionID, ArrayList ans_options, DateTime dt1, DateTime dt2)
    {
        try
        {
            string RespondentIDS = GetRespondentsForReports(QuestionID);
            dbm.OpenConnection(connect);
            DataSet ds = new DataSet();
            if (RespondentIDS != null && RespondentIDS.Length > 0)
            {
                string qry = " Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where QUESTION_ID=" + QuestionID + " and RESPONDENT_ID IN(" + RespondentIDS + ") and DELETED=0 ";
                if (dt1 > Convert.ToDateTime("1/1/0001 12:00:00 AM") && dt2 > Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                {
                    qry += " and RESPONSE_RECEIVED_ON between '" + dt1 + "' and '" + dt2 + "'";
                }
                qry += " order by RESPONDENT_ID";
                ds = dbm.ExecuteDataSet(CommandType.Text, qry);

            }
            return ds;

        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetRankingConstanSum(int QuestionID, ArrayList ans_options, DateTime dt1, DateTime dt2)
    {
        try
        {
            string RespondentIDS = GetRespondentsForReports(QuestionID);
            dbm.OpenConnection(connect);
            DataSet ds = new DataSet();
            if (RespondentIDS != null && RespondentIDS.Length > 0)
            {
                string qry = " Select RESPONSE_ID,RESPONDENT_ID,QUESTION_ID,ANSWER_ID,ANSWER_TEXT,RESPONSE_RECEIVED_ON from osm_responsequestions where QUESTION_ID=" + QuestionID + " and RESPONDENT_ID IN(" + RespondentIDS + ") and DELETED=0";
                if (dt1 > Convert.ToDateTime("1/1/0001 12:00:00 AM") && dt2 > Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                {
                    qry += " and RESPONSE_RECEIVED_ON between '" + dt1 + "' and '" + dt2 + "'";
                }
                qry += " order by RESPONDENT_ID";

                ds = dbm.ExecuteDataSet(CommandType.Text, qry);

            }
            return ds;

        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetTextResponses(int QuesID, int others, int SurveyID)
    {
        try
        {
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            string qry = "";
            string usrLmitDet = GetUserLimit(SurveyID, "");
            int usrLmit = 0; string LienTyp = "";
            string[] det = usrLmitDet.Split('$');
            if (det != null && det.Length > 0)
            {
                LienTyp = Convert.ToString(det.GetValue(1));
                usrLmit = Convert.ToInt32(det.GetValue(0));
            }
            dbm.OpenConnection(connect);
            if (usrLmit != -1 && usrLmit > 0)
            {
                qry = "select TOP " + usrLmit + " RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
            }
            else if (usrLmit == -1)
            {
                qry = "select RESPONDENT_ID from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;

            }
            ds = dbm.ExecuteDataSet(CommandType.Text, qry);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataSet ds_ansidtabbles = new DataSet();
                string qry1 = "";
                if (others == 1)
                    //qry1 = " select (ROW_NUMBER() OVER (ORDER BY RESPONDENT_ID)) as SNO,answer_text,RESPONDENT_ID from osm_responsequestions where question_id=" + QuesID + " and ANSWER_ID=0 and RESPONDENT_ID IN(" + qry + ") and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";
                    qry1 = " select (ROW_NUMBER() OVER (ORDER BY orq.RESPONDENT_ID)) as SNO,ors.SESSIONID,answer_text,orq.RESPONDENT_ID,orq.RESPONSE_RECEIVED_ON  as DateTimeStamp ,eml.EMAIL_ADDRESS,ors.called_number as CalledNumber from osm_responsequestions orq, osm_responsesurvey ors left outer join osm_emaillist eml on ors.EMAIL_ID=eml.EMAIL_ID  where question_id=" + QuesID + " and ANSWER_ID=0 and RESPONDENT_ID IN(" + qry + ") and   orq.DELETED=0  and ors.RESPONDENT_ID = orq.RESPONDENT_ID  and ors.DELETED = 0  order by RESPONDENT_ID,ANSWER_ID";
                else
                    //qry1 = " select (ROW_NUMBER() OVER (ORDER BY RESPONDENT_ID)) as SNO,answer_text,RESPONDENT_ID from osm_responsequestions where question_id=" + QuesID + " and RESPONDENT_ID IN(" + qry + ") and DELETED=0 order by RESPONDENT_ID,ANSWER_ID";
                    qry1 = " select (ROW_NUMBER() OVER (ORDER BY orq.RESPONDENT_ID)) as SNO,ors.SESSIONID,answer_text,orq.RESPONDENT_ID,orq.RESPONSE_RECEIVED_ON as DateTimeStamp,eml.EMAIL_ADDRESS,ors.called_number as CalledNumber from osm_responsequestions orq, osm_responsesurvey ors left outer join osm_emaillist eml on ors.EMAIL_ID=eml.EMAIL_ID  where question_id=" + QuesID + " and orq.RESPONDENT_ID IN(" + qry + ") and   orq.DELETED=0  and ors.RESPONDENT_ID = orq.RESPONDENT_ID  and ors.DELETED = 0 order by RESPONDENT_ID,ANSWER_ID";

                ds_ansidtabbles = dbm.ExecuteDataSet(CommandType.Text, qry1);
                ds1.Tables.Add();
                ds1.Tables[0].Columns.Add("SNO");
                ds1.Tables[0].Columns.Add("CallerID");
                ds1.Tables[0].Columns.Add("answer_text");
                ds1.Tables[0].Columns.Add("CalledNumber");
             //for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
               
             //   {
                    int flag = 0;

                    ds1.Tables[0].Rows.Add();
                    //for (int l = 0; l < ds_ansidtabbles.Tables[0].Rows.Count; l++)
                    //{

                    //    if (Convert.ToInt32(ds.Tables[0].Rows[k]["RESPONDENT_ID"].ToString()) == Convert.ToInt32(ds_ansidtabbles.Tables[0].Rows[l]["RESPONDENT_ID"].ToString()))
                    //    {
                    //        ds1.Tables[0].Rows[k][0] = Convert.ToString(k + 1);
                    //        ds1.Tables[0].Rows[k][1] = Convert.ToString(ds_ansidtabbles.Tables[0].Rows[l]["ANSWER_TEXT"].ToString()).Trim();
                    //        flag = 1;
                    //        break;

                    //    }
                    //}
                    ds1 = ds_ansidtabbles;
                    if (flag == 0)
                    {
                        //ds1.Tables[0].Rows[k][0] = Convert.ToString(k + 1);
                        //ds1.Tables[0].Rows[k][1] = "";
                    }
              //  }
            }
            return ds1;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet getsurveyquestions(int surid)
    {
        try
        {
            dbm.OpenConnection(connect);
            string QryGetSurveyQuestions = "";
            QryGetSurveyQuestions = "Select QUESTION_ID,QUSETION_LABEL,QUESTION_TYPE_ID,OTHER_ANS from osm_surveyquestion  where SURVEY_ID=" + "'" + surid + "' and (QUESTION_TYPE_ID='1' OR QUESTION_TYPE_ID='2' OR ";
            QryGetSurveyQuestions = QryGetSurveyQuestions + "QUESTION_TYPE_ID='3' OR QUESTION_TYPE_ID='4' OR QUESTION_TYPE_ID='10' OR QUESTION_TYPE_ID='11' OR QUESTION_TYPE_ID='12') and DELETED=0 order by QUESTION_SEQ";
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, QryGetSurveyQuestions);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet getcrossreports(int survey_id)
    {
        try
        {
            dbm.OpenConnection(connect);
            string QryGetSurveyCrossReports = "Select CROSS_REPORT_ID,SURVEY_ID,QUESTION_ROW_ID,QUESTION_COLUMN_ID,CREATED_ON,CREPORT_NAME from osm_crossreports  where SURVEY_ID=" + survey_id;
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, QryGetSurveyCrossReports);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public int NewCrossReport(int survey_id, int question_row_id, int question_column_id, DateTime CreatedOn, String creport_name, int Created_by)
    {
        SqlCommand cmd = new SqlCommand();
        try
        {
            dbm.OpenConnection(connect);
            SqlParameter objsqlParameter;
            objsqlParameter = cmd.Parameters.Add("@survey_id", SqlDbType.Int);
            cmd.Parameters["@survey_id"].Value = survey_id;
            cmd.Parameters["@survey_id"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@question_row_id", SqlDbType.Int);
            cmd.Parameters["@question_row_id"].Value = question_row_id;
            cmd.Parameters["@question_row_id"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@question_column_id", SqlDbType.Int);
            cmd.Parameters["@question_column_id"].Value = question_column_id;
            cmd.Parameters["@question_column_id"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ModifiedOn", SqlDbType.DateTime);
            cmd.Parameters["@ModifiedOn"].Value = DateTime.UtcNow;
            cmd.Parameters["@ModifiedOn"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@creport_name", SqlDbType.VarChar);
            cmd.Parameters["@creport_name"].Value = creport_name;
            cmd.Parameters["@creport_name"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Created_by", SqlDbType.Int);
            cmd.Parameters["@Created_by"].Value = Created_by;
            cmd.Parameters["@Created_by"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@REPORT_ID", SqlDbType.Int);
            cmd.Parameters["@REPORT_ID"].Direction = ParameterDirection.Output;
            dbm.SPExecuteNonQuery(CommandType.StoredProcedure, "sp_crossreports", cmd);
            int val = Convert.ToInt16(cmd.Parameters["@REPORT_ID"].Value);
            string val1 = cmd.Parameters["@returnmessage"].Value.ToString();
            return val;
        }
        catch (Exception ex)
        {
            return 0;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet getsurveyanswers(int q1_id, int q2_id, int SurveyID)
    {
        try
        {
            string Respondents = GetActiveRespondent(SurveyID);
            dbm.OpenConnection(connect);
            string str = " select ANSWER_ID,ANSWER_OPTIONS from osm_answeroptions where QUESTION_ID=" + q1_id;
            str += " select ANSWER_ID,ANSWER_OPTIONS from osm_answeroptions where QUESTION_ID=" + q2_id;
            str += " select RESPONSE_ID,RQ1.RESPONDENT_ID,ANSWER_ID,ANSWER_TEXT from osm_responsequestions RQ1 inner join osm_responsesurvey R1 on RQ1.RESPONDENT_ID=R1.RESPONDENT_ID where RQ1.DELETED=0 and QUESTION_ID=" + q1_id + "  and RQ1.RESPONDENT_ID IN(" + Respondents + ")";
            str += " select RESPONSE_ID,RQ1.RESPONDENT_ID,ANSWER_ID,ANSWER_TEXT from osm_responsequestions RQ1 inner join osm_responsesurvey R1 on RQ1.RESPONDENT_ID=R1.RESPONDENT_ID where RQ1.DELETED=0 and QUESTION_ID=" + q2_id + " and RQ1.RESPONDENT_ID IN(" + Respondents + ")";
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, str);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public Survey UpdateQuestionChart(int SurveyID, int QuestionID, string ChartTitle, string Comments, string ChartType, string ChartAppearance, string ChartPalette, string ChartAligment, string ChartDock, int ShowLabel, int LabelAngle, int ValueAspercent, int ZoomPercent, int Transparency, int PerspectiveAngle, int MarkerSize, int ShowMarker, int Inverted, string LabelPosition, string MarkerKind, string HoleRadius, string DiagramType, string TextDirection, string FunctionType, string ExplodedPoint, string LegendHorizontalAlignment, string LegendVerticalAlignment, int LegendMaxHorizontalPercentage, int LegendMaxVerticalPercentage, string LegendDirection, int LegendEquallySpacedItems, int HideLegand, int HideXaixs, int Staggered)
    {
        SqlCommand cmd = new SqlCommand();
        try
        {
            dbm.OpenConnection(connect);
            SqlParameter objsqlParameter;
            objsqlParameter = cmd.Parameters.Add("@ChartTitle", SqlDbType.VarChar);
            cmd.Parameters["@ChartTitle"].Value = ChartTitle;
            cmd.Parameters["@ChartTitle"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Comments", SqlDbType.VarChar);
            cmd.Parameters["@Comments"].Value = Comments;
            cmd.Parameters["@Comments"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ChartType", SqlDbType.VarChar);
            cmd.Parameters["@ChartType"].Value = ChartType;
            cmd.Parameters["@ChartType"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ChartAppearance", SqlDbType.VarChar);
            cmd.Parameters["@ChartAppearance"].Value = ChartAppearance;
            cmd.Parameters["@ChartAppearance"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ChartPalette", SqlDbType.VarChar);
            cmd.Parameters["@ChartPalette"].Value = ChartPalette;
            cmd.Parameters["@ChartPalette"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ChartAligment", SqlDbType.VarChar);
            cmd.Parameters["@ChartAligment"].Value = ChartAligment;
            cmd.Parameters["@ChartAligment"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ChartDock", SqlDbType.VarChar);
            cmd.Parameters["@ChartDock"].Value = ChartDock;
            cmd.Parameters["@ChartDock"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@QuestionID", SqlDbType.Int);
            cmd.Parameters["@QuestionID"].Value = QuestionID;
            cmd.Parameters["@QuestionID"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ShowLabel", SqlDbType.Int);
            cmd.Parameters["@ShowLabel"].Value = ShowLabel;
            cmd.Parameters["@ShowLabel"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LabelAngle", SqlDbType.Int);
            cmd.Parameters["@LabelAngle"].Value = LabelAngle;
            cmd.Parameters["@LabelAngle"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ValueAspercent", SqlDbType.Int);
            cmd.Parameters["@ValueAspercent"].Value = ValueAspercent;
            cmd.Parameters["@ValueAspercent"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ZoomPercent", SqlDbType.Int);
            cmd.Parameters["@ZoomPercent"].Value = ZoomPercent;
            cmd.Parameters["@ZoomPercent"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Transparency", SqlDbType.Int);
            cmd.Parameters["@Transparency"].Value = Transparency;
            cmd.Parameters["@Transparency"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@PerspectiveAngle", SqlDbType.Int);
            cmd.Parameters["@PerspectiveAngle"].Value = PerspectiveAngle;
            cmd.Parameters["@PerspectiveAngle"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@MarkerSize", SqlDbType.Int);
            cmd.Parameters["@MarkerSize"].Value = MarkerSize;
            cmd.Parameters["@MarkerSize"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ShowMarker", SqlDbType.Int);
            cmd.Parameters["@ShowMarker"].Value = ShowMarker;
            cmd.Parameters["@ShowMarker"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Inverted", SqlDbType.Int);
            cmd.Parameters["@Inverted"].Value = Inverted;
            cmd.Parameters["@Inverted"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LabelPosition", SqlDbType.VarChar);
            cmd.Parameters["@LabelPosition"].Value = LabelPosition;
            cmd.Parameters["@LabelPosition"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@MarkerKind", SqlDbType.VarChar);
            cmd.Parameters["@MarkerKind"].Value = MarkerKind;
            cmd.Parameters["@MarkerKind"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@HoleRadius", SqlDbType.VarChar);
            cmd.Parameters["@HoleRadius"].Value = HoleRadius;
            cmd.Parameters["@HoleRadius"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@DiagramType", SqlDbType.VarChar);
            cmd.Parameters["@DiagramType"].Value = DiagramType;
            cmd.Parameters["@DiagramType"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@TextDirection", SqlDbType.VarChar);
            cmd.Parameters["@TextDirection"].Value = TextDirection;
            cmd.Parameters["@TextDirection"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@FunctionType", SqlDbType.VarChar);
            cmd.Parameters["@FunctionType"].Value = FunctionType;
            cmd.Parameters["@FunctionType"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@ExplodedPoint", SqlDbType.VarChar);
            cmd.Parameters["@ExplodedPoint"].Value = ExplodedPoint;
            cmd.Parameters["@ExplodedPoint"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendHorizontalAlignment", SqlDbType.VarChar);
            cmd.Parameters["@LegendHorizontalAlignment"].Value = LegendHorizontalAlignment;
            cmd.Parameters["@LegendHorizontalAlignment"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendVerticalAlignment", SqlDbType.VarChar);
            cmd.Parameters["@LegendVerticalAlignment"].Value = LegendVerticalAlignment;
            cmd.Parameters["@LegendVerticalAlignment"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendDirection", SqlDbType.VarChar);
            cmd.Parameters["@LegendDirection"].Value = LegendDirection;
            cmd.Parameters["@LegendDirection"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendMaxHorizontalPercentage", SqlDbType.Int);
            cmd.Parameters["@LegendMaxHorizontalPercentage"].Value = LegendMaxHorizontalPercentage;
            cmd.Parameters["@LegendMaxHorizontalPercentage"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendMaxVerticalPercentage", SqlDbType.Int);
            cmd.Parameters["@LegendMaxVerticalPercentage"].Value = LegendMaxVerticalPercentage;
            cmd.Parameters["@LegendMaxVerticalPercentage"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@LegendEquallySpacedItems", SqlDbType.Int);
            cmd.Parameters["@LegendEquallySpacedItems"].Value = LegendEquallySpacedItems;
            cmd.Parameters["@LegendEquallySpacedItems"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@HideLegand", SqlDbType.Int);
            cmd.Parameters["@HideLegand"].Value = HideLegand;
            cmd.Parameters["@HideLegand"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@HideXaixs", SqlDbType.Int);
            cmd.Parameters["@HideXaixs"].Value = HideXaixs;
            cmd.Parameters["@HideXaixs"].Direction = ParameterDirection.Input;
            objsqlParameter = cmd.Parameters.Add("@Staggered", SqlDbType.Int);
            cmd.Parameters["@Staggered"].Value = Staggered;
            cmd.Parameters["@Staggered"].Direction = ParameterDirection.Input;
            dbm.SPExecuteNonQuery(CommandType.StoredProcedure, "sp_UpdateQuestionChart", cmd);
            return GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetSurveyAlerts(int Survey_Id)
    {
        try
        {
            dbm.OpenConnection(connect);
            string qrygetsurveyalerts = " Select ALERT_ID,ALERT_TYPE,ALERT_INFO,ALERT_DATE from osm_alertmanagement where ALERT_ID in(select max(alert_id) from osm_alertmanagement where DELETED=0 and SURVEY_ID=" + Survey_Id + "group by alert_type )";
            qrygetsurveyalerts += " Select count(RESPONDENT_ID) from osm_responsesurvey where SURVEY_ID=" + Survey_Id;
            qrygetsurveyalerts += " Select * from osm_responsesurvey where STATUS='Complete' and SURVEY_ID=" + Survey_Id + " order by RESPONDENT_ID ";
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qrygetsurveyalerts);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public DataSet GetIndiviudalRespIds(int Survey_ID, bool responstype, bool validresp, bool excludedresp, bool partialresp, bool partialtocompresp)
    {
        try
        {
            string RespondentIDS = GetActiveRespondent(Survey_ID);

            dbm.OpenConnection(connect);
            //string QryGetResponseDetails = "Select res.RESPONDENT_ID,res.EMAIL_ID,res.STATUS,res.CREATED_ON,eml.EMAIL_ADDRESS,res.FLAG_COMPLETESTATUS,res.DELETED from osm_responsesurvey res left outer join osm_emaillist eml on res.EMAIL_ID=eml.EMAIL_ID where SURVEY_ID=" + Survey_ID;

            string QryGetResponseDetails = "Select res.RESPONDENT_ID,res.EMAIL_ID,res.STATUS,res.CREATED_ON,eml.EMAIL_ADDRESS,res.FLAG_COMPLETESTATUS,res.DELETED,res.sessionid from osm_responsesurvey res left outer join osm_emaillist eml on res.EMAIL_ID=eml.EMAIL_ID where SURVEY_ID=" + Survey_ID;

            if (validresp && excludedresp && partialresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') ";
            }
            else if (validresp && excludedresp && partialresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') ";
            }
            else if (validresp && partialresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') and res.DELETED=0 ";
            }
            else if (partialresp && excludedresp && partialtocompresp)
            {
                QryGetResponseDetails += " and ((res.status='Complete' and res.FLAG_COMPLETESTATUS=1) or (res.STATUS='Partial' and res.FLAG_COMPLETESTATUS=0) or (res.STATUS='Complete' and res.DELETED=1))";
            }
            else if (validresp && excludedresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status='Complete'";
            }
            else if (validresp && partialresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete','Partial') and res.DELETED=0";
            }
            else if (validresp && excludedresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete')";
            }
            else if (validresp && partialtocompresp)
            {
                QryGetResponseDetails += " and res.status IN('Complete') and res.DELETED=0";
            }
            else if (partialresp && excludedresp)
            {
                QryGetResponseDetails += " and ((res.status='Complete' and res.DELETED=1) or (res.STATUS='Partial'))";
            }
            else if (excludedresp && partialtocompresp)
            {
                QryGetResponseDetails += " and ((res.status='Complete' and res.FLAG_COMPLETESTATUS=1) or (res.STATUS='Complete' and res.DELETED=1))";
            }
            else if (partialresp && partialtocompresp)
            {
                QryGetResponseDetails += " and ((res.status='Partial') or (res.STATUS='Complete' and res.FLAG_COMPLETESTATUS=1))";
            }
            else if (validresp)
            {
                QryGetResponseDetails += " and res.status='Complete' and res.DELETED=0 ";
            }
            else if (partialresp)
            {
                QryGetResponseDetails += " and res.status='Partial' and res.DELETED=0 ";
            }
            else if (excludedresp)
            {
                QryGetResponseDetails += " and res.status='Complete' and res.DELETED=1 ";
            }
            else if (partialtocompresp)
            {
                QryGetResponseDetails += " and res.status='Complete' and res.DELETED=0 and res.FLAG_COMPLETESTATUS=1 ";
            }
            if (responstype)
            {
                // QryGetResponseDetails += " and res.RESPONDENT_ID IN(Select DISTINCT ques.RESPONDENT_ID ";
                QryGetResponseDetails += " and res.RESPONDENT_ID IN(Select  ques.RESPONDENT_ID ";
                QryGetResponseDetails += " from osm_responsequestions ques inner join osm_responsesurvey res on  ";
                QryGetResponseDetails += " res.RESPONDENT_ID=ques.RESPONDENT_ID where ques.DELETED=0 ";
                QryGetResponseDetails += " and res.SURVEY_ID=" + Survey_ID + ")";
            }
            QryGetResponseDetails += " order by res.STATUS";
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, QryGetResponseDetails);
            return ds;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public SurveyResponse GetSurveyResponses(int SurveyID, int Respondent_ID)
    {
        try
        {
            dbm.OpenConnection(connect);
            string str1 = "select * from osm_responsequestions where RESPONDENT_ID=" + Respondent_ID + " and DELETED=0 order by QUESTION_ID";

            DataSet ds_resp_ques = dbm.ExecuteDataSet(CommandType.Text, str1);
            SurveyResponse res = new SurveyResponse();
            res.ResponseQuestion = new List<ResponseQuestion>();
            for (int k = 0; k < ds_resp_ques.Tables[0].Rows.Count; k++)
            {
                ResponseQuestion ques_res = new ResponseQuestion();
                ques_res.RESPONSE_ID = Convert.ToInt32(ds_resp_ques.Tables[0].Rows[k]["RESPONSE_ID"]);
                ques_res.RESPONDENT_ID = Convert.ToInt32(ds_resp_ques.Tables[0].Rows[k]["RESPONDENT_ID"]);
                ques_res.QUESTION_ID = Convert.ToInt32(ds_resp_ques.Tables[0].Rows[k]["QUESTION_ID"]);
                //ques_res.ANSWER_ID = Convert.ToInt32(ds_resp_ques.Tables[0].Rows[k]["ANSWER_ID"]);
                //ques_res.ANSWER_TEXT = Convert.ToString(ds_resp_ques.Tables[0].Rows[k]["ANSWER_TEXT"]);
                if (ds_resp_ques.Tables[0].Rows[k]["ANSWER_ID"] != "0")
                {
                    ques_res.ANSWER_ID = Convert.ToInt32(ds_resp_ques.Tables[0].Rows[k]["ANSWER_ID"]);
                }
                if (ds_resp_ques.Tables[0].Rows[k]["ANSWER_TEXT"].ToString() != "NO FILE")
                {
                    ques_res.ANSWER_TEXT = Convert.ToString(ds_resp_ques.Tables[0].Rows[k]["ANSWER_TEXT"]);
                }
                res.ResponseQuestion.Add(ques_res);
            }
            return res;
        }
        catch { return null; }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public void ExcludeIndividualResp(int respondent_id, int SurveyID, int restore_type)
    {
        try
        {
            dbm.OpenConnection(connect);
            string qryQues = "";
            if (restore_type == 0)
                qryQues = "update osm_responsesurvey set DELETED=1 where RESPONDENT_ID=" + respondent_id;
            else if (restore_type == 1)
                qryQues = "update osm_responsesurvey set DELETED=0, FLAG_COMPLETESTATUS=0,VISABLE_LIMIT=0,STATUS='Partial' where RESPONDENT_ID=" + respondent_id;
            dbm.ExecuteNonQuery(CommandType.Text, qryQues);

        }
        catch { }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public bool IncludeResponsetoComplete(int respondent_id, int SurveyID, int restore_value)
    {
        try
        {
            bool ret_value = false;
            int cont = 0;
            string usrLmitDet = GetUserLimit(SurveyID, "");
            int usrLmit = 0; string LienTyp = "";
            string[] det = usrLmitDet.Split('$');
            if (det != null && det.Length > 0)
            {
                LienTyp = Convert.ToString(det.GetValue(1));
                usrLmit = Convert.ToInt32(det.GetValue(0));
            }
            if (LienTyp.ToLower() == "free")
                cont = GetResponseSurveyCount(SurveyID);
            else
                cont = GetUserSurveysResponseCount(SurveyID, 0);
            string qry = "";
            if (usrLmit == -1 || (usrLmit > 0 && usrLmit > cont))
            {
                dbm.OpenConnection(connect);
                if (restore_value == 0)
                    qry += " update osm_responsesurvey set STATUS='Complete', VISABLE_LIMIT=1, FLAG_COMPLETESTATUS=1,DELETED=0 where RESPONDENT_ID=" + respondent_id;
                else if (restore_value == 1)
                    qry += " update osm_responsesurvey set STATUS='Complete', VISABLE_LIMIT=1,  FLAG_COMPLETESTATUS=0,DELETED=0 where RESPONDENT_ID=" + respondent_id;
                dbm.ExecuteNonQuery(CommandType.Text, qry);
                ret_value = true;
            }
            return ret_value;
        }
        catch
        {
            return false;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public int GetResponseSurveyCount(int SurveyID)
    {
        int cnt = 0;
        string qry = "select count(RESPONDENT_ID) from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID=" + SurveyID;
        try
        {
            dbm.OpenConnection(connect);
            object obj = dbm.ExecuteScalar(CommandType.Text, qry);
            if (obj != null)
            {
                cnt = Convert.ToInt32(obj);
            }
            return cnt;
        }
        catch
        {
            return cnt;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }
    public int GetUserSurveysResponseCount(int SurveyID, int UserID)
    {
        int cnt = 0;
        string qry = "select SURVEY_ID from osm_survey where userid= ";
        if (UserID > 0)
            qry += "'" + UserID + "'";
        else
            qry += "(select UserID from osm_survey where SURVEY_ID=" + SurveyID + ")";
        try
        {
            dbm.OpenConnection(connect);
            DataSet ds = dbm.ExecuteDataSet(CommandType.Text, qry);
            if (ds != null && ds.Tables.Count > 0)
            {
                string Surveys = "";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Surveys += "," + ds.Tables[0].Rows[i]["SURVEY_ID"];
                }
                if (Surveys != null && Surveys.Trim().Length > 1)
                {
                    Surveys = Surveys.Substring(1);
                    string qry1 = "select count(RESPONDENT_ID) from osm_responsesurvey where deleted=0 and STATUS='Complete' and SURVEY_ID in (" + Surveys + ")";
                    object obj = dbm.ExecuteScalar(CommandType.Text, qry1);
                    if (obj != null)
                    {
                        cnt = Convert.ToInt32(obj);
                    }
                }
            }
            return cnt;
        }
        catch
        {
            return cnt;
        }
        finally
        {
            dbm.Connection.Dispose();
            dbm.Connection.Close();
        }
    }

    /// <summary>
    /// Gets the response count.
    /// </summary>
    public int GetResponseCount(int surveyId)
    {
        return ServiceFactory.GetService<RespondentService>().GetResponseCount(surveyId);
    }

}