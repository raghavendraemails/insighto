﻿using System;
using System.Data;
using System.Data.SqlClient;

interface IPollCreation
{
    int AddNameandType(string name, string type, int userid, int parentid);
    
    void deleteAnswerOptions(int pollid);
    
    DataSet DeletePoll(int pollid);
    
    int DeletePollByUserId(int userid, int pollid);
    
    DataTable getAllUsersPollLicences();
    
    DataTable getAnswerOptions(int pollid);
    
    DataSet GetCCPollInvoiceInfo(int userid, int orderid, int historyid);
    
    DataTable getImageNames();
    
    DataSet getInstaMojoForPolls(string licensetype);
    
    DataTable getLicencePoll(int userid);
    
    DataSet getpollchartinfo(int pollid);
    
    DataSet getPollCities(string countryname);
    
    DataSet getPollCountries();
    
    DataSet getPollInfo(int pollid);
    
    DataSet getPollInfofromparent(int parentid);
    
    DataSet getPollLicenceInfo(int pollid);
    
    bool getPollName(int userId, string name);
    
    DataSet getPollOrder(string orderid);
    
    DataSet getPollOrderInfo(int orderid, int userid);
    
    DataTable getPollPaymentInformation(string paymentid, string transactiontype);
    
    DataTable getPollSettingsInfo(int pollid);
    
    DataTable getPollThemes(int userid);
    
    DataTable GetQuestionInfo(int PollId);
    
    DataSet getResInfo(int respondentId);
    
    bool GetSettingsInfo(int pollid);
    
    int getSubType(int userid);
    
    DataTable getsuperPollpageurls();
    
    DataTable getsuperPollThemes(int userid);
    
    int GetSurveyCreditCount(int userid);
    
    DataTable getUsersForPollsPromo();
    
    DataSet getVotesInfo(int pollid);

    void InsertintoPollInfo(int PollId, string Question, string VideoCode, string Imagelink, int Questiontype, int Commentreq, int uniqueRespondent, string votebuttontext, string invitationmessage, int isemailrequired, int isphonerequired, string redirecturl, int showsubmitbutton, string submitbuttontext, int issamethankyoumessageusedissamethankyoumessageused);

    void InsertintoPollSettings(int pollid, string pollbgcolor, string questionfont, string questionbgcolor, string questioncolor, string questionstyle, string questionfw, string questionuline, string questionalign, string questionfs, string answerfont, string answercolor, string answerfw, string answeruline, string answerstyle, string answeralign, string answerfs, string votebgcolor, string votefont, string votefs, string votestyle, string voteul, string votecolor, string votefw, string votealign, string sponsorlogo, string sponsortext, string logolink, string messagebgcolor, string messagecolor, string messagefont, string messagestyle, string messageul, string messagealign, string messagefs, string messagefw, string googletext, string twittertext, string fbtext, int votecount, string sharetext, string votetext, int chartwidth, int chartheight, string weblinkbgcolor);
    
    void InsertintoQuestion(int PollId, string questiontext);
    
    int InsertIntoRespondent(int pollid, string status, string country, string state, string city, string ipaddress, string longitude, string latitude, string device, string platform, string brand, string mobplatform, string srcurl);
    
    void InsertIntoResponse(int respondentid, int answerid);
    
    void InsertPollSettingsFromExistingPoll(int copyFromPollId, int newPollId);
    
    void RemoveAnswerOption(int AnswerId, string AnswerOption);
    
    void SaveAnswerOptions(string AnswerOption, int PollId, string InvitationThankYouMessage);
    
    void SaveIframeInfo(int pollid, int width, int height, int recwidth, int recheight);
    
    void SaveImageSpec(int pollid, int width, int height);
    
    void SaveLaunchInfo(int pollid, int unique, string polllaunchurl, string embedcode, int launchtype);
    
    void SaveModiyIframeSpec(int pollid, int height);
    
    void SavePollPaymentInformation(string paymentid, decimal amount, string productslug, string producttitle, int orderid, string status, string type);
    
    SqlConnection strconnectionstring();

    void UpdateAnswerOptions(int AnswerId, string AnswerOption, string InvitationThankYouMessage);
    
    DataSet updateDomainname(int userid, string domainname);
    
    void UpdateEmailtoSurveyUser(int userid);
    
    DataSet updateImage(int pollid, string imagetext);
    
    void UpdateIntoRespondent(int respondentid, string status, string comments);
    
    void UpdateOnlySubscriptionType(int type, string activity, int userid);
    
    void UpdatePollCampaignUser(int userid);
    
    void UpdatePollDetails(int pollid, int userid, string resultaccess, string resulttext, string poweredby, int questiontype, string questiontext, int commentreq, string videolink, string imagelink, string uniquerespondent);
    
    void UpdatePollDimensions(int pollid, int userid, int iframeheight, int iframewidth, int recmheight, int recmwidth);
    
    void UpdatePollinfo(int pollid, string resultaccess, string resulttext, string poweredby);
    
    void UpdatePollLicenseType(int userid, string polllicensetype, int subscriptiontype);
    
    void UpdatePollLicenseTypeOnSignUp(int userid, string polllicensetype, int subscriptiontype);
    
    void UpdatePollName(string name, string type, int pollid);
    
    void updatePollNameOnly(int pollid, string pollname);
    
    void UpdatePollPaymentType(int userid, string polllicensetype, int orderid);

    void UpdatePollSettings(int pollid, string pollbgcolor, string questionfont, string questionbgcolor, string questioncolor, string questionstyle, string questionfw, string questionuline, string questionalign, string questionfs, string answerfont, string answercolor, string answerfw, string answeruline, string answerstyle, string answeralign, string answerfs, string votebgcolor, string votefont, string votefs, string votestyle, string voteul, string votecolor, string votefw, string votealign, string sponsorlogo, string sponsortext, string logolink, string messagebgcolor, string messagecolor, string messagefont, string messagestyle, string messageul, string messagealign, string messagefs, string messagefw, string googletext, string twittertext, string fbtext, int votecount, string sharetext, string votetext, int chartwidth, int chartheight, int autoadjustdimensions, string weblinkbgcolor);
    
    void UpdateQuestion(int PollId, string questiontext);
    
    void updateReplacedPollNameOnlyForThisUser(int userid, int pollid, string pollname);
    
    void UpdateSliderPollsettings(string position, string sliderdisplay, int freqofvisit, int pollid, string countryname, string cityname, string devicename);
    
    void UpdateSponsorDetails(int pollid, string sponsorlogo, string sponsortext, string logolink);
    
    void UpdateStatus(int pollid, string status);
    
    int UpdateStatusByUser(int userid, int pollid, string status);
    
    void UpdateSuperpollStatus(int superpollpollid);
    
    void UpdateUserLastActivity(string activity, int userid);
    
    void UpdateUserSubscriptionType(int type, string activity, int userid);
    
    void UpdatePollWebLinkBgColor(int pollid,int userid, string color);
}