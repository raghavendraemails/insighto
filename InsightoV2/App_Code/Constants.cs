﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


/// <summary>
/// Summary description for Constants
/// </summary>
public class Constants
{
	public Constants()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    #region "Application Constants"
    public const string CATEGORY = "SURVEY_CATEGORY";
    public const string OTHERS = "Others";
    public const string MYFOLDER = "My Folder";
    public const string PARAMETER = "PARAMETER";
    public const string PARAM_VALUE = "PARAM_VALUE";
    public const string COUNTRY = "COUNTRY";
    public const string TAX = "TAXPERCENT";
    public const string MERCHANTID = "MerchantId";
    public const string REDIRECTIONURL = "REDIRECTIONURL";
    public const string WORKINGKEY = "WORKINGKEY";
    public const string PAYBYCHEQUE = "2";
    public const string SUPPORTEMAIL = "ContactusEmailAddress";
    public const string ORDERTYPE = "N";
    public const string UPGRADEORDERTYPE = "U";
    public const string RENEWORDERTYPE = "R";  
    public const string COMPANY_SIZE = "COMPANY_SIZE";
    public const string WORK_INDUSTRY = "WORK_INDUSTRY";
    public const string JOB_FUNCTION = "JOB_FUNCTION";
    public const string SELECT = "--Select--";
    public const string SELECTVALUE = " ";
    public const string PHONE_SEPARATER = "-";
    public const string CURRENTACCOUNTNO = "CURRENT_ACCOUNT_NO";
    public const string KNOWIENCEADDRESS = "KNOWIENCE_ADDRESS";
    public const string KNOWIENCEFAVOUROFADDRESS="KNOWIENCE_FAVOUR_OF_ADDRESS";
    public const string MATRIXSIDEBYSIDE = "SidebySide";
    public const string FONTTYPE = "FONT_TYPE";
    public const string FONTCOLOR = "FONT_COLOR";
    public const string FONTSIZE = "FONT_SIZE";
    public const string SURVEYID = "SurveyId";
    public const string USERID = "UserId";
    public const string CONTACTID="ContactId";
    public const int ExternalLaunch = 2;
    public const int InternalLaunch = 1;

    public const string STATE = "STATE";
    public const string WIDGETCATEGORY = "WIDGETCATEGORY";

    public const string FirstName = "FirstName";
    public const string lastName = "LastName";
    public const string City = "CityName";
    public const string State = "StateName";
    public const string Company = "Company";
    public const string UserType = "UserType";
    public const string WorkIndustry = "WorkIndustry";
    public const string JobFunction = "JobFunction";
    public const string CompanySize = "CompanySize";
    public const string LicenseType = "LicenseType";
    public const string EmailId = "EmailId";
    public const string CreatedFrom = "CreatedDateFrom";
    public const string CreatedTo = "CreatedDateTo";
    public const string ExpiryDateFrom = "ExpiryDateFrom";
    public const string ExpiryDateTo = "ExpiryDateTo";
    public const string CustomerId = "CustomerId";
    public const string LicenseExpiryDateFrom = "LicenseExpiryDateFrom";
    public const string LicenseExpiryDateTo = "LicenseExpiryDateTo";
    public const string LicenseExpiryPeriod = "licenseExpiryPeriod";

    public const string ManageAdmin = "Management Admin";
    public const string CustomerAdmin = "Customer Admin";
    public const string TechAdmin = "Tech Admin";
    public const string AccountAdmin = "Account Admin";
    public const string FolderName = "FOLDER_NAME";
    public const string FolderId = "FOLDER_ID";
    public const string Attentionvalues = "Attentionvalues";

    public const string Active = "Active";
    public const string InActive = "InActive";
    public const string Deactivate = "Deactivate";
    public const string Activate = "Activate";
    public const string DirectDeposit = "1";

    public const string SuperAdmin = "Super Admin";
    public const string FAQCREATE = "Create";
    public const string FAQDESIGN = "Design";
    public const string FAQLAUNCH = "Launch";
    public const string FAQANALYZE = "Analyze";
    public const string FAQMANAGE = "Manage";

    public const string FAQSingle = "Pps";

    public const string LICENSETYPE = "LicenseType";
    #endregion

    #region ""
    public const string SESSION_USERID = "UserId";
    public const string SESSION_USERNAME = "UserName";
    #endregion 
}

