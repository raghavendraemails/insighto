﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="ManageFolderPopUp.aspx.cs" Inherits="Insighto.Pages.ManageFolderPopUp" culture="auto" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" Runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
           
            <h3> <asp:Label ID="lblTitle" runat="server" Text="Create Folder"></asp:Label>
                </h3>
        </div>
      <%-- <div class="popupClosePanel">
			<a href="#" class="popupCloseLink" onclick ="closeModalSelf(false,'');" title="Close">&nbsp;</a>
		</div>--%>
     
        <!-- //popup heading -->
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="popupContentPanel" >
        <!-- popup content panel -->
           <div class="successPanel" id="divSucessPanel" runat="server" visible="false">
                <div><asp:Label ID="lblSuccessMsg" runat="server" Text="Inserted Successfully." meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
           </div>
           <div class="errorMessagePanel" id="divErrMsg" runat="server" visible="false">
            <div><asp:Label ID="lblErrMsg" runat="server" Text="Folder name already exists." meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
            </div>

        <div class="formPanel">
            <!-- form -->
            <div class="forgotpanel">
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblFolderName" runat="server" Text="Enter Folder Name" meta:resourcekey="lblFolderNameResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtFolderName" runat="server" CssClass="textBoxMedium" meta:resourcekey="txtFolderNameResource1"
                            MaxLength="25" />
                           <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regFirstName" CssClass="lblRequired" ControlToValidate="txtFolderName"
                                runat="server" ErrorMessage="Please enter folder name  less than 25 Characters" ValidationExpression="^.{1,25}$"
                                Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqEmailId" ControlToValidate="txtFolderName" CssClass="lblRequired"
                            meta:resourcekey="ReqFolderResource1" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>

                  <div class="con_login_pnl">
                    <div class="loginlbl"> <asp:Label ID="lblbtnNewFolder" runat="server" AssociatedControlID="ibtnSave" meta:resourcekey="lblbtnNewFolderResource1" />
                    </div>
                    <div class="loginControls"> <asp:ImageButton ID="ibtnSave" runat="server" ImageUrl="App_Themes/Classic/Images/button_save.gif"
                            ToolTip="Save" OnClick="ibtnSave_Click" meta:resourcekey="ibtnSaveResource1" />
                    </div>
                </div>
               <div class="con_login_pnl" id="successMessage">
                    <div class="reqlbl btmpadding">
                        <asp:Label ID="lbl" runat="server" meta:resourcekey="lblResource1"></asp:Label>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>

            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>
        <script type="text/javascript">
            $(document).ready(function () {
//                $("input:text").change(function () {
//                    value = $(this).val();
//                    $(this).val(value.replace(/^\s+|\s+$/g, ''));
//                    value = $(this).val();
//                });
            });
   </script>
</asp:Content>

