﻿using System;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using Insighto.Business.Enumerations;
using App_Code;

public partial class ManageFolders : BasePage
{

   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lnkCreateFolder.HRef = EncryptHelper.EncryptQuerystring("ManageFolderPopUp.aspx", "surveyFlag=" + SurveyFlag);
            hdnSurveyFlag.Value = SurveyFlag.ToString();
            lblErrMsg.Text = String.Format(Utilities.ResourceMessage("lblErrMsg"),SurveyMode.ToLower());
            lblHelpText.Text = String.Format(Utilities.ResourceMessage("lblHelpText"), SurveyMode.ToLower());
        }
    }

    
   
}