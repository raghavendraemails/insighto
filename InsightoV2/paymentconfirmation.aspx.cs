﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;

public partial class paymentconfirmation : System.Web.UI.Page
{
    string paymentid = "";
    string status = "";
    int Usrid = 0;
    int ord_id = 0;
    int tran_status = 0;
    string surveylicensetype = "";
    PollCreation pc;
    string urlstr = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        pc = new PollCreation();
        paymentid = (Request.QueryString["payment_id"] ?? "");
        status = (Request.QueryString["status"] ?? "").ToUpper();
        //if (status == "SUCCESS") {
        GetPaymentInformation();
        //}
    }

    protected void GetPaymentInformation()
    {
        DataTable dt = new DataTable();
        string productslug = "";
        try
        {
            dt = pc.getPollPaymentInformation(paymentid, "Survey");
            if (dt.Rows.Count > 0)
            {
                Int32.TryParse(dt.Rows[0]["USERID"].ToString(), out Usrid);
                Int32.TryParse(dt.Rows[0]["PK_ORDERID"].ToString(), out ord_id);
                status = dt.Rows[0]["status"].ToString().ToUpper();

                productslug = dt.Rows[0]["ProductSlug"].ToString().ToLower();
                if (productslug == "insighto-surveys-premium-annual")
                {
                    surveylicensetype = "PREMIUM-YEARLY";
                }
                else if (productslug == "insighto-surveys-pro-single")
                {
                    surveylicensetype = "PPS-PRO";
                }
                else if (productslug == "insighto-surveys-premium-single")
                {
                    surveylicensetype = "PPS-PREM";
                }

                if (status == "CREDIT")
                {
                    urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetUserInvoiceConfirmationURL(), "ord_id=" + ord_id.ToString() + "&Usrid=" + Usrid.ToString() + "&tran_status=1" + "&surveylicensetype=" + surveylicensetype);
                    Response.Redirect(urlstr);
                }
                else
                {
                    urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetUserDeclinedCardURL(), "order_id=" + ord_id.ToString() + "&Usrid=" + Usrid.ToString() + "&Am=0&pk_order=" + ord_id.ToString());
                    Response.Redirect(urlstr);
                }
            }
            else
            {
                urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetUserDeclinedCardURL(), "order_id=" + ord_id.ToString() + "&Usrid=" + Usrid.ToString() + "&Am=0&pk_order=" + ord_id.ToString());
                Response.Redirect(urlstr);
            }
        }
        catch (Exception e1)
        {
        }
    }
}