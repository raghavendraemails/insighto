<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UnderMaintainance.aspx.cs"
    Inherits="UnderMaintainance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>:: Insighto ::</title>
    <link href="App_Themes/Classic/maintenance_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">   
    <div id="wrapper">
        <!-- main pannel -->
        <div id="topLinksPanel">
            &nbsp;</div>
        <div id="header">
            <!-- header -->
            <div class="insightoLogoPanel">
                <!-- insighto logo -->
               <%-- <img src="App_Themes/Classic/Images/logo-insighto.jpg" width="184" height="60" alt="insighto"
                    title="insighto" />--%>
                     <img src="App_Themes/Classic/Images/Insighto_logo.png" width="184" height="60" alt="insighto"
                    title="insighto" />
                <!-- insighto logo -->
            </div>
            <div class="insightoTagLine">
            </div>
            <!-- //header -->
        </div>
        <div id="Div1">
            <!-- top main menu -->
        </div>
        <!-- top main menu -->

        <div>
            <div class="contentPanel">
                <h1 style="text-align: center">
                   <span style="font-weight: normal;">Site Maintenance In Progress.....</span></h1>
                <div style="padding: 20px 0 0 0; width: 580px; margin: 0 auto 20px auto;">
                    <p>
                        This is to inform you that Insighto.com's servers are going through scheduled maintenance and the site will be down for a maximum of 3 hours on March 4,2015.<br>
<br/>
Maintenance window: 
<br><br>
IST:    From 3:30 AM March 4,2015 To 6:30 AM March 4,2014
<br><br>
PST: From 5:00 PM March 4 ,2015 To 8:00 PM March 4,2015
<br><br>
GMT: From 10:00 PM March 3,2015 To 01:00 AM March 4,2014
<br><br>
During this interval, the Insighto website, survey creation, deployment, reporting and survey taking may be affected.<br>
<br/>     
We sincerely regret the inconvenience. Thanks for your understanding. 
<br/><br/>
Regards,
<br/>
Insighto Team<br/>
                     </p>
                </div>
                <%--<div style="border: solid 1px #ccc; padding: 10px 3px 0 10px; width: 570px; margin: 0 auto 15px auto;
                    background: #f5f5f5">
                    <b>Please leave your email address and we will notify you when we are back online.</b><br />
                    <br />
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="35" style="padding-right: 10px">
                                Email
                            </td>
                            <td style="padding-right: 3px;">
                                <asp:TextBox ID="txtEmailId" CssClass="textBoxMedium" runat="server" />
                            </td>
                            <td>
                                <asp:Button ID="btnSend" CssClass="dynamicButton" Text="Submit" runat="server" OnClick="btnSend_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:RegularExpressionValidator SetFocusOnError="true" ID="RexEmail" CssClass="lblRequired"
                                    ControlToValidate="txtEmailId" runat="server" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ErrorMessage="Invalid email address."></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                    <br />
                </div>--%>
                <div class="clear">
                </div>               
            </div>
        </div>
        <div id="Div2">
            <!-- footer -->
            <div class="copyRights">
                Copyright &copy; 2014 Knowience Insights
            </div>
             <div class="clear">
                </div>
            <!-- //footer -->
            <!-- //main pannel -->
        </div>
    </div>
    </form>
</body>
</html>
