﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using System.Configuration;

public partial class EditEmailId : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            txtEmailId.Text = Request.QueryString["email"];
        }
    }
    protected void ibtnSave_Click(object sender, ImageClickEventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            var userService = ServiceFactory.GetService<UsersService>();
            if (userService.IsEmailIdExists(txtEmailId.Text.Trim()))
            {
                lblErrMsg.Visible = true;
                lblSuccessMsg.Visible = false;

            }
            else
            {
                userService.UpdateAlternateEmail(Convert.ToInt32(Request.QueryString["id"].ToString()), txtEmailId.Text.Trim());
                string fromEmail = ConfigurationManager.AppSettings["emailActivation"];
                string subject = (String)GetGlobalResourceObject("CommonMessages", "emailSubjectAccountActivation");
                string ActivationURL = ConfigurationManager.AppSettings["RootVirtualURL"] + EncryptHelper.EncryptQuerystring("Home.aspx", "AlternateId=" + Request.QueryString["id"] + "&Email=" + txtEmailId.Text.Trim());
                string MsgBody = string.Format("URL:{0}<br/>UserName: {1}", ActivationURL, txtEmailId.Text.Trim());
                MailHelper.SendMailMessage(fromEmail, txtEmailId.Text.Trim(), "", "", subject, MsgBody);
                lblSuccessMsg.Visible = true;
                lblErrMsg.Visible = false;
            }
        }
    }
}