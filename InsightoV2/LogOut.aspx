﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="LogOut.aspx.cs" Inherits="LogOut"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3></h3>
        </div>
     <%--   <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(false,'');">
                &nbsp;</a>
        </div>--%>
        <!-- //popup heading -->
    </div>
  <div class="popupContentPanel">
    <div class="formPanel">
        <p class="defaultHeight"></p>
    <p class="defaultHeight"></p>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server">
        <div><%--<asp:Label ID="lblErrMsg" runat="server" Text="Your Session has been Expired" 
                meta:resourcekey="lblErrMsgResource1"></asp:Label>--%>Your session has timed out. Please click here to 
        <a href="#" onclick="closeModalSelf(true, 'Home.aspx');" id="lnkLogin"><b>Sign in</b></a> again</div>
    </div>
    </div>
  </div> 
    
  
    
</asp:Content>
