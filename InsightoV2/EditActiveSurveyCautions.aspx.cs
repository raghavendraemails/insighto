﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using App_Code;

public partial class EditActiveSurveyCautions : SurveyPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        dvEditLefPanel.InnerHtml = Utilities.ResourceMessage("divEditLeftpanel");
        dvEditRightPanel.InnerHtml = Utilities.ResourceMessage("divEditRightPanel");
        dvInformationPanelDefult.InnerHtml = Utilities.ResourceMessage("divInformationPanelDefult");   
    }

    protected void btnProceedBottom_Click(object sender, EventArgs e)
    {
        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetCreateSurveyURL(), "SurveyId=" + base.SurveyID + "&surveyFlag=" + base.SurveyFlag));
    }
    protected void btnCancelBottom_Click(object sender, EventArgs e)
    {
        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(),"surveyFlag="+base.SurveyFlag));
    }
    protected void btnProceedUp_Click(object sender, EventArgs e)
    {
        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetCreateSurveyURL(), "SurveyId=" + base.SurveyID + "&surveyFlag=" + base.SurveyFlag));
    }

    protected void btnCancelUp_Click(object sender, EventArgs e)
    {
        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + base.SurveyFlag));
    }
}