﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PreLaunch.master" AutoEventWireup="true"
    CodeFile="PreLaunchQuestionnaire.aspx.cs" Inherits="Insighto.Pages.PreLaunchQuestionnaire"%>

<%@ Import Namespace="Insighto.Data" %>
<%@ Import Namespace="Insighto.Business.ValueObjects" %>
<%@ Import Namespace="CovalenseUtilities.Extensions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PreLaunchMainContent" runat="Server">
    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccMsg" runat="server" meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
    </div>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
    </div>
    <div class="surveyQuestionTabPanel">
        <div class="surveyQuestionTabPanel">
            <div class="surveyQuestionHeader">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td>
                            <div class="surveyQuestionTitle">
                                <asp:Label ID="lblIntroTitle" runat="server" Text="Survey Introduction" meta:resourcekey="lblIntroTitleResource1"></asp:Label>
                            </div>
                        </td>
                        <td>
                            <div class="rightAlignselect">
                                <asp:HyperLink ID="hlnkEditIntro" runat="server" CssClass="icon_select" Text=" "
                                    ToolTip="Edit Introduction" meta:resourcekey="hlnkEditIntroResource1"></asp:HyperLink></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="surveyQuestionTabPanel">
                <div class="lt_padding" id="lblSurveyIntro" runat="server">
                    <%--<asp:Literal ID="lblSurveyIntro" runat="server" meta:resourcekey="lblSurveyIntroResource1"></asp:Literal>--%>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="successPanel" id="dvSuccessMsg1" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblSuccMsg1" runat="server" meta:resourcekey="lblSuccMsg1Resource1"></asp:Label></div>
            </div>
            <div class="errorMessagePanel" id="dvErrMsg1" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblErrMsg1" runat="server" meta:resourcekey="lblErrMsg1Resource1"></asp:Label></div>
            </div>
            <div class="surveyQuestionHeader">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <div class="surveyQuestionTitle">
                                <asp:Label ID="lblTitle" runat="server" Text="Survey Questions" meta:resourcekey="lblTitleResource1"></asp:Label>
                            </div>
                        </td>
                        <td class="questionAlign">
                            <asp:HyperLink ID="hlnkBTQuestionnaire" runat="server" Font-Underline="True" ToolTip="Quit Pre-Launch Check & Go Back to Questionnaire"
                                Text="Questions" meta:resourcekey="hlnkBTQuestionnaireResource1"></asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="surveyQuestionTabPanel customDynamicCss">
                <table width="100%">
                    <tr>
                        <td width="100%">
                            <asp:ListView ID="lstQuestionTypesItems" runat="server" OnItemDataBound="lstQuestionTypesItems_ItemDataBound">
                                <ItemTemplate>
                                    <div class="colapsPanel">
                                        <!-- tab main -->
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="questionGridRow">
                                                    <!-- tab header text-->
                                                    <div style="float: left; font-weight: bold">
                                                        <asp:Label ID="lblSequence" runat="server" meta:resourcekey="lblSequenceResource1"></asp:Label>
                                                        .
                                                        <%#((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUSETION_LABEL.StripTagsCharArray() %>
                                                        <%--<%# ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_SEQ %>--%>
                                                </td>
                                                <td class="questionGridRow">
                                                    <asp:HyperLink ID="hlnkEdit" runat="server" rel="framebox" h="480" w="850" scrolling="yes"
                                                        CssClass="icon_select" ToolTip="Edit Question" meta:resourcekey="hlnkEditResource1"></asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                        </span>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div class="GridBottomRow">
                                        <div class="GridBottomRowScroll">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-left: 20px;">
                                            <tr>
                                                <td>
                                                    <div id="divQuesAnswers" runat="server" class="form-field">
                                                    </div>
                                                    <div style="padding-left: 8px;">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblOther" runat="server" Text="Other" Visible="False" meta:resourcekey="lblOtherResource1"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtOther" runat="server" Visible="False" meta:resourcekey="txtOtherResource1"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                                    </div>
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                    </tr>
                </table>
                <div class="prelaunchbtn">
                    <asp:Button ID="btnBack" runat="server" Text="Back to Launch Page" ToolTip="Back to Launch Page"
                        CssClass="prelaunchbackbtn" OnClick="btnBack_Click" meta:resourcekey="btnBackResource1" /></div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <div id="divEditSkip" style="border: solid 0 #999; width: 400px; height: 120px; background-color: #fff;
        -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; display: none;">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td class="reminder_title" style="padding-left: 5px;">
                    <asp:Label ID="lblSkipLogic" runat="server" Text=" SKIP LOGIC" meta:resourcekey="lblSkipLogicResource1"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td align="left" style="padding-left: 5px;">
                    <asp:Label ID="lblHelpSkip" runat="server" Text="Edit is not possible because Skip Logic is applied for this question. Remove Skip Logic in Questionnaire page by clicking on 'Remove' link."
                        meta:resourcekey="lblHelpSkipResource1"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td align="right" style="padding-right: 30px;">
                    <table align="right">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <input id="btnOk" type="button" value="OK" title="OK" class="reminderbtn" onclick="Popup.hide('divEditSkip');" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <script src="../Scripts/popup.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript"> 
           $(document).ready(function () {
               var currentTime = new Date();
             $(".txtDate").datetimepicker({ dateFormat: 'mm/dd/yy', changeMonth:
    true, changeYear: true, showOn: "button", buttonImage: "App_Themes/Classic/Images/icon-calander.gif",
        buttonImageOnly: true, yearRange: '1904:2050', buttonText: "Calendar"

    });
            $('iframe').each(function () {
                var url = $(this).attr("src");
                var result = url.search(/youtube/i);
                if (result != -1) {
                    result = url.indexOf('?');
                    if (result != -1) {
                        $(this).attr("src", url + "&wmode=transparent");
                    } else { $(this).attr("src", url + "?wmode=transparent"); }
                }
            });
        });
        function ShowWarningForEdit() {
            Popup.showModal('divEditSkip', null, null, { 'screenColor': '#000000', 'screenOpacity': .6 });
            return false;
        }
    </script>
</asp:Content>
