﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
using System.Collections;
using System.IO;
using CovalenseUtilities.Services;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.IO;

namespace Insighto.Pages
{
    public partial class ChangePassword : SecurePageBase
    {
        LoggedInUserInfo loggedInUserInfo;
        int userId;
        string licenceType;
        int reset;
        private MailMessage mailMessage = null;
        private SmtpClient smtpMail = null;
        string strUrl = ConfigurationManager.AppSettings["RootURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            txtOldPassword.Focus();
            //if (!IsPostBack)
            //{
            dvResource.InnerHtml = Utilities.ResourceMessage("ChangePassword_Insturctions_Info");

            // lblOldPassword.Text = string.Format(Utilities.ResourceMessage("ChangePassword_Insturctions_Info"),"Temporary);

            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            var userFlag = ServiceFactory.GetService<UsersService>().GetUserDetails(userDetails.UserId).FirstOrDefault();
            userId = userDetails.UserId;
            licenceType = userDetails.LicenseType;
            reset = userFlag.RESET;
            var userlicensetype = (userDetails.LicenseType ?? "").ToUpper();//CHANGED TO FIX NULLREF ERROR WHEN LICENSE TYPE IS FREE
            if (userlicensetype == "FREE")
            {
                if (userFlag.RESET == 1)
                {
                    lblOldPassword.Text = string.Format(Utilities.ResourceMessage("lblOldPasswordResource1.Text"), "Temporary"); ;
                    reqOldPassword.ErrorMessage = string.Format(Utilities.ResourceMessage("lblReqOldPasswordResource1.Text"), "temporary password");
                    spnInfo.Visible = true;
                }
                else
                {
                    lblOldPassword.Text = string.Format(Utilities.ResourceMessage("lblOldPasswordResource1.Text"), "Old"); ;
                    reqOldPassword.ErrorMessage = string.Format(Utilities.ResourceMessage("lblReqOldPasswordResource1.Text"), "old password");
                    spnInfo.Visible = false;
                }
                btnCancel.Text = "Clear";
                btnCancel.ToolTip = "Clear";
            }
            else if (userFlag.RESET == 1) //IF SURVEY LICENSE TYPE IS NOT FREE (OR BLANK) BUT A RESET IS REQUIRED
            {
                lblOldPassword.Text = string.Format(Utilities.ResourceMessage("lblOldPasswordResource1.Text"), "Temporary"); ;
                reqOldPassword.ErrorMessage = string.Format(Utilities.ResourceMessage("lblReqOldPasswordResource1.Text"), "temporary password");
                spnInfo.Visible = true;
            }
            else if (userFlag.RESET == 0)
            {
                btnCancel.Text = "Clear";
                btnCancel.ToolTip = "Clear";
                reqOldPassword.ErrorMessage = string.Format(Utilities.ResourceMessage("lblReqOldPasswordResource1.Text"), "old password");
            }
            else
            {
                btnCancel.Text = "Skip, if you don't wish to";
                btnCancel.ToolTip = "Skip, if you don't wish to";
                reqOldPassword.ErrorMessage = string.Format(Utilities.ResourceMessage("lblReqOldPasswordResource1.Text"), "old password");
            }
            //}
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userInfo = sessionService.GetLoginUserDetailsSession();

                if (userInfo != null)
                {
                    var userService = ServiceFactory.GetService<UsersService>();
                    var userDetails = userService.GetUserDetails(userInfo.UserId);
                    string password = EncryptHelper.Decrypt(userDetails[0].PASSWORD);
                    if (password == txtOldPassword.Text)
                    {
                        string fromEmail = ConfigurationManager.AppSettings["emailActivation"];
                        var retValue = userService.UpdatePassword(userInfo.UserId, txtNewPassword.Text, 0);
                        var sendEmailService = ServiceFactory.GetService<SendEmailService>();
                      //  sendEmailService.ChangePassword("Change in Password", userInfo.FirstName, fromEmail, userInfo.LoginName);
                        string OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + userInfo.LoginName); 
                        string unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";
                        changepasswordemail("ChangedPassword", userInfo.FirstName, userInfo.LoginName, unsubscribe);

                        lblSuccessMsg.Text = Utilities.ResourceMessage("SuccessMsg");
                        dvSuccessMsg.Visible = true;
                        dvErrMsg.Visible = false;
                        Response.AddHeader("REFRESH", "5;URL=" + EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL().Replace("~/", ""), "surveyFlag=" + base.SurveyFlag));
                    }
                    else
                    {
                        dvSuccessMsg.Visible = false;
                        dvErrMsg.Visible = true;
                        lblErrMsg.Text = Utilities.ResourceMessage("ErrorMsg");
                        //Response.AddHeader("REFRESH", "5;URL=ChangePassword.aspx");

                    }
                }
                else
                {
                    Response.Redirect("Home.aspx");

                }
            }


        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (licenceType.ToUpper() == "FREE")
            {
                lblErrMsg.Text = string.Empty;
                txtOldPassword.Text = string.Empty;
                txtNewPassword.Text = string.Empty;
                txtConfirmPassword.Text = string.Empty;
            }
            else if (reset == 0)
            {
                lblErrMsg.Text = string.Empty;
                txtOldPassword.Text = string.Empty;
                txtNewPassword.Text = string.Empty;
                txtConfirmPassword.Text = string.Empty;
            }
            else
            {
                ServiceFactory.GetService<UsersService>().UpdateChangePasswordFlag(userId);
                Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + base.SurveyFlag));
            }

        }

        public void changepasswordemail(string templatename, string username, string EmailID,  string unsubscribeurl)
        {

            string viewData1;
            JavaScriptSerializer js1 = new JavaScriptSerializer();
            js1.MaxJsonLength = int.MaxValue;
            clsjsonmandrill prmjson1 = new clsjsonmandrill();
            //  var prmjson1 = new jsonmandrill();
            prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
            prmjson1.name = templatename;
            viewData1 = js1.Serialize(prmjson1);

            string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";


            WebClient request1 = new WebClient();
            request1.Encoding = System.Text.Encoding.UTF8;
            request1.Headers["Content-Type"] = "application/json";
            byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));

            string response1 = System.Text.Encoding.ASCII.GetString(resp1);


            clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);

            string bodycontent = jsmandrill11.code;


            string viewData;
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = int.MaxValue;
            var prmjson = new clsjsonmandrill.jsonmandrillmerge();
            prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
            prmjson.template_name = templatename;



            List<clsjsonmandrill.template_content> mtags = new List<clsjsonmandrill.template_content>();
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content00", content = "<div> Dear *|FNAME|*,</div>" });            
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content02", content = "<div><p style='font-size:8pt;font-family:Arial,Sans-Serif'> *|UNSUBSCRIBEINSIGHTO|* </p></div>" });

            List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
            mvars.Add(new clsjsonmandrill.merge_vars { name = "FNAME", content = username });      
            mvars.Add(new clsjsonmandrill.merge_vars { name = "UNSUBSCRIBEINSIGHTO", content = unsubscribeurl });
            prmjson.template_content = mtags;
            prmjson.merge_vars = mvars;


            viewData = js.Serialize(prmjson);


            string url = "https://mandrillapp.com/api/1.0/templates/render.json";


            WebClient request = new WebClient();
            request.Encoding = System.Text.Encoding.UTF8;
            request.Headers["Content-Type"] = "application/json";
            byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

            string response = System.Text.Encoding.ASCII.GetString(resp);

            string unesc = System.Text.RegularExpressions.Regex.Unescape(response);

            string first4 = unesc.Substring(0, 9);

            string last2 = unesc.Substring(unesc.Length - 2, 2);


            string unesc1 = unesc.Replace(first4, "");
            string unescf = unesc1.Replace(last2, "");


            clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);

            //   string bodycontent = jsmandrill1.code;


            mailMessage = new MailMessage();
            mailMessage.To.Clear();
            mailMessage.Sender = new MailAddress("alerts@insighto.com", "Insighto.com");
            // mailMessage.Sender = new MailAddress(Sendmails.FROM_MAILADDRESS, "Insighto.com");
            mailMessage.From = new MailAddress("alerts@insighto.com", "Insighto.com");

            mailMessage.Subject = "[Important –  From Insighto.com] Your Password has been changed";

            mailMessage.To.Add(EmailID);
            //  mailMessage.Headers.Add("survey_id", "6666");
            // mailMessage.Headers.Add("X-MC-Tags", "survey_respondent");
            mailMessage.Body = unescf;
            //if (Sendmails.SENDER_TOCCADDRESSES != null && Convert.ToString(Sendmails.SENDER_TOCCADDRESSES.Trim()).Length > 0)
            //{
            //    mailMessage.CC.Add(Sendmails.SENDER_TOCCADDRESSES.Trim());
            //}
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.Normal;
            smtpMail = new SmtpClient();
            smtpMail.Host = "smtp.mandrillapp.com";
            smtpMail.Port = 587;
            smtpMail.EnableSsl = true;
            smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpMail.Credentials = new System.Net.NetworkCredential("ram@knowience.com", "T74TJc3EZPYaIcvdBg--Dg");

            try
            {

                smtpMail.Send(mailMessage);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        //  Console.WriteLine("Delivery failed - retrying in 5 seconds.");


                        System.Threading.Thread.Sleep(5000);
                        smtpMail.Send(mailMessage);
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            //catch (System.Net.Mail.SmtpException ex)
            //{
            //    throw ex;
            //}
            mailMessage.Dispose();
        }

        protected void btnEmailCancel_Click(object sender, EventArgs e)
        {
            //txtCurrentEmailId.Text = string.Empty;
            //txtNewEmailAddress.Text = string.Empty;
            //txtConfirmNewEmailAddress.Text = string.Empty;

        }

        protected void btnEmailSave_Click(object sender, EventArgs e)
        {
            ////if (Page.IsValid)
            ////{
            ////    var user = ServiceFactory<UsersService>.Instance.GetUserByLoginName(txtNewEmailAddress.Text.Trim());
            ////    if (user == null)
            ////    {
            ////        var sessionService = ServiceFactory.GetService<SessionStateService>();
            ////        var userInfo = sessionService.GetLoginUserDetailsSession();
            ////        if (userInfo != null)
            ////        {
            ////            var userService = ServiceFactory.GetService<UsersService>();
            ////            var retValue = userService.UpdateEmailId(userInfo.UserId, txtNewEmailAddress.Text);
            ////            lblSuccessMsg.Text = Utilities.ResourceMessage("SuccessMsgEmail");
            ////            dvSuccessMsg.Visible = true;
            ////            dvErrMsg.Visible = false;
            ////            Response.AddHeader("REFRESH", "5;URL=" + EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL().Replace("~/", ""), "surveyFlag=" + base.SurveyFlag));
            ////        }
            ////    }
            ////    else
            ////    {
            ////        dvSuccessMsg.Visible = false;
            ////        dvErrMsg.Visible = true;
            ////        lblErrMsg.Text = Utilities.ResourceMessage("ErrorMsgEmail");

            ////    }
            ////}
            ////else
            ////{
            ////    Response.Redirect("Home.aspx");

            ////}
        }
    }

}