﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="errorpage.aspx.cs" Inherits="errorpage" MasterPageFile="~/Home.Master"  %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/UserControls/HomeHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc:Header ID="Header" runat="server" />
    <div class="container content" style="text-align: center;background: #fff175;width: 100%;height: 85vh;">
        <span class="glyphicon glyphicon-exclamation-sign" style="font-size: 140px;color: #FF5722;padding: 5% 0;"></span>
        <p style="color: #000;font-size: 16px;">
            <b style="font-size: 45px;">
                OOPS!!
                <br/> 
                <b>
                    <small style="font-size: 18px;">
                        <span id="errormsg" runat="server">
                            Something is not right.  Very sorry.  Support team is being notified.
                        </span>
                        <br/> 
                        You may please <a href="Login.aspx">Log In</a> again and continue. <br/> If the error persists, please contact support@insighto.com
                    </small>
                </b>
            </p>
        </div>
</asp:Content>
