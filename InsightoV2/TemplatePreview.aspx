﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TemplatePreview.aspx.cs"
    MasterPageFile="~/SurveyRespondent.master" Inherits="Insighto.Pages.TemplatePreview" %>

<%@ Reference VirtualPath="~/UserControls/RespondentControls/MOSSControl.ascx" %>
<%@ Reference VirtualPath="~/UserControls/RespondentControls/MOMSControl.ascx" %>
<%@ Reference VirtualPath="~/UserControls/RespondentControls/MenuDropDownControl.ascx" %>
<%@ Reference VirtualPath="~/UserControls/RespondentControls/MatrixSideBySideControl.ascx" %>
<asp:Content ContentPlaceHolderID="ContentHead" runat="server" ID="headerPlaceHolder">
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentMain" runat="server" ID="Content1">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="chooseButtonPanel">
        <p class="center">
            <asp:Label runat="server" ID="lblTitle" CssClass="" Text="" /></p>
    </div>
    <div class="divQuestions qOptionPanel">
        <asp:PlaceHolder ID="plhQuestions" runat="server" ClientIDMode="Static" />
    </div>
</asp:Content>
