//function trim(s)
//{
//	var l=0; var r=s.length -1;
//	while(l < s.length && s[l] == ' ')
//	{	l++; }
//	while(r > l && s[r] == ' ')
//	{	r-=1;	}
//	return s.substring(l, r+1);
//}


function Trim(str)
{
    while (str.substring(0,1) == ' ') // check for white spaces from beginning
    {
        str = str.substring(1, str.length);
    }
    while (str.substring(str.length-1, str.length) == ' ') // check white space from end
    {
        str = str.substring(0,str.length-1);
    }
   
    return str;
}

// JavaScript Document

// Start of Registration check

//email verify start
function echeck(str) {
    var at="@";
    var dot=".";
    var lat=str.indexOf(at);
    var lstr=str.length;
    var ldot=str.indexOf(dot);
    if (str.indexOf(at)==-1){
    //alert("Invalid E-mail ID");
    return(false);
}

if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
//alert("Invalid E-mail ID");
return(false);
}

if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
//alert("Invalid E-mail ID");
return(false);
}

if (str.indexOf(at,(lat+1))!=-1){
//alert("Invalid E-mail ID");
return(false);
}

if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
//alert("Invalid E-mail ID");
return(false);
}

if (str.indexOf(dot,(lat+2))==-1){
//alert("Invalid E-mail ID");
return(false);
}
		
if (str.indexOf(" ")!=-1){
//alert("Invalid E-mail ID");
return(false);
}
return true					
}
// email verify end
//phone no validation for numeric
function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}
function iszeroInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if(c!="0")
        {
          return true;
        }
    }
    // All characters are numbers.
    return false;
}
function isInteger1(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) 
        {
        if(c!="+")
        return false;
        }
    }
    // All characters are numbers.
    return true;
}
//end validation
function check()
{

var test=true;
var test1=true;
var test2=true
var test3=true;
var test4=true;
var test5=true;
var test6=true;
var test7=true;
var test8=true;
// Start of Hide all error divs Validation
document.getElementById("errorfname").style.display="none";
document.getElementById("errorlname").style.display="none";
document.getElementById("erroradd1").style.display="none";
document.getElementById("errorcountry").style.display="none";
document.getElementById("errorcity").style.display="none";
document.getElementById("errorphone").style.display="none";
document.getElementById("erroremail").style.display="none";
document.getElementById("errorpass").style.display="none";
document.getElementById("errorcpass").style.display="none";
// End of Hide all error divs Validation

// Start of First name Validation
if(document.form1.txtFname.value=="")
{
document.getElementById("errorfname").style.display="block";
document.getElementById("errorfname").innerHTML="Enter First Name";
document.form1.txtFname.focus();
//return(false);
test=false;
test1=false
}

if((document.form1.txtFname.value.length < 3)|| (document.form1.txtFname.value.length > 50))
{
    document.getElementById("errorfname").style.display="block";
if(test1==true)
document.getElementById("errorfname").innerHTML=" Length must be in between 4 and 50 ";
if(test==true)
document.form1.txtFname.focus();
test=false;
//return(false);
}
// End of First name Validation

// Start of Last name Validation
if(document.form1.txtLname.value=="")
{
document.getElementById("errorlname").style.display="block";
document.getElementById("errorlname").innerHTML="Enter Last Name";
if(test==true)
document.form1.txtLname.focus();
//return(false);
test=false;
test2=false;
}

if((document.form1.txtLname.value.length < 1)|| (document.form1.txtLname.value.length > 50))
{
document.getElementById("errorlname").style.display="block";
if(test2==true)
document.getElementById("errorlname").innerHTML=" Length must be in between 1 and 50 ";
if(test==true)
document.form1.txtLname.focus();

//return(false);
test=false;
}
// End of Last name Validation

// Start of Address1 Validation
if(document.form1.txtAdd1.value=="")
{
document.getElementById("erroradd1").style.display="block";
document.getElementById("erroradd1").innerHTML="Enter Address1";
if(test==true)
document.form1.txtAdd1.focus();
test=false;
//return(false);
}
// End of Address1 Validation

// Start of City Validation
if ( document.form1.txtCity.value=="")
{
document.getElementById("errorcity").style.display="block";
document.getElementById("errorcity").innerHTML="Enter City Name";
if(test==true)
document.form1.txtCity.focus();
test=false;
//return(false);
}
// End of City Validation

// Start of Country Validation
//if ( document.form1.cboCountry.selectedIndex == 0 )
if ( document.form1.cboCountry.selectedIndex <1 )
{
document.getElementById("errorcountry").style.display="block";
document.getElementById("errorcountry").innerHTML="Select Country Name";
if(test==true)
document.form1.cboCountry.focus();

test=false;
//return(false);
}
// End of Country Validation

// Start of Phone No Validation
var stripped = document.form1.txtPhone.value.replace(/[\(\)\.\-\ ]/g, '');
//txtcoupho
if (document.form1.txtcoupho.value == "")
{
document.getElementById("errorphone").style.display="block";
document.getElementById("errorphone").innerHTML="Enter Country Code";
if(test==true)
document.form1.txtcoupho.focus();
test=false;
}
else if(isInteger1(document.form1.txtcoupho.value)==false)
{
document.getElementById("errorphone").style.display="block";
document.getElementById("errorphone").innerHTML="Invalid Format";
if(test==true)
document.form1.txtcoupho.focus();
test=false;
}

if (document.form1.txtregpho.value == "")
{
document.getElementById("errorphone").style.display="block";
document.getElementById("errorphone").innerHTML="Enter Region Code";
if(test==true)
document.form1.txtregpho.focus();
test=false;
}
else if(isInteger(document.form1.txtregpho.value)==false)
{
document.getElementById("errorphone").style.display="block";
document.getElementById("errorphone").innerHTML="Invalid Format";
if(test==true)
document.form1.txtregpho.focus();
test=false;  
}


   
if (document.form1.txtPhone.value == "") {
document.getElementById("errorphone").style.display="block";
document.getElementById("errorphone").innerHTML="Enter Phone No";
if(test==true)
document.form1.txtPhone.focus();
test=false;
test6=false;
//return(false);
    } else if (isNaN(parseInt(stripped))) {
document.getElementById("errorphone").style.display="block";
if(test6==true)
document.getElementById("errorphone").innerHTML="InValid Format";
if(test==true)
document.form1.txtPhone.focus();
test6=false;
test=false;
//return(false);
    }
    else if(!iszeroInteger(stripped)) {
document.getElementById("errorphone").style.display="block";
if(test6==true)
document.getElementById("errorphone").innerHTML="InValid Format";
if(test==true)
document.form1.txtPhone.focus();
test6=false;
test=false;
//return(false);
    }
    
//// else if (!(stripped.length == 10)) {
////document.getElementById("errorphone").style.display="block";
////if(test6==true)
////document.getElementById("errorphone").innerHTML="Phone No must be at least 10 dizits";
////if(test==true)
////document.form1.txtPhone.focus();
////test=false;
                                    //return(false);
//}
// End of Phone No Validation

// Start of Email Validation
if ((document.form1.txtEmail.value==null)||(document.form1.txtEmail.value=="")){
document.getElementById("erroremail").style.display="block";
document.getElementById("erroremail").innerHTML="Please Enter your Email ID";
if(test==true)
document.form1.txtEmail.focus();
test=false;
test7=false;
//return(false);
}
		
if (echeck(document.form1.txtEmail.value)==false){
document.getElementById("erroremail").style.display="block";
if(test7==true)
document.getElementById("erroremail").innerHTML="Invalid Email ID";
if(test==true)
document.form1.txtEmail.focus();
test=false;
//return(false);
}
// End of Email Validation

// Password Strength validation starts
var control = document.form1.txtPassword;
var myString= control.value;
var Stringlen = myString.length;
var ValidateDigits = /[^0-9]/g;
var ValidateSpChar = /[a-zA-Z0-9]/g;
var ValidateChar = /[^a-zA-Z]/g;

var digitString = myString.replace(ValidateDigits , "");
var specialString = myString.replace(ValidateSpChar, "");
var charString = myString.replace(ValidateChar, "");

if((Stringlen < 6) || (Stringlen > 16))
{
document.getElementById("errorpass").style.display="block";
document.getElementById("errorpass").innerHTML="Minimum of 6 and Maximum of 16 characters including special character or number.";
if(test==true)
control.focus();
test=false;
test8=false
//return(false);
}

if (specialString < 1 && digitString.length<1) {

//if (digitString < 1) {

document.getElementById("errorpass").style.display="block";
if(test8==true)
document.getElementById("errorpass").innerHTML="Minimum of 6 and Maximum of 16 characters including special character or number.";
if(test==true)
control.focus();
test=false;
//return(false);
//}
}


if(document.form1.txtPassword.value != document.form1.txtConfirmpwd.value) {
document.getElementById("errorcpass").style.display="block";
document.getElementById("errorcpass").innerHTML="Passwords does not match";
if(test==true)
document.form1.txtConfirmpwd.focus();
test=false;
//return false;
}
// Password Strength Validation Ends
return test;			
}
	
// End of Registration check













function validate()
{

var test=true;
var test1=true;
var test2=true
var test3=true;
var test4=true;
var test5=true;

// Start of Hide all error divs Validation
document.getElementById("errorfname").style.display="none";
document.getElementById("errorphone").style.display="none";
document.getElementById("erroremail").style.display="none";
document.getElementById("errorpass").style.display="none";
document.getElementById("errorcpass").style.display="none";
// End of Hide all error divs Validation


// Start of First name Validation

if(Trim(document.form1.txtFname.value)=="")
{
document.getElementById("errorfname").style.display="block";
document.getElementById("errorfname").innerHTML="Enter Name";
document.form1.txtFname.focus();
//return(false);
test=false;
test5=false
}

if((document.form1.txtFname.value.length < 3)|| (document.form1.txtFname.value.length > 50))
{
    document.getElementById("errorfname").style.display="block";
if(test5==true)
document.getElementById("errorfname").innerHTML=" Length must be in between 3 and 50 ";
if(test==true)
document.form1.txtFname.focus();
test=false;
//return(false);
}
// End of First name Validation

// Start of Email Validation
if ((document.form1.txtEmail.value==null)||(Trim(document.form1.txtEmail.value)=="")){
document.getElementById("erroremail").style.display="block";
document.getElementById("erroremail").innerHTML="Please Enter your Email ID";
document.form1.txtEmail.focus();
test=false;
test1=false;
//return(false);
}
		
if (echeck(document.form1.txtEmail.value)==false){
document.getElementById("erroremail").style.display="block";
if(test1==true)
document.getElementById("erroremail").innerHTML="Invalid Email ID";
if(test1==true)
document.form1.txtEmail.focus();
test=false;
//return(false);
}
// End of Email Validation




// Password Strength validation starts
var control = document.form1.txtPassword;
var myString= control.value;
var Stringlen = myString.length;
var ValidateDigits = /[^0-9]/g;
var ValidateSpChar = /[a-zA-Z0-9]/g;
var ValidateChar = /[^a-zA-Z]/g;

var digitString = myString.replace(ValidateDigits , "");
var specialString = myString.replace(ValidateSpChar, "");
var charString = myString.replace(ValidateChar, "");

if((Stringlen < 6) || (Stringlen > 16))
{
document.getElementById("errorpass").style.display="block";
document.getElementById("errorpass").innerHTML="Invalid Password";
if(test==true)
control.focus();
test=false;
test2=false
//return(false);
}

if (specialString < 1 && digitString.length<1) {

//if (digitString < 1) {

document.getElementById("errorpass").style.display="block";
if(test2==true)
document.getElementById("errorpass").innerHTML="Invalid Password";
if(test==true)
control.focus();
test=false;
//return(false);
//}
}


if(document.form1.txtPassword.value != document.form1.txtConfirmpwd.value) {
document.getElementById("errorcpass").style.display="block";
document.getElementById("errorcpass").innerHTML="Passwords does not match";
if(test==true)
document.form1.txtConfirmpwd.focus();
test=false;
//return false;
}
// Password Strength Validation Ends



// Start of Phone No Validation
var stripped = document.form1.txtPhone.value.replace(/[\(\)\.\-\ ]/g, '');
//txtcoupho
//if (document.form1.txtcoupho.value == "")
//{
//document.getElementById("errorphone").style.display="block";
//document.getElementById("errorphone").innerHTML="Enter Country Code";
//if(test==true)
//document.form1.txtcoupho.focus();
//test=false;
//}
//else if(isInteger1(document.form1.txtcoupho.value)==false)
 if(isInteger1(document.form1.txtcoupho.value)==false)
{
document.getElementById("errorphone").style.display="block";
document.getElementById("errorphone").innerHTML="Invalid Format";
if(test==true)
document.form1.txtcoupho.focus();
test=false;
}

//if (Trim(document.form1.txtregpho.value)== "")
//{
//document.getElementById("errorphone").style.display="block";
//document.getElementById("errorphone").innerHTML="Enter Region Code";
//if(test==true)
//document.form1.txtregpho.focus();
//test=false;
//}
//else if(isInteger(document.form1.txtregpho.value)==false)
if(isInteger(document.form1.txtregpho.value)==false)
{
document.getElementById("errorphone").style.display="block";
document.getElementById("errorphone").innerHTML="Invalid Format";
if(test==true)
document.form1.txtregpho.focus();
test=false;  
}


   
if (Trim(document.form1.txtPhone.value) == "") {
document.getElementById("errorphone").style.display="block";
document.getElementById("errorphone").innerHTML="Enter Phone No";
if(test==true)
document.form1.txtPhone.focus();
test=false;
test3=false;
//return(false);
    }
    // else if (isNaN(parseInt(stripped))) {
    if(isInteger(document.form1.txtPhone.value)==false){
document.getElementById("errorphone").style.display="block";
if(test3==true)
document.getElementById("errorphone").innerHTML="InValid Format";
if(test==true)
document.form1.txtPhone.focus();
test3=false;
test=false;

    }
else if(!iszeroInteger(document.form1.txtPhone.value)) {
document.getElementById("errorphone").style.display="block";
if(test3==true)
document.getElementById("errorphone").innerHTML="InValid Format";
if(test==true)
document.form1.txtPhone.focus();
test3=false;
test=false;
//return(false);
    }
// End of Phone No Validation



return test;			
}




function loginvalidate_Regpage()
{

var test=true;
var test1=true;
var test2=true

// Start of Hide all error divs Validation

document.getElementById("erroremail1").style.display="none";
document.getElementById("errorpass1").style.display="none";

// End of Hide all error divs Validation

// Start of Email Validation
if ((document.form1.txt_login_emailid.value==null)||(document.form1.txt_login_emailid.value=="")){
document.getElementById("erroremail1").style.display="block";
document.getElementById("erroremail1").innerHTML="Please Enter your Email ID";
document.form1.txt_login_emailid.focus();
test=false;
test1=false;
//return(false);
}
		
if (echeck(document.form1.txt_login_emailid.value)==false){
document.getElementById("erroremail1").style.display="block";
if(test1==true)
document.getElementById("erroremail1").innerHTML="Invalid Email ID";
if(test1==true)
document.form1.txt_login_emailid.focus();
test=false;
//return(false);
}
// End of Email Validation




// Password Strength validation starts
var control = document.form1.txt_login_password;
var myString= control.value;
var Stringlen = myString.length;
var ValidateDigits = /[^0-9]/g;
var ValidateSpChar = /[a-zA-Z0-9]/g;
var ValidateChar = /[^a-zA-Z]/g;

var digitString = myString.replace(ValidateDigits , "");
var specialString = myString.replace(ValidateSpChar, "");
var charString = myString.replace(ValidateChar, "");

if((Stringlen < 6) || (Stringlen > 16))
{
document.getElementById("errorpass1").style.display="block";
document.getElementById("errorpass1").innerHTML="Invalid Password";
if(test==true)
control.focus();
test=false;
test2=false
//return(false);
}

if (specialString < 1 && digitString.lenght<1) {

//if (digitString < 1) {

document.getElementById("errorpass1").style.display="block";
if(test2==true)
document.getElementById("errorpass1").innerHTML="Invalid Password";
if(test==true)
control.focus();
test=false;
//return(false);
//}
}



// Password Strength Validation Ends





return test;			
}




function loginvalidate()
{

var test=true;
var test1=true;
var test2=true


// Start of Hide all error divs Validation

document.getElementById("erroremail1").style.display="none";
document.getElementById("errorpass1").style.display="none";

// End of Hide all error divs Validation

// Start of Email Validation
if ((document.form1.txt_login_emailid.value==null)||(document.form1.txt_login_emailid.value=="")){
document.getElementById("erroremail1").style.display="block";
document.getElementById("erroremail1").innerHTML="Please Enter your Email ID";
document.form1.txt_login_emailid.focus();
test=false;
test1=false;
//return(false);
}
		
if (echeck(document.form1.txt_login_emailid.value)==false){
document.getElementById("erroremail1").style.display="block";
if(test1==true)
document.getElementById("erroremail1").innerHTML="Invalid Email ID";
if(test1==true)
document.form1.txt_login_emailid.focus();
test=false;
//return(false);
}
// End of Email Validation




// Password Strength validation starts
var control = document.form1.txt_login_password;
var myString= control.value;
var Stringlen = myString.length;
var ValidateDigits = /[^0-9]/g;
var ValidateSpChar = /[a-zA-Z0-9]/g;
var ValidateChar = /[^a-zA-Z]/g;

var digitString = myString.replace(ValidateDigits , "");
var specialString = myString.replace(ValidateSpChar, "");
var charString = myString.replace(ValidateChar, "");

if((Stringlen < 6) || (Stringlen > 16))
{
document.getElementById("errorpass1").style.display="block";
document.getElementById("errorpass1").innerHTML="Invalid Password";
if(test==true)
control.focus();
test=false;
test2=false
//return(false);
}

if (specialString < 1 && digitString.lenght<1) {

//if (digitString < 1) {

document.getElementById("errorpass1").style.display="block";
if(test2==true)
document.getElementById("errorpass1").innerHTML="Invalid Password";
if(test==true)
control.focus();
test=false;
//return(false);
//}
}



// Password Strength Validation Ends





return test;			
}






function paymentsubscribevalidate()
{

var test=true;
var test1=true;
var test2=true
var test3=true;
var test4=true;
var test5=true;
var test6=true;
var test7=true
var test8=true;
var test9=true;
var test10=true;
var test11=true;

// Start of Hide all error divs Validation
document.getElementById("errorfname").style.display="none";
document.getElementById("errorlname").style.display="none";
document.getElementById("erroremail").style.display="none";
document.getElementById("errorpass").style.display="none";
document.getElementById("errorcpass").style.display="none";
document.getElementById("errorcompany").style.display="none";
document.getElementById("errordesignation").style.display="none";
document.getElementById("errorphone").style.display="none";
document.getElementById("errorcity").style.display="none";
document.getElementById("errorcountry").style.display="none";
document.getElementById("errorpaymenttype").style.display="none";

// End of Hide all error divs Validation


// Start of First name Validation

if(Trim(document.form1.txtFname.value)=="")
{
document.getElementById("errorfname").style.display="block";
document.getElementById("errorfname").innerHTML="Enter First Name";
document.form1.txtFname.focus();
//return(false);
test=false;
test1=false
}

if((Trim(document.form1.txtFname.value).length < 3)|| (Trim(document.form1.txtFname.value).length > 50))
{
    document.getElementById("errorfname").style.display="block";
if(test1==true)
document.getElementById("errorfname").innerHTML=" Length must be in between 3 and 50 ";
if(test==true)
document.form1.txtFname.focus();
test=false;
//return(false);
}
// End of First name Validation





// Start of Last name Validation
if(Trim(document.form1.txtLname.value)=="")
{
document.getElementById("errorlname").style.display="block";
document.getElementById("errorlname").innerHTML="Enter Last Name";
if(test==true)
document.form1.txtLname.focus();
//return(false);
test=false;
test2=false;
}

if((Trim(document.form1.txtLname.value).length < 1)|| (Trim(document.form1.txtLname.value).length > 50))
{
document.getElementById("errorlname").style.display="block";
if(test2==true)
document.getElementById("errorlname").innerHTML=" Length must be in between 1 and 50 ";
if(test==true)
document.form1.txtLname.focus();

//return(false);
test=false;
}
// End of Last name Validation

// Start of Email Validation
if ((Trim(document.form1.txtEmail.value)==null)||(Trim(document.form1.txtEmail.value)=="")){
document.getElementById("erroremail").style.display="block";
document.getElementById("erroremail").innerHTML="Please Enter your Email ID";
if(test==true)
document.form1.txtEmail.focus();
test=false;
test3=false;
//return(false);
}
		
if (echeck(document.form1.txtEmail.value)==false){
document.getElementById("erroremail").style.display="block";
if(test3==true)
document.getElementById("erroremail").innerHTML="Invalid Email ID";
if(test==true)
document.form1.txtEmail.focus();
test=false;
test3=false;
//return(false);
}
// End of Email Validation




// Password Strength validation starts
var control = document.form1.txtPassword;
var myString= Trim(control.value);
var Stringlen = myString.length;
var ValidateDigits = /[^0-9]/g;
var ValidateSpChar = /[a-zA-Z0-9]/g;
var ValidateChar = /[^a-zA-Z]/g;

var digitString = myString.replace(ValidateDigits , "");
var specialString = myString.replace(ValidateSpChar, "");
var charString = myString.replace(ValidateChar, "");
if(Stringlen==0)
{
document.getElementById("errorpass").style.display="block";
document.getElementById("errorpass").innerHTML="Enter Password";
if(test==true)
control.focus();
test=false;
test4=false

}
else if((Stringlen < 6) || (Stringlen > 16))
{
document.getElementById("errorpass").style.display="block";
document.getElementById("errorpass").innerHTML="Invalid Password";
if(test==true)
control.focus();
test=false;
test4=false
//return(false);
}

if (specialString < 1 && digitString.length<1) {

//if (digitString < 1) {

document.getElementById("errorpass").style.display="block";
if(test4==true)
document.getElementById("errorpass").innerHTML="Invalid Password";
if(test==true)
control.focus();
test=false;
test4=false;
//return(false);
//}
}


if(document.form1.txtPassword.value != document.form1.txtConfirmpwd.value) {
document.getElementById("errorcpass").style.display="block";
document.getElementById("errorcpass").innerHTML="Passwords does not match";
if(test==true)
document.form1.txtConfirmpwd.focus();
test=false;
test5=false;
//return false;
}
// Password Strength Validation Ends


if (document.form1.rbnorganization.checked)
{
// Start of company name Validation

            if(Trim(document.form1.txtCompany.value)=="")
            {
            document.getElementById("errorcompany").style.display="block";
            document.getElementById("errorcompany").innerHTML="Enter Company Name";
            if(test==true)
            document.form1.txtCompany.focus();
            //return(false);
            test=false;
            test6=false
            }

            if((Trim(document.form1.txtCompany.value).length < 1)|| (Trim(document.form1.txtCompany.value).length > 100))
            {
                document.getElementById("errorcompany").style.display="block";
            if(test6==true)
            document.getElementById("errorcompany").innerHTML=" Length must be in between 1 and 100 chars ";
            if(test==true)
            document.form1.txtCompany.focus();
            test=false;
            test6=false;
            //return(false);
            }
// End of company name Validation

// Start of Designation validation 

            if(Trim(document.form1.txtdesignation.value)=="")
            {
            document.getElementById("errordesignation").style.display="block";
            document.getElementById("errordesignation").innerHTML="Enter Designation";
            if(test==true)
            document.form1.txtdesignation.focus();
            //return(false);
            test=false;
            test7=false
            }

            if((Trim(document.form1.txtdesignation.value).length < 3)|| (Trim(document.form1.txtdesignation.value).length > 100))
            {
                document.getElementById("errordesignation").style.display="block";
            if(test6==true)
            document.getElementById("errordesignation").innerHTML=" Length must be in between 1 and 100 ";
            if(test==true)
            document.form1.txtdesignation.focus();
            test=false;
            test7=false;
            //return(false);
            }
}
// End of Designation Validation


// Start of Phone No Validation
var stripped = document.form1.txtPhone.value.replace(/[\(\)\.\-\ ]/g, '');
//txtcoupho
//////if (document.form1.txtcoupho.value == "")
//////{
//////document.getElementById("errorphone").style.display="block";
//////document.getElementById("errorphone").innerHTML="Enter Country Code";
//////if(test==true)
//////document.form1.txtcoupho.focus();
//////test=false;
//////test8=false;
//////}
///else if(isInteger1(document.form1.txtcoupho.value)==false)
if(isInteger1(document.form1.txtcoupho.value)==false)
{
document.getElementById("errorphone").style.display="block";
if(test8==true)
document.getElementById("errorphone").innerHTML="Invalid Format";
if(test==true)
document.form1.txtcoupho.focus();
test=false;
}

//if (Trim(document.form1.txtregpho.value)== "")
//{
//document.getElementById("errorphone").style.display="block";
//document.getElementById("errorphone").innerHTML="Enter Region Code";
//if(test==true)
//document.form1.txtregpho.focus();
//test=false;
//test9=false;
//}
//else if(isInteger(document.form1.txtregpho.value)==false)
if(isInteger1(document.form1.txtregpho.value)==false)
{
document.getElementById("errorphone").style.display="block";
if(test9==true)
document.getElementById("errorphone").innerHTML="Invalid Format";
if(test==true)
document.form1.txtregpho.focus();
test=false;
test9=false;  
}


   
if (Trim(document.form1.txtPhone.value) == "") {
document.getElementById("errorphone").style.display="block";
document.getElementById("errorphone").innerHTML="Enter Phone No";
if(test==true)
document.form1.txtPhone.focus();
test=false;
test10=false;
//return(false);
    }
     //else if (isNaN(parseInt(stripped))) {
     else if(isInteger(document.form1.txtPhone.value)==false){
document.getElementById("errorphone").style.display="block";
if(test10==true)
document.getElementById("errorphone").innerHTML="InValid Format";
if(test==true)
document.form1.txtPhone.focus();
test10=false;
test=false;

    }

// End of Phone No Validation


// Start of City Validation
if (Trim(document.form1.txtCity.value)=="")
{
document.getElementById("errorcity").style.display="block";
document.getElementById("errorcity").innerHTML="Enter City Name";
if(test==true)
document.form1.txtCity.focus();
test=false;
test11=false;
//return(false);
}
else if((Trim(document.form1.txtCity.value).length < 3)|| (Trim(document.form1.txtCity.value).length > 100))
{
    document.getElementById("errordesignation").style.display="block";
if(test11==true)
document.getElementById("errordesignation").innerHTML=" Length must be in between 1 and 100 ";
if(test==true)
document.form1.txtCity.focus();
test=false;
test11=false;
//return(false);
}
// End of City Validation

// Start of Country Validation
//if ( document.form1.cboCountry.selectedIndex == 0 )
if ( document.form1.cboCountry.selectedIndex <1 )
{
document.getElementById("errorcountry").style.display="block";
document.getElementById("errorcountry").innerHTML="Select Country Name";
if(test==true)
document.form1.cboCountry.focus();

test=false;
//return(false);
}
// End of Country Validation

////validation of payment type
//if (document.form1.rbncardpayment.checked || document.form1.rbnpaybycheque.checked)
//{
//  
//}
//else
//{
//document.getElementById("errorpaymenttype").style.display="block";
//document.getElementById("errorpaymenttype").innerHTML="Select Payment Type";
//if(test==true)
//document.form1.rbncardpayment.focus();

//test=false;

//}





return test;			
}