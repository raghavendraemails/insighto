﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="UploadExcel.aspx.cs" Inherits="UploadExcel" %>

<%@ Register src="UserControls/AddressBookSideNav.ascx" tagname="AddressBookSideNav" tagprefix="uc" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="contentPanelHeader"><!-- form header panel -->
		<div class="pageTitle"><!-- page title -->
			ADDRESS BOOK
		<!-- //page title --></div>
		
	<!-- //form header panel --></div>
	<div class="clear"></div>
	<div class="contentPanel"><!-- content panel -->
		<div class="surveyCreateTextPanel"><!-- survey created content panel -->
            <uc:AddressBookSideNav ID="ucAddressBookSideNav" runat="server" />
            <div class="surveyQuestionPanel"><!-- survey question panel -->
					<div class="surveyQuestionHeader">
						<div class="surveyQuestionTitle">CREATE EMAIL LIST</div>
					</div>
					<div><!-- text here -->
					<ul class="formList">
						<li>
							 <label for="uploadType">Upload</label>
                             <asp:RadioButton ID="rbtnUploadType" GroupName="uploadType" runat="server" Checked="true"  />
							 <img src="App_Themes/Classic/Images/icon-excel.gif" alt="Excel" title="Excel" />
                             <asp:RadioButton ID="rbtnUploadType2" runat="server" GroupName="uploadType"/>
							 <img src="App_Themes/Classic/Images/icon-word.gif"  alt="Word" title="Word" />
							 <span style="padding-left:20px;"><a href="#" id="lnkSamplePrview" runat="server" class="previewLink">Sample Preview</a></span>
						</li>
						<li>
							<label for="emailList">Email List Name</label>
                            <asp:TextBox ID="txtEmailListName" runat="server" MaxLength=25 class="textBoxMedium"></asp:TextBox>
							
						</li>
                         <li class="requiredLi">
                         <asp:Label ID="Label3"  AssociatedControlID="reqFirstName"  runat="server"  />
                         <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqFirstName" CssClass="lblRequired" 
                         ControlToValidate="txtEmailListName" ErrorMessage="Please enter list name."
                           runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
                        
                         </li>  
						<li>
							<label for="chooseFile">Choose File</label>
                            <asp:FileUpload ID="fileUpload" runat="server" />
						</li>
						<li>
							<label for="uploadFile"></label>
                            <asp:ImageButton ID="iBtnUploadFile" 
                                ImageUrl="App_Themes/Classic/Images/button_upload.gif" runat="server" 
                                onclick="iBtnUploadFile_Click" />
							
						</li>
					</ul>
						
					<!-- //text here --></div>
		<!-- //survey question panel --></div>
		<div class="clear"></div>
		<!-- //survey created content panel --></div>
		<div class="clear"></div>
	<!-- //content panel --></div>
    </div>

</asp:Content>
