/* Write here your custom javascript codes */

$(function(){
	setTimeout(function() {
		$('.hero h2').fadeIn();

		$(".typed").typed({
			strings: [ "customers", "sales force", "employees", "partners", "students", "members", "people"],
			typeSpeed: 10,
			contentType: 'text',
			startDelay: 200,
			backDelay: 2500,
			loop: true,
			showCursor: false
		});
	}, 1000);
});