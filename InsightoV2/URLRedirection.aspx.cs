﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using Insighto.Business.Enumerations;
using App_Code;

namespace Insighto.Pages
{
    public partial class URLRedirection : SurveyPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userDetails = sessionService.GetLoginUserDetailsSession();
                var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
                var listFeatures = ServiceFactory.GetService<FeatureService>();
                if (base.SurveyID != 11098)
                {
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.REDIRECTION_URL).Value == 0)
                    {
                        btnSaveContinue.Attributes.Add("Onclick", "javascript:OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "', 690, 515, 'yes');return false;");
                    }
                }

                if (base.SurveyBasicInfoView.URLREDIRECTION_STATUS == 1)
                {
                    txtUrl.Text = base.SurveyBasicInfoView.URL_REDIRECTION;
                    if (base.SurveyBasicInfoView.TIME_FOR_URL_REDIRECTION != null && base.SurveyBasicInfoView.TIME_FOR_URL_REDIRECTION.Length > 0)
                    {
                        txtTime.Text = base.SurveyBasicInfoView.TIME_FOR_URL_REDIRECTION;
                    }
                    else
                    {
                        txtTime.Text = "0";
                    }
                }
                else
                {
                    txtTime.Text = "0";
                }
                lblHelpText.Text = String.Format(Utilities.ResourceMessage("lblHelpText"),SurveyMode.ToLower());
            }
        }
        protected void btnSaveContinue_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0)
                {

                    osm_surveycontrols controls = new osm_surveycontrols();
                    bool isBlockedWordExists = false;
                    if (base.SurveyBasicInfoView.STATUS == "Active")
                    {
                        var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();
                        isBlockedWordExists = blockedWords.Where(b => txtUrl.Text.Contains(b.BLOCKEDWORD_NAME)).Any();
                    }
                    if (!isBlockedWordExists)
                    {
                        SurveyBasicInfoView.SURVEY_ID = base.SurveyBasicInfoView.SURVEY_ID;
                        SurveyBasicInfoView.URL_REDIRECTION = txtUrl.Text.Trim();
                        SurveyBasicInfoView.TIME_FOR_URL_REDIRECTION = txtTime.Text.Trim();
                        SurveyBasicInfoView.URLREDIRECTION_STATUS = 1;
                        ServiceFactory.GetService<SurveyControls>().UpdateSurveyControlsFromView(SurveyBasicInfoView);
                        //if (btnSaveContinue.Text == "Save and edit more")
                        //{
                        //    string message = AlertMessageText();
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "myscript", "alert('" + message + "');window.location.href='Launch.aspx';", true);
                        //}
                        //else
                        //{
                            lblMessage.Text = "URL redirection saved successfully.";
                            sucessMsg.Visible = true;
                            dvErrMsg.Visible = false;
                        //}
                    }
                    else
                    {
                        lblErrMsg.Text = "URL contains blocked words.";
                        lblErrMsg.Visible = true;
                        dvErrMsg.Visible = true;
                        sucessMsg.Visible = false;
                    }

                }
            }
        }
        public string AlertMessageText()
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            string txtMessage = "";
            txtMessage = "You have saved changes to an active "+ SurveyMode.ToLower() +". These changes are effective immediately and your respondents will now see the revised " + SurveyMode.ToLower();
            if (base.SurveyBasicInfoView.EMAILLIST_TYPE != 2)
            {
                if (base.SurveyBasicInfoView.SCHEDULE_ON == 1 && base.SurveyBasicInfoView.LAUNCH_ON_DATE.AddMinutes(userDetails.StandardBIAS) > DateTime.UtcNow.AddMinutes(userDetails.StandardBIAS))
                {
                    if (base.SurveyBasicInfoView.EMAILLIST_TYPE == 1 && base.SurveyBasicInfoView.CONTACTLIST_ID > 0)
                    {
                      var contactDetails=  ServiceFactory.GetService<EmailService>().GetContactListByContactListId(base.SurveyBasicInfoView.CONTACTLIST_ID);
                      if (contactDetails != null)
                      {
                       var contactListName = contactDetails.Select(s => new { s.CONTACTLIST_NAME });
                       txtMessage = "Your changes have been saved. Your respondents will see the revised changes when the " + SurveyMode.ToLower() + " gets launched on " + base.SurveyBasicInfoView.LAUNCH_ON_DATE.AddMinutes(userDetails.StandardBIAS);
                       txtMessage += ". If you wish to change the date of launch, you may do so from the " + SurveyMode + " Controls page. The " + SurveyMode.ToLower() + " will be sent to " + contactDetails.Count + " email ids that are currently in your email list " + contactListName;
                      }
                    }

                }
                else
                {
                    txtMessage = "Your changes have been saved. Your respondents will see the revised changes when the " + SurveyMode.ToLower() + " gets launched on" + base.SurveyBasicInfoView.LAUNCH_ON_DATE.AddMinutes(userDetails.StandardBIAS);

                }
            }
            else
            {
                txtMessage = "Changes have been made in the " + SurveyMode.ToLower() + " and will reflect in the URL that you have used to ";
                txtMessage += "launch the " + SurveyMode.ToLower() + ". In case you want to send the " + SurveyMode.ToLower() + " to more people, please copy and ";
                txtMessage += "paste the URL below and send through your own mail system / IM / Social media.                                                                                                                                             ";
                txtMessage += " URL : " + base.SurveyBasicInfoView.SURVEY_URL;
            }

            return txtMessage;
        }
    }
}