﻿using System;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Text.RegularExpressions;
using Resources;
using App_Code;
using Insighto.Business.Enumerations;

public partial class UserControls_UserLoginControl : System.Web.UI.UserControl
{
    public BasePage BasePageProperties
    {
        get
        {
            return Page as BasePage;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblErrMsg.Text = string.Empty;            
            if (Request.QueryString["key"] != null)
            {

                Hashtable ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("UserId"))
                {
                    int UserId = ValidationHelper.GetInteger(ht["UserId"].ToString(), 0);
                    UsersService userService = new UsersService();
                    var user = userService.ActivateUser(UserId);
                    if (user != null)
                    {
                        string fromEmail = ConfigurationManager.AppSettings["emailAcknowlegement"];
                        string subject = (String)GetGlobalResourceObject("CommonMessages", "emailSubjectAcknowledgeAccountActivation");
                        MailHelper.SendMailMessage(fromEmail, user.LOGIN_NAME, "", "", subject, subject);
                    }
                }

                if (ht != null && ht.Count > 0 && ht.Contains("AlternateId"))
                {
                    int Id = ValidationHelper.GetInteger(ht["AlternateId"].ToString(), 0);
                    UsersService userService = new UsersService();
                    var user = userService.ActivateAlternateUser(Id);
                    if (user != null)
                    {
                        string fromEmail = ConfigurationManager.AppSettings["emailAcknowlegement"];
                        string subject = (String)GetGlobalResourceObject("CommonMessages", "emailSubjectAcknowledgeAccountActivation");
                        MailHelper.SendMailMessage(fromEmail, ht["Email"].ToString(), "", "", subject, subject);
                    }
                }
            }
        }
    }

    protected void ibtnLogin_Click(object sender, EventArgs e)
    {
        if (!IsPostBack)
            return;

        string alrt = (String)GetLocalResourceObject("stringPleaseEnter");  //"Please enter";
        this.lblErrMsg.Visible = true;
        bool flag = false;
        if (string.IsNullOrEmpty(txtEmailId.Text.Trim()) || txtEmailId.Text.ToLower() == "email address")
        {
            alrt += (String)GetLocalResourceObject("stringEmailAddress"); //" email address.";
            lblErrMsg.Text = alrt;
        }
        else
        {
            Regex reLenient = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            bool isEmail = reLenient.IsMatch(txtEmailId.Text);
            if (!isEmail)
            {
                alrt += (String)GetLocalResourceObject("stringValidEmailAddress");  //" valid email address.";
                lblErrMsg.Text = alrt;
            }
            else if (Convert.ToString(txtPassword.Text).Length == 0)
            {
                alrt += (String)GetLocalResourceObject("stringPassword");  //" password.";
                lblErrMsg.Text = alrt;
            }
            flag = true;
        }

        if (flag && Convert.ToString(txtEmailId.Text).Length > 0 && Convert.ToString(txtPassword.Text).Length > 0)
        {
            // To check authenticated user       
            var userService = ServiceFactory.GetService<UsersService>();
            var user = userService.getAuthenticatedUser(txtEmailId.Text, txtPassword.Text);
            if (user == null)
            {
                lblErrMsg.Text = (String)GetLocalResourceObject("stringInvalidEmail");  //"Invalid Email Address or Password.";
                lblErrMsg.Visible = true;
                return;
            }
            else if (Utilities.ToEnum<SurveyStatus>(user.STATUS) == SurveyStatus.InActive || ValidationHelper.GetInteger(user.DELETED, 0) == 1)
            {
                lblErrMsg.Text = "Your Account is de-activated.";
                lblErrMsg.Visible = true;
                return;
            }
            else if (user.ACTIVATION_FLAG == 0 || user.ACTIVATION_FLAG == null)
            {
                lblErrMsg.Text = (String)GetLocalResourceObject("stringAccountNotActivated");  //" Your Account is not Activated.";
                lblErrMsg.Visible = true;
                return;

            }
            else
            {
                userService.ProcessUserLogin(user);
                if (user.TIME_ZONE == null) //Timezone is null for first user login. hence redirecting to welcome page
                {
                    //create folder with userid 
                    BasePageProperties.CloseModal(EncryptHelper.EncryptQuerystring("Welcome.aspx", "surveyFlag=" + user.SURVEY_FLAG)); 
                }
                else if (user.RESET == 1) // Password reset value is 0 for first login. hence redirectiong to chanepassword page
                {
                    BasePageProperties.CloseModal(EncryptHelper.EncryptQuerystring("ChangePassword.aspx", "surveyFlag=" + user.SURVEY_FLAG)); 
                }
                else // on successful login user is redirected to Myaccounts page
                {
                    Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                    BasePageProperties.CloseModal(EncryptHelper.EncryptQuerystring("MyAccounts.aspx", "surveyFlag=" + user.SURVEY_FLAG)); 
                }
            }

        }
    }
}