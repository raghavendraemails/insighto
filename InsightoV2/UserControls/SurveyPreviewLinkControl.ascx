﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveyPreviewLinkControl.ascx.cs"
    Inherits="Insighto.UserControls.SurveyPreview" %>
<asp:HyperLink ID="hlnkPreview" runat="server" CssClass="previewLink" rel="framebox"
    Text="Survey Preview" h="480" w="950" scrolling='yes' onclick='javascript: return ApplyFrameBox(this);'
    ToolTip="Survey Preview" alt="Survey Preview" title="Survey Preview" 
    meta:resourcekey="hlnkPreviewResource1" />