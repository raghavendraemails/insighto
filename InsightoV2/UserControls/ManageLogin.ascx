﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageLogin.ascx.cs" Inherits="Insighto.UserControls.ManageLogin" %>
<asp:CustomValidator SetFocusOnError="true"  ID="custTextControl" OnServerValidate="custValidateUniqText"
        runat="server" CssClass="lblRequired" Enabled="false" Display="Dynamic" ErrorMessage="Columns should not be same."></asp:CustomValidator>
    <asp:Repeater ID="addRemoveControls" runat="server" OnItemCommand="addRemoveControls_ItemCommand"
        OnItemDataBound="itemDataBoundRepeater_ItemDataBound">
        <ItemTemplate>
            <div class="itempanel">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td class="wid_90">
                            <asp:Label ID="lblColumn" runat="server" Text="Alternate Email "  CommandArgument="<%# Container.ItemIndex %>" /></asp:Label>
                        </td>
                        <td>                            
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="textBoxMedium200" MaxLength="50"
                                Text='<%# Container.DataItem %>'  />
                        </td>
                        <td class="space">&nbsp;</td>
                        <td>
                            <asp:Button ID="btnRemove" runat="server" Text='' CssClass="RemoveIcon" CausesValidation="false"
                                CommandArgument="<%# Container.ItemIndex %>" CommandName='Remove' />
                        </td>
                        <td class="space">&nbsp;</td>
                        <td>
                            <asp:Button ID="btnAddAnother" runat="server" CssClass="addedIcon" Text='' CausesValidation="false"
                                CommandArgument="<%# Container.ItemIndex %>" CommandName='Add' />
                        </td>
                         <td class="space">&nbsp;</td>
                        <td>                            
                            <asp:Button ID="btnLoginMail"  runat="server" CssClass="btn_loginEmail" Text='Make this as my Login Email Id' CausesValidation="false"
                                CommandArgument="<%# Container.ItemIndex %>" CommandName='ManageLogin'  />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="8">
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqAnsoption" runat="server" ControlToValidate="txtEmail"
                                Enabled="true" CssClass="lblRequired" Display="Dynamic" ErrorMessage="Please enter email id."></asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regEmailId" runat="server" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                        CssClass="lblRequired" ControlToValidate="txtEmail" ErrorMessage="Please enter email id in valid format. Ex: john@insighto.com"
                        Display="Dynamic"></asp:RegularExpressionValidator></div>

                            
                        </td>
                    </tr>
                </table>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>