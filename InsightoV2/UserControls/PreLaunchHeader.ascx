﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PreLaunchHeader.ascx.cs" Inherits="Insighto.UserControls.PreLaunchHeader" %>
<div class="contentPanelHeader"><table cellpadding="0" cellspacing="0" width="100%" border="0">
    <tr>
        <td class="wid_70">
           
                <div class="pageTitle">
                    <%= SurveyMode %><asp:Label ID="lblName" runat="server" Text="Name :" 
                        meta:resourcekey="lblNameResource1"></asp:Label>
                    <asp:Label ID="lblSurveyName" runat="server" 
                        meta:resourcekey="lblSurveyNameResource1"></asp:Label>
                    <span><a id="lnkEdit" runat="server" href="" rel="framebox" h="480" w="700" class="smallFont">
                       <asp:Label ID="lblEdit" runat="server" Text="Edit" 
                        meta:resourcekey="lblEditResource1"></asp:Label>   </a></span>
                </div>
           
        </td>
        <td class="pre_rgt_pad">
            <asp:Button ID="btnBack" runat="server" Text="Back to Launch Page" ToolTip="Back to Launch Page"
                CssClass="prelaunchbackbtn" OnClick="btnBack_Click" 
                meta:resourcekey="btnBackResource1" />
        </td>
    </tr>
</table> </div>