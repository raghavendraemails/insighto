﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using InfoSoftGlobal;
using System.Configuration;

public partial class UserControls_MtxSbS : System.Web.UI.UserControl
{
    public string exportchartfilehandler;  

    protected void Page_Load(object sender, EventArgs e)
    {
        //exportHandler='" + exportchartfilehandler + "' exportAction='download'
        exportchartfilehandler = ConfigurationSettings.AppSettings["exportchartfile"].ToString();

        StringBuilder strXML = new StringBuilder();
        strXML.Append("<chart caption='Please rate your level of satisfaction and importance of the following departments(1 being the lowest and 3 being the highest)' numberSuffix='%' exportEnabled='1' canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1'  formatNumberScale='0' bgColor='FFFFFF' showAboutMenuItem='0'> ");
        strXML.AppendFormat("<categories>");
        strXML.AppendFormat(" <category Label='Satisfaction'/>");
        strXML.AppendFormat(" <category Label='Importance'/>");
        strXML.AppendFormat("</categories>");

        strXML.AppendFormat("<dataset seriesName='Sales Team'>");
        strXML.AppendFormat(" <set  value='70' /> ");                
            strXML.AppendFormat("<set  value='90' />");           
            strXML.AppendFormat("</dataset>");

            strXML.AppendFormat("<dataset seriesName='Technical Support'>");
            strXML.AppendFormat(" <set  value='60' /> ");
            strXML.AppendFormat("<set  value='50' />");           
            strXML.AppendFormat("</dataset>");

            strXML.AppendFormat("<dataset seriesName='Customer Service'>");
            strXML.AppendFormat(" <set  value='80' /> ");
            strXML.AppendFormat("<set  value='50' />");          
            strXML.AppendFormat("</dataset>");
            strXML.Append("</chart>");
            Literal1.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/MSColumn3D.swf", "", strXML.ToString(), "MtxSBS", "600", "300", false, true, false);
    }
}