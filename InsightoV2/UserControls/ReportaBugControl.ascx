﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportaBugControl.ascx.cs" Inherits="UserControls_ReportaBugControl" %>
<div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                <asp:Label ID="lblTitle" runat="server" Text="CONTACT SUPPORT" 
                    ></asp:Label>
                </h3>
                
        </div>
    <%--   <div class="popupClosePanel">
			<a href="#" class="popupCloseLink" alt="Close" title="Close" onclick ="closeModalSelf(false,'');">&nbsp;</a>
		</div>--%>
        <!-- //popup heading -->
    </div>
      <div class="defaultHeight"> </div>
<div class="popupContentPanel">
<div class="formPanel">
    <p>&nbsp;</p>
        <div class="text">
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" CssClass="errorMesg" Visible="False" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" 
                    Text="Thank you for contacting us. We will get back to you shortly." 
                    Visible="False" meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
        </div>
    </div>
    <div class="mandatoryPanel" style="margin-top:0;">
        <asp:Label ID="lblRequiredFields" runat="server" 
            Text="indicates required fields" meta:resourcekey="lblRequiredFieldsResource1"></asp:Label>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="lblName" runat="server" Text="Your Name"  
                meta:resourcekey="lblNameResource1" />
        </div>
        <div class="loginControls">
            <asp:TextBox ID="txtName" runat="server" CssClass="textBoxMedium" MaxLength="50"
                TabIndex="1" meta:resourcekey="txtNameResource1"></asp:TextBox>
            <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqUserName" Display="Dynamic"
                ControlToValidate="txtName" runat="server" CssClass="lblRequired" 
                ErrorMessage="Please enter name." meta:resourcekey="reqUserNameResource1"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="Label1" runat="server" Text="Your Email"  
                meta:resourcekey="Label1Resource1" />
        </div>
        <div class="loginControls">
            <asp:TextBox ID="txtMail" runat="server" CssClass="textBoxMedium" MaxLength="50"
                TabIndex="2" meta:resourcekey="txtMailResource1"></asp:TextBox>
            <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqMail" ControlToValidate="txtMail"
                runat="server" Display="Dynamic" ErrorMessage="Please enter email." 
                CssClass="lblRequired" meta:resourcekey="reqMailResource1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator SetFocusOnError="True" ID="rexUsername" CssClass="lblRequired"
                ControlToValidate="txtMail" runat="server" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ErrorMessage="Please enter valid email." 
                meta:resourcekey="rexUsernameResource1"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="Label2" runat="server" Text="Phone Number" 
                meta:resourcekey="Label2Resource1" />
        </div>
        <table>
        <tr>
        <td>
        <div class="loginControls" >
            <table border="0" cellpadding="2" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtCountryCode" runat="server" TabIndex="9" MaxLength="4" CssClass="phoneCountry"
                                                        meta:resourcekey="txtCountryCodeResource1" onkeypress="javascript:return onlyNumbers(event,'Countrycode');"
                                                        onpaste="return false;"></asp:TextBox>
                                                </td>
                                                <td valign="middle">
                                                   <asp:Label ID="dash1" runat="server" Text="&#45;"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAreaCode" runat="server" TabIndex="10" MaxLength="5" CssClass="phoneArea"
                                                        meta:resourcekey="txtAreaCodeResource1" onkeypress="javascript:return onlyNumbers(event,'');"
                                                        onpaste="return false;"></asp:TextBox>
                                                </td>
                                                <td valign="middle">
                                                   <asp:Label ID="dash2" runat="server" Text="&#45;"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPhno" runat="server" MaxLength="10" TabIndex="11" CssClass="phoneNumber_long"
                                                         onkeypress="javascript:return onlyNumbers(event,'');"
                                                        onpaste="return false;" Text=""></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                         </div>
        
        </td>
        </tr>
        <tr>
        <td>
          <table border="0" cellpadding="2" cellspacing="0">
                                            <tr>
                                            
                                                <td>
                                                    <asp:Label ID="Label8" runat="server" Text=" Country Code - Area Code - Phone Number" Font-Size="10px"></asp:Label> 
                                                </td>
                                             
                                            </tr>
                                        </table>
        </td>
        </tr>
        </table>
                                                               
                                     
        <div class="con_login_pnl">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblPhoneHelp" runat="server" AssociatedControlID="lblPhoneHelpText"
                                            meta:resourcekey="lblPhoneHelpResource1" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:Label ID="lblPhoneHelpText" runat="server" CssClass="note" meta:resourcekey="lblPhoneHelpTextResource" />
                                    </div>
                                </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="Label4" runat="server" Text="Attention" 
                meta:resourcekey="Label4Resource1" />
        </div>
        <div class="loginControls">
            <asp:DropDownList ID="ddlAttenValues" CssClass="dropdownMedium" runat="server" 
                TabIndex="4" meta:resourcekey="ddlAttenValuesResource1">              
            </asp:DropDownList>
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="Label5" runat="server" Text="Mail Message" CssClass="requirelbl" 
                meta:resourcekey="Label5Resource1" />
        </div>
        <div class="loginControls">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:TextBox ID="txtMessage" TextMode="MultiLine" CssClass="textAreaMedium" runat="server"
                            TabIndex="5" onpaste="javascript:return false;" 
                            meta:resourcekey="txtMessageResource1" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqtxtMessage" runat="server"
                            ErrorMessage="Please enter message." ControlToValidate="txtMessage" 
                            CssClass="lblRequired" meta:resourcekey="reqtxtMessageResource1"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <span id="spn" runat="server" class="spn"><asp:Label ID="lblMaxtext" 
                            Text="Maximum 500 Characters"  runat="server"></asp:Label></span>
                    </td>
                </tr>
            </table>
        </div>
        <div id="message">
        </div>
    </div>
    <div class="con_login_pnl">
    <div class="loginlbl">
        <asp:Label ID="Label6" runat="server" Text="Upload screenshot"></asp:Label>
    </div>
    <div class="loginControls">
        <asp:FileUpload ID="FileUpload1" runat="server" />
    </div>
    </div>

    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="Label3" runat="server" AssociatedControlID="reCaptchaimg" 
                meta:resourcekey="Label3Resource1" />
        </div>
        <div class="loginControls">
            <img src="~/In/Captcha.aspx" id="reCaptchaimg" alt="Catptcha Text" runat="server" />
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
        </div>
        <div class="loginControls">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:Label ID="lblRecaptcha" Text="Type the above number"  runat="server" 
                            meta:resourcekey="lblRecaptchaResource1" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtCaptcha" CssClass="aspxtextbox textBoxSmall" runat="server" 
                            TabIndex="6" meta:resourcekey="txtCaptchaResource1" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
        </div>
        <div class="loginControls">
            <div class="top_padding">
                <asp:Button ID="btnsubmit" runat="server" CssClass="btn_small_65" ToolTip="Submit"
                    Text="Submit" OnClick="btnsubmit_Click" TabIndex="7" 
                    meta:resourcekey="btnsubmitResource1" /></div>
        </div>
    </div>
<div class="clear"></div>
</div>
</div>
<div class="clear"></div>
<script type="text/javascript">
    function onlyNumbers(e, Type) {
        var key = window.event ? e.keyCode : e.which;
        if (Type == 'Countrycode' && key == 43) {
            return true;
        }
        else if (key > 31 && (key < 48 || key > 57)) {
            return false;
        }
        return true;
    }
    $(function () {
        var limit = 500;
        $('textarea[id$=txtMessage]').keyup(function () {
            var len = $(this).val().length;
            if (len > limit) {
                this.value = this.value.substring(0, limit);
                len = limit;
            }
            $('.spn').text(limit - len + " characters left");
        });
    });
</script>