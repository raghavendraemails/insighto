﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using DevExpress.Web.ASPxEditors;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using sharp = iTextSharp.text;
using FeatureTypes;
using Insighto.Business.ValueObjects;
using System.Text;
using System.Linq;
using System.Threading;
using DevExpress.Web.ASPxClasses;
using System.Diagnostics;

namespace Insighto.UserControls
{
    public partial class SurveyReportHeader : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var page = this.Page as SurveyPageBase;
            if (page == null || page.SurveyBasicInfoView == null) return;

            lblSurveyName.Text = page.SurveyBasicInfoView.SURVEY_NAME;
            int surveyid = page.SurveyBasicInfoView.SURVEY_ID;
            DataSet dsAnalytics = getSurveyAnalytics(surveyid);

            lblCompleteValue.Text = dsAnalytics.Tables[0].Rows[0][0].ToString();

            lblPartialValue.Text = dsAnalytics.Tables[2].Rows[0][0].ToString();

            lblVisitsValue.Text = dsAnalytics.Tables[1].Rows[0][0].ToString();



        }
        public DataSet getSurveyAnalytics(int surveyID)
        {
            try
            {
           
                SqlConnection con = new SqlConnection();

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_surveyAnalytics", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@SURVEY_ID", SqlDbType.Int)).Value = surveyID;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dssurveyprofile = new DataSet();
                sda.Fill(dssurveyprofile);
                con.Close();
                return dssurveyprofile;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public SqlConnection strconnectionstring()
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection strconn = new SqlConnection(connStr);

                strconn.Open();
                return strconn;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}