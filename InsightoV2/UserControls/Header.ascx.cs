﻿using System;
using Insighto.Business.Helpers;

namespace Insighto.UserControls
{
    public partial class Header : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.RawUrl.ToLower().Contains("welcomepolls"))
            {
                logo.Attributes.Add("src", "~/App_Themes/Classic/Images/Insighto_logo_polls.png");
                logolink.Attributes.Add("href", "~/Polls/");
            }
        }
    }
}