﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveyMatrixControl.ascx.cs"
    Inherits="UserControls.SurveyMatrixControl" %>
<%@ Register Src="~/UserControls/SurveyAnswerControl.ascx" TagPrefix="uc" TagName="SurveyMatrixAnswerControl" %>
<div>
<div id="divSidebySide" runat="server">
    <asp:Label runat="server" ID="lblAnswerOptionsHeadings" meta:resourcekey="lblAnswerOptionsHeadingsResource"></asp:Label>
    <uc:SurveyMatrixAnswerControl ID="SurveyMatrixHeadingAnswerControl" runat="server" />
    </div>
    <asp:Label runat="server" ID="lblAnswerOptionsTop" meta:resourcekey="lblAnswerOptionsTopResource"></asp:Label>
    <uc:SurveyMatrixAnswerControl ID="ucSurveyMatrixTopAnswerQuestion" runat="server" />
    <asp:Label runat="server" ID="lblAnswerOptionsLeft" meta:resourcekey="lblAnswerOptionsLeftResource"></asp:Label>
    <uc:SurveyMatrixAnswerControl ID="SurveyMatrixLeftAnswerControl" runat="server" />
</div>
