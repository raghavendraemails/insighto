﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PreLaunchSideNav.ascx.cs"
    Inherits="Insighto.UserControls.PreLaunchSideNav" %>
<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="prelaunchcheckbg"><asp:Label ID="lblPreLaunchCheck" runat="server" 
                Text="Pre Launch Check" meta:resourcekey="lblPreLaunchCheckResource1"></asp:Label></td><td>
            <ul class="launchStatusUL">
           
                <li><a id="aemailinv" rel="EmailInvitation" runat="server">
                   <asp:Label ID="lblViewEmailInvitation" runat="server" 
                        Text="View Email Invitation" meta:resourcekey="lblViewEmailInvitationResource1"></asp:Label></a></li>
                <li><a id="aemaillist" rel="SurveyEmailList" runat="server">
                  <asp:Label ID="lblViewSurveyEmailList" runat="server" 
                        Text="View Survey Email List" 
                        meta:resourcekey="lblViewSurveyEmailListResource1"></asp:Label></a></li>
                <li><a id="aquestion" href="PreLaunchQuestionnaire.aspx?Key=<%= Key %>" rel="ViewQuestions">
                   <asp:Label ID="lblViewQuestions" runat="server" Text="View Questions" 
                        meta:resourcekey="lblViewQuestionsResource1"></asp:Label></a></li>
                <li><a id="acontrols" href="PreLaunchSurveyControls.aspx?Key=<%= Key %>" rel="ViewSurveyControls">
                  <asp:Label ID="lblView" runat="server" Text="View " 
                        meta:resourcekey="lblViewResource1"></asp:Label></a></li>
                <li class="noborder"><a id="amessages" href="PreLaunchSurveyEndMessages.aspx?Key=<%= Key %>"
                    rel="ViewThankyouPage"><asp:Label ID ="lblViewThankyouPage" runat="server" 
                        Text="View Thank you Page" meta:resourcekey="lblViewThankyouPageResource1"></asp:Label></a></li>
            </ul>
        </td>
      
    </tr>
</table>
<div class="clear">
</div>
<script language="javascript" type="text/javascript">
    var url = location.href;
    var url_parts = url.split('?');
    var main_url = url_parts[0];
    var url_split = main_url.split('/');

    if (url_split[3] == 'PreLaunchEmailInvitation.aspx')
    
    { document.getElementById('<%= aemailinv.ClientID %>').className = 'active'; }
    if (url_split[3] == 'PreLaunchSurveyEmailList.aspx')
    { document.getElementById('<%= aemaillist.ClientID %>').className = 'active'; }
    if (url_split[3] == 'PreLaunchQuestionnaire.aspx')
    { document.getElementById('aquestion').className = 'active'; }
    if (url_split[3] == 'PreLaunchSurveyControls.aspx')
    { document.getElementById('acontrols').className = 'active'; }
    if (url_split[3] == 'PreLaunchSurveyEndMessages.aspx') {
        document.getElementById('amessages').className = 'active';
    }

</script>
