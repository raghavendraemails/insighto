﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using App_Code;


namespace Insighto.UserControls
{
    public partial class SurveyPrint : UserControlBase
    {

        #region "Variables"

        Hashtable typeHt = new Hashtable();
        int surveyId = 0;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
                if (Utilities.ShowOrHideFeature(Utilities.GetUserType(), (int)FeatureTypes.SurveyCreation.Print_Survey))
                {


                    string strTemplatePreviewLink = EncryptHelper.EncryptQuerystring("~/SurveyRespondentPreview.aspx", Constants.SURVEYID + "=" + surveyId + "&Mode=" + RespondentDisplayMode.Preview.ToString() + "&surveyFlag=" + base.SurveyFlag);
                    hlnkPrint.NavigateUrl = strTemplatePreviewLink;
                    hlnkPrint.Attributes.Add("onclick", "javascript: return ApplyFrameBox(this);");
                    hlnkPrint.Attributes.Add("rel", "framebox");
                }
                else
                {
                    string strTemplatePreviewLink = EncryptHelper.EncryptQuerystring("~/SurveyRespondentPreview.aspx", Constants.SURVEYID + "=" + surveyId + "&Mode=" + RespondentDisplayMode.Preview.ToString() + "&surveyFlag=" + base.SurveyFlag);
                    hlnkPrint.NavigateUrl = strTemplatePreviewLink;
                    hlnkPrint.Attributes.Add("onclick", "javascript: return ApplyFrameBox(this);");
                    hlnkPrint.Attributes.Add("rel", "framebox");
                    //hlnkPrint.NavigateUrl = "javascript:void(0);";
                    //hlnkPrint.Attributes.Add("onclick", "javascript:OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "")+"',690, 450, 'yes');return false;");
                }               
            }
        }
    }
}