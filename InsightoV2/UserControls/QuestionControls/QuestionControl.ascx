﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuestionControl.ascx.cs"
    Inherits="Insighto.UserControls.QuestionControls.QuestionControl" %>
<%@ Reference VirtualPath="~/UserControls/QuestionControls/QuestionSettingsControl.ascx" %>
<asp:Label ID="lblTypeYpurQuestion" CssClass="purpleColor" Font-Bold="true" Text="Question : Type your question in the space below."
    runat="server"></asp:Label>
<textarea id="EditorQuestion" runat="server" cols="10" rows="8" class="mceEditor"
    visible="false" style="width: 100%; height: 116px;"></textarea>
<asp:RequiredFieldValidator SetFocusOnError="true" ID="rqEditorQuestion" runat="server"
    ErrorMessage="Please enter question." CssClass="lblRequired" Display="Dynamic"
    ControlToValidate="EditorQuestion" Visible="false"></asp:RequiredFieldValidator>
<asp:CustomValidator SetFocusOnError="true" ID="custText" runat="server" ErrorMessage="Characters limit should not limit more than 4000"
    CssClass="lblRequired" Display="Dynamic" OnServerValidate="custValidateText"></asp:CustomValidator>
<%--<asp:RequiredFieldValidator SetFocusOnError="true" ID="reqIntro" runat="server" ErrorMessage="Please enter introduction."
    CssClass="lblRequired" Display="Dynamic" ControlToValidate="EditorQuestion" Visible="false"></asp:RequiredFieldValidator>--%>
<asp:HiddenField runat="server" ID="hdnEditorQuesParams" Value="0" />
<script type="text/javascript">
    tinyMCE.init({
        // General options
        mode: "specific_textareas",
        editor_selector: "mceEditor",
        theme: "advanced",

        plugins: "paste",

        // Theme options
        theme_advanced_buttons1: "bold,italic,underline,fontselect,fontsizeselect,forecolor,pasteword",
        theme_advanced_buttons2: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        //theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing: false,

        // Example content CSS (should be your site CSS)
        content_css: "css/content.css",
        oninit: "postInitWork",
        // Replace values for the template plugin
        template_replace_values: {
            username: "Some User",
            staffid: "991234"
        }
    });

    function postInitWork() {
        var val = document.getElementById("<%=hdnEditorQuesParams.ClientID%>").value;
        var sty_val = val.split("_");
        if (sty_val.length == 3) {
            tinyMCE.getInstanceById('<%=EditorQuestion.ClientID%>').getWin().document.body.style.fontFamily = sty_val[0];
            tinyMCE.getInstanceById('<%=EditorQuestion.ClientID%>').getWin().document.body.style.fontSize = sty_val[1];
            tinyMCE.getInstanceById('<%=EditorQuestion.ClientID%>').getWin().document.body.style.color = sty_val[2];
        }
    }
</script>
<script type="text/javascript">
    function checkLength(validator, args) {
        var editor = '<%=EditorQuestion.ClientID%>';     // get a reference to RadEditor
        var editorText = editor.GetText(true);     //get the HTML content of the control
        // args.IsValid = editorText.length > limitNum && editorText.length < 15;  
        alert(editorText.length);
        args.IsValid = false;
    }     
</script>
