﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactInfoTextBoxControl.ascx.cs"
    Inherits="Insighto.UserControls.QuestionControls.ContactInfoTextBoxControl" %>
<div class="itempanel">
    <table cellpadding="0" cellspacing="0" border="0">
        <tr class="txtRow">
            <td>
                <asp:CheckBox ID="chkInclude" runat="server" />
            </td>
            <td class="space">
                &nbsp;
            </td>
            <td>
                <asp:TextBox ID="txtAnsOption" runat="server" CssClass="textBoxMedium txtAns" MaxLength="150" />
            </td>
            <td class="space">
                &nbsp;
            </td>
            <td>
              <asp:CheckBox ID="chkResposneRequired" runat="server" />
            </td>           
        </tr>
        <tr class="lblRow">
            <td colspan="5">
                <%--    <asp:Label ID="lblMsg" runat="server" Text="Please enter answer option." CssClass="lblRequired"
                    Style="display: none"></asp:Label>--%>
                <asp:RequiredFieldValidator ID="rfvAnswerOption" runat="server" ErrorMessage="Please enter answer option."
                    CssClass="lblRequired" Display="Dynamic" ControlToValidate="txtAnsOption" />
            </td>
        </tr>
    </table>
</div>
