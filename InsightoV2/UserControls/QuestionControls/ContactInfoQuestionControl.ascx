﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactInfoQuestionControl.ascx.cs" Inherits="Insighto.UserControls.QuestionControls.ContactInfoQuestionControl" %>
<%@ Register Src="ContactInfoTextBoxControl.ascx" TagName="ContactInfoTextBoxControl"
    TagPrefix="Insighto" %>
<%@ Reference VirtualPath="~/UserControls/QuestionControls/ContactInfoTextBoxControl.ascx"  %>
<asp:GridView ID="gvAnswerOptions" runat="server" AutoGenerateColumns="false" DataKeyNames="ANSWER_ID, AnswerOptionUniqueId, SequenceId"
    OnRowDataBound="gvAnswerOptions_RowDataBound">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>                        
               <Insighto:InsightoCheckBox
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>