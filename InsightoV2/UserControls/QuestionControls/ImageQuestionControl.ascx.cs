﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Data;
using Insighto.Web.UI.Controls;
using Insighto.UserControls.QuestionControls;
using System.Web.UI.HtmlControls;
using Insighto.Business.ValueObjects;

namespace UserControls.QuestionControls
{

    public partial class ImageQuestionControl : QuestionControlBase
    {
        #region Private properties

        private string _textControlPrefix = string.Empty;
        private List<osm_answeroptions> _lstResponseOtions;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions QuestionAndAnswerOptions
        {
            get
            {
                return base.QuestionAndAnswerOptions;
            }
            set
            {
                base.QuestionAndAnswerOptions = value;
                QuestionControl1.QuestionAndAnswerOptions = value;
               
            }
        }
        
        public override List<SurveyQuestionAndAnswerOptions> SelectedSurveyQuestionAndAnswerOptions
        {
            get
            {
                return GetSurveyQuestionAndAnswers();
            }
            set
            {
                base.SelectedSurveyQuestionAndAnswerOptions = value;
            }
        }

        /// <summary>
        /// Gets the survey question and answers.
        /// </summary>
        /// <returns></returns>
        private List<SurveyQuestionAndAnswerOptions> GetSurveyQuestionAndAnswers()
        {
            var surveyOptionsList = new List<SurveyQuestionAndAnswerOptions>();
            var questionAndAnswers = new SurveyQuestionAndAnswerOptions();

            osm_surveyquestion surveyQuestion = new osm_surveyquestion
            {
                QUESTION_TYPE_ID = (int)SurveyPageBase.QuestionType,
                QUSETION_LABEL = QuestionText,
            };

            questionAndAnswers.Question = surveyQuestion;
            questionAndAnswers.AnswerOption = SelectedAnswerOptions;
            surveyOptionsList.Add(questionAndAnswers);
            return surveyOptionsList;
        }

        /// <summary>
        /// Gets the question text.
        /// </summary>
        public string QuestionText
        {
            get
            {
                var questionControl = Utilities.FindControl<QuestionControl>(this, "QuestionControl1");
                var questionText = Utilities.FindControl<HtmlTextArea>(questionControl, "EditorQuestion");
                return questionText.Value;
            }
        }

        private const string QUESTION_SETTINGS_CONTROLS = "chkRandomAnswer,chkPageBreak,chkResponseRequired,chkOtherAnswer,txtCharLimit";
        //public List<string> QuestionSettingValues
        //{
        //    get
        //    {
        //        List<string> lstSetiingValues = new List<string>();
        //        var questionSettingsControl = Utilities.FindControl<QuestionSettingsControl>(this, "QuestionSettingsControl1");
        //        foreach (var controlName in QUESTION_SETTINGS_CONTROLS.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
        //        {
        //            if (controlName.StartsWith("chk"))
        //            {
        //                var checkBoxControl = Utilities.FindControl<InsightoCheckBox>(questionSettingsControl, controlName);
        //                lstSetiingValues.Add(checkBoxControl.Checked ? "1" : "0");
        //            }
        //            else
        //            {
        //                var txtBoxControl = Utilities.FindControl<TextBox>(questionSettingsControl, controlName);
        //                lstSetiingValues.Add(txtBoxControl.Text);
        //            }
        //        }
        //        return lstSetiingValues;
        //    }
        //}

        public osm_answeroptions AnswerOptionValue { get; set; }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
      

        #endregion



        public string ImageType
        {
            get
            {
                return fuImageUpload.PostedFile.ContentType.Substring(fuImageUpload.PostedFile.ContentType.LastIndexOf('/') + 1);
            }
        }
        public string FileNAme
        {
            get
            {
                return fuImageUpload.PostedFile.FileName;
            }
        }

        public void Visibility()
        {
            fuImageUpload.Visible = false;
        }
        public int FileSize
        {
            get
            {
                return fuImageUpload.PostedFile.ContentLength;
            }
        }
        public bool HasFile
        {
            get
            {
                return fuImageUpload.HasFile;
            }
        }

        public override void Clear()
        {
            imgSurveyImage.Visible = false;
        }

        public string SetImagePath
        {
            set
            {
                imgSurveyImage.Visible = true;
                imgSurveyImage.ImageUrl = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CustCheckImagePath(object source, ServerValidateEventArgs args)
        {
            bool isFileCheck = fuImageUpload.HasFile;
            if (imgSurveyImage.ImageUrl == "" && isFileCheck == false)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }

        }
        public override List<osm_answeroptions> SelectedAnswerOptions
        {
            get
            {
               _lstResponseOtions = new List<osm_answeroptions>();

                return _lstResponseOtions;
            }
        }
    }
}