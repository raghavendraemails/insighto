﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using App_Code;
using CovalenseUtilities.Helpers;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using CovalenseUtilities.Services;
using System.Collections;
using App_Code.SurveyRespondentHelpers;

namespace Insighto.UserControls.QuestionControls
{
    public partial class AddOrEditQuestionsControl : UserControl
    {
        #region Private Properties
        private List<SurveyQuestionAndAnswerOptions> _surveyoptionsList;
        private List<osm_answeroptions> _lstFilledOptions;
        private List<osm_answeroptions> _lstFilledAnswerOptions;

        private SurveyPageBase SurveyPageBase
        {
            get
            {
                return Page as SurveyPageBase;
            }
        }
        #endregion

        #region Event handlers

        #region Load Question Handlers

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            switch (SurveyPageBase.ActionType)
            {
                case SurveyActionTypes.Add:
                    AddOrEditQuestion(e);
                    SetControls();
                    break;

                case SurveyActionTypes.Edit:
                    AddOrEditQuestion(e);
                    SetControls();
                    break;
                case SurveyActionTypes.AddMiddle:
                    AddOrEditQuestion(e);
                    SetControls();
                    break;
                case SurveyActionTypes.Remove:
                    break;
                case SurveyActionTypes.None:
                    dvNoQuesionId.Visible = true;
                    return;

                default:

                    return;
            }
        }

        /// <summary>
        /// Edits the question.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void AddOrEditQuestion(EventArgs e)
        {
            ////set/get default values
            //SetPermissions();
            //SetNotifications();

            //get basic survey questions and options list.
            GetCurrentQuestionsAndOptionsList();

            ////get response question for once per computer user.
            //_lstFilledOptions = SurveyPageBase.EditingQuestionId > 0 ? SurveyAnswerService.GetAnswerOptionsByQuestionsId(SurveyPageBase.EditingQuestionId) : new List<osm_answeroptions>();

            //filter the questions page by page    
            if (SurveyPageBase.EditingQuestionId > 0)
            {
                _surveyoptionsList = _surveyoptionsList.Where(qa => qa.Question.QUESTION_TYPE_ID == (int)SurveyPageBase.QuestionType
                    && qa.Question.QUESTION_ID == SurveyPageBase.EditingQuestionId).ToList();

                if (SurveyPageBase.ActionType == SurveyActionTypes.Edit && (SurveyPageBase.QuestionType == QuestionType.Matrix_SideBySide || SurveyPageBase.QuestionType == QuestionType.Matrix_MultipleSelect || SurveyPageBase.QuestionType == QuestionType.Matrix_SingleSelect))
                {
                    var matrixData = RespondentMatrixQuestions.GenerateMatrixData(_surveyoptionsList.FirstOrDefault());
                    var topHeaders = matrixData.Select(m => m.TopHeaderAttribute).Distinct().ToList();
                    var rowHeaders = matrixData.Select(m => m.RowAttribute).Distinct().ToList();
                    var colHeaders = matrixData.Select(m => m.ColumnAttribute).Distinct().ToList();

                    var matrixAnswerOptions = new List<SurveyQuestionAndAnswerOptions>();
                    matrixAnswerOptions.Add(GetQuestionAndAnswers(topHeaders));
                    matrixAnswerOptions.Add(GetQuestionAndAnswers(colHeaders));
                    matrixAnswerOptions.Add(GetQuestionAndAnswers(rowHeaders));
                    _surveyoptionsList = matrixAnswerOptions;
                }
            }
            else
                _surveyoptionsList = _surveyoptionsList.Where(qa => qa.Question.QUESTION_TYPE_ID == (int)SurveyPageBase.QuestionType).ToList();

            ViewSelectedQuestion();
        }

        #endregion

        #region Auxillary Methods

        private void ViewSelectedQuestion()
        {
            if (SurveyPageBase.QuestionType == QuestionType.Matrix_SideBySide || SurveyPageBase.QuestionType == QuestionType.Matrix_MultipleSelect || SurveyPageBase.QuestionType == QuestionType.Matrix_SingleSelect)
            {
                var surveyoptions = _surveyoptionsList.Where(so => so.Question != null).ToList();
                LoadUserControl<QuestionControlBase>(surveyoptions, PathHelper.GetQuestionControlPath(SurveyPageBase.QuestionType));
            }
            else
            {
                var surveyoption = _surveyoptionsList.Where(so => so.Question != null).FirstOrDefault();
                LoadUserControl<QuestionControlBase>(surveyoption, PathHelper.GetQuestionControlPath(SurveyPageBase.QuestionType));
            }
        }

        /// <summary>
        /// Gets the survey questions and options list.
        /// </summary>
        private void GetCurrentQuestionsAndOptionsList()
        {
            _surveyoptionsList = SurveyPageBase.SurveyQuesAndAnswerOptionsList;
            if (SurveyPageBase.ActionType == SurveyActionTypes.Edit || SurveyPageBase.ActionType == SurveyActionTypes.None)
                return;

            _surveyoptionsList = GetEmptyAnswerOptions();
        }

        /// <summary>
        /// Gets the empty answer options.
        /// </summary>
        /// <returns></returns>
        private List<SurveyQuestionAndAnswerOptions> GetEmptyAnswerOptions()
        {
            _surveyoptionsList = new List<SurveyQuestionAndAnswerOptions>();
            var questionAndOptions = new SurveyQuestionAndAnswerOptions();
            var question = new osm_surveyquestion();

            question.QUESTION_TYPE_ID = (int)SurveyPageBase.QuestionType;
            questionAndOptions.Question = question;
            questionAndOptions.AnswerOption = new List<osm_answeroptions>();

            if (SurveyPageBase.QuestionType == QuestionType.Matrix_SideBySide || SurveyPageBase.QuestionType == QuestionType.Matrix_MultipleSelect || SurveyPageBase.QuestionType == QuestionType.Matrix_SingleSelect)
            {
                for (int i = 0; i < 3; i++)
                {
                    questionAndOptions = new SurveyQuestionAndAnswerOptions();
                    question = new osm_surveyquestion();
                    osm_answeroptions option = new osm_answeroptions();

                    question.QUESTION_TYPE_ID = (int)SurveyPageBase.QuestionType;
                    questionAndOptions.Question = question;
                    questionAndOptions.AnswerOption = new List<osm_answeroptions>();

                    GetEmptyAnswerOptions(questionAndOptions);
                    _surveyoptionsList.Add(questionAndOptions);
                }
                return _surveyoptionsList;
            }
            else if (SurveyPageBase.QuestionType == QuestionType.ContactInformation)
            {
                //read contactinfo from xml.
                return _surveyoptionsList;
            }

            GetEmptyAnswerOptions(questionAndOptions);
            _surveyoptionsList.Add(questionAndOptions);
            return _surveyoptionsList;
        }

        /// <summary>
        /// Gets the empty answer options.
        /// </summary>
        /// <param name="questionAndOptions">The question and options.</param>
        private static void GetEmptyAnswerOptions(SurveyQuestionAndAnswerOptions questionAndOptions)
        {
            for (int i = 0; i < 3; i++)
            {
                osm_answeroptions option = new osm_answeroptions();
                option.AnswerOptionUniqueId = string.Format("an_{0}", i + 1);
                option.SequenceId = Convert.ToString(i + 1);
                questionAndOptions.AnswerOption.Add(option);
            }
        }

        /// <summary>
        /// Gets the question and answers.
        /// </summary>
        /// <param name="headers">The headers.</param>
        /// <returns></returns>
        private SurveyQuestionAndAnswerOptions GetQuestionAndAnswers(List<string> headers)
        {
            var optionsList = new List<osm_answeroptions>();
            var questionAndAnswer = new SurveyQuestionAndAnswerOptions();
            osm_answeroptions option = null;
            foreach (var headerValue in headers)
            {
                questionAndAnswer.Question = _surveyoptionsList.FirstOrDefault().Question;
                option = new osm_answeroptions();
                option.ANSWER_OPTIONS = headerValue;
                option.AnswerOptionUniqueId = headerValue;
                option.SequenceId = headerValue;
                optionsList.Add(option);
            }
            questionAndAnswer.AnswerOption = optionsList;
            return questionAndAnswer;
        }

        private void SetNotifications()
        {
            throw new NotImplementedException();
        }

        private void SetPermissions()
        {
            throw new NotImplementedException();
        }

        private void LoadUserControl<T>(SurveyQuestionAndAnswerOptions surveyoption, string path) where T : QuestionControlBase
        {
            //Load Control
            var control = Page.LoadControl(path) as T;
            if (control != null)
                control.QuestionAndAnswerOptions = surveyoption;
            var optionsList = _lstFilledOptions != null ? _lstFilledOptions.Where(ao => ao.QUESTION_ID == surveyoption.Question.QUESTION_ID).ToList() : null;
            if (control == null) return;

            pnlQuestions.Controls.Add(control);
        }

        private void LoadUserControl<T>(List<SurveyQuestionAndAnswerOptions> surveyoptions, string path) where T : QuestionControlBase
        {
            //Load Control
            var control = Page.LoadControl(path) as T;
            if (control != null)
                control.MatrixQuestionAndAnswerOptions = surveyoptions;
            //var optionsList = _lstFilledOptions != null ? _lstFilledOptions.Where(ao => ao.QUESTION_ID == surveyoptions.Select(so => so.Question).FirstOrDefault).ToList() : null;
            if (control == null) return;

            pnlQuestions.Controls.Add(control);
        }



        #endregion

        #endregion

        /// <summary>
        /// Handles the Click event of the btnCreate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            var currentQuestionControl = CommonHelper.FindControlRecursive<QuestionControlBase>(pnlQuestions, 1).FirstOrDefault();
            var inputValues = currentQuestionControl.SelectedSurveyQuestionAndAnswerOptions;
            if (inputValues.Count > 0)
            {
                //var allQuestions = SurveyPageBase.SurveyQuesAndAnswerOptionsList;
                //allQuestions.Insert(SurveyPageBase.QuestionSequenceId + 1, inputValues.FirstOrDefault());
                SaveQuestion(inputValues);
            }

        }

        /// <summary>
        /// Saves the question.
        /// </summary>
        /// <param name="inputValues">The input values.</param>
        private void SaveQuestion(List<SurveyQuestionAndAnswerOptions> inputValues)
        {

            ServiceFactory<QuestionService>.Instance.SaveQuestion(inputValues, SurveyPageBase.ActionType, SurveyPageBase.QuestionType);
        }

        public bool CheckForBlockedWords(string questionStr)
        {
            bool IsBlockedWordExists = false;
            var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();
            var contentWords = questionStr.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var existedBlockedWords = blockedWords.Where(b => contentWords.Where(c => c.Trim() == b.BLOCKEDWORD_NAME).Any());
            IsBlockedWordExists = existedBlockedWords.Any();
            return IsBlockedWordExists;
        }

        private void SetControls()
        {
            btnCreate.Visible = true;
            btnClear.Visible = true;
            dvNoQuesionId.Visible = false;
        }
    }
}