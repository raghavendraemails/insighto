﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;

namespace Insighto.UserControls.QuestionControls
{
    public partial class ContactInfoQuestionControl : QuestionControlBase
    {
        #region Private properties

        private string _textControlPrefix = string.Empty;
        private List<TextBox> _lstControls;
        private List<osm_answeroptions> _lstResponseOtions;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions QuestionAndAnswerOptions { get; set; }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_answeroptions> SelectedAnswerOptions
        {
            get
            {
                return QuestionAndAnswerOptions.AnswerOption;
            }
        }

        private osm_answeroptions GetInstance(int p)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            if (IsPostBack)
            {
                this.QuestionAndAnswerOptions.AnswerOption = Cache[this.ID] as List<osm_answeroptions>;
            }
            Initialise();
            Display();
            base.OnInit(e);
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        public override void Initialise()
        {

        }

        /// <summary>
        /// Displays this instance.
        /// </summary>
        public override void Display()
        {
            if (QuestionAndAnswerOptions == null || QuestionAndAnswerOptions.Question == null)
                return;

            gvAnswerOptions.DataSource = QuestionAndAnswerOptions.AnswerOption;
            gvAnswerOptions.DataBind();
        }

        #endregion.

        /// <summary>
        /// Handles the RowDataBound event of the gvAnswerOptions control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void gvAnswerOptions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var answerIds = gvAnswerOptions.DataKeys[e.Row.RowIndex].Values;
                var answerOption = answerIds[1] != null ? QuestionAndAnswerOptions.AnswerOption.FirstOrDefault(ao => ao.ANSWER_ID == (int)answerIds[0] && Convert.ToString(ao.AnswerOptionUniqueId) == Convert.ToString(answerIds[1]))
                    : QuestionAndAnswerOptions.AnswerOption.FirstOrDefault(ao => ao.ANSWER_ID == (int)answerIds[0]);
                var control = Utilities.FindControl<ContactInfoTextBoxControl>(e.Row, "ContactInfoTextBoxControl1");
                control.AnswerOptionValue = answerOption;
                control.QuestionAndAnswerOptions = QuestionAndAnswerOptions;
                control.Display();
            }
        }    

        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                Cache[this.ID] = QuestionAndAnswerOptions.AnswerOption.ToList();
            }
            int countOptions = 0;
            foreach (GridViewRow gvrow in gvAnswerOptions.Rows)
            {
                if (gvrow.RowType != DataControlRowType.DataRow)
                    continue;

                var answerIds = gvAnswerOptions.DataKeys[gvrow.RowIndex].Values;
                var answerOptionControl = Utilities.FindControl<ContactInfoTextBoxControl>(gvrow, "AContactInfoTextBoxControl1"); //;
                var txtAnswerOption = Utilities.FindControl<TextBox>(answerOptionControl, "txtAnsOption");

                //get and re-assign the post back values to list.
                QuestionAndAnswerOptions.AnswerOption[countOptions].ANSWER_ID = Convert.ToInt32(answerIds[0]);
                QuestionAndAnswerOptions.AnswerOption[countOptions].AnswerOptionUniqueId = Convert.ToString(answerIds[1]);
                QuestionAndAnswerOptions.AnswerOption[countOptions].SequenceId = Convert.ToString(answerIds[2]);
                QuestionAndAnswerOptions.AnswerOption[countOptions].ANSWER_OPTIONS = txtAnswerOption.Text;
                countOptions++;
            }
            base.OnLoad(e);
        }       

        /// <summary>
        /// Sets the sequence order.
        /// </summary>
        private void SetSequenceOrder()
        {
            int sequenceCtr = 1;
            string uniqueId = string.Format("an_{0}", sequenceCtr);
            foreach (var item in QuestionAndAnswerOptions.AnswerOption)
            {
                if (SurveyPageBase.QuestionType == QuestionType.Matrix_SideBySide || SurveyPageBase.QuestionType == QuestionType.Matrix_MultipleSelect || SurveyPageBase.QuestionType == QuestionType.Matrix_SingleSelect)
                    uniqueId = !string.IsNullOrEmpty(item.AnswerOptionUniqueId) ? item.AnswerOptionUniqueId : uniqueId;

                item.AnswerOptionUniqueId = uniqueId;
                item.SequenceId = sequenceCtr.ToString();
                sequenceCtr++;
            }

            QuestionAndAnswerOptions.AnswerOption.OrderBy(ao => ao.SequenceId);
        }
    }
}