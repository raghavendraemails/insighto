﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MatrixQuestionControl.ascx.cs"
    Inherits="Insighto.UserControls.QuestionControls.MatrixQuestionControl" %>
<%@ Register Src="QuestionControl.ascx" TagName="QuestionControl" TagPrefix="Insighto" %>
<%@ Register Src="AnswerOptionsControl.ascx" TagName="AnswerOptionsControl" TagPrefix="Insighto" %>
<%@ Register Src="QuestionSettingsControl.ascx" TagName="QuestionSettingsControl"
    TagPrefix="Insighto" %>
<Insighto:QuestionControl ID="matrixQuestionControl" runat="server" />
<br />
<Insighto:AnswerOptionsControl ID="topHeaderAnswerOptionsControl" runat="server"
    Visible="false" />
<br />
<Insighto:AnswerOptionsControl ID="columnAnswerOptionsControl" runat="server" />
<br />
<Insighto:AnswerOptionsControl ID="rowAnswerOptionsControl" runat="server" />
<br />
<Insighto:QuestionSettingsControl ID="QuestionSettingsControl1" runat="server" />
<br />
