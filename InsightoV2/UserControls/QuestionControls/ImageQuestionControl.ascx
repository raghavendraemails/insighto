﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImageQuestionControl.ascx.cs" Inherits="UserControls.QuestionControls.ImageQuestionControl" %>
<%@ Register Src="QuestionControl.ascx" TagName="QuestionControl" TagPrefix="Insighto" %>
<Insighto:QuestionControl ID="QuestionControl1" runat="server" />
<br />
<p class="purpleColor"><b>Upload your image.</b></p>
<div>
<asp:FileUpload ID="fuImageUpload" runat="server"  /><br />
<asp:Label ID ="lbluploadImg" CssClass="note"  runat ="server" Text="Please upload image of extension .jpg, .jpeg, .bmp, .png. File size should be less than "></asp:Label>
<%--<asp:RequiredFieldValidator SetFocusOnError="true" id = "reqFuImageUpload" runat = "server" ControlToValidate = "fuImageUpload" Display = "Dynamic" CssClass = "lblRequired"
Enabled = "true"  ErrorMessage = "Please select an image"></asp:RequiredFieldValidator>--%> 

</div>
<div>

<asp:RegularExpressionValidator SetFocusOnError="true"  ID="regFuImageUpload" runat = "server" ValidationExpression = "([0-9a-zA-Z -:\\-_!@$%^&*()])+(.jpg|.JPG|.jpeg|.JPEG|.bmp|.BMP|.png|.PNG)$"
ErrorMessage = "Please upload image of extension .jpg, .jpeg, .bmp, .png." ControlToValidate = "fuImageUpload" CssClass = "lblRequired"></asp:RegularExpressionValidator>
</div>

<p style="padding-top:10px; font-weight:700;" class="purpleColor">Disclaimer:</p>
<div class="disclaimer">
<b>Important:</b> The Image /Video you are uploading should be of your own or you should have the copyrights or be entitled to use the same for public/private consumption.   Insighto will not hold any responsibility or liability for the content uploaded. Any content that is abhorrent or obscene or offending others or violating statutory laws  would  be deleted without any prior notice. And the same would be placed before the appropriate statutory authorities.
</div>

<div>
<asp:Image ID="imgSurveyImage" runat = "server" ImageUrl = "" Visible = "false"/>
<asp:CustomValidator SetFocusOnError="true"  ID="custCheckImage" runat = "server" CssClass = "lblRequired" OnServerValidate = "CustCheckImagePath" ErrorMessage = "Please select an image"></asp:CustomValidator>
</div>
