﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using System.Text;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;

namespace Insighto.UserControls.QuestionControls
{
    public partial class MatrixQuestionControl : QuestionControlBase
    {
        #region Private properties

        private string _textControlPrefix = string.Empty;
        private List<TextBox> _lstControls;
        private List<osm_answeroptions> _lstResponseOtions;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions QuestionAndAnswerOptions
        {
            get
            {
                return base.QuestionAndAnswerOptions;
            }
            set
            {
                base.QuestionAndAnswerOptions = value;
                matrixQuestionControl.QuestionAndAnswerOptions = value;
                topHeaderAnswerOptionsControl.QuestionAndAnswerOptions = value;
                columnAnswerOptionsControl.QuestionAndAnswerOptions = value;
                rowAnswerOptionsControl.QuestionAndAnswerOptions = value;
                QuestionSettingsControl1.QuestionAndAnswerOptions = value;
            }
        }

        public override List<SurveyQuestionAndAnswerOptions> SelectedSurveyQuestionAndAnswerOptions
        {
            get
            {
                var matrixQuestionOptions = GetSurveyQuestionAndAnswers();
                var matrixOptions = new SurveyQuestionAndAnswerOptions();
                matrixOptions.Question = matrixQuestionOptions.FirstOrDefault().Question;
                matrixOptions.AnswerOption = GetMatrixAnswers(matrixQuestionOptions);
                matrixQuestionOptions = null;

                return new List<SurveyQuestionAndAnswerOptions> { matrixOptions };
            }
            set
            {
                base.SelectedSurveyQuestionAndAnswerOptions = value;
            }
        }

        /// <summary>
        /// Gets the matrix answers.
        /// </summary>
        /// <param name="matrixQuestionOptions">The matrix question options.</param>
        /// <returns></returns>
        private List<osm_answeroptions> GetMatrixAnswers(List<SurveyQuestionAndAnswerOptions> matrixQuestionOptions)
        {
            //for m X n matrix
            var joinedAnswerOptions = new List<osm_answeroptions>();
            if (matrixQuestionOptions.Count == 2)
            {
                foreach (var column in matrixQuestionOptions[0].AnswerOption)
                {
                    foreach (var row in matrixQuestionOptions[1].AnswerOption)
                    {
                        joinedAnswerOptions.Add(new osm_answeroptions { ANSWER_OPTIONS = string.Format("{0}$--${1}", column.ANSWER_OPTIONS, row.ANSWER_OPTIONS) });
                    }
                }
            }

            //for l X m X n matrix
            if (matrixQuestionOptions.Count == 3)
            {
                foreach (var top in matrixQuestionOptions[0].AnswerOption)
                {
                    foreach (var column in matrixQuestionOptions[1].AnswerOption)
                    {
                        foreach (var row in matrixQuestionOptions[2].AnswerOption)
                        {
                            joinedAnswerOptions.Add(new osm_answeroptions { ANSWER_OPTIONS = string.Format("{0}$--${1}$--${2}", top.ANSWER_OPTIONS, column.ANSWER_OPTIONS, row.ANSWER_OPTIONS) });
                        }
                    }
                }
            }
            return joinedAnswerOptions;
        }

        /// <summary>
        /// Gets the survey question and answers.
        /// </summary>
        /// <returns></returns>
        private List<SurveyQuestionAndAnswerOptions> GetSurveyQuestionAndAnswers()
        {
            var columnsAndRowsOptionsList = new List<SurveyQuestionAndAnswerOptions>();
            var questionAndAnswers = new SurveyQuestionAndAnswerOptions();
            var topAnswerOptions = Utilities.FindControl<AnswerOptionsControl>(this, "topHeaderAnswerOptionsControl");
            var columnAnswerOptions = Utilities.FindControl<AnswerOptionsControl>(this, "columnAnswerOptionsControl");
            var rowAnswerOptions = Utilities.FindControl<AnswerOptionsControl>(this, "rowAnswerOptionsControl");
            var questionControl = Utilities.FindControl<QuestionControl>(this, "matrixQuestionControl");

            // if l X m X n matrix.
            //if (QuestionType == Business.Enumerations.QuestionType.Matrix_SideBySide)
            {
                questionAndAnswers.Question = questionControl.SelectedQuestion;
                questionAndAnswers.AnswerOption = topAnswerOptions.SelectedAnswerOptions;
                columnsAndRowsOptionsList.Add(questionAndAnswers);
            }

            //if m X n matrix or l X m X n matrix.
            questionAndAnswers = new SurveyQuestionAndAnswerOptions();
            questionAndAnswers.Question = questionControl.SelectedQuestion;
            questionAndAnswers.AnswerOption = columnAnswerOptions.SelectedAnswerOptions;
            columnsAndRowsOptionsList.Add(questionAndAnswers);

            questionAndAnswers = new SurveyQuestionAndAnswerOptions();
            questionAndAnswers.Question = questionControl.SelectedQuestion;
            questionAndAnswers.AnswerOption = rowAnswerOptions.SelectedAnswerOptions;
            columnsAndRowsOptionsList.Add(questionAndAnswers);

            return columnsAndRowsOptionsList;
        }

        public osm_answeroptions AnswerOptionValue { get; set; }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        ///// <summary>
        ///// Gets the selected answer options.
        ///// </summary>
        //public override List<osm_answeroptions> SelectedAnswerOptions
        //{
        //    get
        //    {
        //        _lstResponseOtions = new List<osm_answeroptions>();
        //        var answerControl = Utilities.FindControl<AnswerOptionsControl>(this, "AnswerOptionsControl1");
        //        var answerControl1= Utilities.FindControl<AnswerOptionsControl>(this, "AnswerOptionsControl2");
        //        if (answerControl != null)
        //        {
        //            return answerControl.SelectedAnswerOptions;
        //        }
        //        if (answerControl1 != null)
        //        {
        //            return answerControl1.SelectedAnswerOptions;  
        //        }
        //        return _lstResponseOtions;
        //    }
        //}

        #endregion


        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            if (QuestionType == QuestionType.Matrix_SideBySide)
                topHeaderAnswerOptionsControl.Visible = true;

            Display();
            base.OnInit(e);
        }

        public override void Initialise()
        {
            base.Initialise();
        }

        /// <summary>
        /// Displays this instance.
        /// </summary>
        public override void Display()
        {
            int controlCtr = 0;
            List<AnswerOptionsControl> answerControls = CommonHelper.FindControlRecursive<AnswerOptionsControl>(this, 3);
            foreach (var control in answerControls)
            {
                if (IsPostBack)
                    MatrixQuestionAndAnswerOptions[controlCtr].AnswerOption = Cache[control.ID] as List<osm_answeroptions>;

                control.QuestionAndAnswerOptions = MatrixQuestionAndAnswerOptions[controlCtr];
                control.Display();
                //if (control.Visible == true)
                    controlCtr++;
            }

        }

        #endregion
    }
}