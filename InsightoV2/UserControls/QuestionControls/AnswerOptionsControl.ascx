﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AnswerOptionsControl.ascx.cs"
    Inherits="Insighto.UserControls.QuestionControls.AnswerOptionsControl" %>
<%@ Register Src="AnswerOptionTextBoxControl.ascx" TagName="AnswerOptionTextBoxControl"
    TagPrefix="Insighto" %>
<%@ Reference VirtualPath="~/UserControls/QuestionControls/AnswerOptionTextBoxControl.ascx" %>
<asp:GridView ID="gvAnswerOptions" runat="server" AutoGenerateColumns="false" DataKeyNames="ANSWER_ID, AnswerOptionUniqueId, SequenceId"
    GridLines="None" OnRowDataBound="gvAnswerOptions_RowDataBound" OnRowCommand="gvAnswerOptions_RowCommand">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <Insighto:AnswerOptionTextBoxControl ID="AnswerOptionTextBoxControl1" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
