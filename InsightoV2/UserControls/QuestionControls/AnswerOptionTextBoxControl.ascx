﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AnswerOptionTextBoxControl.ascx.cs"
    Inherits="Insighto.UserControls.QuestionControls.AnswerOptionTextBoxControl" %>
<div class="itempanel">
    <table cellpadding="0" cellspacing="0" border="0">
        <tr class="txtRow">
            <td>
                <asp:TextBox ID="txtAnsOption" runat="server" CssClass="textBoxMedium txtAns" MaxLength="150" />
            </td>
            <td class="space">
                &nbsp;
            </td>
            <td>
                <asp:Button ID="btnRemove" runat="server" Text='' CssClass="RemoveIcon" CausesValidation="false"
                    CommandName='Remove' ToolTip="Delete answer option" />
            </td>
            <td class="space">
                &nbsp;
            </td>
            <td>
                <asp:Button ID="btnAddAnother" runat="server" CssClass="addedIcon" Text='' CausesValidation="false"
                    CommandName='Add' ToolTip="Add answer option" />
            </td>
        </tr>
        <tr class="lblRow">
            <td colspan="5">
            <%--    <asp:Label ID="lblMsg" runat="server" Text="Please enter answer option." CssClass="lblRequired"
                    Style="display: none"></asp:Label>--%>
                <asp:RequiredFieldValidator ID="rfvAnswerOption" runat="server" ErrorMessage="Please enter answer option."
                    CssClass="lblRequired" Display="Dynamic" ControlToValidate="txtAnsOption" />
            </td>
        </tr>
    </table>
</div>
