﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Data;
using Insighto.Business.ValueObjects;

namespace Insighto.UserControls.QuestionControls
{
    public partial class ContactInfoTextBoxControl : QuestionControlBase
    {
        #region Private properties

        private string _textControlPrefix = string.Empty;
        private List<TextBox> _lstControls;
        private List<osm_answeroptions> _lstResponseOtions;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions QuestionAndAnswerOptions { get; set; }

        public osm_answeroptions AnswerOptionValue { get; set; }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_answeroptions> SelectedAnswerOptions
        {
            get
            {
                _lstResponseOtions = new List<osm_answeroptions>();
                if(chkInclude.Checked)
                {
                if (string.IsNullOrEmpty(txtAnsOption.Text))
                    return _lstResponseOtions;

                var option = GetInstance(0);
                option.ANSWER_OPTIONS = txtAnsOption.Text;
                option.ANSWER_ID = AnswerOptionValue.ANSWER_ID;
                option.QUESTION_ID = AnswerOptionValue.QUESTION_ID;
                //option.RESPONSE_REQUIRED = AnswerOptionValue.RESPONSE_REQUIRED;
                option.DEMOGRAPIC_BLOCKID = Utilities.ToInt(AnswerOptionValue.SequenceId);
                _lstResponseOtions.Add(option);
                }
                return _lstResponseOtions;
            }
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            Initialise();
            Display();
            base.OnInit(e);
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        public override void Initialise()
        {

        }

        /// <summary>
        /// Displays this instance.
        /// </summary>
        public override void Display()
        {
            if (AnswerOptionValue == null)
                return;

            txtAnsOption.Text = AnswerOptionValue.ANSWER_OPTIONS;
            //var response = Utilities.GetQuestioAnswerOption(EditAnswerOptions, AnswerOptionValue.ANSWER_ID);
            //txtAnsOption.Text = response != null ? response.ANSWER_OPTIONS : string.Empty;
        }

        #endregion.

        #region Auxillary methods

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="isquestion">if set to <c>true</c> [isquestion].</param>
        /// <returns></returns>
        private static int GetValue(string source, bool isquestion)
        {
            if (!string.IsNullOrEmpty(source))
            {
                var ids = source.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                return Convert.ToInt32(isquestion ? ids[1] : ids[2]);
            }

            return 0;
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="deleted">The deleted.</param>
        /// <returns></returns>
        private osm_answeroptions GetInstance(int deleted)
        {
            return new osm_answeroptions { DELETED = deleted, CREATED_ON = AnswerOptionValue.ANSWER_ID > 0 ? AnswerOptionValue.CREATED_ON : DateTime.UtcNow, LAST_MODIFIED_ON = DateTime.UtcNow };
        }

        #endregion
    }
}