﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuestionSettingsControl.ascx.cs"
    Inherits="Insighto.UserControls.QuestionControls.QuestionSettingsControl" %>
<div id="dvrdNoColumns" runat="server" visible="true">
    <p>
        Number of Columns:
    </p>
    <asp:RadioButtonList ID="rdNoColumns" runat="server" RepeatDirection="Horizontal"
        Enabled="true" />
</div>
<div id="dvCharLimit" runat="server" visible="true">
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                Char Limit
            </td>
            <td class="space">
                &nbsp;
            </td>
            <td class="p_rgt">
                <asp:TextBox ID="txtCharLimit" runat="server" CssClass="form-field" />
            </td>
        </tr>
    </table>
</div>
<div id="dvchkRandomAnswer" runat="server" visible="true">
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <Insighto:InsightoCheckBox ID="chkRandomAnswer" ReadOnly="false" runat="server" CssClass="form-field"
                    Text="Randomize answer options" />
            </td>
            <td class="space">
                &nbsp;
            </td>
            <td class="p_rgt">
                <a href="#" class="helpLink1" onclick="javascript: window.open('help/2_2_13.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">
                    &nbsp;</a>
            </td>
        </tr>
    </table>
</div>
<div id="dvchkPageBreak" runat="server" visible="true">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <Insighto:InsightoCheckBox ID="chkPageBreak" CssClass="form-field" runat="server"
                    Text="Page break" />
            </td>
            <td class="space">
                &nbsp;
            </td>
            <td class="p_rgt">
                <a href="#" class="helpLink1" title="Help" onclick="javascript: window.open('help/2_2_14.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">
                    &nbsp;</a>
            </td>
        </tr>
    </table>
</div>
<div id="dvchkResponseRequired" runat="server" visible="true">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <Insighto:InsightoCheckBox ID="chkResponseRequired" ClientIDMode="Static" CssClass="form-field chkResponseRequired"
                    runat="server" Text="Response required" />
            </td>
            <td class="space">
                &nbsp;
            </td>
            <td>
                <a href="#" class="helpLink1" title="Help" onclick="javascript: window.open('help/2_2_11.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">
                    &nbsp;</a>
            </td>
        </tr>
    </table>
</div>
<div class="clear">
</div>
<p class="defaultHeight">
</p>
<Insighto:InsightoCheckBox ID="chkOtherAnswer" runat="server" CssClass="form-field"
    Text="Check the box to add 'Other' as an additional answer option with its own text field allowing the respondent to &nbsp;key in a different answer" />&nbsp
<span id="divCheckOther" runat="server"><a href="#" class="helpLink1" title="help"
    onclick="javascript: window.open('help/2_2_12.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">
</a></span>