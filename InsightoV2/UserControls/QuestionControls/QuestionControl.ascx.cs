﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using Insighto.Business.Enumerations;


namespace Insighto.UserControls.QuestionControls
{
    public partial class QuestionControl : QuestionControlBase
    {
        [Bindable(true)]
        [DefaultValue(false)]
        public bool IsIntroductionPage { get; set; }

        public override void Clear()
        {
            EditorQuestion.Value = string.Empty;
        }

        public override osm_surveyquestion SelectedQuestion
        {
            get
            {
                QuestionSettingsControl questionSettingsControl = CommonHelper.FindControlRecursive<QuestionSettingsControl>(Page, 1).FirstOrDefault();
                var settings = questionSettingsControl.QuestionSettings;
                osm_surveyquestion surveyQuestion = new osm_surveyquestion
                {
                    QUESTION_TYPE_ID = (int)SurveyPageBase.QuestionType,
                    QUSETION_LABEL = GetQuestionText(EditorQuestion.Value),
                    RANDOMIZE_ANSWERS = Utilities.ToInt(settings[0]),
                    PAGE_BREAK = Utilities.ToInt(settings[2]),
                    RESPONSE_REQUIRED = Utilities.ToInt(settings[2]),
                    OTHER_ANS = Utilities.ToInt(settings[3]),
                    CHAR_LIMIT = Utilities.ToInt(settings[4]),
                    SURVEY_ID = SurveyID,
                    QUESTION_SEQ = SurveyPageBase.ActionType == SurveyActionTypes.Add || SurveyPageBase.ActionType == SurveyActionTypes.AddMiddle ? SurveyPageBase.QuestionSequenceId + 1 : SurveyPageBase.QuestionSequenceId,
                    QUESTION_ID = SurveyPageBase.EditingQuestionId
                };

                return surveyQuestion;
            }
            set
            {
                base.SelectedQuestion = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            Display();
        }

        public void Display()
        {
            if (QuestionAndAnswerOptions == null || Question == null)
                return;

            EditorQuestion.Value = Question.QUSETION_LABEL;
        }

        protected void custValidateText(object source, ServerValidateEventArgs args)
        {
            //string textLength = value.Length >40;
            if (QuestionAndAnswerOptions == null || Question == null || QuestionAndAnswerOptions.AnswerOption == null)
                return;

            if (EditorQuestion.Value.Length > 4000)
                args.IsValid = false;
            else
                args.IsValid = true;// field is empty         
        }

        protected string GetQuestionText(string questionText)
        {
            string questionStr = string.Empty;
            string browserType = Request.Browser.Browser;
            if (browserType == "Firefox")
            {
                questionStr = BuildQuestions.HTMLTextSettings(questionText);
            }
            while (questionStr.Contains("<p>&nbsp;</p>\r\n"))
                questionStr = questionStr.Replace("<p>&nbsp;</p>\r\n", " ");
            while (questionStr.Contains("<p>&nbsp;</p>"))
                questionStr = questionStr.Replace("<p>&nbsp;</p>", " ");
            System.Text.RegularExpressions.Regex.Replace(questionStr, "<p>&nbsp;</p>", "");
            if (questionStr == "")
                return questionText;
            else
                return questionStr;
        }

        protected override void OnPreRender(EventArgs e)
        {
            EditorQuestion.Visible = true;
            if (IsIntroductionPage)
            {
              //  reqIntro.Visible = true;

            }
            else
            {
                rqEditorQuestion.Visible = true;
            }

            if (SurveyID > 0)
            {
                var surveySetting = ServiceFactory.GetService<SurveySetting>();
                var survey = surveySetting.GetSurveySettingsBySurveyId(SurveyID);
                string fontSize = survey[0].SURVEY_FONT_SIZE;
                hdnEditorQuesParams.Value = survey[0].SURVEY_FONT_TYPE + "_" + BuildQuestions.GetFontSizeValue(fontSize) + "_" + BuildQuestions.GetFontColor(survey[0].SURVEY_FONT_COLOR);
            }
        }
    }
}