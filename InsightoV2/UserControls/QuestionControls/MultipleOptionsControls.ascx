﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultipleOptionsControls.ascx.cs"
    Inherits="Insighto.UserControls.QuestionControls.MultipleOptionsControls" %>
<%@ Register Src="QuestionControl.ascx" TagName="QuestionControl" TagPrefix="Insighto" %>
<%@ Register Src="AnswerOptionsControl.ascx" TagName="AnswerOptionsControl" TagPrefix="Insighto" %>
<%@ Register Src="QuestionSettingsControl.ascx" TagName="QuestionSettingsControl"
    TagPrefix="Insighto" %>
<Insighto:QuestionControl ID="QuestionControl1" runat="server" />
<br />
<Insighto:AnswerOptionsControl ID="AnswerOptionsControl1" runat="server" />
<br />
<Insighto:QuestionSettingsControl ID="QuestionSettingsControl1" runat="server" />
<br />

