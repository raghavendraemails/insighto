﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddOrEditQuestionsControl.ascx.cs"
    Inherits="Insighto.UserControls.QuestionControls.AddOrEditQuestionsControl" %>
    <div>
     <div id="dvNoQuesionId" runat="server" class="informationControl" visible="false">
                <asp:Label ID="lblNoQuestion" runat="server" Text= "<ul class='nolisticon'><li>Please select a question type from the list given on the left menu.</li><li>An example of the selected question type can be seen on the bottom left of the screen.</li></ul>" Visible="False"> 
                 </asp:Label>
                <div class="clear">
                </div>
            </div>
           
<div><asp:Panel ID="pnlQuestions" runat="server" 
        meta:resourcekey="pnlQuestionsResource1">
</asp:Panel> </div> 
 <div class="bottomButtonPanel" style="margin-top:20px;">
                            <asp:Button ID="btnCreate" runat="server" Text="Save" Visible="False"  CssClass="dynamicButton btnCreate"
                                OnClientClick="tinyMCE.triggerSave(false,true);" 
                                onclick="btnCreate_Click" meta:resourcekey="btnCreateResource1"  />&nbsp;
                            <asp:Button ID="btnClear" runat="server" Text="Clear"  Visible="False" CssClass="dynamicButton"
                                CausesValidation="False" meta:resourcekey="btnClearResource1" />
                        </div>
</div> 
