﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using Insighto.UserControls.QuestionControls;
using System.Web.UI.HtmlControls;
using Insighto.Web.UI.Controls;

namespace Insighto.UserControls.QuestionControls
{
    public partial class MultipleOptionsControls : QuestionControlBase
    {
        #region Private properties

        private string _textControlPrefix = string.Empty;
        private List<TextBox> _lstControls;
        private List<osm_answeroptions> _lstResponseOtions;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions QuestionAndAnswerOptions
        {
            get
            {
                return base.QuestionAndAnswerOptions;
            }
            set
            {
                base.QuestionAndAnswerOptions = value;
                QuestionControl1.QuestionAndAnswerOptions = value;
                AnswerOptionsControl1.QuestionAndAnswerOptions = value;
                QuestionSettingsControl1.QuestionAndAnswerOptions = value;
            }
        }

        public override List<SurveyQuestionAndAnswerOptions> SelectedSurveyQuestionAndAnswerOptions
        {
            get
            {
                return GetSurveyQuestionAndAnswers();
            }
            set
            {
                base.SelectedSurveyQuestionAndAnswerOptions = value;
            }
        }

        /// <summary>
        /// Gets the survey question and answers.
        /// </summary>
        /// <returns></returns>
        private List<SurveyQuestionAndAnswerOptions> GetSurveyQuestionAndAnswers()
        {
            var surveyOptionsList = new List<SurveyQuestionAndAnswerOptions>();
            var questionAndAnswers = new SurveyQuestionAndAnswerOptions();
             questionAndAnswers.Question = Utilities.FindControl<QuestionControl>(this, "QuestionControl1").SelectedQuestion;
            questionAndAnswers.AnswerOption = SelectedAnswerOptions;
            surveyOptionsList.Add(questionAndAnswers);
            return surveyOptionsList;
        }

        public osm_answeroptions AnswerOptionValue { get; set; }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_answeroptions> SelectedAnswerOptions
        {
            get
            {
                _lstResponseOtions = new List<osm_answeroptions>();
                if ((int)SurveyPageBase.QuestionType != 4)
                {

                    var answerControl = Utilities.FindControl<AnswerOptionsControl>(this, "AnswerOptionsControl1");
                    if (answerControl != null)
                    {
                        return answerControl.SelectedAnswerOptions;
                    }
                }
                else
                {
                    osm_answeroptions ansOptions = new osm_answeroptions();
                    ansOptions.ANSWER_OPTIONS = "Yes";
                    _lstResponseOtions.Add(ansOptions);
                    ansOptions.ANSWER_OPTIONS = "No";
                    _lstResponseOtions.Add(ansOptions);  
                }
                return _lstResponseOtions;
            }
        }


        #endregion
        //private const string QUESTION_SETTINGS_CONTROLS = "chkRandomAnswer,chkPageBreak,chkResponseRequired,chkOtherAnswer,txtCharLimit";
        //public List<string> QuestionSettingValues
        //{
        //    get
        //    {
        //        List<string> lstSetiingValues = new List<string>();
        //        var questionSettingsControl = Utilities.FindControl<QuestionSettingsControl>(this, "QuestionSettingsControl1");
        //        foreach (var controlName in QUESTION_SETTINGS_CONTROLS.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
        //        {
        //            if (controlName.StartsWith("chk"))
        //            {
        //                var checkBoxControl = Utilities.FindControl<InsightoCheckBox>(questionSettingsControl, controlName);
        //                lstSetiingValues.Add(checkBoxControl.Checked ? "1" : "0");
        //            }
        //            else
        //            {
        //                var txtBoxControl = Utilities.FindControl<TextBox>(questionSettingsControl, controlName);
        //                lstSetiingValues.Add(txtBoxControl.Text);
        //            }
        //        }
        //        return lstSetiingValues;
        //    }
        //}
        //public string QuestionText
        //{
        //    get
        //    {
        //        var questionControl = Utilities.FindControl<QuestionControl>(this, "QuestionControl1");
        //        var questionText = Utilities.FindControl<HtmlTextArea>(questionControl, "EditorQuestion");
        //        return questionText.Value;
        //    }
        //}

        public override void Display()
        {

        }
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        public override void Initialise()
        {
            base.Initialise();
        }

        #endregion
    }
}