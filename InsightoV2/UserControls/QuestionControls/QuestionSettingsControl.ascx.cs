﻿using System;
using System.Collections.Generic;
using App_Code;
using CovalenseUtilities.Helpers;
using FeatureTypes;
using Insighto.Business.Enumerations;
using Insighto.Web.UI.Controls;
using Insighto.Business.Enumerations;
using System.Linq;
using System.Web.UI.WebControls;

namespace Insighto.UserControls.QuestionControls
{
    public partial class QuestionSettingsControl : QuestionControlBase
    {
        public override List<string> QuestionSettings
        {
            get
            {
                List<string> lstSetiingValues = new List<string>();
                foreach (var control in CommonHelper.FindControlRecursive<InsightoCheckBox>(this, 4))
                {
                    lstSetiingValues.Add(control.Checked ? "1" : "0");

                }
                lstSetiingValues.Add(txtCharLimit.Text);
                return lstSetiingValues;
            }
            set
            {
                base.QuestionSettings = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetFeaurePermissionsByLicenseType();
            SetFeaurePermissionsByQuestionType();

            if (!IsPostBack)
            {
                rdNoColumns.Items.Clear();
                var lstColumns = (Enum.GetValues(typeof(ColumnType)) as IEnumerable<ColumnType>).Select(
                        p => new ListItem { Text = Convert.ToString((int)p), Value = Convert.ToString((int)p) });

                rdNoColumns.DataSource = lstColumns;
                rdNoColumns.DataBind();
                rdNoColumns.SelectedValue = rdNoColumns.SelectedIndex > 0 ? rdNoColumns.SelectedValue : "1";
            }
            Display();
        }

        public override void Display()
        {
            if (QuestionAndAnswerOptions == null || QuestionAndAnswerOptions.Question == null)
                return;

            rdNoColumns.SelectedIndex = Convert.ToInt32(Question.ANSWER_ALIGNSTYLE);
            chkOtherAnswer.Checked = Question.OTHER_ANS > 0;
            chkPageBreak.Checked = Question.PAGE_BREAK > 0;
            chkRandomAnswer.Checked = Question.RANDOMIZE_ANSWERS > 0;
            chkResponseRequired.Checked = Question.RESPONSE_REQUIRED > 0;
        }

        /// <summary>
        /// Sets the type of the feaure permissions by question.
        /// </summary>
        private void SetFeaurePermissionsByQuestionType()
        {
            if (SurveyPageBase.QuestionType == QuestionType.PageHeader || SurveyPageBase.QuestionType == QuestionType.QuestionHeader)
                ShowOrHideControls(false, false, false, false, true, false);
            else if (SurveyPageBase.QuestionType == QuestionType.Video || SurveyPageBase.QuestionType == QuestionType.Image)
                ShowOrHideControls(false, false, false, false, false, false);
            else if (SurveyPageBase.QuestionType == QuestionType.ContactInformation || SurveyPageBase.QuestionType == QuestionType.DateAndTime || SurveyPageBase.QuestionType == QuestionType.EmailAddress)
                ShowOrHideControls(false, false, false, false);
            else if (SurveyPageBase.QuestionType == QuestionType.Text_CommentBox || SurveyPageBase.QuestionType == QuestionType.Text_MultipleBoxes || SurveyPageBase.QuestionType == QuestionType.Text_SingleRowBox)
                ShowOrHideControls(false, false, false, false);

        }

        /// <summary>
        /// Shows the or hide controls.
        /// </summary>
        /// <param name="noOfColumns">if set to <c>true</c> [no of columns].</param>
        /// <param name="otherAnswer">if set to <c>true</c> [other answer].</param>
        /// <param name="randomAnswer">if set to <c>true</c> [random answer].</param>
        /// <param name="pageBreak">if set to <c>true</c> [page break].</param>
        /// <param name="responseRequired">if set to <c>true</c> [response required].</param>
        private void ShowOrHideControls(bool noOfColumns, bool charLimit, bool otherAnswer, bool randomAnswer, bool pageBreak = true,  bool responseRequired = true)
        {
            dvrdNoColumns.Visible = noOfColumns;
            dvCharLimit.Visible = charLimit;
            chkOtherAnswer.Visible = otherAnswer;
            dvchkPageBreak.Visible = pageBreak;
            dvchkRandomAnswer.Visible = randomAnswer;
            dvchkResponseRequired.Visible = responseRequired;
        }

        /// <summary>
        /// Sets the type of the feaure permissions by license.
        /// </summary>
        private void SetFeaurePermissionsByLicenseType()
        {
            UserType userType = Utilities.GetUserType();
            chkOtherAnswer.Visible = Utilities.ShowOrHideFeature(userType, (int)SurveyCreation.Others_Text_Box);
            chkPageBreak.Visible = Utilities.ShowOrHideFeature(userType, (int)SurveyCreation.Page_Break);
            chkResponseRequired.Visible = Utilities.ShowOrHideFeature(userType, (int)SurveyCreation.Response_Required);
            chkRandomAnswer.Visible = Utilities.ShowOrHideFeature(userType, (int)SurveyCreation.Randomize_Answer_Options);
            txtCharLimit.Visible = Utilities.ShowOrHideFeature(userType, (int)FeatureTypes.SurveyCreation.Character_Limit);
        }
    }
}