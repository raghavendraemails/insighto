﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
namespace UserControls.QuestionControls
{
    public partial class VideoQuestionControl : QuestionControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        string VideoNameType = "";
        string Val = "";

        public string VideoType
        {
            get
            {
                return fileViedoUpload.PostedFile.ContentType.Substring(fileViedoUpload.PostedFile.ContentType.LastIndexOf('/') + 1);
            }
        }

        public string FileName
        {
            get
            {
                return fileViedoUpload.PostedFile.FileName;
            }
        }

        public int FileSize
        {
            get
            {
                return fileViedoUpload.PostedFile.ContentLength;
            }
        }

        public bool HasFile
        {
            get
            {
                return fileViedoUpload.HasFile;
            }
        }

        public string VideoPath
        {
            set
            {
                if (rbListVideos.SelectedValue == "1")
                {
                    Val = value;
                    dvVideoPlay.Visible = true;
                }
                else
                {
                    dvYoutubePlay.InnerHtml = value;
                    dvYoutubePlay.Visible = true;
                }
            }
            get
            {
                return Val;
            }
        }
        protected void rdVideoType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbListVideos.SelectedIndex == 0)
            {
                dvVideoUpload.Visible = true;
                dvYoutubeUpload.Visible = false;
                dvYoutubePlay.Visible = false;
            }
            else if (rbListVideos.SelectedIndex == 1)
            {
                dvYoutubeUpload.Visible = true;
                dvVideoUpload.Visible = false;
                dvVideoPlay.Visible = false;
            }
        }
        protected void CustCheckVideoPath(object source, ServerValidateEventArgs args)
        {
            if (rbListVideos.SelectedIndex == 0)
            {
                bool isFileCheck = fileViedoUpload.HasFile;

                if (dvVideoPlay.Visible == false && isFileCheck == false)
                {
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else if (rbListVideos.SelectedIndex == 1)
            {
                if (dvYoutubePlay.InnerHtml != "")
                {
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = false;
                }
            }
        }
    }
}