﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using App_Code;


namespace Insighto.UserControls
{
    public partial class SurveyPreview : UserControlBase
    {
        #region "Variables"

        Hashtable typeHt = new Hashtable();
        int surveyId = 0;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());

                var questionService = ServiceFactory.GetService<QuestionService>();
                var surveyControls = ServiceFactory.GetService<SurveyControls>().GetSurveycontrolsBySurveyId(surveyId).FirstOrDefault();

                string strRespondentLink = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewURL(), Constants.SURVEYID + "=" + surveyId + "&Mode=" + RespondentDisplayMode.PreviewWithPagination.ToString() + "&surveyFlag=" + base.SurveyFlag+"&Layout="+surveyControls.SURVEY_LAYOUT);
                    hlnkPreview.NavigateUrl = strRespondentLink;
                    hlnkPreview.Text = SurveyMode + " " + GetLocalResourceObject("lblPreviewString").ToString();// " Preview";
                    hlnkPreview.ToolTip = SurveyMode+ " " + GetLocalResourceObject("lblPreviewString").ToString(); ;                   
            }
        }
    }
}