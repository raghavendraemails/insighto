﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SuveyColumnControl.ascx.cs"
    Inherits="Insighto.UserControls.SuveyColumnControl" %>
<p>
   <asp:Label ID="lblNumberOfColumns" runat="server" Text="Number of Columns:" 
        meta:resourcekey="lblNumberOfColumnsResource1"></asp:Label>
</p>
<asp:RadioButtonList ID="rdNoColumns" runat="server" 
    RepeatDirection="Horizontal" meta:resourcekey="rdNoColumnsResource1" />
