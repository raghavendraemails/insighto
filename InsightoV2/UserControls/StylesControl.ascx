﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StylesControl.ascx.cs"
    Inherits="UserControls_Styles" %>
<link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/layoutStyle.css?v=<%= VersionNumber %>" />
<link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/chromestyle.css?v=<%= VersionNumber %>" />
<link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/facebox.css?v=<%= VersionNumber %>" />
<link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/menuStyle.css?v=<%= VersionNumber %>" />
<link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/tabsStyle.css?v=<%= VersionNumber %>" />
<link rel="stylesheet" type="text/css" media="screen" href="/Styles/JqueryUi/jquery-ui-1.8.14.custom.css?v=<%= VersionNumber %>" />
<link rel="stylesheet" type="text/css" media="screen" href="/Styles/ui.jqgrid.css?v=<%= VersionNumber %>" />
<!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" media="screen" href="App_Themes/Classic/IE6.css" />
<![endif]-->
<link rel="icon" type="image/icon" href="insighto.ico" />
