﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HtmlEditor.ascx.cs" Inherits="Insighto.UserControls.HtmlEditor" %>
     <script type="text/javascript" src="Scripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            // General options
            mode: "specific_textareas",
            editor_selector: "mceEditor",
            theme: "advanced",

            plugins: "paste",

            // Theme options
            theme_advanced_buttons1: "bold,italic,underline,fontselect,fontsizeselect,forecolor,pasteword",
            theme_advanced_buttons2: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            //theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing: false,

            // Example content CSS (should be your site CSS)
            content_css: "css/content.css",
            oninit: "postInitWork",
            // Replace values for the template plugin
            template_replace_values: {
                username: "Some User",
                staffid: "991234"
            }
        });

        function postInitWork() {

            var val = document.getElementById("<%=EditorQuesIntroParams.ClientID%>").value;
            var sty_val = val.split("_");
            if (sty_val.length == 3) {
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.fontFamily = sty_val[0];
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.fontSize = sty_val[1];
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.color = sty_val[2];
            }

        }

        function cleartext() {
            tinyMCE.getInstanceById('<%= EditorQuestionIntro.ClientID %>').setContent('');
            return false;
        }

        function resetText() {
            tinyMCE.getInstanceById('<%= EditorQuestionIntro.ClientID %>').setContent('<p> You are invited to join a short survey being held among the customers of COMPANY/BRAND X. There are only (X No. of questions) and it will take about X minutes of your time. We will keep all your information confidential and will not transmit it to any third parties. If you have any questions about this survey, please contact me directly at my email address provided below. <br> Your Feedback is important!</br>');

        }

    </script>
      <input type="hidden" runat="server" id="EditorQuesIntroParams" value="0" />
 <textarea id="EditorQuestionIntro" runat="server" cols="10" rows="8" class="mceEditor"
                    style="width: 643px; height: 150px;"></textarea>
