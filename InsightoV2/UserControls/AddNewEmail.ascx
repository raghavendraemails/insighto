﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddNewEmail.ascx.cs" Inherits="Insighto.UserControl.AddNewEmail" %>
<div id="divAddNewEmail">
    <div class="successPanel dvSuccessMsg" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccessMsg" CssClass="lblSuccessMsg" runat="server" 
                Visible="False" Text="Contact added successfully." 
                meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
    </div>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblErrMsg" runat="server" Visible="False" 
                Text="<%$ Resources:CommonMessages, EmailExistsMsg %>" 
                meta:resourcekey="lblErrMsgResource1"></asp:Label>
        </div>
    </div>
    <div class="clear">
    </div>
    <div>
        <div>
        <table border="0" width="100%">
            <tr>
                <td>&nbsp;</td>
                <td align="right">
<%--                <asp:LinkButton ID="hlnkAddColumns" rel="framebox" h="280" w="550" scrolling='yes'
                        title='Edit / Delete Columns' 
                        ToolTip="Edit / Delete Columns"
                        runat="server" meta:resourcekey="hlnkAddColumnsResource1" OnClick="hlnkAddColumns_Click"> <div class="hyperlink_add_emil_column">Edit / Delete Columns</div></asp:LinkButton>--%>
                <asp:HyperLink ID="hlnkAddColumns" rel="framebox" h="280" w="550" scrolling='yes'
                        title='Edit / Delete Columns' 
                        ToolTip="Edit / Delete Columns"
                        runat="server" meta:resourcekey="hlnkAddColumnsResource1"> <div class="hyperlink_add_emil_column">Edit / Delete Columns</div></asp:HyperLink>
                </td>
            </tr>
        </table>
        </div>
        
        <div style="margin:10px 0 10px 0;">
            <table border="0" cellpadding="3" cellspacing="2" width="100%">
                <tr>
                    <td><asp:Label ID="lblEmail" runat="server"  Text="Email Id"  CssClass="requirelbl" 
                            meta:resourcekey="lblEmailResource1" ></asp:Label></td>
                    <td>  
                        <asp:Label ID="lblFirstName" runat="server"  Text="First Name" meta:resourcekey="lblFirstNameResource1"
                              ></asp:Label></td>
                               <td>  
                                   <asp:Label ID="lblLastName" runat="server"  Text="Last Name" meta:resourcekey="lblLastNameResource1"
                              ></asp:Label></td>
                    <td><asp:Label ID="lblColumn1" runat="server" Visible="False" 
                            meta:resourcekey="lblColumn1Resource1" ></asp:Label>                      
                    </td>
                     <td><asp:Label ID="lblColumn2" runat="server" Visible="False" 
                             meta:resourcekey="lblColumn2Resource1"  ></asp:Label></td>
                          
                </tr>
                <tr>
                    <td> <asp:TextBox ID="txtEmail" runat="server" CssClass="textBoxSmall txtEmail" 
                            style="width:98%" MaxLength="50" meta:resourcekey="txtEmailResource1"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtFirstName" runat="server" CssClass="textBoxSmall txtFirstName" 
                            style="width:98%" MaxLength="50" meta:resourcekey="txtFirstNameResource1"></asp:TextBox></td>
                    <td> <asp:TextBox ID="txtLastName" runat="server" CssClass="textBoxSmall txtLastName" 
                            style="width:98%" MaxLength="50" meta:resourcekey="txtLastNameResource1"></asp:TextBox></td>
                    <td><asp:TextBox ID="txtColumn1" Visible="False" runat="server"  
                            CssClass="textBoxSmall txtColumn1" style="width:98%"
                                MaxLength="50" meta:resourcekey="txtColumn1Resource1" />
                    </td>
                    <td>
                      <asp:TextBox ID="txtColumn2" Visible="False" runat="server"  
                            CssClass="textBoxSmall txtColumn2" style="width:98%"
                                MaxLength="50" meta:resourcekey="txtColumn2Resource1" />
                    </td>
                </tr>
                <tr>
                <td colspan="5">
                 <asp:Label ID="lblInvalidEmail" class="lblRequired" runat="server" Visible="False"
                                    Text="Please enter email id." 
                        meta:resourcekey="lblInvalidEmailResource1"></asp:Label>
                                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqEmailId" Visible="False"
                                    ControlToValidate="txtEmail" CssClass="lblRequired" ErrorMessage="Please enter email id."
                                    runat="server" Display="Dynamic" 
                        meta:resourcekey="reqEmailIdResource1"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator SetFocusOnError="True" 
                        ID="regEmailId" Visible="False"
                                    runat="server" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                    CssClass="lblRequired" ControlToValidate="txtEmail" 
                        Display="Dynamic" ErrorMessage="Please enter valid email id." 
                        meta:resourcekey="regEmailIdResource1"></asp:RegularExpressionValidator>
                </td></tr>
            </table>
        </div>
        <table cellpadding="0" cellspacing="0" width="100%" border="0">
            <tr>
                <td>
                    <div>
                        <div id="dvCol1" class="dvCol1" runat="server" visible="false"></div>
                        <div id="dvCol2" class="dvCol2" runat="server" visible="false"></div>
                        <div id="dvCol4" runat="server" visible="false">
                            <asp:Label ID="lblColumn4" runat="server" CssClass="loginlbl" 
                                AssociatedControlID="txtColumn4" meta:resourcekey="lblColumn4Resource1"></asp:Label>
                            <asp:TextBox ID="txtColumn4" Visible="False" runat="server" CssClass="textBoxMedium"
                                MaxLength="50" meta:resourcekey="txtColumn4Resource1" />
                        </div>
                        <div id="dvCol6" runat="server" visible="false">
                            <asp:Label ID="lblColumn6" runat="server" CssClass="loginlbl" 
                                AssociatedControlID="txtColumn6" meta:resourcekey="lblColumn6Resource1"></asp:Label>
                            <asp:TextBox ID="txtColumn6" Visible="False" runat="server" CssClass="textBoxMedium"
                                MaxLength="50" meta:resourcekey="txtColumn6Resource1" />
                        </div>
                        <div id="dvCol8" runat="server" visible="false">
                            <asp:Label ID="lblColumn8" runat="server" CssClass="loginlbl" 
                                AssociatedControlID="txtColumn8" meta:resourcekey="lblColumn8Resource1"></asp:Label>
                            <asp:TextBox ID="txtColumn8" Visible="False" runat="server" CssClass="textBoxMedium"
                                MaxLength="50" meta:resourcekey="txtColumn8Resource1" />
                        </div>
                        <div id="dvCol3" runat="server" visible="false">
                            <asp:Label ID="lblColumn3" runat="server" CssClass="loginlbl" 
                                AssociatedControlID="txtColumn3" meta:resourcekey="lblColumn3Resource1"></asp:Label>
                            <asp:TextBox ID="txtColumn3" Visible="False" runat="server" CssClass="textBoxMedium"
                                MaxLength="50" meta:resourcekey="txtColumn3Resource1" />
                        </div>
                        <div id="dvCol5" runat="server" visible="false">
                            <asp:Label ID="lblColumn5" runat="server" CssClass="loginlbl" 
                                AssociatedControlID="txtColumn5" meta:resourcekey="lblColumn5Resource1"></asp:Label>
                            <asp:TextBox ID="txtColumn5" Visible="False" runat="server" CssClass="textBoxMedium"
                                MaxLength="50" meta:resourcekey="txtColumn5Resource1" />
                        </div>
                        <div id="dvCol7" runat="server" visible="false">
                            <asp:Label ID="lblColumn7" runat="server" CssClass="loginlbl" 
                                AssociatedControlID="txtColumn7" meta:resourcekey="lblColumn7Resource1"></asp:Label>
                            <asp:TextBox ID="txtColumn7" Visible="False" runat="server" CssClass="textBoxMedium"
                                MaxLength="50" meta:resourcekey="txtColumn7Resource1" />
                        </div>
                        
                    </div>
                </td>
                
            </tr>
            <tr>
                <td align="center">
                <table cellpadding="0" cellspacing="0"><tr><td> <asp:Button runat="server" 
                        CssClass="dynamicButton btnSave" ID="btnSave" OnClick="btnSave_Click"
                            Text="Save" ToolTip="Save" meta:resourcekey="btnSaveResource1" /></td><td>
                        <asp:Button runat="server" CssClass="dynamicButton btnUpdate" ID="btnUpdate" style="display:none;"
                            OnClick="btnUpdate_Click" Text="Update" ToolTip="Update" 
                            meta:resourcekey="btnUpdateResource1" /></td><td><asp:Button runat="server" 
                            CssClass="dynamicButton btnCancel" ID="btnCancel" Text="Cancel"
                            OnClick="btnCancel_Click" Style="margin-left: 3px;" ToolTip="Cancel" 
                            meta:resourcekey="btnCancelResource1"/></td></tr></table>
                            <asp:HiddenField ID="hdnContactListId" runat="server" />
                </td>
            </tr>
        </table>
        <div class="clear">
        </div>
       
    </div>
</div>
