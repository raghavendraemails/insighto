﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Helpers;

namespace Insighto.UserControls
{
    public partial class SurveyHeader : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

         
            var page = this.Page as SurveyPageBase;
            string surveyName = string.Empty;
            string surveyId = string.Empty;
            Hashtable ht1 = new Hashtable();
            ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
            int flag = 0;
            if (ht1 != null)
            {

                if (ht1.Contains("surveyName"))
                {
                    surveyName = ht1["surveyName"].ToString();
                }

                if (ht1.Contains("SurveyId"))
                {
                    surveyId = ht1["SurveyId"].ToString();
                }
                if (ht1.Contains("surveyFlag") && ht1["surveyFlag"].ToString() == "0")
                {
                    lblHeading.InnerText = "Survey Name :";
                    flag = 0;
                }
                else if (ht1.Contains("surveyFlag") && ht1["surveyFlag"].ToString() == "1")
                {
                    lblHeading.InnerText = "Widget Name :";
                    flag = 1;
                }

            }

            if (page == null || page.SurveyBasicInfoView == null)
            {


                lblSurveyName.Text = surveyName;
               // if (flag>0)
                    lnkEdit.HRef = EncryptHelper.EncryptQuerystring("~/SurveyBasics.aspx", String.Format("SurveyId={0}&surveyFlag={1}&Mode=Edit", surveyId, flag));
               // else
               //     lnkEdit.HRef = String.Format("~/SurveyBasics.aspx?SurveyId={0}", surveyId);
                return;
            }
            else
            {
                lblSurveyName.Text = page.SurveyBasicInfoView.SURVEY_NAME;
               // if (flag > 0)
                lnkEdit.HRef = EncryptHelper.EncryptQuerystring("~/SurveyBasics.aspx", String.Format("SurveyId={0}&surveyFlag={1}&Mode=Edit", page.SurveyBasicInfoView.SURVEY_ID, flag));
               // else
              //  lnkEdit.HRef = String.Format("~/SurveyBasics.aspx?SurveyId={0}", page.SurveyBasicInfoView.SURVEY_ID);
            }
               //lblSurveyName.Text = surveyName;
               // lnkEdit.HRef = EncryptHelper.EncryptQuerystring("~/SurveyBasics.aspx", String.Format("SurveyId={0}&surveyFlag={1}", surveyId, flag)); // String.Format("~/SurveyBasics.aspx?SurveyId={0}&surveyFlag={1}", surveyId, flag);
               // return;
        }
    }
}
