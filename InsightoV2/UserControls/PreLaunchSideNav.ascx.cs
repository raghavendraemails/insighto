﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;


namespace Insighto.UserControls
{
    public partial class PreLaunchSideNav : UserControlBase
    {
        #region "Variables"

        Hashtable typeHt = new Hashtable();
        int surveyId = 0;

        #endregion

        // <summary>
        /// Gets the base page.
        /// </summary>
        public SurveyPageBase BasePage
        {
            get
            {
                return Page as SurveyPageBase;
            }
        }
        public string Key
        {
            get
            {
                return HttpUtility.UrlEncode(QueryStringHelper.GetString("Key"));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            lblView.Text = String.Format(GetLocalResourceObject("lblViewResource1.Text").ToString(),BasePage.SurveyMode.ToString());      
            if (!IsPostBack)
            {
                aemailinv.HRef = "~/PreLaunchEmailInvitation.aspx?Key=" + Key;
                aemaillist.HRef = "~/PreLaunchSurveyEmailList.aspx?Key=" + Key;
            }
            if (BasePage.SurveyBasicInfoView.EMAILLIST_TYPE == 2)
            {
                //aemailinv.HRef = "javascript:void(0)";
                //aemaillist.HRef = "javascript:void(0)";
                aemailinv.Visible = false;
                aemaillist.Visible = false;
            }
            else
            {
                //aemailinv.HRef = "PreLaunchEmailInvitation.aspx?Key=" + Key;
                //aemaillist.HRef = "PreLaunchQuestionnaire.aspx?Key="+Key;
                aemailinv.Visible = true;
                aemaillist.Visible = true;
            }
        }
    }
}