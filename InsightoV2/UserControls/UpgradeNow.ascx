﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UpgradeNow.ascx.cs" Inherits="Insighto.UserControls.UpgradeNow" %>
<div class="upgradePanel">
    <!-- upgrade panel -->
    <div class="upgradeTexPanel" id="divFreeUSer" runat="server">
    <div class="clear"></div>
        <div class="needMorePanel">
            <!-- need more-->
            <h2>
               <asp:Label ID="lblNeedMore" runat="server" Text="Need More?" 
                    meta:resourcekey="lblNeedMoreResource1"></asp:Label></h2>
            <!-- //need more-->
        </div>
        <div class="upgradeMessagePanel">
            <!-- upgrade message -->
            <p>
              <asp:Label ID="lblUpgrade1" runat="server" Text="You are using " 
                    meta:resourcekey="lblUpgrade1Resource1"></asp:Label><a href="pricing.aspx" class="orangeLink" target="_blank">
              <asp:Label ID="lblUpgrade2" runat="server" Text=" Free version " 
                    meta:resourcekey="lblUpgrade2Resource1"></asp:Label></a>
              <asp:Label ID ="lblUpgrade3" runat="server" 
                    Text=" of Insighto. Please upgrade to " meta:resourcekey="lblUpgrade3Resource1"></asp:Label> <a href="pricing.aspx" class="orangeLink" target="_blank">
                <asp:Label ID="lblUpgrade4" Text=" Insighto PRO " runat="server" 
                    meta:resourcekey="lblUpgrade4Resource1"></asp:Label></a>
              <asp:Label ID="lblUpgrade5" runat="server" Text="  and enjoy the unique services - Unlimited questions &amp; surveys, Advanced reporting tools, Customize
                your charts &amp; graphs..etc." meta:resourcekey="lblUpgrade5Resource1"></asp:Label>
            </p>
            <!-- //upgrade message -->
        </div>
        <div class="upgradeNowBtnPanel">
            <ul class="HyperlinkBtn">
                <li>
                    <asp:HyperLink ID="Hyperupgradenow" runat="server" Text="Upgrade Now" 
                        ToolTip="Upgrade Now" Target="_blank" 
                        meta:resourcekey="HyperupgradenowResource1" ></asp:HyperLink></li></ul>
        </div>
        <div class="clear">
        </div>
    </div>
    <!-- //upgrade panel -->
    <div class="upgradeTexPanel" id="divProUser" runat="server" visible="false">
        <div class="needMorePanel" style="width:210px;">
            <!-- need more-->
            <h2><asp:Label ID="lblInsightoProVersion" runat="server" 
                    Text="Insighto Pro version..." 
                    meta:resourcekey="lblInsightoProVersionResource1"></asp:Label></h2>
            <!-- //need more-->
        </div>
        <div class="clear">
        </div>
    </div>
</div>
