﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UpgradeNowNew.ascx.cs" Inherits="UserControls_UcUpgradenew" %>

<script type="text/javascript">    
    function fnShowNeedMoreDeatils() {
        $("#vwNeedMoreDeatils").slideDown('slow');
    }

    function fnHideNeedMoreDeatils() {
        $("#vwNeedMoreDeatils").slideUp('slow');
    }
</script>
<script type="text/javascript">

    $(function () {
        $('hplUpgrade1').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);           
            _gaq.push(['lblupgradetext1', 'click', 'hplUpgrade1', '']);

           
            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });

    $(function () {
        $('hplUpgrade2').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);           
            _gaq.push(['lblupgradetext2', 'click', 'hplUpgrade2', '']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });

    $(function () {
        $('hplUpgrade3').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
           
            _gaq.push(['lblupgradetext3', 'click', 'hplUpgrade3', '']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });
    $(function () {
        $('hplUpgrade4').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
           
            _gaq.push(['lblupgradetext4', 'click', 'hplUpgrade4', '']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });

    $(function () {
        $('hplUpgrade5').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
           
            _gaq.push(['lblupgradetext5', 'click', 'hplUpgrade5', '']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });
    $(function () {
        $('hplUpgrade6').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
           
            _gaq.push(['lblupgradetext6', 'click', 'hplUpgrade6', '']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });
    $(function () {
        $('hplUpgrade7').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
           
            _gaq.push(['lblupgradetext7', 'click', 'hplUpgrade7', '']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });
    $(function () {
        $('hplUpgrade8').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
           
            _gaq.push(['lblupgradetext8', 'click', 'hplUpgrade8', '']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });
    $(function () {
        $('hplUpgrade9').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
           
            _gaq.push(['lblupgradetext9', 'click', 'hplUpgrade9', '']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });
    $(function () {
        $('hplUpgrade10').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
           
            _gaq.push(['lblupgradetext10', 'click', 'hplUpgrade10', '']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });
    $(function () {
        $('hplUpgrade11').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
           
            _gaq.push(['lblupgradetext11', 'click', 'hplUpgrade11', '']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });
    $(function () {
        $('hplUpgrade12').click(function () {
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29026379-1']);
            _gaq.push(['_setDomainName', 'insighto.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
           
            _gaq.push(['lblupgradetext12', 'click', 'hplUpgrade12', '']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            return false;
        });
    });
    </script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29026379-1']);
    _gaq.push(['_setDomainName', 'insighto.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);
           

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
 
</script>
<style type="text/css">
.btn_bg
{
    background-color:#FF7708;   
    font-family:Arial,sans-serif; 
     border:0; 
    fonfont-weight:bold;
     border-bottom-left-radius:5px;
    border-bottom-right-radius:5px;
    border-top-left-radius:5px;
    border-top-right-radius:5px;
 
	padding:7px 15px;
	
	font-size:14px;
	font-weight:bold;
	color:#FFF;
	cursor:pointer;
	cursor:hand;
	
}
</style>
     <div id="divFreeUser" runat="server"  >
         <!-- welcome panel -->
	  <table width="100%" cellpadding="0" cellspacing="0">
	  <tbody>
	 
       <tr>
		<td align="right" width="80%">
			<table border="0" cellpadding="0" cellspacing="0"><tbody>
                <tr>
                    <td style="vertical-align:middle; font-weight:bold;font-size:15px;" >
                        <asp:Label ID="Label1" runat="server" Text="Surveys : " ></asp:Label>&nbsp;
                    </td>
                    <td style=" vertical-align:middle" >
                        <a id="UpgradeBuysurveycredit" runat="server"  OnClick="_gaq.push(['_lblupgradetext1', 'Click', 'Buysurveycredit', '']);">  
                            <asp:Label ID="btnsinglesurvey" runat="server" class="btn_bg" Text="Buy A Single Survey" ></asp:Label>
                            &nbsp;
                        </a>
                    </td>
                    <td>&nbsp;</td>
                    <td style=" vertical-align:middle">
                        <a id="UpgradeBuypremannual" runat="server"  OnClick="_gaq.push(['_lblupgradetext1', 'Click', 'Buysurveycredit', '']);">  
                            <asp:Label ID="btnpremium" runat="server" class="btn_bg" Text="Buy Premium Annual" ></asp:Label>
                        </a>
                    </td>
                </tr>
            </tbody>
		</table>
 
        <!-- need more-->
      
        <!-- //need more-->
		
      </td></tr></tbody></table> 
      <!-- welcome panel -->
    </div>

    <div id="divProUser" runat="server">
      <!-- welcome panel -->
	  <table width="100%" cellpadding="0" cellspacing="0">
	  <tbody><tr><td>
      </td>
	  </tr>
	 
       <tr>

		<td align="right">
			<table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="padding-right:10px"> 
	  </td><td><asp:HyperLink ID="hplRenew" CssClass="dynamicHyperLink" style="padding:4px 8px 4px 8px; font-size:12px;" ToolTip="Upgrade Now" Target="_parent"   
                        runat="server" meta:resourcekey="hplUpgradeResource1" >
  </asp:HyperLink></td></tr>
  
  
  
  </tbody></table>
        <!-- need more-->
      
        <!-- //need more-->
		
      </td></tr></tbody></table> 
      <!-- welcome panel -->

    </div>

	<div id="vwNeedMoreDeatils" style="display:none;position:absolute; width:180px;text-align:left;z-index: 0; padding:10px; background:#fff; border:solid 2px #c424a5; font-size:11px; text-align:justify">
    <div id="dvNeedMoreDetails" runat="server">
	<asp:Label ID="lblNeedMore1" runat="server" Text="You are using " 
            meta:resourcekey="lblNeedMore1Resource1"></asp:Label><b>
        <asp:Label ID="lblNeedMoe2" runat ="server" Text=" Free version " 
            meta:resourcekey="lblNeedMoe2Resource1"></asp:Label></b>
        <asp:Label ID="lblNeedMore3" runat="server" 
            Text=" of Insighto. Please upgrade to " 
            meta:resourcekey="lblNeedMore3Resource1"></asp:Label>
         <a href="#" class="orangeLink"><b><asp:Label ID="lblInsightoPro" 
            runat="server" Text=" Insighto PRO " meta:resourcekey="lblInsightoProResource1"></asp:Label></b></a>
         <asp:Label ID="lblNeedMore4" runat="server"  
            Text =" and enjoy the unique services - Unlimited questions & surveys, Advanced reporting tools, Cutomize your charts & graphs..etc." 
            meta:resourcekey="lblNeedMore4Resource1"></asp:Label> 
        </div>
	   </div>
	

    <div id="polls" runat="server" style="margin-top:20px;">
        <span style="font-size:15px;font-weight:bold">Polls : </span>
    <a id="pollslink" runat="server">  
                    <asp:Label ID="Poll" runat="server" class="btn_bg" Text="Upgrade" ToolTip="Upgrade Poll License" ></asp:Label></a>
    </div>
    <!-- //header -->

<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</p>


