﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Contactinfo.ascx.cs" Inherits="Insighto.UserControls.Contactinfo" %>
<div class="clear">
</div>
<div class="contactinfo_lt">
    <asp:CheckBox ID="chkIncludeFormField" CssClass="form-field" runat="server" Checked="true" />
</div>
<div class="contactinfo_rt">
    <asp:TextBox ID="txtFormField" runat="server" meta:resourcekey="txtFormFieldResource1"
        CssClass="textBoxMedium" /></div>
<div class="contactinfomandatory_lt">
    <asp:CheckBox ID="chkMandatoryField" runat="server" Text="Mandatory" 
        CssClass="form-field chkMandatory"  meta:resourcekey="chkMandatoryFieldResource1" />
</div>

<div class="clear">
</div>
