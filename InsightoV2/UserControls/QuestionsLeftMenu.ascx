﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuestionsLeftMenu.ascx.cs"
    Inherits="Insighto.UserControls.QuestionsLeftMenu" %>
<h4>
    <asp:Label ID="lblChooseQuestionType" runat="server" Text="Choose Question Type"
        meta:resourcekey="lblChooseQuestionTypeResource1"></asp:Label></h4>
<asp:ListView ID="lstQuestionTypes" runat="server" GroupItemCount="5" ItemPlaceholderID="itemPlaceHolder"
    GroupPlaceholderID="groupPlaceHolder">
    <GroupTemplate>
        <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
    </GroupTemplate>
    <LayoutTemplate>
        <asp:PlaceHolder ID="groupPlaceholder" runat="server" />
    </LayoutTemplate>
    <ItemTemplate>
        <ul class="menu">
            <li><a href='#'>
                <%# Eval("QUESTION_TYPE_DESC")%></a>
                <ul class="innerMenu">
                    <asp:ListView ID="lstQuestionTypesItems" runat="server" DataSource='<%# Eval("osm_questiontypes") %>'
                        OnItemDataBound="lstQuestionTypesItems_ItemDataBound">
                        <ItemTemplate>
                            <li id='ul<%# Eval("QUESTION_TYPE_ID") %>'>
                               <asp:HyperLink ID="QType" runat="server">
                                    <asp:Label ID="lblpro" runat="server" Text='<%# Eval("QUESTION_TYPE") %>'></asp:Label>
                                </asp:HyperLink>
                                <asp:Image ID="imgPro" runat="server" ImageUrl="~/App_Themes/Classic/Images/icon_pro.png"
                                    Visible="False" /></li>
                        </ItemTemplate>
                    </asp:ListView>
                </ul>
            </li>
        </ul>
    </ItemTemplate>
</asp:ListView>
<div class="popupPreviewBox" id="dvPreview" runat="server">
    <!-- preive panel -->
    <h4>
        <asp:Label ID="lblSampleQuestion" runat="server" Text="Sample Question" meta:resourcekey="lblSampleQuestionResource1"></asp:Label></h4>
    <div class="popupPreviewBoxText">
        <asp:Image runat="server" ID="imgQuestionType" />
    </div>
    <!-- //preive panel -->
</div>
