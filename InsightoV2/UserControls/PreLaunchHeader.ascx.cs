﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;


namespace Insighto.UserControls
{
    public partial class PreLaunchHeader : UserControlBase
    {
        Hashtable typeHt = new Hashtable();
        int surveyId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            var page = this.Page as SurveyPageBase;
            if (page == null || page.SurveyBasicInfoView == null) return;

            lblSurveyName.Text = page.SurveyBasicInfoView.SURVEY_NAME;
            lnkEdit.HRef = String.Format("~/SurveyBasics.aspx?SurveyId={0}", page.SurveyBasicInfoView.SURVEY_ID);
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());

            }

            if (surveyId > 0)
            {
                string strLaunchUrl = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx", Constants.SURVEYID + "=" + surveyId + "&surveyFlag=" + base.SurveyFlag);
                Response.Redirect(strLaunchUrl);
            }
        }

    }
}