﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using System.Text;
using System.Collections;
using Insighto.Business.Helpers;
using Insighto.Data;
using System.Data.OleDb;
using System.IO;
using Insighto.Business.Enumerations;
using System.Text.RegularExpressions;
using App_Code;
using Insighto.Business.ValueObjects;

namespace Insighto.UserControl
{
    public partial class AddNewEmail : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Request.QueryString["key"] != null)
            {
                Hashtable ht = new Hashtable();
                var contactService = ServiceFactory.GetService<ContactListService>();

                int ContactId = 0;
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                {
                    ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                    string editListUrl = EncryptHelper.EncryptQuerystring("ContactList.aspx", "ContactId=" + ContactId + "&surveyFlag=" + base.SurveyFlag);
                }
                var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                string strAddColumns = EncryptHelper.EncryptQuerystring("~/AddAddressBookColumns.aspx", "ContactId=" + ContactId + "&surveyFlag=" + base.SurveyFlag);
                string strCopyList = EncryptHelper.EncryptQuerystring("CopyContactList.aspx", "ContactId=" + ContactId + "&surveyFlag=" + base.SurveyFlag);
                string strUpload = EncryptHelper.EncryptQuerystring("UploadExcelCSV.aspx", "ContactId=" + ContactId + "&surveyFlag=" + base.SurveyFlag);
                string strNewEmail = EncryptHelper.EncryptQuerystring("EmailListPopUp.aspx", "ContactId=" + ContactId + "&surveyFlag=" + base.SurveyFlag);


                DropDownList drpList = (DropDownList)Parent.FindControl("drpList");
                int contid = ValidationHelper.GetInteger(drpList.SelectedValue, 0);
                if (contid <= 0)
                {
                    hlnkAddColumns.Visible = false;
                }
                else
                    hlnkAddColumns.Visible = true;
                hlnkAddColumns.NavigateUrl = strAddColumns;
                if (loggedInUserInfo.LicenseType == "FREE")
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "upgrade", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
                    hlnkAddColumns.Attributes.Add("h", "450");
                    hlnkAddColumns.Attributes.Add("w", "690");
                    hlnkAddColumns.NavigateUrl = "/UpgradeLicense.aspx";
                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "addnewcolumn", "<script>ApplyFrameBox(this);</script>", false);
                    hlnkAddColumns.Attributes.Add("onclick", "return ApplyFrameBox(this);");
                    hlnkAddColumns.NavigateUrl = strAddColumns;
                }   

                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userInfo = sessionService.GetLoginUserDetailsSession();
                var featureService = ServiceFactory.GetService<FeatureService>();
                var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
                var listFeatures = featureService.Find(GenericHelper.ToEnum<UserType>(userInfo.LicenseType));

                


            }
           
            if (!IsPostBack)
            {
                //string listName = "";
                int ContactId = 0;
                var contactList = new osm_contactlist();
                // insert list if not exist
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var contactListService = ServiceFactory.GetService<ContactListService>();
                var userInfo = sessionService.GetLoginUserDetailsSession();
                var emailService = ServiceFactory.GetService<EmailService>();
              
                if (Request.QueryString["key"] != null)
                {
                    Hashtable ht = new Hashtable();
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains("EmailId"))
                    {
                        var EmailId = ValidationHelper.GetInteger(ht["EmailId"].ToString(), 0);
                        var email = emailService.GetEmailListByEmailId(EmailId);
                        contactList = contactListService.FindByContactListID(ValidationHelper.GetInteger(email.CONTACTLIST_ID, 0));
                        btnCancel.Style.Add("display", "block");


                      
                    }

                    //else
                    //{
                    //    btnCancel.Style.Add("display", "none");
                    //}
                    if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                    {
                        ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                    }
                    contactList = contactListService.FindByContactListID(ContactId);

                }

                BindControls(contactList);


            }      
        }


        protected void BindControls(osm_contactlist contactList)
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            //if (loggedInUserInfo.LicenseType == "FREE")
            //{
             //   ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            //}
            //else
            //{
                var emailService = ServiceFactory.GetService<EmailService>();
                var email = new osm_emaillist();

                if (contactList != null)
                {


                    if (contactList.CUSTOM_HEADER1 != null && contactList.CUSTOM_HEADER1.Trim() != "")
                    {
                        txtColumn1.Visible = true;
                        lblColumn1.Visible = true;
                        lblColumn1.Visible = true;
                        lblColumn1.Text = contactList.CUSTOM_HEADER1;

                    }
                    if (contactList.CUSTOM_HEADER2 != null && contactList.CUSTOM_HEADER2.Trim() != "")
                    {
                        txtColumn2.Visible = true;
                        lblColumn2.Visible = true;
                        lblColumn1.Visible = true;
                        lblColumn2.Text = contactList.CUSTOM_HEADER2;

                    }
                    //if (contactList.CUSTOM_HEADER3 != null && contactList.CUSTOM_HEADER3.Trim() != "")
                    //{
                    //    txtColumn3.Visible = true;
                    //    lblColumn3.Visible = true;
                    //    dvCol3.Visible = true;
                    //    lblColumn3.Text = contactList.CUSTOM_HEADER3;

                    //}
                    //if (contactList.CUSTOM_HEADER4 != null && contactList.CUSTOM_HEADER4.Trim() != "")
                    //{
                    //    dvCol4.Visible = true;
                    //    txtColumn4.Visible = true;
                    //    lblColumn4.Visible = true;
                    //    lblColumn4.Text = contactList.CUSTOM_HEADER4;

                    //}
                    //if (contactList.CUSTOM_HEADER5 != null && contactList.CUSTOM_HEADER5.Trim() != "")
                    //{
                    //    dvCol5.Visible = true;
                    //    txtColumn5.Visible = true;
                    //    lblColumn5.Visible = true;
                    //    lblColumn5.Text = contactList.CUSTOM_HEADER5;

                    //}
                    //if (contactList.CUSTOM_HEADER6 != null && contactList.CUSTOM_HEADER6.Trim() != "")
                    //{
                    //    dvCol6.Visible = true;
                    //    txtColumn6.Visible = true;
                    //    lblColumn6.Visible = true;
                    //    lblColumn6.Text = contactList.CUSTOM_HEADER6;

                    //}
                    //if (contactList.CUSTOM_HEADER7 != null && contactList.CUSTOM_HEADER7.Trim() != "")
                    //{
                    //    dvCol7.Visible = true;
                    //    txtColumn7.Visible = true;
                    //    lblColumn7.Visible = true;
                    //    lblColumn7.Text = contactList.CUSTOM_HEADER7;

                    //}
                    //if (contactList.CUSTOM_HEADER8 != null && contactList.CUSTOM_HEADER8.Trim() != "")
                    //{
                    //    dvCol8.Visible = true;
                    //    txtColumn8.Visible = true;
                    //    lblColumn8.Visible = true;
                    //    lblColumn8.Text = contactList.CUSTOM_HEADER8;

                    //}

                    if (Request.QueryString["key"] != null)
                    {
                        Hashtable ht = new Hashtable();
                        ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                        if (ht != null && ht.Count > 0 && ht.Contains("EmailId"))
                        {
                            int emailId = ValidationHelper.GetInteger(ht["EmailId"].ToString(), 0);

                            var emailDetails = emailService.GetEmailListByEmailId(emailId);
                            if (emailDetails != null)
                            {
                                if (emailDetails.FIRST_NAME != null && emailDetails.FIRST_NAME != "")
                                    txtFirstName.Text = emailDetails.FIRST_NAME.Trim();
                                if (emailDetails.LAST_NAME != null && emailDetails.LAST_NAME != "")
                                    txtLastName.Text = emailDetails.LAST_NAME.Trim();
                                if (emailDetails.CUSTOM_VAR1 != null && emailDetails.CUSTOM_VAR1 != "")
                                    txtColumn1.Text = emailDetails.CUSTOM_VAR1.Trim();
                                if (emailDetails.CUSTOM_VAR2 != null && emailDetails.CUSTOM_VAR2 != "")
                                    txtColumn2.Text = emailDetails.CUSTOM_VAR2.Trim();
                                //if (emailDetails.CUSTOM_VAR3 != null && emailDetails.CUSTOM_VAR3 != "")
                                //    txtColumn3.Text = emailDetails.CUSTOM_VAR3.Trim();
                                //if (emailDetails.CUSTOM_VAR4 != null && emailDetails.CUSTOM_VAR4 != "")
                                //    txtColumn4.Text = emailDetails.CUSTOM_VAR4.Trim();
                                //if (emailDetails.CUSTOM_VAR5 != null && emailDetails.CUSTOM_VAR5 != "")
                                //    txtColumn5.Text = emailDetails.CUSTOM_VAR5.Trim();
                                //if (emailDetails.CUSTOM_VAR6 != null && emailDetails.CUSTOM_VAR6 != "")
                                //    txtColumn6.Text = emailDetails.CUSTOM_VAR6.Trim();
                                //if (emailDetails.CUSTOM_VAR7 != null && emailDetails.CUSTOM_VAR7 != "")
                                //    txtColumn7.Text = emailDetails.CUSTOM_VAR7.Trim();
                                //if (emailDetails.CUSTOM_VAR8 != null && emailDetails.CUSTOM_VAR8 != "")
                                //    txtColumn8.Text = emailDetails.CUSTOM_VAR8.Trim();
                                txtEmail.Text = emailDetails.EMAIL_ADDRESS.Trim();
                            }
                            btnSave.Style.Add("display", "none");
                            btnUpdate.Style.Add("display", "block");
                        }
                    }
                    hdnContactListId.Value = contactList.CONTACTLIST_ID.ToString();
                    if (Request.QueryString["key"] != null)
                    {
                        Hashtable ht = new Hashtable();
                        ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                        if (ht != null && ht.Count > 0 && ht.Contains("Mode"))
                        {
                            if (ht["Mode"].ToString() == "Yes")
                            {
                                lblSuccessMsg.Text = "Email updated successfully.";
                                lblSuccessMsg.Visible = true;
                                dvSuccessMsg.Visible = true;
                                dvErrMsg.Visible = false;
                            }
                        }
                    }

                }
           // }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            //if (loggedInUserInfo.LicenseType == "FREE")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            //}
            //else
            //{

                btnUpdate.Style.Add("display", "none");
                btnSave.Style.Add("display", "block");
                string alrt = GetLocalResourceObject("lbl_alrt").ToString();   //"Please enter";
                this.lblInvalidEmail.Visible = true;
                bool flag = false;
                if ((Convert.ToString(txtEmail.Text.Trim()).Length == 0))
                {
                    alrt += GetLocalResourceObject("lbl_EmailAddress").ToString();// " email address.";
                    lblInvalidEmail.Text = alrt;

                }
                else if (Convert.ToString(txtEmail.Text.Trim()).Length > 0)
                {
                    Regex reLenient = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                    bool isEmail = reLenient.IsMatch(txtEmail.Text.Trim());
                    if (!isEmail)
                    {
                        alrt += GetLocalResourceObject("lbl_EmailValidAddress").ToString();    //" valid email address.";
                        lblInvalidEmail.Text = alrt;
                    }
                    else
                        flag = true;
                }

                if (flag)
                {
                    lblInvalidEmail.Visible = false;
                    SaveContactDetails();

                }

          //  }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            //if (loggedInUserInfo.LicenseType == "FREE")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            //}
            //else
            //{
                string alrt = GetLocalResourceObject("lbl_alrt").ToString();  //"Please enter";
                this.lblInvalidEmail.Visible = true;
                bool flag = false;
                if ((Convert.ToString(txtEmail.Text.Trim()).Length == 0))
                {
                    alrt += GetLocalResourceObject("lbl_EmailAddress").ToString();
                    lblInvalidEmail.Text = alrt;

                }
                else if (Convert.ToString(txtEmail.Text.Trim()).Length > 0)
                {
                    Regex reLenient = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                    bool isEmail = reLenient.IsMatch(txtEmail.Text.Trim());
                    if (!isEmail)
                    {
                        alrt += GetLocalResourceObject("lbl_EmailValidAddress").ToString();//" valid email address.";
                        lblInvalidEmail.Text = alrt;
                    }
                    else
                        flag = true;
                }



                if (flag)
                {
                    lblInvalidEmail.Visible = false;
                    SaveContactDetails();
                }

          //  }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            //if (loggedInUserInfo.LicenseType == "FREE")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            //}
            //else
            //{
                ClearData();
                if (Request.QueryString["key"] != null)
                {
                    Hashtable ht = new Hashtable();
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);

                    if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                    {
                        var ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                        var url = EncryptHelper.EncryptQuerystring("CreateEmailList.aspx", "ContactId=" + ht["ContactId"].ToString() + "&surveyFlag=" + base.SurveyFlag);
                        Response.Redirect(url);
                    }
                }

         //   }
        }
        
        private void ClearData()
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
            txtColumn1.Text = "";
            txtColumn2.Text = "";
            txtColumn3.Text = "";
            txtColumn4.Text = "";
            txtColumn5.Text = "";
            txtColumn6.Text = "";
            txtColumn7.Text = "";
            txtColumn8.Text = "";

        }

        protected void SaveContactDetails()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var emaillist = new osm_emaillist();
            DropDownList drpList = (DropDownList)Parent.FindControl("drpList");
            int contid = ValidationHelper.GetInteger(drpList.SelectedValue, 0);

            if (contid > 0)
            {
                emaillist.CONTACTLIST_ID = contid;
                emaillist.CUSTOM_VAR1 = txtColumn1.Text.Trim();
                emaillist.CUSTOM_VAR2 = txtColumn2.Text.Trim();
                emaillist.CUSTOM_VAR3 = txtColumn3.Text.Trim();
                emaillist.CUSTOM_VAR4 = txtColumn4.Text.Trim();
                emaillist.CUSTOM_VAR5 = txtColumn5.Text.Trim();
                emaillist.CUSTOM_VAR6 = txtColumn6.Text.Trim();
                emaillist.CUSTOM_VAR7 = txtColumn7.Text.Trim();
                emaillist.CUSTOM_VAR8 = txtColumn8.Text.Trim();
                emaillist.FIRST_NAME = txtFirstName.Text.Trim();
                emaillist.LAST_NAME = txtLastName.Text.Trim();
                emaillist.EMAIL_ADDRESS = txtEmail.Text.Trim();
                emaillist.LAST_MODIFIED_BY = userInfo.UserId;
                emaillist.MODIFIED_ON = DateTime.Now;
                emaillist.DELETED = 0;
                var unsubscribe = ServiceFactory.GetService<UnsubscribeService>().GetUnsubscribeListByUserId(userInfo.UserId, txtEmail.Text.Trim());
                if (unsubscribe.Count > 0)
                {
                    emaillist.STATUS = "Unsubscribe";
                }
                else
                {
                    emaillist.STATUS = "Active";
                }
                var emailService = ServiceFactory.GetService<EmailService>();

                if (Request.QueryString["key"] != null)
                {
                    Hashtable ht = new Hashtable();
                    var contactService = ServiceFactory.GetService<ContactListService>();


                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains("EmailId"))
                    {
                        int emailId = ValidationHelper.GetInteger(ht["EmailId"].ToString(), 0);
                        // lblTitle.Text = "Edit Email";
                        emaillist.EMAIL_ID = emailId;
                        var rtnValue = emailService.UpdateEmailsByList(emaillist);
                        if (rtnValue > 0)
                        {

                            if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                            {
                                var ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                                var url = EncryptHelper.EncryptQuerystring("CreateEmailList.aspx", "ContactId=" + ht["ContactId"].ToString() + "&Mode=Yes&surveyFlag=" + base.SurveyFlag);

                                Response.Redirect(url);
                                //  this.Page.ClientScript.RegisterStartupScript(this.GetType(), "AddEmail", "showHide('" + param + "','" + param2 + "');", true);
                            }
                            lblSuccessMsg.Text = GetLocalResourceObject("lblSuccessMsg_Updated").ToString() ;  //"Email Id has been updated successfully.";
                            dvSuccessMsg.Visible = true;
                            dvErrMsg.Visible = false;
                            lblSuccessMsg.Visible = true;
                            lblErrMsg.Visible = false;

                        }
                        else
                        {
                            dvSuccessMsg.Visible = false;
                            dvErrMsg.Visible = true;
                            lblErrMsg.Visible = true;
                            lblSuccessMsg.Visible = false;
                        }
                    }
                    else
                    {
                        emaillist.CREATED_BY = userInfo.UserId;
                        int rtnValue = emailService.SaveEmailsByList(emaillist);
                        if (rtnValue > 0)
                        {
                            lblSuccessMsg.Text = GetLocalResourceObject("lblSuccessMsg_Added").ToString();//"Email Id has been added successfully.";
                            dvSuccessMsg.Visible = true;
                            dvErrMsg.Visible = false;
                            lblSuccessMsg.Visible = true;
                            lblErrMsg.Visible = false;
                            ClearData();
                        }
                        else
                        {
                            dvSuccessMsg.Visible = false;
                            dvErrMsg.Visible = true;
                            lblErrMsg.Visible = true;
                            lblSuccessMsg.Visible = false;
                        }
                    }


                }



            }
        }
        
        protected void hlnkAddColumns_Click(object sender, EventArgs e)
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            if(loggedInUserInfo.LicenseType == "FREE")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "upgrade", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            }
            else{
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "addnewcolumn", "<script>ApplyFrameBox(this);</script>", false);
            }   
            //onclick='javascript: return ApplyFrameBox(this);'
        }
}
}