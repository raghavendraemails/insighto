﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProfileRightPanel.ascx.cs" Inherits="Insighto.UserControls.ProfileRightPanel" %>
<div class="paddingNPanel"><div class="signUpPnlRight">
		<div class="profileAccount"><!-- profile quick links -->
			<h3>Profile Quick Links</h3>
			<div class='suckerdiv'>
				 <ul id='suckertree1'>
					 <li ><a  runat="server" id="lnkMyProfile"><asp:Label ID="lblManageProfile" runat="server" Text="Manage Profile"></asp:Label></a></li>
					 <li><a runat="server" id="lnkInvoiceHisotry"><asp:Label ID="lblInvoiceHistory" runat="server" Text="Invoice History - Surveys"></asp:Label></a></li>
                      <li><a runat="server" id="lnkPollInvoiceHisotry"><asp:Label ID="lblPollInvoiceHistory" runat="server" Text="Invoice History - Polls"></asp:Label></a></li>
					 <li><a runat="server" id="lnkChangePassword"><asp:Label ID="lblChangePassword" runat="server" Text="Change Password"></asp:Label> </a></li>
					 <%--<li><a runat="server" id="lnkManageEmail">Manage Email</a></li>--%>
				 </ul>
			</div>
			<div class="clear"></div>
		<!-- //profile quick links --></div>

           <div class="profileAccount" id="dvmngpayments" runat="server"> <!-- folder quick links -->
			<h3>Manage Payments</h3>
			<div class='suckerdiv'>
				 <ul id='Ulmanagepayments' runat="server">
					 <li><a runat="server" id="lnkupdatepayment"><asp:Label ID="lblupdatepayment" runat="server" Text="Update payment method"></asp:Label></a></li>
					 <li><a runat="server" id="lnkcancelsubscription"><asp:Label ID="lblcancelsubscription" runat="server" Text="Cancel subscription"></asp:Label></a></li>
				 </ul>
			</div>
		 <!-- //folder quick links --></div>

		<div class="profileAccount"> <!-- folder quick links -->
			<h3>Folders Quick Links</h3>
			<div class='suckerdiv'>
				 <ul id='suckertree2'>
					 <li><a runat="server" id="lnkManageFolder"><asp:Label ID="lblManageFolders" runat="server" Text="Manage Folders"></asp:Label></a></li>
					 <li><a runat="server" id="lnkManageCategory"><asp:Label ID="lblManageCategory" runat="server" Text="Manage Category"></asp:Label></a></li>
				 </ul>
			</div>
		 <!-- //folder quick links --></div>

      
	</div></div>

    <script type="text/javascript" >
        $(document).ready(function () {
            $('a').each(function () {
                if (document.URL.indexOf($(this).attr('href')) >= 0) {
                    $(this).parentsUntil(".menu").addClass('active');
                }
            });
        });
    </script>