﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;

namespace Insighto.UserControls
{
    public partial class Footer : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string footerstring = GetLocalResourceObject("dvFooter").ToString();
            footerstring = footerstring.Replace("2014", DateTime.Now.Year.ToString());
            dvFooter.InnerHtml = footerstring;
              
        }
    }
}