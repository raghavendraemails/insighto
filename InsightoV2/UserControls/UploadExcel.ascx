﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UploadExcel.ascx.cs" Inherits="Insighto.Pages.UploadExcel" %>
<div class="popupContentPanel">
    <!-- popup content panel -->
    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccessMsg" runat="server" Visible="False" 
                Text="File has been uploaded successfully." 
                meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
    </div>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblErrMsg" runat="server" Visible="False" 
                meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
    </div>
    <div class="defaultH">
    </div>
    <div>
        <!-- form -->
        <div class="con_login_pnl">
            <div class="addNewEmaillbl01">
               </div>
            <div class="loginControls">
            <table border="0">
                    <tr>                       
                        <td>
                            <asp:RadioButton ID="rbtnUploadType"  AutoPostBack="True"
                                GroupName="uploadType" runat="server" Checked="True" 
                                OnCheckedChanged="rbtnUploadType_CheckedChanged" 
                                meta:resourcekey="rbtnUploadTypeResource1" />
                        </td>
                        <td>
                            <img src="App_Themes/Classic/Images/icon-excel.gif" alt="Excel" title="Excel" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rbtnUploadType2" runat="server" 
                                AutoPostBack="True" GroupName="uploadType" 
                                OnCheckedChanged="rbtnUploadType2_CheckedChanged" 
                                meta:resourcekey="rbtnUploadType2Resource1" />
                        </td>
                        <td>
                            <img src="App_Themes/Classic/Images/csv_logo2.png" alt="CSV" title="CSV" />
                        </td>
                        <td>
                            <span class="lt_padding"><a href="#" onclick="fnOpenWindow()" 
                                class="downloadLink1" title="Please note that all the headers in excel file should be exactly similar to the sample format provided below" alt="Please note that all the headers in excel file should be exactly similar to the sample format provided below">
                            <asp:Label ID="lblSampleFormat" runat="server" Text="Sample Format" 
                                meta:resourcekey="lblSampleFormatResource1"></asp:Label></a></span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

         <div class="con_login_pnl">
            <div class="addNewEmaillbl01 requirelbl01">
              <asp:Label ID="lblFileUpload" runat="server" Text="Select File to Upload" 
                    meta:resourcekey="lblFileUploadResource1"></asp:Label></div>
            <div class="loginControls"><asp:FileUpload ID="fileUpload" CssClass="textBoxMedium" 
                    runat="server" meta:resourcekey="fileUploadResource1" />
            </div>
        </div>
         <div class="con_login_pnl" style="display: none">
            <div class="reqlbl btmpadding">
                <asp:Label ID="Label3" Enabled="False" AssociatedControlID="reqFirstName" 
                    runat="server" meta:resourcekey="Label3Resource1" />
                <asp:RequiredFieldValidator SetFocusOnError="True"
                    ID="reqFirstName" CssClass="lblRequired" Enabled="False" ControlToValidate="txtEmailListName"
                     ErrorMessage="Please enter list name." runat="server"
                    Display="Dynamic" meta:resourcekey="reqFirstNameResource1"></asp:RequiredFieldValidator>
            </div>
        </div>
          <div class="con_login_pnl">
            <div class="reqlbl02 btmpadding">
                <asp:Label ID="Label1" AssociatedControlID="lblInvalidEmail" runat="server" 
                    meta:resourcekey="Label1Resource1" />
                <asp:Label ID="lblInvalidEmail" class="lblRequired" runat="server" Visible="False"
                                        Text="Please select a file to upload." 
                    meta:resourcekey="lblInvalidEmailResource1"></asp:Label>
                <%-- <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqFile" Display="Dynamic" CssClass="lblRequired"
                    ControlToValidate="fileUpload"  runat="server" ErrorMessage="Please select a file to upload."></asp:RequiredFieldValidator>
               --%><asp:RegularExpressionValidator SetFocusOnError="True"   Display="Dynamic" CssClass="lblRequired"
                    ID="regFile" ControlToValidate="fileUpload" runat="Server" ErrorMessage="Invalid format in the uploaded file. Please check sample preview."
                    ValidationExpression="^.*\.(xls|xlsx|csv|XLS|XLSX|CSV)$" 
                    meta:resourcekey="regFileResource1" />
            </div>
        </div>
        <div id="divValidators" runat="server">
        </div>
        <div class="con_login_pnl">
            <div class="addNewEmaillbl01">
                <label for="uploadType">
                </label>
            </div>
            <div class="loginControls">
                <asp:Button ID="btnSaveExcel" runat="server"  CssClass="dynamicButton"
                    Text="Upload" ToolTip="Upload" OnClick="btnSaveExcel_Click" 
                    meta:resourcekey="btnSaveExcelResource1" />
            </div>
        </div>
        <div class="con_login_pnl" style="display: none">
            <div class="loginlbl">
                <label for="emailList">
                   <asp:Label ID="lblEmaillIstName" runat="server" Text="Email List Name" 
                    meta:resourcekey="lblEmaillIstNameResource1"></asp:Label></label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtEmailListName" Enabled="False" 
                    Height="20px" runat="server" MaxLength="25" class="textBoxMedium" 
                    meta:resourcekey="txtEmailListNameResource1"></asp:TextBox>
            </div>
        </div>
       
        <!-- //form -->
    </div>
    <div class="clear">
    </div>
    <!-- //popup content panel -->
</div>

    <script type="text/javascript">
        function fnOpenWindow() {
            window.open("sampleEmail.htm", null,
    "height=480,width=850,status=yes,toolbar=no,menubar=no,location=no");

        }
    </script>
