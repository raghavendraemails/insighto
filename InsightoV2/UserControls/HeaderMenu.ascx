﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeaderMenu.ascx.cs" Inherits="Insighto.UserControls.HeaderMenu" %>
<%@ Register TagPrefix="uc" TagName="UpgradeNowNew" Src="~/UserControls/UpgradeNowNew.ascx" %>
<div id="header" style="background-color:White" runat="server">
    <!-- header -->
    <table width="100%">
    <tr>
    <td valign="top" align="left">
    <div class="insightoLogoPanel">
        <!-- insighto logo -->
        <asp:HyperLink ID="lnkLogo" Text="Insighto" ImageUrl="~/App_Themes/Classic/Images/insighto_logo.png"
            runat="server" Target="_blank" meta:resourcekey="lnkLogoResource1"></asp:HyperLink>
        <!-- insighto logo -->
    </div>
    </td>
    <td align="right">
        <table>
            <tr><td align="right" valign="top">
              <div style="float: right">
        <div>
            <div id="welcomePanel">
                <!-- welcome panel -->
                <ul>
                    <li>
                        <asp:Label ID="lblWelcome" CssClass="blackColor" runat="server" Text="Welcome " meta:resourcekey="lblWelcomeResource1"></asp:Label>
                        <asp:Label ID="lblLoginUser" runat="server" meta:resourcekey="lblLoginUserResource1" /></li><%--<span>Paul Martin</span>--%>
                    <li>
                        <asp:HyperLink ID="hlMyProfile" Text="My Profile" runat="server" meta:resourcekey="hlMyProfileResource1"></asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="hlFAQ" NavigateUrl="#" onclick="javascript: window.open('/Faqcms/CNS_AboutSurvey.aspx?key=O1IiHYYZiuqe67HGRNzwF7j%2btdpCU6IQFvQJRPSFZFVdz1jysLODHqPiBlwB3os07lCVCHakwMBdvY2JVZRBS7CpbynHlOEZ','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;"
                            runat="server" Text="FAQ" Visible="False" meta:resourcekey="hlFAQResource1"></asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="hlContact" Text="Contact Us" runat="server" meta:resourcekey="hlContactResource1"
                            h="520" w="700" scrolling='yes'></asp:HyperLink>
                    </li>
                    <li class="noRightBorder">
                        <asp:LinkButton ID="lbtnLogOut" runat="server" Text="Logout" OnClick="lbtnLogOut_Click"
                            ValidationGroup="Logout" meta:resourcekey="lbtnLogOutResource1" /></li>
                </ul>
                <!-- welcome panel -->
            </div>
        </div>
         <div class="clear">
    </div>
    </div>
            </td></tr>
            <tr><td align="right" valign="top">
             <div style="float:right;margin:15px 15px 0 0;position:relative;" >
            <uc:UpgradeNowNew ID="UpgradeNowNew1" runat="server" />
      </div>
            </td></tr>
    </table>    
    </td>
    </tr>
</table>
   
    
      
    <!-- //header -->
</div>
