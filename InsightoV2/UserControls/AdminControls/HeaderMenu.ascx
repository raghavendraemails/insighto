﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeaderMenu.ascx.cs" Inherits="Insigtho.Pages.UserControls.AdminControls.HeaderMenu" %>
<div id="header">
    <!-- header -->
    <div class="insightoLogoPanel">
        <!-- insighto logo -->
        <asp:HyperLink ID="lnkLogo" Text="Insighto" ImageUrl="~/App_Themes/Classic/Images/logo-insighto.jpg"
            NavigateUrl="~/Home.aspx" runat="server" 
            meta:resourcekey="lnkLogoResource1"></asp:HyperLink>
        <!-- insighto logo -->
    </div>
    <div class="insightoTagLine"></div>
    
    <div id="welcomePanelAdmin">
        <!-- welcome panel -->
        <ul>
            <li><asp:Label ID="lblWelcome" runat="server" Text="Welcome " 
                    meta:resourcekey="lblWelcomeResource1"></asp:Label>
                <asp:Label ID="lblLoginUser" runat="server" 
                    meta:resourcekey="lblLoginUserResource1" /></li><%--<span>Paul Martin</span>--%>
            <li class="noRightBorder">
                 <asp:LinkButton ID="lbtnLogOut" runat="server" Text="Logout" OnClick="lbtnLogOut_Click"
                                ValidationGroup="Logout" 
                     meta:resourcekey="lbtnLogOutResource1" /></li>
            
        </ul>
        <!-- welcome panel -->
    </div>
    <!-- //header -->
</div>
