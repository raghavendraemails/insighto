﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;

public partial class UserControls_AdminControls_TopMenu : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginAdminUserDetailsSession();
            hdnRole.Value = userInfo.AdminType;
            if (userInfo.AdminType == Constants.ManageAdmin)
            {
                //liNewsLetters.Visible = false;
                //liLicenseManagement.Visible = false;
                //liInvoices.Visible = false;
                //liPayment.Visible = false;
                //liPickList.Visible = false;
                //liNotification.Visible = false;
                dvAccounts.Visible = false; 

            }
            if (userInfo.AdminType == Constants.AccountAdmin)
            {
                liNewsLetters.Visible = false;
                liLicenseManagement.Visible = false;
                liInvoices.Visible = false;
                //liPayment.Visible = true;
                //liManageAdmin.Visible = false;
                //liManageBlocked.Visible = false;
                //liManageUser.Visible = false;
                //liPickList.Visible = false;
                //liCms.Visible = false;
                //liViewContractlist.Visible = false;
                //liNotification.Visible = false;
                dvAdministration.Visible = false; 

            }
            if (userInfo.AdminType == Constants.CustomerAdmin)
            {
                liLicenseManagement.Visible = false;
                liInvoices.Visible = true;
                liPayment.Visible = false;
                liManageBlocked.Visible = false;
                 liManageAdmin.Visible = false;
                liPickList.Visible = false;
                liCms.Visible = false;
                liViewContractlist.Visible = false;
                liNotification.Visible = false;
            }
            if (userInfo.AdminType == Constants.TechAdmin)
            {
                liNewsLetters.Visible = false;
                liLicenseManagement.Visible = true;
                liInvoices.Visible = false;
                liPayment.Visible = false;
                liManageAdmin.Visible = false;
                liManageUser.Visible = false;
                liViewContractlist.Visible = false;
                liCms.Visible = false;

            }

        }

    }
}