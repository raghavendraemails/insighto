﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class UserControls_AdminControls_TopLinks : UserControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var mode = ConfigurationManager.AppSettings["releaseMode"];
            var version = ConfigurationManager.AppSettings["versionNumber"];         

            divVersion.Visible = (mode != "Release");
            ltlVersion.Text = mode + " - " + version;
        }
    }
}