﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;

namespace Insigtho.Pages.UserControls.AdminControls
{
    public partial class HeaderMenu : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInAdminUserInfo loggedInAdminUserInfo = sessionStateService.GetLoginAdminUserDetailsSession();
            if (loggedInAdminUserInfo != null)
            {
                string name = loggedInAdminUserInfo.FirstName + " " + loggedInAdminUserInfo.LastName;
                if (name.Length > 25)
                {
                    lblLoginUser.Text = name.Substring(0, 25) + "...";
                }
                else
                    lblLoginUser.Text = name;

            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        protected void lbtnLogOut_Click(object sender, EventArgs e)
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            sessionStateService.EndAdminUserSession();
            Response.Redirect("Login.aspx");
        }

    }
}