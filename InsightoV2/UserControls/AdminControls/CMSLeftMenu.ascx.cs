﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Insighto.Business.Helpers;

namespace Insighto.UserControls
{
    public partial class CMSLeftMenu : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lnkAboutUs.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=AboutUs&Title=About Us");
            lnkAntiSpam.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=AntiSpam&Title=Anti spam policy");
            lnkWhyInsighto.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=WhyInsighto&Title=Why insighto");
            lnkPrivacyPlicy.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=PrivacyPolicy&Title=Privacy policy");
            lnkMarketing.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=Marketing&Title=Marketing");
            lnkCustomerRelations.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=Customer Relations&Title=Customer Relations");
            lnkSales.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=Sales&Title=Sales");
            lnkEducationInstitutions.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=Educational Institutions&Title=Educational Institutions");
            lnkHR.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=Human Resources&Title=Human Resources");
            lnkTerms.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=Terms_Conditions&Title=Terms_Conditions");
            lnkTakeaTour.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=TakeaTour&Title=Take a Tour");
        }
       
        protected string ReturnURL(string arguments)
        {
            return EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(),"Page=" + arguments);
        }
    }
}