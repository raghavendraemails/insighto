﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CMSLeftMenu.ascx.cs" Inherits="Insighto.UserControls.CMSLeftMenu" %>
<div class="cmsPagesMenu">
    <!-- menu -->
    <div class='suckerdiv'>
        <ul id='suckertree1'>
            <li><a id="lnkAboutUs" runat="server"><asp:Label ID="lblAboutUs" runat="server" 
                    Text="About Us" meta:resourcekey="lblAboutUsResource1"></asp:Label></a></li>
            <li><a id="lnkAntiSpam" runat="server"><asp:Label ID="lblAntiSpampolicy" 
                    runat="server" Text="Anti spam policy" 
                    meta:resourcekey="lblAntiSpampolicyResource1"></asp:Label></a></li>
            <li><a id="lnkWhyInsighto" runat="server"><asp:Label ID="lblWhyInsighto" 
                    runat="server" Text="Why Insighto" meta:resourcekey="lblWhyInsightoResource1"></asp:Label></a></li>
            <li><a id="lnkPrivacyPlicy" runat="server"><asp:Label ID="lblPrivacyPolicy" 
                    runat="server" Text="Privacy policy" 
                    meta:resourcekey="lblPrivacyPolicyResource1"></asp:Label></a></li>
            <li><a id="lnkMarketing" runat="server"><asp:Label ID="lblMarketing" runat="server" 
                    Text="Marketing" meta:resourcekey="lblMarketingResource1"></asp:Label></a></li>
            <li><a id="lnkCustomerRelations" runat="server">
                <asp:Label ID="lblCustomerRelations" runat="server" Text="Customer Relations" 
                    meta:resourcekey="lblCustomerRelationsResource1"></asp:Label></a></li>
            <li><a id="lnkSales" runat="server"><asp:Label ID="lblSales" runat="server" 
                    Text="Sales" meta:resourcekey="lblSalesResource1"></asp:Label></a></li>
            <li><a id="lnkEducationInstitutions" runat="server">
                <asp:Label ID="lblEducationInstitutions" runat ="server" 
                    Text="Educational Institutions" 
                    meta:resourcekey="lblEducationInstitutionsResource1"></asp:Label></a></li>
            <li><a id="lnkHR" runat="server"><asp:Label ID="lblHumanResources" runat="server" 
                    Text="Human Resources" meta:resourcekey="lblHumanResourcesResource1"></asp:Label></a></li>
            <li><a id="lnkTerms" runat="server"><asp:Label ID="lblTermsConditions" 
                    runat="server" Text="Terms & Conditions" 
                    meta:resourcekey="lblTermsConditionsResource1"></asp:Label></a></li>
            <li><a id="lnkTakeaTour" runat="server"><asp:Label ID="lblTakeaTour" runat="server" 
                    Text="Take a Tour" meta:resourcekey="lblTakeaTourResource1"></asp:Label></a></li>
        </ul>
    </div>
    <!-- //menu -->
</div>
 <script type="text/javascript" >
     $(document).ready(function () {
         $('a').each(function () {
             if (document.URL.indexOf($(this).attr('href')) >= 0) {
                 $(this).parentsUntil(".menu").addClass('active');
             }
         });
     });
    </script>
