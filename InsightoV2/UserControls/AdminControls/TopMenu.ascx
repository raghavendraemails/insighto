﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopMenu.ascx.cs" Inherits="UserControls_AdminControls_TopMenu" %>
<div id="topMainMenu"><!-- top main menu -->
		<div id="mainNavWrapper">
        <div id="mainNavLeft">
            <ul id="ulAdminTopNav">
              <li id="navPlatform">	
              <div id="dvAdministration" runat="server">		  
			  <a id="navPlatformA" href="#"><asp:Label ID="lblAdministration" runat="server" 
                      Text="Administration" meta:resourcekey="lblAdministrationResource1"></asp:Label></a>
                <div class="navOverlayAdminSub">
			          <div class="navOverlayBuckets">
					        <ul class="reportsMenu">
						        <li id="liManageAdmin" runat="server"><asp:HyperLink ID="hlkManageAdmin" 
                                        NavigateUrl="~/Admin/ManageAdmin.aspx" Text="Manage Admins" runat="server" 
                                        meta:resourcekey="hlkManageAdminResource1"></asp:HyperLink></li>
								<li id="liManageUser" runat="server"><asp:HyperLink Id="hlkManagerUser" 
                                        NavigateUrl ="../../Admin/ManageUsers.aspx" Text="Manage Users"  runat="server" 
                                        meta:resourcekey="hlkManagerUserResource1" ></asp:HyperLink></li>
								<%--<li id ="liManagerRole" runat="server"><asp:HyperLink ID ="hlkManageRole" NavigateUrl ="~/Admin/ManageRole.aspx" runat="server" >Manage Roles</asp:HyperLink></li>--%>
								<%--<li><a href="RolesVsPermissions.html">Roles Vs Permissions</a></li>--%>
								<li id="liManageBlocked" runat="server"><asp:HyperLink ID="hlkManageBlocked" 
                                        NavigateUrl="~/Admin/ManagedBlockedWords.aspx" Text="Manage Blocked Words"  
                                        runat="server" meta:resourcekey="hlkManageBlockedResource1"></asp:HyperLink></li>
								<li id="liViewContractlist" runat="server"><asp:HyperLink ID ="hlkViewContractlist" 
                                        runat="server" NavigateUrl="~/Admin/ViewContactlistusers.aspx" 
                                        Text="Contact Users List" meta:resourcekey="hlkViewContractlistResource1"></asp:HyperLink></li>
								<li id ="liPickList" runat="server" ><asp:HyperLink ID="hlkPickList" 
                                        NavigateUrl="~/Admin/PickListManagement.aspx" runat="server" 
                                        Text="Pick List Management" meta:resourcekey="hlkPickListResource1" ></asp:HyperLink></li>
                                <li id ="liCms" runat="server"><asp:HyperLink ID="hlkCms" runat ="server" 
                                        NavigateUrl="~/Admin/ContentManagement.aspx" Text="CMS" 
                                        meta:resourcekey="hlkCmsResource1"></asp:HyperLink></li>
                                <li id="liNotification" runat="server"><asp:HyperLink ID="hlkNotification" 
                                        runat ="server" NavigateUrl="~/Admin/Notifications.aspx" Text="Notifications" 
                                        meta:resourcekey="hlkNotificationResource1" ></asp:HyperLink></li>
					        </ul>
			           </div>
		        </div>
                </div>
              </li>
              <li id="navCustomerSuccess">
              <div id="dvAccounts" runat="server">
              <a id="navCustomerSuccessA" href="#"><asp:Label ID="lblAccounts" runat="server" 
                      Text="Accounts" meta:resourcekey="lblAccountsResource1"></asp:Label></a>
                <div class="navOverlayAccountsSub">
                  <div class="navOverlayBuckets">
				        <ul class="reportsMenu">
					        <li id="liNewsLetters" runat="server"><asp:HyperLink ID="hlkNewsletters" 
                                    runat="server"  NavigateUrl="~/Admin/NewsLetters.aspx" Text="News Letters" 
                                    meta:resourcekey="hlkNewslettersResource1" ></asp:HyperLink></li>
							<li id="liLicenseManagement" runat="server"><asp:HyperLink ID="hlkLicenseMangement" 
                                    runat="server" NavigateUrl="~/Admin/LicenseManagement.aspx" 
                                    Text="License Management" meta:resourcekey="hlkLicenseMangementResource1"></asp:HyperLink></li>
                            <li id="liInvoices" runat="server"><asp:HyperLink ID="hlnkInvoices" 
                                    NavigateUrl="~/Admin/ViewPendingInvoices.aspx" runat="server" 
                                    Text="View Pending Invoices" meta:resourcekey="hlnkInvoicesResource1"></asp:HyperLink></li>
                            <li id="liPayment" runat="server"><asp:HyperLink ID="hlnkPayment" 
                                    NavigateUrl="~/Admin/ViewPaymentPendingUsers.aspx" runat="server" 
                                    Text="Payment Pending Users" meta:resourcekey="hlnkPaymentResource1"></asp:HyperLink></li>							
				        </ul>
                  </div>
		          
                </div>
                </div>
              </li>     
              <li id="navAboutEloqua">              
              <a id="navAboutEloquaA" href="#"><asp:Label ID="lblMyProfile" runat="server" 
                      Text="My Profile" meta:resourcekey="lblMyProfileResource1"></asp:Label></a>
                <div class="navOverlayAbout">
                  <div class="navOverlayBuckets">
			        <ul class="reportsMenu">
				        <li><asp:HyperLink ID ="hlkEditProfile" runat="server"  
                                NavigateUrl="~/Admin/EditMyprofile.aspx" Text="Edit My Information" 
                                meta:resourcekey="hlkEditProfileResource1" ></asp:HyperLink></li>
				        <li><asp:HyperLink ID ="hlkChangepassword" runat="server"  
                                NavigateUrl="~/Admin/Changepassword.aspx" Text="Change Password" 
                                meta:resourcekey="hlkChangepasswordResource1"></asp:HyperLink></li>
			        </ul>
		          </div>
                </div>
              </li>
            </ul>
          </div>
          <asp:HiddenField ID="hdnRole" runat ="server" /> 
    </div>
		
	<!-- top main menu --></div>
	<div class="clear"></div>
   <script type="text/javascript" >
       $(document).ready(function () {
           $('a').each(function () {
               if (document.URL.indexOf($(this).attr('href')) >= 0) {
                   $(this).parentsUntil(".menu").addClass('active');
               }
           });
       });
    </script>