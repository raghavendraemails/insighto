﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using App_Code;

namespace Insighto.UserControls
{
    public partial class SurveyQuestionnaire : UserControlBase
    {
        #region "Variables"

        List<SurveyQuestionAndAnswerOptions> surveyQuestionsAllList;
        List<SurveyQuestionAndAnswerOptions> surveyQuestionsList;
        Hashtable typeHt = new Hashtable();
        int surveyId = 0;

        #endregion

        public string Key
        {
            get
            {
                return HttpUtility.UrlEncode(QueryStringHelper.GetString("Key"));
            }
        }
        public int QuestionSeq
        {
            get
            {
                Hashtable ht = new Hashtable();
                int expandedQns = 0;
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Contains("QuestionSeq"))
                {
                    expandedQns = ValidationHelper.GetInteger(ht["QuestionSeq"].ToString(), 0);
                }
                return expandedQns;
            }
        }
        #region "Event : Page_Load"
        /// <summary>
        /// Page Load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = true;
            dvErrMsg.Visible = false;
            dvsuccess.Visible = false;

            if (!IsPostBack)
            {
                BindQuestionnaire();
                if (QuestionSeq > 0)
                {
                    string strParam1 = "divQuestionDetails" + QuestionSeq.ToString();
                    string strParam2 = "spanExpand" + QuestionSeq.ToString();
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ExpandSingle", "showHide('" + strParam1 + "','" + strParam2 + "');", true);

                    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showHide", "showHide(divQuestionDetails6,spanExpand6);", true);
                }
                hlnkCopyFromSurvey.ToolTip = "Copy questions from existing " + SurveyMode.ToLower();
            }

            //if (QuestionSeq <=0)
            //{
            //    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ExpandAll", "ExpandAll();", true);
            //}

        }
        #endregion


        #region "Method : BindQuestionnaire()"
        /// <summary>
        /// This method is used to bind the questionnaire based on SurveyId.
        /// </summary>

        private void BindQuestionnaire()
        {

            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());

            }

            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            ViewState["STATUS"] = "";
            var surveySer = ServiceFactory.GetService<SurveyService>();
            var surveyDet = surveySer.GetSurveyBySurveyId(surveyId);

            if (surveyDet.Any())
            {
                if (surveyDet[0].STATUS == SurveyStatus.Active.ToString())
                {
                    ViewState["STATUS"] = surveyDet[0].STATUS;
                }
            }


            string strTemplateLink = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyTemplateURL(), Constants.SURVEYID + "=" + surveyId + "&surveyName=" + surveyDet[0].SURVEY_NAME + "&surveyFlag=" + base.SurveyFlag);
            string strSurveyLink = EncryptHelper.EncryptQuerystring(PathHelper.GetCopyExsistingSurveyURL(), Constants.SURVEYID + "=" + surveyId + "&surveyName=" + surveyDet[0].SURVEY_NAME + "&surveyFlag=" + base.SurveyFlag);
            string strSavetowordLink = EncryptHelper.EncryptQuerystring("~/ReportDownloader.ashx", Constants.SURVEYID + "=" + surveyId + "&Mode=&surveyFlag=" + base.SurveyFlag + "&ReportType=SaveToWord");

            if (ViewState["STATUS"].ToString() == SurveyStatus.Active.ToString())
            {
                if (loggedInUserInfo.LicenseType == "FREE")
                {
                    hlnkCopyFromTemplate.NavigateUrl = "../#";
                    hlnkCopyFromTemplate.Enabled = false;
                    hlnkCopyFromTemplate.ToolTip = (String)GetLocalResourceObject("hlnkCopyQuestionTemplateFree");  //"Copying this template will only copy questions and number of questions available for free subscription.";

                    //hlnkSaveToWord.NavigateUrl = "../#";
                    //hlnkSaveToWord.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                    hlnkSaveToWord.NavigateUrl = strSavetowordLink;
                
                }
                else
                {
                    hlnkCopyFromTemplate.NavigateUrl = "javascript:void(0)";
                    hlnkCopyFromTemplate.Style.Add("cursor", "default");
                    hlnkCopyFromTemplate.ToolTip = (String)GetLocalResourceObject("hlnkCopyQuestionTemplate");   //"Copy questions from template";
                    hlnkSaveToWord.NavigateUrl = strSavetowordLink;
                }

                hlnkCopyFromSurvey.NavigateUrl = "javascript:void(0);";
                hlnkCopyFromSurvey.Style.Add("cursor", "default");


            }
            else
            {

                if (loggedInUserInfo.LicenseType == "FREE")
                {
                    hlnkCopyFromTemplate.NavigateUrl = "../#";
                    hlnkSaveToWord.NavigateUrl = strSavetowordLink;
                    //hlnkSaveToWord.NavigateUrl = "../#";
                    //hlnkSaveToWord.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                }
                else
                {

                    hlnkSaveToWord.NavigateUrl = strSavetowordLink;
                }

                hlnkCopyFromTemplate.NavigateUrl = strTemplateLink;
                hlnkCopyFromTemplate.ToolTip = (String)GetLocalResourceObject("hlnkCopyQuestionTemplate");  //"Copy questions from template";
                hlnkCopyFromSurvey.NavigateUrl = strSurveyLink;

            }


            var surveyService = ServiceFactory.GetService<RespondentService>();

            if (surveyDet[0].USERID == loggedInUserInfo.UserId)
            {
                surveyQuestionsAllList = surveyService.GetSurveyQuestionAndAnswerOptionsList(surveyId, false, true);
                surveyQuestionsList = surveyService.GetSurveyQuestionAndAnswerOptionsList(surveyId, false, false);
            }
            else
            {
                Response.Write("Sorry!! unexpected error. please try again");
                Response.End();
            }
            var QuestionsList = from sq in surveyQuestionsList
                                group sq by sq.PageIndex into sqg
                                select new { PageIndex = sqg.Key, surveyQuestions = sqg };
            hfQuestionsCount.Value = surveyQuestionsAllList.Count().ToString();


            chkExpandAllQuestions.Enabled = surveyQuestionsAllList.Count() > 0 ? true : false;


            lstQuestionTypes.DataSource = QuestionsList;
            lstQuestionTypes.DataBind();

            var questionService = ServiceFactory.GetService<QuestionService>();

            if (questionService.CheckQuestions(surveyId))
            {
                chkPagebreak.Enabled = true;
                chkQuestionMadatory.Enabled = true;


                bool isAnyPageBreak = questionService.CheckPageBreaksAllQuestions(surveyId, 0);
                chkPagebreak.Checked = isAnyPageBreak == true ? false : true;

                bool isAnyRR = questionService.CheckResponseRequiredAllQuestions(surveyId, 0);
                chkQuestionMadatory.Checked = isAnyRR == true ? false : true;

                bool isDisablePageBreak = questionService.CheckPageBreakForDisable(surveyId);
                if (isDisablePageBreak == false)
                {
                    chkPagebreak.Checked = false;
                    chkPagebreak.Enabled = false;
                }

                bool isDisableResonsRequired = questionService.CheckResponseRequiredForDisable(surveyId);
                if (isDisableResonsRequired == false)
                {
                    chkQuestionMadatory.Enabled = false;
                    chkQuestionMadatory.Checked = false;
                }
            }
            else
            {
                chkPagebreak.Enabled = false;
                chkQuestionMadatory.Enabled = false;

                hlnkSaveToWord.ToolTip = (String)GetLocalResourceObject("hlnkSaveToWordNoQuestion");  //"No question available to save to word.";
                hlnkSaveToWord.NavigateUrl = "../#";
                //hlnkReOrder.Enabled = false;
            }
        }

        #endregion

        #region "Event : lstQuestionTypesItems_ItemDataBound"
        /// <summary>
        /// In this event to bind the values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void lstQuestionTypesItems_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                ListViewDataItem dataItem = (ListViewDataItem)e.Item;
                int questionSequence = ValidationHelper.GetInteger(((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_TYPE_ID.ToString(), 0);
                if (questionSequence == 16)
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = "N";
                }
                else if (questionSequence == 17)
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = "P";
                }
                else if (questionSequence == 18)
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = "Q";
                }
                else if (questionSequence == 19)
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = "I";
                }
                else if (questionSequence == 21)
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = "V";
                }
                else
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).RankIndex.ToString();
                }
                string strQuerystring = Constants.SURVEYID + "=" + ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.SURVEY_ID.ToString() + "&QuestionType=" + ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_TYPE_ID.ToString() + "&QuestionId=" + ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_ID.ToString() + "&QuestionSeq=" + ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_SEQ.ToString() + "&surveyFlag=" + base.SurveyFlag;
                string strPage = "Questionnaire";
                string strEditLink = EncryptHelper.EncryptQuerystring(PathHelper.GetAddNewQuestionURL(), "Page=" + strPage + "&Mode=Edit&" + strQuerystring);
                //string strCopyLink = EncryptHelper.EncryptQuerystring(PathHelper.GetAddNewQuestionURL(), "Page=" + strPage + "&Mode=Copy&" + strQuerystring);
                string strCopyLink = EncryptHelper.EncryptQuerystring("~/CopyQuestion.aspx", "Page=" + strPage + "&Mode=Copy&" + strQuerystring);
                string strAddLink = EncryptHelper.EncryptQuerystring(PathHelper.GetAddNewQuestionURL(), "Page=" + strPage + "&Mode=Add&" + strQuerystring);
                string strReorderLink = EncryptHelper.EncryptQuerystring(PathHelper.GetReorderURL(), strQuerystring);

                string strSkipLogic;
                if (SetFeaturesSkip("SKIPLOGIC") == 0)
                {
                    strSkipLogic = EncryptHelper.EncryptQuerystring(PathHelper.GetSkipLogicURL(), "Edit=0&" + strQuerystring);

                    // strSkipLogic = "~/UpgradeLicense.aspx";
                }
                else
                    strSkipLogic = EncryptHelper.EncryptQuerystring(PathHelper.GetSkipLogicURL(), "Edit=0&" + strQuerystring);


                string strSkipLogicRemove;
                if (SetFeaturesSkip("SKIPLOGIC") == 0)
                    strSkipLogicRemove = "~/UpgradeLicense.aspx";
                else
                    strSkipLogicRemove = EncryptHelper.EncryptQuerystring(PathHelper.GetSkipLogicURL(), "Edit=1&" + strQuerystring);

                var questionTypeId = GenericHelper.ToEnum<QuestionType>(ValidationHelper.GetInteger(((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_TYPE_ID, 0));


                HyperLink hlnkEdit = e.Item.FindControl("hlnkEdit") as HyperLink;
                //if (ViewState["STATUS"].ToString() == SurveyStatus.Active.ToString() && questionTypeId == QuestionType.ConstantSum)
                //{
                //    hlnkEdit.NavigateUrl = "javascript:void(0)";
                //    hlnkEdit.Attributes.Remove("rel");
                //    hlnkEdit.Style.Add("cursor", "default");

                //}
                //else
                //{
                //    hlnkEdit.NavigateUrl = strEditLink;
                //}
                hlnkEdit.NavigateUrl = strEditLink;
                HyperLink hlnkCopy = e.Item.FindControl("hlnkCopy") as HyperLink;
                hlnkCopy.NavigateUrl = strCopyLink;

                HyperLink hlnkAdd = e.Item.FindControl("hlnkAdd") as HyperLink;
                hlnkAdd.NavigateUrl = strAddLink;

                LinkButton lbtnDelete = e.Item.FindControl("lbtnDelete") as LinkButton;
                //lbtnDelete.Attributes.Add("onclick", "return deleteConfirmation");
                lbtnDelete.Attributes.Add("onclick", "Javascript:return deleteConfirmation();");

                Image imgPageBreak = e.Item.FindControl("imgPageBreak") as Image;
                LinkButton lbtnPageBreak = e.Item.FindControl("lbtnPageBreak") as LinkButton;


                HyperLink hlnkReOrder = e.Item.FindControl("hlnkReOrder") as HyperLink;
                hlnkReOrder.NavigateUrl = strReorderLink;

                if (questionTypeId == QuestionType.PageHeader || questionTypeId == QuestionType.QuestionHeader || questionTypeId == QuestionType.Image || questionTypeId == QuestionType.Video)
                {
                    lbtnPageBreak.CssClass = "icon-pageBreak-normal";
                    lbtnPageBreak.ToolTip = (String)GetLocalResourceObject("stringCannotApplyPageBreak"); //"Cannot apply page break";
                    lbtnPageBreak.Enabled = false;
                }
                else
                {
                    var currentQuestion = ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question;
                    lbtnPageBreak.CssClass = GetCssClassByPageBreak(Utilities.ToEnum<PageBreakType>(currentQuestion.PAGE_BREAK));
                    lbtnPageBreak.ToolTip = (String)GetLocalResourceObject("stringRemovePageBreak"); //"Remove or apply page break";
                    lbtnPageBreak.Enabled = true;
                }

                Image imgSkipLogic = e.Item.FindControl("imgSkipLogic") as Image;
                HyperLink hlnkSkipLogic = e.Item.FindControl("hlnkSkipLogic") as HyperLink;

                Image imgSkipLogic1 = e.Item.FindControl("imgSkipLogic1") as Image;
                HyperLink hlnkSkipLogic1 = e.Item.FindControl("hlnkSkipLogic1") as HyperLink;

                if (questionTypeId == QuestionType.MultipleChoice_SingleSelect || questionTypeId == QuestionType.Choice_YesNo || questionTypeId == QuestionType.Menu_DropDown)
                {
                    if (((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.SKIP_LOGIC.ToString() == "1")
                    {
                        hlnkSkipLogic.CssClass = "icon-skipLogic-red";
                        hlnkSkipLogic.ToolTip = (String)GetLocalResourceObject("stringRemoveSkip");  //"Remove skip logic";
                        hlnkSkipLogic.NavigateUrl = strSkipLogicRemove;
                        hlnkSkipLogic.Attributes.Add("rel", "framebox");
                        hlnkEdit.NavigateUrl = "javascript:void(0);";
                        hlnkEdit.Attributes.Add("onclick", "javascript:return ShowWarningForEdit()");
                        hlnkEdit.Attributes.Remove("rel");

                    }
                    else
                    {
                        hlnkSkipLogic.CssClass = "icon-skipLogic-blue";
                        hlnkSkipLogic.ToolTip = (String)GetLocalResourceObject("stringApplySkip"); //"Apply skip logic";
                        hlnkSkipLogic.NavigateUrl = "../#";
                        //ApplySkipLogic
                        //hlnkSkipLogic.Attributes.Add("rel", "confirmSkipLogic1");
                        string strApplySkip = "Javascript:return ApplySkipLogic('" + strSkipLogic.Replace("~/", "") + "');";
                        hlnkSkipLogic.Attributes.Add("onclick", strApplySkip);
                        hlnkEdit.Attributes.Add("rel", "framebox");
                        hlnkEdit.Attributes.Remove("onclick");
                    }
                }
                else
                {
                    hlnkSkipLogic.CssClass = "icon-skipLogic-normal";
                    hlnkSkipLogic.ToolTip = (String)GetLocalResourceObject("stringCannotApplySkip");  //"Cannot apply skip logic";
                    hlnkSkipLogic.NavigateUrl = "../#";
                }


                Image imgRequired = e.Item.FindControl("imgRequired") as Image;
                LinkButton lbtnRequired = e.Item.FindControl("lbtnRequired") as LinkButton;

                if (questionTypeId == QuestionType.Narration || questionTypeId == QuestionType.PageHeader || questionTypeId == QuestionType.QuestionHeader || questionTypeId == QuestionType.Image || questionTypeId == QuestionType.Video)
                {
                    lbtnRequired.CssClass = "icon-response-normal";
                    lbtnRequired.ToolTip = (String)GetLocalResourceObject("stringCannotApplyResponse");// "Cannot apply response required";
                    lbtnRequired.Enabled = false;
                }
                else
                {
                    if (((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.RESPONSE_REQUIRED.ToString() == "1")
                    {
                        lbtnRequired.CssClass = "icon-response-red";
                        lbtnRequired.ToolTip = (String)GetLocalResourceObject("stringRemoveResponse");  //"Remove response required";
                        lbtnRequired.Enabled = true;
                    }
                    else
                    {
                        lbtnRequired.CssClass = "icon-response-blue";
                        lbtnRequired.ToolTip = (String)GetLocalResourceObject("stringApplyResponse");  //"Apply response required";
                        lbtnRequired.Enabled = true;
                    }
                }


                if (ViewState["STATUS"].ToString() == SurveyStatus.Active.ToString())
                {
                    hlnkAdd.Attributes.Add("rel", "");
                    hlnkCopy.Attributes.Add("rel", "");

                    hlnkAdd.NavigateUrl = "../#";
                    hlnkCopy.NavigateUrl = "../#";

                    hlnkAdd.CssClass = "icon-add-inactive";
                    hlnkCopy.CssClass = "icon-copy-inactive";

                    lbtnDelete.Attributes.Add("onclick", "");
                    lbtnDelete.CssClass = "icon-delete-inactive";
                    lbtnDelete.Enabled = false;

                    hlnkSkipLogic.Attributes.Add("onclick", "");
                    hlnkSkipLogic.CssClass = "icon-skip-inactive";
                    hlnkSkipLogic.NavigateUrl = "../#";
                }



                HtmlGenericControl divQuesAnswers = e.Item.FindControl("divQuesAnswers") as HtmlGenericControl;
                TextBox txtOther = e.Item.FindControl("txtOther") as TextBox;
                Label lblOther = e.Item.FindControl("lblOther") as Label;
                int questionId = ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_ID;
                var questionType = GenericHelper.ToEnum<QuestionType>(ValidationHelper.GetInteger(((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_TYPE_ID, 0));

                BindAnswerControls(divQuesAnswers, txtOther, lblOther, questionId, questionType);

            }
        }

        private string GetCssClassByPageBreak(PageBreakType pageBreakType)
        {
            switch (pageBreakType)
            {
                case PageBreakType.NoPageBreakWithNoSkip:
                    return "icon-pageBreak-blue";

                case PageBreakType.PageBreak:
                    return "icon-pageBreak-red";

                case PageBreakType.NoPageBreakWithSkip:
                    return "icon-pageBreak-skip-no";

                case PageBreakType.PageBreakWithSkip:
                    return "icon-pageBreak-skip-yes";

                default:
                    return "icon-pageBreak-blue";
            }
        }

        #endregion


        private void BindAnswerControls(HtmlGenericControl divQuesAnswers, TextBox txtOther, Label lblOther, int questionId, Insighto.Business.Enumerations.QuestionType questionType)
        {
            SurveyCore othertext = new SurveyCore();
            var surveyQuestion = surveyQuestionsAllList.Where(s => s.Question.QUESTION_ID == questionId).FirstOrDefault();
            //TextBox txtOther = new TextBox();
            HtmlTable tblContactInfo = new HtmlTable();
            tblContactInfo.CellPadding = 2;
            tblContactInfo.CellSpacing = 2;
            HtmlGenericControl divAnswers = new HtmlGenericControl();
            //tblContactInfo.Width = "100%";

            switch (questionType)
            {
                case QuestionType.MultipleChoice_MultipleSelect:
                    divAnswers = new HtmlGenericControl();
                    divAnswers.Attributes.Add("class", "singleSelectRadio");
                    CheckBoxList chkMultipleSelect = new CheckBoxList();
                    ServiceFactory<RespondentMultipleQuestions>.Instance.CreateMOMSQuestion(chkMultipleSelect,
                                                                                            surveyQuestion, "chk", null,
                                                                                            txtOther,
                                                                                            surveyQuestion.Question.
                                                                                                RANDOMIZE_ANSWERS > 0);
                  //  divAnswers.Controls.Add(chkMultipleSelect);
                    txtOther.Visible = surveyQuestion.Question.OTHER_ANS > 0;
                  //  lblOther.Visible = txtOther.Visible;
                  //  lblOther.Text = othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID));
                    if (txtOther.Visible == true)
                    {
                        chkMultipleSelect.Items.Add(othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID)));
                    }
                        divAnswers.Controls.Add(chkMultipleSelect);  
                  divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.Menu_DropDown:
                    DropDownList ddlMenu = new DropDownList();
                    ServiceFactory<RespondentMultipleQuestions>.Instance.CreateDropdownMenuQuestion(ddlMenu, surveyQuestion,
                                                                                                    "ddl", null, txtOther,
                                                                                                    surveyQuestion.Question.
                                                                                                        RANDOMIZE_ANSWERS >
                                                                                                    0, true);
                    divAnswers.Controls.Add(ddlMenu);
                    txtOther.Visible = surveyQuestion.Question.OTHER_ANS > 0;
                    lblOther.Visible = txtOther.Visible;
                    lblOther.Text = othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID));
                    //if (txtOther.Visible == true)
                    //{
                    //    ddlMenu.Items.Add(othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID)));
                    //}
                    //   divAnswers.Controls.Add(ddlMenu); 
                   divQuesAnswers.Controls.Add(divAnswers);

                    break;

                case QuestionType.Choice_YesNo:
                    RadioButtonList rbChoiceYesNo = new RadioButtonList();
                    ServiceFactory<RespondentMultipleQuestions>.Instance.CreateYesNoOptionQuestion(rbChoiceYesNo,
                                                                                                   surveyQuestion, "rbl",
                                                                                                   null, txtOther,
                                                                                                   surveyQuestion.Question.
                                                                                                       RANDOMIZE_ANSWERS > 0);

                    //divAnswers.Controls.Add(rbChoiceYesNo);
                    txtOther.Visible = surveyQuestion.Question.OTHER_ANS > 0;
                  //  lblOther.Visible = txtOther.Visible;
                  //  lblOther.Text = othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID));
                    if (txtOther.Visible == true)
                    {
                        rbChoiceYesNo.Items.Add(othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID)));
                    }
                      divAnswers.Controls.Add(rbChoiceYesNo); 
                  divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.MultipleChoice_SingleSelect:
                    divAnswers = new HtmlGenericControl();
                    divAnswers.Attributes.Add("class", "singleSelectRadio");
                    RadioButtonList rbSingleSelect = new RadioButtonList();
                    ServiceFactory<RespondentMultipleQuestions>.Instance.CreateMossQuestion(rbSingleSelect, surveyQuestion,
                                                                                            "rbl", null, txtOther,
                                                                                            surveyQuestion.Question.
                                                                                                RANDOMIZE_ANSWERS > 0);

                   // divAnswers.Controls.Add(rbSingleSelect);
                    
                    
                    txtOther.Visible = surveyQuestion.Question.OTHER_ANS > 0;
                   // lblOther.Visible = txtOther.Visible;
                  //  lblOther.Text = othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID));
                    if (txtOther.Visible == true)
                    {
                        rbSingleSelect.Items.Add(othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID)));
                    }
                      divAnswers.Controls.Add(rbSingleSelect);  
                  divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.ContactInformation:
                    divQuesAnswers.Controls.Add(
                        ServiceFactory<RespondentMultipleQuestions>.Instance.CreateContactInfoQuestion(tblContactInfo,
                                                                                                       surveyQuestion));
                    break;

                case QuestionType.Image:
                    foreach (var imageItem in surveyQuestion.AnswerOption)
                    {
                        Image imgType = new Image();
                        imgType.ImageUrl = ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateImageQuestion(imageItem.ANSWER_OPTIONS);
                        divAnswers.Controls.Add(imgType);
                        divQuesAnswers.Controls.Add(divAnswers);
                    }
                    break;

                case QuestionType.Narration:
                    Label lblNarration = new Label();
                    lblNarration.Text =
                        ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateNarrationQuestion(
                            surveyQuestion.Question.QUSETION_LABEL);
                    break;

                case QuestionType.PageHeader:
                    Label lblPageHeader = new Label();
                    lblPageHeader.Text =
                        ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreatePageHeaderQuestion(
                            surveyQuestion.Question.QUSETION_LABEL);
                    break;

                case QuestionType.QuestionHeader:
                    Label lblQuestionHeader = new Label();
                    lblQuestionHeader.Text =
                        ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateQuestionHeaderQuestion(
                            surveyQuestion.Question.QUSETION_LABEL);
                    break;

                case QuestionType.Matrix_MultipleSelect:
                    ServiceFactory<RespondentMatrixQuestions>.Instance.CreateMatrixMultipleSelectQuestion(tblContactInfo,
                                                                                                          surveyQuestion);
                    tblContactInfo.Attributes.Add("class", "tblMatrix2D");
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.Matrix_SideBySide:
                    ServiceFactory<RespondentMatrixQuestions>.Instance.CreateMatrixSideBySideQuestion(surveyQuestion,
                                                                                                      tblContactInfo);
                    tblContactInfo.Attributes.Add("class", "tblMatrixSideBySide");
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.Matrix_SingleSelect:
                    ServiceFactory<RespondentMatrixQuestions>.Instance.CreateMatrixSingleSelectQuestion(tblContactInfo,
                                                                                                          surveyQuestion);
                    tblContactInfo.Attributes.Add("class", "tblMatrix2D");
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.Text_CommentBox:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateTextCommentBoxQuestion(this.Page, divAnswers, surveyQuestion);
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;
                case QuestionType.Text_MultipleBoxes:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateTextMultipleBoxesQuestion(this.Page, divAnswers, surveyQuestion);
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.Text_SingleRowBox:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateTextSingleRowBoxQuestion(this.Page, divAnswers, surveyQuestion);
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.EmailAddress:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateEmailAddressQuestion(this.Page, divAnswers, surveyQuestion);
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;
                case QuestionType.Numeric_SingleRowBox:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateNumericSingleRowBoxQuestion(this.Page, divAnswers, surveyQuestion);
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.ConstantSum:
                    ServiceFactory<RespondentNumericQuestions>.Instance.CreateConstantSumQuestion(tblContactInfo, surveyQuestion);
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.DateAndTime:
                    ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateDateTimeQuestion(tblContactInfo, surveyQuestion);
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.Ranking:
                    ServiceFactory<RespondentNumericQuestions>.Instance.CreateRankingQuestion(tblContactInfo, surveyQuestion);
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.Video:
                    foreach (var videoItem in surveyQuestion.AnswerOption)
                    {
                        divAnswers.Controls.Add(ServiceFactory<RespondentOtherTypeQuestions>.Instance.DisplayVideoQuestion(videoItem.ANSWER_OPTIONS, videoItem.ANSWER_ID.ToString()));
                        divQuesAnswers.Controls.Add(divAnswers);
                    }
                    break;
            }
        }

        #region "Event : lstQuestionTypesItems_ItemCommand"
        /// <summary>
        /// In this event fair row command buttons based on CommanName. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void lstQuestionTypesItems_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete1")
            {
                string strQId = (String)e.CommandArgument;

                if (strQId != "")
                {
                    int questionId = ValidationHelper.GetInteger(strQId, 0);
                    var questionService = ServiceFactory.GetService<QuestionService>();
                    questionService.DeleteQuestionByQuestionId(questionId);

                    BindQuestionnaire();
                }
            }
            if (e.CommandName == "PageBreak")
            {
                string strQDetails = (String)e.CommandArgument;

                if (SetFeatures("PAGEBREAK") == 0)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Upgrade", "OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');", true);
                }
                else
                {
                    if (strQDetails != "")
                    {
                        string[] strDetailsSplit = strQDetails.Split(',');

                        if (strDetailsSplit.Length > 0)
                        {
                            int surveyId = ValidationHelper.GetInteger(strDetailsSplit[0], 0);
                            int questionId = ValidationHelper.GetInteger(strDetailsSplit[1], 0);
                            int pageBreak = ValidationHelper.GetInteger(strDetailsSplit[2], 0);
                            int skipLogic = ValidationHelper.GetInteger(strDetailsSplit[3], 0);
                            int questionSeq = ValidationHelper.GetInteger(strDetailsSplit[4], 0);

                            if (skipLogic == 1)
                            {
                                if (pageBreak == 1)
                                {
                                    dvErrMsg.Visible = true;
                                    lblErrMsg.Text = (String)GetLocalResourceObject("lblErrorPageBreak");  //"Page break cannot be removed. Skip Logic is applied for this question."; //"Page break already applied for this question.";

                                }
                                else
                                {
                                    dvErrMsg.Visible = true;
                                    lblErrMsg.Text = (String)GetLocalResourceObject("lblErrorPageBreak"); //"Page break cannot be removed. Skip Logic is applied for this question.";

                                }
                            }
                            else
                            {
                                if (pageBreak % 2 == 0)
                                {

                                    var questionService = ServiceFactory.GetService<QuestionService>();
                                    questionService.UpdatePageBreakOrReposeRequired(surveyId, questionId, 1, 1);

                                    dvsuccess.Visible = true;
                                    lblSucess.Text = (String)GetLocalResourceObject("lblSucessPageBreakApplied");  //"Page break applied successfully.";

                                }
                                else
                                {
                                    var questionService = ServiceFactory.GetService<QuestionService>();
                                    questionService.UpdatePageBreakOrReposeRequired(surveyId, questionId, 0, 1);

                                    dvsuccess.Visible = true;
                                    lblSucess.Text = (String)GetLocalResourceObject("lblSucessPageBreakRemoved");  //"Page break removed successfully.";
                                }

                                BindQuestionnaire();

                                string strParam1 = "divQuestionDetails" + questionSeq.ToString();
                                string strParam2 = "spanExpand" + questionSeq.ToString();
                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ExpandSingle", "showHide('" + strParam1 + "','" + strParam2 + "');", true);
                            }
                        }
                    }
                }
            }
            if (e.CommandName == "Required")
            {
                string strQDetails = (String)e.CommandArgument;

                if (SetFeatures("RESPONSE_REQUIRED") == 0)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Upgrade", "OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');", true);
                }
                else
                {

                    if (strQDetails != "")
                    {
                        string[] strDetailsSplit = strQDetails.Split(',');

                        if (strDetailsSplit.Length > 0)
                        {
                            int surveyId = ValidationHelper.GetInteger(strDetailsSplit[0], 0);
                            int questionId = ValidationHelper.GetInteger(strDetailsSplit[1], 0);
                            int response = ValidationHelper.GetInteger(strDetailsSplit[2], 0);
                            int skipLogic = ValidationHelper.GetInteger(strDetailsSplit[3], 0);
                            int questionSeq = ValidationHelper.GetInteger(strDetailsSplit[5], 0);

                            var questionTypeId = GenericHelper.ToEnum<QuestionType>(ValidationHelper.GetInteger(strDetailsSplit[4], 0));

                            if (!(questionTypeId == QuestionType.Narration || questionTypeId == QuestionType.PageHeader || questionTypeId == QuestionType.QuestionHeader || questionTypeId == QuestionType.Image || questionTypeId == QuestionType.Video))
                            {
                                if (skipLogic == 1)
                                {
                                    if (response == 1)
                                    {
                                        dvErrMsg.Visible = true;
                                        lblErrMsg.Text = (String)GetLocalResourceObject("lblErrorResponseRequired");  //"Response required cannot be removed. Skip Logic is applied for this question."; //"Page break already applied for this question.";

                                    }
                                    else
                                    {
                                        dvErrMsg.Visible = true;
                                        lblErrMsg.Text = (String)GetLocalResourceObject("lblErrorResponseRequired"); //"Response required cannot be removed. Skip Logic is applied for this question.";

                                    }
                                }
                                else
                                {

                                    if (response == 0)
                                    {
                                        var questionService = ServiceFactory.GetService<QuestionService>();
                                        questionService.UpdatePageBreakOrReposeRequired(surveyId, questionId, 1, 0);

                                        dvsuccess.Visible = true;
                                        lblSucess.Text = (String)GetLocalResourceObject("lblSucessResponseRequired");  //"Response required applied successfully.";
                                    }
                                    else
                                    {
                                        var questionService = ServiceFactory.GetService<QuestionService>();
                                        questionService.UpdatePageBreakOrReposeRequired(surveyId, questionId, 0, 0);

                                        dvsuccess.Visible = true;
                                        lblSucess.Text = (String)GetLocalResourceObject("lblSucessResponseRemoved"); //"Response required removed successfully.";
                                    }

                                    BindQuestionnaire();

                                    string strParam1 = "divQuestionDetails" + questionSeq.ToString();
                                    string strParam2 = "spanExpand" + questionSeq.ToString();
                                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ExpandSingle", "showHide('" + strParam1 + "','" + strParam2 + "');", true);
                                }
                            }
                            else
                            {
                                dvErrMsg.Visible = true;
                                lblErrMsg.Text = (String)GetLocalResourceObject("lblErrMsgResponseRequiredCannot");  //"Response Required cannot applied for this question.";

                            }

                        }
                    }
                }
            }

            if (chkExpandAllQuestions.Checked)
            {
                Page.RegisterStartupScript("expandAll", "<script>ExpandAll()</script>");
            }
        }

        #endregion

        #region "Method : SetFeatures"
        /// <summary>
        /// This method returns bool value, if the feature flag is one.
        /// </summary>
        /// <param name="strFeature"></param>
        /// <returns></returns>

        public int SetFeatures(string strFeature)
        {
            int iVal = 0;
            var session = ServiceFactory.GetService<SessionStateService>();
            var userService = ServiceFactory.GetService<UsersService>();
            var sessdet = session.GetLoginUserDetailsSession();
            if (sessdet != null)
            {

                var serviceFeature = ServiceFactory.GetService<FeatureService>();
                var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));

                if (sessdet.UserId > 0 && listFeatures != null)
                {
                    foreach (var listFeat in listFeatures)
                    {

                        if (listFeat.Feature.Contains(strFeature))
                        {
                            iVal = (int)listFeat.Value;
                        }
                    }
                }
            }

            else
            {
                Response.Redirect(PathHelper.GetUserLoginPageURL());
            }
            return iVal;
        }

        #endregion

        #region "Method : SetFeaturesSkip"
        /// <summary>
        /// This method returns bool value, if the feature flag is one.
        /// </summary>
        /// <param name="strFeature"></param>
        /// <returns></returns>

        public int SetFeaturesSkip(string strFeature)
        {
            int iVal = 1;
            var session = ServiceFactory.GetService<SessionStateService>();
            var userService = ServiceFactory.GetService<UsersService>();
            var sessdet = session.GetLoginUserDetailsSession();
            if (sessdet != null)
            {

                var serviceFeature = ServiceFactory.GetService<FeatureService>();
                var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));

                if (sessdet.UserId > 0 && listFeatures != null)
                {
                    foreach (var listFeat in listFeatures)
                    {

                        if (listFeat.Feature.Contains(strFeature))
                        {
                            iVal = (int)listFeat.Value;
                        }
                    }
                }
            }

            else
            {
                Response.Redirect(PathHelper.GetUserLoginPageURL());
            }
            return iVal;
        }

        #endregion

        #region "Event : chkQuestionMadatory_CheckedChanged"
        /// <summary>
        /// Used to add or remove respose required.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void chkQuestionMadatory_CheckedChanged(object sender, EventArgs e)
        {
            //if (SetFeatures("RESPONSE_REQUIRED") == 1)
            //{
            //    chkQuestionMadatory.Checked = false;
            //    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Upgrade", "OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');", true);
            //}
            //else
            //{
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            }

            if (surveyId > 0)
            {

                int reponseRequired = chkQuestionMadatory.Checked == true ? 1 : 0;
                var questionService = ServiceFactory.GetService<QuestionService>();
                int isupdate = questionService.UpdateResponseRequiredAllQuestions(surveyId, reponseRequired);

                if (isupdate == 1)
                {
                    if (reponseRequired == 1)
                    {
                        if (chkExpandAllQuestions.Checked)
                            chkExpandAllQuestions.Checked = false;
                        dvsuccess.Visible = true;
                        lblSucess.Text = (String)GetLocalResourceObject("lblSucessResponseRequiredAll");  //"Response required is applied successfully for all the questions.";
                    }
                    else
                    {
                        if (chkExpandAllQuestions.Checked)
                            chkExpandAllQuestions.Checked = false;
                        dvsuccess.Visible = true;
                        lblSucess.Text = (String)GetLocalResourceObject("lblSucessResponseRequiredRemoved");// "Response required is removed successfully for all the questions.";
                    }
                }

                BindQuestionnaire();
            }

        }

        #endregion

        #region "Event : chkPagebreak_CheckedChanged"
        /// <summary>
        /// Used to add or remove page break.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void chkPagebreak_CheckedChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            }

            if (surveyId > 0)
            {
                var questionService = ServiceFactory.GetService<QuestionService>();
                int isupdate = questionService.UpdatePageBreaksAllQuestions(surveyId, chkPagebreak.Checked);
                if (isupdate == 1)
                {
                    if (chkPagebreak.Checked)
                    {
                        if (chkExpandAllQuestions.Checked)
                            chkExpandAllQuestions.Checked = false;
                        dvsuccess.Visible = true;
                        lblSucess.Text = (String)GetLocalResourceObject("lblSucessPageBreakAppliedAll");  //"Page break is applied successfully for all the questions.";
                    }
                    else
                    {
                        if (chkExpandAllQuestions.Checked)
                            chkExpandAllQuestions.Checked = false;
                        dvsuccess.Visible = true;
                        lblSucess.Text = (String)GetLocalResourceObject("lblSucessPageBreakAppliedRemoved"); //"Page break is removed successfully for all the questions.";
                    }
                }
                BindQuestionnaire();
            }
        }
        #endregion
    }
}