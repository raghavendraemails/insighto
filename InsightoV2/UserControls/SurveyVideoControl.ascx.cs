﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Business.Enumerations;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using System.Collections;
using System.Configuration;
namespace Insighto.UserControls
{
    public partial class SurveyVideoControl : SurveyAnswerControlBase
    {
        string Mode;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Display();
              //  lblUpload.Text = lblUpload.Text + ConfigurationManager.AppSettings["videoMsg"] + ".";
            }

            if (Request.QueryString["key"] != null)
            {
                Hashtable typeHt = new Hashtable();
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains("Mode"))

                    Mode = (typeHt["Mode"].ToString());

            }
            if (Mode == "Edit")
            {
                dvVideoTypes.Visible = true;
            }
            if ((rbListVideos.SelectedIndex == 0) && Mode != "Edit")
            {
               // dvVideoUpload.Visible = true;
                dvYoutubeUpload.Visible = true;
            }
            else if ((rbListVideos.SelectedIndex == 1) && Mode != "Edit")
            {
                dvYoutubeUpload.Visible = true;
              //  dvVideoUpload.Visible = false;
            }
            else if ((rbListVideos.SelectedIndex==0) && Mode == "Edit")
            {
                dvYoutubeUpload.Visible = true;
               // dvVideoUpload.Visible = true;
               
            }
            else if ((rbListVideos.SelectedIndex == 1) && Mode == "Edit")
            {
                dvYoutubeUpload.Visible = true ;
              //  dvVideoUpload.Visible = false;
            }


        }

        string VideoNameType = "";
        string Val = "";

        public string VideoType
        {
            get
            {
               // return fileViedoUpload.PostedFile.ContentType.Substring(fileViedoUpload.PostedFile.ContentType.LastIndexOf('/') + 1);
                return null;
            }
        }

        public string FileName
        {
            get
            {
               // return fileViedoUpload.PostedFile.FileName;
                return null;
            }
        }

        public int FileSize
        {
            get
            {
               // return fileViedoUpload.PostedFile.ContentLength;
                return 0;
            }
        }

        public bool HasFile
        {
            get
            {
                //return fileViedoUpload.HasFile;
                return true;
            }
        }

        public string VideoPath
        {
            set
            {
                if (rbListVideos.SelectedValue == "1")
                {
                   // Val = value;
                    dvVideoPlay.InnerHtml = value;
                    dvVideoPlay.Visible = true;
                }
                else
                {
                    dvYoutubePlay.InnerHtml = value;
                    dvYoutubePlay.Visible = true;
                }
            }
            get
            {
                return Val;
            }
        }

        protected void rdVideoType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbListVideos.SelectedIndex == 0)
            {
               // dvVideoUpload.Visible = true;
                dvYoutubeUpload.Visible = true;
                dvYoutubePlay.Visible = true;
            }
            else if (rbListVideos.SelectedIndex == 1)
            {
                dvYoutubeUpload.Visible = true;
              //  dvVideoUpload.Visible = false;
               // dvVideoPlay.Visible = false;
            }
        }

        public string YoutubePath
        {
            get
            {
                return txtYoutube.Text.Trim();
            }
        }

        public string TypeVideo
        {
            get
            {
                return rbListVideos.SelectedValue;
            }
        }

        public string Value
        {
            set
            {
                rbListVideos.SelectedValue = value;
            }
        }
        public void Visibility()
        {
           // dvVideoUpload.Visible = false;
            dvYoutubeUpload.Visible = false;
            //dvVideoPlay.Visible = false;
            dvVideoTypes.Visible = false;
            //dvYoutubePlay.Visible = false;
        }
        public override void Display()
        {
            rbListVideos.Items.Clear();
            var lstColumns = (Enum.GetValues(typeof(VideoTypes)) as IEnumerable<VideoTypes>).Where(v => (int)v != 0).Select(
                    p => new ListItem { Text = Convert.ToString(p).Replace("_","  ") , Value = Convert.ToString((int)p) });
            //lstColumns.ToList().Remove(new ListItem("None", "0"));
        
            rbListVideos.DataTextField = "Text";
            rbListVideos.DataValueField = "Value";
            rbListVideos.DataSource = lstColumns;
            rbListVideos.DataBind();
            
            rbListVideos.SelectedValue = rbListVideos.SelectedIndex > -1 ? rbListVideos.SelectedValue : "2";
        }

        protected void CustCheckVideoPath(object source, ServerValidateEventArgs args)
        {
            if (rbListVideos.SelectedIndex == 0)
            {
                //bool isFileCheck = fileViedoUpload.HasFile;

                //if (dvVideoPlay.Visible == false && isFileCheck == false)
                //{
                //    args.IsValid = false;
                //}
                //else
                //{
                //    args.IsValid = true;
                //}

                if (dvVideoPlay.InnerHtml != "")
                {
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = false;
                }
            }
            else if (rbListVideos.SelectedIndex == 1)
            {
                if (dvYoutubePlay.InnerHtml != "")
                {
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = false;
                }
            }
        }
    }
}
