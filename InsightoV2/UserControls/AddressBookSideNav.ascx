﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddressBookSideNav.ascx.cs"
    Inherits="UserControls_AddressBookSideNav" %>
<div class="surveyCreateLeftMenu">
    <!-- survey left menu -->
    <ul class="menu" id="menu">
        <li><a href="#" ><asp:Label ID="lblManageEmailList" runat="server" 
                Text="Manage Email List" meta:resourcekey="lblManageEmailListResource1"></asp:Label></a>
            <ul class="innerMenu">
                <li class="activeLink">
                    <asp:HyperLink ID="hlnkManageEmail" runat="server" 
                        Text="Create / Manage Email List" meta:resourcekey="hlnkManageEmailResource1"></asp:HyperLink>
                    </li>
            </ul>
        </li>
        <li><a href="#" ><asp:Label ID="lblAddEmails" runat="server" Text="Add Emails" 
                meta:resourcekey="lblAddEmailsResource1"></asp:Label></a>
            <ul class="innerMenu" id="innerMenu">
                <li class="activelink"><asp:HyperLink ID="hlnkCreateEmail"  runat="server" 
                        Text="Add One By One" meta:resourcekey="hlnkCreateEmailResource1"></asp:HyperLink></li>
                <li><asp:HyperLink ID="hlnkCopy"  runat="server" Text="Add CSV List" 
                        meta:resourcekey="hlnkCopyResource1"  ></asp:HyperLink></li>
                <li id="liUpload" runat="server"><asp:HyperLink ID="hlnkUpload"  runat="server" 
                        meta:resourcekey="hlnkUploadResource1" ></asp:HyperLink></li>
            </ul>
        </li>
    </ul>
    <!-- //survey left menu -->
</div>


 