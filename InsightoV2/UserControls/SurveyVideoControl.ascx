﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveyVideoControl.ascx.cs"
    Inherits="Insighto.UserControls.SurveyVideoControl" %>
<script type="text/javascript" src="/Scripts/swfobject.js"></script>
<script type="text/javascript">

</script>
<p class="purpleColor"><b>Upload your video.</b></p>
<div id="dvVideoTypes" runat="server">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:RadioButtonList ID="rbListVideos" RepeatDirection="Horizontal" runat="server"
                    OnSelectedIndexChanged="rdVideoType_SelectedIndexChanged" AutoPostBack="true">
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
</div>
<div class="clear"></div> 
<%--<div id="dvVideoUpload" runat="server" visible="false">
    <asp:FileUpload ID="fileViedoUpload" runat="server" /><br />
    <asp:Label ID ="lblUpload" CssClass="note"  runat="server" Text="Please upload videos of extension .wmv, .mpeg, .mpg, .flv. File size should be less than "></asp:Label>  
    <br /><asp:RegularExpressionValidator SetFocusOnError="true" ID="regFuVideoUpload" runat="server"
        ValidationExpression="([0-9a-zA-Z -:\\-_!@$%^&*()])+(.wmv|.mpeg|.mpg|.flv)$" ErrorMessage="Please upload video of extension .wmv, .mpeg, .mpg, .flv. "
        ControlToValidate="fileViedoUpload" CssClass="lblRequired"></asp:RegularExpressionValidator>
    <asp:CustomValidator SetFocusOnError="true" ID="custCheckVideo" runat="server" CssClass="lblRequired"
        OnServerValidate="CustCheckVideoPath" style="float:left;" Display="Dynamic" ErrorMessage="Please select video."></asp:CustomValidator>

</div>--%>
<div id="dvYoutubeUpload" runat="server" visible="false">
    <asp:TextBox ID="txtYoutube" TextMode="MultiLine" CssClass="textAreaCommentView"
        runat="server" MaxLength="100"></asp:TextBox>
        <b>Please copy and paste the embed code of your YouTube video and click on save.</b>
    <asp:RegularExpressionValidator SetFocusOnError="true" ID="regFuyoutubeUpload" runat="server"
        ValidationExpression="(https?://(www\.)?youtube\.com/.*v=\w+.*)|(https?://youtu\.be/\w+.*)|(.*src=.https?://(www\.)?youtube\.com/v/\w+.*)|(.*src=.https?://(www\.)?youtube\.com/embed/\w+.*)"      
        ErrorMessage="Please provide YouTube video embed path." ControlToValidate="txtYoutube"
        CssClass="lblRequired"></asp:RegularExpressionValidator><asp:RequiredFieldValidator style="float:left;" SetFocusOnError="true" ID="reqYoutube" Display="Dynamic"
        runat="server" CssClass="lblRequired" ErrorMessage="Please provide YouTube video embed path." ControlToValidate="txtYoutube"></asp:RequiredFieldValidator><br /><br />
        

</div>
&nbsp
<%--<div id="dvVideoPlay" runat="server" visible="false">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <p id="player1">
        <a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to
        see this Video.</p>
    <script type="text/javascript">
        var s1 = new SWFObject("flvplayer.swf", "single", "250", "200", "7");
        var url = '<%= VideoPath %>';

        s1.addParam("allowfullscreen", "true");
        s1.addVariable("file", url);
        s1.addVariable('showdigits', 'false');
        s1.addVariable('bufferlength', '3');
        s1.addVariable('type', 'flv');
        s1.addVariable('usekeys', 'false');
        s1.addVariable('autostart', 'false');
        s1.write("player1");
    </script>
</div>--%>
<div id="dvVideoPlay" runat="server" visible="false">
</div>
<div id="dvYoutubePlay" runat="server" visible="false">

</div>


<p style="font-weight:700;" class="purpleColor">Disclaimer:</p>
<div class="disclaimer">
<b>Important:</b> The Image / Video you are uploading should be of your own or you should have the copyrights or be entitled to use the same for public/private consumption.   Insighto will not hold any responsibility or liability for the content uploaded. Any content that is abhorrent or obscene or offending others or violating statutory laws  would  be deleted without any prior notice. And the same would be placed before the appropriate statutory authorities.
</div>