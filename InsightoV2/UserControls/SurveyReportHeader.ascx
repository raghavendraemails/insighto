﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveyReportHeader.ascx.cs"
    Inherits="Insighto.UserControls.SurveyReportHeader" %>
<div class="contentPanelHeader">
    <div class="pageAnalytics">
    <table width="100%">
    <tr>
    <td width="27%">    
      <asp:Label ID="lblReport" runat="server" Text="Report :" meta:resourcekey="lblReportResource1"></asp:Label>
        <asp:Label ID="lblSurveyName" runat="server" meta:resourcekey="lblSurveyNameResource1"></asp:Label>
        </td>
        <td width="20%" align="left">
             <asp:Label ID="lblVisits" runat="server" Text="VISITS :" Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            <asp:Label ID="lblVisitsValue" runat="server"  Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            </td>
            <td width="20%" align="center">
            <asp:Label ID="lblComplete" runat="server" Text="COMPLETES :" Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            <asp:Label ID="lblCompleteValue" runat="server"  Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>  
            </td>      
            <td width="20%" align="right">     
              <asp:Label ID="lblPartial" runat="server" Text="PARTIALS :" Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            <asp:Label ID="lblPartialValue" runat="server"  Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            </td>
            <td width="12%">&nbsp;</td>
            </tr>
            </table>
    </div>
</div>
