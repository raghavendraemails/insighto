﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;

namespace Insighto.UserControls
{
    public partial class Contactinfo : SurveyAnswerControlBase
    {    
        public override bool IsRequired
        {
            get;          
            set;           
        }

        /// <summary>
        /// The Group (Validation Group)
        /// </summary>
        public override string Group
        {
            get;            
            set;            
        }

        /// <summary>
        /// Setup the Control
        /// </summary>
        public override void Initialise()
        {

        }

        /// <summary>
        /// Binds the Data
        /// </summary>
        public override void Display()
        {

        }

        public override void Clear()
        {
            chkIncludeFormField.Checked = true;
        }

        public string FormFieldLabelText
        {
            set
            {
                chkIncludeFormField.Text = value;
            }
        }

        public bool CheckIncludeFieldChcked
        {
            get
            {
                if (chkIncludeFormField.Checked)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string FormFieldText
        {
            get
            {
                if (chkIncludeFormField.Checked)
                {
                    if (txtFormField.Text.Trim().Length > 0)
                    {
                        return txtFormField.Text.Trim();
                    }
                    else
                    {
                        return chkIncludeFormField.Text.Trim();
                    }
                }
                else
                {
                    return " ";
                }
            }
            set
            {
                txtFormField.Text = value;
            }

        }
        public string IsMandatory
        {
            get
            {
                if (chkIncludeFormField.Checked)
                {
                    if (chkMandatoryField.Checked)
                    {
                        return "1";
                    }
                    else
                    {
                        return "0";
                    }
                }
                else
                {
                    return "0";
                }
               
            }

            set
            {
                string strMand = value;
                bool boolMand;
                if (strMand == "0")
                {
                    boolMand = false;
                }
                else
                {
                    boolMand = true;
                }
                chkMandatoryField.Checked = boolMand;
            }
        }

        public bool UnCheckFields
        {
            set
            {
                //string strMand = value;
               // if (strMand == "0")
                //{
                    chkIncludeFormField.Checked = value;
               // }
                
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public bool  checkMandatory
        {
           get
           {
               if (chkMandatoryField.Checked)
                   return true;
               else
                   return false;
           }
           
        }
    }
}