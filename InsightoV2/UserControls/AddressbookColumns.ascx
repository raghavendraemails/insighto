﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddressbookColumns.ascx.cs" Inherits="Insighto.UserControls.AddressbookColumns" %>
<asp:CustomValidator SetFocusOnError="true"  ID="custTextControl" OnServerValidate="custValidateUniqText"
        runat="server" CssClass="lblRequired" Enabled="false" Display="Dynamic" ErrorMessage="Columns should not be same."></asp:CustomValidator>
    <asp:Repeater ID="addRemoveControls" runat="server" OnItemCommand="addRemoveControls_ItemCommand"
        OnItemDataBound="itemDataBoundRepeater_ItemDataBound">
        <ItemTemplate>
            <div class="itempanel">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td class="staticwidth">
                            <asp:Label ID="lblColumn" runat="server" Text="Column "  CommandArgument="<%# Container.ItemIndex %>" /></asp:Label>
                        </td>
                        <td>                            
                            <asp:TextBox ID="txtColumn" runat="server" onkeypress="return numbersonly(event)" CssClass="textBoxMedium" MaxLength="12"
                                Text='<%# Container.DataItem %>' />
                        </td>
                        <td class="space">&nbsp;</td>
                        <td>
                            <asp:Button ID="btnRemove" runat="server" Text='' CssClass="RemoveIcon" CausesValidation="false"
                                CommandArgument="<%# Container.ItemIndex %>" CommandName='Remove' ToolTip="Delete" />
                        </td>
                        <td class="space">&nbsp;</td>
                        <td>
                            <asp:Button ID="btnAddAnother" runat="server" CssClass="addedIcon" Text='' CausesValidation="false"
                                CommandArgument="<%# Container.ItemIndex %>" CommandName='Add' ToolTip="Add" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="5">
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqAnsoption" runat="server" ControlToValidate="txtColumn"
                                Enabled="true" CssClass="lblRequired" Display="Dynamic" ErrorMessage="Please enter column name."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <table width="100%">
    <tr>
    <td align="left" style="padding-left:97px;">
    <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" CssClass="Prelaunch_small" Text="Add Column" Visible="false"/>
    </td>
    </tr>
    </table>
    
    <script type="text/javascript" language="javascript">

        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
           
            if (unicode != 8) {
                if (unicode == 60 || unicode == 62)
                    return false;
                
            }
           
        }
    </script>