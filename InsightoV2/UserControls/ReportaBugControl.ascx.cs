﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Collections;
using Insighto.Business.ValueObjects;
using App_Code;
using System.Configuration;
using System.Web.Mail;

public partial class UserControls_ReportaBugControl : UserControlBase
{
    Hashtable ht = new Hashtable();
    int userId = 0;
    string pageName = "";
    string filename = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAttentionvalues();
        }
        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0)
            {
                if (ht.Contains("Userid"))
                    userId = Convert.ToInt32(ht["Userid"]);
                if (ht.Contains("PageName"))
                    pageName = ht["PageName"].ToString();
            }
        }
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        if (loggedInUserInfo != null)
        {
            if (loggedInUserInfo.UserId > 0)
            {
                var userDetails = ServiceFactory.GetService<UsersService>().GetUserDetails(loggedInUserInfo.UserId).FirstOrDefault();
                if (userDetails != null)
                {
                    txtName.Text = userDetails.FIRST_NAME;
                    if (userDetails.LAST_NAME != null)
                        txtName.Text += " " + userDetails.LAST_NAME;
                    txtName.Enabled = false;
                    txtMail.Text = userDetails.LOGIN_NAME;
                    txtMail.Enabled = false;
                    if (userDetails.PHONE != null)
                    {
                        txtPhno.Text = userDetails.PHONE;
                    }
                        //  txtPhno.Enabled = false;
                    ddlAttenValues.SelectedValue = ddlAttenValues.Items.FindByText("Support Needed").Value;
                }
            }
        }
        if (loggedInUserInfo != null)
        {
            ddlAttenValues.SelectedValue = ddlAttenValues.Items.FindByText("Support Needed").Value;
        }
        else
        {
            ddlAttenValues.SelectedValue = ddlAttenValues.Items.FindByText("Feedback").Value;
        }
        lblErrMsg.Visible = false;
        dvErrMsg.Visible = false;
        lblSuccMsg.Visible = false;
        dvSuccessMsg.Visible = false;
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {

        Boolean fileOK = false;
        if (FileUpload1.HasFile)
        {

            filename = System.IO.Path.GetFileName(FileUpload1.FileName);

            string fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
            string[] allowedExtensions = { ".gif", ".png", ".jpeg", ".jpg" };
            for (int i = 0; i < allowedExtensions.Length; i++)
            {
                if (fileExtension == allowedExtensions[i])
                {
                    fileOK = true;
                }
            }
        }
        if (fileOK)
        {
            try
            {
                FileUpload1.SaveAs(Server.MapPath("~/images/") + FileUpload1.FileName);

            }
            catch
            {
                Console.Write("error");
            }
        }

        if (ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession() != null)
        {
            if (txtCaptcha.Text == ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession())
            {
                InsertContactDetails();
                lblErrMsg.Visible = false;
                dvErrMsg.Visible = false;
                dvSuccessMsg.Visible = true;
                lblSuccMsg.Visible = true;
                // base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL().Replace("~/",""), "surveyFlag=" + base.SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
            }
            else
            {
                lblErrMsg.Text = GetLocalResourceObject("lblErrMsgResource1.Text").ToString();   //"Please enter the numbers displayed.";
                dvSuccessMsg.Visible = false;
                lblSuccMsg.Visible = false;
                lblErrMsg.Visible = true;
                dvErrMsg.Visible = true;
                txtCaptcha.Focus();
                spn.InnerText = (500 - txtMessage.Text.Length).ToString() + " characters left";
            }
        }
        else
        {
            txtCaptcha.Focus();
            lblErrMsg.Text = GetLocalResourceObject("lblErrMsgResource1.Text").ToString(); //"Please enter the numbers displayed.";
            dvSuccessMsg.Visible = false;
            lblSuccMsg.Visible = false;
            lblErrMsg.Visible = true;
            dvErrMsg.Visible = true;
            spn.InnerText = (500 - txtMessage.Text.Length).ToString() + " characters left";
        }
    }
    #region "Method:InsertContactDetails"
    public void InsertContactDetails()
    {
        osm_contactusdetails osmContactUs = new osm_contactusdetails();
        osmContactUs.USERNAME = txtName.Text.Trim();
        osmContactUs.EMAILADDRESS = txtMail.Text.Trim();
        osmContactUs.PHONENO =txtCountryCode.Text.Trim()+txtAreaCode.Text.Trim()+ txtPhno.Text.Trim();
       // osmContactUs.MAILMESSAGE = txtMessage.Text.Trim();
        osmContactUs.ATTENTIONVALUE = ddlAttenValues.SelectedItem.Value;
        osmContactUs.Organisation = "";
        var contactusService = ServiceFactory.GetService<ContactUsService>();
        contactusService.InsertContactUs(osmContactUs);
        if (osmContactUs.CONTACTUSUSERID > 0)
        {

            var configService = ServiceFactory.GetService<ConfigService>();
            List<string> configValue = new List<string>();
            configValue.Add("ContactusEmailAddress");
            var configDet = configService.GetConfigurationValues(configValue);
            //string subject = "UserName: " + osmContactUs.USERNAME + "- Issue Type: " + osmContactUs.ATTENTIONVALUE + "- EmailID: " + osmContactUs.EMAILADDRESS + "- Phone No: " + osmContactUs.PHONENO;
            string subject = ""+ddlAttenValues.SelectedItem.Value+" (" + txtName.Text.Trim() + ")";
            string emailBody = "User name:" + txtName.Text.Trim() +"<br>"+ "\n" + "Phone Number:" + txtCountryCode.Text.Trim() + txtAreaCode.Text.Trim() + txtPhno.Text.Trim() +"<br>"+ "\n" + "Email Id:" + txtMail.Text.Trim() + "<br>"+"\n" + "Message:" + txtMessage.Text.Trim();
            if (pageName != string.Empty)
                emailBody += "\n Page Name:" + pageName;
            osmContactUs.MAILMESSAGE = emailBody;
           
            Hashtable ht = new Hashtable();
             List<string> attchments = new List<string>();
             attchments.Add(Server.MapPath("~/images") + "\\" + System.IO.Path.GetFileName(FileUpload1.FileName));


             if (FileUpload1.FileName == "")
             {
                 MailHelper.SendMailMessage(osmContactUs.EMAILADDRESS, configDet[0].CONFIG_VALUE, "", "", subject, osmContactUs.MAILMESSAGE);
             }
             else
             {
                 MailHelper.SendMailMessage(osmContactUs.EMAILADDRESS, configDet[0].CONFIG_VALUE, "", "", subject, osmContactUs.MAILMESSAGE, attchments);
             }
            
            ClearControls();
        }
    }

    public void ClearControls()
    {
        lblSuccMsg.Text = GetLocalResourceObject("lblSuccMsgResource1.Text").ToString();//"Thank you for contacting us. We will get back to you shortly.";
        txtCaptcha.Text = "";
        txtMail.Text = "";
        txtMessage.Text = "";
        txtCountryCode.Text = "";
        txtAreaCode.Text = "";
        txtPhno.Text = "";
        txtName.Text = "";
        lblSuccMsg.Visible = true;
        dvSuccessMsg.Visible = true;
       // ddlAttenValues.SelectedItem.Text = "Support Needed";
        ddlAttenValues.SelectedValue = ddlAttenValues.Items.FindByText("Support Needed").Value;
    }
    #endregion

    #region "Method : BindCountry"
    /// <summary>
    /// Used to get country list and bind values to dropdownlist.
    /// </summary>

    private void BindAttentionvalues()
    {
        // object on osm_picklist class
        osm_picklist osmPickList = new osm_picklist();
        //Assign vaule category
        osmPickList.CATEGORY = Constants.Attentionvalues;
        // object on PickListService class
        PickListService pickListService = new PickListService();
        //inovke method GetPickList
        var vPickList = pickListService.GetPickList(osmPickList);

        if (vPickList.Count > 0)
        {
            //Bind list values to dropdownlist
            this.ddlAttenValues.DataSource = vPickList;
            this.ddlAttenValues.DataTextField = Constants.PARAMETER;
            this.ddlAttenValues.DataValueField = Constants.PARAMETER;
            this.ddlAttenValues.DataBind();
            // ddlAttenValues.Items.Insert(0, new ListItem("--Select--", ""));
        }

    }
    #endregion
}