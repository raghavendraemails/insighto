﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Insighto.UserControls
{
    public partial class HtmlEditor : SurveyAnswerControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public  string Value
        {
            get
            {
                return EditorQuestionIntro.Value;
            }
            set
            {
                EditorQuestionIntro.Value = value;
            }
        }

    }
}