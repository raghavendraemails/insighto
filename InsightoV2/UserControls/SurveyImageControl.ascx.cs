﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace Insighto.UserControls
{
    public partial class SurveyImageControl : SurveyAnswerControlBase
    {
        public string ImageType
        {
            get
            {
                return fuImageUpload.PostedFile.ContentType.Substring(fuImageUpload.PostedFile.ContentType.LastIndexOf('/') + 1);
            }
        }
        public string FileNAme
        {
            get
            {
                return fuImageUpload.PostedFile.FileName;
            }
        }

        public void Visibility()
        {
                fuImageUpload.Visible = false;
        }
        public int FileSize
        {
            get
            {
                return fuImageUpload.PostedFile.ContentLength;
            }
        }
        public bool HasFile
        {
            get
            {
                return fuImageUpload.HasFile;
            }
        }

        public override void Clear()
        {
            imgSurveyImage.Visible = false;
        }
     
        public string  SetImagePath
        {          
            set 
            {
                 imgSurveyImage.Visible = true;
                 imgSurveyImage.ImageUrl = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lbluploadImg.Text = lbluploadImg.Text + ConfigurationManager.AppSettings["imgMsg"] + ".";
            }
        }

        protected void CustCheckImagePath(object source, ServerValidateEventArgs args)
        {
            bool isFileCheck = fuImageUpload.HasFile;
            if (imgSurveyImage.ImageUrl == "" && isFileCheck == false)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
            
        }

    }
}