<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveyAnswerControl.ascx.cs"
    Inherits="Insighto.UserControls.SurveyAnswerControl" %>
<div> 
    <asp:CustomValidator SetFocusOnError="true"  ID="custTextControl" OnServerValidate="custValidateUniqText"
        runat="server" CssClass="lblRequired" Display="Dynamic" ErrorMessage="Answer options should not be same."></asp:CustomValidator>
    
    <asp:CustomValidator SetFocusOnError="true"  ID="custEmptyCheck" ClientValidationFunction="ValidateControl"
        runat="server"></asp:CustomValidator>
     <asp:UpdatePanel ID="uPanelAnswerOptions" runat="server">
            <ContentTemplate>
            <table>
            <tr>
            <td>
            
             <asp:Repeater ID="addRemoveControls" runat="server" OnItemCommand="addRemoveControls_ItemCommand"
        OnItemDataBound="itemDataBoundRepeater_ItemDataBound"> 
        <ItemTemplate>
            <div class="itempanel">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr class="txtRow">
                    
                        <td>
                            <asp:TextBox ID="txtAnsOption" runat="server"  CssClass="textBoxMedium txtAns" MaxLength="150"
                                Text='<%# Container.DataItem %>' />
                        </td>
                        <td class="space">&nbsp;</td>
                        <td>
                            <asp:Button ID="btnRemove" runat="server" Text='' CssClass="RemoveIcon" CausesValidation="false"
                                CommandArgument="<%# Container.ItemIndex %>" CommandName='Remove' ToolTip="Delete answer option" />
                        </td>
                        <td class="space">&nbsp;</td>
                        <td>
                            <asp:Button ID="btnAddAnother" runat="server" CssClass="addedIcon" Text='' CausesValidation="false"
                                CommandArgument="<%# Container.ItemIndex %>" CommandName='Add' ToolTip="Add answer option" />
                        </td>
                    </tr>
                    <tr class="lblRow">
                        <td colspan="5">
                           <%-- <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqAnsoption" runat="server" ControlToValidate="txtAnsOption"
                                Enabled="true" CssClass="lblRequired" Display="Dynamic" ErrorMessage="Please enter answer option."></asp:RequiredFieldValidator>--%>
                                <asp:Label ID="lblMsg" runat="server" text="Please enter answer option." CssClass="lblRequired" style="display:none"></asp:Label>
                        </td>
                    </tr>
                </table>
                
            </div>
        </ItemTemplate>
        <FooterTemplate>
       </FooterTemplate>
    </asp:Repeater>
    </td>
    <td>
    <asp:Panel Visible="false" ID="MyPanel" runat="server">
     <div id="multiple">
            <div class="informationPanelDefault" runat="server" id ="dvInformationPanelDefault">
            <div > 
    <asp:Label ID="lblmessage" runat="server"></asp:Label>
   </div>
   </div>
        </div>
        </asp:Panel>
    </td>
            
            </tr>
            </table>

    </ContentTemplate>
    <Triggers>
    <asp:AsyncPostBackTrigger ControlID="addRemoveControls" EventName="ItemDataBound" />
    </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.txtAns').blur(function () {
                var parentTr = $(this).parent().parent();
                var lbl = $(parentTr).next().find(".lblRequired");
                if ($(this).val() != "") {
                    $(lbl).hide();
                }
            });
            
        });
        function ValidateControl(Src, args) {
            args.IsValid = true;
            $('.txtAns').each(function () {
                var parentTr = $(this).parent().parent();
                var lbl = $(parentTr).next().find(".lblRequired");
                if ($(this).val() == "") {
                    $(lbl).show();
                    args.IsValid = false;
                }
                else {
                    $(lbl).hide();
                }
            });
            
        }
    </script>
  
</div>
