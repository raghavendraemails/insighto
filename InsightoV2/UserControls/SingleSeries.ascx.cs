﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using InfoSoftGlobal;
using System.Configuration;
public partial class UserControls_SingleSeries : System.Web.UI.UserControl
{
     public string exportchartfilehandler;  


    protected void Page_Load(object sender, EventArgs e)
    {
        //exportHandler='" + exportchartfilehandler + "'  exportAction='download'
        exportchartfilehandler = ConfigurationSettings.AppSettings["exportchartfile"].ToString();
        object[,] arrData = new object[5, 2];
        arrData[0, 0] = "Extremely Satisfied"; arrData[1, 0] = "Satisfied"; arrData[2, 0] = "Somewhat satisfied"; arrData[3, 0] = "Dissatisfied"; arrData[4, 0] = "Extremely Dissatisfied"; 
        arrData[0, 1] = 23; arrData[1, 1] = 47; arrData[2, 1] = 13; arrData[3, 1] = 10; arrData[4, 1] = 7; 
        StringBuilder xmlData = new StringBuilder();
        xmlData.Append("<chart caption='State overall satisfaction with our services?' numberSuffix='%' exportEnabled='1' canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1'   formatNumberScale='0' bgColor='FFFFFF' showAboutMenuItem='0'  animate3D='1' animated='1'   interactiveLegend='1' >");
        for (int i = 0; i < arrData.GetLength(0); i++) { xmlData.AppendFormat("<set label='{0}' value='{1}' />", arrData[i, 0], arrData[i, 1]); }
        xmlData.Append("</chart>");
        Literal1.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/Column3D.swf", "", xmlData.ToString(), "MSSS", "600", "300", false, true);
       
        Literal1.Dispose();
        
    }
}