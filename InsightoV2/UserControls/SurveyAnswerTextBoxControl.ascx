﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveyAnswerTextBoxControl.ascx.cs"
    Inherits="Insighto.UserControls.SurveyAnsTextControl" %>
<div class="borderPanel">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:Label ID="lblTextCount" runat="server" Text="">
                </asp:Label>
            </td>
            <td>
                &nbsp; <asp:TextBox ID="txtNoofCharacters" runat="server" class="textBoxTextCount" Enabled="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqNoofCharacters" runat="server" ControlToValidate="txtNoofCharacters"
                    CssClass="lblRequired" Display="Dynamic" ErrorMessage="Please enter numerics."></asp:RequiredFieldValidator>
                <%--  <asp:RangeValidator SetFocusOnError="true"  ID="rngtxtNoofCharacters" runat="server" MinimumValue = "1" ControlToValidate = "txtNoofCharacters" ></asp:RangeValidator>--%>
                <asp:RegularExpressionValidator SetFocusOnError="true"  ID="revtxtNoofCharacters" runat="server" CssClass="lblRequired"
                    Display="Dynamic" ControlToValidate="txtNoofCharacters"></asp:RegularExpressionValidator>
            </td>
        </tr>
    </table>
</div>
