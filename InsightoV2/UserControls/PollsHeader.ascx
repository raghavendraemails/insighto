﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollsHeader.ascx.cs" Inherits="new_UserControls_PollsHeader" %>
    <link rel="stylesheet" href="/assets/css/pages/page_pricing.css">
  <div class="header header-sticky">
    <div class="container"> <a class="logo pull-left big-link active" href="home.aspx"> <img src="../assets/img/insighto-polls.png" alt="Logo" > </a>
      <div class="topbar">
        <ul class="loginbar pull-right">
          <li><a href="login.aspx">Login</a></li>
          <li class="topbar-devider"></li>
          <li><a href="registration.aspx" class="signupBtn">SIGNUP</a></li>
        </ul>
      </div>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"> <span class="sr-only">Toggle navigation</span> <span class="fa fa-bars"></span> </button>
    </div>
    <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
      <div class="container">
        <ul class="nav navbar-nav">
          <li> <a href="/Home.aspx" class="page-scroll"> Home </a> </li>
          <li> <a href="/surveys/Home.aspx" class="page-scroll"> Surveys </a> </li>
          <li class="active"> <a href="Home.aspx"> Polls </a> </li>
          <li> <a href="pricing.aspx"> Pricing </a> </li>
          <%--<li> <a href="https://www.insighto.com/blog/" target="_blank"> Blog </a> </li>--%>
          <%--<li> <a href="#"> Blog </a> </li>--%>
        </ul>
      </div>

    </div>
  </div>
