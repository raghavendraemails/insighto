﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CMSCreateLeftMenu.ascx.cs" Inherits="Insigtho.Pages.UserControls.FaqControls.CMSCreateLeftMenu" %>
<div class="faqCmsMenu">
    <!-- create survey mennu -->
    <div class="faqSearchPanel">
        CREATE SURVEY
     </div>
    <div class="menuLeft">
    <asp:TreeView ID="tvHelpIcon" CssClass="FAQLeafNode" runat="server" NodeIndent="0" NodeWrap="true"
        ShowLines="false" SelectedNodeStyle-CssClass="active">
    
        <NodeStyle CssClass="mainNode" />        
        <LeafNodeStyle CssClass="FaqSubNode" />
     </asp:TreeView>
           
    </div>
   </div>
