﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Collections;
using Insighto.Business.ValueObjects;
using App_Code;
public partial class UserControls_FaqControls_AnalyzeLeftMenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAnalyzeTreeview();
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht["page"].ToString().ToUpper() == "ANALYZE")
            {
                tvAnalyze.FindNode(ht["parentNode"].ToString()).Expand();
            }
        }
    }    
    public void BindAnalyzeTreeview()
    {
        osm_faqpages faqPage = new osm_faqpages();
        faqPage.Page_Name_Group = "Analyze";
        var faqService = ServiceFactory.GetService<FAQService>();
        var pages = faqService.GetFAQPages(faqPage);

        IEnumerable<IGrouping<string, string>> parentPage;

        parentPage = (from p in pages
                      orderby p.Page_Name_Group ascending
                      group p.Page_Name_Parent by p.Page_Name_Parent into subcats
                      select subcats);

        TreeNode parentNode = null;

        foreach (var node in parentPage)
        {
            parentNode = new TreeNode(node.Key, node.Key);
            var subpages = faqService.GetSubPages(node.Key);
            var childPages = (from p in subpages
                              orderby p.Page_Sort ascending
                              group p.Page_Name by p.Page_Name into subcats
                              select subcats);

            foreach (var cp in childPages)
            {
                if (cp.Key != null)
                {
                    TreeNode childNode = new TreeNode(cp.Key, cp.Key);
                    var pageURL = faqService.GetPageURL(cp.Key, node.Key);
                    childNode.NavigateUrl = EncryptHelper.EncryptQuerystring(pageURL.Page_URL, "page=Analyze&pageName=" + cp.Key + "&parentNode=" + node.Key);
                    parentNode.ChildNodes.Add(childNode);
                }

            }
            parentNode.Selected = false;
            tvAnalyze.Nodes.Add(parentNode);
            tvAnalyze.CollapseAll();
        }
    }

    protected void tvAnalyze_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
    {
        if (tvAnalyze.Nodes == null)
            return;
        string strNodeValue = e.Node.Value;
        for (int i = 0; i < tvAnalyze.Nodes.Count; i++)
        {
            if (tvAnalyze.Nodes[i].Value == strNodeValue)
            {
                tvAnalyze.Nodes[i].Expand();
            }
            else
            {
                tvAnalyze.Nodes[i].Collapse();
            }
        }
    }
}