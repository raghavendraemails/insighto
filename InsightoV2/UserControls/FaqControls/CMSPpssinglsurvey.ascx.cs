﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Collections;
using Insighto.Business.ValueObjects;
using App_Code;


public partial class UserControls_FaqControls_CMSPpssinglsurvey : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindManageTreeview();
        }
    }
    public void BindManageTreeview()
    {
        osm_faqpages faqPage = new osm_faqpages();
        faqPage.Page_Name_Group = "Pps";
        var faqService = ServiceFactory.GetService<FAQService>();
        var pages = faqService.GetFAQPages(faqPage);

        IEnumerable<IGrouping<string, string>> parentPage;

        parentPage = (from p in pages
                      orderby p.Page_Name_Group ascending
                      group p.Page_Name_Parent by p.Page_Name_Parent into subcats
                      select subcats);

        TreeNode parentNode = null;

        foreach (var node in parentPage)
        {
            parentNode = new TreeNode(node.Key, node.Key);
            var subpages = faqService.GetSubPages(node.Key);
            var childPages = (from p in subpages
                              orderby p.Page_Sort ascending
                              group p.Page_Name by p.Page_Name into subcats
                              select subcats);

            foreach (var cp in childPages)
            {
                if (cp.Key != null)
                {
                    TreeNode childNode = new TreeNode(cp.Key, cp.Key);
                    var pageURL = faqService.GetPageURL(cp.Key, node.Key);
                    childNode.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetFAQContentManagementURL(), "page=Pps&pageName=" + cp.Key + "&parentNode=" + node.Key);
                    parentNode.ChildNodes.Add(childNode);
                }

            }
            tvManage.Nodes.Add(parentNode);
        }
    }
}