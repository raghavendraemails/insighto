﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CMSAnalyzeLeftMenu.ascx.cs" Inherits="Insigtho.Pages.UserControls.FaqControls.CMSAnalyzeLeftMenu" %>
<div  class="faqCmsMenu"><!--analyze menu -->
<div class="faqSearchPanel">
        ANALYZE SURVEY
    </div>
    <div class="menuLeft">
    <asp:TreeView ID="tvAnalyze" CssClass="FAQLeafNode" runat="server" NodeIndent="0" NodeWrap="true"
        ShowLines="false" SelectedNodeStyle-CssClass="active">
    
     <NodeStyle CssClass="mainNode" />        
        <LeafNodeStyle CssClass="FaqSubNode" />
    </asp:TreeView>  
    </div>
<!--//analyzemenu --></div>