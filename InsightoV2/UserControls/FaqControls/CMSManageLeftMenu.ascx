﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CMSManageLeftMenu.ascx.cs" Inherits="Insigtho.Pages.UserControls.FaqControls.CMSManageLeftMenu" %>
<div class="faqCmsMenu"><!-- manage menu-->
    <div class="faqSearchPanel">
        MANAGE SURVEY
    </div>
    <div class="menuLeft">
    <asp:TreeView ID="tvManage" runat="server" CssClass="FAQLeafNode" SelectedNodeStyle-CssClass="active" NodeIndent="0" NodeWrap="true"
        ShowLines="false">
  
     <NodeStyle CssClass="mainNode" />        
        <LeafNodeStyle CssClass="FaqSubNode" />
    </asp:TreeView>   
    
  </div>
<!-- //manage menu--></div>