﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Collections;
using Insighto.Business.ValueObjects;
using App_Code;

public partial class UserControls_FaqControls_ManageLeftMenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindManageTreeview();
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht["page"].ToString().ToUpper() == "MANAGE")
            {
                if (tvManage.FindNode(ht["parentNode"].ToString().Trim()) == null)
                    tvManage.Nodes[0].Expand();
                else if (tvManage.FindNode(ht["parentNode"].ToString().Trim()) != null)
                    tvManage.FindNode(ht["parentNode"].ToString()).Expand();
            }
        }
    }
    public void BindManageTreeview()
    {
        osm_faqpages faqPage = new osm_faqpages();
        faqPage.Page_Name_Group = "Manage";
        var faqService = ServiceFactory.GetService<FAQService>();
        var pages = faqService.GetFAQPages(faqPage);

        IEnumerable<IGrouping<string, string>> parentPage;

        parentPage = (from p in pages
                      orderby p.Page_Name_Group ascending
                      group p.Page_Name_Parent by p.Page_Name_Parent into subcats
                      select subcats);

        TreeNode parentNode = null;

        foreach (var node in parentPage)
        {
            parentNode = new TreeNode(node.Key, node.Key);
            var subpages = faqService.GetSubPages(node.Key);
            var childPages = (from p in subpages
                              orderby p.Page_Sort ascending
                              group p.Page_Name by p.Page_Name into subcats
                              select subcats);

            foreach (var cp in childPages)
            {
                if (cp.Key != null)
                {
                    TreeNode childNode = new TreeNode(cp.Key, cp.Key);
                    var pageURL = faqService.GetPageURL(cp.Key, node.Key);
                    childNode.NavigateUrl = EncryptHelper.EncryptQuerystring(pageURL.Page_URL, "page=Manage&pageName=" + cp.Key + "&parentNode=" + node.Key);
                    parentNode.ChildNodes.Add(childNode);
                }

            }
            parentNode.Selected = false;
            tvManage.Nodes.Add(parentNode);
            tvManage.CollapseAll();
        }
    }

    protected void tvManage_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
    {
        if (tvManage.Nodes == null)
            return;
        string strNodeValue = e.Node.Value;
        for (int i = 0; i < tvManage.Nodes.Count; i++)
        {
            if (tvManage.Nodes[i].Value == strNodeValue)
            {
                tvManage.Nodes[i].Expand();
            }
            else
            {
                tvManage.Nodes[i].Collapse();
            }
        }
    }
}