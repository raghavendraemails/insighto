﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Collections;
using Insighto.Business.ValueObjects;
using App_Code;
namespace Insigtho.Pages.UserControls.FaqControls
{
    public partial class CMSCreateLeftMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              BindCreateSurveyTreeview();
            }
        }
        public void BindCreateSurveyTreeview()
        {
            osm_faqpages faqPage = new osm_faqpages();
            faqPage.Page_Name_Group = "Create Survey";
            var faqService = ServiceFactory.GetService<FAQService>();
            var pages = faqService.GetFAQPages(faqPage);

            IEnumerable<IGrouping<string, string>> parentPage;

            parentPage = (from p in pages
                          orderby p.Page_Name_Group ascending
                          group p.Page_Name_Parent by p.Page_Name_Parent into subcats
                          select subcats);

            TreeNode parentNode = null;

            foreach (var node in parentPage)
            {
                parentNode = new TreeNode(node.Key, node.Key);
                var subpages = faqService.GetSubPages(node.Key);
                var childPages = (from p in subpages
                                  orderby p.Page_Sort ascending
                                  group p.Page_Name by p.Page_Name into subcats
                                  select subcats);

                foreach (var cp in childPages)
                {
                    if (cp.Key != null)
                    {
                        TreeNode childNode = new TreeNode(cp.Key, cp.Key);
                        childNode.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Admin/FAQContentManagement.aspx", "page=Create_Survey&pageName=" + cp.Key + "&parentNode=" + node.Key);
                        parentNode.ChildNodes.Add(childNode);
                    }

                }
                tvHelpIcon.Nodes.Add(parentNode);
            }
        }
    }
}