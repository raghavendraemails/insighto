﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CMSDesignLeftMenu.ascx.cs" Inherits="Insigtho.Pages.UserControls.FaqControls.CMSDesignLeftMenu" %>
<div class="faqCmsMenu">
    <!-- design menu -->
    <div class="faqSearchPanel">
        DESIGN SURVEY
    </div>
    <div class="menuLeft">
    <asp:TreeView ID="tvDesign" CssClass="FAQLeafNode" runat="server" NodeIndent="0" NodeWrap="true"
        ShowLines="false" SelectedNodeStyle-CssClass="active">
   
     <NodeStyle CssClass="mainNode" />        
        <LeafNodeStyle CssClass="FaqSubNode" />
    </asp:TreeView>  
 </div>
    <!-- //design menu -->
</div>