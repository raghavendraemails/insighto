﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Collections;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using App_Code;
namespace Insigtho.Pages.UserControls.FaqControls
{
    public partial class FaqTopMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                lnkCreate.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Faqcms/CNS_AboutSurvey.aspx", "page=Create&pageName=About survey creation&parentNode=Create New Survey");
                lnkDesign.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Faqcms/Design_Theme.aspx", "page=Design&pageName=Theme&parentNode=Choose look and feel");
                lnkAnalyze.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Faqcms/Analyze_Reports.aspx", "page=Analyze&pageName=Reports&parentNode=View Reports");
                lnkLaunch.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Faqcms/Launch_ScheduleLaunch.aspx", "page=Launch&pageName=Schedule launch&parentNode=Survey / Alerts Scheduler");
                lnkManage.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Faqcms/Manage_SurveyOverview.aspx", "page=Manage&pageName=Overview&parentNode=My Surveys");
                   
            }
        }
    }
}