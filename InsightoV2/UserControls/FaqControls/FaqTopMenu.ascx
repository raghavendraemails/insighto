﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FaqTopMenu.ascx.cs" Inherits="Insigtho.Pages.UserControls.FaqControls.FaqTopMenu" %>

<script type="text/javascript" src="../../Scripts/dropdowntabs.js"></script>
<link rel="stylesheet" type="text/css" href="../../Styles/ddcolortabs.css" />
<div id="topLinksPanel">
    &nbsp;
</div>
<div id="header">
            <!-- header -->
            <div class="insightoLogoPanel">
                <!-- insighto logo -->
                <asp:HyperLink ID="lnkLogo"  
                    ImageUrl="~/App_Themes/Classic/Images/logo-insighto.jpg" Text="Insighto" 
                    runat="server" Target="_blank" meta:resourcekey="lnkLogoResource1"></asp:HyperLink>
                    
                  
                <!-- insighto logo -->
            </div>
            <div class="insightoTagLine">
            </div>
            <!-- //header -->
  </div>
<div id="topMainMenu">
    <!-- top main menu -->
    <div id="topMainLeftLinks">
        <!-- top left menu-->
        <ul id="nav">
            <li id="liMyAccount" runat="server">
                <asp:HyperLink ID="lnkCreate" Text="Create Survey" runat="server"></asp:HyperLink></li>
            <li id="liAddressBook" runat="server">
                <asp:HyperLink ID="lnkDesign" Text="Design" runat="server"></asp:HyperLink></li>
                <li id="li1" runat="server">
                <asp:HyperLink ID="lnkLaunch" Text="Launch" runat="server"></asp:HyperLink></li>
                <li id="li2" runat="server">
                <asp:HyperLink ID="lnkAnalyze" Text="Analyze" runat="server"></asp:HyperLink></li>
                <li id="li3" runat="server">
                <asp:HyperLink ID="lnkManage" Text="Manage" runat="server"></asp:HyperLink></li>
             
        </ul>
        <!-- //top left menu-->
    </div>
   
    <!-- //top menu right -->
</div>

 <%--<script type="text/javascript">
     $(document).ready(function ($) {
         $('a[rel*=framebox]').click(function (e) {
             e.preventDefault();
             ApplyFrameBox($(this));
         });
     });
</script>--%>
  <script type="text/javascript" >
      $(document).ready(function () {
          $('a').each(function () {
              if (document.URL.indexOf($(this).attr('href')) >= 0) {
                  //alert($(this).parentsUntil);
                  $(this).parentsUntil(".menu").addClass('activelink');
              }
          });
      });
    </script>

