﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CMSLaunchLeftMenu.ascx.cs" Inherits="Insigtho.Pages.UserControls.FaqControls.CMSLaunchLeftMenu" %>
<div class="faqCmsMenu"><!-- launch menu -->
<div class="faqSearchPanel">
        LAUNCH SURVEY
    </div>
    <div class="menuLeft">
<asp:TreeView ID="tvLaunch" runat="server" CssClass="FAQLeafNode" NodeIndent="0" NodeWrap="true"
        ShowLines="false" SelectedNodeStyle-CssClass="active">
   
     <NodeStyle CssClass="mainNode" />        
        <LeafNodeStyle CssClass="FaqSubNode" />
    </asp:TreeView>  
    </div>
<!-- //launch menu --></div>