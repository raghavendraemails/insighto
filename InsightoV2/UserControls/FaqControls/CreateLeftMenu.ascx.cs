﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Collections;
using Insighto.Business.ValueObjects;
using App_Code;
namespace Insigtho.Pages.UserControls.FaqControls
{

    public partial class FAQLeftMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCreateSurveyTreeview();
                if (Request.QueryString["key"] == null)
                    tvHelpIcon.Nodes[0].Expand();
                else
                {
                    Hashtable ht = new Hashtable();
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht["page"].ToString().ToUpper() =="CREATE")
                    {
                        tvHelpIcon.FindNode(ht["parentNode"].ToString()).Expand();
                    }
                }
            }
        }

        protected void tvHelpIcon_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
        {
            if (tvHelpIcon.Nodes == null)
                return;
            string strNodeValue = e.Node.Value;
            for (int i = 0; i < tvHelpIcon.Nodes.Count; i++)
            {
                if (tvHelpIcon.Nodes[i].Value == strNodeValue)
                {
                    tvHelpIcon.Nodes[i].Expand();
                }
                else
                {
                    tvHelpIcon.Nodes[i].Collapse();
                }
            }
        }        

        public void BindCreateSurveyTreeview()
        {
            osm_faqpages faqPage = new osm_faqpages();
            faqPage.Page_Name_Group = "Create Survey";
            var faqService = ServiceFactory.GetService<FAQService>();
            var pages = faqService.GetFAQPages(faqPage);

            IEnumerable<IGrouping<string, string>> parentPage;

            parentPage = (from p in pages
                          orderby p.Page_Name_Group ascending
                          group p.Page_Name_Parent by p.Page_Name_Parent into subcats
                          select subcats);

            TreeNode parentNode = null;           

            foreach (var node in parentPage)
            {
                parentNode = new TreeNode(node.Key, node.Key);
                parentNode.SelectAction = TreeNodeSelectAction.Expand;
                var subpages = faqService.GetSubPages(node.Key);
                var childPages = (from p in subpages
                                  orderby p.Page_Sort ascending
                                  group p.Page_Name by p.Page_Name into subcats
                                  select subcats);

                foreach (var cp in childPages)
                {
                    if (cp.Key != null)
                    {
                        TreeNode childNode = new TreeNode(cp.Key, cp.Key);
                        var pageURL = faqService.GetPageURL(cp.Key, node.Key);
                        childNode.NavigateUrl = EncryptHelper.EncryptQuerystring(pageURL.Page_URL, "page=Create&pageName=" + cp.Key + "&parentNode=" + node.Key);
                       parentNode.ChildNodes.Add(childNode);
                    }

                }
                parentNode.Selected = false;
                tvHelpIcon.Nodes.Add(parentNode);
                tvHelpIcon.CollapseAll();
                tvHelpIcon.ShowExpandCollapse = true;
              

            }
        }
    }
}