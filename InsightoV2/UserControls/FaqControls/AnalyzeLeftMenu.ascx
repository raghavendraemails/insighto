﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AnalyzeLeftMenu.ascx.cs" Inherits="UserControls_FaqControls_AnalyzeLeftMenu" %>
<div style="display:block"><!--analyze menu -->
<div class="faqSearchPanel">
        ANALYZE SURVEY
    </div>
    <div class="menuLeft">
    <asp:TreeView ID="tvAnalyze" CssClass="FAQLeafNode" runat="server" NodeIndent="0" NodeWrap="true"
        ShowLines="false" SelectedNodeStyle-CssClass="active" OnTreeNodeExpanded="tvAnalyze_TreeNodeExpanded">
    
     <NodeStyle CssClass="mainNode" />        
        <LeafNodeStyle CssClass="FaqSubNode" />
    </asp:TreeView>  
    </div>
<!--//analyzemenu --></div>
