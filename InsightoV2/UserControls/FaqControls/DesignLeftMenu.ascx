﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DesignLeftMenu.ascx.cs" Inherits="UserControls_FaqControls_DesignLeftMenu" %>
<div style="display:block">
    <!-- design menu -->
    <div class="faqSearchPanel">
        DESIGN SURVEY
    </div>
    <div class="menuLeft">
    <asp:TreeView ID="tvDesign" CssClass="FAQLeafNode" runat="server" NodeIndent="0" NodeWrap="true"
        ShowLines="false" SelectedNodeStyle-CssClass="active" OnTreeNodeExpanded="tvDesign_TreeNodeExpanded">
    
     <NodeStyle CssClass="mainNode" />        
        <LeafNodeStyle CssClass="FaqSubNode" />
    </asp:TreeView>  
 </div>
    <!-- //design menu -->
</div>
 