﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageLeftMenu.ascx.cs" Inherits="UserControls_FaqControls_ManageLeftMenu" %>
<div style="display:block;"><!-- manage menu-->
    <div class="faqSearchPanel">
        MANAGE SURVEY
    </div>
    <div class="menuLeft">
    <asp:TreeView ID="tvManage" runat="server" CssClass="FAQLeafNode" SelectedNodeStyle-CssClass="active" NodeIndent="0" NodeWrap="true"
        ShowLines="false" OnTreeNodeExpanded="tvManage_TreeNodeExpanded">
    
     <NodeStyle CssClass="mainNode" />        
        <LeafNodeStyle CssClass="FaqSubNode" />
    </asp:TreeView>   
    
  </div>
<!-- //manage menu--></div>
 