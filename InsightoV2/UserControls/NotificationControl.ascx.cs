﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using System.Web.UI.HtmlControls;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using App_Code;

public partial class NotificationControl : UserControl
{
    #region Private Properties

    private const string KEY_NOTIFICATION_MESSAGES = "NotificationMessages";

    private static List<NotificationMessage> NotificationMessages
    {
        get
        {
            List<NotificationMessage> messages = (List<NotificationMessage>)
                HttpContext.Current.Session[KEY_NOTIFICATION_MESSAGES];
            return messages;
        }
    }

    #endregion
  

    public static void AddMessage(NotificationMessage msg)
    {
        List<NotificationMessage> messages = NotificationMessages;
        if (messages == null)
        {
            messages = new List<NotificationMessage>();
        }
        messages.Add(msg);
        HttpContext.Current.Session[KEY_NOTIFICATION_MESSAGES] = messages;
    }

    private static void ClearMessages()
    {
        HttpContext.Current.Session[KEY_NOTIFICATION_MESSAGES] = null;
    }

    public static void AddInfoMessage(string msg, bool autoHide = true, bool buttonRenew = true)
    {
        AddMessage(new NotificationMessage()
        {
            Text = msg,
            Type = MessageType.Info,
            AutoHide = autoHide,
            ButtonRenew = buttonRenew

        });
    }

   
    public static void AddSuccessMessage(string msg, bool autoHide = true, bool buttonRenew = true)
    {

        SessionStateService sessionStateService = new SessionStateService();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        if (loggedInUserInfo.LicenseOpted == "SOCIALPREMIUM")
        {
            buttonRenew = false;
        }
        else
        {
            buttonRenew = true;
        }

        AddMessage(new NotificationMessage()
        {
            Text = msg,
            Type = MessageType.Success,
            AutoHide = autoHide,
            ButtonRenew = buttonRenew
        });
    }


    public static void AddSuccessMessagemaxAllowedResponseLimit(string msg, bool autoHide = true, bool buttonRenew = true)
    {

        SessionStateService sessionStateService = new SessionStateService();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

       
            buttonRenew = false;
       

        AddMessage(new NotificationMessage()
        {
            Text = msg,
            Type = MessageType.Success,
            AutoHide = autoHide,
            ButtonRenew = buttonRenew
        });
    }

    public static void AddWarningMessage(string msg, bool autoHide = false, bool buttonRenew = true)
    {
        AddMessage(new NotificationMessage()
        {
            Text = msg,
            Type = MessageType.Warning,
            AutoHide = autoHide,
            ButtonRenew = buttonRenew
        });
    }

    public static void AddErrorMessage(string msg, bool autoHide = false, bool buttonRenew = true)
    {
        AddMessage(new NotificationMessage()
        {
            Text = msg,
            Type = MessageType.Error,
            AutoHide = autoHide,
            ButtonRenew = buttonRenew
        });
    }
    private bool IsLicenseExpiringSoon(LoggedInUserInfo loggedInUser)
    {
        if (loggedInUser == null)
            return false;

        TimeSpan ts = Convert.ToDateTime(loggedInUser.ExpiryDate) - DateTime.UtcNow;
        return Utilities.GetUserType() != UserType.FREE && ts.Days <= 15;
    }
    protected void Page_Prerender(object sender, EventArgs e)
    {
        SessionStateService sessionStateService = new SessionStateService();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        if (NotificationMessages != null)
        {
            int index = 1;
            foreach (var msg in NotificationMessages)
            {
                Panel msgPanel = new Panel();

                if (loggedInUserInfo.LicenseOpted == "SOCIALPREMIUM")
                {
                    if (IsLicenseExpiringSoon(loggedInUserInfo))
                    {
                        msgPanel.CssClass = "PanelNotificationBox PanelErrorSocialSharing";
                    }
                    else
                    {
                        msgPanel.CssClass = "PanelNotificationBox PanelSuccessSocialSharing";
                    }
                }
                else
                {
                    msgPanel.CssClass = "PanelNotificationBox Panel" + msg.Type;
                }
                if (msg.AutoHide)
                {
                    msgPanel.CssClass += " AutoHide";
                }
                msgPanel.ID = msg.Type + "Msg" + index;

                //set message
                Label msgLiteral = new Label();
                msgLiteral.Text = msg.Text;
                msgPanel.Controls.Add(msgLiteral);

                //set renew link
                var loggedInUser = Session["UserInfo"] as LoggedInUserInfo;
               // string renewLink = loggedInUser != null ? Utilities.GetAbsoluteUrl(EncryptHelper.EncryptQuerystring(PathHelper.GetPricingURL(), Constants.USERID + "=" + loggedInUser.UserId + "&Flag=Renew"), Page) : "#";
                string renewLink = "";
                if (loggedInUser != null)
                {
                    string NavUrl = Constants.LicenseType + "=" + loggedInUser.LicenseType + "&UserId=" + loggedInUser.UserId;
                    renewLink = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl);
                     //renewLink = EncryptHelper.EncryptQuerystring(PathHelper.GetPricingURL(), Constants.USERID + "=" + loggedInUser.UserId + "&Flag=Renew");
                 }  
                
                HtmlAnchor lnkRenew = new HtmlAnchor();
                lnkRenew.HRef = renewLink;
                lnkRenew.InnerText = "Renew";
                lnkRenew.Title = "Renew";
                lnkRenew.Attributes.Add("class", "dynamicHyperLink");
                lnkRenew.Visible = msg.ButtonRenew;
                msgPanel.Controls.Add(lnkRenew);

                //set close button link
                HtmlAnchor lnkClose = new HtmlAnchor();
                lnkClose.HRef = "javascript:void(0);";
                lnkClose.Attributes.Add("onclick", "javascript:closePanel(" + msgPanel.ID + ")");
                lnkClose.Attributes.Add("class", "notificationCloseLink");
                lnkClose.InnerText = " ";
                lnkClose.Title = "Close";
                //lnkClose.ClientIDMode = ClientIDMode.Static;
                lnkClose.ID = "lnkCloseNotification";
                msgPanel.Controls.Add(lnkClose);
                
                this.Controls.Add(msgPanel);
                index++;
            }
            ClearMessages();

            IncludeTheCssAndJavaScript();
        }
    }

    private void IncludeTheCssAndJavaScript()
    {
        ClientScriptManager cs = Page.ClientScript;

        // Include the ErrorSuccessNotifier.js library (if not already included)
        string notifierScriptURL = "/Scripts/ErrorSuccessNotifier.js";
        if (!cs.IsStartupScriptRegistered(notifierScriptURL))
        {
            cs.RegisterClientScriptInclude(notifierScriptURL, notifierScriptURL);
        }
    }
}