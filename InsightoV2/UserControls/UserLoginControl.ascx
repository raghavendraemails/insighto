﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserLoginControl.ascx.cs"
    Inherits="UserControls_UserLoginControl" %>
<%@ Register TagPrefix="uc" TagName="OpenAuthLogin" Src="~/UserControls/OpenAuthLogin.ascx" %>

    <div class="LoginNewPanel" style="width:500px; margin:50px auto 0 auto; float:none; padding-top:30px;">
        <!-- content panel -->        
        <div class="errorPanel" id="dvErrMsg" runat="server">
            <asp:Label ID="lblErrMsg" runat="server" Visible="False" CssClass="lblRequired" 
                meta:resourcekey="lblErrMsgResource1"></asp:Label>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblEmailId" runat="server" AssociatedControlID="txtEmailId" meta:resourcekey="lblEmailId" /></div>
            <div class="loginControls">
                <asp:TextBox ID="txtEmailId" runat="server" CssClass="textBoxLogin" MaxLength="100"
                    TabIndex="1" meta:resourcekey="txtEmailIdResource1" /></div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqEmailId" ControlToValidate="txtEmailId"
                    CssClass="lblRequired" runat="server" Display="Dynamic" meta:resourcekey="reqEmailIdResource1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator SetFocusOnError="True" ID="regEmailId" runat="server"
                    ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                    CssClass="lblRequired" ControlToValidate="txtEmailId" 
                    Display="Dynamic"    meta:resourcekey="regEmailIdResource1"></asp:RegularExpressionValidator></div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblPassword" runat="server" AssociatedControlID="txtPassword" meta:resourcekey="lblPassword" /></div>
            <div class="loginControls">
                <asp:TextBox ID="txtPassword" runat="server" CssClass="textBoxLogin" MaxLength="16"
                    TabIndex="2" TextMode="Password" meta:resourcekey="txtPasswordResource1" /></div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqPassword" CssClass="lblRequired"
                    ControlToValidate="txtPassword" runat="server" Display="Dynamic" meta:resourcekey="reqPasswordResource1"></asp:RequiredFieldValidator></div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:Button ID="ibtnLogin" runat="server" TabIndex="3"
                    CssClass="dynamicButton" meta:resourcekey="ibtnLoginResource1" OnClick="ibtnLogin_Click" />
                    <asp:HyperLink ID="hlkForgotPassword" runat="server" CssClass="orangeColor" 
                                        Text="ForgotPassword" h="350" w="600" scrolling='yes' NavigateUrl="../ForgotPassword.aspx" ToolTip="Forgot Password"
                                        alt="ForgotPassword" title="ForgotPassword" />
                    </div>
            
        </div>
 
        <div class="clear">
        </div>
        
    </div>



<script type="text/javascript">
    function fnOpenWindow(reqType) {
        window.open("LoginWithFavoriteAccount.aspx?key=" + reqType, null,
    "height=480,width=850,status=yes,toolbar=no,menubar=no,location=no");

    }
    function ClearData() {

        document.getElementById("<%= lblErrMsg.ClientID %>").style.visibility = 'hidden';
    }

</script>
