﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuDropDownControl.ascx.cs"
    Inherits="UserControls.RespondentControls.MenuDropDownControl" %>
<%@ Register Src="DisplayQuestionControl.ascx" TagName="DisplayQuestionControl" TagPrefix="Insighto" %>

<div>
    <Insighto:DisplayQuestionControl ID="ucDisplayQuestionControl" runat="server" />
</div>
<div class="ResondLbl">
<%--<asp:ScriptManager ID="scriptmanager1" runat="server"></asp:ScriptManager>
 <asp:UpdatePanel ID="updatepanel1" runat="server"><ContentTemplate>--%>
    <p>
        <asp:CustomValidator SetFocusOnError="true" ID="cvMenu" runat="server" CssClass="errorMesg"
            ErrorMessage="This is mandatory." OnServerValidate="cvMenu_ServerValidate" /></p>
    <p>
   
        <asp:DropDownList ID="ddlDropDownMenu" runat="server" CssClass="dropDownMedium"  AutoPostBack="true" 
            onselectedindexchanged="ddlDropDownMenu_SelectedIndexChanged">
        </asp:DropDownList>
        
    </p>
    
        <asp:Label Text="Other" ID="lblOther" Visible="false" runat="server" />
        &nbsp;<asp:TextBox runat="server" ID="txtOther" Visible="true" CssClass="textBoxMedium" />
        <asp:HiddenField ID="hdnother" runat="server" />
      <%--  </ContentTemplate></asp:UpdatePanel>--%>
</div>
