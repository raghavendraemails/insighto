﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Business.Enumerations;

namespace Insighto.UserControls.RespondentControls
{
    public partial class RespondentPopupHeader : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets the base page.
        /// </summary>
        public SurveyRespondentPageBase BasePage
        {
            get
            {
                return Page as SurveyRespondentPageBase;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            divPopupHeading.Visible = !(RespondentDisplayMode.Respondent == BasePage.Mode || RespondentDisplayMode.RespondentWithRedirection == BasePage.Mode);
            dvPopUpPrint.Visible = (RespondentDisplayMode.Preview == BasePage.Mode);
        }
    }
}