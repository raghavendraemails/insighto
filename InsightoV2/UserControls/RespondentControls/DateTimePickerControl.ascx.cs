﻿using System;
using System.Collections.Generic;
using System.Linq;
using App_Code;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using System.Globalization;
using System.Web.UI.WebControls;

namespace UserControls.RespondentControls
{
    public partial class DateTimePickerControl : SurveyRespondentControlBase
    {
        private List<osm_responsequestions> _lstResponseOtions;

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions
        {
            get
            {
                return base.SurveyQuestionAndAnswerOptions;
            }
            set
            {
                base.SurveyQuestionAndAnswerOptions = value;
                ucDisplayQuestionControl.SurveyQuestionAndAnswerOptions = value;

            }
        }

        public override bool IsRequired
        {
            get { return Question.RESPONSE_REQUIRED > 0; }
        }
        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            txtDate.Attributes.Add("Readonly", "true");
            if (SurveyQuestionAndAnswerOptions == null || SurveyQuestionAndAnswerOptions.Question == null)
                return;

            //if user is halfway through
            cvDateTime.Visible = IsRequired;
            var option = SurveyQuestionAndAnswerOptions.AnswerOption.Take(1).FirstOrDefault();
            var existedResponse = Utilities.GetResponseQuestion(RespondentAnswerOptions, option != null ? option.ANSWER_ID : 0);

            if (!IsPostBack)
                txtDate.Text = existedResponse != null ? string.Format("{0:dd-MMM-yyyy HH:mm}", existedResponse.ANSWER_TEXT) : string.Empty;
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                _lstResponseOtions = new List<osm_responsequestions>();
                if (!Utilities.IsEmptyRecord(null, txtDate))
                {
                    DateTime dateValue;
                    var response = GetInstance(0);
                    response.RESPONDENT_ID = RespondentPageBase.RespondentId;
                    response.QUESTION_ID = ValidationHelper.GetInteger(Question.QUESTION_ID, 0);
                    response.ANSWER_ID = 0;
                    response.ANSWER_TEXT = !string.IsNullOrEmpty(txtDate.Text) ? Convert.ToDateTime(txtDate.Text).ToString() : string.Empty;

                    _lstResponseOtions.Add(response);
                }
                return _lstResponseOtions;
            }
        }

        #region Auxillary methods

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="deleted">The deleted.</param>
        /// <returns></returns>
        private static osm_responsequestions GetInstance(int deleted)
        {
            return new osm_responsequestions { DELETED = deleted, RESPONSE_RECEIVED_ON = DateTime.UtcNow };
        }

        /// <summary>
        /// Handles the ServerValidate event of the cvMoms control.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="args">The <see cref="System.Web.UI.WebControls.ServerValidateEventArgs"/> instance containing the event data.</param>
        protected void cvDateTime_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (IsRequired)
                args.IsValid = !string.IsNullOrEmpty(txtDate.Text);
        }

        #endregion
    }
}