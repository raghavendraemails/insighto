﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateTimePickerControl.ascx.cs"
    Inherits="UserControls.RespondentControls.DateTimePickerControl" %>
<%@ Register Src="DisplayQuestionControl.ascx" TagName="DisplayQuestionControl" TagPrefix="Insighto" %>
<div class="surveyResponseQContent">
    <Insighto:DisplayQuestionControl ID="ucDisplayQuestionControl" runat="server" />
    <div class="ResondLbl">
        <table cellpadding="0" cellspacing="0" class="datetbllbl">
            <tr>
                <td colspan="2">
                    <asp:CustomValidator SetFocusOnError="true" ID="cvDateTime" runat="server" CssClass="errorMesg"
                        ErrorMessage="This is mandatory." OnServerValidate="cvDateTime_ServerValidate" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDateTime" Text="Date" />
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="textBoxDateToTime txtDate" ID="txtDate" />
                </td>
                <td>
                    &nbsp;<a id="lnkDateClear" href='javascript:void(0)' title="Clear">Clear</a>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var currentTime = new Date();
            $(".txtDate").datetimepicker({
                dateFormat: 'dd-M-yy',
                changeMonth: true,
                changeYear: true,
                showOn: "button",
                buttonImage: "App_Themes/Classic/Images/icon-calander.gif",
                buttonImageOnly: true,
                yearRange: '1904:2050',
                buttonText: "Calendar"
            });
        });
        $('#lnkDateClear').click(function () {
            $('.txtDate').val('');
        });
    </script>
</div>
