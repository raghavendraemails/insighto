﻿using System;
using System.Collections.Generic;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using Insighto.Data;
using CovalenseUtilities.Services;

namespace UserControls.RespondentControls
{
    public partial class IntroductionTextControl : SurveyRespondentControlBase
    {
        public override void Initialise()
        {

        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                return new List<osm_responsequestions>();
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
        }

        public override void Display()
        {
            if (SurveyQuestionAndAnswerOptions == null || Question == null)
                return;

            lblSurveyIntroduction.Text = ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateIntroductionQuestion(Question.QUSETION_LABEL);
            lblSurveyIntroduction.Visible = RespondentPageBase.PageIndex == 1;
        }
    }
}