﻿using System;
using System.Collections.Generic;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Services;
using Insighto.Data;

namespace UserControls.RespondentControls
{
    public partial class NarrationHeaderControl : SurveyRespondentControlBase
    {
        public override void Initialise()
        {

        }


        protected override void OnInit(EventArgs e)
        {
            Initialise();
            Display();
            base.OnInit(e);
        }

        public override void Display()
        {
            if (Question == null)
                return;
            lblHeader.Text = ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateNarrationQuestion(Question.QUSETION_LABEL);
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                return new List<osm_responsequestions>();
            }
        }
    }
}