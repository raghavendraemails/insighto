﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using App_Code;
using Insighto.Data;
using Insighto.Business.ValueObjects;

namespace UserControls.RespondentControls
{
    public partial class VideoConrol : SurveyRespondentControlBase
    {
        public override void Initialise()
        {

        }

        public VideoTypes DisplayVideoType
        {
            get
            {
                return ServiceFactory<RespondentOtherTypeQuestions>.Instance.GetVideoDispalyType(Question.QUSETION_LABEL);
            }
        }

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions
        {
            get
            {
                return base.SurveyQuestionAndAnswerOptions;
            }
            set
            {
                base.SurveyQuestionAndAnswerOptions = value;
                ucNarrationHeaderControl.SurveyQuestionAndAnswerOptions = value;

            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                return new List<osm_responsequestions>();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            Initialise();
            Display();
            base.OnInit(e);
        }

        public override void Display()
        {
            if (SurveyQuestionAndAnswerOptions == null || SurveyQuestionAndAnswerOptions.Question == null || SurveyQuestionAndAnswerOptions.AnswerOption == null)
                return;

            foreach (var videoItem in SurveyQuestionAndAnswerOptions.AnswerOption)
            {
                divCustomVideo.Controls.Add(ServiceFactory<RespondentOtherTypeQuestions>.Instance.DisplayVideoQuestion(videoItem.ANSWER_OPTIONS, videoItem.ANSWER_ID.ToString()));
            }
        }
    }
}