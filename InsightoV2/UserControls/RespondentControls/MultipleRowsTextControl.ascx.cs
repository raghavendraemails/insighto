﻿using System;
using System.Collections.Generic;
using System.Linq;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;

namespace UserControls.RespondentControls
{
    public partial class MultipleRowsTextControl : SurveyRespondentControlBase
    {
        #region Private properties

        private string _textControlPrefix = string.Empty;
        private List<osm_responsequestions> _lstResponseOtions;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions
        {
            get
            {
                return base.SurveyQuestionAndAnswerOptions;
            }
            set
            {
                base.SurveyQuestionAndAnswerOptions = value;
                ucDisplayQuestionControl.SurveyQuestionAndAnswerOptions = value;

            }
        }

        /// <summary>
        /// Gets or sets the control id prefix.
        /// </summary>
        /// <value>
        /// The control id prefix.
        /// </value>
        public override string ControlIdPrefix
        {
            get
            {
                return _textControlPrefix;
            }
            set
            {
                _textControlPrefix = value;
            }
        }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        /// 
        List<SurveyRespondentControlBase> _lstControls;
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                _lstResponseOtions = new List<osm_responsequestions>();
                _lstControls = CommonHelper.FindControlRecursive<SurveyRespondentControlBase>(divMultipleTextControls, (QuestionType == QuestionType.Text_MultipleBoxes) ? SurveyQuestionAndAnswerOptions.AnswerOption.Count : 1);

                foreach (var responseList in _lstControls.Where(item => item.SelectedAnswerOptions.Count > 0).Select(item => item.SelectedAnswerOptions).Where(responseList => responseList.Count > 0))
                {
                    _lstResponseOtions.AddRange(responseList);
                }
                return _lstResponseOtions;
            }
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            Initialise();
            Display();

            base.OnInit(e);
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        public override void Initialise()
        {

        }

        /// <summary>
        /// Displays this instance.
        /// </summary>
        public override void Display()
        {
            switch (QuestionType)
            {
                case QuestionType.Text_CommentBox:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateTextCommentBoxQuestion(Page, divMultipleTextControls, SurveyQuestionAndAnswerOptions, RespondentAnswerOptions, true);
                    break;

                case QuestionType.Text_SingleRowBox:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateTextSingleRowBoxQuestion(Page, divMultipleTextControls, SurveyQuestionAndAnswerOptions, RespondentAnswerOptions, true);
                    break;

                case QuestionType.Numeric_SingleRowBox:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateNumericSingleRowBoxQuestion(Page, divMultipleTextControls, SurveyQuestionAndAnswerOptions, RespondentAnswerOptions, true);
                    break;

                case QuestionType.EmailAddress:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateEmailAddressQuestion(Page, divMultipleTextControls, SurveyQuestionAndAnswerOptions, RespondentAnswerOptions, true);
                    break;

                case QuestionType.Text_MultipleBoxes:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateTextMultipleBoxesQuestion(Page, divMultipleTextControls, SurveyQuestionAndAnswerOptions, RespondentAnswerOptions, true);
                    break;
            }

        }

        #endregion

        public const string QtypePrefix = "tx";
    }
}