﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using System.Linq;
using Insighto.Business.Helpers;

namespace UserControls.RespondentControls
{
    
    public partial class MenuDropDownControl : SurveyRespondentControlBase
    {
        SurveyCore othertext = new SurveyCore();

        #region Private properties

        private string _dropdownPrefix = "dd";
        private List<osm_responsequestions> _lstResponseOtions;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the control id prefix.
        /// </summary>
        /// <value>
        /// The control id prefix.
        /// </value>
        public override string ControlIdPrefix
        {
            get
            {
                return _dropdownPrefix;
            }
            set
            {
                _dropdownPrefix = value;
            }
        }

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions
        {
            get
            {
                return base.SurveyQuestionAndAnswerOptions;
            }
            set
            {
                base.SurveyQuestionAndAnswerOptions = value;
                ucDisplayQuestionControl.SurveyQuestionAndAnswerOptions = value;

            }
        }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets or sets the selected anwer value.
        /// </summary>
        /// <value>
        /// The selected anwer value.
        /// </value>
        public override int SelectedAnwerValue { get; set; }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                _lstResponseOtions = new List<osm_responsequestions>();
                if (!Utilities.IsEmptyRecord(ddlDropDownMenu))
                {
                    var response = GetInstance(0);
                    response.RESPONDENT_ID = RespondentPageBase.RespondentId;
                    response.QUESTION_ID = ValidationHelper.GetInteger(Question.QUESTION_ID, 0);
                    response.ANSWER_ID = ddlDropDownMenu.SelectedIndex > 0 ? GetValue(ddlDropDownMenu.SelectedValue, false) : 0;
                    response.ANSWER_TEXT = string.Empty;

                    SelectedAnwerValue = response.ANSWER_ID;
                    _lstResponseOtions.Add(response);
                }
                if (!Utilities.IsEmptyRecord(txtOther))
                {
                    var response = GetInstance(0);
                    response.RESPONDENT_ID = RespondentPageBase.RespondentId;
                    response.QUESTION_ID = ValidationHelper.GetInteger(Question.QUESTION_ID, 0);
                    response.ANSWER_ID = 0;
                    response.ANSWER_TEXT = txtOther.Text.Trim();
                    lblOther.Text = hdnother.Value;

                    _lstResponseOtions.Add(response);
                }
                return _lstResponseOtions;
            }
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
            base.OnLoad(sender, e);
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        public override void Initialise()
        {
            cvMenu.Enabled = IsRequired;
            cvMenu.Visible = IsRequired;
        }

        /// <summary>
        /// Displays this instance.
        /// </summary>
        public override void Display()
        {
            if (SurveyQuestionAndAnswerOptions == null || SurveyQuestionAndAnswerOptions.Question == null || SurveyQuestionAndAnswerOptions.AnswerOption == null)
                return;

            // Utilities.FillAnswerOptions(ddlDropDownMenu, SurveyQuestionAndAnswerOptions, ControlIdPrefix, RespondentAnswerOptions, txtOther, Question.RANDOMIZE_ANSWERS > 0, true);
            ServiceFactory<RespondentMultipleQuestions>.Instance.CreateDropdownMenuQuestion(ddlDropDownMenu, SurveyQuestionAndAnswerOptions, ControlIdPrefix, RespondentAnswerOptions, txtOther, Question.RANDOMIZE_ANSWERS > 0, true);
            osm_responsequestions answer = RespondentAnswerOptions != null ? RespondentAnswerOptions.Where(p => p.QUESTION_ID == SurveyQuestionAndAnswerOptions.Question.QUESTION_ID).FirstOrDefault() : new osm_responsequestions();
            ddlDropDownMenu.SelectedIndex = answer != null ? ddlDropDownMenu.Items.IndexOf(ddlDropDownMenu.Items.FindByValue(
                SurveyRespondentHelper.FormatControlId("dd", SurveyQuestionAndAnswerOptions.Question.QUESTION_TYPE_ID, answer.QUESTION_ID, answer.ANSWER_ID))) : 0;
           lblOther.Visible = SurveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0;

            if (Question.SURVEY_ID == 9454)
            {
                lblOther.Text = "Otro";
                lblOther.Visible = SurveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0;
            }
            else
            {
                lblOther.Text = othertext.Selectsurveyques_other(Convert.ToInt32(SurveyQuestionAndAnswerOptions.Question.QUESTION_ID));

            }

            //if (SurveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0)
            //{
            //    ddlDropDownMenu.Items.Add(new ListItem("Other"));
            //}
            //lblOther.Visible = true;
            //txtOther.Visible = true;
            //txtOther.Enabled = false;
            //}
            //else
            //{
            //lblOther.Visible = false;
            //txtOther.Visible = false;
            // }
            txtOther.Attributes.Add("data", SurveyRespondentHelper.FormatControlId("txt", Question.QUESTION_TYPE_ID, Question.QUESTION_ID, 0));
        }

        #endregion

        #region Auxillary methods

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="deleted">The deleted.</param>
        /// <returns></returns>
        private static osm_responsequestions GetInstance(int deleted)
        {
            return new osm_responsequestions { DELETED = deleted, RESPONSE_RECEIVED_ON = DateTime.UtcNow };
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="isquestion">if set to <c>true</c> [isquestion].</param>
        /// <returns></returns>
        private static int GetValue(string source, bool isquestion)
        {
            if (string.IsNullOrEmpty(source))
                return 0;

            var ids = source.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
            return Convert.ToInt32(isquestion ? ids[1] : ids[2]);
        }

        #endregion

        protected void cvMenu_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (IsRequired)
                args.IsValid = (ddlDropDownMenu.SelectedItem != null && ddlDropDownMenu.SelectedIndex > 0) || (txtOther.Visible
                                                                                            ? txtOther.Text.Length > 0
                                                                                            : false);
        }
        protected void ddlDropDownMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDropDownMenu.SelectedItem.Text == "-- Select --")
            {
              //  lblOther.Visible = true;
               // txtOther.Visible = true;
                txtOther.Enabled = true;
                txtOther.Focus();
               // txtOther.Attributes.Add("data", SurveyRespondentHelper.FormatControlId("txt", Question.QUESTION_TYPE_ID, Question.QUESTION_ID, 0));
            }
            else
            {
                ddlDropDownMenu.Focus();
                //lblOther.Visible = false;
                //txtOther.Visible = false;
                txtOther.Text = "";
                txtOther.Enabled = false;               


            }
        }

    }
}