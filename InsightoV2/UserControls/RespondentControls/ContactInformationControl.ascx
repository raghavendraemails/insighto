﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactInformationControl.ascx.cs"
    Inherits="UserControls.RespondentControls.ContactInformationControl" %>
<%@ Register Src="DisplayQuestionControl.ascx" TagName="DisplayQuestionControl" TagPrefix="Insighto" %>
<Insighto:DisplayQuestionControl ID="ucDisplayQuestionControl" runat="server" />

<div class="ResondLbl">
<div id="divValidators" runat="server" style="width: 100%" />
<table id="tblContactInfo" runat="server" border="0" cellpadding="0" cellspacing="0" class="contactinfolbl">
</table>
</div>