﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultipleRowsTextControl.ascx.cs"
    Inherits="UserControls.RespondentControls.MultipleRowsTextControl" %>
<%@ Register Src="DisplayQuestionControl.ascx" TagName="DisplayQuestionControl" TagPrefix="Insighto" %>
<Insighto:DisplayQuestionControl ID="ucDisplayQuestionControl" runat="server" />
<div id="divMultipleTextControls" runat="server" />
