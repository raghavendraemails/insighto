﻿using System;
using System.Collections.Generic;
using System.Linq;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using System.Text.RegularExpressions;

namespace UserControls.RespondentControls
{
    public partial class MOMSControl : SurveyRespondentControlBase
    {
        SurveyCore othertext = new SurveyCore();
        #region Private properties

        private string _checkBoxPrefix = "cb";
        private List<osm_responsequestions> _lstResponseOtions;
        private osm_responsequestions _response;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the control id prefix.
        /// </summary>
        /// <value>
        /// The control id prefix.
        /// </value>
        public override string ControlIdPrefix
        {
            get
            {
                return _checkBoxPrefix;
            }
            set
            {
                _checkBoxPrefix = value;
            }
        }

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions
        {
            get
            {
                return base.SurveyQuestionAndAnswerOptions;
            }
            set
            {
                base.SurveyQuestionAndAnswerOptions = value;
                ucDisplayQuestionControl.SurveyQuestionAndAnswerOptions = value;

            }
        }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets or sets the selected anwer value.
        /// </summary>
        /// <value>
        /// The selected anwer value.
        /// </value>
        public new int SelectedAnwerValue
        {
            get
            {
                return base.SelectedAnwerValue;
            }
            set
            {
                base.SelectedAnwerValue = value;
            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                _lstResponseOtions = new List<osm_responsequestions>();
                if (!Utilities.IsEmptyRecord(cbAnswersList))
                {
                    foreach (var item in cbAnswersList.Items.Cast<ListItem>().Where(item => item.Selected))
                    {
                       
                        if (item.Text != "Other")
                        {
                            //if (Question.OTHER_ANS == 1)
                            //{
                                _response = GetInstance(0);
                                _response.RESPONDENT_ID = RespondentPageBase.RespondentId;
                                _response.QUESTION_ID = ValidationHelper.GetInteger(Question.QUESTION_ID, 0);

                         //   bool isnumb =  Regex.IsMatch(item.Value, @"^\d+$");
                          //  if (item.Value.Contains("_"))
                            //if (isnumb == false)
                            //{
                                _response.ANSWER_ID = GetValue(item.Value, false);
                                if (_response.ANSWER_ID == 0)
                                {
                                    _response.ANSWER_TEXT = txtOther.Text.Trim();
                                }
                                else
                                {
                                    _response.ANSWER_TEXT = string.Empty;
                                }
                            //}
                            //else
                            //{
                            //    _response.ANSWER_ID = 0;
                            //    _response.ANSWER_TEXT = txtOther.Text.Trim();
                            //}                                
                                _lstResponseOtions.Add(_response);
                            //}
                            //else
                            //{
                            //    _response = GetInstance(0);
                            //    _response.RESPONDENT_ID = RespondentPageBase.RespondentId;
                            //    _response.QUESTION_ID = ValidationHelper.GetInteger(Question.QUESTION_ID, 0);
                            //    _response.ANSWER_ID = GetValue(item.Value, false);
                            //    _response.ANSWER_TEXT = string.Empty;

                            //    SelectedAnwerValue = _response.ANSWER_ID;
                            //    _lstResponseOtions.Add(_response);
                            //}
                        }
                    }
                }
                if (!Utilities.IsEmptyRecord(txtOther))
                {
                    _response = GetInstance(0);
                    _response.RESPONDENT_ID = RespondentPageBase.RespondentId;
                    _response.QUESTION_ID = ValidationHelper.GetInteger(Question.QUESTION_ID, 0);
                    _response.ANSWER_ID = 0;
                    _response.ANSWER_TEXT = txtOther.Text.Trim();

                    _lstResponseOtions.Add(_response);
                }
                return _lstResponseOtions;
            }
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
            base.OnLoad(sender, e);
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        public override void Initialise()
        {
            cvMoms.Enabled = IsRequired;
            cvMoms.Visible = IsRequired;
            cvMoms.ErrorMessage = "భారతదేశం";
        }

        /// <summary>
        /// Displays this instance.
        /// </summary>
        public override void Display()
        {
            if (SurveyQuestionAndAnswerOptions == null || SurveyQuestionAndAnswerOptions.Question == null || SurveyQuestionAndAnswerOptions.AnswerOption == null)
                return;

            //Utilities.FillAnswerOptions(cbAnswersList, SurveyQuestionAndAnswerOptions, ControlIdPrefix, RespondentAnswerOptions, txtOther, Question.RANDOMIZE_ANSWERS > 0);
            ServiceFactory<RespondentMultipleQuestions>.Instance.CreateMOMSQuestion(cbAnswersList, SurveyQuestionAndAnswerOptions, ControlIdPrefix, RespondentAnswerOptions, txtOther, Question.RANDOMIZE_ANSWERS > 0);
            //lblOther.Visible = SurveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0;
            if (SurveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0)
            {
                string othertxt = othertext.Selectsurveyques_other(Convert.ToInt32(SurveyQuestionAndAnswerOptions.Question.QUESTION_ID));

                ListItem lstAnswerOption = new ListItem
                {
                    Text = othertxt,
                    Value = SurveyRespondentHelper.FormatControlId(ControlIdPrefix,
                                                                   SurveyQuestionAndAnswerOptions.Question.QUESTION_TYPE_ID,
                                                                   SurveyQuestionAndAnswerOptions.Question.QUESTION_ID,
                                                                   0)
                };

                cbAnswersList.Items.Add(lstAnswerOption);
             //  cbAnswersList.Items.Add(new ListItem(othertxt,"0"));
                txtOther.Visible = true;
            }
         //   txtOther.Attributes.Add("data", SurveyRespondentHelper.FormatControlId("txt", Question.QUESTION_TYPE_ID, Question.QUESTION_ID, 0));

        }

        #endregion

        #region Auxillary methods

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="deleted">The deleted.</param>
        /// <returns></returns>
        private static osm_responsequestions GetInstance(int deleted)
        {
            return new osm_responsequestions { DELETED = deleted, RESPONSE_RECEIVED_ON = DateTime.UtcNow };
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="isquestion">if set to <c>true</c> [isquestion].</param>
        /// <returns></returns>
        private static int GetValue(string source, bool isquestion)
        {


            if (string.IsNullOrEmpty(source))
                return 0;

            var ids = source.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
            return Convert.ToInt32(isquestion ? ids[1] : ids[2]);
        }

        #endregion

        /// <summary>
        /// Handles the ServerValidate event of the cvMoms control.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="args">The <see cref="System.Web.UI.WebControls.ServerValidateEventArgs"/> instance containing the event data.</param>
        protected void cvMoms_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (IsRequired)
                args.IsValid = cbAnswersList.SelectedItem != null || (txtOther.Visible ? txtOther.Text.Length > 0 : false);
        }
    }
}