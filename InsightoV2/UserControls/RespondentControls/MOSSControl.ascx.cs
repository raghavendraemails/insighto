﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Helpers;
using Resources;
using Insighto.Business.Helpers;

namespace UserControls.RespondentControls
{
    public partial class MOSSControl : SurveyRespondentControlBase
    {
        #region Private properties

        private string _radioButtonPrefix = "ro";
        private List<osm_responsequestions> _lstResponseOtions;
        private osm_responsequestions _response;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the control id prefix.
        /// </summary>
        /// <value>
        /// The control id prefix.
        /// </value>
        /// 
        SurveyCore othertext = new SurveyCore();
        public override string ControlIdPrefix
        {
            get
            {
                return _radioButtonPrefix;
            }
            set
            {
                _radioButtonPrefix = value;
            }
        }

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions
        {
            get
            {
                return base.SurveyQuestionAndAnswerOptions;
            }
            set
            {
                base.SurveyQuestionAndAnswerOptions = value;
                ucDisplayQuestionControl.SurveyQuestionAndAnswerOptions = value;

            }
        }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets or sets the selected anwer value.
        /// </summary>
        /// <value>
        /// The selected anwer value.
        /// </value>
        public override int SelectedAnwerValue { get; set; }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                _lstResponseOtions = new List<osm_responsequestions>();
                if (!Utilities.IsEmptyRecord(rbAnswersList))
                {
                    if (rbAnswersList.SelectedValue != "Other")
                    {
                        _response = GetInstance(0);
                        _response.RESPONDENT_ID = RespondentPageBase.RespondentId;
                        _response.QUESTION_ID = ValidationHelper.GetInteger(Question.QUESTION_ID, 0);
                        _response.ANSWER_ID = GetValue(rbAnswersList.SelectedValue, false);

                       // _response.ANSWER_TEXT = string.Empty;
                        if (_response.ANSWER_ID == 0)
                        {
                            _response.ANSWER_TEXT = txtOther.Text.Trim();
                        }
                        else
                        {
                            _response.ANSWER_TEXT = string.Empty;
                        }
                        SelectedAnwerValue = _response.ANSWER_ID;
                        _lstResponseOtions.Add(_response);
                    }
                }
                if (!Utilities.IsEmptyRecord(txtOther))
                {
                    _response = GetInstance(0);
                    _response.RESPONDENT_ID = RespondentPageBase.RespondentId;
                    _response.QUESTION_ID = ValidationHelper.GetInteger(Question.QUESTION_ID, 0);
                    _response.ANSWER_ID = 0;
                    _response.ANSWER_TEXT = txtOther.Text.Trim();

                    _lstResponseOtions.Add(_response);
                }
                return _lstResponseOtions;
            }
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// 

        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
            base.OnLoad(sender, e);
            
                
            
        }

        protected override void OnInit(EventArgs e)
        {
            divValidators.Controls.Add(GetCustomValidator());
            base.OnInit(e);
           
        }
        /// <summary>
        /// Initialises this instance.
        /// </summary>
        public override void Initialise()
        {

        }

        private CustomValidator GetCustomValidator()
        {
            var revTemp = new CustomValidator
                              {
                                  CssClass = RespondentResources.ErrorMessageCssClass,
                                  ID = string.Format("cvMoss{0}_{1}", Question.QUESTION_TYPE_ID, Question.QUESTION_ID),
                                  ErrorMessage = CommonMessages.Respondent_Mandatory,
                                  Display = ValidatorDisplay.Dynamic,
                                  Visible = true
                              };
            revTemp.ServerValidate += cvMoss_ServerValidate;
            return revTemp;
        }
        /// <summary>
        /// Displays this instance.
        /// </summary>
        public override void Display()
        {
            if (SurveyQuestionAndAnswerOptions == null || SurveyQuestionAndAnswerOptions.Question == null || SurveyQuestionAndAnswerOptions.AnswerOption == null)
                return;

            var cvmoss = Utilities.FindControl<CustomValidator>(this,
                                                                string.Format("cvMoss{0}_{1}", Question.QUESTION_TYPE_ID,
                                                                              Question.QUESTION_ID));
            if (cvmoss != null)
            {
                cvmoss.Visible = true;
                cvmoss.SetFocusOnError = true;
            }
            // Utilities.FillAnswerOptions(rbAnswersList, SurveyQuestionAndAnswerOptions, ControlIdPrefix, RespondentAnswerOptions, txtOther, Question.RANDOMIZE_ANSWERS > 0);
            ServiceFactory<RespondentMultipleQuestions>.Instance.CreateMossQuestion(rbAnswersList, SurveyQuestionAndAnswerOptions, ControlIdPrefix, RespondentAnswerOptions, txtOther, Question.RANDOMIZE_ANSWERS > 0);

           //ServiceFactory<RespondentMultipleQuestions>.Instance.CreateMossQuestiontable(rbAnswersList, SurveyQuestionAndAnswerOptions, ControlIdPrefix, RespondentAnswerOptions, txtOther,tblMOSS, Question.RANDOMIZE_ANSWERS > 0);
           // lblOther.Visible = SurveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0;
           // radioButtonList.Items.Add(new ListItem("Other - <input id=\"txtOther\" name=\"txtOther\" type=\"text\" value=\"test\" />", "-1"));
            
            if (SurveyQuestionAndAnswerOptions.Question.OTHER_ANS > 0)
            {
                if (Question.SURVEY_ID == 9454)
                {
                    rbAnswersList.Items.Add(new ListItem("Otro"));
                }
                else
                {
                    string othertxt= othertext.Selectsurveyques_other(Convert.ToInt32(SurveyQuestionAndAnswerOptions.Question.QUESTION_ID));

                    ListItem lstAnswerOption = new ListItem
                    {
                        Text = othertxt,
                        Value = SurveyRespondentHelper.FormatControlId(ControlIdPrefix,
                                                                       SurveyQuestionAndAnswerOptions.Question.QUESTION_TYPE_ID,
                                                                       SurveyQuestionAndAnswerOptions.Question.QUESTION_ID,
                                                                       0)
                    };
                    
              // rbAnswersList.Items.Add(new ListItem(othertxt));
                    rbAnswersList.Items.Add(lstAnswerOption);
                }
                txtOther.Visible = true;
              //  rbAnswersList.Items.Add(new ListItem("Other - <input id=\"txtOther\" name=\"txtOther\" type=\"text\" value=\"Enter the comments\" />", "-1"));
            }

            txtOther.Attributes.Add("data", SurveyRespondentHelper.FormatControlId("txt", Question.QUESTION_TYPE_ID, Question.QUESTION_ID, 0));
        }

        #endregion

        #region Auxillary methods

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="deleted">The deleted.</param>
        /// <returns></returns>
        private static osm_responsequestions GetInstance(int deleted)
        {
            return new osm_responsequestions { DELETED = deleted, RESPONSE_RECEIVED_ON = DateTime.UtcNow };
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="isquestion">if set to <c>true</c> [isquestion].</param>
        /// <returns></returns>
        private static int GetValue(string source, bool isquestion)
        {
            if (string.IsNullOrEmpty(source))
                return 0;

            var ids = source.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
            return Convert.ToInt32(isquestion ? ids[1] : ids[2]);
        }

        #endregion

        protected void cvMoss_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (IsRequired)
                args.IsValid = rbAnswersList.SelectedItem != null || (txtOther.Visible
                                                                                               ? txtOther.Text.Length > 0
                                                                                               : false);
        }

        
        protected void rbAnswersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //for (int rbi = 0; rbi < rbAnswersList.Items.Count; rbi++)
            //{
            //    if ((rbAnswersList.Items[rbi].Text == "Other") && (rbAnswersList.Items[rbi].Selected == true))
            //    {
            //        txtOther.Visible = true;
            //      //  rbAnswersList.SelectedItem.Selected = true; 
            //    }
            //    else
            //    {
            //        txtOther.Visible = false;
            //      //  rbAnswersList.SelectedItem.Selected = true; 
            //    }
            //}
        }
}
}