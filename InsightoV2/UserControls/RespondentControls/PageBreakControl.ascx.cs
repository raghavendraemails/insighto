﻿using System;
using System.Collections.Generic;
using App_Code;
using Insighto.Data;

namespace UserControls.RespondentControls
{
    public partial class PageBreakControl : SurveyRespondentControlBase
    {
        public override void Initialise()
        {
            lblPageBreak.Visible = false;
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                return new List<osm_responsequestions>();
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
            base.OnLoad(sender, e);
        }

        public override void Display()
        {
            if (Question == null)
                return;

            lblPageBreak.Visible = Question.PAGE_BREAK > 0;
        }
    }
}