﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MatrixSingleSelectControl.ascx.cs"
    Inherits="UserControls.RespondentControls.MatrixSingleSelectControl" %>
<%@ Register Src="DisplayQuestionControl.ascx" TagName="DisplayQuestionControl" TagPrefix="Insighto" %>
<Insighto:DisplayQuestionControl ID="ucDisplayQuestionControl" runat="server" />
<div class="surveyResponseQContent">
    <div class="ResondLbl">
        <div>
            <asp:CustomValidator SetFocusOnError="true" ID="cvMatrix" runat="server" CssClass="errorMesg"
                ErrorMessage="This is mandatory." OnServerValidate="cvMatrix_ServerValidate" />
            <div class="matrixSingleSelect">
                <table id="tblMatrix2D" runat="server" visible="false" class="tblMatrix2D">
                </table>
            </div>
        </div>
    </div>
</div>
