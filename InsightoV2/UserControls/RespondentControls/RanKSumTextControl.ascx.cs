﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using Resources;

namespace UserControls.RespondentControls
{
    public partial class RanKSumTextControl : SurveyRespondentControlBase
    {
        #region Private properties

        private string _textControlPrefix = "rs";
        private List<osm_responsequestions> _lstResponseOtions;
        private List<TextBox> _lstControls;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions
        {
            get
            {
                return base.SurveyQuestionAndAnswerOptions;
            }
            set
            {
                base.SurveyQuestionAndAnswerOptions = value;
                ucDisplayQuestionControl.SurveyQuestionAndAnswerOptions = value;
            }
        }

        /// <summary>
        /// Gets or sets the control id prefix.
        /// </summary>
        /// <value>
        /// The control id prefix.
        /// </value>
        public override string ControlIdPrefix
        {
            get
            {
                return _textControlPrefix;
            }
            set
            {
                _textControlPrefix = value;
            }
        }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                _lstControls = new List<TextBox>();
                _lstResponseOtions = new List<osm_responsequestions>();
                foreach (var txtId in SurveyQuestionAndAnswerOptions.AnswerOption)
                {
                    var txtRankSum = Utilities.FindControl<TextBox>(Page, GetFormattedId("txt", txtId.ANSWER_ID));
                    if (Utilities.IsEmptyRecord(null, txtRankSum))
                        continue;

                    _lstControls.Add(txtRankSum);
                }

                foreach (var item in _lstControls)
                {
                    var response = GetInstance(0);
                    response.RESPONDENT_ID = RespondentPageBase.RespondentId;
                    response.QUESTION_ID = ValidationHelper.GetInteger(Question.QUESTION_ID, 0);
                    response.ANSWER_ID = GetValue(item.ID, false);
                    response.ANSWER_TEXT = item.Text.Trim();

                    _lstResponseOtions.Add(response);
                }
                return _lstResponseOtions;
            }
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            //set custom validator
            var trOption = new HtmlTableRow();
            var tdOption = new HtmlTableCell { ColSpan = 2 };

            if (IsRequired)
                tdOption.Controls.Add(GetCustomRequiredValidator(0));

            tdOption.Controls.Add(GetCustomValidator(0, QuestionType == QuestionType.Ranking));
            trOption.Controls.Add(tdOption);
            tblRankSum.Controls.Add(trOption);

            switch (QuestionType)
            {
                case QuestionType.Ranking:
                    ServiceFactory<RespondentNumericQuestions>.Instance.CreateRankingQuestion(tblRankSum, SurveyQuestionAndAnswerOptions, true);
                    break;

                case QuestionType.ConstantSum:
                    ServiceFactory<RespondentNumericQuestions>.Instance.CreateConstantSumQuestion(tblRankSum, SurveyQuestionAndAnswerOptions, true);
                    break;
            }

            Initialise();
            Display();
            base.OnInit(e);
        }

        private CustomValidator GetCustomRequiredValidator(int answerId)
        {
            var revTemp = new CustomValidator
            {
                CssClass = RespondentResources.ErrorMessageCssClass,
                ID = GetFormattedId("cvRaS", answerId),
                ErrorMessage = CommonMessages.Respondent_Mandatory,
                Display = ValidatorDisplay.Dynamic,
                Visible = false
            };
            revTemp.ServerValidate += cvRankSumRequired_ServerValidate;
            return revTemp;
        }

        private void cvRankSumRequired_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (IsRequired)
            {
                if (
                    SurveyQuestionAndAnswerOptions.AnswerOption.Select(
                        txtId => Utilities.FindControl<TextBox>(Page, GetFormattedId("txt", txtId.ANSWER_ID))).Any(
                            textControl => textControl != null && !string.IsNullOrEmpty(textControl.Text)))
                {
                    args.IsValid = true;
                    return;
                }
                args.IsValid = false;
                return;
            }
            args.IsValid = true;
        }

        /// <summary>
        /// Gets the custom validator.
        /// </summary>
        /// <param name="answerId">The answer id.</param>
        /// <param name="isRanking">if set to <c>true</c> [is ranking].</param>
        /// <returns></returns>
        private CustomValidator GetCustomValidator(int answerId = 0, bool isRanking = false)
        {
            var revTemp = new CustomValidator();
            if (isRanking)
            {
                revTemp.ID = GetFormattedId("revRng", answerId);
                revTemp.ErrorMessage = string.Format("{0}{1} {2}", RespondentResources.RankingRangeText, SurveyQuestionAndAnswerOptions.AnswerOption.Count, RespondentResources.RankingRepeatedText);
                revTemp.ServerValidate += Ranking_ServerValidate;
            }
            else
            {
                revTemp.ID = GetFormattedId("revSum", answerId);
                revTemp.ErrorMessage = RespondentResources.ConstantSum + Question.CHAR_LIMIT;
                revTemp.ServerValidate += ConstantSum_ServerValidate;
                revTemp.SetFocusOnError = true;
            }
            revTemp.CssClass = RespondentResources.ErrorMessageCssClass;
            revTemp.Display = ValidatorDisplay.Dynamic;
            revTemp.Visible = false;
            return revTemp;
        }

        protected void ConstantSum_ServerValidate(object source, ServerValidateEventArgs args)
        {
            _lstControls = new List<TextBox>();
            var result = 0;
            foreach (var textControl in SurveyQuestionAndAnswerOptions.AnswerOption.Select(
                        txtId => Utilities.FindControl<TextBox>(Page, GetFormattedId("txt", txtId.ANSWER_ID))))
            {
                if (textControl.Text.Trim().Length > 0)
                {
                    if (Utilities.IsNegative(Convert.ToInt32(textControl.Text)))
                    {
                        args.IsValid = false;
                        return;
                    }
                    _lstControls.Add(textControl);
                }
                result += Utilities.ToInt(textControl.Text.Trim());
            }
            args.IsValid = _lstControls.Count > 0 ? result == Question.CHAR_LIMIT : true;
            return;
        }

        protected void Ranking_ServerValidate(object source, ServerValidateEventArgs args)
        {
            const int minValue = 1;
            var maxValue = SurveyQuestionAndAnswerOptions.AnswerOption.Count;
            _lstControls = new List<TextBox>();
            foreach (var textControl in SurveyQuestionAndAnswerOptions.AnswerOption.Select(
                        txtId => Utilities.FindControl<TextBox>(Page, GetFormattedId("txt", txtId.ANSWER_ID))))
            {
                if (textControl.Text.Trim().Length > 0)
                    _lstControls.Add(textControl);

                if (_lstControls.Count <= 0) continue;
                if (Utilities.ToInt(textControl.Text.Trim()) == 0)
                {
                    args.IsValid = false;
                    return;
                }
                if (Utilities.ToInt(textControl.Text.Trim()) >= minValue && Utilities.ToInt(textControl.Text.Trim()) <= maxValue)
                    continue;
                args.IsValid = false;
                return;
            }

            for (var i = 1; i <= _lstControls.Count - 1; i++)
            {
                var sourceText = _lstControls[i].Text.Trim();
                var targetText = _lstControls[i - 1].Text.Trim();

                if (sourceText != targetText) continue;
                args.IsValid = false;
                return;
            }
        }


        private string GetFormattedId(string prefix, int answerId)
        {
            return string.Format("{0}{1}_{2}_{3}", prefix, Question.QUESTION_TYPE_ID, Question.QUESTION_ID, answerId);
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        public override void Initialise()
        {
            if (SurveyQuestionAndAnswerOptions != null && SurveyQuestionAndAnswerOptions.AnswerOption != null)
                lblRankSum.Text = string.Format("{0} {1}", RespondentResources.RankingRangeText, SurveyQuestionAndAnswerOptions.AnswerOption.Count);
        }

        /// <summary>
        /// Displays this instance.
        /// </summary>
        public override void Display()
        {
            var rfvTemp = Utilities.FindControl<CustomValidator>(this, GetFormattedId("cvRaS", 0));
            if (rfvTemp != null) rfvTemp.Visible = IsRequired;

            var revSum = Utilities.FindControl<CustomValidator>(this, GetFormattedId("revSum", 0));
            if (revSum != null) revSum.Visible = QuestionType == QuestionType.ConstantSum;

            var revRng = Utilities.FindControl<CustomValidator>(this, GetFormattedId("revRng", 0));
            if (revRng != null) revRng.Visible = QuestionType == QuestionType.Ranking;

            if (SurveyQuestionAndAnswerOptions == null || SurveyQuestionAndAnswerOptions.Question == null)
                return;

            foreach (var item in SurveyQuestionAndAnswerOptions.AnswerOption)
            {

                using (var txtContent = Utilities.FindControl<TextBox>(this, GetFormattedId("txt", item.ANSWER_ID)))
                {
                    if (QuestionType == QuestionType.Ranking)
                        divSum.Style.Add("display", "block");

                    if (QuestionType == QuestionType.ConstantSum)
                        divSum.Style.Add("display", "none");

                    txtContent.MaxLength = 10;

                    //if user is halfway through
                    var existedResponse = Utilities.GetResponseQuestion(RespondentAnswerOptions, item.ANSWER_ID);
                    txtContent.Text = existedResponse != null ? existedResponse.ANSWER_TEXT : string.Empty;
                }
            }
        }

        #endregion.

        #region Auxillary methods

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="deleted">The deleted.</param>
        /// <returns></returns>
        private static osm_responsequestions GetInstance(int deleted)
        {
            return new osm_responsequestions { DELETED = deleted, RESPONSE_RECEIVED_ON = DateTime.UtcNow };
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="isquestion">if set to <c>true</c> [isquestion].</param>
        /// <returns></returns>
        private static int GetValue(string source, bool isquestion)
        {
            if (!string.IsNullOrEmpty(source))
            {
                var ids = source.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                return Convert.ToInt32(isquestion ? ids[1] : ids[2]);
            }

            return 0;
        }

        #endregion
    }
}