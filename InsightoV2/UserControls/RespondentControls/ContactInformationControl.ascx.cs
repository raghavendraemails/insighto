﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using Resources;
using Insighto.Business.Helpers;

namespace UserControls.RespondentControls
{
    public partial class ContactInformationControl : SurveyRespondentControlBase
    {
        #region Private properties

        private string _textControlPrefix = "ci";
        private List<osm_responsequestions> _lstResponseOtions;
        private List<TextBox> _lstControls;
        private int _optionRequiredEnabledCount;
        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions
        {
            get
            {
                return base.SurveyQuestionAndAnswerOptions;
            }
            set
            {
                base.SurveyQuestionAndAnswerOptions = value;
                ucDisplayQuestionControl.SurveyQuestionAndAnswerOptions = value;
            }
        }

        /// <summary>
        /// Gets or sets the control id prefix.
        /// </summary>
        /// <value>
        /// The control id prefix.
        /// </value>
        public override string ControlIdPrefix
        {
            get
            {
                return _textControlPrefix;
            }
            set
            {
                _textControlPrefix = value;
            }
        }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                _lstControls = new List<TextBox>();
                _lstResponseOtions = new List<osm_responsequestions>();
                foreach (var txtId in SurveyQuestionAndAnswerOptions.AnswerOption)
                {
                    var txtOther = Utilities.FindControl<TextBox>(Page, GetFormattedId("txt", txtId.ANSWER_ID));
                    if (!Utilities.IsEmptyRecord(null, txtOther))
                        _lstControls.Add(txtOther);
                    continue;


                }

                foreach (var item in _lstControls.Where(item => item != null))
                {
                    var response = GetInstance(0);
                    response.RESPONDENT_ID = RespondentPageBase.RespondentId;
                    response.QUESTION_ID = ValidationHelper.GetInteger(Question.QUESTION_ID, 0);
                    response.ANSWER_ID = GetValue(item.ID, false);
                    response.ANSWER_TEXT = item.Text.Trim();

                    _lstResponseOtions.Add(response);
                }
                return _lstResponseOtions;
            }
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
            base.OnLoad(sender, e);
        }


        protected override void OnInit(EventArgs e)
        {
            foreach (var item in SurveyQuestionAndAnswerOptions.AnswerOption)
            {
                if (item.RESPONSE_REQUIRED > 0)
                    _optionRequiredEnabledCount++;
            }
            ServiceFactory<RespondentMultipleQuestions>.Instance.CreateContactInfoQuestion(tblContactInfo, SurveyQuestionAndAnswerOptions, true);

            if (_optionRequiredEnabledCount == 0)
                divValidators.Controls.Add(GetCustomValidator());
            base.OnInit(e);
        }

        private CustomValidator GetCustomValidator()
        {
            var revTemp = new CustomValidator
            {
                CssClass = RespondentResources.ErrorMessageCssClass,
                ID = string.Format("cvCon{0}_{1}", Question.QUESTION_TYPE_ID, Question.QUESTION_ID),
                ErrorMessage = CommonMessages.Respondent_Mandatory,
                Display = ValidatorDisplay.Dynamic,
                Visible = true
            };
            revTemp.ServerValidate += cvContact_ServerValidate;
            return revTemp;
        }

        private void cvContact_ServerValidate(object source, ServerValidateEventArgs args)
        {
            _lstControls = new List<TextBox>();
            foreach (var textControl in
                SurveyQuestionAndAnswerOptions.AnswerOption.Select(item => Utilities.FindControl<TextBox>(Page, GetFormattedId("txt", item.ANSWER_ID))).Where(textControl => textControl.Text.Length > 0))
            {
                _lstControls.Add(textControl);
            }
            args.IsValid = _lstControls.Count > 0;
            return;
        }



        /// <summary>
        /// Gets the formatted id.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <param name="answerId"></param>
        /// <returns></returns>
        private string GetFormattedId(string prefix, int answerId)
        {
            return string.Format("{0}{1}_{2}_{3}", prefix, Question.QUESTION_TYPE_ID, Question.QUESTION_ID, answerId);
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        public override void Initialise()
        {

        }

        /// <summary>
        /// Displays this instance.
        /// </summary>
        public override void Display()
        {
            if (SurveyQuestionAndAnswerOptions == null || SurveyQuestionAndAnswerOptions.Question == null)
                return;

            var cvTemp = Utilities.FindControl<CustomValidator>(this, string.Format("cvCon{0}_{1}", Question.QUESTION_TYPE_ID, Question.QUESTION_ID));
            if (cvTemp != null) cvTemp.Visible = IsRequired;

            foreach (var item in SurveyQuestionAndAnswerOptions.AnswerOption)
            {
                var rfvTemp = Utilities.FindControl<RequiredFieldValidator>(this, GetFormattedId("rfv", item.ANSWER_ID));
                using (var txtContent = Utilities.FindControl<TextBox>(this, GetFormattedId("txt", item.ANSWER_ID)))
                {
                    if (rfvTemp != null)
                    {
                        rfvTemp.ControlToValidate = txtContent.ID;
                        rfvTemp.Visible = IsRequired;
                    }

                    switch (item.DEMOGRAPIC_BLOCKID)
                    {
                        case 10:
                            var revEml = Utilities.FindControl<RegularExpressionValidator>(this, GetFormattedId("revEml", item.ANSWER_ID));
                            revEml.ControlToValidate = txtContent.ID;
                            revEml.Visible = true;
                            break;
                    }
                    txtContent.MaxLength = Question.CHAR_LIMIT;

                    //if user is halfway through
                    var response = Utilities.GetResponseQuestion(RespondentAnswerOptions, item.ANSWER_ID);
                    txtContent.Text = response != null ? response.ANSWER_TEXT : string.Empty;
                }
            }

        }

        #endregion.

        #region Auxillary methods

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="deleted">The deleted.</param>
        /// <returns></returns>
        private static osm_responsequestions GetInstance(int deleted)
        {
            return new osm_responsequestions { DELETED = deleted, RESPONSE_RECEIVED_ON = DateTime.UtcNow };
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="isquestion">if set to <c>true</c> [isquestion].</param>
        /// <returns></returns>
        private static int GetValue(string source, bool isquestion)
        {
            if (string.IsNullOrEmpty(source))
                return 0;

            var ids = source.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
            return Convert.ToInt32(isquestion ? ids[1] : ids[2]);
        }

        #endregion
    }
}