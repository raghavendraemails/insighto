﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MOSSControl.ascx.cs" Inherits="UserControls.RespondentControls.MOSSControl" %>
<%@ Register Src="DisplayQuestionControl.ascx" TagName="DisplayQuestionControl" TagPrefix="Insighto" %>
<div>
    <Insighto:DisplayQuestionControl ID="ucDisplayQuestionControl" runat="server" />
</div>
<div class="ResondLbl">
    <div id="divValidators" runat="server" style="width: 100%"  />
    <div class="singleSelectRadio" >
     <table id="tblMOSS" runat="server" visible="false" class="tblMatrix2D">
      </table>        
    <asp:RadioButtonList ID="rbAnswersList" runat="server" 
            AutoPostBack="true"  
            onselectedindexchanged="rbAnswersList_SelectedIndexChanged" >
           
    </asp:RadioButtonList>
      
    </div>
    <asp:Label Text="Other" ID="lblOther" Visible="false" runat="server" />
    &nbsp;<asp:TextBox runat="server" ID="txtOther" Visible="false" CssClass="textBoxMedium"  />
    <asp:HiddenField ID="hdnOther" runat="server" />
   
</div>
