﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using Resources;
using System.Web.UI.HtmlControls;

namespace UserControls.RespondentControls
{
    public partial class TextControl : SurveyRespondentControlBase
    {
        #region Private properties

        private string _textControlPrefix = string.Empty;
        private List<TextBox> _lstControls;
        private List<osm_responsequestions> _lstResponseOtions;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions { get; set; }

        /// <summary>
        /// Gets or sets the control id prefix.
        /// </summary>
        /// <value>
        /// The control id prefix.
        /// </value>
        public override string ControlIdPrefix
        {
            get
            {
                return _textControlPrefix;
            }
            set
            {
                _textControlPrefix = value;
            }
        }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                _lstControls = new List<TextBox>();
                _lstResponseOtions = new List<osm_responsequestions>();
                foreach (var txtId in SurveyQuestionAndAnswerOptions.AnswerOption)
                {
                    var txtOther = Utilities.FindControl<TextBox>(Page, GetFormattedId("txt", txtId.ANSWER_ID));
                    if (!Utilities.IsEmptyRecord(null, txtOther))
                        _lstControls.Add(txtOther);
                    continue;
                }

                foreach (var item in _lstControls.Where(item => item != null))
                {
                    var response = GetInstance(0);
                    response.RESPONDENT_ID = RespondentPageBase.RespondentId;
                    response.QUESTION_ID = ValidationHelper.GetInteger(Question.QUESTION_ID, 0);
                    response.ANSWER_ID = GetValue(item.ID, false);
                    response.ANSWER_TEXT = item.Text;

                    _lstResponseOtions.Add(response);
                }
                return _lstResponseOtions;
            }
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            //set custom validator
            var trOption = new HtmlTableRow();
            var tdOption = new HtmlTableCell { ColSpan = 2 };
            tdOption.Controls.Add(GetCustomValidator(0));
            trOption.Controls.Add(tdOption);
            tblRankSum.Controls.Add(trOption);

            ServiceFactory<RespondentTextQuestions>.Instance.CreateTextQuestion(tblRankSum,
                                                                               SurveyQuestionAndAnswerOptions, EnableValidation);

            Initialise();
            Display();
            base.OnInit(e);
        }



        private CustomValidator GetCustomValidator(int answerId)
        {
            var revTemp = new CustomValidator
            {
                CssClass = RespondentResources.ErrorMessageCssClass,
                ID = GetFormattedId("cvMul", answerId),
                ErrorMessage = CommonMessages.Respondent_Mandatory,
                Display = ValidatorDisplay.Dynamic,
                Visible = false
            };
            if (IsRequired && QuestionType == QuestionType.Text_MultipleBoxes)
            {
                revTemp.ServerValidate += cvMulltitextBoxes_ServerValidate;
            }
            else
            {
                revTemp.ServerValidate += cvSingleTextBox_ServerValidate;
            }
            return revTemp;
        }

        /// <summary>
        /// Handles the ServerValidate event of the cvMulltitextBoxes control.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="args">The <see cref="System.Web.UI.WebControls.ServerValidateEventArgs"/> instance containing the event data.</param>
        private void cvMulltitextBoxes_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (IsRequired && QuestionType == QuestionType.Text_MultipleBoxes)
            {
                if (SurveyQuestionAndAnswerOptions.AnswerOption.Select(
                        txtId => Utilities.FindControl<TextBox>(Page, GetFormattedId("txt", txtId.ANSWER_ID))).Any(
                            textControl => textControl != null && !string.IsNullOrEmpty(textControl.Text.Trim())))
                {
                    args.IsValid = true;
                    return;
                }
                args.IsValid = false;
                return;
            }
            args.IsValid = true;
        }

        /// <summary>
        /// Handles the ServerValidate event of the cvSingleTextBox control.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="args">The <see cref="System.Web.UI.WebControls.ServerValidateEventArgs"/> instance containing the event data.</param>
        private void cvSingleTextBox_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (IsRequired && (QuestionType == QuestionType.Text_SingleRowBox || QuestionType == QuestionType.Text_CommentBox || QuestionType == QuestionType.EmailAddress || QuestionType == QuestionType.Numeric_SingleRowBox))
            {
                if (SurveyQuestionAndAnswerOptions.AnswerOption.Select(
                        txtId => Utilities.FindControl<TextBox>(Page, GetFormattedId("txt", txtId.ANSWER_ID))).Any(
                            textControl => textControl != null && !string.IsNullOrEmpty(textControl.Text.Trim())))
                {
                    args.IsValid = true;
                    return;
                }
                args.IsValid = false;
                Utilities.FindControl<TextBox>(Page, GetFormattedId("txt", 0)).Focus();
                return;
            }
            args.IsValid = true;
        }
        /// <summary>
        /// Gets the formatted id.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <param name="answerId">The answer id.</param>
        /// <returns></returns>
        private string GetFormattedId(string prefix, int answerId)
        {
            return string.Format("{0}{1}_{2}_{3}", prefix, Question.QUESTION_TYPE_ID, Question.QUESTION_ID, answerId);
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        public override void Initialise()
        {

        }

        /// <summary>
        /// Displays this instance.
        /// </summary>
        public override void Display()
        {
            if (SurveyQuestionAndAnswerOptions == null || SurveyQuestionAndAnswerOptions.Question == null)
                return;

            var cvMul = Utilities.FindControl<CustomValidator>(this, GetFormattedId("cvMul", 0));
            if (cvMul != null) cvMul.Visible = true; cvMul.SetFocusOnError = true;

            foreach (var item in SurveyQuestionAndAnswerOptions.AnswerOption)
            {
                var lblOption = Utilities.FindControl<Label>(this, GetFormattedId("lbl", item.ANSWER_ID));
                using (var txtContent = Utilities.FindControl<TextBox>(this, GetFormattedId("txt", item.ANSWER_ID)))
                {
                    var rfvTemp = Utilities.FindControl<RequiredFieldValidator>(this, GetFormattedId("rfv", item.ANSWER_ID));
                    if (rfvTemp != null)
                    {
                        rfvTemp.ControlToValidate = txtContent.ID;
                        rfvTemp.Visible = IsRequired;
                    }

                    txtContent.TextMode = QuestionType == QuestionType.Text_CommentBox ? TextBoxMode.MultiLine : TextBoxMode.SingleLine;
                    switch (QuestionType)
                    {
                        case QuestionType.EmailAddress:
                            var revEmail = Utilities.FindControl<RegularExpressionValidator>(this, GetFormattedId("revEml", item.ANSWER_ID));
                            if (revEmail != null)
                            {
                                revEmail.ControlToValidate = txtContent.ID;
                                revEmail.Visible = true;
                            }
                            break;

                        case QuestionType.Numeric_SingleRowBox:
                            var revNumeric = Utilities.FindControl<RegularExpressionValidator>(this, GetFormattedId("revNum", item.ANSWER_ID));
                            if (revNumeric != null)
                            {
                                revNumeric.ControlToValidate = txtContent.ID;
                                revNumeric.Visible = true;
                            }
                            break;

                        case QuestionType.Text_SingleRowBox:
                        case QuestionType.Text_CommentBox:
                            var revCharLimit = Utilities.FindControl<RegularExpressionValidator>(this, GetFormattedId("revLmt", item.ANSWER_ID));
                            if (revCharLimit != null)
                            {
                                revCharLimit.ControlToValidate = txtContent.ID;
                                revCharLimit.Visible = true;
                            }
                            break;
                    }

                    lblOption.Visible = QuestionType == QuestionType.Text_MultipleBoxes;
                    txtContent.MaxLength = Question.CHAR_LIMIT;

                    //if user is halfway through
                    var response = Utilities.GetResponseQuestion(RespondentAnswerOptions, item.ANSWER_ID);
                    txtContent.Text = response != null ? response.ANSWER_TEXT : string.Empty;
                }
            }
        }

        #endregion.

        #region Auxillary methods

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="isquestion">if set to <c>true</c> [isquestion].</param>
        /// <returns></returns>
        private static int GetValue(string source, bool isquestion)
        {
            if (!string.IsNullOrEmpty(source))
            {
                var ids = source.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                return Convert.ToInt32(isquestion ? ids[1] : ids[2]);
            }

            return 0;
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="deleted">The deleted.</param>
        /// <returns></returns>
        private static osm_responsequestions GetInstance(int deleted)
        {
            return new osm_responsequestions { DELETED = deleted, RESPONSE_RECEIVED_ON = DateTime.UtcNow };
        }

        #endregion

    }
}