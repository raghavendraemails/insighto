﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RanKSumTextControl.ascx.cs"
    Inherits="UserControls.RespondentControls.RanKSumTextControl" %>
<%@ Register Src="DisplayQuestionControl.ascx" TagName="DisplayQuestionControl" TagPrefix="Insighto" %>
<Insighto:DisplayQuestionControl ID="ucDisplayQuestionControl" runat="server" />
<div class="surveyResponseQContent">
    <div class="ResondLbl">
        <div style="display: none;" runat="server" id="divSum">
            <ul>
                <li>
                    <asp:Label ID="lblRankSum" runat="server" />
                </li>
            </ul>
            <ul>
                <li>Ranks should not be repeated.</li>
            </ul>
        </div>
        <table border="0" cellpadding="0" cellspacing="0" runat="server" id="tblRankSum" class="rankingLbl">
        </table>
    </div>
</div>
