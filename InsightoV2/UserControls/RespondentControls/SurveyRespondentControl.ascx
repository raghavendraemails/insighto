﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveyRespondentControl.ascx.cs"
    Inherits="UserControls.RespondentControls.SurveyRespondentControl" %>
<%@ Reference VirtualPath="~/UserControls/RespondentControls/IntroductionTextControl.ascx" %>
<%@ Register src="../ErrorSuccessNotifier.ascx" tagname="ErrorSuccessNotifier" tagprefix="Insighto" %>
<div class="surveyHeader">
    <input type="hidden" id="hdnIsPageValid" runat="server" class="isPageValidClass"
        value="Valid" />
    <div class="customHeaderText">
        <asp:Label ID="lblSurveyRespondentHeader" runat="server" Text=" "></asp:Label>
    </div>
    <div style="display: block;" id="divLogo" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" id="tblLogo" runat="server">
        </table>
    </div>
    <div class="clear">
    </div>
</div>
<p align="right" class="proButton">
    <asp:LinkButton ID="lbnSaveForLater" CssClass="surveyLinks" runat="server" Text="Save for later"
        CausesValidation="false" Visible="false" OnClick="lbnSaveForLater_Click" />
    <asp:LinkButton ID="lbnExitSurvey" CssClass="surveyLinks" runat="server" Text="Exit this survey"
        CausesValidation="false" OnClick="lbnExitSurvey_Click" />
</p>
<br /><br />
<div class="meter-wrap" id="divProgressBarPanel" runat="server" visible="false">
    <div class="meter-value" runat="server" id="divProgressBar" visible="false">
    </div>
</div>
<div class="clear">
</div>
<div class="defaultHeight">
</div>
<div class="progresstextPnl" id="divViewedtext" runat="server" style="display: none;">
    <span class="questionView">
        <asp:Literal ID="ltQuesViewed" runat="server" /></span> <span class="questionPercentage">
            <asp:Literal ID="ltQuesPercentage" runat="server" /></span>
</div>
<div class="quationtxtPanel" id="divMandatoryText" runat="server" visible="false">
    Questions with an asterisk (<span class="mandatorylbl"> * </span>) are mandatory
    <Insighto:ErrorSuccessNotifier ID="esMandatoryQuestions" runat="server" />
</div>
<div class="clear">
</div>
<div class="defaultHeight">
</div>
<div class="">
    <asp:Label ID="lblNoQuestions" runat="server" Text="No questions in this survey"
        Visible="false" />
</div>
<!-- STARTS Single column lay out container STARTS -->
<div class="divQuestions qOptionPanel" id="divQuestionsPanel" runat="server">
    <div class="text">
        <asp:Panel ID="pnlQuestions" runat="server" />
    </div>
</div>
<!-- END Single column lay out container END -->
<!-- STARTS Two column lay out container STARTS -->
<div class="divQuestions" runat="server" id="divTwoColumnPanel">
    <asp:Panel CssClass="twocolumnpanel" runat="server" ID="pnlTwoColumnQuestions">
    </asp:Panel>
    <div class="clear">
    </div>
</div>
<!-- END Two column lay out container END -->
<div class="clear">
</div>
<div class="bottomButtonPanel" id="divButtonsPanel" runat="server">
    <asp:Button ID="btnContinue" runat="server" Text="Continue" CssClass="dynamicButton"
        OnClick="btnContinue_Click" />
    <asp:ImageButton ID="imgContinueButton" runat="server" OnClick="btnContinue_Click" />
</div>
<div class="customFooterText">
    <asp:Label ID="lblSurveyRespondentFooter" runat="server" Text=" "></asp:Label>
</div>
<asp:HiddenField ID="hdnpageIndex" runat="server" />
<input type="hidden" id="hdnRespondentId" runat="server" class="hdnRespondentId" />
<asp:HiddenField ID="hdnPasswordVerified" runat="server" ClientIDMode="Static" />
<div id="modal" class="confirmationPopup" style="display: none;">
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <div class="ServerUserConfirmation">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div class="LoginNewPanel">
                                    <h2 class="popupH2">
                                        User Confirmation</h2>
                                    <!-- survey password panel -->
                                    <div runat="server" id="divLogin">
                                        <p class="defaultHeight">
                                        </p>
                                        <p class="defaultHeight">
                                        </p>
                                        <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <span class="noteText">
                                                        <asp:Literal ID="ltlConfirmmessage" runat="server" Text="Please enter your password as given in the email invitation." /></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="defaultHeight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" align="right">
                                                    Survey Name
                                                </td>
                                                <td width="70%">
                                                    <asp:Label ID="lblPopupSurveyName" Font-Bold="true" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    Survey Password
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="textBoxMedium"
                                                        IDd="txtPassword" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvpassword" runat="server"
                                                        ControlToValidate="txtPassword" ValidationGroup="LoginGroup" Display="Dynamic"
                                                        CssClass="errorMesg" ErrorMessage="Please enter the password."></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnPassword" runat="server" Text="Submit" Style="background: #C424A5;"
                                                        CssClass="dynamicButton" OnClick="btnPassword_Click" ValidationGroup="LoginGroup" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr id="trPartialMessgae" visible="false" align="center" runat="server">
                                            <td colspan="2">
                                                <p class="defaultHeight">
                                                </p>
                                                <p class="defaultHeight">
                                                </p>
                                                <p class="defaultHeight">
                                                </p>
                                                <asp:Label ID="lblPartialFillMessage" runat="server" Text="You had exited this survey halfway.<br/> Do you want to continue the survey from where you left?"
                                                    Width="100%" encodehtml="false" />
                                            </td>
                                        </tr>
                                        <tr id="trButtons" runat="server" visible="false">
                                            <td colspan="2" align="center">
                                                <p class="defaultHeight">
                                                </p>
                                                <asp:Button ID="btnYes" runat="server" Text="Yes" Style="background: #C424A5;" CssClass="dynamicButton"
                                                    OnClick="btnYes_Click" />
                                                <asp:Button ID="btnNo" runat="server" Text="No, start afresh" Style="background: #C424A5;"
                                                    CssClass="dynamicButton" OnClick="btnNo_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- //survey password panel -->
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="clear">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <script src="../../Scripts/popup.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function ShowPasswordWindow() {
            Popup.showModal('modal', null, null, { 'screenColor': '#000000', 'screenOpacity': .6 });
        }
    </script>
</div>
<script type="text/javascript" src="Scripts/mobileJs.js"></script>
