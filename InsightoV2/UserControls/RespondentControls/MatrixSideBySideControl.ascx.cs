﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Extensions;
using CovalenseUtilities.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using CovalenseUtilities.Services;

namespace UserControls.RespondentControls
{
    public partial class MatrixSideBySideControl : SurveyRespondentControlBase
    {
        #region Private properties

        private List<osm_responsequestions> _lstResponseOptions = new List<osm_responsequestions>();

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions
        {
            get
            {
                return base.SurveyQuestionAndAnswerOptions;
            }
            set
            {
                base.SurveyQuestionAndAnswerOptions = value;
                ucDisplayQuestionControl.SurveyQuestionAndAnswerOptions = value;

            }
        }

        /// <summary>
        /// Whether the field is required
        /// </summary>
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                if (!_lstResponseOptions.Any())
                    _lstResponseOptions = GetSelectedAnswerOptions();
                return _lstResponseOptions;
            }
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            if (SurveyQuestionAndAnswerOptions == null || SurveyQuestionAndAnswerOptions.Question == null || SurveyQuestionAndAnswerOptions.AnswerOption == null)
                return;

            ServiceFactory<RespondentMatrixQuestions>.Instance.CreateMatrixSideBySideQuestion(SurveyQuestionAndAnswerOptions, tblMatrixSideBySide, RespondentAnswerOptions);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
            base.OnLoad(sender, e);
        }

        protected void cvMatrix_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            args.IsValid = SelectedAnswerOptions.Any();
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        public override void Initialise()
        {
            cvMatrix.Enabled = IsRequired;
            cvMatrix.Visible = IsRequired;
        }

        /// <summary>
        /// Displays this instance.
        /// </summary>
        public override void Display()
        {

        }

        #endregion

        #region Auxillary methods

        private List<osm_responsequestions> GetSelectedAnswerOptions()
        {
            var selectedAnswerIDs = tblMatrixSideBySide.Controls.All()
                .OfType<HtmlInputRadioButton>()
                .Where(cb => cb.Checked)
                .Select(cb => cb.Value).ToList();

            if (selectedAnswerIDs == null || selectedAnswerIDs.Count <= 0)
                return _lstResponseOptions;

            return selectedAnswerIDs.Select(answerId => new osm_responsequestions
                                                            {
                                                                RESPONDENT_ID = RespondentPageBase.RespondentId,
                                                                QUESTION_ID = Question.QUESTION_ID,
                                                                ANSWER_ID = RespondentQuestionsHelper.GetValue(answerId, false),
                                                                ANSWER_TEXT = string.Empty, // txtOther.Text;
                                                                DELETED = 0,
                                                                RESPONSE_RECEIVED_ON = DateTime.UtcNow
                                                            }).ToList();
        }

        #endregion
    }

}
