﻿using System;
using System.Collections.Generic;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System.Web.UI.WebControls;

namespace UserControls.RespondentControls
{
    public partial class ImageConrol : SurveyRespondentControlBase
    {
        public override void Initialise()
        {

        }

        /// <summary>
        /// Gets or sets the survey question and answer options.
        /// </summary>
        /// <value>
        /// The survey question and answer options.
        /// </value>
        public override SurveyQuestionAndAnswerOptions SurveyQuestionAndAnswerOptions
        {
            get
            {
                return base.SurveyQuestionAndAnswerOptions;
            }
            set
            {
                base.SurveyQuestionAndAnswerOptions = value;
                ucNarrationHeaderControl.SurveyQuestionAndAnswerOptions = value;
            }
        }

        /// <summary>
        /// Gets the selected answer options.
        /// </summary>
        public override List<osm_responsequestions> SelectedAnswerOptions
        {
            get
            {
                return new List<osm_responsequestions>();
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public override void OnLoad(object sender, EventArgs e)
        {
            Initialise();
            Display();
            base.OnLoad(sender, e);
        }

        public override void Display()
        {
            if (Question == null)
                return;

            foreach (var videoItem in SurveyQuestionAndAnswerOptions.AnswerOption)
            {
                Image imgType = new Image();
                imgType.ImageUrl = Utilities.GetAbsoluteUrl(ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateImageQuestion(videoItem.ANSWER_OPTIONS), Page);
                divImages.Controls.Add(imgType);
            }
        }
    }
}