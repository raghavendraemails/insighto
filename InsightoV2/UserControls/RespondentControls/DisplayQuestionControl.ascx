﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DisplayQuestionControl.ascx.cs"
    Inherits="UserControls.RespondentControls.DisplayQuestionControl" %>
<div class="quationlbl">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td height="5">
            </td>
        </tr>
        <tr>
            <td class="surveyRespondQuestionColumn">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="star">
                            <asp:Literal runat="server" ID="ltrlMandatory" Text="*" Visible="false" />
                        </td>
                        <td class="surveyRespondQNo">
                            <asp:Literal ID="ltQuestionIndex" runat="server" />
                        </td>
                        <td class="surveyRespondQTextPanel">
                            <asp:Label runat="server" ID="lblQuestion" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
