﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RespondentPopupHeader.ascx.cs"
    Inherits="Insighto.UserControls.RespondentControls.RespondentPopupHeader" %>
<div id="divPopupHeading" runat="server" class="popupHeading">
    <!-- popup heading -->
    <div class="popupPrintPanel" runat="server" id="dvPopUpPrint" visible="false">
        <asp:HyperLink ID="hlnkPrint" runat="server" NavigateUrl="#" CssClass="printLink"
            onclick="Javascript:window.print();return false;" Text="Print" ToolTip="Print"></asp:HyperLink>
    </div>
  <%--  <div class="popupClosePanel">
        <a href="#" class="popupCloseLink" onclick="closeModalSelf(false,'');" title = "Close">&nbsp;</a>
    </div>--%>
</div>
<div class="clear"></div>

