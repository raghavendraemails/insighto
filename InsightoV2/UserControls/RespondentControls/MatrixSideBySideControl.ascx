﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MatrixSideBySideControl.ascx.cs"
    Inherits="UserControls.RespondentControls.MatrixSideBySideControl" %>
<%@ Register Src="DisplayQuestionControl.ascx" TagName="DisplayQuestionControl" TagPrefix="Insighto" %>
<Insighto:DisplayQuestionControl ID="ucDisplayQuestionControl" runat="server" />
<div class="surveyResponseQContent">
    <div class="surveyoverflowy">
        <div class="ResondLbl">
            <asp:CustomValidator SetFocusOnError="true" ID="cvMatrix" runat="server" CssClass="errorMesg"
                ErrorMessage="This is mandatory." OnServerValidate="cvMatrix_ServerValidate" /><br />
            <table id="tblMatrixSideBySide" runat="server" visible="false" class="tblMatrixSideBySide">
            </table>
        </div>
    </div>
</div>
