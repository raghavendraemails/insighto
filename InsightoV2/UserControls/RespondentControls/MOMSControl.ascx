﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MOMSControl.ascx.cs"
    Inherits="UserControls.RespondentControls.MOMSControl" %>
<%@ Register Src="DisplayQuestionControl.ascx" TagName="DisplayQuestionControl" TagPrefix="Insighto" %>
<Insighto:DisplayQuestionControl ID="ucDisplayQuestionControl" runat="server" />
<div class="ResondLbl checkList">
    <asp:CustomValidator SetFocusOnError="true" ID="cvMoms" runat="server" CssClass="errorMesg"
        ErrorMessage="This is mandatory." OnServerValidate="cvMoms_ServerValidate" />
    <asp:CheckBoxList ID="cbAnswersList" runat="server" CssClass="testCheckClass" />
    <asp:Label Text="Other" ID="lblOther" Visible="false" runat="server" />
    <asp:TextBox runat="server" ID="txtOther" Visible="false" CssClass="textBoxMedium" />
    <asp:HiddenField ID="hdnOther" runat="server" />
</div>
