﻿using System;
using App_Code;
using Insighto.Business.Enumerations;
using Insighto.Business.ValueObjects;

namespace UserControls.RespondentControls
{
    public partial class DisplayQuestionControl : SurveyRespondentControlBase
    {
        public override bool IsRequired
        {
            get
            {
                return Question.RESPONSE_REQUIRED > 0;
            }
        }

        public override void Initialise()
        {

        }
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
            base.OnLoad(sender, e);
        }

        public override void Display()
        {
            if (SurveyQuestionAndAnswerOptions == null || Question == null || SurveyQuestionAndAnswerOptions.AnswerOption == null)
                return;

            ltQuestionIndex.Visible = !(QuestionType == QuestionType.Narration || QuestionType == QuestionType.PageHeader || QuestionType == QuestionType.QuestionHeader);
            ltQuestionIndex.Text = SurveyQuestionAndAnswerOptions.RankIndex.ToString();
            lblQuestion.Text = Question.QUSETION_LABEL;
            ltrlMandatory.Visible = IsRequired;
        }
    }
}