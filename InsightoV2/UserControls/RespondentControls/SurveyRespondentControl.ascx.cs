﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using Resources;
using System.Data;
using System.Data.SqlClient;

namespace UserControls.RespondentControls
{
    public partial class SurveyRespondentControl : UserControl
    {
        #region Public properties

        /// <summary>
        /// Gets the base page.
        /// </summary>
        public SurveyRespondentPageBase BasePage
        {
            get
            {
                return Page as SurveyRespondentPageBase;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public const string QtypePrefix = "qType";

        /// <summary>
        /// Gets or sets a value indicating whether this instance is quota reached.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is quota reached; otherwise, <c>false</c>.
        /// </value>
        public bool IsQuotaReached { get; set; }

        /// <summary>
        /// Gets or sets the email id.
        /// </summary>
        /// <value>
        /// The email id.
        /// </value>
        public int EmailId { get; set; }

        /// <summary>
        /// Sets the deleted.
        /// </summary>
        /// <value>
        /// The deleted.
        /// </value>
        public int Deleted { get; set; }

        /// <summary>
        /// Gets or sets the type of the skip logic.
        /// </summary>
        /// <value>
        /// The type of the skip logic.
        /// </value>
        public SkipType SkipLogicType { get; set; }

        /// <summary>
        /// Gets or sets the survey id.
        /// </summary>
        /// <value>
        /// The survey id.
        /// </value>
        public int SurveyId { get; set; }

        /// <summary>
        /// Gets or sets the last question seq no.
        /// </summary>
        /// <value>
        /// The last question seq no.
        /// </value>
        public int? LastQuestionSeqNo { get; set; }

        /// <summary>
        /// Gets or sets the index of the page.
        /// </summary>
        /// <value>
        /// The index of the page.
        /// </value>
        public int PageIndex { get; set; }

        public int truecallerstat { get; set; }
       

        /// <summary>
        /// Gets the name of the cookie.
        /// </summary>
        /// <value>
        /// The name of the cookie.
        /// </value>
        public string CookieName
        {
            get
            {
                return string.Format("{0}_{1}", BasePage.SurveyId, BasePage.LaunchId);
            }
        }

        #endregion

        #region Private Properties

        private List<SurveyQuestionAndAnswerOptions> _surveyoptionsList;
        private List<osm_responsequestions> _lstFilledResponses;
        private SurveyResponseStatus _fillStatus;
        DataSet dsSqa = new DataSet();
        string panelid;
        SurveyCore scapp = new SurveyCore();
        #endregion

        #region Event handlers

        #region Respondent Handlers

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
           
            //checks whether survey has required data to process furthur or not
            //if yes, display respondent data; otherwise, redirects to last page.
            if (!BasePage.IsSurveyHasValidKey)
                Response.Redirect(GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTINVALIDKEY));

            //checks whether survey launched or not.
            if (BasePage.Mode == RespondentDisplayMode.Respondent && !BasePage.IsSurveyLaunchedByDate)
                Response.Redirect(GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTLAUNCHLATER));

            //check whether valid user or not for password user
            if (BasePage.Mode == RespondentDisplayMode.Respondent)
            {
                switch (BasePage.CurrentRespondentType)
                {
                    case RespondentType.OnlyOnce:
                        EnableOncePerUser();
                        if (!IsPostBack && IsSurveyHalfWayThrough())
                        {
                            ShowPasswordConfirmationPage(false);
                            return;
                        }
                        else
                        {
                            ViewAllQuestions(e);
                        }
                        break;

                    case RespondentType.OnlyOnceWithPassword:
                        EnableOncePerUser();
                        if (!IsPostBack && BasePage.PasswordVerified == 0)
                        {
                            ShowPasswordConfirmationPage(true);
                            return;
                        }
                        else if (BasePage.PasswordVerified == 1)
                        {
                            ViewAllQuestions(e);
                        }
                        break;

                    case RespondentType.MultipleTimes:
                        ViewAllQuestions(e);
                        break;

                    default:
                        return;
                }
            }
            else if (BasePage.Mode == RespondentDisplayMode.Preview || BasePage.Mode == RespondentDisplayMode.PreviewWithPagination || BasePage.Mode == RespondentDisplayMode.RespondentWithRedirection)
            {
                ViewAllQuestions(e);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnContinue control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                hdnIsPageValid.Value = "InValid"; // this is for custom radio and checkbox controls; use by javascript when page is invalid
                ErrorSuccessNotifier.AddErrorMessage(RespondentResources.NotifyBannerErrorMessage);
              //  ErrorSuccessNotifier.AddErrorMessage("దయచేసి శోధించడానికి Enter నొక్కండి");
                return;
            }
            hdnIsPageValid.Value = "Valid"; // this is for custom radio and checkbox controls; use by javascript when page is valid

            SaveAnswers();
            string navigationUrl;

            string strcusturlstatus = string.Empty;
            SurveyId = BasePage.SurveyId;
            dsSqa = getCustomizeURLbit(SurveyId);

            if (dsSqa.Tables[0].Rows.Count > 0)
            {
                strcusturlstatus = dsSqa.Tables[0].Rows[0][0].ToString();
            }
            if (strcusturlstatus == "True")
            {
                panelid = Request.QueryString["id"];
            }


            //GetNavigationUrl(SkipLogicType, out navigationUrl);
            GetNavigationUrl(SkipLogicType,strcusturlstatus, out navigationUrl);
            Response.Redirect(navigationUrl);
        }

        /// <summary>
        /// Handles the Click event of the lbnSaveForLater control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lbnSaveForLater_Click(object sender, EventArgs e)
        {
            SaveForLater(sender);
            string navigationUrl;
            switch (BasePage.SurveyBasicInfoView.EMAILLIST_TYPE)
            {
                case 1:
                    navigationUrl = GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTUNQUINTERNALSAVEFORLATER);
                    break;
                case 2:
                    navigationUrl = GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTUNQUEXTERNALSAVEFORLATER);
                    break;
                default:
                    navigationUrl = GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTEXITSURVEY);
                    break;
            }
            ClearCache();

            Response.Redirect(navigationUrl);
        }

        /// <summary>
        /// Handles the Click event of the lbnExitSurvey control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lbnExitSurvey_Click(object sender, EventArgs e)
        {
            ClearCache();
            var pageType = (int)EndMessages.INSIGHTO_DEFAULTEXITSURVEY;
            switch (Utilities.ToEnum<SurveyStatus>(BasePage.SurveyBasicInfoView.STATUS))
            {
                case SurveyStatus.Active:
                    if (BasePage.SurveyBasicInfoView.LAUNCH_ON_DATE > DateTime.UtcNow)
                        pageType = (int)EndMessages.INSIGHTO_DEFAULTLAUNCHLATER;
                    break;
                case SurveyStatus.Closed:
                    IsQuotaReached = true;
                    RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, SurveyResponseStatus.Overquota, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);
                    pageType = (int)EndMessages.INSIGHTO_DEFAULTCLOSEPAGE;
                    break;
            }

            if (sender == null && BasePage.SurveyBasicInfoView.SCHEDULE_ON_CLOSE == 0)
            {
                if (BasePage.ResponseCount >= BasePage.SurveyBasicInfoView.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS)
                {
                    IsQuotaReached = true;
                    RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, SurveyResponseStatus.Overquota, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);
                    pageType = (int)EndMessages.INSIGHTO_DEFAULTOVERQUOTA;
                }
            }

            SaveForLater(sender);

            //string strcusturlstatus = string.Empty;
            //SurveyId = BasePage.SurveyId;
            //dsSqa = getCustomizeURLbit(SurveyId);

            //if (dsSqa.Tables[0].Rows.Count > 0)
            //{
            //    strcusturlstatus = dsSqa.Tables[0].Rows[0][0].ToString();
            //}
            //if (strcusturlstatus == "True")
            //{
            //    panelid = Request.QueryString["id"];
            //    Response.Redirect(GetLastpageUrl(pageType) + "&id=" + panelid);
            //}
            //else
            //{
                Response.Redirect(GetLastpageUrl(pageType));
            //}
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (BasePage.SurveyBasicInfoView == null) return;

            SetHeaderDetails();
            SetLogo();
            SetStyles();
            SetButtons();

            GetSurveyQuestionsAndOptionsList();
            _surveyoptionsList = BasePage.Mode != RespondentDisplayMode.Preview ? _surveyoptionsList.Where(qa => qa.PageIndex == PageIndex).ToList() : _surveyoptionsList;
            if (!IsPostBack)
            {
                GenerateProgressBar(_surveyoptionsList);
            }

            if (BasePage.Embed == "Emb")
            {
                lbnExitSurvey.Visible = false;
            }
            else if ((BasePage.SurveyId == 9862) || (BasePage.SurveyId == 11028))
            {
                lbnExitSurvey.Visible = false;
            }
            else
            {
                DataSet dssrvysettings = scapp.getSurveySettings(BasePage.SurveyId);

                if (dssrvysettings.Tables[0].Rows.Count > 0)
                {
                    if (dssrvysettings.Tables[0].Rows[0]["CUSTOMEXIT_TEXT"].ToString() == "")
                    {
                        lbnExitSurvey.Text = "Exit this survey";
                    }
                    else
                    {
                        lbnExitSurvey.Text = dssrvysettings.Tables[0].Rows[0]["CUSTOMEXIT_TEXT"].ToString();
                    }


                }
                else
                {
                    lbnExitSurvey.Text = "Exit this survey";
                }

                lbnExitSurvey.Visible = true;
                DisplayMandatoryText(_surveyoptionsList);
            }
           // DisplayMandatoryText(_surveyoptionsList);
        }

        #endregion

        #region Password confirmation Handlers

        protected void btnPassword_Click(object sender, EventArgs e)
        {
            var launchId = BasePage.LaunchId;
            var emailId = BasePage.EmailId;
            SurveyId = BasePage.SurveyId;
            if (txtPassword.Text == Utilities.GetPassword(SurveyId.ToString(), launchId, emailId))
            {
                var navigationUrl = EncryptHelper.EncryptQuerystring("SurveyRespondent.aspx", string.Format("SurveyId={0}&LauId={1}&Pass_Verf={2}&emID={3}&Mode={4}&RespondentId={5}", SurveyId, launchId, 1, emailId, BasePage.Mode, BasePage.RespondentId));
                if (IsSurveyHalfWayThrough())
                {
                    divLogin.Visible = false;
                    trPartialMessgae.Visible = true;
                    trButtons.Visible = true;
                    ShowPasswordConfirmationPage(false);
                }
                else
                {
                    SetCookie(1, RespondentVisitorStatus.InProgress);
                    Response.Redirect(navigationUrl);
                }
            }
            else
            {
                var redirectUrl = EncryptHelper.EncryptQuerystring("SurveyLastPage.aspx", string.Format("SurveyId={0}&PageType={1}&RespondentId={2}", SurveyId, 9,BasePage.RespondentId));
                Response.Redirect(redirectUrl);
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            if (BasePage.CurrentRespondentType == RespondentType.MultipleTimes)
                return;

            SurveyId = BasePage.SurveyId;
            var launchId = BasePage.LaunchId;
            var emailId = BasePage.EmailId;
            var respondentCookie = Request.Cookies[CookieName];
            var redirectUrl = string.Empty;
            if (respondentCookie != null)
            {
                //check whether user is halfwaythrough
                var queryStringHelper =
                string.Format("SurveyId={0}&RespondentId={1}&PageIndex={2}&Pass_Verf={3}&IntroShown={4}&LauId={5}&emID={6}&Mode={7}&Layout={8}", SurveyId, respondentCookie.Values["RespondentId"], respondentCookie.Values["PageIndex"],
                    1, respondentCookie.Values["IntroShown"], launchId, emailId, BasePage.Mode, BasePage.ShowTwoColumnLayOut == true ? 2 : 1);

                redirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetRespondentPage(BasePage.Mode), queryStringHelper);
                SetCookie(1, RespondentVisitorStatus.InProgress);
            }
            Response.Redirect(redirectUrl);
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            if (BasePage.CurrentRespondentType == RespondentType.MultipleTimes)
                return;

            SurveyId = BasePage.SurveyId;
            var launchId = BasePage.LaunchId;
            var emailId = BasePage.EmailId;
            var respondentCookie = Request.Cookies[CookieName];
            var redirectUrl = string.Empty;
            if (respondentCookie != null)
            {
                var queryStringHelper = string.Format("SurveyId={0}&RespondentId={1}&Pass_Verf={2}&LauId={3}&emID={4}&Mode={5}&Layout={6}",
                    SurveyId, respondentCookie.Values["RespondentId"], 1, launchId, emailId, BasePage.Mode, BasePage.ShowTwoColumnLayOut == true ? 2 : 1);

                ServiceFactory<RespondentService>.Instance.DeleteRecords(ValidationHelper.GetInteger(respondentCookie.Values["RespondentId"], 0));
                redirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetRespondentPage(BasePage.Mode), queryStringHelper);
                SetCookie(-1, RespondentVisitorStatus.InProgress);
            }
            Response.Redirect(redirectUrl);
        }

        #endregion

        #endregion

        #region Private Methods

        /// <summary>
        /// Saves for later.
        /// </summary>
        private void SaveForLater(object sender)
        {
            if (sender != null)
            {
                //create cookie with required data.
                if (BasePage.Mode == RespondentDisplayMode.Respondent && BasePage.CurrentRespondentType == RespondentType.OnlyOnce ||
                    BasePage.CurrentRespondentType == RespondentType.OnlyOnceWithPassword)
                {
                    SaveAnswers(true);
                    SetCookie(1, RespondentVisitorStatus.SaveForLater);
                }
            }
        }

        /// <summary>
        /// View all the questions.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ViewAllQuestions(EventArgs e)
        {
            //set/get default values
            SetLocalParamaeters();
            SetSurveyIntroductionText();

            //get basic survey questions and options list.
            GetSurveyQuestionsAndOptionsList();

            //get response question for once per computer user.
            _lstFilledResponses = ValidationHelper.GetInteger(hdnRespondentId.Value, 0) > 0 ? SurveyService.GetSurveyResponseQuestionsById(ValidationHelper.GetInteger(hdnRespondentId.Value, 0)) : new List<osm_responsequestions>();

            //filter the questions page by page           
            _surveyoptionsList = BasePage.Mode != RespondentDisplayMode.Preview
                                     ? _surveyoptionsList.Where(qa => qa.PageIndex == PageIndex).ToList()
                                     : _surveyoptionsList;

            DisplayQuestionsInSingleOrTwoColumn();
            base.OnInit(e);
        }

        /// <summary>
        /// Displays the questions in single or two column.
        /// </summary>
        private void DisplayQuestionsInSingleOrTwoColumn()
        {
            int columnCount = 1;
            bool isTwoColumnLayOut = BasePage.ShowTwoColumnLayOut;
            divQuestionsPanel.Visible = !isTwoColumnLayOut;
            divTwoColumnPanel.Visible = isTwoColumnLayOut;
            Label lblpgbrk = new Label();
        
            foreach (var surveyoption in _surveyoptionsList.Where(surveyoption => surveyoption.Question != null))
            {
                LoadUserControl<SurveyRespondentControlBase>(surveyoption, PathHelper.GetRespondentControlPath(surveyoption.QuestionType), surveyoption.Question.PAGE_BREAK,ref columnCount, isTwoColumnLayOut);
                columnCount++;


                if (BasePage.Mode == RespondentDisplayMode.Preview && surveyoption.Question.PAGE_BREAK >= 1)
                {
                    LoadUserControl<SurveyRespondentControlBase>(new SurveyQuestionAndAnswerOptions { Question = new osm_surveyquestion { QUESTION_ID = new Random(DateTime.Now.Millisecond).Next(), PAGE_BREAK = 1 }, AnswerOption = new List<osm_answeroptions> { new osm_answeroptions() } },
                                                                    PathHelper.GetRespondentControlPath(
                                                                        QuestionType.PageBreak), surveyoption.Question.PAGE_BREAK,ref columnCount);

                    //lblpgbrk.Text = "===========================Page Break=====================================";
                    //pnlQuestions.Controls.Add(lblpgbrk);
                  //  LoadUserControl<SurveyRespondentControlBase>(surveyoption, PathHelper.GetRespondentControlPath(surveyoption.QuestionType), surveyoption.Question.PAGE_BREAK, ref columnCount, isTwoColumnLayOut);
                    columnCount++;
                }  
            }
        }

        /// <summary>
        /// Determines whether [is survey active].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is survey active]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsSurveyActive()
        {
            if (BasePage.SurveyBasicInfoView == null) return false;
            return BasePage.IsSurveyLaunched && BasePage.IsSurveyClosed;
        }

        /// <summary>
        /// Gets the survey questions and options list.
        /// </summary>
        private void GetSurveyQuestionsAndOptionsList()
        {
            _surveyoptionsList = BasePage.SurveyQuesAndAnswerOptionsList;
        }

        /// <summary>
        /// Loads the contr fol.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="surveyoption">The surveyoption.</param>
        /// <param name="path">The path.</param>
        private void LoadUserControl<T>(SurveyQuestionAndAnswerOptions surveyoption, string path,int pagebreak, ref  int columnCount, bool isTwoColumnLayOut = false) where T : SurveyRespondentControlBase
        {
            Label lbltext = new Label();

            if (path != "")
            {
                //Load Control
                var control = Page.LoadControl(path) as T;
                if (control != null)
                    control.SurveyQuestionAndAnswerOptions = surveyoption;

                var optionsList = _lstFilledResponses != null ? _lstFilledResponses.Where(ao => ao.QUESTION_ID == surveyoption.Question.QUESTION_ID).ToList() : null;
                if (control == null)
                { 
                    return; 
                }
                if (BasePage.SurveyId == 10923)
                {
                    foreach(var item in optionsList){
                        var anstext = item.ANSWER_TEXT;
                        anstext = anstext + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        optionsList.Remove(item);
                        item.ANSWER_TEXT = anstext;
                        optionsList.Add(item);
                    }
                }
                control.RespondentAnswerOptions = optionsList;

                if (!isTwoColumnLayOut)
                {
                    pnlQuestions.Controls.Add(control);
                   
                }
                else
                {
                    //each question will be added to this div.
                    var div2ColumnQuestion = new Panel();
                    if (columnCount == 1)
                        div2ColumnQuestion.CssClass = "firstcolumn";
                    else
                    {
                        div2ColumnQuestion.CssClass = "secondcolumn";
                        columnCount = 0;
                    }

                    if (control is IntroductionTextControl)
                    {
                        pnlTwoColumnQuestions.Controls.Add(control);
                        columnCount = 0;
                    }
                    else
                    {
                        div2ColumnQuestion.Controls.Add(control);
                        pnlTwoColumnQuestions.Controls.Add(div2ColumnQuestion);
                    }
                }
            }
            else
            {
                if (pagebreak >= 1)
                {
                    lbltext.Text = "=====================================PageBreak============================================";
                    pnlQuestions.Controls.Add(lbltext);
                }
            }
        }

        /// <summary>
        /// Sets the cookie.
        /// </summary>
        private void SetCookie(int years = 1, RespondentVisitorStatus respondentVisitorStatus = RespondentVisitorStatus.Exit)
        {
            if (BasePage.CurrentRespondentType == RespondentType.MultipleTimes || BasePage.Mode == RespondentDisplayMode.Preview || BasePage.Mode == RespondentDisplayMode.PreviewWithPagination) return;
            var respondentCookie = Request.Cookies[CookieName];
            if (respondentCookie == null)
            {
                var cookieCollection = new NameValueCollection
                                           {
                                               {"RespondentId", hdnRespondentId.Value},
                                               {"SurveyId", SurveyId.ToString()},
                                               {"PageIndex", PageIndex.ToString()},
                                               {"Status", respondentVisitorStatus.ToString()},
                                               {"Pass_Verf", BasePage.PasswordVerified.ToString()}
                                              
                                           };
                respondentCookie = new HttpCookie(CookieName);
                respondentCookie.Values.Add(cookieCollection);

                respondentCookie.Expires = DateTime.Now.AddYears(years);
                Response.Cookies.Add(respondentCookie);
            }
            else
            {
                respondentCookie.Values["RespondentId"] = hdnRespondentId.Value;
                respondentCookie.Values["SurveyId"] = SurveyId.ToString();
                respondentCookie.Values["PageIndex"] = PageIndex.ToString();
                respondentCookie.Values["Status"] = respondentVisitorStatus.ToString();
                respondentCookie.Values["Pass_Verf"] = BasePage.PasswordVerified.ToString();

                respondentCookie.Expires = DateTime.Now.AddYears(years);
                Response.Cookies.Set(respondentCookie);
            }
        }

        /// <summary>
        /// Gets the navigation URL.
        /// </summary>
        /// <param name="skipLogicType">Type of the skip logic.</param>
        /// <param name="navigationUrl"></param>
        /// <returns></returns>
        private void GetNavigationUrl(SkipType skipLogicType,string strcusturlstatus, out string navigationUrl)
        {
            navigationUrl = string.Empty;
            switch (skipLogicType)
            {
                case SkipType.End:
                    ClearCache();
                    navigationUrl = GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTTHANKYOUCONTENT);
                    break;

                case SkipType.ScreenOut:
                    ClearCache();
                    if (strcusturlstatus == "True")
                    {
                        navigationUrl = GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTSCREENOUTPAGE) + "&id=" + panelid;
                    }
                    else
                    {
                        navigationUrl = GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTSCREENOUTPAGE);
                    }
                    break;

                case SkipType.NoSkip:
                    navigationUrl = FormatRespondentUrl(navigationUrl);
                    break;

                default:

                    switch (_fillStatus)
                    {
                        case SurveyResponseStatus.Partial:
                            if (strcusturlstatus == "True")
                            {
                                navigationUrl = FormatRespondentcustomizeUrl(navigationUrl, panelid);
                            }
                            else
                            {
                            navigationUrl = FormatRespondentUrl(navigationUrl);
                            }
                            break;
                        case SurveyResponseStatus.Complete:

                            if (strcusturlstatus == "True")
                            {
                                navigationUrl = GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTTHANKYOUCONTENT) + "&id=" + panelid;
                            }
                            else
                            {
                                navigationUrl = GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTTHANKYOUCONTENT);
                            }
                      
                            ClearCache();
                            break;
                        case SurveyResponseStatus.Overquota:
                            {

                                if (strcusturlstatus == "True")
                                {
                                    
                                    var isItLastSequenceNo = IsItlastQuestion(LastQuestionSeqNo, BasePage.SurveyQuesAndAnswerOptionsList);
                                    navigationUrl = !isItLastSequenceNo
                                                        ? FormatRespondentcustomizeUrl(navigationUrl, panelid)

                                                        : GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTOVERQUOTA);
                                }
                                else
                                {
                                    var isItLastSequenceNo = IsItlastQuestion(LastQuestionSeqNo, BasePage.SurveyQuesAndAnswerOptionsList);
                                    navigationUrl = !isItLastSequenceNo
                                                        ? FormatRespondentUrl(navigationUrl)

                                                        : GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTOVERQUOTA);
                                }

                                ClearCache();
                            }
                            break;
                        default:
                            if (BasePage.SurveyBasicInfoView.SURVEY_STATUS == 0)
                            {
                                navigationUrl = GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTCLOSEPAGE);
                                ClearCache();
                            }
                            break;
                    }
                    break;
            }
        }

        /// <summary>
        /// Formats the respondent URL.
        /// </summary>
        /// <param name="navigationUrl">The navigation URL.</param>
        /// <returns></returns>
        private string FormatRespondentUrl(string navigationUrl)
        {
            navigationUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetRespondentPage(BasePage.Mode), string.Format("SurveyId={0}&PageIndex={1}&RespondentId={2}&Mode={3}&IntroShown={4}&Pass_Verf={5}&LauId={6}&emID={7}&Layout={8}", SurveyId, Utilities.ToInt(hdnpageIndex.Value), hdnRespondentId.Value, BasePage.Mode, 1, BasePage.PasswordVerified, BasePage.LaunchId, BasePage.EmailId, BasePage.ShowTwoColumnLayOut == true ? 2 : 1));
            return navigationUrl;
        }
        private string FormatRespondentcustomizeUrl(string navigationUrl,string panelid)
        {
            navigationUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetRespondentPage(BasePage.Mode), string.Format("SurveyId={0}&PageIndex={1}&RespondentId={2}&Mode={3}&IntroShown={4}&Pass_Verf={5}&LauId={6}&emID={7}&Layout={8}", SurveyId, Utilities.ToInt(hdnpageIndex.Value), hdnRespondentId.Value, BasePage.Mode, 1, BasePage.PasswordVerified, BasePage.LaunchId, BasePage.EmailId, BasePage.ShowTwoColumnLayOut == true ? 2 : 1)) + "&id=" + panelid;
            return navigationUrl;
        }

        /// <summary>
        /// Clears the cache.
        /// </summary>
        private void ClearCache()
        {
            Cache.Remove("BasicInfoView");
            Cache.Remove("SurveyQuestionsList");
        }

        /// <summary>
        /// Saves the answers.
        /// </summary>
        private void SaveAnswers(bool isSaveForLater = false)
        {
            var lstAllOptions = BasePage.SurveyQuesAndAnswerOptionsList;
            var lstResponseOtions = new List<osm_responsequestions>();

            var isSkipLogicExisted = false;
            _surveyoptionsList = _surveyoptionsList.Where(qa => qa.PageIndex == PageIndex).ToList();
            Control controlQuestions = BasePage.ShowTwoColumnLayOut == false ? pnlQuestions : pnlTwoColumnQuestions;
            var lstControls = CommonHelper.FindControlRecursive<SurveyRespondentControlBase>(controlQuestions, _surveyoptionsList.Count);

            
            foreach (var item in lstControls)
            {
                var responseList = item.SelectedAnswerOptions;


                if (responseList.Count > 0)
                {
                    lstResponseOtions.AddRange(responseList);

                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 9042)
                    {

                        if ((responseList[0].QUESTION_ID == 51368) || (responseList[0].QUESTION_ID == 51369))
                        {
                            DataSet dsanscnt = getanswertextCount(responseList[0].ANSWER_TEXT, BasePage.SurveyBasicInfoView.SURVEY_ID);
                            if (dsanscnt.Tables[0].Rows.Count > 0)
                            {
                                Response.Redirect(GetLastpageUrl(5));
                            }
                        }
                    }

                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10319)
                    {

                        if (responseList[0].QUESTION_ID == 57441)
                        {
                            isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, responseList[0].ANSWER_ID, item.SurveyQuestionAndAnswerOptions);


                            if (responseList[0].ANSWER_ID == 0)
                            {
                                
                                    int pageindxval = PageIndex + 1;
                                    hdnpageIndex.Value = pageindxval.ToString();
                                
                            }
                        }
                        
                    }

                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10362)
                    {
                        if (responseList[0].QUESTION_ID == 57911)
                        {
                            isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, responseList[0].ANSWER_ID, item.SurveyQuestionAndAnswerOptions);
                        }
                    }

                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10366)
                    {
                        if (responseList[0].QUESTION_ID == 57951)
                        {
                            isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, responseList[0].ANSWER_ID, item.SurveyQuestionAndAnswerOptions);
                        }
                    }

                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10359)
                    {
                        if (responseList[0].QUESTION_ID == 57868)
                        {
                            isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, responseList[0].ANSWER_ID, item.SurveyQuestionAndAnswerOptions);
                        }
                    }

                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10367)
                    {
                        if (responseList[0].QUESTION_ID == 57968)
                        {
                            isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, responseList[0].ANSWER_ID, item.SurveyQuestionAndAnswerOptions);
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10380)
                    {

                        if (responseList[0].QUESTION_ID == 58116)
                        {
                            isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, responseList[0].ANSWER_ID, item.SurveyQuestionAndAnswerOptions);
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10381)
                    {

                        if (responseList[0].QUESTION_ID == 58138)
                        {
                            isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, responseList[0].ANSWER_ID, item.SurveyQuestionAndAnswerOptions);
                        }
                    }

                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10395)
                    {

                        if (responseList[0].QUESTION_ID == 58224)
                        {
                            isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, responseList[0].ANSWER_ID, item.SurveyQuestionAndAnswerOptions);
                            if (responseList[0].ANSWER_ID == 0)
                            {

                                int pageindxval = PageIndex + 1;
                                hdnpageIndex.Value = pageindxval.ToString();

                            }
                        }
                    }

                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10392)
                    {

                        if (responseList[0].QUESTION_ID == 58182)
                        {
                            isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, responseList[0].ANSWER_ID, item.SurveyQuestionAndAnswerOptions);

                            if (responseList[0].ANSWER_ID == 0)
                            {

                                int pageindxval = PageIndex + 1;
                                hdnpageIndex.Value = pageindxval.ToString();

                            }
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10394)
                    {
                        if (responseList[0].QUESTION_ID == 58203)
                        {
                            isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, responseList[0].ANSWER_ID, item.SurveyQuestionAndAnswerOptions);
                            if (responseList[0].ANSWER_ID == 0)
                            {

                                int pageindxval = PageIndex + 1;
                                hdnpageIndex.Value = pageindxval.ToString();

                            }
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10391)
                    {
                        if (responseList[0].QUESTION_ID == 58161)
                        {
                            isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, responseList[0].ANSWER_ID, item.SurveyQuestionAndAnswerOptions);

                            if (responseList[0].ANSWER_ID == 0)
                            {

                                int pageindxval = PageIndex + 1;
                                hdnpageIndex.Value = pageindxval.ToString();

                            }
                        }
                    }
                }
                else
                {
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10319)
                    {
                        if (item.Question.QUESTION_ID == 57441)
                        {
                            int pageindxval = PageIndex + 1;
                            hdnpageIndex.Value = pageindxval.ToString();                           
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10362)
                    {
                        if (item.Question.QUESTION_ID == 57911)
                        {
                            int pageindxval = PageIndex + 1;
                            hdnpageIndex.Value = pageindxval.ToString();
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10366)
                    {
                        if (item.Question.QUESTION_ID == 57951)
                        {
                            int pageindxval = PageIndex + 1;
                            hdnpageIndex.Value = pageindxval.ToString();
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10359)
                    {
                        if (item.Question.QUESTION_ID == 57868)
                        {
                            int pageindxval = PageIndex + 1;
                            hdnpageIndex.Value = pageindxval.ToString();
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10367)
                    {
                        if (item.Question.QUESTION_ID == 57968)
                        {
                            int pageindxval = PageIndex + 1;
                            hdnpageIndex.Value = pageindxval.ToString();
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10380)
                    {
                        if (item.Question.QUESTION_ID == 58116)
                        {
                            int pageindxval = PageIndex + 1;
                            hdnpageIndex.Value = pageindxval.ToString();
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10381)
                    {
                        if (item.Question.QUESTION_ID == 58138)
                        {
                            int pageindxval = PageIndex + 1;
                            hdnpageIndex.Value = pageindxval.ToString();
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10395)
                    {
                        if (item.Question.QUESTION_ID == 58224)
                        {
                            int pageindxval = PageIndex + 1;
                            hdnpageIndex.Value = pageindxval.ToString();
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10392)
                    {
                        if (item.Question.QUESTION_ID == 58182)
                        {
                            int pageindxval = PageIndex + 1;
                            hdnpageIndex.Value = pageindxval.ToString();
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10394)
                    {
                        if (item.Question.QUESTION_ID == 58203)
                        {
                            int pageindxval = PageIndex + 1;
                            hdnpageIndex.Value = pageindxval.ToString();
                        }
                    }
                    if (BasePage.SurveyBasicInfoView.SURVEY_ID == 10391)
                    {
                        if (item.Question.QUESTION_ID == 58161)
                        {
                            int pageindxval = PageIndex + 1;
                            hdnpageIndex.Value = pageindxval.ToString();
                        }
                    }
                }

                //  327584

                //check if skip logic applied
                if (item.QuestionType == QuestionType.MultipleChoice_SingleSelect || item.QuestionType == QuestionType.Choice_YesNo || item.QuestionType == QuestionType.Menu_DropDown)
                    isSkipLogicExisted = IsCheckSkipLogicApplied(lstAllOptions, item.SelectedAnwerValue, item.SurveyQuestionAndAnswerOptions);

                LastQuestionSeqNo = item.LastQuestionSeqNo;
                DataSet dsscreenout = getTruecallercnt(SurveyId);

                if (item.SelectedAnwerValue == 397018)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 397021) 
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 397022) 
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 397023) 
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 396874)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 397153)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 397237)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 397240)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 397241)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 397242)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 397180)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 397253)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 397655)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 397658)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                  else if (item.SelectedAnwerValue == 397659)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 397660)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 397598)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 397599)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }                    
                else if (item.SelectedAnwerValue == 397671)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 403013)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
              else if (item.SelectedAnwerValue == 403070)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 403073)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 403074)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                  else if (item.SelectedAnwerValue == 403075)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 403086)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 404643)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 404701)
                {
                    
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 404704)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 404705)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 404706)
                {

                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 404717)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 404880)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 404938)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 404941)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 404942)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 404943)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 404954)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 404991)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 404992)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 405049)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 405052)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 405053)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                  else if (item.SelectedAnwerValue == 405054)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 405065)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 411412)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 411470)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 411473)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                  else if (item.SelectedAnwerValue == 411474)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                  else if (item.SelectedAnwerValue == 411475)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 411486)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                    // }
                    //call SaveVisitsInfo method to update the response survey status.
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 415585)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 415587)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 415588)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 415589)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415643)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415645)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                  else if (item.SelectedAnwerValue == 415646)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                  else if (item.SelectedAnwerValue == 415647)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415648)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415650)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 415655)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 415656)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415659)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415737)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 415738)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 415795)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415797)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415798)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415799)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415800)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415802)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415807)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 415811)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
                
                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

                                       
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }

                else if (item.SelectedAnwerValue == 446408)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 446411)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 446412)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 446413)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 446831)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 446834)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                  else if (item.SelectedAnwerValue == 446835)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 446836)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 448352)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 448355)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 448356)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 448357)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 446917)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 446920)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                 else if (item.SelectedAnwerValue == 446921)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 446922)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 447068)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 447071)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 447072)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }
                else if (item.SelectedAnwerValue == 447073)
                {
                    _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);

                    if (BasePage.Mode == RespondentDisplayMode.Respondent)
                        RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);


                    SetCookie(1, RespondentVisitorStatus.Complete);
                    Response.Redirect(GetLastpageUrl(3));
                }






                
                if (item.SelectedAnwerValue == 329106) 
                {
                    truecallerstat = 1;

                    Session["truecallersat"] = truecallerstat;

                    int cnt = Convert.ToInt32(dsscreenout.Tables[0].Rows[0][0].ToString());

                    if (cnt > 283) 
                    {
                        Response.Redirect(GetLastpageUrl(3));
                    }
                }
                else if (item.SelectedAnwerValue == 329107)
                {
                    truecallerstat = 0;

                    Session["truecallersat"] = truecallerstat;

                    int cnt = Convert.ToInt32(dsscreenout.Tables[1].Rows[0][0].ToString());

                    if (cnt > 300)
                    {
                        Response.Redirect(GetLastpageUrl(3));
                    }
                }
                else if (item.SelectedAnwerValue == 329150)
                {
                    truecallerstat = 1;

                    Session["truecallersat"] = truecallerstat;

                    int cnt = Convert.ToInt32(dsscreenout.Tables[2].Rows[0][0].ToString());

                    if (cnt > 250)
                    {
                        Response.Redirect(GetLastpageUrl(3));
                    }
                }
                else if (item.SelectedAnwerValue == 329151)
                {
                    truecallerstat = 0;

                    Session["truecallersat"] = truecallerstat;

                    int cnt = Convert.ToInt32(dsscreenout.Tables[3].Rows[0][0].ToString());

                    if (cnt > 300)
                    {
                        Response.Redirect(GetLastpageUrl(3));
                    }
                }
              
                
            }

           

            if (lstResponseOtions.Count > 0 && BasePage.Mode == RespondentDisplayMode.Respondent)
                ServiceFactory.GetService<RespondentService>().Save(lstResponseOtions, true, true);

            if (BasePage.Mode == RespondentDisplayMode.Respondent && BasePage.CurrentRespondentType == RespondentType.OnlyOnce)
            {
                //SurveyBasicInfoView sbi = new SurveyBasicInfoView();
                //if (sbi.SCREENOUTPAGE_TYPE == 0)
                //{
                //    SetCookie(1, RespondentVisitorStatus.Complete);
                //}
                //else
                //{
                    SetCookie(1, RespondentVisitorStatus.InProgress);
                //}
            }

            hdnpageIndex.Value = isSkipLogicExisted == false ? Convert.ToString(Utilities.ToInt(hdnpageIndex.Value) + 1) : hdnpageIndex.Value;
            //get status of current responsee.


            //DataSet dsscreenout = getTruecallercnt();
            //int cnt = Convert.ToInt32(dsscreenout.Tables[0].Rows[0][0].ToString());

            //if ((cnt > 5) && (truecalleranswerid == 327584))
            //{
            //    _fillStatus = GetSurveyResponseStatus(SkipType.ScreenOut, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
            //    Response.Redirect(GetLastpageUrl(3));
            //}
            //else
            //{
                _fillStatus = GetSurveyResponseStatus(SkipLogicType, BasePage.SurveyBasicInfoView, lstAllOptions, isSaveForLater);
           // }
            //call SaveVisitsInfo method to update the response survey status.
            if (BasePage.Mode == RespondentDisplayMode.Respondent)
                RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, _fillStatus, null, DbOperationType.UpdateStatus, Utilities.ToInt(hdnRespondentId.Value), LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);

        }

        /// <summary>
        /// Gets the survey response status.
        /// </summary>
        /// <param name="skipLogicType"></param>
        /// <param name="surveyBasicView">The survey basic info view.</param>
        /// <param name="lstAllOptions">The LST all options.</param>
        /// <returns></returns>
        private SurveyResponseStatus GetSurveyResponseStatus(SkipType skipLogicType, SurveyBasicInfoView surveyBasicView, IEnumerable<SurveyQuestionAndAnswerOptions> lstAllOptions, bool isSaveForLater = false)
        {
            IsQuotaReached = true;
            var isItLastSequenceNo = IsItlastQuestion(LastQuestionSeqNo, lstAllOptions);
            var maxAllowedResponseLimit = RespondentService.GetNoOfResponses(Utilities.ToEnum<UserType>(surveyBasicView.LICENSE_TYPE));
            var fillStatus = SurveyResponseStatus.None;
            switch (skipLogicType)
            {
                case SkipType.End:
                    fillStatus = SurveyResponseStatus.Complete;
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    break;

                case SkipType.ScreenOut:
                    fillStatus = SurveyResponseStatus.ScreenOut;
                    SetCookie(1, RespondentVisitorStatus.Complete);
                    break;

                default:
                    //COMMENTED THE BELOW LINES BECAUSE SURVEY RESPONDENT'S REDIRECTION WILL NOT DEPEND ON THE NUMBER OF RESPONSES
                    //THAT IS ONLY FOR THE CUSTOMER TO VIEW IN THE BACKEND REPORTS
                    //SATISH 12-OCT-15
                    //if (BasePage.Mode == RespondentDisplayMode.Preview || isSaveForLater == true)
                    //    fillStatus = SurveyResponseStatus.Partial;
                    //else if (!isItLastSequenceNo && surveyBasicView.ResponseCount <= maxAllowedResponseLimit)
                    //    fillStatus = SurveyResponseStatus.Partial;
                    //else if (isItLastSequenceNo && surveyBasicView.ResponseCount <= maxAllowedResponseLimit)
                    //{
                    //    fillStatus = SurveyResponseStatus.Complete;
                    //    SetCookie(1, RespondentVisitorStatus.Complete);
                    //}
                    //else if (surveyBasicView.ResponseCount >= maxAllowedResponseLimit)
                    //{
                    //    if (surveyBasicView.LICENSE_TYPE == "FREE")
                    //    {
                    //        IsQuotaReached = false;
                    //        fillStatus = SurveyResponseStatus.Complete;
                    //        SetCookie(1, RespondentVisitorStatus.Complete);
                    //    }
                    //    else
                    //    {
                    //        IsQuotaReached = false;
                    //        fillStatus = SurveyResponseStatus.Overquota;
                    //        SetCookie(1, RespondentVisitorStatus.Complete);
                    //    }
                    //}
                    //break;
                    if (BasePage.Mode == RespondentDisplayMode.Preview || isSaveForLater == true)
                    {
                        fillStatus = SurveyResponseStatus.Partial;
                    }
                    else if (!isItLastSequenceNo)
                    {
                        fillStatus = SurveyResponseStatus.Partial;
                    }
                    else if (isItLastSequenceNo)
                    {
                        fillStatus = SurveyResponseStatus.Complete;
                        SetCookie(1, RespondentVisitorStatus.Complete);
                    }
                    if (surveyBasicView.ResponseCount >= maxAllowedResponseLimit)
                    {
                        if (surveyBasicView.LICENSE_TYPE == "FREE")
                        {
                            IsQuotaReached = false;
                            //fillStatus = SurveyResponseStatus.Complete;
                            SetCookie(1, RespondentVisitorStatus.Complete);
                        }
                        else
                        {
                            IsQuotaReached = false;
                            //fillStatus = SurveyResponseStatus.Overquota;
                            SetCookie(1, RespondentVisitorStatus.Complete);
                        }
                    }
                    break;
            }

            return fillStatus;

        }

        /// <summary>
        /// Determines whether [is itlast question] [the specified last question seq no].
        /// </summary>
        /// <param name="lastQuestionSeqNo">The last question seq no.</param>
        /// <param name="lstAllOptions">The LST all options.</param>
        /// <returns>
        ///   <c>true</c> if [is itlast question] [the specified last question seq no]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsItlastQuestion(int? lastQuestionSeqNo, IEnumerable<SurveyQuestionAndAnswerOptions> lstAllOptions)
        {
            return lastQuestionSeqNo != null ? (lstAllOptions.OrderByDescending(qs => qs.Question.QUESTION_SEQ).Take(1).Where(
                q => q.Question.QUESTION_SEQ == lastQuestionSeqNo).FirstOrDefault() != null) : false;
        }

        /// <summary>
        /// Checks the skip logic applied.
        /// </summary>
        /// <param name="lstAllOptions">The LST all options.</param>
        /// <param name="selectedValue">The selected value.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private bool IsCheckSkipLogicApplied(IEnumerable<SurveyQuestionAndAnswerOptions> lstAllOptions, int selectedValue, SurveyQuestionAndAnswerOptions item)
        {
            osm_answeroptions skipOption = null;

          

            if (item.Question.SKIP_LOGIC == 1)
            {
                skipOption = item.AnswerOption.Where(an => an.ANSWER_ID == selectedValue).FirstOrDefault();
                SkipLogicType = Utilities.ToEnum<SkipType>(skipOption != null ? skipOption.SKIP_QUESTION_ID : 0);

                if (SkipLogicType == SkipType.None || SkipLogicType == SkipType.NoSkip)
                {
                    if (skipOption != null)
                    {
                        var found = lstAllOptions.Where(qa => qa.Question.QUESTION_ID == skipOption.SKIP_QUESTION_ID).FirstOrDefault();
                        if (found != null)
                            hdnpageIndex.Value = found.PageIndex.ToString();
                        else if (SkipLogicType == SkipType.NoSkip)
                            hdnpageIndex.Value = Convert.ToString(Convert.ToInt32(hdnpageIndex.Value) + 1);
                        else
                            Response.Redirect(GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTSKIPLOGICISSUE));
                    }
                }
            }
            return skipOption != null;
        }

        /// <summary>
        /// Generates the progressbar.
        /// </summary>
        public void GenerateProgressBar(List<SurveyQuestionAndAnswerOptions> filteredQuestions)
        {
            //this for hiding progress bar only for nasscom events.
            string[] customIds = ConfigurationManager.AppSettings["NasscomSurveyIds"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (!IsCustomSurvey(customIds))
            {
                var totalNoOfQuestions = BasePage.LastRank;
                var currentNoOfQuestions = 0;
                if (filteredQuestions != null)
                {
                    var query = from q in filteredQuestions
                                select q.RankIndex;
                    currentNoOfQuestions = filteredQuestions.Count > 0 ? filteredQuestions.Count == 1 && filteredQuestions[0].RankIndex == 0 ? 0 : Convert.ToInt32(query.Max()) : 0;
                }
                if (totalNoOfQuestions != 0)
                {
                    currentNoOfQuestions = currentNoOfQuestions > totalNoOfQuestions ? totalNoOfQuestions : currentNoOfQuestions;
                    var result = (currentNoOfQuestions * 100 / totalNoOfQuestions);

                    divProgressBarPanel.Visible = Convert.ToBoolean(BasePage.SurveyBasicInfoView.Show_Progressbar);
                    divProgressBar.Visible = divProgressBarPanel.Visible;
                    divProgressBar.Style.Add("width", string.Format("{0}%", result));

                    //set progress bar text
                    DisplayProgressBarText(totalNoOfQuestions, currentNoOfQuestions, result);
                }
            }
        }

        /// <summary>
        /// Displays the progress bar text.
        /// </summary>
        /// <param name="totalNoOfQuestions">The total no of questions.</param>
        /// <param name="currentNoOfQuestions">The current no of questions.</param>
        /// <param name="result">The result.</param>
        private void DisplayProgressBarText(int totalNoOfQuestions, int currentNoOfQuestions, int result)
        {
            //progress bar text
            if (BasePage.SurveyBasicInfoView.Show_Progressbartext == true)
            {
                divViewedtext.Style.Add("display", "block");
                ltQuesViewed.Text = string.Format("{0} {1}/{2}", BasePage.SurveyBasicInfoView.ProgressbarText, currentNoOfQuestions, totalNoOfQuestions);
                ltQuesPercentage.Text = string.Format("({0}%)", result);
            }
        }

        /// <summary>
        /// Displays the mandatory text.
        /// </summary>
        /// <param name="_surveyoptionsList">The _surveyoptions list.</param>
        private void DisplayMandatoryText(List<SurveyQuestionAndAnswerOptions> _surveyoptionsList)
        {
            DataSet dssrvysettings = scapp.getSurveySettings(BasePage.SurveyId);

            if (dssrvysettings.Tables[0].Rows.Count > 0)
            {
                if (dssrvysettings.Tables[0].Rows[0]["showQM"].ToString() == "True")
                {
                    divMandatoryText.Visible = true;
                }
                else
                {
                    divMandatoryText.Visible = false;
                }
            }
            else
            {
                divMandatoryText.Visible = _surveyoptionsList.Select(q => q.Question.RESPONSE_REQUIRED == 1).Any();
            }
        }

        /// <summary>
        /// Enables the once per user and password user.
        /// </summary>
        private void EnableOncePerUser()
        {

            DataSet dssrvysettings = scapp.getSurveySettings(BasePage.SurveyId);

            if (dssrvysettings.Tables[0].Rows.Count > 0)
            {
                if (dssrvysettings.Tables[0].Rows[0]["CUSTOMSAVELATER_TEXT"].ToString() == "")
                {
                    lbnSaveForLater.Text = "Save for later";
                }
                else
                {
                    lbnSaveForLater.Text = dssrvysettings.Tables[0].Rows[0]["CUSTOMSAVELATER_TEXT"].ToString();
                }
            }
            else
            {
                lbnSaveForLater.Text = "Save for later";
            }
            if (BasePage.SurveyId == 10964)//FOR INDIA.COM MOTOX SURVEY
            {
                lbnSaveForLater.Visible = false;
            }
            else {
                lbnSaveForLater.Visible = true;
            }
            if (BasePage.Mode == RespondentDisplayMode.Respondent)
            {
                if (BasePage.EmailId > 0 && BasePage.IsOncePerComputerUserCompletedResponses)
                    Response.Redirect(GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTINFORESPONDENT));
                else
                    return;
            }
            lbnSaveForLater.Visible = false;
            lbnExitSurvey.Visible = false;
        }

        /// <summary>
        /// Determines whether [the survey is half way through].
        /// </summary>
        private bool IsSurveyHalfWayThrough()
        {
            if (BasePage.CurrentRespondentType == RespondentType.MultipleTimes || BasePage.Mode == RespondentDisplayMode.PreviewWithTemplate || BasePage.Mode == RespondentDisplayMode.Preview || BasePage.Mode == RespondentDisplayMode.PreviewWithPagination)
                return false;

            var respondentCookie = Request.Cookies[CookieName];
            if (respondentCookie != null)
            {
                //Set Cookie
                switch (Utilities.ToEnum<RespondentVisitorStatus>(respondentCookie.Values["Status"]))
                {
                    case RespondentVisitorStatus.Complete:
                        Response.Redirect(GetLastpageUrl((int)EndMessages.INSIGHTO_DEFAULTINFORESPONDENT));
                        return true;
                    case RespondentVisitorStatus.SaveForLater:
                        //ShowPasswordConfirmationPage(false);
                        return true;
                }
                return false;
            }
            return false;
        }

        /// <summary>
        /// Gets the lastpage URL.
        /// </summary>
        /// <param name="pageType">Type of the page.</param>
        /// <returns></returns>
        private string GetLastpageUrl(int pageType)
        {
            return pageType != 0
                       ? EncryptHelper.EncryptQuerystring("SurveyLastPage.aspx",
                                                          string.Format("SurveyId={0}&PageType={1}&Mode={2}&RespondentId={3}&truecallerstat={4}", BasePage.SurveyId,
                                                                        pageType, BasePage.Mode, BasePage.RespondentId, Session["truecallersat"]))
                       : EncryptHelper.EncryptQuerystring("SurveyLastPage.aspx",
                                                          string.Format("SurveyId={0}&PageType={1}&Mode={2}&RespondentId={3}&truecallerstat={4}", 0, 0,
                                                                        string.Empty, BasePage.RespondentId, Session["truecallersat"]));
        }
        private string GetLastpageUrl1(int pageType)
        {
            return pageType != 0
                       ? EncryptHelper.EncryptQuerystring("SurveyLastPage.aspx",
                                                          string.Format("SurveyId={0}&PageType={1}&Mode={2}&RespondentId={3}&Embed={4}&truecallerstat={5}", BasePage.SurveyId,
                                                                        pageType, BasePage.Mode, BasePage.RespondentId, BasePage.Embed, Session["truecallersat"]))
                       : EncryptHelper.EncryptQuerystring("SurveyLastPage.aspx",
                                                          string.Format("SurveyId={0}&PageType={1}&Mode={2}&RespondentId={3}&Embed={4}&truecallerstat={5}", 0, 0,
                                                                        string.Empty, BasePage.RespondentId, BasePage.Embed, Session["truecallersat"]));
        }
        /// <summary>
        /// Sets the logo.
        /// </summary>
        private void SetLogo()
        {
            divLogo.Visible = Convert.ToBoolean(BasePage.SurveyBasicInfoView.Show_Logo);
            if (BasePage.SurveyBasicInfoView.Show_Logo == false || string.IsNullOrEmpty(BasePage.SurveyBasicInfoView.SURVEY_LOGO))
                return;

            Label ltlCompanyName = new Label();
            Image imgLogo = new Image();
            //set allignments
            HtmlTableRow trLogo = null;
            HtmlTableCell tdLogo = null;

            //assign survey company name
            ltlCompanyName.Text = !string.IsNullOrEmpty(BasePage.SurveyBasicInfoView.COMPANY_NAME)
                    ? BasePage.SurveyBasicInfoView.COMPANY_NAME
                    : string.Empty; //@"insighto";           

            //assign survey logo
            var logoSplit = BasePage.SurveyBasicInfoView.SURVEY_LOGO.Split(new[] { '<' }, StringSplitOptions.RemoveEmptyEntries);
            var url = "~/" + logoSplit[0].Replace(">", string.Empty);
            var alignment = logoSplit.Length > 1 ? logoSplit[2].Replace(">", string.Empty) : string.Empty;
            imgLogo.ImageUrl = url;

            //this is meant for only nasscom. "this is not a good practice." we will face many issues with these kind of changes.
            string[] customIds = ConfigurationManager.AppSettings["NasscomSurveyIds"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (IsCustomSurvey(customIds))
            {
                ltlCompanyName.Text = @"<div class='NasscomlogoView'><span style='color:maroon;font-weight:bold;'>NASSCOM</span> Product Conclave 2011 <br /><div class='nasspc_lbl'>(#nasscompc)</div></div>";

                trLogo = new HtmlTableRow();
                GetCompanyNameOrLogo(ltlCompanyName, alignment, trLogo);
                tblLogo.Rows.Add(trLogo);
                divLogo.Attributes.Add("class", "logo" + alignment.ToLower());
                return;
            }

            switch (alignment)
            {
                case "left":
                    trLogo = new HtmlTableRow();
                    GetCompanyNameOrLogo(imgLogo, alignment, trLogo);
                    GetCompanyNameOrLogo(ltlCompanyName, alignment, trLogo);

                    tblLogo.Rows.Add(trLogo);
                    break;

                case "center":
                    trLogo = new HtmlTableRow();
                    GetCompanyNameOrLogo(imgLogo, alignment, trLogo);
                    tblLogo.Rows.Add(trLogo);

                    trLogo = new HtmlTableRow();
                    GetCompanyNameOrLogo(ltlCompanyName, alignment, trLogo);
                    tblLogo.Rows.Add(trLogo);
                    break;

                case "right":
                    trLogo = new HtmlTableRow();
                    GetCompanyNameOrLogo(ltlCompanyName, alignment, trLogo);
                    GetCompanyNameOrLogo(imgLogo, alignment, trLogo);

                    tblLogo.Rows.Add(trLogo);
                    break;

                default:
                    trLogo = new HtmlTableRow();
                    GetCompanyNameOrLogo(ltlCompanyName, alignment, trLogo);
                    GetCompanyNameOrLogo(imgLogo, alignment, trLogo);

                    tblLogo.Rows.Add(trLogo);
                    break;

            }
            divLogo.Attributes.Add("class", "logo" + alignment.ToLower());
        }

        /// <summary>
        /// Gets the company name or logo.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="alignment">The alignment.</param>
        /// <param name="trLogo">The tr logo.</param>
        private void GetCompanyNameOrLogo(Control control, string alignment, HtmlTableRow trLogo)
        {
            var tdLogo = new HtmlTableCell();
            tdLogo.Style.Add("text-align", alignment);
            tdLogo.Controls.Add(control);
            trLogo.Controls.Add(tdLogo);
        }

        /// <summary>
        /// Sets the styles.
        /// </summary>
        private void SetStyles()
        {
            if (Page.Master == null) return;
            dynamic dynamicStyle = Page.Master.FindControl("dynamicStyle");
            if (dynamicStyle == null)
                dynamicStyle = Page.FindControl("dynamicStyle");

            if (dynamicStyle == null) return;
            const string fontFormat = "font:normal {0}pt {1}, Helvetica, sans-serif; ";
            const string fontColor = "color:{0};";
            var fontStringBuilder = new StringBuilder("body {");
            fontStringBuilder.AppendFormat(fontFormat, BasePage.SurveyBasicInfoView.SURVEY_FONT_SIZE, BasePage.SurveyBasicInfoView.SURVEY_FONT_TYPE);
            fontStringBuilder.AppendFormat(fontColor, ColorCodeHelper.GetColorCodes(BasePage.SurveyBasicInfoView.SURVEY_FONT_COLOR));
            fontStringBuilder.Append("}");
            fontStringBuilder.AppendLine();
            fontStringBuilder.Append(".dynamicButton {");
            fontStringBuilder.AppendFormat("background:{0};", ColorCodeHelper.GetColorCodes(BasePage.SurveyBasicInfoView.BUTTON_BG_COLOR));
            fontStringBuilder.Append("}");
            dynamicStyle.InnerText = fontStringBuilder.ToString();
        }

        /// <summary>
        /// Sets the buttons.
        /// </summary>
        private void SetButtons()
        {
            if (BasePage.Mode == RespondentDisplayMode.Preview)
            {
                btnContinue.Visible = false;
                imgContinueButton.Visible = false;
                return;
            }
            var isArrowLabelBtn = BasePage.SurveyBasicInfoView.SURVEY_BUTTON_TYPE == "Label";
            var isArrowPlainBtn = BasePage.SurveyBasicInfoView.SURVEY_BUTTON_TYPE == "Plain";

            var isArrowBtn = (isArrowLabelBtn || isArrowPlainBtn);

            btnContinue.Visible = !isArrowBtn;
            imgContinueButton.Visible = isArrowBtn;

            var buttonColor = String.IsNullOrEmpty(BasePage.SurveyBasicInfoView.BUTTON_BG_COLOR) ? "purple" : BasePage.SurveyBasicInfoView.BUTTON_BG_COLOR;
            btnContinue.Text = "";

            imgContinueButton.ImageUrl = GetArrowButtonPath(buttonColor);

            if (isArrowLabelBtn)
                imgContinueButton.ImageUrl = GetArrowButtonPath(buttonColor, "continue");
            else if (!isArrowBtn)
                btnContinue.Text = String.IsNullOrEmpty(BasePage.SurveyBasicInfoView.CUSTOM_CONTINUE_TEXT)
                                    ? "Continue"
                                    : BasePage.SurveyBasicInfoView.CUSTOM_CONTINUE_TEXT;
            // btnContinue.BackColor = System.Drawing.Color.FromName(buttonColor);

            //Note: When user tries to see preview Page_Break will be null.
            if (PageIndex == 1)
            {

                if (isArrowLabelBtn)
                    imgContinueButton.ImageUrl = GetArrowButtonPath(buttonColor, "start");
                else if (!isArrowBtn)
                    btnContinue.Text = String.IsNullOrEmpty(BasePage.SurveyBasicInfoView.CUSTOMSTART_TEXT)
                                    ? "Start"
                                    : BasePage.SurveyBasicInfoView.CUSTOMSTART_TEXT;
            }

            if (BasePage.Mode == RespondentDisplayMode.Preview || BasePage.IsLastPage)
            {
                if (isArrowLabelBtn)
                    imgContinueButton.ImageUrl = GetArrowButtonPath(buttonColor, "submit");
                else if (!isArrowBtn)
                    btnContinue.Text = String.IsNullOrEmpty(BasePage.SurveyBasicInfoView.CUSTOMSUBMITTEXT)
                                           ? "Submit"
                                           : BasePage.SurveyBasicInfoView.CUSTOMSUBMITTEXT;
            }
        }

        /// <summary>
        /// Gets the arrow button path.
        /// </summary>
        /// <param name="color">The color.</param>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        private static string GetArrowButtonPath(string color = "purple", string text = "plain")
        {
            return string.Format("~/Styles/ThemeImages/arrow-{0}-{1}.gif", color.ToLower().Replace(" ", ""), text.ToLower());
        }

        /// <summary>
        /// Shows the password confirmation page.
        /// </summary>
        /// <param name="IspasswordPage">if set to <c>true</c> [ispassword page].</param>
        private void ShowPasswordConfirmationPage(bool IspasswordPage)
        {
            if (IspasswordPage)
            {
                lblPopupSurveyName.Text = BasePage.SurveyBasicInfoView.SURVEY_NAME;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PasswordUserWindow", "ShowPasswordWindow();", true);
            }
            else
            {
                divLogin.Visible = false;
                trPartialMessgae.Visible = true;
                trButtons.Visible = true;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PasswordUserWindow", "ShowPasswordWindow();", true);
            }
        }

        /// <summary>
        /// Sets local propertis form base, generates response id if there isnt one
        /// </summary>
        private void SetLocalParamaeters()
        {
            IsQuotaReached = false;
            SurveyId = BasePage.SurveyId;
            PageIndex = BasePage.PageIndex;
            hdnpageIndex.Value = PageIndex.ToString();
            hdnPasswordVerified.Value = BasePage.PasswordVerified.ToString();

            if (!IsPostBack)
            {
                if (BasePage.RespondentId == 0 && BasePage.Mode == RespondentDisplayMode.Respondent)
                    BasePage.RespondentId = SaveRespondentData(DbOperationType.Insert);
                hdnRespondentId.Value = BasePage.RespondentId.ToString();

                if (BasePage.Mode == RespondentDisplayMode.Respondent && !IsSurveyActive())
                    lbnExitSurvey_Click(null, null);
            }
            hdnRespondentId.Value = BasePage.RespondentId.ToString();
        }

        private void SetSurveyIntroductionText()
        {
            //set survey introduction text
            lblSurveyRespondentHeader.Text = BasePage.SurveyBasicInfoView.SURVEY_HEADER;
            lblSurveyRespondentFooter.Text = BasePage.SurveyBasicInfoView.SURVEY_FOOTER;
            Page.Title = RespondentResources.RespondentPageTitle;
        }

        /// <summary>
        /// Sets the header details.
        /// </summary>
        private void SetHeaderDetails()
        {
            if (BasePage.Mode == RespondentDisplayMode.Print || BasePage.Mode == RespondentDisplayMode.Preview || BasePage.Mode == RespondentDisplayMode.PreviewWithPagination)
            {
                lblNoQuestions.Visible = BasePage.SurveyQuesAndAnswerOptionsList.Count == 0;
                divQuestionsPanel.Visible = BasePage.SurveyQuesAndAnswerOptionsList.Count > 0 && !BasePage.ShowTwoColumnLayOut;
                divTwoColumnPanel.Visible = BasePage.SurveyQuesAndAnswerOptionsList.Count > 0 && BasePage.ShowTwoColumnLayOut;
                divButtonsPanel.Visible = (BasePage.SurveyQuesAndAnswerOptionsList.Count == 1 &&
                                           BasePage.SurveyQuesAndAnswerOptionsList[0].QuestionType !=
                                           QuestionType.IntroductionText) ||
                                          BasePage.SurveyQuesAndAnswerOptionsList.Count > 1;

            }
        }

        /// <summary>
        /// Determines whether [is custom survey] [the specified custom ids].
        /// </summary>
        /// <param name="customIds">The custom ids.</param>
        /// <returns>
        ///   <c>true</c> if [is custom survey] [the specified custom ids]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsCustomSurvey(string[] customIds)
        {
            foreach (var item in customIds)
            {
                if (item.Trim() == SurveyId.ToString())
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Saves the respondent data.
        /// </summary>
        /// <param name="dbOperationType">Type of the db operation.</param>
        /// <param name="respondentId">The respondent id.</param>
        /// <returns></returns>
        private int SaveRespondentData(DbOperationType dbOperationType, int respondentId = 0)
        {
            int effectedrows = 0;
            respondentId= RespondentService.SaveVisitInfo(SurveyId, BasePage.EmailId, SurveyResponseStatus.Visits, null, dbOperationType, respondentId, LastQuestionSeqNo, BasePage.LaunchId, IsQuotaReached);
            RespondentService rs = new RespondentService();
            return effectedrows = rs.updateembed(respondentId, BasePage.Embed);
        }

        #endregion

        public SqlConnection strconnectionstring()
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection strconn = new SqlConnection(connStr);

                strconn.Open();
                return strconn;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public DataSet getCustomizeURLbit(int surveyID)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetCustomizeURL", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyID;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dsSurveyQA = new DataSet();
                sda.Fill(dsSurveyQA);
                return dsSurveyQA;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet getTruecallercnt(int surveyID)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetTrueCallerCount", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyID;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dsTrucaller = new DataSet();
                sda.Fill(dsTrucaller);
                return dsTrucaller;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet getanswertextCount(string answertext, int surveyid)
        {
            try
            {
                SqlConnection con = new SqlConnection();

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetAnswerTextCount", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@answertext", SqlDbType.VarChar)).Value = answertext;
                scom.Parameters.Add(new SqlParameter("@surveyid", SqlDbType.Int)).Value = surveyid;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dsanswercount = new DataSet();
                sda.Fill(dsanswercount);
                con.Close();
                return dsanswercount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}