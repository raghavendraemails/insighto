﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopLinks.ascx.cs" Inherits="Insighto.UserControls.TopLinks" %>
<div id="topLinksPanel">
    <div id="divVersion" runat="server" class="divVersion">
        <asp:Literal ID="ltlVersion" Visible="false"    runat="server" 
            meta:resourcekey="ltlVersionResource1" />
    </div>
    <!-- top links panel -->
    <div id="topLinkNav">
        <ul>
            <li>
                <asp:HyperLink ID="hlPricing" NavigateUrl="http://www.insighto.com/pricing/" runat="server" 
                    Text="Price"  Visible="False" meta:resourcekey="hlPricingResource1"></asp:HyperLink></li>
            <li>
                <asp:HyperLink ID="hlFAQ" NavigateUrl="#" onclick="javascript: window.open('Faqcms/CNS_AboutSurvey.aspx?key=O1IiHYYZiuqe67HGRNzwF7j%2btdpCU6IQFvQJRPSFZFVdz1jysLODHqPiBlwB3os07lCVCHakwMBdvY2JVZRBS7CpbynHlOEZ','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;" runat="server" Text="FAQ" 

                    Visible="False"                      
                    meta:resourcekey="hlFAQResource1"></asp:HyperLink></li>
            <li class="noRightBorder">
                <asp:HyperLink ID="hlContact" Text="Contact Us"  runat="server" 
                    meta:resourcekey="hlContactResource1"  h="520" w="700" scrolling='yes'></asp:HyperLink>
            </li>
        </ul>
    </div>
    <!-- //top links panel -->
</div>
<!-- onclick="javascript: window.open('help/1.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;" -->