﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using InfoSoftGlobal;
using System.Configuration;

public partial class MtxSS : System.Web.UI.UserControl
{
    public string exportchartfilehandler;  


    protected void Page_Load(object sender, EventArgs e)
    {
        // exportHandler='" + exportchartfilehandler + "' exportAction='download'
        exportchartfilehandler = ConfigurationSettings.AppSettings["exportchartfile"].ToString();


        //In this example, we plot a Combination chart from data contained
        //in an array. The array will have three columns - first one for Quarter Name
        //second one for sales figure and third one for quantity. 

        object[,] arrData = new object[4, 4];
        //Store Quarter Name
        arrData[0, 0] = "Demonstrated expertise in the subject matter";
        arrData[1, 0] = "Enhanced my knowledge in the subject";
        arrData[2, 0] = "Persuaded trainees interaction";
        arrData[3, 0] = "Was open and receptive to new ideas";
        //Store revenue data
        arrData[0, 1] = 20;
        arrData[1, 1] = 10;
        arrData[2, 1] = 60;
        arrData[3, 1] = 10;
        //Store Quantity
        arrData[0, 2] = 13;
        arrData[1, 2] = 47;
        arrData[2, 2] = 33;
        arrData[3, 2] = 7;


        arrData[0, 3] = 23;
        arrData[1, 3] = 7;
        arrData[2, 3] = 40;
        arrData[3, 3] = 30;
        //Now, we need to convert this data into combination XML. 
        //We convert using string concatenation.
        //strXML - Stores the entire XML
        //strCategories - Stores XML for the <categories> and child <category> elements
        //strDataRev - Stores XML for current year's sales
        //strDataQty - Stores XML for previous year's sales

        StringBuilder strXML = new StringBuilder();
        StringBuilder strCategories = new StringBuilder();
        StringBuilder strDataRev = new StringBuilder();
        StringBuilder strDataQty = new StringBuilder();
        StringBuilder strDataDA = new StringBuilder();

        //Initialize <chart> element
        strXML.Append("<chart palette='4' caption='Please rate your level of agreement with the following statements: The trainer' PYAxisName='' SYAxisName='' numberSuffix='%' formatNumberScale='0' showValues='1' decimals='0' canvasBgColor='f7eed2' canvasbgAlpha='0' bgColor='FFFFFF' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1'  formatNumberScale='0' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e' formatNumberScale='0' bgAlpha='0' exportEnabled='1'  showAboutMenuItem='0'  >");
        //  xmlData.Append("<chart caption='Sales by Product' numberPrefix='$' formatNumberScale='1' rotateValues='1' placeValuesInside='1' decimals='0' >");

        //Initialize <categories> element - necessary to generate a multi-series chart
        strCategories.Append("<categories>");

        //Initiate <dataset> elements

        strDataRev.Append("<dataset seriesName='Agree'>");
        strDataQty.Append("<dataset seriesName='Neutral' >");
        strDataDA.Append("<dataset seriesName='DisAgree' >");

        //Iterate through the data	
        for (int i = 0; i < arrData.GetLength(0); i++)
        {
            //Append <category name='...' /> to strCategories
            strCategories.AppendFormat("<category name='{0}' />", arrData[i, 0]);
            //Add <set value='...' /> to both the datasets
            strDataRev.AppendFormat("<set value='{0}' />", arrData[i, 1]);
            strDataQty.AppendFormat("<set value='{0}' />", arrData[i, 2]);
            strDataDA.AppendFormat("<set value='{0}' />", arrData[i, 3]);
        }

        //Close <categories> element
        strCategories.Append("</categories>");

        //Close <dataset> elements
        strDataRev.Append("</dataset>");
        strDataQty.Append("</dataset>");
        strDataDA.Append("</dataset>");
        //Assemble the entire XML now
        strXML.Append(strCategories.ToString());
        strXML.Append(strDataRev.ToString());
        strXML.Append(strDataQty.ToString());
        strXML.Append(strDataDA.ToString());
        strXML.Append("</chart>");

        //Create the chart - MS Column 3D Line Combination Chart with data contained in strXML
        Literal1.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/MSLine.swf", "", strXML.ToString(), "MtxSS", "600", "300", false, true);

    }
}