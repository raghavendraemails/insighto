﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactUsControl.ascx.cs"
    Inherits="In_UserControls_ContactUsControl" %>
<div class="contentPanelHeader">
    <div class="pageTitle"><h1>CONTACT US</h1></div>
</div>
<div class="contentPanel">

    <p>&nbsp;</p>
    <div class="mandatoryPanel" style="margin-top:0;">
        <asp:Label ID="lblRequiredFields" runat="server" 
            Text="indicates required fields" meta:resourcekey="lblRequiredFieldsResource1"></asp:Label>
    </div>
    <div class="text">
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" CssClass="errorMesg" Visible="False" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" 
                    Text="Thank you for contacting us. We will get back to you shortly." 
                    Visible="False" meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="lblName" runat="server" Text="Your Name" CssClass="requirelbl" 
                meta:resourcekey="lblNameResource1" />
        </div>
        <div class="loginControls">
            <asp:TextBox ID="txtName" runat="server" CssClass="textBoxMedium" MaxLength="50"
                TabIndex="1" meta:resourcekey="txtNameResource1"></asp:TextBox>
            <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqUserName" Display="Dynamic"
                ControlToValidate="txtName" runat="server" CssClass="lblRequired" 
                ErrorMessage="Please enter name." meta:resourcekey="reqUserNameResource1"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="Label1" runat="server" Text="Your Email" CssClass="requirelbl" 
                meta:resourcekey="Label1Resource1" />
        </div>
        <div class="loginControls">
            <asp:TextBox ID="txtMail" runat="server" CssClass="textBoxMedium" MaxLength="50"
                TabIndex="2" meta:resourcekey="txtMailResource1"></asp:TextBox>
            <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqMail" ControlToValidate="txtMail"
                runat="server" Display="Dynamic" ErrorMessage="Please enter email." 
                CssClass="lblRequired" meta:resourcekey="reqMailResource1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator SetFocusOnError="True" ID="rexUsername" CssClass="lblRequired"
                ControlToValidate="txtMail" runat="server" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ErrorMessage="Please enter valid email." 
                meta:resourcekey="rexUsernameResource1"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="Label2" runat="server" Text="Phone Number" CssClass="requirelbl" 
                meta:resourcekey="Label2Resource1" />
        </div>
        <div class="loginControls">
            <asp:TextBox ID="txtPhno" runat="server" MaxLength="10" CssClass="phoneNum_long"
                onpaste="return false;" TabIndex="3" 
                onkeypress="javascript:return onlyNumbers(event,'');" 
                meta:resourcekey="txtPhnoResource1"></asp:TextBox>
            <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqPhNo" runat="server" ControlToValidate="txtPhno"
                ErrorMessage="Please enter phone number." CssClass="lblRequired" 
                meta:resourcekey="reqPhNoResource1"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="Label4" runat="server" Text="Attention" 
                meta:resourcekey="Label4Resource1" />
        </div>
        <div class="loginControls">
            <asp:DropDownList ID="ddlAttenValues" CssClass="dropdownMedium" runat="server" 
                TabIndex="4" meta:resourcekey="ddlAttenValuesResource1">              
            </asp:DropDownList>
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="Label5" runat="server" Text="Mail Message" CssClass="requirelbl" 
                meta:resourcekey="Label5Resource1" />
        </div>
        <div class="loginControls">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:TextBox ID="txtMessage" TextMode="MultiLine" CssClass="textAreaMedium" runat="server"
                            TabIndex="5" onpastte="javascript:return false;" 
                            meta:resourcekey="txtMessageResource1" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqtxtMessage" runat="server"
                            ErrorMessage="Please enter message." ControlToValidate="txtMessage" 
                            CssClass="lblRequired" meta:resourcekey="reqtxtMessageResource1"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <span id="spn" runat="server" class="spn"><asp:Label ID="lblMaxtext" 
                            Text="Maximum 500 Characters"  runat="server" 
                            meta:resourcekey="lblMaxtextResource1"></asp:Label></span>
                    </td>
                </tr>
            </table>
        </div>
        <div id="message">
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="Label3" runat="server" AssociatedControlID="reCaptchaimg" 
                meta:resourcekey="Label3Resource1" />
        </div>
        <div class="loginControls">
            <img src="~/In/Captcha.aspx" id="reCaptchaimg" alt="Catptcha Text" runat="server" />
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
        </div>
        <div class="loginControls">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:Label ID="lblRecaptcha" Text="Type the above number"  runat="server" 
                            meta:resourcekey="lblRecaptchaResource1" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtCaptcha" CssClass="aspxtextbox textBoxSmall" runat="server" 
                            TabIndex="6" meta:resourcekey="txtCaptchaResource1" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
        </div>
        <div class="loginControls">
            <div class="top_padding">
                <asp:Button ID="btnsubmit" runat="server" CssClass="btn_small_65" ToolTip="Submit"
                    Text="Submit" OnClick="btnsubmit_Click" TabIndex="7" 
                    meta:resourcekey="btnsubmitResource1" /></div>
        </div>
    </div>
<div class="clear"></div>
</div>
<div class="clear"></div>
<script type="text/javascript">
    function onlyNumbers(e, Type) {
        var key = window.event ? e.keyCode : e.which;
        if (Type == 'Countrycode' && key == 43) {
            return true;
        }
        else if (key > 31 && (key < 48 || key > 57)) {
            return false;
        }
        return true;
    }
    $(function () {
        var limit = 500;
        $('textarea[id$=txtMessage]').keyup(function () {
            var len = $(this).val().length;
            if (len > limit) {
                this.value = this.value.substring(0, limit);
                len = limit;
            }
            $('.spn').text(limit - len + " characters left");
        });
    });
</script>
