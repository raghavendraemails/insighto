﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using InfoSoftGlobal;
using System.Configuration;

public partial class UserControls_ConstSum : System.Web.UI.UserControl
{
    public string exportchartfilehandler;  


    protected void Page_Load(object sender, EventArgs e)
    {
        exportchartfilehandler = ConfigurationSettings.AppSettings["exportchartfile"].ToString();


           StringBuilder strXML = new StringBuilder();

           strXML.Append("<chart caption='What percentage of your discretionary money do you spend on the following?' numberSuffix='%' exportEnabled='1' canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' pieSliceDepth='30' showBorder='1' borderColor='6e1473' borderThickness='1'  exportHandler='" + exportchartfilehandler + "' exportAction='download' formatNumberScale='0' bgColor='FFFFFF' showAboutMenuItem='0'>");
 
          strXML.Append("<set value='15' label='Entertainment'/>");  
        strXML.Append("<set value='22' label='Electronics'/>");   
        strXML.Append("<set value='22' label='Clothes'/>");
        strXML.Append("<set value='41' label='Food'/>");

        strXML.Append("</chart>");
        Literal1.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/Doughnut3D.swf", "", strXML.ToString(), "ConstantSum", "600", "300", false, true);
    }
}