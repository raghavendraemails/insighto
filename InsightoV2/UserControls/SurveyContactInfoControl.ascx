﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveyContactInfoControl.ascx.cs"
    Inherits="UserControls.SurveyContactInfoControl" %>
<%@ Register Src="~/UserControls/Contactinfo.ascx" TagName="SurveyContactInfo" TagPrefix="uc" %>
<div>
    Choose the contact information you wish the respondent to provide:<br />
    First check the boxes for the fields you'd like to appear.<br />
    Modify the field names in the boxes (optional)<br />
</div>
<div class="contactInfo">
    <asp:CustomValidator SetFocusOnError="true" ID="custTextControl" OnServerValidate="custValidateUniqText"
        runat="server" CssClass="lblRequired" Display="Dynamic" ErrorMessage="Answer options should not be same."></asp:CustomValidator>
    <uc:SurveyContactInfo ID="ucFirstName" FormFieldText="First Name"
        runat="server" />
    <uc:SurveyContactInfo ID="ucLastName" FormFieldText="Last Name"
        runat="server" />
    <uc:SurveyContactInfo ID="ucAddress1" FormFieldText="Address 1"
        runat="server" />
    <uc:SurveyContactInfo ID="ucAddress2" FormFieldText="Address 2"
        runat="server" />
    <uc:SurveyContactInfo ID="ucCity" FormFieldText="City"
        runat="server" />
    <uc:SurveyContactInfo ID="ucCountry"  FormFieldText="Country"
        runat="server" />
    <uc:SurveyContactInfo ID="ucHomePhone"  FormFieldText="Home Phone"
        runat="server" />
    <uc:SurveyContactInfo ID="ucWorkPhone"  FormFieldText="Work Phone"
        runat="server" />
    <uc:SurveyContactInfo ID="ucCellPhone"  FormFieldText="Cell Phone"
        runat="server" />
    <uc:SurveyContactInfo ID="ucEmailAddress"  FormFieldText="Email Address"
        runat="server" />
</div>
<div style="padding-left:30px; padding-top:5px;">
    Use this for capturing email id. Invalid email format will be validated.
</div>
