﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Insighto.UserControls
{
    public partial class AddressbookColumns : UserControlBase
    {
        private List<string> _value = new List<string> { string.Empty, string.Empty, string.Empty };
        private string validationGroup = "";
        private bool IsReq;
        public   List<string> Value
        {
            get
            {
                _value = GetRepeaterValues();
                return _value;
            }
            set
            {
                _value = value;
            }
        }


        public override bool IsRequired
        {
            get
            {
                return IsReq;
            }
            set
            {
                IsReq = value;
            }
        }

        /// <summary>
        /// The Group (Validation Group)
        /// </summary>
        public override string Group
        {
            get
            {
                return validationGroup;
            }
            set
            {
                validationGroup = value;
            }
        }

        public override void OnLoad(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
            base.OnLoad(sender, e);
        }

        /// <summary>
        /// Setup the Control
        /// </summary>
        public override void Initialise()
        {

        }
        /// <summary>
        /// Binds the Data
        /// </summary>
        public override void Display()
        {
            if (_value != null)
            {
                if (_value.Count > 0)
                {
                    btnAdd.Visible = false;
                }
                BindRepeater(_value);
            }
        }

        public override void Clear()
        {
            int i=1;
            foreach (RepeaterItem item in addRemoveControls.Items)
            {

                ((TextBox)item.FindControl("txtColumn")).Text = "";
               
                
            }
        }


        private void BindRepeater(List<string> data1)
        {           
            addRemoveControls.DataSource = data1;
            addRemoveControls.DataBind();
        }

        protected void addRemoveControls_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //base.SurveyPageBase.IsSurveyActive 
            _value.Clear();
            if (e.CommandName == "Add")
            {
               
                foreach (RepeaterItem item in addRemoveControls.Items)
                {
                    //getting the values of user entered fields
                    string title = ((TextBox)item.FindControl("txtColumn")).Text.Trim();
                    Button btnAdd = ((Button)item.FindControl("btnAddAnother"));                   
                    
                    _value.Add(title);
                    if (btnAdd == e.CommandSource) //the current row on which user click will removed
                    {
                        _value.Add(string.Empty);
                    }
                }
                BindRepeater(_value);
            }
            else if (e.CommandName == "Remove")
            {

                
                foreach (RepeaterItem item in addRemoveControls.Items)
                {
                    Button btnRemove = ((Button)item.FindControl("btnRemove"));
                    if (btnRemove != e.CommandSource) //the current row on which user click will removed
                    {
                        string title = ((TextBox)item.FindControl("txtColumn")).Text.Trim();                       
                        _value.Add(title);
                    }
                }
                if (_value.Count == 0)
                    btnAdd.Visible = true;
                BindRepeater(_value);
            }
        }

        private List<string> GetRepeaterValues()
        {
            var list = new List<string>();
            foreach (RepeaterItem item in addRemoveControls.Items)
            {
                string title = ((TextBox)item.FindControl("txtColumn")).Text.Trim();
             
                list.Add(title);
            }

            return list;
        }

        protected void itemDataBoundRepeater_ItemDataBound(object source, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var reqAnsOption = e.Item.FindControl("reqAnsoption") as RequiredFieldValidator;
                if (reqAnsOption != null)
                {
                    reqAnsOption.ValidationGroup = validationGroup;
                    IsReq = true;
                    reqAnsOption.Enabled = IsReq;
                }

                var btnAdd = e.Item.FindControl("btnAddAnother") as Button;
                var btnRemove = e.Item.FindControl("btnRemove") as Button;

                //btnAdd.Visible = !base.SurveyPageBase.IsSurveyActive;
               // btnRemove.Visible = !base.SurveyPageBase.IsSurveyActive;

                if (btnRemove != null && _value.Count == 1)
                {
                   // btnRemove.Visible = false;
                    reqAnsOption.Enabled = false;
                }
                if (btnAdd != null && _value.Count > 1)
                {
                    btnAdd.Visible = false;
                }
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            List<string> lstStr = new List<string>();
            lstStr.Add(" ");
            Value= lstStr;
            Display();
        }

        protected void custValidateUniqText(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (Value.Count == Value.Select(v => v.ToLower()).Distinct().Count());  // field is empty  
        }
    }
}