﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using InfoSoftGlobal;
using System.Configuration;

public partial class UserControls_Ranking : System.Web.UI.UserControl
{
    public string exportchartfilehandler;  

    protected void Page_Load(object sender, EventArgs e)
    {
        //exportHandler='" + exportchartfilehandler + "' exportAction='download'
        exportchartfilehandler = ConfigurationSettings.AppSettings["exportchartfile"].ToString();


        StringBuilder strXML = new StringBuilder();
        strXML.Append("<chart caption='Please rank the following sports in the order of their popularity.' numberSuffix='%' exportEnabled='1' canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1'   formatNumberScale='0' bgColor='FFFFFF' showAboutMenuItem='0'> ");
        strXML.AppendFormat("<categories>");
        strXML.AppendFormat(" <category Label='Cricket'/>");
        strXML.AppendFormat(" <category Label='Football'/>");
        strXML.AppendFormat(" <category Label='Tennis'/>");
        strXML.AppendFormat(" <category Label='Rugby'/>");
        strXML.AppendFormat("</categories>");

        strXML.AppendFormat("<dataset seriesName='1'>");
        strXML.AppendFormat(" <set  value='50' /> ");
        strXML.AppendFormat("<set  value='20' />");
        strXML.AppendFormat("<set  value='60' />");
        strXML.AppendFormat("<set  value='30' />");
        strXML.AppendFormat("</dataset>");

        strXML.AppendFormat("<dataset seriesName='2'>");
        strXML.AppendFormat(" <set  value='20' /> ");
        strXML.AppendFormat("<set  value='40' />");
        strXML.AppendFormat("<set  value='10' />");
        strXML.AppendFormat("<set  value='30' />");
        strXML.AppendFormat("</dataset>");

        strXML.AppendFormat("<dataset seriesName='3'>");
        strXML.AppendFormat(" <set  value='10' /> ");
        strXML.AppendFormat("<set  value='20' />");
        strXML.AppendFormat("<set  value='15' />");
        strXML.AppendFormat("<set  value='20' />");
        strXML.AppendFormat("</dataset>");

        strXML.AppendFormat("<dataset seriesName='4'>");
        strXML.AppendFormat(" <set  value='20' /> ");
        strXML.AppendFormat("<set  value='20' />");
        strXML.AppendFormat("<set  value='15' />");
        strXML.AppendFormat("<set  value='20' />");
        strXML.AppendFormat("</dataset>");
        strXML.Append("</chart>");
        Literal1.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/MSColumn3D.swf", "", strXML.ToString(), "Rankchart", "600", "300", false, true, false);
    }
}