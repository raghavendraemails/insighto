﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Data;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using System.Collections;
using CovalenseUtilities.Helpers;
using Insighto.Business.Enumerations;
using App_Code;
using FeatureTypes;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace Insighto.UserControls
{
    public partial class TopMenuSurvey : UserControlBase
    {
        Hashtable ht = new Hashtable();
        string NavigateUrl = "";
        
        DataTable dtpoll;
        string surveylicencetype = "";

        [Flags]
        enum SubType
        {
            Survey = 1,
            Poll=2
        }
        int subtype;
        PollCreation Pc = new PollCreation();
        string polllicencetype = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetPremiumPermissions();
                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0)
                    {
                            lnkAddressBook.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetAddressBookURL(), "surveyFlag=" + SurveyFlag);
                            lnkMyProfile.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetMyProfileURL(), "surveyFlag=" + SurveyFlag);
                            lnkFolders.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetManageFoldersURL(), "surveyFlag=" + SurveyFlag);
                            var sessionService = ServiceFactory.GetService<SessionStateService>();
                            var getUserInfo = sessionService.GetLoginUserDetailsSession();
                            SqlConnection con = new SqlConnection();
                            con = strconnectionstring();
                            SqlCommand scom = new SqlCommand("sp_GetTeleUser", con);
                            scom.CommandType = CommandType.StoredProcedure;

                            scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = getUserInfo.UserId;

                            SqlDataAdapter sda = new SqlDataAdapter(scom);
                            DataSet dsteleuser = new DataSet();
                            sda.Fill(dsteleuser);
                            con.Close();
                            if (dsteleuser.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
                            {
                           
                                lnkAddressBook.Visible = false;
                            }
                            else
                            {
                                lnkAddressBook.Visible = true;
                            }
                       
                            lnkCreate.Visible = true;
                            lnkMyProfile.Visible = true;
                            lnkFolders.Visible = true;
                            if (Request.RawUrl.ToLower().Contains("addressbook.aspx") || Request.RawUrl.ToLower().Contains("createemaillist.aspx"))
                            {
                                liAddressBook.Attributes.Add("class", "activelink");
                                liMyProfile.Visible = false;
                                liFolders.Visible = false;
                            }
                            else if (Request.RawUrl.ToLower().Contains("myprofile.aspx")
                                || Request.RawUrl.ToLower().Contains("invoicehistory.aspx")
                                || Request.RawUrl.ToLower().Contains("managelogin.aspx")
                                || Request.RawUrl.ToLower().Contains("changepassword.aspx")
                                || Request.RawUrl.ToLower().Contains("managefolders.aspx")
                                || Request.RawUrl.ToLower().Contains("managecategory.aspx"))
                            {
                                if (Request.RawUrl.ToLower().Contains("managefolders.aspx") || Request.RawUrl.ToLower().Contains("managecategory.aspx"))
                                {
                                    liFolders.Attributes.Add("class", "activelink");
                                }
                                else
                                    liMyProfile.Attributes.Add("class", "activelink");
                                liMyProfile.Visible = false;
                                liFolders.Visible = false;
                            }
                            else if (Request.RawUrl.ToLower().Contains("polls/mypolls.aspx"))
                            {
                                liMyPolls.Attributes.Add("class", "activelink");
                            }
                            else if (Request.RawUrl.ToLower().Contains("surveydrafts.aspx") || Request.RawUrl.ToLower().Contains("surveymiddlepage.aspx") || Request.RawUrl.ToLower().Contains("addintroduction.aspx") || Request.RawUrl.ToLower().Contains("theme.aspx") || Request.RawUrl.ToLower().Contains("logo.aspx") || Request.RawUrl.ToLower().Contains("font.aspx") || Request.RawUrl.ToLower().Contains("headerfooter.aspx") || Request.RawUrl.ToLower().Contains("button.aspx") || Request.RawUrl.ToLower().Contains("surveymessages.aspx") || Request.RawUrl.ToLower().Contains("surveyalerts.aspx") || Request.RawUrl.ToLower().Contains("launchsurvey.aspx"))
                            {
                                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Hide", "$('#topMainLeftLinksPnlPoll').hide();", true);
                                topmenurightpanel.Attributes.CssStyle.Add("display", "none");
                            }
                            else if (Request.RawUrl.ToLower().Contains("polls/superpoll.aspx") || Request.RawUrl.ToLower().Contains("polls/createpoll.aspx") || Request.RawUrl.ToLower().Contains("polls/pollbasics.aspx") || Request.RawUrl.ToLower().Contains("polls/pollreports.aspx"))
                            {
                                liMyPolls.Attributes.Add("class", "activelink");
                                topMainLeftLinksPnl.Attributes.CssStyle.Add("display", "none");
                                litemplates.Attributes.CssStyle.Add("display", "none");
                                liAddressBook.Attributes.CssStyle.Add("display", "none");
                                liAddressBook.Attributes.CssStyle.Add("display", "none");
                            }
                            else if (Request.RawUrl.ToLower().Contains("polls/pollreports.aspx"))
                            {
                                liMyPolls.Attributes.Add("class", "activelink");
                            }
                            else if (Request.RawUrl.ToLower().Contains("polls/pollmanager.aspx"))
                            {
                                liMyPolls.Attributes.Add("class", "activelink");
                            } 
                            else if (Request.RawUrl.ToLower().Contains("surveytemplates.aspx"))
                            {
                                lnktemplates.Attributes.Add("class", "activelink");
                            }
                            else
                            {
                                liMyProfile.Visible = false;
                                liFolders.Visible = false;
                                liMyAccount.Attributes.Add("class", "activelink");
                            }
                        }
                }

                lnktemplates.NavigateUrl = EncryptHelper.EncryptQuerystring("~/SurveyTemplate.aspx", "&surveyFlag=" + SurveyFlag);
            }                    
        }

        public SqlConnection strconnectionstring()
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection strconn = new SqlConnection(connStr);

                strconn.Open();
                return strconn;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// Sets the premium permissions.
        /// </summary>
        private void SetPremiumPermissions()
        {
            UserType userType = Utilities.GetUserType();
            bool showWidget = Utilities.ShowOrHideFeature(userType, (int)CreateWidget.Create_Widget);
        }

        protected void lnkWidget_Click(object sender, EventArgs e)
        {
        }
        
        protected void lnkMyAccount_Click(object sender, EventArgs e)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var getUserInfo = sessionService.GetLoginUserDetailsSession();
            if (getUserInfo.UserId > 0)
            {
                dtpoll = Pc.getLicencePoll(getUserInfo.UserId);
                if (dtpoll.Rows.Count > 0)
                {
                    surveylicencetype = dtpoll.Rows[0]["LICENSE_TYPE"].ToString();
                }
                if (surveylicencetype != "")
                {
                    NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + SurveyFlag);
                    Response.Redirect(NavigateUrl);
                }
                else
                {
                    string ActivationLink = EncryptHelper.EncryptQuerystring("ActivateSubType.aspx", "userid=" + getUserInfo.UserId + "&tag=" + "Survey");
                    //Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + ActivationLink + "',330, 200, 'yes');</script>");
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Upgrade", "OpenModalPopUP('" + ActivationLink + "',330, 200, 'yes');", true);
                }
            }
        }
        protected void lnkMyPolls_Click(object sender, EventArgs e)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var getUserInfo = sessionService.GetLoginUserDetailsSession();
            if (getUserInfo.UserId > 0)
            {
                dtpoll = Pc.getLicencePoll(getUserInfo.UserId);
                if (dtpoll.Rows.Count > 0)
                {
                    polllicencetype = dtpoll.Rows[0]["polllicencetype"].ToString();
                }
                if (polllicencetype != "")
                {
                    NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyPollPageURL(), "surveyFlag=" + SurveyFlag);
                    Response.Redirect(NavigateUrl);
                }
                else
                {
                    string NavUrl = "userid=" + getUserInfo.UserId;
                    Response.Redirect(EncryptHelper.EncryptQuerystring("~/Polls/signup.aspx", NavUrl));
                }
            }
        }
        
        protected void lnkCreate_Click(object sender, EventArgs e)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var getUserInfo = sessionService.GetLoginUserDetailsSession();
            if (getUserInfo.UserId > 0)
            {
                dtpoll = Pc.getLicencePoll(getUserInfo.UserId);
                if (dtpoll.Rows.Count > 0)
                {
                    surveylicencetype = dtpoll.Rows[0]["LICENSE_TYPE"].ToString();
                }
                if (surveylicencetype != "")
                {
                    NavigateUrl = EncryptHelper.EncryptQuerystring("../SurveyBasics.aspx", "surveyFlag=" + SurveyFlag);
                    Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + NavigateUrl + "',700, 500, 'yes');</script>");
                }
                else
                {
                    string ActivationLink = EncryptHelper.EncryptQuerystring("ActivateSubType.aspx", "userid=" + getUserInfo.UserId + "&tag=" + "Survey");
                    Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + ActivationLink + "',330, 200, 'yes');</script>");
                }
            }
        }
        
        //protected void lnkCreatePoll_Click(object sender, EventArgs e)
        //{
        //    var sessionService = ServiceFactory.GetService<SessionStateService>();
        //    var getUserInfo = sessionService.GetLoginUserDetailsSession();
        //    if (getUserInfo.UserId > 0)
        //    {
        //        dtpoll = Pc.getLicencePoll(getUserInfo.UserId);
        //        if (dtpoll.Rows.Count > 0)
        //        {
        //            polllicencetype = dtpoll.Rows[0]["polllicencetype"].ToString();
        //        }
        //        if (polllicencetype != "")
        //        {
        //            NavigateUrl = EncryptHelper.EncryptQuerystring("/Polls/superpoll.aspx", "surveyFlag=" + SurveyFlag);
        //            Response.Redirect(NavigateUrl);
        //        }
        //        else
        //        {
        //            string NavUrl = "userid=" + getUserInfo.UserId;
        //            Response.Redirect(EncryptHelper.EncryptQuerystring("~/Polls/signup.aspx", NavUrl));
        //        }
        //    }
        //}
}
}