﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OtherHeader.ascx.cs" Inherits="new_UserControls_OtherHeader" %>
<!--=== Header v6 ===-->
  <div class="header header-sticky">
    <div class="container"> <a class="logo pull-left big-link active" href="home.aspx"> <img src="assets/img/insighto-logo.png" alt="Logo" > </a>
      <div class="topbar">
        <ul class="loginbar pull-right">
          <li><a href="login.aspx">Login</a></li>
          <li class="topbar-devider"></li>
          <li><a href="registration.aspx" class="signupBtn">SIGNUP</a></li>
        </ul>
      </div>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"> <span class="sr-only">Toggle navigation</span> <span class="fa fa-bars"></span> </button>
    </div>
    <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
      <div class="container">
        <ul class="nav navbar-nav">
          <li class="active"> <a href="home.aspx"> Home </a> </li>
          <li> <a href="/surveys/home.aspx" class=active> Surveys </a> </li>
          <li> <a href="/polls/home.aspx" class=active> Polls </a> </li>
          <li> <a href="/home.aspx#pricing" class=active> Pricing </a> </li>
          <%--<li> <a href="https://www.insighto.com/blog/" target="_blank"> Blog </a> </li>--%>
          <%--<li> <a href="#"> Blog </a> </li>--%>
        </ul>
      </div>
      <!--/end container-->
    </div>
    <!--/navbar-collapse-->
  </div>
  <!--=== End Header v6 ===-->

  