﻿using System;
using System.Text.RegularExpressions;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Resources;
using System.Collections;

public partial class UserControls_LoginControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblInvaildAlert.Text = string.Empty;
        lblInvaildAlert.Visible = false;
    }
    protected void ibtnLogin_Click(object sender, EventArgs e)
    {
        if (!IsPostBack)
            return;



        if (Request.QueryString["key"] != null)
        {
            Hashtable ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 && ht.Contains("UserId"))
            {
                int userId = ValidationHelper.GetInteger(ht["UserId"].ToString(), 0);
                UsersService userService = new UsersService();
                var user = userService.FindUserByUserId(userId);
                userService.ProcessUserLogin(user);

                //Timezone is null for first user login. hence redirecting to welcome page
                if (user.ACTIVATION_FLAG == null || user.ACTIVATION_FLAG == 0)
                {
                    userService.ActivateUser(userId);
                   // Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=" + SurveyFlag));
                }
            }
            if (ht != null && ht.Count > 0 && ht.Contains("AlternateId"))
            {
                int Id = ValidationHelper.GetInteger(ht["AlternateId"].ToString(), 0);
                UsersService userService = new UsersService();
                var user = userService.ActivateAlternateUser(Id);
            }
        }


        string alrt = "Please enter";
        this.lblInvaildAlert.Visible = true;
        bool flag = false;
        if (string.IsNullOrEmpty(txtEmailAdd.Text.Trim()) || txtEmailAdd.Text.ToLower() == "email address")
        {
            alrt += " email address.";
            lblInvaildAlert.Text = alrt;            
        }
        else
        {
            Regex reLenient = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            bool isEmail = reLenient.IsMatch(txtEmailAdd.Text);
            if (!isEmail)
            {
                alrt += " valid email address.";
                lblInvaildAlert.Text = alrt;
            }
            else if (Convert.ToString(txtPassword.Text).Length == 0)
            {
                alrt += " password.";
                lblInvaildAlert.Text = alrt;
            }
            flag = true;
        }

        if (flag && Convert.ToString(txtEmailAdd.Text).Length > 0 && Convert.ToString(txtPassword.Text).Length > 0)
        {
            // To check authenticated user       
            var userService = ServiceFactory.GetService<UsersService>();
            var user = userService.getAuthenticatedUser(txtEmailAdd.Text, txtPassword.Text);
            if (user == null)
            {
                lblInvaildAlert.Text = "Invalid Email Address or Password.";
                lblInvaildAlert.Visible = true;
                
                return;
            }
            else if (Utilities.ToEnum<SurveyStatus>(user.STATUS) == SurveyStatus.InActive || ValidationHelper.GetInteger(user.DELETED, 0) == 1)
            {
                lblInvaildAlert.Text = "Your Account is de-activated.";
                lblInvaildAlert.Visible = true;
                return;
            }
            //else if (user.ACTIVATION_FLAG == 0 || user.ACTIVATION_FLAG == null)
            //{
            //    lblInvaildAlert.Text = " Your Account is not activated.";
            //    lblInvaildAlert.Visible = true;
            //    return;



            //}
            else
            {
                userService.ProcessUserLogin(user);
                if (user.TIME_ZONE == null) //Timezone is null for first user login. hence redirecting to welcome page
                {
                    //create folder with userid 
                    //CreateUserFolders(user); this function is moved to register free user .. as every user must have this.
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                }
                else if (user.RESET == 1) // Password reset value is 1 for first login. hence redirectiong to chanepassword page
                {
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserChangePasswordPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                }
                else // on successful login user is redirected to Myaccounts page
                {
                    Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                }
            }

        }
        else
        {


        }
    }
}