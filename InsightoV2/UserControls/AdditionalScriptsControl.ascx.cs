﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class UserControls_AdditionalScriptsControl : System.Web.UI.UserControl
{
    public string VersionNumber
    {
        get
        {
            return ConfigurationManager.AppSettings["versionNumber"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}