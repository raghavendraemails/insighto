﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class UserControls_UcUpgradenew : System.Web.UI.UserControl
{
    public static int countfree = 0;
    public static int countproyear = 0;
    public static int countpromonth = 0;
    string polllicence = "";
    int userid = 0;    


    protected void Page_Load(object sender, EventArgs e)
    {

        pollslink.Visible = false;
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        userid = userDetails.UserId;
        PollCreation pc = new PollCreation();
        DataTable dtpoll = pc.getLicencePoll(userid);
        string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
        SqlConnection strconn = new SqlConnection(connStr);
        strconn.Open();
        SqlCommand scom = new SqlCommand("sp_getpassword", strconn);
        scom.CommandType = CommandType.StoredProcedure;
        scom.Parameters.Add(new SqlParameter("@login_name", SqlDbType.VarChar)).Value = userDetails.LoginName;

        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet dsreferredby = new DataSet();
        sda.Fill(dsreferredby);

        string strreferredby = dsreferredby.Tables[0].Rows[0][1].ToString();
        string surveylicencetype = "";
        surveylicencetype = (userDetails.LicenseType ?? "");
        if (userDetails.LicenseType == UserType.PREMIUM_YEARLY.ToString())
        {
            if (userDetails.LicenseOpted == "SOCIALPREMIUM")
            {
                divFreeUser.Visible = false;
                divProUser.Visible = false;
            }
            else
            {
                divFreeUser.Visible = false;
                divProUser.Visible = false;
            }
        }
       
        else if ((userDetails.LicenseType == UserType.FREE.ToString()) || (surveylicencetype == ""))
        {

            divFreeUser.Visible = true;
            divProUser.Visible = false;

            string strUpgrate_pro;
            string strUpgrate_prem;
            if (strreferredby == "")
            {
                strUpgrate_pro = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&Prem=pre");
                strUpgrate_prem= EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&Prem=Prem");
            }
            else
            {
                 strUpgrate_pro = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&Prem=pre");
                strUpgrate_prem= EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&Prem=Prem");
            }
            UpgradeBuysurveycredit.HRef= strUpgrate_pro;

            if (Request.RawUrl.ToLower().Contains("polls/"))
            {
                UpgradeBuysurveycredit.Visible = false;
                UpgradeBuypremannual.Visible = false;
                divProUser.Visible = false;
                divFreeUser.Visible = false;
                pollslink.Visible = true;
            }

        }
        else if (userDetails.LicenseType == UserType.PRO_QUARTERLY.ToString())
        {
        }
        else if (userDetails.LicenseType == UserType.PRO_MONTHLY.ToString())
        {
            divProUser.Visible = false;
            divFreeUser.Visible = true;

            string strUpgrate_pro;

            if (strreferredby == "")
            {
                 strUpgrate_pro = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&Prem=pre");
            }
            else
            {
                strUpgrate_pro = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&Prem=pre");
            }

            UpgradeBuysurveycredit.HRef = strUpgrate_pro;

            string strUpgrate_prem;
            if (strreferredby == "")
            {
                 strUpgrate_prem = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&Prem=Prem");
            }
            else
            {
                strUpgrate_prem = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&Prem=Prem");
            }
            UpgradeBuysurveycredit.HRef = strUpgrate_prem;
        }
        else if (userDetails.LicenseType == UserType.PRO_YEARLY.ToString())
        {

            divProUser.Visible = false;
            divFreeUser.Visible = true;

            string strUpgrate_prem;
            
            if (strreferredby == "")
            {
                 strUpgrate_prem = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&Prem=Prem");
            }
            else
            {
                strUpgrate_prem = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&Prem=Prem");
            }
            UpgradeBuysurveycredit.HRef = strUpgrate_prem;
        }
        else
        {
            UpgradeBuysurveycredit.Visible = false;
            UpgradeBuypremannual.Visible = false;
            divProUser.Visible = false;
            divFreeUser.Visible = false;
        }

        if (Request.RawUrl.ToLower().Contains("polls/") || Request.RawUrl.ToLower().Contains("myprofile.aspx"))
        {
            if (dtpoll.Rows.Count > 0)
            {
                if ((dtpoll.Rows[0]["polllicencetype"].ToString().ToLower() == "free") || (dtpoll.Rows[0]["polllicencetype"].ToString().ToLower() == "publisher_freetrial"))
                {
                    pollslink.Visible = true;
                    polls.Visible = true;
                }
            }
            if (Request.RawUrl.ToLower().Contains("polls/"))
            {
                polls.Style.Add("margin-top", "0px");
            }
        }
        else {
            polls.Visible = false;
        }
        string upgradeprem = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userDetails.UserId + "&utm_source=" + strreferredby + "&FromPartner=Yes";
        UpgradeBuypremannual.HRef = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", upgradeprem);
        string upgradepolls = "UserId=" + userDetails.UserId + "&licensetype=Polls";
        pollslink.HRef = EncryptHelper.EncryptQuerystring("~/Polls/price.aspx", upgradepolls);
    }
}

