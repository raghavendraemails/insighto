﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using App_Code;
using System.Data;

namespace Insighto.UserControls
{
    public partial class ProfileRightPanel : UserControlBase
    {
        LoggedInUserInfo loggedInUserInfo;
        string countryName = "";
        string strsaasyrefno = "";
        SurveyCore surcore = new SurveyCore();
        protected void Page_Load(object sender, EventArgs e)
        {
            lnkMyProfile.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetMyProfileURL(), "surveyFlag=" + SurveyFlag);
            lnkInvoiceHisotry.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetInvoiceHistroyUrl(), "surveyFlag=" + SurveyFlag);
            lnkPollInvoiceHisotry.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetPollInvoiceHistroyUrl(), "surveyFlag=" + SurveyFlag);
            lnkChangePassword.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetUserChangePasswordPageURL(), "surveyFlag=" + SurveyFlag);
           // lnkManageEmail.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetManageLoginUrl(), "surveyFlag=" + SurveyFlag);
            lnkManageFolder.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetManageFoldersURL(), "surveyFlag=" + SurveyFlag);
            lnkManageCategory.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetManageCategoryUrl(), "surveyFlag=" + SurveyFlag);

           

            SessionStateService sessionStateService = new SessionStateService();
            loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            DataSet dssaasyrefno = surcore.getSaasyorderno(loggedInUserInfo.UserId);

            if (dssaasyrefno.Tables[0].Rows.Count > 0)
            {
                strsaasyrefno = dssaasyrefno.Tables[0].Rows[0]["SaasySubscriptionReference"].ToString();
            }

            if ((countryName == null)||(countryName == ""))
            {
                string ipAddress;
                if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
                }
                //ipAddress = "125.236.193.250";
                countryName = Utilities.GetCountryName(ipAddress);
                ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(countryName);
                ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
                countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
                //countryName = "singapore";
            }
            if ((countryName.ToUpper() == "INDIA")||(countryName.ToUpper() == ""))
            {
               
                Ulmanagepayments.Visible = false;
                dvmngpayments.Visible = false;
            }
            else
            {
                dvmngpayments.Visible = true;
                Ulmanagepayments.Visible = true;
                lnkupdatepayment.HRef =  strsaasyrefno;
                lnkcancelsubscription.HRef =  strsaasyrefno;
               
            }
           

        }
    }
}