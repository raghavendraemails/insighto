﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Linq;

namespace UserControls
{
    public partial class SurveyContactInfoControl : SurveyAnswerControlBase
    {

        private List<string> _value = new List<string>();
        public override List<string> Value
        {
            get
            {
                _value = new List<string> {

            ucFirstName.FormFieldText+"~"+ucFirstName.IsMandatory+"~"+"1",
            ucLastName.FormFieldText+"~"+ucLastName.IsMandatory+"~"+"2",
            ucAddress1.FormFieldText+"~"+ucAddress1.IsMandatory+"~"+"3",
            ucAddress2.FormFieldText+"~"+ucAddress2.IsMandatory+"~"+"4",
            ucCity.FormFieldText+"~"+ucCity.IsMandatory+"~"+"5",
            ucCountry.FormFieldText+"~"+ucCountry.IsMandatory+"~"+"6",
            ucHomePhone.FormFieldText+"~"+ucHomePhone.IsMandatory+"~"+"7", 
            ucWorkPhone.FormFieldText+"~"+ucWorkPhone.IsMandatory+"~"+"8",
            ucCellPhone.FormFieldText+"~"+ucCellPhone.IsMandatory+"~"+"9",
            ucEmailAddress.FormFieldText+"~"+ucEmailAddress.IsMandatory+"~"+"10"
                    
                };
                return _value;
            }
            set
            {
                _value = value;
                GetContactValues();
            }
        }

        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ucFirstName.Focus();

            }

            base.OnLoad(sender, e);
        }

        public override bool IsRequired
        {
            get;          
            set;           
        }

        /// <summary>
        /// The Group (Validation Group)
        /// </summary>
        public override string Group
        {
            get;
            set;
        }


        /// <summary>
        /// Setup the Control
        /// </summary>
        public override void Initialise()
        {

        }

        /// <summary>
        /// Binds the Data
        /// </summary>
        public override void Display()
        {

        }
        public override void Clear()
        {
            ucFirstName.Clear();
            ucLastName.Clear();
            ucAddress1.Clear();
            ucAddress2.Clear();
            ucCity.Clear();
            ucCountry.Clear();
            ucHomePhone.Clear();
            ucWorkPhone.Clear();
            ucCellPhone.Clear();
            ucEmailAddress.Clear();

            ucFirstName.FormFieldText = "First Name";
            ucLastName.FormFieldText = "Last Name";
            ucAddress1.FormFieldText = "Address 1";
            ucAddress2.FormFieldText = "Address 2";
            ucCity.FormFieldText = "City";
            ucCountry.FormFieldText = "Country";
            ucHomePhone.FormFieldText = "Home Phone";
            ucWorkPhone.FormFieldText = "Work Phone";
            ucCellPhone.FormFieldText = "Cell Phone";
            ucEmailAddress.FormFieldText = "Email Address";

            ucFirstName.Focus();
        }

        public void GetContactValues()
        {
            UnCheckIncluedcheckBox();

            for (int i = 0; i < _value.Count; i++)
            {
                    string demoGrapId = _value[i].Split('~')[2];
                    switch (demoGrapId)
                    {
                        case "1":
                            ucFirstName.FormFieldText = _value[i].Split('~')[0];
                            ucFirstName.IsMandatory = _value[i].Split('~')[1];
                            ucFirstName.UnCheckFields = true;
                            break;
                        case "2":
                            ucLastName.FormFieldText = _value[i].Split('~')[0];
                            ucLastName.IsMandatory = _value[i].Split('~')[1];
                            ucLastName.UnCheckFields = true;
                            break;
                        case "3":
                            ucAddress1.FormFieldText = _value[i].Split('~')[0];
                            ucAddress1.IsMandatory = _value[i].Split('~')[1];
                            ucAddress1.UnCheckFields = true;
                            break;
                        case "4":
                            ucAddress2.FormFieldText = _value[i].Split('~')[0];
                            ucAddress2.IsMandatory = _value[i].Split('~')[1];
                            ucAddress2.UnCheckFields = true;
                            break;
                        case "5":
                            ucCity.FormFieldText = _value[i].Split('~')[0];
                            ucCity.IsMandatory = _value[i].Split('~')[1];
                            ucCity.UnCheckFields = true;
                            break;
                        case "6":
                            ucCountry.FormFieldText = _value[i].Split('~')[0];
                            ucCountry.IsMandatory = _value[i].Split('~')[1];
                            ucCountry.UnCheckFields = true;
                            break;
                        case "7":
                            ucHomePhone.FormFieldText = _value[i].Split('~')[0];
                            ucHomePhone.IsMandatory = _value[i].Split('~')[1];
                            ucHomePhone.UnCheckFields = true;
                            break;
                        case "8":
                            ucWorkPhone.FormFieldText = _value[i].Split('~')[0];
                            ucWorkPhone.IsMandatory = _value[i].Split('~')[1];
                            ucWorkPhone.UnCheckFields = true;
                            break;
                        case "9":
                            ucCellPhone.FormFieldText = _value[i].Split('~')[0];
                            ucCellPhone.IsMandatory = _value[i].Split('~')[1];
                            ucCellPhone.UnCheckFields = true;
                            break;
                        case "10":
                            ucEmailAddress.FormFieldText = _value[i].Split('~')[0];
                            ucEmailAddress.IsMandatory = _value[i].Split('~')[1];
                            ucEmailAddress.UnCheckFields = true;
                            break;
                    }
                }
                      
        }

        public void UnCheckIncluedcheckBox()
        {
          ucFirstName.UnCheckFields = false;
          ucLastName.UnCheckFields = false;
          ucAddress1.UnCheckFields = false;
          ucAddress2.UnCheckFields = false;
          ucCity.UnCheckFields = false;
          ucCountry.UnCheckFields = false;
          ucHomePhone.UnCheckFields = false;
          ucWorkPhone.UnCheckFields = false;
          ucCellPhone.UnCheckFields = false;
          ucEmailAddress.UnCheckFields = false;        
        }

        public bool CheckMandatory()
        {

            bool firstName = ucFirstName.checkMandatory;
            bool secondName = ucLastName.checkMandatory;
            bool address1 = ucAddress1.checkMandatory;
            bool address2 =  ucAddress2.checkMandatory;
            bool city = ucCity.checkMandatory;
            bool country = ucCountry.checkMandatory;
            bool homePhone = ucHomePhone.checkMandatory;
            bool workHome = ucWorkPhone.checkMandatory;
            bool cellPhone = ucCellPhone.checkMandatory;
            bool emailAddress = ucEmailAddress.checkMandatory;

            if (firstName || secondName || address1 || address2 || city || country || homePhone || workHome || cellPhone || emailAddress)
            {
                return true;
            }
            else
                return false;
            //string secondName = ucLastName.IsMandatory;
            //return secondName;
        }

        public bool IsFieldsAreChecked()
        {
            bool firstName = ucFirstName.CheckIncludeFieldChcked;
            bool secondName = ucLastName.CheckIncludeFieldChcked;
            bool address1 = ucAddress1.CheckIncludeFieldChcked;
            bool address2 = ucAddress2.CheckIncludeFieldChcked;
            bool city = ucCity.CheckIncludeFieldChcked;
            bool country = ucCountry.CheckIncludeFieldChcked;
            bool homePhone = ucHomePhone.CheckIncludeFieldChcked;
            bool workHome = ucWorkPhone.CheckIncludeFieldChcked;
            bool cellPhone = ucCellPhone.CheckIncludeFieldChcked;
            bool emailAddress = ucEmailAddress.CheckIncludeFieldChcked;

            if (firstName || secondName || address1 || address2 || city || country || homePhone || workHome || cellPhone || emailAddress)
            {
                return true;
            }
            else
                return false;
        }

        public override bool GetStatusOfMandatory
        {
            
            get
            {
               bool mandatory;
               return mandatory = CheckMandatory();
            }
            
        }

        public override bool  GetStatusOfIncludedFields
        {
            get
            {
                bool includedFiled;
                return includedFiled = IsFieldsAreChecked();
            }
        }

        protected void custValidateUniqText(object source, ServerValidateEventArgs args)
        {
            List<string> uniqueCount = new List<string>();
            for (int i=0;i<Value.Count;i++)
            {
                if (Convert.ToString(Value[i]).Split('~')[0] != " ")
                    uniqueCount.Add(Value[i].Split('~')[0]);
            }
            args.IsValid = (uniqueCount.Count == uniqueCount.Distinct().Count());  // field is empty         
        }

    }
}