﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using Insighto.Business.Enumerations;
using System.Collections;
using App_Code;


namespace Insighto.UserControls
{
    public partial class SurveyCopyFromTemplate : SurveyAnswerControlBase
    {
        private List<string> _value = new List<string> { string.Empty };
        private string _Category = string.Empty;
        private string _DisplayMode = string.Empty;
        private string _Columns = string.Empty;
        public string DisplayMode
        { 
            get
            {
                return _DisplayMode;
            }
            set
            {
                _DisplayMode = value;
            }
            
        }
        public string Category
        {
            get
            {
                _Category = hdnCategory.Value;
                return _Category;                
            }
        }

        public UserType GetLicenceType
        {
            get
            {
                var serviceSession = ServiceFactory.GetService<SessionStateService>();
                var userDet = serviceSession.GetLoginUserDetailsSession();
                return GenericHelper.ToEnum<UserType>(userDet.LicenseType);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (_DisplayMode == "CopyExsistingSurvey")
            {
                lblChangeQeustionMode.Text = GetLocalResourceObject("stringCopyExistingSurvey").ToString() ;// "Copy from Existing Survey";
            }
            else
            {
                lblChangeQeustionMode.Text = GetLocalResourceObject("stringCopyInsighto").ToString() ; //"Copy from Insighto Template";
            }

            NavigatequestionariePage(); 
           
        }
        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
               
            }
            lblCategory.Text = hdnCategory.Value;
            base.OnLoad(sender, e);
        }

        public override void Initialise()
        {

        }
        /// <summary>
        /// Binds the Data
        /// </summary>
        public override void Display()
        {
            //if (_DisplayMode == "CopyFromTemplate")
            bindTreeView();

        }

        private void bindTreeView()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var surveyService = ServiceFactory.GetService<SurveyService>();
            var suvey = surveyService.GetSurveys();
            
            var osmsurvey = new List<osm_survey>();
            IEnumerable<IGrouping<string, string>> templateCategory;

            if (_DisplayMode == "CopyExsistingSurvey")
            {
                templateCategory = (from p in suvey
                                        orderby p.SURVEY_CATEGORY ascending
                                        group p.SURVEY_CATEGORY by p.SURVEY_CATEGORY into subcats
                                        select subcats);
            }
            else
            {
                templateCategory = (from p in suvey where p.USERID == -1
                                        orderby p.TEMPLATE_CATEGORY ascending
                                        group p.TEMPLATE_SUBCATEGORY by p.TEMPLATE_CATEGORY into subcats
                                        select subcats);
            }
           
            TreeNode parentNode = null;
            string reqCategory = "";
            if (Request.QueryString["key"] != null)
                reqCategory = Request.QueryString["key"].ToString();           
            foreach (var category in templateCategory)
            {
                parentNode = new TreeNode(category.Key, category.Key);
                if (hdnCategory.Value == "")
                    //if (_DisplayMode == "CopyExsistingSurvey")
                    //{
                        hdnCategory.Value = "All";
                    //}
                    //else
                    //{
                        //hdnCategory.Value = category.Key;
                    //}
                if (_DisplayMode != "CopyExsistingSurvey")
                {
                    var subcategory = surveyService.GetSurveysByCategory(category.Key);
                    var templateSubCat = (from p in subcategory
                                          orderby p.TEMPLATE_SUBCATEGORY 
                                          group p.TEMPLATE_SUBCATEGORY by p.TEMPLATE_SUBCATEGORY into subcats
                                          select subcats);

                    foreach (var sc in templateSubCat)
                    {
                        if (sc.Key != null)
                        {
                            TreeNode childNode = new TreeNode(sc.Key, sc.Key);
                            parentNode.ChildNodes.Add(childNode);
                        }

                    }
                    

                }            
                if (category.Key != null)
                {
                    parentNode.Collapse();
                    tvSurvey.Nodes.Add(parentNode);                   
                }
               
            }
            //if (_DisplayMode == "CopyExsistingSurvey")
            //{
                TreeNode topNode = new TreeNode("All", "All");
                tvSurvey.Nodes.AddAt(0, topNode);
            //}
            
        }
        protected void tvSurvey_SelectedNodeChanged(object sender, EventArgs e)
        {
                
               hdnCategory.Value = tvSurvey.SelectedNode.Value;
               lblCategory.Text = hdnCategory.Value;
               //tvSurvey.SelectedNodeStyle.CssClass ="active";  
        }

        protected void NavigatequestionariePage()
        {
            string surveyName = string.Empty;
            Hashtable ht1 = new Hashtable();
            ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
            if (ht1 != null)
            {

                if (ht1.Contains("surveyName"))
                {
                    surveyName = ht1["surveyName"].ToString();
                }

            }

            hlnkGoToQuestionarie.NavigateUrl = EncryptHelper.EncryptQuerystring("~/SurveyDrafts.aspx", "SurveyId=" + base.SurveyID + "&surveyName=" + surveyName + "&surveyFlag=" + base.SurveyFlag);
        }

        //protected void btnGoToQueationarie_Click(object sender, EventArgs e)
        //{
        //    string surveyName = string.Empty;
        //    Hashtable ht1 = new Hashtable();
        //    ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
        //    if (ht1 != null)
        //    {

        //        if (ht1.Contains("surveyName"))
        //        {
        //            surveyName = ht1["surveyName"].ToString();
        //        }

        //    }//Commented the code due to replace the Hyperlink in case button control.

        //    Response.Redirect(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + base.SurveyID + "&surveyName=" + surveyName + "&surveyFlag=" + base.SurveyFlag));
        //} 
}
    
}