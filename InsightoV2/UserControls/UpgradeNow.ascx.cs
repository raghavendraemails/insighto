﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;


namespace Insighto.UserControls
{
    public partial class UpgradeNow : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            if (userDetails.LicenseType == UserType.FREE.ToString())
            {
                divFreeUSer.Visible = true;
                divProUser.Visible = false;
                string strUpgrate = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), Constants.USERID + "=" + userDetails.UserId);
                Hyperupgradenow.NavigateUrl = strUpgrate;
            }
            else
            {
                divFreeUSer.Visible = false;
                divProUser.Visible = true;
                Hyperupgradenow.NavigateUrl = "#";
            }
        }
    }
}