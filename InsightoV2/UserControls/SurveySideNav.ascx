﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveySideNav.ascx.cs"
    Inherits="Insighto.UserControls.SurveySideNav" %>
<!-- content panel -->
<!-- survey created content panel -->
<ul class="menu" id="menu">
    <li><a href="#"><asp:Label ID="lblCreate" runat="server" Text="create" 
            meta:resourcekey="lblCreateResource1"></asp:Label></a>
        <ul class="innerMenu">
            <li><a id="lnkCreateQuestion" runat="server" rel="SurveyDrafts">
                <asp:Label ID="lblAddQuestions" runat="server" Text="Questions" 
                    meta:resourcekey="lblAddQuestionsResource1"></asp:Label></a></li>
            <li><a href="AddIntroduction.aspx?Key=<%= Key %>" rel="AddIntroduction">
                <asp:Label ID="lblAddIntroduction" runat="server" Text="Introduction" 
                    meta:resourcekey="lblAddIntroductionResource1"></asp:Label></a></li>
        </ul>
    </li>
    <li><a href="#">Design</a>
        <ul class="innerMenu">
            <li><a href="Theme.aspx?Key=<%= Key %>" rel="Theme"><span><asp:Label ID ="lblTheme" 
                    runat="server" Text="Theme" meta:resourcekey="lblThemeResource1"></asp:Label> &nbsp;<asp:Image
                ID="imgTheme" runat="server" ImageUrl="~/App_Themes/Classic/Images/icon_pro.png"
                Visible="False" meta:resourcekey="imgThemeResource1" /></span></a></li>
            <li><a href="Logo.aspx?Key=<%= Key %>" rel="Logo"><span><asp:Label ID ="lblLogo" 
                    runat="server" Text="Logo" meta:resourcekey="lblLogoResource1"></asp:Label> &nbsp;<asp:Image ID="imgLogo"
                runat="server" ImageUrl="~/App_Themes/Classic/Images/icon_pro.png" 
                    Visible="False" meta:resourcekey="imgLogoResource1" /></span></a></li>
            <li><a href="Font.aspx?Key=<%= Key %>" rel="Font"><span><asp:Label ID ="lblFonts" 
                    runat="server" Text="Fonts" meta:resourcekey="lblFontsResource1"></asp:Label> &nbsp;<asp:Image ID="imgFonts"
                runat="server" ImageUrl="~/App_Themes/Classic/Images/icon_pro.png" 
                    Visible="False" meta:resourcekey="imgFontsResource1" /></span></a></li>
            <li><a href="HeaderFooter.aspx?Key=<%= Key %>" rel="HeaderFooter"><span>
                <asp:Label ID ="lblHeader" runat="server" Text="Header" 
                    meta:resourcekey="lblHeaderResource1"></asp:Label> &amp;
                Footer &nbsp;<asp:Image ID="imgHeader" runat="server" ImageUrl="~/App_Themes/Classic/Images/icon_pro.png"
                    Visible="False" meta:resourcekey="imgHeaderResource1" /></span></a></li>
            <li><a href="Button.aspx?Key=<%= Key %>" rel="Button"><span>
                <asp:Label ID ="lblButton" runat="server" Text="Button" 
                    meta:resourcekey="lblButtonResource1"></asp:Label> &nbsp;<asp:Image
                ID="imgButton" runat="server" ImageUrl="~/App_Themes/Classic/Images/icon_pro.png"
                Visible="False" meta:resourcekey="imgButtonResource1" /></span></a></li>
            <li><a id="lnkProgressBar" runat="server" rel="ProgressBar"><span>
                <asp:Label ID ="lblProgressBar" runat="server" Text="Progress Bar" 
                    meta:resourcekey="lblProgressBarResource1"></asp:Label> &nbsp;<asp:Image
                ID="imgProgress" runat="server" ImageUrl="~/App_Themes/Classic/Images/icon_pro.png"
                Visible="False" meta:resourcekey="imgProgressResource1" />
            </span></a></li>
        </ul>
    </li>
    <li><a href="#">Launch</a>
        <ul class="innerMenu">
            <li id="liSurveyAlert" runat="server"><asp:HyperLink  rel="SurveyAlerts" ID="lnkSurveyAlerts"
                runat="server" meta:resourcekey="lblSurveyAlertsResource1"></asp:HyperLink></li>
            <li id="liSurveyMessages" runat="server"><asp:HyperLink  rel="SurveyMessages" id="lnkSurveyEndMessages"
                runat="server" meta:resourcekey="lblSurveyMessageResource1"></asp:HyperLink>
                </li>
            <li><a id="lnkUrlRedirect" runat="server" rel="URLRedirection"><span>
                <asp:Label ID="lblUrlRedirection" runat="server" Text="URL Redirection" 
                    meta:resourcekey="lblUrlRedirectionResource1"></asp:Label>&nbsp;<asp:Image
                ID="imgUrlredirection" runat="server" ImageUrl="~/App_Themes/Classic/Images/icon_prem.png"
                Visible="False" meta:resourcekey="imgUrlredirectionResource1" /></span></a></li>
            <li id="liLaunch" runat="server"><a id="lnkLaunchSurvey" runat="server" rel="LaunchSurvey">
               <asp:Label ID="lblLaunchSurvey" runat="server" Text="Launch Survey" 
                    meta:resourcekey="lblLaunchSurveyResource1"></asp:Label></a></li>
        </ul>
    </li>
</ul>
<script type="text/javascript">
</script>
