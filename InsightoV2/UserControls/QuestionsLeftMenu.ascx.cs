﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using System.Collections;
using FeatureTypes;
using App_Code;

namespace Insighto.UserControls
{
    public partial class QuestionsLeftMenu : SurveyAnswerControlBase
    {
        int iQID = 0;
        int questionID = 0;
        string mode = "";
        int questSeq;
        public int QuestId
        {
            get
            {
                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (ht.Contains("Mode"))
                {
                    string tempMode = Convert.ToString(ht["Mode"]);
                    if (tempMode == "Edit")
                    {
                        if (ht.Contains("QuestionId"))
                        {
                            questionID = ValidationHelper.GetInteger(ht["QuestionId"].ToString(), 0);
                        }
                    }
                }
                return questionID;
            }
        }



        public string Mode
        {
            get
            {
                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (ht.Contains("Mode"))
                {
                    mode = Convert.ToString(ht["Mode"]);

                }
                return string.IsNullOrEmpty(mode) ? "Add" : mode;
            }
        }

        public int QuestSeq
        {
            get
            {
                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (ht.Contains("QuestionSeq"))
                {
                    questSeq = ValidationHelper.GetInteger(ht["QuestionSeq"].ToString(), 0);

                }
                return questSeq;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                QuestionTypeService QTService = new QuestionTypeService();
                var lstQuestionType = QTService.GetQuestionType();
                var questiontypes = from qt in lstQuestionType
                                    orderby qt.QUESTION_SEQ ascending                                    
                                    group qt by qt.QUESTION_TYPE_DESC into qtg
                                    select new
                                    {
                                        QUESTION_TYPE_DESC = qtg.Key,                                       
                                        osm_questiontypes = qtg
                                    };


                lstQuestionTypes.DataSource = questiontypes;
                lstQuestionTypes.DataBind();

                
                if (Request.QueryString["ID"] != null)
                {
                    iQID = int.Parse(Request.QueryString["ID"].ToString());
                }
                
            }
            if (QuestionType == QuestionType.Image)
            {

            }
            GetImagePreviewPathByQuestype();

        }


        protected void lstQuestionTypesItems_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

           // if (e.Item.ItemType == ListViewItemType.DataItem)
           // {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            var questionTypeId = ((Insighto.Data.osm_questiontypes)(dataItem.DataItem)).QUESTION_TYPE_ID;
            var questSeq = ((Insighto.Data.osm_questiontypes)(dataItem.DataItem)).QUESTION_SEQ;
            var itemQuestionType = GenericHelper.ToEnum<QuestionType>(questionTypeId);

            string strQuerystring = "QuestionType=" + questionTypeId + "&QuestionId=" + QuestId + "&Mode=" + Mode + "&SurveyId=" + SurveyID + "&QuestionSeq=" + QuestSeq + "&surveyFlag=" + base.SurveyFlag;

            string strAddLink = EncryptHelper.EncryptQuerystring(PathHelper.GetAddNewQuestionURL(), strQuerystring);

            HyperLink QType = e.Item.FindControl("QType") as HyperLink;
            QType.NavigateUrl = strAddLink;

            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var featureService = ServiceFactory.GetService<FeatureService>();

            if (featureService.Find(licenceType, FeatureQuestionType.Q_IMAGE).Value == 1 || featureService.Find(licenceType, FeatureQuestionType.Q_Video).Value == 1)
            {
                var lblPro = e.Item.FindControl("lblpro") as Label;
                lblPro.CssClass = "";

            }
            else
            {
                var lblPro = e.Item.FindControl("lblpro") as Label;
                if (questionTypeId == 12)
                    if (!licenceType.ToString().Contains("PRO"))
                        lblPro.CssClass = "pro";

                if (questionTypeId == 19 || questionTypeId == 21)
                {
                    lblPro.CssClass = "premium";
                    lblPro.Attributes.Add("onclick", "javascript:OpenModalPopUP('" + EncryptHelper.EncryptQuerystring(PathHelper.GetUpgradeLicenseURL().Replace("~/", ""), "&Flag=yes&pre=pre&surveyFlag=" + base.SurveyFlag) + "' ,690, 450, 'yes');return false;");
                }
            }
            


            if (itemQuestionType == QuestionType)
                QType.CssClass = "current";

            //if (itemQuestionType == QuestionType.Video)
            //    QType.Visible = Utilities.ShowOrHideFeature(licenceType, (int)QuestionTypes.Video);
           // }
        }
        #region "Method : GetImagePreviewPath"
        /// <summary>
        /// 
        /// </summary>
        public void GetImagePreviewPathByQuestype()
        {

            imgQuestionType.ImageUrl = String.Format("~/images/PreviewImages/{0}.jpg", QuestionType.ToString()); ;
            string temp = QuestionType.ToString();
            if (temp == "0")
            {
                dvPreview.Visible = false;
            }
        }
        #endregion

    }
}