﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using System.Collections;
using CovalenseUtilities.Helpers;

public partial class UserControls_AddressBookSideNav : UserControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        var featureService = ServiceFactory.GetService<FeatureService>(); 
        var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
        var listFeatures = featureService.Find(GenericHelper.ToEnum<UserType>(userInfo.LicenseType));
        hlnkManageEmail.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetAddressBookURL(), "surveyFlag=" + SurveyFlag);
        string addUrl = EncryptHelper.EncryptQuerystring("~/CreateEmailList.aspx", "Control=AddEmail&surveyFlag=" + base.SurveyFlag);
        hlnkCreateEmail.NavigateUrl = addUrl;
            
        string copylistUrl = EncryptHelper.EncryptQuerystring("~/CreateEmailList.aspx", "Control=CopyList&surveyFlag=" + base.SurveyFlag);
        hlnkCopy.NavigateUrl = copylistUrl;         
       
        if (featureService.Find(licenceType, FeatureDeploymentType.EXCEL_UPLOAD).Value == 0 || featureService.Find(licenceType, FeatureDeploymentType.CSV_UPLOAD).Value == 0)
        {
            string uploadUrl = EncryptHelper.EncryptQuerystring("~/CreateEmailList.aspx", "Control=Upload&surveyFlag=" + base.SurveyFlag);
            hlnkUpload.NavigateUrl = uploadUrl;
            //impPro.Visible = true;
            liUpload.Attributes.Add("class", "proLinkHyper");
        }
        else
        {
            string uploadUrl = EncryptHelper.EncryptQuerystring("~/CreateEmailList.aspx", "Control=Upload&surveyFlag=" + base.SurveyFlag);
            hlnkUpload.NavigateUrl = uploadUrl;
            liUpload.Attributes.Remove("class");
           // impPro.Visible = false;
        }
               

       
       

    }
    public int SetFeatures(string strFeature)
    {
        int iVal = 0;
        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var userInfo = session.GetLoginUserDetailsSession();
        if (userInfo != null)
        {
            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(userInfo.LicenseType));

            if (userInfo.UserId > 0 && listFeatures != null)
            {
                foreach (var listFeat in listFeatures)
                {

                    if (listFeat.Feature.Contains(strFeature))
                    {
                        iVal = (int)listFeat.Value;
                    }
                }
            }
        }
        else
        {
            Response.Redirect(PathHelper.GetUserLoginPageURL());
        }
        return iVal;
    }
}