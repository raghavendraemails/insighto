﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;

namespace Insighto.UserControls
{
    public partial class ManageLogin : UserControlBase
    {
        private List<string> _value = new List<string> { string.Empty, string.Empty, string.Empty };
        private string validationGroup = "";
        private bool IsReq;
        public List<string> Value
        {
            get
            {
                _value = GetRepeaterValues();
                return _value;
            }
            set
            {
                _value = value;
            }
        }


        public override bool IsRequired
        {
            get
            {
                return IsReq;
            }
            set
            {
                IsReq = value;
            }
        }

        /// <summary>
        /// The Group (Validation Group)
        /// </summary>
        public override string Group
        {
            get
            {
                return validationGroup;
            }
            set
            {
                validationGroup = value;
            }
        }

        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
            base.OnLoad(sender, e);
        }

        /// <summary>
        /// Setup the Control
        /// </summary>
        public override void Initialise()
        {

        }
        /// <summary>
        /// Binds the Data
        /// </summary>
        public override void Display()
        {
            if (_value != null)
            {
                BindRepeater(_value);
            }
        }

        public override void Clear()
        {
            int i = 1;
            foreach (RepeaterItem item in addRemoveControls.Items)
            {

                ((TextBox)item.FindControl("txtEmail")).Text = "";



            }
        }


        private void BindRepeater(List<string> data1)
        {
            addRemoveControls.DataSource = data1;
            addRemoveControls.DataBind();
        }

        protected void addRemoveControls_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //base.SurveyPageBase.IsSurveyActive 
            Control divErr = Parent.FindControl("dvErrMsg");
            Control divSuccess = Parent.FindControl("dvSuccessMsg");
            divErr.Visible = false;
            divSuccess.Visible = false;
            _value.Clear();
            if (e.CommandName == "Add")
            {

                foreach (RepeaterItem item in addRemoveControls.Items)
                {
                    //getting the values of user entered fields
                    string title = ((TextBox)item.FindControl("txtEmail")).Text.Trim();
                    Button btnAdd = ((Button)item.FindControl("btnAddAnother"));
                    Button btnLoginMail = ((Button)item.FindControl("btnLoginMail"));
                   
                    _value.Add(title);
                    if (btnAdd == e.CommandSource) //the current row on which user click will removed
                    {
                        _value.Add(string.Empty);
                    }
                }
               
                BindRepeater(_value);
            }
            else if (e.CommandName == "Remove")
            {
                foreach (RepeaterItem item in addRemoveControls.Items)
                {
                    Button btnRemove = ((Button)item.FindControl("btnRemove"));
                    if (btnRemove != e.CommandSource) //the current row on which user click will removed
                    {
                        string title = ((TextBox)item.FindControl("txtEmail")).Text.Trim();
                        _value.Add(title);
                    }
                }
                BindRepeater(_value);
            }
            else if (e.CommandName == "ManageLogin")
            {
                if (((TextBox)e.Item.FindControl("txtEmail")).Text.Trim().Length > 0)
                {
                    string title = ((TextBox)e.Item.FindControl("txtEmail")).Text.Trim();
                    TextBox txtEmailId = (TextBox)Parent.FindControl("txtEmail");
                    string email = txtEmailId.Text.Trim();
                    var sessionState = ServiceFactory.GetService<SessionStateService>();
                    var loginUser = sessionState.GetLoginUserDetailsSession();
                    var userService = ServiceFactory.GetService<UsersService>();
                    if (title != "" && title != null)
                    {
                        var loginEmail = userService.ChangePrimaryEmail(title, loginUser.UserId);
                        if (loginEmail == email)
                        {
                        
                            Label lblErrMsg = (Label)Parent.FindControl("lblErrorMsg");
                            lblErrMsg.Text = "Please activate your account to make as primary email.";
                            divErr.Visible = true;
                            divSuccess.Visible = false;
                        }
                        else
                        {
                            //  e.Item.FindControl("dvErrMsg").Visible = false;
                            // e.Item.FindControl("dvSuccessMsg").Visible = true;
                            //   ((Label)e.Item.FindControl("lblSuccessMsg")).Text = "Please active your account to move.";
                           
                            Label lblSuccessMsg = (Label)Parent.FindControl("lblSuccessMsg");
                            lblSuccessMsg.Text = "The login email id has been updated! Please login  with your new email id and current password.";
                            divErr.Visible = false;
                            divSuccess.Visible = true;
                            txtEmailId.Text = loginEmail;
                            ((TextBox)e.Item.FindControl("txtEmail")).Text = email;
                        }
                    }
                }
              
            }
        }

        private List<string> GetRepeaterValues()
        {
            var list = new List<string>();
            Control divErr = Parent.FindControl("dvErrMsg");
            foreach (RepeaterItem item in addRemoveControls.Items)
            {
                string title = ((TextBox)item.FindControl("txtEmail")).Text.Trim();
                list.Add(title);
                //if (title != "" && divErr.Visible == false)
                //{

                //    ((TextBox)item.FindControl("txtEmail")).Enabled = false;
                //}
                //else
                    ((TextBox)item.FindControl("txtEmail")).Enabled = true;
            }

            return list;
        }

        protected void itemDataBoundRepeater_ItemDataBound(object source, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var reqAnsOption = e.Item.FindControl("reqAnsoption") as RequiredFieldValidator;
                if (reqAnsOption != null)
                {
                    reqAnsOption.ValidationGroup = validationGroup;
                    IsReq = true;
                    reqAnsOption.Enabled = IsReq;
                }

                var btnAdd = e.Item.FindControl("btnAddAnother") as Button;
                var btnRemove = e.Item.FindControl("btnRemove") as Button;
                var btnLoginEmail = e.Item.FindControl("btnLoginMail") as Button;

                //btnAdd.Visible = !base.SurveyPageBase.IsSurveyActive;
                // btnRemove.Visible = !base.SurveyPageBase.IsSurveyActive;
                string title = ((TextBox)e.Item.FindControl("txtEmail")).Text.Trim();
                if (title.Length < 1)
                {
                    btnLoginEmail.Enabled = false;
                }
                else
                {
                    btnLoginEmail.Enabled = true;
                }

                if (btnRemove != null && _value.Count == 1)
                {
                    btnRemove.Visible = false;
                    
                }

                int manageEmailCount = 2;
                if (btnAdd != null && _value.Count >= manageEmailCount)
                {
                    btnAdd.Visible = false;
                }
            }
        }

        protected void custValidateUniqText(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (Value.Count == Value.Select(v => v.ToLower()).Distinct().Count());  // field is empty  
        }
    }
}