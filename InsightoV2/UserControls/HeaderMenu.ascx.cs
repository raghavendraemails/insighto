﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using System.Collections;

namespace Insighto.UserControls
{
    public partial class HeaderMenu : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Hashtable ht = new Hashtable();
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            hlMyProfile.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetMyProfileURL(), "surveyFlag=" + SurveyFlag);
            if (Request.RawUrl.ToLower().Contains("polls/"))
            {
                lnkLogo.ImageUrl = "~/App_Themes/Classic/Images/insighto_logo_polls.png";
                lnkLogo.NavigateUrl = EncryptHelper.EncryptQuerystring("~/polls/Home.aspx", "Type=redirect");
            }
            else {
                lnkLogo.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Home.aspx", "Type=redirect");
            }

            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            }
            if (ht != null && ht.Contains("Type"))
            {

                hlFAQ.Visible = false;
                hlContact.Text = GetLocalResourceObject("stringContactUs").ToString(); //"Contact Us";
                hlContact.NavigateUrl = "~/In/ContactUs.aspx";
            }
            else if (loggedInUserInfo != null)
            {                
                if (loggedInUserInfo.UserId != null)
                {
                    hlFAQ.Visible = true;
                    hlContact.Text = GetLocalResourceObject("stringReportBug").ToString();    //"Report a Bug";
                    hlContact.Attributes.Add("rel", "framebox");
                    hlContact.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Feedback.aspx", "surveyFlag=" + SurveyFlag); ;
                }
                else
                {
                    hlFAQ.Visible = false;
                }
                string name = loggedInUserInfo.FirstName;// +" " + loggedInUserInfo.LastName;
                //if (name.Length > 25)
                //{
                //    lblLoginUser.Text = name.Substring(0, 25) + "...";
                //}
                //else
                    lblLoginUser.Text = name;
               // lnkLogo.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + SurveyFlag);

            }
            if (Request.RawUrl.ToLower().Contains("myprofile.aspx")
                                || Request.RawUrl.ToLower().Contains("invoicehistory.aspx")
                                || Request.RawUrl.ToLower().Contains("managelogin.aspx")
                                || Request.RawUrl.ToLower().Contains("changepassword.aspx")
                                || Request.RawUrl.ToLower().Contains("managefolders.aspx")
                                || Request.RawUrl.ToLower().Contains("managecategory.aspx"))
            {
                hlMyProfile.Style.Add("color","#FF9000");
            }
        }
        protected void lbtnLogOut_Click(object sender, EventArgs e)
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            sessionStateService.EndUserSession();
            HttpContext.Current.Session.Clear();

            Response.Redirect("~/Home.aspx");
        }

        public string LoginUserText
        {
            get
            {
                return lblLoginUser.Text;
            }
            set
            {
                lblLoginUser.Text = value;
            }
        }
       
    }

}