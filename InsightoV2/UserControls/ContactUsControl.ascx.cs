﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Collections;
using Insighto.Business.ValueObjects;
using App_Code;

public partial class In_UserControls_ContactUsControl : System.Web.UI.UserControl
{
    Hashtable ht = new Hashtable();
    int userId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAttentionvalues();
        }
        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0)
            {
                if (ht.Contains("Userid"))
                    userId = Convert.ToInt32(ht["Userid"]);
            }
        }
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        if (userId > 0)
        {
            var userDetails = ServiceFactory.GetService<UsersService>().GetUserDetails(userId).FirstOrDefault();
            if (userDetails != null)
            {
                txtName.Text = userDetails.FIRST_NAME;
                if (userDetails.LAST_NAME != null)
                    txtName.Text += " " + userDetails.LAST_NAME;
                txtMail.Text = userDetails.LOGIN_NAME;
                txtPhno.Text = userDetails.PHONE;
                ddlAttenValues.SelectedValue = ddlAttenValues.Items.FindByText("Report a bug").Value;
            }
        }
        if (loggedInUserInfo != null)
        {
            ddlAttenValues.SelectedValue = ddlAttenValues.Items.FindByText("Report a bug").Value;
        }
        else
        {
            ddlAttenValues.SelectedValue = ddlAttenValues.Items.FindByText("Feedback").Value;
        }
        lblErrMsg.Visible = false;
        dvErrMsg.Visible = false;
        lblSuccMsg.Visible = false;
        dvSuccessMsg.Visible = false;
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession() != null)
        {
            if (txtCaptcha.Text == ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession())
            {
                InsertContactDetails();
            }
            else
            {
                lblErrMsg.Text = GetLocalResourceObject("lblErrMsgResource1.Text").ToString();   //"Please enter the numbers displayed.";
                lblErrMsg.Visible = true;
                dvErrMsg.Visible = true;
                txtCaptcha.Focus();
            }
        }
        else
        {
            txtCaptcha.Focus();
            lblErrMsg.Text = GetLocalResourceObject("lblErrMsgResource1.Text").ToString() ; //"Please enter the numbers displayed.";
            lblErrMsg.Visible = true;
            dvErrMsg.Visible = true;
        }

    }

    #region "Method:InsertContactDetails"
    public void InsertContactDetails()
    {
        osm_contactusdetails osmContactUs = new osm_contactusdetails();
        osmContactUs.USERNAME = txtName.Text.Trim();
        osmContactUs.EMAILADDRESS = txtMail.Text.Trim();
        osmContactUs.PHONENO = txtPhno.Text.Trim();
        osmContactUs.MAILMESSAGE = txtMessage.Text.Trim();
        osmContactUs.ATTENTIONVALUE = ddlAttenValues.SelectedItem.Value;
        osmContactUs.Organisation = "";
        var contactusService = ServiceFactory.GetService<ContactUsService>();
        contactusService.InsertContactUs(osmContactUs);
        if (osmContactUs.CONTACTUSUSERID > 0)
        {
            var configService = ServiceFactory.GetService<ConfigService>();
            List<string> configValue = new List<string>();
            configValue.Add("ContactusEmailAddress");
            var configDet = configService.GetConfigurationValues(configValue);
            string subject = "UserName: " + osmContactUs.USERNAME + "- Issue Type: " + osmContactUs.ATTENTIONVALUE + "- EmailID: " + osmContactUs.EMAILADDRESS + "- Phone No: " + osmContactUs.PHONENO;
            Hashtable ht = new Hashtable();
            MailHelper.SendMailMessage(osmContactUs.EMAILADDRESS, configDet[0].CONFIG_VALUE, "", "", subject, osmContactUs.MAILMESSAGE);

            ClearControls();
        }
    }

    public void ClearControls()
    {
        lblSuccMsg.Text = GetLocalResourceObject("lblSuccMsgResource1.Text").ToString();//"Thank you for contacting us. We will get back to you shortly.";
        txtCaptcha.Text = "";
        txtMail.Text = "";
        txtMessage.Text = "";
        txtPhno.Text = "";
        txtName.Text = "";
        lblSuccMsg.Visible = true;
        dvSuccessMsg.Visible = true;
        ddlAttenValues.SelectedItem.Text = "Feedback";
    }
    #endregion

    #region "Method : BindCountry"
    /// <summary>
    /// Used to get country list and bind values to dropdownlist.
    /// </summary>

    private void BindAttentionvalues()
    {
        // object on osm_picklist class
        osm_picklist osmPickList = new osm_picklist();
        //Assign vaule category
        osmPickList.CATEGORY = Constants.Attentionvalues;
        // object on PickListService class
        PickListService pickListService = new PickListService();
        //inovke method GetPickList
        var vPickList = pickListService.GetPickList(osmPickList);

        if (vPickList.Count > 0)
        {
            //Bind list values to dropdownlist
            this.ddlAttenValues.DataSource = vPickList;
            this.ddlAttenValues.DataTextField = Constants.PARAMETER;
            this.ddlAttenValues.DataValueField = Constants.PARAMETER;
            this.ddlAttenValues.DataBind();
           // ddlAttenValues.Items.Insert(0, new ListItem("--Select--", ""));
        }

    }
    #endregion
}