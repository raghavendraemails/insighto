﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using App_Code;

public partial class ErrorSuccessNotifier : UserControl
{
    private const string KEY_NOTIFICATION_MESSAGES = "ErrorSuccessMessages";

    /// <summary>
    /// Adds the message.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    public static void AddMessage(NotificationMessage msg)
    {
        List<NotificationMessage> messages = NotificationMessages;
        if (messages == null)
        {
            messages = new List<NotificationMessage>();
        }
        messages.Add(msg);
        HttpContext.Current.Session[KEY_NOTIFICATION_MESSAGES] = messages;
    }

    /// <summary>
    /// Clears the messages.
    /// </summary>
    private static void ClearMessages()
    {
        HttpContext.Current.Session[KEY_NOTIFICATION_MESSAGES] = null;
    }

    /// <summary>
    /// Gets the notification messages.
    /// </summary>
    private static List<NotificationMessage> NotificationMessages
    {
        get
        {
            List<NotificationMessage> messages = (List<NotificationMessage>)
                HttpContext.Current.Session[KEY_NOTIFICATION_MESSAGES];
            return messages;
        }
    }

    /// <summary>
    /// Adds the info message.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    public static void AddInfoMessage(string msg)
    {
        AddMessage(new NotificationMessage()
        {
            Text = msg,
            Type = MessageType.Info,
            AutoHide = true
        });
    }

    /// <summary>
    /// Adds the success message.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    public static void AddSuccessMessage(string msg)
    {
        AddMessage(new NotificationMessage()
        {
            Text = msg,
            Type = MessageType.Success,
            AutoHide = true
        });
    }

    /// <summary>
    /// Adds the warning message.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    public static void AddWarningMessage(string msg)
    {
        AddMessage(new NotificationMessage()
        {
            Text = msg,
            Type = MessageType.Warning,
            AutoHide = false
        });
    }

    /// <summary>
    /// Adds the error message.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    public static void AddErrorMessage(string msg)
    {
        AddMessage(new NotificationMessage()
        {
            Text = msg,
            Type = MessageType.Error,
            AutoHide = false
        });
    }

    /// <summary>
    /// Handles the Prerender event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Prerender(object sender, EventArgs e)
    {
        if (NotificationMessages != null)
        {
            int index = 1;
            foreach (var msg in NotificationMessages)
            {
                Panel msgPanel = new Panel();
                msgPanel.CssClass = "PanelNotificationBox Panel" + msg.Type;
                if (msg.AutoHide)
                {
                    msgPanel.CssClass += " AutoHide";
                }
                msgPanel.ID = msg.Type + "Msg" + index;
                Literal msgLiteral = new Literal();
                msgLiteral.Mode = LiteralMode.Encode;
                msgLiteral.Text = msg.Text;
                msgPanel.Controls.Add(msgLiteral);
                this.Controls.Add(msgPanel);
                index++;
            }
            ClearMessages();

            IncludeTheCssAndJavaScript();
        }
    }

    private void IncludeTheCssAndJavaScript()
    {
        ClientScriptManager cs = Page.ClientScript;

        // Include the ErrorSuccessNotifier.js library (if not already included)
        string notifierScriptURL = this.TemplateSourceDirectory + "/Scripts/ErrorSuccessNotifier.js";
        if (!cs.IsStartupScriptRegistered(notifierScriptURL))
        {
            cs.RegisterClientScriptInclude(notifierScriptURL, notifierScriptURL);
        }

        // Include the ErrorSuccessNotifier.css stylesheet (if not already included)
        string cssRelativeURL = Utilities.GetAbsoluteUrl("~/Styles/ErrorSuccessNotifier.css", Page);
        if (!cs.IsClientScriptBlockRegistered(cssRelativeURL))
        {
            string cssLinkCode = string.Format(@"<link href='{0}' rel='stylesheet' type='text/css' />", cssRelativeURL);
            cs.RegisterClientScriptBlock(this.GetType(), cssRelativeURL, cssLinkCode);
        }
    }
}