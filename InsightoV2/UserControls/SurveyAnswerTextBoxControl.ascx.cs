﻿using System;
using System.Collections.Generic;
using System.Linq;
using Insighto.Business.Enumerations;

namespace Insighto.UserControls
{
    public partial class SurveyAnsTextControl : SurveyAnswerControlBase
    {

        public override List<string> Value
        {
            get
            {
                return new List<string> { txtNoofCharacters.Text.Trim() };
            }
            set
            {
                txtNoofCharacters.Text = value.FirstOrDefault();
            }

        }

        public override void OnLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
            base.OnLoad(sender, e);
        }

        /// <summary>
        /// Setup the Control
        /// </summary>
        public override void Initialise()
        {
            if (QuestionType == QuestionType.Text_CommentBox)
            {
                lblTextCount.Text = "Maximum number of characters allowed in respondent's answers (max 2000):";
                revtxtNoofCharacters.ValidationExpression = @"^[0-9]+$";
                revtxtNoofCharacters.ErrorMessage = "Please enter numerics only.";
            }
            else if (QuestionType == QuestionType.Text_SingleRowBox || QuestionType == QuestionType.Text_MultipleBoxes)
            {
                lblTextCount.Text = "Maximum number of characters allowed in respondent's answers (max 500):";
                revtxtNoofCharacters.ErrorMessage = "Please enter numerics only.";
                revtxtNoofCharacters.ValidationExpression = @"^[0-9]+$";
            }
            else if (QuestionType == QuestionType.ConstantSum)
            {
                lblTextCount.Text = "Please Enter Constant Sum value :";
                revtxtNoofCharacters.ErrorMessage = "Please enter numerics only.";
                revtxtNoofCharacters.ValidationExpression = @"^[0-9]+$";
            }
        }

        /// <summary>
        /// Binds the Data
        /// </summary>
        public override void Display()
        {

        }

        public override void Clear()
        {
            txtNoofCharacters.Text = "";
        }

        public void focus()
        {
            txtNoofCharacters.Focus();
        }
    }
}
