﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CopyContactList.ascx.cs" Inherits="Insighto.UserControls.CopyContactList" %>
<div  >
        <!-- popup content panel -->
         <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div> <asp:Label ID="lblSuccessMsg"   runat="server" Visible="False" 
                    Text="Email(s) added successfully." 
                    meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
         </div>
          <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">  
            
            <div>
                <asp:Label ID="lblErrMsg"  runat="server" Visible="False"   
                    Text="Email(s) already exist." meta:resourcekey="lblErrMsgResource1" ></asp:Label></div>
            </div>
        
            <!-- form -->
                <div class="defaultHeight"></div>
               <asp:Panel ID="pnlMultipleEmails" runat="server" 
            meta:resourcekey="pnlMultipleEmailsResource1">                        
                         <div class="informationPanelDefault" style="width:99%" id="dvInformationPanel" runat="server"   >
                           <%-- <div>
                            Paste or type multiple emails addresses into this field, separated by a comma,
                            in the same line and <b>without</b> any space. <br /><b>Example: </b>Tom@mycompany.com,John@mycompany.com
                            </div>--%>
                         </div>
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">                          
                            <tr>
                                <td valign="top">
                                 <asp:TextBox ID="txtMultipleEmails"  runat="server" CssClass="textAreaMedium" 
                                        TextMode="MultiLine" Rows="4"
                        Columns="200" Width="98%" meta:resourcekey="txtMultipleEmailsResource1"></asp:TextBox><br />
                       
                                </td>
                             
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblDuplicateEmails" runat="server" Visible="False" 
                                        meta:resourcekey="lblDuplicateEmailsResource1"></asp:Label>
                                 <asp:Label ID="lblDupTitle" runat="server" Visible="False" Font-Bold="True" 
                                        Text="Duplicate Emails:" meta:resourcekey="lblDupTitleResource1"></asp:Label><br />
                                 <asp:TextBox ID="txtDuplicateEmails" Visible="False" TextMode="MultiLine" Rows="4" 
                                        width="590px"  runat="server"  Enabled="False" 
                                        meta:resourcekey="txtDuplicateEmailsResource1"></asp:TextBox>   
                                </td>
                            </tr>
                            
                        </table>
                     <div class="clear"> </div>
                     <asp:Label ID="lblInvalidEmail" class="lblRequired"  runat="server" Visible="False"
                                        Text="Please enter email id's." 
                             meta:resourcekey="lblInvalidEmailResource1"></asp:Label>
                     <div id="divValidators" runat="server"></div>
                     <div id="divdupValidator" runat="server"></div>
                        <div class="clear"> </div>
                    <div class="defaultHeight"></div>
                    <asp:Button ID="btnSaveCopy" runat="server"   CssClass="btn_small_65" Text="Save"
                        ToolTip="Save" onclick="btnSaveCopy_Click" 
                             meta:resourcekey="btnSaveCopyResource1"  />                      
                    <!-- //text here -->
                    <div >
                        <asp:HiddenField ID="hdnError" runat="server" Value="List contains invalid email id's."/>
                        
                    </div>
                </asp:Panel>
           
            
            <!-- //form -->
       
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>
