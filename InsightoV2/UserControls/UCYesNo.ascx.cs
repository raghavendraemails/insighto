﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using InfoSoftGlobal;

public partial class UserControls_UCYesNo : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // exportHandler='" + exportchartfilehandler + "' exportAction='download' 
        object[,] arrData = new object[2, 2];
        arrData[0, 0] = "Yes"; arrData[1, 0] = "No"; 
        arrData[0, 1] = 70; arrData[1, 1] = 30; 
        StringBuilder xmlData = new StringBuilder();
        xmlData.Append("<chart caption='When I need help, the trainer is available for individual assistance.' numberSuffix='%' exportEnabled='1' canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' pieSliceDepth='30' showBorder='1' borderColor='6e1473' borderThickness='1'  formatNumberScale='0' bgColor='FFFFFF' showAboutMenuItem='0'>");
        for (int i = 0; i < arrData.GetLength(0); i++) { xmlData.AppendFormat("<set label='{0}' value='{1}' />", arrData[i, 0], arrData[i, 1]); }
        xmlData.Append("</chart>");
        Literal1.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/Pie3D.swf", "", xmlData.ToString(), "YesNO", "600", "300", false, true);
      
    }
}