﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveyCopyFromTemplate.ascx.cs"
    Inherits="Insighto.UserControls.SurveyCopyFromTemplate" %>
<div class="contentPanelHeader">
    <!-- form header panel -->
    <div class="pageTitle">
        <!-- page title -->
        <span>
            <asp:Label ID="lblChangeQeustionMode" runat="server" Text="CREATE QUESTION" meta:resourcekey="lblChangeQeustionModeResource1"></asp:Label></span>
        <!-- //page title -->
    </div>
    <!-- //form header panel -->
</div>
<div class="clear">
</div>
<div class="contentPanel">
    <!-- content panel -->
    <div class="surveyCreateTextPanel">
        <!-- survey created content panel -->
        <table width="100%">
        <tr align="center">
        <td width="28%">
        &nbsp;
        </td>
        <td>
        <div class="informationPanel">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <p class="fontSize">
                                                        <asp:Label ID="lblHelp" runat="server" Text="Choose your existing survey and click on copy icon."></asp:Label>
                                                            &nbsp;</p>
                                                    </td>
                                                    <td>
                                                        <span><a href="#" class="helpLink2" onclick="javascript: window.open('help/5_2.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;"
                                                            title="Help">&nbsp</a></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
        </td>
        </tr>
        </table>
        <div>&nbsp;</div>
        
        <div class="treeMenuLeftPanel">
            <!-- survey left menu -->
            <asp:TreeView ID="tvSurvey" runat="server" NodeWrap="True" OnSelectedNodeChanged="tvSurvey_SelectedNodeChanged"
                meta:resourcekey="tvSurveyResource1">
                <RootNodeStyle ChildNodesPadding="5px" />
                <LeafNodeStyle CssClass="leafNode" />
            </asp:TreeView>
            <!-- //survey left menu -->
        </div>
        <div class="surveyQuestionPanel">
            <!-- survey question panel -->
            <table width="100%">
            <tr>
            <td>
             <div id="dvCategory" runat="server" visible="true" class="lt_padding">
                <asp:Label ID="lblCategory" runat="server" class="questionHeaderName" meta:resourcekey="lblCategoryResource1"></asp:Label>
            </div>
            </td>
            <td>
            <div id="divgotoquestionarie">
            <p align="right" style=" padding:0px 30px 0px 0px">
                <%-- <asp:Button ID = "btnGoToQueationarie" runat  = "server" CssClass = "dynamicButton" 
                Text = "Go to Questionnaire" onclick="btnGoToQueationarie_Click" 
                    meta:resourcekey="btnGoToQueationarieResource1" />--%>

           <asp:HyperLink ID="hlnkGoToQuestionarie" CssClass="dynamicHyperLinkGray" runat="server"  
                        meta:resourcekey="btnGoToQueationarieResource1"></asp:HyperLink>
            </p>
        </div>
            </td>
            </tr>
            </table>
           
            <div>
                <!-- text here -->
                <div class="surveysGridPanel">
                    <div>
                        <div id="ptoolbar">
                        </div>
                        <table id="tblAddressBook">
                        </table>
                        <asp:HiddenField ID="hdnSurveyId" runat="server" />
                        <asp:HiddenField ID="hdnCategory" runat="server" />
                    </div>
                </div>
                <!-- //text here -->
            </div>
            <!-- //survey question panel -->
        </div>
        <div class="clear">
        </div>
        <!-- //survey created content panel -->
    </div>
    <div class="clear">
    </div>
    <div id="modal" style="border: 1px solid #ccc; width: 400px; height: 180px; background-color: #fff;
        display: none;">
        <table cellpadding="0" cellspacing="0" width="94%">
            <tr style="background: #F8F8F8; height: 40px">
                <td>
                    <div class="popupTitle">
                        <h3>
                            <asp:Label ID="lblCopyFromTemplate" runat="server" Text="Copy From Template" meta:resourcekey="lblCopyFromTemplateResource1"></asp:Label>
                        </h3>
                    </div>
                </td>
                <td valign="top">
                   <%-- <div class="popupClosePanel">
                        <a href="#" class="popupCloseLink" title="Close" onclick="Popup.hide('modal')"></a>
                    </div>--%>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="lt_padding">
                        <asp:Label ID="lblComptext" runat="server" Text="Copying this template will enable you copy only those questions that are entitled for Free membership."
                            meta:resourcekey="lblComptextResource1"></asp:Label>
                        <br />
                        <asp:Label ID="lblComptext1" runat="server" Text="The number of questions that could be copied would also be limited as per this entitlement."
                            meta:resourcekey="lblComptext1Resource1"></asp:Label></div>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td colspan="2" class="lt_padding">
                    <table align="right">
                        <tr>
                            <td>
                                <input id="btnOk" type="button" value="Ok" class="dynamicButton" onclick="Popup.hide('modal');" />
                                <asp:HiddenField ID="hfSkipUrl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="divLoading" style="width: 400px; padding: 20px; display: none; text-align: center">
        <span>
            <img src="../App_Themes/Classic/Images/ajax-loader.gif" alt="Loading..." /></span>
    </div>
    <!-- //content panel -->
</div>
<asp:Button ID="btnSubmit" runat="server" Visible="False" meta:resourcekey="btnSubmitResource1" />
<script src="../Scripts/popup.js" language="javascript" type="text/javascript"></script>
<script type="text/javascript">   
      
        var serviceUrl = 'AjaxService.aspx';
        var categoryName = '<%= Category %>';
        var displayMode = '<%= DisplayMode %>';
      
        var surveyId = <%= SurveyID %>;
    
        $(document).ready(function () {

            $("#tblAddressBook").jqGrid({
                url: serviceUrl,
                postData: { method: "GetSuveysByCategory", category : categoryName , newSurveyID : surveyId, mode : displayMode },
                datatype: 'json',
                colNames: ['Surveyname','Status','Preview','Copy'],
                colModel: [
                { name: 'SURVEY_NAME', index: 'SURVEY_NAME', width: 75, align:'left',resizable: false },
                { name: 'STATUS', index: 'STATUS', width: 25, align:'left',resizable: false },
                { name: 'PreviewLinkUrl', index: 'PreviewLinkUrl', width: 20, align:'center', classes: 'nohand', formatter: PreviewLinkFormatter, sortable:false,resizable: false}, 
                { name: 'SURVEY_ID', index: 'SURVEY_ID', width: 15, align:'center', classes: 'nohand', formatter: CopyLinkFormatter, sortable:false,resizable: false }, 

                 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'SURVEY_NAME',
                sortorder: "asc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 630,
                caption: '',
                height: '100%',         
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblAddressBook").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false,refresh:false });
            $("#tblAddressBook").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');
            if(displayMode!="CopyExsistingSurvey")
            {             
            $("#tblAddressBook").jqGrid('setGridParam', { url: serviceUrl , page: 1 }).hideCol("STATUS").trigger("reloadGrid");
            }
            var myGrid = $("#tblAddressBook");
            var cm = myGrid[0].p.colModel;
            $.each(myGrid[0].grid.headers, function(index, value) {
                var cmi = cm[index], colName = cmi.name;
                if(!cmi.sortable) {
                    $('div.ui-jqgrid-sortable',value.el).css({cursor:"default"});
                  }
            });
         });

        function PreviewLinkFormatter(cellvalue, options, rowObject) {  
        return "<a href='" + cellvalue + "'  rel='framebox' h='480' w='850' scrolling='yes' ><img src='App_Themes/Classic/Images/icon-tv-active.gif' title='Preview'/></a>";             
              
        }
   
        function CopyLinkFormatter(cellvalue, options, rowObject) {     
        var SurveyUrls = cellvalue.split('~');
        
        if(SurveyUrls[2]!=undefined)
        {
        document.getElementById('divgotoquestionarie').style.visibility = "hidden";
        return "<a href='" + SurveyUrls[2]+"&tempsurveyid="+SurveyUrls[0]+ "'  rel='framebox' h='480' w='850' scrolling='yes' ><img src='App_Themes/Classic/Images/icon-copy.gif' title='Copy' /></a>";
          
        }
        else
        {
         return "<a href='#' class='button-small-gap' id='lnkDelete' onclick='javascript: return CopySurvey(" + SurveyUrls[0] + ");'><img src='App_Themes/Classic/Images/icon-copy.gif' title='Copy' /></a>"; 
        }     
                                    
        }        

        function CopySurvey(copySuveyId) {
   Popup.showModal('divLoading', null, null, { 'screenColor': '#000000', 'screenOpacity': .6 });
                $.post("AjaxService.aspx", { "method": "CopySurveyFromTemplate", "copySurveyId": copySuveyId ,  "newSurveyId" : surveyId },
               function (result) {
               if (result !=null) {
               Popup.hide('divLoading');
               window.location.href = result;
               }
               else {

                   $('#lblMessage').attr('style', 'disply:none;');
               }
           });  
          
        }
//        function confiramtionModal()
//        {
//         Popup.showModal('modal', null, null, { 'screenColor': '#000000', 'screenOpacity': .6 });
//         return false;
//        }
       

        
</script>
