﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveyAnsYesNoControl.ascx.cs" Inherits="Insighto.UserControls.SurveyAnsYesNoControl" %>

<div>
<asp:Label ID="lbltxt" runat="server" Text="Answer for this question is automatically taken as displayed in preview."> </asp:Label>
</div> 
<div class="borderPanel" > 
<table cellpadding="0" cellspacing="0"><tr><td>Answer Alignment: </td><td> </td>
<td><asp:RadioButtonList ID ="rdAlignmentStyle" RepeatDirection="Horizontal" runat ="server">
<asp:ListItem Value ="Horizontal" Text="Horizontal" Selected="True"></asp:ListItem><asp:ListItem Value ="Vertical" Text="Vertical"></asp:ListItem>
</asp:RadioButtonList></td></tr></table>
</div>
<div class="clear"></div>