﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using InfoSoftGlobal;
using System.Configuration;
public partial class UserControls_MenuDD : System.Web.UI.UserControl
{
    public string exportchartfilehandler;  


    protected void Page_Load(object sender, EventArgs e)
    {
        //exportHandler='" + exportchartfilehandler + "' exportAction='download'
        exportchartfilehandler = ConfigurationSettings.AppSettings["exportchartfile"].ToString();


        object[,] arrData = new object[4, 2];
        arrData[0, 0] = "Internet Chatting"; arrData[1, 0] = "MObile Phone"; arrData[2, 0] = "Emails"; arrData[3, 0] = "Mails";
        arrData[0, 1] = 33; arrData[1, 1] = 40; arrData[2, 1] = 20; arrData[3, 1] = 7;
        StringBuilder xmlData = new StringBuilder();
        xmlData.Append("<chart caption='Which is your favourite mode of communication?' numberSuffix='%' exportEnabled='1' canvasBgColor='f7eed2' canvasbgAlpha='0' bgColor='FFFFFF' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1'   formatNumberScale='0' showAboutMenuItem='0'>");
        for (int i = 0; i < arrData.GetLength(0); i++) { xmlData.AppendFormat("<set label='{0}' value='{1}' />", arrData[i, 0], arrData[i, 1]); }
        xmlData.Append("</chart>");
        Literal1.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/Bar2D.swf", "", xmlData.ToString(), "MDD", "600", "300", false, true);
    }
}