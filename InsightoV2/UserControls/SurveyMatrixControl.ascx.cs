﻿using System;
using System.Collections.Generic;
using System.Linq;
using Insighto.Business.Enumerations;

namespace UserControls
{
    public partial class SurveyMatrixControl : SurveyAnswerControlBase
    {
        private List<string> _value = new List<string>();
        public override bool IsRequired
        {
            get
            {
                return SurveyMatrixHeadingAnswerControl.IsRequired;
            }
            set
            {
                SurveyMatrixHeadingAnswerControl.IsRequired = value;
                ucSurveyMatrixTopAnswerQuestion.IsRequired = value;
                SurveyMatrixLeftAnswerControl.IsRequired = value;
            }
        }

        /// <summary>
        /// The Group (Validation Group)
        /// </summary>
        public override string Group
        {
            get
            {
                return SurveyMatrixHeadingAnswerControl.Group;
            }
            set
            {
                SurveyMatrixHeadingAnswerControl.Group = value;
                ucSurveyMatrixTopAnswerQuestion.Group = value;
                SurveyMatrixLeftAnswerControl.Group = value;
            }
        }
        public override List<string> Value
        {
            get
            {
                return GetMatrixAnswers();
            }

            set
            {
                _value = value;
                 SetMatrixAnswers();
            }
        }

        public override void Initialise()
        {
           
        }

        public override void Display()
        {
            
        }

        public override void Clear()
        {
            SurveyMatrixHeadingAnswerControl.Clear();
            ucSurveyMatrixTopAnswerQuestion.Clear();
            SurveyMatrixLeftAnswerControl.Clear();
        }

        public List<string> GetMatrixAnswers()
        {
            var dimlist = new List<string>();
            var list = (from rowVal in ucSurveyMatrixTopAnswerQuestion.Value
                        from colVal in SurveyMatrixLeftAnswerControl.Value
                        select rowVal + "$--$" + colVal).ToList();
            if (SurveyMatrixHeadingAnswerControl.Visible)
            {
                dimlist.AddRange(from dimVal in SurveyMatrixHeadingAnswerControl.Value
                                 from matrixVal in list
                                 select dimVal + "$--$" + matrixVal);
                _value = dimlist;
            }
            else
                _value = list;

            return _value;
        }

        public void SetMatrixAnswers()
        {
            var delimiter = new[] { "$--$" };
            if (QuestionType == QuestionType.Matrix_SideBySide)
            {
                var tempMatrixSideBySideList = _value.Select(s => new
                {
                    Option1 = s.Split(delimiter, StringSplitOptions.RemoveEmptyEntries)[0],
                    Option2 = s.Split(delimiter, StringSplitOptions.RemoveEmptyEntries)[1],
                    Option3 = s.Split(delimiter, StringSplitOptions.RemoveEmptyEntries)[2]
                }
                                                     ).ToList();

                var option1 = tempMatrixSideBySideList.Select(s => s.Option1).Distinct().ToList();
                var option2 = tempMatrixSideBySideList.Select(s => s.Option2).Distinct().ToList();
                var option3 = tempMatrixSideBySideList.Select(s => s.Option3).Distinct().ToList();

                ucSurveyMatrixTopAnswerQuestion.Value = option2;                
                SurveyMatrixLeftAnswerControl.Value = option3;
                SurveyMatrixHeadingAnswerControl.Value = option1;
            }

            else
            {
                var tempList = _value.Select(s => new
                {
                    Option1 = s.Split(delimiter, StringSplitOptions.RemoveEmptyEntries)[0],
                    Option2 = s.Split(delimiter, StringSplitOptions.RemoveEmptyEntries)[1],
                    //  Option3= s.Split(delimiter, StringSplitOptions.RemoveEmptyEntries)[2]
                }
                                                      ).ToList();

                var option1 = tempList.Select(s => s.Option1).Distinct().ToList();
                var option2 = tempList.Select(s => s.Option2).Distinct().ToList();
                //var option3 = tempList.Select(s => s.Option3).Distinct().ToList();

                ucSurveyMatrixTopAnswerQuestion.Value = option1;
                //SurveyMatrixHeadingAnswerControl.Value = option1;
                SurveyMatrixLeftAnswerControl.Value = option2;
            }
            


        }

        protected override void OnLoad(EventArgs e)
        {
            if (IsPostBack) return;
            divSidebySide.Visible = QuestionType == QuestionType.Matrix_SideBySide;
        }
    }
}