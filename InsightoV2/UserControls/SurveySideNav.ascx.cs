﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using App_Code;
using System.Data;

namespace Insighto.UserControls
{
    public partial class SurveySideNav : UserControlBase
    {

        #region "Variables"

        Hashtable typeHt = new Hashtable();
        int surveyId = 0;

        SurveyCore surcore = new SurveyCore();

        #endregion

        // <summary>
        /// Gets the base page.
        /// </summary>
        public SurveyPageBase BasePage
        {
            get
            {
                return Page as SurveyPageBase;
            }
        }
        public string Key
        {
            get
            {
                return HttpUtility.UrlEncode(QueryStringHelper.GetString("Key"));
            }
        }

        public int SurveyFlag
        {
            get
            {
                var key = QueryStringHelper.GetString("key");
                if (String.IsNullOrEmpty(key))
                    return 0;
                Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
                return ValidationHelper.GetInteger(ht["surveyFlag"], 0);
            }

        }
        public int _surveyId
        {
            get
            {
                var key = QueryStringHelper.GetString("key");                
                if (String.IsNullOrEmpty(key))
                    return 0;
                Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
                return ValidationHelper.GetInteger(ht["SurveyId"], 0);
                
            }
           
        }
       
        #region "Event : Page_Load"
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                lnkSurveyAlerts.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyAlertsURL(), "SurveyId=" + _surveyId + "&surveyFlag=" + SurveyFlag);
                lnkSurveyEndMessages.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyMessagesURL(), "SurveyId=" + _surveyId + "&surveyFlag=" + SurveyFlag);
                lnkCreateQuestion.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetCreateSurveyURL(), "SurveyId=" + _surveyId + "&surveyFlag=" + SurveyFlag);
                if (SurveyFlag == 0)
                {
                    lnkSurveyAlerts.Text = "Survey / Alerts Scheduler";
                    lnkLaunchSurvey.InnerText = "Launch Survey";
                    lnkSurveyEndMessages.Text = "Survey End Messages";                   
                }
                else
                {
                    lnkSurveyAlerts.Text = "Widget / Alerts Scheduler";
                    lnkLaunchSurvey.InnerText = "Launch Widget";
                    lnkSurveyEndMessages.Text = "Widget End Messages";
                   
                }
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userDetails = sessionService.GetLoginUserDetailsSession();

                UserType licenceType = 0;
              
                DataSet dssurveytype = surcore.getSurveyType(_surveyId);
                if (dssurveytype.Tables[0].Rows.Count != 0)
                {
                    if (dssurveytype.Tables[0].Rows[0][1].ToString() != "")
                    {
                        licenceType = (UserType)Enum.Parse(typeof(UserType), dssurveytype.Tables[0].Rows[0][1].ToString());

                    }
                    else
                    {
                        licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
                    }

                }
                else
                {
                    licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
                }
                

               // var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
                var featureService = ServiceFactory.GetService<FeatureService>();
                imgTheme.Visible = false;
                if (featureService.Find(licenceType, FeaturePersonalisationType.CUSTOMIZE_FONT).Value == 1)
                {
                    imgFonts.Visible = false;
                }
                else
                {
                    imgFonts.Visible = false;
                  //  imgFonts.Visible = true;
                }
                if (featureService.Find(licenceType, FeaturePersonalisationType.HEADER).Value == 1)
                {
                    imgHeader.Visible = false;
                }
                else
                {
                    imgHeader.Visible = false;
                   // imgHeader.Visible = true;
                }
                if (featureService.Find(licenceType, FeaturePersonalisationType.SURVEY_LOGO).Value == 1)
                {
                    imgLogo.Visible = false;
                }
                else
                {
                  //  imgLogo.Visible = true;
                    imgLogo.Visible = false;
                }
                if (featureService.Find(licenceType, FeaturePersonalisationType.BUTTONSETTINGS).Value == 1)
                {
                    imgButton.Visible = false;
                   
                }
                else
                {
                    imgButton.Visible = false;
                    //imgButton.Visible = true;
                }
                if (featureService.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYLAUNCH).Value == 1 && featureService.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYCLOSE).Value == 1)
                {

                    liSurveyAlert.Attributes.Remove("class");
                    lnkSurveyAlerts.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyAlertsURL(), "SurveyId=" + BasePage.SurveyID + "&surveyFlag=" + SurveyFlag);
                }
                else
                {
                    liSurveyAlert.Attributes.Remove("class");
                    lnkSurveyAlerts.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyAlertsURL(), "SurveyId=" + BasePage.SurveyID + "&surveyFlag=" + SurveyFlag);
             
                    //liSurveyAlert.Attributes.Add("class", "proImgIcon");
                    //lnkSurveyAlerts.Attributes.Add("onclick", "javascript:OpenModalPopUP('" + EncryptHelper.EncryptQuerystring(PathHelper.GetUpgradeLicenseURL().Replace("~/", ""), "SurveyId=" + BasePage.SurveyID + "&Flag=alert&surveyFlag=" + base.SurveyFlag) + "' ,690, 450, 'yes');return false;");
                                       
                }

                if (licenceType!=UserType.FREE)
                {
                    liSurveyMessages.Attributes.Remove("class");
                    lnkSurveyEndMessages.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyMessagesURL(), "SurveyId=" + BasePage.SurveyID + "&surveyFlag=" + SurveyFlag);
                }
                else
                {
                    liSurveyMessages.Attributes.Remove("class");
                    lnkSurveyEndMessages.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyMessagesURL(), "SurveyId=" + BasePage.SurveyID + "&surveyFlag=" + SurveyFlag);
                    //liSurveyMessages.Attributes.Add("class", "proImgIcon1");
                    //lnkSurveyEndMessages.Attributes.Add("onclick", "javascript:OpenModalPopUP('" + EncryptHelper.EncryptQuerystring(PathHelper.GetUpgradeLicenseURL().Replace("~/", ""), "SurveyId=" + BasePage.SurveyID + "&Flag=messages&surveyFlag=" + base.SurveyFlag) + "' ,690, 450, 'yes');return false;");

                }


                if (featureService.Find(licenceType, FeatureDeploymentType.REDIRECTION_URL).Value == 1)
                {
                    imgUrlredirection.Visible = false;
                    lnkUrlRedirect.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetUrlRedirectionURL(), "SurveyId=" + BasePage.SurveyID + "&surveyFlag=" + SurveyFlag);
                }
                else
                {
                    imgUrlredirection.Visible = true;
                    lnkUrlRedirect.Attributes.Add("onclick", "javascript:OpenModalPopUP('" + EncryptHelper.EncryptQuerystring(PathHelper.GetUpgradeLicenseURL().Replace("~/", ""), "SurveyId=" + BasePage.SurveyID + "&Flag=yes&pre=pre&surveyFlag=" + base.SurveyFlag) + "' ,690, 450, 'yes');return false;");
                    lnkUrlRedirect.HRef = "#";
                    //lnkUrlRedirect.Attributes.Add("onclick","javascript:OpenModalPopUP('"+EncryptHelper.EncryptQuerystring(PathHelper.GetUpgradeLicenseURL().Replace("~/",""),"SurveyId="+BasePage.SurveyID+"&Flag=yes&surveyFlag=" + base.SurveyFlag)+"' ,690, 450, 'yes');return false;");
                    //lnkUrlRedirect.HRef = "#";
                }

                if (BasePage.IsSurveyActive && BasePage.SurveyBasicInfoView.EMAILLIST_TYPE == Constants.ExternalLaunch)
                {
                    //lnkLaunchSurvey.HRef = "#";
                    //lnkLaunchSurvey.Attributes.Add("onclick", "alert('You cannot access the Launch page as this survey is already launched and active.');return false;");
                    lnkLaunchSurvey.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetLaunchSurveyURL(), "SurveyId=" + BasePage.SurveyID + "&Flag=Active&surveyFlag=" + SurveyFlag);
                }
                else if (BasePage.IsSurveyActive && BasePage.SurveyBasicInfoView.EMAILLIST_TYPE == Constants.InternalLaunch) 
                {
                    lnkLaunchSurvey.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetLaunchSurveyURL(), "SurveyId=" + BasePage.SurveyID + "&Flag=Active&surveyFlag="+SurveyFlag);
                }

                else
                {
                    lnkLaunchSurvey.HRef = PathHelper.GetLaunchSurveyURL() + "?key=" + Key;
                }

                UserType userType = Utilities.GetUserType();
                if (userType == UserType.FREE)
                {
                    lnkProgressBar.Visible = true;
                    //imgProgress.Visible = true;
                   // lnkProgressBar.Attributes.Add("onclick", "javascript:OpenModalPopUP('" + EncryptHelper.EncryptQuerystring(PathHelper.GetUpgradeLicenseURL().Replace("~/", ""), "SurveyId=" + BasePage.SurveyID + "&surveyFlag=" + base.SurveyFlag) + "' ,690, 450, 'yes');return false;");
                   // lnkProgressBar.HRef = "#";
                   // lnkProgressBar.Visible = Utilities.ShowOrHideFeature(userType, (int)FeatureTypes.Personalization.Progress_Bar);
                    lnkProgressBar.HRef = string.Format("~/ProgressBar.aspx?Key={0}", Key);
              
                }
                else
                {
                    lnkProgressBar.Visible = Utilities.ShowOrHideFeature(userType, (int)FeatureTypes.Personalization.Progress_Bar);
                    lnkProgressBar.HRef = string.Format("~/ProgressBar.aspx?Key={0}", Key);
                }
            }
        }
        #endregion


    }
}
