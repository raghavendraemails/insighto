﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SurveyQuestionnaire.ascx.cs"
    Inherits="Insighto.UserControls.SurveyQuestionnaire" %>
<%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>
<%@ Register Src="~/UserControls/SurveyPrint.ascx" TagPrefix="uc" TagName="SurveyPrint" %>
<%@ Import Namespace="Insighto.Data" %>
<%@ Import Namespace="Insighto.Business.ValueObjects" %>
<%@ Import Namespace="CovalenseUtilities.Extensions" %>
<!-- survey question panel -->
<div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
    <div>
        <asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1" /></div>
</div>
<div class="successPanel" id="dvsuccess" runat="server" visible="false">
    <div>
        <asp:Label ID="lblSucess" runat="server" meta:resourcekey="lblSucessResource1" /></div>
</div>
<div class="surveyQuestionHeader">
    <div class="surveyQuestionTitle" style="width: 25%;">
        <asp:Label ID="lblTitle" runat="server" Text="Questionnaire" meta:resourcekey="lblTitleResource1"></asp:Label>
    </div>
    <div class="previewPanelQues">
        <span id="dvCopyQuestion" runat="server" visible="true"><span class="quesIconslinks mt_12">
            <asp:HyperLink ID="hlnkCopyFromTemplate" CssClass="icon-Template" runat="server"
                ToolTip="Copy questions from template" meta:resourcekey="hlnkCopyFromTemplateResource1">[hlnkCopyFromTemplate]</asp:HyperLink></span><span
                    class="quesIconslinks"><a href="#" class="helpLink2 surveyHelpIcon" title="Help"
                        onclick="javascript: window.open('https://www.insighto.com:/Faqcms/QA_CopyQuestionsTemplates.aspx?key=O1IiHYYZiuqe67HGRNzwF%2bkEhfgu87DXgnrRt5vR1FMtDEyUF%2fS%2bAEjzk88UGHsxBSwgJJGzG%2b%2bbAz4jN0wXsp39zDnBVoFxDe777smAeTUv62dpA1%2bqNY5mEhinnzCj','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;"></a></span>
            <span class="quesIconslinks mt_12">
                <asp:HyperLink ID="hlnkCopyFromSurvey" CssClass="icon-Copy" runat="server" ToolTip="Copy questions from existing survey"
                    meta:resourcekey="hlnkCopyFromSurveyResource1">[hlnkCopyFromSurvey]</asp:HyperLink></span>
            <span class="quesIconslinks"><a href="#" title="Help" class="helpLink2 surveyHelpIcon"
                onclick="javascript: window.open('https://www.insighto.com/Faqcms/QA_CopyQuestionsExistingSurvey.aspx?key=O1IiHYYZiuqe67HGRNzwF%2bkEhfgu87DXgnrRt5vR1FMtDEyUF%2fS%2bAPVodhhr%2bx3K9GmdItSiWB2UAFvW78MfCYrhEeUTqwHukPWq%2bTTdj6kgnpqgrzshhz4u7Mpt1YhkA9c%2f4YKtc9s%3d','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">
            </a></span></span><span class="quesIconslinks">
                <asp:HyperLink ID="hlnkSaveToWord" runat="server" CssClass="wordLink" Text="Save to word"
                    ToolTip="Save to word" meta:resourcekey="hlnkSaveToWordResource1"></asp:HyperLink></span>
        <span class="quesIconslinks">
            <uc:SurveyPrint ID="ucSurveyPrint" runat="server" />
        </span><span class="quesIconslinks">
            <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
        </span>
    </div>
    <div class="clear">
    </div>
</div>
<div class="surveyQuestionSubHeader rightAlign">
    <!-- sub header -->
    <table cellpadding="2" cellspacing="0" align="right">
        <tr>
            <td>
                <asp:CheckBox ID="chkExpandAllQuestions" runat="server" onclick="Javascript:ExpandAll();"
                    meta:resourcekey="chkExpandAllQuestionsResource1" />
                <asp:HiddenField ID="hfQuestionsCount" runat="server" />
            </td>
            <td class="v-middle">
                <asp:Label ID="lblExpandAll" runat="server" Text="Expand all questions" meta:resourcekey="lblExpandAllResource1"></asp:Label>
            </td>
            <td>
                <asp:CheckBox ID="chkQuestionMadatory" runat="server" AutoPostBack="True" OnCheckedChanged="chkQuestionMadatory_CheckedChanged"
                    meta:resourcekey="chkQuestionMadatoryResource1" />
            </td>
            <td class="v-middle">
                <asp:Label ID="lblMakeMandatory" runat="server" Text="Make every question mandatory"
                    meta:resourcekey="lblMakeMandatoryResource1"></asp:Label>
            </td>
            <td>
                <asp:CheckBox ID="chkPagebreak" runat="server" AutoPostBack="True" OnCheckedChanged="chkPagebreak_CheckedChanged"
                    meta:resourcekey="chkPagebreakResource1" />
            </td>
            <td class="v-middle">
                <asp:Label ID="lblPageBreak" runat="server" Text="Page break to  all questions" meta:resourcekey="lblPageBreakResource1"></asp:Label>
            </td>
            <td>
                <a href="#" title="Help" onclick="javascript: window.open('help/2_2_14.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;"
                    class="helpLink2">&nbsp;</a>
            </td>
        </tr>
    </table>
    <!-- //sub header -->
</div>
<div class="surveyQuestionTabPanel" style="display: block">
    <!-- survey question tabs -->
</div>
<div id="mainscroll">
    <asp:ListView ID="lstQuestionTypes" runat="server" GroupItemCount="5" ItemPlaceholderID="itemPlaceHolder"
        GroupPlaceholderID="groupPlaceHolder">
        <GroupTemplate>
            <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
        </GroupTemplate>
        <LayoutTemplate>
            <asp:PlaceHolder ID="groupPlaceholder" runat="server" />
        </LayoutTemplate>
        <ItemTemplate>
            <div class="surveyQuestionTabPanel" id="pnlQuestionnaire">
                <div class="surveypage_lbl">
                    Page
                    <%# Eval("PageIndex")%></div>
                <asp:ListView ID="lstQuestionTypesItems" runat="server" DataSource='<%# Eval("surveyQuestions") %>'
                    OnItemDataBound="lstQuestionTypesItems_ItemDataBound" OnItemCommand="lstQuestionTypesItems_ItemCommand">
                    <ItemTemplate>
                        <div class="colapsPanel">
                            <!-- tab main -->
                            <div class="colapsGridRow">
                                <!-- tab header text-->
                                <div style="float: left;">
                                    <a href="javascript:void(0);" onclick="showHide('<%# "divQuestionDetails" + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_SEQ.ToString() %>','<%# "spanExpand" + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_SEQ.ToString() %>')">
                                        <span id='<%# "spanExpand" + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_SEQ.ToString() %>'
                                            style="font-size: 10px; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: normal;
                                            color: #666666">[+]</span></a>
                                    <asp:Label ID="lblSequence" runat="server"></asp:Label>.
                                    <%--<%# ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_SEQ %>--%>
                                    <%# ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUSETION_LABEL.Replace("&nbsp;", "").StripTagsCharArray().GetBlurb(55)%>
                                </div>
                                <div class="surveyQuetionIcons">
                                    <span class="quesIconslinks">
                                        <asp:HyperLink ID="hlnkEdit" runat="server" rel="framebox" h="540" w="950" scrolling="yes"
                                            CssClass="icon_select" ToolTip="Edit Question" meta:resourcekey="hlnkEditResource1"></asp:HyperLink></span>
                                    <span class="quesIconslinks">
                                        <asp:HyperLink ID="hlnkCopy" runat="server" rel="framebox" h="400" w="700" scrolling="yes"
                                            CssClass="icon-copy" ToolTip="Copy Question" meta:resourcekey="hlnkCopyResource1"></asp:HyperLink></span>
                                    <span class="quesIconslinks">
                                        <asp:HyperLink ID="hlnkAdd" runat="server" rel="framebox" h="540" w="950" scrolling="yes"
                                            CssClass="icon-plus" ToolTip="Add Question" meta:resourcekey="hlnkAddResource1"></asp:HyperLink></span>
                                    <span class="quesIconslinks">
                                        <asp:LinkButton ID="lbtnDelete" runat="server" ToolTip="Delete question" CommandName="Delete1"
                                            CssClass="icon-delete" CommandArgument='<%# ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_ID %>'
                                            meta:resourcekey="lbtnDeleteResource1"></asp:LinkButton></span> <span class="quesIconslinks">
                                                <asp:LinkButton ID="lbtnPageBreak" runat="server" CssClass="icon-pageBreak-blue"
                                                    CommandName="PageBreak" CommandArgument='<%# ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.SURVEY_ID + "," + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_ID + "," + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.PAGE_BREAK + "," + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.SKIP_LOGIC + "," + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_SEQ %>'
                                                    meta:resourcekey="lbtnPageBreakResource1"></asp:LinkButton>
                                            </span><span class="quesIconslinks">
                                                <asp:HyperLink ID="hlnkSkipLogic" CssClass="icon-skipLogic-blue confirm" runat="server"
                                                    h="490" w="700" scrolling="yes" meta:resourcekey="hlnkSkipLogicResource1">
                                                    <asp:Image ID="imgSkipLogic" runat="server" Visible="False" meta:resourcekey="imgSkipLogicResource1" />
                                                </asp:HyperLink>
                                            </span><span class="quesIconslinks">
                                                <asp:LinkButton ID="lbtnRequired" CssClass="icon-response-blue" runat="server" CommandName="Required"
                                                    CommandArgument='<%# ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.SURVEY_ID + "," + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_ID + "," + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.RESPONSE_REQUIRED + "," + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.SKIP_LOGIC + "," + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_TYPE_ID + "," + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_SEQ %>'
                                                    meta:resourcekey="lbtnRequiredResource1">
                                                    <asp:Image ID="imgRequired" runat="server" Visible="False" meta:resourcekey="imgRequiredResource1" />
                                                </asp:LinkButton>
                                            </span><span class="quesIconslinks">
                                                <asp:HyperLink ID="hlnkReOrder" CssClass="icon-record" runat="server" rel="framebox"
                                                    h="450" w="700" ToolTip="Reorder questions" meta:resourcekey="hlnkReOrderResource1"></asp:HyperLink></span>
                                </div>
                            </div>
                            <div style="clear: both;">
                            </div>
                            <div class="GridBottomRow" id='<%# "divQuestionDetails" + ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUESTION_SEQ.ToString() %>'
                                style="display: none; overflow: auto;">
                                <div class="QuestionnaireHeading">
                                    <%# ((SurveyQuestionAndAnswerOptions)Container.DataItem).Question.QUSETION_LABEL %>
                                </div>
                                <div id="divQuesAnswers" runat="server" style="padding-left: 18px;">
                                </div>
                                <div style="padding-left: 18px;">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblOther" runat="server" Text="Other" Visible="False" meta:resourcekey="lblOtherResource1"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtOther" runat="server" Visible="False" meta:resourcekey="txtOtherResource1"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ItemTemplate>
    </asp:ListView>
    <div id="modal" style="border: 0px solid #999; width: 400px; height: 160px; background-color: #fff;
        -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; display: none;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <div class="reminder_title" style="padding-left: 5px;">
                        <asp:Label ID="lblSkipConfirmation" runat="server" Text="Skip Confirmation" meta:resourcekey="lblSkipConfirmationResource1"></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td>
                    <div class="lt_padding" style="padding-right: 5px;">
                        <asp:Label ID="lblHelpText" runat="server" Text="Insighto recommends applying skip logic only after finalizing all your questions and their order - as additions/deletions of questions would hamper the logic.
                           Else, you may have to remove this skip logic to reorder / add / edit / delete any question." 
                            meta:resourcekey="lblHelpTextResource1"></asp:Label>

                    </div>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td class="lt_padding" style="padding-right: 30px;">
                    <table align="right">
                        <tr>
                            <td>
                                <input id="btnYes" type="button" value="Yes" class="reminderbtn" onclick="Popup.hide('modal');openskipmodel();" />
                                <asp:HiddenField ID="hfSkipUrl" runat="server" />
                            </td>
                            <td>
                                <input id="btnNo" type="button" value="No" class="reminderbtn" onclick="Popup.hide('modal');hidemodel();" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="divEditSkip" style="border: solid 0 #999; width: 400px; height: 120px; background-color: #fff;
        -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; display: none;">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td class="reminder_title" style="padding-left: 5px;">
                    <asp:Label ID="lblSkipLogic" runat="server" Text=" SKIP LOGIC" meta:resourcekey="lblSkipLogicResource1"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td align="left" style="padding-left: 5px;">
                    <asp:Label ID="lblHelpSkip" runat="server" Text="Edit is not possible because Skip Logic is applied for this question. Remove Skip Logic in Questionnaire page by clicking on 'Remove' link."
                        meta:resourcekey="lblHelpSkipResource1"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td align="right" style="padding-right: 30px;">
                    <table align="right">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <input id="btnOk" type="button" value="OK" title="OK" class="reminderbtn" onclick="Popup.hide('divEditSkip');" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
<%--<script type = "text/javascript"src="../Scripts/swfobject.js"></script>--%>
<script src="../Scripts/popup.js" language="javascript" type="text/javascript"></script>
<script language="javascript" type="text/javascript">

    function hidemodel() {
        document.getElementById('<%= hfSkipUrl.ClientID %>').value = "";
    }

    function openskipmodel() {
        var link2 = document.getElementById('<%= hfSkipUrl.ClientID %>').value;
        //alert(link2);
        document.getElementById('<%= hfSkipUrl.ClientID %>').value = "";
        OpenModalPopUP(link2, '700', '490', 'yes');
        return false;
    }


    // navigation switcher (requires jquery)



    $('a[rel*=confirmSkipLogic1]').click(function (e) {
        e.preventDefault();
        var link1 = $(this);
        //alert(link1);
        document.getElementById('<%= hfSkipUrl.ClientID %>').value = link1;
        Popup.showModal('modal', null, null, { 'screenColor': '#000000', 'screenOpacity': .6 });
        return false;
    });

    function ApplySkipLogic(val) {
        document.getElementById('<%= hfSkipUrl.ClientID %>').value = val;
        Popup.showModal('modal', null, null, { 'screenColor': '#000000', 'screenOpacity': .6 });
        return false;
    }
    function ShowWarningForEdit() {
        Popup.showModal('divEditSkip', null, null, { 'screenColor': '#000000', 'screenOpacity': .6 });
        return false;
    }
    function deleteConfirmation(deleteItem) {
        var msg = confirm("Are you sure you want to delete this question?");
        if (msg)
        { }
        else {
            return false;
        }
    }

    function showHide(EL, PM) {

        var flag = document.getElementById('<%= chkExpandAllQuestions.ClientID %>').checked;
        document.getElementById('<%= chkExpandAllQuestions.ClientID %>').checked = false;
        CollapseAll(EL);
        ELpntr = document.getElementById(EL);
        if (flag == true) {
            document.getElementById(PM).innerHTML = "[-] ";
            ELpntr.style.display = 'block';
            ELpntr.scrollIntoView(true);
        }
        else {
            if (ELpntr.style.display == 'none') {
                document.getElementById(PM).innerHTML = "[-] ";
                ELpntr.style.display = 'block';
                ELpntr.scrollIntoView(true);
            }
            else {
                document.getElementById(PM).innerHTML = "[+]";
                ELpntr.style.display = 'none';
            }
        }

    }


    function CollapseAll(idcol) {
        var count = parseInt(document.getElementById('<%= hfQuestionsCount.ClientID %>').value);
        var i = 1;

        for (i = 1; i <= count; i++) {
            var PM = "spanExpand" + i.toString();
            var EL = "divQuestionDetails" + i.toString();
            ELpntr = document.getElementById(EL);
            if (idcol != EL) {
                if (document.getElementById('<%= chkExpandAllQuestions.ClientID %>').checked == true) {
                    if (ELpntr.style.display == 'none') {
                        document.getElementById(PM).innerHTML = "[-] ";
                        ELpntr.style.display = 'block';
                    }
                }
                else {

                    document.getElementById(PM).innerHTML = "[+]";
                    ELpntr.style.display = 'none';

                }
            }
        }
    }



    function ExpandAll() {
        var count = parseInt(document.getElementById('<%= hfQuestionsCount.ClientID %>').value);

        var i = 1;

        for (i = 1; i <= count; i++) {
            var PM = "spanExpand" + i.toString();
            var EL = "divQuestionDetails" + i.toString();
            ELpntr = document.getElementById(EL);
            if (document.getElementById('<%= chkExpandAllQuestions.ClientID %>').checked == true) {
                if (ELpntr.style.display == 'none') {
                    document.getElementById(PM).innerHTML = "[-] ";
                    ELpntr.style.display = 'block';
                }
            }
            else {
                document.getElementById(PM).innerHTML = "[+]";
                ELpntr.style.display = 'none';
            }
        }
    }


    $(document).ready(function () {
        var currentTime = new Date();
        $(".txtDate").datetimepicker({
            dateFormat: 'mm/dd/yy',
            changeMonth: true,
            changeYear: true,
            showOn: "button",
            buttonImage: "App_Themes/Classic/Images/icon-calander.gif",
            buttonImageOnly: true,
            buttonText: "Calendar",
            yearRange: '1904:2050'
        });
    });


</script>
