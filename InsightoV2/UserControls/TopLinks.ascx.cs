﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using System.Collections;

namespace Insighto.UserControls
{
    public partial class TopLinks : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var mode = ConfigurationManager.AppSettings["releaseMode"];
            var version = ConfigurationManager.AppSettings["versionNumber"];
            Hashtable ht = new Hashtable();

            divVersion.Visible = (mode != "Release");
            ltlVersion.Text = mode + " - " + version;

            var showVersionDetails = ConfigurationManager.AppSettings["ShowVersionDetails"];
            ltlVersion.Visible = Convert.ToInt32(showVersionDetails) > 0;
            if (!IsPostBack)
            {

                var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                }
                if (ht != null && ht.Contains("Type"))
                {

                    hlFAQ.Visible = false;
                    hlContact.Text = GetLocalResourceObject("stringContactUs").ToString(); //"Contact Us";
                    hlContact.NavigateUrl = "~/In/ContactUs.aspx";
                }
                else if (loggedInUserInfo != null)
                {
                    hlPricing.Visible = false;
                    if (loggedInUserInfo.UserId != null)
                    {
                        hlFAQ.Visible = false;
                        hlContact.Text = GetLocalResourceObject("stringContactUs").ToString();    //"Report a Bug";
                        hlContact.Attributes.Add("rel", "framebox");
                        hlContact.Attributes.Add("h", "520");
                        hlContact.Attributes.Add("w", "700");
                        hlContact.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Feedback.aspx", "surveyFlag=" + SurveyFlag); ;
                    }
                    else
                    {
                        hlFAQ.Visible = false;
                    }
                }
                else
                {
                    hlPricing.Visible = false;
                    hlFAQ.Visible = false;
                    hlContact.Text = GetLocalResourceObject("stringContactUs").ToString(); //"Contact Us";
                    hlContact.NavigateUrl = "~/In/ContactUs.aspx";
                }
            }

        }
    }
}