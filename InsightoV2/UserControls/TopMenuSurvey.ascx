﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopMenuSurvey.ascx.cs"
    Inherits="Insighto.UserControls.TopMenuSurvey" %>
<script type="text/javascript" src="../Scripts/dropdowntabs.js"></script>
<link rel="stylesheet" type="text/css" href="../Styles/ddcolortabs.css" />
<div style="clear:both;"></div>
<div id="topMainMenu">
    <!-- top main menu -->
    <div id="topMainLeftLinksPoll">
        <!-- top left menu-->
        <ul id="nav">
            <li id="liMyAccount" runat="server">
                <asp:LinkButton ID="lnkMyAccount" CausesValidation="false" runat="server" onclick="lnkMyAccount_Click">Surveys</asp:LinkButton>
                </li>
                <li id="liMyPolls" runat="server">
               <asp:LinkButton ID="lnkMyPolls" runat="server" CausesValidation="false" onclick="lnkMyPolls_Click">Polls</asp:LinkButton>
               </li>
            <li id="liAddressBook" runat="server">
                <asp:HyperLink ID="lnkAddressBook" Text="Address Book"  runat="server" 
                    meta:resourcekey="lnkAddressBookResource1"></asp:HyperLink></li>
            <li id="liMyProfile" runat="server" visible="false">
                <asp:HyperLink ID="lnkMyProfile" Text="Manage Profile" runat="server" 
                    meta:resourcekey="lnkMyProfileResource1"></asp:HyperLink></li>
            <li id="liFolders" runat="server" visible="false">
                <asp:HyperLink ID="lnkFolders" runat="server" Text="Folders" 
                    meta:resourcekey="lnkFoldersResource1"></asp:HyperLink></li>
            <li id="litemplates" runat="server" >
                <asp:HyperLink ID="lnktemplates" runat="server" Text="Templates" 
                    ></asp:HyperLink></li>

        </ul>
        <!-- //top left menu-->
    </div>
    <div class="rightmenu">
        <div class="topMennuRightPanel" runat="server" id="topmenurightpanel">
        <!-- top menu right -->
        <div id="topMainLeftLinksPnlPoll" class="ddcolortabs">
            <ul id="rightNavnew">
                <li class="activelink"><img src="../App_Themes/Classic/Images/icon-arrow-down123.gif" style="float: left;margin-left: 10px;margin-top: 10px;"
                            alt="" />
                    <%--<asp:LinkButton ID="lnkCreatePoll" CausesValidation="false" runat="server" onclick="lnkCreatePoll_Click" >Create Poll</asp:LinkButton>--%>
                    <a href="/Polls/MyPolls.aspx">Create Poll</a>
                </li>
            </ul>
        </div>
        <script type="text/javascript">
        </script>
    </div>
        <div id="topMennuRightPanel">
        <!-- top menu right -->
        <div id="topMainLeftLinksPnl" class="ddcolortabs" runat="server">
            <ul id="rightNav">
                <li class="activelink">
                    <img src="../App_Themes/Classic/Images/icon-arrow-down123.gif" style="float: left;margin-left: 10px;margin-top: 10px;"
                            alt="" /><asp:LinkButton ID="lnkCreate" CausesValidation="false" runat="server" onclick="lnkCreate_Click">Create Survey</asp:LinkButton></li>
            </ul>
        </div>
        <script type="text/javascript">
            //SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
            //tabdropdown.init("topMainLeftLinksPnl", 3)
        </script>
        <!--1st drop down menu -->
        <!-- //top menu right -->
    </div>
    </div>
    <div class="clear">
    </div>
    <!--1st drop down menu -->
    <div id="dropmenu1" class="dropmenudiv">
        <a href="MyPoll.html">Create Poll</a> <a href="MyWidget.html">
           <asp:Label ID="lblCreateWidget" runat="server" Text="Create Widget" 
            meta:resourcekey="lblCreateWidgetResource1"></asp:Label></a>
    </div>
    <!-- //top menu right -->
</div>
<!-- top main menu
</div>  -->
<script type="text/javascript">
    // jquery popup 
    $(document).ready(function ($) {
        $('a[rel*=dropmenu1_a]').click(function (e) {
            e.preventDefault();
            ApplyFrameBox($(this));
        });
    });

    function ApplyFrameBox(link) {
        var frameWidth = $(link).attr('w');
        var frameHeight = $(link).attr('h');
        var scrolling = $(link).attr('scrolling');
        var href = $(link).attr('href');
        if (!scrolling) {
            scrolling = 'no';
        }

        $.facebox($('<iframe id="modalFrame" name="modalFrame" src="' + href + '" width="' + frameWidth + '" height="' + frameHeight + '" frameborder="0" scrolling="' + scrolling + '"></iframe>'));

        return false;
    }

</script>
