﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using System.ComponentModel;


namespace Insighto.UserControls
{
    public partial class SurveyQuestionControl : SurveyAnswerControlBase
    {
        [Bindable(true)]
        [DefaultValue(false)]
        public bool IsIntroductionPage { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (IsIntroductionPage)
            //{
            //    reqIntro.Visible = true;

            //}
            //else
            //{
            //    rqEditorQuestion.Visible = true;
            //}

            //if (SurveyID > 0)
            //{
            //    var surveySetting = ServiceFactory.GetService<SurveySetting>();
            //    var survey = surveySetting.GetSurveySettingsBySurveyId(SurveyID);
            //    string fontSize = survey[0].SURVEY_FONT_SIZE;
            //    hdnEditorQuesParams.Value = survey[0].SURVEY_FONT_TYPE + "_" + BuildQuestions.GetFontSizeValue(fontSize) + "_" + BuildQuestions.GetFontColor(survey[0].SURVEY_FONT_COLOR);
            //}

        }

        public override void Clear()
        {
            EditorQuestion.Value = string.Empty;
        }
        public string value
        {
            get
            {
                //return  EditorQuestion.Value.ToString();
                return GetQuestionText(EditorQuestion.Value);
            }

            set
            {
                EditorQuestion.Value = value;
            }

        }

        protected void custValidateText(object source, ServerValidateEventArgs args)
        {
            //string textLength = value.Length >40;
            if (value.Length > 4000)
                args.IsValid = false;
            else
                args.IsValid = true;// field is empty         
        }

        protected string GetQuestionText(string questionText)
        {
            string questionStr = string.Empty;
            string browserType = Request.Browser.Browser;
            if (browserType == "Firefox")
            {
                questionStr = BuildQuestions.HTMLTextSettings(questionText);
            }
            while (questionStr.Contains("<p>&nbsp;</p>\r\n"))
                questionStr = questionStr.Replace("<p>&nbsp;</p>\r\n", " ");
            while (questionStr.Contains("<p>&nbsp;</p>"))
                questionStr = questionStr.Replace("<p>&nbsp;</p>", " ");
            System.Text.RegularExpressions.Regex.Replace(questionStr, "<p>&nbsp;</p>", "");
            if (questionStr == "")
                return questionText;
            else
                return questionStr;
        }

        protected override void OnPreRender(EventArgs e)
        {
            EditorQuestion.Visible = true;
            if (IsIntroductionPage)
            {
              //  reqIntro.Visible = true;

            }
            else
            {
                rqEditorQuestion.Visible = true;
            }

            if (SurveyID > 0)
            {
                var surveySetting = ServiceFactory.GetService<SurveySetting>();
                var survey = surveySetting.GetSurveySettingsBySurveyId(SurveyID);
                string fontSize = survey[0].SURVEY_FONT_SIZE;
                hdnEditorQuesParams.Value = survey[0].SURVEY_FONT_TYPE + "_" + BuildQuestions.GetFontSizeValue(fontSize) + "_" + BuildQuestions.GetFontColor(survey[0].SURVEY_FONT_COLOR);
            }
        }
    }
}