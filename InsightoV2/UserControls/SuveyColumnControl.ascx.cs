﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Enumerations;
using Insighto.Data;
using Insighto.Business.Services;

namespace Insighto.UserControls
{
    public partial class SuveyColumnControl : SurveyAnswerControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialise();
                Display();
            }
            base.OnLoad(sender, e);
        }

        public override List<string> Value
        {
            get
            {
                return new List<string> { rdNoColumns.SelectedItem.Value };
            }
            set
            {
                rdNoColumns.SelectedValue = value.FirstOrDefault();
            }

        }

        public override void Display()
        {
            rdNoColumns.Items.Clear();
            var lstColumns = (Enum.GetValues(typeof(ColumnType)) as IEnumerable<ColumnType>).Select(
                    p => new ListItem { Text = Convert.ToString((int)p), Value = Convert.ToString((int)p) });

            rdNoColumns.DataSource = lstColumns;
            rdNoColumns.DataBind();
            rdNoColumns.SelectedValue = rdNoColumns.SelectedIndex > 0 ? rdNoColumns.SelectedValue : "1";
        }

        public override void Initialise()
        {

        }

        public string SetValuetoRadioListItem(string val)
        {
            val = val.Substring(0, 0);

            return val;
        }

        public override void Clear()
        {
            rdNoColumns.SelectedIndex = 0;
        }
    }
}
