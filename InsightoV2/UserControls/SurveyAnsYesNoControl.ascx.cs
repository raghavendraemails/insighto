﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;

namespace Insighto.UserControls
{
    public partial class SurveyAnsYesNoControl : SurveyAnswerControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

         public  override  List<string> Value
        {
            get
            {
                 return new List<string>{rdAlignmentStyle.SelectedItem.Value};
            }

            set 
            {
                rdAlignmentStyle.SelectedValue  = value.FirstOrDefault();
            }


        }

         //public override string Clear
         //{
         //    get
         //    {
         //        return rdAlignmentStyle.ClearSelection;
         //    }
         //    set
         //    {
         //        rdAlignmentStyle.c
         //    }
         //}

         public override void Clear()
         {
             rdAlignmentStyle.SelectedValue = "Horizontal";
             
         }
    }
}
