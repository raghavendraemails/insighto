﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Social_Sharing.aspx.cs" Inherits="Default3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Free Online Survey & Questionnaire Software for Customer Feedback - Insighto</title>
 <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="images/insighto.ico" />
    <link href="App_Themes/Classic/style.css" rel="stylesheet" type="text/css" />
    <meta name="description" content="Share update about Insighto on Facebook, Twitter and LinkedIn. Create and manage free online surveys. With Insighto’s Online Survey Software." />
    <meta name="keywords" content="Share, Facebook, Twitter, LinkedIn, online survey, free online survey tool, build online surveys, survey software, online survey templates, create a survey" />
    <meta name="google-site-verification" content="dN8GbxVASaehpUTe9XmfLLBsmCpVtZuXme-JWmgvcMI" />
    
<script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>

 <script type="text/javascript">

     var _gaq = _gaq || [];
     _gaq.push(['_setAccount', 'UA-29026379-1']);
     _gaq.push(['_setDomainName', 'insighto.com']);
     _gaq.push(['_setAllowLinker', true]);
     _gaq.push(['_trackPageview']);

     (function () {
         var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://' : 'https://') + 'stats.g.doubleclick.net/dc.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
     })();

</script>



 <script type="text/javascript">
  
     function linkedinsuccess() {
         alert("linked shared successfully");
     }

     function Confirm() {
         var confirm_value = document.createElement("INPUT");
         confirm_value.type = "hidden";
         confirm_value.name = "confirm_value";
        if (confirm("Do you want to tweet message?")) {
             confirm_value.value = "Yes";
             window.open("https://twitter.com/share", 'Twitter', 'height=600,width=600,left=200,top=40,resizable=no,scrollbars=no,toolbar=no,menubar=no,status=no');
             
        } else {
           confirm_value.value = "No";
       }
         document.forms[0].appendChild(confirm_value);
       
     }


     twttr.events.bind('tweet', function (event) {

       //  alert('thank you for tweeting');

         var parms = "TWITTER_USER" + ";" + 50;
         PageMethods.MyMethod(parms, OnSucceededtwitter);

      
     });

     
                    function OnSucceededtwitter(result) {
                        var a = result.split(";")
                        displaySuccess("twitter", a[0], a[1]);
                    }
                

//    function show_login_status(network, status) {

//     
//        

//        if (status) {

//            alert("Logged in to " + network);
//           

//        } else {

//            alert("Not logged in to " + network);

//        }

//       

//    }
    
 </script>



</head>

<body id="share_account_body">
 <form  runat="server">

<%--   <script src="https://platform.linkedin.com/in.js" type="text/javascript">
       lang: en_US
                 </script>
                <script type="IN/Share"> </script>
--%>
     <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true" />
     
         
   
<div id="share_account_wrapper" class="clr">

	<div id="share_account_header_main" class="clr">

    	<div id="share_account_headerL"><a href="https://www.insighto.com"><img src="App_Themes/Classic/Images/insighto_logo.png" border="0" alt="" />
        </a></div>
   

  </div>
    <div id="share_account_container_main" class="clr">
        <div class="title_blk clr">
        	<h1>Tell your friends you are on Insighto. </h1>
            <h4>Spread some love. Get some love back! <br />
				Share and win 2 months of Premium Subscription worth <%=strDiscAmt%>.</h4>
            <h2>Absolutely FREE of cost!</h2>
        </div>
        <div class="box_blk clr">
        	<div class="colm3" id="fbBox">
                <h3 class="heading ">
                    <asp:ImageButton ID="btnFB" runat="server" ImageUrl="~/App_Themes/Classic/Images/FB1.png"  ToolTip="Facebook Timeline Share"  OnClientClick="return postToFB()"/></h3>
                <div class="cont_blk" >
                    <p>Tell your Facebook friends about<br />Insighto.</p>
                    <p class="last">And get <strong><span>15</span> days of Premium subscription FREE!</strong></p>
                </div>
            </div>
        	<div class="colm3" id="TweetBox">
               <%-- <h3 class="heading " >--%>
                 
                    <div  id="twitteruser" class="tweet">
                     <h3 class="heading ">
                 <a href="https://twitter.com/share" class="twitter-share-button"  data-text="Do you know? Features you pay for on other survey tools are free on Insighto. I just signed up! https://s.insighto.com/H9cmAj" data-url="false" data-size="large" data-count="none" data-dnt="true" target="_blank" >Tweet</a>
                 </h3>
                   </div>                 
                 <%--    </h3>--%>
                <div class="cont_blk">
                    <p>Tell your Twitter followers about <br />Insighto.</p>
                    <p class="last">And get <strong><span>15</span> days of Premium subscription FREE!</strong></p>
                </div>
            </div>
        	<div class="colm3" id="INBox" >
                <h3 class="heading " >
               
                    <asp:ImageButton ID="bnIN" runat="server"  ToolTip="LinkedIn Share" ImageUrl="~/App_Themes/Classic/Images/Linkedin1.png" OnClientClick="return postToLinkedIn()" /></h3>
                <div class="cont_blk" >
                    <p>Tell your LinkedIn connections about Insighto.</p>
                    <p class="last">And get <strong><span>30</span> days of Premium subscription FREE!</strong></p>
                </div>
            </div>
        </div>
        <div class="txtcnt">
            <asp:Button ID="btnnothanks" class="btn_bg" runat="server" 
                Text="No, Thanks. Proceed to My Surveys" onclick="btnnothanks_Click"  />
        </div>
        <div class="txtcnt">
            <asp:Button ID="btnproceed" runat="server" Text="Proceed to My Surveys" 
                class="btn_bg" onclick="btnproceed_Click"  />
        </div>
    	         	
  </div>
</div>
<div id="footer_main">
    <div id="footer_blk" class="clr">
        <div id="footerL">Copyright &copy; 2015 Insighto</div>
        <div id="footerR">
           <%-- <ul>
                <li><a href="../In/ContactUs.aspx">Contact Us</a></li>
                <li><a href="antispam.aspx?key=6auQ29rICLYtdOGhX9UHsQ%3d%3d">Anti spam policy</a></li>
                <li><a href="privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy policy</a></li>
                <li><a href="#">Sitemap</a></li>
            </ul>--%>
        </div>
    </div>
</div>
   
   </form>
</body>

<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29026379-1']);
    _gaq.push(['_setDomainName', 'insighto.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'https://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
  <script type="text/jscript" src="https://connect.facebook.net/en_US/all.js"></script>
    <script type="text/javascript">
        document.getElementById('btnproceed').style.visibility = "hidden";
        function displaySuccess(name, result, msg) {
           
            if (name == "linkedIn") {
                if (result == "success") {
                    
                    document.getElementById('INBox').innerHTML = msg;
                    document.getElementById('btnproceed').style.visibility = "visible";
                    document.getElementById('btnnothanks').style.visibility = "hidden";
                }
                else if (result == "errorcredit") {
                    document.getElementById('INBox').innerHTML = msg;
                    document.getElementById('btnproceed').style.visibility = "visible";
                    document.getElementById('btnnothanks').style.visibility = "hidden";
                   
                }
                else if (result == "error") {
              
                    document.getElementById('INErr').value = msg;
                    document.getElementById('btnproceed').style.visibility = "visible";
                    document.getElementById('btnnothanks').style.visibility = "hidden";
                }
            }
            if (name == "twitter") {
                if (result == "success") {
                    document.getElementById('TweetBox').innerHTML = msg;
                    document.getElementById('btnproceed').style.visibility = "visible";
                    document.getElementById('btnnothanks').style.visibility = "hidden";
                  
                }
                else if (result == "errorcredit") {
                    document.getElementById('TweetBox').innerHTML = msg;
                    document.getElementById('btnproceed').style.visibility = "visible";
                    document.getElementById('btnnothanks').style.visibility = "hidden";
                  
                }
                else if (result == "error") {
                    document.getElementById('TweetErr').value = msg;

                    document.getElementById('btnproceed').style.visibility = "visible";
                    document.getElementById('btnnothanks').style.visibility = "hidden";
                }
            }
            if (name == "facebook") {
                if (result == "success") {
                    document.getElementById('fbBox').innerHTML = msg;
                    document.getElementById('btnproceed').style.visibility = "visible";
                    document.getElementById('btnnothanks').style.visibility = "hidden";
                }
                else if (result == "errorcredit") {
                    document.getElementById('fbBox').innerHTML = msg;
                    document.getElementById('btnproceed').style.visibility = "visible";
                    document.getElementById('btnnothanks').style.visibility = "hidden";
                }
                else if (result == "error") {
                    document.getElementById('fbErr').value = msg;
                    document.getElementById('btnproceed').style.visibility = "visible";
                    document.getElementById('btnnothanks').style.visibility = "hidden";
                }
            }
        }

        // Popup window code
        function postToTwitter() {
            //   window.open("twittershare.aspx?initial=true", 'Twitter', 'height=500,width=600,left=200,top=40,resizable=no,scrollbars=no,toolbar=no,menubar=no,status=no');
            window.open('https://twitter.com/share','Twitter', 'height=500,width=600,left=200,top=40,resizable=no,scrollbars=no,toolbar=no,menubar=no,status=no');
            return true;
           // document.location.reload(true);
        }

        function postToLinkedIn() {
           
            window.open("linkedinshare.aspx?initial=true", 'linkedIn', 'height=600,width=600,left=200,top=40,resizable=no,scrollbars=no,toolbar=no,menubar=no,status=no');

            return false;
        }

        // Login in the current user via Facebook and ask for email permission
        function postToFB() {
            var i = 0;
            var error_no_option = 0, error_no_fb = 0;
            var error_msg;
            // Initialize the Facebook JavaScript SDK
            FB.init({
                appId: '390001971057863',
                xfbml: true,
                status: true,
                cookie: true
            });
            FB.login(checkLoginStatus, { scope: 'email' });
            return false;
        }

        // Check the result of the user status and display login button if necessary
        function checkLoginStatus(response) {
            var email;
            if (response && response.status == 'connected') {
                FB.api('/me', function (resp) {
                    email = resp.email;
                });
                FB.api('me/friends/', function (resp) {
                    var length = 0;
                    for (var dummy in resp.data) length++;
                    FB.ui(
                {
                    method: 'feed',
                    name: '<b>Do you know? Premium online survey features are now free with Insighto. I just signed up.</b>',
                    link: 'https://s.insighto.com/1grXOfn',
                    picture: 'https://www.insighto.com/App_Themes/Classic/Images/insighto_logo.png',
                    caption: 'Insighto has launched a new free plan that offers a lot more and includes features that you otherwise pay for on other online survey tools.',
                    description: 'www.insighto.com'
                },
                function (response) {
                    if (response && response.post_id) {
                        var parms = email + ";" + length;
                        PageMethods.MyMethod(parms, OnSucceeded);
                    }
                }

                );
                    function OnSucceeded(result) {
                        var a = result.split(";")
                        displaySuccess("facebook", a[0], a[1]);
                    }
                });
            }

        }
    </script>
    
    
<script type="text/javascript">
    var fb_param = {};
    fb_param.pixel_id = '6009428940138';
    fb_param.value = '0.00';
    fb_param.currency = 'USD';
    (function () {
        var fpw = document.createElement('script');
        fpw.async = true;
        fpw.src = '//connect.facebook.net/en_US/fp.js';
        var ref = document.getElementsByTagName('script')[0];
        ref.parentNode.insertBefore(fpw, ref);
    })();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6009428940138&amp;value=0&amp;currency=USD" /></noscript>

<!-- Google Code for Lead_signup Conversion Page --> 
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1027814833;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "dEWdCImw7gIQseuM6gM"; var google_conversion_value = 0; var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"  src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  src="//www.googleadservices.com/pagead/conversion/1027814833/?value=0&amp;label=dEWdCImw7gIQseuM6gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</html>
