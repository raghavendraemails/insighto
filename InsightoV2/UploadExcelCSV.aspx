﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="UploadExcelCSV.aspx.cs" Inherits="Insighto.Pages.UploadExcelCSV" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                Upload Excel / CSV</h3>
        </div>
       <%-- <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" title="Close" onclick="closeModalSelf(false,'');">
                &nbsp;</a>
        </div>--%>
        <%--<div class="popupClosePanel">
            <a href="ManageFolders.aspx">X</a>
        </div>--%>
        <!-- //popup heading -->
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupContentPanel">
        <!-- popup content panel -->
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccessMsg" runat="server" Visible="False" Text="File has been uploaded successfully."></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" Visible="False" Text="Invalid  file format."></asp:Label></div>
        </div>
        <div class="defaultH">
        </div>
        <div>
            <!-- form -->
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <label for="uploadType">
                        Upload</label>
                </div>
                <div class="loginControls">
                    <table>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rbtnUploadType" AutoPostBack="true" GroupName="uploadType" runat="server"
                                    Checked="true" OnCheckedChanged="rbtnUploadType_CheckedChanged" />
                            </td>
                            <td>
                                <img src="App_Themes/Classic/Images/icon-excel.gif" alt="Excel" title="Excel" />
                            </td>
                            <td>
                                <asp:RadioButton ID="rbtnUploadType2" runat="server" AutoPostBack="true" GroupName="uploadType"
                                    OnCheckedChanged="rbtnUploadType2_CheckedChanged" />
                            </td>
                            <td>
                                <img src="App_Themes/Classic/Images/csv_logo2.png" alt="CSV" title="CSV" />
                            </td>
                            <td>
                                <span class="lt_padding"><a href="SamplePreview.xlsx" id="lnkSamplePrview" runat="server"
                                    class="downloadLink1">Sample Preview</a></span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <label for="emailList">
                        Email List Name</label>
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="txtEmailListName" Enabled="false" Height="20px" runat="server" MaxLength="25"
                        class="textBoxMedium"></asp:TextBox>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:Label ID="Label3" AssociatedControlID="reqFirstName" runat="server" /><asp:RequiredFieldValidator SetFocusOnError="true"
                        ID="reqFirstName" CssClass="lblRequired" ControlToValidate="txtEmailListName"
                        ErrorMessage="Please enter list name." runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <label for="chooseFile">
                        Choose File</label>
                </div>
                <div class="loginControls">
                    <asp:FileUpload ID="fileUpload" runat="server" />
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:Label ID="Label1" AssociatedControlID="reqFile" runat="server" />
                    <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqFile" Display="Dynamic" CssClass="lblRequired"
                        ControlToValidate="fileUpload" runat="server" ErrorMessage="Please select a file to upload."></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator SetFocusOnError="true"  Display="Dynamic" CssClass="lblRequired" ID="regFile"
                        ControlToValidate="fileUpload" runat="Server" ErrorMessage="Please select Excel / CSV files only."
                        ValidationExpression="^.*\.(xls|xlsx|csv)$" />
                </div>
            </div>
            <div id="divValidators" runat="server">
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <label for="uploadFile">
                    </label>
                </div>
                <div class="loginControls">
                    <asp:Button ID="btnSave" runat="server" CssClass="btn_small_65" Text="Save" ToolTip="Save"
                        OnClick="btnSave_Click" />
                </div>
            </div>
            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>
</asp:Content>
