﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using Insighto.Business.Services;
using Insighto.Data;
using CovalenseUtilities.Services;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using System.IO;
using System.Text.RegularExpressions;

namespace Insighto.Pages
{
    public partial class UploadExcelCSV : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int contactId = 0;
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (!IsPostBack)
            {
                if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                {
                    contactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0); 
                }                         
                var contactService = ServiceFactory.GetService<ContactListService>();
                var contacts = contactService.FindContactsByContactId(contactId);
                if (contacts != null)
                {
                    txtEmailListName.Text = contacts.CONTACTLIST_NAME;
                }
                
            }
         
        }

        protected override void OnInit(EventArgs e)
        {
            //divValidators.Controls.Add(GetCustomValidator());
            //base.OnInit(e);
        }

        protected void cvMoss_ServerValidate(object source, ServerValidateEventArgs args)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            string save = "In/UserImages/" + userInfo.UserId + "/";
            string file = "";
            var finfo = new FileInfo(fileUpload.FileName);
            bool flag = true;
            if (finfo.Extension == ".xls" || finfo.Extension == ".xlsx" || finfo.Extension == ".csv")
            {
                if (fileUpload.HasFile)
                {
                    file = System.Guid.NewGuid().ToString() + "_" + fileUpload.FileName;
                    save += file;
                    fileUpload.SaveAs(Server.MapPath(save));
                }
                if (rbtnUploadType.Checked == true && (finfo.Extension == ".xls" || finfo.Extension == ".xlsx"))
                {
                    DataSet xlds = new DataSet();
                    DataSet ds = new DataSet();

                    string str = ("Provider=Microsoft.ACE.OLEDB.12.0;" + ("Data Source=" + Server.MapPath(save) + ";" + "Excel 12.0 Xml;HDR=No;IMEX=1"));
                    using (OleDbConnection mycon = new OleDbConnection(str))
                    {
                        try
                        {
                            OleDbConnection objConn = new OleDbConnection(str);

                            objConn.Open();
                            DataTable dt1 = new DataTable();
                            dt1 = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            String[] excelSheets = new String[dt1.Rows.Count];
                            int l = 0;
                            foreach (DataRow row in dt1.Rows)
                            {
                                excelSheets[l] = row["TABLE_NAME"].ToString();
                                l++;
                            }

                            OleDbCommand command = new OleDbCommand();
                            OleDbDataAdapter dbcomm = new OleDbDataAdapter();
                            mycon.Open();
                            for (int j = 0; j <= 0; j++)
                            {
                                string strSelectString = "SELECT * FROM [" + dt1.Rows[j][2] + "]";
                                command = new OleDbCommand(strSelectString, mycon);
                                command.CommandTimeout = 999999;
                                dbcomm = new OleDbDataAdapter(strSelectString, mycon);
                                dbcomm.Fill(ds);
                            }
                            objConn.Close();
                            bool status = false;

                            for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                            {

                                if (ds.Tables[0].Rows[0][j].ToString() != "" && ds.Tables[0].Rows[0][j].ToString() != null)
                                {
                                    status = true;
                                }

                            }
                            int cust_id = ds.Tables[0].Columns.Count;
                            int cust_id1 = ds.Tables[0].Columns.Count;
                            int temp = 0;
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                for (int k = 3; k < ds.Tables[0].Columns.Count; k++)
                                {
                                    if (ds.Tables[0].Rows[i][k].ToString() != "")
                                    {
                                        if (k > temp)
                                        {
                                            temp = k;
                                            cust_id = k;
                                        }
                                    }
                                }

                            }

                            for (int m = 3; m < cust_id; m++)
                            {
                                if (ds.Tables[0].Rows[0][m].ToString() == "")
                                    ds.Tables[0].Rows[0][m] = Convert.ToString("Custom" + (m - 2));
                            }

                            if (status == true)
                            {
                                var contacts = new osm_contactlist();
                                if (ds.Tables[0].Rows[0][0].ToString() != "")
                                {
                                   // SaveContactList(ds);
                                }
                            }
                            else
                            {
                                //this.ClientScript.RegisterStartupScript(this.GetType(), "Some Title", "<script language=\"javaScript\">" + "alert('Invalid Excel Format!');" + "window.location.href='CreateEmailList.aspx';" + "<" + "/script>");
                                flag = false;
                            }
                        }
                        catch
                        {
                            flag = false;
                        }
                    }
                }
                else
                {
                    DataSet ds = new DataSet();
                    string savecsv = "In/UserImages/" + userInfo.UserId + "/";
                    string strSql = "SELECT * FROM [" + file + "]";
                    string strCSVConnString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath(savecsv) + ";" + "Extended Properties='text;HDR=No;IMEX=1'";
                    OleDbDataAdapter oleda = new OleDbDataAdapter(strSql, strCSVConnString);
                    try
                    {
                        oleda.Fill(ds);
                        //SaveContactList(ds);
                    }
                    catch (Exception)
                    {
                        flag = false;
                    }
                }
            }
            args.IsValid = flag;
            
        }

        private CustomValidator GetCustomValidator()
        {
            var revTemp = new CustomValidator
            {
                CssClass = "lblRequired",
                ID = "reqFormat",
                ErrorMessage = "List contains duplicate/invalid email id's.",
                Display = ValidatorDisplay.Dynamic,
                Visible = true
            };
            revTemp.ServerValidate += cvMoss_ServerValidate;
            return revTemp;
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
               
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userInfo = sessionService.GetLoginUserDetailsSession();
                string save = "In/UserImages/" + userInfo.UserId + "/";
                string file = "";
                var finfo = new FileInfo(fileUpload.FileName);

                if (finfo.Extension == ".xls" || finfo.Extension == ".xlsx" || finfo.Extension == ".csv")
                {
                    if (fileUpload.HasFile)
                    {
                        file = System.Guid.NewGuid().ToString() + "_" + fileUpload.FileName;
                        save += file;
                        fileUpload.SaveAs(Server.MapPath(save));
                    }
                    if (rbtnUploadType.Checked == true && (finfo.Extension == ".xls" || finfo.Extension == ".xlsx"))
                    {
                        DataSet xlds = new DataSet();
                        DataSet ds = new DataSet();

                        string str = ("Provider=Microsoft.ACE.OLEDB.12.0;" + ("Data Source=" + Server.MapPath(save) + ";" + "Excel 12.0 Xml;HDR=No;IMEX=1"));
                        using (OleDbConnection mycon = new OleDbConnection(str))
                        {
                            try
                            {
                                OleDbConnection objConn = new OleDbConnection(str);

                                objConn.Open();
                                DataTable dt1 = new DataTable();
                                dt1 = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                String[] excelSheets = new String[dt1.Rows.Count];
                                int l = 0;
                                foreach (DataRow row in dt1.Rows)
                                {
                                    excelSheets[l] = row["TABLE_NAME"].ToString();
                                    l++;
                                }

                                OleDbCommand command = new OleDbCommand();
                                OleDbDataAdapter dbcomm = new OleDbDataAdapter();
                                mycon.Open();
                                for (int j = 0; j <= 0; j++)
                                {
                                    string strSelectString = "SELECT * FROM [" + dt1.Rows[j][2] + "]";
                                    command = new OleDbCommand(strSelectString, mycon);
                                    command.CommandTimeout = 999999;
                                    dbcomm = new OleDbDataAdapter(strSelectString, mycon);
                                    dbcomm.Fill(ds);
                                }
                                objConn.Close();
                                bool status = false;

                                for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                                {


                                    if (ds.Tables[0].Rows[0][j].ToString() != "" && ds.Tables[0].Rows[0][j].ToString() != null)
                                    {
                                        status = true;
                                    }

                                }
                                int cust_id = ds.Tables[0].Columns.Count;
                                int cust_id1 = ds.Tables[0].Columns.Count;
                                int temp = 0;
                                //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                //{
                                //    for (int k = 3; k < ds.Tables[0].Columns.Count; k++)
                                //    {
                                //        if (ds.Tables[0].Rows[i][k].ToString() != "")
                                //        {
                                //            if (k > temp)
                                //            {
                                //                temp = k;
                                //                cust_id = k;
                                //            }
                                //        }
                                //    }

                                //}

                                //for (int m = 3; m < cust_id; m++)
                                //{
                                //    if (ds.Tables[0].Rows[0][m].ToString() == "")
                                //        ds.Tables[0].Rows[0][m] = Convert.ToString("Custom" + (m - 2));
                                //}

                                if (status == true)
                                {
                                    var contacts = new osm_contactlist();
                                    if (ds.Tables[0].Rows[0][3].ToString() != "")
                                    {
                                        SaveContactList(ds);
                                    }
                                }
                                else
                                {
                                    //this.ClientScript.RegisterStartupScript(this.GetType(), "Some Title", "<script language=\"javaScript\">" + "alert('Invalid Excel Format!');" + "window.location.href='CreateEmailList.aspx';" + "<" + "/script>");
                                    dvSuccessMsg.Visible = false;
                                    lblErrMsg.Text = "File is not in correct format.";
                                    dvErrMsg.Visible = true;
                                    lblSuccessMsg.Visible = false;
                                    lblErrMsg.Visible = true;
                                }
                            }
                            catch
                            {
                                dvSuccessMsg.Visible = false;
                                lblErrMsg.Text = "File is not in correct format.";
                                dvErrMsg.Visible = true;
                                lblSuccessMsg.Visible = false;
                                lblErrMsg.Visible = true;
                            }
                        }
                    }
                    else 
                    {
                        DataSet ds = new DataSet();
                        string savecsv = "In/UserImages/" + userInfo.UserId + "/";
                        string strSql = "SELECT * FROM [" + file + "]";
                        string strCSVConnString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath(savecsv) + ";" + "Extended Properties='text;HDR=No;IMEX=1'";
                        OleDbDataAdapter oleda = new OleDbDataAdapter(strSql, strCSVConnString);
                        try
                        {
                            oleda.Fill(ds);
                            SaveContactList(ds);
                        }
                        catch (Exception)
                        {
                            dvSuccessMsg.Visible = false;
                            lblErrMsg.Text = "File is not in correct format.";
                            dvErrMsg.Visible = true;
                            lblSuccessMsg.Visible = false;
                            lblErrMsg.Visible = true;
                           // ClientScript.RegisterStartupScript(GetType(), "Some Title", "<script language=\"javaScript\">" + "alert('File is Not in Correct Format');" + "window.location.href='CreateEmailList.aspx';" + "<" + "/script>");
                        }
                    }
                }
                else
                {
                    dvSuccessMsg.Visible = false;
                    lblErrMsg.Text = "Invalid  file format.";
                    dvErrMsg.Visible = true;
                    lblSuccessMsg.Visible = false;
                    lblErrMsg.Visible = true;
                }
            }
           
        }


        protected void SaveContactList(DataSet ds)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var emailService = ServiceFactory.GetService<EmailService>();
            var contactlist = emailService.SaveEmailContactList(userInfo.UserId, txtEmailListName.Text);

            if (ds.Tables[0].Columns.Count > 3)
                contactlist.CUSTOM_HEADER1 = ds.Tables[0].Rows[0][3].ToString().Trim();
            if (ds.Tables[0].Columns.Count > 4)
                contactlist.CUSTOM_HEADER2 = ds.Tables[0].Rows[0][4].ToString().Trim();
            if (ds.Tables[0].Columns.Count > 5)
                contactlist.CUSTOM_HEADER3 = ds.Tables[0].Rows[0][5].ToString().Trim();
            if (ds.Tables[0].Columns.Count > 6)
                contactlist.CUSTOM_HEADER4 = ds.Tables[0].Rows[0][6].ToString().Trim();
            if (ds.Tables[0].Columns.Count > 7)
                contactlist.CUSTOM_HEADER5 = ds.Tables[0].Rows[0][7].ToString().Trim();
            if (ds.Tables[0].Columns.Count > 8)
                contactlist.CUSTOM_HEADER6 = ds.Tables[0].Rows[0][8].ToString().Trim();
            if (ds.Tables[0].Columns.Count > 9)
                contactlist.CUSTOM_HEADER7 = ds.Tables[0].Rows[0][9].ToString().Trim();
            if (ds.Tables[0].Columns.Count > 10)
                contactlist.CUSTOM_HEADER8 = ds.Tables[0].Rows[0][10].ToString().Trim();
            emailService.SaveAddressBookColumnsByContactListId(contactlist);
            SaveEmailList(contactlist.CONTACTLIST_ID, ds);

        }

        protected void SaveEmailList(int contactListId, DataSet ds)
        {
            string Inval_dupIDs = "";
            if (ds != null && ds.Tables.Count > 0)
            {
                Hashtable ht = new Hashtable();
                string patternLenient = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Regex reLenient = new Regex(patternLenient);
                string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
                   + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                   + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                   + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                   + @"[a-zA-Z]{2,}))$";
                Regex reStrict = new Regex(patternStrict);
                for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i][0].ToString() != null && Convert.ToString(ds.Tables[0].Rows[i][0].ToString()).Trim().Length > 0)
                    {

                          string email = Convert.ToString(ds.Tables[0].Rows[i][0]);
                          bool isEmail = reLenient.IsMatch(email);
                          if (isEmail)
                          {

                              if (ht.Contains(Convert.ToString(ds.Tables[0].Rows[i][0].ToString()).Trim()))
                              {
                                  Inval_dupIDs += "," + ds.Tables[0].Rows[i][0].ToString().Trim();
                              }
                              else
                              {
                                  var emails = new osm_emaillist();
                                  emails.CONTACTLIST_ID = contactListId;
                                  ht.Add(ds.Tables[0].Rows[i][0].ToString().Trim(), i);


                                  if (ds.Tables[0].Rows[i][0].ToString() != "")
                                  {

                                      if (ds.Tables[0].Columns.Count > 0)
                                          emails.EMAIL_ADDRESS = ds.Tables[0].Rows[i][0].ToString();
                                      if (ds.Tables[0].Columns.Count > 1)
                                          emails.FIRST_NAME = ds.Tables[0].Rows[i][1].ToString();
                                      if (ds.Tables[0].Columns.Count > 2)
                                          emails.LAST_NAME = ds.Tables[0].Rows[i][2].ToString();
                                      if (ds.Tables[0].Columns.Count > 3)
                                          emails.CUSTOM_VAR1 = ds.Tables[0].Rows[i][3].ToString();
                                      if (ds.Tables[0].Columns.Count > 4)
                                          emails.CUSTOM_VAR2 = ds.Tables[0].Rows[i][4].ToString();
                                      if (ds.Tables[0].Columns.Count > 5)
                                          emails.CUSTOM_VAR3 = ds.Tables[0].Rows[i][5].ToString();
                                      if (ds.Tables[0].Columns.Count > 6)
                                          emails.CUSTOM_VAR4 = ds.Tables[0].Rows[i][6].ToString();
                                      if (ds.Tables[0].Columns.Count > 7)
                                          emails.CUSTOM_VAR5 = ds.Tables[0].Rows[i][7].ToString();
                                      if (ds.Tables[0].Columns.Count > 8)
                                          emails.CUSTOM_VAR6 = ds.Tables[0].Rows[i][8].ToString();
                                      if (ds.Tables[0].Columns.Count > 9)
                                          emails.CUSTOM_VAR7 = ds.Tables[0].Rows[i][9].ToString();
                                      if (ds.Tables[0].Columns.Count > 10)
                                          emails.CUSTOM_VAR8 = ds.Tables[0].Rows[i][10].ToString();
                                      var emailService = ServiceFactory.GetService<EmailService>();
                                      emailService.SaveEmailsByList(emails);
                                      lblSuccessMsg.Visible = true;
                                      lblErrMsg.Visible = false;
                                      dvSuccessMsg.Visible = true;
                                      dvErrMsg.Visible = false;
                                  }
                              }
                          }
                    }
                }
            }
        }
        protected void rbtnUploadType_CheckedChanged(object sender, EventArgs e)
        {
            lnkSamplePrview.HRef = "SamplePreview.xlsx";
        }
        protected void rbtnUploadType2_CheckedChanged(object sender, EventArgs e)
        {
            lnkSamplePrview.HRef = "SamplePreview.csv";
        }
}
}