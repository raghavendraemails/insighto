﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true"
    CodeFile="SurveyMessages.aspx.cs" Inherits="Insighto.Pages.SurveyMessages"%>

<%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" runat="Server">
    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccMsg" runat="server" Visible="False" 
                meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
    </div>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
        <asp:Label ID="lblErrMsg" runat="server" Visible="False" 
            meta:resourcekey="lblErrMsgResource1"></asp:Label>
    </div>
    <div class="surveyQuestionPanel">
        <!-- survey question panel -->
        <div class="surveyQuestionHeader">
            <div class="surveyQuestionTitle">
                <asp:Label ID="lblTitle" runat="server" Text="SURVEY END MESSAGES" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>
            </div>
            <div class="previewPanel">
                <table align="right">
                    <tr>
                        <td class="leftAlign">
                            <asp:HyperLink ID="hlnkBack" title='Back' ToolTip="Back to Pre Launch Check" 
                                runat="server" meta:resourcekey="hlnkBackResource1"> <div class="backbglink">Back </div></asp:HyperLink>
                        </td>
                        <td>
                            <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="surveyQuestionTabPanel">
            <!-- survey question tabs -->
            <div>
                <!-- THANK YOU PAGE -->
                <div class="surveyEndTab">
                    <!-- thank you page panel -->
                    <div class="surveyEndTabTitle tabThankyou">
                       
                        <div id="dvThankyou" runat="server"><span id="iconThankyou" class="iconMinus">Thank You Page</span></div>
                    </div>
                    <div class="surveyEndTabRadioPanel">
                        <asp:RadioButtonList ID="rbtnThankyou" CssClass="rbtnThankyou" runat="server" 
                            RepeatDirection="Horizontal" meta:resourcekey="rbtnThankyouResource1">
                            <asp:ListItem Value="0" Text="Default" Selected="True" 
                                meta:resourcekey="ListItemResource1"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Custom Text" meta:resourcekey="ListItemResource2"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="helpdiv">
                        <a href="#" title="Help" class="helpLink2" onclick="javascript: window.open('help/2_5_2.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">
                        </a>
                    </div>
                    <!-- //thank you page panel -->
                </div>
                <div class="surveryEdnTabTextPanel pnlThankyou" id="thankyou" style="display: block;"
                    runat="server">
                    <!-- text here -->
                    <div class="sampleLogoHeader" id="thankyouLogo" runat="server">
                    </div>
                    <h2 class="surveyMessage divThankyouDefault" id="divThankyouDefault" runat="server">
                        <asp:Label ID="lblDefaultMsg" runat="server" 
                            Text="Thank you for participating in the survey. Your feedback is highly appreciated." 
                            meta:resourcekey="lblDefaultMsgResource1"></asp:Label>
                    </h2>
                    <div class="divThankyouCustom" id="divThankyouCustom" style="display: none;" runat="server">
                        <asp:TextBox ID="txtThankyouMsg" CssClass="surveyMessageTextBox txtThankyouMsg" onfocus="watermark('txtThankyouMsg','You can put your custom text here');"
                            onblur="watermark('txtThankyouMsg','You can put your custom text here');" runat="server"
                            Text="You can put your custom text here" 
                            meta:resourcekey="txtThankyouMsgResource1" />
                        <input type="hidden" id="hdnThanku" runat="server" class="hdnThanku" value="0" />
                    </div>
                    <div style="display:block;" class="reqThankyou">
                        <asp:CustomValidator ID="custThankyou" runat="server" CssClass="lblRequired" Display="Dynamic"
                            ErrorMessage="Please enter thank you page message." ControlToValidate="txtThankyouMsg"
                            ValidateEmptyText="True" ClientValidationFunction="ValidateThanku" 
                            meta:resourcekey="custThankyouResource1"></asp:CustomValidator>
                    </div>
                    <div id="ThankyouContent" runat="server">
                    </div>                    
                </div>
                <div class="clear"></div>
                <!-- //THANK YOU PAGE -->
            
            </div>
             <div class="clear"></div>
            <div >
                <!-- SURVEY CLOSE PAGE -->
                <div class="surveyEndTab">
                    <!-- survey close page panel -->
                    <div class="surveyEndTabTitle tabClose">
                       <div id="dvSurveyClose" runat="server"><span id="iconClose" class="icon-plus">SURVEY CLOSE PAGE</span></div> 
                    </div>
                    <div class="surveyEndTabRadioPanel">
                        <asp:RadioButtonList ID="rbtnClose" CssClass="rbtnClose" runat="server" 
                            RepeatDirection="Horizontal" meta:resourcekey="rbtnCloseResource1">
                            <asp:ListItem Value="0" Text="Default" Selected="True" 
                                meta:resourcekey="ListItemResource3"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Custom Text" meta:resourcekey="ListItemResource4"></asp:ListItem>
                        </asp:RadioButtonList>
                        <input type="hidden" id="hdnClose" runat="server" class="hdnClose" value="0" />
                    </div>
                    <div class="helpdiv">
                        <a href="#" title="Help" class="helpLink2" onclick="javascript: window.open('help/2_5_4.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">
                        </a>
                    </div>
                    <!-- //survey close page panel -->
                </div>
                <div class="surveryEdnTabTextPanel pnlClose" id="close" runat="server" style="display: none;">
                    <div class="sampleLogoHeader" id="closeLogo" runat="server">
                    </div>
                    <div>
                        <div id="div2">
                            <h2 class="surveyMessage divCloseDefault" id="divCloseDefault" runat="server">
                                <asp:Label ID="lblCloseDefaultMsg" runat="server" 
                                    Text="This survey is no longer open for Input." 
                                    meta:resourcekey="lblCloseDefaultMsgResource1"></asp:Label>
                            </h2>
                            <div class="divCloseCustom divCloseCustom" style="display: none;" id="divCloseCustom"
                                runat="server">
                                <asp:TextBox ID="txtCloseCustom" CssClass="surveyMessageTextBox txtCloseCustom" onfocus="watermark('txtCloseCustom','You can put your custom text here');"
                                    onblur="watermark('txtCloseCustom','You can put your custom text here');" runat="server"
                                    Text="You can put your custom text here" 
                                    meta:resourcekey="txtCloseCustomResource1" />
                            </div>
                            <div  style="display:block;" class="reqClose">
                                <asp:CustomValidator ID="custClose" runat="server" CssClass="lblRequired" Display="Dynamic"
                                    ErrorMessage="Please enter close page message." ControlToValidate="txtCloseCustom"
                                    ValidateEmptyText="True" ClientValidationFunction="ValidateClose" 
                                    meta:resourcekey="custCloseResource1"></asp:CustomValidator>
                            </div>
                            <div id="CloseContent" runat="server">
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <!-- //SURVEY CLOSE PAGE -->
            </div>
            <div class="clear"></div>
            <div>
                <!-- SCREEN OUT PAGE -->
                <div class="surveyEndTab">
                    <!-- screen out page panel -->
                    <div class="surveyEndTabTitle tabScreenout">
                        <div id="dvIconOut" runat="server"><span id="iconOutPage" class="icon-plus">SCREEN OUT PAGE</span></div>
                    </div>
                    <div class="surveyEndTabRadioPanel">
                        <asp:RadioButtonList ID="rbtnScreenOut" CssClass="rbtnScreenOut" runat="server" 
                            RepeatDirection="Horizontal" meta:resourcekey="rbtnScreenOutResource1">
                            <asp:ListItem Value="0" Text="Default" Selected="True" 
                                meta:resourcekey="ListItemResource5"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Custom Text" meta:resourcekey="ListItemResource6"></asp:ListItem>
                        </asp:RadioButtonList>
                        <input type="hidden" id="hdnScreenOut" class="hdnScreenOut" value="0" runat="server" />
                    </div>
                    <div class="helpdiv">
                        <a href="#" title="Help" class="helpLink2" onclick="javascript: window.open('help/2_5_3.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">
                        </a>
                    </div>
                    <!-- //screen out page panel -->
                </div>
                <div class="surveryEdnTabTextPanel pnlOutPage" runat="server" id="screenOut" style="display: none;">
                    <div class="sampleLogoHeader" id="screenOutLogo" runat="server">
                    </div>
                    <div>
                        <div id="div1">
                            <h2 class="surveyMessage divScreenoutdefault" id="divScreenoutdefault" runat="server">
                                <asp:Label ID="lblScreenoutMsg" runat="server" 
                                    Text="Thank you for your interest in  taking the survey. However, for this survey, we  are looking for respondents who fit different profile. Please do not get discouraged, as  there will be many surveys in future  where you will be invited to participate." 
                                    meta:resourcekey="lblScreenoutMsgResource1"></asp:Label>
                            </h2>
                            <div class="divScreenoutcustom" style="display: none;" id="divScreenoutcustom" runat="server">
                                <asp:TextBox ID="txtScreenoutCustom" CssClass="surveyMessageTextBox txtScreenoutCustom"
                                    onfocus="watermark('txtScreenoutCustom','You can put your custom text here');"
                                    onblur="watermark('txtScreenoutCustom','You can put your custom text here');"
                                    runat="server" Text="You can put your custom text here" 
                                    meta:resourcekey="txtScreenoutCustomResource1" />
                            </div>
                            <div  style="display:block;" class="reqScreenout">
                                <asp:CustomValidator ID="custScreenout" runat="server" CssClass="lblRequired" Display="Dynamic"
                                    ErrorMessage="Please enter screen out page message." ControlToValidate="txtScreenoutCustom"
                                    ValidateEmptyText="True" ClientValidationFunction="ValidateScreenOut" 
                                    meta:resourcekey="custScreenoutResource1"></asp:CustomValidator>
                            </div>
                            <div id="ScreenoutContent" runat="server">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //SCREEN OUT PAGE -->
            </div>
            <div class="clear"></div>
            <div>
                <!-- OVER QUOTA PAGE -->
                <div class="surveyEndTab">
                    <!-- over quota page panel -->
                    <div class="surveyEndTabTitle tabOverQuota">
                       <div id="dvOverQuota" runat="server"><span id="iconOverQuota" class="icon-plus">OVER QUOTA PAGE</span></div> 
                    </div>
                    <div class="surveyEndTabRadioPanel">
                        <asp:RadioButtonList ID="rbtnOverQuota" CssClass="rbtnOverQuota" runat="server" 
                            RepeatDirection="Horizontal" meta:resourcekey="rbtnOverQuotaResource1">
                            <asp:ListItem Value="0" Text="Default" Selected="True" 
                                meta:resourcekey="ListItemResource7"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Custom Text" meta:resourcekey="ListItemResource8"></asp:ListItem>
                        </asp:RadioButtonList>
                        <input type="hidden" id="hdnOverQuota" class="hdnOverQuota" value="0" runat="server" />
                    </div>
                    <div class="helpdiv">
                        <a href="#" title="Help" class="helpLink2" onclick="javascript: window.open('help/2_5_5.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">
                        </a>
                    </div>
                    <!-- //over quota page panel -->
                </div>
                <div class="surveryEdnTabTextPanel pnlOverQuota" runat="server" id="overQuota" style="display: none;">
                    <div class="sampleLogoHeader" id="overQuotaLogo" runat="server">
                    </div>
                    <div>
                        <div id="div3">
                            <h2 class="surveyMessage divOverQuotaDefault" id="divOverQuotaDefault" runat="server">
                                <asp:Label ID="lblOverQuotaMsg" runat="server" 
                                    Text="Thank you for your interest in  taking the survey. However, this  survey is  completed and closed. Please visit us again." 
                                    meta:resourcekey="lblOverQuotaMsgResource1"></asp:Label>
                                <%--Text="Thank you for your interest in  taking the survey. However, this  survey is  completed and closed. Please visit us again."--%>
                            </h2>
                            <div class="divOverQuotaCustom" style="display: none;" id="divOverQuotaCustom" runat="server">
                                <asp:TextBox ID="txtOverQuota" CssClass="surveyMessageTextBox txtOverQuota" onfocus="watermark('txtOverQuota','You can put your custom text here');"
                                    onblur="watermark('txtOverQuota','You can put your custom text here');" runat="server"
                                    Text="You can put your custom text here" 
                                    meta:resourcekey="txtOverQuotaResource1" />
                            </div>
                            <div  style="display:block;" class="reqOverQuota">
                                <asp:CustomValidator ID="custOverQuota" runat="server" CssClass="lblRequired" Display="Dynamic"
                                    ErrorMessage="Please enter over quota page message." ControlToValidate="txtOverQuota"
                                    ValidateEmptyText="True" ClientValidationFunction="ValidateOverQuota" 
                                    meta:resourcekey="custOverQuotaResource1"></asp:CustomValidator>
                            </div>
                            <div id="OverQuotaContent" runat="server">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //OVER QUOTA PAGE -->
            </div>
            <!-- //survey question tabs -->
            <input type="hidden" id="hdnLicenseType" class="hdnLicenseType" runat="server" />           
        </div>
        <div class="clear">
        </div>
        <div class="bottomButtonPanel">
            <div>
                <asp:Button ID="btnSaveContinue" runat="server" CssClass="dynamicButton" Text="Save"
                    ToolTip="Save" OnClick="BtnSaveContinue_Click" 
                    meta:resourcekey="btnSaveContinueResource1" />
                <asp:Button ID="btnSaveChanges" runat="server" CssClass="dynamicButton" Text="Save and Continue"
                    ToolTip="Save Changes" OnClick="btnSaveChanges_Click" Visible="False" 
                    meta:resourcekey="btnSaveChangesResource1" />
                <asp:Button ID="btnAddEditEmailList" runat="server" CssClass="dynamicButton" Text="Add/Edit Email List"
                    ToolTip="Add/Edit Email List" OnClick="btnAddEditEmailList_Click" 
                    Visible="False" meta:resourcekey="btnAddEditEmailListResource1" />
            </div>
        </div>
        <!-- //survey question panel -->
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tabThankyou').click(function () {
                var className = $('#iconThankyou').attr('class');
                if (className == 'iconMinus') {

                    $('#iconThankyou').removeClass('iconMinus');
                    $('.pnlThankyou').hide();
                }
                else {
                    $('#iconThankyou').addClass('iconMinus');
                    $('.pnlThankyou').show();
                }
            });
            $('.tabClose').click(function () {
                var className = $('#iconClose').attr('class');
                if (className == 'icon-plus') {
                    $('#iconClose').removeClass('icon-plus');
                    $('#iconClose').addClass('iconMinus');
                    $('.pnlClose').show();
                }
                else {
                    $('#iconClose').removeClass('iconMinus');
                    $('#iconClose').addClass('icon-plus');
                    $('.pnlClose').hide();
                }
            });
            $('.tabScreenout').click(function () {
                var className = $('#iconOutPage').attr('class');
                if (className == 'icon-plus') {
                    $('#iconOutPage').removeClass('icon-plus');
                    $('#iconOutPage').addClass('iconMinus');
                    $('.pnlOutPage').show();
                }
                else {
                    $('#iconOutPage').removeClass('iconMinus');
                    $('#iconOutPage').addClass('icon-plus');
                    $('.pnlOutPage').hide();
                }
            });

            $('.tabOverQuota').click(function () {
                var className = $('#iconOverQuota').attr('class');
                if (className == 'icon-plus') {
                    $('#iconOverQuota').removeClass('icon-plus');
                    $('#iconOverQuota').addClass('iconMinus');
                    $('.pnlOverQuota').show();
                }
                else {
                    $('#iconOverQuota').removeClass('iconMinus');
                    $('#iconOverQuota').addClass('icon-plus');
                    $('.pnlOverQuota').hide();
                }
            });

            $('.rbtnThankyou input').change(function () {
                if ((this).value == "0") {
                    $('.divThankyouDefault').show();
                    $('.divThankyouCustom').hide();
                    $('.reqThankyou').hide();
                    $('.hdnThanku').val('0');
                }
                else {
// if ($('.hdnLicenseType').val() == "FREE")
                    if ($('.hdnLicenseType').val() == "") {
                        OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                        $('.rbtnThankyou').find("input[value='0']").attr("checked", "checked");
                        $('.hdnThanku').val('0');
                    }
                    else {

                        $('.divThankyouDefault').hide();
                        $('.divThankyouCustom').show();                      
                        $('.reqThankyou').show();
                        $('.hdnThanku').val('1');
                        $('#iconThankyou').addClass('iconMinus');
                        $('.pnlThankyou').show();
                        $('.txtThankyouMsg').val('You can put your custom text here');
                        $('.txtThankyouMsg').attr("style", "color:#C8C8C8")

                    }
                }

            });
            $('.rbtnClose input').change(function () {
                if ((this).value == "0") {
                    $('.divCloseDefault').show();
                    $('.divCloseCustom').hide();
                    $('.reqClose').hide();
                    $('.hdnClose').val('0');
                }
                else {
                    if ($('.hdnLicenseType').val() == "") {
                        OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                        $('.rbtnClose').find("input[value='0']").attr("checked", "checked");
                        $('.hdnClose').val('0');
                    }
                    else {
                        $('.divCloseDefault').hide();
                        $('.divCloseCustom').show();
                        $('.reqClose').show();
                        $('.hdnClose').val('1');

                    }
                }
                var className = $('#iconClose').attr('class');
                if (className == 'icon-plus') {
                    $('#iconClose').removeClass('icon-plus');
                    $('#iconClose').addClass('iconMinus');
                    $('.pnlClose').show();
                }
            });
            $('.rbtnScreenOut input').change(function () {

                if ((this).value == "0") {
                    $('.divScreenoutdefault').show();
                    $('.divScreenoutcustom').hide();
                    $('.reqScreenout').hide();
                    $('.hdnScreenOut').val('0');
                }
                else {
                    if ($('.hdnLicenseType').val() == "") {
                        OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                        $('.rbtnScreenOut').find("input[value='0']").attr("checked", "checked");
                        $('.hdnScreenOut').val('0');
                    }

                    else {
                        $('.divScreenoutdefault').hide();
                        $('.divScreenoutcustom').show();
                        $('.reqScreenout').show();
                        $('.hdnScreenOut').val('1');
                    }
                }

                var className = $('#iconOutPage').attr('class');
                if (className == 'icon-plus') {
                    $('#iconOutPage').removeClass('icon-plus');
                    $('#iconOutPage').addClass('iconMinus');
                    $('.pnlOutPage').show();
                }
            });
            $('.rbtnOverQuota input').change(function () {

                if ((this).value == "0") {
                    $('.divOverQuotaDefault').show();
                    $('.divOverQuotaCustom').hide();
                    $('.reqOverQuota').hide();
                    $('.hdnOverQuota').val('0');

                }
                else {
 //if ($('.hdnLicenseType').val() == "FREE")
                    if ($('.hdnLicenseType').val() == "") {
                        OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                        $('.rbtnOverQuota').find("input[value='0']").attr("checked", "checked");
                        $('.hdnOverQuota').val('0');
                    }
                    else {
                        $('.divOverQuotaDefault').hide();
                        $('.divOverQuotaCustom').show();
                        $('.reqOverQuota').show();
                        $('.hdnOverQuota').val('1');
                    }
                }

                var className = $('#iconOverQuota').attr('class');
                if (className == 'icon-plus') {
                    $('#iconOverQuota').removeClass('icon-plus');
                    $('#iconOverQuota').addClass('iconMinus');
                    $('.pnlOverQuota').show();
                }

            });
            var txtQuota = $('.txtOverQuota').val();
            var txtScreenOut = $('.txtScreenoutCustom').val();
            var txtClose = $('.txtCloseCustom').val();
            var txtThankyou = $('.txtThankyouMsg').val();
            var cusText = "You can put your custom text here";

            if (txtQuota == cusText) {
                $('.txtOverQuota').attr("style", "color:#C8C8C8")
            }
            if (txtScreenOut == cusText) {
                $('.txtScreenoutCustom').attr("style", "color:#C8C8C8")
            }
            if (txtClose == cusText) {
                $('.txtCloseCustom').attr("style", "color:#C8C8C8")
            }
            if (txtThankyou == cusText) {
                $('.txtThankyouMsg').attr("style", "color:#C8C8C8")
            }
        });

        function watermark(inputId, text) {
            var inputBox = $('.' + inputId + '').val();
          
            if (inputBox.length > 0) {
                if (inputBox == text)
                    $('.' + inputId + '').val('');
                $('.' + inputId + '').attr("style", "color:#F77601");
            }
            else {
                $('.' + inputId + '').val(text);
                $('.' + inputId + '').attr("style", "color:#C8C8C8");
            }
        }
        function ValidateThanku(sender, args) {
            var flag = $('.hdnThanku').val();
            var value = args.Value;           
            if (flag == '0') {
                args.IsValid = true;
            }
            else {
                if (value != 'You can put your custom text here') {
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }
            }
        }
        function ValidateClose(sender, args) {
            var flag = $('.hdnClose').val();           
            var value = args.Value;
            if (flag == '0') {
                args.IsValid = true;
            }
            else {
                if (value != 'You can put your custom text here') {
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }
            }
        }
        function ValidateOverQuota(sender, args) {
            var flag = $('.hdnOverQuota').val();         
            var value = args.Value;          
            if (flag == '0') {
                args.IsValid = true;
            }
            else {
                if (value != 'You can put your custom text here') {
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }
            }
        }
        function ValidateScreenOut(sender, args) {
            var flag = $('.hdnScreenOut').val();
            var value = args.Value;          
            if (flag == '0') {
                args.IsValid = true;
            }
            else {
                if (value != 'You can put your custom text here') {
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }
            }
        }
    </script>
</asp:Content>
