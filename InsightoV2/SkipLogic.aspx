﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="SkipLogic.aspx.cs" Inherits="SkipLogic" EnableViewState="true"%>

<%@ Import Namespace="Insighto.Data" %>
<%@ Import Namespace="Insighto.Business.ValueObjects" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupHeading">
        <div class="popupTitle">
            <h3><asp:Label ID="lblSkipLogic" runat="server" Text=" Skip Logic" 
                    meta:resourcekey="lblSkipLogicResource1"></asp:Label>
               </h3>
        </div>
       <%-- <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" title="Close" onclick="closeModalSelf(false,'');">
            </a>
        </div>--%>
    </div>
    <div class="popupContentPanel">
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" 
                    Text="Skip Logic cannot be Applied/Modified for Active Surveys" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <div class="formPanel" id="dvSkipLogic" runat="server">
                <p class="defaultHeight"></p>
                <table style="width: 100%" border="0">
                    <tr>
                        <td style="width: 30%">
                            <b>
                                <asp:Label ID="lblcaption" runat="server" Text="Skip Based on a Response:" 
                                meta:resourcekey="lblcaptionResource1" />
                            </b>
                        </td>
                        <td align="right">
                            <span class="redText"><b>
                                <asp:Label ID="lblReorder" runat="server" meta:resourcekey="lblReorderResource1"></asp:Label></b></span>
                        </td>
                    </tr>
                </table>            
            <p class="defaultHeight"></p>
            <div class="skipLogicTextPanel">
            <p>
               </p>
                <asp:Label ID="lblSkipLogicText" runat="server" Text=" Skip logic enables you to redirect respondents to specific question in your survey
                based on response to previous question." 
                    meta:resourcekey="lblSkipLogicTextResource1"></asp:Label>
            <p class="defaultHeight"></p>
            <p>
                &nbsp;<asp:Label ID="lblTriggerSkip" runat="server" 
                    Text="  Currently, only following question types are allowed to trigger a skip. <ul>  <li>Multiple Choice - Single Select</li>   <li>Menu - Drop Down</li>  <li>Choice - Yes / No </li></ul>" 
                    meta:resourcekey="lblTriggerSkipResource1"></asp:Label>
            </p>
           <p class="defaultHeight"></p>
           
           </div>
           <div class="clear"></div>
           <p>
                <b><asp:Label ID="lblApplySkip" runat="server" Text="Apply Skip Logic for:" 
                    meta:resourcekey="lblApplySkipResource1"></asp:Label></b></p>
            
            <p class="defaultHeight"></p>
           
            <%--<p> 
            -- Commented to remove the text question: on the pop up after discussion with anita!
                <b>
                    <asp:Label ID="lblQuesCaption" runat="server" Text="Question:" /></b></p>
            <p>
                &nbsp;</p>--%>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr class="headerRwoGray">
                    <td class="maxWid  skiplogicheaderRwoGray">
                        <asp:Panel ID="pnlSkipQuestion" BorderStyle="None" runat="server" Height="100%" 
                            Width="100%" meta:resourcekey="pnlSkipQuestionResource1">
                            <p>
                                <asp:Label ID="lblQuestion" runat="server" EncodeHtml="False" Width="100%" 
                                    meta:resourcekey="lblQuestionResource1" />
                            </p>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <asp:ListView ID="lstSkipOptions" runat="server" OnItemDataBound="lstSkipOptions_ItemDataBound">
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr runat="server" id="itemPlaceholder">
                        </tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr class="rowStyleGray">
                        <td class="maxWid">
                            <asp:Label ID="lblAns" runat="server" meta:resourcekey="lblAnsResource1" />
                        </td>
                        <td style="display: none;">
                            <asp:Label ID="lblSkipQuestionId" runat="server" 
                                meta:resourcekey="lblSkipQuestionIdResource1" />
                        </td>
                        <td class="minWid" align="right">
                            <asp:DropDownList ID="cbSkip" CssClass="dropdownSmall" runat="server" 
                                meta:resourcekey="cbSkipResource1" />
                        </td>
                        <td style="display: none;">
                            <asp:Label ID="lblAnswerId" runat="server" 
                                meta:resourcekey="lblAnswerIdResource1" />
                            <asp:Label ID="lblQuestionId" runat="server" 
                                meta:resourcekey="lblQuestionIdResource1" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
            <div class="bottomButtonPanel">
                <asp:Button ID="btnSaveSkipLogic" runat="server" Text="Save" ToolTip="Save" CssClass="btn_small_65"
                    OnClick="btnSaveSkipLogic_Click" 
                    meta:resourcekey="btnSaveSkipLogicResource1" />
                <asp:Button ID="btnRmoveSkipLogic" runat="server" Text="Remove" ToolTip="Remove"
                    CssClass="btn_small" OnClick="btnRmoveSkipLogic_Click" Visible="False" 
                    meta:resourcekey="btnRmoveSkipLogicResource1" /></div>
        </div>
    </div>
   
</asp:Content>
