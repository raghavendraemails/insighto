﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SurveyReport.master" AutoEventWireup="true"
    CodeFile="DescriptiveResponses.aspx.cs" Inherits="DescriptiveResponses" %>

<%@ Register Src="~/UserControls/SurveyReportHeader.ascx" TagName="SurveyReportHeader"
    TagPrefix="uc" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyReportHeader" runat="Server">
    <uc:SurveyReportHeader ID="ucSurveyReportHeader" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SurveyReportMainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel runat="server" ID="pnlLoading" Style="height: 450px; vertical-align: middle;"
        HorizontalAlign="Center">
        <div style="width: 100%; height: 220px;">
        </div>
        <img src="App_Themes/Classic/Images/ajax-loader.gif" alt="LOADING..." /><br />
        <div style="width: 100%; height: 220px;">
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlMain" Style="display: none;">
        <div id="intercontainer">
            <table width="100%">
                <tr>
                    <td align="left">
                        <dxe:ASPxLabel ID="lblHeader" runat="server" Text="Responses" EncodeHtml="false"></dxe:ASPxLabel>
                    </td>
                </tr>
                <tr><td class="defaultH"></td></tr>
                <tr>
                    <td align="left">
                    <div style="overflow: auto; width: 900px; height: 600px">

                        <asp:GridView ID="grdResponses" runat="server" AutoGenerateColumns="false" GridLines="None" Width="100%">
                        <HeaderStyle CssClass="reportheaderGray" />
                        <RowStyle CssClass="reportrowGray" />
                        </asp:GridView>

                        </div>
                    </td>
                </tr>
                <tr valign="top">
                    <td align="right">                   
                     <asp:Button ID="btnBack" runat="server" ToolTip="Back" Text="Back" CssClass="dynamicButton" OnClick="cmdBack_Click" runat="server" />
                       
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <script type="text/javascript" language="javascript">

        function pageLoad(sender, args) {
            //alert('hai');
            TogglePannel('<%= pnlMain.ClientID %>', true);
            TogglePannel('<%= pnlLoading.ClientID %>', false);
        }

        function TogglePannel(target, OnOff) {
            obj = document.getElementById(target);

            if (obj) {
                obj.style.display = (OnOff) ? 'inline' : 'none';
            }
        }
 
    </script>
</asp:Content>
