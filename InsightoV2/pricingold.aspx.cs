﻿using System;
using System.Collections;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class pricing : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int userId = 0;
    string transType = "New";
    public string surveytype = "";
    public int surveyID;
    string country;
    private string licenseType = "";
    LoggedInUserInfo loggedInUserInfo;
    string strsource;
    string strterm;
    string currurl;
    string querystring;
    private double prodisprice;
    private double premdisprice;
    private double price;
    string strproprice = ConfigurationManager.AppSettings["proprice"];
    string strpremiumprice = ConfigurationManager.AppSettings["premiumprice"];
    private string utmsource;
    private string utmterm;

    SurveyCore surcoe = new SurveyCore();

    protected void Page_Load(object sender, EventArgs e)
    {
             
        //string NavurlMonth = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", "LicenseType==PRO_MONTHLY");
        //PayMonth.HRef = NavurlMonth;

        lnkcheckitout.HRef = "http://www.youtube.com/watch?v=7xHGLyowVRk";

        string ipAddress = "";

        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
        {
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        }


        var country = Utilities.GetCountryName(ipAddress);
        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);

        country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();


        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (typeHt.Contains("Type"))
                licenseType = typeHt["Type"].ToString();
            if (typeHt.Contains(Constants.USERID))
                userId = ValidationHelper.GetInteger(typeHt[Constants.USERID].ToString(), 0);
            if (typeHt.Contains("Flag"))
                transType = Convert.ToString(typeHt["Flag"]);
            if (typeHt.Contains("utm_source"))
                utmsource = Convert.ToString(typeHt["utm_source"]);
            if (typeHt.Contains("Survey_type"))
                surveytype = Convert.ToString(typeHt["Survey_type"]);

            if (typeHt.Contains("SurveyId"))
                surveyID = Convert.ToInt32(typeHt["SurveyId"]);

            if(typeHt.Contains("Prem"))
            {
                if (typeHt["Prem"].ToString() == "True")
                {
                    divbasic.Visible = false;
                    divpro.Visible = false;
                    divpremium.Visible = true;
                 //   lblMonthly.Visible = false;
                }
                }
        }
            SessionStateService sessionStateService = new SessionStateService();
            loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            

            if (loggedInUserInfo != null)
            {
               // DataSet dscredits = surcoe.getCreditsCount(userId);

                //lblpro.Text = "Upgrade";
               // btnpro1.Text = "Upgrade";
               // btnpro2.Text = "Upgrade";

                if (surveytype == "BASIC")
                {
                    btnpro1.Text = "Upgrade";
                    btnpro2.Text = "Upgrade";
             
                    btnppsprem.Text = "Upgrade";
                    btnppsprem2.Text = "Upgrade";
                }

                if (surveytype == "PPS_PRO")
                {
                    divpro.Visible = false;
                }

                btnpremium1.Text = "Upgrade";
                btnpremium2.Text = "Upgrade";
                if (loggedInUserInfo.LicenseType == "FREE")
                {
               
                    divbasic.Visible = false;
                    td1.Visible = false;
                    td2.Visible = true;
                    td2.Width = "25%";                    
                    td3.Align = "Center";
                    td3.Width = "25%";
                    tdppsprem.Align = "Center";
                    tdppsprem.Width = "25%";
                    td4.Align = "Center";
                    td4.Width = "25%";
                    td5.Visible = true;
                    td5.Width = "0%";

                }
               
              
                  else if(loggedInUserInfo.LicenseType == "PRO_MONTHLY")
                {
                   // div1.Visible = true;
                    divbasic.Visible = false;
                    //lblMonthly.Visible = false;
                   // prodiv.Visible = true;
                    td1.Visible = false;
                    td2.Visible = true;
                    td2.Width = "25%";
                    td3.Align = "Center";
                    td3.Width = "25%";
                    tdppsprem.Align = "Center";
                    tdppsprem.Width = "25%";
                    td4.Align = "Center";
                    td4.Width = "25%";
                    td5.Visible = true;
                    td5.Width = "0%";
                  
                } 
                 else if (loggedInUserInfo.LicenseType == "PRO_QUARTERLY" )
                 {
                     
                     divbasic.Visible = false;
                     divpro.Visible = true;
                     //lblMonthly.Visible = false;
                     td1.Visible = false;
                     td2.Visible = true;
                     td2.Width = "25%";
                     td3.Align = "Center";
                     td3.Width = "25%";
                     tdppsprem.Align = "Center";
                     tdppsprem.Width = "25%";
                     td4.Align = "Center";
                     td4.Width = "25%";
                     td5.Visible = true;
                     td5.Width = "0%";
                 }
                else if (loggedInUserInfo.LicenseType == "PRO_YEARLY")
                {
                    divbasic.Visible = false;
                   // prodiv.Visible = true;
                    //lblMonthly.Visible = false;
                    divpro.Visible = false;

                    td1.Visible = false;
                    td2.Visible = false;
                    td2.Width = "25%";
                   td3.Align = "Center";
                    
                    td3.Width = "25%";
                    tdppsprem.Align = "Center";
                    tdppsprem.Width = "25%"; 
                    td4.Align = "Center";
                    td4.Width = "25%";
                    td5.Visible = true;
                    td5.Width = "0%";
                 
                }
                
            }

            //string NavUrl = Constants.LicenseType + "=" + "PRO_MONTHLY" + "&UserId=" + userId;
            //PayMonth.HRef = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl);

            //string NavUrlbasic = Constants.LicenseType + "=" + "&UserId=" + userId;
            //basicviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlbasic);

            //string NavUrlpro = Constants.LicenseType + "=" +  "&UserId=" + userId;
            //proviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlpro);

            //string NavUrlprem = Constants.LicenseType + "="  + "&UserId=" + userId;
            //premviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlprem);

             SurveyCore surcore = new SurveyCore();


            currurl = HttpContext.Current.Request.RawUrl;
            querystring = HttpContext.Current.Request.QueryString.ToString();

            if ((querystring != "") && (!querystring.Contains("key")))
            {

                if (Request.Cookies["Partner"] != null)
                {

                    HttpCookie aCookie = Request.Cookies["Partner"];
                   
                    strsource = Server.HtmlEncode(aCookie.Values["utm_source"]);
                    strterm = Server.HtmlEncode(aCookie.Values["utm_term"]);

                    if (strterm.ToString() != Request.QueryString["utm_term"].ToString())
                    {
                        if (Request.QueryString["utm_source"].ToString().ToUpper() != "INSIGHTO")
                        {
                            WriteClicked();
                        }
                    }

                }
                else
                {
                    
                    if (Request.QueryString["utm_source"].ToString().ToUpper() != "INSIGHTO")
                    {
                        WriteClicked();
                    }
                    
                }
            }

            
            DataSet dspartnerinfo;

            if (querystring == "")
            {

               
                if (Request.Cookies["Partner"] != null)
                {

                    HttpCookie aCookie = Request.Cookies["Partner"];
                    utmsource = Server.HtmlEncode(aCookie.Values["utm_source"]);
                    utmterm = "";
                    // DataSet dsRefby = surcore.updateReferredby(user.USERID, strsource, strterm);

                    //string NavUrl = Constants.LicenseType + "=" + "PRO_MONTHLY" + "&UserId=" + userId + "&partner=" + utmsource;
                    //PayMonth.HRef = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl);

                    string NavUrlbasic = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=Yes";
                    basicviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlbasic);

                    string NavUrlpro = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=Yes";
                    proviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlpro);

                    string NavUrlprem = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=Yes";
                    premviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlprem);

                }
                else
                {
                    utmsource = "";
                   // utmsource = "Insighto";
                    utmterm = "";

                    //string NavUrl = Constants.LicenseType + "=" + "PRO_MONTHLY" + "&UserId=" + userId +  "&partner=" + utmsource;
                    //PayMonth.HRef = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl);

                    string NavUrlbasic = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=No";
                    basicviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlbasic);

                    string NavUrlpro = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=No";
                    proviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlpro);

                    string NavUrlprem = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=No";
                    premviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlprem);
                }


                


                dspartnerinfo = surcore.getPartnerInfo(utmsource);
                              
              
                topleftmsg.Visible = false;
                toprightmsg.Visible = false;

                shinebullet.Visible = false;
                insightobullet.Visible = false;
                shinesignup.Visible = false;
                insightosignup.Visible = true;

                td1.Visible = false;
                td1.Width = "0%";
                td2.Visible = true;
                td2.Align = "Left";
                td2.Width = "25%";
                tdpromonth.Visible = true;
                tdpricefaq.Visible = true;
                tdonlyaplan.Visible = false;
                tdfreespace.Visible = false;
                divbasic.Attributes.Add("class", "price_1_blkpromo1");
                divbasiccont.Attributes.Add("class", "price_1_blk_contpromo1");

                lblfree.Attributes.Add("class", "freetextl");
                Label1.Attributes.Add("class", "freetextl1");

                td3.Visible = true;
                td3.Align = "left";
                td3.Width = "25%";
                divpro.Attributes.Add("class", "price_1_blkpromo1");
                divprocont.Attributes.Add("class", "price_1_blk_contpromo1");

                tdppsprem.Visible = true;
                tdppsprem.Align = "left";
                tdppsprem.Width = "25%";
                divppsprem.Attributes.Add("class", "price_1_blkpromo1");
                divppspremcont.Attributes.Add("class", "price_1_blk_contpromo1");


                td4.Visible = true;
                td4.Align = "left";
                td4.Width = "30%";
                divpremium.Attributes.Add("class", "price_1_blkpromo1");
                divpremiumcont.Attributes.Add("class", "price_1_blk_contpromo1");
                td5.Visible = true;
                td5.Align = "right";
                td5.Width = "5%";
                
                dvpricetagfree.Visible = false;
                dvpricetagfreepps.Visible = false;
                dvprempricetagfree.Visible = false;
                divstrikepro.Visible = false;
                divstrikeprepps.Visible = false;
                divstrikeprem.Visible = false;
                dvprempricetagfree.Visible = false;

                probr4.Visible = true;

                // brprempersonalize.Visible = true;

                basictr.Visible = true;
               

                //brbasicpremed.Visible = true;
                // brbasicreportindata.Visible = true;
                brbasicviresp.Visible = true;
                //brbasicpersonalize.Visible = true;
                //  probr1.Visible = true;
                

                int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());

                string strproprice1;
                string strpremiumprice1;


                //if (country.ToUpper() == "INDIA")
                //{
                //    int promovalpro = Convert.ToInt32(strproprice.Replace(",", "")) - (prodisval * Convert.ToInt32(strproprice.Replace(",", ""))) / 100;
                //    strproprice1 = string.Format("{0:N0}", Convert.ToInt32(promovalpro.ToString()));

                //    int promovalprem = Convert.ToInt32(strpremiumprice.Replace(",", "")) - (premdisval * Convert.ToInt32(strpremiumprice.Replace(",", ""))) / 100 - 40;
                //    strpremiumprice1 = string.Format("{0:N0}", Convert.ToInt32(promovalprem.ToString()));

                //    //lblorgpro.Text = strproprice;
                //    // lblorgpremium.Text = strpremiumprice;
                //    //lblpremium.Text = strpremiumprice1;
                //    //lblpro.Text = strproprice1;
                //    lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
                //    lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);
                //    lblpremium.Text = lblorgpremium.Text;
                //    lblpro.Text = lblorgpro.Text;

                //    //lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice1);
                //    //lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice1);
                //    //  lblfree.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");
                    

                //}

                //else
                //{
                    tdpricefaq.Visible = false;
                    payinrupeesinsighto.Visible = false;
                    payinrupeesshine.Visible = false;
                    phonesupportinsighto.Visible = false;
                    phonesupportshine.Visible = false;
                    pricesUSDinsighto.Visible = true;
                    pricesUSDshine.Visible = true;
                    paymethodinsighto.InnerText = "Convenient payment options available";
                    paymethodshine.InnerText = "Convenient payment options available";

                    string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                    SqlConnection strconn = new SqlConnection(connStr);
                    strconn.Open();
                    SqlCommand scom = new SqlCommand("sp_getPriceLicenseCountry", strconn);
                    scom.CommandType = CommandType.StoredProcedure;
                    scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = country;
                    scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = "";
                    SqlDataAdapter sda = new SqlDataAdapter(scom);
                    DataSet pricedetails = new DataSet();
                    sda.SelectCommand = scom;
                    sda.Fill(pricedetails);

                 //   string pro_monthlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();


                    string pro_yearlicencetype = pricedetails.Tables[0].Rows[1][1].ToString();


                  //  string prem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();
                    string prem_yearlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();

                    string ppsprem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();

                    strconn.Close();

                    //DataRow[] dr = pricedetails.Tables[0].Select("License_Type = '" + "PRO_MONTHLY'");

                    //string pro_monthprice = "";



                    //if (dr.Length > 0)
                    //{
                    //    pro_monthprice = dr[0].ItemArray[4].ToString();
                    //}
                    //else
                    //{
                    //    pro_monthprice = "";
                    //}


                    DataRow[] dr1 = pricedetails.Tables[0].Select("License_Type = '" + "PPS_PRO'");

                    string pro_yearprice = "";
                    if (dr1.Length > 0)
                    {
                        double promovalpro = Convert.ToDouble(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToDouble(dr1[0].ItemArray[4].ToString())) / 100;
                        //int promovalpro = Convert.ToInt32(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToInt32(dr1[0].ItemArray[4].ToString())) / 100;
                        pro_yearprice = Math.Round(promovalpro).ToString();

                        lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr1[0].ItemArray[4].ToString());
                        //lblorgpro.Attributes.Add("class", "priceRs");
                    }
                    else
                    {
                        pro_yearprice = "";

                        lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                        //  lblorgpro.Attributes.Add("class", "priceRs");
                    }


                    DataRow[] drpps = pricedetails.Tables[0].Select("License_Type = '" + "PPS_PREMIUM'");

                    string ppsprem_yearprice = "";
                    if (drpps.Length > 0)
                    {
                        double promovalppsprem = Convert.ToDouble(drpps[0].ItemArray[4].ToString()) - (prodisval * Convert.ToDouble(drpps[0].ItemArray[4].ToString())) / 100;
                        //int promovalpro = Convert.ToInt32(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToInt32(dr1[0].ItemArray[4].ToString())) / 100;
                        ppsprem_yearprice = Math.Round(promovalppsprem).ToString();

                        lblorgpppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", drpps[0].ItemArray[4].ToString());
                        //lblorgpro.Attributes.Add("class", "priceRs");
                    }
                    else
                    {
                        ppsprem_yearprice = "";

                        lblorgpppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                        //  lblorgpro.Attributes.Add("class", "priceRs");
                    }


                    DataRow[] dr2 = pricedetails.Tables[0].Select("License_Type = '" + "PREMIUM_YEARLY'");

                    string prem_yearprice = "";
                    if (dr2.Length > 0)
                    {
                        double promovalprem = Convert.ToDouble(dr2[0].ItemArray[4].ToString()) - (premdisval * Convert.ToDouble(dr2[0].ItemArray[4].ToString())) / 100;

                        prem_yearprice = Math.Round(promovalprem).ToString();

                        lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr2[0].ItemArray[4].ToString());
                        //  lblorgpremium.Attributes.Add("class", "priceRs");
                    }
                    else
                    {
                        prem_yearprice = "";

                        lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                        //   lblorgpremium.Attributes.Add("class", "priceRs");
                    }

                    ////lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                    ////lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                    ////lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $" + pro_monthprice);

                    lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr2[0].ItemArray[4].ToString());
                    lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr1[0].ItemArray[4].ToString());
                    lblppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", drpps[0].ItemArray[4].ToString());
                  //  lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $" + pro_monthprice);

                    //   lblfree.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");

                //}

                
                ////if (country.ToUpper() == "INDIA")
                ////{
                ////    //lblpremium.Text = strpremiumprice;
                ////    //lblpro.Text = strproprice;                   
                ////   // lblfree.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");
                ////    lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);
                ////    lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
                ////}
                ////else
                ////{
                ////    tdpricefaq.Visible = false;
                ////    string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                ////    SqlConnection strconn = new SqlConnection(connStr);
                ////    strconn.Open();
                ////    SqlCommand scom = new SqlCommand("sp_getPriceLicenseCountry", strconn);
                ////    scom.CommandType = CommandType.StoredProcedure;
                ////    scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = country;
                ////    scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = "";
                ////    SqlDataAdapter sda = new SqlDataAdapter(scom);
                ////    DataSet pricedetails = new DataSet();
                ////    sda.SelectCommand = scom;
                ////    sda.Fill(pricedetails);

                ////    string pro_monthlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();


                ////    string pro_yearlicencetype = pricedetails.Tables[0].Rows[1][1].ToString();


                ////    string prem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();

                ////    strconn.Close();

                ////    DataRow[] dr = pricedetails.Tables[0].Select("License_Type = '" + "PRO_MONTHLY'");

                ////    string pro_monthprice = "";
                ////    if (dr.Length > 0)
                ////    {
                ////        pro_monthprice = dr[0].ItemArray[4].ToString();
                ////    }
                ////    else
                ////    {
                ////        pro_monthprice = "";
                ////    }

                ////    DataRow[] dr1 = pricedetails.Tables[0].Select("License_Type = '" + "PRO_YEARLY'");

                ////    string pro_yearprice = "";
                ////    if (dr1.Length > 0)
                ////    {
                ////        pro_yearprice = dr1[0].ItemArray[4].ToString();
                ////    }
                ////    else
                ////    {
                ////        pro_yearprice = "";
                ////    }


                ////    DataRow[] dr2 = pricedetails.Tables[0].Select("License_Type = '" + "PREMIUM_YEARLY'");

                ////    string prem_yearprice = "";
                ////    if (dr2.Length > 0)
                ////    {
                ////        prem_yearprice = dr2[0].ItemArray[4].ToString();
                ////    }
                ////    else
                ////    {
                ////        prem_yearprice = "";
                ////    }

                ////    lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                ////    lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                ////    lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $" + pro_monthprice);
                ////  //  lblfree.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");
                ////}
                
            }
           
            
            else
            {

                if (!querystring.Contains("key"))
                {
                    utmsource = Request.QueryString["utm_source"].ToString();
                    utmterm = Request.QueryString["utm_term"].ToString();
                }
                else if (utmsource == null)
                {
                    utmsource = "";

                }
                

                //string NavUrl = Constants.LicenseType + "=" + "PRO_MONTHLY" + "&UserId=" + userId + "&partner=" + utmsource;
                //PayMonth.HRef = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl);

                string NavUrlbasic = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=Yes";
                basicviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlbasic);

                string NavUrlpro = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=Yes";
                proviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlpro);

                string NavUrlprem = Constants.LicenseType + "=" + "&UserId=" + userId + "&partner=" + utmsource + "&FromPartner=Yes";
                premviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlprem);

                dspartnerinfo = surcore.getPartnerInfo(utmsource);
                
                if ((Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) == 0) || (Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) < 0))
                {
                    utmsource = "Insighto";
                    utmterm = "";

                    dspartnerinfo = surcore.getPartnerInfo(utmsource);

                    //tdmsg.Visible = false;
                    //tdpartnerlogo.Visible = false;
                    topleftmsg.Visible = false;
                    toprightmsg.Visible = false;
                    tdrightpanel.Visible = false;
                    topinspromo.Visible = false;
                    td1.Visible = false;
                    td1.Width = "0%";
                    td2.Visible = true;
                    td2.Width = "25%";
                    divbasic.Attributes.Add("class", "price_1_blk");
                    divbasiccont.Attributes.Add("class", "price_1_blk_cont");
                    td3.Visible = true;
                    td3.Width = "25%";
                    divpro.Attributes.Add("class", "price_1_blk");
                    divprocont.Attributes.Add("class", "price_1_blk_cont");

                    tdppsprem.Visible = true;
                    tdppsprem.Width = "25%";
                    divppsprem.Attributes.Add("class", "price_1_blk");
                    divppspremcont.Attributes.Add("class", "price_1_blk_cont");


                    td4.Visible = true;
                    td4.Width = "25%";
                    divpremium.Attributes.Add("class", "price_1_blk");
                    divpremiumcont.Attributes.Add("class", "price_1_blk_cont");
                    td5.Visible = false;
                    td5.Width = "0%";
                    dvpricetagfree.Visible = false;
                    dvpricetagfreepps.Visible = false;
                    divstrikepro.Visible = false;
                    divstrikeprepps.Visible = false;
                    divstrikeprem.Visible = false;
                    dvprempricetagfree.Visible = false;


                    //if (country.ToUpper() == "INDIA")
                    //{
                    //    //lblpremium.Text = strpremiumprice;
                    //    //lblpro.Text = strproprice;
                    //  //  lblfree.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");
                    //    lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);
                    //    lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
                    //}
                    //else
                    //{
                        tdpricefaq.Visible = false;
                        string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                        SqlConnection strconn = new SqlConnection(connStr);
                        strconn.Open();
                        SqlCommand scom = new SqlCommand("sp_getPriceLicenseCountry", strconn);
                        scom.CommandType = CommandType.StoredProcedure;
                        scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = country;
                        scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = "";
                        SqlDataAdapter sda = new SqlDataAdapter(scom);
                        DataSet pricedetails = new DataSet();
                        sda.SelectCommand = scom;
                        sda.Fill(pricedetails);

                        //string pro_monthlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();


                        string pro_yearlicencetype = pricedetails.Tables[0].Rows[1][1].ToString();


                        //  string prem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();
                        string prem_yearlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();

                        string ppsprem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();

                        strconn.Close();

                        //DataRow[] dr = pricedetails.Tables[0].Select("License_Type = '" + "PRO_MONTHLY'");

                        //string pro_monthprice = "";
                        //if (dr.Length > 0)
                        //{
                        //    pro_monthprice = dr[0].ItemArray[4].ToString();
                        //}
                        //else
                        //{
                        //    pro_monthprice = "";
                        //}

                        DataRow[] dr1 = pricedetails.Tables[0].Select("License_Type = '" + "PPS_PRO'");

                        string pro_yearprice = "";
                        if (dr1.Length > 0)
                        {
                            pro_yearprice = dr1[0].ItemArray[4].ToString();
                        }
                        else
                        {
                            pro_yearprice = "";
                        }


                        DataRow[] drpps = pricedetails.Tables[0].Select("License_Type = '" + "PPS_PREMIUM'");

                        string ppsprem_yearprice = "";
                        if (drpps.Length > 0)
                        {
                            ppsprem_yearprice = drpps[0].ItemArray[4].ToString();
                        }
                        else
                        {
                            ppsprem_yearprice = "";

                           
                        }





                        DataRow[] dr2 = pricedetails.Tables[0].Select("License_Type = '" + "PREMIUM_YEARLY'");

                        string prem_yearprice = "";
                        if (dr2.Length > 0)
                        {
                            prem_yearprice = dr2[0].ItemArray[4].ToString();
                        }
                        else
                        {
                            prem_yearprice = "";
                        }

                        lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                        lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                        lblppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", ppsprem_yearprice);
                      //  lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $" + pro_monthprice);
                       // lblfree.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");
                    //}
                }
                else
                {


                    if (utmsource.ToUpper() == "INSIGHTO")
                    {

                      
                        topleftmsg.Visible = false;
                        toprightmsg.Visible = false;

                        shinebullet.Visible = false;
                        insightobullet.Visible = true;
                        shinesignup.Visible = false;
                        insightosignup.Visible = true;

                        td1.Visible = false;
                        td1.Width = "0%";
                        td2.Visible = true;
                        td2.Align = "Left";
                        td2.Width = "25%";
                        tdpromonth.Visible = true;
                        tdpricefaq.Visible = true;
                        tdonlyaplan.Visible = false;
                        tdfreespace.Visible = false;
                        divbasic.Attributes.Add("class", "price_1_blkpromo");
                        divbasiccont.Attributes.Add("class", "price_1_blk_contpromo");



                        td3.Visible = true;
                        td3.Align = "left";
                        td3.Width = "25%";
                        divpro.Attributes.Add("class", "price_1_blkpromo");
                        divprocont.Attributes.Add("class", "price_1_blk_contpromo");


                        tdppsprem.Visible = true;
                        tdppsprem.Align = "left";
                        tdppsprem.Width = "25%";
                        divppsprem.Attributes.Add("class", "price_1_blkpromo");
                        divppspremcont.Attributes.Add("class", "price_1_blk_contpromo");

                        td4.Visible = true;
                        td4.Align = "left";
                        td4.Width = "25%";
                        divpremium.Attributes.Add("class", "price_1_blkpromo");
                        divpremiumcont.Attributes.Add("class", "price_1_blk_contpromo");
                        td5.Visible = true;
                        td5.Align = "right";
                        td5.Width = "0%";

                        dvpricetagfree.Visible = true;
                        dvpricetagfreepps.Visible = true;
                        divstrikepro.Visible = true;
                        divstrikeprepps.Visible = true;
                        divstrikeprem.Visible = true;
                        dvprempricetagfree.Visible = true;

                        basicbr1.Visible = true;
                        basicbr2.Visible = true;
                        basicbr3.Visible = true;
                        basicbr4.Visible = true;
                        basicbr5.Visible = true;
                        basicbr6.Visible = true;
                        // brprempersonalize.Visible = true;
                        brpremreportindata.Visible = true;
                        brviresp.Visible = true;
                        prembr1.Visible = true;

                        //brbasicpremed.Visible = true;
                       // brbasicreportindata.Visible = true;
                        brbasicviresp.Visible = true;
                        //brbasicpersonalize.Visible = true;
                      //  probr1.Visible = true;
                        probr2.Visible = true;
                        probr3.Visible = true;

                        int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                        int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());

                        string strproprice1;
                        string strpremiumprice1;

                        //if (country.ToUpper() == "INDIA")
                        //{
                        //    int promovalpro = Convert.ToInt32(strproprice.Replace(",", "")) - (prodisval * Convert.ToInt32(strproprice.Replace(",", ""))) / 100;
                        //    strproprice1 = string.Format("{0:N0}", Convert.ToInt32(promovalpro.ToString()));

                        //    int promovalprem = Convert.ToInt32(strpremiumprice.Replace(",", "")) - (premdisval * Convert.ToInt32(strpremiumprice.Replace(",", ""))) / 100 - 40;
                        //    strpremiumprice1 = string.Format("{0:N0}", Convert.ToInt32(promovalprem.ToString()));

                        //    //lblorgpro.Text = strproprice;
                        //   // lblorgpremium.Text = strpremiumprice;
                        //    //lblpremium.Text = strpremiumprice1;
                        //    //lblpro.Text = strproprice1;

                        //    lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice1);
                        //    lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice1);
                        //  //  lblfree.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");
                        //    lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
                        //    lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);

                        //}

                        //else
                        //{
                            tdpricefaq.Visible = false;
                            payinrupeesinsighto.Visible = false;
                            payinrupeesshine.Visible = false;
                            phonesupportinsighto.Visible = false;
                            phonesupportshine.Visible = false;
                            pricesUSDinsighto.Visible = true;
                            pricesUSDshine.Visible = true;
                            paymethodinsighto.InnerText = "Convenient payment options available";
                            paymethodshine.InnerText = "Convenient payment options available";

                            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                            SqlConnection strconn = new SqlConnection(connStr);
                            strconn.Open();
                            SqlCommand scom = new SqlCommand("sp_getPriceLicenseCountry", strconn);
                            scom.CommandType = CommandType.StoredProcedure;
                            scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = country;
                            scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = "";
                            SqlDataAdapter sda = new SqlDataAdapter(scom);
                            DataSet pricedetails = new DataSet();
                            sda.SelectCommand = scom;
                            sda.Fill(pricedetails);

                            //string pro_monthlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();


                            string pro_yearlicencetype = pricedetails.Tables[0].Rows[1][1].ToString();


                            //  string prem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();
                            string prem_yearlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();


                            string ppsprem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();
                            strconn.Close();

                            //DataRow[] dr = pricedetails.Tables[0].Select("License_Type = '" + "PRO_MONTHLY'");

                            //string pro_monthprice = "";



                            //if (dr.Length > 0)
                            //{
                            //    pro_monthprice = dr[0].ItemArray[4].ToString();
                            //}
                            //else
                            //{
                            //    pro_monthprice = "";
                            //}


                            DataRow[] dr1 = pricedetails.Tables[0].Select("License_Type = '" + "PPS_PRO'");

                            string pro_yearprice = "";
                            if (dr1.Length > 0)
                            {
                                double promovalpro = Convert.ToDouble(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToDouble(dr1[0].ItemArray[4].ToString())) / 100;
                                //int promovalpro = Convert.ToInt32(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToInt32(dr1[0].ItemArray[4].ToString())) / 100;
                                pro_yearprice = Math.Round(promovalpro).ToString();


                               
                                lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr1[0].ItemArray[4].ToString());
                                //lblorgpro.Attributes.Add("class", "priceRs");
                            }
                            else
                            {
                                pro_yearprice = "";

                                lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                              //  lblorgpro.Attributes.Add("class", "priceRs");
                            }


                            DataRow[] drpps = pricedetails.Tables[0].Select("License_Type = '" + "PPS_PREMIUM'");

                            string ppsprem_yearprice = "";
                            if (drpps.Length > 0)
                            {
                                double promovalppsprem = Convert.ToDouble(drpps[0].ItemArray[4].ToString()) - (prodisval * Convert.ToDouble(drpps[0].ItemArray[4].ToString())) / 100;
                                //int promovalpro = Convert.ToInt32(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToInt32(dr1[0].ItemArray[4].ToString())) / 100;
                                ppsprem_yearprice = Math.Round(promovalppsprem).ToString();

                                lblorgpppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", drpps[0].ItemArray[4].ToString());
                                //lblorgpro.Attributes.Add("class", "priceRs");
                            }
                            else
                            {
                                ppsprem_yearprice = "";

                                lblorgpppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                                //  lblorgpro.Attributes.Add("class", "priceRs");
                            }

                            DataRow[] dr2 = pricedetails.Tables[0].Select("License_Type = '" + "PREMIUM_YEARLY'");

                            string prem_yearprice = "";
                            if (dr2.Length > 0)
                            {
                                double promovalprem = Convert.ToDouble(dr2[0].ItemArray[4].ToString()) - (premdisval * Convert.ToDouble(dr2[0].ItemArray[4].ToString())) / 100;

                                prem_yearprice = Math.Round(promovalprem).ToString();

                                lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr2[0].ItemArray[4].ToString());
                              //  lblorgpremium.Attributes.Add("class", "priceRs");
                            }
                            else
                            {
                                prem_yearprice = "";

                                lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                             //   lblorgpremium.Attributes.Add("class", "priceRs");
                            }

                            lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                            lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                            lblppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", ppsprem_yearprice);
                         //   lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $" + pro_monthprice);
                         //   lblfree.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");

                        //}

                    }

                    else if (utmsource.ToUpper() == "SHINE")
                    {

                        dspartnerinfo = surcore.getPartnerInfo(utmsource);

                        td2.Visible = false;
                        tdpromonth.Visible = true;
                        tdpricefaq.Visible = false;
                        topinspromo.Visible = false;

                        shinebullet.Visible = true;
                        insightobullet.Visible = false;
                        shinesignup.Visible = true;
                        insightosignup.Visible = false;
                        probr4.Visible = true;
                        //tdmsg.Visible = true;
                        //tdmsg.ColSpan = 2;


                        //lblpromomsg.Text = "<h3>Make your online employee surveys swifter and easier.</h3>";
                        //lblpromomsg1.Text = "<h4>Now for a lot less!</h4>";

                        //tdpartnerlogo.Visible = true;


                        int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                        int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());

                        string strproprice1 = "";
                        string strpremiumprice1 = "";

                        //if (country.ToUpper() == "INDIA")
                        //{
                        //    int promovalpro = Convert.ToInt32(strproprice.Replace(",", "")) - (prodisval * Convert.ToInt32(strproprice.Replace(",", ""))) / 100;
                        //    strproprice1 = promovalpro.ToString();

                        //    string compro = string.Format("{0:N0}", Convert.ToInt32(strproprice1));

                        //    int promovalprem = Convert.ToInt32(strpremiumprice.Replace(",", "")) - (premdisval * Convert.ToInt32(strpremiumprice.Replace(",", ""))) / 100 - 40;
                        //    strpremiumprice1 = promovalprem.ToString();

                        //    string comprem = string.Format("{0:N0}", Convert.ToInt32(strpremiumprice1));

                        //    //lblorgpro.Text = strproprice;
                        //    //lblorgpremium.Text = strpremiumprice;
                        //    //lblpremium.Text = comprem;
                        //    //lblpro.Text = compro;

                        //    lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", comprem);
                        //    lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", compro);

                        //    lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
                        //    lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);


                        //}

                        //else
                        //{
                            tdpricefaq.Visible = true;
                            payinrupeesshine.Visible = false;
                            payinrupeesinsighto.Visible = false;
                            paymethodinsighto.InnerText = "Convenient payment options available";
                            paymethodshine.InnerText = "Convenient payment options available";
                            phonesupportinsighto.Visible = false;
                            phonesupportshine.Visible = false;
                            pricesUSDinsighto.Visible = true;
                            pricesUSDshine.Visible = true;


                            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                            SqlConnection strconn = new SqlConnection(connStr);
                            strconn.Open();
                            SqlCommand scom = new SqlCommand("sp_getPriceLicenseCountry", strconn);
                            scom.CommandType = CommandType.StoredProcedure;
                            scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = country;
                            scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = "";
                            SqlDataAdapter sda = new SqlDataAdapter(scom);
                            DataSet pricedetails = new DataSet();
                            sda.SelectCommand = scom;
                            sda.Fill(pricedetails);

                            //string pro_monthlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();


                            string pro_yearlicencetype = pricedetails.Tables[0].Rows[1][1].ToString();


                            //  string prem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();
                            string prem_yearlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();

                            string ppsprem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();

                            strconn.Close();

                            //DataRow[] dr = pricedetails.Tables[0].Select("License_Type = '" + "PRO_MONTHLY'");

                            //string pro_monthprice = "";



                            //if (dr.Length > 0)
                            //{
                            //    pro_monthprice = dr[0].ItemArray[4].ToString();
                            //}
                            //else
                            //{
                            //    pro_monthprice = "";
                            //}


                            DataRow[] dr1 = pricedetails.Tables[0].Select("License_Type = '" + "PPS_PRO'");

                            string pro_yearprice = "";
                            if (dr1.Length > 0)
                            {
                                double promovalpro = Convert.ToDouble(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToDouble(dr1[0].ItemArray[4].ToString())) / 100;
                                pro_yearprice = Math.Round(promovalpro).ToString();

                                lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr1[0].ItemArray[4].ToString());
                                // lblorgpro.Attributes.Add("class", "priceRs");
                            }
                            else
                            {
                                pro_yearprice = "";

                                lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                                // lblorgpro.Attributes.Add("class", "priceRs");
                            }


                            DataRow[] drpps = pricedetails.Tables[0].Select("License_Type = '" + "PPS_PREMIUM'");

                            string ppsprem_yearprice = "";
                            if (drpps.Length > 0)
                            {
                                double promovalppsprem = Convert.ToDouble(drpps[0].ItemArray[4].ToString()) - (prodisval * Convert.ToDouble(drpps[0].ItemArray[4].ToString())) / 100;
                                //int promovalpro = Convert.ToInt32(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToInt32(dr1[0].ItemArray[4].ToString())) / 100;
                                ppsprem_yearprice = Math.Round(promovalppsprem).ToString();

                                lblorgpppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", drpps[0].ItemArray[4].ToString());
                                //lblorgpro.Attributes.Add("class", "priceRs");
                            }
                            else
                            {
                                ppsprem_yearprice = "";

                                lblorgpppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                                //  lblorgpro.Attributes.Add("class", "priceRs");
                            }



                            DataRow[] dr2 = pricedetails.Tables[0].Select("License_Type = '" + "PREMIUM_YEARLY'");

                            string prem_yearprice = "";
                            if (dr2.Length > 0)
                            {
                                double promovalprem = Convert.ToDouble(dr2[0].ItemArray[4].ToString()) - (premdisval * Convert.ToDouble(dr2[0].ItemArray[4].ToString())) / 100;

                                prem_yearprice = Math.Round(promovalprem).ToString();

                                lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr2[0].ItemArray[4].ToString());
                                // lblorgpremium.Attributes.Add("class", "priceRs");
                            }
                            else
                            {
                                prem_yearprice = "";

                                lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                                // lblorgpremium.Attributes.Add("class", "priceRs");
                            }

                            lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                            lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                            lblppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", ppsprem_yearprice);
                          //  lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $" + pro_monthprice);
                            // lblfree.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");




                        //}



                        td1.Visible = false;
                        td1.Width = "0%";

                        td3.Width = "25%";
                        tdonlyaplan.Visible = false;
                        divpro.Attributes.Add("class", "price_1_blkpartner");
                        divprocont.Attributes.Add("class", "price_1_blk_contpartner");

                        tdppsprem.Width = "25%";
                        tdonlyaplan.Visible = false;
                        divppsprem.Attributes.Add("class", "price_1_blkpartner");
                        divppspremcont.Attributes.Add("class", "price_1_blk_contpartner");

                        td4.Visible = true;
                        td4.Width = "25%";
                        divpremium.Attributes.Add("class", "price_1_blkpartner");
                        divpremiumcont.Attributes.Add("class", "price_1_blk_contpartner");
                        td5.Visible = true;
                        td5.Width = "0%";

                        dvpricetagfree.Visible = true;
                        dvpricetagfreepps.Visible = true;
                        divstrikepro.Visible = true;
                        divstrikeprepps.Visible = true;
                        divstrikeprem.Visible = true;
                        dvprempricetagfree.Visible = true;

                    }
                    else
                    {
                    utmsource = "";
                   // utmsource = "Insighto";
                    utmterm = "";

                    //string NavUrl1 = Constants.LicenseType + "=" + "PRO_MONTHLY" + "&UserId=" + userId;
                    //PayMonth.HRef = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl1);

                    string NavUrlbasic1 = Constants.LicenseType + "=" + "&UserId=" + userId + "&FromPartner=No";
                    basicviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlbasic1);

                    string NavUrlpro1 = Constants.LicenseType + "=" + "&UserId=" + userId + "&FromPartner=No";
                    proviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlpro1);

                    string NavUrlprem1 = Constants.LicenseType + "=" + "&UserId=" + userId + "&FromPartner=No";
                    premviewall.HRef = EncryptHelper.EncryptQuerystring("~/features.aspx", NavUrlprem1);
                

                   topleftmsg.Visible = false;
                   toprightmsg.Visible = false;

                   shinebullet.Visible = false;
                   insightobullet.Visible = false;
                   shinesignup.Visible = false;
                   insightosignup.Visible = true;

                   td1.Visible = false;
                   td1.Width = "5%";
                td2.Visible = true;
                td2.Align = "Left";
                td2.Width = "30%";
                tdpromonth.Visible = true;
                tdpricefaq.Visible = true;
                tdonlyaplan.Visible = false;
                tdfreespace.Visible = false;
                divbasic.Attributes.Add("class", "price_1_blkpromo1");
                divbasiccont.Attributes.Add("class", "price_1_blk_contpromo1");

                lblfree.Attributes.Add("class", "freetextl");
                Label1.Attributes.Add("class", "freetextl1");

                td3.Visible = true;
                td3.Align = "left";
                td3.Width = "30%";
                divpro.Attributes.Add("class", "price_1_blkpromo1");
                divprocont.Attributes.Add("class", "price_1_blk_contpromo1");

                tdppsprem.Visible = true;
                tdppsprem.Align = "left";
                tdppsprem.Width = "30%";
                divppsprem.Attributes.Add("class", "price_1_blkpromo1");
                divppspremcont.Attributes.Add("class", "price_1_blk_contpromo1");

                td4.Visible = true;
                td4.Align = "left";
                td4.Width = "30%";
                divpremium.Attributes.Add("class", "price_1_blkpromo1");
                divpremiumcont.Attributes.Add("class", "price_1_blk_contpromo1");
                td5.Visible = true;
                td5.Align = "right";
                td5.Width = "5%";
                
                dvpricetagfree.Visible = false;
                dvpricetagfreepps.Visible = false;
                divstrikepro.Visible = false;
                divstrikeprepps.Visible = false;
                divstrikeprem.Visible = false;
                dvprempricetagfree.Visible = false;

                probr4.Visible = true;

                // brprempersonalize.Visible = true;

                basictr.Visible = true;
               

                //brbasicpremed.Visible = true;
                // brbasicreportindata.Visible = true;
                brbasicviresp.Visible = true;
                //brbasicpersonalize.Visible = true;
                //  probr1.Visible = true;
                

                int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());

                string strproprice1;
                string strpremiumprice1;


                //if (country.ToUpper() == "INDIA")
                //{
                //    int promovalpro = Convert.ToInt32(strproprice.Replace(",", "")) - (prodisval * Convert.ToInt32(strproprice.Replace(",", ""))) / 100;
                //    strproprice1 = string.Format("{0:N0}", Convert.ToInt32(promovalpro.ToString()));

                //    int promovalprem = Convert.ToInt32(strpremiumprice.Replace(",", "")) - (premdisval * Convert.ToInt32(strpremiumprice.Replace(",", ""))) / 100 - 40;
                //    strpremiumprice1 = string.Format("{0:N0}", Convert.ToInt32(promovalprem.ToString()));

                //    //lblorgpro.Text = strproprice;
                //    // lblorgpremium.Text = strpremiumprice;
                //    //lblpremium.Text = strpremiumprice1;
                //    //lblpro.Text = strproprice1;
                //    lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
                //    lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);
                //    lblpremium.Text = lblorgpremium.Text;
                //    lblpro.Text = lblorgpro.Text;

                //    //lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice1);
                //    //lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice1);
                //    //  lblfree.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");
                    

                //}

                //else
                //{
                    tdpricefaq.Visible = false;
                    payinrupeesinsighto.Visible = false;
                    payinrupeesshine.Visible = false;
                    phonesupportinsighto.Visible = false;
                    phonesupportshine.Visible = false;
                    pricesUSDinsighto.Visible = true;
                    pricesUSDshine.Visible = true;
                    paymethodinsighto.InnerText = "Convenient payment options available";
                    paymethodshine.InnerText = "Convenient payment options available";

                    string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                    SqlConnection strconn = new SqlConnection(connStr);
                    strconn.Open();
                    SqlCommand scom = new SqlCommand("sp_getPriceLicenseCountry", strconn);
                    scom.CommandType = CommandType.StoredProcedure;
                    scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = country;
                    scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = "";
                    SqlDataAdapter sda = new SqlDataAdapter(scom);
                    DataSet pricedetails = new DataSet();
                    sda.SelectCommand = scom;
                    sda.Fill(pricedetails);

                    //string pro_monthlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();


                    string pro_yearlicencetype = pricedetails.Tables[0].Rows[1][1].ToString();


                    //  string prem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();
                    string prem_yearlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();

                    string ppsprem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();


                    strconn.Close();

                    //DataRow[] dr = pricedetails.Tables[0].Select("License_Type = '" + "PRO_MONTHLY'");

                    //string pro_monthprice = "";



                    //if (dr.Length > 0)
                    //{
                    //    pro_monthprice = dr[0].ItemArray[4].ToString();
                    //}
                    //else
                    //{
                    //    pro_monthprice = "";
                    //}


                    DataRow[] dr1 = pricedetails.Tables[0].Select("License_Type = '" + "PPS_PRO'");

                    string pro_yearprice = "";
                    if (dr1.Length > 0)
                    {
                        double promovalpro = Convert.ToDouble(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToDouble(dr1[0].ItemArray[4].ToString())) / 100;
                        //int promovalpro = Convert.ToInt32(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToInt32(dr1[0].ItemArray[4].ToString())) / 100;
                        pro_yearprice = Math.Round(promovalpro).ToString();

                        lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr1[0].ItemArray[4].ToString());
                        //lblorgpro.Attributes.Add("class", "priceRs");
                    }
                    else
                    {
                        pro_yearprice = "";

                        lblorgpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                        //  lblorgpro.Attributes.Add("class", "priceRs");
                    }

                    DataRow[] drpps = pricedetails.Tables[0].Select("License_Type = '" + "PPS_PREMIUM'");

                    string ppsprem_yearprice = "";
                    if (drpps.Length > 0)
                    {
                        double promovalppsprem = Convert.ToDouble(drpps[0].ItemArray[4].ToString()) - (prodisval * Convert.ToDouble(drpps[0].ItemArray[4].ToString())) / 100;
                        //int promovalpro = Convert.ToInt32(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToInt32(dr1[0].ItemArray[4].ToString())) / 100;
                        ppsprem_yearprice = Math.Round(promovalppsprem).ToString();

                        lblorgpppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", drpps[0].ItemArray[4].ToString());
                        //lblorgpro.Attributes.Add("class", "priceRs");
                    }
                    else
                    {
                        ppsprem_yearprice = "";

                        lblorgpppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                        //  lblorgpro.Attributes.Add("class", "priceRs");
                    }



                    DataRow[] dr2 = pricedetails.Tables[0].Select("License_Type = '" + "PREMIUM_YEARLY'");

                    string prem_yearprice = "";
                    if (dr2.Length > 0)
                    {
                        double promovalprem = Convert.ToDouble(dr2[0].ItemArray[4].ToString()) - (premdisval * Convert.ToDouble(dr2[0].ItemArray[4].ToString())) / 100;

                        prem_yearprice = Math.Round(promovalprem).ToString();

                        lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr2[0].ItemArray[4].ToString());
                        //  lblorgpremium.Attributes.Add("class", "priceRs");
                    }
                    else
                    {
                        prem_yearprice = "";

                        lblorgpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "");
                        //   lblorgpremium.Attributes.Add("class", "priceRs");
                    }

                    lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr2[0].ItemArray[4].ToString());
                    lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", dr1[0].ItemArray[4].ToString());
                    lblppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", drpps[0].ItemArray[4].ToString());


                    //lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                    //lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                    //lblppsprem.Text = string.Format("{0}{1}{2}{3}", "", "$", "", ppsprem_yearprice);
                  //  lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $" + pro_monthprice);
                    //   lblfree.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");

                //}


                    }
                }
            }


            //if (country.ToUpper() == "INDIA")
            //{
            //    lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);
            //    lblpremium.Attributes.Add("class", "priceRs");
            //    lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
            //    lblpro.Attributes.Add("Class", "priceRs");
            //    lblfree.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");

            //}

            //else
            //{

            //    string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            //    SqlConnection strconn = new SqlConnection(connStr);
            //    strconn.Open();
            //    SqlCommand scom = new SqlCommand("sp_getPriceLicenseCountry", strconn);
            //    scom.CommandType = CommandType.StoredProcedure;
            //    scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = country;
            //    scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = "";
            //    SqlDataAdapter sda = new SqlDataAdapter(scom);
            //    DataSet pricedetails = new DataSet();
            //    sda.SelectCommand = scom;
            //    sda.Fill(pricedetails);
              
            //    string pro_monthlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();

              
            //    string pro_yearlicencetype = pricedetails.Tables[0].Rows[1][1].ToString();
              
                
            //    string prem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();
             
            //    strconn.Close();

            //     DataRow[] dr = pricedetails.Tables[0].Select("License_Type = '" + "PRO_MONTHLY'");

            //     string pro_monthprice = "";
            //     if (dr.Length > 0)
            //     {
            //         pro_monthprice = dr[0].ItemArray[4].ToString();
            //     }
            //     else
            //     {
            //         pro_monthprice = "";
            //     }


            //     DataRow[] dr1 = pricedetails.Tables[0].Select("License_Type = '" + "PRO_YEARLY'");

            //     string pro_yearprice = "";
            //     if (dr1.Length > 0)
            //     {
            //         pro_yearprice = dr1[0].ItemArray[4].ToString();
            //     }
            //     else
            //     {
            //         pro_yearprice = "";
            //     }


            //     DataRow[] dr2 = pricedetails.Tables[0].Select("License_Type = '" + "PREMIUM_YEARLY'");

            //     string prem_yearprice = "";
            //     if (dr2.Length > 0)
            //     {
            //         prem_yearprice = dr2[0].ItemArray[4].ToString();
            //     }
            //     else
            //     {
            //         prem_yearprice = "";
            //     }

            //    lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
            //    lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
            //    lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $" +pro_monthprice);
            //    lblfree.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");
            //}


        }

   

    private string GetCurrencySymbol(string countryName)
    {
        var ci = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.EnglishName.Contains(countryName)).FirstOrDefault();
        return ci.NumberFormat.CurrencySymbol;
    }

    private double GetPriceByCountryName(string countryNAme, string subscription)
    {
        var price = ServiceFactory<OrderInfoService>.Instance.GetPriceByCountryName(countryNAme);
        if (subscription == "MONTHLY")
            return Convert.ToDouble(price.MONTHLY_PRICE);
        else
            return Convert.ToDouble(price.YEARLY_PRICE);
    }
    protected void button_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/In/SignUpFree.aspx");
    }
    protected void button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/In/SignUpFree.aspx");
    }
   
    protected void btnpro1_Click(object sender, EventArgs e)
    {
        if (querystring != "")        
        {           
            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            
            //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes";
            //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            string NavUrl = Constants.LicenseType + "=" + "Publisher_Monthly" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes";
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
                //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
                string NavUrl = Constants.LicenseType + "=" + "Publisher_Monthly " + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                //string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&FromPartner=No";
                //Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
                string NavUrl = Constants.LicenseType + "=" + "Publisher_Monthly" + "&UserId=" + userId + "&FromPartner=No";
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }
    protected void btnpro2_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        
        {

            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes";
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userId + "&FromPartner=No";
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }
    protected void btnpremium1_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        
        {

            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes";
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&FromPartner=No";
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }

    }
    protected void btnpremium2_Click(object sender, EventArgs e)
    {
        if (querystring != "")        
        {

            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes";
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes";
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&FromPartner=No";
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }

    protected void WriteClicked()
    {

        //HttpCookie aCookie = new HttpCookie("lastVisit");
        //aCookie.Value = DateTime.Now.ToString();
        //aCookie.Expires = DateTime.Now.AddDays(1);
        //Response.Cookies.Add(aCookie);


        //Create a new cookie, passing the name into the constructor
        HttpCookie acookie = new HttpCookie("Partner");

        //Set the cookies value
        acookie.Value = Request.QueryString["utm_source"].ToString();


         if (querystring != "")
        
        {
            acookie.Values["utm_source"] = Request.QueryString["utm_source"].ToString();
            acookie.Values["utm_term"] = Request.QueryString["utm_term"].ToString();
        }
        else
        {
            acookie.Values["utm_source"] = "";
            acookie.Values["utm_term"] = "";
        }

        //Set the cookie to expire in 1 minute
        //DateTime dtNow = DateTime.Now;
        //TimeSpan tsMinute = new TimeSpan(1, 1, 1, 1);
        //cookie.Expires = dtNow + tsMinute;

        Session["Category"] = acookie.Values["utm_term"];

      //  acookie.Expires = DateTime.Now.AddDays(30);
       // acookie.Expires = Convert.ToDateTime("9/15/2013 4:00:05 PM");
        //Add the cookie
        Response.Cookies.Add(acookie);

       // Response.Write("Cookie written. <br><hr>");
    }
    protected void btnppsprem2_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        {

            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes&surveytype=" + surveytype;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes&surveytype=" + surveytype;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&FromPartner=No&surveytype=" + surveytype;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }
    protected void btnppsprem_Click(object sender, EventArgs e)
    {
        if (querystring != "")
        {

            if ((strsource == null) || (strsource == ""))
            {
                strsource = utmsource;
            }
            string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&utm_source=" + strsource + "&utm_term=" + strterm + "&FromPartner=Yes&surveytype=" + surveytype;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            if (utmsource != "")
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&utm_source=" + utmsource + "&utm_term=" + utmterm + "&FromPartner=Yes&surveytype=" + surveytype;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
            else
            {
                string NavUrl = Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userId + "&FromPartner=No&surveytype=" + surveytype;
                Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
            }
        }
    }
}