﻿using System;
using System.Collections;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using System.Data.SqlClient;
using System.Data;

public partial class partnersfeaturesall : System.Web.UI.Page
{
    #region "Variables"
    Hashtable typeHt = new Hashtable();
    int userId = 0;
    string transType = "New";
    string country;
    private string licenseType = "";
    LoggedInUserInfo loggedInUserInfo;
    string strproprice = ConfigurationManager.AppSettings["proprice"];
    string strpremiumprice = ConfigurationManager.AppSettings["premiumprice"];
    private string  utmsource;
    private string utmsourcefeature;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

        string ipAddress = "";
     
        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
        {
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        }

        var country = Utilities.GetCountryName(ipAddress);

        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);

        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
        ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
        country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();


        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (typeHt.Contains("Type"))
                licenseType = typeHt["Type"].ToString();
            if (typeHt.Contains(Constants.USERID))
                userId = ValidationHelper.GetInteger(typeHt[Constants.USERID].ToString(), 0);
            if (typeHt.Contains("Flag"))
                transType = Convert.ToString(typeHt["Flag"]);
            if (typeHt.Contains("partner"))
            {
                if (Convert.ToString(typeHt["partner"]) != "")
                {
                    divpro_strike.Visible = true;
                    divpremium_strike.Visible = true;
                    basicprice_tag_header.Attributes.Add("class", "partnersbasicprice_tag_header");
                    proprice_tag_header.Attributes.Add("class", "partnersproprice_tag_header");

                    divpro_strike.Attributes.Add("class", "strike_txt");
                    divpro_strikebg.Attributes.Add("class", "strike_bg");

                    premiumprice_tag_header.Attributes.Add("class", "partnerspremiumprice_tag_header");
                    divpremium_strike.Attributes.Add("class", "strike_txt");
                    divpremium_strikebg.Attributes.Add("class", "strike_bg");
                }
                
            }
        }

        if (!IsPostBack)
        {
            //if (country.ToUpper() == "INDIA")
            //{
            //    if (Request.QueryString["key"] != null)
            //    {
            //        typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            //        if (typeHt.Contains("partner"))
            //        {
            //            if (Convert.ToString(typeHt["partner"]) != "")
            //            {
            //                SurveyCore surcore = new SurveyCore();
            //                DataSet dspartnerinfo;

            //                utmsource = typeHt["partner"].ToString();
            //                dspartnerinfo = surcore.getPartnerInfo(utmsource);

            //                int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
            //                int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());

            //                int promovalpro = Convert.ToInt32(strproprice.Replace(",", "")) - (prodisval * Convert.ToInt32(strproprice.Replace(",", ""))) / 100;
            //                string strproprice1 = string.Format("{0:N0}", Convert.ToInt32(promovalpro.ToString()));

            //                int promovalprem = Convert.ToInt32(strpremiumprice.Replace(",", "")) - (premdisval * Convert.ToInt32(strpremiumprice.Replace(",", ""))) / 100 - 40;
            //                string strpremiumprice1 = string.Format("{0:N0}", Convert.ToInt32(promovalprem.ToString()));

            //                // lblbasic.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");
            //                lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice1);
            //                lblprostrike.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
            //                lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice1);
            //                lblpremstrike.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);
            //            }
            //            else
            //            {
            //                lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
            //                lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);
            //            }
            //        }
            //        else
            //        {
            //           // lblbasic.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");
            //            lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
            //            lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);
            //        }
            //    }
            //    else
            //    {
            //       // lblbasic.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");
            //        lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strproprice);
            //        lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", strpremiumprice);
            //    }
            //}
            //else
            //{

              
              

                SurveyCore surcore = new SurveyCore();
                DataSet dspartnerinfo;

                utmsource = typeHt["partner"].ToString();
                dspartnerinfo = surcore.getPartnerInfo(utmsource);

                int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());


                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection strconn = new SqlConnection(connStr);
                strconn.Open();
                SqlCommand scom = new SqlCommand("sp_getPriceLicenseCountry", strconn);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = country;
                scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = "";
                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet pricedetails = new DataSet();
                sda.SelectCommand = scom;
                sda.Fill(pricedetails);
                string pro_monthlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();

                //  string pro_monthprice = pricedetails.Tables[0].Rows[0][4].ToString();
                string pro_yearlicencetype = pricedetails.Tables[0].Rows[1][1].ToString();
                // string pro_yearprice = pricedetails.Tables[0].Rows[1][4].ToString();

                string prem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();
                //  string prem_yearprice = pricedetails.Tables[0].Rows[2][4].ToString();
                strconn.Close();

                DataRow[] dr = pricedetails.Tables[0].Select("License_Type = '" + "PRO_MONTHLY'");

                string pro_monthprice = "";
                if (dr.Length > 0)
                {
                    pro_monthprice = dr[0].ItemArray[4].ToString();
                }
                else
                {
                    pro_monthprice = "";
                }


                DataRow[] dr1 = pricedetails.Tables[0].Select("License_Type = '" + "PRO_YEARLY'");

                string pro_yearprice = "";
                string pro_yearprice1 = "";
                if (dr1.Length > 0)
                {
                    double promovalpro = Convert.ToDouble(dr1[0].ItemArray[4].ToString()) - (prodisval * Convert.ToDouble(dr1[0].ItemArray[4].ToString())) / 100;                   
                    pro_yearprice1 = Math.Round(promovalpro).ToString();

                    pro_yearprice = dr1[0].ItemArray[4].ToString();
                }
                else
                {
                    pro_yearprice = "";
                }


                DataRow[] dr2 = pricedetails.Tables[0].Select("License_Type = '" + "PREMIUM_YEARLY'");

                string prem_yearprice = "";
                string prem_yearprice1 = "";
                if (dr2.Length > 0)
                {
                    double promovalprem = Convert.ToDouble(dr2[0].ItemArray[4].ToString()) - (premdisval * Convert.ToDouble(dr2[0].ItemArray[4].ToString())) / 100;

                    prem_yearprice1 = Math.Round(promovalprem).ToString();

                    prem_yearprice = dr2[0].ItemArray[4].ToString();
                }
                else
                {
                    prem_yearprice = "";
                }

                if (Request.QueryString["key"] != null)
                {
                    typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (typeHt.Contains("partner"))
                    {

                        if (Convert.ToString(typeHt["partner"]) != "")
                        {

                            lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice1);
                            lblprostrike.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                            lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice1);
                            lblpremstrike.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                        }
                        else
                        {
                            lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                            lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                        }
                    }
                    else
                    {
                        lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                        lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                    }
                }
                else
                {
                    lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                    lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                }
                //  Label1.Attributes.Add("Class", "monthlyHeading");
                // lnkBtnMonthly.Text = string.Format("{0}{1}{2}{3}", "SAVE ON ANNUAL PLAN - ", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), " ", GetPriceByCountryName(country, "Yearly"));
                // lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $" + pro_monthprice);

                // lblMonthly.Attributes.Add("Class", "monthlyHeading");

               // lblbasic.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");

            //}

            dvPageInfo.InnerHtml = Utilities.ResourceMessage("dvPageInfo");

        }
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            if (typeHt.Contains(Constants.USERID))
                userId = ValidationHelper.GetInteger(typeHt[Constants.USERID].ToString(), 0);
            if (typeHt.Contains("Flag"))
                transType = Convert.ToString(typeHt["Flag"]);
        }

        if (typeHt.Contains("partner"))
        {
            utmsourcefeature = typeHt["partner"].ToString();
        }
        else
        {
            utmsourcefeature = "";
        }
    }

    /// <summary>
    /// Sets the permissions.
    /// </summary>


    /// <summary>
    /// Formats the currency and symbol.
    /// </summary>

    /// <summary>
    /// Handles the Click event of the imgTryItFree control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>

    private string GetCurrencySymbol(string countryName)
    {
        var ci = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.EnglishName.Contains(countryName)).FirstOrDefault();
        return ci.NumberFormat.CurrencySymbol;
    }

    private double GetPriceByCountryName(string countryNAme, string subscription)
    {
        var price = ServiceFactory<OrderInfoService>.Instance.GetPriceByCountryName(countryNAme);
        if (subscription == "MONTHLY")
            return Convert.ToDouble(price.MONTHLY_PRICE);
        else
            return Convert.ToDouble(price.YEARLY_PRICE);
    }

    protected void button_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/In/SignUpFree.aspx");
    }
    protected void button1_Click(object sender, EventArgs e)
    {

        Response.Redirect("~/In/SignUpFree.aspx");

    }
    protected void btnpro1_Click(object sender, EventArgs e)
    {
       

        if (utmsourcefeature != "")
        {
            string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsourcefeature;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
    }
    protected void btnpro2_Click(object sender, EventArgs e)
    {
        if (utmsourcefeature != "")
        {
            string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsourcefeature;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
    }
    protected void btnpremium1_Click(object sender, EventArgs e)
    {
        if (utmsourcefeature != "")
        {
            string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsourcefeature;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
    }
    protected void btnpremium2_Click(object sender, EventArgs e)
    {
        if (utmsourcefeature != "")
        {
            string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId + "&utm_source=" + utmsourcefeature;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
        else
        {
            string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId;
            Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
        }
    }
}