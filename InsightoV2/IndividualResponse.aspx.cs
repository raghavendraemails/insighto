﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts.Web;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using sharp = iTextSharp.text;
using FeatureTypes;
using Insighto.Business.ValueObjects;
using System.Text;
using System.Linq;
using System.Threading;
using DevExpress.Web.ASPxClasses;
using System.Diagnostics;
using InfoSoftGlobal;
using System.Security.Cryptography;
using iTextSharp.text.html.simpleparser;
using System.Reflection;
using OfficeOpenXml;
using Office = Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.Runtime.InteropServices;
using Word = Microsoft.Office.Interop.Word;


public partial class IndividualResponse : SurveyPageBase
{
    int SurveyID = 0;
    DataSet ds_responsedetails = new DataSet();
    int i, ds_count;
    DataSet ds_excludedrespondents = new DataSet();
    DataSet ds_includedrespondents = new DataSet();
    SurveyCore surresponse = new SurveyCore();
    SurveyCore obj1 = new SurveyCore();
    SurveyCore surcore = new SurveyCore();
    Hashtable ht = new Hashtable();
    public int surveyID = 0;
    string strlicensetype;
    string strsurvytype;

    public string Key
    {
        get
        {
            return HttpUtility.UrlEncode(QueryStringHelper.GetString("Key"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
            {
                SurveyID = Convert.ToInt32(ht[Constants.SURVEYID]);
            }
        }

        
        DataSet dssurveytype = surcore.getSurveyType(SurveyID);
        if (dssurveytype.Tables[0].Rows[0][1].ToString() != "")
        {
            strlicensetype = dssurveytype.Tables[0].Rows[0][1].ToString();
            strsurvytype = dssurveytype.Tables[0].Rows[0][0].ToString();
        }
        else
        {
            strlicensetype = loggedInUserInfo.LicenseType;
        }

       
        //if (loggedInUserInfo.LicenseType == "FREE" || loggedInUserInfo.LicenseType == "PRO_MONTHLY" || loggedInUserInfo.LicenseType == "PRO_QUARTERLY" || loggedInUserInfo.LicenseType == "PRO_YEARLY")
        if (strlicensetype == "FREE" || strlicensetype == "PRO_MONTHLY" || strlicensetype == "PRO_QUARTERLY" || strlicensetype == "PRO_YEARLY")
        {
            
         //   ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('" + EncryptHelper.EncryptQuerystring(PathHelper.GetUpgradeLicenseURL().Replace("~/", ""), "&Flag=yes&pre=pre&surveyFlag=" + base.SurveyFlag) + "', 690, 515, 'yes');</script>", false);

            
            pnlMain.Enabled = false;
            pnlLoading.Enabled = false;
            ASPxPanel3.Enabled = false;
            rawdatavalid.Enabled = false;
            rawdatapartial.Enabled = false;
            lbn_ExcludeResponse.Enabled = false;
        }
        else
        {
            ASPxPanel3.Enabled = true;
            rawdatavalid.Enabled = true;
            rawdatapartial.Enabled = true;
            lbn_ExcludeResponse.Enabled = true;

            dvErrMsg.Visible = false;
            dvSuccessMsg.Visible = false;

            i = Convert.ToInt32(Session["ds_resp_row_id"]);
            lbl_message.Visible = false;
            if (!IsPostBack)
            {

                Session["resp_count"] = 0;
                Session["ds_resp_row_id"] = 0;

                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                    {
                        SurveyID = Convert.ToInt32(ht[Constants.SURVEYID]);
                        SelectedSurvey = surresponse.GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                    }
                }
                else if (SurveyBasicInfoView != null)
                {
                    SurveyID = SurveyBasicInfoView.SURVEY_ID;
                    SelectedSurvey = surresponse.GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                }

                if (strsurvytype == "PPS_PREMIUM")
                {
                    ds_responsedetails = obj1.GetIndiviudalRespIds1000(SurveyBasicInfoView.SURVEY_ID, false, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                }
                else
                {
                    ds_responsedetails = obj1.GetIndiviudalRespIds(SurveyBasicInfoView.SURVEY_ID, false, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                }

                if (ds_responsedetails != null && ds_responsedetails.Tables.Count > 0)
                {
                    if (ds_responsedetails.Tables[0].Rows.Count == 0)
                    {
                        ASPxPanel3.Visible = false;
                        lbl_message.Text = "No responses to display.";
                        lbl_message.Visible = true;
                        lbn_ExcludeResponse.Visible = false;
                    }
                    else
                    {
                        Individual_Res();
                        panelfunction();
                        ASPxPanel3.Visible = true;
                    }
                }
                CheckUserFeatureLinks();
            }
        }
    }

    #region "Method : SetFeatures"
    /// <summary>
    /// This method returns bool value, if the feature flag is one.
    /// </summary>
    /// <param name="strFeature"></param>
    /// <returns></returns>

    public int SetFeatures(string strFeature)
    {
        int iVal = 0;
        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var sessdet = session.GetLoginUserDetailsSession();
        if (sessdet != null)
        {

            var serviceFeature = ServiceFactory.GetService<FeatureService>();

            string strlicensetype;
            DataSet dssurveytype = surcore.getSurveyType(SurveyID);
            if (dssurveytype.Tables[0].Rows[0][1].ToString() != "")
            {
                strlicensetype = dssurveytype.Tables[0].Rows[0][1].ToString();
            }
            else
            {
                strlicensetype = sessdet.LicenseType;
            }


            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(strlicensetype));

            if (sessdet.UserId > 0 && listFeatures != null)
            {
                foreach (var listFeat in listFeatures)
                {

                    if (listFeat.Feature.Contains(strFeature))
                    {
                        iVal = (int)listFeat.Value;
                    }
                }
            }
        }

        else
        {
            Response.Redirect(PathHelper.GetUserLoginPageURL());
        }
        return iVal;
    }

    #endregion

    public void CheckUserFeatureLinks()
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (userDetails != null && userDetails.UserId > 0)
        {

            string Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag);
            if (SetFeatures("CUSTOMCHART") == 0)
            {
                //CustomReports.HRef = "#";
                //CustomReports.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
            else
            {
                string strCustomTab = EncryptHelper.EncryptQuerystring("CustomizeReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
               // CustomReports.HRef = strCustomTab;
            }
            if (SetFeatures("CROSS_TAB") == 0)
            {
                CrossTabRpts.HRef = "#";
                CrossTabRpts.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
            else
            {
                string strCrossTab = EncryptHelper.EncryptQuerystring("CrossTabReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
                CrossTabRpts.HRef = strCrossTab;
            }
            if (SetFeatures("RAWDATA_EXPORT") == 0)
            {
                rawdatavalid.Attributes.Add("Onclick", "javascript: OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                rawdatapartial.Attributes.Add("Onclick", "javascript: OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
            if (SetFeatures("EXCLUDE_BLANKRESPONSES") == 0)
            {
                chk_blankresponse.ClientSideEvents.CheckedChanged = "function(s, e) { OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');e.processOnServer = false;  }";
            }
            if (SetFeatures("TRACKEMAIL_ID") == 0)
            {
                chk_respondentemail.ClientSideEvents.CheckedChanged = "function(s, e) { OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');e.processOnServer = false;  }";
            }
            if (SetFeatures("PARTIALS") == 0)
            {
                chk_partialresponses.ClientSideEvents.CheckedChanged = "function(s, e) { OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');e.processOnServer = false;  }";
            }
        }
    }

    public void SetSubNaviLinks()
    {
        string strCrossTab = EncryptHelper.EncryptQuerystring("CrossTabReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
        string strCustomTab = EncryptHelper.EncryptQuerystring("CustomizeReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
        CrossTabRpts.HRef = strCrossTab;
       // CustomReports.HRef = strCustomTab;
    }
    public static string StripTagsRegex(string source)
    {
        return Regex.Replace(source, "<.*?>", string.Empty);
    }
    void Individual_Res()
    {
        SurveyCore cor = new SurveyCore();
        if (Request.QueryString["key"] != null)
        {
            Hashtable ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
           
            if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
            {
                SurveyID = Convert.ToInt32(ht[Constants.SURVEYID]);
                // Srini, 27/08/2013, this is an expensive operation and duplicate call which is best avoided
                if (SelectedSurvey == null)
                    SelectedSurvey = surresponse.GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                if (ds_responsedetails == null)
                    if (strsurvytype == "PPS_PREMIUM")
                    {
                        ds_responsedetails = obj1.GetIndiviudalRespIds1000(SurveyID, false, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                    }
                    else
                    {
                        ds_responsedetails = obj1.GetIndiviudalRespIds(SurveyID, false, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                    }
            }
        }

        filteroptionsfunction();
        if (ds_responsedetails != null && ds_responsedetails.Tables.Count > 0 && ds_responsedetails.Tables[0].Rows.Count > 0)
        {
            ds_count = ds_responsedetails.Tables[0].Rows.Count;
            ASPxPanel3.Visible = true;
            lbn_ExcludeResponse.Visible = true;
        }
        else
        {
            lbl_message.Text = "No responses to display.";
            lbl_message.Visible = true;
            ASPxPanel3.Visible = false;
            lbn_ExcludeResponse.Visible = false;
        }
        Session["resp_count"] = ds_count;
        int j = 0;
        if (Session["ds_resp_row_id"] != null)
        {
            j = Convert.ToInt32(Session["ds_resp_row_id"]);
            if (j == ds_count || j > ds_count)
            {
                j = 0;
                Session["ds_resp_row_id"] = 0;

            }
        }
        for (i = j; i < ds_count; i++)
        {
            //Survey sur = SelectedSurvey;
            if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
            {
                SelectedSurvey.surveyQues.Sort();
            }
           Insighto.Business.Charts.SurveyResponse surRes = null;

            if (SelectedSurvey.SURVEY_ID > 0 && ds_responsedetails != null && ds_responsedetails.Tables.Count > 0)
            {
                if (ds_responsedetails.Tables[0].Rows[i].ItemArray[0] != null)
                {
                    surRes = cor.GetSurveyResponses(SelectedSurvey.SURVEY_ID, Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i].ItemArray[0].ToString()));
                    respondent_idvalue.Value = ds_responsedetails.Tables[0].Rows[i].ItemArray[0].ToString();

                    if ((ds_responsedetails.Tables[0].Rows[i]["STATUS"] != DBNull.Value) && (ds_responsedetails.Tables[0].Rows[i]["DELETED"] != DBNull.Value) && (ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"] != DBNull.Value))
                    {
                        if (Convert.ToString(ds_responsedetails.Tables[0].Rows[i]["STATUS"]) == "Complete" && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["DELETED"]) == 0 && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"]) == 0)
                        {
                            if (SetFeatures("RESTEXCL_RESPTOCOMPLETES") == 0)
                            {
                                lbn_ExcludeResponse.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                            }
                            lbn_ExcludeResponse.Text = "Exclude this response";

                        }
                        else if ((ds_responsedetails.Tables[0].Rows[i]["STATUS"] != null) && (ds_responsedetails.Tables[0].Rows[i]["DELETED"] != null) && (ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"] != null) && Convert.ToString(ds_responsedetails.Tables[0].Rows[i]["STATUS"]) == "Partial" && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["DELETED"]) == 0 && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"]) == 0)
                        {
                            if (SetFeatures("RESTEXCL_RESPPARTTOCOMPLETES") == 0)
                            {
                                lbn_ExcludeResponse.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                            }
                            lbn_ExcludeResponse.Text = "Take this response to complete";

                        }
                        else if ((ds_responsedetails.Tables[0].Rows[i]["STATUS"] != null) && (ds_responsedetails.Tables[0].Rows[i]["DELETED"] != null) && (ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"] != null) && Convert.ToString(ds_responsedetails.Tables[0].Rows[i]["STATUS"]) == "Complete" && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["DELETED"]) == 1 && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"]) == 0)
                        {
                            if (SetFeatures("RESTEXCL_RESPTOCOMPLETES") == 0)
                            {
                                lbn_ExcludeResponse.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                            }
                            lbn_ExcludeResponse.Text = "Restore this response to complete";

                        }
                        else if ((ds_responsedetails.Tables[0].Rows[i]["STATUS"] != null) && (ds_responsedetails.Tables[0].Rows[i]["DELETED"] != null) && (ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"] != null) && Convert.ToString(ds_responsedetails.Tables[0].Rows[i]["STATUS"]) == "Complete" && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["DELETED"]) == 0 && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"]) == 1)
                        {
                            if (SetFeatures("RESTEXCL_RESPPARTTOCOMPLETES") == 0)
                            {
                                lbn_ExcludeResponse.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                            }
                            lbn_ExcludeResponse.Text = "Restore this response as partial";

                        }
                    }

                }
            }
            if (SelectedSurvey.surveyQues != null)
            {
                List<SurveyQuestion> QuesList = SelectedSurvey.surveyQues;
                int c = 0;
                int qid = 0;
                int flag = 1;
                int[] ans_id = new int[ds_count];
                int aid;
                int ques_count = 0;
                if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
                {
                    SelectedSurvey.surveyQues.Sort();
                    foreach (SurveyQuestion ques in QuesList)
                    {

                        ques_count = ques_count + 1;

                    }
                }
                if (surRes != null && surRes.ResponseQuestion.Count > 0)
                {
                    foreach (ResponseQuestion resp in surRes.ResponseQuestion)
                    {
                        c = c + 1;

                    }
                }
                string[] ques_array = new string[ques_count];
                int[] quesid_array = new int[ques_count];
                string[] ans_array = new string[ques_count];
                c = 0;

                if (surRes != null && surRes.ResponseQuestion.Count > 0)
                {
                    foreach (ResponseQuestion resp in surRes.ResponseQuestion)
                    {

                        if (resp.QUESTION_ID == qid)
                        {
                            flag = 0;

                        }
                        else
                        {
                            flag = 1;
                            qid = resp.QUESTION_ID;
                            c = c + 1;

                        }
                        aid = resp.ANSWER_ID;
                        if (QuesList != null)
                        {
                            foreach (SurveyQuestion ques in QuesList)
                            {
                                if (ques.QUESTION_ID == Convert.ToInt32(qid))
                                {

                                    if (flag == 1)
                                    {

                                        ques_array[c - 1] = System.Text.RegularExpressions.Regex.Replace(ques.QUESTION_LABEL, "<span[^>]*>", "");
                                        //ques_array[c - 1] = ques.QUESTION_LABEL;
                                        quesid_array[c - 1] = ques.QUESTION_ID;

                                    }


                                    if (aid != 0)
                                    {
                                        List<SurveyAnswers> anslist = ques.surveyAnswers;
                                        foreach (SurveyAnswers ans in anslist)
                                        {
                                            if (aid == Convert.ToInt32(ans.ANSWER_ID))
                                            {
                                                if (flag == 1)
                                                {
                                                    if (ques.QUESTION_TYPE != 20)
                                                    {
                                                        if (ques.QUESTION_TYPE != 4)
                                                        {
                                                            if ((ques.QUESTION_TYPE == 10) || (ques.QUESTION_TYPE == 11))
                                                            {
                                                                int ansopt = ans.ANSWER_OPTIONS.IndexOf("$--$");
                                                                string strcolopt = ans.ANSWER_OPTIONS.Substring(0, ansopt);
                                                                string stranspot = ans.ANSWER_OPTIONS.Substring(ansopt + 4);
                                                                ans_array[c - 1] = stranspot + " - " + strcolopt + "<p>" + resp.ANSWER_TEXT + "</p>";
                                                            }
                                                            else if (ques.QUESTION_TYPE == 12)
                                                            {
                                                                int ansopt = ans.ANSWER_OPTIONS.IndexOf("$--$");
                                                                string strcolopt = ans.ANSWER_OPTIONS.Substring(0, ansopt);
                                                                                                                        
                                                                string stranspot = ans.ANSWER_OPTIONS.Substring(ansopt + 4);
                                                                int ansopt2 = stranspot.IndexOf("$--$");

                                                                string subcol = stranspot.Substring(0, ansopt2);
                                                                string strrowanspot = stranspot.Substring(ansopt2 + 4);

                                                                ans_array[c - 1] = strrowanspot + " - " + strcolopt + " - " + subcol  + "<p>" + resp.ANSWER_TEXT + "</p>";
                                                            }
                                                            else if (ques.QUESTION_TYPE == 13)
                                                            {
                                                                ans_array[c - 1] = ans.ANSWER_OPTIONS.Replace("$--$", " ") + " - " + resp.ANSWER_TEXT + "<p></p>";
                                                            }
                                                            else if (ques.QUESTION_TYPE == 15)
                                                            {
                                                                ans_array[c - 1] = ans.ANSWER_OPTIONS.Replace("$--$", " ") + " - " +   resp.ANSWER_TEXT + "<p></p>";
                                                            }
                                                            else if (ques.QUESTION_TYPE == 9)
                                                            {
                                                                ans_array[c - 1] = ans.ANSWER_OPTIONS.Replace("$--$", " ") +  resp.ANSWER_TEXT + "<p></p>";
                                                            }
                                                            else
                                                            {
                                                                ans_array[c - 1] = ans.ANSWER_OPTIONS.Replace("$--$", " ") + "<p>" + resp.ANSWER_TEXT + "</p>";
                                                            }


                                                            //ans_array[c - 1] = ans.ANSWER_OPTIONS.Replace("$--$", " ");
                                                        }
                                                        else
                                                        {
                                                            ans_array[c - 1] = " " + ans.ANSWER_OPTIONS + " " + "<p>" + resp.ANSWER_TEXT + "</p>";
                                                            //ans_array[c - 1] = " " + ans.ANSWER_OPTIONS;
                                                        }

                                                    }
                                                    else
                                                        ans_array[c - 1] = ans.ANSWER_OPTIONS + "  :  " + resp.ANSWER_TEXT;
                                                       // ans_array[c - 1] = ans.ANSWER_OPTIONS;
                                                }
                                                else
                                                {
                                                    if (ques.QUESTION_TYPE == 20)
                                                        ans_array[c - 1] = ans_array[c - 1] + "<p>" + ans.ANSWER_OPTIONS + "  :  " + resp.ANSWER_TEXT + "</p>";
                                                       // ans_array[c - 1] = ans_array[c - 1] + "<p>" + ans.ANSWER_OPTIONS;
                                                    else
                                                    {
                                                        if ((ques.QUESTION_TYPE == 10) || (ques.QUESTION_TYPE == 11))
                                                        {
                                                            int ansopt = ans.ANSWER_OPTIONS.IndexOf("$--$");
                                                            string strcolopt = ans.ANSWER_OPTIONS.Substring(0, ansopt);
                                                            string stranspot = ans.ANSWER_OPTIONS.Substring(ansopt + 4);
                                                            ans_array[c - 1] = ans_array[c - 1] + stranspot + " - " + strcolopt + "<p>" + resp.ANSWER_TEXT + "</p>";
                                                        }
                                                        else if (ques.QUESTION_TYPE == 12)
                                                        {
                                                            int ansopt = ans.ANSWER_OPTIONS.IndexOf("$--$");
                                                            string strcolopt = ans.ANSWER_OPTIONS.Substring(0, ansopt);

                                                            string stranspot = ans.ANSWER_OPTIONS.Substring(ansopt + 4);
                                                            int ansopt2 = stranspot.IndexOf("$--$");

                                                            string subcol = stranspot.Substring(0, ansopt2);
                                                            string strrowanspot = stranspot.Substring(ansopt2 + 4);

                                                            ans_array[c - 1] = ans_array[c - 1] + strrowanspot + " - " + strcolopt + " - " + subcol + "<p>" + resp.ANSWER_TEXT + "</p>";
                                                        }
                                                        else if (ques.QUESTION_TYPE == 13)
                                                        {
                                                            ans_array[c - 1] = ans_array[c - 1] + ans.ANSWER_OPTIONS.Replace("$--$", " ") + " - " + resp.ANSWER_TEXT + "<p></p>";
                                                        }
                                                        else if (ques.QUESTION_TYPE == 15)
                                                        {
                                                            ans_array[c - 1] = ans_array[c - 1] + ans.ANSWER_OPTIONS.Replace("$--$", " ") + " - " + resp.ANSWER_TEXT + "<p></p>";
                                                        }
                                                        else if (ques.QUESTION_TYPE == 9)
                                                        {
                                                            ans_array[c - 1] = ans_array[c - 1] + ans.ANSWER_OPTIONS.Replace("$--$", " ") + resp.ANSWER_TEXT + "<p></p>";
                                                        }
                                                        else
                                                        {
                                                            ans_array[c - 1] = ans_array[c - 1] + ans.ANSWER_OPTIONS.Replace("$--$", " ") + "<p>" + resp.ANSWER_TEXT + "</p>";
                                                        }

                                                        
                                                        //ans_array[c - 1] = ans_array[c - 1] + ans.ANSWER_OPTIONS.Replace("$--$", " ");
                                                    }
                                                }

                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (flag == 1)
                                            ans_array[c - 1] = "<p>" + resp.ANSWER_TEXT + "</p>";
                                        else
                                            ans_array[c - 1] = ans_array[c - 1] + "<p>" + resp.ANSWER_TEXT + "</p>";
                                    }
                                    break;
                                }
                            }
                        }

                    }

                }
                int ques_resp_check_flag = 0;
                int quesNo = 1;
                if (QuesList != null && QuesList.Count > 0)
                {
                   rndpanel_surveyresponses.Controls.Clear();
                    foreach (SurveyQuestion ques in QuesList)
                    {

                        if (ques.QUESTION_TYPE != 16 && ques.QUESTION_TYPE != 17 && ques.QUESTION_TYPE != 18 && ques.QUESTION_TYPE != 19 && ques.QUESTION_TYPE != 21)
                        {
                            ques_resp_check_flag = 0;
                            for (int q = 0; q < ques_array.Length; q++)
                            {
                                if (ques.QUESTION_ID == quesid_array[q])
                                {
                                    HtmlTable ques_resp = new HtmlTable();
                                    ques_resp.Width = "100%";
                                    ques_resp.CellPadding = 1;
                                    ques_resp.CellSpacing = 0;
                                    ques_resp.Border = 1;
                                    ques_resp.BorderColor = "Purple";
                                    ques_resp.Align = "Center";
                                    

                                    HtmlTableRow quesrow = new HtmlTableRow();
                                    HtmlTableCell quescell = new HtmlTableCell();
                                    //quescell.BgColor = "#d6e7ff";
                                    quescell.Attributes.Add("style", "background:#F4F4F4; padding:10px 8px 10px 8px; font-size:14px;");
                                    //quescell.Height = "24px";

                                    System.Web.UI.WebControls.Label lbl_ques = new System.Web.UI.WebControls.Label();
                                    ques_array[q] = ques_array[q].Replace("<p>", "");
                                    ques_array[q] = ques_array[q].Replace("</p>", "");
                                    ques_array[q] = ques_array[q].Replace("&nbsp;", ""); 
                                    lbl_ques.Text = "Q" + quesNo + ". " + Convert.ToString(ques_array[q]);
                                    lbl_ques.CssClass = "indiquestxt";
                                    lbl_ques.EnableTheming = false;
                                    lbl_ques.BorderStyle = BorderStyle.None;
                                    quescell.Controls.Add(lbl_ques);
                                    quesrow.Cells.Add(quescell);
                                    ques_resp.Rows.Add(quesrow);

                                    //HtmlTableRow ansrow = new HtmlTableRow();
                                    //ansrow.Height = "40px";

                                    HtmlTableCell anscell = new HtmlTableCell();
                                    anscell.Width = "50%";
                                    anscell.Attributes.Add("style", "background:#F4F4F4; padding:10px 8px 10px 8px; font-size:14px;");

                                    System.Web.UI.WebControls.Label lbl_answer = new System.Web.UI.WebControls.Label();
                                    if (ans_array.Length > 0)
                                    {
                                        if (ans_array[q] != null)
                                        {
                                            ans_array[q] = ans_array[q].Replace("&nbsp;", "");
                                        }
                                        lbl_answer.Text = Convert.ToString(ans_array[q]);
                                    }
                                    anscell.Controls.Add(lbl_answer);
                                    lbl_answer.CssClass = "indianswtxt";
                                   // ansrow.Cells.Add(anscell);
                                   // ques_resp.Rows.Add(ansrow);
                                    quesrow.Cells.Add(anscell);
                                    ques_resp.Rows.Add(quesrow);

                                    rndpanel_surveyresponses.Controls.Add(ques_resp);
                                    ques_resp_check_flag = 1;
                                    quesNo += 1;
                                    break;
                                }
                            }
                            if (ques_resp_check_flag == 0)
                            {
                                HtmlTable ques_resp = new HtmlTable();
                                ques_resp.Width = "100%";
                                ques_resp.CellPadding = 1;
                                ques_resp.CellSpacing = 0;
                                //ques_resp.Border = 0;
                                ques_resp.Border = 1;
                                ques_resp.BorderColor = "Purple";
                                ques_resp.Align = "Center";
                                HtmlTableRow quesrow = new HtmlTableRow();
                                HtmlTableCell quescell = new HtmlTableCell();
                                quescell.Width = "50%";
                                quescell.Attributes.Add("style", "background:#F4F4F4; padding:10px 8px 10px 8px; font-size:14px;");

                                System.Web.UI.WebControls.Label lbl_ques = new System.Web.UI.WebControls.Label();
                                string temp_ques_str = "";
                                temp_ques_str = ques.QUESTION_LABEL.Replace("<p>", "");
                                temp_ques_str = temp_ques_str.Replace("</p>", "");
                                temp_ques_str = temp_ques_str.Replace("&nbsp;", ""); 
                                lbl_ques.CssClass = "indiquestxt";
                                lbl_ques.Text = "Q" + quesNo + "." + temp_ques_str;
                                lbl_ques.EnableTheming = false;
                                lbl_ques.BorderStyle = BorderStyle.None;
                                quescell.Controls.Add(lbl_ques);
                                quesrow.Cells.Add(quescell);
                                ques_resp.Rows.Add(quesrow);
                               // HtmlTableRow ansrow = new HtmlTableRow();
                                HtmlTableCell anscell = new HtmlTableCell();
                                anscell.Width = "50%";
                                System.Web.UI.WebControls.Label lbl_answer = new System.Web.UI.WebControls.Label();
                                lbl_answer.CssClass = "indianswtxt";
                                lbl_answer.Text = "&nbsp;";

                                anscell.Controls.Add(lbl_answer);
                               // ansrow.Cells.Add(anscell);
                                quesrow.Cells.Add(anscell);
                               // ques_resp.Rows.Add(ansrow);
                                ques_resp.Rows.Add(quesrow);
                                rndpanel_surveyresponses.Controls.Add(ques_resp);
                                quesNo += 1;
                            }

                        }
                    }
                }
            }

            Session["ds_resp_row_id"] = i;
            EmailIDsHeaderFunction();
            break;
        }
    }
    protected void btn_first_Click(object sender, EventArgs e)
    {
        Session["ds_resp_row_id"] = 0;
        Individual_Res();
        panelfunction();

    }
    protected void btn_prev_Click(object sender, EventArgs e)
    {
        if (Session["ds_resp_row_id"] != null)
            Session["ds_resp_row_id"] = Convert.ToInt32(Session["ds_resp_row_id"]) - 1;
        Individual_Res();
        panelfunction();
    }
    protected void btn_nxt_Click(object sender, EventArgs e)
    {
        if (Session["ds_resp_row_id"] != null)
            Session["ds_resp_row_id"] = Convert.ToInt32(Session["ds_resp_row_id"]) + 1;
        Individual_Res();
        panelfunction();

    }
    protected void btn_last_Click(object sender, EventArgs e)
    {
        if (Session["resp_count"] != null)
            ds_count = Convert.ToInt32(Session["resp_count"]);
        Session["ds_resp_row_id"] = ds_count - 1;
        Individual_Res();
        panelfunction();
    }
    protected void btn_go_Click(object sender, EventArgs e)
    {

        ds_count = Convert.ToInt32(Session["resp_count"]);
        if (txt_req.Text.Length == 0 || txt_req.Text == null || txt_req.Text == "" || txt_req.Text.Trim().Length == 0)
        {
            Response.Write("<script language='javascript'>alert('Please select page number ');</script>");
            Session["ds_resp_row_id"] = i;

        }
        else
        {

            if (txt_req.Text != null)
            {
                if (Convert.ToInt32(txt_req.Text) > ds_count || Convert.ToInt32(txt_req.Text) == 0)
                {
                    ClientScript.RegisterStartupScript(typeof(System.Web.UI.Page), "Individual Response", "<script>alert('Please select valid page number');</script>");
                    Session["ds_resp_row_id"] = i;
                }
                else
                {
                    if (txt_req.Text != null && txt_req.Text.Trim().Length > 0)
                        Session["ds_resp_row_id"] = Convert.ToInt32(txt_req.Text) - 1;
                }
            }

        }
        Individual_Res();
        panelfunction();
    }

    void panelfunction()
    {
        if (Session["ds_resp_row_id"] != null)
        {
            int l_value = 0;
            int value_ds_count = ds_count;
            if (Convert.ToInt32(Session["ds_resp_row_id"]) == 0)
                l_value = Convert.ToInt32(Session["ds_resp_row_id"]) + 1;
            else
                l_value = Convert.ToInt32(Session["ds_resp_row_id"]) + 1;
            lbl_currentpage.Text = Convert.ToString(l_value + "/" + ds_count);
            if (Convert.ToInt32(Session["ds_resp_row_id"]) == 0)
            {
                btn_first.Enabled = false;
                btn_prev.Enabled = false;
                if (value_ds_count == 1)
                {
                    btn_nxt.Enabled = false;
                    btn_last.Enabled = false;                   
                }
                else
                {
                    btn_nxt.Enabled = true;
                    btn_last.Enabled = true;  
                }
            }


            if (Convert.ToInt32(Session["ds_resp_row_id"]) == (ds_count - 1))
            {
                btn_last.Enabled = false;
                btn_nxt.Enabled = false;
               
                if (value_ds_count == 1)
                {
                    btn_first.Enabled = false;
                    btn_prev.Enabled = false;                    
                }
                else
                {
                    btn_first.Enabled = true;
                    btn_prev.Enabled = true;                   
                }
            }
            int temp_value = Convert.ToInt32(Session["ds_resp_row_id"]);
            if (temp_value != ds_count - 1 && temp_value != 0)
            {
                btn_first.Enabled = true;               
                btn_last.Enabled = true;              
                btn_prev.Enabled = true;              
                btn_nxt.Enabled = true;              
            }
        }
    }
    protected void btn_first_down_Click(object sender, EventArgs e)
    {
        Session["ds_resp_row_id"] = 0;
        Individual_Res();
        panelfunction();
    }
    protected void btn_prev_down_Click(object sender, EventArgs e)
    {
        if (Session["ds_resp_row_id"] != null)
        {
            Session["ds_resp_row_id"] = Convert.ToInt32(Session["ds_resp_row_id"]) - 1;
        }
        Individual_Res();
        panelfunction();

    }
    protected void btn_next_down_Click(object sender, EventArgs e)
    {
        if (Session["ds_resp_row_id"] != null)
        {
            Session["ds_resp_row_id"] = Convert.ToInt32(Session["ds_resp_row_id"]) + 1;
        }
        Individual_Res();
        panelfunction();
    }
    protected void btn_last_down_Click(object sender, EventArgs e)
    {
        if (Session["resp_count"] != null)
        {
            ds_count = Convert.ToInt32(Session["resp_count"]);
        }
        Session["ds_resp_row_id"] = ds_count - 1;
        Individual_Res();
        panelfunction();
    }


    protected void ASPxButton5_Click(object sender, EventArgs e)
    {
    }
    protected void cbo_responses_SelectedIndexChanged(object sender, EventArgs e)
    {
        filteroptionsfunction();
        Session["ds_resp_row_id"] = 0;
        txt_req.Text = "";
        Individual_Res();
        panelfunction();
    }
    protected void chk_respondentemail_CheckedChanged(object sender, EventArgs e)
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (userDetails != null && userDetails.UserId > 0)
        {
            if (SetFeatures("TRACKEMAIL_ID") == 0)
            {
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Upgrade", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", true);
            }
            else
            {
                txt_req.Text = "";
                Individual_Res();
                panelfunction();
            }
        }
    }
    public void EmailIDsHeaderFunction()
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        string str_mailid = "";
        if (ds_responsedetails.Tables[0].Rows[i].ItemArray[4] != DBNull.Value)
        {
            str_mailid = ds_responsedetails.Tables[0].Rows[i].ItemArray[4].ToString();
        }
        string heading = "";
        if (str_mailid != null)
        {
            if (str_mailid == "")
            {
                if (chk_respondentemail.Checked == true)
                    heading += "Email Address :" + " " + "Unknown/WebDeployment || ";

                heading += "     Completion Time :  ";
                if (ds_responsedetails.Tables[0].Rows[i]["CREATED_ON"] != DBNull.Value)
                {
                    DateTime dt = Convert.ToDateTime(ds_responsedetails.Tables[0].Rows[i]["CREATED_ON"]);
                    dt = BuildQuetions.GetConvertedDatetime(dt, userDetails.StandardBIAS, SurveyBasicInfoView.TIME_ZONE, true);
                    heading += Convert.ToString(dt);
                }
            }
            else
            {
                if (chk_respondentemail.Checked == true)
                    heading += "Email Address :" + " " + str_mailid + "|| ";

                heading += "Completion Time :  ";
                if (ds_responsedetails.Tables[0].Rows[i]["CREATED_ON"] != DBNull.Value)
                {
                    DateTime dt1 = Convert.ToDateTime(ds_responsedetails.Tables[0].Rows[i]["CREATED_ON"]);
                    dt1 = BuildQuetions.GetConvertedDatetime(dt1, userDetails.StandardBIAS, SurveyBasicInfoView.TIME_ZONE, true);
                    heading += Convert.ToString(dt1);
                }

            }
        }

        rndpanel_surveyresponses.HeaderText = heading.ToString();

    }
    protected void chk_blankresponse_CheckedChanged(object sender, EventArgs e)
    {
        //if (SelectedSurvey != null && SelectedSurvey.SURVEY_ID > 0)
        //{
            Session["ds_resp_row_id"] = 0;
            Individual_Res();
            panelfunction();
        //}
    }

    public void filteroptionsfunction()
    {
        if (SelectedSurvey != null && SelectedSurvey.SURVEY_ID > 0)
        {
            if (chk_blankresponse.Checked == true)
            {
                if (chk_validresponses.Checked || chk_excludedresponses.Checked || chk_partialresponses.Checked || chk_partialtocompletes.Checked)
                {
                    if (strsurvytype == "PPS_PREMIUM")
                    {
                        ds_responsedetails = obj1.GetIndiviudalRespIds1000(SelectedSurvey.SURVEY_ID, true, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                    }
                    else
                    {
                        ds_responsedetails = obj1.GetIndiviudalRespIds(SelectedSurvey.SURVEY_ID, true, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                    }
                }
                else
                {
                    if (strsurvytype == "PPS_PREMIUM")
                    {
                        ds_responsedetails = obj1.GetIndiviudalRespIds1000(SelectedSurvey.SURVEY_ID, true, true, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                    }
                    else
                    {
                        ds_responsedetails = obj1.GetIndiviudalRespIds(SelectedSurvey.SURVEY_ID, true, true, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                    }
                }

            }

            else
            {
                if (chk_validresponses.Checked || chk_excludedresponses.Checked || chk_partialresponses.Checked || chk_partialtocompletes.Checked)
                {
                    if (strsurvytype == "PPS_PREMIUM")
                    {
                        ds_responsedetails = obj1.GetIndiviudalRespIds1000(SelectedSurvey.SURVEY_ID, false, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                    }
                    else
                    {
                        ds_responsedetails = obj1.GetIndiviudalRespIds(SelectedSurvey.SURVEY_ID, false, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                    }
                 }
                else
                {
                    if (strsurvytype == "PPS_PREMIUM")
                    {
                        ds_responsedetails = obj1.GetIndiviudalRespIds1000(SelectedSurvey.SURVEY_ID, false, true, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                    }
                    else
                    {
                        ds_responsedetails = obj1.GetIndiviudalRespIds(SelectedSurvey.SURVEY_ID, false, true, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                    }
                }

            }

        }

    }

    protected void lbn_ExcludeResponse_Click(object sender, EventArgs e)
    {
        string strRootUrl = ConfigurationManager.AppSettings["RootURL"].ToString();
        int delete_rowid = 0;
        int restoretype = 0;
        if (Session["ds_resp_row_id"] != null)
            delete_rowid = Convert.ToInt32(Session["ds_resp_row_id"]);

        if (lbn_ExcludeResponse.Text == "Exclude this response")
        {
            surresponse.ExcludeIndividualResp(Convert.ToInt32(respondent_idvalue.Value), base.SurveyBasicInfoView.SURVEY_ID, 0);
        }
        else if (lbn_ExcludeResponse.Text == "Take this response to complete")
        {
            bool resstat = surresponse.IncludeResponsetoComplete(Convert.ToInt32(respondent_idvalue.Value), base.SurveyBasicInfoView.SURVEY_ID, 0);
            if (!resstat)
            {

                lbl_message.Text = "You have exceeded the response limit set for your account. <br/>To include this and more responses, upgrade your account now ";
                lbl_message.Text += "<a href='" + strRootUrl + "Pricing.aspx' target='_blank' style='color:#49649A;'>Upgrade Now!</a>";
                lbl_message.Visible = true;
            }
        }
        else if (lbn_ExcludeResponse.Text == "Restore this response to complete")
        {
            bool resstat = surresponse.IncludeResponsetoComplete(Convert.ToInt32(respondent_idvalue.Value), base.SurveyBasicInfoView.SURVEY_ID, 1);
            if (!resstat)
            {
                lbl_message.Text = "You have exceeded the response limit set for your account. <br/>To include this and more responses, upgrade your account now ";
                lbl_message.Text += "<a href='" + strRootUrl + "Pricing.aspx' target='_blank' style='color:#49649A;'>Upgrade Now!</a>";
                lbl_message.Visible = true;
            }
        }
        else if (lbn_ExcludeResponse.Text == "Restore this response as partial")
        {
            surresponse.ExcludeIndividualResp(Convert.ToInt32(respondent_idvalue.Value), base.SurveyBasicInfoView.SURVEY_ID, 1);
        }

        txt_req.Text = "";
        //txt_req_down.Text = "";
        Individual_Res();
        panelfunction();
    }
    protected void rbncomplete_CheckedChanged(object sender, EventArgs e)
    {


    }
    protected void rbnpartialResponses_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rawdatapartial_Click(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            bool flag = false;
            flag = surresponse.GetRawDataExportResponses(base.SurveyBasicInfoView.SURVEY_ID, "Partial");
          //  ExportResponsestoExcel(flag, "Partial");
            WritePartialDataToExcelSheet();
           Individual_Res();
        }

    }


    public void WritePartialDataToExcelSheet()
    {
        try
        {
            //create FileInfo object  to read you ExcelWorkbook
            string strDirector = ConfigurationManager.AppSettings["RootPath"].ToString();
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            if (!Directory.Exists(strDirector + SurveyBasicInfoView.USERID + @"\" + SurveyBasicInfoView.SURVEY_ID))
            {
                Directory.CreateDirectory(strDirector + SurveyBasicInfoView.USERID + @"\" + SurveyBasicInfoView.SURVEY_ID);
            }


            string strXLFileName = strDirector + SurveyBasicInfoView.USERID + "\\" + SurveyBasicInfoView.SURVEY_ID + "\\" + "PartialDataExportComplete_" + SurveyBasicInfoView.SURVEY_NAME.ToString() + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";

            var file = new FileInfo(strXLFileName);

            using (ExcelPackage xlPackage = new ExcelPackage(file))
            {

                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Sheet1");

                //Select a range of cells to insert the data and use the LoadFromDataTable method to write the
                //data                

                worksheet.Cells[1, 1, 600, 600].Clear();

                DataSet dsquestions = surcore.getPartialdataexportquestions(SurveyBasicInfoView.SURVEY_ID);
                System.Data.DataTable dtquestions = dsquestions.Tables[0];

                System.Data.DataTable mtxcolnames10 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames11 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames12 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames15 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames13 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames20 = createMtxColumnDataTable();

                int colval = 0;

                worksheet.Cells[2, colval + 1].Value = "Email";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                colval = colval + 1;

                worksheet.Cells[2, colval + 1].Value = "Submit Time";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                worksheet.Cells[2, colval + 1].Value = "Serial number";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                for (int i = 0; i < dtquestions.Rows.Count; i++)
                {

                    //getting answeroptions as columns

                    DataSet dsmultiselectques = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[i][1].ToString()));


                    //getting questions
                    worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + dtquestions.Rows[i][2].ToString();
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                    worksheet.Column(colval + 1).Width = 30;


                    if ((dtquestions.Rows[i][3].ToString() == "1") || (dtquestions.Rows[i][3].ToString() == "3") || (dtquestions.Rows[i][3].ToString() == "4"))
                    {
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                        if (dtquestions.Rows[i][5].ToString() == "1")
                        {
                            worksheet.Cells[2, colval + 2, 2, colval + 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[2, colval + 2, 2, colval + 2].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        }
                        else
                        {
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        }
                    }
                    else
                    {
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    }

                    colval = colval + 1;

                    //  DataTable dtmultiselectques;
                    if (dtquestions.Rows[i][3].ToString() == "2")
                    {

                        for (int i2 = 0; i2 < dsmultiselectques.Tables[0].Rows.Count; i2++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + dsmultiselectques.Tables[0].Rows[i2][3].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "9")
                    {
                        for (int i11 = 0; i11 < dsmultiselectques.Tables[0].Rows.Count; i11++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + dsmultiselectques.Tables[0].Rows[i11][3].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "10")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[1].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[1].Rows[0][0].ToString()), mtxcolnames10);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames10.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[1].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[1].Rows[imtxcol][0].ToString()), mtxcolnames10);
                                }
                            }
                        }
                        for (int i10 = 0; i10 < mtxcolnames10.Rows.Count; i10++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames10.Rows[i10][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                        mtxcolnames10.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "11")
                    {
                        for (int i11 = 0; i11 < dsmultiselectques.Tables[0].Rows.Count; i11++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + dsmultiselectques.Tables[0].Rows[i11][3].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "12")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[2].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[0][0].ToString()), mtxcolnames12);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[imtxcol][0].ToString()), mtxcolnames12);
                                }
                            }
                        }
                        for (int i12 = 0; i12 < mtxcolnames12.Rows.Count; i12++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ":" + mtxcolnames12.Rows[i12][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                        mtxcolnames12.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "15")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[3].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[0][0].ToString()), mtxcolnames15);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames15.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()), mtxcolnames15);

                                }
                            }
                        }
                        for (int i15 = 0; i15 < mtxcolnames15.Rows.Count; i15++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames15.Rows[i15][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                        mtxcolnames15.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "13")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[3].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[0][0].ToString()), mtxcolnames13);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames13.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()), mtxcolnames13);

                                }
                            }
                        }
                        for (int i13 = 0; i13 < mtxcolnames13.Rows.Count; i13++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames13.Rows[i13][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                        mtxcolnames13.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "20")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[3].Rows.Count; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[0][0].ToString()), mtxcolnames20);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames20.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()) + "'");


                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()), mtxcolnames20);

                                }
                            }
                        }
                        for (int i20 = 0; i20 < mtxcolnames20.Rows.Count; i20++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames20.Rows[i20][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                        mtxcolnames20.Clear();
                    }
                    //displaying other option
                    if ((dtquestions.Rows[i][3].ToString() == "1") || (dtquestions.Rows[i][3].ToString() == "2") || (dtquestions.Rows[i][3].ToString() == "3") || (dtquestions.Rows[i][3].ToString() == "4"))
                    {
                        if (dtquestions.Rows[i][5].ToString() == "1")
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": Other, Please specify";
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                    }

                    worksheet.Cells[2, 1, 2, colval].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[2, 1, 2, colval].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                    worksheet.Cells[2, 1, 2, colval].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[2, 1, 2, colval].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);



                }

                worksheet.Cells[2, colval, 2, colval].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval, 2, colval].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                //loading data

                int rowvaldata = 3;               
                for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                {
                    int colvaldata = 0;
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsquestions.Tables[1].Rows[resrow][5].ToString();
                    colvaldata = colvaldata + 1;
                    DateTime dt = Convert.ToDateTime(dsquestions.Tables[1].Rows[resrow][3].ToString());
                    dt = BuildQuetions.GetConvertedDatetime(dt, userDetails.StandardBIAS, SurveyBasicInfoView.TIME_ZONE, true);
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dt.ToString();
                   // worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsquestions.Tables[1].Rows[resrow][3].ToString();
                    colvaldata = colvaldata + 1;
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = resrow + 1;
                    colvaldata = colvaldata + 1;

                    worksheet.Cells[rowvaldata, 1, rowvaldata, colvaldata].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[rowvaldata, 1, rowvaldata, colvaldata].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);

                    rowvaldata = rowvaldata + 1;
                }

                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, 1, dsquestions.Tables[1].Rows.Count + 2, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, 1, dsquestions.Tables[1].Rows.Count + 2, 3].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                //data based on question
                int Qcolvaldata = 3;
                int Tempcoldata = 0;
                int Tempcoldata10 = 0;
                int Tempcoldata11 = 0;
                int Tempcoldata12 = 0;
                int Tempcoldata13 = 0;
                int Tempcoldata15 = 0;
                int Tempcoldata20 = 0;
                int Tempcoldata9 = 0;

                for (int rdt = 0; rdt < dtquestions.Rows.Count; rdt++)
                {
                    int Qrowvaldata = 3;

                    DataSet dsRespondentdata = surcore.getPartialdatarespondents(SurveyBasicInfoView.SURVEY_ID, Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                    if ((dtquestions.Rows[rdt][3].ToString() == "1") || (dtquestions.Rows[rdt][3].ToString() == "3") || (dtquestions.Rows[rdt][3].ToString() == "4"))
                    {

                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            DataRow[] dr1 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");

                            if (dr1.Length > 0)
                            {
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = dr1[0].ItemArray[4].ToString();
                            }
                            else
                            {
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);

                            if (dtquestions.Rows[rdt][5].ToString() == "1")
                            {

                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 2, Qrowvaldata, Qcolvaldata + 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 2, Qrowvaldata, Qcolvaldata + 2].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 2].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 2].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                            }
                            else
                            {
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                            }




                            Qrowvaldata = Qrowvaldata + 1;
                        }

                        Qcolvaldata = Qcolvaldata + 1;

                        if (dtquestions.Rows[rdt][5].ToString() == "1")
                        {
                            int tempQrowdata = 3;
                            System.Data.DataTable dtQother = dsRespondentdata.Tables[1];
                            for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                            {
                                DataRow[] drother = dtQother.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");

                                if (drother.Length > 0)
                                {
                                    worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = drother[0].ItemArray[3].ToString();
                                }
                                else
                                {
                                    worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = "";
                                }
                                tempQrowdata = tempQrowdata + 1;
                            }
                            Qcolvaldata = Qcolvaldata + 1;


                        }



                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "2")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;

                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));

                        //Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata = Qcolvaldata + 1;
                            for (int i2 = 0; i2 < dsmultiselectques.Tables[0].Rows.Count; i2++)
                            {
                                DataRow[] dr1 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and AnswerID = '" + dsmultiselectques.Tables[0].Rows[i2][2] + "'");

                                if (dr1.Length > 0)
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata].Value = "Yes";
                                    Tempcoldata = Tempcoldata + 1;
                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata].Value = "No";
                                    Tempcoldata = Tempcoldata + 1;
                                }

                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1, Qrowvaldata, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1, Qrowvaldata, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }


                        Qcolvaldata = Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count;

                        if (dtquestions.Rows[rdt][5].ToString() == "1")
                        {
                            int tempQrowdata = 3;
                            System.Data.DataTable dtQother = dsRespondentdata.Tables[1];
                            for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                            {
                                //  DataRow[] drother = dtQother.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and answer_id=0 ");
                                DataRow[] drother = dtQother.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");
                                if (drother.Length > 0)
                                {
                                    worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = drother[0].ItemArray[3].ToString();
                                }
                                else
                                {
                                    worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = "";
                                }
                                tempQrowdata = tempQrowdata + 1;
                            }
                            Qcolvaldata = Qcolvaldata + 1;
                        }
                        //}
                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "10")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];


                        DataSet dsmultiselectques = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));

                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[1].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(dsmultiselectques.Tables[1].Rows[0][0].ToString(), mtxcolnames10);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames10.Select("columnname = '" + dsmultiselectques.Tables[1].Rows[imtxcol][0].ToString().Replace("'", "''") + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(dsmultiselectques.Tables[1].Rows[imtxcol][0].ToString(), mtxcolnames10);
                                }
                            }
                        }

                        //Tempcoldata10 = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata10 = Qcolvaldata + 1;

                            DataRow[] dr1 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");

                            if (dr1.Length > 0)
                            {
                                DataTable dtc = dr1.CopyToDataTable();

                                DataTable dtrescols = new DataTable();
                                DataRow dtrescolrow;

                                dtrescols.Columns.Add("columnname");

                                for (int irg1 = 0; irg1 < dr1.Length; irg1++)
                                {
                                    dtrescolrow = dtrescols.NewRow();

                                    dtrescolrow[0] = dr1[irg1][5].ToString();

                                    dtrescols.Rows.Add(dtrescolrow);
                                }

                                for (int mtxcrow = 0; mtxcrow < mtxcolnames10.Rows.Count; mtxcrow++)
                                {
                                    DataRow[] drnotsel = dtrescols.Select("columnname = '" + mtxcolnames10.Rows[mtxcrow][0].ToString().Replace("'","''") + "'");
                                    DataRow drns;
                                    if (drnotsel.Length == 0)
                                    {
                                        drns = dtc.NewRow();
                                        drns[0] = dsquestions.Tables[1].Rows[resrow][0];
                                        drns[1] = 0;
                                        drns[2] = 0;
                                        drns[3] = "";
                                        drns[4] = "";
                                        drns[5] = mtxcolnames10.Rows[mtxcrow][0].ToString();
                                        dtc.Rows.Add(drns);
                                    }
                                }


                                for (int mtxcrow1 = 0; mtxcrow1 < mtxcolnames10.Rows.Count; mtxcrow1++)
                                {
                                    for (int r10 = 0; r10 < dtc.Rows.Count; r10++)
                                    {
                                        if (dtc.Rows[r10][5].ToString().Contains(mtxcolnames10.Rows[mtxcrow1][0].ToString()))
                                        {
                                            int dlri = dtc.Rows[r10][4].ToString().IndexOf("$");
                                            if (dlri > 0)
                                            {
                                                string strrawdata = dtc.Rows[r10][4].ToString().Substring(0, dlri);
                                                worksheet.Cells[Qrowvaldata, Tempcoldata10].Value = strrawdata;
                                            }
                                            else
                                            {
                                                worksheet.Cells[Qrowvaldata, Tempcoldata10].Value = "";
                                            }
                                            Tempcoldata10 = Tempcoldata10 + 1;
                                        }
                                    }
                                }


                            }


                            else
                            {
                                for (int mtxcrow = 0; mtxcrow < mtxcolnames10.Rows.Count; mtxcrow++)
                                {
                                    for (int r10 = 0; r10 < mtxcolnames10.Rows.Count; r10++)
                                    {
                                        if (mtxcolnames10.Rows[r10][0].ToString().Contains(mtxcolnames10.Rows[mtxcrow][0].ToString()))
                                        {
                                            worksheet.Cells[Qrowvaldata, Tempcoldata10].Value = "";
                                            Tempcoldata10 = Tempcoldata10 + 1;
                                        }
                                    }
                                }
                            }



                            worksheet.Cells[Qrowvaldata, Qcolvaldata + mtxcolnames10.Rows.Count, Qrowvaldata, Qcolvaldata + mtxcolnames10.Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + mtxcolnames10.Rows.Count, Qrowvaldata, Qcolvaldata + mtxcolnames10.Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);



                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + mtxcolnames10.Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + mtxcolnames10.Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }



                        Qcolvaldata = Qcolvaldata + mtxcolnames10.Rows.Count;
                        mtxcolnames10.Clear();
                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "11")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques11 = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        //Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata11 = Qcolvaldata + 1;

                            for (int i11 = 0; i11 < dsmultiselectques11.Tables[0].Rows.Count; i11++)
                            {
                                DataRow[] dr1 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and AnswerID = '" + dsmultiselectques11.Tables[0].Rows[i11][2] + "'");

                                if (dr1.Length > 0)
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata11].Value = "Yes";
                                    Tempcoldata11 = Tempcoldata11 + 1;
                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata11].Value = "No";
                                    Tempcoldata11 = Tempcoldata11 + 1;
                                }
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count;

                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "12")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[2].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[0][0].ToString()), mtxcolnames12);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[imtxcol][0].ToString()), mtxcolnames12);
                                }
                            }
                        }

                        // Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata12 = Qcolvaldata + 1;

                            for (int xcol = 0; xcol < mtxcolnames12.Rows.Count; xcol++)
                            {

                                DataRow[] dr12 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and ordcolumn= '" + System.Web.HttpUtility.HtmlDecode(mtxcolnames12.Rows[xcol][0].ToString()) + "'");

                                if (dr12.Length > 0)
                                {
                                    for (int r12 = 0; r12 < dr12.Length; r12++)
                                    {

                                        int dlri = dr12[r12].ItemArray[4].ToString().IndexOf("$") + 4;
                                        int strlenth = dr12[r12].ItemArray[4].ToString().Length - dlri;

                                        string strrawdata = dr12[r12].ItemArray[4].ToString().Substring(dlri, strlenth);
                                        int dlri1 = strrawdata.IndexOf("$");
                                        string strrawdata1 = strrawdata.Substring(0, dlri1);

                                        worksheet.Cells[Qrowvaldata, Tempcoldata12].Value = strrawdata1;

                                        Tempcoldata12 = Tempcoldata12 + 1;
                                    }
                                }
                                else
                                {
                                    //for (int r12 = 0; r12 < mtxcolnames12.Rows.Count; r12++)
                                    //  {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata12].Value = "";
                                    Tempcoldata12 = Tempcoldata12 + 1;
                                    //  }
                                }

                            }
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + mtxcolnames12.Rows.Count, Qrowvaldata, Qcolvaldata + mtxcolnames12.Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + mtxcolnames12.Rows.Count, Qrowvaldata, Qcolvaldata + mtxcolnames12.Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + mtxcolnames12.Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + mtxcolnames12.Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + mtxcolnames12.Rows.Count;
                        mtxcolnames12.Clear();
                    }
                    else if ((dtquestions.Rows[rdt][3].ToString() == "5") || (dtquestions.Rows[rdt][3].ToString() == "6") || (dtquestions.Rows[rdt][3].ToString() == "8") || (dtquestions.Rows[rdt][3].ToString() == "14"))
                    {
                        int tempQrowdata = 3;
                        System.Data.DataTable dtQother = dsRespondentdata.Tables[1];
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            DataRow[] drother = dtQother.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");

                            if (drother.Length > 0)
                            {
                                worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = drother[0].ItemArray[3].ToString();
                            }
                            else
                            {
                                worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = "";
                            }

                            worksheet.Cells[tempQrowdata, Qcolvaldata + 1, tempQrowdata, Qcolvaldata + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[tempQrowdata, Qcolvaldata + 1, tempQrowdata, Qcolvaldata + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            tempQrowdata = tempQrowdata + 1;


                        }
                        Qcolvaldata = Qcolvaldata + 1;

                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "9")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques9 = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        //Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata9 = Qcolvaldata + 1;

                            for (int i11 = 0; i11 < dsmultiselectques9.Tables[0].Rows.Count; i11++)
                            {
                                DataRow[] dr9 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and AnswerID = '" + dsmultiselectques9.Tables[0].Rows[i11][2] + "'");

                                if (dr9.Length > 0)
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata9].Value = dr9[0].ItemArray[3].ToString();
                                    Tempcoldata9 = Tempcoldata9 + 1;
                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata9].Value = "";
                                    Tempcoldata9 = Tempcoldata9 + 1;
                                }
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count;

                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "7")
                    {
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[1];

                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            DataRow[] dr1 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");

                            if (dr1.Length > 0)
                            {
                                //if (dr1[0].ItemArray[3].ToString().Length < 14)
                                //{
                                //    worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = Convert.ToInt64(dr1[0].ItemArray[3].ToString());
                                //}
                                //else
                                //{
                                    worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = dr1[0].ItemArray[3].ToString();
                              //  }
                            }
                            else
                            {
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);


                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + 1;
                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "15")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques15 = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        //Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata15 = Qcolvaldata + 1;

                            for (int i15 = 0; i15 < dsmultiselectques15.Tables[0].Rows.Count; i15++)
                            {
                                DataRow[] dr15 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and AnswerID = '" + dsmultiselectques15.Tables[0].Rows[i15][2] + "'");

                                if (dr15.Length > 0)
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata15].Value = Convert.ToInt32(dr15[0].ItemArray[3].ToString());
                                    Tempcoldata15 = Tempcoldata15 + 1;
                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata15].Value = "";
                                    Tempcoldata15 = Tempcoldata15 + 1;
                                }
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count;

                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "13")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques13 = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        // Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata13 = Qcolvaldata + 1;

                            for (int i13 = 0; i13 < dsmultiselectques13.Tables[0].Rows.Count; i13++)
                            {
                                DataRow[] dr13 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and AnswerID = '" + dsmultiselectques13.Tables[0].Rows[i13][2] + "'");

                                if (dr13.Length > 0)
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata13].Value = Convert.ToInt32(dr13[0].ItemArray[3].ToString());
                                    Tempcoldata13 = Tempcoldata13 + 1;
                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata13].Value = "";
                                    Tempcoldata13 = Tempcoldata13 + 1;
                                }
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count;

                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "20")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[3].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[0][0].ToString()), mtxcolnames20);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames20.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()), mtxcolnames20);

                                }
                            }
                        }
                        // Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {

                            Tempcoldata20 = Qcolvaldata + 1;
                            DataSet dscontinfo = surcore.getRawdataexportcontactinfo(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()), Convert.ToInt32(dsquestions.Tables[1].Rows[resrow][0].ToString()));

                            for (int r20 = 0; r20 < dscontinfo.Tables[0].Rows.Count; r20++)
                            {

                                string s = dscontinfo.Tables[0].Rows[r20][1].ToString();

                                Boolean numval = Regex.IsMatch(s, @"^\d+$");

                                if (numval == true)
                                {

                                  //  worksheet.Cells[Qrowvaldata, Tempcoldata20].Value = Convert.ToInt64(s.Replace(" ", ""));
                                    worksheet.Cells[Qrowvaldata, Tempcoldata20].Value = s.Replace(" ", "");

                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata20].Value = s;
                                }

                                Tempcoldata20 = Tempcoldata20 + 1;
                            }


                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dscontinfo.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dscontinfo.Tables[0].Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dscontinfo.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dscontinfo.Tables[0].Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dscontinfo.Tables[0].Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dscontinfo.Tables[0].Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + mtxcolnames20.Rows.Count;
                        mtxcolnames20.Clear();

                    }

                }

                //Then we save our changes so that they are reflected in ou excel sheet.
                xlPackage.Save();

                if (File.Exists(strXLFileName))
                {


                    string name_survey = SurveyBasicInfoView.SURVEY_NAME.Replace(" ", "");
                    name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                    name_survey = name_survey.Replace("/", "");
                    Response.ContentType = "application/ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + "_Partialdataexport" + ".xlsx");
                    Response.TransmitFile(strXLFileName);
                    Response.End();

                }

            }

        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            //set workbook object to null
            //if (workBook != null)
            //    workBook = null;
        }
    }

   

    private void AddMtxrowsDatatoTable(string rowoption, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["rowname"] = rowoption;

            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void AddMtxcolsDatatoTable(string columnoption, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["columnname"] = columnoption;

            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private System.Data.DataTable createMtxColumnDataTable()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();


            myDataColumn.DataType = Type.GetType("System.String");
            myDataColumn.ColumnName = "columnname";
            myDataTable.Columns.Add(myDataColumn);


            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private System.Data.DataTable createMtxRowDataTable()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();


            myDataColumn.DataType = Type.GetType("System.String");
            myDataColumn.ColumnName = "rowname";
            myDataTable.Columns.Add(myDataColumn);


            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void AddDatatoTable(int QuestionID, string rowansweroption, string columnansweroption, string percentage, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = QuestionID;
            dr["rowansweroption"] = rowansweroption;
            dr["columnansweroption"] = columnansweroption;
            dr["percentage"] = percentage;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void SaveDialogBox(string sourcePath, string SurveyName, string fileExtension)
    {
        if (File.Exists(sourcePath))
        {
           // divSendReport.Visible = true;
            //ErrorSuccessNotifier.AddSuccessMessage(Resources.CommonMessages.SendReportMessage); 
            string name_survey = SurveyName.Replace(" ", "");
            name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
            name_survey = name_survey.Replace("/", "");
            Response.ContentType = "application/ms-excel";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + fileExtension);
            Response.TransmitFile(sourcePath);
            Response.End();
        }
        else
        {
            lblErrMsg.Text = "File not created by service.";
            dvErrMsg.Visible = true;
        }
    }
    protected void cmdviewResponses_Click(object sender, EventArgs e)
    {

        Session["ds_resp_row_id"] = 0;
        Individual_Res();
        panelfunction();

    }
    protected void rawdatavalid_Click(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            bool flag = false;
            flag = surresponse.GetRawDataExportResponses(base.SurveyBasicInfoView.SURVEY_ID, "Complete");
           // ExportResponsestoExcel(flag, "Complete");
            WriteRawDataToExcelSheet();
            Individual_Res();
        }
    }


    public void WriteRawDataToExcelSheet()
    {
        try
        {
            //create FileInfo object  to read you ExcelWorkbook
            string strDirector = ConfigurationManager.AppSettings["RootPath"].ToString();
            if (!Directory.Exists(strDirector + SurveyBasicInfoView.USERID + @"\" + SurveyBasicInfoView.SURVEY_ID))
            {
                Directory.CreateDirectory(strDirector + SurveyBasicInfoView.USERID + @"\" + SurveyBasicInfoView.SURVEY_ID);
            }

            string strXLFileName = strDirector + SurveyBasicInfoView.USERID + "\\" + SurveyBasicInfoView.SURVEY_ID + "\\" + "RawDataExportComplete_" +
               SurveyBasicInfoView.SURVEY_NAME.ToString() + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";


            var file = new FileInfo(strXLFileName);

            using (ExcelPackage xlPackage = new ExcelPackage(file))
            {

                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Sheet1");

                //Select a range of cells to insert the data and use the LoadFromDataTable method to write the
                //data                

                worksheet.Cells[1, 1, 600, 600].Clear();

                DataSet mxreslimit = surcore.getSurveyType(SurveyBasicInfoView.SURVEY_ID);
                     string strsurvytype = mxreslimit.Tables[0].Rows[0][0].ToString();
                     DataSet dsquestions = null;

                     if ((strsurvytype == "PPS_PREMIUM") && (SurveyBasicInfoView.ResponseCount > 1000))
                     {

                         dsquestions = surcore.getRawdataexportquestions1000(SurveyBasicInfoView.SURVEY_ID);
                     }
                     else
                     {
                         dsquestions = surcore.getRawdataexportquestions(SurveyBasicInfoView.SURVEY_ID);
                     }
                System.Data.DataTable dtquestions = dsquestions.Tables[0];

                System.Data.DataTable mtxcolnames10 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames11 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames12 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames15 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames13 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames20 = createMtxColumnDataTable();

                int colval = 0;

                worksheet.Cells[2, colval + 1].Value = "Email";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                colval = colval + 1;

                worksheet.Cells[2, colval + 1].Value = "Submit Time";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                worksheet.Cells[2, colval + 1].Value = "Serial number";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                for (int i = 0; i < dtquestions.Rows.Count; i++)
                {

                    //getting answeroptions as columns

                    DataSet dsmultiselectques = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[i][1].ToString()));


                    //getting questions
                    worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + dtquestions.Rows[i][2].ToString();
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                    worksheet.Column(colval + 1).Width = 30;


                    if ((dtquestions.Rows[i][3].ToString() == "1") || (dtquestions.Rows[i][3].ToString() == "3") || (dtquestions.Rows[i][3].ToString() == "4"))
                    {
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                        if (dtquestions.Rows[i][5].ToString() == "1")
                        {
                            worksheet.Cells[2, colval + 2, 2, colval + 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[2, colval + 2, 2, colval + 2].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        }
                        else
                        {
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        }
                    }
                    else
                    {
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    }

                    colval = colval + 1;

                    //  DataTable dtmultiselectques;
                    if (dtquestions.Rows[i][3].ToString() == "2")
                    {

                        for (int i2 = 0; i2 < dsmultiselectques.Tables[0].Rows.Count; i2++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + dsmultiselectques.Tables[0].Rows[i2][3].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "9")
                    {
                        for (int i11 = 0; i11 < dsmultiselectques.Tables[0].Rows.Count; i11++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + dsmultiselectques.Tables[0].Rows[i11][3].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "10")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[1].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[1].Rows[0][0].ToString()), mtxcolnames10);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames10.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[1].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[1].Rows[imtxcol][0].ToString()), mtxcolnames10);
                                }
                            }
                        }
                        for (int i10 = 0; i10 < mtxcolnames10.Rows.Count; i10++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames10.Rows[i10][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                        mtxcolnames10.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "11")
                    {
                        for (int i11 = 0; i11 < dsmultiselectques.Tables[0].Rows.Count; i11++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + dsmultiselectques.Tables[0].Rows[i11][3].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "12")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[2].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[0][0].ToString()), mtxcolnames12);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[imtxcol][0].ToString()), mtxcolnames12);
                                }
                            }
                        }
                        for (int i12 = 0; i12 < mtxcolnames12.Rows.Count; i12++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ":" + mtxcolnames12.Rows[i12][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                        mtxcolnames12.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "15")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[3].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[0][0].ToString()), mtxcolnames15);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames15.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()), mtxcolnames15);

                                }
                            }
                        }
                        for (int i15 = 0; i15 < mtxcolnames15.Rows.Count; i15++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames15.Rows[i15][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                        mtxcolnames15.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "13")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[3].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[0][0].ToString()), mtxcolnames13);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames13.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()), mtxcolnames13);

                                }
                            }
                        }
                        for (int i13 = 0; i13 < mtxcolnames13.Rows.Count; i13++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames13.Rows[i13][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                        mtxcolnames13.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "20")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[3].Rows.Count; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[0][0].ToString()), mtxcolnames20);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames20.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()) + "'");


                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()), mtxcolnames20);

                                }
                            }
                        }
                        for (int i20 = 0; i20 < mtxcolnames20.Rows.Count; i20++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames20.Rows[i20][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                        mtxcolnames20.Clear();
                    }
                    //displaying other option
                    if ((dtquestions.Rows[i][3].ToString() == "1") || (dtquestions.Rows[i][3].ToString() == "2") || (dtquestions.Rows[i][3].ToString() == "3") || (dtquestions.Rows[i][3].ToString() == "4"))
                    {
                        if (dtquestions.Rows[i][5].ToString() == "1")
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": Other, Please specify";
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            colval = colval + 1;
                        }
                    }

                    worksheet.Cells[2, 1, 2, colval].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[2, 1, 2, colval].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                    worksheet.Cells[2, 1, 2, colval].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[2, 1, 2, colval].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);



                }

                worksheet.Cells[2, colval, 2, colval].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval, 2, colval].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                //loading data

                int rowvaldata = 3;
                var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
                for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                {
                    int colvaldata = 0;
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsquestions.Tables[1].Rows[resrow][5].ToString();
                    colvaldata = colvaldata + 1;

                    DateTime dt = Convert.ToDateTime(dsquestions.Tables[1].Rows[resrow][3].ToString());
                    dt = BuildQuetions.GetConvertedDatetime(dt, userDetails.StandardBIAS, SurveyBasicInfoView.TIME_ZONE, true);
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dt.ToString();
                   // worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsquestions.Tables[1].Rows[resrow][3].ToString();
                    colvaldata = colvaldata + 1;
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = resrow + 1;
                    colvaldata = colvaldata + 1;

                    worksheet.Cells[rowvaldata, 1, rowvaldata, colvaldata].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[rowvaldata, 1, rowvaldata, colvaldata].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);

                    rowvaldata = rowvaldata + 1;
                }

                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, 1, dsquestions.Tables[1].Rows.Count + 2, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, 1, dsquestions.Tables[1].Rows.Count + 2, 3].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                //data based on question
                int Qcolvaldata = 3;
                int Tempcoldata = 0;
                int Tempcoldata10 = 0;
                int Tempcoldata11 = 0;
                int Tempcoldata12 = 0;
                int Tempcoldata13 = 0;
                int Tempcoldata15 = 0;
                int Tempcoldata20 = 0;
                int Tempcoldata9 = 0;

                for (int rdt = 0; rdt < dtquestions.Rows.Count; rdt++)
                {
                    int Qrowvaldata = 3;

                    DataSet dsRespondentdata = null;

                    if ((strsurvytype == "PPS_PREMIUM") && (SurveyBasicInfoView.ResponseCount > 1000))
                    {

                        dsRespondentdata = surcore.getRawdatarespondents1000(SurveyBasicInfoView.SURVEY_ID, Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                    }
                    else
                    {
                        dsRespondentdata = surcore.getRawdatarespondents(SurveyBasicInfoView.SURVEY_ID, Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                    }


                    
                    if ((dtquestions.Rows[rdt][3].ToString() == "1") || (dtquestions.Rows[rdt][3].ToString() == "3") || (dtquestions.Rows[rdt][3].ToString() == "4"))
                    {

                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            DataRow[] dr1 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");

                            if (dr1.Length > 0)
                            {
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = dr1[0].ItemArray[4].ToString();
                            }
                            else
                            {
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);

                            if (dtquestions.Rows[rdt][5].ToString() == "1")
                            {

                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 2, Qrowvaldata, Qcolvaldata + 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 2, Qrowvaldata, Qcolvaldata + 2].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 2].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 2].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                            }
                            else
                            {
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                            }




                            Qrowvaldata = Qrowvaldata + 1;
                        }

                        Qcolvaldata = Qcolvaldata + 1;

                        if (dtquestions.Rows[rdt][5].ToString() == "1")
                        {
                            int tempQrowdata = 3;
                            System.Data.DataTable dtQother = dsRespondentdata.Tables[1];
                            for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                            {
                                DataRow[] drother = dtQother.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");

                                if (drother.Length > 0)
                                {
                                    worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = drother[0].ItemArray[3].ToString();
                                }
                                else
                                {
                                    worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = "";
                                }
                                tempQrowdata = tempQrowdata + 1;
                            }
                            Qcolvaldata = Qcolvaldata + 1;


                        }



                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "2")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;

                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));

                        //Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata = Qcolvaldata + 1;
                            for (int i2 = 0; i2 < dsmultiselectques.Tables[0].Rows.Count; i2++)
                            {
                                DataRow[] dr1 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and AnswerID = '" + dsmultiselectques.Tables[0].Rows[i2][2] + "'");

                                if (dr1.Length > 0)
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata].Value = "Yes";
                                    Tempcoldata = Tempcoldata + 1;
                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata].Value = "No";
                                    Tempcoldata = Tempcoldata + 1;
                                }

                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1, Qrowvaldata, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1, Qrowvaldata, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count + 1].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }


                        Qcolvaldata = Qcolvaldata + dsmultiselectques.Tables[0].Rows.Count;

                        if (dtquestions.Rows[rdt][5].ToString() == "1")
                        {
                            int tempQrowdata = 3;
                            System.Data.DataTable dtQother = dsRespondentdata.Tables[1];
                            for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                            {
                                //  DataRow[] drother = dtQother.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and answer_id=0 ");
                                DataRow[] drother = dtQother.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");
                                if (drother.Length > 0)
                                {
                                    worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = drother[0].ItemArray[3].ToString();
                                }
                                else
                                {
                                    worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = "";
                                }
                                tempQrowdata = tempQrowdata + 1;
                            }
                            Qcolvaldata = Qcolvaldata + 1;
                        }
                        //}
                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "10")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];


                        DataSet dsmultiselectques = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));

                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[1].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(dsmultiselectques.Tables[1].Rows[0][0].ToString(), mtxcolnames10);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames10.Select("columnname = '" + dsmultiselectques.Tables[1].Rows[imtxcol][0].ToString().Replace("'", "''") + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(dsmultiselectques.Tables[1].Rows[imtxcol][0].ToString(), mtxcolnames10);
                                }
                            }
                        }

                        //Tempcoldata10 = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata10 = Qcolvaldata + 1;

                            DataRow[] dr1 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");

                            if (dr1.Length > 0)
                            {
                                DataTable dtc = dr1.CopyToDataTable();

                                DataTable dtrescols = new DataTable();
                                DataRow dtrescolrow;

                                dtrescols.Columns.Add("columnname");

                                for (int irg1 = 0; irg1 < dr1.Length; irg1++)
                                {
                                    dtrescolrow = dtrescols.NewRow();

                                    dtrescolrow[0] = dr1[irg1][5].ToString();

                                    dtrescols.Rows.Add(dtrescolrow);
                                }

                                for (int mtxcrow = 0; mtxcrow < mtxcolnames10.Rows.Count; mtxcrow++)
                                {
                                    DataRow[] drnotsel = dtrescols.Select("columnname = '" + mtxcolnames10.Rows[mtxcrow][0].ToString().Replace("'","''") + "'");
                                    DataRow drns;
                                    if (drnotsel.Length == 0)
                                    {
                                        drns = dtc.NewRow();
                                        drns[0] = dsquestions.Tables[1].Rows[resrow][0];
                                        drns[1] = 0;
                                        drns[2] = 0;
                                        drns[3] = "";
                                        drns[4] = "";
                                        drns[5] = mtxcolnames10.Rows[mtxcrow][0].ToString();
                                        dtc.Rows.Add(drns);
                                    }
                                }


                                for (int mtxcrow1 = 0; mtxcrow1 < mtxcolnames10.Rows.Count; mtxcrow1++)
                                {
                                    for (int r10 = 0; r10 < dtc.Rows.Count; r10++)
                                    {
                                        if (dtc.Rows[r10][5].ToString().Contains(mtxcolnames10.Rows[mtxcrow1][0].ToString()))
                                        {
                                            int dlri = dtc.Rows[r10][4].ToString().IndexOf("$");
                                            if (dlri > 0)
                                            {
                                                string strrawdata = dtc.Rows[r10][4].ToString().Substring(0, dlri);
                                                worksheet.Cells[Qrowvaldata, Tempcoldata10].Value = strrawdata;
                                            }
                                            else
                                            {
                                                worksheet.Cells[Qrowvaldata, Tempcoldata10].Value = "";
                                            }
                                            Tempcoldata10 = Tempcoldata10 + 1;
                                        }
                                    }
                                }


                            }


                            else
                            {
                                for (int mtxcrow = 0; mtxcrow < mtxcolnames10.Rows.Count; mtxcrow++)
                                {
                                    for (int r10 = 0; r10 < mtxcolnames10.Rows.Count; r10++)
                                    {
                                        if (mtxcolnames10.Rows[r10][0].ToString().Contains(mtxcolnames10.Rows[mtxcrow][0].ToString()))
                                        {
                                            worksheet.Cells[Qrowvaldata, Tempcoldata10].Value = "";
                                            Tempcoldata10 = Tempcoldata10 + 1;
                                        }
                                    }
                                }
                            }



                            worksheet.Cells[Qrowvaldata, Qcolvaldata + mtxcolnames10.Rows.Count, Qrowvaldata, Qcolvaldata + mtxcolnames10.Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + mtxcolnames10.Rows.Count, Qrowvaldata, Qcolvaldata + mtxcolnames10.Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);



                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + mtxcolnames10.Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + mtxcolnames10.Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }



                        Qcolvaldata = Qcolvaldata + mtxcolnames10.Rows.Count;
                        mtxcolnames10.Clear();
                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "11")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques11 = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        //Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata11 = Qcolvaldata + 1;

                            for (int i11 = 0; i11 < dsmultiselectques11.Tables[0].Rows.Count; i11++)
                            {
                                DataRow[] dr1 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and AnswerID = '" + dsmultiselectques11.Tables[0].Rows[i11][2] + "'");

                                if (dr1.Length > 0)
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata11].Value = "Yes";
                                    Tempcoldata11 = Tempcoldata11 + 1;
                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata11].Value = "No";
                                    Tempcoldata11 = Tempcoldata11 + 1;
                                }
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + dsmultiselectques11.Tables[0].Rows.Count;

                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "12")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[2].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[0][0].ToString()), mtxcolnames12);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[2].Rows[imtxcol][0].ToString()), mtxcolnames12);
                                }
                            }
                        }

                        // Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata12 = Qcolvaldata + 1;

                            for (int xcol = 0; xcol < mtxcolnames12.Rows.Count; xcol++)
                            {
                                // DataRow[] dr12 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");
                                DataRow[] dr12 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and ordcolumn= '" + System.Web.HttpUtility.HtmlDecode(mtxcolnames12.Rows[xcol][0].ToString()) + "'");


                                if (dr12.Length > 0)
                                {
                                    for (int r12 = 0; r12 < dr12.Length; r12++)
                                    {

                                        int dlri = dr12[r12].ItemArray[4].ToString().IndexOf("$") + 4;
                                        int strlenth = dr12[r12].ItemArray[4].ToString().Length - dlri;

                                        string strrawdata = dr12[r12].ItemArray[4].ToString().Substring(dlri, strlenth);
                                        int dlri1 = strrawdata.IndexOf("$");
                                        string strrawdata1 = strrawdata.Substring(0, dlri1);

                                        worksheet.Cells[Qrowvaldata, Tempcoldata12].Value = strrawdata1;

                                        Tempcoldata12 = Tempcoldata12 + 1;
                                    }
                                }
                                else
                                {
                                    //for (int r12 = 0; r12 < mtxcolnames12.Rows.Count; r12++)
                                    //{
                                        worksheet.Cells[Qrowvaldata, Tempcoldata12].Value = "";
                                        Tempcoldata12 = Tempcoldata12 + 1;
                                   // }
                                }
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + mtxcolnames12.Rows.Count, Qrowvaldata, Qcolvaldata + mtxcolnames12.Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + mtxcolnames12.Rows.Count, Qrowvaldata, Qcolvaldata + mtxcolnames12.Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + mtxcolnames12.Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + mtxcolnames12.Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + mtxcolnames12.Rows.Count;
                        mtxcolnames12.Clear();
                    }
                    else if ((dtquestions.Rows[rdt][3].ToString() == "5") || (dtquestions.Rows[rdt][3].ToString() == "6") || (dtquestions.Rows[rdt][3].ToString() == "8") || (dtquestions.Rows[rdt][3].ToString() == "14"))
                    {
                        int tempQrowdata = 3;
                        System.Data.DataTable dtQother = dsRespondentdata.Tables[1];
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            DataRow[] drother = dtQother.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");

                            if (drother.Length > 0)
                            {
                                worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = drother[0].ItemArray[3].ToString();
                            }
                            else
                            {
                                worksheet.Cells[tempQrowdata, Qcolvaldata + 1].Value = "";
                            }

                            worksheet.Cells[tempQrowdata, Qcolvaldata + 1, tempQrowdata, Qcolvaldata + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[tempQrowdata, Qcolvaldata + 1, tempQrowdata, Qcolvaldata + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            tempQrowdata = tempQrowdata + 1;


                        }
                        Qcolvaldata = Qcolvaldata + 1;

                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "9")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques9 = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        //Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata9 = Qcolvaldata + 1;

                            for (int i11 = 0; i11 < dsmultiselectques9.Tables[0].Rows.Count; i11++)
                            {
                                DataRow[] dr9 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and AnswerID = '" + dsmultiselectques9.Tables[0].Rows[i11][2] + "'");

                                if (dr9.Length > 0)
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata9].Value = dr9[0].ItemArray[3].ToString();
                                    Tempcoldata9 = Tempcoldata9 + 1;
                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata9].Value = "";
                                    Tempcoldata9 = Tempcoldata9 + 1;
                                }
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + dsmultiselectques9.Tables[0].Rows.Count;

                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "7")
                    {
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[1];

                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            DataRow[] dr1 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "'");

                            if (dr1.Length > 0)
                            {
                                //if (dr1[0].ItemArray[3].ToString().Length < 14)
                                //{
                                //    worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = Convert.ToInt64(dr1[0].ItemArray[3].ToString());
                                //}
                                //else
                                //{
                                    worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = dr1[0].ItemArray[3].ToString();
                                //}
                            }
                            else
                            {
                                worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + 1, Qrowvaldata, Qcolvaldata + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + 1].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);


                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + 1;
                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "15")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        System.Data.DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques15 = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        //Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata15 = Qcolvaldata + 1;

                            for (int i15 = 0; i15 < dsmultiselectques15.Tables[0].Rows.Count; i15++)
                            {
                                DataRow[] dr15 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and AnswerID = '" + dsmultiselectques15.Tables[0].Rows[i15][2] + "'");

                                if (dr15.Length > 0)
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata15].Value = Convert.ToInt32(dr15[0].ItemArray[3].ToString());
                                    Tempcoldata15 = Tempcoldata15 + 1;
                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata15].Value = "";
                                    Tempcoldata15 = Tempcoldata15 + 1;
                                }
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + dsmultiselectques15.Tables[0].Rows.Count;

                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "13")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques13 = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        // Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {
                            Tempcoldata13 = Qcolvaldata + 1;

                            for (int i13 = 0; i13 < dsmultiselectques13.Tables[0].Rows.Count; i13++)
                            {
                                DataRow[] dr13 = dtQ1.Select("RespondentID='" + dsquestions.Tables[1].Rows[resrow][0] + "' and AnswerID = '" + dsmultiselectques13.Tables[0].Rows[i13][2] + "'");

                                if (dr13.Length > 0)
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata13].Value = Convert.ToInt32(dr13[0].ItemArray[3].ToString());
                                    Tempcoldata13 = Tempcoldata13 + 1;
                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata13].Value = "";
                                    Tempcoldata13 = Tempcoldata13 + 1;
                                }
                            }

                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + dsmultiselectques13.Tables[0].Rows.Count;

                    }
                    else if (dtquestions.Rows[rdt][3].ToString() == "20")
                    {
                        worksheet.Cells[Qrowvaldata, Qcolvaldata + 1].Value = "";
                        Qcolvaldata = Qcolvaldata + 1;
                        DataTable dtQ1 = dsRespondentdata.Tables[0];

                        DataSet dsmultiselectques = surcore.getRawdataexportansweroptions(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()));
                        for (int imtxcol = 0; imtxcol < dsmultiselectques.Tables[3].Rows.Count; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[0][0].ToString()), mtxcolnames20);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames20.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmultiselectques.Tables[3].Rows[imtxcol][0].ToString()), mtxcolnames20);

                                }
                            }
                        }
                        // Tempcoldata = Qcolvaldata;
                        for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                        {

                            Tempcoldata20 = Qcolvaldata + 1;
                            DataSet dscontinfo = surcore.getRawdataexportcontactinfo(Convert.ToInt32(dtquestions.Rows[rdt][1].ToString()), Convert.ToInt32(dsquestions.Tables[1].Rows[resrow][0].ToString()));

                            for (int r20 = 0; r20 < dscontinfo.Tables[0].Rows.Count; r20++)
                            {

                                string s = dscontinfo.Tables[0].Rows[r20][1].ToString();

                                Boolean numval = Regex.IsMatch(s, @"^\d+$");

                                if (numval == true)
                                {

                                  //  worksheet.Cells[Qrowvaldata, Tempcoldata20].Value = Convert.ToInt64(s.Replace(" ", ""));
                                    worksheet.Cells[Qrowvaldata, Tempcoldata20].Value = s.Replace(" ", "");

                                }
                                else
                                {
                                    worksheet.Cells[Qrowvaldata, Tempcoldata20].Value = s;
                                }

                                Tempcoldata20 = Tempcoldata20 + 1;
                            }


                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dscontinfo.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dscontinfo.Tables[0].Rows.Count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[Qrowvaldata, Qcolvaldata + dscontinfo.Tables[0].Rows.Count, Qrowvaldata, Qcolvaldata + dscontinfo.Tables[0].Rows.Count].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dscontinfo.Tables[0].Rows.Count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata, dsquestions.Tables[1].Rows.Count + 2, Qcolvaldata + dscontinfo.Tables[0].Rows.Count].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                            Qrowvaldata = Qrowvaldata + 1;
                        }
                        Qcolvaldata = Qcolvaldata + mtxcolnames20.Rows.Count;
                        mtxcolnames20.Clear();

                    }

                }

                //Then we save our changes so that they are reflected in ou excel sheet.
                xlPackage.Save();

                if (File.Exists(strXLFileName))
                {

                    string name_survey = SurveyBasicInfoView.SURVEY_NAME.Replace(" ", "");
                    name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                    name_survey = name_survey.Replace("/", "");
                    Response.ContentType = "application/ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + "_Rawdataexport" + ".xlsx");
                    Response.TransmitFile(strXLFileName);
                    Response.End();

                }

            }

        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            //set workbook object to null
            //if (workBook != null)
            //    workBook = null;
        }
    }

   
    private void CreateExportFile(int rep_type, string exp_type)
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (exp_type != "" && SurveyBasicInfoView != null && SurveyBasicInfoView.SURVEY_ID > 0 && userDetails != null && userDetails.UserId > 0)
        {
            int exp_id = surresponse.InsertExportInfo(SurveyBasicInfoView.SURVEY_ID, rep_type, exp_type, userDetails.UserId);
            if (exp_id > 0)
            {
                //System.Threading.Thread.Sleep(5000 * 10);
                DataSet ds = new DataSet();
                int flag = 0;
                for (int i = 0; i < 4; i++)
                {
                    System.Threading.Thread.Sleep(5000*10);
                    ds = surresponse.GetExportInfo(exp_id);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[0]["REPORT_STATUS"]) == 2)
                        {
                            string strDirector = ConfigurationManager.AppSettings["RootPath"].ToString();
                            if (!Directory.Exists(strDirector +  base.SurveyBasicInfoView.USERID + @"\" + base.SurveyBasicInfoView.SURVEY_ID))
                            {
                                Directory.CreateDirectory(strDirector +  base.SurveyBasicInfoView.USERID + @"\" + base.SurveyBasicInfoView.SURVEY_ID);
                            }
                            string strXLFileName = strDirector + base.SurveyBasicInfoView.USERID + "\\" + base.SurveyBasicInfoView.SURVEY_ID + "\\" + Convert.ToString(ds.Tables[0].Rows[0]["REPORT_FILENAME"]);

                            SaveDialogBox(strXLFileName, base.SurveyBasicInfoView.SURVEY_NAME, ".xls");
                            flag = 1;
                            break;

                        }
                        else if (Convert.ToInt32(ds.Tables[0].Rows[0]["REPORT_STATUS"]) == 4)
                        {
                            flag = 2;
                            break;
                        }
                    }


                }
                if (flag == 2)
                {
                    string Naurlstr = EncryptHelper.EncryptQuerystring("IndividualResponse.aspx", Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "myscript", "alert('No Responses has been Recorded for this survey');window.location.href='" + Naurlstr + "';", true);
                }
            }
        }
    }
    private void ExportResponsestoExcel(bool flag, string Resp_type)
    {
        if (flag)
        {
            int exptype = 4;
            string rawdatatype = "RawDataComplete";
            if (Resp_type == "Partial")
            {
                exptype = 5;
                rawdatatype = "RawDataPartial";
            }
            CreateExportFile(exptype, rawdatatype);
        }
        else
        {
            string Naurlstr = EncryptHelper.EncryptQuerystring("IndividualResponse.aspx", Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID + "&Alert=" + "noerror&surveyFlag=" + SurveyFlag);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "myscript", "alert('No Responses has been Recorded for this survey');window.location.href='" + Naurlstr + "';", true);
        }

    }
    protected void chk_validresponses_CheckedChanged(object sender, EventArgs e)
    {
        Session["ds_resp_row_id"] = 0;
        Individual_Res();
        panelfunction();
    }

}