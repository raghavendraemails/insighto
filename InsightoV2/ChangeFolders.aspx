﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="ChangeFolders.aspx.cs" Inherits="ChangeFolders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="popupHeading">
  <!-- popup heading -->
  <div class="popupTitle">
    <h3>My Folder</h3>
  </div>
  <!--<div class="popupClosePanel">
			<a href="#">X</a>
		</div>	-->
  <!-- //popup heading -->
</div>
<div class="popupContentPanel">
  <!-- popup content panel -->
    
    <div class="gridTopPanel">

		<p align="right"><asp:DropDownList ID="ddlFolders" runat="server"></asp:DropDownList></p>
	</div>
	<div class="surveysGridPanel">
		<table border="0" width="100%" cellpadding="0" cellspacing="1">
			<tr class="headerRwoGray">
				<td width="5%" align="center"><input type="checkbox" /></td>
				<td width="28%" align="center">Survey Name</td>
				<td width="17%">Survey Category</td>
				<td width="22%">Created By</td>
				<td width="8%" align="right">Sent to</td>
				<td width="7%" align="right">Responses</td>
				<td width="9%">Status</td>
			</tr>
			<tr class="rowStyleGray">
				<td align="center"><input type="checkbox" /></td>
				<td ><a href="#">Survey External multiple</a></td>
				<td >Others</td>
				<td >Chaitanya.B</td>
				<td align="right">4</td>
				<td align="right">8</td>
				<td>Closed</td>
			</tr>
			<tr class="rowStyleGray">
				<td align="center"><input type="checkbox" /></td>
				<td ><a href="#">Survey close On Date test</a></td>
				<td >Others</td>
				<td >Chaitanya.B</td>
				<td align="right">7</td>
				<td align="right">14</td>
				<td>Closed</td>
			</tr>
			<tr class="rowStyleGray">
				<td align="center"><input type="checkbox" /></td>
				<td ><a href="#">testing12</a></td>
				<td >Product Feedback</td>
				<td >anita</td>
				<td align="right">0</td>
				<td align="right">0</td>
				<td>Draft</td>
			</tr>
			<tr class="rowStyleGray">
				<td align="center"><input type="checkbox" /></td>
				<td ><a href="#">Survey Launch on date</a></td>
				<td >Others</td>
				<td >Chaitanya.B	</td>
				<td align="right">4</td>
				<td align="right">9</td>
				<td>Active</td>
			</tr>
			<tr class="rowStyleGray">
				<td align="center"><input type="checkbox" /></td>
				<td ><a href="#">No Of Respondents Quota</a></td>
				<td >Others</td>
				<td >Chaitanya.B	</td>
				<td align="right">7</td>
				<td align="right">12</td>
				<td>Closed</td>
			</tr>
			<tr class="footerRowStyleGray">
					<td colspan="7">
					<div class="showMorePanel">
						Show <select class="dropDownGrid" id="showMore">
							<option>5</option>
							<option>10</option>
							<option>15</option>
							<option>20</option>
							</select>
					</div>
					<div class="gridFooterNav">
							<a href="#">Prev</a>
							<a href="#"><span>1</span></a>
							<a href="#">2</a>
							<a href="#">3</a>
							<a href="#">4</a>
							<a href="#">5</a>
							<a href="#">Next</a>
					</div>
					</td>
				</tr>
		</table>
	</div>
   
  <div class="clear"></div>
  <!-- //popup content panel -->
</div>
<script type="text/javascript">
    function showContents(itemName) {
        if (itemName == 'addNewFolderLink') {
            document.getElementById('addNewFolderLink').style.display = 'none';
            document.getElementById('addNewFolderUL').style.display = 'block';
            document.getElementById('successMessage').style.display = 'none';
        } else if (itemName == 'addNewFolderUL') {
            document.getElementById('addNewFolderLink').style.display = 'block';
            document.getElementById('addNewFolderUL').style.display = 'none';
            document.getElementById('successMessage').style.display = 'block';
        }
    }
</script>
</asp:Content>


