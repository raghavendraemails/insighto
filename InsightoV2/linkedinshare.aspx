﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="linkedinshare.aspx.cs" Inherits="linkedinshare" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
    <meta content="IE=9" http-equiv="X-UA-Compatible" />
    <meta content="cda4765a-ff5d-4984-90ce-c89941bea8f7" name="pageImpressionID" />
    <meta content="cws-share-widget" name="pageKey" />
    <link href="http://s.c.lnkd.licdn.com/img/icon/apple-touch-icon.png" rel="apple-touch-icon-precomposed" />
    <!--

    [if lte IE 8]>
        <link rel="shortcut icon" href="http://s.c.lnkd.licdn.com/scds/common/u/images/logos/favicons/v1/16x16/favicon.ico">
    <![endif]

    -->
    <!--

    [if IE 9]>
        <link rel="shortcut icon" href="http://s.c.lnkd.licdn.com/scds/common/u/images/logos/favicons/v1/favicon.ico">
    <![endif]

    -->
    <link href="http://s.c.lnkd.licdn.com/scds/common/u/images/logos/favicons/v1/favicon.ico" rel="icon" />
    <meta content="http://s.c.lnkd.licdn.com/scds/common/u/images/logos/favicon_w8_tile.png" name="msapplication-TileImage" />
    <meta content="LinkedIn" name="application-name" />
    <title>
        Share on LinkedIn | LinkedIn
    </title>
    <meta content="text/html;" http-equiv="content-type" />
    <meta content="cws-share-widget" name="pageKey" />
    <meta content="/cws/login-popup" name="IN-signin" />
        
    <link href="App_Themes/Classic/LinkedInShare.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.7.1.js"></script>

     <script type="text/javascript">

         ShowValue = function (name, result, msg) {
          
             window.opener.displaySuccess(name, result, msg);
             window.close();
         }

    </script>
</head>

<body id="pagekey-cws-share-widget" class="en member v2 sharing-popup share-dialog-v2 chrome-v5 chrome-v5-responsive background-v4 sticky-bg js popup">
    <asp:Label ID="lblerror" runat="server"></asp:Label>
    <div id="frame-contents" style="min-width: 600px;height:600px;">
        <div id="header" class="header">
            <h1 class="logo">LinkedIn</h1>
            <div id="nav-utility">
                <ul class="util">
                    <li class="name" id="INNameli">name</li>
                </ul>
            </div>
        </div>
        <div id="body">
            <div class="wrapper">
                <div id="share-offsite" class="share-in">
                    <form class="share-dialog" name="shareForm" method="POST" runat="server">
                    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true" />
                        <div id="sharing-error"></div>

                    <ul class="global-form">
                    <li>
                        <div id="yui-gen1">
                            <input id="isPostingToNetworkUpdates-shareForm" type="checkbox"  value="isPostingToNetworkUpdates" name="isPostingToNetworkUpdates"  checked />
                            <label class="primary" for="isPostingToNetworkUpdates-shareForm">
                                Share an update
                            </label>
                        </div>
                        <div id="nus-share" class="mini-form" style="display: block;">
                            <input id="reshare-entity-mentions" type="hidden" name="mentions" />
                            <div id="reshare-entity-mentions-container" class="mentions-container">
                                <pre id="reshare-entity-highlighter" class="mentions-highlighter"></pre>
                                <textarea id="postText-shareForm" class="post mentions-input" cols="40" rows="8" name="postText"></textarea>
                            </div>
                        </div>
                    </li>
                </ul>


		<asp:HiddenField ID="shareTxt" Value="" Visible="false" runat="server" />
		<asp:HiddenField ID="AT" Value="" Visible="false" runat="server" />
		<asp:HiddenField ID="initial" Value="" Visible="false" runat="server" />
        <asp:HiddenField ID="INName" Value="" runat="server" />
        <asp:HiddenField ID="postuserid" Value="" runat="server" />

        <p class="actions">
        <asp:Button ID="ShareButton" runat="server" Text="Share" CssClass="btn-primary" 
        OnClick="postShare" OnClientClick="return CheckStatus();" meta:resourcekey="btnBackResource1" />
        <label id="checkboxerr" style="color:Red;visibility:hidden;">Share an update must be selected to allow LinkedIn share submission</label>
        </p>						
		
                    </form>
                </div>
            </div>
        </div>
        <div id="footer" class="footer">
            <div class="legal">
                <p>
                     By using this plugin, you agree to the LinkedIn 
                    <a href="http://www.linkedin.com/legal/user-agreement" target="_blank">User Agreement</a>
                     and 
                    <a href="http://www.linkedin.com/legal/privacy-policy" target="_blank">Privacy Policy</a>
                </p>
            </div>
        </div>
    </div>

</body>

<script type="text/javascript">
    $(document).ready(function () {
        window.resizeTo(620, 350);
        $('#INNameli').text($("#<%=INName.ClientID%>").val());
        var shareText = "Insighto has an amazing new free plan that puts other survey tools to shade. I just signed up!. Check this out – you will know what I am talking about. http://s.insighto.com/1gTlZ3X";
        $('#postText-shareForm').text(shareText);       
        $('#postText-shareForm').attr('readonly', true);
    });

    $("#isPostingToNetworkUpdates-shareForm").change(function () {
        if ($('#isPostingToNetworkUpdates-shareForm').is(':checked')) {
            $("#postText-shareForm").prop("disabled", false);
            document.getElementById("checkboxerr").style.visibility = 'hidden';
        }
        else {
            $("#postText-shareForm").attr("disabled", "disabled");
        }
    });

    function CheckStatus() {
        if ($('#isPostingToNetworkUpdates-shareForm').is(':checked') == false) {
            document.getElementById("checkboxerr").style.visibility = 'visible';
            return false;
        }
        else
            return true;
    }

</script>

</html>
