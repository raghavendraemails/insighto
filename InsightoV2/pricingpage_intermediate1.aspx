﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true" CodeFile="pricingpage_intermediate1.aspx.cs" Inherits="pricingpage_intermediate1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Price plan - Insighto</title>
    <link href="App_Themes/Classic/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="wrapper">
	
    <div class="clr"></div>
    <div id="container_main">
            <div class="thx_txt">
            	Thank you for registering with Insighto.com. <br />
				Choose one of the following options and proceed.
            </div>
    	<div class="priceplan_blk">
        	
            
            <!--  Basic Price Blk  -->
        
        	<div class="price_1_blk">
            	<h3>BASIC</h3>
                <div class="price_1_blk_cont">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header">
                                    <div class="price_tag_free">Free <br /> for<br /> LIFE</div>
                                    <div class="price">
                                        <asp:Label ID="Label2" runat="server" Text=""></asp:Label></div>
                                    per month
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td><a id="timezone" runat="server"><asp:Button ID="Button5" runat="server" 
                                    Text="Continue" CssClass="btn_bg" onclick="Button5_Click" 
                                     /></a></td>    </tr>
                          <tr>
                            <td>Unlimited number of surveys</td>
                          </tr>
                          <tr>
                            <td>15 questions per survey</td>
                          </tr>
                          <tr>
                            <td>150 responses per survey</td>
                          </tr>
                          <tr>
                            <td>18 question types</td>
                          </tr>
                          <tr>
                            <td>Survey templates</td>
                          </tr>
                          <tr>
                            <td>Choice of themes</td>
                          </tr>
                          <tr>
                            <td>Launch through web link</td>
                          </tr>
                          <tr>
                            <td>Report in data tables</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">
                                <asp:Button ID="Button1" runat="server" Text="Continue" CssClass="btn_bg" 
                                    onclick="Button1_Click" />
                          </td>
                          </tr>
                    </table>
                </div>

            </div>
            <!--  Basic Price Blk End  -->
        	
            <!--  Basic Pro Blk  -->
        
        	<div class="price_1_blk">
            	<h3>PRO</h3>
                <div class="price_1_blk_cont">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header green">
                                    <div class="price">
                                        <asp:Label ID="Label1" runat="server" Text="" CssClass="price" ></asp:Label></div>
                                   	per annum
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border"><a id="PayMonth" runat="server">
                                <asp:Label ID="lblMonthly" runat="server" Text="Monthly Plan - <span class='font_ruppe'>`</span>450"></asp:Label>
                                </a>
                            </td>
                            
                          </tr>
                          
                          <tr>
                            <td><a id="PayYear" runat="server" class="no_border"><asp:Button ID="Button2" runat="server" Text="Buy Now"  CssClass="btn_bg" onclick="Button2_Click"/></a></td>
                          </tr>
                          <tr>
                            <td>Unlimited number of surveys</td>
                          </tr>
                          <tr>
                            <td>Unlimited number of questions</td>
                          </tr>
                          <tr>
                            <td>Unlimited responses</td>
                          </tr>
                          <tr>
                            <td>19 question types</td>
                          </tr>
                          <tr>
                            <td>Survey templates</td>
                          </tr>
                          <tr>
                            <td>Choice of themes</td>
                          </tr>
                          <tr>
                            <td>Insert brand logo</td>
                          </tr>
                          <tr>
                            <td>Personalize buttons</td>
                          </tr>
                          <tr>
                            <td>Personalize survey end message</td>
                          </tr>
                          <tr>
                            <td>Schedule alerts</td>
                          </tr>
                          <tr>
                            <td>Print survey</td>
                          </tr>
                          <tr>
                            <td>Save survey to Word</td>
                          </tr>
                          <tr>
                            <td>Launch through web link</td>
                          </tr>
                          <tr>
                            <td>Launch via Insighto emails</td>
                          </tr>
                          <tr>
                            <td>Report in data and graphs</td>
                          </tr>
                          <tr>
                            <td>Export data to Excel, PPT, PDF and Word</td>
                          </tr>
                          <tr>
                            <td>Cross Tab</td>
                          </tr>
                          <tr>
                            <td class="no_border"><asp:Button ID="Button3" runat="server" Text="Buy Now"  
                                    CssClass="btn_bg" onclick="Button3_Click" 
                                    /></td>
                          </tr>
                    </table>
                </div>

            </div>
            <!--  Basic Pro Blk End  -->
        
        	
   			<div class="clr"></div>

        </div>
   		<div class="clr"></div>
  </div>
</div>
</body>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29026379-1']);
    _gaq.push(['_setDomainName', 'insighto.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<!-- Google Code for Lead_signup Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1027814833;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "dEWdCImw7gIQseuM6gM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1027814833/?value=0&amp;label=dEWdCImw7gIQseuM6gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</html>

</asp:Content>

