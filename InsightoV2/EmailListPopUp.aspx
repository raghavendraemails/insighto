﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="EmailListPopUp.aspx.cs" Inherits="EmailListPopUp" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">            
            <h3>
                <asp:Label runat="server" ID="lblTitle" Text="Create Emails"></asp:Label></h3>
        </div>
      <%--  <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" title="Close" onclick="closeModalSelf(false,'');">&nbsp;</a>
        </div>--%>
        <!-- //popup heading -->
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupContentPanel">
    <div class="defaultH"> </div>
        <!-- popup content panel -->
         <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div> <asp:Label ID="lblSuccessMsg" runat="server" Visible="False" 
                    Text="Contact added successfully." meta:resourcekey="lblSuccessMsgResource1"
               ></asp:Label></div>
         </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">   
           <div>
            <asp:Label ID="lblErrMsg" runat="server" Visible="False"  
                   Text="<%$ Resources:CommonMessages, EmailExistsMsg %>"></asp:Label>
            </div>
        </div>
        <div >
            <!-- form -->
            <div class="con_login_pnl">
                <div class="loginlbl">
                <asp:Label ID="lblEmail" AssociatedControlID="txtEmail" runat="server" 
                        Text="Email Id" meta:resourcekey="lblEmailResource1" />
                </div>
                <div class="loginControls">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="textBoxMedium" MaxLength="50" 
                        meta:resourcekey="txtEmailResource1" />
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqEmailId" 
                        ControlToValidate="txtEmail" CssClass="lblRequired"
                             ErrorMessage="Please enter email id." runat="server" 
                        Display="Dynamic" meta:resourcekey="reqEmailIdResource1"></asp:RequiredFieldValidator>
               <asp:RegularExpressionValidator SetFocusOnError="True"  ID="regEmailId" 
                        runat="server" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                        CssClass="lblRequired" ControlToValidate="txtEmail" 
                        Display="Dynamic" ErrorMessage = "Please enter valid email id." 
                        meta:resourcekey="regEmailIdResource1"></asp:RegularExpressionValidator></div>  
                </div>
            </div>
            
            <div class="con_login_pnl" >
                <div class="loginlbl"><asp:Label ID="lblFirstName" 
                        AssociatedControlID="txtFirstName" runat="server" Text="First Name" 
                        meta:resourcekey="lblFirstNameResource1" />
                </div>
                <div class="loginControls"> <asp:TextBox ID="txtFirstName" runat="server" 
                        CssClass="textBoxMedium" MaxLength="50" 
                        meta:resourcekey="txtFirstNameResource1" />
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl"><asp:Label ID="lblLastName" AssociatedControlID="txtLastName" 
                        runat="server" Text="Last Name" meta:resourcekey="lblLastNameResource1" />
                </div>
                <div class="loginControls"><asp:TextBox ID="txtLastName" runat="server" 
                        CssClass="textBoxMedium" MaxLength="50" 
                        meta:resourcekey="txtLastNameResource1" />
                </div>
            </div>
            <div class="con_login_pnl" runat="server" id="Column1">
                <div class="loginlbl"><asp:Label ID="lblColumn1" AssociatedControlID="txtColumn1" 
                        runat="server" Visible="False" meta:resourcekey="lblColumn1Resource1" />
                </div>
                <div class="loginControls"> <asp:TextBox ID="txtColumn1" Visible="False" 
                        runat="server" CssClass="textBoxMedium"
                            MaxLength="50" meta:resourcekey="txtColumn1Resource1" />
                </div>
            </div>
            <div class="con_login_pnl" runat="server" id="Column2">
                <div class="loginlbl"><asp:Label ID="lblColumn2" AssociatedControlID="txtColumn2" 
                        runat="server" Visible="False" meta:resourcekey="lblColumn2Resource1" />
                </div>
                <div class="loginControls"><asp:TextBox ID="txtColumn2" runat="server" 
                        Visible="False" CssClass="textBoxMedium"
                            MaxLength="50" meta:resourcekey="txtColumn2Resource1" />
                </div>
            </div>
            <div class="con_login_pnl"  runat="server" id="Column3">
                <div class="loginlbl"> <asp:Label ID="lblColumn3" AssociatedControlID="txtColumn3" 
                        runat="server" Visible="False" meta:resourcekey="lblColumn3Resource1" />
                </div>
                <div class="loginControls">
                <asp:TextBox ID="txtColumn3" runat="server" Visible="False" CssClass="textBoxMedium"
                            MaxLength="50" meta:resourcekey="txtColumn3Resource1" />
                </div>
            </div>
            <div class="con_login_pnl"  runat="server" id="Column4">
                <div class="loginlbl"><asp:Label ID="lblColumn4" AssociatedControlID="txtColumn4" 
                        runat="server" Visible="False" meta:resourcekey="lblColumn4Resource1" />
                </div>
                <div class="loginControls"><asp:TextBox ID="txtColumn4" runat="server" 
                        Visible="False" CssClass="textBoxMedium"
                            MaxLength="50" meta:resourcekey="txtColumn4Resource1" />
                </div>
            </div>
            <div class="con_login_pnl"  runat="server" id="Column5">
                <div class="loginlbl"><asp:Label ID="lblColumn5" AssociatedControlID="txtColumn5" 
                        runat="server" Visible="False" meta:resourcekey="lblColumn5Resource1" />
                </div>
                <div class="loginControls"><asp:TextBox ID="txtColumn5" runat="server" 
                        Visible="False" CssClass="textBoxMedium"
                            MaxLength="50" meta:resourcekey="txtColumn5Resource1" />
                </div>
            </div>
            <div class="con_login_pnl"  runat="server" id="Column6">
                <div class="loginlbl"><asp:Label ID="lblColumn6" AssociatedControlID="txtColumn6" 
                        runat="server" Visible="False" meta:resourcekey="lblColumn6Resource1" />
                </div>
                <div class="loginControls"> <asp:TextBox ID="txtColumn6" runat="server" 
                        Visible="False" CssClass="textBoxMedium"
                            MaxLength="50" meta:resourcekey="txtColumn6Resource1" />
                </div>
            </div>
            <div class="con_login_pnl"  runat="server" id="Column7">
                <div class="loginlbl"><asp:Label ID="lblColumn7" AssociatedControlID="txtColumn7" 
                        runat="server" Visible="False" meta:resourcekey="lblColumn7Resource1" />
                </div>
                <div class="loginControls"> <asp:TextBox ID="txtColumn7" runat="server" 
                        Visible="False" CssClass="textBoxMedium"
                            MaxLength="50" meta:resourcekey="txtColumn7Resource1" />
                </div>
            </div>
            <div class="con_login_pnl"  runat="server" id="Column8">
                <div class="loginlbl"><asp:Label ID="lblColumn8" AssociatedControlID="txtColumn8" 
                        runat="server" Visible="False" meta:resourcekey="lblColumn8Resource1" />
                </div>
                <div class="loginControls"> <asp:TextBox ID="txtColumn8" runat="server" 
                        Visible="False" CssClass="textBoxMedium"
                            MaxLength="50" meta:resourcekey="txtColumn8Resource1" />
                </div>
            </div>
           
            <div class="con_login_pnl">
                <div class="loginlbl"><asp:Label ID="lblbtnNewFolder" runat="server" AssociatedControlID="ibtnSave" meta:resourcekey="lblbtnNewFolderResource1" />
                </div>
                <div class="loginControls">
                <asp:Button ID="ibtnSave" runat="server" CssClass="btn_small_65" Text="Save"
                            ToolTip="Save"  OnClick="ibtnSave_Click" 
                        meta:resourcekey="ibtnSaveResource1" />
                        <asp:HiddenField ID="hdnContactListId" runat="server" />
                </div>
            </div>
           
            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
   
</asp:Content>
