﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using Resources;
using System.Text;
using System.Threading;
using System.Net;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;

public partial class SurveyLastPage : SurveyRespondentPageBase
{
    private SurveyBasicInfoView _surveyBasicInfoView;
    private Hashtable _htKeys;
    private List<oms_config> _lstConfigInfo;
    public string redirectURL;
    public int timeOut;
    string panelid;
    string strcusturlstatus = string.Empty;
    public string airtimeurl;
    string thankyoumessage = "";
    SurveyCore surcore = new SurveyCore();
    /// <summary>
    /// Gets the survey baic info.
    /// </summary>
    private void GetSurveyBaicInfo()
    {
        var configService = ServiceFactory.GetService<ConfigService>();
        _surveyBasicInfoView = ServiceFactory.GetService<RespondentService>().GetSurveyViewInformation(SurveyId);
        _lstConfigInfo = configService.GetConfigurationValues();
    }

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
    /// </summary>
    /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
    protected override void OnLoad(EventArgs e)
    {

        customthankyoumessage.Visible = false;
        defaultthankyoumessage.Visible = false;

        _htKeys = EncryptHelper.DecryptQuerystringParam(Request.QueryString["Key"]);

        if (_htKeys != null && _htKeys.Count > 0)
        {
            SurveyId = Utilities.ToInt(_htKeys["SurveyId"]);
            Embed = Utilities.ToString(_htKeys["Embed"]);
            PageType = Utilities.ToInt(_htKeys["PageType"]);
            RespondentId = Utilities.ToInt(_htKeys["RespondentId"]);
            truecaller = Utilities.ToInt(_htKeys["truecallerstat"]);
        }

        divSurveyLastPage.Style.Add("display", "block");

        if (SurveyId == 11028)
        {
            customthankyoumessage.Style.Add("background", "#330000");

            defaultthankyoumessage.Style.Add("background", "#330000");
            customimage1.Visible = true;
        }

        GetSurveyBaicInfo();
        var checkCusFlag = false;
        var serviceFeature = ServiceFactory.GetService<FeatureService>();


        var fileName = Server.MapPath("~/TemplateFolder/SurveyEndPageContent.txt");

        if (_surveyBasicInfoView.LICENSE_TYPE != "PREMIUM_YEARLY")
        {
            var dic = new Dictionary<string, string>
                      {
                          {"##LogoImageUrl##", RespondentResources.LogoImageUrl },
                          {"##ContentImageUrl##", RespondentResources.ContentImageUrl },
                          {"##InsightoUrl##", Request.Url.Host}
                      };

            var templateContent = ServiceFactory.GetService<TemplateService>().GetDataFromTempalte(fileName, dic);
            ltlLogo.Text = templateContent[0];
            ltlContent.Text = templateContent[1];
        }

        if (_surveyBasicInfoView == null || SurveyId <= 0)
        {
            thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTINVALIDKEY.ToString());
            defaultthankyoumessage.Visible = true;
            ltlMessage.Text = thankyoumessage;
            customthankyoumessage.Visible = false;
            return;
        }

        var dicFeatres = serviceFeature.Find(Utilities.ToEnum<UserType>(_surveyBasicInfoView.LICENSE_TYPE)).ToDictionary(f => f.Feature, f => f.Value);
        switch (PageType)
        {
            case 1:


                if (_surveyBasicInfoView.THANKUPAGE_TYPE == 0)
                {

                    thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTTHANKYOUCONTENT.ToString());

                    if (SurveyId == 9128)
                    {
                        DataSet dsmcenturl = getmcenturl();

                        if (dsmcenturl.Tables[0].Rows.Count > 0)
                        {

                            airtimeurl = dsmcenturl.Tables[0].Rows[0][1].ToString();
                            DataSet dsupdateurl = updatemcenturlstatus(RespondentId, Convert.ToInt32(dsmcenturl.Tables[0].Rows[0][0].ToString()));
                        }

                    }
                    checkCusFlag = true;
                }
                else
                {
                    thankyoumessage = _surveyBasicInfoView.THANKYOU_PAGE;
                    if (SurveyId == 9128)
                    {
                        DataSet dsmcenturl = getmcenturl();

                        if (dsmcenturl.Tables[0].Rows.Count > 0)
                        {
                            airtimeurl = dsmcenturl.Tables[0].Rows[0][1].ToString();
                            DataSet dsupdateurl = updatemcenturlstatus(RespondentId, Convert.ToInt32(dsmcenturl.Tables[0].Rows[0][0].ToString()));
                        }
                    }
                }

                break;

            case 2:

                if (_surveyBasicInfoView.CLOSEPAGE_TYPE == 0)
                {
                    thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTCLOSEPAGE.ToString());
                    checkCusFlag = true;
                }
                else
                {
                    thankyoumessage = _surveyBasicInfoView.CLOSE_PAGE;
                }
                //  h2airtime.Visible = false;
                break;

            case 3:

                if (_surveyBasicInfoView.SCREENOUTPAGE_TYPE == 0)
                {
                    thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTSCREENOUTPAGE.ToString());
                    checkCusFlag = true;
                    screenoutstatus = "screenout";
                }
                else
                {
                    thankyoumessage = _surveyBasicInfoView.SCREENOUT_PAGE;
                    screenoutstatus = "screenout";
                }
                //  h2airtime.Visible = false;
                break;

            case 4:

                if (_surveyBasicInfoView.OVERQUOTAPAGE_TYPE == 0)
                {
                    thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTOVERQUOTA.ToString());
                    checkCusFlag = true;
                }
                else
                {
                    thankyoumessage = _surveyBasicInfoView.OVERQUOTA_PAGE;
                }
                //  h2airtime.Visible = false;
                break;

            case 5:
                thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTINFORESPONDENT.ToString());
                //  h2airtime.Visible = false;
                break;

            case 6:
                thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTUNQUINTERNALSAVEFORLATER.ToString());
                //  h2airtime.Visible = false;
                break;

            case 7:
                thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTUNQUEXTERNALSAVEFORLATER.ToString());
                screenoutstatus = "screenout";
                break;

            case 8:
                if (SurveyId == 9414)
                {
                    thankyoumessage = "Thank you for visiting the survey page. If you wish to participate in the survey later you may do by pasting the link ‘http://s.insighto.com/1hE5QjY' on your browser";
                    screenoutstatus = "screenout";
                }
                else
                {
                    thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTEXITSURVEY.ToString());
                    screenoutstatus = "screenout";
                }

                break;

            case 9:
                thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTUNAUTHORISEDSCREENOUT.ToString());
                break;
            case 10:
                thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTSKIPLOGICISSUE.ToString());
                break;
            case 11:
                thankyoumessage = GetConfigVal(_lstConfigInfo, EndMessages.INSIGHTO_DEFAULTLAUNCHLATER.ToString());
                break;
        }

        if (Embed != "")
        {
            thankyoumessage = "Thank you.If you wish to participate in the survey later, you may do so by coming back to this site";
        }

        if (!string.IsNullOrEmpty(_surveyBasicInfoView.URL_REDIRECTION))// ||
            CheckSessionTimeout();

        if (RespondentId != 0)
        {

            if (screenoutstatus == "screenout")
            {
                // string respondentid_url = "http://www.pokkt.com/pokktapi/callPokktApiWithTId?advid=-1";
                string respondentid_url = "http://www.pokkt.com/pokktapi/screenout/";
                partnerimg.Controls.Add(new HtmlImage()
                {
                    Src = respondentid_url,
                    Alt = "",
                    Width = 0,
                    Height = 0,
                    Visible = true
                });
            }
            else
            {

                //string respondentid_url = "http://www.pokkt.com/pokktapi/callPokktApiWithTId?advid=" +
                //                          RespondentId.ToString() + "&meta=" + truecaller;


                //   string respondentid_url = "http://www.pokkt.com/pokktapi/callPokktApiWithTId/ZQp3IiRN/";

                DataSet dsrespstatus = surcore.getRespondentStatus(RespondentId, SurveyId);

                if (dsrespstatus.Tables[0].Rows.Count > 0)
                {

                    if (dsrespstatus.Tables[0].Rows[0][0].ToString() == "Complete")
                    {

                        byte[] encodedbytes = System.Text.ASCIIEncoding.ASCII.GetBytes(RespondentId.ToString());
                        string encoded = System.Convert.ToBase64String(encodedbytes);

                        string respondentid_url = "http://www.pokkt.com/pokktapi/callPokktApiWithTId/ZQp3IiRN?para=" + encoded;

                        partnerimg.Controls.Add(new HtmlImage()
                        {
                            Src = respondentid_url,
                            Alt = "",
                            Width = 0,
                            Height = 0,
                            Visible = true
                        });
                    }
                }
            }
        }


        if (SurveyBasicInfoView.THANKUPAGE_TYPE == 1)
        {
            customthankyoumessage.Visible = true;
            ltlMessageCustom.Text = thankyoumessage;
            ltlMessageCustom.Visible = true;
            ltlMessage.Visible = false;
            defaultthankyoumessage.Visible = false;
        }
        else
        {
            customthankyoumessage.Visible = false;
            ltlMessage.Text = thankyoumessage;
            ltlMessageCustom.Visible = false;
            ltlMessage.Visible = true;
            defaultthankyoumessage.Visible = true;
        }
    }

    private bool IsCustomSurvey(string[] customIds)
    {
        foreach (var item in customIds)
        {
            if (item.Trim() == SurveyId.ToString())
                return true;
        }
        return false;
    }

    /// <summary>
    /// Gets the config val.
    /// </summary>
    /// <param name="lstConfigInfo">The LST config info.</param>
    /// <param name="key">The key.</param>
    /// <returns></returns>
    private static string GetConfigVal(IEnumerable<oms_config> lstConfigInfo, string key)
    {
        var configInfo = lstConfigInfo.Where(cv => cv.CONFIG_KEY == key).FirstOrDefault();
        return configInfo != null ? configInfo.CONFIG_VALUE : string.Empty;
    }

    /// <summary>
    /// Checks the session timeout.
    /// </summary>
    private void CheckSessionTimeout()
    {
        if (_surveyBasicInfoView == null)
            return;

        var time = 0;
        if (!string.IsNullOrEmpty(_surveyBasicInfoView.TIME_FOR_URL_REDIRECTION))
            time = ValidationHelper.GetInteger(_surveyBasicInfoView.TIME_FOR_URL_REDIRECTION, 0);
        var intMilliSecondsTimeOut = time * 1000;
        timeOut = time * 1000;
        if (intMilliSecondsTimeOut < 0)
            intMilliSecondsTimeOut = 5000;

        DataSet dsSqa = new DataSet();

        dsSqa = getCustomizeURLbit(SurveyId);

        if (dsSqa.Tables[0].Rows.Count > 0)
        {

            strcusturlstatus = dsSqa.Tables[0].Rows[0][0].ToString();

        }
        var redirecturl = "";
        if (strcusturlstatus == "True")
        {
            panelid = Request.QueryString["id"];

            if (PageType == 1)
            {
                redirecturl = "http://thepanelstation.dubinterviewer.com/dkr/end?id=" + panelid + "&s=sHfcPFmm&rst=1";
            }
            else if (PageType == 3)
            {
                redirecturl = "http://thepanelstation.dubinterviewer.com/dkr/end?id=" + panelid + "&s=sHfcPFmm&rst=2";
            }
            else if (PageType == 4)
            {
                redirecturl = "http://thepanelstation.dubinterviewer.com/dkr/end?id=" + panelid + "&s=sHfcPFmm&rst=3";
            }

        }
        else
        {
            redirecturl = string.Format("http://{0}", _surveyBasicInfoView.URL_REDIRECTION.Replace("http://", ""));
        }
        var iframeSrc = new StringBuilder();
        var strScript = new StringBuilder();
        if (Mode == RespondentDisplayMode.Respondent || Mode == RespondentDisplayMode.RespondentWithRedirection)
        {
            iframeSrc.AppendLine(@"function doRedirect(){ window.location.href='" + redirecturl + "'; }");
        }
        else
        {
            iframeSrc.AppendLine("function makeFrame() {");
            iframeSrc.AppendLine("var ifrm = document.getElementById('ContentMain_divRedirectMsg');");
            iframeSrc.AppendLine("var msgDiv = document.getElementById('ContentMain_divSurveyLastPage');");
            iframeSrc.AppendLine("var footerDiv = document.getElementById('divFooter');");
            iframeSrc.AppendLine("msgDiv.style.display = 'none';");
            iframeSrc.AppendLine("footerDiv.style.display = 'none';");
            iframeSrc.AppendLine("ifrm.style.display = 'block';");
            iframeSrc.AppendLine("}");
            iframeSrc.AppendLine(@"function doRedirect(){ makeFrame(); }");
        }
        strScript.AppendLine("var myTimeReminder, myTimeOut;");
        strScript.AppendLine("clearTimeout(myTimeOut);");
        strScript.AppendLine("var sessionTimeout = " + intMilliSecondsTimeOut + ";");
        strScript.AppendLine(iframeSrc.ToString());
        strScript.AppendLine("myTimeOut=setTimeout('doRedirect()', sessionTimeout);");

        if (Mode == RespondentDisplayMode.Respondent || Mode == RespondentDisplayMode.RespondentWithRedirection || Mode == RespondentDisplayMode.Preview || Mode == RespondentDisplayMode.PreviewWithPagination)
        {
            ClientScript.RegisterClientScriptBlock(typeof(Page), "CheckSessionOut", strScript.ToString(), true);
        }
        if (Mode == RespondentDisplayMode.Respondent || Mode == RespondentDisplayMode.RespondentWithRedirection)
            return;
        lblRedirectMsg.Text = string.Format("Respondents would be re-directed to {0} after {1} seconds.", redirecturl, time);
        lblRedirectMsg.Visible = true;
    }

    /// <summary>
    /// Gets or sets the survey id.
    /// </summary>
    /// <value>
    /// The survey id.
    /// </value>
    public new int SurveyId { get; set; }

    /// <summary>
    /// Gets or sets the type of the page.
    /// </summary>
    /// <value>
    /// The type of the page.
    /// </value>
    public int PageType { get; set; }

    public int RespondentId { get; set; }

    public int truecaller { get; set; }

    public string screenoutstatus { get; set; }

    public static string GeTHTMLString(string url)
    {
        // used to build entire input
        StringBuilder sb = new StringBuilder();

        // used on each read operation
        byte[] buf = new byte[8192];

        // prepare the web page we will be asking for
        HttpWebRequest request = (HttpWebRequest)
            WebRequest.Create(url);

        // execute the request
        HttpWebResponse response = (HttpWebResponse)
            request.GetResponse();

        // we will read data via the response stream
        Stream resStream = response.GetResponseStream();

        string tempString = null;
        int count = 0;

        do
        {
            // fill the buffer with data
            count = resStream.Read(buf, 0, buf.Length);

            // make sure we read some data
            if (count != 0)
            {
                // translate from bytes to ASCII text
                tempString = Encoding.ASCII.GetString(buf, 0, count);

                // continue building the string
                sb.Append(tempString);
            }
        }
        while (count > 0); // any more data to read?
        return sb.ToString();
    }
    public SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet getCustomizeURLbit(int surveyID)
    {
        SqlConnection con = new SqlConnection();
        try
        {

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetCustomizeURL", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsSurveyQA = new DataSet();
            sda.Fill(dsSurveyQA);
            return dsSurveyQA;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }
    }
    public DataSet getmcenturl()
    {
        SqlConnection con = new SqlConnection();
        try
        {

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetmcentURL", con);
            scom.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsmcenturl = new DataSet();
            sda.Fill(dsmcenturl);
            return dsmcenturl;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }
    }
    public DataSet updatemcenturlstatus(int respondentid, int mcentid)
    {
        SqlConnection con = new SqlConnection();
        try
        {

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updatemcentURL", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@respondentid", SqlDbType.Int)).Value = respondentid;
            scom.Parameters.Add(new SqlParameter("@pkmcentid", SqlDbType.Int)).Value = mcentid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdatemcent = new DataSet();
            sda.Fill(dsupdatemcent);
            return dsupdatemcent;


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }
    }
}