﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="SurveyCredits.aspx.cs" Inherits="UserControls_SurveyCredits" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <link href="App_Themes/Classic/SurveyRespondent.css" rel="stylesheet" type="text/css" />
    <title></title>
   
</head>
<body>
    <form id="form1" runat="server">
    <div>
  
<table style=" background:#fff; width:350px; height:210px; border-color:#666666; border-color:Purple"   >
<tr>
<td  style=" font-size:14px; background-color:#F4F4F4; border-color:#C150D2; width:400px; color:White; height:43px; color:#B765C8;  font-family:Arial,sans-serif;">
<asp:Label ID="lblcredittext" runat="server"  Text="Label"  style="font-size:14px;font-weight:normal;color:#B765C8; font-family:Arial,sans-serif; text-align:center;" Width="300px"></asp:Label>
    &nbsp;</td>
</tr>
<tr>
<td align="center">
    
</td>
</tr>
<tr>
<td align="center" style=" border-color:#CDCDCD; width:402px; height:150;">
<div id="pps_buynow" runat="server" style="padding-bottom:5px">
    <asp:RadioButtonList ID="Rbl_buynow" runat="server" 
        style="font-size:13px; font-weight:normal; color:#3F3838; font-family:Arial,sans-serif;" 
        Height="60px">
        <asp:ListItem Value="buypro"  >&nbsp;&nbsp;Buy Pro-Single – $9/Survey</asp:ListItem>
        <asp:ListItem Value="buyprem" >&nbsp;&nbsp;Buy Premium-Single – $29/Survey</asp:ListItem>
    </asp:RadioButtonList>

     <asp:RadioButtonList ID="Rbl_buynowindia" runat="server" 
        style="font-size:13px; font-weight:normal; color:#3F3838; font-family:Arial,sans-serif;" 
        Height="60px">
        <asp:ListItem Value="buypro"  >&nbsp;&nbsp;Buy Pro-Single – &#8377;399/Survey</asp:ListItem>
        <asp:ListItem Value="buyprem" >&nbsp;&nbsp;Buy Premium-Single – &#8377;1,299/Survey</asp:ListItem>
    </asp:RadioButtonList>
</div>
<div id="pps_pro" runat="server" style="padding-bottom:5px">
    <table>
    <tr>
    <td>
    <asp:RadioButtonList ID="Rbl_pro" runat="server" 
            style="font-size:13px; font-weight:normal; color:#3F3838;font-family:Arial,sans-serif;" 
            Height="60px">
        <asp:ListItem Value="apply_pro">&nbsp;&nbsp;Apply Pro-Single Credit</asp:ListItem>
        <asp:ListItem Value="buy_prem" >&nbsp;&nbsp;Buy Premium-Single – $29/Survey</asp:ListItem>
    </asp:RadioButtonList>

     <asp:RadioButtonList ID="Rbl_proindia" runat="server" 
            style="font-size:13px; font-weight:normal; color:#3F3838;font-family:Arial,sans-serif;" 
            Height="60px">
        <asp:ListItem Value="apply_pro">&nbsp;&nbsp;Apply Pro-Single Credit</asp:ListItem>
        <asp:ListItem Value="buy_prem" >&nbsp;&nbsp;Buy Premium-Single - &#8377;1,299/Survey</asp:ListItem>
    </asp:RadioButtonList>

    </td>
    <td>
    <table style=" height:60px">
    <tr>
    <td>
        <asp:Label ID="lblapplypro" runat="server" Text="Label" Visible="false" style="font-size:12px; font-weight:normal; color:#3F3838; height:60px; font-family:Arial,sans-serif;"></asp:Label>
    </td>
    </tr>
    <tr>
    <td style="font-size:12px; font-weight:normal; color:#3F3838; font-family:Arial,sans-serif;">
    &nbsp;
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
</div>
<div id="pps_Basic" runat="server" style="padding-bottom:5px">
<table>
<tr>
<td>
    <asp:RadioButtonList ID="Rbl_basic" runat="server" 
        style="font-size:13px; font-weight:normal; color:#3F3838;font-family:Arial,sans-serif;" 
        Height="60px">
        <asp:ListItem  Value="apply_pro" >&nbsp;&nbsp;Apply Pro-Single Credit</asp:ListItem>
        <asp:ListItem Value="apply_prem" >&nbsp;&nbsp;Apply Premium-Single Credit</asp:ListItem>     
    </asp:RadioButtonList>

</td>
<td>
<table style=" height:60px">
<tr>
<td>
<asp:Label ID="lblapplypro1" runat="server" Text="Label" Visible="false" style="font-size:12px; font-weight:normal; color:#3F3838; font-family:Arial,sans-serif;"></asp:Label>
</td>
</tr>
<tr>
<td>
<asp:Label ID="lblapplyprem1" runat="server" Text="Label" Visible="false" style="font-size:12px; font-weight:normal; color:#3F3838; font-family:Arial,sans-serif;"></asp:Label>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<div id="pps_prem" runat="server" style="padding-bottom:5px" >
<table>
<tr>
<td>
    <asp:RadioButtonList ID="Rbl_prem" runat="server" 
        style="font-size:13px; font-weight:normal; color: #3F3838;font-family:Arial,sans-serif;" 
        Height="60px">        
        <asp:ListItem Value="buy_pro" >&nbsp;&nbsp;Buy Pro-Single – $9/Survey</asp:ListItem>
        <asp:ListItem Value="apply_prem"  >&nbsp;&nbsp;Apply Premium-Single Credit</asp:ListItem>
    </asp:RadioButtonList>

    <asp:RadioButtonList ID="Rbl_premindia" runat="server" 
        style="font-size:13px; font-weight:normal; color: #3F3838;font-family:Arial,sans-serif;" 
        Height="60px">        
        <asp:ListItem Value="buy_pro" >&nbsp;&nbsp;Buy Pro-Single – &#8377;399/Survey</asp:ListItem>
        <asp:ListItem Value="apply_prem"  >&nbsp;&nbsp;Apply Premium-Single Credit</asp:ListItem>
    </asp:RadioButtonList>

</td>
<td>
<table style=" height:60px">
<tr>
<td style="font-size:12px; font-weight:normal; color:#3F3838; font-family:Arial,sans-serif; display:none">
&nbsp;
</td>
</tr>
<tr>
<td>
<asp:Label ID="lblapplyprem" runat="server" Text="Label" Visible="false" style="font-size:12px; font-weight:normal; color:#3F3838; font-family:Arial,sans-serif;"></asp:Label>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>

<div align="center">
<asp:Button ID="btnconfirm" runat="server" Text="Confirm" style="font-size:14px;background-color:#FF7708; color:White; width:90px;font-family:Arial,sans-serif; height:25px; border:0; fonfont-weight:bold; border-bottom-left-radius:5px;border-bottom-right-radius:5px;border-top-left-radius:5px;border-top-right-radius:5px"
        onclick="btnconform_Click" Font-Size="15px" Font-Bold="True" ></asp:Button>


</div>
</td>
</tr>
</table>
    </div>
    </form>
</body>
</html>








    

