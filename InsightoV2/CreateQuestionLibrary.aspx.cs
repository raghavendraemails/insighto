﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using System.Configuration;
using App_Code;

public partial class CreateQuestionLibrary : BasePage
{
    int surveyId=0,userId=0;
    public int copySurveyId
    {

        get
        {
            Hashtable ht = new Hashtable();           
            if (Request["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request["key"].ToString());
                if (ht != null && ht.Contains("SurveyId"))
                    surveyId = ValidationHelper.GetInteger(ht["SurveyId"].ToString(), 0);
            }
            return surveyId;
        }
    }

    public int _userId
    {

        get
        {
            Hashtable ht = new Hashtable();
            if (Request["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request["key"].ToString());
                if (ht != null && ht.Contains("UserId"))
                    userId = ValidationHelper.GetInteger(ht["UserId"].ToString(), 0);
            }
            return userId;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
            {
        if (!IsPostBack)
        {
            BindTemplateCategory();
            
                    var survey = ServiceFactory.GetService<SurveyService>().GetSurveyBySurveyId(copySurveyId).FirstOrDefault();
                    if (survey != null)
                    {
                        txtSurveyName.Text = survey.SURVEY_NAME.Trim();
                    }
                }
    }
    private void BindTemplateCategory()
    {
        var survey = ServiceFactory.GetService<SurveyService>();
        var surveyDetails = from surveydata in survey.GetSurveyByUserID(-1)
                            where surveydata.TEMPLATE_CATEGORY != null
                            orderby surveydata.TEMPLATE_CATEGORY ascending
                            group surveydata by surveydata.TEMPLATE_CATEGORY into grouped
                            select new { TEMPLATE_CATEGORY = grouped.Key };
        ddlTemplateCategory.DataSource = surveyDetails;
        ddlTemplateCategory.DataValueField = "TEMPLATE_CATEGORY";
        ddlTemplateCategory.DataTextField = "TEMPLATE_CATEGORY";
        ddlTemplateCategory.DataBind();
        ddlTemplateCategory.SelectedIndex = 0;
        BindTemplateSubCategory(ddlTemplateCategory.SelectedValue);

    }
    protected void ddlTemplateCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindTemplateSubCategory(ddlTemplateCategory.SelectedItem.Value);
    }
    private void BindTemplateSubCategory(string category)
    {
        var survey = ServiceFactory.GetService<SurveyService>();
        var surveyDetails = from surveydata in survey.GetSurveyByUserID(-1)
                            where surveydata.TEMPLATE_CATEGORY == category
                            orderby surveydata.TEMPLATE_SUBCATEGORY ascending
                            group surveydata by surveydata.TEMPLATE_SUBCATEGORY into grouped
                            select new { TEMPLATE_SUBCATEGORY = grouped.Key };
        ddlTemplateSubCategory.DataSource = surveyDetails;
        ddlTemplateSubCategory.DataValueField = "TEMPLATE_SUBCATEGORY";
        ddlTemplateSubCategory.DataTextField = "TEMPLATE_SUBCATEGORY";
        ddlTemplateSubCategory.DataBind();
        ddlTemplateSubCategory.Items.Insert(0, "Select");

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        var surveyService = ServiceFactory.GetService<SurveyService>();

        //int copySurveyID = ValidationHelper.GetInteger(Request["copySurveyId"].ToString(), 0);
        //int newSurveyID = ValidationHelper.GetInteger(Request["newSurveyId"].ToString(), 0);
        var oldSurvey = surveyService.GetSurveyBySurveyId(copySurveyId).FirstOrDefault();
        var folderDetails = ServiceFactory.GetService<FolderService>().GetFoldersByUserId(-1);
        oldSurvey.USERID = -1;
        oldSurvey.SURVEY_NAME = txtSurveyName.Text.Trim();
        oldSurvey.TEMPLATE_CATEGORY = ddlTemplateCategory.SelectedValue;
        if (ddlTemplateSubCategory.SelectedIndex > 0)
        {
            oldSurvey.TEMPLATE_SUBCATEGORY = ddlTemplateSubCategory.SelectedValue;
        }
        oldSurvey.FOLDER_ID = folderDetails.FOLDER_ID;
        oldSurvey.STATUS = "Draft";
        var newSurvey = surveyService.AddSurvey(oldSurvey);
        if (newSurvey != null)
        {
            surveyService.CopySurveyDet(copySurveyId, newSurvey.SURVEY_ID);
            var surveDet = surveyService.GetSurveyBySurveyId(newSurvey.SURVEY_ID);
            if (surveDet != null)
            {
                divSucessPanel.Visible = true;
                divErrMsg.Visible = false;
                lblSuccessMsg.Text = Utilities.ResourceMessage("CreateQueLibrary_Success_Created"); //"Question library created successfully.";
                base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("../Admin/UserDashboard.aspx","Id="+_userId), ConfigurationManager.AppSettings["TimeOut"]);  
            }
        }
        else
        {
            divErrMsg.Visible = true;
            lblErrMsg.Text = Utilities.ResourceMessage("CreateQueLibrary_Error_Created"); //"This survey already exist in question library.";
        }
    }
}