﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="Insighto.Pages.ForgotPassword" meta:resourcekey="PageResource1" %>
<%@ Register TagPrefix="uc" TagName="HomeHeader" Src="~/UserControls/HomeHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc:HomeHeader ID="HomeHeader" runat="server" />
    <div> 

      <div class="breadcrumbs-v3 img-v1 text-center">
        <div class="container">
       <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;">
            <span>Forgot Password </span></h2>
        </div>
      </div>
      <div class="container content">
        <div class="row">
          <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
              <div class="reg-header">
                <h2 style="color:#1035A2 !important;">RESET PASSWORD</h2>
              </div>
              <div class="input-group margin-bottom-20"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" class="form-control"></asp:TextBox>
              </div>

                <div class="row">
                <div class="col-md-12" align="left">
                    <asp:Button ID="btnResetPassword" OnClick="btnResetPassword_Click" class="btn-u" Text="Reset Password" ToolTip="Reset Password" runat="server"/>
                </div>
              </div>
                <div class="row" id="divalert" runat="server">
                <div class="col-md-12 center-block alert-danger">
                <asp:Label ID="lblInvaildAlert" class="lblRequired " runat="server" Visible="False"
                                                        Text="In valid Email" meta:resourcekey="lblInvaildAlertResource1"></asp:Label></div> 
                </div>
          </div>
        </div>
        <!--/row--> 
      </div>

    </div>
    
    
</asp:Content>
