﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using System.Collections;

namespace Insighto.Pages
{
    public partial class UpgradeLicense : SecurePageBase
    {
        Hashtable ht = new Hashtable();
        private int surveyId=0;
        private string prem = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Contains("SurveyId"))
                    {
                        surveyId = ValidationHelper.GetInteger(ht["SurveyId"].ToString(), 0);
                    }
                    if (ht != null && ht.Contains("pre"))
                    {
                       
                        lblDetails.Visible = false;
                        lblFeature.Visible = false;
                        lblExperience.Visible = false;
                    }
                    else
                    {
                        lblprodetails.Visible = false;
                        lblproexperience.Visible = false;
                        lblprofreatures.Visible = false;
                        
                    }
                    if (ht != null && ht.Contains("Flag"))
                    {

                        if (ht["Flag"].ToString() == "yes")
                        {
                            lnkClose.Attributes.Add("onclick", "javascript:closeModalSelf(true,'" + EncryptHelper.EncryptQuerystring(PathHelper.GetUrlRedirectionURL().Replace("~/", ""), "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag) + "')");
                        }
                        else if (ht["Flag"].ToString() == "alert")
                        {
                            lnkClose.Attributes.Add("onclick", "javascript:closeModalSelf(true,'" + EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyAlertsURL().Replace("~/", ""), "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag) + "')");
                        }
                        else if (ht["Flag"].ToString() == "messages")
                        {
                            lnkClose.Attributes.Add("onclick", "javascript:closeModalSelf(true,'" + EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyMessagesURL().Replace("~/", ""), "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag) + "')");
                        }

                        //else if (ht["Flag"].ToString() == "ProgressBar")
                        //{
                        //     lnkClose.Attributes.Add("onclick", "javascript:closeModalSelf(true,'" + EncryptHelper.EncryptQuerystring("ProgressBar.aspx", "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag) + "')");
                        //}
                    }
                    else
                    {

                        lnkClose.Attributes.Add("onclick", "javascript:closeModalSelf(false,'')");
                    }
                }
                else
                {
                    lblprodetails.Visible = false;
                    lblproexperience.Visible = false;
                    lblprofreatures.Visible = false;
                }

            }
            var session = ServiceFactory.GetService<SessionStateService>();
            var userService = ServiceFactory.GetService<UsersService>();
            var sessdet = session.GetLoginUserDetailsSession();
            if (sessdet != null)
            {
                string renew_link = "";

                renew_link = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + sessdet.UserId + "&Prem=Prem");
               // renew_link = EncryptHelper.EncryptQuerystring(PathHelper.GetPricingURL(), "UserId=" + sessdet.UserId + "&utm_source=insighto&Prem=" + ht.Contains("pre"));
                var userInfo = userService.GetUserDetails(sessdet.UserId);


                if (sessdet != null && sessdet.UserId > 0 && userInfo[0].RENEWAL_FLAG == 1)
                {
                    //this.ClientScript.RegisterStartupScript(this.GetType(), "Some Title", "<script language=\"javaScript\">" + "alert('User is already Renewed');" + "<" + "/script>");
                }
                else
                {
                    hyp_priclink.Target = "_blank";
                    hyp_priclink.HRef = renew_link;
                }
            }

            else
            {
                Response.Redirect(PathHelper.GetUserLoginPageURL());
            }
        }

        protected void cmdback_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathHelper.GetUserMyAccountPageURL());
        }
    }
}