﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="MyAccounts.aspx.cs" Inherits="MyAccounts" EnableEventValidation="true" %>

<%@ Register Src="~/UserControls/NotificationControl.ascx" TagName="NotificationControl" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--<div class="popbox" id="abc" runat="server">
<div class='content'>
<img src='http://www.developertips.net/demos/popbox-dialog/img/x.png' alt='quit' class='x' id='x' />
<div  >
        <table width="100%" align="center" border="0" cellspacing="0">
            <tr>
             <td   bgcolor="#994ea3" align="center"  border="1" style="font-weight: 500; font-family: Tahoma; font-size: small; color: #FFFFFF; vertical-align: middle">
              <b>Apply/Buy Survey Credits
                 </b>  
                </td>
            </tr>
            <tr>
                <td >
                </td>
            </tr>
            <tr>
                <td id="Td2">
                <div id="ppsprodv">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                    
                        <asp:ListItem Value="1" >PRO_CREDITS</asp:ListItem>
    
                        <asp:ListItem Value="2">PREM_CREDITS</asp:ListItem>
                        <asp:ListItem Value="3">BUY NOW</asp:ListItem>
                        
                    </asp:RadioButtonList> 
                    </div> 

                     <div id="ppspemdiv">
                    <asp:RadioButtonList ID="RadioButtonList2" runat="server">
                    
                        <asp:ListItem Value="2">PREM_CREDITS</asp:ListItem>
                        <asp:ListItem Value="3">BUY NOW</asp:ListItem>
                        
                    </asp:RadioButtonList> 
                    </div> 
                   &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<button id="btnconfirm"  onclick='javascript: return Survey();' type="button" >Confirm</button> 
               
                           
                </td>
            </tr>
           
        </table>
    </div>
<a href='' class='close'>Close</a>

</div>
</div>--%>


    <div>
          <!-- notification panel -->
        <uc1:NotificationControl ID="NotificationControl1" runat="server" />
        <!-- //notification panel -->    
    </div>
   
    <div class="contentPanelHeader">
        <input id="hdnStatus" class="hdnStatus" type="hidden" value="All" />
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <span id="spnHeading" runat="server"></span>
            <!-- //page title -->
        </div>
        <div class="previewPanel actionLinks">
            <!-- preview link -->
            <div class="surveyTabHyperLinks">
               <asp:Label ID="lblFilterBy" runat="server" meta:resourcekey="lblFilterBy"></asp:Label><a href="#" class="active" id="lnkAll"><asp:Label ID="lblAll" 
                    runat="server" meta:resourcekey="lblAllResource1"></asp:Label></a>|<a href="#" class="blackLink"
                    id="lnkActive"><asp:Label ID="lblActive" runat="server" meta:resourcekey="lblActiveResource1"></asp:Label></a>|<a href="#" class="blackLink" id="lnkClose">
                    <asp:Label ID="lblClosed" runat="server" meta:resourcekey="lblClosedResource1"></asp:Label></a>|<a
                        href="#" class="blackLink" id="lnkDraft"><asp:Label ID="lblDraft" runat="server"  meta:resourcekey="lblDraftResource1"></asp:Label></a>
                <!-- //preview link -->
            </div>
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="clear">
    </div>
    <%--<div class="adminGridSearchPanel">
    <div class="searchLble">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblSearch" runat="server" Text="Search"></asp:Label>
                    </div>
                    <div class="searchTxtBoxPanel">
     &nbsp;&nbsp; 
      </div>
    </div>--%>
    
    <div class="contentPanel">
    <div class="clear" visible="true">
    &nbsp;
    </div>
    <div>
    <table width="100%">
    <tr>
    <td width="46%">
    <input type="text" class="textBoxAdminSearch"  id="txtSearch" 
            onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);" />
    </td>
    <td  width="54%">
    <table >
    <tr>
    <td style="  color:#3f3f3f;font-family:Arial,sans-serif;" valign="middle">
      <a href="#" class="tooltip" style=" font-weight:bold" id="tolltip" runat="server"> Available Survey Credits&nbsp;<img src="App_Themes/Classic/Images/icon-help.gif" width="13px" height="12px" />&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;<span><b></b>This count represents the number of single surveys you have purchased. You can apply these credits to any of your surveys. The count will get debited only when you are launching the survey.</span></a><asp:Label ID="lblcredits" runat="server" 
             Height="30px" style="border:2px solid #CCCCCC; padding-top:5px"></asp:Label>
    </td>
    
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </div>
        <p class="defaultHeight">
        </p>
        <div class="successPanel" id="divSuccess" style="display: none">
            <div>
                <%-- <label id="lblMessage" />--%>
                <asp:Label ID="lblMessage" CssClass="successMsg" runat="server" 
                    Style="display: none;" meta:resourcekey="lblMessageResource1"></asp:Label>
            </div>
        </div>
        <div class="text" style="display: none">
            <div class="newSurevyPanel">
                <h2 style="color:#FF7F27;">
                    <asp:Label ID="lblYourAccountHasNo" runat="server" 
                        meta:resourcekey="lblYourAccountHasNoResource1" style="display:none;"></asp:Label>  
                    <asp:Label ID="lblSurvey" runat="server" CssClass="lblSurvey" 
                        meta:resourcekey="lblSurveyResource1"  style="display:none;"></asp:Label>
                        <%-- <asp:Label ID="lblHeading" runat="server" Text="Welcome to Insighto!"></asp:Label>--%>
                </h2>
                      <br /><br />
             <div class ="SurveyStepAway">           
                 <table class="style1" width="475" cellspacing="0" cellpadding="0" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: medium; font-style: normal">
                    <tr>
                        <td  height="60">
                        You are just one step away from creating your first survey.<br />
                        <br />
                       <a id="lnkCreate" runat="server" h="500" w="700" class="btn_bg" scrolling='yes' title="Create Survey" rel="dropmenu1_a"><asp:Label ID="Label1" runat="server" Text="Start now!" CssClass="btncolor"></asp:Label></a><br />
                        </td>
                    </tr>
                </table>
                
             </div>              
               <%--<img src="App_Themes/Classic/Images/Arrow4.png" alt="" style="margin-top: -15%; margin-left: -22%; z-index: 9999999;"></img>--%>
              <%--  <a id="lnkCreate" runat="server" class="dynamicmainLink lnkCreate" rel="framebox"
                    h='480' w='700' scrolling='true'>Create Survey ></a>--%>
                <%--<img src="App_Themes/Classic/Images/button_create-survey.gif" alt="Create Survey"
                       title="Create Survey" />--%>
            </div>
        </div>
        <div class="clear">
        </div>
        <!-- content panel -->
        <div class="surveysGridPanel" id="ptoolbar">
        </div>
        <table id="tblJobKits">
        </table>
        <div class="clear">
        </div>
        <input type="hidden" id="hdnSurveyId" />
        <input type="hidden" id="Hidden1" />
        <input type="hidden" id="hdnSurveyFlag" runat="server" class="hdnSurveyFlag" />
    </div>


    &nbsp;&nbsp;&nbsp;


    <script type="text/javascript">

        var serviceUrl = 'AjaxService.aspx';
        var displayMode;
        var surveyFlag = $('.hdnSurveyFlag').val(); 

        var closeHandler=function(){
         $('#lnkClose').removeClass("blackLink").addClass("active");
            $('#lnkActive').removeClass("active").addClass("blackLink");
            $('#lnkDraft').removeClass("active").addClass("blackLink");
            $('#lnkAll').removeClass("active").addClass("blackLink");
            displayMode = 'Closed';
            $('#hdnStatus').val('Closed');
             $('#lnkActive').unbind();
               $('#lnkDraft').unbind();
                $('#lnkAll').unbind();
            gridReload(displayMode);
        }
        var activeHandler=function(){
         $('#lnkClose').removeClass("active").addClass("blackLink");
            $('#lnkActive').removeClass("blackLink").addClass("active");
            $('#lnkDraft').removeClass("active").addClass("blackLink");
            $('#lnkAll').removeClass("active").addClass("blackLink");
            displayMode = 'Active';
            $('#hdnStatus').val('Active');
              $('#lnkClose').unbind();
               $('#lnkDraft').unbind();
                $('#lnkAll').unbind();
            gridReload(displayMode);
        }
        var draftHandler=function(){
          $('#lnkClose').removeClass("active").addClass("blackLink");
            $('#lnkActive').removeClass("active").addClass("blackLink");
            $('#lnkDraft').removeClass("blackLink").addClass("active");
            $('#lnkAll').removeClass("active").addClass("blackLink");
            displayMode = 'Draft';
            $('#hdnStatus').val('Draft');
             $('#lnkClose').unbind();
               $('#lnkActive').unbind();
                $('#lnkAll').unbind();
            gridReload(displayMode);
        }
        var allHandler=function(){
          $('#lnkClose').removeClass("active").addClass("blackLink");
            $('#lnkActive').removeClass("active").addClass("blackLink");
            $('#lnkDraft').removeClass("active").addClass("blackLink");
            $('#lnkAll').removeClass("blackLink").addClass("active");
            displayMode = 'All';
            $('#hdnStatus').val('All');
              $('#lnkClose').unbind();
               $('#lnkActive').unbind();
                $('#lnkDraft').unbind();
            gridReload(displayMode);
        }
        $("#tblJobKits").jqGrid({
            url: serviceUrl,
            postData: { method: "GetSurveys", mode: displayMode ,surveyFlag:surveyFlag },
            datatype: 'json',
            colNames: <%=Headers %>,
            colModel: [
                 { name: 'survey_type', index: 'survey_type', width: 30, align: 'left', editable: false, resizable: false, size: 100  },
                { name: 'SurveyManagerUrl', index: 'SURVEY_NAME', width: 220, align: 'left', editable: false, resizable: false, size: 100},
                { name: 'UpgradeUrl', index: 'UpgradeUrl', width: 80, align: 'left', editable: false, resizable: false, size: 100,formatter: UpgradeFormatter,sortable: false },
                { name: 'Status', index: 'Status', width: 80, align: 'center', editable: false, size: 100, resizable: false},
                { name: 'TOTAL_RESPONSES', index: 'TOTAL_RESPONSES', width: 80, align: 'center', editable: false, resizable: false, size: 100 },
                { name: 'Complete', index: 'Complete', width: 120, align: 'center', editable: false, resizable: false, size: 100 },
                { name: 'Partial', index: 'Partial', width: 80, align: 'center', editable: false, resizable: false, size: 100 },
                { name: 'MODIFIED_ON', index: 'MODIFIED_ON', width: 100, align: 'center', editable: false, resizable: false, size: 100 },
                { name: 'appendedUrls', index: 'appendedUrls', width: 206, align: 'center', editable: false, resizable: false, size: 100, formatter: EditLinkFormatter, sortable: false }
                ],
            rowNum: <%=_recordsCount %>,
            rowList: [10, 20, 30, 40, 50],
            pager: '#ptoolbar',
            gridview: true,
            sortname: 'MODIFIED_ON',
            sortorder: "desc",
            viewrecords: true,
            jsonReader: { repeatitems: false },
            width: 898,
            caption: '',
            height: '100%',
            loadComplete: function () {
           // $('.actionLinks').show();
           $('#lnkClose').bind('click',closeHandler);
           $('#lnkDraft').bind('click',draftHandler);
           $('#lnkAll').bind('click',allHandler);
           $('#lnkActive').bind('click',activeHandler);
                if($("#tblJobKits").getGridParam("records") == -302)
                {
                    window.location.href = "/";
                }
                 else if($("#txtSearch").val()!="" ||$("#txtSearch").val()==null )
                {
                if(($("#tblJobKits").getGridParam("records") == 0))
                {
                 $('#gbox_tblJobKits').show();
                }
                }
                else if ($("#tblJobKits").getGridParam("records") == 0) {
                    $('#ptoolbar').hide();
                    $('#txtSearch').hide();
                    $('#gbox_tblJobKits').hide();                 
                    $('.text').show();
                    if(surveyFlag =="1")
                    {
                    switch($('#hdnStatus').val())
                    {
                        case 'Active':
                            $('.lblSurvey').text(' active Widgets');
                            break;
                        case 'Closed':
                            $('.lblSurvey').text(' closed  Widgets');
                            break;
                            case 'Draft':
                            $('.lblSurvey').text(' draft  Widgets');
                            break;
                        default:
                            $('.lblSurvey').text(' Widgets');
                     }                       
                        $('.lnkCreate').text('Create your first widget >');
                    }
                    else
                    {
                  
                     switch($('#hdnStatus').val())
                    {
                        case 'Active':                        
                            $('.lblSurvey').text(' active surveys');
                            break;
                        case 'Closed':
                            $('.lblSurvey').text(' closed  surveys');
                            break;
                            case 'Draft':
                            $('.lblSurvey').text(' draft  surveys');
                            break;
                        default:
                            $('.lblSurvey').text(' surveys');                           
                    }                      
                        $('.lnkCreate').text('Create your first survey >');
                    }                    
                }
                else {
                    $('#ptoolbar').show();
                    $('#gbox_tblJobKits').show();
                    $('.text').hide();
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }

            }
        });

        $("#tblJobKits").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh:false, caption: "Add" });
        //});

      function UpgradeFormatter(cellvalue, options, rowObject){
             var str;
             var mailUrls = cellvalue.split('~');

            

              if(mailUrls[3]=="FREE")
             {
              
              if(mailUrls[6]=="Closed")
              {
              str ="<a href='"+ mailUrls[0] +"'rel='framebox' w='350' h='210' title='Upgrade to Premium-Single'><img src='App_Themes/Classic/Images/ppsupgrade.png' width='55px' height='22px' /></a>";
              }
              else if(mailUrls[2]=="PPS_PRO" || mailUrls[2]=="BASIC" || mailUrls[2]=="")
              {
              if(mailUrls[2]=="BASIC")
              {
                  str ="<a href='"+ mailUrls[0] +"'rel='framebox' w='350' h='210' title='Upgrade'><img src='App_Themes/Classic/Images/ppsupgrade.png' width='55px' height='22px' /></a>";
              }
              else if(mailUrls[2]=="PPS_PRO")
              {
                  str ="<a href='"+ mailUrls[0] +"'rel='framebox' w='350' h='210' title='Upgrade to Premium-Single'><img src='App_Themes/Classic/Images/ppsupgrade.png' width='55px' height='22px' /></a>";
              }

            //                if (mailUrls[4] == "0" && mailUrls[5] == "0")
            //                {  
            //                    
            //                str ="<a href='"+ mailUrls[0] +"'  w='453' h='283' title='Upgrade'><img src='App_Themes/Classic/Images/ppsupgrade.png' width='50px' height='20px' /></a>";
            //                }
           //                else
           //                {        
               
               
               // }
             }
             else
             {
             str ="";
             }
             }
             else
             {
             str ="";
             }
             return str;            
         }

        function EditLinkFormatter(cellvalue, options, rowObject) {

            var mailUrls = cellvalue.split('~');
            var str;
            $('#hdnSurveyId').val(mailUrls[1]);
            for (var i = 2; i < mailUrls.length; i++) {
                var subUrl = mailUrls[i].split(':');
                if (subUrl[0] == "ActiveEdit") {
                    if (subUrl[1] != '#') {
                        str = "<a href='" + subUrl[1] + "' class='button-small-gap icon-Edit-active'  w='" + 600 + "' h='" + 600 + "' scrolling='yes' title='Edit'></a>";
                    }
                    else if (mailUrls[0] == "Closed") {
                        str = "<a href='javascript:void(0);'class='button-small-gap icon-Edit-off' title='Edit'></a>";
                    }
                    else {
                        str = "<a class='button-small-gap icon-Edit-off' title='Edit'></a>";
                    }
                }
                else if (subUrl[0] == "Preview") {
               
                  str += "<a href='" + subUrl[1] + "' class='button-small-gap icon-View-active' rel='framebox' h='480' w='950' scrolling='true' title='View'></a>";

              

                    if (mailUrls[7] == "VOICE")
                    {
                     str += "<a href='javascript:void(0);' class='button-small-gap icon-Del-off deleteSurvey' id='lnkDelete' title='Delete'></a>";
               //       str += "<a href='javascript:void(0);' class='button-small-gap icon-Close-off deleteSurvey' id='lnkClosePoll' title='Close'></a>";
                     }
                     else
                     {
                    str += "<a href='#' class='button-small-gap icon-Del-active deleteSurvey' id='lnkDelete' onclick='javascript: return DeleteSurvey(" + mailUrls[1] + ");'  title='Delete'></a>";
//                    
//                    if (mailUrls[0] == "Draft") {
//                      str += "<a href='javascript:void(0);' class='button-small-gap icon-Close-off' title='Close'></a>";
//                      }
//                      else
//                      {
//                     str += "<a href='#' class='button-small-gap icon-Close-active deleteSurvey' id='lnkClosePoll' onclick='javascript: return CloseSurvey(" + mailUrls[1] + ");'  title='Close'></a>";
//                   }
                    }
                    //onclick='javascript: return deletesurvey(" + mailUrls[1] + ");'
                }
                else if (subUrl[0] == "Report") {
                    if (mailUrls[0] == "Draft") {
                        str += "<a href='javascript:void(0);' class='button-small-gap icon-Reports-off' title='Reports'></a>";
                    }
                    else {
                        str += "<a href='" + subUrl[1] + "' class='button-small-gap icon-Reports-active' title='Reports'></a>";
                    }

                }
                else if (subUrl[0] == "Remainder") {
                    if (subUrl[1] != '#') {
                        str += "<a href='" + subUrl[1] + "' class='button-small-gap icon-Remainder-active' title='Reminder'></a>";
                    }
                    else {
                        str += "<a href='javascript:void(0);' class='button-small-gap icon-Remainder-off' title='Reminder'></a>";

                    }

                }
                if (subUrl[0] == "Launch") {

                    if (subUrl[1] != '#') {
                        str += "<a href='" + subUrl[1] + "' class='button-small-gap icon-Activate-draft' title='Launch'></a>";
                    }
                    else {
                        str += "<a href='javascript:void(0);' class='button-small-gap icon-Activate-draft-off' title='Launch'></a>";
                    }

                    if (mailUrls[0] == "Closed") {

                    if (mailUrls[7] == "VOICE")
                    {
                    str += "<a href='javascript:void(0);' class='button-small-gap icon-Launch-close-off' title='Activate'></a>";
                     
                    }
                    else
                    {
                          str += "<a href='" + subUrl[1] + "' class='button-small-gap icon-Launch-close' onclick='javascript: return ActivateSurvey(" + mailUrls[1] + ");'  title='Activate'></a>";
                        }
                    }
                    else {
                        str += "<a href='javascript:void(0);' class='button-small-gap icon-Launch-close-off' title='Activate'></a>";
                    }
                    
                }
                
            }
            return str;

        }

        function gridReload(keyword) {  
          
            $("#tblJobKits").jqGrid('setGridParam', { url: serviceUrl + "?mode=" + displayMode, page: 1 }).trigger("reloadGrid");
            
        }

        
        function ActivateSurvey(surveyId) {
            $.post("AjaxService.aspx", { "method": "ActivateSurveyBySurveyId", "Id": surveyId },
           function (result) {
               if (result == 'Activated') {
                   $('#divSuccess').removeClass('errorMessagePanel').addClass('successPanel');
                   $('#divSuccess').show();
                   $('.successMsg').show()
                   $('.successMsg').html('Survey activated successfully.');
//                   var status = $('#hdnStatus').val();
//                  
//                   gridReload(status);
               }
               else if (result == 'ProFeatures') {
                   $('#divSuccess').removeClass('successPanel').addClass('errorMessagePanel');
                   $('#divSuccess').show();
                    $('.successMsg').show()
//                   $('.successMsg').html('Survey has pro features please upgarde your license to activate this survey.');
                    $('.successMsg').html('Survey has features that come with Paid plans.Please upgrade to activate the survey.');

               }
           });
        }
        function DeleteSurvey(surveyId) {
            jConfirm('Are you sure you want to delete ?', 'Delete Confirmation', function (r) {
                if (r == '1') {
                    $.post("AjaxService.aspx", { "method": "DeleteSurveyBySurveyId", "Id": surveyId },
           function (result) {
               if (result == '0') {
                   $('#divSuccess').show();
                   $('.successMsg').show()
                   $('.successMsg').html('Survey deleted successfully.');
                   var status = $('#hdnStatus').val();   
                   location.reload();                
               }
               else {
                   $('#divSuccess').hide();
                   $('.successMsg').hide()                  
               }
           });
                }

            });
        }
         
        function gridReload() {
       
            var keyword = $("#txtSearch").val();
            if(keyword=="Search surveys")
            {
            keyword="";
            }
        
         $("#tblJobKits").jqGrid('setGridParam', { url: serviceUrl +"?keyword=" + keyword + "&mode="+ displayMode, page: 1 }).trigger("reloadGrid");
         
        }

        $("#txtSearch").bind('keyup', function () { gridReload(); });   

    </script>

  <style type="text/css">
   a.tooltip 
   {
       outline:none;
       text-decoration:none;
      
   } 
   a.tooltip strong 
   {
       line-height:30px;
   } a.tooltip > span  
   {
       width:250px; 
       padding: 10px 20px;
       margin-top: 20px; 
       font-weight:normal; 
       margin-left: -85px; 
       opacity: 0; 
       visibility: hidden; 
       z-index: 10; 
       position: absolute; 
       font-family: Arial; 
       font-size: 11px; 
       font-style: normal; 
       -webkit-border-radius: 3px; 
       -moz-border-radius: 3px; 
       -o-border-radius: 3px; 
       border-radius: 3px; 
       -webkit-box-shadow: 2px 2px 2px #999; 
       -moz-box-shadow: 2px 2px 2px #999; 
       box-shadow: 2px 2px 2px #999; 
       -webkit-transition-property:opacity, margin-top, visibility, margin-left; 
       -webkit-transition-duration:0.4s, 0.3s, 0.4s, 0.3s; 
       -webkit-transition-timing-function: ease-in-out, ease-in-out, ease-in-out, ease-in-out; 
       -moz-transition-property:opacity, margin-top, visibility, margin-left; 
       -moz-transition-duration:0.4s, 0.3s, 0.4s, 0.3s; 
       -moz-transition-timing-function: ease-in-out, ease-in-out, ease-in-out, ease-in-out; 
       -o-transition-property:opacity, margin-top, visibility, margin-left; 
       -o-transition-duration:0.4s, 0.3s, 0.4s, 0.3s; 
       -o-transition-timing-function: ease-in-out, ease-in-out, ease-in-out, ease-in-out; 
       transition-property:opacity, margin-top, visibility, margin-left; 
       transition-duration:0.4s, 0.3s, 0.4s, 0.3s; 
       transition-timing-function: ease-in-out, ease-in-out, ease-in-out, ease-in-out; }
        /*a.tooltip > span:hover,*/ 
        a.tooltip:hover > span  
        {
            opacity: 1; 
            text-decoration:none; 
            visibility: visible; 
            overflow: visible; 
            margin-top:40px; 
            display: inline; 
            margin-left: -140px;
        } 
        a.tooltip span b  
        {
            width: 15px; 
            height: 15px; 
            margin-left: 20px; 
            margin-top: -19px; 
            display: block; 
            position: absolute; 
            -webkit-transform: rotate(-45deg); 
            -moz-transform: rotate(-45deg); 
            -o-transform: rotate(-45deg); 
            transform: rotate(-45deg); 
            -webkit-box-shadow: inset -1px 1px 0 #fff; 
            -moz-box-shadow: inset 0 1px 0 #fff; 
            -o-box-shadow: inset 0 1px 0 #fff; 
            box-shadow: inset 0 1px 0 #fff; 
            display: none\0/; *display: none;
          } 
          a.tooltip > span 
          { 
              color: #000000; 
              background: #EEEEEE; 
              background: -moz-linear-gradient(top, #EEEEEE 0%, #EEEEEE 100%); 
              background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#EEEEEE), color-stop(100%,#EEEEEE)); 
              filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#EEEEEE', endColorstr='#EEEEEE',GradientType=0 ); 
              border: 1px solid #DDDDDD; 
          } 
          a.tooltip span b 
          {
               background: #EEEEEE; 
               border-top: 1px solid #DDDDDD; 
               border-right: 1px solid #DDDDDD; 
          } 
  
     
  </style>



 <style type="text/css">
 .btncolor
{
    
    font-family:Arial,sans-serif; 
    border:0; 
    fonfont-weight:bold;
    border-bottom-left-radius:5px;
    border-bottom-right-radius:5px;
    border-top-left-radius:5px;
    border-top-right-radius:5px;

	font-size:15px;
	font-weight:bold;
	color:#FFF;
	cursor:pointer;
	cursor:hand;
	
}
    .textBoxAdminSearch {
	background:url('../images/text-box-bgr.jpg') #FCFCFC bottom repeat-x;
	border:solid 1px #CDCDCD;
	padding:8px 0 8px 5px;
	margin:0 5px 0 0;
	font-size:12px;
	width:240px;
}
.adminGridSearchPanel {
	margin:0 0 10px 0;
	height:40px;
	line-height:40px;
	
}
    .searchLble {
	width:60px;
	float:left;
	color:#000 ;
	font-weight:700;
	font-size:14px;
	text-align:left;
}

.searchTxtBoxPanel, .gridTitlePanel {
	width:628px;
	float:left;
	height:36px;
	line-height:36px; 
	padding-left:5px;
	padding-top:4px;
	margin:0 0 0 0;
}
    </style>
   <script language="javascript" type="text/javascript">

        function WaterMark(objtxt, event) {
          
            var defaultText = "Search surveys";
            
            // Condition to check textbox length and event type
            if (objtxt.id == "txtSearch") {
                if (objtxt.value.length == 0 & event.type == "blur") {
                    //  alert(event.type);
                    //if condition true then setting text color and default text in textbox
                    if (objtxt.id == "txtSearch") {
                        objtxt.style.color = "Gray";
                        objtxt.style.fontsize = "10em";
                        objtxt.value = defaultText;

                    }
                }
            }

            if ((objtxt.value == defaultText) & event.type == "focus") {

                if (objtxt.id == "txtSearch") {
                    objtxt.style.color = "black";
                    objtxt.value = "";
                }
            }

                }
    </script>


<%-- <link rel="stylesheet" type="text/css" href="mktg/css/style.css" />
<style type="text/css">
#overlay {
position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background-color: #000;
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
z-index: 100;
display: none;
}
.content a{
text-decoration: none;
}
.popbox{
width: 100%;
margin: 0;
position: fixed;
display:none;
z-index: 101;
}
.content{
min-width: 300px;
width: 300px;
min-height: 150px;
margin: 100px auto;
background: #f3f3f3;
position: relative;
z-index: 103;
padding: 10px;
border-radius: 5px;
box-shadow: 0 2px 5px #000;
}
.content p{
clear: both;
color: #555555;
text-align: justify;
}
.content p a{
color: #d91900;
font-weight: bold;
}
.content .x{
float: right;
height: 35px;
left: 10px;
position: relative;
top: -25px;
width: 34px;
}
.content .x:hover{
cursor: pointer;
}
</style>
<script type='text/javascript'>
    $(function () {
        var overlay = $('<div id="overlay"></div>');
        $('.close').click(function () {
            $('.popbox').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });

        $('.x').click(function () {
            $('.popbox').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });
    }); 
</script>
--%>

<script type="text/javascript">
    $(document).ready(function () {
        $("#tblJobKits tr:even").css("background-color", "#dedede");
        $("#tblJobKits tr:odd").css("background-color", "#ffffff");
    });
</script>

</asp:Content>
