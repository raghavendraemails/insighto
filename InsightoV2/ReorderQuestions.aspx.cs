﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Extensions;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using App_Code;

public partial class ReorderQuestions : SecurePageBase
{

    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int surveyId = 0;
    int questionId = 0;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = false;
        

        if (!IsPostBack)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains("SurveyId"))
                    surveyId = int.Parse(typeHt["SurveyId"].ToString());
                if (typeHt.Contains("QuestionId"))
                {
                    questionId = int.Parse(typeHt["QuestionId"].ToString());
                    ViewState["QuestionId"] = typeHt["QuestionId"].ToString();
                }
                else
                {
                    ViewState["QuestionId"] = 0;
                }

            }

            var questionService = ServiceFactory.GetService<QuestionService>();
            var surveyQuestionsList = questionService.GetAllSurveyQuestionsBySurveyId(surveyId);

            if (surveyQuestionsList.Any())
            {
                dvQuestionReorder.Visible = true;

                int QuesNo = 1;
                this.lbQuesList.Items.Clear();
                bool isLenghtExceeded = false;                
                foreach (var qus in surveyQuestionsList)
                {
                    string Queslbl = "";                    
                    var questionTypeId = GenericHelper.ToEnum<QuestionType>(ValidationHelper.GetInteger(qus.QUESTION_TYPE_ID, 0));

                    if (questionTypeId != QuestionType.Narration && questionTypeId != QuestionType.PageHeader && questionTypeId != QuestionType.QuestionHeader && questionTypeId != QuestionType.Image && questionTypeId != QuestionType.Video)
                    {
                        if (QuesNo > 9)
                        {
                            Queslbl = "Q" + QuesNo++;
                        }
                        else
                        {
                            Queslbl = "Q0" + QuesNo++;
                        }
                        //Queslbl = "Q" + qus.QUESTION_SEQ.ToString();
                    }
                    else if (questionTypeId == QuestionType.Narration)
                        Queslbl = "N ";
                    else if (questionTypeId == QuestionType.PageHeader)
                        Queslbl = "P ";
                    else if (questionTypeId == QuestionType.QuestionHeader)
                        Queslbl = "Q ";
                    else if (questionTypeId == QuestionType.Image)
                        Queslbl = "I ";
                    else if (questionTypeId == QuestionType.Video)
                        Queslbl = "V ";

                    string PlnQues = CommonMethods.clearHTML(qus.QUSETION_LABEL);
                    Queslbl += (questionTypeId == QuestionType.Image) ? " Image" : " " + PlnQues;
                    //Queslbl += (questionTypeId == QuestionType.Video) ? " Video" : " " + PlnQues;

                    ListItem li = new ListItem();
                    li.Text = Queslbl;
                    li.Value = qus.QUESTION_ID.ToString();
                    if (Queslbl.Length > 83)
                    {
                        isLenghtExceeded = true;
                    }
                    this.lbQuesList.Items.Add(li);

                    if (qus.SKIP_LOGIC == 1)
                    {
                        cmdMoveQuesUp.Enabled = false;
                        cmdMoveQuesDown.Enabled = false;
                        btnSaveReorder.Enabled = false;

                        cmdMoveQuesUp.CssClass = "reorder_upimg_off";
                        cmdMoveQuesDown.CssClass = "reorder_downimg_off";
                        btnSaveReorder.CssClass = "defaultButtonGray";

                        lblReorderAlert.Visible = true;
                        dvReorderNotpossible.Visible = true;
                    }
                }
                if (!isLenghtExceeded)
                {
                    this.lbQuesList.Style.Add("width", "620px");
                }

                if (ViewState["QuestionId"] != null)
                {
                    if (ViewState["QuestionId"].ToString() == "0")
                    {
                        this.lbQuesList.SelectedIndex = 0;
                    }
                    else
                    {
                        int iIndex = lbQuesList.Items.IndexOf(lbQuesList.Items.FindByValue(ViewState["QuestionId"].ToString()));
                        this.lbQuesList.SelectedIndex = iIndex;
                    }
                }
                else
                {
                    this.lbQuesList.SelectedIndex = 0;
                }

            }
            else
            {
                dvQuestionReorder.Visible = false;
                lblErrMsg.Text = Utilities.ResourceMessage("ReorderQuestions_Error_NoQuestions"); //"Reorder questions cannot be applied as there are no questions in the survey.";
                dvErrMsg.Visible = true;
            }
        }
    }

    protected void cmdMoveQuesUp_Click(object sender, EventArgs e)
    {
        UpClick();        
    }
    protected void cmdMoveQuesDown_Click(object sender, EventArgs e)
    {
        DownClick();
    }

    private void UpClick()
    {
        // only if the first item isn't the current one
        if (lbQuesList.SelectedIndex > 0)
        {

            ListItem li = new ListItem();
            li.Text = lbQuesList.SelectedItem.Text;
            li.Value = lbQuesList.SelectedValue;

            // add a duplicate item up in the listbox
            lbQuesList.Items.Insert(lbQuesList.SelectedIndex - 1, li);
            // make it the current item
            lbQuesList.SelectedIndex = (lbQuesList.SelectedIndex - 2);
            // delete the old occurrence of this item
            lbQuesList.Items.RemoveAt(lbQuesList.SelectedIndex + 2);
        }
    }

    private void DownClick()
    {
        // only if the last item isn't the current one
        if ((lbQuesList.SelectedIndex != -1) && (lbQuesList.SelectedIndex < lbQuesList.Items.Count - 1))
        {
            ListItem li = new ListItem();
            li.Text = lbQuesList.SelectedItem.Text;
            li.Value = lbQuesList.SelectedValue;

            // add a duplicate item down in the listbox
            lbQuesList.Items.Insert(lbQuesList.SelectedIndex + 2, li);
            // make it the current item
            lbQuesList.SelectedIndex = lbQuesList.SelectedIndex + 2;
            // delete the old occurrence of this item
            lbQuesList.Items.RemoveAt(lbQuesList.SelectedIndex - 2);
        }
    }


    protected void btnSaveReorder_Click(object sender, EventArgs e)
    {

        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains("SurveyId"))
                surveyId = int.Parse(typeHt["SurveyId"].ToString());

        }

        var questionService = ServiceFactory.GetService<QuestionService>();
        var surveyQuestionsList = questionService.GetAllSurveyQuestionsBySurveyId(surveyId);

        if (surveyQuestionsList.Any())
        {
            bool skipNo = true;
            foreach (var que in surveyQuestionsList)
            {
                if (que.SKIP_LOGIC == 1)
                    skipNo = false;
            }

            if (skipNo)
            {
                Hashtable QuesOrder = new Hashtable();
                for (int i = 0; i < lbQuesList.Items.Count; i++)
                {
                    QuesOrder.Add(i + 1, lbQuesList.Items[i].Value);
                }
                questionService.ReOrderQuestions(surveyId, QuesOrder);

                //string strSurveyDraftLink = EncryptHelper.EncryptQuerystring(PathHelper.GetCreateSurveyURL(), "SurveyId=" + surveyId);
                //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "load", "<script>window.parent.location.href='" + strSurveyDraftLink + "';</script>", true);
                base.CloseModal();

            }
            else
            {
                lblErrMsg.Text = Utilities.ResourceMessage("ReorderQuestions_Error_SkipLogic"); //"Reorder questions is not possible because Skip Logic is applied for survey";
                dvErrMsg.Visible = true;
            }

        }
    }
}