﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using App_Code;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;
using System.Data;
using Resources;
using Insighto.Business.Helpers;
using System.Collections;
using Insighto.Data;

public partial class new_login : System.Web.UI.Page
{
    SurveyCore surcore = new SurveyCore();
    int userId = 0;
    public string abovepanel;
    public string withinpanel;
    osm_user user;
    List<osm_user> listuser;
    int activationflag = -1;
    bool ispollcampaign = false;
    bool iseligiblefor60dayfreetrial = false;
    PollCreation Pc;
    bool freepolltrialfor60days = true;
    string polllicencetype = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        user = new osm_user();
        listuser = new List<osm_user>();
        lblInvaildAlert.Text = string.Empty;
        lblInvaildAlert.Visible = false;
        activationmessage.Visible = false;
        activationmessagediv.Visible = false;
        activationmessageheading.Style.Add("display", "none");
        UsersService userService = new UsersService();
        Pc = new PollCreation();
        if (Request.QueryString["key"] != null)
        {
            Hashtable ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 && ht.Contains("UserId"))
            {
                userId = ValidationHelper.GetInteger(ht["UserId"].ToString(), 0);
                listuser = userService.GetUserDetails(userId);
                activationflag = (listuser[0].ACTIVATION_FLAG ?? 0);
                user = userService.ActivateUser(userId);
                if (ht.Contains("IsPollCampaign"))
                {
                    if (ht["IsPollCampaign"].ToString() == "1")
                    {
                        ispollcampaign = true;
                    }
                }
                txtEmail.Text = user.LOGIN_NAME;
            }
        }

        //DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        //DateTime dt1 = new DateTime(2015, 8, 31);

        //DataSet dsactivity = surcore.getPollRespondentLimit(userId);
        //if (dsactivity.Tables.Count > 0)
        //{
        //    if (dsactivity.Tables[0].Rows.Count > 0)
        //    {
        //        if (dsactivity.Tables[0].Rows[0]["eligiblefor60dayfreepolltrial"].ToString() == "1")
        //        {
        //            iseligiblefor60dayfreetrial = true;
        //            if (dsactivity.Tables[0].Rows[0]["freepolltrialfor60days"].ToString() != "1")
        //            {
        //                freepolltrialfor60days = false;
        //            }
        //        }
        //        polllicencetype = dsactivity.Tables[0].Rows[0]["polllicencetype"].ToString();
        //    }
        //}
        //if (iseligiblefor60dayfreetrial == true && freepolltrialfor60days == false && ispollcampaign == true && (dt < dt1))
        //{
        //    //logo.Attributes.Add("src", "App_Themes/Classic/Images/insighto_logo_polls.png");
        //    //logolink.NavigateUrl = "/Polls/";
        //    abovepanel = "Create exciting Text – Image – Video Polls";
        //    withinpanel = "Sign in to My Polls";
        //    BTNSIGNIN.Text = "Start your 60 day free trial";
        //    //dvblwpanel.Visible = false;
        //    IsEmailCampaign.Value = "1";
        //}
        //else if (polllicencetype != "")
        //{
        //    //logo.Attributes.Add("src", "App_Themes/Classic/Images/insighto_logo_polls.png");
        //    //logolink.NavigateUrl = "/Polls/";
        //    abovepanel = "Create exciting Text – Image – Video Polls";
        //    withinpanel = "Sign in to My Polls";
        //    //dvblwpanel.Visible = false;
        //}
        //else //if (dsactivity.Tables[0].Rows[0]["surveylicence"].ToString() != "")
        //{
        //    abovepanel = "Create smarter, more professional surveys.";
        //    withinpanel = "Sign in to My Surveys";
        //    //dvblwpanel.Visible = true;
        //}
        if ((user.ACTIVATION_FLAG == 1) && (activationflag == 0))
        {
            activationmessage.Visible = true;
            activationmessageheading.Style.Add("display", "block");
            activationmessagediv.Visible = true;
        }
        //else if (iseligiblefor60dayfreetrial == true && freepolltrialfor60days == false && ispollcampaign == true && (dt < dt1))
        //{
        //    activationmessage.Visible = true;
        //    activationmessageheading.Style.Add("display", "block");
        //    activationmessage.Text = "Sign up before 31st August and get 60 days' free trial of Insighto Polls";
        //}

    }

    protected void BTNSIGNIN_Click(object sender, EventArgs e)
    {
        DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        DateTime dt1 = new DateTime(2015, 8, 31);


        if (!IsPostBack)
            return;



        if (Request.QueryString["key"] != null)
        {
            Hashtable ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 && ht.Contains("UserId"))
            {
                int userId = ValidationHelper.GetInteger(ht["UserId"].ToString(), 0);
                UsersService userService = new UsersService();
                var user = userService.FindUserByUserId(userId);
                userService.ProcessUserLogin(user);

                //Timezone is null for first user login. hence redirecting to welcome page
                if (user.ACTIVATION_FLAG == null || user.ACTIVATION_FLAG == 0)
                {
                    userService.ActivateUser(userId);
                    // Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=" + SurveyFlag));
                }
            }
            if (ht != null && ht.Count > 0 && ht.Contains("AlternateId"))
            {
                int Id = ValidationHelper.GetInteger(ht["AlternateId"].ToString(), 0);
                UsersService userService = new UsersService();
                var user = userService.ActivateAlternateUser(Id);
            }
        }


        string alrt = "Please enter";
        this.lblInvaildAlert.Visible = true;
        bool flag = false;
        if (string.IsNullOrEmpty(txtEmail.Text.Trim()) || txtEmail.Text == "Email")
        {
            alrt += " email address.";
            lblInvaildAlert.Text = alrt;
        }
        else
        {
            Regex reLenient = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            bool isEmail = reLenient.IsMatch(txtEmail.Text);
            if (!isEmail)
            {
                alrt += " valid email address.";
                lblInvaildAlert.Text = alrt;
            }
            else if (Convert.ToString(txtpassword1.Text).Length == 0)
            {
                alrt += " password.";
                lblInvaildAlert.Text = alrt;
            }
            flag = true;
        }

        if (flag && Convert.ToString(txtEmail.Text).Length > 0 && Convert.ToString(txtpassword1.Text).Length > 0)
        {
            // To check authenticated user       
            var userService = ServiceFactory.GetService<UsersService>();
            var user = userService.getAuthenticatedUser(txtEmail.Text, txtpassword1.Text);
            if (user == null)
            {
                lblInvaildAlert.Text = "Invalid Email Address or Password.";
                lblInvaildAlert.Visible = true;

                return;
            }
            else if (Utilities.ToEnum<SurveyStatus>(user.STATUS) == SurveyStatus.InActive || ValidationHelper.GetInteger(user.DELETED, 0) == 1)
            {
                lblInvaildAlert.Text = "Your Account is de-activated.";
                lblInvaildAlert.Visible = true;
                return;
            }
            else
            {
                userService.ProcessUserLogin(user);
                if (IsEmailCampaign.Value == "1")
                {
                    ispollcampaign = true;
                }
                DataSet dsactivity = surcore.getPollRespondentLimit(user.USERID);
                if (dsactivity.Tables[0].Rows[0]["eligiblefor60dayfreepolltrial"].ToString() == "1")
                {
                    iseligiblefor60dayfreetrial = true;
                }

                if (dsactivity.Tables[0].Rows[0]["freepolltrialfor60days"].ToString() != "1")
                {
                    freepolltrialfor60days = false;
                }
                if (iseligiblefor60dayfreetrial == true && freepolltrialfor60days == false && ispollcampaign == true && (dt < dt1))
                {
                    Pc.UpdatePollCampaignUser(user.USERID);
                    if (user.TIME_ZONE == null) //Timezone is null for first user login. hence redirecting to welcome page
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    }
                    else {
                        Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                        Response.Redirect(EncryptHelper.EncryptQuerystring("/Polls/MyPolls.aspx", "surveyFlag=" + user.SURVEY_FLAG));
                    }
                }

                if (user.TIME_ZONE == null) //Timezone is null for first user login. hence redirecting to welcome page
                {
                    if (dsactivity.Tables[3].Rows[0][0].ToString() == "survey")
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    }
                    else
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    }
                }
                else if (user.RESET == 1) // Password reset value is 1 for first login. hence redirectiong to chanepassword page
                {
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserChangePasswordPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                }
                else // on successful login user is redirected to Myaccounts page
                {
                    if (dsactivity.Tables[3].Rows[0][0].ToString() == "survey")
                    {
                        Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    }
                    else
                    {
                        Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                        Response.Redirect(EncryptHelper.EncryptQuerystring("/Polls/MyPolls.aspx", "surveyFlag=" + user.SURVEY_FLAG));
                    }

                }
            }

        }
        else
        {


        }



    }
}