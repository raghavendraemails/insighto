﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using System.Configuration;
using App_Code;


public partial class Logo : SurveyPageBase
{

    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int surveyId = 0;

    #endregion

    string strUpload = "upload";
    string strOriginal = "original";
    string strLarge = "large";
    string strMedium = "medium";
    string strSmall = "small";

    string strRight = "right";
    string strCenter = "center";
    string strLeft = "left";

    protected void Page_Load(object sender, EventArgs e)
    {
        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = false;
        dvinformationPanelDefault.InnerHtml = Utilities.ResourceMessage("divinformationPanelDefault"); 
        if (!IsPostBack)
        {
            if (SetFeatures("SURVEY_LOGO") == 0)
            {
                ibtnUpdalod.ValidationGroup = "";
                ibtnUpdalod.Attributes.Add("Onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                btnSave.ValidationGroup = "";
                btnSave.Attributes.Add("Onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                //div_logo.Visible = true;
                fuLogo.Enabled = false;
            }
            else
            {
                btnSave.ValidationGroup = "company";
                fuLogo.Enabled = true;
            }
            BindExistsData();

            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            //if (loggedInUserInfo.LicenseType == "FREE")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            //}
        }

    }

    private void BindExistsData()
    {

        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }

        if (surveyId > 0)
        {
            var surveySettingsService = ServiceFactory.GetService<SurveySetting>();
            var surveySettings = surveySettingsService.GetSurveySettings(surveyId);
            var userlogo = ServiceFactory<AccountSettingService>.Instance.GetUserSettingsBySurveyId(surveyId);
            if (userlogo.Length > 0 && surveySettings[0].SURVEY_LOGO.Length >0)
            {
                chkSetDefault.Checked = true;
            }
            if (surveySettings.Any())
            {
                txtCompanyName.Text = surveySettings[0].COMPANY_NAME;
                bool showLogo = ValidationHelper.GetBool(surveySettings[0].Show_Logo.ToString(), false);
                if (showLogo)
                {
                    chkShowLogo.Checked = true;
                }
                else
                {
                    chkShowLogo.Checked = false; 
                }
                if (surveySettings[0].SURVEY_LOGO != null)
                {
                    if (surveySettings[0].SURVEY_LOGO.ToString() != "")
                    {
                        if (SetFeatures("SURVEY_LOGO") == 1)
                        {
                            string usrlogo = "";
                            usrlogo = surveySettings[0].SURVEY_LOGO;
                            string[] str = usrlogo.Split('>');


                            if (str.Length > 0)
                            {
                                string[] strUpload = str[1].Split('_');
                                string url = str[0].Replace("<", "");
                                string align = str[2].Replace("<", "");

                                ViewState["url"] = url;

                                if (align.ToLower() == strRight.ToLower())
                                    rbtnalignRight.Checked = true;
                                else if (align.ToLower() == strCenter.ToLower())
                                    rbtnalignCenter.Checked = true;
                                else
                                    rbtnalignleft.Checked = true;

                                if (strUpload[2].ToLower() == strLarge.ToLower())
                                {
                                    rbtnSizeLarge.Checked = true;
                                    divLarge.Style.Add("display", "block");
                                    divMedium.Style.Add("display", "none");
                                    divSmall.Style.Add("display", "none");
                                    divOriginal.Style.Add("display", "none");
                                }
                                else if (strUpload[2].ToLower() == strOriginal.ToLower())
                                {
                                    rbtnSizeOriginal.Checked = true;
                                    divOriginal.Style.Add("display", "block");
                                    divLarge.Style.Add("display", "none");
                                    divMedium.Style.Add("display", "none");
                                    divSmall.Style.Add("display", "none");
                                }
                                else if (strUpload[2].ToLower() == strSmall.ToLower())
                                {
                                    rbtnSizeSmall.Checked = true;
                                    divSmall.Style.Add("display", "block");
                                    divLarge.Style.Add("display", "none");
                                    divMedium.Style.Add("display", "none");
                                    divOriginal.Style.Add("display", "none");
                                }
                                else
                                {
                                   
                                    rbtnSizeMedium.Checked = true;
                                    divMedium.Style.Add("display", "block");
                                    divLarge.Style.Add("display", "none");
                                    divSmall.Style.Add("display", "none");
                                    divOriginal.Style.Add("display", "none");
                                }

                                ViewState["Size"] = strUpload[2].ToString();

                                string fileOriginal = url.Replace(strUpload[2].ToLower(), strOriginal.ToLower());
                                string fileLarge = url.Replace(strUpload[2].ToLower(), strLarge.ToLower());
                                string fileMedium = url.Replace(strUpload[2].ToLower(), strMedium.ToLower());
                                string fileSmall = url.Replace(strUpload[2].ToLower(), strSmall.ToLower());

                                imgOriginal.ImageUrl = fileOriginal;
                                imgLarge.ImageUrl = fileLarge;
                                imgMedium.ImageUrl = fileMedium;
                                imgSmall.ImageUrl = fileSmall;
                                divLogoPreviews.Visible = true;
                            }

                        }
                        else 
                        {
                            BindDefaultImages();
                        }

                    }
                    else
                    {
                        BindDefaultImages();
                    }
                }
                else
                {
                    BindDefaultImages();
                }
            }
            else
            {
                BindDefaultImages();
            }

        }
    }

    protected void btnRemoveLogo_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }

        ServiceFactory<SurveySetting>.Instance.RemoveSurveyLogo(surveyId);
        BindExistsData();
        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = true;
        lblSuccMsg.Visible = true;
        lblSuccMsg.Text="Survey logo removed successfully.";
    }


    #region "Method : SetFeatures"
    /// <summary>
    /// This method returns bool value, if the feature flag is one.
    /// </summary>
    /// <param name="strFeature"></param>
    /// <returns></returns>

    public int SetFeatures(string strFeature)
    {
        int iVal = 0;
        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var sessdet = session.GetLoginUserDetailsSession();
        if (sessdet != null)
        {

            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));

            if (sessdet.UserId > 0 && listFeatures != null)
            {
                foreach (var listFeat in listFeatures)
                {

                    if (listFeat.Feature.Contains(strFeature))
                    {
                        iVal = (int)listFeat.Value;
                    }
                }
            }
        }

        else
        {
            Response.Redirect(PathHelper.GetUserLoginPageURL());
        }
        return iVal;
    }

    #endregion

    private void BindDefaultImages()
    {
        divLogoPreviews.Visible = false;
        rbtnSizeMedium.Checked = true;
        divMedium.Style.Add("display", "block");
        divLarge.Style.Add("display", "none");
        divSmall.Style.Add("display", "none");
        divOriginal.Style.Add("display", "none");
        imgOriginal.ImageUrl = "App_Themes/Classic/Images/insighto_logo_original.jpg";
        imgLarge.ImageUrl = "App_Themes/Classic/Images/insighto_logo_large.jpg";
        imgMedium.ImageUrl = "App_Themes/Classic/Images/insighto_logo_medium.jpg";
        imgSmall.ImageUrl = "App_Themes/Classic/Images/insighto_logo_small.jpg";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string strImageUrl = "";
        string aligning = "";
        string size = "";
        string presize = "";
        string height = "";
        string width = "";

        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }

        if (surveyId > 0)
        {
            if (SetFeatures("SURVEY_LOGO") == 1)
            {
                if (ViewState["url"] != null)
                {
                    if (ViewState["url"].ToString() != "")
                    {
                        strImageUrl = ViewState["url"].ToString();
                        presize = ViewState["Size"].ToString().ToLower();

                        if (rbtnSizeLarge.Checked == true)
                        {
                            size = strLarge.ToLower();
                            strImageUrl = strImageUrl.Replace(presize, strLarge.ToLower());
                            width = "300";
                            height = "100";
                        }
                        else if (rbtnSizeMedium.Checked == true)
                        {
                            size = strMedium.ToLower();
                            strImageUrl = strImageUrl.Replace(presize, strMedium.ToLower());
                            width = "200";
                            height = "80";
                        }
                        else if (rbtnSizeSmall.Checked == true)
                        {
                            size = strSmall.ToLower();
                            strImageUrl = strImageUrl.Replace(presize, strSmall.ToLower());
                            width = "150";
                            height = "50";
                        }
                        else
                        {
                            size = strOriginal.ToLower();
                            strImageUrl = strImageUrl.Replace(presize, strOriginal.ToLower());
                            width = "600";
                            height = "183";
                        }

                        if (rbtnalignleft.Checked == true)
                            aligning = strLeft.ToLower();
                        else if (rbtnalignCenter.Checked == true)
                            aligning = strCenter.ToLower();
                        else
                            aligning = strRight.ToLower();


                        strImageUrl = "<" + strImageUrl + ">" + "<" + width + "_" + height + "_" + size + "_" + width + "_" + height + "><" + aligning + ">";

                        osm_surveysetting osmSurveySetting = new osm_surveysetting();
                        osmSurveySetting.SURVEY_ID = surveyId;
                        osmSurveySetting.SURVEY_LOGO = strImageUrl;
                        osmSurveySetting.COMPANY_NAME = txtCompanyName.Text.Trim();
                        if (chkShowLogo.Checked)
                        {
                            osmSurveySetting.Show_Logo = true;
                        }
                        else 
                        {
                            osmSurveySetting.Show_Logo = false;
                        }

                        var surveySettingsService = ServiceFactory.GetService<SurveySetting>();
                        surveySettingsService.SaveOrUpdateSurveySetting(osmSurveySetting, "L");

                        var sessionService = ServiceFactory.GetService<SessionStateService>();
                        var userInfo = sessionService.GetLoginUserDetailsSession();

                        if (chkSetDefault.Checked)
                        {
                            if (userInfo.UserId > 0)
                            {
                                osm_accountsetting osmAccountSetting = new osm_accountsetting();
                                osmAccountSetting.USERID = userInfo.UserId;
                                osmAccountSetting.SURVEY_LOGO = strImageUrl;
                                osmAccountSetting.COMPANY_NAME = txtCompanyName.Text.Trim();

                                var accountSettingService = ServiceFactory.GetService<AccountSettingService>();
                                accountSettingService.SaveOrUpdateAccountSetting(osmAccountSetting, "L");
                            }
                        }

                        lblSuccMsg.Text = Utilities.ResourceMessage("lblSuccMsg");// "Logo saved successfully.";
                        dvSuccessMsg.Visible = true;

                        BindExistsData();
                    }
                    else
                    {
                        lblErrMsg.Text = Utilities.ResourceMessage("lblErrMsg");   // "Please upload image.";
                        dvErrMsg.Visible = true;
                    }
                }
                else
                {
                    lblErrMsg.Text = Utilities.ResourceMessage("lblErrMsg");   // "Please upload image.";
                    dvErrMsg.Visible = true;
                }
            }
        }
    }

    protected void ibtnUpdalod_Click(object sender, ImageClickEventArgs e)
    {
        int imgSize = ValidationHelper.GetInteger(ConfigurationManager.AppSettings["imgSize"], 0);
        string imgMsg = ConfigurationManager.AppSettings["imgMsg"];  
        if (fuLogo.HasFile)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            }
            if (fuLogo.FileContent.Length > imgSize)
            {
                dvErrMsg.Visible = true;
                lblErrMsg.Text = Utilities.ResourceMessage("lblErrMsgSize") + " " + imgMsg + ".";
                lblErrMsg.Visible = true;
                dvSuccessMsg.Visible = false;
                return;
            }

            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();

            string save = "";
            string strDirector = Server.MapPath(".");
            if (surveyId > 0)
            {
                if (!Directory.Exists(strDirector + "/" + "In/UserImages/" + userInfo.UserId + "/" + surveyId))
                {
                    Directory.CreateDirectory(strDirector + "/" + "In/UserImages/" + userInfo.UserId + "/" + surveyId);
                }

                save = "In/UserImages/" + userInfo.UserId + "/" + surveyId + "/";
            }
            else
            {
                if (!Directory.Exists(strDirector + "/" + "In/UserImages/" + userInfo.UserId))
                {
                    Directory.CreateDirectory(strDirector + "/" + "In/UserImages/" + userInfo.UserId);
                }
                save = "In/UserImages/" + userInfo.UserId + "/";
            }

            string file = "";
            //Original, Large, Medium, Small
            string fileOriginal = "";
            string fileLarge = "";
            string fileMedium = "";
            string fileSmall = "";

            string strImageUrl = "";
            string size = "";
            string presize = "";
            string height = "";
            string width = "";


            if (fuLogo.HasFile)
            {

                file = System.Guid.NewGuid().ToString() + "_" + strUpload.ToLower() + "_" + fuLogo.FileName;
                save += file;
                fuLogo.SaveAs(Server.MapPath(save));

                fileOriginal = save.Replace(strUpload.ToLower(), strOriginal.ToLower());
                fileLarge = save.Replace(strUpload.ToLower(), strLarge.ToLower());
                fileMedium = save.Replace(strUpload.ToLower(), strMedium.ToLower());
                fileSmall = save.Replace(strUpload.ToLower(), strSmall.ToLower());

                CommonMethods.ResizeImage(Server.MapPath(save), Server.MapPath(fileOriginal), 600, 183, true); //Original
                CommonMethods.ResizeImage(Server.MapPath(save), Server.MapPath(fileLarge), 300, 100, true); //Large
                CommonMethods.ResizeImage(Server.MapPath(save), Server.MapPath(fileMedium), 200, 80, true);  //Medium
                CommonMethods.ResizeImage(Server.MapPath(save), Server.MapPath(fileSmall), 150, 50, true);  //Small

                string aligning = "";
                if (rbtnalignleft.Checked == true)
                    aligning = strLeft.ToLower();
                else if (rbtnalignCenter.Checked == true)
                    aligning = strCenter.ToLower();
                else if (rbtnalignRight.Checked == true)
                    aligning = strRight.ToLower();


                if (rbtnSizeLarge.Checked == true)
                {
                    size = strLarge.ToLower();
                    strImageUrl = fileLarge;
                    width = "300";
                    height = "100";
                }
                else if (rbtnSizeMedium.Checked == true)
                {
                    size = strMedium.ToLower();
                    strImageUrl = fileMedium;
                    width = "200";
                    height = "80";
                }
                else if (rbtnSizeSmall.Checked == true)
                {
                    size = strSmall.ToLower();
                    strImageUrl = fileSmall;
                    width = "150";
                    height = "50";
                }
                else
                {
                    size = strOriginal.ToLower();
                    strImageUrl = fileOriginal;
                    width = "600";
                    height = "183";
                }

                strImageUrl = "<" + strImageUrl + ">" + "<" + width + "_" + height + "_" + size + "_" + width + "_" + height + "><" + aligning + ">";

                osm_surveysetting osmSurveySetting = new osm_surveysetting();
                osmSurveySetting.SURVEY_ID = surveyId;
                osmSurveySetting.SURVEY_LOGO = strImageUrl;
                osmSurveySetting.Show_Logo = chkShowLogo.Checked;   
                var surveySettingsService = ServiceFactory.GetService<SurveySetting>();
                surveySettingsService.SaveOrUpdateSurveySetting(osmSurveySetting, "U");

                if (chkSetDefault.Checked)
                {
                    if (base.SurveyBasicInfoView.USERID > 0)
                    {
                        osm_accountsetting osmAccountSetting = new osm_accountsetting();
                        osmAccountSetting.USERID = userInfo.UserId;
                        osmAccountSetting.SURVEY_LOGO = strImageUrl;
                        var accountSettingService = ServiceFactory.GetService<AccountSettingService>();
                        accountSettingService.SaveOrUpdateAccountSetting(osmAccountSetting, "U");
                    }
                }

                BindExistsData();
                lblSuccMsg.Visible = true;
                lblSuccMsg.Text = Utilities.ResourceMessage("lblSuccMsg_Sucess");   //"Image uploaded successfully. Please scroll down to view.";
                dvSuccessMsg.Visible = true;
            }
        }
    }




}