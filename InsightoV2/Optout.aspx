﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Optout.aspx.cs" Inherits="Optout"%>

<%@ Register TagPrefix="uc" TagName="Footer" Src="~/UserControls/Footer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Free Online Survey and Questionnaire Software for Customer Research and Feedback</title>
     <link runat="server" id="lnkFavIcon" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link href="App_Themes/Classic/layoutStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper" >
        <div id="header">
            <div class="insightoLogoPanel">
                <img src="App_Themes/Classic/Images/insighto_logo.png" border="0" alt="" /></div>
        </div>
        <div class="contentPanelHeader">
            <div class="pageTitle">
        <asp:Label ID="lblTitle" runat="server" Text="OPT OUT" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>
                </div>
        </div>
        <div class="clear">
        </div>
        <div class="contentPanel">
            <p class="defaultHeight">
            </p>
            <div class="text">
                <asp:Label ID="lblText" runat="server" meta:resourcekey="lblTextResource1"></asp:Label>
                <p class="defaultHeight">
                </p>
                <p class="defaultHeight">
                </p>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Button ID="btnOk" runat="server" Text="Unsubscribe" CssClass="dynamicButton"
                                ToolTip="Unsubscribe" OnClick="btnOk_Click" 
                                meta:resourcekey="btnOkResource1" />
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="dynamicButton"
                                ToolTip="Cancel" meta:resourcekey="btnCancelResource1" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <uc:Footer ID="Footer" runat="server" />
    </div>
    </form>
    <script type="text/javascript">
        function WindowClose() {
            //            window.open('', '_self', '');
            //            window.close();
            if (navigator.appName == "Microsoft Internet Explorer") {
                this.focus(); self.opener = this; self.close();
            }
            else {
                window.open('', '_self', ''); window.close();
            }
        }        
    </script>
</body>
</html>
