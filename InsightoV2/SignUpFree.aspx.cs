﻿#region Namespaces
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Services;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;
using DotNetOpenAuth.OpenId.RelyingParty;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Web;
using System.Data;

namespace Insighto.Pages.In
{
    public partial class In_SignUpFree : BasePage
    {
        #region Events
        #region Page_Load
     //   string countryName = "";
        string strsource;
        string strterm;
        SurveyCore surcore = new SurveyCore();
        protected void Page_Load(object sender, EventArgs e)
        {
            //countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
            //if (countryName == null)
            //{
            string ipAddress = "";
                if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
                }

               var countryName = Utilities.GetCountryName(ipAddress);
            //}
                ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(countryName);
           // ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();

            countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
            // countryName = "singapore";

            if (countryName.ToUpper() == "INDIA")
            {
                lblName.Text = "Name";
                divlastname.Visible = false;
                reqUsername.Visible = false;
                RexUsername.Visible = false;
            }
            else
            {
                lblName.Text = "First Name";
                lblUserName.Text = "Last Name";

            }

            if (!IsPostBack)
            {
                lnkFavIcon.Href = Utilities.GetAbsoluteUrl("~/insighto.ico", Page);
                lnktermsandcondtions.HRef = "javascript:void(window.open('"+EncryptHelper.EncryptQuerystring(PathHelper.GetTermsAndConditionsUrl().Replace("~",""),"Page=Terms_Conditions")+ "','_blank','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no'))";
                lnkPrivacyPolicy.HRef = "javascript:void(window.open('" + EncryptHelper.EncryptQuerystring(PathHelper.GetPrivacyPolicyUrl().Replace("~", ""), "Page=PrivacyPolicy") + "','blank','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no'))";
            }
            OpenIdRelyingParty rp = new OpenIdRelyingParty();
            var r = rp.GetResponse();
            if (r != null)
            {
                switch (r.Status)
                {
                    case AuthenticationStatus.Authenticated:
                        // NotLoggedIn.Visible = false;
                        Session["GoogleIdentifier"] = r.ClaimedIdentifier.ToString();
                        var fetchResponse = r.GetExtension<FetchResponse>();
                        Session["FetchResponse"] = fetchResponse;
                        Response.Redirect("~/MyProfile.aspx"); //redirect to main page of your website
                        break;
                    case AuthenticationStatus.Canceled:
                        // lblAlertMsg.Text = "Cancelled.";
                        break;
                    case AuthenticationStatus.Failed:
                        // lblAlertMsg.Text = "Login Failed.";
                        break;
                }
            }
        }
        #endregion

        #region btnCreateAccount_Click
        protected void btnCreateAccount_Click(object sender, EventArgs e)
        {
           
            if (Page.IsValid)
            {
                //if (txtCaptcha.Text == ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession())
                //{
                    var userService = new UsersService();
                    string ipAddress = string.Empty;
                    if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                    {
                        ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    else
                        ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //countryName = Utilities.GetCountryName(ipAddress);
                    //ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(countryName);
                    //ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
                   
                //countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
                   // countryName = "singapore";
                
               
                    var user = userService.RegisterFreeUser(txtName.Text.Trim(), txtUserName.Text.Trim(), txtEmailId.Text.Trim(), string.Empty,0, ipAddress, txtPassword.Text.Trim());
                   
                   // Response.Cookies["password"].Value = txtPassword.Text;

                    if (Request.Cookies["Partner"] != null)
                    {

                        HttpCookie aCookie = Request.Cookies["Partner"];

                        strsource = Server.HtmlEncode(aCookie.Values["utm_source"]);
                        strterm = Server.HtmlEncode(aCookie.Values["utm_term"]);

                        DataSet dsRefby = surcore.updateReferredby(user.USERID, strsource, strterm);
                    }

                    if (user == null)
                    {
                        lblErrorMsg.Text = Utilities.ResourceMessage("lblErrMsg");
                        dvErrMsg.Visible = true;
                        lblErrMsg.Visible = false;

                    }
                    else
                    {
                        ServiceFactory.GetService<SessionStateService>().SaveUserToSession(user);
                     //  Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserSignUpFreeNextURL(),"userId="+user.USERID));

                      //  Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserPricingpageintermediate(), "UserId=" + user.USERID));
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.Getsocialnetwork(), "surveyFlag=0"));
                      //  Response.Redirect(EncryptHelper.EncryptQuerystring("~/pricingpage_intermediate1.aspx", "surveyFlag=0"));
                    }
                //}
                //else
                //{
                //    txtCaptcha.Focus();
                //    lblErrMsg.Text = Utilities.ResourceMessage("lblCaptchaErrMsg");
                //    lblErrMsg.Visible = true;
                //    dvErrMsg.Visible = false;
                //}
            }

        }
        #endregion


        #region imgbtnFBLogin_Click
        protected void imgbtnFBLogin_Click(object sender, EventArgs e)
        {
            oAuthFacebook oFB = new oAuthFacebook();
            Response.Redirect(oFB.AuthorizationLinkGet());
        }
        #endregion

        #region imgbtnGoodleLogin_Click
        protected void imgbtnGoodleLogin_Click(object src, CommandEventArgs e)
        {
            string discoveryUri = e.CommandArgument.ToString();
            OpenIdRelyingParty openid = new OpenIdRelyingParty();
            var b = new UriBuilder(Request.Url) { Query = "" };
            var req = openid.CreateRequest(discoveryUri, b.Uri, b.Uri);
            var fetchRequest = new FetchRequest();
            fetchRequest.Attributes.AddRequired(WellKnownAttributes.Contact.Email);
            fetchRequest.Attributes.AddRequired(WellKnownAttributes.Name.First);
            fetchRequest.Attributes.AddRequired(WellKnownAttributes.Name.Last);
            fetchRequest.Attributes.AddRequired(WellKnownAttributes.Contact.HomeAddress.Country);
            req.AddExtension(fetchRequest);
            req.RedirectToProvider();

        }
        #endregion

        public string GetRandomPasswordUsingGUID(int length)
        {
            // Get the GUID
            string guidResult = System.Guid.NewGuid().ToString();
            // Remove the hyphens
            guidResult = guidResult.Replace("-", string.Empty);
            // Make sure length is valid
            if (length <= 0 || length > guidResult.Length)
                throw new ArgumentException("Length must be between 1 and " + guidResult.Length);
            // Return the first length bytes        
            return guidResult.Substring(0, length);
        }
        #endregion
#endregion
        
}
}