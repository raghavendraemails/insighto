﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Configuration;
using CovalenseUtilities.Helpers;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using App_Code;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;

public partial class InsightoPollsPromoEmail : System.Web.UI.Page
{
    private MailMessage mailMessage = null;
    private SmtpClient smtpMail = null;
    string strUrl = ConfigurationManager.AppSettings["RootURL"].ToString();
    DataTable dt;
    PollCreation pc;
    SurveyCore surveycore;
    string username = "";
    string useremail = "";
    int userid = 0;
    string pollpromoURL = "";
    string rootURL = ConfigurationManager.AppSettings["RootURL"].ToString();
    string OpturlParams = "";

    string unsubscribe = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        Server.ScriptTimeout = 1500;
        pc = new PollCreation();
        surveycore = new SurveyCore();
        GetExistingUsers();
    }

    public void GetExistingUsers() {
        try
        {
            dt = surveycore.getUsersForAWSEmail();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    try
                    {
                        userid = (int)dt.Rows[i]["USERID"];
                        useremail = dt.Rows[i]["LOGIN_NAME"].ToString();
                        username = dt.Rows[i]["FIRST_NAME"].ToString() + " " + dt.Rows[i]["LAST_NAME"].ToString();

                        unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + rootURL + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";
                        sendemail("MoveToAWS", username, useremail, pollpromoURL, unsubscribe);
                        SqlConnection con = new SqlConnection();
                        string sqlquery = "UPDATE osm_user SET AWSEmailSent = 1 WHERE USERID = " + userid.ToString();
                        con = strconnectionstring();
                        SqlCommand scom = new SqlCommand(sqlquery, con);
                        scom.CommandType = CommandType.Text;
                        scom.ExecuteNonQuery();
                        scom.Dispose();
                        con.Close();
                        con.Dispose();
                    }
                    catch (Exception e)
                    {
                        lblerror2.Text = e.ToString();
                    }
                }
            }
        }
        catch (Exception)
        {
        }
    }

    public void sendemail(string templatename, string username, string EmailID, string loginurl, string unsubscribeurl)
    {

        string viewData1;
        JavaScriptSerializer js1 = new JavaScriptSerializer();
        js1.MaxJsonLength = int.MaxValue;
        clsjsonmandrill prmjson1 = new clsjsonmandrill();
        //  var prmjson1 = new jsonmandrill();
        prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
        prmjson1.name = templatename;
        viewData1 = js1.Serialize(prmjson1);

        string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";


        WebClient request1 = new WebClient();
        request1.Encoding = System.Text.Encoding.UTF8;
        request1.Headers["Content-Type"] = "application/json";
        byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));

        string response1 = System.Text.Encoding.ASCII.GetString(resp1);


        clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);

        string bodycontent = jsmandrill11.code;


        string viewData;
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = int.MaxValue;
        var prmjson = new clsjsonmandrill.jsonmandrillmerge();
        prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
        prmjson.template_name = templatename;



        List<clsjsonmandrill.template_content> mtags = new List<clsjsonmandrill.template_content>();
        mtags.Add(new clsjsonmandrill.template_content { name = "std_content00", content = "<div> Dear *|FNAME|*,</div>" });
        //mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div><ul><li>&nbsp;User Name: *|Login_Name|*</li><li>&nbsp;Temporary Password:  *|Password|*</li></ul></div>" });
        //mtags.Add(new clsjsonmandrill.template_content { name = "std_content02", content = "<div><p style='font-size:8pt;font-family:Arial,Sans-Serif'> *|UNSUBSCRIBEINSIGHTO|* </p></div>" });

        List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
        mvars.Add(new clsjsonmandrill.merge_vars { name = "FNAME", content = username });
        //mvars.Add(new clsjsonmandrill.merge_vars { name = "INSIGHTOPOLLLINK", content = loginurl });
        //mvars.Add(new clsjsonmandrill.merge_vars { name = "UNSUBSCRIBEINSIGHTO", content = unsubscribeurl });
        prmjson.template_content = mtags;
        prmjson.merge_vars = mvars;


        viewData = js.Serialize(prmjson);


        string url = "https://mandrillapp.com/api/1.0/templates/render.json";


        WebClient request = new WebClient();
        request.Encoding = System.Text.Encoding.UTF8;
        request.Headers["Content-Type"] = "application/json";
        byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

        string response = System.Text.Encoding.ASCII.GetString(resp);

        string unesc = System.Text.RegularExpressions.Regex.Unescape(response);

        string first4 = unesc.Substring(0, 9);

        string last2 = unesc.Substring(unesc.Length - 2, 2);


        string unesc1 = unesc.Replace(first4, "");
        string unescf = unesc1.Replace(last2, "");


        clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);

        mailMessage = new MailMessage();
        mailMessage.To.Clear();
        mailMessage.Sender = new MailAddress("alerts@insighto.com", "Insighto.com");
        mailMessage.From = new MailAddress("alerts@insighto.com", "Insighto.com");

        mailMessage.Subject = "AWS - the new home of Insighto.com ";

        mailMessage.To.Add(EmailID);
        mailMessage.Body = unescf;
        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;
        smtpMail = new SmtpClient();
        smtpMail.Host = "smtp.mandrillapp.com";
        smtpMail.Port = 587;
        smtpMail.EnableSsl = true;
        smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
        smtpMail.Credentials = new System.Net.NetworkCredential("ram@knowience.com", "T74TJc3EZPYaIcvdBg--Dg");

        try
        {

            smtpMail.Send(mailMessage);
        }
        catch (SmtpFailedRecipientsException ex)
        {
            lblerror2.Text = ex.ToString();
            for (int i = 0; i < ex.InnerExceptions.Length; i++)
            {
                SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                if (status == SmtpStatusCode.MailboxBusy ||
                    status == SmtpStatusCode.MailboxUnavailable)
                {
                    //  Console.WriteLine("Delivery failed - retrying in 5 seconds.");


                    System.Threading.Thread.Sleep(5000);
                    smtpMail.Send(mailMessage);
                }
                else
                {
                    throw ex;
                }
            }
        }
        //catch (System.Net.Mail.SmtpException ex)
        //{
        //    throw ex;
        //}
        mailMessage.Dispose();
    }

    public SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
}