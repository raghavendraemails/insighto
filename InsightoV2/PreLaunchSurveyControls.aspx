﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PreLaunch.master" AutoEventWireup="true"
    CodeFile="PreLaunchSurveyControls.aspx.cs" Inherits="PreLaunchSurveyControls"%>

<asp:Content ID="Content1" ContentPlaceHolderID="PreLaunchMainContent" runat="Server">
    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccMsg" runat="server" 
                meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
    </div>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
    </div>
    <div class="surveyQuestionHeader">
        <div class="surveyQuestionTitle">            
    <asp:Label ID="lblTitle" runat="server" Text="View Survey Controls" 
                meta:resourcekey="lblTitleResource1"></asp:Label></div>
    </div>
    <div class="surveyQuestionTabPanel">
        <div class="surveyQuestionHeader">
            <table width="100%">
                <tr>
                    <td class="wid_70">
                        <div class="surveyQuestionTitle">                           
                        <asp:Label ID="lblSurveyScheduler" runat="server"
                                meta:resourcekey="lblSurveySchedulerResource1"></asp:Label> </div>
                    </td>
                    <td class="wid_30">
                        <asp:Button ID="btnEdit" runat="server" Text=" "  CssClass="icon-pen-active"
                            OnClick="btnEdit_Click" meta:resourcekey="btnEditResource1" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="surveyQuestionTabPanel">
            <!-- survey question tabs -->
            <h4>
                <asp:Label ID="lblLaunchStart" runat="server" 
                   meta:resourcekey="lblLaunchStartResource1"></asp:Label><span></span></h4>
            <div>                
                <p>
                                    <asp:RadioButtonList ID="rbtnLaunchSchedule" ValidationGroup="a" runat="server" CssClass="rbtnLaunchSchedule"
                        meta:resourcekey="rbtnLaunchScheduleResource1" Enabled="False">
                        <asp:ListItem Value="0" Text="On Submit" meta:resourcekey="ListItemResource1"></asp:ListItem>
                        <asp:ListItem Value="1" Text="On Specific Date" meta:resourcekey="ListItemResource2"></asp:ListItem>
                    </asp:RadioButtonList>
                </p>
                <div id="launchDateTime" class="launchDateTime" style="display: none;" runat="server">
                    <table>
                        <tr>
                            <td width="150">
                                <table border="0" cellpadding="1" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblDate" runat="server" AssociatedControlID="txtDateTime" meta:resourcekey="lblDateResource1" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDateTime" runat="server" CssClass="textBoxDateTime txtDateTime"
                                                meta:resourcekey="txtDateTimeResource1" Enabled="False" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <p>
                    <br />
                    <asp:CheckBox ID="chkAlertOnSurveyLaunch" runat="server" Enabled="False" 
                        Text="Send an email alert on survey launch" 
                        meta:resourcekey="chkAlertOnSurveyLaunchResource1"/>
                </p>
                <p>
                    &nbsp;</p>
                <h4>
                    
                <asp:Label ID="lblCloseSurvey" runat="server" 
                        Text="When would you like to close the survey?" 
                        meta:resourcekey="lblCloseSurveyResource1"></asp:Label><span></span></h4>
                <p>
                    <asp:RadioButtonList ID="rbtnSurveyClose" runat="server" ValidationGroup="a"
                        CssClass="rbtnSurveyClose" Enabled="False" 
                        meta:resourcekey="rbtnSurveyCloseResource1">
                        <asp:ListItem Text="I will close the survey myself" Value="2" 
                            meta:resourcekey="ListItemResource3"></asp:ListItem>
                        <asp:ListItem Text="On specific date" Value="1" 
                            meta:resourcekey="ListItemResource4"></asp:ListItem>
                    </asp:RadioButtonList>
                </p>
                <div id="surveyDateTime" class="surveyCloseDateTime" style="display: none;" runat="server">
                    <table border="0" cellpadding="1" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Label ID="lblSurveyClosingDate" runat="server" 
                                    AssociatedControlID="txtClosingDateTime" 
                                    meta:resourcekey="lblSurveyClosingDateResource1" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtClosingDateTime" runat="server" CssClass="textBoxDateTime txtDateTime closeDate"
                                    Enabled="False" meta:resourcekey="txtClosingDateTimeResource1" />
                            </td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbtnNoofRespondents" runat="server" CssClass="rbtnNoofRespondents"
                                Enabled="False" meta:resourcekey="rbtnNoofRespondentsResource1" />
                        </td>
                        <td>
                            <asp:Label ID="lblOnReaching" runat="server" Text="On reaching" 
                                meta:resourcekey="lblOnReachingResource1"></asp:Label>
                            
                        </td>
                        <td>
                            <asp:TextBox ID="txtNoofRespondents" runat="server" Text="0" CssClass="textBoxTextCount"
                                Enabled="False" meta:resourcekey="txtNoofRespondentsResource1"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblResponseOnReach" runat="server" Text="Responses" 
                                meta:resourcekey="lblResponseOnReachResource1"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <p>
                <br />
                <asp:CheckBox ID="chkAlertOnSurveyClose" runat="server" Enabled="False" 
                    meta:resourcekey="chkAlertOnSurveyCloseResource1" Text="Send an email alert on survey close" />
            </p>
            <!-- //survey question tabs -->
        </div>
        <div class="clear">
        </div>
        <!-- //// SURVEY SCHEDULER ////// -->
        <!-- /// ALERT SCHEDULER /// -->
        <div class="surveyQuestionHeader">
            <table width="100%">
                <tr>
                    <td class="wid_70">
                        <div class="surveyQuestionTitle">
                         <asp:Label ID="lblAlertScheduler" runat="server" Text=" ALERT SCHEDULER" 
                                meta:resourcekey="lblAlertSchedulerResource1"></asp:Label>  </div>
                    </td>
                    
                    <td class="wid_30">
                        <asp:Button ID="btnEdit1" runat="server" Text=" " ToolTip="Edit" CssClass="icon-pen-active"
                            OnClick="btnEdit_Click" meta:resourcekey="btnEdit1Resource1" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="surveyQuestionTabPanel">
            <!-- survey question tabs -->
            <h4>                
                <asp:Label ID="lblMailOnReaching" runat="server" 
                    Text="Displays alert in survey manager page." 
                    meta:resourcekey="lblMailOnReachingResource1"></asp:Label></h4>
            <div>
                <table border="0" width="70%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="25%" align="right" style="padding-right:10px;">
                        <asp:Label ID="lblResponse1" runat="server" Text="Minimum Responses" 
                                meta:resourcekey="lblResponse1Resource1"></asp:Label>
                        </td>
                        <td width="15%">
                        <asp:TextBox ID="txtAlert1" runat="server" CssClass="textBoxTextCount" 
                                Enabled="False" meta:resourcekey="txtAlert1Resource1"></asp:TextBox>
                        </td>
                        <td width="20%" align="right" style="padding-right:10px;">
                        <asp:Label ID="lblAlertDate1" runat="server" Text="	on Specific Date" 
                                meta:resourcekey="lblAlertDate1Resource1"></asp:Label>
                            <asp:Label ID="lblDate1" runat="server" AssociatedControlID="txtDate1" 
                                meta:resourcekey="lblDate1Resource1" />
                        </td>
                        <td width="40%">
                        <asp:TextBox ID="txtDate1" runat="server" CssClass="textBoxDateTime txtDateTime"
                                Enabled="False" meta:resourcekey="txtDate1Resource1" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="padding-right:10px;">
                             <asp:Label ID="lblResponse2" runat="server" Text="Maximum Responses" 
                                meta:resourcekey="lblResponse2Resource1"></asp:Label>
                        </td>
                        <td>
                        <asp:TextBox ID="txtAlert2" runat="server" CssClass="textBoxTextCount" 
                                Enabled="False" meta:resourcekey="txtAlert2Resource1"></asp:TextBox>
                        </td>
                        
                        <td align="right" style="padding-right:10px;">
                           <asp:Label ID="lblAlertDate2" runat="server" Text="	on Specific Date" 
                                meta:resourcekey="lblAlertDate2Resource1"></asp:Label>
                            <asp:Label ID="lblDate2" runat="server" AssociatedControlID="txtDate2" 
                                meta:resourcekey="lblDate2Resource1" />
                        </td>
                        <td>
                        <asp:TextBox ID="txtDate2" runat="server" CssClass="textBoxDateTime txtDateTime"
                                Enabled="False" meta:resourcekey="txtDate2Resource1" />
                        </td>                        
                    </tr>
                   
                </table>
            </div>
            <%--<h4>
                <asp:Label ID="lblMailOnNoOfResponses" runat="server" 
                    Text="Send me an email alert on number of responses on the following dates" 
                    meta:resourcekey="lblMailOnNoOfResponsesResource1"></asp:Label></h4>
            --%>
           
            <!-- //survey question tabs -->
        </div>
        <div class="clear"></div>


        <div class="surveyQuestionHeader">
            <table width="100%" border="0">
                <tr>
                    <td class="wid_70">
                        <div class="surveyQuestionTitle" style="width:90%">
                            <asp:Label ID="lblSelfReminder" runat="server" 
                                Text="SELF REMINDER - ON ANY ASPECTS OF THE SURVEY" 
                                meta:resourcekey="lblSelfReminderResource1"></asp:Label></div>
                    </td>
                    <td class="wid_30">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div class="surveyQuestionTabPanel">
            <table border="0" width="43%">
                <tr>
                    <td >
                            <asp:Label ID="lblReminder" runat="server" Text="Reminder" 
                                meta:resourcekey="lblReminderResource1"></asp:Label>
                        </td>
                    
                        <td >
                            <asp:Label ID="lblReminderDate" runat="server" Text="Remainder Date" 
                                meta:resourcekey="lblReminderDateResource1"></asp:Label>
                            <asp:Label ID="lblDate3" runat="server" AssociatedControlID="txtRemaiderDate" 
                                meta:resourcekey="lblDate3Resource1" />
                        </td>
                </tr>
                <tr>
                    <td>
                            <asp:TextBox ID="txtRemainder" runat="server" CssClass="textArea-200-40" TextMode="MultiLine"
                                Enabled="False" meta:resourcekey="txtRemainderResource1"></asp:TextBox>
                        </td>
                         <td valign="top">
                            <asp:TextBox ID="txtRemaiderDate" runat="server" CssClass="textBoxDate txtRemaiderDate"
                                Enabled="False" meta:resourcekey="txtRemaiderDateResource1" />
                        </td>
                </tr>
            </table>
        </div>


        <div class="clear"></div>
        <div class="prelaunchbtn"><asp:Button ID="btnBack" runat="server" 
                Text="Back to Launch Page" ToolTip="Back to Launch Page"
                                CssClass="prelaunchbackbtn" OnClick="btnBack_Click" 
                meta:resourcekey="btnBackResource1" /></div>  
        <div class="clear"></div>
        
        <!-- /// ALERT SCHEDULER /// -->
    </div>
</asp:Content>
