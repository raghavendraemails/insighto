﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using Resources;
using System.Configuration;
using App_Code;

public partial class Admin_AddRole : BasePage
{
    Hashtable ht = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
                if (ht != null && ht.Count > 0 && ht.Contains("RoleId"))
                {
                    int adminUserId = ValidationHelper.GetInteger(ht["RoleId"].ToString(), 0);
                    txtRoleName.Text = ht["RoleName"].ToString();  
                   
                }
                lblCreate.Visible = false;
                lblEdit.Visible = true;
                btnCancel.Visible = true;
                btnSave.Text = Utilities.ResourceMessage("btnUpdateResource1.Text"); //"Update";
            }
            else
            {
                lblCreate.Visible = true;
                lblEdit.Visible = false;
                btnCancel.Visible = true;
                btnSave.Text = Utilities.ResourceMessage("btnSaveResource1.Text"); //"Save";           
            }
        }
    }

    
    protected void btnSave_Click(object sender, EventArgs e)
    {
        int roleId = 0;
        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            if (ht != null && ht.Count > 0 && ht.Contains("RoleId"))
            {
                roleId = ValidationHelper.GetInteger(ht["RoleId"].ToString(), 0);
            }
        }
        var roleService = ServiceFactory.GetService<RoleService>();
        var role = new osm_roles();
        if (roleId > 0)
        {
            role.ROLEID = roleId; 
            role.ROLE_NAME = txtRoleName.Text.Trim();
            role.MODIFIED_ON = DateTime.UtcNow;
            role.DELETED = 0;
        }
        else
        {
            role.ROLE_NAME = txtRoleName.Text.Trim();
            role.CREATED_ON = DateTime.UtcNow;   
            role.MODIFIED_ON = DateTime.UtcNow;
            role.DELETED = 0;
        }
        var resultrole = roleService.SaveRole(role);

        if (resultrole == null)
        {
            lblErrMsg.Text = Utilities.ResourceMessage("AddRole_Error_Save"); //"Role Name already exist.";
            dvErrMsg.Visible = true;
        }
        else
        {
            dvSuccessMsg.Visible = true;
        }
        base.CloseModelForSpecificTime("ManageRole.aspx", ConfigurationManager.AppSettings["TimeOut"]);  

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {       
        base.CloseModelForSpecificTime("ManageRole.aspx", ConfigurationManager.AppSettings["TimeOut"]);
    }
}