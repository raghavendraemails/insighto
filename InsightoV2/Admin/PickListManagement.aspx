﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="PickListManagement.aspx.cs" Inherits="Insighto.Pages.Admin.PickListManagement" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <span>
                <asp:Label ID="lblChangeQeustionMode" runat="server" 
                Text="Pick List Management" meta:resourcekey="lblChangeQeustionModeResource1"></asp:Label></span>
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="surveyCreateTextPanel ">
            <!-- survey created content panel -->
            <div class="cmsPagesMenu">
                <h2><asp:Label ID="lblTitle" runat="server" Text="Category" 
                        meta:resourcekey="lblTitleResource1"></asp:Label>
                    </h2>
                
                    <div class="defaultHeight"> </div>
                <div class='suckerdiv'>
                    <div class="treeMenuLeftPanel">
                        <!-- survey left menu -->
                        <asp:TreeView ID="tvCategory" runat="server" NodeWrap="True" 
                            OnSelectedNodeChanged="tvCategory_SelectedNodeChanged" 
                            meta:resourcekey="tvCategoryResource1">
                            <%-- onselectednodechanged="tvSurvey_SelectedNodeChanged"--%>
                            <RootNodeStyle ChildNodesPadding="5px" />
                            <LeafNodeStyle CssClass="adminleafNode" />
                        </asp:TreeView>
                        <!-- //survey left menu -->
                    </div>
                </div>
            </div>
            <div class="surveyQuestionPanel">
                <!-- survey question panel -->
                <div class="pickListCategoryTop">
                    <div class="pickListTitel">
                        <asp:Label ID="lblCategoryName" runat="server" Text="Category Name :" 
                            meta:resourcekey="lblCategoryNameResource1"></asp:Label>
                        <asp:Label ID="lblCategory" runat="server" class="questionHeaderName"
                            meta:resourcekey="lblCategoryResource1"></asp:Label>
                    </div>
                    <div class="pickListAddBtnPanel">
                        <asp:HyperLink ID="hlnkAddList" CssClass="dynamicHyperLink" rel="framebox" 
                            h="250" w="600"
                            scrolling='yes' title='Copy' onclick='javascript: return ApplyFrameBox(this);'
                            ToolTip="Add List" runat="server" Text="Add" 
                            meta:resourcekey="hlnkAddListResource1"></asp:HyperLink>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div>
                    <!-- text here -->
                    <div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="surveysGridPanel">
                        <div>
                            <div id="ptoolbar">
                            </div>
                            <table id="tblAddressBook">
                            </table>
                            <asp:HiddenField ID="hdnSurveyId" runat="server" />
                            <asp:HiddenField ID="hdnCategory" runat="server" />
                        </div>
                    </div>
                    <!-- //text here -->
                </div>
                <!-- //survey question panel -->
            </div>
            <div class="clear">
            </div>
            <!-- //survey created content panel -->
        </div>
        <div class="clear">
        </div>
        <div id="modal" style="border: 1px solid #ccc; width: 400px; height: 180px; background-color: #fff;
            display: none;">
            <table cellpadding="0" cellspacing="0" width="94%">
                <tr style="background: #F8F8F8; height: 40px">
                    <td>
                        <div class="popupTitle">
                            <h3>
                                <asp:Label ID="lblCopyFromTemplate" runat="server" Text=" Copy From Template" 
                                    meta:resourcekey="lblCopyFromTemplateResource1"></asp:Label>
                            </h3>
                        </div>
                    </td>
                    <td valign="top">
                       <%-- <div class="popupClosePanel">
                            <a href="#" class="popupCloseLink" title="Close" onclick="Popup.hide('modal')"></a>
                        </div>--%>
                    </td>
                </tr>
                <tr>
                    <td class="defaultHeight">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="lt_padding">
                            <asp:Label ID="lblInfo" runat="server" 
                                Text="  Copying this template will enable you copy only those questions that are entitled
                            for Free membership.
                            <br /> The number of questions that could be copied would also be limited as per this entitlement." 
                                meta:resourcekey="lblInfoResource1"></asp:Label>
                          </div>
                    </td>
                </tr>
                <tr>
                    <td class="defaultHeight">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="lt_padding">
                        <table align="right">
                            <tr>
                                <td>
                                    <input id="btnOk" type="button" value="Ok" class="dynamicButton" onclick="Popup.hide('modal');" />
                                    <asp:HiddenField ID="hfSkipUrl" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <!-- //content panel -->
    </div>
    <script type="text/javascript">

        var serviceUrl = '../AjaxService.aspx';
        var categoryName = '<%= Category %>';



        $(document).ready(function () {
            $("#tblAddressBook").jqGrid({
                url: serviceUrl,
                postData: { method: "GetPickListByCategory", category: categoryName },
                datatype: 'json',
                colNames: ['Item Name', 'Edit'],
                colModel: [
                { name: 'PARAMETER', index: 'PARAMETER', width: 85, align: 'left' },
                { name: 'EditLinkUrl', index: 'EditLinkUrl', width: 15, align: 'center', formatter: EditLinkFormatter, sortable: false },
                 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'PARAMETER',
                sortorder: "asc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 630,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblAddressBook").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });



        });



        function EditLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='" + cellvalue + "' rel='framebox' w='600' h='250' ><img src='../App_Themes/Classic/Images/icon-pen-active.gif' title='Edit'/></a>";
        }   

       
       

        
    </script>
</asp:Content>
