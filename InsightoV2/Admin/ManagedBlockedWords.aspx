﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="ManagedBlockedWords.aspx.cs" Inherits="Admin_ManagedBlockedWords" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
     <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text=" Manage Blocked Words" 
                meta:resourcekey="lblTitleResource1"></asp:Label>            
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    
    <div class="contentPanel">
        <div class="surveyCreateTextPanel">
            <div class="adminUserGridViewPanel" style="display: block;">
                <div class="pickListCategoryTop">
                    <div class="pickListTitel">
                      <!--Manage Blocked Words-->
                    </div>
                    <div class="pickListAddBtnPanel">
                        
                        <asp:HyperLink ID="hlnkViewList" CssClass="dynamicHyperLink" NavigateUrl="ViewBlockedWords.aspx"
                        ToolTip="View Blocked Words" runat="server" Text="View Blocked Words" meta:resourcekey="hlnkViewListResource1" 
                        ></asp:HyperLink>&nbsp;
                        <asp:HyperLink ID="hlnkAddUser" CssClass="dynamicHyperLink"   
                        rel="framebox" h="320" w="600"
                            scrolling='yes' title='Copy' 
                        onclick='javascript: return ApplyFrameBox(this);' NavigateUrl="AddBlockedWord.aspx"
                            ToolTip=" Add New" runat="server" Text=" Add New" meta:resourcekey="hlnkAddUserResource1" 
                           ></asp:HyperLink>             

                       
                    </div>
                    <div class="clear">
                    </div>
                </div>
              <div class="successPanel" id="dvSuccessMsg" style="display:none">
                 <div>
                    <asp:Label ID="lblSuccessMsg" runat="server" meta:resourcekey="lblSuccessMsg" Text="Blocked word deleted successfully."></asp:Label>
                 </div>
              </div>
                <div class="defaultH">
                </div>
                <div id="ptoolbar">
                </div>
                <table id="tblManagedBlockedWords">
                </table>
                <div class="defaultH">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var serviceUrl = '../AjaxService.aspx';
        $(document).ready(function () {
            $("#tblManagedBlockedWords").jqGrid({
                url: serviceUrl,
                postData: { method: "GetAllBlockedWords" },
                datatype: 'json',
                colNames: ['Block Word', 'Description', '', ''],
                colModel: [
                { name: 'BLOCKEDWORD_NAME', index: 'BLOCKEDWORD_NAME', width: 75, align: 'left', resizable: false },
                { name: 'BLOCKEDWORD_DESCRIPTION', index: 'BLOCKEDWORD_DESCRIPTION', align: 'left', resizable: false },
                { name: 'EditUrl', index: 'EditUrl', width: 10, resizable: false, align: 'center', formatter: EditLinkFormatter, sortable: false },
                { name: 'BLOCKEDWORD_ID', index: 'BLOCKEDWORD_ID', width: 10, align: 'center', resizable: false, formatter: DeleteLinkFormatter, sortable: false },

                 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'BLOCKEDWORD_NAME',
                sortorder: "asc",

                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 875,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblManagedBlockedWords").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
            $("#tblManagedBlockedWords").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');
                   });

        function EditLinkFormatter(cellvalue, options, rowObject) {

            return "<a href='" + cellvalue + "' rel='framebox' w='600' h='300' ><img src='../App_Themes/Classic/Images/icon-pen-active.gif' title='Edit'/></a>";
        }
        function DeleteLinkFormatter(cellvalue, options, rowObject) {

            return "<a href='#' class='button-small-gap' id='lnkDelete' onclick='javascript: return DeleteBlockedWord(" + cellvalue + ");'><img src='../App_Themes/Classic/Images/icon-delete-active.gif' title='Delete' /></a>";
        }

        function gridReload() {

            $("#tblManagedBlockedWords").jqGrid('setGridParam', { url: serviceUrl, page: 1 }).trigger("reloadGrid");
        }


        function DeleteBlockedWord(BlockedWordId) {
            var answer = confirm('Are you sure! you want to delete?');
             if (answer) {
                 $.post("../AjaxService.aspx", { "method": "DeleteBlockedWordById", "BlockedWordId": BlockedWordId },
                  function (result) {
                      if (result == '1') {
                          $("#dvSuccessMsg").show();
                          gridReload();
                      }

                  });
            }
        }
    </script>
</asp:Content>
