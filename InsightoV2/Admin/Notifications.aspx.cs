﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Data;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using App_Code;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using Insighto.Exceptions;
using CovalenseUtilities.Helpers;

public partial class Admin_Notifications : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtDateTime.Attributes.Add("Readonly", "true");
            txtClosingDateTime.Attributes.Add("Readonly", "true");
            var configService = ServiceFactory.GetService<ConfigService>();
            List<string> configKey = new List<string>();
            configKey.Add("SERVER_MAINTAINANCE");
            var configDet = configService.GetConfigurationValues(configKey);
            txtMessage.Text =  configDet[0].CONFIG_VALUE;
            if (configDet[0].FromDate != null)
            {
                  DateTime fromDate = Convert.ToDateTime(configDet[0].FromDate);
                  txtDateTime.Text = fromDate.ToString("dd-MMM-yyyy HH:mm");
            }
            if (configDet[0].ToDate != null)
            {
                DateTime toDate = Convert.ToDateTime(configDet[0].ToDate);
                txtClosingDateTime.Text = toDate.ToString("dd-MMM-yyyy HH:mm");
            }



        }
    }

    protected void btnUpdate_Click(object sender,EventArgs e)
    {
        // txtDateTime.Text 
        if (Page.IsValid)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginAdminUserDetailsSession();
            var config = new oms_config();
            config.CONFIG_KEY = "SERVER_MAINTAINANCE";
            config.CONFIG_VALUE = txtMessage.Text;
            //Convert.ToDateTime(d.LICENSE_EXPIRY_DATE).ToString("dd-MMM-yyyy") 
            DateTime dt;
            config.FromDate = null;
            config.ToDate = null;
            if (txtDateTime.Text.Length > 0)
            {
                dt = Convert.ToDateTime(txtDateTime.Text);
                config.FromDate = dt;
            }

            if (txtClosingDateTime.Text.Length > 0)
            {
                dt = Convert.ToDateTime(txtClosingDateTime.Text);
                config.ToDate = dt;
            }
            config.DELETED = 0;
            var configService = ServiceFactory.GetService<ConfigService>();
            configService.SaveConfigurationValues(config);
            dvSuccessMsg.Visible = true;
        }
        else
        {
            dvSuccessMsg.Visible = false;
        }
    }

    protected void ValidateDuration(object sender, ServerValidateEventArgs e)

    {
        DateTime start = DateTime.Parse(txtDateTime.Text);
        DateTime end = DateTime.Parse(txtClosingDateTime.Text);
        e.IsValid = start < end;
    }

    
}