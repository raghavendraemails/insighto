﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Data;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using App_Code;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using Insighto.Exceptions;
using System.Collections;
using CovalenseUtilities.Helpers; 
public partial class Admin_ManageUsers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindDropDownList(Constants.COMPANY_SIZE, drpCompanySize);
        }
    }
    protected void bindDropDownList(string CategoryName, DropDownList ddl)
    {
        PickListService pickListService = new PickListService();
        osm_picklist osm_picklist = new osm_picklist();
        osm_picklist.PARAM_VALUE = Constants.PARAM_VALUE;
        osm_picklist.PARAMETER = Constants.PARAMETER;
        osm_picklist.CATEGORY = CategoryName;
        var list = new List<osm_picklist>();

        if (ddl.ID == "drpCompanySize")
        {
            list = pickListService.GetPickListFontSize(osm_picklist);
        }
        else
        {
            list = pickListService.GetPickList(osm_picklist);
        }

        if (list.Count > 0)
        {
            ddl.DataSource = list;
            ddl.DataTextField = Constants.PARAM_VALUE;
            ddl.DataValueField = Constants.PARAMETER;

            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem(Constants.SELECT, Constants.SELECTVALUE));
            ddl.SelectedIndex = 0;
        }
    }
}