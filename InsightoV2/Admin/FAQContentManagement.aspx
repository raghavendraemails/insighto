﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="FAQContentManagement.aspx.cs" Inherits="Admin_FAQContentManagement" %>

<%@ Register src="../UserControls/FaqControls/CMSCreateLeftMenu.ascx" tagname="CMSCreateLeftMenu" tagprefix="uc1" %>
<%@ Register src="../UserControls/FaqControls/CMSDesignLeftMenu.ascx" tagname="CMSDesignLeftMenu" tagprefix="uc2" %>
<%@ Register src="../UserControls/FaqControls/CMSLaunchLeftMenu.ascx" tagname="CMSLaunchLeftMenu" tagprefix="uc3" %>
<%@ Register src="../UserControls/FaqControls/CMSAnalyzeLeftMenu.ascx" tagname="CMSAnalyzeLeftMenu" tagprefix="uc4" %>
<%@ Register src="../UserControls/FaqControls/CMSManageLeftMenu.ascx" tagname="CMSManageLeftMenu" tagprefix="uc5" %>
<%@ Register src="../UserControls/FaqControls/CMSPpssinglsurvey.ascx" tagname="CMSPpssingleLeftMenu" tagprefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
 
 <div class="clear">
    </div>
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text="FAQ Content Management System (CMS)"></asp:Label>          
            <!-- //page title -->
        </div>
        <div class="previewPanel">
            <asp:HyperLink ID="hlBackLink" NavigateUrl="~/Admin/ContentManagement.aspx" runat="server" Text="Back to CMS"></asp:HyperLink>
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
    <div style="padding-top:8px;">
    <div class="faqTopLinks">
    <ul>
        <li><asp:HyperLink ID="hlCreateSurveyLink" runat="server" Text="Create"></asp:HyperLink></li>
        <li><asp:HyperLink ID="hlDesignLink"  runat="server" Text="Design"></asp:HyperLink></li>
        <li><asp:HyperLink ID="hlLaunchLink"  runat="server" Text="Launch"></asp:HyperLink></li>
        <li><asp:HyperLink ID="hlAnalyzeLink" runat="server" Text="Analyze"></asp:HyperLink></li>
        <li><asp:HyperLink ID="hlManageLink"  runat="server" Text="Manage"></asp:HyperLink></li>
        <li><asp:HyperLink ID="hlppssurvey"  runat="server" Text="Pay per survey"></asp:HyperLink></li>
    </ul>
    <div class="clear"></div>
 </div>
 </div>

 <div class="clear"></div>
        <!-- content panel -->
        <div class="surveyCreateTextPanel">
            <!-- admin content panel -->
            <uc1:CMSCreateLeftMenu ID="CMSCreateLeftMenu1"  runat="server" />
            <uc2:CMSDesignLeftMenu ID="CMSDesignLeftMenu1"  runat="server" />
            <uc3:CMSLaunchLeftMenu ID="CMSLaunchLeftMenu1" runat="server" />
            <uc4:CMSAnalyzeLeftMenu ID="CMSAnalyzeLeftMenu1" runat="server" />
            <uc5:CMSManageLeftMenu ID="CMSManageLeftMenu1" runat="server" />
            <uc6:CMSPpssingleLeftMenu ID="CMSPpssingleLeftMenu1" runat="server" />
           
                 <div class="faqCmsContentPanel">
                <div class="successPanel" runat="server" visible="false" id="sucessMsg">
                    <div>
                        <asp:Label ID="lblMessage" runat="server" /></div>
                </div>
                <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                    <div>
                        <asp:Label ID="lblErrMsg" runat="server" Visible="False" ></asp:Label></div>
                </div>
                <div class="cmsPathway" id="divHeading" runat="server"></div>
                
                <p>                   
                    <b><asp:Label ID="lblPageTitle" class="purpleColor" runat="server" 
                        Text="Page Title"></asp:Label></b>
                </p>
                <p>
                    <asp:TextBox ID="txtPageName" runat="server" CssClass="textBoxMedium" 
                        Enabled="False" ></asp:TextBox></p>
                <p>
                    &nbsp;</p>
                <p>
                    <b><asp:Label ID="lblContent" class="purpleColor" runat="server" Text="Content" 
                       ></asp:Label></b>
                </p>
                <p>
                    <input type="hidden" runat="server" id="EditorQuesIntroParams" value="0" />
                    <textarea id="EditorQuestionIntro" runat="server" cols="10" rows="8" class="mceEditor EditorQuestionIntro"
                        style="width: 640px; height: 250px;"></textarea></p>
                <p>
                    &nbsp;</p>
                <div class="bottomButtonPanel">
                    <asp:Button ID="btnSave" runat="server" CssClass="dynamicButton" Text="Save" 
                        ToolTip="Save" onclick="btnSave_Click"
                         />
                    <asp:Button ID="btnCancel" runat="server" CssClass="dynamicButton" 
                        Text="Cancel" ToolTip="Cancel" onclick="btnCancel_Click" />
                </div>
                <asp:HiddenField ID ="hdnLinkPage" runat="server" /> 
            </div>
            <!-- //admin content panel -->
   
        </div>
        <div class="clear">
        </div>
        <!-- //content panel -->
    </div>
     <script type="text/javascript" src="../Scripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            // General options
            mode: "textareas",
            //editor_selector: "mceEditor",
            theme: "advanced",
            relative_urls: false,
            //plugins: "advimage",
            plugins: "paste",
            // Theme options
            theme_advanced_buttons1: "bold,italic,underline,fontselect,fontsizeselect,forecolor,pasteword,link,unlink,code",
            theme_advanced_buttons2: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            //theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing: false,
            // Drop lists for link/image/media/template dialogs          
            external_image_list_url: "lists/image_list.js",
            // Example content CSS (should be your site CSS)
            content_css: "css/content.css",
            oninit: "postInitWork",
            // Replace values for the template plugin
            template_replace_values: {
                username: "Some User",
                staffid: "991234"
            }
        });

        function postInitWork() {

            var val = document.getElementById("<%=EditorQuesIntroParams.ClientID%>").value;
            var sty_val = val.split("_");
            if (sty_val.length == 3) {
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.fontFamily = sty_val[0];
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.fontSize = sty_val[1];
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.color = sty_val[2];
            }

        }

        function cleartext() {
            tinyMCE.getInstanceById('<%= EditorQuestionIntro.ClientID %>').setContent('');
            return false;
        }

        function resetText() {
            tinyMCE.getInstanceById('<%= EditorQuestionIntro.ClientID %>').setContent('<p> You are invited to join a short survey being held among the customers of COMPANY/BRAND X. There are only (X No. of questions) and it will take about X minutes of your time. We will keep all your information confidential and will not transmit it to any third parties. If you have any questions about this survey, please contact me directly at my email address provided below. <br> Your Feedback is important!</br>');

        }
        function CopyIntro() {
            var introText = $('.hdnIntroText').val();
            $('.EditorQuestionIntro').text("hello");
            return false;
        }

        function hidevalidation() {
            document.getElementById('MainContent_SurveyMainContent_EditorQuestionIntro_parent').style.display = "none";
        }
    </script>
</asp:Content>

