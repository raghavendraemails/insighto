﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using System.Configuration;
using App_Code;

namespace Insighto.Pages.Admin
{
    public partial class AddPickList : BasePage
    {
        Hashtable ht = new Hashtable();
        #region Events

        #region Page_Load Event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int pickId = 0;
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
                if (ht != null && ht.Count > 0 && ht.Contains("Category") && ht.Contains("PickId") == false)
                {
                    lblCreate.Visible = true;
                    lblEdit.Visible = false;
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnCancel.Visible = false;
                    txtParameter.Attributes.Add("onkeyup","javascript:return fnUpdateValue()");
                }
                else if (ht != null && ht.Count > 0 && ht.Contains("PickId"))
                {
                    lblCreate.Visible = false;
                    lblEdit.Visible = true;
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    btnCancel.Visible = true;
                    pickId = ValidationHelper.GetInteger(ht["PickId"].ToString(), 0);
                }
            }
            if (!IsPostBack && pickId > 0)
            {
                var pickListService = ServiceFactory.GetService<PickListService>();
                var pickList = pickListService.FindPickListByPickId(pickId);
                if (pickList != null)
                {
                    txtParameter.Text = pickList.PARAMETER;
                    txtParamValue.Text = pickList.PARAM_VALUE;
                }
            }
        }
        #endregion

        #region Save Button Click Event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveItem();
        }
        #endregion

        #region Update Button Click Event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            SaveItem();
        }
        #endregion

        #region Cancel Click Event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            base.CloseModal("PickListManagement.aspx");
        }
        #endregion
        #endregion

        #region Methods
        /// <summary>
        /// Saves the data in to database
        /// </summary>
        private void SaveItem()
        {
            var pickListService = ServiceFactory.GetService<PickListService>();
            var pickList = new osm_picklist();
           
            pickList.PARAMETER = txtParameter.Text.Trim();
            int rtnValue = 0;
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            if (ht != null && ht.Count > 0 && ht.Contains("Category") && ht.Contains("PickId") == false)
            {
                pickList.PARAM_VALUE = txtParameter.Text.Trim(); 
                pickList.CATEGORY = ht["Category"].ToString();
                pickList.DELETED = 0;
                rtnValue = pickListService.SavePickListItem(pickList);
                if (rtnValue > 0)
                {
                    lblSuccessMsg.Text = Utilities.ResourceMessage("AddPickList_Success_Saved"); //"Pick list item has been created succesfully.";
                    base.CloseModelForSpecificTime("PickListManagement.aspx?Category=" + ht["Category"].ToString(), ConfigurationManager.AppSettings["TimeOut"]);
                }
            }
            else if (ht != null && ht.Count > 0 && ht.Contains("PickId"))
            {
                pickList.PICK_ID = ValidationHelper.GetInteger(ht["PickId"].ToString(), 0);
                pickList.CATEGORY = ht["Category"].ToString();   
                rtnValue = pickListService.UpdatePickListItem(pickList);
                if (rtnValue > 0)
                {
                    lblSuccessMsg.Text = Utilities.ResourceMessage("AddPickList_Success_Updated");//"Pick list item has been updated successfully.";
                    base.CloseModelForSpecificTime("PickListManagement.aspx?Category=" + ht["Category"].ToString(), ConfigurationManager.AppSettings["TimeOut"]);
                }
            }
            if (rtnValue > 0)
            {
                dvSuccessMsg.Visible = true;
                dvErrMsg.Visible = false;
            }
            else
            {
                dvErrMsg.Visible = true;
                dvSuccessMsg.Visible = false;
            }
        }
        #endregion
    }
}