﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="EditMyprofile.aspx.cs" Inherits="Insighto.Pages.Admin.EditMyprofile" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="contentPanelHeader"><div class="pageTitle">
    <asp:Label ID="lblTitle" runat="server" Text="Edit My Information" 
        meta:resourcekey="lblTitleResource1"></asp:Label></div></div>
<div class="contentPanel"><!-- content panel -->
		<div class="surveyCreateTextPanel"><!-- admin content panel -->
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccessMsg" runat="server" Text="Updated successfully." 
                    meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <div>
            <table border="0" cellpadding="5" cellspacing="2" width="100%">
                <tr>
                    <td class="wid_30" style="width:16%;"><asp:Label ID="lblLoginNameText" 
                            runat="server" Text="Login Name" meta:resourcekey="lblLoginNameTextResource1"></asp:Label></td>
                    <td class="wid_70"><b><asp:Label ID ="lblLoginName" runat ="server" 
                            meta:resourcekey="lblLoginNameResource1"></asp:Label></b></td>
                </tr>
                <tr>
                    <td class="wid_30" style="width:16%;"><asp:Label ID="lblAdminTypeText" 
                            runat="server" Text="Admin Type" meta:resourcekey="lblAdminTypeTextResource1"  ></asp:Label></td>
                    <td class="wid_70"><b><asp:Label ID="lblAdminType"  runat ="server" 
                            meta:resourcekey="lblAdminTypeResource1" ></asp:Label></b></td>
                </tr>
            </table>
        </div>

         <div class="con_login_pnl">
            <div class="loginlbl">
            <asp:Label ID="lblFirstName" runat="server" Text="First Name" 
                    meta:resourcekey="lblFirstNameResource1"></asp:Label>
            </div>
            <div class="loginControls">
            <asp:TextBox ID="txtFirstName" runat="server" CssClass="textBoxMedium" 
                    MaxLength="30" meta:resourcekey="txtFirstNameResource1"></asp:TextBox>
                     
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True"  ID="reqFirstName" 
                    CssClass="lblRequired" ControlToValidate="txtFirstName"
                        ErrorMessage="Please enter first name." runat="server" 
                    Display="Dynamic" meta:resourcekey="reqFirstNameResource1"></asp:RequiredFieldValidator>
            </div>
        </div>

         <div class="con_login_pnl">
            <div class="loginlbl">
            <asp:Label ID="lblLastName" runat="server" Text="Last Name" 
                    meta:resourcekey="lblLastNameResource1"></asp:Label>
            </div>
            <div class="loginControls">
            <asp:TextBox ID="txtLastName" runat="server" CssClass="textBoxMedium" 
                    MaxLength="30" meta:resourcekey="txtLastNameResource1"></asp:TextBox>
            </div>
        </div>

        <div class="con_login_pnl">
            <div class="reqlbl btmpadding"></div>
        </div>

        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
            <asp:Button ID="btnSave" runat="server"  CssClass="dynamicButton" 
                        ToolTip="Update" Text="Update" onclick="btnSave_Click" 
                    meta:resourcekey="btnSaveResource1"  />
            </div>
        </div>
        
         
		<!-- //admin content panel --></div>
		<div class="clear"></div>

	<!-- //content panel --></div>
</asp:Content>

