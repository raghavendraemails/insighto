﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="ManageUsers.aspx.cs" Inherits="Admin_ManageUsers" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
<input id="hdnStatus" class="hdnStatus" type="hidden" value="All" />
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text="Manage User"></asp:Label>
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
      <div class="successPanel" id="divSuccess" style="display: none">
            <div>
                <%-- <label id="lblMessage" />--%>
                <asp:Label ID="lblMessage" CssClass="successMsg" Text="Deleted Successfully." runat="server"
                    Style="display: none;"></asp:Label>
            </div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" style="display: none">
            <div>
                <asp:Label ID="lblErrMsg" CssClass="erorrMsg" runat="server" Text=""></asp:Label></div>
        </div>
        <div class="surveyCreateTextPanel">
            <!-- admin content panel -->
            <div class="adminGridSearchPanel">
                <div class="searchLble">
                    <asp:Label ID="lblSearch" runat="server" Text="Search"></asp:Label>
                    </div>
                <div class="searchTxtBoxPanel">
                    <%--<input type="text" class="textBoxUserSearch" />--%>
                    <input type="text" class="textBoxAdminSearch" id="txtSearch" /> 
                    <asp:Button ID="btnaddedIcon" runat="server" CssClass="searchBtnPlus btnaddedIcon" 
                        meta:resourcekey="btnaddedIconResource1"  />
                </div>
                <div class="editDeletePanel">
                     <asp:HyperLink ID="hlnkAddUser" CssClass="dynamicHyperLink"  
                        rel="framebox" h="500" w="950"
                            scrolling='yes' title='Copy' 
                        onclick='javascript: return ApplyFrameBox(this);' NavigateUrl="AddUser.aspx"
                            ToolTip=" Add User" runat="server" Text=" Add User" meta:resourcekey="hlnkAddUserResource1" 
                           ></asp:HyperLink>  
                </div>
                <div class="clear">
                </div>
            </div>
            <%--<div class="popupHeading"><!-- popup heading -->		
		<div class="popupTitle">
			<h3>Search User Accounts</h3>
		</div>		
		<!--<div class="popupClosePanel">
			<a href="#">X</a>
		</div>	-->
	<!-- //popup heading --></div>--%>
            <%--<div class="popupContentPanel"><!-- popup content panel -->--%>
           <div id="dvAdvanceSearch" style="display:none"> <div class="adminFormPanel01">
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblLicenseType" Text="License Type" runat="server" 
                            meta:resourcekey="lblLicenseTypeResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:DropDownList ID="drpLicenseType" CssClass="dropDown drpLicenseType" 
                            runat="server" meta:resourcekey="drpLicenseTypeResource1">
                             <asp:ListItem Value="FREE">FREE</asp:ListItem>
                             <asp:ListItem Value ="PRO_MONTHLY">PRO_MONTHLY</asp:ListItem>
                             <asp:ListItem Value ="PRO_QUARTERLY">PRO_QUARTERLY</asp:ListItem>
                             <asp:ListItem Value ="PRO_YEARLY">PRO_YEARLY</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblUserType" Text="User Type" runat="server" 
                            meta:resourcekey="lblUserTypeResource1"></asp:Label></div>
                    <div class="loginControls">
                        <asp:DropDownList ID="drpUserType" CssClass="dropDown drpUserType" 
                            runat="server" meta:resourcekey="drpUserTypeResource1">
                            <asp:ListItem Value="Active" meta:resourcekey="ListItemResource4">Active</asp:ListItem>
                            <asp:ListItem Value="InActive" meta:resourcekey="ListItemResource5">InActive</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblEmailId" runat="server" Text="EmailId" 
                            meta:resourcekey="lblEmailIdResource1"></asp:Label>
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtEmailId" runat="server" CssClass="textBoxMedium txtEmailId" 
                            meta:resourcekey="txtEmailIdResource1" />
                          <%--<asp:RegularExpressionValidator SetFocusOnError="true"  ID="regEmailId" runat="server" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                         CssClass="lblRequired" ControlToValidate="txtEmailId" ErrorMessage="Please enter valid emailId."
                                     Display="Dynamic"></asp:RegularExpressionValidator>--%>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblFirstName" runat="server" Text="First Name" 
                            meta:resourcekey="lblFirstNameResource1"></asp:Label>
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtFirstName" runat="server" 
                            CssClass="textBoxMedium txtFirstName" 
                            meta:resourcekey="txtFirstNameResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblLastName" runat="server" Text="Last Name" 
                            meta:resourcekey="lblLastNameResource1"></asp:Label>
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtLastName" runat="server" 
                            CssClass="textBoxMedium txtLastName" meta:resourcekey="txtLastNameResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblCreatedDateFrom" runat="server" Text="Created Date From" 
                            meta:resourcekey="lblCreatedDateFromResource1"></asp:Label>
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtCreatedDateFrom" runat="server" 
                            CssClass="textBoxDate txtCreatedDateFrom" 
                            meta:resourcekey="txtCreatedDateFromResource1" />
                        To
                        <asp:TextBox ID="txtCreatedDateTo" runat="server" 
                            CssClass="textBoxDate txtCreatedDateTo" 
                            meta:resourcekey="txtCreatedDateToResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblCustomerId" Text="Customer ID" runat="server" 
                            meta:resourcekey="lblCustomerIdResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtCustomerId" runat="server" 
                            CssClass="textBoxMedium txtCustomerId" 
                            meta:resourcekey="txtCustomerIdResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblCity" Text="City" runat="server" 
                            meta:resourcekey="lblCityResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtCity" runat="server" CssClass="textBoxMedium txtCity" 
                            meta:resourcekey="txtCityResource1" />
                    </div>
                </div>
                
            </div>
            <div class="adminFormPanel03">
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblState" Text="State" runat="server" 
                            meta:resourcekey="lblStateResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtState" runat="server" CssClass="textBoxMedium txtState" 
                            meta:resourcekey="txtStateResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblCompany" Text="Company" runat="server" 
                            meta:resourcekey="lblCompanyResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtCompany" runat="server" CssClass="textBoxMedium txtCompany" 
                            meta:resourcekey="txtCompanyResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblExpiryDateFrom" Text="License Expiry Date" runat="server" 
                            meta:resourcekey="lblExpiryDateFromResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtExpiryDateFrom" runat="server" 
                            CssClass="textBoxDate txtExpiryDateFrom" 
                            meta:resourcekey="txtExpiryDateFromResource1" /> To
                        <asp:TextBox ID="txtExpiryDateTo" CssClass="textBoxDate txtExpiryDateTo" 
                            runat="server" meta:resourcekey="txtExpiryDateToResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblWorkIndustry" Text="Work Industry" runat="server" 
                            meta:resourcekey="lblWorkIndustryResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtWorkIndustry" runat="server" 
                            CssClass="textBoxMedium txtWorkIndustry" 
                            meta:resourcekey="txtWorkIndustryResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblJobFunction" Text="Job Function" runat="server" 
                            meta:resourcekey="lblJobFunctionResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtJobFunction" runat="server" 
                            CssClass="textBoxMedium txtJobFunction" 
                            meta:resourcekey="txtJobFunctionResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl"> <asp:Label ID="lblCompanySize" Text="Company Size" 
                            runat="server" meta:resourcekey="lblCompanySizeResource1" />
                       
                    </div>
                    <div class="loginControls">
                    <%-- <asp:TextBox ID="txtCompanySize" runat="server" 
                            CssClass="textBoxMedium txtCompanySize" 
                            meta:resourcekey="txtCompanySizeResource1" />--%>
                 <asp:DropDownList ID="drpCompanySize" runat="server" class="dropdownMedium drpCompanySize">
                </asp:DropDownList>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">   <asp:Label ID="lblLicenseExpiryPeriod" 
                            Text="License Expiry Period" runat="server" 
                            meta:resourcekey="lblLicenseExpiryPeriodResource1" />
                       
                    </div>
                    <div class="loginControls"> 
                        <asp:DropDownList ID="drpLicenseExpiryPeriod" CssClass="dropDown drpLicenseExpiryPeriod"
                            runat="server" meta:resourcekey="drpLicenseExpiryPeriodResource1">
                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                            <asp:ListItem Value="7" meta:resourcekey="ListItemResource6">7 Days</asp:ListItem>
                            <asp:ListItem Value="15" meta:resourcekey="ListItemResource7">15 Days</asp:ListItem>
                            <asp:ListItem Value="30" meta:resourcekey="ListItemResource8">1 Month</asp:ListItem>
                            <asp:ListItem Value="90" meta:resourcekey="ListItemResource9">3 Months</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
           
                
            </div><div class="clear"> </div>
            <div class="bottomButtonPanel" align="center">
                <asp:Button ID="btnAdvanceSearch" runat="server" Text="Search" ToolTip="Search" 
                    CssClass="dynamicButton btnAdvanceSearch" 
                    meta:resourcekey="btnAdvanceSearchResource1" />
            </div></div>
            <%-- <!-- //popup content panel -->--%>
            <div class="adminUserGridViewPanel" style="display: block;">
            </div>
            <div class="defaultH">
            </div>
            <div id="ptoolbar">
            </div>
            <table id="tblUsersList">
            </table>
            <div class="defaultH">
            </div>
            <!-- //admin content panel -->

           
        </div>
        <div class="clear">
        </div>
    </div>
    <script type="text/javascript" src="../Scripts/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        var serviceUrl = '../AjaxService.aspx';
        $(document).ready(function () {
            
            $("#tblUsersList").jqGrid({
                url: serviceUrl,
                postData: { method: "GetAllUserDetails" },
                datatype: 'json',

                headertitles: true,
                //headers: ['', '', '', '', '--', 'SURVEY LICENSE', '--', '--', 'POLL LICENSE', '--', '', '', ''],
                colNames: ['User Name', 'First Name', 'Created', 'Modified', 'Status', 'Survey Lic. Type', 'Survey Lic. Expiry', 'Survey Lic. Opted', 'Credits', 'Poll Lic. Type', 'Poll Lic. Expiry', 'Poll Lic. Opted', '', '', ''],
                colModel: [
                { name: 'LOGIN_NAME', index: 'LOGIN_NAME', resizable: true},
                { name: 'FIRST_NAME', index: 'FIRST_NAME', resizable: true},
                { name: 'CREATED_ON', index: 'CREATED_ON', align: 'left' },
                { name: 'MODIFIED_ON', index: 'MODIFIED_ON', align: 'left' },
                { name: 'STATUS', index: 'STATUS', align: 'center', resizable: false },
                { name: 'LICENSE_TYPE', index: 'LICENSE_TYPE', align: 'center', resizable: true, width: 170 },
                { name: 'LICENSE_EXPIRY_DATE', index: 'LICENSE_EXPIRY_DATE', align: 'left', resizable: true },
                { name: 'LICENSE_OPTED', index: 'LICENSE_OPTED', align: 'center', resizable: true, width: 180 },
                { name: 'CREDITCOUNT', index: 'CREDITCOUNT', align: 'center', resizable: false},
                { name: 'POLLLICENCETYPE', index: 'POLLLICENCETYPE', align: 'center', resizable: true },
                { name: 'POLLLICEXPIRYDATE', index: 'POLLLICEXPIRYDATE', align: 'center', resizable: false },
                { name: 'POLLLICENCETYPEOPTED', index: 'POLLLICENCETYPEOPTED', align: 'center', resizable: true },
                { name: 'EditUrl', index: 'editUrl', width: 20, resizable: false, align: 'center', formatter: EditLinkFormatter, sortable: false },
                { name: 'ViewUrl', index: 'passHistoryUrl', width: 20, align: 'center', resizable: false, formatter: ViewLinkFormatter, sortable: false },
                { name: 'USERID', index: 'USERID', width: 25, align: 'center', resizable: false, formatter: DeleteLinkFormatter, sortable: false },
                ],
                //colNames: ['User Name', 'First Name', 'Created', 'Modified', 'Status', 'Survey Lic. Type', 'Survey Lic. Expiry', 'Survey Lic. Opted', 'Credits', '', '', ''],
                //colModel: [
                //    { name: 'LOGIN_NAME', index: 'LOGIN_NAME', resizable: true},
                //    { name: 'FIRST_NAME', index: 'FIRST_NAME', resizable: true},
                //    { name: 'CREATED_ON', index: 'CREATED_ON', align: 'left' },
                //    { name: 'MODIFIED_ON', index: 'MODIFIED_ON', align: 'left' },
                //    { name: 'STATUS', index: 'STATUS', align: 'center', resizable: false },
                //    { name: 'LICENSE_TYPE', index: 'LICENSE_TYPE', align: 'center', resizable: true, width: 170 },
                //    { name: 'LICENSE_EXPIRY_DATE', index: 'LICENSE_EXPIRY_DATE', align: 'left', resizable: true },
                //    { name: 'LICENSE_OPTED', index: 'LICENSE_OPTED', align: 'center', resizable: true, width: 180 },
                //    { name: 'CREDITCOUNT', index: 'CREDITCOUNT', align: 'center', resizable: false},
                //    { name: 'EditUrl', index: 'editUrl', width: 20, resizable: false, align: 'center', formatter: EditLinkFormatter, sortable: false },
                //    { name: 'ViewUrl', index: 'passHistoryUrl', width: 20, align: 'center', resizable: false, formatter: ViewLinkFormatter, sortable: false },
                //    { name: 'USERID', index: 'USERID', width: 25, align: 'center', resizable: false, formatter: DeleteLinkFormatter, sortable: false },
                //],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'MODIFIED_ON',
                sortorder: "desc",

                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: window.innerWidth*95/100,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblUsersList").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
            $("#tblUsersList").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');
            //$("#tblUsersList").jqGrid('setGroupHeaders', {
            //    useColSpanStyle: true,
            //    groupHeaders: [{
            //        startColumnName: "amount",
            //        numberOfColumns: 5,
            //        titleText: 'abcd'
            //    }]
            //});
            
            var currentTime = new Date();
            var hours = currentTime.getHours();
            var minutes = currentTime.getMinutes();
            $(".txtExpiryDateFrom").datepicker({
                minDate: 0,
                dateFormat: 'dd-M-yy'
            });
            $(".txtExpiryDateTo").datepicker({
                minDate: 0,
                dateFormat: 'dd-M-yy'
            });

            $(".txtCreatedDateFrom").datepicker({
                minDate: 0,
                dateFormat: 'dd-M-yy'
            });
            $(".txtCreatedDateTo").datepicker({
                minDate: 0,
                dateFormat: 'dd-M-yy'
            });
        });

        function EditLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='" + cellvalue + "' rel='framebox' w='950' h='500' scrolling='true' ><img src='../App_Themes/Classic/Images/icon-pen-active.gif' title='Edit'/></a>";
        }

        function ViewLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='" + cellvalue + "' rel='framebox' w='950' h='500'  scrolling='yes'><img src='../App_Themes/Classic/Images/icon-tv-active.gif' alt='View Profile Info' title='View Profile Info'/></a>";
        }

        function DeleteLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='#' class='button-small-gap' id='lnkDelete' onclick='javascript: return DeleteUser(" + cellvalue + ");'><img src='../App_Themes/Classic/Images/icon-delete-active.gif' title='Delete' /></a>";
        }

        function gridReload() {
            var keyword = $("#txtSearch").val();
            $("#tblUsersList").jqGrid('setGridParam', { url: serviceUrl + "?keyword=" + keyword, page: 1 }).trigger("reloadGrid");
        }


        function DeleteUser(UserId) {
            var answer = confirm('Are you sure! you want to delete?');

            if (answer) {
                $.post("../AjaxService.aspx", { "method": "DeleteUserByUserId", "Id": UserId },
                   function (result) {

                       if (result == '0') {
                           $('#divSuccess').show();
                           $('.successMsg').show()
                           $('.successMsg').html('User deleted successfully.');
                           var status = $('#hdnStatus').val();
                           gridReload(status);
                       }
                       else {
                           $('#divSuccess').hide();
                           $('.successMsg').hide()
                       }
                   });
            }
        }

 

        $("#txtSearch").bind('keyup', function () { gridReload(); });

        $('.btnAdvanceSearch').click(function (e) {
            e.preventDefault();
            var firstName = $(".txtFirstName").val();
            var lastName = $(".txtLastName").val();
            var customerId = $(".txtCustomerId").val();
            var cityName = $(".txtCity").val();
            var stateName = $(".txtState").val();
            var company = $(".txtCompany").val();
            var workIndustry = $(".txtWorkIndustry").val();
            var jobFunction = $(".txtJobFunction").val();
            var companySize = $(".drpCompanySize").val();
            var licenseType = $(".drpLicenseType").val();
            var userType = $(".drpUserType").val();
            var licenseExpiryPeriod = $(".drpLicenseExpiryPeriod").val();
            var expiryDateFrom = $(".txtExpiryDateFrom").val();
            var expiryDateTo = $(".txtExpiryDateTo").val();
            var createdDateFrom = $(".txtCreatedDateFrom").val();
            var createdDateTo = $(".txtCreatedDateTo").val();
            var emailId = $(".txtEmailId").val();

            var isAdvancedSearch = true;
            if (createdDateFrom != '' || createdDateTo != '') {
                if (createdDateFrom >= createdDateTo) {

                    $('#dvErrMsg').show();
                    $('.erorrMsg').show()
                    $('.erorrMsg').html('Created from date should be less than to date.');
                    return false;
                }
                else {
                    $('#divSuccess').hide();

                }
            }
            if (expiryDateFrom != '' || expiryDateTo != '') {
                if (expiryDateFrom >= expiryDateTo) {

                    $('#dvErrMsg').show();
                    $('.erorrMsg').show()
                    $('.erorrMsg').html('Expiry from date should be less than to date.');
                    return false;
                }
                else {
                    $('#dvErrMsg').hide();

                }
            }
            $("#tblUsersList").jqGrid('setGridParam',
            { url: serviceUrl + "?IsAdvancedSearch=" + isAdvancedSearch + "&FirstName=" + firstName + "&LastName=" + lastName + "&CityName=" + cityName
             + "&StateName=" + stateName + "&Company=" + company + "&WorkIndustry=" + workIndustry + "&JobFunction=" + jobFunction + "&CompanySize=" + companySize +
             "&LicenseType=" + licenseType + "&UserType=" + userType + "&licenseExpiryPeriod=" + licenseExpiryPeriod + "&ExpiryDateFrom=" + expiryDateFrom + "&ExpiryDateTo=" + expiryDateTo
             + "&CreatedDateFrom=" + createdDateFrom + "&CreatedDateTo=" + createdDateTo + "&CustomerId=" + customerId + "&EmailId=" + emailId, page: 1
            }).trigger("reloadGrid");

        });

        $('.btnaddedIcon').click(function (e) {
            e.preventDefault();

            if ($('#dvAdvanceSearch').css('display') == "none") {
                $('#dvAdvanceSearch').show();
                $('.btnaddedIcon').addClass("searchBtnMinus");
            }
            else {
                $('#dvAdvanceSearch').hide();
                $('.btnaddedIcon').removeClass("searchBtnMinus");
               
            }

        });
            
      
    </script>
</asp:Content>
