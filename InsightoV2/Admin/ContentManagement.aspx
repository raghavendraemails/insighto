﻿<%@ Page Title="Insighto" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="ContentManagement.aspx.cs" Inherits="Admin_ContentManagement" meta:resourcekey="PageResource1" %>

<%@ Register Src="~/UserControls/AdminControls/CMSLeftMenu.ascx" TagName="LeftMenu"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="clear">
    </div>
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text="Content Management System (CMS)" 
                meta:resourcekey="lblTitleResource1"></asp:Label>          
            <!-- //page title -->
        </div>
        <div class="previewPanel actionLinks"><asp:HyperLink ID="hlFAQContent" Text="FAQ Content Management" runat="server"></asp:HyperLink>  </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="surveyCreateTextPanel">
            <!-- admin content panel -->
            <uc:LeftMenu ID="ucLeftMenu" runat="server" />
            <div class="cmsContentPanel">
                <div class="successPanel" runat="server" visible="false" id="sucessMsg">
                    <div>
                        <asp:Label ID="lblMessage" runat="server" 
                            meta:resourcekey="lblMessageResource1" /></div>
                </div>
                <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                    <div>
                        <asp:Label ID="lblErrMsg" runat="server" Visible="False" 
                            meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
                </div>
                <div class="cmsPathway" id="divHeading" runat="server"></div>
                <p>                   
                    <b><asp:Label ID="lblPageTitle" class="purpleColor" runat="server" 
                        Text="Page Title" meta:resourcekey="lblPageTitleResource1"></asp:Label></b>
                </p>
                <p>
                    <asp:TextBox ID="txtPageName" runat="server" CssClass="textBoxMedium" 
                        Enabled="False" meta:resourcekey="txtPageNameResource1"></asp:TextBox></p>
                <p>
                    &nbsp;</p>
                <p>
                    <b><asp:Label ID="lblContent" class="purpleColor" runat="server" Text="Content" 
                        meta:resourcekey="lblContentResource1"></asp:Label></b>
                </p>
                <p>
                    <input type="hidden" runat="server" id="EditorQuesIntroParams" value="0" />
                    <textarea id="EditorQuestionIntro" runat="server" cols="10" rows="8" class="mceEditor EditorQuestionIntro"
                        style="width: 643px; height: 150px;"></textarea></p>
                <p>
                    &nbsp;</p>
                <div class="bottomButtonPanel">
                    <asp:Button ID="btnSave" runat="server" CssClass="dynamicButton" Text="Save" ToolTip="Save"
                        OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="dynamicButton" 
                        Text="Cancel" OnClick="btnCancel_Click"
                        ToolTip="Cancel" meta:resourcekey="btnCancelResource1" />
                </div>
            </div>
            <!-- //admin content panel -->
        </div>
        <div class="clear">
        </div>
        <!-- //content panel -->
    </div>
    <script type="text/javascript" src="../Scripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            // General options
            mode: "textareas",
            //editor_selector: "mceEditor",
            theme: "advanced",
            relative_urls: false,
            //plugins: "paste,advimage",
            plugins: "paste",
            // Theme options
            theme_advanced_buttons1: "bold,italic,underline,fontselect,fontsizeselect,forecolor,pasteword,link,unlink,code",
            theme_advanced_buttons2: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            //theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing: false,

            // Drop lists for link/image/media/template dialogs          
            external_image_list_url: "lists/image_list.js",
            // Example content CSS (should be your site CSS)
            content_css: "css/content.css",
            oninit: "postInitWork",
            // Replace values for the template plugin
            template_replace_values: {
                username: "Some User",
                staffid: "991234"
            }
        });

        function postInitWork() {

            var val = document.getElementById("<%=EditorQuesIntroParams.ClientID%>").value;
            var sty_val = val.split("_");
            if (sty_val.length == 3) {
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.fontFamily = sty_val[0];
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.fontSize = sty_val[1];
                tinyMCE.getInstanceById('<%=EditorQuestionIntro.ClientID%>').getWin().document.body.style.color = sty_val[2];
            }

        }

        function cleartext() {
            tinyMCE.getInstanceById('<%= EditorQuestionIntro.ClientID %>').setContent('');
            return false;
        }

        function resetText() {
            tinyMCE.getInstanceById('<%= EditorQuestionIntro.ClientID %>').setContent('<p> You are invited to join a short survey being held among the customers of COMPANY/BRAND X. There are only (X No. of questions) and it will take about X minutes of your time. We will keep all your information confidential and will not transmit it to any third parties. If you have any questions about this survey, please contact me directly at my email address provided below. <br> Your Feedback is important!</br>');

        }
        function CopyIntro() {
            var introText = $('.hdnIntroText').val();
            $('.EditorQuestionIntro').text("hello");
            return false;
        }

        function hidevalidation() {
            document.getElementById('MainContent_SurveyMainContent_EditorQuestionIntro_parent').style.display = "none";
        }
    </script>
</asp:Content>
