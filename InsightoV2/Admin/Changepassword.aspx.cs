﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using App_Code;
using System.Configuration;
namespace Insighto.Admin.Pages
{
    public partial class Changepassword : AdminBasePage
    {
        #region Events
        #region Page_Load
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {


        }
        #endregion
        #region Save Button Click
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            ChangePassword();
        }
        #endregion

        #endregion

        #region Methods
        #region ChangePassword
        /// <summary>
        /// 
        /// </summary>
        public void ChangePassword()
        {
            int rtnVal;
            var adminService = ServiceFactory.GetService<AdminUserService>();
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginAdminUserDetailsSession();
            var adminUser = new osm_adminuser();
            if (userInfo != null)
            {
                var user = adminService.GetAuthenticatedUser(userInfo.Email, txtOldPassword.Text);
                if (user != null)
                {
                    adminUser.PASSWORD = EncryptHelper.Encrypt(txtNewPassword.Text);
                    adminUser.MODIFIED_ON = DateTime.UtcNow;
                    adminUser.DELETED = 0;
                    adminUser.RESET = 0;
                    adminUser.ADMINUSERID = userInfo.AdminUserId;
                    adminService.UpdateAdminProfileByUserId(adminUser, "C");
                    string fromEmail = ConfigurationManager.AppSettings["emailActivation"];
                    var sendEmailService = ServiceFactory.GetService<SendEmailService>();
                    sendEmailService.ChangePassword("Change in Password", userInfo.FirstName, fromEmail, userInfo.Email);

                    lblSuccessMsg.Text = Utilities.ResourceMessage("SuccessMsg");
                    dvSuccessMsg.Visible = true;
                    dvErrMsg.Visible = false;
                    Response.AddHeader("REFRESH", "5;URL=" + "WelcomeAdmin.aspx");
                }
                else
                {

                    dvErrMsg.Visible = true;
                    dvSuccessMsg.Visible = false;
                }
            }


        }
        #endregion
        #endregion
    }
}