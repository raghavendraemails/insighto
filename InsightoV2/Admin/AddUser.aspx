﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="AddUser.aspx.cs" Inherits="Insighto.Admin.Pages.AddUser" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>Add User</h3>
        </div>
        <!--<div class="popupClosePanel">
			<a href="#">X</a>
		</div>	-->
        <!-- //popup heading -->
      <%--  <div class="popupClosePanel">
        <a href="#" class="popupCloseLink" onclick ="closeModalSelf(true,'ManageUsers.aspx');" title="Close">&nbsp;</a>
        </div>--%>
    </div>
    <div class="defaultHeight">
    </div>
    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccessMsg" runat="server" Text="User created successfully." meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
    </div>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblErrorMsg" runat="server" meta:resourcekey="lblErrorMsgResource1"></asp:Label></div>
    </div>
    <div class="lt_padding">
                                <asp:Label ID="requiredfield" CssClass="left_requirelbl" Text="indicates required fields"
                                    runat="server"></asp:Label></div>
    <div class="adminFormPanel01">
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblUserType" Text="Select User Type" runat="server" meta:resourcekey="lblUserTypeResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:DropDownList ID="drpUserType" CssClass="dropDown drpUserType" runat="server" meta:resourcekey="drpUserTypeResource1">
                    <asp:ListItem Value="FREE" meta:resourcekey="ListItemResource3">FREE</asp:ListItem>
                    <asp:ListItem Value="PRO_MONTHLY" meta:resourcekey="ListItemResource1">PRO_MONTHLY</asp:ListItem>
                    <asp:ListItem Value="PRO_QUARTERLY" meta:resourcekey="ListItemResource2">PRO_QUARTERLY</asp:ListItem>
                    <asp:ListItem Value="PRO_YEARLY" meta:resourcekey="ListItemResource4">PRO_YEARLY</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="con_login_pnl" id="dvpaymentType" style="display:none">
            <div class="loginlbl">
                <asp:Label ID="paymentType" runat="server" Text="Payment Type" AssociatedControlID="paymentTypeSelect"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:RadioButtonList ID="paymentTypeSelect" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Direct Deposit"></asp:ListItem>
                    <asp:ListItem Text="Cheque" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblFirstName" CssClass="requirelbl" Text="First Name" AssociatedControlID="txtFirstName"
                    runat="server" meta:resourcekey="lblFirstNameResource1" />
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtFirstName" runat="server" CssClass="textBoxMedium" MaxLength="30"
                    meta:resourcekey="txtFirstNameResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqFirstName" CssClass="lblRequired"
                    ControlToValidate="txtFirstName" ErrorMessage="Please enter first name." runat="server"
                    Display="Dynamic" meta:resourcekey="reqFirstNameResource1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator SetFocusOnError="true" ID="regName" CssClass="lblRequired"
                    ControlToValidate="txtFirstName" runat="server" ErrorMessage="Please enter alphabets only."
                    ValidationExpression="^[a-zA-Z ]*$" Display="Dynamic">
                </asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblLastName" CssClass="requirelbl" Text="Last Name" AssociatedControlID="txtLastName"
                    runat="server" meta:resourcekey="lblLastNameResource1" />
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtLastName" runat="server" class="textBoxMedium" MaxLength="30"
                    meta:resourcekey="txtLastNameResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqLastName" CssClass="lblRequired"
                    ControlToValidate="txtLastName" ErrorMessage="Please enter last name." runat="server"
                    Display="Dynamic" meta:resourcekey="reqLastNameResource1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regLastName" CssClass="lblRequired" ControlToValidate="txtLastName"
                                runat="server" ErrorMessage="Please enter alphabets only." ValidationExpression="^[a-zA-Z ]*$"
                                Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblAddressLine1" CssClass="requirelbl" AssociatedControlID="txtAddressLine1"
                    Text="Address Line 1" runat="server" meta:resourcekey="lblAddressLine1Resource1" />
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtAddressLine1" runat="server" class="textBoxMedium" MaxLength="100"
                    meta:resourcekey="txtAddressLine1Resource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqAddressLine1" CssClass="lblRequired"
                    ControlToValidate="txtAddressLine1" ErrorMessage="Please enter address line1."
                    runat="server" Display="Dynamic" meta:resourcekey="reqAddressLine1Resource1"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblAddressLine2" Text="Address Line 2" AssociatedControlID="txtAddressLine2"
                    runat="server" meta:resourcekey="lblAddressLine2Resource1" />
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtAddressLine2" runat="server" class="textBoxMedium" MaxLength="100"
                    meta:resourcekey="txtAddressLine2Resource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblCity" Text="City" CssClass="requirelbl" runat="server" AssociatedControlID="txtCity"
                    meta:resourcekey="lblCityResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtCity" runat="server" class="textBoxMedium" MaxLength="50" meta:resourcekey="txtCityResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqCity" CssClass="lblRequired"
                    ControlToValidate="txtCity" ErrorMessage="Please enter city." runat="server"
                    Display="Dynamic" meta:resourcekey="reqCityResource1"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblState" Text="State" AssociatedControlID="txtState" runat="server"
                    meta:resourcekey="lblStateResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtState" runat="server" class="textBoxMedium" MaxLength="50" />
                <%--  <asp:DropDownList ID="drpState" runat="server" class="dropdownMedium" 
                    meta:resourcekey="drpStateResource1">
                </asp:DropDownList>--%>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblPostalCode" Text="Postal Code" AssociatedControlID="txtPostalCode"
                    runat="server" meta:resourcekey="lblPostalCodeResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtPostalCode" runat="server" class="textBoxDateTime" MaxLength="10"
                    meta:resourcekey="txtPostalCodeResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RegularExpressionValidator SetFocusOnError="True" ID="RegularExpressionValidator1"
                    CssClass="lblRequired" ControlToValidate="txtPostalCode" runat="server" ValidationExpression="^[0-9]+$"
                    ErrorMessage="Please enter numbers only." Display="Dynamic" meta:resourcekey="RegularExpressionValidator1Resource1"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblCountry" CssClass="requirelbl" Text="Country" AssociatedControlID="drpCountry"
                    runat="server" meta:resourcekey="lblCountryResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:DropDownList ID="drpCountry" runat="server" class="dropdownMedium" meta:resourcekey="drpCountryResource1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqCountry" CssClass="lblRequired"
                    InitialValue=" " ControlToValidate="drpCountry" ErrorMessage="Please select country."
                    runat="server" Display="Dynamic" meta:resourcekey="reqCountryResource1"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <div class="adminFormPanel01">
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblCompany" Text="Company" AssociatedControlID="txtCompany" runat="server"
                    meta:resourcekey="lblCompanyResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" class="textBoxMedium"
                    meta:resourcekey="txtCompanyResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblWorkIndustry" Text="Work Industry" AssociatedControlID="drpWorkIndustry"
                    runat="server" meta:resourcekey="lblWorkIndustryResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:DropDownList ID="drpWorkIndustry" runat="server" class="dropdownMedium" meta:resourcekey="drpWorkIndustryResource1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblJobFunction" Text="Job Function" AssociatedControlID="drpJobFunction"
                    runat="server" meta:resourcekey="lblJobFunctionResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:DropDownList ID="drpJobFunction" runat="server" class="dropdownMedium" meta:resourcekey="drpJobFunctionResource1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblCompanySize" Text="Company Size" AssociatedControlID="drpCompanySize"
                    runat="server" meta:resourcekey="lblCompanySizeResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:DropDownList ID="drpCompanySize" runat="server" class="dropdownMedium" meta:resourcekey="drpCompanySizeResource1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblDept" Text="Department" AssociatedControlID="txtDept" runat="server"
                    meta:resourcekey="lblDeptResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtDept" runat="server" class="textBoxMedium" MaxLength="50" meta:resourcekey="txtDeptResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblPhoneNumber" CssClass="requirelbl" Text="Phone Number" AssociatedControlID="txtPhoneNumber"
                    runat="server" meta:resourcekey="lblPhoneNumberResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtPhoneNumber" runat="server" class="phoneCountry" MaxLength="4"
                    onkeypress="javascript:return onlyNumbers(event,'Countrycode');" onpaste="return false;"
                    meta:resourcekey="txtPhoneNumberResource1" />
                <asp:TextBox ID="txtPhoneNumber1" runat="server" class="phoneArea" MaxLength="5"
                    onkeypress="javascript:return onlyNumbers(event,'');" onpaste="return false;"
                    meta:resourcekey="txtPhoneNumber1Resource1" />
                <asp:TextBox ID="txtPhoneNumber2" runat="server" class="phoneNumber" MaxLength="10"
                    onkeypress="javascript:return onlyNumbers(event,'');" onpaste="return false;"
                    meta:resourcekey="txtPhoneNumber2Resource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqPhone" CssClass="lblRequired"
                    ControlToValidate="txtPhoneNumber2" ErrorMessage="Please enter phone number"
                    runat="server" Display="Dynamic" meta:resourcekey="reqPhoneResource1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator SetFocusOnError="True" ID="regPhone" CssClass="lblRequired"
                    ControlToValidate="txtPhoneNumber2" runat="server" ValidationExpression="^.*(?=.{1,10})(?=.*\d).*$"
                    Display="Dynamic" meta:resourcekey="regPhoneResource1"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator SetFocusOnError="True" ID="regPhoneArea" CssClass="lblRequired"
                    ControlToValidate="txtPhoneNumber1" runat="server" ValidationExpression="^.*(?=.{0,5})(?=.*\d).*$"
                    Display="Dynamic" meta:resourcekey="regPhoneAreaResource1"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator SetFocusOnError="True" ID="regCountryCode" CssClass="lblRequired"
                    ControlToValidate="txtPhoneNumber" runat="server" ValidationExpression="^.*(?=.{0,4})(?=.*\d).*$"
                    Display="Dynamic" meta:resourcekey="regCountryCodeResource1"></asp:RegularExpressionValidator></div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <span id="phoneText" class="note">Country Code - Area Code - Phone Number.</span>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblEmailId" runat="server" CssClass="requirelbl" Text="Email Id" AssociatedControlID="txtEmailId"
                    meta:resourcekey="lblEmailIdResource1" />
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtEmailId" runat="server" CssClass="textBoxMedium" MaxLength="40"
                    meta:resourcekey="txtEmailIdResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqEmailId" ControlToValidate="txtEmailId"
                    CssClass="lblRequired" ErrorMessage="Please enter emailId." runat="server" Display="Dynamic"
                    meta:resourcekey="reqEmailIdResource1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator SetFocusOnError="True" ID="regEmailId" runat="server"
                    ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                    CssClass="lblRequired" ControlToValidate="txtEmailId" ErrorMessage="Please enter email id in valid format. Ex: john@insighto.com."
                    Display="Dynamic" meta:resourcekey="regEmailIdResource1"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblPassword" CssClass="requirelbl" Text="Password" AssociatedControlID="txtPassword"
                    runat="server" meta:resourcekey="lblPasswordResource1" />
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtPassword" runat="server" CssClass="textBoxMedium" MaxLength="16"
                    TextMode="Password" meta:resourcekey="txtPasswordResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqNewPassword" CssClass="lblRequired"
                    ControlToValidate="txtPassword" ErrorMessage="Please enter password." runat="server"
                    Display="Dynamic" meta:resourcekey="reqNewPasswordResource1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator SetFocusOnError="true" ID="regNewPassword" CssClass="lblRequired"
                    ControlToValidate="txtPassword" runat="server" meta:resourcekey="lblLenPasswordResource2"
                    ValidationExpression="^.{6,16}$" Display="Dynamic" ValidationGroup="tab1"></asp:RegularExpressionValidator>
                <asp:CustomValidator SetFocusOnError="true" ID="cvNewPassword" Display="Dynamic"
                    runat="server" ControlToValidate="txtPassword" CssClass="lblRequired" ErrorMessage="Please enter password with combination of any of the two categories being mentioned."  ClientValidationFunction="ValidatePassword">
                </asp:CustomValidator>
            </div>
        </div>
        <div class="con_login_pnl" runat="server" id="divPasswordhelp">
            <div class="reqlbl btmpadding">
                <asp:Label ID="lblPasswordHelpText" CssClass="note" runat="server" meta:resourcekey="lblPasswordHelpTextResource" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblConfirmPassword" CssClass="requirelbl" Text="Confirm  Password"
                    AssociatedControlID="txtConfirmPassword" runat="server" meta:resourcekey="lblConfirmPasswordResource1" />
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="textBoxMedium" MaxLength="16"
                    TextMode="Password" meta:resourcekey="txtConfirmPasswordResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqConfirmPassword" CssClass="lblRequired"
                    ControlToValidate="txtConfirmPassword" ErrorMessage="Please enter confirm password."
                    runat="server" Display="Dynamic" meta:resourcekey="reqConfirmPasswordResource1"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="compConfirmPassword" Display="Dynamic" ControlToValidate="txtConfirmPassword"
                    ControlToCompare="txtPassword" CssClass="lblRequired" Text="Password and confirm  password did not match."
                    runat="server" SetFocusOnError="True" meta:resourcekey="compConfirmPasswordResource1" />
            </div>
        </div>
    </div>
    <div class="clear">
    </div>
    <div class="bottomButtonPanel" align="center">
        <asp:Button ID="btnSave" CssClass="dynamicButton" Text="Save" runat="server" ToolTip="Save"
            OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" />
        <asp:Button ID="btnSaveAddNew" CssClass="dynamicButton" Text="Save &amp; Add New"
            runat="server" ToolTip="Save & Add New" OnClick="btnSaveAddNew_Click" meta:resourcekey="btnSaveAddNewResource1" />
        <asp:HiddenField ID="hdnPrice" runat="server" />
        <asp:HiddenField ID="hdnTax" runat="server" />
        <asp:HiddenField ID="hdnMerchantId" runat="server" />
        <asp:HiddenField ID="hdnTaxAmount" runat="server" />
        <asp:HiddenField ID="hdnTotal" runat="server" />
        <asp:HiddenField ID="hdnOrderNo" runat="server" />
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".drpUserType").change(function () {
                if ($('.drpUserType').val() != "FREE") 
                    $('#dvpaymentType').show();
                else
                    $('#dvpaymentType').hide();

            });
        });
        function onlyNumbers(e, Type) {
            var key = window.event ? e.keyCode : e.which;
            if (Type == 'Countrycode' && key == 43) {
                return true;
            }
            else if (key > 31 && (key < 48 || key > 57)) {
                return false;
            }
            return true;
        }
        function ValidatePassword(oSrc, args) {

//            var l = /^\w*(?=\w*[a-z])\w*$/
//            var u = /^\w*(?=\w*[A-Z])\w*$/
//            var d = /^\w*(?=\w*\d)\w*$/
            var lower = /^.*(?=.*[a-z]).*$/ ;
            var upper = /^.*(?=.*[A-Z]).*$/ ;
            var digit = /^.*(?=.*\d).*$/ ;
            var spl = /^.*(?=.*[@#$%^&+=]).*$/ ;
            var cnt = 0;
            if (lower.test(args.Value)) { cnt = cnt + 1; }
            if (upper.test(args.Value)) { cnt = cnt + 1; }
            if (digit.test(args.Value)) { cnt = cnt + 1; }
            if (spl.test(args.Value)) { cnt = cnt + 1; }
            if (args.Value.length >= 6) {
                if (cnt < 2) {

                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            }
            else {
                args.IsValid = true;
            }
        }
    </script>
</asp:Content>
