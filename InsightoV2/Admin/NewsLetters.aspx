﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="NewsLetters.aspx.cs" Inherits="Admin_NewsLetters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <script src="../JQuery/jquery.MultiFile.js" type="text/javascript"></script>
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblPageTitle" runat="server" Text="News Letters"></asp:Label>
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    <div class="contentPanel">
        <p>
            &nbsp;</p>
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccessMsg" runat="server" Text="Newsletter sent successfully."></asp:Label>
            </div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrorMsg"  runat="server"></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg1" style="display:none">
            <div>
                <asp:Label ID="lblErroMsg1" CssClass="lblErrorMsg" runat="server"></asp:Label></div>
        </div>
         <div class="lt_padding">
                                <asp:Label ID="requiredfield" CssClass="left_requirelbl" Text="indicates required fields"
                                    runat="server"></asp:Label></div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblFrom" Text="From" CssClass="requirelbl" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtFrom" CssClass="textBoxMedium" MaxLength="50" runat="server"></asp:TextBox></div>
        </div>
         <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqEmailId" ControlToValidate="txtFrom"
                    CssClass="lblRequired" ErrorMessage="Please enter emailId." runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator SetFocusOnError="True" ID="regEmailId" runat="server"
                    ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                    CssClass="lblRequired" ControlToValidate="txtFrom" ErrorMessage="Please enter email id in valid format. Ex: john@insighto.com."
                    Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblUserType" Text="Select User Type" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:DropDownList ID="drpUserType" CssClass="dropDown drpUserType" runat="server">
                    <asp:ListItem Value="Select">Select</asp:ListItem>
                    <asp:ListItem Value="FREE">FREE</asp:ListItem>
                    <asp:ListItem Value="PRO_MONTHLY">PRO_MONTHLY</asp:ListItem>
                    <asp:ListItem Value="PRO_QUARTERLY">PRO_QUARTERLY</asp:ListItem>
                    <asp:ListItem Value="PRO_YEARLY">PRO_YEARLY</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
         <div class="con_login_pnl">
            <div class="loginlbl">
               <asp:Label ID="Label3" Text="" Visible="false" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
              <div class="informationPanelDefault" id="dvInformationDefault" style="width:635px" >
                            <div>
                            Paste or type multiple emails addresses into this field, separated by a comma,
                            in the same line and <b>without</b> any space. <br /><b>Example: </b>Tom@mycompany.com,John@mycompany.com</span>
                            </div>
                         </div>
             </div>
        </div>
       
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblAdditionEmailId" Text="Additional Email IDs" CssClass="requirelbl" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                            <asp:TextBox ID="txtAdditonalEmailID" CssClass="textAreaMedium"  Rows="4"
                        Columns="400" TextMode="MultiLine" Width="635"  runat ="server" ></asp:TextBox>
               
            </div>
        </div>
         <div class="con_login_pnl">
          <div class="loginlbl">
          </div>
            <div class="loginControls">
                <asp:Label ID="Label2" AssociatedControlID="regMultiple" runat="server" />              
               <asp:RegularExpressionValidator SetFocusOnError="true"   Display="Dynamic" CssClass="lblRequired"
                    ID="regMultiple" ControlToValidate="txtAdditonalEmailID" runat="Server" ErrorMessage="List contain invalid email(s)."
                    ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z](,[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z])*$" />                    
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <%--<asp:RequiredFieldValidator SetFocusOnError="True" ID="reqEditorQuestion" ControlToValidate="EditorQuestionIntro"
                    CssClass="lblRequired" ErrorMessage="Please enter message text." runat="server" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    <asp:CustomValidator SetFocusOnError="true" ID="CustomValidator1" runat="server" ErrorMessage="Please enter additional email ids"
                      CssClass="lblRequired" Display="Dynamic" OnServerValidate="custValidatAdditionalEmail"></asp:CustomValidator>
                  </div>
        </div>
       
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblSubject" CssClass="requirelbl" Text="Subject" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtSubject" CssClass="textBoxMedium" MaxLength="100" runat="server"></asp:TextBox>
            </div>
        </div>
         <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqSubject" ControlToValidate="txtSubject"
                    CssClass="lblRequired" ErrorMessage="Please enter subject." runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                  </div>
        </div>
        <div>
        </div>
        
         <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblAttachment" Text="Add Attachment" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
            <div>
                <asp:FileUpload ID="fuUpload" ToolTip="Please upload files of extension .xls, .xlsx, .pdf, .doc., .Jpeg, .jpg, .bmp, .txt, .png, .csv, .docx"  runat="server" class="multi"  accept="xls|xlsx|pdf|doc|Jpeg|jpg|bmp|txt|png|csv|docx"   /><br />
            </div>
           
           </div>
           </div>
         <div class="con_login_pnl">
              <div class="loginlbl">
                   <asp:Label ID="Label1" Text="" Visible="false" runat="server"></asp:Label>
                </div>
             <div class="loginControls">
             <div class="informationPanelDefault">
             <div>
                  <asp:Label ID="lblUploadFiles" runat="server"  Text="Please upload files of extension .xls, .xlsx, .pdf, .doc., .Jpeg, .jpg, .bmp, .txt, .png, .csv, .docx"></asp:Label>
              </div>
              </div> 
              </div> 
         </div>
          
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblEnterMessage" Text="Enter Message Text" CssClass="requirelbl" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <textarea id="EditorQuestionIntro"  runat="server"   cols="10" rows="8" class="mceEditor EditorQuestionIntro"
                    style="width: 643px; height: 150px;"></textarea>
            </div>
        </div>
         <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <%--<asp:RequiredFieldValidator SetFocusOnError="True" ID="reqEditorQuestion" ControlToValidate="EditorQuestionIntro"
                    CssClass="lblRequired" ErrorMessage="Please enter message text." runat="server" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    <asp:CustomValidator SetFocusOnError="true" ID="custText" runat="server" ErrorMessage="Please enter message text."
                      CssClass="lblRequired" Display="Dynamic" OnServerValidate="custValidateText"></asp:CustomValidator>
                  </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
            </div>
            <div class="loginControls">
                <p class="defaultHeight">
                </p>
                <asp:Button ID="btnSend" CssClass="dynamicButton"  runat="server" Text="Send" OnClick="btnSend_Click" />
            </div>
        </div>
        <asp:HiddenField ID="hdnAttchments" runat="server" />
        <div class="clear">
        </div>
    </div>
    <script type="text/javascript" src="../Scripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            // General options
            mode: "textareas",
            editor_selector: "mceEditor",
            theme: "advanced",
            relative_urls: false,
            plugins: "paste",
            editor_deselector: " mceNoEditor",
            // Theme options
            theme_advanced_buttons1: "bold,italic,underline,fontselect,fontsizeselect,forecolor,pasteword,link,unlink",
            theme_advanced_buttons2: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            //theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing: false,

            // Drop lists for link/image/media/template dialogs          
            external_image_list_url: "lists/image_list.js",
            // Example content CSS (should be your site CSS)
            content_css: "css/content.css",
            //oninit: "postInitWork",
            // Replace values for the template plugin
            template_replace_values: {
                username: "Some User",
                staffid: "991234"
            }
        });

       
    </script>
    <script type="text/javascript">
        function CheckMail() {
            var mailId = $(".drpUserType").val();           
            if (mailId == "Select") {
                $('#dvErrMsg1').show();
                $('.lblErrorMsg').html('Please enter additional email ids');
                return false;
            }
            else {
                $('#dvErrMsg1').hide();
            } 
        } 
    </script> 
 
</asp:Content>
