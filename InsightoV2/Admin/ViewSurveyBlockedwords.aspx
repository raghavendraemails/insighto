﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="ViewSurveyBlockedwords.aspx.cs" Inherits="Insighto.Admin.Pages.ViewSurveyBlockedwords" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                <asp:Label ID="lblViewBlcokedWords" runat="server" Text="View Blocked Words" 
                    meta:resourcekey="lblViewBlcokedWordsResource1"></asp:Label>
                </h3>
        </div>
        <!--<div class="popupClosePanel">
			<a href="#">X</a>
		</div>	-->
        <!-- //popup heading -->
     <%--   <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(false,'');">
                &nbsp;</a>
        </div>--%>

        
     </div>
     <div class="popupContentPanel"><!-- popup content panel -->
      <div class="defaultH">
                </div>
                <div id="ptoolbar">
                </div>
                <table id="tblSurveyBlockedList">
                </table>
                <div class="defaultH">
                </div>
                <asp:HiddenField ID ="hdnSurveyId" runat="server" /> 
     </div> 
     <script type="text/javascript">
         var serviceUrl = '../AjaxService.aspx';
          var surveyId = '<%= SurveyId %>';
        
         $(document).ready(function () {
             $("#tblSurveyBlockedList").jqGrid({
                 url: serviceUrl,
                 postData: { method: "ViewSurveyBlockedWordList", SurveyId: surveyId },
                 datatype: 'json',
                 colNames: ['Field Name', 'Blocked Words'],
                 colModel: [
                { name: 'FeildName', index: 'FeildName', width: 25, resizable: false },
                { name: 'BlockeWords', index: 'BlockeWords', width: 25, align: 'center', resizable: false },
                ],
                 rowNum: 10,
                 rowList: [10, 20, 30, 40, 50],
                 pager: '#ptoolbar',
                 gridview: true,
                 sortname: 'FeildName',
                 sortorder: "asc",

                 viewrecords: true,
                 jsonReader: { repeatitems: false },
                 width: 640,
                 caption: '',
                 height: '100%',
                 loadComplete: function () {
                     $('a[rel*=framebox]').click(function (e) {
                         e.preventDefault();
                         ApplyFrameBox($(this));
                     });
                 }
             });
             $("#tblSurveyBlockedList").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
             $("#tblSurveyBlockedList").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');
         });
     </script>
</asp:Content>

