﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using App_Code;

public partial class Admin_WelcomeAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginAdminUserDetailsSession();
            if (userInfo != null)
            {
                lblWelcomeMsg.Text = Utilities.ResourceMessage("lblWelcomeMsgResource1.Text") + " " + userInfo.AdminType;
            }
        }

    }
}