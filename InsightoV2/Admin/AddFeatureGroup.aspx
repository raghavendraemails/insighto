﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="AddFeatureGroup.aspx.cs" Inherits="Insighto.Pages.Admin.AddFeatureGroup" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" Runat="Server">
<div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
               <h3><asp:Label runat="server" ID="lblTitle" Text=""  ></asp:Label>
               </h3>
            
        </div>
       <%-- <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick ="closeModalSelf(true,'LicenseManagement.aspx');" title="Close">&nbsp;</a>
        </div>--%>
       
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="popupContentPanel" >
        <!-- popup content panel -->
           <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                <div><asp:Label ID="lblSuccessMsg"  runat="server" 
                    Text="Feature has been created succesfully." 
                        meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
           </div>
           <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">                
                <div>
                    <asp:Label ID="lblErrMsg"  runat="server" 
                    Text="Feature already exist." meta:resourcekey="lblErrMsgResource1" ></asp:Label></div>
            </div>
        <div class="formPanel">
            <!-- form -->
            <div class="forgotpanel">
                <div class="con_login_pnl" >
                    <div class="loginlbl">
                        <asp:Label ID="lblFeatureGroup" runat="server" Text="Feature Group" 
                            meta:resourcekey="lblFeatureGroupResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtFeatureGroup" runat="server" CssClass="textBoxMedium" 
                            MaxLength="25" meta:resourcekey="txtFeatureGroupResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqList" 
                            ControlToValidate="txtFeatureGroup" CssClass="lblRequired"
                            ErrorMessage="Please enter feature group." runat="server" 
                            Display="Dynamic" meta:resourcekey="reqListResource1"></asp:RequiredFieldValidator>
                    </div>
                </div>        
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblFeatureName" CssClass="requirelbl" runat="server" 
                            Text="Feature Name" meta:resourcekey="lblFeatureNameResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtFeatureName" runat="server" CssClass="textBoxMedium" 
                            MaxLength="25" meta:resourcekey="txtFeatureNameResource1" />

                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqFeatureName" 
                            ControlToValidate="txtFeatureName" CssClass="lblRequired"
                            ErrorMessage="Please enter feature name." runat="server" Display="Dynamic" 
                            meta:resourcekey="reqFeatureNameResource1"></asp:RequiredFieldValidator>
                    </div>
                </div>       
                 <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblFree" CssClass="requirelbl" runat="server" Text="Free" 
                            meta:resourcekey="lblFreeResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtFree" runat="server" CssClass="textBoxMedium" 
                            MaxLength="7" meta:resourcekey="txtFreeResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqFree" 
                            ControlToValidate="txtFree" CssClass="lblRequired"
                            ErrorMessage="Please enter free user." runat="server" Display="Dynamic" 
                            meta:resourcekey="reqFreeResource1"></asp:RequiredFieldValidator>                                           
                        <asp:RegularExpressionValidator ID="regFree" runat="server"  Display="Dynamic" 
                            CssClass="lblRequired" ValidationExpression="^[-+]?\d+?$" 
                            ControlToValidate="txtFree"  ErrorMessage="Please enter numeric values only." 
                            meta:resourcekey="regFreeResource1"></asp:RegularExpressionValidator>
                    </div>
                </div>   
                 <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblProMonthly" CssClass="requirelbl" runat="server" 
                            Text="Pro Monthly" meta:resourcekey="lblProMonthlyResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtProMonthly" runat="server" CssClass="textBoxMedium" 
                            MaxLength="7" meta:resourcekey="txtProMonthlyResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqProMonthly" 
                            ControlToValidate="txtProMonthly" CssClass="lblRequired"
                            ErrorMessage="Please enter pro monthly." runat="server" Display="Dynamic" 
                            meta:resourcekey="reqProMonthlyResource1"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                            runat="server"  Display="Dynamic" CssClass="lblRequired" 
                            ValidationExpression="^[-+]?\d+?$" ControlToValidate="txtProMonthly"  
                            ErrorMessage="Please enter numeric values only." 
                            meta:resourcekey="RegularExpressionValidator1Resource1"></asp:RegularExpressionValidator>
                    </div>
                </div>  
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblProQuarterly" CssClass="requirelbl" runat="server" 
                            Text="Pro Quarterly" meta:resourcekey="lblProQuarterlyResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtProQuarterly" runat="server" CssClass="textBoxMedium" 
                            MaxLength="7" meta:resourcekey="txtProQuarterlyResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqProQuarterly" 
                            ControlToValidate="txtProQuarterly" CssClass="lblRequired"
                            ErrorMessage="Please enter pro quarterly." runat="server" 
                            Display="Dynamic" meta:resourcekey="reqProQuarterlyResource1"></asp:RequiredFieldValidator>
                   <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"  
                            Display="Dynamic" CssClass="lblRequired" ValidationExpression="^[-+]?\d+?$" 
                            ControlToValidate="txtProQuarterly"  
                            ErrorMessage="Please enter numeric values only." 
                            meta:resourcekey="RegularExpressionValidator2Resource1"></asp:RegularExpressionValidator>
                    </div>
                </div>  
                 <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblProYearly" CssClass="requirelbl" runat="server" 
                            Text="Pro Yearly" meta:resourcekey="lblProYearlyResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtProYearly" runat="server" CssClass="textBoxMedium" 
                            MaxLength="7" meta:resourcekey="txtProYearlyResource1" />

                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqProYearly" 
                            ControlToValidate="txtProYearly" CssClass="lblRequired"
                            ErrorMessage="Please enter pro yearly." runat="server" Display="Dynamic" 
                            meta:resourcekey="reqProYearlyResource1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" 
                            runat="server"  Display="Dynamic" CssClass="lblRequired" 
                            ValidationExpression="^[-+]?\d+?$" ControlToValidate="txtProYearly"  
                            ErrorMessage="Please enter numeric values only." 
                            meta:resourcekey="RegularExpressionValidator3Resource1"></asp:RegularExpressionValidator>
                    </div>
                </div>  
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblPremiumMonthly" CssClass="requirelbl" runat="server" 
                            Text="Premium Monthly" meta:resourcekey="lblPremiumMonthlyResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtPreimiumMonthly" runat="server" CssClass="textBoxMedium" 
                            MaxLength="7" meta:resourcekey="txtPreimiumMonthlyResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqPremeiumMonthly" 
                            ControlToValidate="txtPreimiumMonthly" CssClass="lblRequired"
                            ErrorMessage="Please enter premium monthly." runat="server" 
                            Display="Dynamic" meta:resourcekey="reqPremeiumMonthlyResource1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" 
                            runat="server"  Display="Dynamic" CssClass="lblRequired" 
                            ValidationExpression="^[-+]?\d+?$" ControlToValidate="txtPreimiumMonthly"  
                            ErrorMessage="Please enter numeric values only." 
                            meta:resourcekey="RegularExpressionValidator4Resource1"></asp:RegularExpressionValidator>
                    </div>
                </div>  
                 <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblPremiumQuarterly" CssClass="requirelbl" runat="server" 
                            Text="Premium Quarterly" meta:resourcekey="lblPremiumQuarterlyResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtPremiumQuarterly" runat="server" CssClass="textBoxMedium" 
                            MaxLength="7" meta:resourcekey="txtPremiumQuarterlyResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqPremiumQuarterly" 
                            ControlToValidate="txtPremiumQuarterly" CssClass="lblRequired"
                            ErrorMessage="Please enter premium quarterly." runat="server" 
                            Display="Dynamic" meta:resourcekey="reqPremiumQuarterlyResource1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" 
                            runat="server"  Display="Dynamic" CssClass="lblRequired" 
                            ValidationExpression="^[-+]?\d+?$" ControlToValidate="txtPremiumQuarterly"  
                            ErrorMessage="Please enter numeric values only." 
                            meta:resourcekey="RegularExpressionValidator5Resource1"></asp:RegularExpressionValidator>
                    </div>
                </div>  
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblPremiumYearly" CssClass="requirelbl" runat="server" 
                            Text="Premium Yearly" meta:resourcekey="lblPremiumYearlyResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtPremiumYearly"  runat="server" CssClass="textBoxMedium" 
                            MaxLength="7" meta:resourcekey="txtPremiumYearlyResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqPremiumYearly" 
                            ControlToValidate="txtPremiumYearly" CssClass="lblRequired"
                            ErrorMessage="Please enter premium yearly." runat="server" 
                            Display="Dynamic" meta:resourcekey="reqPremiumYearlyResource1"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" 
                            runat="server"  Display="Dynamic" CssClass="lblRequired" 
                            ValidationExpression="^[-+]?\d+?$" ControlToValidate="txtPremiumYearly"  
                            ErrorMessage="Please enter numeric values only." 
                            meta:resourcekey="RegularExpressionValidator6Resource1"></asp:RegularExpressionValidator>
                    </div>
                </div>  
                 <%--<div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblFeatureTypeFlag" runat="server" Text="FEATURE_TYPE_FLAG"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtFeatureTypeFlag" runat="server" CssClass="textBoxMedium" 
                            MaxLength="25" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqFeatureTypeFlag" ControlToValidate="txtFeatureTypeFlag" CssClass="lblRequired"
                            ErrorMessage="Please enter feature flag." runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>--%>  
                 <%--<div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblFeature" runat="server" Text="FEATURE"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtFeature" runat="server" CssClass="textBoxMedium" 
                            MaxLength="25" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqFeature" ControlToValidate="txtFeature" CssClass="lblRequired"
                            ErrorMessage="Please enter feature." runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>  --%>

   
                 <div class="con_login_pnl">
                    <div class="loginlbl">
                     <asp:Label ID="lblbtnNewFolder" runat="server" AssociatedControlID="btnSave" meta:resourcekey="lblbtnNewFolderResource1" />
                    </div>
                    <div class="loginControls">
                             <asp:Button ID="btnSave" runat="server" CssClass="btn_small_65" Text="Save"
                            ToolTip="Save"  
                            onclick="btnSave_Click" meta:resourcekey="btnSaveResource1" />
                             <asp:Button ID="btnUpdate" runat="server" CssClass="btn_small_65" Text="Update"
                            ToolTip="Update"   Visible="False"
                            onclick="btnUpdate_Click" meta:resourcekey="btnUpdateResource1" />
                           <%--  <asp:Button ID="btnCancel" runat="server" CssClass="btn_small_65" Text="Cancel"
                            ToolTip="Cancel"   Visible="false" ValidationGroup="cancel"
                            onclick="btnCancel_Click" />--%>
                    </div>
                </div>
               <div class="con_login_pnl" id="successMessage">
                    <div class="reqlbl btmpadding">
                        <asp:Label ID="lbl" runat="server" meta:resourcekey="lblResource1"></asp:Label>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>            
            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>
</asp:Content>

