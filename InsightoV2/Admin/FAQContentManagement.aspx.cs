﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using App_Code;
public partial class Admin_FAQContentManagement : System.Web.UI.Page
{
    public string _pageName
    {
        get
        {
            Hashtable ht = new Hashtable();
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Contains("pageName"))
                    return ht["pageName"].ToString();
            }
            return "";
        }

    }
    public string _parentPage
    {
        get
        {
            Hashtable ht = new Hashtable();
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Contains("parentNode"))
                    return ht["parentNode"].ToString();
            }
            return "";
        }

    }
    public string _page
    {
        get
        {
            Hashtable ht = new Hashtable();
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Contains("page"))
                    return ht["page"].ToString();
            }
            return "";
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        FAQLinks();
        if (!IsPostBack)
        {
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Contains("page") && ht.Contains("pageName"))
            {
                hdnLinkPage.Value = ht["page"].ToString();  
                SetControls();
                txtPageName.Text = ht["pageName"].ToString();
                divHeading.InnerHtml = ht["pageName"].ToString().Replace("_", " & ");
                var pageContent = ServiceFactory.GetService<FAQService>().GetPageURL(ht["pageName"].ToString(), ht["parentNode"].ToString());
                if (pageContent != null)
                {
                    EditorQuestionIntro.Value = pageContent.Page_Content; 
                }
                else
                {
                    EditorQuestionIntro.Value = "";
                }
            }
            else
            {
                if (ht != null && ht.Contains("LinkPage"))
                {
                    string LinkPage = string.Empty;
                    LinkPage = ht["LinkPage"].ToString();
                    if (LinkPage == Constants.FAQCREATE )
                    {                       
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetFAQContentManagementURL(), "page=Create_Survey&pageName=About survey creation&parentNode=Create New Survey"));
                    }
                    else if (LinkPage == Constants.FAQDESIGN)
                    {                       
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetFAQContentManagementURL(), "page=Design&pageName=Theme&parentNode=Choose look and feel"));
                    }
                    else if (LinkPage == Constants.FAQLAUNCH)
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetFAQContentManagementURL(), "page=Launch&pageName=Schedule launch&parentNode=Survey / Alerts Scheduler"));
                    }
                    else if (LinkPage == Constants.FAQANALYZE)
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetFAQContentManagementURL(), "page=Analyze&pageName=Reports&parentNode=View Reports"));
                    }
                    else if (LinkPage == Constants.FAQMANAGE)
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetFAQContentManagementURL(), "page=Manage&pageName=Overview&parentNode=My Surveys"));
                    }
                    else if (LinkPage == Constants.FAQSingle)
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetFAQContentManagementURL(), "page=Pps&pageName=Overview&parentNode=Pps surveys"));
                    }
                    else
                    {
                       
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetFAQContentManagementURL(), "page=Create_Survey&pageName=About survey creation&parentNode=Create New Survey"));
                    }
                }
                else
                {
                   Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetFAQContentManagementURL(), "page=Create_Survey&pageName=About survey creation&parentNode=Create New Survey"));
                }
            }
        }
    }
    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        Hashtable ht = new Hashtable();
        ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
        if (ht != null && ht.Contains("page") && ht.Contains("pageName"))
        {
            txtPageName.Text = ht["pageName"].ToString(); ;
            divHeading.InnerHtml = ht["pageName"].ToString().Replace("_", " & ");
            var pageContent = ServiceFactory.GetService<FAQService>().GetPageURL(ht["pageName"].ToString(), ht["parentNode"].ToString());
            if (pageContent != null)
            {
                EditorQuestionIntro.Value = pageContent.Page_Content;
            }
        }
        else
        {
            EditorQuestionIntro.Value = "";
        }
    }
    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        osm_faqpages faqPages = new osm_faqpages();
        faqPages.Page_Name = _pageName.Replace("_", "&");
        faqPages.Page_Content = EditorQuestionIntro.Value;
        faqPages.Page_Name_Group = _page.Replace("_", " "); ;
        faqPages.Page_Name_Parent = _parentPage;

        faqPages = ServiceFactory.GetService<FAQService>().Update(faqPages);
        if (faqPages != null)
        {
            dvErrMsg.Visible = false;
            lblMessage.Text = "FAQ CMS Content updated successfully";// Utilities.ResourceMessage("CMS_Success_Update");//"CMS content updated successfully.";
            sucessMsg.Visible = true;
        }
        else
        {
            lblErrMsg.Text = "FAQ CMS Content updated successfully"; //Utilities.ResourceMessage("CMS_Failed_Update");//"CMS content updation failed.";
            dvErrMsg.Visible = true;
            sucessMsg.Visible = false;
        }
    }
    protected void FAQLinks()
    {
        hlCreateSurveyLink.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Admin/FAQContentManagement.aspx", "LinkPage=Create");
        hlDesignLink.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Admin/FAQContentManagement.aspx", "LinkPage=Design");
        hlLaunchLink.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Admin/FAQContentManagement.aspx", "LinkPage=Launch");
        hlAnalyzeLink.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Admin/FAQContentManagement.aspx", "LinkPage=Analyze");
        hlManageLink.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Admin/FAQContentManagement.aspx", "LinkPage=Manage");
        hlppssurvey.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Admin/FAQContentManagement.aspx", "LinkPage=Pps");
    }

    protected void SetControls()
    {
        if (hdnLinkPage.Value == Constants.FAQCREATE )
        {
            CMSPpssingleLeftMenu1.Visible = false;
            CMSCreateLeftMenu1.Visible = true;
            CMSDesignLeftMenu1.Visible = false;
            CMSLaunchLeftMenu1.Visible = false;
            CMSAnalyzeLeftMenu1.Visible = false;
            CMSManageLeftMenu1.Visible = false;
            hlCreateSurveyLink.CssClass = "active";  
        }
        else if (hdnLinkPage.Value == Constants.FAQDESIGN )
        {
            CMSPpssingleLeftMenu1.Visible = false;
            CMSCreateLeftMenu1.Visible = false;
            CMSDesignLeftMenu1.Visible = true;
            CMSLaunchLeftMenu1.Visible = false;
            CMSAnalyzeLeftMenu1.Visible = false;
            CMSManageLeftMenu1.Visible = false;
            hlDesignLink.CssClass = "active";
        }
        else if(hdnLinkPage.Value == Constants.FAQLAUNCH)
        {
            CMSPpssingleLeftMenu1.Visible = false;
            CMSCreateLeftMenu1.Visible = false;
            CMSDesignLeftMenu1.Visible = false;
            CMSLaunchLeftMenu1.Visible = true;
            CMSAnalyzeLeftMenu1.Visible = false;
            CMSManageLeftMenu1.Visible = false;
            hlLaunchLink .CssClass = "active";
        }
        else if (hdnLinkPage.Value == Constants.FAQANALYZE)
        {
            CMSPpssingleLeftMenu1.Visible = false;
            CMSCreateLeftMenu1.Visible = false;
            CMSDesignLeftMenu1.Visible = false;
            CMSLaunchLeftMenu1.Visible = false;
            CMSAnalyzeLeftMenu1.Visible = true;
            CMSManageLeftMenu1.Visible = false;
            hlAnalyzeLink.CssClass = "active";
        
        }
        else if (hdnLinkPage.Value == Constants.FAQMANAGE)
        {
            CMSPpssingleLeftMenu1.Visible = false;
            CMSCreateLeftMenu1.Visible = false;
            CMSDesignLeftMenu1.Visible = false;
            CMSLaunchLeftMenu1.Visible = false;
            CMSAnalyzeLeftMenu1.Visible = false;
            CMSManageLeftMenu1.Visible = true;
            hlManageLink.CssClass = "active";
        
        }
        else if (hdnLinkPage.Value == Constants.FAQSingle)
        {
            CMSCreateLeftMenu1.Visible = false;
            CMSDesignLeftMenu1.Visible = false;
            CMSLaunchLeftMenu1.Visible = false;
            CMSAnalyzeLeftMenu1.Visible = false;
            CMSManageLeftMenu1.Visible = false;
            CMSPpssingleLeftMenu1.Visible = true;
            hlppssurvey.CssClass = "active";
        }
        else
        {
            CMSPpssingleLeftMenu1.Visible = false;
            CMSCreateLeftMenu1.Visible = true;
            CMSDesignLeftMenu1.Visible = false;
            CMSLaunchLeftMenu1.Visible = false;
            CMSAnalyzeLeftMenu1.Visible = false;
            CMSManageLeftMenu1.Visible = false;
            hlCreateSurveyLink.CssClass = "active";
        }
    }
}