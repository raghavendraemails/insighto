﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="EditUser.aspx.cs" Inherits="Insighto.Admin.Pages.EditUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                Edit User</h3>
        </div>
        <!--<div class="popupClosePanel">
			<a href="#">X</a>
		</div>	-->
        <!-- //popup heading -->
      <%--  <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick ="closeModalSelf(true,'ManageUsers.aspx');" title="Close">&nbsp;</a>

        </div>--%>
    </div>
    <div class="defaultHeight">
    </div>
    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccessMsg" runat="server" Text="User updated successfully."></asp:Label></div>
    </div>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
        <asp:Label ID="lblErrorMsg" runat="server"></asp:Label></div>
        <div class="lt_padding">
                                <asp:Label ID="requiredfield" CssClass="left_requirelbl" Text="indicates required fields"
                                    runat="server"></asp:Label></div>
    <div class="adminFormPanel01">
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblUserType" runat="server" Text="Select User Type"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:DropDownList ID="drpUserType" CssClass="dropDown" runat="server">
                    <asp:ListItem Value="FREE">FREE</asp:ListItem>
                    <asp:ListItem Value="PRO_MONTHLY">PRO_MONTHLY</asp:ListItem>
                    <asp:ListItem Value="PRO_QUARTERLY">PRO_QUARTERLY</asp:ListItem>
                    <asp:ListItem Value="PRO_YEARLY">PRO_YEARLY</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblFirstName" CssClass="requirelbl" Text="First Name" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtFirstName"  runat="server" MaxLength="30" CssClass="textBoxMedium" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqFirstName" CssClass="lblRequired"
                    ControlToValidate="txtFirstName" ErrorMessage="Please enter first name." runat="server"
                    Display="Dynamic"></asp:RequiredFieldValidator>

                 <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regName" CssClass="lblRequired" ControlToValidate="txtFirstName"
                   runat="server" ErrorMessage="Please enter alphabets only." ValidationExpression="^[a-zA-Z ]*$" Display="Dynamic">
                </asp:RegularExpressionValidator>
               
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lbllastName" CssClass="requirelbl" Text="Last Name" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtLastName" runat="server" MaxLength="30" CssClass="textBoxMedium" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqLastName" CssClass="lblRequired"
                    ControlToValidate="txtLastName" ErrorMessage="Please enter last name." runat="server"
                    Display="Dynamic">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regLastName" CssClass="lblRequired" ControlToValidate="txtLastName"
                                runat="server" ErrorMessage="Please enter alphabets only." ValidationExpression="^[a-zA-Z ]*$"
                                Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblAddress1" CssClass="requirelbl" Text="Address 1" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtAddress1" runat="server" MaxLength="100" CssClass="textBoxMedium" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqAddressLine1" CssClass="lblRequired"
                    ControlToValidate="txtAddress1" ErrorMessage="Please enter address line1." runat="server"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblAddress2" Text="Address 2" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtAddress2" runat="server" MaxLength="100" CssClass="textBoxMedium" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblCity" CssClass="requirelbl" Text="City" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtCity" runat="server" MaxLength="50"  CssClass="textBoxMedium" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqCity" CssClass="lblRequired"
                    ControlToValidate="txtCity" ErrorMessage="Please enter city." runat="server"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblState" Text="State" AssociatedControlID="txtState" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
             <asp:TextBox ID="txtState" runat="server" MaxLength="50"  CssClass="textBoxMedium" />
              <%--  <asp:DropDownList ID="drpState" runat="server" class="dropdownMedium">
                </asp:DropDownList>--%>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblPostCode" Text="Post Code" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtPostCode" runat="server" MaxLength="10"  CssClass="textBoxDateTime" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionValidator1"
                    CssClass="lblRequired" ControlToValidate="txtPostCode" runat="server" ValidationExpression="^[0-9]+$"
                    ErrorMessage="Please enter numbers only." Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblCountry" CssClass="requirelbl" Text="Country" AssociatedControlID="drpCountry"
                    runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:DropDownList ID="drpCountry" runat="server" class="dropdownMedium" meta:resourcekey="drpCountryResource1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqCountry" CssClass="lblRequired"
                    InitialValue=" " ControlToValidate="drpCountry" ErrorMessage="Please select country."
                    runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <div class="adminFormPanel01">
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblCompany" Text="Company" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtCompany" runat="server" CssClass="textBoxMedium" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblWorkIndustry" Text="Work Industry" AssociatedControlID="drpWorkIndustry"
                    runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:DropDownList ID="drpWorkIndustry" runat="server" class="dropdownMedium">
                </asp:DropDownList>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblJobFunction" Text="Job Function" AssociatedControlID="drpJobFunction"
                    runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:DropDownList ID="drpJobFunction" runat="server" class="dropdownMedium">
                </asp:DropDownList>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblCompanySize" Text="Company Size" AssociatedControlID="drpCompanySize"
                    runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:DropDownList ID="drpCompanySize" runat="server" class="dropdownMedium">
                </asp:DropDownList>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblDept" Text="Department" AssociatedControlID="txtDept" runat="server"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtDept" runat="server" class="textBoxMedium" MaxLength="100" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblPhoneNumber" CssClass="requirelbl" Text="Phone Number" AssociatedControlID="txtPhoneNumber"
                    runat="server" meta:resourcekey="lblPhoneNumberResource1"></asp:Label>
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtPhoneNumber" runat="server" class="phoneCountry" MaxLength="4"
                    meta:resourcekey="txtPhoneNumberResource1" onkeypress="javascript:return onlyNumbers(event,'Countrycode');"
                    onpaste="return false;" />
                <asp:TextBox ID="txtPhoneNumber1" runat="server" class="phoneArea" MaxLength="5"
                    meta:resourcekey="txtPhoneNumber1Resource1" onkeypress="javascript:return onlyNumbers(event,'');"
                    onpaste="return false;" />
                <asp:TextBox ID="txtPhoneNumber2" runat="server" class="phoneNumber" MaxLength="10"
                    meta:resourcekey="txtPhoneNumber2Resource1" onkeypress="javascript:return onlyNumbers(event,'');"
                    onpaste="return false;" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqPhone" CssClass="lblRequired"
                    ControlToValidate="txtPhoneNumber2" ErrorMessage="Please enter phone number."
                    runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
            </div>
            <div class="loginControls">
                <label for="phoneText">
                </label>
                <span id="phoneText" class="note">Country Code - Area Code - Phone Number.</span>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblEmailId" runat="server" CssClass="requirelbl" Text="Email Id" AssociatedControlID="txtEmailId"
                    meta:resourcekey="lblEmailId" />
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtEmailId" runat="server" CssClass="textBoxMedium" MaxLength="40" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqEmailId" ControlToValidate="txtEmailId"
                    CssClass="lblRequired" ErrorMessage="Please enter emailId." runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator SetFocusOnError="true" ID="regEmailId" runat="server"
                    ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                    CssClass="lblRequired" ControlToValidate="txtEmailId" ErrorMessage="Please enter valid emailId"
                    Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
              <asp:HiddenField ID ="hdnPrice" runat="server" /> 
            <asp:HiddenField ID ="hdnTax" runat="server" /> 
            <asp:HiddenField ID ="hdnMerchantId" runat="server" /> 
            <asp:HiddenField ID ="hdnTaxAmount" runat="server" /> 
            <asp:HiddenField ID ="hdnTotal" runat ="server" />
            <asp:HiddenField ID ="hdnOrderNo" runat="server" /> 
            <asp:HiddenField ID ="hdnUserId" runat="server" /> 
            <asp:HiddenField ID ="hdnUpdateFlag" runat="server" /> 
        </div>
    </div>
    <div class="clear">
    </div>
    <div class="bottomButtonPanel" align="center">
        <asp:Button ID="btnUpdateUser" CssClass="dynamicButton" Text="Update User" ToolTip="Update User"  runat="server"
            OnClick="btnUpdateUser_Click" />
    </div>
    <script type="text/javascript">
        function onlyNumbers(e, Type) {
            var key = window.event ? e.keyCode : e.which;
            if (Type == 'Countrycode' && key == 43) {
                return true;
            }
            else if (key > 31 && (key < 48 || key > 57)) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
