﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="AddRole.aspx.cs" Inherits="Admin_AddRole" meta:resourcekey="PageResource1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
 
</asp:Content> 
        
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
  <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                <asp:Label runat="server" ID="lblCreate"  Visible="False" 
                    meta:resourcekey="lblCreateResource1"></asp:Label>
                <asp:Label runat="server" ID="lblEdit" Visible="False" 
                    meta:resourcekey="lblEditResource1"></asp:Label>
            </h3>
        </div>
      <%--  <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick="closeModalSelf(false,'');">&nbsp;</a>
        </div>--%>
    </div>
      <div class="popupContentPanel">

       <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccessMsg" runat="server"
                    meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server"  
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
         <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="lblRoleName" runat="server" 
                        meta:resourcekey="lblRoleNameResource1"></asp:Label>
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="txtRoleName" runat="server" CssClass="textBoxMedium" 
                        MaxLength="25" meta:resourcekey="txtRoleNameResource1"></asp:TextBox>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:RequiredFieldValidator SetFocusOnError="True"  ID="reqRoleName" 
                        CssClass="lblRequired" ControlToValidate="txtRoleName"
                        ErrorMessage="Please enter role name." runat="server" Display="Dynamic" 
                        meta:resourcekey="reqRoleNameResource1"></asp:RequiredFieldValidator>
                </div>
            </div>
        <div class="defaultHeight">
        </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                <div class="loginControls">
                    <asp:Button ID="btnSave" runat="server"  CssClass="dynamicButton" 
                        ToolTip="Save" onclick="btnSave_Click" 
                        meta:resourcekey="btnSaveResource1"/>
                     <asp:Button ID="btnUpdate" runat="server" 
                        ToolTip="Update" Visible="False"  CssClass="dynamicButton" 
                        meta:resourcekey="btnUpdateResource1" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" ValidationGroup="Cancel"
                        ToolTip="Cancel" CssClass="dynamicButton" 
                        meta:resourcekey="btnCancelResource1" onclick="btnCancel_Click" />
                </div>
             </div> 
                 </div> 
                </div> 
</asp:Content>

