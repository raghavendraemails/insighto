﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="ViewPendingInvoices.aspx.cs" Inherits="Admin_ViewPendingInvoices" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text="View Pending Invoices" 
                meta:resourcekey="lblTitleResource1"></asp:Label>
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="errorPanel" id="recordDeleted" style="display: none;">
            <span class="successMesg">
        <asp:Label ID="lblDeleted" runat="server" Text="Record deleted successfully" 
                meta:resourcekey="lblDeletedResource1"></asp:Label></span>
        </div>
        <div class="surveyCreateTextPanel">
            <!-- admin content panel -->
            <div class="adminGridSearchPanel">
                <div class="searchLblelong">
                    <asp:Label ID="lblHeading" runat="server" 
                        Text=" Pending Invoices Users for upgradation" 
                        meta:resourcekey="lblHeadingResource1"></asp:Label>
                </div>
                <div class="searchrghtPanel">
                    <asp:HyperLink ID="hlnkAddList" CssClass="dynamicHyperLink" NavigateUrl="RenewalPendingInvoices.aspx"
                        ToolTip="Renew Users" runat="server" Text="Renew Users" 
                        meta:resourcekey="hlnkAddListResource1"></asp:HyperLink>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="adminUserGridViewPanel" style="display: block;">
                <div id="ptoolbar">
                </div>
                <table id="tblAddressBook">
                </table>
            </div>
            <!-- //admin content panel -->
        </div>
        <div class="clear">
        </div>
    </div>
    <script type="text/javascript">

        var serviceUrl = '../AjaxService.aspx';
        $(document).ready(function () {
            $("#tblAddressBook").jqGrid({
                url: serviceUrl,
                postData: { method: "FindPendingInvoices", ordType: "" },
                datatype: 'json',
                colNames: ['Name', 'Email Id', 'Telephone', 'Created Date', 'Account Type', 'License Type', 'License Opted', 'Customer Id', 'Upgrade'],
                colModel: [
                { name: 'FIRST_NAME', index: 'FIRST_NAME', width: 35, resizable: false },
                { name: 'LOGIN_NAME', index: 'LOGIN_NAME', width: 80, align: 'left', resizable: false },
                { name: 'PHONE', index: 'PHONE', width: 50, align: 'left' },
                { name: 'CREATED_ON', index: 'CREATED_ON', width: 40, align: 'left', resizable: false},
                { name: 'ACCOUNT_TYPE', index: 'ACCOUNT_TYPE', width: 50, resizable: false, align: 'center' },
                { name: 'LICENSE_TYPE', index: 'LICENSE_TYPE', width: 30, align: 'center', resizable: false },
                { name: 'LICENSE_OPTED', index: 'LICENSE_OPTED', width: 35, align: 'center', resizable: false },
                { name: 'CUSTOMER_ID', index: 'CUSTOMER_ID', width: 35, align: 'left', resizable: false, sortable: false },
                { name: 'PK_ORDERID', index: 'PK_ORDERID', width: 15, align: 'center', resizable: false, formatter: UpgradeLinkFormatter, sortable: false },
                 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'CREATED_ON',
                sortorder: "asc",
                viewrecords: true,

                jsonReader: { repeatitems: false },
                width: 880,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblAddressBook").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
            $("#tblAddressBook").jqGrid('setLabel', 'FIRST_NAME', '', 'textalignleft');
        });

        function UpgradeLinkFormatter(cellvalue, options, rowObject) {
            return "<a href ='AddUserPaymentDetails.aspx?OrderId=" + cellvalue + "' rel='framebox' w='760' h='480' scrolling='yes' class='button-small-gap' id='lnkUpgrade'><img src='../App_Themes/Classic/Images/icon-pen-active.gif' title='Upgrade'/></a>"
        }

    </script>
</asp:Content>
