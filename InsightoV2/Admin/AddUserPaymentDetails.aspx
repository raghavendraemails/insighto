﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="AddUserPaymentDetails.aspx.cs" Inherits="Insighto.Admin.Pages.AddUserPaymentDetails" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                Add payment details</h3>
        </div>
     <%--   <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(false,'');">
                &nbsp;</a>
        </div>--%>
    </div>

    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccessMsg" runat="server" 
                Text="Payment details saved successfully." 
                meta:resourcekey="lblSuccessMsgResource1"></asp:Label>
         </div>
    </div>

    <div class="popupContentPanel">
       <div class="clear"></div>
       <p class="defaultHeight"></p>
        <div>
            <table border="0" cellpadding="5" cellspacing="2" width="100%">
                <tr>
                    <td width="20%" align="right"> Email ID</td>
                    <td width="30%"><b><asp:Label ID="lblEmailId" runat="server" meta:resourcekey="lblEmailIdResource1"></asp:Label></b></td>
                    <td width="20%" align="right">First Name</td>
                    <td width="30%"><b><asp:Label ID="lblFirsName" runat="server" meta:resourcekey="lblFirsNameResource1"></asp:Label></b></td>
                </tr>
                <tr>
                    <td align="right">License Type</td>
                    <td><b><asp:Label ID="lblLicenseType" runat="server" meta:resourcekey="lblLicenseTypeResource1"></asp:Label></b></td>
                    <td align="right">Phone</td>
                    <td><b><asp:Label ID="lblPhone" runat="server" meta:resourcekey="lblPhoneResource1"></asp:Label></b></td>
                </tr>
               
            </table>
            <p class="defaultHeight"></p>
        </div>
       
        <div class="clear">
        </div>
        <div class="surveyQuestionHeader">
        <span class="surveyQuestionTitle">
            Billing Info</span></div>
        <br />
        <div class="lt_padding">&nbsp;&nbsp;<b>Add Payment Details</b></div><br />

        <div class="con_login_pnl">
            <div class="loginlbl">
                Payment Type</div>
           
                <div class="bdrcontrols loginControls">
                    <asp:RadioButtonList ID="rblPaymentType" runat="server" CssClass="rblPaymentType"  RepeatDirection="Horizontal" 
                        meta:resourcekey="rblPaymentTypeResource1">
                        <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Direct Deposit</asp:ListItem>
                        <asp:ListItem Value="2" meta:resourcekey="ListItemResource2">Cheque</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqPaymentType" 
                        CssClass="lblRequired" ControlToValidate="rblPaymentType"
                        ErrorMessage="Please Select Payment Type." runat="server" 
                        Display="Dynamic" meta:resourcekey="reqPaymentTypeResource1"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>

        <div class="con_login_pnl" id="dvChequeNumber">
            <div class="loginlbl">
                Cheque Number</div>
            <div class="loginControls">
                <asp:TextBox ID="txtChequeNumber" CssClass="textBoxMedium" runat="server" 
                    meta:resourcekey="txtChequeNumberResource1"></asp:TextBox>
            </div>
        </div>

        <div class="con_login_pnl">
            <div class="loginlbl">
                Amount Paid</div>
            <div class="loginControls">
                <asp:TextBox ID="txtAmountPaid" CssClass="textBoxMedium" runat="server" 
                    meta:resourcekey="txtAmountPaidResource1"></asp:TextBox>
            </div>
        </div>

        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqAmountPaid" 
                    CssClass="lblRequired" ControlToValidate="txtAmountPaid"
                    ErrorMessage="Enter Amout Details." runat="server" Display="Dynamic" 
                    meta:resourcekey="reqAmountPaidResource1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator SetFocusOnError="True"  ID="regAmountPaid"   
                    runat="server" ValidationExpression="^[1-9]\d*(\.\d+)?$"
                    CssClass="lblRequired" ControlToValidate="txtAmountPaid" ErrorMessage="Please enter numeric values only."
                    Display="Dynamic" meta:resourcekey="regAmountPaidResource1"></asp:RegularExpressionValidator>
            </div>
        </div>

        <div class="con_login_pnl">
            <div class="loginlbl">
                Bank Name</div>
            <div class="loginControls">
                <asp:TextBox ID="txtBankName" CssClass="textBoxMedium" runat="server" 
                    meta:resourcekey="txtBankNameResource1"></asp:TextBox>
            </div>
        </div>

        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqBankName" 
                    CssClass="lblRequired" ControlToValidate="txtBankName"
                    ErrorMessage="Enter Bank Name." runat="server" Display="Dynamic" 
                    meta:resourcekey="reqBankNameResource1"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="con_login_pnl">
            <div class="loginlbl">
                Payment Date</div>
            <div class="loginControls">
                <asp:TextBox ID="txtPaymentDate" CssClass="textBoxDateTime txtPaymentDate" 
                    runat="server" meta:resourcekey="txtPaymentDateResource1"></asp:TextBox>
            </div>
        </div>

        <div class="con_login_pnl">
            
            <div class="reqlbl btmpadding">
                <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="dynamicButton"
                    OnClick="btnApprove_Click" meta:resourcekey="btnApproveResource1" />
            <asp:HiddenField ID="hdnPassword" runat="server" />
            <asp:HiddenField ID="hdnUserId" runat="server" />
            <asp:HiddenField ID="hdnOrderId" runat="server" />  
            <asp:HiddenField ID="hdnLicenseOpted" runat="server" /> </div> 
            <asp:HiddenField ID ="hdnMode" runat="server" /> 
            <asp:HiddenField ID ="hdnPage" runat="server" /> 
        </div>
        <div class="clear">
        </div>
    </div>

    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var currentTime = new Date();
            var hours = currentTime.getHours();
            var minutes = currentTime.getMinutes();
            $(".txtPaymentDate").datepicker({
                minDate: 0,
                dateFormat: 'dd-M-yy'
            });
        });

        $('.rblPaymentType input').change(function () {
            var value = $('.rblPaymentType input:radio:checked').val();
            if (value == 1) {
               $('#dvChequeNumber :input').attr('disabled', true);
            }
            if (value == 2) {

                $('#dvChequeNumber :input').removeAttr('disabled');
            }

        });



    </script> 

</asp:Content>
