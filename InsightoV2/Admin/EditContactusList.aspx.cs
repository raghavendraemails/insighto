﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Data;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using App_Code;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using Insighto.Exceptions;
using System.Collections;
using CovalenseUtilities.Helpers;
using System.Configuration; 

namespace  Insighto.Admin.Pages
{
    public partial class EditContactusList : System.Web.UI.Page
    {
        #region Private Variables
        int ContactUsID;
        #endregion

        #region Events
        #region Page_Load
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Hashtable ht1 = new Hashtable();
            ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));

            if (ht1.Contains("ContactusId"))
            {
                ContactUsID = ValidationHelper.GetInteger(ht1["ContactusId"].ToString(), 0);

            }
            if (!IsPostBack)
            {
                GetContactUsDetails();
            }
        }
        #endregion
        #endregion

        #region Methods
        #region GetContactUsDetails
        /// <summary>
        /// 
        /// </summary>
        private void GetContactUsDetails()
        {
            var contactService = new ContactUsService();
            var Acontactus = contactService.FindContacusDetailsById(ContactUsID);
            lblUserNameText.Text = Acontactus.USERNAME;
            lblMailMessagetext.Text = Acontactus.MAILMESSAGE;
        }
        #endregion
        #endregion
    }
}