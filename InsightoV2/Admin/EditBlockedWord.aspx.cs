﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Data;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using App_Code;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using Insighto.Exceptions;
using System.Collections;
using CovalenseUtilities.Helpers;
using System.Configuration; 
namespace Insighto.Admin.Pages
{
    public partial class EditBlockedWord : BasePage
    {
        #region private variables
        int BlockId;
        #endregion

        #region Events
        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            Hashtable ht1 = new Hashtable();
            ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));

            if (ht1.Contains("BlockWordId"))
            {
                BlockId = ValidationHelper.GetInteger(ht1["BlockWordId"].ToString(), 0);
                hdnBlockId.Value = BlockId.ToString();
            }
            if (!IsPostBack)
            {
                GetBlockWordDetails();
            }
        }
        #endregion
        #region SaveButtonClikEvent
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            UpdateBlockedWordDetails();
            base.CloseModelForSpecificTime("ManagedBlockedWords.aspx", ConfigurationManager.AppSettings["TimeOut"]);
        }
        #endregion
        #endregion

        #region Methods

        #region GetBlockWordDetails
        /// <summary>
        /// 
        /// </summary>
        public void GetBlockWordDetails()
        {
            var blockService = new BlockedWordsService();
            var ABlockWords = blockService.FindBlockWordById(BlockId);
            txtBlockWord.Text = ABlockWords.BLOCKEDWORD_NAME;
            txtDescription.Text = ABlockWords.BLOCKEDWORD_DESCRIPTION;
        }
        #endregion

        #region UpdateBlockedWordDetails
        public void UpdateBlockedWordDetails()
        {
            var blockService = new BlockedWordsService();
            var result = blockService.UpdateBlockedWord(ValidationHelper.GetInteger(hdnBlockId.Value, 0), txtBlockWord.Text.Trim(), txtDescription.Text.Trim());
            if (result != null)
            {
                dvSuccessMsg.Visible = true;
            }
        }
        #endregion

        #endregion
    }
}