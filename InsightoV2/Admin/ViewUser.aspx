﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="ViewUser.aspx.cs" Inherits="Admin_ViewUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                View User Profile Information</h3>
        </div>
        <%--   <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(false,'');">
                &nbsp;</a>
        </div>--%>
    </div>
    <div class="defaultHeight"> </div>
    <div class="popupContentPanel">
     <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccessMsg" runat="server" Text="User activated successfully."></asp:Label></div>
        </div>
        <div class="adminFormPanel01">
        <table border="0">
        <tr >
        <td>
        <div class="loginlbl">
                    <asp:Label ID="lblFirstName" runat="server" Text="First Name" />
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <asp:TextBox ID="txtFirstName" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        <td>
        <div class="loginlbl" style="width:101px; height: 23px;">
                    <asp:Label ID="lblIPAddress" runat="server" Text="IP" />
                </div>
        </td>
        <td>
          <div class="loginControls">
                    <asp:TextBox ID="txtIPAddress" style="width:150px;" Enabled="false" 
                        runat="server" CssClass="textBoxMedium" 
                         />
                </div>
        </td>
        <td>
        <asp:Button ID="btnChkIPAddress" Text="Check IP" runat="server" style="margin-top:2px; padding:0;"
                    OnClick="btnChkIPAddress_Click" Height="29px" CssClass="btn_small" />
        </td>
        </tr>
        <tr>
        <td>
        <div class="loginlbl">
                    <asp:Label ID="lblLastName" runat="server" Text="Last Name"></asp:Label>
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <asp:TextBox ID="txtLastName" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        <td>
        <div class="loginlbl" style="width:98px; height: 23px;" id="lblCityCountry">
                    &nbsp;IP City/Country</div>
        </td>
        <td>
         <asp:TextBox ID="txtIPCityCountry" style="width:150px;" Enabled="false" 
                    runat="server" CssClass="textBoxMedium" />
        </td>
        <td>
        
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl">
                    <asp:Label ID="lblAddress1" runat="server" Text="Address1" />
                </div>
        </td>
        <td>
        <div class="loginControls">
                    <asp:TextBox ID="txtAddress1" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        <td>
         
        </td>
        <td >
        <asp:HyperLink ID="urlMaxMindAccuracy" NavigateUrl="http://www.maxmind.com/en/geolite_city_accuracy" runat="server">Click to check accuracy</asp:HyperLink>
        </td>
        <td>
        
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl">
                    <asp:Label ID="lblAddress2" runat="server" Text="Address2" />
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <asp:TextBox ID="txtAddress2" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        <td>
        
        </td>
        <td>
         <asp:Button ID="btnDashBoard" runat="server" CssClass="dynamicButton" 
                      OnClick="btnDashBoard_Click" Text="Customer Dashboard" 
                      ToolTip="Customer Dashboard" />
        </td>
        <td>
        
        </td>
        </tr>
        <tr>
        <td>
        <div class="loginlbl">
                    <asp:Label ID="lblCity" runat="server" Text="City" />
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <asp:TextBox ID="txtCity" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        <td>
        
        </td>
        <td>
        <asp:Button ID="btncustomeraccount" runat="server" CssClass="dynamicButton" 
                      Text="Customer Account" 
                      ToolTip="Customer Dashboard" onclick="btncustomeraccount_Click" />
        </td>
        <td>
        </td>
        </tr>

        <tr>
        <td>
        <div class="loginlbl">
                    <asp:Label ID="lblState" runat="server" Text="State" />
                </div>
        </td>
        <td>
        <div class="loginControls">
                    <asp:TextBox ID="txtState" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        <td colspan="3" style="width:100%;">
        <div style="border: 1px solid;">
            <table cellpadding="2" cellspacing="2" style="margin-left:10px;">
                <tr>
                    <td>Select license type</td>
                    <td>
                        <asp:DropDownList runat="server" ID="creditlicensetype">
                            <asp:ListItem Text="Pro Single" Value="PPS_PRO"></asp:ListItem>
                            <asp:ListItem Text="Premium Single" Value="PPS_PREMIUM"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td rowspan="2">
                        <asp:Button ID="btnAddCredits" runat="server" CssClass="dynamicButton" Text="Add Credits" onclick="btnAddCredits_Click" />
                    </td>
                </tr>
                <tr>
                    <td>Select credits</td>
                    <td>
                        <asp:DropDownList runat="server" ID="creditscount">
                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                   <td colspan="3" style="text-align:center;color:red;">
                       <asp:Label ID="addcreditstatus" runat="server"></asp:Label>
                   </td>
                </tr>
            </table>
        </div>
        
        </td>
      <%--  <td>
        
        </td>--%>
        <td>
        
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl">
                    <asp:Label ID="lblCountry" runat="server" Text="Country" />
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <asp:TextBox ID="txtCountry" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl">
                    <asp:Label ID="lblPostCode" runat="server" Text="Post Code" />
                </div>
        </td>
        <td>
        <div class="loginControls">
                    <asp:TextBox ID="txtPostCode" Enabled="false" runat="server" CssClass="textBoxDateTime" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
        <div class="loginlbl">
                    <asp:Label ID="lblEmailId" runat="server" Text="Email Id" />
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <asp:TextBox ID="txtEmailId" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl">
                    <asp:Label ID="lblPhoneNo" runat="server" Text="Phone Number" />
                </div>
        </td>
        <td>
           <div class="loginControls">
                    <asp:TextBox ID="txtPhone" Enabled="false" runat="server" CssClass="phoneCountry" />
                    <asp:TextBox ID="txtPhone1" Enabled="false" runat="server" CssClass="phoneArea" />
                    <asp:TextBox ID="txtPhone2" Enabled="false" runat="server" CssClass="phoneNumber" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl">
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <label for="phoneText">
                    </label>
                    <span id="phoneText" class="note">Country Code - Area Code - Phone Number.</span>
                </div>
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl"> <asp:Label ID="lblCompany" runat="server" Text="Company" />
                </div>
        </td>
        <td>
        <div class="loginControls">
                    <asp:TextBox ID="txtCompany" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl"> <asp:Label ID="lblWorkIndustry" runat="server" Text="Work Industry" />
                </div>
        </td>
        <td>
        <div class="loginControls">
                    <asp:TextBox ID="txtWorkIndustry" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl">  <asp:Label ID="lblJobFunction" runat="server" Text="Job Function" />
                </div>
        </td>
        <td>
        <div class="loginControls">
                    <asp:TextBox ID="txtJobFunction" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
        <div class="loginlbl"> <asp:Label ID="lblCompanySize" runat="server" Text="Company Size" />
                </div>
        </td>
        <td>
        <div class="loginControls">
                    <asp:TextBox ID="txtCompanySize" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
        <div class="loginlbl"> <asp:Label ID="lblDepartment" runat="server" Text="Department" />
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <asp:TextBox ID="txtDepartment" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl">  <asp:Label ID="lblAccountType" runat="server" Text="Account Type" />
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <asp:TextBox ID="txtAccountType" Enabled="false" runat="server" CssClass="textBoxDateTime" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
        <div class="loginlbl"> <asp:Label ID="lblLicenseType" runat="server" Text="License Type" />
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <asp:TextBox ID="txtLicenseType" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl">  <asp:Label ID="lblLicenseExpiryDate" runat="server" Text="License Expiry Date" />
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <asp:TextBox ID="txtLicenseExpiryDate" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl"> <asp:Label ID="lblCreatedDate" runat="server" Text="Created Date" />
                </div>
        </td>
        <td>
         <div class="loginControls">
                    <asp:TextBox ID="txtCreatedDate" Enabled="false" runat="server" CssClass="textBoxMedium" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
         <div class="loginlbl"> <asp:Label ID="lblStatus" runat="server" Text="Status" />
                </div>
        </td>
        <td>
        <div class="loginControls"> 
                    <asp:TextBox ID="txtStatus" Enabled="false" runat="server" CssClass="textBoxSmall" />
                </div>
        </td>
        </tr>
        <tr>
        <td>
        &nbsp;
        </td>
        </tr>
        <tr>
        <td>
        
        </td>
        <td>
        <div class="bottomButtonPanel" align="center">
            <asp:Button ID="btnDeactive" Text="Deactivate" CssClass="dynamicButton" runat="server"
                OnClick="btnDeactive_Click" />
           
        </div>
        </td>
        </tr>
        </table>

           
            
            
        
          
           
           
          
        </div>
      
      
        <asp:HiddenField ID="hdnUserId" runat="server" />
    </div>
    <script type="text/javascript">
        var firstName = $(".txtFirstName").val();
    </script>
</asp:Content>
