﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Web.Script.Serialization;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
namespace Insighto.Admin.Pages
{
    public partial class UserDashboard : System.Web.UI.Page
    {
        Hashtable ht = new Hashtable();
        #region Events
        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);

                if (ht != null && ht.Count > 0 && ht.Contains("Id"))
                {
                    hdnUserId.Value = ht["Id"].ToString();
                }
            }
        }
        #endregion
        #endregion
    }
}