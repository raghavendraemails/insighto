﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Data;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using App_Code;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using Insighto.Exceptions;
using System.Collections;
using CovalenseUtilities.Helpers;
using System.Net.Mail;
using System.Configuration;
using System.IO;

public partial class Admin_NewsLetters : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginAdminUserDetailsSession();
            var userservice = ServiceFactory.GetService<UsersService>();
            var adminTrackingService = ServiceFactory.GetService<AdminTrackingServices>();
            var adminTrackInfo = new osm_admintracking();
            List<string> attchments = new List<string>();
            var emaliIds = string.Empty;
            var fromEmail = txtFrom.Text;
            var subject = txtSubject.Text;
            var MsgBody = "";

            if (drpUserType.SelectedIndex != -1)
            {
                if (drpUserType.SelectedIndex > 0)
                {
                    var userEmailId = userservice.GetUserEmailIdsByType(drpUserType.SelectedItem.Text);

                    if (userEmailId != null)
                    {
                        foreach (var User in userEmailId)
                        {
                            emaliIds = emaliIds + User.LOGIN_NAME + ",";
                        }
                    }
                }
            }

            int len = 0;
            // Get the HttpFileCollection
            HttpFileCollection hfc = Request.Files;
            for (int i = 0; i < hfc.Count; i++)
            {
                HttpPostedFile hpf = hfc[i];
                if (hpf.ContentLength > 0)
                {
                    len += hpf.ContentLength;
                }
            }


            //FileSize less than 2MB
            if (len <= ValidationHelper.GetInteger(ConfigurationManager.AppSettings["FileSize"].ToString(), 0))
            {
                //emaliIds = emaliIds + txtAdditonalEmailID.InnerText;
                if (txtAdditonalEmailID.Text.Length > 0)
                {
                    emaliIds = emaliIds + txtAdditonalEmailID.Text.Trim() + "," ;
                }
                if (emaliIds != "")
                {
                   emaliIds = emaliIds.Substring(0, emaliIds.Length - 1);
                   MsgBody = EditorQuestionIntro.Value;
                
                    // Get the HttpFileCollection
                    // hfc = Request.Files;
                    for (int i = 0; i < hfc.Count; i++)
                    {
                        HttpPostedFile hpf = hfc[i];

                        if (hpf.ContentLength > 0)
                        {
                            if (!Directory.Exists(Server.MapPath("~/adminfiles")))
                            {
                                Directory.CreateDirectory(Server.MapPath("~/adminfiles"));
                            }
                            hpf.SaveAs(Server.MapPath("~/adminfiles") + "\\" + System.IO.Path.GetFileName(hpf.FileName));
                            attchments.Add(Server.MapPath("~/adminfiles") + "\\" + System.IO.Path.GetFileName(hpf.FileName));
                        }
                    }

                    string toEmail = ConfigurationManager.AppSettings["emailActivation"];
                    MailHelper.SendMailMessage(fromEmail, toEmail, emaliIds, "", subject, MsgBody, attchments);
                    string tracking_desc = "";
                    tracking_desc = "News Letters has been Sent By " + userInfo.FirstName + " " + userInfo.LastName;
                    adminTrackInfo.ADMINID = ValidationHelper.GetInteger(userInfo.AdminUserId, 0);
                    adminTrackInfo.ACTION_DONE_ON_PAGE = "Sending News Letter";
                    adminTrackInfo.EVENT_ACTION_DESCRIPTION = tracking_desc;
                    adminTrackInfo.CREATED_ON = DateTime.UtcNow;
                    adminTrackInfo.FKUSERID = 0;
                    var resultTrack = adminTrackingService.InsertAdminTracking(adminTrackInfo);
                    dvSuccessMsg.Visible = true;
                    dvErrMsg.Visible = false;
                }
                else
                {
                    dvSuccessMsg.Visible = false;
                    dvErrMsg.Visible = true;
                    lblErrorMsg.Text = "No email Id exist to send mail.";
                }
            }
            else
            {
                lblErrorMsg.Text = "Total attachment(s) size exceeded than " + ConfigurationManager.AppSettings["FileSize"].ToString();
                dvSuccessMsg.Visible = false;
                dvErrMsg.Visible = true;
            }

        }
    }

    protected void custValidateText(object source, ServerValidateEventArgs args)
    {
        //string textLength = value.Length >40;
        if (EditorQuestionIntro.Value.Length > 0)
            args.IsValid = true;
        else
            args.IsValid = false;// field is empty         
    }
    protected void custValidatAdditionalEmail(object source, ServerValidateEventArgs args)
    {
        //string textLength = value.Length >40;
        if (drpUserType.SelectedValue =="Select")
            args.IsValid = false;
        else
            args.IsValid = true;// field is empty         
    }

}