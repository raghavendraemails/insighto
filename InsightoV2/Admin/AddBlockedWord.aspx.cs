﻿using System;
using Insighto.Data;
using Insighto.Business.Services;
using System.Configuration;
namespace Insighto.Admin.Pages
{
    public partial class AddBlockedWord : BasePage
    {
        #region Events
        #region Page_Load
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region SaveButton Click Event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            AddBlockedWordDetails();

        }
        #endregion
        #endregion

        #region Methods
        public void AddBlockedWordDetails()
        {
            var blockedService = new BlockedWordsService();
            var blockedServiceList = new osm_blockedwords();
            blockedServiceList.BLOCKEDWORD_NAME = txtBlockWord.Text.Trim();
            blockedServiceList.BLOCKEDWORD_DESCRIPTION = txtDescription.Text.Trim();
            blockedServiceList.DELETED = 0;
            var result = blockedService.InsertBlockedWord(blockedServiceList);
            if (result > 0)
            {
                dvSuccessMsg.Visible = true;
                dvErrMsg.Visible = false;
                base.CloseModelForSpecificTime("ManagedBlockedWords.aspx", ConfigurationManager.AppSettings["TimeOut"]);
            }
            else
            {
                dvSuccessMsg.Visible = false;
                dvErrMsg.Visible = true;
            }
        }
        #endregion
    }
}