﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="Notifications.aspx.cs" Inherits="Admin_Notifications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <span>Add Server Notification Text Message </span>
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    <div class="contentPanel">
        <p>
            &nbsp;</p>
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccessMsg" runat="server" Text="Notification updated successfully."></asp:Label>
            </div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrorMsg" runat="server"></asp:Label></div>
        </div>
        <%--    <div class="lt_padding">
                                <asp:Label ID="requiredfield" CssClass="left_requirelbl" Text="indicates required fields"
                                    runat="server"></asp:Label></div>--%>
        <div class="con_login_pnl">
            <div class="loginControls">
                <div class="informationPanelDefault" style="width: 635px">
                    <div>
                        Text Message Added here will be displayed as Alert on MyInsighto Page(For Not displaying
                        any message enter the text message has blank and Update.
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="con_login_pnl">
                <div class="loginContorls">
                    <table>
                        <tr>
                            
                        </tr>
                        <tr>
                         
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginControls">
                <asp:TextBox ID="txtMessage" CssClass="textAreaMedium" Rows="4" Columns="400" TextMode="MultiLine" MaxLength="100"
                    Width="635" runat="server"></asp:TextBox>
            </div>
        </div>
        <div>
            <div class="con_login_pnl">
                <div class="loginContorls">
                    <table>
                        <tr>
                            <td>
                                <table border="0" cellpadding="1" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblDate" runat="server" AssociatedControlID="txtDateTime" Text="From Date"  />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDateTime" runat="server"    CssClass="textBoxDateTime txtDateTime launchdate" />
                                        </td>
                                        <td>
                                            <a id="lnkStartDateClear" class="cleartext" href='javascript:void(0)' title="Clear">
                                                Clear</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                            <td>
                                <table border="0" cellpadding="1" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblSurveyClosingDate" runat="server" AssociatedControlID="txtClosingDateTime" 
                                                Text="To Date"/>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtClosingDateTime" runat="server" CssClass="textBoxDateTime txtDateTime closeDate" 
                                                 />
                                        </td>
                                        <td>
                                            <a id="lnkCloseDateClear" class="cleartext" href='javascript:void(0)' title="Clear">
                                                Clear</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               
                                
                            </td>
                               <td>
                                <asp:CustomValidator SetFocusOnError="True" ID="customClosingDate" runat="server"
                                    ControlToValidate="txtClosingDateTime" Display="Dynamic"
                                     OnServerValidate="ValidateDuration"
                                    CssClass="lblRequired" ErrorMessage="To date should be greater than from date." ></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="con_login_pnl">
            <div class="loginControls">
                <p class="defaultHeight">
                </p>
                <asp:Button ID="btnUpdate" CssClass="dynamicButton" runat="server" OnClick="btnUpdate_Click"
                    Text="Update" />
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var currentTime = new Date();
            var hours = currentTime.getHours();
            var minutes = currentTime.getMinutes();
            $('.txtDateTime').datetimepicker({
                minDate: currentTime,
                dateFormat: 'dd-M-yy',
                showOn: "button",
                buttonImage: "../App_Themes/Classic/Images/icon-calander.gif",
                buttonImageOnly: true,
                buttonText: "Calendar"
                //hourMin: hours,
                //minuteMin: minutes
            });

            //            $(".txtRemaiderDate").datepicker({
            //                minDate: 0,
            //                showOn: "button",
            //                buttonImage: "App_Themes/Classic/Images/icon-calander.gif",
            //                buttonImageOnly: true,
            //                dateFormat: 'dd-M-yy',
            //                buttonText: "Calendar"
            //            });
        });

        $('#lnkStartDateClear').click(function () {
            $('.launchdate').val('');
        });
        $('#lnkCloseDateClear').click(function () {
            $('.closeDate').val('');
        });
          
    </script>
</asp:Content>
