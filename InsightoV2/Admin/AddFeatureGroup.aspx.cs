﻿using System;
using System.Web.UI;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using System.Configuration;
using App_Code;

namespace Insighto.Pages.Admin
{
    public partial class AddFeatureGroup : BasePage
    {
        Hashtable ht = new Hashtable();
        #region Events
        #region PageLoad Event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);

                    if (ht != null && ht.Count > 0 && ht.Contains("FeatureID"))
                    {
                        GetFeatureDetailsById(ValidationHelper.GetInteger(ht["FeatureID"].ToString(), 0));
                        //btnCancel.Visible = true;
                        btnUpdate.Visible = true;
                        btnSave.Visible = false;
                        lblFeatureGroup.Visible = false;
                        txtFeatureGroup.Visible = false;
                        txtFeatureName.Enabled = false;
                        // = ValidationHelper.GetInteger(ht["FeatureId"].ToString(), 0);
                    }
                    if (ht != null && ht.Count > 0 && ht.Contains("FeatureGroup") && ht.Contains("FeatureID")==false)
                    {
                        txtFeatureGroup.Text = lblTitle.Text = ht["FeatureGroup"].ToString();
                        //btnCancel.Visible = false;
                        btnUpdate.Visible = false;
                        btnSave.Visible = true;
                        lblFeatureGroup.Visible = false;
                        txtFeatureGroup.Visible = false;
                    }
                }
            }
        }
        #endregion

        #region Save Button event 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveFeature("Save");
            }
        }
        #endregion

        #region Update button click Event
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveFeature("Update");
            }
        }
        #endregion
        #endregion

        #region Methods

        #region GetFeatureDetailsById
        /// <summary>
        /// 
        /// </summary>
        /// <param name="featureId"></param>
        private void GetFeatureDetailsById(int featureId)
        {
            var featureService = ServiceFactory.GetService<FeatureService>();
            var features = featureService.GetFeaturesById(featureId);
            if (features != null)
            {
                lblTitle.Text = features.FEATURE_GROUP;
                txtFeatureGroup.Text = features.FEATURE_GROUP;
                txtFeatureName.Text = features.FEATURE_NAME;
                txtFree.Text = features.FREE.ToString();
                txtProMonthly.Text = features.PRO_MONTHLY.ToString();
                txtProQuarterly.Text = features.PRO_QUARTERLY.ToString();
                txtProYearly.Text = features.PRO_YEARLY.ToString();
                txtPreimiumMonthly.Text = features.PREMIUM_MONTHLY.ToString();
                txtPremiumQuarterly.Text = features.PREMIUM_QUARTERLY.ToString();
                txtPremiumYearly.Text = features.PREMIUM_YEARLY.ToString();
            }
        }
        #endregion

        #region SaveFeature
        /// <summary>
        /// 
        /// </summary>
        /// <param name="saveType"></param>
        private void SaveFeature(string saveType)
        {
            int rtnValue = 0;
            var features = new osm_features();
            features.FEATURE_NAME = txtFeatureName.Text;
            features.FREE = ValidationHelper.GetInteger(txtFree.Text, 0);
            features.PRO_MONTHLY = ValidationHelper.GetInteger(txtProMonthly.Text, 0);
            features.PRO_QUARTERLY = ValidationHelper.GetInteger(txtProQuarterly.Text, 0);
            features.PRO_YEARLY = ValidationHelper.GetInteger(txtProYearly.Text, 0);
            features.PREMIUM_MONTHLY = ValidationHelper.GetInteger(txtPreimiumMonthly.Text, 0);
            features.PREMIUM_QUARTERLY = ValidationHelper.GetInteger(txtPremiumQuarterly.Text, 0);
            features.PREMIUM_YEARLY = ValidationHelper.GetInteger(txtPremiumYearly.Text, 0);
            features.FEATURE_GROUP = txtFeatureGroup.Text;
            
            features.DELETED = 0;
            features.FEATURE = txtFeatureName.Text.Replace(" ", "_");
            var featureService = ServiceFactory.GetService<FeatureService>();
            if (saveType == "Save")
            {
                features.FEATURE_TYPE_FLAG = 1;
                rtnValue = featureService.SaveFeatures(features);
                lblSuccessMsg.Text = Utilities.ResourceMessage("AddFeatureGroup_Success_Saved"); //"Feature has been created succesfully.";
            }
            else
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("FeatureID"))
                {
                    features.PK_FEATURE_ID = ValidationHelper.GetInteger(ht["FeatureID"].ToString(), 0);
                }
                rtnValue = featureService.UpdateFeatures(features);
                lblSuccessMsg.Text = Utilities.ResourceMessage("AddFeatureGroup_Success_Updated"); //"Feature updated successfully.";
            }
            if (rtnValue < 1)
            {
                dvErrMsg.Visible = true;
                dvSuccessMsg.Visible = false;
            }
            else
            {
                dvSuccessMsg.Visible = true;
                dvErrMsg.Visible = false;
                base.CloseModelForSpecificTime("LicenseManagement.aspx?featureGroup=" + ht["FeatureGroup"].ToString(), ConfigurationManager.AppSettings["TimeOut"]);
            }

        }
        #endregion
        #endregion

    }
}