﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using App_Code;
public partial class Admin_ContentManagement : System.Web.UI.Page
{

    public string _pageName
    {
        get
        {
            Hashtable ht = new Hashtable();
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Contains("Page"))
                    return ht["Page"].ToString();
            }
            return "";
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Contains("Page") && ht.Contains("Title"))
            {
                txtPageName.Text = _pageName.Replace("_", "&");
                divHeading.InnerHtml = ht["Title"].ToString().Replace("_", " & ");
               // _pageName = ht["Page"].ToString();
                var pageContent = ServiceFactory.GetService<HelpPageService>().GetHelpPageContent(ht["Page"].ToString().Replace("_", "&"));
                if (pageContent != null)
                {
                    EditorQuestionIntro.Value = pageContent.PageContent;
                }
                else
                {
                    EditorQuestionIntro.Value = "";
                }
            }
            else
            {
                Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetContentManagementURL(), "Page=AboutUs&Title=About Us"));
            }
            hlFAQContent.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetFAQContentManagementURL(), "Page=FAQ");  
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        osm_HelpPage helpPages = new osm_HelpPage();
        helpPages.PageName = _pageName.Replace("_","&");
        helpPages.PageContent = EditorQuestionIntro.Value;
        helpPages.ParentId = 0;
        helpPages.Order = 0;
        helpPages.CreatedDate = DateTime.UtcNow;
        helpPages.Category = "";
           helpPages= ServiceFactory.GetService<HelpPageService>().Update(helpPages);
           if (helpPages != null)
           {
               dvErrMsg.Visible = false;
               lblMessage.Text = Utilities.ResourceMessage("CMS_Success_Update");//"CMS content updated successfully.";
               sucessMsg.Visible = true;
           }
           else
           {
               lblErrMsg.Text = Utilities.ResourceMessage("CMS_Failed_Update");//"CMS content updation failed.";
               dvErrMsg.Visible = true;          
               sucessMsg.Visible = false;
           }
    }

    protected void btnCancel_Click(object sender,EventArgs e)
    {
          Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Contains("Page") && ht.Contains("Title"))
            {
                txtPageName.Text = _pageName.Replace("_", "&");
                divHeading.InnerHtml = ht["Title"].ToString().Replace("_", " & ");
                // _pageName = ht["Page"].ToString();
                var pageContent = ServiceFactory.GetService<HelpPageService>().GetHelpPageContent(ht["Page"].ToString().Replace("_", "&"));
                if (pageContent != null)
                {
                    EditorQuestionIntro.Value = pageContent.PageContent;
                }
            }
            else
            {
                EditorQuestionIntro.Value = "";
            }
    }


}