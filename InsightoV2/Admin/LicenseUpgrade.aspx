﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="LicenseUpgrade.aspx.cs" Inherits="Admin_LicenseUpgrade" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->   
             <asp:Label ID="lblTitle" runat="server" Text="Payment Pending Users" 
                meta:resourcekey="lblTitleResource1"></asp:Label>            
            <!-- //page title -->        
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="errorPanel" id="recordDeleted" style="display: none;">
            <span class="successMesg">
                <asp:Label ID="lblDeleted" runat="server" 
                Text="Record deleted successfully" meta:resourcekey="lblDeletedResource1"></asp:Label></span>
        </div>
        <div class="surveyCreateTextPanel">
            <!-- admin content panel -->
            <div class="adminGridSearchPanel">
                <div class="searchLblelong">
                    <asp:Label ID="lblPendingUsers" runat="server" 
                        Text="List of Users Pending For License Upgrade" 
                        meta:resourcekey="lblPendingUsersResource1"></asp:Label>
                </div>
                <div class="searchrghtPanel">
                    <asp:HyperLink ID="hlnkAddList" CssClass="dynamicHyperLink" NavigateUrl="ViewPaymentPendingUsers.aspx"
                        ToolTip="Payment Pending Users" runat="server" 
                        Text="Payment Pending Users" meta:resourcekey="hlnkAddListResource1"></asp:HyperLink>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="adminUserGridViewPanel" style="display: block;">
                <div id="ptoolbar">
                </div>
                <table id="tblAddressBook">
                </table>
            </div>
            <!-- //admin content panel -->
        </div>
        <div class="clear">
        </div>
    </div>
    <script type="text/javascript">

        var serviceUrl = '../AjaxService.aspx';
        $(document).ready(function () {
            $("#tblAddressBook").jqGrid({
                url: serviceUrl,
                postData: { method: "FindPaymentPendingUsers", paymentType: "Upgrade" },
                datatype: 'json',
                colNames: ['Name', 'Email Id', 'Telephone', 'License Type', 'License To Be Upgraded', 'Upgrade'],
                colModel: [
                { name: 'FIRST_NAME', index: 'FIRST_NAME', width: 35, resizable: false },
                { name: 'LOGIN_NAME', index: 'LOGIN_NAME', width: 80, align: 'center', resizable: false },
                { name: 'PHONE', index: 'PHONE', width: 50, align: 'center' },
                { name: 'LICENSE_TYPE', index: 'LICENSE_TYPE', width: 30, align: 'center', resizable: false },
                { name: 'LICENSE_UPGRADE', index: 'LICENSE_UPGRADE', width: 70, align: 'center', resizable: false },
                { name: 'UpgradeLinkUrl', index: 'UpgradeLinkUrl', width: 25, align: 'center', resizable: false, formatter: UpgradeLinkFormatter, sortable: false },
                 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'FIRST_NAME',
                sortorder: "asc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 880,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblAddressBook").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
            $("#tblAddressBook").jqGrid('setLabel', 'FIRST_NAME', '', 'textalignleft');
        });


        function UpgradeLinkFormatter(cellvalue, options, rowObject) {
            var finalcellvalue = cellvalue + "&Page=L"
            return "<a href='" + finalcellvalue + "' rel='framebox'rel='framebox' w='760' h='480' >Upgrade</a>";
        }
    </script>
</asp:Content>
