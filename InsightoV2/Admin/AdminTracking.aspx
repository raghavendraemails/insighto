﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AdminTracking.aspx.cs" Inherits="Admin_AdminTracking" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text="View Admin Tracking Details" 
                meta:resourcekey="lblTitleResource1"></asp:Label>
          
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
       
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="errorPanel" id="recordDeleted" style="display: none;">
            <span class="successMesg">
                <asp:Label ID="lblSuccessDelete" runat="server" 
                Text="Record deleted successfully" meta:resourcekey="lblSuccessDeleteResource1"></asp:Label></span>
        </div>
        <div class="surveyCreateTextPanel">
            <!-- admin content panel -->
            <div class="adminGridSearchPanel">
                <div class="searchLble">
                </div>
                <div class="searchtxtcontrols">
                </div>
                <div class="searchrghtPanel">
                    <asp:HyperLink ID="hlnkAddList" CssClass="dynamicHyperLink" NavigateUrl="ManageAdmin.aspx"
                        ToolTip="Back" runat="server" Text="Back" 
                        meta:resourcekey="hlnkAddListResource1"></asp:HyperLink>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="adminUserGridViewPanel" style="display: block;">
                <div id="ptoolbar">
                </div>
                <table id="tblAddressBook">
                </table>
            </div>
            <!-- //admin content panel -->
        </div>
        <div class="clear">
        </div>
    </div>
    <script type="text/javascript">
        var serviceUrl = '../AjaxService.aspx';
        $(document).ready(function () {
            $("#tblAddressBook").jqGrid({
                url: serviceUrl,
                postData: { method: "FindAdminTracking" },
                datatype: 'json',
                colNames: ['Tracking ID', 'Email Address', 'Admin Name', 'Event Date', 'Event Action Type', 'Event Description'],
                colModel: [
                { name: 'TRACKINGID', index: 'TRACKINGID', width: 30, align: 'center', resizable: false },
                { name: 'LOGIN_NAME', index: 'LOGIN_NAME', width: 50, align: 'center', resizable: false },
                { name: 'NAME', index: 'NAME', width: 50, align: 'center', resizable: false },
                { name: 'CREATED_ON', index: 'CREATED_ON', width: 50, resizable: false, align: 'center' },
                { name: 'ACTION_DONE_ON_PAGE', index: 'ACTION_DONE_ON_PAGE', width: 50, align: 'center', resizable: false },
                { name: 'EVENT_ACTION_DESCRIPTION', index: 'EVENT_ACTION_DESCRIPTION', width: 60, align: 'center', resizable: false }

                 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'CREATED_ON',
                sortorder: "desc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 880,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblAddressBook").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false,refresh:false });
            $("#tblAddressBook").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');
        });



        function DeleteLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='#' class='button-small-gap' id='lnkDelete' onclick='javascript: return DeleteContact(" + cellvalue + ");'>Reset Pasword</a>";
        }

        function gridReload() {
            var keyword = $("#txtKeyword").val();
            $("#tblAddressBook").jqGrid('setGridParam', { url: serviceUrl + "?keyword=" + keyword, page: 1 }).trigger("reloadGrid");
        }


        function DeleteContact(ContactListId) {
            var answer = confirm('Are you sure you want to delete ?');

            if (answer) {
                $.post("AjaxService.aspx", { "method": "DeleteContactByContactListId", "Id": ContactListId },
                  function (result) {
                      if (result == '1') {


                          var status = $('#hdnStatus').val();
                          gridReload(status);
                      }

                  });
            }
        }

        function ndateFormatter(cellval, opts, rwdat, _act) {

            if (cellval != "" && cellval != null) {
                var time = cellval.replace(/\/Date\(([0-9]*)\)\//, '$1');
                var date = new Date();
                date.setTime(time);
                return date.toDateString();
            } else return "-";
        }
      
    </script>
</asp:Content>
