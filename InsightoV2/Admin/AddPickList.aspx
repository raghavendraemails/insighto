﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="AddPickList.aspx.cs" Inherits="Insighto.Pages.Admin.AddPickList" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" Runat="Server">
<div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
               <h3><asp:Label runat="server" ID="lblCreate" Text="ADD ITEM" Visible="False" ></asp:Label>
               <asp:Label runat="server" ID="lblEdit" Text="EDIT ITEM" Visible="False" ></asp:Label> </h3>
            
        </div>
      <%--  <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick ="closeModalSelf(true,'PickListManagement.aspx');" title="Close">&nbsp;</a>
        </div>--%>
       
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="popupContentPanel" >
        <!-- popup content panel -->
           <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                <div><asp:Label ID="lblSuccessMsg"  runat="server" 
                    Text="Pick list item has been created succesfully." 
                        meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
           </div>
           <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">                
                <div>
                    <asp:Label ID="lblErrMsg"  runat="server" 
                    Text="Picklist already exist." meta:resourcekey="lblErrMsgResource1" ></asp:Label></div>
            </div>
        <div class="formPanel">
            <!-- form -->
            <div class="forgotpanel">
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblparameter" runat="server" Text="Parameter" 
                            meta:resourcekey="lblparameterResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtParameter"  runat="server" CssClass="textBoxMedium" 
                            MaxLength="25" meta:resourcekey="txtParameterResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqList" 
                            ControlToValidate="txtParameter" CssClass="lblRequired"
                            ErrorMessage="Please enter parameter." runat="server" Display="Dynamic" 
                            meta:resourcekey="reqListResource1"></asp:RequiredFieldValidator>
                    </div>
                </div>        
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblParamValue" runat="server" Text="Parameter Value" 
                            meta:resourcekey="lblParamValueResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtParamValue" Enabled="False" runat="server" CssClass="textBoxMedium" 
                            MaxLength="25" meta:resourcekey="txtParamValueResource1" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                      <%--  <asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator1" ControlToValidate="txtParamValue" CssClass="lblRequired"
                            ErrorMessage="Please enter param value." runat="server" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>          
                  <div class="con_login_pnl">
                    <div class="loginlbl"> <asp:Label ID="lblbtnNewFolder" runat="server" AssociatedControlID="btnSave" meta:resourcekey="lblbtnNewFolderResource1" />
                    </div>
                    <div class="loginControls"> <asp:Button ID="btnSave" runat="server" CssClass="btn_small_65" Text="Save"
                            ToolTip="Save"  meta:resourcekey="ibtnSaveResource1" 
                            onclick="btnSave_Click" />

                             <asp:Button ID="btnUpdate" runat="server" CssClass="btn_small_65" Text="Update"
                            ToolTip="Update"  meta:resourcekey="ibtnUpdateResource1" Visible="False"
                            onclick="btnUpdate_Click" />
                             <asp:Button ID="btnCancel" runat="server" CssClass="btn_small_65" Text="Cancel"
                            ToolTip="Cancel"  meta:resourcekey="ibtnCancelResource1"  Visible="False"
                            onclick="btnCancel_Click" />
                    </div>
                </div>
               <div class="con_login_pnl" id="successMessage">
                    <div class="reqlbl btmpadding">
                        <asp:Label ID="lbl" runat="server" meta:resourcekey="lblResource1"></asp:Label>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            
            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>
    <script type="text/javascript">
        function fnUpdateValue() {
           
            var paramText = document.getElementById('<%=txtParameter.ClientID%>').value;
            document.getElementById('<%=txtParamValue.ClientID%>').value = paramText;

        }
    
    </script>
</asp:Content>

