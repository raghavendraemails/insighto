﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
namespace Insighto.Pages.Admin
{
    public partial class LicenseManagement : BasePage
    {
        private string _FeatureGroup = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFeatureGroups();
                
                if (Request.QueryString["FeatureGroup"] != null)
                {
                    hdnGroupName.Value = Request.QueryString["FeatureGroup"].ToString();

                  foreach (TreeNode node in tvLicenseManagement.Nodes)
                  {
                      if (node.Value == hdnGroupName.Value)
                      {
                          node.Selected = true;
                          tvLicenseManagement.SelectedNodeStyle.CssClass = "active";
                          break; 
                      }
                  }

                }
                lblGroupName.Text = hdnGroupName.Value;
                string url = EncryptHelper.EncryptQuerystring("AddFeatureGroup.aspx", "FeatureGroup=" + hdnGroupName.Value);
                hlnkAddList.NavigateUrl = url;   
            }
          
        }

        public string FeatureGroup
        {
            get {
                _FeatureGroup = hdnGroupName.Value;
                return _FeatureGroup;
            }
        }

        private void  BindFeatureGroups()
        {
            var featureService = ServiceFactory.GetService<FeatureService>();
            var features = featureService.GetFeaturesGroups();           
            TreeNode parentNode = null;
          
            foreach (var item in features)
            {             
                parentNode = new TreeNode(item.FeatureGroup, item.FeatureGroup);
                if (hdnGroupName.Value == "")
                {
                    hdnGroupName.Value = item.FeatureGroup;
                }
                if (item.FeatureGroup != null)
                {
                    parentNode.Collapse();
                    tvLicenseManagement.Nodes.Add(parentNode);
                }
            }            
        }

        protected void tvLicenseManagement_SelectedNodeChanged(object sender, EventArgs e)
        {           
            lblGroupName.Text = tvLicenseManagement.SelectedNode.Value;
            hdnGroupName.Value = tvLicenseManagement.SelectedNode.Value;
            tvLicenseManagement.SelectedNodeStyle.CssClass = "active";
            string url = EncryptHelper.EncryptQuerystring("AddFeatureGroup.aspx", "FeatureGroup=" + tvLicenseManagement.SelectedNode.Value);
            hlnkAddList.NavigateUrl = url;  
        }
    }
}