﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="UserDashboard.aspx.cs" Inherits="Insighto.Admin.Pages.UserDashboard" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text="User Dashboard" meta:resourcekey="lblTitleResource1"></asp:Label>
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <div class="surveyCreateTextPanel">
            <div class="pickListCategoryTop">
                <div class="pickListAddBtnPanel">
                    <asp:HyperLink ID="hlnkAddList" CssClass="dynamicHyperLink" NavigateUrl="ManageUsers.aspx"
                        ToolTip="User Account" runat="server" Text="User Account" meta:resourcekey="hlnkAddListResource1"></asp:HyperLink>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="adminUserGridViewPanel" style="display: block;">
            </div>
            <div class="successPanel" id="divSuccess" style="display: none">
                <div>
                    <label id="lblMessage" />
                </div>
            </div>
            <div class="defaultH">
            </div>
            <div id="ptoolbar">
            </div>
            <table id="tblUsersList">
            </table>
            <div class="defaultH">
            </div>
            <div class="text" style="display: none">
            <div class="newSurevyPanel">
                <h3>
                    <asp:Label ID="lblYourAccountHasNo" Text="No deleted surveys" runat="server"></asp:Label></h3>   
             </div>
        </div>
        <div id="dvDeletedSurvey" style="display:none">
        <h2><asp:Label ID="lblDeleted" Text="Deleted surveys" runat="server"></asp:Label></h2>
        </div>
            <div class="defaultH">
            </div>
            <div id="ptoolbar1">
            </div>
            <table id="tblDeletedSurvey">
            </table>
            <div class="defaultH">
            </div>
            <div class="dvRestoreSurveys" style="display:none"  >
            <asp:Button ID="btnRestoreSurveys" runat="server"   CssClass="dynamicButton btnRestoreSurveys"
                Text="Restore Surveys" />
                </div>
            <input type="hidden" id="hdnUserId" runat="server" class="hdnUserId" />
            <input type ="hidden" id="hdnSurveyId" runat="server" class="hdnSurveyId" />    
            <script type="text/javascript">
        var userId = $('.hdnUserId').val();
        var serviceUrl = '../AjaxService.aspx';

        $(document).ready(function () {
            $("#tblUsersList").jqGrid({
                url: serviceUrl,
                postData: { "method": "GetUserSurveyDetails", "UserId": userId },
                datatype: 'json',
                colNames: ['Survey Name', 'Category', 'Status', 'Sent To', 'Responses', ''],
                colModel: [

                { name: 'SurveyName', index: 'SurveyName', width: 65, resizable: false },
                { name: 'CategoryName', index: 'CategoryName', width: 65, align: 'center', resizable: false },
                { name: 'Status', index: 'Status', width: 75, align: 'center', resizable: false },
                { name: 'SendTo', index: 'SendTo', width: 100, align: 'center', formatter: "integer", summaryType: 'sum', resizable: false },
                { name: 'Responses', index: 'Responses', width: 100, align: 'center', formatter: "integer", summaryType: 'sum', resizable: false },
                { name: 'url', index: 'url', width: 15, align: 'center', formatter: CopyLinkFormatter, sortable: false },

                 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'SurveyName',
                sortorder: "asc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 875,
                caption: '',
                height: '100%',
                grouping: true,
                groupingView:
                 { groupField: ['Status'],
                     groupSummary: [true],
                     groupColumnShow: [true],
                     groupText: ['<b>{0}</b>'],
                     groupCollapse: false,
                     groupOrder: ['asc']
                 },

                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }

            });
            $("#tblUsersList").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
            $("#tblUsersList").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');

            function CopyLinkFormatter(cellvalue, options, rowObject) {
                var str = "<a href='" + cellvalue + "' class='button-small-gap' id='lnkDelete' rel='framebox' h='350' w='550' scrolling='true'><img src='../App_Themes/Classic/Images/icon-copy.gif' title='Copy question library.' /></a>";
                return str;
            }

            $("#tblDeletedSurvey").jqGrid({
                url: serviceUrl,
                postData: { "method": "GetUserDeletedSurveyDetails", "UserId": userId },
                datatype: 'json',
                colNames: ['', 'Survey Name', 'Name', 'Category', 'Sent To', 'Responses'],
                colModel: [
                { name: 'SurveyId', index: 'SurveyId', hidden: true },
                { name: 'SurveyName', index: 'SurveyName', width: 60, resizable: false },
                { name: 'Name', index: 'Name', width: 60, align: 'center', resizable: false },
                { name: 'CategoryName', index: 'CategoryName', width: 75, align: 'center', resizable: false },
                { name: 'SendTo', index: 'SendTo', width: 100, align: 'center', formatter: "integer", summaryType: 'sum', resizable: false },
                { name: 'Responses', index: 'Responses', width: 100, align: 'center', formatter: "integer", summaryType: 'sum', resizable: false },

                 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar1',
                gridview: true,
                sortname: 'SurveyId',
                sortorder: "asc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 875,
                caption: '',
                height: '100%',
                multiselect: true,
                loadComplete: function () {
                    if ($("#tblDeletedSurvey").getGridParam("records") == 0) {
                        $('#ptoolbar1').hide();
                        $('#gbox_tblDeletedSurvey').hide();
                        $('.dvRestoreSurveys').hide();
                        $('.text').show();
                    }
                    else {
                        $('.dvRestoreSurveys').show();
                        $('#dvDeletedSurvey').show();
                    }
                }

            });
            $("#tblDeletedSurvey").jqGrid('navGrid', '#ptoolbar1', { del: false, add: false, edit: false, search: false, refresh: false });
            $("#tblDeletedSurvey").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');


        });

        function CopySurvey(copySuveyId) {
           
            $.post("../AjaxService.aspx", { "method": "CopySurveyForQuestionLibrary", "copySurveyId": copySuveyId },
               function (result) {
                   if (result == '1') {
                       $('.successPanel').show();
                       $('.lblMessage').html('Survey copied into the question library');
                   }
                   else {

                   }
               });
           }
           $(".btnRestoreSurveys").click(function (e) {
               e.preventDefault();
               var ids;
               var grid = jQuery("#tblDeletedSurvey");
               ids = jQuery("#tblDeletedSurvey").jqGrid('getGridParam', 'selarrrow');

               if (ids.length > 0) {
                   var SurveyIds = [];
                   for (var i = 0, il = ids.length; i < il; i++) {
                       var SurveyId = grid.jqGrid('getCell', ids[i], 'SurveyId');
                       SurveyIds.push(SurveyId);
                   }
               }
               $('.hdnSurveyId').val(SurveyIds);
               $.post("../AjaxService.aspx", { "method": "RestoreSurveys", "SurveyIds": $('.hdnSurveyId').val() },
               function (result) {
                if (result == '0') {
                       DeletedgridReload();
                   }
               });

           });

           function DeletedgridReload() {

               $("#tblDeletedSurvey").jqGrid('setGridParam', { url: serviceUrl, page: 1 }).trigger("reloadGrid");
           }
            </script>
        </div>
    </div>
</asp:Content>
