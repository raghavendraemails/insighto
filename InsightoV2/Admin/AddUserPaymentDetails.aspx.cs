﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using Insighto.Exceptions;
using System.Collections;
using CovalenseUtilities.Helpers;
using System.Configuration;
using Insighto.Business.Helpers;
using Insighto.Data;
using CovalenseUtilities.Services;
namespace Insighto.Admin.Pages
{
    public partial class AddUserPaymentDetails : BasePage
    {
        #region Private variables
        int UserId;
        int ModeofPayment;
        int OrderId;
        Hashtable ht1 = new Hashtable();
        #endregion

        #region Events
        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["OrderId"] != null)
                {
                    OrderId = ValidationHelper.GetInteger(Request.QueryString["OrderId"], 0);
                    var userServices = ServiceFactory.GetService<OrderInfoService>();
                    var order = userServices.GetUserIdByOrderId(OrderId);
                    UserId = order[0].USERID;
                    ModeofPayment = order[0].MODEOFPAYMENT;
                    hdnUserId.Value = Convert.ToString(UserId);
                    hdnOrderId.Value = Convert.ToString(OrderId);
                    if (!string.IsNullOrEmpty(Request.QueryString["Mode"]))
                        hdnMode.Value = Request.QueryString["Mode"];
                }
                else
                {

                    ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                    if (ht1.Contains("UserId"))
                    {
                        UserId = ValidationHelper.GetInteger(ht1["UserId"].ToString(), 0);
                        hdnUserId.Value = Convert.ToString(UserId);
                    }
                    if (ht1.Contains("Mode"))
                    {
                        hdnMode.Value = ht1["Mode"].ToString();
                    }
                    if (Request.QueryString["Page"] != null)
                        hdnPage.Value = Request.QueryString["Page"].ToString();

                    getOrderInfoByUserId();


                }
                GetUserDetails();
                txtPaymentDate.Text = DateTime.UtcNow.ToString("dd-MMM-yyyy");
            }
        }
        #endregion

        #region "Event:ApproveButtonClickEvent"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            InsertUserPaymentInfo();
            if (ValidationHelper.GetInteger(hdnOrderId.Value, 0) > 0)
            {

                if (hdnMode.Value == "R")
                {
                    UpgradeUsers(hdnMode.Value);
                    base.CloseModelForSpecificTime("RenewalPendingInvoices.aspx", ConfigurationManager.AppSettings["TimeOut"]);
                }
                else if (hdnMode.Value == "U" && hdnPage.Value == "L")
                {
                    UpgradeUsers(hdnMode.Value);
                    base.CloseModelForSpecificTime("LicenseUpgrade.aspx", ConfigurationManager.AppSettings["TimeOut"]);
                }
                else if (hdnMode.Value == "U" && hdnPage.Value == "P")
                {
                    UpgradeUsers(hdnMode.Value);
                    base.CloseModelForSpecificTime("ViewPaymentPendingUsers.aspx", ConfigurationManager.AppSettings["TimeOut"]);
                }
                else
                {
                    UpgradeUsers();
                    base.CloseModelForSpecificTime("ViewPendingInvoices.aspx", ConfigurationManager.AppSettings["TimeOut"]);
                }
            }

        }
        #endregion

        #endregion

        #region Methods

        #region "Method:GetUserDetaisls"
        /// <summary>
        /// 
        /// </summary>
        public void GetUserDetails()
        {
            var userServices = ServiceFactory.GetService<UsersService>();
            var AUser = userServices.GetUserDetails(UserId);
            lblEmailId.Text = AUser[0].EMAIL;
            lblFirsName.Text = AUser[0].FIRST_NAME + "" + AUser[0].LAST_NAME;
            lblLicenseType.Text = AUser[0].LICENSE_TYPE;
            hdnPassword.Value = EncryptHelper.Decrypt(AUser[0].PASSWORD);
            hdnLicenseOpted.Value = AUser[0].LICENSE_OPTED;
            if (AUser[0].PHONE != null)
            {
                string phone = AUser[0].PHONE;
                string coutrycode = "";
                string regioncode = "";
                int x = phone.IndexOf(Constants.PHONE_SEPARATER);
                if (x >= 0)
                {
                    coutrycode = phone.Substring(0, x);
                    phone = phone.Substring(x + 1);
                    x = phone.IndexOf(Constants.PHONE_SEPARATER);
                    regioncode = phone.Substring(0, x);
                    regioncode = phone.Substring(0, x);
                    phone = phone.Substring(x + 1);
                }
                lblPhone.Text = coutrycode + "-" + regioncode + "-" + phone;

            }
            if (ModeofPayment == 2)
                rblPaymentType.SelectedIndex = 1;
        }

        #endregion

        #region "Method:InsertUserPaymentInfo"
        /// <summary>
        /// 
        /// </summary>
        public void InsertUserPaymentInfo()
        {

            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginAdminUserDetailsSession();

            var userPayment = ServiceFactory.GetService<UserPaymentService>();
            var userPaymentInfo = new osm_userpaymentinfo();
            var userServices = ServiceFactory.GetService<UsersService>();
            var user = new osm_user();
            var adminTrackingService = ServiceFactory.GetService<AdminTrackingServices>();
            var adminTrackInfo = new osm_admintracking();

            userPaymentInfo.ADMINID = ValidationHelper.GetInteger(userInfo.AdminUserId, 0);
            userPaymentInfo.USERID = ValidationHelper.GetInteger(hdnUserId.Value, 0);
            userPaymentInfo.BANK_NAME = txtBankName.Text.Trim();
            if (rblPaymentType.SelectedValue == Constants.DirectDeposit)
            {
                userPaymentInfo.CHEQUE_NUMBER = "";
                userPaymentInfo.CREDITCARDNUMBER = "";
            }
            userPaymentInfo.AMOUNT_PAID = txtAmountPaid.Text.Trim();
            if (rblPaymentType.SelectedValue == Constants.PAYBYCHEQUE)
            {
                userPaymentInfo.CHEQUE_NUMBER = txtChequeNumber.Text.Trim();
                userPaymentInfo.CREDITCARDNUMBER = "";
            }

            userPaymentInfo.PAYMENT_OPTION = ValidationHelper.GetInteger(rblPaymentType.SelectedValue, 0);

            userPaymentInfo.PAYMENT_ENTRY_DATE = DateTime.UtcNow;
            userPaymentInfo.DELETED = 0;
            userPaymentInfo.PAYMENT_DATE = AddDateTime(txtPaymentDate.Text);

            var result = userPayment.SavePaymentInfo(userPaymentInfo);

            if (result != null)
            {
                userServices.UpdateApprovalFlag(ValidationHelper.GetInteger(hdnUserId.Value, 0));

                string fromEmail = ConfigurationManager.AppSettings["emailActivation"];
                string subject = "Knowience Registration";
                string MsgBody = "Your Knowience Registration has been done successfully " + "  " + " Your Username is: " + lblEmailId.Text + "   " + " and " + " password is " + hdnPassword.Value;

                if (!String.IsNullOrEmpty(lblLicenseType.Text.Trim()))
                {
                    userServices.UpgradeLicenseLevelAfterPayment(ValidationHelper.GetInteger(hdnUserId.Value, 0), hdnLicenseOpted.Value);
                    MsgBody = "Your Knowience License Level Upgrade to " + hdnLicenseOpted.Value + "  " + " has been done successfully " + "  " + " Your Username is: " + lblEmailId.Text + "  " + " and " + " password is " + hdnPassword.Value;
                }

                MailHelper.SendMailMessage(fromEmail, lblEmailId.Text, "", "", subject, MsgBody);
                string tracking_desc = "";
                tracking_desc = "Entry For Payment Details has been Done for User With Email " + lblEmailId.Text + " By " + userInfo.FirstName + " " + userInfo.LastName + " with track_id " + result.PAYMENTID;

                adminTrackInfo.ADMINID = ValidationHelper.GetInteger(userInfo.AdminUserId, 0);
                adminTrackInfo.ACTION_DONE_ON_PAGE = "Adding Payment Details";
                adminTrackInfo.FKUSERID = ValidationHelper.GetInteger(hdnUserId.Value, 0);
                adminTrackInfo.EVENT_ACTION_DESCRIPTION = tracking_desc;
                adminTrackInfo.CREATED_ON = DateTime.UtcNow;
                var resultTrack = adminTrackingService.InsertAdminTracking(adminTrackInfo);
                if (resultTrack != null)
                {
                    dvSuccessMsg.Visible = true;
                }

            }

        }
        #endregion

        #region Method:AddDateTime
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateText"></param>
        /// <returns></returns>
        private DateTime AddDateTime(string dateText)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginAdminUserDetailsSession();
            DateTime returnDate;
            returnDate = Convert.ToDateTime(dateText);
            return CommonMethods.GetConvertedDatetime(returnDate, -(userDetails.StandardBIAS), true);
        }
        #endregion

        #region "Method:UpgradeUsers"
        /// <summary>
        /// 
        /// </summary>
        private void UpgradeUsers()
        {

            var orderService = ServiceFactory.GetService<OrderInfoService>();
            var rtnmsg = orderService.UpgradeLicense(ValidationHelper.GetInteger(hdnOrderId.Value, 0));

        }
        private void UpgradeUsers(string mode)
        {

            var orderService = ServiceFactory.GetService<OrderInfoService>();
            var rtnmsg = orderService.UpgradeLicense(ValidationHelper.GetInteger(hdnOrderId.Value, 0), hdnMode.Value);

        }

        private void getOrderInfoByUserId()
        {
            var orderService = ServiceFactory.GetService<OrderInfoService>();
            var orderInfo = orderService.GetOrderInfoByUserId(ValidationHelper.GetInteger(hdnUserId.Value, 0));
            if (orderInfo != null)
            {
                hdnOrderId.Value = orderInfo[0].PK_ORDERID.ToString();
                ModeofPayment = orderInfo[0].MODEOFPAYMENT;
            }
        }
        #endregion

        #endregion
    }
}