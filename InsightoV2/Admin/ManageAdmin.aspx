﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="ManageAdmin.aspx.cs" Inherits="Insighto.Pages.Admin.ManageAdmin" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="contentPanelHeader">
        <input id="hdnStatus" class="hdnStatus" type="hidden" value="All" />
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text="Manage Admin User" 
                meta:resourcekey="lblTitleResource1"></asp:Label>
          
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="successPanel" id="divSuccess" style="display: none">
            <div>
                <%-- <label id="lblMessage" />--%>
                <asp:Label ID="lblMessage" CssClass="successMsg" Text="Deleted Successfully." runat="server"
                    Style="display: none;" meta:resourcekey="lblMessageResource1"></asp:Label>
            </div>
        </div>
        <div class="surveyCreateTextPanel">
            <!-- admin content panel -->
            <div class="adminGridSearchPanel">
                <div class="searchLble">
                    <asp:Label ID="lblSearch" runat="server" Text="Search" 
                        meta:resourcekey="lblSearchResource1"></asp:Label>
                    </div>
                <div class="searchtxtcontrols">
                    <input type="text" class="textBoxAdminSearch" id="txtSearch" />
                </div>
                <div class="searchrghtPanel">
                    <asp:HyperLink ID="hlnkAddList" CssClass="dynamicHyperLink" NavigateUrl="AdminTracking.aspx"
                        ToolTip="Admin Tracking" runat="server" Text="Admin Tracking" 
                        meta:resourcekey="hlnkAddListResource1"></asp:HyperLink>&nbsp;
                        <asp:HyperLink ID="hlnkAddUser" CssClass="dynamicHyperLink"   
                        rel="framebox" h="480" w="650"
                            scrolling='yes' title='Copy' 
                        onclick='javascript: return ApplyFrameBox(this);' NavigateUrl="AddAdminUser.aspx"
                            ToolTip="Add Admin User" runat="server" Text="Add Admin User" meta:resourcekey="hlnkAddUserResource1" 
                           ></asp:HyperLink>              
                    
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="adminUserGridViewPanel" style="display: block;">
                <div id="ptoolbar">
                </div>
                <table id="tblAddressBook">
                </table>
            </div>
            <!-- //admin content panel -->
        </div>
        <div class="clear">
        </div>
    </div>
    <script type="text/javascript">

        var serviceUrl = '../AjaxService.aspx';
        $(document).ready(function () {
            $("#tblAddressBook").jqGrid({
                url: serviceUrl,
                postData: { method: "FindAdminUsers" },
                datatype: 'json',
                colNames: ['User Name', 'First Name', 'Last Name', 'Phone', 'Created On', 'Admin Type', ' ', '', ''],
                colModel: [
                { name: 'LOGIN_NAME', index: 'LOGIN_NAME', width: 90, resizable: false },
                { name: 'FIRST_NAME', index: 'FIRST_NAME', width: 40, align: 'left', resizable: false },
                { name: 'LAST_NAME', index: 'LAST_NAME', width: 50, align: 'left' },
                { name: 'PHONE', index: 'PHONE', width: 60, align: 'center', resizable: false },
                { name: 'CREATED_ON', index: 'CREATED_ON', width: 50, resizable: false, align: 'left' },
                { name: 'ADMIN_TYPE', index: 'ADMIN_TYPE', width: 80, align: 'left', resizable: false },
                { name: 'EditLinkUrl', index: 'EditLinkUrl', width: 10, align: 'center', resizable: false, formatter: EditLinkFormatter, sortable: false },
                { name: 'ADMINUSERID', index: 'ADMINUSERID', width: 10, align: 'center', resizable: false, formatter: ResetLinkFormatter, sortable: false },
                { name: 'ADMINUSERID', index: 'ADMINUSERID', width: 10, align: 'center', resizable: false, formatter: DeleteLinkFormatter, sortable: false },

                 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'MODIFIED_ON',
                sortorder: "desc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 880,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblAddressBook").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
            $("#tblAddressBook").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');
            $("#tblAddressBook").jqGrid('setLabel', 'LOGIN_NAME', '', 'textalignleft');
        });

        function DisplayLinkFormatter(cellvalue, options, rowObject) {
            return cellvalue;
        }

        function EditLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='" + cellvalue + "' rel='framebox' w='650' h='480' ><img src='../App_Themes/Classic/Images/icon-pen-active.gif' title='Edit'/></a>";
        }
        function ResetLinkFormatter(cellvalue, options, rowObject) {

            return "<a href='#' class='button-small-gap' id='lnkDelete' onclick='javascript: return ResetPassword(" + cellvalue + ");'><img src='../App_Themes/Classic/Images/icon-reset.gif' title='Reset Password' /></a>";
        }
        function DeleteLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='#' class='button-small-gap' id='lnkDelete' onclick='javascript: return DeleteUser(" + cellvalue + ");'><img src='../App_Themes/Classic/Images/icon-delete-active.gif' title='Delete' /></a>";
        }

        function gridReload() {
            var keyword = $("#txtSearch").val();
            $("#tblAddressBook").jqGrid('setGridParam', { url: serviceUrl + "?keyword=" + keyword, page: 1 }).trigger("reloadGrid");
        }

        function ResetPassword(userId) {
            var answer = confirm('Are you sure! you want to Reset Password ?');
            if (answer) {
                $.post("../AjaxService.aspx", { "method": "ResetAdminPassword", "Id": userId },
                //                  function (result) {
                //                      if (result == '1') {
                //                          var status = $('#hdnStatus').val();
                //                          $("#recordDeleted").show();
                //                          gridReload(status);
                //                      }
                //                  });
               function (result) {

                   if (result != '') {
                       $('#divSuccess').show();
                       $('.successMsg').show()
                       $('.successMsg').html('Password has been reset successfully.');
                       var status = $('#hdnStatus').val();
                       gridReload(status);
                   }
                   else {
                       $('#divSuccess').hide();
                       $('.successMsg').hide()
                   }
               });
            }
        }

        function DeleteUser(userId) {
            var answer = confirm('Are you sure! you want to delete?');
            if (answer) {
                $.post("../AjaxService.aspx", { "method": "DeleteAdminUser", "Id": userId },

                  function (result) {

                      if (result == '1') {
                          $('#divSuccess').show();
                          $('.successMsg').show()
                          $('.successMsg').html('Admin user deleted successfully.');
                          var status = $('#hdnStatus').val();
                          gridReload(status);
                      }
                      else {
                          $('#divSuccess').hide();
                          $('.successMsg').hide()
                      }
                  });

                //                  function (result) {
                //                      if (result == '1') {
                //                          var status = $('#hdnStatus').val();
                //                          gridReload(status);
                //                      }
                //                  });
            }
        }

        function ndateFormatter(cellval, opts, rwdat, _act) {

            if (cellval != "" && cellval != null) {
                var time = cellval.replace(/\/Date\(([0-9]*)\)\//, '$1');
                var date = new Date();
                date.setTime(time);
                return date.toDateString();
            } else return "-";
        }

        $("#txtSearch").bind('keyup', function () { gridReload(); });           
              
    </script>
</asp:Content>
