﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="EditBlockedWord.aspx.cs" Inherits="Insighto.Admin.Pages.EditBlockedWord" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                Edit Blocked Word</h3>
        </div>
      <%--  <div class="popupClosePanel"><a href="#" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(false,'');"> </a></div>--%>
    </div>
    <div class="clear"></div>
    <div class="defaultHeight">
    </div>
    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccessMsg" runat="server" Text="Blocked word updated successfully."></asp:Label></div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="lblBlockWord" Text="Block Word" runat="server"></asp:Label>
        </div>
        <div class="loginControls">
            <asp:TextBox ID="txtBlockWord" runat="server" MaxLength="100" CssClass="textBoxMedium" />
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="reqlbl btmpadding">
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqBlockWord" CssClass="lblRequired" ControlToValidate="txtBlockWord"
                ErrorMessage="Please enter Block Word." runat="server" Display="Dynamic"></asp:RequiredFieldValidator></div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="lblDescription" Text="Description" runat="server"></asp:Label>
        </div>
        <div class="loginControls">
            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="textAreaMedium" />
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="reqlbl btmpadding">
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
        </div>
        <div class="loginControls">
            <asp:Button ID="btnSave" Text="Update" CssClass="dynamicButton" runat="server" OnClick="btnSave_Click" />
            <input type="button" value="Cancel" onclick="Javascript:window.parent.location.href='ManagedBlockedWords.aspx';"
                class="dynamicButton" />
        </div>
        <asp:HiddenField ID="hdnBlockId" runat="server" />
    </div>
</asp:Content>
