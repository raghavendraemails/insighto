﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using System.Collections;
using CovalenseUtilities.Helpers;
using System.Configuration;
using App_Code;
using Resources;

namespace Insighto.Admin.Pages
{
    public partial class AddAdminUser : BasePage
    {
        Hashtable ht = new Hashtable();

        #region Events

        #region PageLoad 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRoles();
                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
                    if (ht != null && ht.Count > 0 && ht.Contains("AdminUserId"))
                    {
                        int adminUserId = ValidationHelper.GetInteger(ht["AdminUserId"].ToString(), 0);
                        BindUserDetails(adminUserId);
                        dvPassword.Visible = false;
                        txtEmailId.Enabled = false; 
                    }
                    lblCreate.Visible = false;
                    lblEdit.Visible = true;
                    btnUpdate.Visible = true;
                   // btnCancel.Visible = true;
                    btnSave.Visible = false;
                    btnSaveNew.Visible = false;
                }
                else
                {
                    lblCreate.Visible = true;
                    lblEdit.Visible = false;
                    btnUpdate.Visible = false;
                    //btnCancel.Visible = false;
                    btnSave.Visible = true;
                    btnSaveNew.Visible = true;
                }
            }
        }
        #endregion

        #region SaveButton Click Event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            SaveAdminUser(1);       
           
            
        }
        #endregion

        #region SaveNewButton Click Event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveAdminUser(0);
               
            }
        }
        #endregion

        #region Update Button Click
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveAdminUser(1);
            }
          
           
        }
        #endregion

        #endregion

        #region Methods
        #region BindUserDetails
        /// <summary>
        /// Get the details of admin userd based on userid
        /// </summary>
        /// <param name="adminUserId"></param>
        private void BindUserDetails(int adminUserId)
        {
            var adminUserService = ServiceFactory.GetService<AdminUserService>();
            var userInfo = adminUserService.FindAdminUserByUserId(adminUserId);
            txtEmailId.Text = userInfo.LOGIN_NAME;
            txtFirstName.Text = HttpContext.Current.Server.HtmlDecode(userInfo.FIRST_NAME);
            txtLastName.Text = HttpContext.Current.Server.HtmlDecode(userInfo.LAST_NAME);

            if (!string.IsNullOrEmpty(userInfo.ADMIN_TYPE))

                ddlAdminType.SelectedValue = userInfo.ADMIN_TYPE;
            else
                ddlAdminType.SelectedIndex = 0;
            if (userInfo.PHONE != null)
            {
                string phone = userInfo.PHONE;
                string coutrycode = string.Empty;
                string regioncode = "";
                int index = phone.IndexOf(Constants.PHONE_SEPARATER);
                if (index >= 0)
                {
                    coutrycode = phone.Substring(0, index);
                    phone = phone.Substring(index + 1);
                    index = phone.IndexOf(Constants.PHONE_SEPARATER);
                    regioncode = phone.Substring(0, index);
                    regioncode = phone.Substring(0, index);
                    phone = phone.Substring(index + 1);
                }
                txtCountryCode.Text = coutrycode;
                txtAreaCode.Text = regioncode;
                txtPhoneNumber.Text = phone;
            }

            //reqNewPassword.Enabled = false;
            //reqConfirmPassword.Enabled = false;
        }
        #endregion

        #region BindRoles
        /// <summary>
        /// Bind roles from back end
        /// </summary>
        protected void BindRoles()
        {
            var roleService = ServiceFactory.GetService<RoleService>();
            var roles = roleService.FindAllRoles();
            if (roles.Count > 0)
            {
                ddlAdminType.DataSource = roles;
                ddlAdminType.DataTextField = "ROLE_NAME";
                ddlAdminType.DataValueField = "ROLE_NAME";
                ddlAdminType.DataBind();
            }
            ddlAdminType.Items.Insert(0, new ListItem("Super Admin", ""));
        }
        #endregion

        private void SaveAdminUser(int flag)

        {
            int adminUserId = 0,rtnVal = 0;
            
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
                if (ht != null && ht.Count > 0 && ht.Contains("AdminUserId"))
                {
                    adminUserId = ValidationHelper.GetInteger(ht["AdminUserId"].ToString(), 0);
                }
            }

            var adminService = ServiceFactory.GetService<AdminUserService>();
            var user = new osm_adminuser();
            user.LOGIN_NAME = HttpContext.Current.Server.HtmlEncode(txtEmailId.Text.Trim());
            user.FIRST_NAME =  HttpContext.Current.Server.HtmlEncode(txtFirstName.Text.Trim());
            user.LAST_NAME = txtLastName.Text.Trim();
            user.PASSWORD = EncryptHelper.Encrypt(txtPassword.Text.Trim());
            user.RESET = 1;
            user.ADMIN_TYPE = ddlAdminType.SelectedValue;
            user.CREATED_ON = DateTime.UtcNow;
            user.MODIFIED_ON = DateTime.UtcNow;
            user.DELETED = 0;
            string phoneNo = txtCountryCode.Text.Trim() + Constants.PHONE_SEPARATER + txtAreaCode.Text.Trim() + Constants.PHONE_SEPARATER + txtPhoneNumber.Text.Trim();
            user.PHONE = phoneNo;
            if (adminUserId > 0)
            {
                user.ADMINUSERID = adminUserId;
                rtnVal = adminService.UpdateAdminUserByUserId(user);
                if (rtnVal > 0)
                {
                    lblSuccessMsg.Text = Utilities.ResourceMessage("UpdateUser");    //"Admin user details updated successfully.";
                }
                else
                {
                    lblErrMsg.Text = CommonMessages.EmailExistsMsg.ToString(); 
                }
            }
            else
            {
                rtnVal = adminService.SaveAdminUsers(user);
                if (rtnVal > 0)
                {
                    string fromEmail = ConfigurationManager.AppSettings["emailActivation"];
                    string subject = Utilities.ResourceMessage("Registration");  //"Knowience Registration";
                    string MsgBody = Utilities.ResourceMessage("RegistrationSuccess") + "  " + Utilities.ResourceMessage("UserName")  + txtEmailId.Text.Trim() + " and " +  Utilities.ResourceMessage("Password")   + txtPassword.Text.Trim();
                    MailHelper.SendMailMessage(fromEmail, txtEmailId.Text.Trim(), "", "", subject, MsgBody);


                    var adminTrackingService = ServiceFactory.GetService<AdminTrackingServices>();
                    var adminTrackInfo = new osm_admintracking();
                    var sessionService = ServiceFactory.GetService<SessionStateService>();
                    var userInfo = sessionService.GetLoginAdminUserDetailsSession();
                    string tracking_desc = "";
                    tracking_desc = " Admin User had been registered as " + txtEmailId.Text + " By " + userInfo.FirstName + " " + userInfo.LastName;
                    string temp = "Admin Registration";
                    adminTrackInfo.ADMINID = ValidationHelper.GetInteger(userInfo.AdminUserId, 0);
                    adminTrackInfo.ACTION_DONE_ON_PAGE = temp;
                    adminTrackInfo.EVENT_ACTION_DESCRIPTION = tracking_desc;
                    adminTrackInfo.CREATED_ON = DateTime.UtcNow;
                    adminTrackInfo.FKUSERID = 0;
                    var trackResult = adminTrackingService.InsertAdminTracking(adminTrackInfo);
                }
            }
           
            if (rtnVal > 0)
            {
                dvSuccessMsg.Visible = true;
                dvErrMsg.Visible = false;
                if (flag > 0)
                {
                    base.CloseModelForSpecificTime("ManageAdmin.aspx", ConfigurationManager.AppSettings["TimeOut"]);
                }
                else
                {
                    lblSuccessMsg.Text = Utilities.ResourceMessage("addAnotherUser");// "Admin created successfully. You are adding a new admin user.";
                    Clearfields();
                }
            }
            else
            {
                dvSuccessMsg.Visible = false;
                dvErrMsg.Visible = true;
                 
            }
        }
        
        private void Clearfields()
        {
            txtAreaCode.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtPhoneNumber.Text = string.Empty;
            txtCountryCode.Text = string.Empty;
            txtEmailId.Text = string.Empty; 
            ddlAdminType.SelectedIndex = 0;
        }
        #endregion
    }
}