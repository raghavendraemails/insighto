﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Data;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using App_Code;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using Insighto.Exceptions;
using System.Configuration;
using CovalenseUtilities.Helpers;
using System.Globalization;
using CurrencyExchangeRates;
using Resources;

namespace Insighto.Admin.Pages
{
    public partial class AddUser : BasePage
    {
        #region Private Member declaration
        Hashtable typeHt = new Hashtable();
        private string licenseType = "";
        private string Type;
        private double price;
        private double tax = 0;
        private double taxPercentage;
        private string orderNo = "PRO";
        int userId = 0;
        #endregion

        #region Events
        #region Page_Load
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionStateService sessionStateService = new SessionStateService();

            if (!IsPostBack)
            {
                bindDropDownList(Constants.COMPANY_SIZE, drpCompanySize);
                bindDropDownList(Constants.WORK_INDUSTRY, drpWorkIndustry);
                bindDropDownList(Constants.JOB_FUNCTION, drpJobFunction);
                bindDropDownList(Constants.COUNTRY, drpCountry);
                // bindDropDownList(Constants.STATE, drpState);
                // List <SomeEnum> theList

            }
        }
        #endregion

        #region SaveButton Click event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            InsertUserDeatails();
            if (dvErrMsg.Visible == false)
            {
                base.CloseModelForSpecificTime("ManageUsers.aspx", ConfigurationManager.AppSettings["TimeOut"]);
            }
        }
        #endregion

        #region SaveAddNew Click Event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAddNew_Click(object sender, EventArgs e)
        {
            InsertUserDeatails();
            ClearFields();
        }
        #endregion

        #endregion

        #region Methods
        #region ClearFields
        /// <summary>
        /// 
        /// </summary>
        public void ClearFields()
        {
            txtEmailId.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            txtDept.Text = string.Empty;
            txtPhoneNumber.Text = string.Empty;
            txtPhoneNumber1.Text = string.Empty;
            txtPhoneNumber2.Text = string.Empty;
            txtAddressLine1.Text = string.Empty;
            txtAddressLine2.Text = string.Empty;
            txtPostalCode.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtCompany.Text = string.Empty;
            drpCompanySize.SelectedIndex = 0;
            drpCountry.SelectedIndex = 0;
            drpJobFunction.SelectedIndex = 0;
            drpUserType.SelectedIndex = 0;
            // drpState.SelectedIndex = 0;
            drpWorkIndustry.SelectedIndex = 0;
        }
        #endregion

        #region BindDrowDownList
        /// <summary>
        /// Binds the values to dropdown List
        /// </summary>
        /// <param name="CategoryName"></param>
        /// <param name="ddl"></param>
        protected void bindDropDownList(string CategoryName, DropDownList ddl)
        {
            PickListService pickListService = new PickListService();
            osm_picklist osm_picklist = new osm_picklist();
            osm_picklist.PARAM_VALUE = Constants.PARAM_VALUE;
            osm_picklist.PARAMETER = Constants.PARAMETER;
            osm_picklist.CATEGORY = CategoryName;
            var list = new List<osm_picklist>();

            if (ddl.ID == "drpCompanySize")
            {
                list = pickListService.GetPickListFontSize(osm_picklist);
            }
            else
            {
                list = pickListService.GetPickList(osm_picklist);
            }

            if (list.Count > 0)
            {
                ddl.DataSource = list;
                ddl.DataTextField = Constants.PARAM_VALUE;
                ddl.DataValueField = Constants.PARAMETER;

                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem(Constants.SELECT, Constants.SELECTVALUE));
                ddl.SelectedIndex = 0;
            }
        }
        #endregion

        #region InsertUserDeatails
        /// <summary>
        /// 
        /// </summary>
        public void InsertUserDeatails()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginAdminUserDetailsSession();
            SessionStateService sessionStateService = new SessionStateService();
            var adminTrackingService = ServiceFactory.GetService<AdminTrackingServices>();
            var adminTrackInfo = new osm_admintracking();
            var userServices = new UsersService();
            string phoneNo = "";
            phoneNo = txtPhoneNumber.Text.Trim() + Constants.PHONE_SEPARATER + txtPhoneNumber1.Text.Trim() + Constants.PHONE_SEPARATER + txtPhoneNumber2.Text.Trim();

            var userList = new List<osm_user>();
            var user = new osm_user();

            user.FIRST_NAME = HttpContext.Current.Server.HtmlEncode(txtFirstName.Text.Trim());
            user.LAST_NAME = HttpContext.Current.Server.HtmlEncode(txtLastName.Text.Trim());
            user.LOGIN_NAME = txtEmailId.Text.Trim();
            user.ADDRESS_LINE1 = txtAddressLine1.Text.Trim();
            user.ADDRESS_LINE2 = txtAddressLine2.Text.Trim();
            user.CITY = txtCity.Text.Trim();
            user.STATE = txtState.Text;// drpState.SelectedValue;
            user.COUNTRY = drpCountry.SelectedValue;
            user.POSTAL_CODE = txtPostalCode.Text.Trim();
            user.USER_COMPANY = txtCompany.Text.Trim();
            user.DEPT = txtDept.Text.Trim();
            user.WORK_INDUSTRY = drpWorkIndustry.SelectedValue;
            user.JOB_FUNCTION = drpJobFunction.SelectedValue;
            user.COMPANY_SIZE = drpCompanySize.SelectedValue;
            user.EMAIL = txtEmailId.Text.Trim();
            user.PASSWORD = EncryptHelper.Encrypt(txtPassword.Text.Trim());
            user.PHONE = phoneNo;
            user.MODIFIED_ON = DateTime.UtcNow;
            user.CREATED_ON = DateTime.UtcNow;
            user.ADMINID = 0;
            user.DELETED = 0;
            user.UPDATES_FLAG = 0;
            user.EMAIL_FLAG = 0;
            user.STATUS = Constants.Active;
            user.APPROVAL_FLAG = 0;
            user.RENEWAL_FLAG = 0;
            user.LICENSE_TYPE = "FREE";
            user.LICENSE_OPTED = drpUserType.SelectedValue.ToUpper();
            user.MAIL_REMINDER_FLAG = 0;
            user.MAIL_REMINDER_DATE = DateTime.UtcNow;
            user.ACTIVATION_FLAG = 0;
            user.QUICKUSERGUIDE_FLAG = 0;
            user.LICENSE_UPGRADE = 0;
            user.USER_TYPE = 0;
            user.RESET = 1;
            user.PAYMENT_TYPE = 0;
            if (drpUserType.SelectedValue == "FREE")
            {
                user.EMAIL_COUNT = 0;
                user.LICENSE_EXPIRY_DATE = Convert.ToDateTime("1/1/1800");
                user.WALLETPRICEPLAN = "FREE";
            }
            if (drpUserType.SelectedValue == "PRO_MONTHLY")
            {
                user.ACCOUNT_TYPE = "Master";
                user.APPROVAL_FLAG = 1;
                user.LICENSE_EXPIRY_DATE = DateTime.UtcNow;
                user.PAYMENT_TYPE = 2;
                user.WALLETPRICEPLAN = "PRO";
            }
            if (drpUserType.SelectedValue == "PRO_QUARTERLY")
            {
                user.ACCOUNT_TYPE = "Master";
                user.APPROVAL_FLAG = 1;
                user.LICENSE_EXPIRY_DATE = DateTime.UtcNow;
                user.PAYMENT_TYPE = 2;
                user.WALLETPRICEPLAN = "PRO";
            }
            if (drpUserType.SelectedValue == "PRO_YEARLY")
            {
                user.ACCOUNT_TYPE = "Master";
                user.APPROVAL_FLAG = 1;
                user.LICENSE_EXPIRY_DATE = DateTime.UtcNow;
                user.PAYMENT_TYPE = 2;
                user.WALLETPRICEPLAN = "PRO";
            }
            string ipAddress = string.Empty;

            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
            user.IP_ADDRESS = ipAddress;
            userList.Add(user);

            var resultuser = userServices.SaveUser(user);
            if (resultuser == null)
            {
                lblErrorMsg.Text = CommonMessages.EmailExistsMsg.ToString();
                dvErrMsg.Visible = true;
            }
            else
            {
                if (drpUserType.SelectedValue == "FREE")
                {
                    dvSuccessMsg.Visible = true;
                    dvErrMsg.Visible = false;
                }
                else
                {
                    BindPaymentDetails(drpUserType.SelectedItem.Value);
                    var order = GetOrderInfoDetails(resultuser.USERID);
                    order = userServices.InsertOrderInfo(order);
                    if (order != null)
                    {
                        var history = GetAccountHistoryInfo(user.USERID, order.PK_ORDERID.ToString());
                        var historyId = userServices.InsertUserAccountHistory(history);
                        string orderID = orderNo + order.PK_ORDERID;
                        lblSuccessMsg.Text = Utilities.ResourceMessage("lblSuccessMsgResource1.Text");
                        dvSuccessMsg.Visible = true;
                        dvErrMsg.Visible = false;

                    }

                }
                string tracking_desc = "";
                tracking_desc = Convert.ToString(drpUserType.SelectedItem.Text) + " User had been registered as  " + txtEmailId.Text + " By  " + userInfo.FirstName + " " + userInfo.LastName + "and TrackingId=" + resultuser.USERID;
                string temp = Convert.ToString(drpUserType.SelectedItem.Text) + "User Registration";
                adminTrackInfo.ADMINID = ValidationHelper.GetInteger(userInfo.AdminUserId, 0);
                adminTrackInfo.ACTION_DONE_ON_PAGE = temp;
                adminTrackInfo.EVENT_ACTION_DESCRIPTION = tracking_desc;
                adminTrackInfo.CREATED_ON = DateTime.UtcNow;
                adminTrackInfo.FKUSERID = resultuser.USERID;
                var trackResult = adminTrackingService.InsertAdminTracking(adminTrackInfo);

            }
        }
        #endregion

        #region BindPaymentDetails
        /// <summary>
        /// bind the payment details based on the license type
        /// </summary>
        /// <param name="licenseType"></param>
        private void BindPaymentDetails(string licenseType)
        {

            string[] LICINFO = new string[2];
            LICINFO = licenseType.ToUpper().Split('_');
            LICINFO[0] = "ORDER_" + LICINFO[0];
            LICINFO[1] = LICINFO[1] + "_NOTATION";
            List<string> listConfg = new List<string>();
            listConfg.Add(licenseType);
            listConfg.Add(Constants.TAX);
            listConfg.Add(LICINFO[0]);
            listConfg.Add(LICINFO[1]);
            listConfg.Add(Constants.MERCHANTID);
            var configvalue = ServiceFactory.GetService<ConfigService>().GetConfigurationValues(listConfg);

            foreach (var cv in configvalue)
            {
                if (cv.CONFIG_KEY == licenseType)
                {
                    price = Convert.ToDouble(cv.CONFIG_VALUE.ToString());
                    hdnPrice.Value = price.ToString();
                }
                if (cv.CONFIG_KEY == Constants.TAX)
                {
                    taxPercentage = Convert.ToDouble(cv.CONFIG_VALUE.ToString());
                    hdnTax.Value = taxPercentage.ToString();
                }
                if (cv.CONFIG_KEY == Constants.MERCHANTID)
                {
                    hdnMerchantId.Value = cv.CONFIG_VALUE.ToString();
                }
                if (cv.CONFIG_KEY == LICINFO[1])
                {
                    orderNo += "-" + cv.CONFIG_VALUE.ToString();
                }

            }
            tax = Math.Round(Convert.ToDouble(price * Convert.ToDouble(taxPercentage) / 100), 2);
            hdnTaxAmount.Value = tax.ToString();
            hdnTotal.Value = Math.Round(price + tax, 2).ToString();
            hdnOrderNo.Value = orderNo;
        }
        #endregion

        #region GetOrderInfoDetails
        public osm_orderinfo GetOrderInfoDetails(int userId)
        {
            orderNo = hdnOrderNo.Value;
            osm_orderinfo order = new osm_orderinfo();
            if (DateTime.UtcNow.Month < 10)
                orderNo += "0" + DateTime.UtcNow.Month;
            else
                orderNo += DateTime.UtcNow.Month;
            orderNo += Convert.ToString(DateTime.UtcNow.Year).Substring(2);
            order.TAX_PERCENT = ValidationHelper.GetDouble(hdnTax.Value, 0);
            order.TAX_PRICE = ValidationHelper.GetDouble(hdnTaxAmount.Value, 0);
            order.TOTAL_PRICE = ValidationHelper.GetDouble(hdnTotal.Value, 0);
            order.USERID = userId;
            order.PRODUCT_PRICE = ValidationHelper.GetDouble(hdnPrice.Value, 0);
            order.MODEOFPAYMENT = 2;//ValidationHelper.GetInteger(rbtnListPaymentType.SelectedValue, 0);
            order.ORDERID = orderNo;
            order.ORDER_TYPE = Constants.ORDERTYPE;
            order.CREATED_DATE = DateTime.UtcNow;
            order.TRANSACTION_STATUS = 0;
            order.CurrencyType = "INR"; // new RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name).ISOCurrencySymbol;
            order.ModifiedDate = DateTime.UtcNow;
            //var currency =new CurrencyServiceClient().GetConversionRate(CurrencyCode.USD, CurrencyCode.INR);
            order.ExchangeRate = 0;// currency.Rate;
            return order;
        }
        #endregion

        #region GetAccountHistoryInfo
        /// <summary>
        /// fills data into user account history object and returns user acc 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public osm_useraccounthistory GetAccountHistoryInfo(int userId, string orderId)
        {
            osm_useraccounthistory history = new osm_useraccounthistory();
            history.USERID = userId;
            history.ORDERID = orderId;
            history.EMAIL_FLAG = 1;
            history.CREATED_DATE = DateTime.UtcNow;
            history.CURRENT_LICENSETYPE = drpUserType.SelectedValue.ToUpper();
            history.LICENSETYPE_OPTED = drpUserType.SelectedValue.ToUpper();
            return history;
        }
        #endregion
        #endregion
    }
}