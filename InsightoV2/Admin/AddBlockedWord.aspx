﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="AddBlockedWord.aspx.cs" Inherits="Insighto.Admin.Pages.AddBlockedWord" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                <asp:Label ID="lblTitle" runat="server" Text="Add Blocked Word" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>
                </h3>
        </div>
        <!--<div class="popupClosePanel">
			<a href="#">X</a>
		</div>	-->
        <!-- //popup heading -->
       <%-- <div class="popupClosePanel">
        <a href="#" class="popupCloseLink" onclick ="closeModalSelf(true,'ManagedBlockedWords.aspx');" title="Close">&nbsp;</a>
            
        </div>--%>
    </div>
    <div class="defaultHeight">
    </div>
    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccessMsg" runat="server" 
                Text="Blocked word saved successfully." 
                meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
    </div>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" Text="Blocked word already exist." 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="lblBlockWord" Text="Block Word" runat="server" 
                meta:resourcekey="lblBlockWordResource1"></asp:Label>
        </div>
        <div class="loginControls">
            <asp:TextBox ID="txtBlockWord" runat="server" MaxLength="200" 
                CssClass="textBoxMedium" meta:resourcekey="txtBlockWordResource1" />
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="reqlbl btmpadding">
            <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqBlockWord" 
                CssClass="lblRequired" ControlToValidate="txtBlockWord"
                ErrorMessage="Please enter Block Word." runat="server" Display="Dynamic" 
                meta:resourcekey="reqBlockWordResource1"></asp:RequiredFieldValidator></div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
            <asp:Label ID="lblDescription" Text="Description" runat="server" 
                meta:resourcekey="lblDescriptionResource1"></asp:Label>
        </div>
        <div class="loginControls">
            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" 
                CssClass="textAreaMedium" meta:resourcekey="txtDescriptionResource1" />
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="reqlbl btmpadding">
        </div>
    </div>
    <div class="con_login_pnl">
        <div class="loginlbl">
        </div>
        <div class="loginControls">
            <asp:Button ID="btnSave" Text="Save" ToolTip="Save" CssClass="dynamicButton" 
                runat="server" OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" />
            <input type="button" value="Cancel" title="Cancel" onclick="Javascript:window.parent.location.href='ManagedBlockedWords.aspx';"
                class="dynamicButton" />
        </div>
    </div>
</asp:Content>
