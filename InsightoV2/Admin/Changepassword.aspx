﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="Changepassword.aspx.cs" Inherits="Insighto.Admin.Pages.Changepassword" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
<div class="contentPanelHeader"><div class="pageTitle">
    <asp:Label ID="lblTitle" runat="server" Text="Change Password" 
        meta:resourcekey="lblTitleResource1"></asp:Label></div></div>
    <div class="contentPanel">
        <div style="width: 70%; margin:0 auto 0 auto;">
            <p class="defaultHeight"></p>
            <p class="defaultHeight"></p>
            <p>
                <asp:Label ID="lblPasswordHelp" runat="server" Text=" If you change your Password here, remember to use the new Password the next time
                you log into Insighto" meta:resourcekey="lblPasswordHelpResource1"></asp:Label>
            </p>
            <p>
                &nbsp;</p>
            <p>
                <asp:Label ID="lblPasswordFormat" runat="server" Text=" Please make sure that your password is at least 6 digits and contains characters
                from two of the following categories:" 
                    meta:resourcekey="lblPasswordFormatResource1"></asp:Label>
            </p>
            <p>
                &nbsp;</p>
            <div class="lt_padding">
                <div class="icon-tick-content">
                    <asp:Label ID="lblPasswordAlphabets" runat="server" 
                        Text=" Uppercase alphabet characters (A-Z)" 
                        meta:resourcekey="lblPasswordAlphabetsResource1"></asp:Label>
                   </div>
                <div class="icon-tick-content">
                    <asp:Label ID="lblPasswordLower" runat="server" 
                        Text="Lowercase alphabet characters (a-z)" 
                        meta:resourcekey="lblPasswordLowerResource1"></asp:Label></div>
                <div class="icon-tick-content">
                    <asp:Label ID="lblPasswordDigits" runat="server" Text="Numbers (0-9)" 
                        meta:resourcekey="lblPasswordDigitsResource1"></asp:Label>
                    </div>
                <div class="icon-tick-content">
                    <asp:Label ID="lblPasswordSplChars" runat="server" Text="Special Characters" 
                        meta:resourcekey="lblPasswordSplCharsResource1"></asp:Label>
                    </div>
            </div>
        </div>
       
        <div class="clear">
        </div>
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccessMsg" runat="server" 
                    Text="Contact list has been created succesfully." 
                    meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" Text="Invalid Old Password." 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblOldPassword" Text="Old Password" AssociatedControlID="txtOldPassword"
                    runat="server" meta:resourcekey="lblOldPasswordResource1" />
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtOldPassword" runat="server" CssClass="textBoxMedium" MaxLength="100"
                    TextMode="Password" meta:resourcekey="txtOldPasswordResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqOldPassword" CssClass="lblRequired"
                    ControlToValidate="txtOldPassword" runat="server" Display="Dynamic" 
                    meta:resourcekey="reqOldPasswordResource1"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblNewPassword" Text="New Password" AssociatedControlID="txtNewPassword"
                    runat="server" meta:resourcekey="lblNewPasswordResource1" />
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtNewPassword" runat="server" CssClass="textBoxMedium" MaxLength="16"
                    TextMode="Password" meta:resourcekey="txtNewPasswordResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl btmpadding">
                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqNewPassword" CssClass="lblRequired"
                    ControlToValidate="txtNewPassword" runat="server" Display="Dynamic" 
                    meta:resourcekey="reqNewPasswordResource1"></asp:RequiredFieldValidator>
                <asp:CustomValidator SetFocusOnError="True" ID="cvNewPassword" Display="Dynamic"
                    runat="server" ControlToValidate="txtNewPassword" CssClass="lblRequired" 
                    ClientValidationFunction="validatePassword" 
                    meta:resourcekey="cvNewPasswordResource1"></asp:CustomValidator>
                <asp:CompareValidator ID="compOldpassword" Display="Dynamic" Operator="NotEqual"
                    ControlToValidate="txtNewPassword" ControlToCompare="txtOldPassword" CssClass="lblRequired"
                    Text="New password should not be same as the old password." runat="server" 
                    meta:resourcekey="compOldpasswordResource1" />
            </div>
        </div>
        <div class="con_login_pnl">
            <div class="reqlbl">
                <asp:RegularExpressionValidator SetFocusOnError="True" ID="regNewPassword" CssClass="lblRequired"
                    ControlToValidate="txtNewPassword" runat="server" 
                    ValidationExpression="^.*(?=.{6,16}).*$" meta:resourcekey="lblregNewPasswordResource1"
                    Display="Dynamic"></asp:RegularExpressionValidator></div>
        </div>
        <div class="con_login_pnl">
            <div class="loginlbl">
                <asp:Label ID="lblConfirmPassword" Text="Confirm  Password" AssociatedControlID="txtConfirmPassword"
                    runat="server" meta:resourcekey="lblConfirmPasswordResource1" />
            </div>
            <div class="loginControls">
                <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="textBoxMedium" MaxLength="16"
                    TextMode="Password" meta:resourcekey="txtConfirmPasswordResource1" />
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqConfirmPassword" CssClass="lblRequired"
                        ControlToValidate="txtConfirmPassword" runat="server" Display="Dynamic" 
                        meta:resourcekey="reqConfirmPasswordResource1"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="compConfirmPassword" Display="Dynamic" ControlToValidate="txtConfirmPassword"
                        ControlToCompare="txtNewPassword" CssClass="lblRequired" Text="Pasword not match"
                        runat="server" meta:resourcekey="compConfirmPasswordResource1" /></div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="dynamicButton" ToolTip="Save"
                        OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" />&nbsp;&nbsp;
                    <asp:Button ID="btnCancel" Text="Cancel" CssClass="dynamicButton" ToolTip="Cancel"
                        runat="server" ValidationGroup="cancel" 
                        meta:resourcekey="btnCancelResource1" /></div>
            </div>
        </div>
        <div class="clear">
        </div>
        <!-- //change password -->
    </div>
    <script type="text/javascript" language="JavaScript">
        function validatePassword(oSrc, args) {

            var l = /^\w*(?=\w*[a-z])\w*$/
            var u = /^\w*(?=\w*[A-Z])\w*$/
            var d = /^\w*(?=\w*\d)\w*$/
            var lower = /^.*(?=.*[a-z]).*$/
            var upper = /^.*(?=.*[A-Z]).*$/
            var digit = /^.*(?=.*\d).*$/
            var spl = /^.*(?=.*[@#$%^&+=]).*$/
            var cnt = 0;
            if (lower.test(args.Value)) { cnt = cnt + 1; }
            if (upper.test(args.Value)) { cnt = cnt + 1; }
            if (digit.test(args.Value)) { cnt = cnt + 1; }
            if (spl.test(args.Value)) { cnt = cnt + 1; }
            if (cnt < 2 && args.Value.length > 5) {
                args.IsValid = false;
            }
        }

      
    </script>
</asp:Content>
