﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Data;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using App_Code;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using Insighto.Exceptions;
using System.Collections;
using CovalenseUtilities.Helpers;
using System.Configuration;
using Resources;


public partial class Admin_ViewUser : BasePage
{
    #region private variables
    int userId;
    #endregion

    #region Events

    #region Page_Load
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Hashtable ht1 = new Hashtable();
        ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));

        if (ht1.Contains("UserId"))
        {
            userId = ValidationHelper.GetInteger(ht1["UserId"].ToString(), 0);

        }

        if (!IsPostBack) //&& loggedInUserInfo != null)
        {
            getUserDetails();
        }
    }
    #endregion

    #region Button Deactive Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDeactive_Click(object sender, EventArgs e)
    {
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginAdminUserDetailsSession();
        SessionStateService sessionStateService = new SessionStateService();
        var adminTrackingService = ServiceFactory.GetService<AdminTrackingServices>();
        var adminTrackInfo = new osm_admintracking();
        var userServices = new UsersService();
        var AUser = userServices.DeActivateUserByUserId(userId);
        string msg = "";
        if (txtStatus.Text == Constants.Active)
        {
            lblSuccessMsg.Text = "User De-activated succesfully.";
            dvSuccessMsg.Visible = true;
            msg = " deactivated";
        }
        if (txtStatus.Text == Constants.InActive)
        {
            dvSuccessMsg.Visible = true;
            msg = " activated";
        }

        string tracking_desc = "";
        tracking_desc = " User " + txtEmailId.Text + " had been " + msg + " by " + userInfo.FirstName + " " + userInfo.LastName + " and Tracking Id=" + ValidationHelper.GetInteger(hdnUserId.Value, 0);
        string temp = "User Activate/De-Activate";
        adminTrackInfo.ADMINID = ValidationHelper.GetInteger(userInfo.AdminUserId, 0);
        adminTrackInfo.ACTION_DONE_ON_PAGE = temp;
        adminTrackInfo.EVENT_ACTION_DESCRIPTION = tracking_desc;
        adminTrackInfo.CREATED_ON = DateTime.UtcNow;
        adminTrackInfo.FKUSERID = ValidationHelper.GetInteger(hdnUserId.Value, 0);
        var trackResult = adminTrackingService.InsertAdminTracking(adminTrackInfo);

        base.CloseModelForSpecificTime("ManageUsers.aspx", ConfigurationManager.AppSettings["TimeOut"]);

    }
    #endregion

    #region Button DashBoard Click Event
    protected void btnDashBoard_Click(object sender, EventArgs e)
    {
        base.CloseModal(EncryptHelper.EncryptQuerystring("UserDashboard.aspx", "Id=" + hdnUserId.Value));

    }
    #endregion

    #endregion

    #region Methods
    protected void getUserDetails()
    {

        var sessionService = ServiceFactory.GetService<SessionStateService>();
        //var userInfo =sessionService.GetLoginUserDetailsSession();
        if (userId != 0)
        {

            var userServices = new UsersService();
            var AUser = userServices.GetUserDetails(userId);

            txtFirstName.Text = AUser[0].FIRST_NAME;
            txtLastName.Text = AUser[0].LAST_NAME;
            txtDepartment.Text = AUser[0].DEPT;
            txtCompany.Text = AUser[0].USER_COMPANY;
            txtAddress1.Text = AUser[0].ADDRESS_LINE1;
            txtAddress2.Text = AUser[0].ADDRESS_LINE2;
            txtCity.Text = AUser[0].CITY;
            txtPostCode.Text = AUser[0].POSTAL_CODE;
            txtCountry.Text = AUser[0].COUNTRY;
            txtJobFunction.Text = AUser[0].JOB_FUNCTION;
            txtCompanySize.Text = AUser[0].COMPANY_SIZE;
            txtWorkIndustry.Text = AUser[0].WORK_INDUSTRY;
            txtState.Text = AUser[0].STATE;
            txtAccountType.Text = AUser[0].ACCOUNT_TYPE;
            txtEmailId.Text = AUser[0].EMAIL;
            txtLicenseType.Text = AUser[0].LICENSE_TYPE;
            txtLicenseExpiryDate.Text = AUser[0].LICENSE_TYPE == "FREE" ? AUser[0].LICENSE_EXPIRY_DATE.ToString() : Convert.ToDateTime(AUser[0].LICENSE_EXPIRY_DATE).AddMinutes(AUser[0].STANDARD_BIAS).ToString(); 
            txtCreatedDate.Text = Convert.ToDateTime(AUser[0].CREATED_ON).AddMinutes(AUser[0].STANDARD_BIAS).ToString();
            hdnUserId.Value = userId.ToString();// AUser[0].CUSTOMER_ID.ToString();   
            txtStatus.Text = AUser[0].STATUS;
            // Srini, 10/20/2013, Spam User Check
            txtIPAddress.Text = AUser[0].IP_ADDRESS;

            if (txtStatus.Text == Constants.Active)
            {
                btnDeactive.Text = Constants.Deactivate;
                btnDeactive.ToolTip = Constants.Deactivate;
            }
            if (txtStatus.Text == Constants.InActive)
            {
                btnDeactive.Text = Constants.Activate;
                btnDeactive.ToolTip = Constants.Activate;  
            }

            if (AUser[0].PHONE != null)
            {
                string phone = AUser[0].PHONE;
                string coutrycode = string.Empty;
                string regioncode = string.Empty;
                int index = phone.IndexOf(Constants.PHONE_SEPARATER);
                if (index >= 0)
                {
                    coutrycode = phone.Substring(0, index);
                    phone = phone.Substring(index + 1);
                    index = phone.IndexOf(Constants.PHONE_SEPARATER);
                    regioncode = phone.Substring(0, index);
                    regioncode = phone.Substring(0, index);
                    phone = phone.Substring(index + 1);
                }
                txtPhone.Text = coutrycode;
                txtPhone1.Text = regioncode;
                txtPhone2.Text = phone;
            }

        }
        else
        {
            //Response.Redirect(PathHelper.GetUserLoginPageURL());        
        }

        // Srini, 10/21/2013, IP Country and City, other details available as well
        string country = Utilities.GetCountryName(txtIPAddress.Text);
        string city = Utilities.GetCityName(txtIPAddress.Text);
        txtIPCityCountry.Text = city + "/" + country;
       
    }
    #endregion

    protected void btnChkIPAddress_Click(object sender, EventArgs e)
    {
        // Srini, 10/20/2013, Spam User Check
        if (txtIPAddress.Text.Length == 0)
            return;

        string ipCheckResult = HttpBlQuerier.Lookup(txtIPAddress.Text);
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('" + ipCheckResult + "')", true);
    }

    protected void btncustomeraccount_Click(object sender, EventArgs e)
    {

        UsersService userService = new UsersService();
        var user = userService.FindUserByUserId(userId);
        userService.ProcessUserLogin(user);



        var userServices = new UsersService();
        var AUser = userServices.GetUserDetails(userId);

        var userService1 = ServiceFactory.GetService<UsersService>();
        //Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
        userService1.ProcessUserLogin(user);

        if (user.TIME_ZONE == null) //Timezone is null for first user login. hence redirecting to welcome page
        {
            //create folder with userid 
            //CreateUserFolders(user); this function is moved to register free user .. as every user must have this.
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
        }
        else if (user.RESET == 1) // Password reset value is 1 for first login. hence redirectiong to chanepassword page
        {
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserChangePasswordPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
        }
        else // on successful login user is redirected to Myaccounts page
        {
            Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
        }
    }
    
    protected void btnAddCredits_Click(object sender, EventArgs e)
    {
        int creditloop = 0;
        int creditsuccess = 0;
        int creditfailure = 0;
        string creditstatus = "";
        try
        {
            Int32.TryParse(creditscount.SelectedValue.ToString(), out creditloop);
            if (creditloop > 0)
            {
                
                UsersService userService = new UsersService();
                var user = userService.FindUserByUserId(userId);
                userService.ProcessUserLogin(user);
                SurveyCore survcore = new SurveyCore();
                for (int x = 1; x <= creditloop; x++)
                {
                    string status = survcore.AddSurveyCredit(user.USERID, creditlicensetype.SelectedValue);
                    if (status == "Error")
                    {
                        creditfailure++;
                    }
                    else
                    {
                        creditsuccess++;
                    }
                }
                
            }
        }
        catch (Exception)
        {
            creditfailure = -1;
            creditsuccess = 0;
        }
        if (creditfailure == -1)
        {
            creditstatus = "ERROR! No credits were added.";
        }
        if (creditsuccess > 0)
        {
            creditstatus = creditsuccess.ToString() + " credits were successfully added";
        }
        else if (creditfailure > 0)
        {
            creditstatus += creditfailure.ToString() + " credits could not be added";
        }
        addcreditstatus.Text = creditstatus;
    }
}