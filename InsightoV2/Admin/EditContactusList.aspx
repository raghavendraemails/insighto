﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="EditContactusList.aspx.cs" Inherits="Insighto.Admin.Pages.EditContactusList" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                <asp:Label ID="lblTitle" runat="server" Text="Message Details" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>
               </h3>
        </div>
        <%--<div class="popupClosePanel"><a href="#" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(false,'');"> </a></div>--%>
    </div>
    <div class="clear"></div>
    <div class="defaultHeight">
    </div>
   <div>
    <table border="0" cellpadding="5" cellspacing="2" width="100%">
        <tr>
            <td class="wid_30" valign="top"><asp:Label ID="lblUserName" Text="Name :" 
                    runat="server" meta:resourcekey="lblUserNameResource1"></asp:Label></td>
            <td class="wid_70"><b><asp:Label ID="lblUserNameText" runat="server" 
                    meta:resourcekey="lblUserNameTextResource1"></asp:Label></b></td>
        </tr>
        <tr>
            <td class="wid_30" valign="top"><asp:Label ID="lblMailMessage" Text="Message :" 
                    runat="server" meta:resourcekey="lblMailMessageResource1"></asp:Label></td>
            <td class="wid_70"><b><asp:Label ID="lblMailMessagetext" runat="server" 
                    meta:resourcekey="lblMailMessagetextResource1"></asp:Label></b></td>
        </tr>
    </table>
   </div>
 
   
</asp:Content>

