﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="LicenseManagement.aspx.cs" Inherits="Insighto.Pages.Admin.LicenseManagement" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
  
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <input id="hdnStatus" class="hdnStatus" type="hidden" value="All" />
        <div class="pageTitle">
            <!-- page title -->
            <span>
                <asp:Label ID="lblTitle" runat="server" Text="License Management" 
                meta:resourcekey="lblTitleResource1"></asp:Label></span>
            <!-- //page title -->
        </div>
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <!-- //form header panel -->
      <div class="successPanel" id="divSuccess" style="display: none">
            <div>
                <%-- <label id="lblMessage" />--%>
                <asp:Label ID="lblMessage" CssClass="successMsg" Text="Deleted Successfully." 
                    runat="server" Style="display: none;" meta:resourcekey="lblMessageResource1"></asp:Label>
            </div>
        </div>
        <div class="surveyCreateTextPanel">
            <!-- admin content panel -->
            <div class="cmsPagesMenu">
                <!-- license management pages list -->
                <h2><asp:Label ID="lblHeading" runat="server" Text="Feature Groups" 
                        meta:resourcekey="lblHeadingResource1"></asp:Label>
                    </h2>
                
                <div class="defaultHeight">
                </div>
                <div class='suckerdiv'>
                    <asp:TreeView ID="tvLicenseManagement" runat="server" NodeWrap="True" 
                        OnSelectedNodeChanged="tvLicenseManagement_SelectedNodeChanged" 
                        meta:resourcekey="tvLicenseManagementResource1" >
                        <RootNodeStyle ChildNodesPadding="5px" />
                        <LeafNodeStyle CssClass="leafNode" />
                    </asp:TreeView>
                </div>
                <!-- //license management pages list -->
            </div>
            <div class="cmsContentPanel">
                <!-- survey question panel -->
                <div class="pickListCategoryTop">
                    <div class="pickListTitel">
                        <asp:Label ID="lblFeatureGroup" runat="server" Text=" Feature Group  :" 
                            meta:resourcekey="lblFeatureGroupResource1"></asp:Label>
                        <asp:Label ID="lblGroupName" runat="server" 
                            meta:resourcekey="lblGroupNameResource1"></asp:Label>
                    </div>
                    <div class="pickListAddBtnPanel">
                     <asp:HyperLink ID="hlnkAddList" CssClass="dynamicHyperLink" rel="framebox" h="550" w="550"
                            scrolling='yes' title='Copy' onclick='javascript: return ApplyFrameBox(this);'
                            ToolTip="Add List" runat="server" Text="Add" 
                            meta:resourcekey="hlnkAddListResource1"></asp:HyperLink>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div>
                    <!-- text here -->
                    <div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="surveysGridPanel">
                        <div>
                            <div id="ptoolbar">
                            </div>
                            <table id="tblCommonFeature">
                            </table>
                            <asp:HiddenField ID="hdnGroupName" runat="server" />
                        </div>
                    </div>
                    <!-- //text here -->
                </div>
                <!-- //survey question panel -->
            </div>
            <div class="clear">
            </div>
        </div>
        <!-- //content panel -->
    </div>
    <script type="text/javascript">

        var serviceUrl = '../AjaxService.aspx';
        var featureGroup = '<%= FeatureGroup %>'
        $(document).ready(function () {

            $("#tblCommonFeature").jqGrid({
                url: serviceUrl,
                postData: { method: "GetFeaturesByGroup", featureGroup: featureGroup },
                datatype: 'json',
                colNames: ['Feature Name', 'Free', 'Pro Monthly', 'Pro Quarterly', ''],
                colModel: [
                { name: 'FeatureName', index: 'Feature_Name', width: 100, resizable: false },               
                { name: 'Free', index: 'Free', width: 75, align: 'center', resizable: false },
                { name: 'ProMonthly', index: 'Pro_Monthly', width: 75, align: 'center', resizable: false },
                { name: 'ProQuaterly', index: 'Pro_Quarterly', width: 75, align: 'center', resizable: false },
                { name: 'EditLinkUrl', index: 'EditLinkUrl', width: 20, align: 'center', resizable: false, formatter: EditLinkFormatter, sortable: false },
               
                 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'Feature_Name',
                sortorder: "asc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 630,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblCommonFeature").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false,refresh:false });



        });

        function EditLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='" + cellvalue + "' rel='framebox' w='550' h='550' ><img src='../App_Themes/Classic/Images/icon-pen-active.gif' title='Edit'/></a>";
        }

        function ViewLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='" + cellvalue + "' rel='framebox' w='700' h='350'  scrolling='yes'><img src='../App_Themes/Classic/Images/icon-percent-active.gif' title='Past History'/></a>";
        }


        function DeleteLinkFormatter(cellvalue, options, rowObject) {
            //return "<a href='#' class='button-small-gap' id='lnkDelete' onclick='javascript: return DeleteContact(" + cellvalue + ");'><img src='App_Themes/Classic/Images/icon-delete-active.gif' title='Delete' /></a>";
            return "<a href='#' class='button-small-gap' id='lnkDelete' onclick='javascript: return DeleteFeature(" + cellvalue + ");'><img src='../App_Themes/Classic/Images/icon-delete-active.gif' title='Delete' /></a>";
        }

        function gridReload() {
            var keyword = $("#txtKeyword").val();
            $("#tblCommonFeature").jqGrid('setGridParam', { url: serviceUrl + "?keyword=" + keyword, page: 1 }).trigger("reloadGrid");
        }


        function DeleteFeature(featureID) {
            var answer = confirm('Are you sure! you want to delete?');
            if (answer) {
                $.post("../AjaxService.aspx", { "method": "DeleteFeatureById", "Id": featureID },

                    function (result) {
                       
                        if (result == '1') {
                            $('#divSuccess').show();
                            $('.successMsg').show()
                            $('.successMsg').html('Feature deleted successfully.');
                            var status = $('#hdnStatus').val();
                            gridReload(status);
                        }
                        else {
                            $('#divSuccess').hide();
                            $('.successMsg').hide()
                        }
                    });                 
                  
                  
//                  function (result) {
//                      if (result >= '1') {
//                          $('.successPanel').show();
//                          $('.successMsg').show();
//                          $('.successMsg').html('Feature deleted successfully.');
//                          gridReload();
//                      }

//                  });
            }
        }
     

        
    </script>
</asp:Content>
