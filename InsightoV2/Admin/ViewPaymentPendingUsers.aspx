﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="ViewPaymentPendingUsers.aspx.cs" Inherits="Admin_ViewPaymentPendingUsers" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text="Payment Pending Users" 
                meta:resourcekey="lblTitleResource1"></asp:Label>
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="errorPanel" id="recordDeleted" style="display: none;">
            <span class="successMesg">
                <asp:Label ID="lblDeleted" runat="server" 
                Text="Record deleted successfully" meta:resourcekey="lblDeletedResource1"></asp:Label></span>
        </div>
        <div class="surveyCreateTextPanel">
            <!-- admin content panel -->
            <div class="adminGridSearchPanel">
                <div class="searchLblelong">
                    <asp:Label ID="lblHeading" runat="server" 
                        Text="List of Users Pending For Payment" meta:resourcekey="lblHeadingResource1"></asp:Label></div>
                <div class="searchrghtPanel">
                    <asp:HyperLink ID="hlnkAddList" CssClass="dynamicHyperLink" NavigateUrl="LicenseUpgrade.aspx"
                        ToolTip="License Upgrade Users" runat="server" 
                        Text="License Upgrade Users" meta:resourcekey="hlnkAddListResource1"></asp:HyperLink>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="adminUserGridViewPanel" style="display: block;">
                <div id="ptoolbar">
                </div>
                <table id="tblAddressBook">
                </table>
            </div>
            <!-- //admin content panel -->
        </div>
        <div class="clear">
        </div>
    </div>
    <script type="text/javascript">

        var serviceUrl = '../AjaxService.aspx';
        $(document).ready(function () {
            $("#tblAddressBook").jqGrid({
                url: serviceUrl,
                postData: { method: "FindPaymentPendingUsers", paymentType: "Pending" },
                datatype: 'json',
                colNames: ['Name', 'Email Id', 'Telephone', 'License Type', 'Approve'],
                colModel: [
                { name: 'FIRST_NAME', index: 'FIRST_NAME', width: 200, resizable: false },
                { name: 'LOGIN_NAME', index: 'LOGIN_NAME', width: 380, align: 'left', resizable: false },
                { name: 'PHONE', index: 'PHONE', width: 100, align: 'left' },
                { name: 'LICENSE_TYPE', index: 'LICENSE_TYPE', width: 100, align: 'center', resizable: false },
                { name: 'UpgradeLinkUrl', index: 'UpgradeLinkUrl', width: 100, align: 'center', resizable: false, formatter: UpgradeLinkFormatter, sortable: false },
                 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'FIRST_NAME',
                sortorder: "asc",
                viewrecords: true,

                jsonReader: { repeatitems: false },
                width: 880,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblAddressBook").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
            $("#tblAddressBook").jqGrid('setLabel', 'FIRST_NAME', '', 'textalignleft');
            $("#tblAddressBook").jqGrid('setLabel', 'LOGIN_NAME', '', 'textalignleft');
        });
        
        function UpgradeLinkFormatter(cellvalue, options, rowObject) {

            var finalCellValue = cellvalue + "&Page=P";
            return "<a href='" + finalCellValue + "' rel='framebox' w='760' h='480' scrolling='true' >Approve</a>";
        }

        function gridReload() {
           $("#tblAddressBook").jqGrid('setGridParam', { url: serviceUrl, page: 1 }).trigger("reloadGrid");
        }

       

     

        $("#txtSearch").bind('keyup', function () { gridReload(); });           
              
    </script>
</asp:Content>
