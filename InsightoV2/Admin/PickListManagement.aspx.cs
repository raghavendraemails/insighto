﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using System.Collections;


namespace Insighto.Pages.Admin
{
    public partial class PickListManagement : BasePage
    {
        Hashtable ht = new Hashtable();
        private List<string> _value = new List<string> { string.Empty };
        private string _Category = string.Empty;
        private string _DisplayMode = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCategories();
                if (Request.QueryString["Category"] != null)
                {
                  hdnCategory.Value =Request.QueryString["Category"].ToString();

                  foreach (TreeNode node in tvCategory.Nodes)
                  {
                      if (node.Value == hdnCategory.Value)
                      {
                          node.Selected = true;
                          tvCategory.SelectedNodeStyle.CssClass = "active";
                          break; 
                      }
                  }

                }
                string url = EncryptHelper.EncryptQuerystring("AddPickList.aspx", "Category=" + hdnCategory.Value);
                hlnkAddList.NavigateUrl = url;
                lblCategory.Text = hdnCategory.Value;
               
            }
        }
      
        public string DisplayMode
        {
            get
            {
                return _DisplayMode;
            }
            set
            {
                _DisplayMode = value;
            }
        }
        public string Category
        {
            get
            {
                _Category = hdnCategory.Value;
                return _Category;
            }
        }

        private void BindCategories()
        {
            var pickListService = ServiceFactory.GetService<PickListService>();
            var category = pickListService.FindAllList();
            TreeNode parentNode = null;
            var cat = (from c in category
                                orderby c.CATEGORY ascending
                                group c.CATEGORY by c.CATEGORY into cateogoryList
                                    select cateogoryList);
            foreach (var item in cat)
            {
                if (hdnCategory.Value == "")
                    hdnCategory.Value = item.Key;
                parentNode = new TreeNode(item.Key, item.Key);
                if (item.Key != null)
                {
                    parentNode.Collapse();
                    tvCategory.Nodes.Add(parentNode);
                }
            }
            
        }

        protected void tvCategory_SelectedNodeChanged(object sender, EventArgs e)
        {
            hdnCategory.Value = tvCategory.SelectedNode.Value;          
            lblCategory.Text = hdnCategory.Value;
            tvCategory.SelectedNodeStyle.CssClass = "active";
            string url = EncryptHelper.EncryptQuerystring("AddPickList.aspx", "Category=" + hdnCategory.Value);
            hlnkAddList.NavigateUrl = url;
        }
    }
}