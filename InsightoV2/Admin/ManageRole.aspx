﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="ManageRole.aspx.cs" Inherits="Insighto.Pages.Admin.ManageRole" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="contentPanel">
            <div class="adminGridSearchPanel">
               <%-- <div class="searchLble">
                    Search</div>
                <div class="searchtxtcontrols">
                    <input type="text" class="textBoxAdminSearch" id="txtSearch" />
                </div>--%>
                <div class="searchrghtPanel">
                 
                    <asp:HyperLink ID="hlnkAddUser" CssClass="dynamicHyperLink" NavigateUrl="AddRole.aspx"
                        rel="framebox" h="250" w="550" scrolling='yes' title='Add Role' onclick='javascript: return ApplyFrameBox(this);'
                        ToolTip="Add Role" Text="Add Role"  runat="server" ></asp:HyperLink>
                </div>
                <div class="clear">
                </div>
            </div>
             <div class="manageAdminRole" style="display: block;">
                <div id="ptoolbar">
                </div>
                <table id="tblMangeRole">
                </table>
            </div>
            <!-- //admin content panel -->
       
        <div class="clear">
        </div>
  
</div>
    <script type="text/javascript">

        var serviceUrl = '../AjaxService.aspx';
        $(document).ready(function () {
            $("#tblMangeRole").jqGrid({
                url: serviceUrl,
                postData: { method: "GetRoles" },
                datatype: 'json',
                colNames: ['Role Name',  ''],
                colModel: [
                { name: 'ROLE_NAME', index: 'ROLE_NAME', width: 250, resizable: false },
                { name: 'EditLinkUrl', index: 'EditLinkUrl', width: 130, align: 'center', resizable: false, formatter: EditLinkFormatter, sortable: false },
                ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'ROLE_NAME',
                sortorder: "asc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 680,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblMangeRole").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
             $("#tblMangeRole").jqGrid('setLabel', 'ROLE_NAME', '', 'textalignleft');
        });

        function EditLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='" + cellvalue + "' rel='framebox' w='550' h='250' ><img src='../App_Themes/Classic/Images/icon-pen-active.gif' title='Edit'/></a>";
        }
        function gridReload() {
            var keyword = $("#txtSearch").val();
            $("#tblMangeRole").jqGrid('setGridParam', { url: serviceUrl + "?keyword=" + keyword, page: 1 }).trigger("reloadGrid");
        }
 
 
        $("#txtSearch").bind('keyup', function () { gridReload(); });           
              
    </script>

</asp:Content>

