﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="AddAdminUser.aspx.cs" Inherits="Insighto.Admin.Pages.AddAdminUser"
    meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                <asp:Label runat="server" ID="lblCreate" Text="ADD ADMIN USER" Visible="False"></asp:Label>
                <asp:Label runat="server" ID="lblEdit" Text="EDIT ADMIN USER" Visible="False"></asp:Label>
            </h3>
        </div>
       <%-- <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick ="closeModalSelf(true,'ManageAdmin.aspx');" title="Close">&nbsp;</a>
        </div>--%>
      
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupContentPanel">
        <!-- popup content panel -->
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccessMsg" runat="server" Text="Admin user details saved successfully."
                    meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" Text="User already exist." meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <div class="lt_padding">
                                <asp:Label ID="requiredfield" CssClass="left_requirelbl" Text="indicates required fields"
                                    runat="server"></asp:Label></div>
        <div class="defaultHeight">
        </div>
        <div class="formPanel">
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="lblAdminType" runat="server" Text="Select Admin Type" meta:resourcekey="lblAdminTypeResource1"></asp:Label>
                </div>
                <div class="loginControls">
                    <asp:DropDownList ID="ddlAdminType" CssClass="dropdownMedium" runat="server" meta:resourcekey="ddlAdminTypeResource1">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="lblFirstName" CssClass="requirelbl" runat="server" Text="First Name" meta:resourcekey="lblFirstNameResource1"></asp:Label>
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="textBoxMedium" MaxLength="30"
                        meta:resourcekey="txtFirstNameResource1"></asp:TextBox>
              
                </div>
            </div>

              

          
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqFirstName" CssClass="lblRequired"
                        ControlToValidate="txtFirstName" ErrorMessage="Please enter first name." runat="server"
                        Display="Dynamic" meta:resourcekey="reqFirstNameResource1"></asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator SetFocusOnError="true"  ID="regName" CssClass="lblRequired" ControlToValidate="txtFirstName"
                   runat="server" ErrorMessage="Please enter alphabets only." ValidationExpression="^[a-zA-Z ]*$" Display="Dynamic">
                </asp:RegularExpressionValidator> 
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="lblLastName" CssClass="requirelbl" runat="server" Text="Last Name" meta:resourcekey="lblLastNameResource1"></asp:Label>
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="txtLastName" runat="server" CssClass="textBoxMedium" MaxLength="30"
                        meta:resourcekey="txtLastNameResource1"></asp:TextBox>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqLastName" CssClass="lblRequired"
                        ControlToValidate="txtLastName" ErrorMessage="Please enter last name." runat="server"
                        Display="Dynamic" meta:resourcekey="reqLastNameResource1"></asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator SetFocusOnError="true"  ID="RegularExpressionValidator1" CssClass="lblRequired" ControlToValidate="txtLastName"
                   runat="server" ErrorMessage="Please enter alphabets only." ValidationExpression="^[a-zA-Z ]*$" Display="Dynamic">
                </asp:RegularExpressionValidator> 
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="lblPhone" CssClass="requirelbl" runat="server" Text="Phone Number" meta:resourcekey="lblPhoneResource1"></asp:Label>
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="txtCountryCode" runat="server" CssClass="phoneCountry" MaxLength="4"
                        onkeypress="javascript:return onlyNumbers(event,'Countrycode');" onpaste="return false;"
                        meta:resourcekey="txtCountryCodeResource1"></asp:TextBox>
                    -
                    <asp:TextBox ID="txtAreaCode" runat="server" CssClass="phoneArea" MaxLength="5"
                        onkeypress="javascript:return onlyNumbers(event,'Countrycode');" onpaste="return false;"
                        meta:resourcekey="txtAreaCodeResource1"></asp:TextBox>
                    -
                    <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="phoneNumber" MaxLength="10"
                        onkeypress="javascript:return onlyNumbers(event,'Countrycode');" onpaste="return false;"
                        meta:resourcekey="txtPhoneNumberResource1"></asp:TextBox>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqPhone" CssClass="lblRequired"
                        ControlToValidate="txtPhoneNumber" ErrorMessage="Please enter phone number" runat="server"
                        Display="Dynamic" meta:resourcekey="reqPhoneResource1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator SetFocusOnError="True" ID="regPhone" CssClass="lblRequired"
                        ControlToValidate="txtPhoneNumber" runat="server" ValidationExpression="^.*(?=.{1,10})(?=.*\d).*$"
                        ErrorMessage="Please enter 1-10 numeric values" Display="Dynamic" meta:resourcekey="regPhoneResource1"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator SetFocusOnError="True" ID="regPhoneArea" CssClass="lblRequired"
                        ControlToValidate="txtAreaCode" runat="server" ValidationExpression="^.*(?=.{0,5})(?=.*\d).*$"
                        ErrorMessage="Please enter numbers only" Display="Dynamic" meta:resourcekey="regPhoneAreaResource1"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator SetFocusOnError="True" ID="regCountryCode" CssClass="lblRequired"
                        ControlToValidate="txtCountryCode" runat="server" ValidationExpression="^.*(?=.{0,4})(?=.*\d).*$"
                        ErrorMessage="Please enter numbers only" Display="Dynamic" meta:resourcekey="regCountryCodeResource1"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <span id="phoneText" class="note">Country Code - Area Code -- Phone Number.</span>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="lblEmailId" CssClass="requirelbl" runat="server" Text="Email Id" meta:resourcekey="lblEmailIdResource1"></asp:Label>
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="txtEmailId" runat="server" CssClass="textBoxMedium" MaxLength="40"
                        meta:resourcekey="txtEmailIdResource1"></asp:TextBox>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqEmailId" ControlToValidate="txtEmailId"
                        CssClass="lblRequired" ErrorMessage="Please enter email id." runat="server" Display="Dynamic"
                        meta:resourcekey="reqEmailIdResource1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator SetFocusOnError="True" ID="regEmailId" runat="server"
                        ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                        CssClass="lblRequired" ControlToValidate="txtEmailId" ErrorMessage="Please enter valid email id."
                        Display="Dynamic" meta:resourcekey="regEmailIdResource1"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div id="dvPassword" runat="server">
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblPassword" CssClass="requirelbl" runat="server" Text="Password" meta:resourcekey="lblPasswordResource1"></asp:Label>
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="textBoxMedium"
                            MaxLength="16" meta:resourcekey="txtPasswordResource1"></asp:TextBox>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqNewPassword" CssClass="lblRequired"
                            ControlToValidate="txtPassword" ErrorMessage="Please enter password." runat="server"
                            Display="Dynamic" meta:resourcekey="reqNewPasswordResource1"></asp:RequiredFieldValidator>
                           <asp:RegularExpressionValidator SetFocusOnError="true" ID="regNewPassword" CssClass="lblRequired"
                                            ControlToValidate="txtPassword" runat="server" meta:resourcekey="lblLenPasswordResource2"
                                            ValidationExpression="^.{6,16}$" Display="Dynamic"></asp:RegularExpressionValidator>

                              <asp:CustomValidator SetFocusOnError="true"  ID="cvNewPassword" Display="Dynamic" runat="server" ControlToValidate="txtPassword"
                            CssClass="lblRequired" meta:resourcekey="lblcvNewPasswordResource1" ClientValidationFunction="validatePassword">
                        </asp:CustomValidator>
                    </div>
                </div>
                <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <span id="Span1" class="note">6-16 characters. Special character or number or uppercase letter or lowercase letter is mandatory.</span>
                </div>
            </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblConfirmPassword" CssClass="requirelbl"  runat="server" Text="Confirm Password" meta:resourcekey="lblConfirmPasswordResource1"></asp:Label>
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" CssClass="textBoxMedium"
                            meta:resourcekey="txtConfirmPasswordResource1"></asp:TextBox>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                       <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqConfirmPassword" CssClass="lblRequired"
                            ControlToValidate="txtConfirmPassword" ErrorMessage="Please enter confirm password."
                            runat="server" Display="Dynamic" meta:resourcekey="reqConfirmPasswordResource1"></asp:RequiredFieldValidator>
                    
                        <asp:CompareValidator ID="compConfirmPassword" Display="Dynamic" ControlToValidate="txtConfirmPassword"
                            ControlToCompare="txtPassword" CssClass="lblRequired" Text="Password and confirm  password did not match."
                            runat="server" SetFocusOnError="True" meta:resourcekey="compConfirmPasswordResource1" />
                    </div>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                </div>
                <div class="loginControls">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="dynamicButton" ToolTip="Save"
                        OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" />
                    <asp:Button ID="btnSaveNew" runat="server" Text="Save & Add New" CssClass="dynamicButton"
                        OnClick="btnSaveNew_Click" ToolTip="Save & Add New" meta:resourcekey="btnSaveNewResource1" />
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click"
                        ToolTip="Update" CssClass="dynamicButton" meta:resourcekey="btnUpdateResource1" />
                    <%-- <asp:Button ID="btnCancel" runat="server" Text="Cancel" ValidationGroup="Cancel"
                        OnClick="btnCancel_Click" ToolTip="Cancel" CssClass="dynamicButton" 
                        meta:resourcekey="btnCancelResource1" />--%>
                </div>
            </div>
            <!-- form -->
            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>
    <script type="text/javascript">
        function onlyNumbers(e, Type) {
            var key = window.event ? e.keyCode : e.which;
            if (Type == 'Countrycode' && key == 43) {
                return true;
            }
            else if (key > 31 && (key < 48 || key > 57)) {
                return false;
            }
            return true;
        }

        function validatePassword(oSrc, args) {

//            var l = /^\w*(?=\w*[a-z])\w*$/
//            var u = /^\w*(?=\w*[A-Z])\w*$/
//            var d = /^\w*(?=\w*\d)\w*$/
            var lower = /^.*(?=.*[a-z]).*$/ ;
            var upper = /^.*(?=.*[A-Z]).*$/ ;
            var digit = /^.*(?=.*\d).*$/;
            var spl = /^.*(?=.*[@#$%^&+=]).*$/ ;
            var cnt = 0;
            if (lower.test(args.Value)) { cnt = cnt + 1; }
            if (upper.test(args.Value)) { cnt = cnt + 1; }
            if (digit.test(args.Value)) { cnt = cnt + 1; }
            if (spl.test(args.Value)) { cnt = cnt + 1; }
            if (cnt < 2 && args.Value.length > 5) {
                args.IsValid = false;
            }
        }
    </script>
   
</asp:Content>
