﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="Insighto.Admin.Pages.ForgotPassword" meta:resourcekey="PageResource1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                <asp:Label ID="lblTitle" runat="server" Text="Forgot Password" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>
                </h3>
        </div>
      <%--  <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick="closeModalSelf(false,'');" title="Close">&nbsp;</a>
        </div>--%>
        <!-- //popup heading -->
    </div>
    <div class="popupContentPanel">
    <p class="defaultHeight"></p>
        <!-- popup content panel -->
        
        <div class="formPanel">
            <!-- form -->
            <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">

                <div> <asp:Label ID="lblSuccessMsg"   runat="server"  
                        Text="Your username and password have been sent to the email address." 
                        meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
           </div>
           <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">                
                <div><asp:Label ID="lblErrMsg" runat="server"   
                        Text="Email id does not exist / acccount is not activated yet! Please try again." 
                        meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
            </div>
        </div>
           
        <div class="clear">
        </div>
        <div class="informationPanelDefault">
            <div>
            <p>
               <asp:Label ID="lblInformation" runat="server" Text=" Please enter your registered email address. We will reset your password and send
                 it to your email address." meta:resourcekey="lblInformationResource1"></asp:Label>
            </p>
                
            </div>            
        </div>
                
                <p>
                    &nbsp;</p>
                <div class="con_login_pnl">
                    <div class="forgotlbl">
                        <asp:Label ID="lblEmail" AssociatedControlID="txtEmail" runat="server" 
                            Text="Enter Email Id" meta:resourcekey="lblEmailResource1" /></div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="textBoxMedium" 
                            MaxLength="50" meta:resourcekey="txtEmailResource1" /></div>
                </div>
              <div class="con_login_pnl">
                   <div class="reqlbl btmpadding">
                     <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqEmailId" 
                           ControlToValidate="txtEmail" CssClass="lblRequired"
                        meta:resourcekey="ReqEmailIdResource1" runat="server" Display="Dynamic" 
                           ErrorMessage = "Please enter email id."></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator SetFocusOnError="True"  ID="regEmailId" 
                           runat="server" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                        CssClass="lblRequired" ControlToValidate="txtEmail" meta:resourcekey="RegEmailIdResource1"
                        Display="Dynamic" 
                           ErrorMessage = "Please enter email id in valid format. Ex: john@insighto.com"></asp:RegularExpressionValidator></div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblbtnNewFolder" runat="server" 
                            AssociatedControlID="btnResetPassword" 
                            meta:resourcekey="lblbtnNewFolderResource1" /></div>
                    <div class="loginControls">
                        <asp:Button ID="btnResetPassword" runat="server" CssClass="btn_medium" OnClick="btnResetPassword_Click"
                            Text="Reset Password" ToolTip="Reset Password" 
                            meta:resourcekey="btnResetPasswordResource1" /></div>
                </div>
                <div class="clear">
                </div>
            </div>
</asp:Content>

