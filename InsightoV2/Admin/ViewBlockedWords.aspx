﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="ViewBlockedWords.aspx.cs" Inherits="Admin_ViewBlockedWords" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
   <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text=" Manage Blocked Words" 
                meta:resourcekey="lblTitleResource1"></asp:Label>
           
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div><div class="contentPanel">
    <div class="surveyCreateTextPanel">
     <div class="pickListCategoryTop">
                <div class="pickListTitel">
                    <asp:Label ID="lblHeading" runat="server" Text=" View Blocked Words List" 
                        meta:resourcekey="lblHeadingResource1"></asp:Label>
                </div>
                <div class="pickListAddBtnPanel">
                   <asp:HyperLink ID="hlnkManageBlocked" CssClass="dynamicHyperLink" NavigateUrl="ManagedBlockedWords.aspx"
                        ToolTip="Manage Blocked Words" runat="server" Text="Manage Blocked Words" meta:resourcekey="hlnkManageBlockedResource1" 
                       ></asp:HyperLink>
                    
                </div>
                <div class="clear"></div>
            </div>
             <div class="defaultH">
            </div>
            <div id="ptoolbar">
            </div>
            <table id="tblViewBlockedWordsList">
            </table>
            <div class="defaultH">
            </div>
            </div></div>
           <script type="text/javascript">
               var serviceUrl = '../AjaxService.aspx';
               $(document).ready(function () {
                   $("#tblViewBlockedWordsList").jqGrid({
                       url: serviceUrl,
                       postData: { method: "ViewBlockedWordList" },
                       datatype: 'json',
                       colNames: ['Login Name', 'User Name', 'Survey Name', 'View Blocked Word List', 'Preview', 'Legitimate'],
                       colModel: [
                { name: 'LoginName', index: 'LoginName', width: 35, resizable: false },
                { name: 'UserName', index: 'UserName', width: 35, align: 'left', resizable: false },
                { name: 'SurveyName', index: 'SurveyName', width: 30, align: 'left', resizable: false },
                { name: 'EditUrl', index: 'EditUrl', width: 30, resizable: false, align: 'center', formatter: EditLinkFormatter, sortable: false },
                { name: 'Preview', index: 'Preview', width: 25, align: 'center', resizable: false, formatter: ViewLinkFormatter, sortable: false },
                { name: 'Legitimate', index: 'SurveyId', width: 25, align: 'center', resizable: false,formatter:ReleaseLinkFormatter,sortable:false},

                 ],
                       rowNum: 10,
                       rowList: [10, 20, 30, 40, 50],
                       pager: '#ptoolbar',
                       gridview: true,
                       sortname: 'LoginName',
                       sortorder: "asc",

                       viewrecords: true,
                       jsonReader: { repeatitems: false },
                       width: 875,
                       caption: '',
                       height: '100%',
                       loadComplete: function () {
                           $('a[rel*=framebox]').click(function (e) {
                               e.preventDefault();
                               ApplyFrameBox($(this));
                           });
                       }
                   });
                   $("#tblViewBlockedWordsList").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
                   $("#tblViewBlockedWordsList").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');
               });

               function gridReload() {
                $("#tblViewBlockedWordsList").jqGrid('setGridParam', { url: serviceUrl, page: 1 }).trigger("reloadGrid");
               }

               function EditLinkFormatter(cellvalue, options, rowObject) {
                   return "<a href='" + cellvalue + "' rel='framebox' w='700' h='480' scrolling='true'>View</a>";
               }
               function ViewLinkFormatter(cellvalue, options, rowObject) {

                   return "<a href='" + cellvalue + "' rel='framebox' w='700' h='480' scrolling='true'>Preview</a>";
               }
               function ReleaseLinkFormatter(cellvalue, options, rowObject) {
                   return "<a href='#' class='button-small-gap' id='lnkUpgrade' onclick='javascript: return Upgrade(" + cellvalue + ");'><img src='../App_Themes/Classic/Images/icon-legitimate.gif' title='Legitimate'/></a>";
               }
               function Upgrade(surveyId) {
                   var answer = confirm('Are you sure you want to Legitimate survey ?');
                   if (answer) {
                       $.post("../AjaxService.aspx", { "method": "UpdateSurveyStatus", "surveyId": surveyId },
                  function (result) {
                      if (result == '1') {
                          gridReload();
                      }
                  });
                   }
               }

           </script>  
</asp:Content>

