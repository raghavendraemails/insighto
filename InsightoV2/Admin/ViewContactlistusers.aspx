﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="ViewContactlistusers.aspx.cs" Inherits="Admin_ViewContactlistusers" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
 <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text="View Contact user list" 
                meta:resourcekey="lblTitleResource1"></asp:Label>
            <!-- //page title -->
        </div>
    
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div><div class="contentPanel">
    <div class="surveyCreateTextPanel">
  
            <div id="ptoolbar">
            </div>
            <table id="tblViewContactList">
            </table>
            <div class="defaultH">
            </div>
            </div></div>
           <script type="text/javascript">
               var serviceUrl = '../AjaxService.aspx';
               $(document).ready(function () {
                   $("#tblViewContactList").jqGrid({
                       url: serviceUrl,
                       postData: { method: "GetContactuserList" },
                       datatype: 'json',
                       colNames: ['Name', 'Email Address', 'Phone No', 'Attention Value', 'Created On', 'Mail Message',''],
                       colModel: [
                { name: 'USERNAME', index: 'USERNAME', width: 35, align: 'left', resizable: false },
                { name: 'EMAILADDRESS', index: 'EMAILADDRESS', width: 35, align: 'left', resizable: false },
                { name: 'PHONENO', index: 'PHONENO', width: 35, align: 'left', resizable: false },
                { name: 'ATTENTIONVALUE', index: 'ATTENTIONVALUE', width: 35, align: 'left', resizable: false },
                { name: 'CREATED_ON', index: 'CREATED_ON', width: 45, align: 'center', resizable: false },
                { name: 'MAILMESSAGE', index: 'MAILMESSAGE', width: 35, align: 'left', resizable: false },
                { name: 'EditUrl', index: 'EditUrl', width: 15, resizable: false, align: 'center', formatter: EditLinkFormatter, sortable: false },
              
                 ],
                       rowNum: 10,
                       rowList: [10, 20, 30, 40, 50],
                       pager: '#ptoolbar',
                       gridview: true,
                       sortname: 'CREATED_ON',
                       sortorder: "desc",

                       viewrecords: true,
                       jsonReader: { repeatitems: false },
                       width: 875,
                       caption: '',
                       height: '100%',
                       loadComplete: function () {
                           $('a[rel*=framebox]').click(function (e) {
                               e.preventDefault();
                               ApplyFrameBox($(this));
                           });
                       }
                   });
                   $("#tblViewContactList").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
                   $("#tblViewContactList").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');
               });

               function EditLinkFormatter(cellvalue, options, rowObject) {
                   return "<a href='" + cellvalue + "' rel='framebox' w='500' h='380' scrolling='true'><img src='../App_Themes/Classic/Images/icon-tv-active.gif' alt='View' title='View'/></a>";
               }
              
               function ndateFormatter(cellval, opts, rwdat, _act) {

                   if (cellval != "" && cellval != null) {
                       var time = cellval.replace(/\/Date\(([0-9]*)\)\//, '$1');
                       var date = new Date();
                       date.setTime(time);
                       return date.toDateString();
                   } else return "-";
               }
           </script>  
</asp:Content>

