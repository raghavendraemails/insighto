﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using Insighto.Exceptions;
using System.Collections;
using CovalenseUtilities.Helpers;
using System.Configuration;
using Insighto.Business.Helpers;
namespace Insighto.Admin.Pages
{
    public partial class ViewSurveyBlockedwords : BasePage
    {
        #region private variables
        Hashtable ht = new Hashtable();
        private int _surveyId;
        #endregion

        #region properties
        public int SurveyId
        {
            get
            {
                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains("SurveyId"))
                    {
                        _surveyId = ValidationHelper.GetInteger(ht["SurveyId"].ToString(), 0);
                    }
                }
                return _surveyId;
            }
            set
            {

            }
        }
        #endregion

        #region Events
        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {

            ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));

            if (ht.Contains("SurveyId"))
            {
                hdnSurveyId.Value = ValidationHelper.GetInteger(ht["SurveyId"].ToString(), 0).ToString();

            }
        }
        #endregion

        #endregion


    }
}