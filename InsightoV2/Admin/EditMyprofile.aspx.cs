﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using App_Code;
using CovalenseUtilities.Helpers;
namespace Insighto.Pages.Admin
{
    public partial class EditMyprofile :BasePage
    {
        #region Events
        #region Page_Load
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoginUserInfo();
            }
        }
        #endregion

        #region SaveButton Click Event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
     
             UpdateAdminUserInfo(); 

        }
        #endregion

        #endregion

        #region Methods

        #region LoginUserInfo
        public void LoginUserInfo()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginAdminUserDetailsSession();
            var adminUserServie = ServiceFactory.GetService<AdminUserService>();
            var adminDetails = adminUserServie.FindAdminUserByUserId(ValidationHelper.GetInteger(userInfo.AdminUserId,0) );  
            var adminUser = new osm_adminuser(); 
            if (userInfo != null)
            {
                lblLoginName.Text = adminDetails.LOGIN_NAME;
                if (!string.IsNullOrEmpty(adminDetails.ADMIN_TYPE))
                    lblAdminType.Text = adminDetails.ADMIN_TYPE;
                else
                    lblAdminType.Text = Constants.SuperAdmin;     
                txtFirstName.Text = adminDetails.FIRST_NAME;
                txtLastName.Text = adminDetails.LAST_NAME;
            }
        }
        #endregion

        #region UpdateAdminUserInfo
        /// <summary>
        /// 
        /// </summary>
        public void UpdateAdminUserInfo()
        {
            int rtnval = 0;
            var adminUserService = ServiceFactory.GetService<AdminUserService>();
            var adminUser = new osm_adminuser();
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginAdminUserDetailsSession();
            adminUser.ADMINUSERID = userInfo.AdminUserId;  
            adminUser.FIRST_NAME = txtFirstName.Text;
            adminUser.LAST_NAME = txtLastName.Text;
            adminUser.MODIFIED_ON = DateTime.UtcNow;
            adminUser.DELETED = 0;

            rtnval= adminUserService.UpdateAdminProfileByUserId(adminUser,"N");
            if (rtnval != 0)
            {
                dvSuccessMsg.Visible = true;
            }
        }
        #endregion

        #endregion

    }
}