﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using InfoSoftGlobal;

public partial class SingleSeries : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        object[,] arrData = new object[6, 2];
        arrData[0, 0] = "Product A"; arrData[1, 0] = "Product B"; arrData[2, 0] = "Product C"; arrData[3, 0] = "Product D"; arrData[4, 0] = "Product E"; arrData[5, 0] = "Product F";
        arrData[0, 1] = 567500; arrData[1, 1] = 815300; arrData[2, 1] = 556800; arrData[3, 1] = 734500; arrData[4, 1] = 676800; arrData[5, 1] = 648500;
        StringBuilder xmlData = new StringBuilder();
        xmlData.Append("<chart caption='Sales by Product' exportEnabled='1'  numberPrefix='$' formatNumberScale='0'>");
        for (int i = 0; i < arrData.GetLength(0); i++) { xmlData.AppendFormat("<set label='{0}' value='{1}' />", arrData[i, 0], arrData[i, 1]); }
        xmlData.Append("</chart>");
        Literal1.Text =  InfoSoftGlobal.FusionCharts.RenderChart("../FusionCharts/Column3D.swf", "", xmlData.ToString(), "productSales", "600", "300", false, true);
    }
}