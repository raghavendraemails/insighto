﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="termsofuse.aspx.cs" Inherits="new_termsofuse" MasterPageFile="~/Home.Master"%>
<%@ Register TagPrefix="uc" TagName="OtherHeader" Src="~/UserControls/OtherHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc:OtherHeader ID="OtherHeader" runat="server" />
<div class="wrapper"> 

  <!--=== Content Part ===-->
  <div class="breadcrumbs-v3 img-v1 text-center">
    <div class="container">
      <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;"> <span>Terms and Conditions</span></h2>
      <!--<h1 style="color:#fff900 !important; text-transform:none !important;">Survey Services</h1>
      <h5 style="color:#FFF !important;">Don’t let your lack of time or skill to create surveys abstain you from useful insights. </h5>
      <h5 style="color:#FFF !important;">Let Insighto Experts build powerful surveys for you.</h5>--> 
    </div>
  </div>
  <p>&nbsp;</p>
  <!--=== Content Part ===-->
  <div class="container">
    <div class="row-fluid privacy">
      <h4>Terms and Conditions of use</h4>
      <h4>Thank you for using products and services of Insighto.com</h4>
      <p>These Terms and Conditions of use contain the terms under which Knowience Insights Private Limited  (the Company that owns Insighto products and services) and its affiliates and resellers provide the products and services to you and describe how these may be accessed and used.</p>
      <p>You indicate your agreement to these Terms by clicking or tapping on a button indicating your acceptance of these Terms, by executing a document that references to them, or by using the services.</p>
      <p>If you will be using the Insighto products and services on behalf of an organization, you agree to these Terms on behalf of that organization and you represent that you have the authority to do so. In such case, <b>“you” and “your” will refer to that organization</b>.</p>

      <h4><strong>1. Definitions</strong></h4>
      <p>“Knowience” means Knowience Insights Private Limted - the company that owns and runs Insighto.com for online survey, polls and other related software.</p>
      <p>“Insighto.com” or “Website” means the website that facilitates the use of the online survey, polls and other related software.</p>
      <p>"Content" means the information, data, text, software, applications, games, messages, videos, songs, photographs, graphics and other material published on the website.</p>
      <p>“Software Application Services” or “Software+” means computer software, database, data storage / related systems, online or electronic documentation</p>
      <p>"Post" means display, exhibit, publish, distribute, transmit and/or disclose information, details and/or other material on the Website, and the phrases "Posted" and "Posting" shall         be interpreted accordingly.</p>
      <p>"Services" means all software application service or any service provided by us through the Web Site.</p>
      <p>“User” means any person who uses the Services or visits the Web Site for any purpose.</p>
      
      <h4><strong>2. The Agreement</strong></h4>
      <p>ANY USER ACCESSING INSIGHTO.COM WEBSITE OR ANY OF THE CONTENT HEREIN AGREES TO AND IS BOUND BY THE FOLLOWING TERMS AND CONDITIONS OF INSIGHTO.COM TERMS OF USE (hereinafter the “Agreement”).</p>
      <p>This Agreement is a legal agreement between the USER and KNOWIENCE for use of INSIGHTO.COM and its Software+. By using Insighto.com’s Software+, you agree to be bound by the terms of this Agreement.</p>
      
      <h4><strong>3. Price and Payment</strong></h4>
      <p>3.1	You agree to pay the applicable subscription fees including any government taxes if any for paid use of Insighto.com.</p>
      <p>3.2	Where applicable, you will be billed using the billing method you select at the appropriate pages of the website.</p>
      <p>3.3	Some of our subscriptions are billed on a recurring subscription basis. This means  that you will be billed in advance on a recurring, periodic basis – and will depend on the subscription plan you select at the time of purchase. Your subscription will automatically renew at the end of each billing cycle unless you cancel auto-renewal through the account management page of payment gateway services provider or by contacting our support team. When you cancel your auto-renewal, your subscription will continue till the end of your billing cycle before terminating.</p>
      
      <p>3.4	Knowience is entitled to alter, revise or modify the pricing on the Website at any time.</p>
      <p>3.5	You will pay all sums due to Knowience under this Agreement by the means specified without any set-off, deduction or counterclaim.</p>
      <p>3.6	All monies paid by you to us are non-refundable and cancellation and/or termination of this Agreement by User or Knowience at any time for any reason (unless required by law) will not entitle you to a refund of monies paid.</p>
      
      <h4><strong>4. Account Activation and Management</strong></h4>
      <p>4.1	You will set your  Username and password yourelf to complete the sign up process on Insighto.com. You alone are responsible for maintaining the confidentiality and integrity of the password and the account, and are fully responsible for all activities related to the account.</p>
      <p>4.2	You are responsible for any activity occurring in your account (other than activity that Insighto(Knowience Insights) is directly responsible for which is not peformed in accordance with customer’s instructions), whether or not you authorized that activity.</p>
      <p>4.3	You agree to (a) immediately notify Knowience of any unauthorized use of your password or account or any other breach of security, and (b) ensure that you logout from your account at the end of each session. Knowience cannot and will not take responsibility and cannot and will not be liable for any loss or damage arising from your failure to comply with this Section.</p>
      <p>4.4	You must keep your account details including your name, contact details and email address accurate. You will receive notices and occasional emails to the email address registered with your account. </p>
      <p>4.5	You are responsible for maintaining, protecting and making back ups of your content and your survey/poll data reports. To the extent permitted by applicable law, Insighto will not be liable for any failure to store, or for loss or corruption of your content.</p>
      <p>4.6	Insighto may terminate your account and delete any content contained in it if there is no account activity for over 12 months.</p>
      
      <h4><strong>5.	Your Content creation and ownership </strong></h4>
      <p>5.1	You retain the ownership of all of your intellectual property rights in your content. These terms do not grant us any licenses or rights to your content except for the limited rights needed by us to provide the services of Insighto and as otherwise described in these Terms.</p>
      <p>5.2	You grant Knowience, a world wide, royalty fre license to use, reproduce, distribute, modify, adapt, create derivative works, make publicly available and otherwise exploit your content, but only for limited purposes of providing Insighto to you and as otherwise permitted by Insighto’s privacy policy. The license for such limited purposes continues even after you stop using Insighto though you may have the ability to delete your content in relation to certain services such that Insighto has no longer access to it. The license also extends to any trusted third parties we work to the extent necessary to provide Insighto to you. If you provide Insighto with feedback on the product and services, we may use the same without any obligation to you.</p>
      <p>5.3	You acknowledge that in order to ensure compliance with legal obligations, Insighto may be required to review certain content to determine whether it is illegal or whether it violates these Terms (such as when unlawful content is reported to us or when any obscene words are used). We may also modify, prevent access to, delete or refuse to display content that we believe violates the law or these Terms. </p>
      <p>5.4	Knowience may publish links in Insighto to internet websites maintained by third parties. Knowience does not represent that it has reviewed such third party websites and is not responsible for them or any content appearing on them. Trademarks displayed in conjunction with Insighto are the property of their respective owners.</p>
      
      <h4><strong>6.	Acceptable use Policy</strong></h4>
      <h4>You agree to comply with these provisions:</h4>
      
      <p>6.1	You are legally eligible to use Insighto products and services as per the laws of your country in terms of your age, legal status and contractual ability.</p>
      <p>6.2	You will use Insighto products and services in compliance with, and only as permitted by law.</p>
      <p>6.3	You will not use or allow anyone else to use the Web Site to upload, Post, email, transmit or otherwise publish or make available any Content that infringes any patent, trademark, trade secret, copyright works or other proprietary rights ("Rights") of any party; commercial audio, video or music files; any material which violates the law of any established jurisdiction; unlicensed software; software, which assists in or promotes: emulators, freaking, hacking, password cracking, IP spoofing; links to any of the material specified in this paragraph; pornographic material; any material promoting discrimination or animosity to any person on grounds of gender, race or colour.</p>
      <p>6.4	You will not use the Services for spamming. Spamming includes, but is not limited to: the bulk sending of unsolicited messages, or the sending of unsolicited emails which provoke complaints from recipients; the sending of junk mail; the use of distribution lists that include people who have not given specific permission to be included in such distribution process; excessive and repeated Posting off-topic messages to newsgroups; excessive and repeated cross-posting; email harassment of another Internet user, including but not limited to, transmitting any threatening, libelous or obscene material, or material of any nature which could be deemed to be offensive; the emailing of age inappropriate communications or content to anyone under the age of 18.</p>
      <p>6.5	You will not use Insighto in a manner, which violates any city, local, state, national or international law or regulation, or which fails to comply with accepted Internet protocol. You will not attempt to interfere in any way with our networks or network security, or attempt to use the Services to gain unauthorized access to any other computer system.</p>
      <p>6.6	You will immediately notify us, of any security breach or unauthorised use of your account. You will not interfere in any way with another User(s) use of Insighto. </p>
      <p>6.7	You will not resell, rent, lease, grant a security interest in, or make commercial use of Insighto without our express written consent.</p>
      <p>6.8	You agree not to transfer your email address for gain or otherwise. Transfer of such email address will result in immediate termination of your membership and your contract.</p>
      <p>6.9	You agree not to assign, transfer, or authorise any other person to use, your membership.  If you try to do so, we have the right to terminate your membership.</p>
      <p>6.10	You may not engage in abusive or excessive usage of Insighto products and services, which is usage significantly in excess of average usage patterns that adversely affects the speed, responsiveness, stability, availability or functionality of Insighto  for other users. </p>
      <p>6.11	You will not probe, scan or test the vulnerability of any Insighto product or systems.</p>
      <p>6.12	Unless permitted by applicable law, you should not reverse engineer Insighto products and services</p>
      <p>6.13	You shall not transmit any viruses, malware, or other types of malicious software or links to such software.</p>
      <p>6.14	You may not use Insighto products and services to infringe the intellectual property rights of others or to commit any unlawful activity.</p>
      <p>6.15	Unless authorized by Knowience Insights in writing, you may not resell or lease Insighto products and services.</p>
      <p>6.16	You are responsible for complying with any industry-specific regulations that are applicable to use while using Insighto products and services.</p>
      
      <h4><strong>7.	System Security</strong></h4>
      <p>7.1	You agree that you will not, and will not allow any other person to, violate or attempt to violate any aspect of the security of the Web Site.</p>
      <p>7.2	You agree that you will, in no way, modify, reverse engineer, disassemble, decompile, copy, or cause damage or unintended effect to any portion of the Web Site, or any software used on the Web Site, and that you will not permit any other person to do so.</p>
      <p>7.3	You understand that any such violation is unlawful in many jurisdictions and that any contravention of law may result in criminal prosecution.</p>
      <p>Examples of violations are:</p>
      <div style="margin-left:30px;"><p>7.4.1	accessing data unlawfully or without consent</p>
      <p>7.4.2	attempting to probe, scan or test the vulnerability of a system or network or to breach security or authentication measures</p>
      <p>7.4.3	attempting to interfere with service to any user, host or network, including, without limitation, via means of overloading, "flooding", "mail bombing" or "crashing"</p>
      <p>7.4.4	forging any TCP/IP packet header or any part of the header information in any e-mail or newsgroup Posting</p>
      <p>7.4.5	taking any action in order to obtain services to which you are not entitled.</p></div>
 
      <h4><strong>8.	Intellectual Property Rights</strong></h4>
      <p>8.1	Insighto Software Application Services are protected by Indian and international intellectual property laws and treaties. The Software Application Services are licensed, not sold. All title and copyrights in and to Software+ [and the Web Site] are owned by Knowience.</p>
      <p>8.2	Title, ownership rights, and intellectual property rights in the Content whether provided by us or by any other Content provider shall remain the sole property of us and / or the other Content provider. We will strongly protect its rights in all countries.</p>
      <p>8.3	You may not copy, modify, publish, transmit, transfer or sell, reproduce, create derivative works from, distribute, perform, display, or in any way exploit any of the Content, in whole or in part, except as is expressly permitted in this Agreement.</p>
      <p>8.4	You may Post into the Services any Content owned by you. You accept all risk and responsibility for determining whether any Content is in the public domain. </p>
      <p>8.5	You represent that any user name or email address selected by you, when used alone or combined with a second or third level domain name, does not interfere with the rights of any third party and has not been selected for any unlawful purpose. You acknowledge and agree that if such selection does interfere with the rights of any third party or is being selected for any unlawful purpose, we may immediately suspend the use of such name or email address, and you will indemnify us for any claim or demand that arises out of your selection. You acknowledge and agree that we shall not be liable to you in the event that we are ordered or required by a court or judicial authority, to desist from using or permitting the use of a particular domain name as part of a name or email address. If as a result of such action, you lose an email address; your sole remedy shall be the receipt of a replacement.</p>
      
      
      <h4><strong>9.	Legal Action</strong></h4>
      <p>Knowience may initiate such legal action as it may deem necessary or is advised or shall co-operate with any authority and provide all information, material and or such other material and information as it may have access to, to protect its rights as also of its clients, customers, users, and others who may either directly or indirectly be effected in any way.</p>
      
      <h4><strong>10.	Indemnity</strong></h4>
      <p>You agree to indemnify us against any claim or demand, including reasonable lawyers’ fees, made by any third party due to or arising out of your use of the Services, the breach or violation of this Agreement by you, or the infringement by you, or by any other user of the Services using your computer or your username and password, of any intellectual property or other right of any person or entity, or as a result of any threatening, libelous, obscene, harassing or offensive material contained in any of your communications.</p>
      
      <h4><strong>11.	Termination</strong></h4>
      <p>11.1	You may terminate this Agreement at any time, for any reason, with immediate effect. You may terminate the agreement either by sending notices to us by email. We reserve the right to check the validity of any request to terminate membership.If you terminate a subscription in the middle of the billing cycle, you will not receive a refund for any period of time you did not use in that billing cycle, unless you are terminating the Agreement for our breach and have notified us in writing, or unless a refund is required by law</p>
      <p>11.2	We may terminate this Agreement at any time, for any reason, with immediate effect by sending you notice to that effect by email. If you sent a spam email or registered unlawfully, your account would be terminated without any notice or information. And Insighto would take measures to prevent the further use of the Products and servces by you, including blocking of your IP address.</p>
      <p>11.3	If we terminate, we shall be under no liability to you whatsoever, and you hereby release us from any such liability.</p>
      <p>11.4	Termination by either party shall have the following effects: </p>
      <div style="margin-left:30px;"><p>11.4.1	your right to use the Services immediately ceases;</p>
      <p>11.4.2	we are under no obligation to forward any unread or unsent messages to you or any third party;</p></div>
      <p>11.5	We reserve the right to terminate your email address in the event that our right to use certain domain names or email addresses terminate or expire.</p>
      <p>11.6	We retain the right, at our sole discretion, to terminate any and all parts of Insighto provided to you, without refunding to you any fees paid if we decide in our absolute discretion that you have failed to comply with any of the terms of this Agreement.</p>
      
      <h4><strong>12.	Interruption to the Service</strong></h4>
      <p>12.1	If it is necessary for us to interrupt the Services then we may do so without telling you first.</p>
      <p>12.2	You acknowledge that the Services may also be interrupted for reasons beyond our control.</p>
      <p>12.3	You agree that we are not liable to you for any loss whether foreseeable or not, arising as a result of interruption to the Services.</p>
      
      <h4><strong>13.	Storage of Data</strong></h4>
      <p>13.1	We assume no responsibility for the deletion or failure to store, deliver or timely delivery of messages/emails.</p>
      <p>13.2	We may, from time to time and without notice, set limit(s) on the number of messages/emails  a User may send, store, or receive through the service, and we retain the right to delete any emails above such limit(s) without any liability whatsoever, and you hereby release us from any such liability. Any notice provided by us to you in connection with such limit(s) shall not create any obligation to provide future notification regarding any change(s) to such limit(s).</p>
      
      <h4><strong>14.	Our liability</strong></h4>
      <p>14.1	Your use of the Services and the Web Site is without any warranty or guarantee.</p>
      <p>14.2	Where we provide a service without specific charge, then it is deemed to be provided free of charge, and not to be associated with any other service for which a charge is made. Accordingly, there is no contractual or other obligation upon us in respect of any such service.</p>
      <p>14.3	We or our Content suppliers may make improvements or changes to the Web Site, the content, or to any of the products and services described on the Web Site, at any time and without notice to you.</p>
      <p>14.4	You are advised that content may include technical inaccuracies or typographical errors.</p>
      <p>14.5	We give no warranty and make no representation, express or implied, as to:</p>
      <div style="margin-left:30px;"><p>14.5.1	the truth of any information given on the Web Site by any Associate or third party;</p>
      <p>14.5.2	suitability, reliability, availability, timeliness, lack of viruses or other harmful components and accuracy of the information, software, products, Services and related graphics contained within the Web Sites;</p>
      <p>14.5.3	any implied warranty or condition as to merchantability or fitness for a particular purpose;</p>
      <p>14.5.4	compliance with any law;</p>
      <p>14.5.5	Non-infringement of any right.</p></div>
      <p>14.6	Much of the material provided on the Web Site is Posted (and thereby published) by Users. We are under no obligation to monitor, vet, and check or approve any such material. We disclaim all responsibility for information published on the Web Site by any person.</p>
      <p>14.7	The Web Site contains links to other Internet web sites. We have neither power nor control over any such web site. You acknowledge and agree that presence of such link or other means of accessing such third party web site does not constitute endorsement, recommendation, or acceptance of any responsibility for the content of that linked web site or the operators of that linked web site and that we shall not be liable in any way for the content of any such linked web site, nor for any loss or damage arising from your use of any such web site.</p>
      <p>14.8	In no event shall we be liable for any special, indirect or consequential damages or any damages whatsoever resulting from loss of use, loss of data or loss of revenues or profits, whether in an action of contract, negligence or otherwise, arising out of or in connection with the use of the Web Site or the content available from this Web Site.</p>
      
      <h4><strong>15.	Disclaimers</strong></h4>
      <p>15.1	We try to keep our online products and services up, but they may be unavailable from time to time for various reasons. EXCEPT AS EXPRESSLY PROVIDED IN THESE TERMS AND TO THE EXTENT PERMITTED BY APPLICABLE LAW, THE PRODUCTS AND SERVICES ARE PROVIDED “AS IS” AND  DOES NOT MAKE WARRANTIES OF ANY KIND, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING THOSE OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT OR ANY REGARDING AVAILABILITY, RELIABILITY, OR ACCURACY OF THE SERVICES.</p>
      <p>15.2	TO THE EXTENT PERMITTED BY APPLICABLE LAW,KNOWIENCE, ITS AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, SUPPLIERS, AND LICENSORS WILL NOT BE LIABLE FOR ANY INDIRECT, CONSEQUENTIAL, SPECIAL, INCIDENTAL, PUNITIVE, OR EXEMPLARY DAMAGES WHATSOEVER, INCLUDING DAMAGES FOR LOST PROFITS, LOSS OF USE, LOSS OF DATA, ARISING OUT OF OR IN CONNECTION WITH THE SERVICES AND THESE TERMS, AND WHETHER BASED ON CONTRACT, TORT, STRICT LIABILITY, OR ANY OTHER LEGAL THEORY, EVEN IF SURVEYMONKEY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES AND EVEN IF A REMEDY FAILS OF ITS ESSENTIAL PURPOSE.</p>
      <p>15.3	TO THE EXTENT PERMITTED BY APPLICABLE LAW, THE AGGREGATE LIABILITY OF EACH OF KNOWIENCE, ITS AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, SUPPLIERS, AND LICENSORS ARISING OUT OF OR IN CONNECTION WITH THE SERVICES AND THESE TERMS WILL NOT EXCEED THE GREATER OF: (A) THE AMOUNTS PAID BY YOU TO USE OF INSIGHTO AT ISSUE DURING THE 3 MONTHS PRIOR TO THE EVENT GIVING RISE TO THE LIABILITY; AND (B) US$25.00.</p>
      <p>15.4	<b>Consumers.</b> We acknowledge that the laws of certain jurisdictions provide legal rights to consumers that may not be overridden by contract or waived by those consumers. If you are such a consumer, nothing in these Terms limits any of those consumer rights.</p>
      <p>15.5	<b>Businesses.</b> If you are a business, you will indemnify and hold harmless Knowience and its affiliates, officers, agents, and employees from all liabilities, damages, and costs (including settlement costs and reasonable attorneys’ fees) arising out of a third party claim regarding or in connection with your use of the Services or a breach of these Terms, to the extent that such liabilities, damages and costs were caused by you.</p>
      
      <h4><strong>16.	Modification/Changes </strong></h4>
      <p>We reserve the right to modify the Changes and to change the terms and conditions of this agreement at any time, without notice. Any changes will be posted to the location at which those terms appear.</p>
      <p>Insighto may also provide notification of changes on the blog or through email. Your continued use of the Services after such modifications shall be deemed an acceptance by you to be bound by the terms of the modified agreement. </p>
      
      <h4><strong>17.	Severability</strong></h4>
      <p>If any of these terms is at any time held by any jurisdiction to be void, invalid or unenforceable, then it shall be treated as changed or reduced, only to the extent minimally necessary to bring it within the laws of that jurisdiction and to prevent it from being void and it shall be binding in that changed or reduced form. Subject to that, each provision shall be interpreted as severable and shall not in any way affect any other of these terms.</p>
      
      <h4><strong>18.	Action Limit</strong></h4>
      <p>You and we agree that any cause of action arising out of or related to the Services must commence within one year after the cause of action arose; otherwise, such cause of action is permanently barred.</p>
     
      <h4><strong>19.	Force majeure</strong></h4>
      <p>19.1	Neither party shall be liable for any breach of its obligations resulting from causes beyond its reasonable control including acts of God, fire, natural disaster, war or military hostilities and strikes of its own employees.</p>
      <p>19.2	Each of the parties agrees to give notice immediately to the other upon becoming aware of an event of force majeure such notice to contain details of the circumstances giving rise to it.</p>
      
      <h4><strong>20.	No Waiver</strong></h4>
      <p>No waiver by us, in exercising any right shall operate as a waiver of any other right or of that same right at a future time; nor shall any delay in exercise of any power or right be interpreted as a waiver. </p>
      
      <h4><strong>21.	Dispute Resolution</strong></h4>
      <p>In the event of a dispute arising out of or in connection with the terms of this Agreement between you and us, then you agree to attempt to settle the dispute by engaging in good faith with us in a process of mediation before commencing arbitration or litigati</p>
      
      <h4><strong>22. Jurisdiction</strong></h4>
      <p>This Contract shall be interpreted according to the Laws of Republic of India and the parties agree to submit to the exclusive jurisdiction of the Indian courts at Hyderabad. This Agreement shall not be governed by the United Nations Convention on Contracts for the International Sale of Goods, the application of which is hereby expressly excluded.</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </div>
    <!--/row-fluid--> 
  </div>
  <!--/container--> 
  
</div>
<!--/wrapper--> 
</asp:Content>