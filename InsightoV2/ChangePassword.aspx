﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs" Inherits="Insighto.Pages.ChangePassword" Culture="auto"
    UICulture="auto" %>

<%@ Register Src="UserControls/ProfileRightPanel.ascx" TagName="ProfileRightPanel"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="myProfileLeftPanel">
        <div class="marginNewPanel" style="margin-top: 0;">
            <div class="signUpPnl">
                <div id="dvResource" runat="server">
                </div>
                <div class="clear">
                </div>
                <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                    <div>
                        <asp:Label ID="lblSuccessMsg" runat="server" Text="Contact list has been created succesfully."></asp:Label></div>
                </div>
                <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                    <div>
                        <asp:Label ID="lblErrMsg" runat="server" Text="Contact list already exist."></asp:Label></div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblOldPassword" Text="Old Password" AssociatedControlID="txtOldPassword"
                            runat="server" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtOldPassword" runat="server" CssClass="textBoxMedium txtbox" MaxLength="100"
                            meta:resourcekey="txtOldPasswordResource1" TextMode="Password" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <div class="Nogap">
                            <span class="note" id="spnInfo" runat="server" visible="false">Type the password provided
                                in your authentication mail.</span>
                        </div>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqOldPassword" CssClass="lblRequired"
                            ControlToValidate="txtOldPassword" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblNewPassword" Text="New Password" AssociatedControlID="txtNewPassword"
                            runat="server" meta:resourcekey="lblNewPasswordResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtNewPassword" runat="server" CssClass="textBoxMedium" MaxLength="16"
                            meta:resourcekey="txtNewPasswordResource1" TextMode="Password" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqNewPassword" CssClass="lblRequired"
                            ControlToValidate="txtNewPassword" meta:resourcekey="lblReqNewPasswordResource1"
                            runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:CustomValidator SetFocusOnError="true" ID="cvNewPassword" Display="Dynamic"
                            runat="server" ControlToValidate="txtNewPassword" CssClass="lblRequired" meta:resourcekey="lblcvNewPasswordResource1"
                            ClientValidationFunction="validatePassword">
                        </asp:CustomValidator></div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:CompareValidator ID="compOldpassword" Display="Dynamic" Operator="NotEqual"
                            ControlToValidate="txtNewPassword" ControlToCompare="txtOldPassword" CssClass="lblRequired"
                            Text="New password should not be same as the old password." runat="server" /></div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl">
                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="regNewPassword" CssClass="lblRequired"
                            ControlToValidate="txtNewPassword" runat="server" ErrorMessage="6 char" ValidationExpression="^.*(?=.{6,16}).*$"
                            meta:resourcekey="lblregNewPasswordResource1" Display="Dynamic"></asp:RegularExpressionValidator></div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="lblConfirmPassword" Text="Confirm  Password" AssociatedControlID="txtConfirmPassword"
                            runat="server" meta:resourcekey="lblConfirmPasswordResource1" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="textBoxMedium" MaxLength="16"
                            meta:resourcekey="txtConfirmPasswordResource1" TextMode="Password" />
                    </div>
                    <div class="con_login_pnl">
                        <div class="reqlbl btmpadding">
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqConfirmPassword" CssClass="lblRequired"
                                ControlToValidate="txtConfirmPassword" meta:resourcekey="lblReqConfirmPasswordResource1"
                                runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="compConfirmPassword" Display="Dynamic" ControlToValidate="txtConfirmPassword"
                                ControlToCompare="txtNewPassword" CssClass="lblRequired" Text="Pasword not match"
                                runat="server" meta:resourcekey="compConfirmPasswordResource1" /></div>
                    </div>
                    <div class="con_login_pnl">
                        <div class="reqlbl btmpadding">
                            <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="dynamicButton" OnClick="btnSave_Click"
                                ToolTip="Save" />&nbsp;&nbsp;
                            <asp:Button ID="btnCancel" Text="Clear" CssClass="dynamicButton" ToolTip="Clear"
                                runat="server" ValidationGroup="cancel" OnClick="btnCancel_Click" /></div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <!-- //change password -->
            </div>
        </div>
    </div>
    <uc:ProfileRightPanel ID="ucProfileRightPanel" runat="server" />
    <%--<div class="myProfileLeftPanel" style="padding-top: 20px">
        <div class="marginNewPanel" style="margin-top: 0;">
            <div class="signUpPnl">
                <div id="dvChangeEmail" runat="server">
                    <h3>
                        CHANGE EMAILID</h3>
                </div>
                <div class="clear">
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="Label3" Text="Current Email Address" AssociatedControlID="txtCurrentEmailId"
                            runat="server" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtCurrentEmailId" runat="server" CssClass="textBoxMedium" MaxLength="150" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvCurrenEmailId" CssClass="lblRequired"
                            ControlToValidate="txtCurrentEmailId" runat="server" Display="Dynamic" ErrorMessage="Please enter email address." ValidationGroup="email"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="con_login_pnl">
                    <div class="reqlbl">
                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="regExCurrentEmail"
                            CssClass="lblRequired" ControlToValidate="txtCurrentEmailId" runat="server" ErrorMessage="Current email address in invalid format."
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                            Display="Dynamic" ValidationGroup="email"></asp:RegularExpressionValidator></div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="Label4" Text="Type New Email Address" AssociatedControlID="txtNewPassword"
                            runat="server" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtNewEmailAddress" runat="server" CssClass="textBoxMedium" MaxLength="150" />
                    </div>
                </div>
                <div class="con_login_pnl">
                    <div class="reqlbl btmpadding">
                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvNewEmailId" CssClass="lblRequired"
                            ControlToValidate="txtNewEmailAddress" ErrorMessage="Please enter new emailid." runat="server"
                            Display="Dynamic" ValidationGroup="email"></asp:RequiredFieldValidator>
                    </div>
                </div>                
                <div class="con_login_pnl">
                    <div class="reqlbl">
                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="regExNewEmailId"
                            CssClass="lblRequired" ControlToValidate="txtNewEmailAddress" runat="server" ErrorMessage="New email address in invalid format."
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                            Display="Dynamic"></asp:RegularExpressionValidator></div>
                </div>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                        <asp:Label ID="Label5" Text="Confirm New Email Address" AssociatedControlID="txtConfirmNewEmailAddress"
                            runat="server" />
                    </div>
                    <div class="loginControls">
                        <asp:TextBox ID="txtConfirmNewEmailAddress" runat="server" CssClass="textBoxMedium"
                            MaxLength="150" />
                    </div>
                    </div>
                    <div class="con_login_pnl">
                        <div class="reqlbl btmpadding">
                            <asp:RequiredFieldValidator SetFocusOnError="true" ID="revConfirmEmailAddress" CssClass="lblRequired"
                                ControlToValidate="txtConfirmNewEmailAddress" ErrorMessage="Please enter confirm new email address."
                                runat="server" Display="Dynamic" ValidationGroup="email"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cmpEmailAddress" Display="Dynamic" ControlToValidate="txtConfirmNewEmailAddress"
                                ControlToCompare="txtNewEmailAddress" CssClass="lblRequired" Text="New email address and confirm email address should match."
                                runat="server" ValidationGroup="email" /></div>
                    </div>
                    <div class="con_login_pnl">
                        <div class="reqlbl btmpadding">
                            <asp:Button ID="btnEmailSave" Text="Save" runat="server" CssClass="dynamicButton" 
                                ToolTip="Save" ValidationGroup="email" OnClick="btnEmailSave_Click" />&nbsp;&nbsp;
                            <asp:Button ID="btnEmailCancel" Text="Clear" CssClass="dynamicButton" ToolTip="Clear" runat="server"
                                ValidationGroup="email" OnClick="btnEmailCancel_Click"/></div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <!-- //change password -->
            </div>
        </div>--%>    
    <div class="clear">
    </div>
    <script type="text/javascript" language="JavaScript">
        function validatePassword(oSrc, args) {

            var l = /^\w*(?=\w*[a-z])\w*$/
            var u = /^\w*(?=\w*[A-Z])\w*$/
            var d = /^\w*(?=\w*\d)\w*$/
            var lower = /^.*(?=.*[a-z]).*$/
            var upper = /^.*(?=.*[A-Z]).*$/
            var digit = /^.*(?=.*\d).*$/
            var spl = /^.*(?=.*[@#$%^&+=]).*$/
            var cnt = 0;
            if (lower.test(args.Value)) { cnt = cnt + 1; }
            if (upper.test(args.Value)) { cnt = cnt + 1; }
            if (digit.test(args.Value)) { cnt = cnt + 1; }
            if (spl.test(args.Value)) { cnt = cnt + 1; }
            if (cnt < 2 && args.Value.length > 5) {
                args.IsValid = false;
            }
        }

      
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.txtbox').focus();
        });
    </script>
</asp:Content>
