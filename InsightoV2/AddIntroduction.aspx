﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true"
    CodeFile="AddIntroduction.aspx.cs" Inherits="AddIntroduction" EnableEventValidation="true"%>

<%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>
<%@ Register Src="UserControls/SurveyQuestionControl.ascx" TagName="SurveyQuestionControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" runat="Server">
<script type="text/javascript" src="Scripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        function cleartext() {
            tinyMCE.getInstanceById('<%= EditorQuestionIntro.ClientID %>').setContent('');
            return false;
        }

        function resetText() {
            tinyMCE.getInstanceById('<%= EditorQuestionIntro.ClientID %>').setContent('<p> You are invited to join a short survey being held among the customers of COMPANY/BRAND X. There are only (X No. of questions) and it will take about X minutes of your time. We will keep all your information confidential and will not transmit it to any third parties. If you have any questions about this survey, please contact me directly at my email address provided below. <br> Your Feedback is important!</br>');

        }

    </script>
    <div class="surveyQuestionPanel">
        <!-- survey question panel -->
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <div class="surveyQuestionHeader">
            <div class="surveyQuestionTitle">               
                        <asp:Label ID="lblTitle" runat="server" Text="Add Introduction" 
                            meta:resourcekey="lblTitleResource1"></asp:Label>
            </div>
            <div class="previewPanel">
                <table align="right">
                    <tr>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" ToolTip="Back to Pre Launch Check "
                                CssClass="backbgbtn" OnClick="btnBack_Click" Visible="False" 
                                meta:resourcekey="btnBackResource1" />
                        </td>
                        <td>
                            <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="surveyQuestionTabPanel">
            <!-- survey question tabs -->
            <p>
                <asp:Label ID="lblIntroHelp" runat="server" 
                    Text="As a best practice, it would help you if your survey carries an introduction.Given below is a default text.</br>You can edit it or write your own introduction." 
                    meta:resourcekey="lblIntroHelpResource1"></asp:Label>
            </p>
            <p>
                <input type="hidden" runat="server" id="EditorQuesIntroParams" value="0" />
                <uc1:SurveyQuestionControl ID="SurveyQuestionControl1" runat="server" IsIntroductionPage="true" />
            </p>
            <p>
                &nbsp;</p>
            <p>
                <asp:CheckBox ID="chkPageBreak" runat="server" 
                    meta:resourcekey="chkPageBreakResource1" />
                <asp:Label ID="lblPageBreak" runat="server" Text="Page break" 
                    meta:resourcekey="lblPageBreakResource1"></asp:Label> &nbsp<span><a href="#" title="Help" class="helpLink1" onclick="javascript: window.open('help/2_2_14.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">&nbsp;</a></span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button
                        ID="btnClear" runat="server" Text="Clear" ToolTip="Clear" CssClass="dynamicButton"
                        OnClientClick="Javascript:return cleartext();" 
                meta:resourcekey="btnClearResource1" />
                    </p>
            <!-- //survey question tabs -->
            
        </div>
        <div class="bottomButtonPanel">
            <asp:Button ID="btnReset" runat="server" Text="Restore Default" ToolTip="Reset" CssClass="btn_Restore"
                OnClientClick="Javascript:return resetText();" OnClick="btnReset_Click" 
                Visible="False" meta:resourcekey="btnResetResource1" />&nbsp;&nbsp;<asp:Button
                    ID="btnSave" runat="server" Text="Save & Next" ToolTip="Save & Next" CssClass="dynamicButton"
                    OnClientClick="tinyMCE.triggerSave(false,true);" 
                OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" />
        </div>
        <!-- //survey question panel -->
    </div>
</asp:Content>
