﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="price.aspx.cs" Inherits="price" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!--Set Viewport for Mobile Devices -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>
Insighto Pricing | Insighto Plans and Pricing</title>
<!-- Setup OpenGraph support-->
<meta property="og:title" content="Pricing"/>
<meta property="og:description" content=""/>
<meta property="og:url" content="https://www.insighto.com/pricing.aspx"/>
<meta property="og:image" content=""/>
<meta property="og:type" content="article"/>
<meta property="og:site_name" content="Insighto Pricing"/>

<!-- Begin Styling -->
	<link href="https://www.insighto.com/blog/wp-content/uploads/2013/12/insighto.ico" rel="icon" type="image/png" />
<link rel="alternate" type="application/rss+xml" title="Insighto Pricing &raquo; Feed" href="https://www.insighto.com/pricing/feed/" />
<link rel="alternate" type="application/rss+xml" title="Insighto Pricing &raquo; Comments Feed" href="https://www.insighto.com/pricing/comments/feed/" />
<link rel='stylesheet' id='thickbox-css'  href='https://www.insighto.com/pricing/wp-includes/js/thickbox/thickbox.css?ver=20121105' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://www.insighto.com/pricing/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=3.5.4' type='text/css' media='all' />
<link rel='stylesheet' id='gpp_shortcodes-css'  href='https://www.insighto.com/pricing/wp-content/plugins/gpp-shortcodes/gpp-shortcodes.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='gpp-sc-genericons-css'  href='https://www.insighto.com/pricing/wp-content/plugins/gpp-shortcodes/genericons/genericons.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='mtf_css-css'  href='https://www.insighto.com/pricing/wp-content/plugins/mini-twitter-feed/minitwitter.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='emember-css-css'  href='https://www.insighto.com/pricing/wp-content/plugins/membership-pricing-table/css/user/user.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='flatpack-style-css'  href='https://www.insighto.com/pricing/wp-content/themes/flatpack/style.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='flatpack-responsive-css'  href='https://www.insighto.com/pricing/wp-content/themes/flatpack/responsive.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='flatpack-jplayer-css'  href='https://www.insighto.com/pricing/wp-content/themes/flatpack/ocmx/jplayer.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='flatpack-customizer-css'  href='https://www.insighto.com/pricing/?stylesheet=custom&#038;ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='googleFonts1-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700%2C400italic%2C700italic&#038;ver=3.6' type='text/css' media='all' />
<script type='text/javascript' src='https://www.insighto.com/pricing/wp-includes/js/jquery/jquery.js?ver=1.10.2'></script>
<script type='text/javascript' src='https://www.insighto.com/pricing/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='https://www.insighto.com/pricing/wp-content/plugins/q2w3-fixed-widget/js/q2w3-fixed-widget.min.js?ver=4.0.5'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ThemeAjax = {"ajaxurl":"http:\/\/www.insighto.com\/pricing\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.insighto.com/pricing/wp-content/themes/flatpack/ocmx/includes/upgrade.js?ver=3.6'></script>
<script type='text/javascript' src='https://www.insighto.com/pricing/wp-content/plugins/membership-pricing-table/js/user/view.js?ver=3.6'></script>
<script type='text/javascript' src='https://jquery-textfill.github.io/jquery-textfill/jquery.textfill.min.js?ver=3.6'></script>
<script type='text/javascript' src='https://www.insighto.com/pricing/wp-content/themes/flatpack/scripts/menus.js?ver=3.6'></script>
<script type='text/javascript' src='https://www.insighto.com/pricing/wp-content/themes/flatpack/scripts/fitvid.js?ver=3.6'></script>
<script type='text/javascript' src='https://www.insighto.com/pricing/wp-content/themes/flatpack/scripts/theme.js?ver=3.6'></script>
<script type='text/javascript' src='https://www.insighto.com/pricing/wp-content/themes/flatpack/scripts/portfolio.js?ver=3.6'></script>
<script type='text/javascript' src='https://www.insighto.com/pricing/wp-includes/js/comment-reply.min.js?ver=3.6'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.insighto.com/pricing/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.insighto.com/pricing/wp-includes/wlwmanifest.xml" /> 
<link rel='prev' title='Blog' href='https://www.insighto.com/pricing/blog/' />
<link rel='next' title='Pricing Detail' href='https://www.insighto.com/pricing/pricing-detail/' />
<meta name="generator" content="WordPress 3.6" />
<link rel='canonical' href='https://www.insighto.com/pricing/' />
<style>p, .copy{font-family: Arial, Helvetica, sans-serif; font-size: 15px; } 
</style>	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29026379-1']);
    _gaq.push(['_setDomainName', 'insighto.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'https://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</head>

<body class="home page page-id-160 page-template page-template-fullwidth-php">
<form id="Form1" runat="server">
</form>
</body>
</html>





