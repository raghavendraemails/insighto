﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;

namespace Insighto.Pages
{
    public partial class PageNotFound : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dvResourceMessage.InnerHtml = Utilities.ResourceMessage("PageNotFound_Error_Info");
        }
    }
}