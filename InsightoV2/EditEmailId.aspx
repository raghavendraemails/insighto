﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="EditEmailId.aspx.cs" Inherits="EditEmailId" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3><asp:Label ID="lblChangeEmail" Text="Change Email" runat="server" 
                    meta:resourcekey="lblChangeEmailResource1"></asp:Label></h3>
        </div>
     <%--  <div class="popupClosePanel">
			<a href="ManageLogin.aspx">X</a>
		</div>--%>
        <!-- //popup heading -->
    </div>
    <div class="popupContentPanel">
        <!-- popup content panel -->
           <div class="errorPanel" id="dvErrMsg" runat="server">
                <asp:Label ID="lblSuccessMsg" runat="server" Visible="False" 
                     meta:resourcekey="lblSuccessMsgResource1"></asp:Label>
                <asp:Label ID="lblErrMsg" runat="server" Visible="False" 
                    Text="<%$ Resources:CommonMessages, EmailExistsMsg %>"  />
            </div>
        <div class="formPanel">
            <!-- form -->
            <div class="chooseButtonPanel">
                <ul class="formList">
                    <li>
                        <asp:Label ID="lblCategoryName" runat="server"  
                            meta:resourcekey="lblCategoryNameResource1" />
                        <asp:TextBox ID="txtEmailId" runat="server" CssClass="textBoxMedium" 
                            meta:resourcekey="txtEmailIdResource1" />
                        <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqcategory" ControlToValidate="txtEmailId" 
							CssClass="lblRequired"    runat="server" Display="Dynamic" 
                            meta:resourcekey="reqcategoryResource1"></asp:RequiredFieldValidator>				
                    </li>                    
                    <li id="successMessage" >
                        <asp:Label ID="lbl" runat="server" meta:resourcekey="lblResource1" ></asp:Label>                       
                     </li>
                               
                    <li>
                        <asp:Label ID="lblbtnNewFolder" runat="server" AssociatedControlID="ibtnSave" 
                            meta:resourcekey="lblbtnNewFolderResource1" />
                        <asp:ImageButton ID="ibtnSave" runat="server" ImageUrl="App_Themes/Classic/Images/button_save.gif"
                            ToolTip="Save" onclick="ibtnSave_Click" 
                            meta:resourcekey="ibtnSaveResource1" />
                    </li>
                </ul>
                <div class="clear">
                </div>
            </div>
            
            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>
</asp:Content>

