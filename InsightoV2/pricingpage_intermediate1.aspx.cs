﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using App_Code;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Helpers;
using System.Collections;
using Insighto.Data;
using System.Globalization;

public partial class pricingpage_intermediate1 : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int userId = 0;
    string transType = "New";
    string country;
    LoggedInUserInfo loggedInUserInfo;
    private string licenseType = "";
   
    protected void Page_Load(object sender, EventArgs e)
    {
       // UserControl ul = ((UserControl)this.Master.FindControl("HeaderOld.ascx"));
       // ul.Visible = false;
       // Label lbl=((Label)this.Master.FindControl("btnSub")).Visible = false;
        
        //Label lbl1 = ((Label)this.Master.FindControl("")).Visible = false;


        country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
        if (country == null)
        {
            string ipAddress;
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            //ipAddress = "125.236.193.250";
            country = Utilities.GetCountryName(ipAddress);
            ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
            ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
            country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
        }
           





        //string ipAddress = "";

        //if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        //{

        //    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();

        //}

        //else
        //{

        //    ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();

        //}

        ////ipAddress = "125.236.193.250";

        //var country = Utilities.GetCountryName(ipAddress);



        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);

        //  ipAddress = "125.236.193.250";

        // country = Utilities.GetCountryName(ipAddress);

        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);

        ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();

        country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();

        //}

        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (typeHt.Contains("Type"))
                licenseType = typeHt["Type"].ToString();
            if (typeHt.Contains(Constants.USERID))
                userId = ValidationHelper.GetInteger(typeHt[Constants.USERID].ToString(), 0);
            if (typeHt.Contains("Flag"))
                transType = Convert.ToString(typeHt["Flag"]);

        }

        if (!IsPostBack)
        {
            SessionStateService sessionStateService = new SessionStateService();
            loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            if (loggedInUserInfo == null)
            {
                Response.Redirect("Home.aspx");
            }

            string NavUrl = Constants.LicenseType + "=" + "PRO_MONTHLY" + "&UserId=" + userId;

            PayMonth.HRef = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl);

            if (country.ToUpper() == "INDIA")
            {
                Label1.Text = string.Format("{0}{1}{2}{3}","","`", "", "4,500");
                Label1.Attributes.Add("Class", "priceRs");
               // lblMonthly.Text = string.Format("{0}{1}{2}{3}", "","", "","450");
             //  lblMonthly.Attributes.Add("Class", "priceRs");


                Label2.Text = string.Format("{0}{1}{2}{3}", "","`", "", "0");
                
                
                
                //  lblMonthly.Attributes.Add("Class", "priceRs");

                // divFaqOtherContent.Visible = false;

                //  divFaqIndiaContent.Visible = true;

            }

            else
            {
               
             //   Label1.Text = string.Format("{0}{1}{2}{3}","", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), "", GetPriceByCountryName(country, "Yearly"));

             ////  Label1.Attributes.Add("Class", "monthlyHeading");
             //   // lnkBtnMonthly.Text = string.Format("{0}{1}{2}{3}", "SAVE ON ANNUAL PLAN - ", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), " ", GetPriceByCountryName(country, "Yearly"));

             //   lblMonthly.Text = string.Format("{0}{1}{2}{3}","", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), "", GetPriceByCountryName(country, "MONTHLY"));

             //   lblMonthly.Attributes.Add("Class", "monthlyHeading");

             //   Label2.Text = string.Format("{0}{1}{2}{3}", "", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), "", "0");
                
             //   // divFaqOtherContent.Visible = true;

             //   //  divFaqIndiaContent.Visible = false;

                Label1.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "159");


                //  Label1.Attributes.Add("Class", "monthlyHeading");
                // lnkBtnMonthly.Text = string.Format("{0}{1}{2}{3}", "SAVE ON ANNUAL PLAN - ", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), " ", GetPriceByCountryName(country, "Yearly"));

                lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $15");

                // lblMonthly.Attributes.Add("Class", "monthlyHeading");

                Label2.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");





            }



            // dvPageInfo.InnerHtml = Utilities.ResourceMessage("dvPageInfo");

            //FormatCurrencyAndSymbol();

            //    SetPermissions();


        }
    }
     
    protected void Button2_Click(object sender, EventArgs e)
    {
        string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId;

       
        Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
    }

     private string GetCurrencySymbol(string countryName)
        {
            var ci = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.EnglishName.Contains(countryName)).FirstOrDefault();
            return ci.NumberFormat.CurrencySymbol;
        }

    protected void Button3_Click(object sender, EventArgs e)
    {
        string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId;


        Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
       // Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", "Type=PRO_YEARLY"));
       
    }
       //private void FormatCurrencyAndSymbol()
       // {
       //     rbtnMonthly.Text = Utilities.FormatCurrency(Math.Round(450.00, 2), false, true);
       //     rbtnQuarterly.Text = Utilities.FormatCurrency(Math.Round(1300.00, 2), false, true);
       //     rbtnYear.Text = Utilities.FormatCurrency(Math.Round(4500.00, 2), false, true);
       // }

     private double GetPriceByCountryName(string countryNAme, string subscription)
        {
            var price = ServiceFactory<OrderInfoService>.Instance.GetPriceByCountryName(countryNAme);
            if (subscription == "MONTHLY")
                return Convert.ToDouble(price.MONTHLY_PRICE);
            else
                return Convert.ToDouble(price.YEARLY_PRICE);
        }
     //private void SetPermissions()
     //   {
     //       var userType = Utilities.GetUserType();
     //       if (userType != UserType.None)
     //       {
     //           imgTryItFree.Enabled = false;
     //           imgTryItFree.Style.Add("cursor", "default");
     //           if (userType == UserType.PRO_MONTHLY)
     //           {
     //               rbtnMonthly.Checked = true;
     //           }
     //           else if (userType == UserType.PRO_QUARTERLY)
     //           {
     //               rbtnQuarterly.Checked = true;
     //           }
     //           else if (userType == UserType.PRO_YEARLY)
     //           {
     //               rbtnYear.Checked = true;
     //           }
     //       }
     //       else
     //       {
     //           rbtnMonthly.Enabled = true;
     //           imgTryItFree.Enabled = true;
     //           rbtnYear.Checked = true;
     //       }
     //   }


    protected void Button1_Click(object sender, EventArgs e)
    {

        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=0"));
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=0"));
    }
}