﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" 
CodeFile="SurveyTemplate.aspx.cs" Inherits="SurveyTemplate"  EnableEventValidation="true" %>
<%@ Register src="~/UserControls/SurveyCopyFromTemplate.ascx" tagname="SurveyTemplate" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<uc:SurveyTemplate id="ucSurveyTemplate" runat="server" DisplayMode="CopyFromTemplate" />

</asp:Content>

