﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetOpenAuth.OpenId.RelyingParty;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;
using DotNetOpenAuth.OAuth2;
using DotNetOpenAuth.ApplicationBlock;
using System.Configuration;
using System.Net;
using DotNetOpenAuth.ApplicationBlock.Facebook;
using CovalenseUtilities.Helpers;
using DotNetOpenAuth.OpenId.Extensions.UI;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;

public partial class LoginWithFavoriteAccount : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckWhetherUserLoggedinUsingGoogle();
            CheckWhetherUserLoggedinUsingFacebook();
            if (Request.QueryString["key"] != null)
            {
                string ReqType = Request.QueryString["key"].ToString();
                if (ReqType == "Facebook")
                FaceBookLogin();
                else if (ReqType == "Google")
                GoogleLogin();
            }            
        }       
    }

    private void CheckWhetherUserLoggedinUsingGoogle()
    {
        //If user redirected from Google then we will have authentication response
        var relyingParty = new OpenIdRelyingParty();
        var resp = relyingParty.GetResponse();

        //No Point in contuning if response is null..
        if (resp == null)
            return;

        //Check the response status
        switch (resp.Status)
        {
            case AuthenticationStatus.Authenticated:
                Session["GoogleIdentifier"] = resp.ClaimedIdentifier.ToString();
                var fetchResponse = resp.GetExtension<FetchResponse>();

                Session["FetchResponse"] = fetchResponse;
                Response.Redirect("~/LoginIntermediate.aspx?acc=google");
                break;
            case AuthenticationStatus.Canceled:
                Response.Write("<script>window.opener.location = 'Home.aspx'; window.close();</script>");
                //Response.Redirect("~/Login.aspx");
                break;
            case AuthenticationStatus.Failed:
                Response.Write("<script>window.opener.location = 'Home.aspx'; window.close();</script>");
                //Response.Redirect("~/Login.aspx");
                break;
        }

    }

    private void CheckWhetherUserLoggedinUsingFacebook()
    {
        FacebookClient client = new FacebookClient
        {
            ClientIdentifier = ConfigurationManager.AppSettings["facebookAppID"],
            ClientSecret = ConfigurationManager.AppSettings["facebookAppSecret"],
        };
        IAuthorizationState authorization = client.ProcessUserAuthorization();
        if (authorization != null)
        {
            if (authorization.AccessToken != null)
            {
                var request = WebRequest.Create("https://graph.facebook.com/me?access_token=" + Uri.EscapeDataString(authorization.AccessToken));
                using (var response = request.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        var graph = FacebookGraph.Deserialize(responseStream);
                        Session["facebookGraph"] = graph;
                        Response.Redirect("~/LoginIntermediate.aspx");
                    }
                }
            }
            else
                Response.Write("<script>window.opener.location = 'Home.aspx'; window.close();</script>");
              
        } else
            Response.Write("<script>window.opener.location = 'Home.aspx'; window.close();</script>");
    }

    private void FaceBookLogin()
    {

        FacebookClient client = new FacebookClient
        {
            ClientIdentifier = ConfigurationManager.AppSettings["facebookAppID"],
            ClientSecret = ConfigurationManager.AppSettings["facebookAppSecret"],
        };
        IAuthorizationState authorization = client.ProcessUserAuthorization();
        if (authorization == null)
        {
            // Kick off authorization request
            var scope = new List<string>();
            scope.Add("email");
            Uri returnTo = new Uri(ConfigurationManager.AppSettings["facebookReturnUrl"]);
            client.RequestUserAuthorization(scope, null, returnTo);
        }
        else
        {
            var request = WebRequest.Create("https://graph.facebook.com/me?access_token=" + Uri.EscapeDataString(authorization.AccessToken));
            using (var response = request.GetResponse())
            {
                using (var responseStream = response.GetResponseStream())
                {
                    var graph = FacebookGraph.Deserialize(responseStream);
                    Session["facebookGraph"] = graph;
                   
                }
            }
        }
    }
    private void GoogleLogin()
    {
        string discoveryUri = ConfigurationManager.AppSettings["googleAuthUrl"];
        var urlBuilder = new UriBuilder(Request.Url) { Query = "" };

        //Create Request
        var relyingParty = new OpenIdRelyingParty();
        var req = relyingParty.CreateRequest(discoveryUri, urlBuilder.Uri, urlBuilder.Uri);
        req.AddExtension(new UIRequest()
        {
            Mode = UIModes.Popup
        });

        //Add aditional parameters to the URL
        var fetchRequest = new FetchRequest();
        fetchRequest.Attributes.AddRequired(WellKnownAttributes.Contact.Email);
        fetchRequest.Attributes.AddRequired(WellKnownAttributes.Name.First);
        fetchRequest.Attributes.AddRequired(WellKnownAttributes.Name.Last);
        fetchRequest.Attributes.AddRequired(WellKnownAttributes.Contact.HomeAddress.Country);
        req.AddExtension(fetchRequest);

        //Redirect Request To provider
        req.RedirectToProvider();
      
    }

   

    
}