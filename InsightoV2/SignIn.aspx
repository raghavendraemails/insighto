﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="SignIn.aspx.cs" Inherits="SignIn"%>

<%@ Register Src="UserControls/UserLoginControl.ascx" TagName="UserLoginControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupHeading">
        <div class="popupTitle">
            <h3>
                <asp:Label ID="lblChangeQeustionMode" runat="server" Text="Login" 
                    meta:resourcekey="lblChangeQeustionModeResource1"></asp:Label></h3>
        </div>
      <%--  <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(false,'');">
                &nbsp;</a>
        </div>--%>
    </div>
    <div class="popupContentPanel">
        <div class="clear">
        </div>
        <uc1:UserLoginControl ID="UserLoginControl1" runat="server" />
    </div>
</asp:Content>
