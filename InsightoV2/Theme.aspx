﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true"
    CodeFile="Theme.aspx.cs" Inherits="Theme"%>

<%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" runat="Server">
<script language="javascript" type="text/javascript">


    function SetSingleRadioButton(nameregex, current) {
        re = new RegExp(nameregex);
        for (i = 0; i < document.forms[0].elements.length; i++) {
            elm = document.forms[0].elements[i];
            if (elm.type == 'radio') {
                if (elm != current) {
                    elm.checked = false;
                }
            }
        }
        current.checked = true;
    }


</script>

    <div class="surveyQuestionPanel">
        <!-- survey question panel -->
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div><asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
        </div>
         <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div><asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <div class="surveyQuestionHeader">
            <div class="surveyQuestionTitle">
                <asp:Label ID="lblTitle" runat="server" Text="Choose a Theme" 
                    meta:resourcekey="lblTitleResource1"></asp:Label></div>
            <div class="previewPanel">
                <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
            </div>
        </div>
        <div class="surveyQuestionSubHeader">
            <!-- sub header -->
            <asp:CheckBox ID="chkDefault" runat="server"               
                meta:resourcekey="chkDefaultResource1" />
            <asp:Label ID="lblDefaultTheme" runat="server" 
                Text=" Set as default theme [Saved as the default theme for new surveys]" 
                meta:resourcekey="lblDefaultThemeResource1"></asp:Label>
            <!-- //sub header -->
        </div>
        <p>
            &nbsp;</p>
        <asp:ListView ID="lstThemesCategory" runat="server" GroupItemCount="5" ItemPlaceholderID="itemPlaceHolder"
            GroupPlaceholderID="groupPlaceHolder">
            <GroupTemplate>
                <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
            </GroupTemplate>
            <LayoutTemplate>
                <asp:PlaceHolder ID="groupPlaceholder" runat="server" />
            </LayoutTemplate>
            <ItemTemplate>
                <div class="surveyQuestionHeader">
                    <!-- classic theme header -->
                    <div class="surveyQuestionTitle">
                        <%# Eval("THEME_CATEGORY")%></div>
                    <div class="previewPanel">
                        &nbsp;</div>
                    <!-- //classic theme header -->
                </div>
                <p>
                    &nbsp;</p>
                <asp:ListView ID="lstThemes" runat="server" DataSource='<%# Eval("osm_themes") %>'
                    OnItemDataBound="lstThemes_ItemDataBound">
                    <ItemTemplate>
                        <div class="themeThumb theme_mn">
                            <p>
                                <asp:RadioButton ID="rbtnTheme" runat="server" Text='<%# Eval("THEME_NAME") %>' 
                                    meta:resourcekey="rbtnThemeResource1"></asp:RadioButton>
                            </p>
                            <asp:Image ID="imgTheme" runat="server" meta:resourcekey="imgThemeResource1" />
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                <p>
                    &nbsp;</p>
                <div class="clear">
                </div>
            </ItemTemplate>
        </asp:ListView>
        <div class="clear">
        </div>
        <div class="bottomButtonPanel">
            <asp:Button ID="btnSave" runat="server" Text="Save" ToolTip="Save"
                CssClass="dynamicButton" onclick="btnSave_Click" 
                meta:resourcekey="btnSaveResource1" />
        </div>
        <!-- //survey question panel -->
    </div>
</asp:Content>
