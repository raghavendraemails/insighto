﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SurveyReport.master" AutoEventWireup="true" CodeFile="DescriptiveResponsesVoice.aspx.cs" Inherits="DescriptiveResponsesVoice" %>

<%@ Register Src="~/UserControls/SurveyReportHeader.ascx" TagName="SurveyReportHeader"
    TagPrefix="uc" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyReportHeader" runat="Server">
    <uc:SurveyReportHeader ID="ucSurveyReportHeader" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SurveyReportMainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel runat="server" ID="pnlLoading" Style="height: 450px; vertical-align: middle;"
        HorizontalAlign="Center">
        <div style="width: 100%; height: 220px;">
        </div>
        <img src="App_Themes/Classic/Images/ajax-loader.gif" alt="LOADING..." /><br />
        <div style="width: 100%; height: 220px;">
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlMain" Style="display: none;">

        <div id="intercontainer">

    <%--     <table  width="100%">
         <tr>
         <td align="right">
        <object id="mediaplayer" classid="clsid:22d6f312-b0f6-11d0-94ab-0080c74c7e95" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#version=5,1,52,701" standby="loading microsoft windows media player components..." type="application/x-oleobject" width="320" height="90">
<param name="filename" value="./AudioFiles/Q1ohrisl.wav"/>             
     <param name="animationatstart" value="true"/>
     <param name="transparentatstart" value="true"/>
     <param name="autostart" value="true"/>
     <param name="showcontrols" value="true"/>
     <param name="ShowStatusBar" value="true"/>
     <param name="windowlessvideo" value="true"/>
     <embed src="./AudioFiles/Q1ohrisl.wav" autostart="true" showcontrols="true" showstatusbar="1" bgcolor="white" width="320" height="100"/>
</object>


<object id="MediaPlayer1"  
        CLASSID="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95"  
        codebase="http://activex.microsoft.com/activex/controls/mplayer/  
                  en/nsmp2inf.cab#Version=5,1,52,701" 
        standby="Loading Microsoft Windows® Media Player components..." 
        TYPE="application/x-oleobject" 
        width="280" 
        height="90"> 
<param name="fileName" value=""> 
<param name="animationatStart" value="true"> 
<param name="transparentatStart" value="true"> 
<param name="autoStart" value="true"> 
<param name="showControls" value="true"> 
<param name="Volume" value="-20"> 
<embed type="application/x-mplayer2" 
      id="myEmbededTag" 
      pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" 
      src="" 
      name="MediaPlayer1" 
      width=280 
      height=256  
      autostart=1 
      showcontrols=1 
      volume=-20> 
</object> 

         </td>
         </tr>
         </table> 
--%>
            <table width="100%">
                <tr>
                    <td align="left">
                        <dxe:ASPxLabel ID="lblHeader" runat="server" Text="Responses" EncodeHtml="false"></dxe:ASPxLabel>
                    </td>
                </tr>
                <tr><td align="right">
              <%--   <img  id="imgLoading" src="App_Themes/Classic/Images/ajax-loader.gif" style="display:none" alt="Downloading the files.Please wait..." />--%>
             <%-- <asp:Label ID="lbldownload" runat="server" Text="test"></asp:Label>--%>

                 <asp:Button runat="server" ID="btndownloadfiles"  Text="DownloadAudioFiles" onclick="btndownloadfiles_Click"  />
                        </td></tr>
                <tr><td class="defaultH"></td></tr>
                <tr>
                    <td align="left">
                        <asp:GridView ID="grdResponses" runat="server" AutoGenerateColumns="false" 
                            GridLines="None" Width="100%" onrowdatabound="grdResponses_RowDataBound">
                        <HeaderStyle CssClass="reportheaderGray" />
                        <RowStyle CssClass="reportrowGray" />
                         <Columns>            
                      <asp:BoundField HeaderText="Sno" DataField="Sno"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  />
                      <asp:BoundField HeaderText="CallerID" DataField="SESSIONID" HeaderStyle-HorizontalAlign ="Center"  ItemStyle-HorizontalAlign="Center" />                   
                       <%--  <asp:HyperLinkField  HeaderText="Answers" DataTextField="Answer_Text" DataTextFormatString="AudioResponse" DataNavigateUrlFields="Answer_Text" runat="server" HeaderStyle-HorizontalAlign="Center"   ItemStyle-HorizontalAlign="Center" />--%>
                         <asp:BoundField HeaderText="Date&Time" DataField="DateTimestamp" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false" />
                         <asp:BoundField HeaderText="ResponseDateTime" DataField="ResponseDateTime" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" />
                         <asp:BoundField HeaderText="CalledNumber" DataField="CalledNumber" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                         <asp:TemplateField HeaderText="Answers" SortExpression="Answer_Text" ItemStyle-HorizontalAlign="Center"> 
                            <ItemTemplate> 
                                <a onclick="javascript:SetVid('<%#Eval("Answer_Text")%>')" class="defaultlink" title='<%#Eval("Answer_Text")%>'>        
                                <asp:Label runat="server"  Text=" <img src='images/Sound-on-icon.png' />Click to listen Audio Response" ></asp:Label>                         
                                    <%--<img src="images/Sound-on-icon.png" alt="Click to listen Audio Response" width="25px" height="25px" title="Click to listen Audio Response" />Click to listen Audio Response--%>
                                    </a> 
                            </ItemTemplate>
                         </asp:TemplateField> 
                      </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr valign="top">
                    <td align="right">                   
                     <asp:Button ID="btnBack" runat="server" ToolTip="Back" Text="Back" CssClass="dynamicButton" OnClick="cmdBack_Click"  />
                       
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <script type="text/javascript" language="javascript">

        function pageLoad(sender, args) {
            //alert('hai');
            TogglePannel('<%= pnlMain.ClientID %>', true);
            TogglePannel('<%= pnlLoading.ClientID %>', false);
        }

        function TogglePannel(target, OnOff) {
            obj = document.getElementById(target);

            if (obj) {
                obj.style.display = (OnOff) ? 'inline' : 'none';
            }
        }


        function HideProgress() {
            parent.document.getElementById("imgLoading").style.display = 'none';
           
        }
        function ShowProgress() {
            parent.document.getElementById("imgLoading").style.display = 'block';
            
        }

    </script><script type="text/javascript">
                 function SetVid(audioUrl) {
//                     var element = document.getElementById('myEmbededTag');
//                   //  alert(element);
//                     element.src = audioUrl;
//                   //  alert(element.src);


//                     var obj = document.getElementById('MediaPlayer1');
//                     document.getElementById('MediaPlayer1').fileName = audioUrl
                     //                     document.getElementById('MediaPlayer1').autoStart = true;
                   //  alert(audioUrl);
                     window.open(audioUrl, "AudioPlay", "toolbar=0,location=0,directories=0,status=0,statusbar=0;menubar=0,resizable=0,scorllbars=0,height=100,width=400,left=650,top=150");
//                                        
                 } 
</script> 
  <script language="JavaScript" type="text/javascript">
      function getFolder() {
          return showModalDialog("folderDialog.htm", "", "width:400px;height:400px;resizeable:yes;");
      }
</script>

<script type="text/javascript">

    function SendMessageToWinControl() {

        var winCtrl = document.getElementById("MyWinControl");

      //  winCtrl..SendMessage("Message sent from the HTML page!!");

    }        

  </script> 

</asp:Content>
