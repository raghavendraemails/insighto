﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Extensions;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using System.Configuration;
using App_Code;

public partial class CopyQuestion : SecurePageBase
{

    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int surveyId = 0;
    int questionId = 0;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = false;

       
        if (!IsPostBack)
        {
            //Load all items in the list box.
            //int nItem = Convert.ToInt32(lbQuesList.Items.Count * 17);
            //lbQuesList.Height = nItem; //Set height depends on the font size.
            //lbQuesList.Width = 800; //This will ensure the list item won't be shrinked!
            //dvPaidSubscription.InnerHtml = Utilities.ResourceMessage("CopyQuestion_PaidSubscription_Details");

            dvNoOfQuestions.InnerHtml = Utilities.ResourceMessage("dvNoOfQuestions");
            dvImagePaidSubscription.InnerHtml = Utilities.ResourceMessage("dvImagePaidSubscription");
            //dvInformation.InnerHtml = Utilities.ResourceMessage("dvInformation");
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains("SurveyId"))
                    surveyId = int.Parse(typeHt["SurveyId"].ToString());
                if (typeHt.Contains("QuestionId"))
                {
                    questionId = int.Parse(typeHt["QuestionId"].ToString());
                    ViewState["QuestionId"] = typeHt["QuestionId"].ToString();
                }
                else
                {
                    ViewState["QuestionId"] = 0;
                }
                if (typeHt.Contains("QuestionSeq"))
                {
                    ViewState["QuestionSeq"] = typeHt["QuestionSeq"].ToString();
                }

            }

            var questionService = ServiceFactory.GetService<QuestionService>();
            var surveyQuestionsList = questionService.GetAllSurveyQuestionsBySurveyId(surveyId);

            if (surveyQuestionsList.Any())
            {
                dvQuestionReorder.Visible = true;

                int QuesNo = 1;
                this.lbQuesList.Items.Clear();
                bool isLenghtExceeded = false;
                foreach (var qus in surveyQuestionsList)
                {
                    string Queslbl = "";
                    var questionTypeId = GenericHelper.ToEnum<QuestionType>(ValidationHelper.GetInteger(qus.QUESTION_TYPE_ID, 0));
                    string PlnQues = CommonMethods.clearHTML(qus.QUSETION_LABEL);

                    if (questionTypeId == QuestionType.Image)
                    {
                        Queslbl = "I " + PlnQues;
                    }
                    else if (questionTypeId == QuestionType.Video)
                    {
                        Queslbl = "V " + PlnQues;
                    }
                    else if (questionTypeId == QuestionType.Narration)
                    {
                        Queslbl = "N " + PlnQues;
                    }
                    else if (questionTypeId == QuestionType.QuestionHeader)
                    {
                        Queslbl = "Q " + PlnQues;
                    }
                    else if (questionTypeId == QuestionType.PageHeader)
                    {
                        Queslbl = "P " + PlnQues;
                    }
                    else
                    {
                        if (QuesNo > 9)
                        {
                            Queslbl = "Q" + QuesNo++ + " " + PlnQues;
                        }
                        else
                        {
                            Queslbl = "Q0" + QuesNo++ + " " + PlnQues;
                        }
                    }
                    ListItem li = new ListItem();
                    //li.Text = Queslbl.GetBlurb(125);
                    
                    li.Text = Queslbl;
                    li.Value = qus.QUESTION_ID.ToString() + "," + qus.QUESTION_SEQ.ToString();
                    if (Queslbl.Length >83)
                    {
                        isLenghtExceeded = true;                       
                    }
                    this.lbQuesList.Items.Add(li);
                }
                if (!isLenghtExceeded)
                {
                    this.lbQuesList.Style.Add("width", "620px");
                }
                lbQuesList.Rows = lbQuesList.Items.Count + 1;

                if (ViewState["QuestionId"] != null)
                {
                    if (ViewState["QuestionId"].ToString() == "0")
                    {
                        this.lbQuesList.SelectedIndex = 0;
                    }
                    else
                    {
                        int iIndex = lbQuesList.Items.IndexOf(lbQuesList.Items.FindByValue(ViewState["QuestionId"].ToString() + "," + ViewState["QuestionSeq"].ToString()));
                        this.lbQuesList.SelectedIndex = iIndex;
                    }
                }
                else
                {
                    this.lbQuesList.SelectedIndex = 0;
                }

            }
            else
            {
                dvQuestionReorder.Visible = false;
                lblErrMsg.Text = Utilities.ResourceMessage("CopyQuestion_Error_Copy"); //"There are no questions to copy.";
                dvErrMsg.Visible = true;
            }
            bool checkQuestionStatus = CheckQuestionCount();
            SetDivByQestion(checkQuestionStatus);
        }
       
    }



    protected void btnSaveReorder_Click(object sender, EventArgs e)
    {

        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains("SurveyId"))
                surveyId = int.Parse(typeHt["SurveyId"].ToString());
            if (typeHt.Contains("QuestionId"))
                    questionId = int.Parse(typeHt["QuestionId"].ToString());
        }

        var questionService = ServiceFactory.GetService<QuestionService>();
        var surveyService = ServiceFactory.GetService<SurveyService>();
       string[] quesDet =  lbQuesList.SelectedValue.Split(',');
       if (quesDet.Length > 0)
       {
           var quesDetails = questionService.GetQuestionDetByQuestionId(ValidationHelper.GetInteger(questionId, 0));
           int questionseq = ValidationHelper.GetInteger(quesDet[1].ToString(),0);

          osm_surveyquestion osmQuestion = new osm_surveyquestion();
          osmQuestion.ANSWER_ALIGNSTYLE = quesDetails[0].ANSWER_ALIGNSTYLE;
          osmQuestion.CHAR_LIMIT = quesDetails[0].CHAR_LIMIT;
          osmQuestion.CHART_ALIGNMENT = quesDetails[0].CHART_ALIGNMENT;
          osmQuestion.CHART_APPEARANCE = quesDetails[0].CHART_APPEARANCE;
          osmQuestion.OTHER_ANS = quesDetails[0].OTHER_ANS;
          osmQuestion.PAGE_BREAK  = 0;
          osmQuestion.QUESTION_SEQ  = questionseq + 1;
          osmQuestion.QUESTION_TYPE_ID = quesDetails[0].QUESTION_TYPE_ID;
          osmQuestion.SURVEY_ID = quesDetails[0].SURVEY_ID;
          osmQuestion.RANDOMIZE_ANSWERS = quesDetails[0].RANDOMIZE_ANSWERS;
          osmQuestion.SKIP_LOGIC = 0;
          osmQuestion.RESPONSE_REQUIRED = quesDetails[0].RESPONSE_REQUIRED;
          osmQuestion.QUSETION_LABEL = quesDetails[0].QUSETION_LABEL;
          questionService.AddSurveyQuestion(osmQuestion);

          //int questSeq = osmQuestion.QUESTION_ID;
          //osmQuestion.QUESTION_SEQ = questionseq + 1;
          //questionService.UpdateQuestionSeqBySurveyId(osmQuestion);
          var surveyQuestionsList = questionService.GetAllSurveyQuestionsBySurveyId(surveyId).Where(quests => quests.QUESTION_SEQ >= questionseq +1 && quests.QUESTION_ID != osmQuestion.QUESTION_ID);

          if (surveyQuestionsList.Any())
          {
              foreach (var questsList in surveyQuestionsList)
              {
                  questsList.QUESTION_SEQ = questsList.QUESTION_SEQ + 1;

                  questionService.UpdateQuestionSeqBySurveyId(questsList);
                  //questSeq++;
              }
          }

            var answerService = ServiceFactory.GetService<SurveyAnswerService>();
       var ansDet = answerService.GetAnswersbyQuestionId(questionId);
       osm_answeroptions osmAnswers = new osm_answeroptions();
        foreach (var answers in ansDet)
        {
            osmAnswers.ANSWER_OPTIONS = answers.ANSWER_OPTIONS;
            osmAnswers.QUESTION_ID = osmQuestion.QUESTION_ID;
            osmAnswers.RESPONSE_REQUIRED = answers.RESPONSE_REQUIRED;
            osmAnswers.DEMOGRAPIC_BLOCKID = answers.DEMOGRAPIC_BLOCKID;
            osmAnswers.SKIP_QUESTION_ID = 0;
            osmAnswers.DELETED = answers.DELETED;
            answerService.AddAnsOptions(osmAnswers);
        }
       }

      


       surveyService.UpdateModifedDateBySurId(surveyId);
       dvSuccessMsg.Visible = true;
       lblSuccMsg.Text = Utilities.ResourceMessage("CopyQuestion_Success_Included"); //"Question included successfully.";
       lblSuccMsg.Visible = true;
       dvErrMsg.Visible = false;
       lblErrMsg.Visible = false;
       base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + surveyId+"&surveyFlag="+base.SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);



    }

    #region "Method :SetDivByQestion"
    /// <summary>
    /// Set Default message when click on AddNew Question
    /// </summary>
    public void SetDivByQestion(bool status)
    {
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userDetails = sessionService.GetLoginUserDetailsSession();
        var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
        var featureService = ServiceFactory.GetService<FeatureService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var licenceValue = featureService.Find(licenceType, FeatureQuestionType.Q_IMAGE).Value;
        string renew_link = "";
        renew_link = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&surveyFlag=" + SurveyFlag);
        var userInfo = userService.GetUserDetails(userDetails.UserId);
        hyp_priclink.Target = "_blank";
        hyp_priclink.HRef = renew_link;

       
        if (licenceType.ToString() == "FREE")
        {
            if (status == false)
            {
                dvImagePaidSubscription.Visible = true;
                dvNoOfQuestions.Visible = true;
                dvPaidSubscription.Visible = true;
                dvReorder.Visible = false;
            }
            else
            {
                dvImagePaidSubscription.Visible = false;
                dvNoOfQuestions.Visible = false;
                dvPaidSubscription.Visible = false;
                dvReorder.Visible = true;

               
            }
            
        }
        


    }
    #endregion

    #region "CheckQuestionCount"
    public bool CheckQuestionCount()
    {
        var questionService = ServiceFactory.GetService<QuestionService>();
        bool checkQuestCount = questionService.CheckQuestionCount(surveyId);
        return checkQuestCount;
    }
    #endregion
}