﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="csat_c4.aspx.cs" Inherits="mktg_popupdemo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Insighto</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<style type="text/css">
#overlay {
position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background-color: #000;
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
z-index: 100;
display: none;
}
.content a{
text-decoration: none;
}
.popup{
width: 100%;
margin: 0;
display: none;
position: fixed;
z-index: 101;
}
.content{
min-width: 600px;
width: 600px;
min-height: 150px;
margin: 100px auto;
background: #f3f3f3;
position: relative;
z-index: 103;
padding: 10px;
border-radius: 5px;
box-shadow: 0 2px 5px #000;
}
.content p{
clear: both;
color: #555555;
text-align: justify;
}
.content p a{
color: #d91900;
font-weight: bold;
}
.content .x{
float: right;
height: 35px;
left: 22px;
position: relative;
top: -25px;
width: 34px;
}
.content .x:hover{
cursor: pointer;
}
</style>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type='text/javascript'>
    $(function () {
        var overlay = $('<div id="overlay"></div>');
        $('.close').click(function () {
            $('.popup').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });

        $('.x').click(function () {
            $('.popup').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });

        $('.click').click(function () {
            overlay.show();
            overlay.appendTo(document.forms);
            overlay.appendTo(document.body);
            $('.popup').show();
            return false;
        });
    });
</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29026379-1']);
    _gaq.push(['_setDomainName', 'insighto.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<style type="text/css">

.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 101;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 607px;
   height: 342px;
   top: 50%;
   left: 35%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #ffffff;
   border: 2px solid Purple;
   padding: 0px;
   z-index: 102;
   font-family: Verdana;
   font-size: 10pt;
}
.web_dialog_title
{
   border-bottom: solid 2px Purple;
   background-color: Purple;
   padding: 4px;
   color: White;
   font-weight:bold;
}
.web_dialog_title a
{
   color: White;
   text-decoration: none;
}
.align_right
{
   text-align: right;
}
.align_left
{
   text-align: left;
}

</style>
    <script src="Scripts/jquery-1.6.2.js" type="text/javascript"></script>

    


<script language="javascript" type="text/javascript">

    function WaterMark(objtxt, event) {
        //        alert("event="+event);
        //        alert("objid=" + objtxt.id);
        //        alert(document.getElementById("<%=lblvalidmsg.ClientID%>").innerText);
        var defaultText = "Name";
        var defaultEmailText = "Email";
        var defaultpwdText = "Password";
        var defaultrpwdText = "Re-type Password";
        // Condition to check textbox length and event type
        if (objtxt.id == "txtName" || objtxt.id == "txtEmail" || objtxt.id == "txtpassword" || objtxt.id == "txtrepassword") {
            if (objtxt.value.length == 0 & event.type == "blur") {
                //  alert(event.type);
                //if condition true then setting text color and default text in textbox
                if (objtxt.id == "txtName") {
                    objtxt.style.color = "Gray";
                    objtxt.value = defaultText;
                    document.getElementById("<%=lblvalidmsg.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtEmail") {
                    objtxt.style.color = "Gray";
                    objtxt.value = defaultEmailText;
                    document.getElementById("<%=lblvalidmsgemail.ClientID%>").innerText = "";

                }
                else if (objtxt.id == "txtpassword") {
                    document.getElementById("<%= txttempwd.ClientID %>").style.display = "block";
                    objtxt.style.display = "none";
                    document.getElementById("<%=lblvalidmsgpwd.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtrepassword") {
                    document.getElementById("<%= txttemrpwd.ClientID %>").style.display = "block";
                    objtxt.style.display = "none";
                    document.getElementById("<%=lblvalidmsgrepwd.ClientID%>").innerText = "";
                }
            }
            else if (objtxt.value.length != 0 & event.type == "blur") {

                // alert("length>0" + event.type);

                if (objtxt.id == "txtName") {
                    document.getElementById("<%=lblvalidmsg.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtEmail") {
                    document.getElementById("<%=lblvalidmsgemail.ClientID%>").innerText = "";

                }
                else if (objtxt.id == "txtpassword") {
                    document.getElementById("<%=lblvalidmsgpwd.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtrepassword") {
                    document.getElementById("<%=lblvalidmsgrepwd.ClientID%>").innerText = "";
                }
            }
        }

        // Condition to check textbox value and event type

        if ((objtxt.value == defaultText || objtxt.value == defaultEmailText || objtxt.value == defaultpwdText || objtxt.value == defaultrpwdText) & event.type == "focus") {

            //  alert(event.type);

            if (objtxt.id == "txtName") {
                objtxt.style.color = "black";
                objtxt.value = "";
            }
            if (objtxt.id == "txtEmail") {
                objtxt.style.color = "black";
                objtxt.value = "";
            }
            if (objtxt.id == "txttempwd") {
                objtxt.style.display = "none";
                document.getElementById("<%= txtpassword.ClientID %>").style.display = "";
                document.getElementById("<%= txtpassword.ClientID %>").focus();
            }
            if (objtxt.id == "txttemrpwd") {
                objtxt.style.display = "none";
                document.getElementById("<%= txtrepassword.ClientID %>").style.display = "";
                document.getElementById("<%= txtrepassword.ClientID %>").focus();
            }
        }
    }

</script>


<style type="text/css">
.signup_blk .frm_blk_txt {
    padding-bottom: 18px;
}

 .lblRequired
        {
            z-index: 1;
            left: 160px;
            top: 335px;
            position: absolute;
            width: 198px;
            height: 19px;
        }
    
</style>
</head>
<body>

     <div class="popup" id="abc" runat="server">
<div class='content'>
<img src='http://www.developertips.net/demos/popup-dialog/img/x.png' alt='quit' class='x' id='x' />
<div  >
        <table width="100%" align="center" border="0" cellspacing="0">
            <tr>
                <%--<td>
                    <div >
                        <div>
                            <p>
                                <b>
                        <asp:Label ID="Label1" runat="server" 
                                    Text="Your Basic Plan on Insighto.com gives you" 
                                    ></asp:Label>
                                    </b></p>
                        </div>
                    </div>
                </td>--%>
                <td   bgcolor="#994ea3" align="center"  border="1" style="font-weight: 500; font-family: Tahoma; font-size: small; color: #FFFFFF; vertical-align: middle">
              <b>Your Basic Plan on Insighto.com gives you
                 </b>  
                </td>
            </tr>
            <tr>
                <td >
                </td>
            </tr>
            <tr>
                <td id="Td2">
                    <asp:Label ID="Label2" runat="server" 
                        Text=" <ul class='liInsighoIcon'> <li><b>Unlimited number of surveys</b></li> <li><b>15 questions per survey</b></li> <li><b>150 responses per survey (that’s 50% more than what you get elsewhere)</b></li> <li><b>18 question types</b></li><li><b>Survey templates</b></li><li><b>Choice of 6 design themes</b></li><li><b>Survey launch through weblink</b></li><li><b>Results in data tables</b></li></ul>" 
                        meta:resourcekey="lblDetailsResource1"></asp:Label>
                </td>
            </tr>
           
        </table>
    </div>
<a href='' class='close'>Close</a>

</div>
</div>    
   <table width="100%">
<tr>
<td>
 
<div id="wrapper">
	<div id="header_main">
    	<a href="http://insighto.com" class="header_logo" target="_blank"><img src="Images/logo_insighto.png"  alt="insighto" /></a>
    </div>
    <div class="clr"></div>
    <form id="Form2" action="#" method="post" name="signup" runat="server">        
    <div id="banner_main" style="position:absolute;margin:auto;z-index:100;top:115px;left:3px;height:420px;">
   	  <div class="bannerL noimg" >
        	<h1>Customer insights made simple.</h1>
            <h3><b>Create questions. Share survey link. Collect responses. See results in real-time. Simple!</b></h3>
        	<p>When you want to survey your customers to get to know them better, you need a tool that makes your job simple. This is where Insighto.com comes in.</p>
            <p>Insighto.com is an online survey software designed to free you from hiring expensive research firms. With step-by-step guidance, you will never be lost while creating and launching a survey. Moreover, you will have access to helpful templates that can jumpstart your survey. And of course, you can see your survey results in real-time. </p>
           <p>And guess what? Your basic plan on Insighto is absolutely free of cost. For life! (What do you get with the Basic Plan? <a href='' class='click'>  <b> Click here </b></a> to know more)</p>
          
            <p class="last">Sign up now and discover the survey engine that’s really simple to use! (no credit card required)</p>
         <%--  <div id="trgtDiv" runat="server" style="position: absolute;margin:auto;z-index:1000;top:100px;left:200px; width: 400px; height: 250px; background-color:#CCFFCC ;"  visible="false"> 

          <div>
            <a href="#"  alt="Close" title="Close" runat="server" id="lnkClose"
                onclick="closeModalSelf(false,'');"></a>
              <asp:LinkButton ID="LinkButton1" runat="server" >X</asp:LinkButton>
        
    </div>
         <div class="popupContentPanel" >
        <table width="100%" align="center" border="0" cellspacing="0">
            
            
            <tr>
                <td>
                    <div class="informationPanelDefault">
                        <div>
                            <p>
                                <b>
                        <asp:Label ID="lblFeature" runat="server" 
                                    Text="Your Basic Plan on Insighto.com gives you" 
                                    ></asp:Label>
                                    </b></p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td id="Td1">
                    <asp:Label ID="lblDetails" runat="server" 
                        Text=" <ul class='liInsighoIcon'> <li><b>Unlimited number of surveys</b></li> <li><b>15 questions per survey</b></li> <li><b>150 responses per survey (that’s 50% more than what you get elsewhere)</b></li> <li><b>18 question types</b></li><li><b>Survey templates</b></li><li><b>Choice of 6 design themes</b></li><li><b>Survey launch through weblink</b></li><li><b>Results in data tables</b></li></ul>" 
                        meta:resourcekey="lblDetailsResource1"></asp:Label>
                </td>
            </tr>
           
        </table>
    </div> 

        </div>--%>
            <div class="testi_blk">
                <div class="testi_container">
                    <h3><b>What the client says</b></h3>
                    <div class="quote_blk"></div>
                    <div class="testi_cont">We used Insighto.com to research customers for an important business pitch. Their survey engine is very easy to use – my team was not only able to build the questionnaire with ease but also analyze the results in real time. Extracting consumer insights from the data was a breeze with Insighto.com and helped us win high appreciation from our Client. </div>
                  
                </div>                <div class="testi_container txtrgt last">
                    <b>Pradeep Ramakrishnan</b><br />
                    Vice President – DDB Mudra
                </div>
            </div>
   			<div class="clr"></div>
        
        </div>
    	<div id="bannerR" style="position:absolute;margin:auto;z-index:1000;top:0px;left:685px;height:445px;">
        	<div class="signup_blk" style="position:absolute;margin:auto;z-index:1000;top:0.5px;left:0.5px;height:425px;">
            	<h2>Discover the really simple survey engine.</h2>
                <div class="title" style="position:absolute;margin:auto;z-index:1000;top:70px;left:35px;font-size:15px;">Sign up now for a Basic Subscription</div>
                <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                <div><asp:Label ID="lblErrorMsg" ForeColor="Red" CssClass="lblRequired" Font-Size="14px" runat="server"></asp:Label></div> 
             </div>  
             <div>
            <asp:Label ID="lblErrMsg" runat="server" ForeColor="Red" CssClass="lblRequired" Visible="False" meta:resourcekey="lblErrMsgResource1">
             </asp:Label>
             </div>
                <div style="position:absolute;margin:auto;z-index:1000;top:100px;left:12px;"><asp:TextBox ID="txtName" Text="Name" runat="server" size="40" onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:133px;left:22px;">
             <asp:Label ID="lblvalidmsg" runat="server" ForeColor="Red" Height="10px"   text-align="top" Font-Size="12px" font-family="Arial"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:150px;left:12px;"><asp:TextBox ID="txtEmail" Text="Email" runat="server" size="40" onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/>
             </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:183px;left:22px;">
           <asp:Label ID="lblvalidmsgemail" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:200px;left:12px;"><asp:TextBox ID="txttempwd" Text="Password" runat="server" size="40" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/></div>
            <div style="position:absolute;margin:auto;z-index:1000;top:200px;left:12px;"><asp:TextBox ID="txtpassword" MaxLength="16" TextMode="password" Text="Password" runat="server" size="40" Style="display:none" onblur="WaterMark(this, event);" Font-Names="Arial" CssClass="txtbox"/>
             </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:233px;left:22px;">
            <asp:Label ID="lblvalidmsgpwd" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:250px;left:12px;"><asp:TextBox ID="txttemrpwd" Text="Re-type Password" runat="server" size="40" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/></div>
            <div style="position:absolute;margin:auto;z-index:1000;top:250px;left:12px;"><asp:TextBox ID="txtrepassword" TextMode="password" Text="Re-type Password" runat="server" size="40" Style="display:none" onblur="WaterMark(this, event);" Font-Names="Arial" CssClass="txtbox"/>
             </div>
             </div>
       
            <div style="position:absolute;margin:auto;z-index:1000;top:283px;left:22px;">
            <asp:Label ID="lblvalidmsgrepwd" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/></div>
           
           
                <%--<div class="frm_blk"><input name="" type="text" class="txtbox" value="Name" /></div>
                <div class="frm_blk"><input name="" type="text" class="txtbox" value="Email" /></div>
                <div class="frm_blk"><input name="" type="text" class="txtbox" value="Password" /></div>
                <div class="frm_blk_txt"><input name="" type="text" class="txtbox" value="Re-type Password" /></div>
                --%>
                <div class="frm_blk_txt" style="position:absolute;margin:auto;z-index:1000;top:300px;left:10px">By signing up, you agree to the <a href="http://www.insighto.com/In/termsandconditions.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Terms & Conditions</a> and the <a href="http://www.insighto.com/In/privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy Policy</a></div>
                <div class="frm_blk_txt" style="position:absolute;margin:auto;z-index:1000;top:350px;left:15px">
                    <asp:Button ID="SignUp" runat="server" src="Images/btn_bg.png" 
                                Text="Sign Up"  Border="0" 
                        BackColor="#3366FF" CssClass="btn_bg" /></div>

                        
                        <div class="txt_free" style="position:absolute;margin:auto;z-index:1000;top:360px;left:150px;color:#9B4795;">It's Free</div>
                <div class="frm_blk_txt note last" style="position:absolute;margin:auto;z-index:1000;top:400px;left:35px">*No credit card details required</div>
                <div class="clr"></div>
            </div>
        </div>
        <div id="Div2" class="web_dialog">  
 <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
     <tr>
      <td style="background-color:Purple;height:25px">
       <table width="100%">
         <tr>
            <td class="web_dialog_title align_left" width="50%">
              <label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:white">Yes / No</label>
            </td>
         <td class="web_dialog_title align_right" width="50%">
            <a href="#" id="btnClose2">Close</a>
         </td>
          </tr>
      </table>
      </td>
      </tr>
    
      <tr>
   
   <td>
   </td>
   </tr>
   </table>
    </div>
        </form>
   
    
    
    <div class="clr"></div> 
    
    
        
    <div id="container_main" style="position:absolute;margin:auto;z-index:100;top:600px;left:0px">
    	     
          <div class="box_blk" style="position:absolute;margin:auto;z-index:100;left:0px;">
        <table>
        <tr>
        <td>
        	<div class="colm3">
               <h3 class="txtcnt">Use helpful templates</h3>
                <p class="last1">Get the benefit of 15 ready-made templates for the most popular types of surveys conducted in the market. And start on your surveys faster. </p>
            </div>
            </td>
            <td>
        	<div class="colm3">
                <h3 class="txtcnt">Get 50% more responses</h3>
                <p class="last1">With Insighto.com, even a free subscription gives you 150 responses per survey making it 50% better than what others in the market give.</p>
            </div>
            </td>
            <td>
        	<div class="colm3">
                <h3 class="txtcnt">See results in real-time</h3>
                <p class="last1">You will get survey results in your dashboard updated to the minute. The feedback your audience gives you is with you in real-time.</p>
            </div>
            </td>
            </tr>
            <tr>
            <td>                               
        	<div class="colm3">
                <h3 class="txtcnt">Insighto is intuitive</h3>
                <p class="last1">You don’t have to be a researcher to start researching your audience. Insighto is a DIY survey tool that requires no training.</p>
            </div>
            </td>
            <td>
             	<div class="colm3">
                <h3 class="txtcnt">Insighto is simple</h3>
                <p class="last1">We work by the belief that software should be easy to use. Which is why, simplicity is a feature built into Insighto.</p>
            </div>
            </td>
            <td>
        	<div class="colm3">
                <h3 class="txtcnt">Insighto is versatile</h3>
                <p class="last1">You might want to test different aspects of Customer Satisfaction at different points in time. Whatever your survey needs, you can do it with Insighto.</p>
            </div>
            </td>
            </tr>
            </table>
            
      
   			<div class="clr"></div>
        </div>
   		<div class="clr"></div>
  </div>
    

  </div>
  
     
<div id="footer_main" style="position:absolute;margin:auto;z-index:1000;top:960px;left:0px;">
    <div id="footer_blk">
            <div id="footerL">Copyright &copy; 2014 Insights</div>
            <div id="footerR">
                <ul>
                    <li><a href="http://www.insighto.com/In/ContactUs.aspx">Contact Us</a></li>
                    <li><a href="http://www.insighto.com/In/antispam.aspx?key=6auQ29rICLYtdOGhX9UHsQ%3d%3d">Anti spam policy</a></li>
                    <li><a href="http://www.insighto.com/In/privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy policy</a></li>
                    
                </ul>
            </div>
            <div class="clr"></div>
        </div>
</div>

</td>
</tr>
</table>

</body>
</html>
