﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cust_sat.aspx.cs" Inherits="MKTG_cust_sat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sign Up for Free Online Survey & Questionnaire Software for Customer Feedback - Insighto</title>
<link id="lnkFavIcon" rel="shortcut icon" type="image/vnd.microsoft.icon" href="http://www.insighto.com/insighto.ico" />
<link href="css/master.css" rel="stylesheet" type="text/css" />
<link href="css/fonts.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29026379-1']);
    _gaq.push(['_setDomainName', 'insighto.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>


<%--<script type="text/javascript">
 $(document).ready(function ($) {
     $('.left_main_txtbox').css({ width: $(bannerbg).width() / 2 });
 });

 </script>--%>
<script language="javascript" type="text/javascript">



    function WaterMark(objtxt, event) {
//        alert("event="+event);
//        alert("objid=" + objtxt.id);
//        alert(document.getElementById("<%=lblvalidmsg.ClientID%>").innerText);
        var defaultText = "Enter Name";
        var defaultEmailText = "Enter Email Id";
        var defaultpwdText = "Enter Password";
        var defaultrpwdText = "Retype Password";
        // Condition to check textbox length and event type
        if (objtxt.id == "txtName" || objtxt.id == "txtEmail" || objtxt.id == "txtpassword" || objtxt.id == "txtrepassword") {
            if (objtxt.value.length == 0 & event.type == "blur") {
              //  alert(event.type);
                //if condition true then setting text color and default text in textbox
                if (objtxt.id == "txtName") {
                    objtxt.style.color = "Gray";
                    objtxt.value = defaultText;
                    document.getElementById("<%=lblvalidmsg.ClientID%>").innerText = "";
                }
              else if (objtxt.id == "txtEmail") {
                    objtxt.style.color = "Gray";
                    objtxt.value = defaultEmailText;
                    document.getElementById("<%=lblvalidmsgemail.ClientID%>").innerText = "";

                }
               else if (objtxt.id == "txtpassword") {
                    document.getElementById("<%= txttempwd.ClientID %>").style.display = "block";
                    objtxt.style.display = "none";
                    document.getElementById("<%=lblvalidmsgpwd.ClientID%>").innerText = "";
                }
               else if (objtxt.id == "txtrepassword") {
                    document.getElementById("<%= txttemrpwd.ClientID %>").style.display = "block";
                    objtxt.style.display = "none";
                    document.getElementById("<%=lblvalidmsgrepwd.ClientID%>").innerText = "";
                }
            }
            else if (objtxt.value.length != 0 & event.type == "blur") {

               // alert("length>0" + event.type);

                if (objtxt.id == "txtName") {                  
                    document.getElementById("<%=lblvalidmsg.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtEmail") {                   
                    document.getElementById("<%=lblvalidmsgemail.ClientID%>").innerText = "";

                }
                else if (objtxt.id == "txtpassword") {                   
                    document.getElementById("<%=lblvalidmsgpwd.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtrepassword") {                   
                    document.getElementById("<%=lblvalidmsgrepwd.ClientID%>").innerText = "";
                }
            }
        }

        // Condition to check textbox value and event type

        if ((objtxt.value == defaultText || objtxt.value == defaultEmailText || objtxt.value == defaultpwdText || objtxt.value == defaultrpwdText) & event.type == "focus") {

          //  alert(event.type);

            if (objtxt.id == "txtName") {
                objtxt.style.color = "black";
                objtxt.value = "";
            }
            if (objtxt.id == "txtEmail") {
                objtxt.style.color = "black";
                objtxt.value = "";
            }
            if (objtxt.id == "txttempwd") {
                objtxt.style.display = "none";
                document.getElementById("<%= txtpassword.ClientID %>").style.display = "";
                document.getElementById("<%= txtpassword.ClientID %>").focus();
            }
            if (objtxt.id == "txttemrpwd") {
                objtxt.style.display = "none";
                document.getElementById("<%= txtrepassword.ClientID %>").style.display = "";
                document.getElementById("<%= txtrepassword.ClientID %>").focus();
            }
        }
    }

</script>

    <style type="text/css">
        .lblRequired
        {
            z-index: 1;
            left: 115px;
            top: 299px;
            position: absolute;
            width: 198px;
            height: 19px;
        }
    </style>

</head>
 
<body>
<table width="100%">
<tr>
<td>


<!--Header Start from here-->
<div id="header">
	<div class="maincontainer">
    	<div class="logo"><a href="http://insighto.com" target="_blank"><img src="images/logo.png" border="0" /></a></div>
    </div>
</div>
<!--Header Ends here-->
 
<!--Banner Start from here-->
<div id="bannerbg">
	<div class="maincontainer">
    	<div class="left_main_txtbox">
        	<div class="main_txt" style="margin:115px 20px 15px 20px;" >Measuring Customer Satisfaction is easier done than said<span style="color:#9a4694	;">.</span></div>
            <div class="and_its_better" style="margin:10px 20px 20px 20px;">And it&rsquo;s 50% better.</div>
        </div>
        <form id="Form2" action="#" method="post" name="signup" runat="server">
        <div class="from_box" style="position:absolute;margin:auto;z-index:1000;top:110px;left:900px;height:390px;">
        	<div class="to_discoverd_survey" style="position:absolute;margin:auto;z-index:1000;top:10px;left:20px;">To discover the survey engine, that&rsquo;s 50% better</div>
            <div class="sign_up_now" style="position:absolute;margin:auto;z-index:1000;top:70px;left:40px;font-size:15px;"><strong>sign up now</strong> for a Basic Subscription</div>
            <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
              
              <div><asp:Label ID="lblErrorMsg" ForeColor="Red" CssClass="lblRequired" Font-Size="14px" runat="server"></asp:Label></div> 
             </div>  
             <div>
            <asp:Label ID="lblErrMsg" runat="server" ForeColor="Red" CssClass="lblRequired" Visible="False" meta:resourcekey="lblErrMsgResource1">
             </asp:Label>
             </div>
            <%--<a href="../In/App_LocalResources/SignUpFree.aspx.resx">../In/App_LocalResources/SignUpFree.aspx.resx</a>--%>
             
            <div style="position:absolute;margin:auto;z-index:1000;top:100px;left:4px;" ><asp:TextBox ID="txtName" Text="Enter Name" runat="server" size="40" onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="form_field"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:140px;left:30px;" >
             <asp:Label ID="lblvalidmsg" runat="server" ForeColor="Red" Height="10px"   text-align="top" Font-Size="14px" font-family="Arial"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:155px;left:4px;"><asp:TextBox ID="txtEmail" Text="Enter Email Id" runat="server" size="40" onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="form_field"/>
             </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:190px;left:30px;" >
           <asp:Label ID="lblvalidmsgemail" runat="server" ForeColor="Red" text-align="top" Font-Size="14px" font-family="Arial"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:205px;left:4px;"><asp:TextBox ID="txttempwd" Text="Enter Password" runat="server" size="40" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="form_field"/></div>
            <div style="position:absolute;margin:auto;z-index:1000;top:205px;left:4px;"><asp:TextBox ID="txtpassword" MaxLength="16" TextMode="password" Text="Enter Password" runat="server" size="40" Style="display:none" onblur="WaterMark(this, event);" Font-Names="Arial" CssClass="form_field"/>
             </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:240px;left:30px;" >
            <asp:Label ID="lblvalidmsgpwd" runat="server" ForeColor="Red" text-align="top" Font-Size="14px" font-family="Arial"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:255px;left:4px;"><asp:TextBox ID="txttemrpwd" Text="Retype Password" runat="server" size="40" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="form_field"/></div>
            <div style="position:absolute;margin:auto;z-index:1000;top:255px;left:4px;"><asp:TextBox ID="txtrepassword" TextMode="password" Text="Enter Re-type Password" runat="server" size="40" Style="display:none" onblur="WaterMark(this, event);" Font-Names="Arial" CssClass="form_field"/>
             </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:290px;left:30px;" >
            <asp:Label ID="lblvalidmsgrepwd" runat="server" ForeColor="Red" text-align="top" Font-Size="14px" font-family="Arial"/></div>
             
            <%--<div><input type="text" id="txtName" runat="server" value="Enter Name" size="40" onblur="WaterMark(txtName, event);" onfocus="WaterMark(txtName, event);" class="form_field"/></div>
            <div><input type="text" id="txtEmail" runat="server" value="Enter Email Id" size="40" onblur="WaterMarkeml(txtEmail, event);" onfocus="WaterMarkeml(txtEmail, event);" class="form_field"/></div>
            <div><input type="password" id="txtpassword" runat="server" value="Enter Password"  size="40" onblur="WaterMarkpwd(txtpassword, event);" onfocus="WaterMarkpwd(txtpassword, event);" class="form_field"/></div>
            <div><input type="password" id="txtrepassword" runat="server" value="Enter Re-typePassword" size="40" onblur="WaterMarkrpwd(txtrepassword, event);" onfocus="WaterMarkrpwd(txtrepassword, event);" class="form_field"/></div>
            --%>
            <div class="sign_up_detail">
            	<div class="clear"></div>
                    <div>
                        <div class="sign_up_button" style="position:absolute;margin:auto;z-index:1000;top:320px;left:35px">
                            <asp:ImageButton ID="SignUp" runat="server" src="Images/sign_up_tab.png" 
                                Text="SIGN UP"  Height="40px" 
                                Width="120px" BackColor="#3366FF" Border="0" OnClick="SignUp_Click" /></div>
                        <div class="its_free" style="position:absolute;margin:auto;z-index:1000;top:327px;left:170px">It's Free</div>
                    </div><br />
                    <!--<div style="padding:9px 0px 3px 0px;"><img src="images/sign_up_with_linkedin.png" border="0" /></div>-->
                    <div class="no_credit_card"  style="position:absolute;margin:auto;z-index:1000;top:360px;left:35px">*No credit card details required</div>
                <div class="clear"></div>
            </div>
        </div>
        </form>
    </div>
</div>
<br />
<!--Banenr Ends here-->
 
<!--Main container Start from here-->
<div class="maincontainer">
    <div class="clear"></div>
    <div class="main_content" style="position:absolute;margin:auto;z-index:1000;top:550px;left:160px">
    	<div class="helpful_temp_box">
        	<div class="helpful_template">Helpful templates</div>
            <div class="header_bottom_border"></div>
            <div class="content">You have the benefit of 15 templates for the most popular type of surveys that are conducted in the market – so, you don’t have to spend too much time on what questions to ask and how to ask them.</div>
        </div>
        
        <div class="helpful_temp_box">
        	<div class="helpful_template">50% more responses</div>
            <div class="header_bottom_border"></div>
            <div class="content">With Insighto.com, even a free subscription gives you 150 responses per survey making it 50% better than what others in the market give.</div>
        </div>
        
        <div class="real_time_box">
        	<div class="helpful_template">Real-time reporting</div>
            <div class="header_bottom_border"></div>
            <div class="content">You will see reports in your dashboard updated to the minute. Thus, the feedback your audience gives you is with you in real-time.</div>
        </div>
        
    </div>
    <div class="clear"></div>
    <div style="margin:30px 0px 30px 0px;">
    <div class="page_main_content" style="position:absolute;margin:auto;z-index:1000;top:760px;left:150px">
    	<div style="height:31px;">&nbsp;</div>
        <div class="page_main_content_shadow">&nbsp;</div>
        <div class="content">There was a time when customer satisfaction surveys were handled only by professional research agencies and used to be a costly affair. But no longer.<br /><br />
        You don&rsquo;t have to be a professional researcher to start your Customer Satisfaction Survey with the help of the DIY easy to use survey tool from Insighto.com.<br /><br />
        Sign up now (no credit card details required) and discover why Insighto.com is the home for measuring Customer Satisfaction.</div>
    </div>
    
    <div class="wht_client_sys_box" style="position:absolute;margin:auto;z-index:1000;top:760px;right:250px;left:620px">
    	<div class="wht_client_sys">What the client says</div>
        <div class="header_bottom_border"></div>
        <div><img src="images/apostrophy.png" border="0" />
        <div class="testimonial_content">We used Insighto.com to research customers for an important business pitch. Their survey engine is very easy to use – my team was not only able to build the questionnaire with ease but also analyze the results in real time. Extracting consumer insights from the data was a breeze with Insighto.com and helped us win high appreciation from our Client.</div>
        <div class="testimonial_by"><strong style="color:#5a5a5a;">Pradeep Ramakrishnan</strong><br />
        Vice President, Data Analytics, Research, Tools. MudraMax Media
    </div>
    
</div>
</div>
</div>
<div class="clear"></div>
<!--Main container Ends here-->
 
<!--Footer Start from here-->
<div class="footer_area" style="position:absolute;margin:auto;z-index:1000;top:1100px;left:100px;right:10px">
	<div class="copyright">Copyright © 2014 Knowience Insights</div>
    <div class="privacy">
    	<ul>
        	<li><a href="http://www.insighto.com/In/ContactUs.aspx" target="_blank">Contact Us</a></li>
            <li>|</li>
            <li><a href="http://www.insighto.com/In/antispam.aspx?key=6auQ29rICLYtdOGhX9UHsQ%3d%3d" target="_blank">Anti spam policy</a></li>
            <li>|</li>
            <li><a href="http://www.insighto.com/In/privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI" target="_blank">Privacy policy</a></li>
        </ul>
    </div>
</div>
<!--Main container Ends here-->

</td>
</tr>
</table>
</body>
</html>

