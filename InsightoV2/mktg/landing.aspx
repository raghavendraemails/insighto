﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="landing.aspx.cs" Inherits="landing" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sign Up for Free Online Survey & Questionnaire Software for Customer Feedback - Insighto</title>
<link id="lnkFavIcon" rel="shortcut icon" type="image/vnd.microsoft.icon" href="http://www.insighto.com/insighto.ico" />
<link href="css/master.css" rel="stylesheet" type="text/css" />
<link href="css/fonts.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
    function WaterMark(txtName, event) {
        var defaultText = "Enter Name";
        // Condition to check textbox length and event type
        if (txtName.value.length == 0 & event.type == "blur") {
            //if condition true then setting text color 
            //and default text in textbox
            txtName.style.color = "Gray";
            txtName.value = defaultText;
        }
        // Condition to check textbox value and event type
        if (txtName.value == defaultText & event.type == "focus") {
            txtName.style.color = "black";
            txtName.value = "";
        }
        }
        function WaterMarkeml(txtEmail, event) {
        var defaultText = "Enter Email Id";
        // Condition to check textbox length and event type
        if (txtEmail.value.length == 0 & event.type == "blur") {
            //if condition true then setting text color 
            //and default text in textbox
            txtEmail.style.color = "Gray";
            txtEmail.value = defaultText;
        }
        // Condition to check textbox value and event type
        if (txtEmail.value == defaultText & event.type == "focus") {
            txtEmail.style.color = "black";
            txtEmail.value = "";
        }
        }
        function WaterMarkpwd(txtpassword, event) {
            var defaultText = "Enter Password";
            // Condition to check textbox length and event type
            if (txtpassword.value.length == 0 & event.type == "blur") {
                //if condition true then setting text color 
                //and default text in textbox
                txtpassword.style.color = "Gray";
                txtpassword.value = defaultText;
            }
            // Condition to check textbox value and event type
            if (txtpassword.value == defaultText & event.type == "focus") {
                txtpassword.style.color = "black";
                txtpassword.value = "";
            }
        }
        function WaterMarkrpwd(txtrepassword, event) {
        var defaultText = "Enter Re-typePassword";
        // Condition to check textbox length and event type
        if (txtrepassword.value.length == 0 & event.type == "blur") {
            //if condition true then setting text color 
            //and default text in textbox
            txtrepassword.style.color = "Gray";
            txtrepassword.value = defaultText;
        }
        // Condition to check textbox value and event type
        if (txtrepassword.value == defaultText & event.type == "focus") {
            txtrepassword.style.color = "black";
            txtrepassword.value = "";
        }
    }
</script>




</head>
 
<body>
<!--Header Start from here-->
<div id="header">
	<div class="maincontainer">
    	<div class="logo"><a href="#" target="_blank"><img src="images/logo.png" border="0" /></a></div>
    </div>
</div>
<!--Header Ends here-->
 
<!--Banner Start from here-->
<div id="bannerbg">
	<div class="maincontainer">
    	<div class="left_main_txtbox">
        	<div class="main_txt" style="margin:150px 20px 15px 20px;">Measuring Customer Satisfaction is easier done than said<span style="color:#9a4694	;">.</span></div>
            <div class="and_its_better" style="margin:10px 20px 20px 20px;">And it&rsquo;s 50% better.</div>
        </div>
        <form id="Form2" action="#" method="post" name="signup" runat="server">
        <div class="from_box">
        	<div class="to_discoverd_survey">To discover the survey engine, that&rsquo;s 50% better</div>
            <div class="sign_up_now"><strong>sign up now</strong> for a Basic Subscription</div><div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            
            
            <div><input type="text" id="txtName" runat="server" value="Enter Name" size="40" onblur="WaterMark(txtName, event);" onfocus="WaterMark(txtName, event);" class="form_field"/></div>
            <div><input type="text" id="txtEmail" runat="server" value="Enter Email Id" size="40" onblur="WaterMarkeml(txtEmail, event);" onfocus="WaterMarkeml(txtEmail, event);" class="form_field"/></div>
            <div><input type="text" id="txtpassword" runat="server" value="Enter Password"  size="40" onblur="WaterMarkpwd(txtpassword, event);" onfocus="WaterMarkpwd(txtpassword, event);" class="form_field"/></div>
            <div><input type="text" id="txtrepassword" runat="server" value="Enter Re-typePassword" size="40" onblur="WaterMarkrpwd(txtrepassword, event);" onfocus="WaterMarkrpwd(txtrepassword, event);" class="form_field"/></div>
            
            <div class="sign_up_detail">
            	<div class="clear"></div>
                    <div>
                        <div class="sign_up_button"><asp:Button ID="SignUp" runat="server" src="Images/sign_up_tab.png" 
                                Text="SIGN UP"  Height="40px" 
                                Width="120px" BackColor="#3366FF" Border="0"/></div>
                        <div class="its_free">It's Free</div>
                    </div>
                    <!--<div style="padding:9px 0px 3px 0px;"><img src="images/sign_up_with_linkedin.png" border="0" /></div>-->
                    <div class="no_credit_card">*No credit card details required</div>
                <div class="clear"></div>
            </div>
        </div>
        </form>
    </div>
</div>
<!--Banenr Ends here-->
 
<!--Main container Start from here-->
<div class="maincontainer">
	<div class="logo_area">
    	<div class="logo_imrb">&nbsp;</div>
        <div class="logo_ddb_mudra">&nbsp;</div>
        <div class="logo_the_strategist">&nbsp;</div>
        <div class="logo_people_matters">&nbsp;</div>
        <div class="logo_tmp_worldwide">&nbsp;</div>
    </div>
    <div class="clear"></div>
    <div class="main_content">
    	<div class="helpful_temp_box">
        	<div class="helpful_template">Helpful templates</div>
            <div class="header_bottom_border"></div>
            <div class="content">You have the benefit of 15 templates for the most popular type of surveys that are conducted in the market – so, you don’t have to spend too much time on what questions to ask and how to ask them.</div>
        </div>
        
        <div class="helpful_temp_box">
        	<div class="helpful_template">50% more responses</div>
            <div class="header_bottom_border"></div>
            <div class="content">With Insighto.com, even a free subscription gives you 150 responses per survey making it 50% better than what others in the market give.</div>
        </div>
        
        <div class="real_time_box">
        	<div class="helpful_template">Real-time reporting</div>
            <div class="header_bottom_border"></div>
            <div class="content">You will see reports in your dashboard updated to the minute. Thus, the feedback your audience gives you is with you in real-time.</div>
        </div>
        
    </div>
    <div class="clear"></div>
    <div style="margin:30px 0px 30px 0px;">
    <div class="page_main_content">
    	<div style="height:31px;">&nbsp;</div>
        <div class="page_main_content_shadow">&nbsp;</div>
        <div class="content">There was a time when customer satisfaction surveys were handled only by professional research agencies and used to be a costly affair. But no longer.<br /><br />
        You don&rsquo;t have to be a professional researcher to start your Customer Satisfaction Survey with the help of the DIY easy to use survey tool from Insighto.com.<br /><br />
        Sign up now (no credit card details required) and discover why Insighto.com is the home for measuring Customer Satisfaction.</div>
    </div>
    
    <div class="wht_client_sys_box">
    	<div class="wht_client_sys">What the client says</div>
        <div class="header_bottom_border"></div>
        <div><img src="images/apostrophy.png" border="0" />
        <div class="testimonial_content">We used Insighto.com to research customers for an important business pitch. Their survey engine is very easy to use – my team was not only able to build the questionnaire with ease but also analyze the results in real time. Extracting consumer insights from the data was a breeze with Insighto.com and helped us win high appreciation from our Client.</div>
        <div class="testimonial_by"><strong style="color:#5a5a5a;">Pradeep Ramakrishnan</strong><br />
        Vice President, Data Analytics, Research, Tools. MudraMax Media
    </div>
    
</div>
</div>
</div>
<div class="clear"></div>
<!--Main container Ends here-->
 
<!--Footer Start from here-->
<div class="footer_area">
	<div class="copyright">Copyright © 2014 Knowience Insights</div>
    <div class="privacy">
    	<ul>
        	<li><a href="http://www.insighto.com/In/ContactUs.aspx" target="_blank">Contact Us</a></li>
            <li>|</li>
            <li><a href="http://www.insighto.com/In/antispam.aspx?key=6auQ29rICLYtdOGhX9UHsQ%3d%3d" target="_blank">Anti spam policy</a></li>
            <li>|</li>
            <li><a href="http://www.insighto.com/In/privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI" target="_blank">Privacy policy</a></li>
        </ul>
    </div>
</div>
<!--Main container Ends here-->
</body>
</html>

