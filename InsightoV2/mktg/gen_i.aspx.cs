﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using App_Code;
using CovalenseUtilities.Services;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;
using DotNetOpenAuth.OpenId.RelyingParty;
using Insighto.Business.Helpers;
using Insighto.Business.Services;

public partial class mktg_gen_i : System.Web.UI.Page
{
    string country;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public bool IsPasswordStrong(string p)
    {
        if (string.IsNullOrEmpty(p))
            return false;
        else
        {
            var regex = new Regex("^.{6,16}$");
            return regex.IsMatch(p) && !p.EndsWith(".");
        }
    }

    public bool Isvalidname(string a)
    {
        if (string.IsNullOrEmpty(a))
            return false;
        else
        {
            var regex = new Regex("^[a-zA-Z ]*$");
            return regex.IsMatch(a) && !a.EndsWith(".");
        }

    }

    public bool IsValidEmailAddress(string s)
    {
        if (string.IsNullOrEmpty(s))
            return false;
        else
        {
            var regex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            return regex.IsMatch(s) && !s.EndsWith(".");
        }
    }

    #region btnCreateAccount_Click
    protected void SignUp_Click(object sender, EventArgs e)
    {
        if (txtName.Text == "Name")
        {
            lblvalidmsg.Text = "Enter Name";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";

        }
        else if (!Isvalidname(txtName.Text))
        {
            lblvalidmsg.Text = "Please enter alphabets only";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";
        }

        else if (txtEmail.Text == "Email")
        {

            lblvalidmsgemail.Text = "Enter email";
            lblvalidmsg.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";
        }
        else if (!IsValidEmailAddress(txtEmail.Text))
        {
            lblvalidmsgemail.Text = "Enter valid Email";
            lblvalidmsg.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";
        }

        else if (!IsPasswordStrong(txtpassword.Text))
        {
            lblvalidmsgpwd.Text = "Password should contain 6-16 characters";
            lblvalidmsgemail.Text = "";
            lblvalidmsg.Text = "";
            lblvalidmsgrepwd.Text = "";
        }

        else if (lblvalidmsgpwd.Text == "Password")
        {

            lblvalidmsgpwd.Text = "Enter Password";
            lblvalidmsgemail.Text = "";
            lblvalidmsg.Text = "";
            lblvalidmsgrepwd.Text = "";
        }
        else if (txtrepassword.Text == "Re-typePassword")
        {

            lblvalidmsgrepwd.Text = "Enter Re-typePassword";
            lblvalidmsg.Text = "";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
        }


        else if (txtpassword.Text != txtrepassword.Text)
        {
            lblvalidmsgrepwd.Text = "Password and Retype Password should match";
            lblvalidmsg.Text = "";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
        }


        else
        {


            if (Page.IsValid)
            {
                //if (txtCaptcha.Text == ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession())
                //{
                var userService = new UsersService();
                string ipAddress = string.Empty;
                if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                    ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
                var user = userService.RegisterFreeUser(txtName.Text.Trim(), string.Empty, txtEmail.Text.Trim(), string.Empty, 0, ipAddress, txtpassword.Text.Trim());
                country = Utilities.GetCountryName(ipAddress);
                ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
                ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
                country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();

                if (user == null)
                {

                    lblErrorMsg.Text = Utilities.ResourceMessage("lblErrMsg");
                    dvErrMsg.Visible = true;
                    lblErrMsg.Visible = false;
                }
                else
                {
                    ServiceFactory.GetService<SessionStateService>().SaveUserToSession(user);
                    //  Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserSignUpFreeNextURL(),"userId="+user.USERID));
                    //  Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=0"));
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserPricingpageintermediate(), "UserId=" + user.USERID));
                }


                //}
                //else
                //{
                //    txtCaptcha.Focus();
                //    lblErrMsg.Text = Utilities.ResourceMessage("lblCaptchaErrMsg");
                //    lblErrMsg.Visible = true;
                //    dvErrMsg.Visible = false;
                //}
            }
    #endregion
        }
    }
    

    }
