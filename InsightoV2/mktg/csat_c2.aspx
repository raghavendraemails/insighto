﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="csat_c2.aspx.cs" Inherits="mktg_csat_c2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Insighto</title>
<link href="css/style.css" rel="stylesheet" type="text/css">

<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29026379-1']);
    _gaq.push(['_setDomainName', 'insighto.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>


<script language="javascript" type="text/javascript">

    function WaterMark(objtxt, event) {
        //        alert("event="+event);
        //        alert("objid=" + objtxt.id);
        //        alert(document.getElementById("<%=lblvalidmsg.ClientID%>").innerText);
        var defaultText = "Name";
        var defaultEmailText = "Email";
        var defaultpwdText = "Password";
        var defaultrpwdText = "Re-type Password";
        // Condition to check textbox length and event type
        if (objtxt.id == "txtName" || objtxt.id == "txtEmail" || objtxt.id == "txtpassword" || objtxt.id == "txtrepassword") {
            if (objtxt.value.length == 0 & event.type == "blur") {
                //  alert(event.type);
                //if condition true then setting text color and default text in textbox
                if (objtxt.id == "txtName") {
                    objtxt.style.color = "Gray";
                    objtxt.value = defaultText;
                    document.getElementById("<%=lblvalidmsg.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtEmail") {
                    objtxt.style.color = "Gray";
                    objtxt.value = defaultEmailText;
                    document.getElementById("<%=lblvalidmsgemail.ClientID%>").innerText = "";

                }
                else if (objtxt.id == "txtpassword") {
                    document.getElementById("<%= txttempwd.ClientID %>").style.display = "block";
                    objtxt.style.display = "none";
                    document.getElementById("<%=lblvalidmsgpwd.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtrepassword") {
                    document.getElementById("<%= txttemrpwd.ClientID %>").style.display = "block";
                    objtxt.style.display = "none";
                    document.getElementById("<%=lblvalidmsgrepwd.ClientID%>").innerText = "";
                }
            }
            else if (objtxt.value.length != 0 & event.type == "blur") {

                // alert("length>0" + event.type);

                if (objtxt.id == "txtName") {
                    document.getElementById("<%=lblvalidmsg.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtEmail") {
                    document.getElementById("<%=lblvalidmsgemail.ClientID%>").innerText = "";

                }
                else if (objtxt.id == "txtpassword") {
                    document.getElementById("<%=lblvalidmsgpwd.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtrepassword") {
                    document.getElementById("<%=lblvalidmsgrepwd.ClientID%>").innerText = "";
                }
            }
        }

        // Condition to check textbox value and event type

        if ((objtxt.value == defaultText || objtxt.value == defaultEmailText || objtxt.value == defaultpwdText || objtxt.value == defaultrpwdText) & event.type == "focus") {

            //  alert(event.type);

            if (objtxt.id == "txtName") {
                objtxt.style.color = "black";
                objtxt.value = "";
            }
            if (objtxt.id == "txtEmail") {
                objtxt.style.color = "black";
                objtxt.value = "";
            }
            if (objtxt.id == "txttempwd") {
                objtxt.style.display = "none";
                document.getElementById("<%= txtpassword.ClientID %>").style.display = "";
                document.getElementById("<%= txtpassword.ClientID %>").focus();
            }
            if (objtxt.id == "txttemrpwd") {
                objtxt.style.display = "none";
                document.getElementById("<%= txtrepassword.ClientID %>").style.display = "";
                document.getElementById("<%= txtrepassword.ClientID %>").focus();
            }
        }
    }

</script>

<style type="text/css">
.signup_blk .frm_blk_txt {
    padding-bottom: 18px;
}

 .lblRequired
        {
            z-index: 1;
            left: 160px;
            top: 335px;
            position: absolute;
            width: 198px;
            height: 19px;
        }
    
</style>
</head>

<body>
<table width="100%">
<tr>
<td>

<div id="wrapper">
	<div id="header_main">
    	<a href="http://insighto.com" class="header_logo" target="_blank"><img src="Images/logo_insighto.png"  alt="insighto" /></a>
    </div>
    <div class="clr"></div>
    <form id="Form2" action="#" method="post" name="signup" runat="server">        
    <div id="banner_main" style="position:absolute;margin:auto;z-index:1000;top:115px;left:3px;height:420px;">
   	  <div class="bannerL noimg" >
        	<h1>Welcome to the really simple way of gathering customer feedback.</h1>
        	<p>When you are managing marketing in your company, you know how important (and how difficult) it is to keep your customers delighted. And at the core of keeping customers delighted is to know them well. This is where Insighto.com comes in. </p>
            <p>Insighto.com is an online survey software designed to free you from hiring expensive research firms. With step-by-step guidance, you will never be lost while creating and launching a survey on Insighto.com. Moreover, you will have access to helpful templates that can jumpstart your survey. Of course, you can see your survey results in real-time.<br />
            And guess what? Your basic plan on Insighto is absolutely free of cost. For life!</p>
            <p class="last">Sign up now and discover the survey engine that’s really simple to use! (no credit card required)</p>
            <br />
            <div class="testi_blk">
                <div class="testi_container">
                    <h3><b>What the client says</b></h3>
                    <div class="quote_blk"></div>
                    <div class="testi_cont">We used Insighto.com to research customers for an important business pitch. Their survey engine is very easy to use – my team was not only able to build the questionnaire with ease but also analyze the results in real time. Extracting consumer insights from the data was a breeze with Insighto.com and helped us win high appreciation from our Client. </div>
                    <div class="clr"></div>
                </div>
                <div class="clr"></div>
                <div class="testi_container txtrgt last">
                    <b>Pradeep Ramakrishnan</b><br />
                    Vice President – DDB Mudra
                </div>
            </div>
   			<div class="clr"></div>
        
        </div>
    	<div id="bannerR" style="position:absolute;margin:auto;z-index:1000;top:0px;left:685px;height:445px;">
        	<div class="signup_blk" style="position:absolute;margin:auto;z-index:1000;top:0.5px;left:0.5px;height:425px;">
            	<h2>Discover the really simple survey engine.</h2>
                <div class="title" style="position:absolute;margin:auto;z-index:1000;top:70px;left:35px;font-size:15px;">Sign up now for a Basic Subscription</div>
                <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                <div><asp:Label ID="lblErrorMsg" ForeColor="Red" CssClass="lblRequired" Font-Size="14px" runat="server"></asp:Label></div> 
             </div>  
             <div>
            <asp:Label ID="lblErrMsg" runat="server" ForeColor="Red" CssClass="lblRequired" Visible="False" meta:resourcekey="lblErrMsgResource1">
             </asp:Label>
             </div>
                <div style="position:absolute;margin:auto;z-index:1000;top:100px;left:12px;"><asp:TextBox ID="txtName" Text="Name" runat="server" size="40" onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:133px;left:22px;">
             <asp:Label ID="lblvalidmsg" runat="server" ForeColor="Red" Height="10px"   text-align="top" Font-Size="12px" font-family="Arial"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:150px;left:12px;"><asp:TextBox ID="txtEmail" Text="Email" runat="server" size="40" onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/>
             </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:183px;left:22px;">
           <asp:Label ID="lblvalidmsgemail" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:200px;left:12px;"><asp:TextBox ID="txttempwd" Text="Password" runat="server" size="40" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/></div>
            <div style="position:absolute;margin:auto;z-index:1000;top:200px;left:12px;"><asp:TextBox ID="txtpassword" MaxLength="16" TextMode="password" Text="Password" runat="server" size="40" Style="display:none" onblur="WaterMark(this, event);" Font-Names="Arial" CssClass="txtbox"/>
             </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:233px;left:22px;">
            <asp:Label ID="lblvalidmsgpwd" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/>
            </div>
            <div style="position:absolute;margin:auto;z-index:1000;top:250px;left:12px;"><asp:TextBox ID="txttemrpwd" Text="Re-type Password" runat="server" size="40" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/></div>
            <div style="position:absolute;margin:auto;z-index:1000;top:250px;left:12px;"><asp:TextBox ID="txtrepassword" TextMode="password" Text="Re-type Password" runat="server" size="40" Style="display:none" onblur="WaterMark(this, event);" Font-Names="Arial" CssClass="txtbox"/>
             </div>
             </div>
       
            <div style="position:absolute;margin:auto;z-index:1000;top:283px;left:22px;">
            <asp:Label ID="lblvalidmsgrepwd" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/></div>
           
           
                <%--<div class="frm_blk"><input name="" type="text" class="txtbox" value="Name" /></div>
                <div class="frm_blk"><input name="" type="text" class="txtbox" value="Email" /></div>
                <div class="frm_blk"><input name="" type="text" class="txtbox" value="Password" /></div>
                <div class="frm_blk_txt"><input name="" type="text" class="txtbox" value="Re-type Password" /></div>
                --%>
                <div class="frm_blk_txt" style="position:absolute;margin:auto;z-index:1000;top:300px;left:10px">By signing up, you agree to the <a href="http://www.insighto.com/In/termsandconditions.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Terms & Conditions</a> and the <a href="http://www.insighto.com/In/privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy Policy</a></div>
                <div class="frm_blk_txt" style="position:absolute;margin:auto;z-index:1000;top:350px;left:15px">
                    <asp:Button ID="SignUp" runat="server" src="Images/btn_bg.png" 
                                Text="Sign Up"  Border="0" 
                        BackColor="#3366FF" CssClass="btn_bg" onclick="SignUp_Click"/></div>

                        
                        <div class="txt_free" style="position:absolute;margin:auto;z-index:1000;top:360px;left:150px;color:#9B4795;">It's Free</div>
                <div class="frm_blk_txt note last" style="position:absolute;margin:auto;z-index:1000;top:400px;left:35px">*No credit card details required</div>
                <div class="clr"></div>
            </div>
        </div>
        </form>
   
    
    
    <div class="clr"></div> 
    
        
    <div id="container_main" style="position:absolute;margin:auto;z-index:1000;top:600px;left:0px">
    	     
          <div class="box_blk" style="position:absolute;margin:auto;z-index:1000;left:0px;">
        <table>
        <tr>
        <td>
        	<div class="colm3">
                <h3 class="txtcnt">Insighto is intuitive</h3>
                <p class="last1">You don't have to be a researcher to start researching your audience. Insighto is a DIY survey tool that requires no training.</p>
            </div>
            </td>
            <td>
        	<div class="colm3">
                <h3 class="txtcnt">Insighto is simple</h3>
                <p class="last1">We work by the belief that software should be easy to use. Which is why, simplicity is a feature built into Insighto.</p>
            </div>
            </td>
            <td>
        	<div class="colm3">
                <h3 class="txtcnt">Insighto is versatile</h3>
                <p class="last1">You might want to test different aspects of Customer Satisfaction at different points in time. Whatever your survey needs, you can do it with Insighto.</p>
            </div>
            </td>
            </tr>
            <tr>
            <td>                               
        	<div class="colm3">
                <h3 class="txtcnt">Use helpful templates</h3>
                <p class="last1">Get the benefit of 15 ready-made templates for the most popular types of surveys conducted in the market. And start on your surveys faster. </p>
            </div>
            </td>
            <td>
             	<div class="colm3">
                <h3 class="txtcnt">Get 50% more responses</h3>
                <p class="last1">With Insighto.com, even a free subscription gives you 150 responses per survey making it 50% better than what others in the market give.</p>
            </div>
            </td>
            <td>
        	<div class="colm3">
                <h3 class="txtcnt">Leverage real-time reports</h3>
                <p class="last1">You will get reports in your dashboard updated to the minute. The feedback your audience gives you is with you in real-time.  </p>
            </div>
            </td>
            </tr>
            </table>
            
      
   			<div class="clr"></div>
        </div>
   		<div class="clr"></div>
  </div>
    

  </div>
  
     
<div id="footer_main" style="position:absolute;margin:auto;z-index:1000;top:960px;left:0px;">
    <div id="footer_blk">
            <div id="footerL">Copyright &copy; 2014 Insights</div>
            <div id="footerR">
                <ul>
                    <li><a href="http://www.insighto.com/In/ContactUs.aspx">Contact Us</a></li>
                    <li><a href="http://www.insighto.com/In/antispam.aspx?key=6auQ29rICLYtdOGhX9UHsQ%3d%3d">Anti spam policy</a></li>
                    <li><a href="http://www.insighto.com/In/privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy policy</a></li>
                    
                </ul>
            </div>
            <div class="clr"></div>
        </div>
</div>
</td>
</tr>
</table>
</body>
</html>
