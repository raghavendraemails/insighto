﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PreLaunch.master" AutoEventWireup="true"
    CodeFile="PreLaunchSurveyEmailList.aspx.cs" Inherits="PreLaunchSurveyEmailList"%>

<asp:Content ID="Content1" ContentPlaceHolderID="PreLaunchMainContent" runat="Server">
    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblSuccMsg" runat="server" 
                meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
    </div>
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
    </div>
    <div class="surveyQuestionHeader">
        <table width="100%">
            <tr>
                <td class="wid_70">
                    <div class="surveyQuestionTitle">
                      <asp:Label ID="lblSurveyEmailList" runat="server" 
                            meta:resourcekey="lblSurveyEmailListResource1"></asp:Label></div>
                </td>
                <td class="wid_30">
                    <asp:Button ID="btnEdit" runat="server" Text=" "  CssClass="icon-pen-active"
                        OnClick="btnEdit_Click" meta:resourcekey="btnEditResource1" />
                </td>
            </tr>
        </table>
    </div>
    <div class="surveyQuestionTabPanel">
        <table width="100%" id="tblEmailList" runat="server" visible="false">
            <tr>
                <td width="100%">
                    <asp:HyperLink ID="hlnkEmailList" runat="server" 
                        meta:resourcekey="hlnkEmailListResource1"></asp:HyperLink>
                </td>
            </tr>
        </table>
       <div class="prelaunchbtn"> <asp:Button ID="btnBack" runat="server" 
                CssClass="prelaunchbackbtn" OnClick="btnBack_Click" 
               meta:resourcekey="btnBackResource1" /></div>
    </div>
</asp:Content>
