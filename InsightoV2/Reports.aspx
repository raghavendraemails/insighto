﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SurveyReport.master" AutoEventWireup="true"  
    CodeFile="Reports.aspx.cs" Inherits="Reports" ViewStateMode="Enabled" EnableViewState="false"  %>

<%@ Register Src="~/UserControls/SurveyReportHeader.ascx" TagName="SurveyReportHeader"
    TagPrefix="uc" %>
    <%@ Register Src="~/UserControls/NotificationControl.ascx" TagName="NotificationControl"
    TagPrefix="uc1" %>
    <%@ Register Src="~/UserControls/Header.ascx" TagPrefix="ucheader" TagName="Header" %>

<%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.XtraCharts.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.XtraCharts.v9.1.Web, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
    <%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxcp" %>
 <%@ Register src="~/UserControls/ErrorSuccessNotifier.ascx" tagname="ErrorSuccessNotifier" tagprefix="Insighto" %>

<asp:Content ID="surveyReportHeader" ContentPlaceHolderID="SurveyReportHeader" runat="Server">
    <uc:SurveyReportHeader ID="ucSurveyReportHeader" runat="server" />
     
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="SurveyReportMainContent" runat="Server" >
    <div >
        <!-- notification panel -->
        <uc1:NotificationControl ID="NotificationControl1" runat="server" />
        <!-- //notification panel -->
    </div>
    <asp:ScriptManager ID="ScriptMgr1" runat="server" AsyncPostBackTimeout="100000">
    <Scripts>
   <asp:ScriptReference Path="~/UpdatePanel/updatepanelhook.fusioncharts.js" />
    </Scripts>  

    </asp:ScriptManager>
    <%--<script src="Scripts/fusioncharts.js"></script>--%>
    <script type="text/javascript">

        // Get the instance of PageRequestManager.

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        // Add initializeRequest and endRequest

        prm.add_initializeRequest(prm_InitializeRequest);

        prm.add_endRequest(prm_EndRequest);



        // Called when async postback begins

        function prm_InitializeRequest(sender, args) {

            // get the divImage and set it to visible

            var panelProg = $get('divImage');

            panelProg.style.display = '';


            // reset label text

            var lbl = $get('<%= this.lblText.ClientID %>');

            lbl.innerHTML = '';



            // Disable button that caused a postback

            $get(args._postBackElement.id).disabled = true;

        }



        // Called when async postback ends

        function prm_EndRequest(sender, args) {

            // get the divImage and hide it again

            var panelProg = $get('divImage');

            panelProg.style.display = 'none';


            // Enable button that caused a postback

            $get(sender._postBackSettings.sourceElement.id).disabled = false;

        }

     
         </script>
  
    <script type="text/javascript">

        function upgradelicence() {
            OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
            return false;
        }       
        function CheckAccess() {
            OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
            $('.rbtnData').find("input[value='rbtnData']").attr("checked", "checked");          
            return false;
        } 
               function alertmessage(s, e) {

                 
                   var licensetype = '<%=strlicensetype%>';

                   var index = rdgdisplayset2.GetSelectedIndex();
                 
            if (index == 0 && document.getElementById("MainContent_SurveyReportMainContent_fea_exppt").value == "0") {
                //e.processOnServer=false;
                OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                e.processOnServer = false;

                //return false;
            }
            else if (index == 1 && document.getElementById("MainContent_SurveyReportMainContent_fea_expdf").value == "0") {
                //e.processOnServer=false;
                OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                e.processOnServer = false;
                //return false; 
            }
            else if (index == 2 && document.getElementById("MainContent_SurveyReportMainContent_fea_exexc").value == "0") {
                OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                e.processOnServer = false;
                //return false;
            }
            else if (index == 3 && document.getElementById("MainContent_SurveyReportMainContent_fea_exwor").value == "0") {
                OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                e.processOnServer = false;
                //return false;
            }
            if (licensetype != "FREE") {
                if (index != 2 && document.getElementById("MainContent_SurveyReportMainContent_fea_exexc").value == "1") {
                    var x = "This action exports only closed ended questions and responses. For exporting open ended responses, please use “Export to Excel” feature.\nExport of report will take several seconds. You will be notified once the report is generated.Please wait....";
                    jAlert(x);
                    //return true;
                }


                if (index == 2) {
                    var x = "Export of report will take several seconds. You will be notified once the report is generated.Please wait....";
                    jAlert(x);
                    //return true;
                }
            }

        }
        function OnExportChanged(s, e) {


        }

        function showHide(EL, PM) {
            ELpntr = document.getElementById(EL);
            if (ELpntr.style.display == 'none') {
                document.getElementById(PM).innerHTML = "[-] ";
                ELpntr.style.display = 'block';
                document.getElementById("<%= lblExportmessage.ClientID %>").innerHTML = "***Export of Report will take few minutes.Please wait...";
               
            }
            else {
                document.getElementById(PM).innerHTML = "[+]";
                ELpntr.style.display = 'none';
                document.getElementById("<%= lblExportmessage.ClientID %>").innerHTML = "";
            }
        }

    </script>
    <script type='text/javascript'>

        function centerVertically(objectID) {


            var thisObj = document.getElementById(objectID);
            
            var height = (window.innerHeight) ? window.innerHeight : document.documentElement.clientHeight;
            var objectHeight = parseInt(thisObj.style.height);
            var newLocation = (height - objectHeight) / 2;


            thisObj.style.top = newLocation + 'px';




        }
        function centerHorizontally(objectID) {
            var thisObj = document.getElementById(objectID);
            var width = (window.innerWidth) ? window.innerWidth : document.body.clientWidth;
            var objectWidth = parseInt(thisObj.style.width);
            var newLocation = (width - objectWidth) / 2;
            thisObj.style.left = newLocation + 'px';
        } 


        function GetRadioButtonListSelectedValue(radioButtonList) {

            for (var i = 0; i < radioButtonList.rows.length; ++i) {

                if (radioButtonList.rows[i].cells[0].firstChild.checked) {

                    radioButtonList.rows[i].cells[0].firstChild.checked = true;
                    document.getElementsByName('<%=HiddenField1.ClientID %>').value = radioButtonList.rows[i].cells[0].firstChild.value;
                    return true;
                }

            }

        }
</script>
  

    <script src="Charts/FusionCharts.js" type="text/javascript"></script>
     <script type="text/javascript" src="Charts/jquery.min.js"></script>
      <script type="text/javascript" src="FusionCharts/FusionCharts.js"></script>   
    <script src="Charts/FusionChartsExportComponent.js" type="text/javascript"></script>

	<script type="text/javascript">
	    //The Preloader is loaded here by checking the Initialization of JavaScript Class.
	  //  var chartnamerepeat = "chart,";
	    FusionCharts.addEventListener("initialized", function (e, a) {
	   // alert(e.sender.id);


	    $("#chartContainerParent #floader").show();
	      
	    });
	    FusionCharts.setCurrentRenderer("javascript");

	    FusionCharts.addEventListener("Rendered", function (e, a) {
	       // alert("ppt");
	        FusionCharts(e.sender.id).exportChart(); 
            $("#chartContainerParent #floader").hide();
	    });

</script>



    <asp:Panel runat="server" ID="pnlLoading" Style="height: 450px; vertical-align: middle;"
        HorizontalAlign="Center">
        <div style="width: 100%; height: 220px;">
        </div>
        <div style="width: 100%; height: 220px;">
        </div>
    </asp:Panel>
    
    <asp:UpdatePanel ID="updpan1" runat="server">
    
    <ContentTemplate>
    <asp:Label ID="lblText" runat="server" Text=""></asp:Label>
  <div id="divImage" style="display:none;position:absolute;margin:auto;z-index:1000;top:185px;left:600px" >      
    
                     <table border="0px" width="100%" >
                   <tr>
                   <td align="center">
                   <table border="1px" style="border-bottom-color:Purple;border-left-color:Purple;border-right-color:Purple;border-top-color:Purple;border:1px;width:100px;color:Purple;">
                   <tr><td align="center">Processing.....</td></tr></table>
                    </td>
                   </tr>
                   </table>
                    
                </div> 

    <asp:Panel runat="server" ID="pnlMain" Style="display: none;">
        
        <input id="fea_exppt" type="hidden" value="1" runat="server" />
        <input id="fea_exexc" type="hidden" value="1" runat="server" />
        <input id="fea_exwor" type="hidden" value="1" runat="server" />
        <input id="fea_expdf" type="hidden" value="1" runat="server" />
        <input id="fea_datacharts" type="hidden" value="1" runat="server" />
        <input id="fea_charts" type="hidden" value="1" runat="server" />

        <div id="trgtDiv" runat="server" style="position: absolute;margin:auto;z-index:1000;top:100px;left:400px; width: 600px; height: 400px; background-color: white;"  visible="false"> 

          <div class="popupClosePanel">
            <a href="#"  alt="Close" title="Close" runat="server" id="lnkClose"
                onclick="closeModalSelf(false,'');">X</a>
        
    </div>
         <div class="popupContentPanel" >
        <table width="100%" align="center" border="0" cellspacing="0">
            <tr>
                <td align="left" valign="middle">
                    <ucheader:Header ID="Header1" runat="server" />
                </td>
            </tr>
            <tr>
                <td height="1" bgcolor="#994ea3">
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td>
                    <div class="informationPanelDefault">
                        <div>
                            <p>
                                <b>
                        <asp:Label ID="lblFeature" runat="server" 
                                    Text="This is a paid feature!" 
                                    meta:resourcekey="lblFeatureResource1"></asp:Label>
                                    <asp:Label ID="lblPreFeature" runat="server" 
                                    Text="This feature is available only for Insighto Premium Single / premium Annual subscriptions." 
                                    meta:resourcekey="lblPreFeatureResource1" Visible="false"></asp:Label></b></p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-left:40px;">
                    <h4>
                        <asp:Label ID="lblExperience" runat="server" 
                            Text="Experience Insighto Paid plans and get access to these Powerful features:" 
                            meta:resourcekey="lblExperienceResource1"></asp:Label>
                            <asp:Label ID="lblPreExperience" runat="server" 
                            Text="Buy Insighto Premium and get access to these powerful features:" 
                            meta:resourcekey="lblPreExperienceResource1" Visible="false"></asp:Label>
                        </h4>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td id="Td1">
                    <asp:Label ID="lblDetails" runat="server" 
                        Text="<ul class='liInsighoIcon'> <li><b>Download results to PPT, Excel, Word or PDF</b></li> <li><b>Launch survey through insighto's email system</b></li>  <li><b>Password protected survey</b></li> <li><b>Schedule survey launch and close time and more </b></li></ul>" 
                        meta:resourcekey="lblDetailsResource1"></asp:Label>
                        <asp:Label ID="lblPreDetails" runat="server" 
                        Text="<ul class='liInsighoIcon'> <li><b>All features of Pro +</b></li> <li><b>Export raw data</b></li><li><b>View individual responses</b></li><li><b>Redirection of URL</b></li> <li><b>Remove Insighto branding on your survey end page </b></li></ul>" 
                        meta:resourcekey="lblPreDetailsResource1" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="upgradeNowBtnPanel">
                        <ul class="HyperlinkBtn">
                            <li><a href="#" id="hyp_priclink" runat="server" onclick="opener.focus()">
                                <asp:Label ID="lblUpgrade" runat="server" Text="Upgrade Now!" 
                                    meta:resourcekey="lblUpgradeResource1"></asp:Label></a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div> 

        </div>
        <div class="surveyCreateLeftMenu">
    
            <!-- survey left menu -->
            <ul class="reportsMenu">
                <li><a href="#" class="activeLink">Reports</a></li>
            </ul>
            <div class="reportExpandPanel">
                <p>
                    <a href="#" onclick="showHide('divDisplay','spanDisplay')"><span id="spanDisplay"
                        style="font-size: 10px; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: normal;
                        color: #666666">[+]</span><b> Display:</b></a></p>
                    
                      
                <div id="divDisplay" style="display: block;">
                    
                     <asp:RadioButtonList ID="rblgrp" runat="server"  onclick="return GetRadioButtonListSelectedValue(this);"  AutoPostBack="true" >
                  <asp:ListItem Value="DataChart" Text="Data+Chart"></asp:ListItem>
                  <asp:ListItem Value="Data" Text="Data"></asp:ListItem>
                  <asp:ListItem Value="Chart" Text="Chart"></asp:ListItem>
          </asp:RadioButtonList>
          
          <asp:HiddenField ID="HiddenField1" runat="server"  />


     
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                             
                         <asp:Button ID="btnviewall" runat="server" Text="View All" onclick="btnviewall_Click" CssClass="btn_small_viewall"  />

                </div>

                
                   
            </div>
            <div class="reportExpandPanel">
                <p>
                    <a href="#" onclick="showHide('divExportTo','spanExportTo')"><span id="spanExportTo"
                        style="font-size: 10px; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: normal;
                        color: #666666">[+]</span><b> Export to:</b></a></p>
                <div id="divExportTo" style="display: none;">
                    <table width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblExportmessage" ForeColor="Red" Font-Size="Smaller" Font-Bold="true" runat="server"></asp:Label>
                        </td>
                    </tr>
                        <tr>
                            <td colspan="2">
                            
                                <dxe:ASPxRadioButtonList ID="rdgdisplayset2" runat="server" RepeatDirection="Horizontal"
                                    ItemSpacing="5px" TextWrap="False" SelectedIndex="0" ClientInstanceName="rdgdisplayset2"  >

                                    <Items>
                                        <dxe:ListEditItem Value="PPT" ImageUrl="App_Themes/Classic/Images/icon-ppt-18x18.gif" />
                                        <dxe:ListEditItem Value="PDF" ImageUrl="App_Themes/Classic/Images/icon-pdf-18x18.gif" />
                                        <dxe:ListEditItem Value="Excel" ImageUrl="App_Themes/Classic/Images/icon-excel-18x18.gif" />
                                        <dxe:ListEditItem Value="Word" ImageUrl="App_Themes/Classic/Images/icon-word-18x18.gif" />
                                    </Items>
                                    <Border BorderColor="Transparent" BorderStyle="None" />
                                  
                                </dxe:ASPxRadioButtonList>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lbn_rawdataexport" runat="server" Text="Export Raw Data to Excel" 
                                    OnClick="lbn_rawdataexport_Click"></asp:LinkButton>
                            </td>
                             <td align="right">
                                <asp:Button ID="cmdExport" runat="server" Text="OK" CssClass="btn_small_viewall"  OnClick="cmdExport_Click" OnClientClick="prm_InitializeRequest()" />  
                               
                           </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="reportExpandPanel">
                <p>
                    <a href="#" onclick="showHide('divDate','spanDate')"><span id="spanDate" style="font-size: 10px;
                        font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: normal; color: #666666">
                        [+]</span><b> Filter responses by date range:</b></a></p>
                <div id="divDate" style="display: none;">
                    <p class="defaultHeight">
                    </p>
                    <p>
                        <dxe:ASPxLabel ID="lblstartdate" runat="server" Text="Start Date :">
                        </dxe:ASPxLabel>
                    </p>
                    <p>
                        <dxe:ASPxDateEdit ID="dedstartdate" CssClass="dropdownSmall" runat="server">
                        </dxe:ASPxDateEdit>
                    </p>
                    <p class="defaultHeight">
                    </p>
                    <p>
                        <dxe:ASPxLabel ID="lblenddate" runat="server" Text="End Date :">
                        </dxe:ASPxLabel>
                    </p>
                    <p>
                        <dxe:ASPxDateEdit ID="dedEnddate" CssClass="dropdownSmall" runat="server">
                        </dxe:ASPxDateEdit>
                    </p>
                    <p class="defaultHeight">
                    </p>
                   
                        <table border="0" width="100%">
                            <tr>
                                <td align="right">                             
                                    <asp:Button ID="cmdSearch" CssClass="dynamicButtonSmall" runat="server" Text="Search" OnClick="cmdSearch_Click" />
                                </td>
                                </tr>
                        </table>
                    
                </div>
            </div>
            <ul class="reportsMenu">

                <li><a id="CrossTabRpts" runat="server"><span>Cross Tab Report&nbsp;<asp:Image ID="imgCrossTabReports"
                    runat="server" ImageUrl="~/App_Themes/Classic/Images/icon_pro.png" Visible="false" /></span></a></li>
                <li><a id="IndividualRes" runat="server"><span>Individual Responses&nbsp;&nbsp;<asp:Image
                    ID="imgIndividualResponses" runat="server" ImageUrl="~/App_Themes/Classic/Images/icon_prem.png"
                    Visible="false" /></span></a></li>
            </ul>
            <!-- //survey left menu -->
        </div>
        <div class="surveyQuestionPanel">
            <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblSuccMsg" runat="server"></asp:Label></div>
            </div>
            <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label></div>
            </div>
            <div class="quationtxtPanel" id="divSendReport" runat="server" visible="false">
  
             <Insighto:ErrorSuccessNotifier ID="esSendReportMail" runat="server" />
            </div>
            <!-- survey question panel -->

            <div class="surveyQuestionHeader">
                <div class="surveyQuestionTitle">
                    Reports</div>
            </div>
            <!-- //survey question panel -->
            <div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin: 0 0 8px 0">
                    <tr>
                        <td align="left" valign="top">
                            <span class="rightheads">Questions:</span><br />                            

                            <asp:Label ID="ASPxRoundPanelquestions" runat="server" Text="Label"  AutoPostBack="true"></asp:Label>
                        </td>
                    </tr>
                    
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left" valign="top">      
                         <asp:Panel ID="rpnl_reportsdata" runat="server"  CssClass="padno"  Width="600">
                         </asp:Panel>  
                         
                            <dxcp:ASPxCallbackPanel ID="ASPxRoundPanelQuestionreports" ClientInstanceName="panel" runat="server" >  
                              </dxcp:ASPxCallbackPanel>                          
                             
                              <asp:Panel ID="rpnl_reportsdataexport" runat="server"  CssClass="padno"   Width="600">
                         </asp:Panel>  
                               
                        </td>
                    </tr>
                </table>
            </div>
           
           <div id="chartContainerParent" runat="server"><div id="floader"></div>

           <asp:Literal ID="LiteralQTInitial" runat="server"></asp:Literal>
            <asp:Literal ID="LiteralQT1" runat="server"></asp:Literal>
            <asp:Literal ID="LiteralQT2" runat="server"></asp:Literal>
             <asp:Literal ID="LiteralQT3" runat="server"></asp:Literal>
              <asp:Literal ID="LiteralQT4" runat="server"></asp:Literal>
               <asp:Literal ID="LiteralQT13" runat="server"></asp:Literal>
               <asp:Literal ID="LiteralQT10" runat="server"></asp:Literal>
               <asp:Literal ID="LiteralQT11" runat="server"></asp:Literal>
               <asp:Literal ID="LiteralQT12" runat="server"></asp:Literal>
               <asp:Literal ID="LiteralQT15" runat="server"></asp:Literal>
                 <div class="clear"></div>              
         
        </div>
       

      
        <dxp:ASPxPanel ID="ASPxRoundPanelQuestionreports1" runat="server" Visible="false">
            <PanelCollection>
                <dxp:PanelContent ID="PanelContent3" runat="server">
                </dxp:PanelContent>
            </PanelCollection>
        </dxp:ASPxPanel>
</asp:Panel>
       
        
       
</ContentTemplate>
</asp:UpdatePanel>      

    <script type="text/javascript">
        function OpenNewTabPage() {
            var surveyid = '<%=surveyID %>';
           // window.open('OnlineReport.aspx?surveyid=' + surveyid, "", "");

             window.open('OnlineReport.aspx?surveyid=' + surveyid, '', 'directories=no,location=no,resize=yes,addressbar=no,status=no,scrollbars=yes,titlebar=no,toolbar=no,fullscreen=yes,menubar=no');
            //mywindow.focus();
        }
    </script>

    <script type="text/javascript" language="javascript">

        
        function pageLoad(sender, args) {
            //alert('hai');
            //  TogglePannel('<%= pnlMain.ClientID %>', true);
            TogglePannel('<%= pnlMain.ClientID %>', true);
            TogglePannel('<%= pnlLoading.ClientID %>', false);
        }

        function TogglePannel(target, OnOff) {
            obj = document.getElementById(target);

            if (obj) {
                obj.style.display = (OnOff) ? 'inline' : 'none';
            }
        }
        function CheckPermissions() {      
            OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
            return false;
        }
 
    </script>
    <script type="text/javascript">

        //Confirmation box sample

        function ConfirmationBox() {

            var result = confirm("Are you sure you want to continue?");

            if (result == true) {

                return true;

            }

            else {

                var lbl = document.getElementById('lbltxt');

                lbl.innerHTML = "";

                return false;

            }

        }
      
</script>
    <div id="reportsdata" style="display:none;">

    </div>
</asp:Content>