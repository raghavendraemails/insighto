﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using App_Code;


public partial class Font : SurveyPageBase
{
    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int surveyId = 0;

    #endregion

    #region "Event : Page_Load"
    /// <summary>
    /// page load.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void Page_Load(object sender, EventArgs e)
    {
        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = false;

        if (!IsPostBack)
        {
            BindType();
            BindSize();
            BindColor();            

            BindExistsData();

            if (SetFeatures("CUSTOMIZE_FONT") == 0)
            {
                //ddlType.Attributes.Add("onchange", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                //ddlColor.Attributes.Add("onchange", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                //ddlSize.Attributes.Add("onchange", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                btnSave.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }

            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            //if (loggedInUserInfo.LicenseType == "FREE")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            //}

        }
    }

    #endregion

    #region "Method : BindExistsData"
    /// <summary>
    /// used to bind existing data.
    /// </summary>

    public void BindExistsData()
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }
        if (surveyId > 0)
        {
            var surveySettingsService = ServiceFactory.GetService<SurveySetting>();
            var surveySettings = surveySettingsService.GetSurveySettings(surveyId);

            if (SetFeatures("CUSTOMIZE_FONT") == 1)
            {
                ddlType.SelectedValue = surveySettings[0].SURVEY_FONT_TYPE;
                ddlSize.SelectedValue = surveySettings[0].SURVEY_FONT_SIZE;
                ddlColor.SelectedValue = surveySettings[0].SURVEY_FONT_COLOR;
            }
            else
            {
                ddlType.SelectedValue = "Arial";
                ddlSize.SelectedValue = "12";
                ddlColor.SelectedValue = "Black";
                //div_font.Visible = true;
            }
        }
        else
        {
            ddlType.SelectedValue = "Arial";
            ddlSize.SelectedValue = "12";
            ddlColor.SelectedValue = "Black";
        }
        lblPreview.Style.Add("font-family", ddlType.SelectedValue);
        lblPreview.Style.Add("font-size", ddlSize.SelectedValue + "pt");
        lblPreview.Style.Add("color", ddlColor.SelectedValue);
    }
    #endregion

    #region "Method : BindType"
    /// <summary>
    /// used to bind font types.
    /// </summary>

    public void BindType()
    {
        var pickListService = ServiceFactory.GetService<PickListService>();
        osm_picklist osmPicklist = new osm_picklist();
        osmPicklist.CATEGORY = Constants.FONTTYPE;
        List<osm_picklist> picklist = pickListService.GetPickList(osmPicklist);
        ddlType.DataSource = picklist;
        ddlType.DataTextField = Constants.PARAMETER;
        ddlType.DataValueField = Constants.PARAMETER;
        ddlType.DataBind();
    }
    #endregion

    #region "Method : BindColor"
    /// <summary>
    /// used to bind font colors.
    /// </summary>

    public void BindColor()
    {
        var pickListService = ServiceFactory.GetService<PickListService>();
        osm_picklist osmPicklist = new osm_picklist();
        osmPicklist.CATEGORY = Constants.FONTCOLOR;
        List<osm_picklist> picklist = pickListService.GetPickList(osmPicklist);
        ddlColor.DataSource = picklist;
        ddlColor.DataTextField = Constants.PARAMETER;
        ddlColor.DataValueField = Constants.PARAMETER;
        ddlColor.DataBind();
    }

    #endregion

    #region "Method : BindSize"
    /// <summary>
    /// used to bind font sizes.
    /// </summary>

    public void BindSize()
    {
        var pickListService = ServiceFactory.GetService<PickListService>();
        osm_picklist osmPicklist = new osm_picklist();
        osmPicklist.CATEGORY = Constants.FONTSIZE;
        List<osm_picklist> picklist = pickListService.GetPickListFontSize(osmPicklist);
        ddlSize.DataSource = picklist;
        ddlSize.DataTextField = Constants.PARAMETER;
        ddlSize.DataValueField = Constants.PARAMETER;
        ddlSize.DataBind();
    }

    #endregion

    #region "Event : btnSave_Click"
    /// <summary>
    /// Used to save font type, colors and sizes.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }

        if (surveyId > 0)
        {
            osm_surveysetting osmSurveySetting = new osm_surveysetting();
            osmSurveySetting.SURVEY_ID = surveyId;
            if (SetFeatures("CUSTOMIZE_FONT") == 1)
            {
                osmSurveySetting.SURVEY_FONT_TYPE = ddlType.SelectedValue;
                osmSurveySetting.SURVEY_FONT_SIZE = ddlSize.SelectedValue;
                osmSurveySetting.SURVEY_FONT_COLOR = ddlColor.SelectedValue;
            }
            else
            {
                osmSurveySetting.SURVEY_FONT_TYPE = "Arial";
                osmSurveySetting.SURVEY_FONT_SIZE = "12";
                osmSurveySetting.SURVEY_FONT_COLOR = "Black";
            }

            var surveySettingsService = ServiceFactory.GetService<SurveySetting>();
            surveySettingsService.SaveOrUpdateSurveySetting(osmSurveySetting, "F");

            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();

            if (chkSetDefault.Checked)
            {
                if (userInfo.UserId > 0)
                {
                    osm_accountsetting osmAccountSetting = new osm_accountsetting();
                    osmAccountSetting.USERID = userInfo.UserId;

                    if (SetFeatures("CUSTOMIZE_FONT") == 1)
                    {
                        osmAccountSetting.SURVEY_FONTTYPE = ddlType.SelectedValue;
                        osmAccountSetting.SURVEY_FONTSIZE = ddlSize.SelectedValue;
                        osmAccountSetting.SURVEY_FONTCOLOR = ddlColor.SelectedValue;
                    }
                    else
                    {
                        osmAccountSetting.SURVEY_FONTTYPE = "Arial";
                        osmAccountSetting.SURVEY_FONTSIZE = "12";
                        osmAccountSetting.SURVEY_FONTCOLOR = "Black";
                    }

                    var accountSettingService = ServiceFactory.GetService<AccountSettingService>();
                    accountSettingService.SaveOrUpdateAccountSetting(osmAccountSetting, "F");
                }
            }

            BindExistsData();

            if (SetFeatures("CUSTOMIZE_FONT") == 1)
            {
                lblSuccMsg.Text = Utilities.ResourceMessage("lblSuccMsgResource1.Text");
                dvSuccessMsg.Visible = true;
            }
        }
    }
    #endregion

    protected void ChangeFontAttributes(object sender, EventArgs e)
    {
        lblPreview.Style.Add("font-family", ddlType.SelectedValue);
        lblPreview.Style.Add("font-size", ddlSize.SelectedValue+"pt");
        lblPreview.Style.Add("color", ddlColor.SelectedValue);       
    }


    #region "Method : SetFeatures"
    /// <summary>
    /// This method returns bool value, if the feature flag is one.
    /// </summary>
    /// <param name="strFeature"></param>
    /// <returns></returns>

    public int SetFeatures(string strFeature)
    {
        int iVal = 1;
        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var sessdet = session.GetLoginUserDetailsSession();
        if (sessdet != null)
        {

            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));

            if (sessdet.UserId > 0 && listFeatures != null)
            {
                foreach (var listFeat in listFeatures)
                {

                    if (listFeat.Feature.Contains(strFeature))
                    {
                        iVal = (int)listFeat.Value;
                    }
                }
            }
        }

        else
        {
            Response.Redirect(PathHelper.GetUserLoginPageURL());
        }
        return iVal;
    }

    #endregion

}