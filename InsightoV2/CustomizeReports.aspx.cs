﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using App_Code;
using FeatureTypes;

public partial class CustomizeReports : SurveyPageBase
{
    int QuestionID = 0, Preveiewtyp = 0;
    int SMode = 0;
    int surveyID = 0;
    DateTime dt1 = Convert.ToDateTime("1/1/0001 12:00:00 AM");
    DateTime dt2 = Convert.ToDateTime("1/1/0001 12:00:00 AM"); 
    Hashtable ht = new Hashtable();
    SurveyCore cor = new SurveyCore();
    public string Key
    {
        get
        {
            return HttpUtility.UrlEncode(QueryStringHelper.GetString("Key"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = false;

        if (!IsPostBack)
        {
            SetPremiumPermissions();
        }
        if (Request.QueryString["key"] != null)
        {
            string key = Request.Params["key"];
            key = key.Replace(' ', '+');
            ht = EncryptHelper.DecryptQuerystringParam(key);

            if (ht != null && ht.Count > 0 && ht.Contains("QuesId"))
            {
                QuestionID = Convert.ToInt32(ht["QuesId"]);
            }
        }
        if (ht != null && ht.Count > 0 && ht.Contains("Prev"))
        {
            Preveiewtyp = Convert.ToInt32(ht["Prev"]);
        }
        int savecustid = 0;
        if (ht != null && ht.Count > 0 && ht.Contains("Saveid"))
        {
            savecustid = Convert.ToInt32(ht["Saveid"]);

        }
        if (!IsPostBack)
        {

            if (Session["PickListManager"] == null)
            {
                PickListManager plmagrtemp = new PickListManager();
                Session["PickListManager"] = plmagrtemp;
            }
            if (Session["PickListManager"] != null && cmbchrttype.SelectedIndex == -1)
            {
                PickListManager plmgr = (PickListManager)Session["PickListManager"];
                plmgr.AddControl("CHART_TYPE", cmbchrttype, "CHART_TYPE");
                plmgr.LoadCombo("CHART_TYPE");
                plmgr.AddControl("CHART_APPEARANCE", cmbchartappearance, "CHART_APPEARANCE");
                plmgr.LoadCombo("CHART_APPEARANCE");
                plmgr.AddControl("CHART_PALETTE", cmbpalette, "CHART_PALETTE");
                plmgr.LoadCombo("CHART_PALETTE");
                plmgr.AddControl("CHART_ALIGNMENT", cmbalignment, "CHART_ALIGNMENT");
                plmgr.LoadCombo("CHART_ALIGNMENT");
                plmgr.AddControl("CHART_DOCK", cmbDock, "CHART_DOCK");
                plmgr.LoadCombo("CHART_DOCK");

            }
            BindCharts();
        }
        else
        {
            bool test = true;
            if (SelectedSurvey != null && SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
            {
                foreach (SurveyQuestion Ques in SelectedSurvey.surveyQues)
                {
                    if ((Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12) && QuestionID == 0 && test)
                    {
                        QuestionID = Ques.QUESTION_ID;
                        test = false;
                        break;
                    }
                }
            }
        }


        if (IsPostBack)
        {
            if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
            {
                surveyID = Convert.ToInt32(ht[Constants.SURVEYID]);
                if (surveyID > 0)
                    SelectedSurvey = cor.GetSurveyWithResponsesCount(surveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));

            }

            if (SelectedSurvey != null && SelectedSurvey.SURVEY_ID > 0)
            {

                SetQuestionNos(SelectedSurvey);
                SurveyQuestion Surques = (SurveyQuestion)Session["TempChart_det"];
                GetChartControls();

            }

            if (Session["Save"] != null)
            {
                if (Session["Save"].ToString() == "1")
                {
                    dvSuccessMsg.Visible = true;
                    lblSuccMsg.Text = "Customization saved successfully.";
                    Session["Save"] = "0";
                }
            }
        }

        try
        {
            SurveyQuestion ques = Session["TempChart_det"] as SurveyQuestion;
            if (ques.QUESTION_TYPE != null && ques.QUESTION_TYPE == 12)
                sidebysidetext.Visible = true;
            else
                sidebysidetext.Visible = false;
            webchrtcntrl = BuildQuetions.GetChart(ques, SMode,dt1,dt2);
            webchrtcntrl.Height = (Unit)600;
            webchrtcntrl.Width = (Unit)500;
            if (ASPxRoundPanelwebchart.Controls.Count > 0)
                ASPxRoundPanelwebchart.Controls.Clear();
            ASPxRoundPanelwebchart.Controls.Add(webchrtcntrl);
            if (ques != null && webchrtcntrl != null)
                ChartExtraFeatures.SetchartDiagramProp(webchrtcntrl, ques);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        CheckUserFeatureLinks();
    }

    /// <summary>
    /// Sets the premium permissions.
    /// </summary>
    private void SetPremiumPermissions()
    {
        UserType userType = Utilities.GetUserType();
        bool showIndivisualResponse = Utilities.ShowOrHideFeature(userType, (int)RealTimeReports.Individual_responses);
        bool showCustomCharts = Utilities.ShowOrHideFeature(userType, (int)RealTimeReports.Customization_of_Charts);

        IndividualRes.Visible = showIndivisualResponse;
        CrossTabRpts.Visible = showCustomCharts;

    }

    private void BindCharts()
    {

        if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
        {
            surveyID = Convert.ToInt32(ht[Constants.SURVEYID]);
            if (surveyID > 0)
                SelectedSurvey = cor.GetSurveyWithResponsesCount(surveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));

        }

        if (SelectedSurvey != null && SelectedSurvey.SURVEY_ID > 0)
        {
            SetQuestionNos(SelectedSurvey);
        }
        if (SelectedSurvey != null && SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0 && QuestionID > 0)
        {
            foreach (SurveyQuestion ques in SelectedSurvey.surveyQues)
            {
                if (ques.QUESTION_ID == QuestionID)
                {
                    if (Session["TempChart_det"] == null)
                    {
                        string ChartTyp = "";
                        if (Session["Temp_ChartType"] != null)
                        {
                            ChartTyp = (string)Session["Temp_ChartType"];
                            Session["Temp_ChartType"] = null;
                        }
                        else
                            ChartTyp = ques.CHART_TYPE;
                        SetChartClss(ques, ChartTyp);
                    }
                    else
                    {
                        SurveyQuestion Surques = (SurveyQuestion)Session["TempChart_det"];

                        if (Surques.QUESTION_ID != QuestionID || Surques.QUESTION_ID == QuestionID)
                        {
                            if (Surques.QUESTION_ID != QuestionID)
                            {
                                string ChrtTyp = ques.CHART_TYPE;

                                SetChartClss(ques, ChrtTyp);
                            }
                            else
                            {
                                fillchartcontrols(Surques);
                            }
                        }
                    }
                }
            }
        }
    }

    WebChartControl webchrtcntrl = new WebChartControl();

    public void SetSubNaviLinks()
    {
        string strCrossTab = EncryptHelper.EncryptQuerystring("CrossTabReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
        CrossTabRpts.HRef = strCrossTab;
    }

    #region "Method : SetFeatures"
    /// <summary>
    /// This method returns bool value, if the feature flag is one.
    /// </summary>
    /// <param name="strFeature"></param>
    /// <returns></returns>

    public int SetFeatures(string strFeature)
    {
        int iVal = 0;
        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var sessdet = session.GetLoginUserDetailsSession();
        if (sessdet != null)
        {

            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));

            if (sessdet.UserId > 0 && listFeatures != null)
            {
                foreach (var listFeat in listFeatures)
                {

                    if (listFeat.Feature.Contains(strFeature))
                    {
                        iVal = (int)listFeat.Value;
                    }
                }
            }
        }

        else
        {
            Response.Redirect(PathHelper.GetUserLoginPageURL());
        }
        return iVal;
    }

    #endregion

    public void CheckUserFeatureLinks()
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();

        if (userDetails != null && userDetails.UserId > 0)
        {

            if (SetFeatures("CUSTOMCHART") == 0)
            {
                cust_reports.HRef = "#";
                cust_reports.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
            else
            {
                string strCustomTab = EncryptHelper.EncryptQuerystring("CustomizeReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
                cust_reports.HRef = strCustomTab;
            }

            if (SetFeatures("CROSS_TAB") == 0)
            {
                CrossTabRpts.HRef = "#";
                CrossTabRpts.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
            else
            {
                string strCrossTab = EncryptHelper.EncryptQuerystring("CrossTabReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
                CrossTabRpts.HRef = strCrossTab;
            }

            if (SetFeatures("INDIVIDUAL_RESPONSES") == 0)
            {
                IndividualRes.HRef = "#";
                IndividualRes.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
            else
            {
                string strIndividual = EncryptHelper.EncryptQuerystring("IndividualResponse.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
                IndividualRes.HRef = strIndividual;
            }

        }
    }
    private void fillchartcontrols(SurveyQuestion ques)
    {
        ValidationCore Vcor = new ValidationCore();
        cmbalignment.Value = (ques.CHART_ALIGNMENT.Trim().Length > 0) ? ques.CHART_ALIGNMENT : "Center";
        cmbchartappearance.Value = (ques.CHART_APPEARANCE.Trim().Length > 0) ? ques.CHART_APPEARANCE : "Pastel Kit";
        cmbDock.Value = (ques.CHART_DOCK.Trim().Length > 0) ? ques.CHART_DOCK : "Top";
        cmbpalette.Value = (ques.CHART_PALETTE.Trim().Length > 0) ? ques.CHART_PALETTE : "Nature Colors";
        memcharttitle.Text = (ques.CHART_TITLE.Trim().Length > 0) ? ques.CHART_TITLE : Utilities.ClearHTML(ques.QUESTION_LABEL);
        if (ques.CHART_TYPE.Trim().Length > 0)
        {
            cmbchrttype.Value = ques.CHART_TYPE;
        }
        else
        {
            if (ques.QUESTION_TYPE == 1)
            {
                cmbchrttype.Value = "Bar";
            }
            else if (ques.QUESTION_TYPE == 2)
            {
                cmbchrttype.Value = "Bar3D";
            }
            else if (ques.QUESTION_TYPE == 3)
            {
                cmbchrttype.Value = "Doughnut";
            }
            else if (ques.QUESTION_TYPE == 4)
            {
                cmbchrttype.Value = "Doughnut3D";
            }
            else if (ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11 || ques.QUESTION_TYPE == 12)
            {
                cmbchrttype.Value = "StackedBar";
            }
            else if (ques.QUESTION_TYPE == 15)
            {
                cmbchrttype.Value = "StackedArea";
            }
            else
            {
                cmbchrttype.Value = "Pie";
            }

        }

        memoaddcomments.Text = ques.COMMENTS;
        FillLabelPositionCombo();
        chkShowLabel.Checked = Convert.ToBoolean(ques.SHOW_LABEL);
        cboLabelAngle.Value = ques.LABEL_ANGLE;
        chkValueAsPercent.Checked = Convert.ToBoolean(ques.VALUEAS_PERCENT);
        cboZoomPercent.Value = ques.ZOOMPERCENT;
        cboTransparency.Value = ques.TRANSPARENCY;
        cboPerspectiveAngle.Value = ques.PERSPECTIVE_ANGLE;
        cboMarkerSize.Value = ques.MARKER_SIZE;
        chkShowMarker.Checked = Convert.ToBoolean(ques.SHOW_MARKER);
        chkInverted.Checked = Convert.ToBoolean(ques.INVERTED);
        cboLabelPostioning.Value = ques.LABEL_POSITION;
        if (cmbchrttype.Value.ToString() != "Bar")
        {
            cboLabelPostioning.Value = (ques.LABEL_POSITION.Trim().Length > 0) ? ques.LABEL_POSITION : "Inside";
        }
        else if (cmbchrttype.Value.ToString() == "Bar")
        {
            cboLabelPostioning.Value = (ques.LABEL_POSITION.Trim().Length > 0) ? ques.LABEL_POSITION : "Top";
        }


        cboMarkerKind.Value = (ques.MARKER_KIND.Trim().Length > 0) ? ques.MARKER_KIND : "Circle";

        cboHoleRadius.Value = ques.HOLE_RADIUS;
        cboDiagramType.Value = (ques.DIAGRAM_TYPE.Trim().Length > 0) ? ques.DIAGRAM_TYPE : "Circle";

        cboTextDirection.Value = (ques.TEXT_DIRECTION.Trim().Length > 0) ? ques.TEXT_DIRECTION : "LeftToRight";

        cboFunctionType.Value = ques.FUNCTION_TYPE;
        cboExplodedPoint.Value = (ques.EXPLODED_POINT.Trim().Length > 0) ? ques.EXPLODED_POINT : "None";

        cboLHorizontalAlign.Value = (ques.LEGEND_HORIZONTALALIGN.Trim().Length > 0) ? ques.LEGEND_HORIZONTALALIGN : "Left";
        cboLVerticalAlign.Value = (ques.LEGEND_VERTICALALIGN.Trim().Length > 0) ? ques.LEGEND_VERTICALALIGN : "TopOutside";
        cboLDirection.Value = (ques.LEGEND_DIRECTION.Trim().Length > 0) ? ques.LEGEND_DIRECTION : "LeftToRight";


        cboLMaxHorizontalPercentage.Value = (ques.LEGEND_MAX_HORIZONTALALIGN > 0) ? ques.LEGEND_MAX_HORIZONTALALIGN : 100;
        cboLMaxVerticalPercentage.Value = (ques.LEGEND_MAX_VERTICALALIGN > 0) ? ques.LEGEND_MAX_VERTICALALIGN : 50;

        chkLEquallySpacedItems.Checked = Convert.ToBoolean(ques.LEGEND_EQUALLYSPACED_ITEMS);
        chkHideXaxis.Checked = Convert.ToBoolean(ques.HIDE_XAXIS);
        chkHideLegands.Checked = Convert.ToBoolean(ques.HIDE_LEGAND);
        chkStaggered.Checked = Convert.ToBoolean(ques.STAGGERED);


        if (cboLDirection.Text.Trim().Length > 0)
        {
            bool chek = false;
            if (cboLDirection.Text == "LeftToRight" || cboLDirection.Text == "RightToLeft")
                chek = true;

            chkLEquallySpacedItems.Enabled = chek;

        }
        GetChartControls();
    }

    private void FillLabelPositionCombo()
    {
        if (cmbchrttype.Value.ToString() == "Bar")
        {
            cboLabelPostioning.Items.Clear();
            cboLabelPostioning.Items.Add("Top", "Top");
            cboLabelPostioning.Items.Add("Center", "Center");
        }
        else if (cmbchrttype.Value.ToString() == "Pie" || cmbchrttype.Value.ToString() == "Pie3D" || cmbchrttype.Value.ToString() == "Doughnut" || cmbchrttype.Value.ToString() == "Doughnut3D")
        {
            cboLabelPostioning.Items.Clear();
            cboLabelPostioning.Items.Add("Inside", "Inside");
            cboLabelPostioning.Items.Add("Outside", "Outside");
            cboLabelPostioning.Items.Add("TwoColumns", "TwoColumns");
            cboLabelPostioning.Items.Add("Radial", "Radial");
        }
    }

    public void SetQuestionNos(Insighto.Business.Charts.Survey sur)
    {
        int i = 0, cl = 0;
        int qnsCount = 1;
        List<SurveyQuestion> QuesList = sur.surveyQues;
        if (QuesList != null)
        {
            ValidationCore Vcore = new ValidationCore();
            QuesList.Sort();
            bool test = true;
            ASPxHyperLink[] quesLink = new ASPxHyperLink[QuesList.Count];
            foreach (SurveyQuestion Ques in QuesList)
            {
                quesLink[i] = new ASPxHyperLink();
                quesLink[i].EncodeHtml = false;
                Label lbl = new Label();
                lbl.Text = "&nbsp;";
                if (Ques.QUESTION_TYPE != 16 && Ques.QUESTION_TYPE != 17 && Ques.QUESTION_TYPE != 18 && Ques.QUESTION_TYPE != 19 && Ques.QUESTION_TYPE != 21)
                { 
                    if (i >= 9)
                    {
                        quesLink[i].Text = Convert.ToString(i + 1);
                    }
                    else
                    {
                        quesLink[i].Text = "0" + Convert.ToString(i + 1);
                    }
                    if (Ques.QUESTION_TYPE == 1 || Ques.QUESTION_TYPE == 2 || Ques.QUESTION_TYPE == 3 || Ques.QUESTION_TYPE == 4 || Ques.QUESTION_TYPE == 10 || Ques.QUESTION_TYPE == 11 || Ques.QUESTION_TYPE == 12 || Ques.QUESTION_TYPE == 13 || Ques.QUESTION_TYPE == 15)
                    {
                        quesLink[i].Enabled = true;
                        quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL);
                        if (cl == 0)
                        {
                            string urlstr1 = EncryptHelper.EncryptQuerystring("Customizereports.aspx", Constants.SURVEYID + "=" + Ques.SURVEY_ID + "&QuesId=" + Ques.QUESTION_ID + "&Saveid=3&surveyFlag=" + SurveyFlag);
                            cust_reports.HRef = urlstr1;
                        }
                        cl += 1;
                    }
                    else if (Ques.QUESTION_TYPE == 5 || Ques.QUESTION_TYPE == 6 || Ques.QUESTION_TYPE == 7 || Ques.QUESTION_TYPE == 8 || Ques.QUESTION_TYPE == 9 || Ques.QUESTION_TYPE == 14 || Ques.QUESTION_TYPE == 20)
                    {
                        quesLink[i].Enabled = false;
                        quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL) + "-  No Customization for this Question Type";
                    }
                    string urlstr = EncryptHelper.EncryptQuerystring("Customizereports.aspx", Constants.SURVEYID + "=" + Ques.SURVEY_ID + "&QuesId=" + Ques.QUESTION_ID + "&Saveid=3&surveyFlag=" + SurveyFlag);
                                       quesLink[i].NavigateUrl = urlstr;
                    quesLink[i].Font.Bold = true;

                    quesLink[i].Font.Underline = true;
                    ASPxRoundPanelquestions.Controls.Add(quesLink[i]);
                   // ASPxRoundPanelquestions.Controls.Add(lbl);
                    if (qnsCount % 30 != 0)
                    {
                        ASPxRoundPanelquestions.Controls.Add(lbl);
                    }
                    else
                    {
                        lbl.Text = "<br/>";
                        ASPxRoundPanelquestions.Controls.Add(lbl);
                    }
                    qnsCount++;
                    if (Ques.QUESTION_TYPE != 16 && Ques.QUESTION_TYPE != 17 && Ques.QUESTION_TYPE != 18 && Ques.QUESTION_TYPE != 19 && Ques.QUESTION_TYPE != 21)
                        i++;
                    if (QuestionID == 0 && test)
                    {
                        QuestionID = Ques.QUESTION_ID;
                        test = false;
                    }
                }
                else if(Ques.QUESTION_TYPE == 17)
                {
                    quesLink[i].Text = "P";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL) + "- No Analysis for this Question Type";
                    quesLink[i].Font.Bold = true;
                    quesLink[i].Font.Underline = true;
                    ASPxRoundPanelquestions.Controls.Add(quesLink[i]);
                    ASPxRoundPanelquestions.Controls.Add(lbl);
                }
                else if (Ques.QUESTION_TYPE == 18)
                {
                    quesLink[i].Text = "Q";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL) + "- No Analysis for this Question Type";
                    quesLink[i].Font.Bold = true;
                    quesLink[i].Font.Underline = true;
                    ASPxRoundPanelquestions.Controls.Add(quesLink[i]);
                    ASPxRoundPanelquestions.Controls.Add(lbl);
                }
                else if (Ques.QUESTION_TYPE == 19)
                {
                    quesLink[i].Text = "I";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = "Image" + "- No Analysis for this Question Type";
                    quesLink[i].Font.Bold = true;
                    quesLink[i].Font.Underline = true;
                    ASPxRoundPanelquestions.Controls.Add(quesLink[i]);
                    ASPxRoundPanelquestions.Controls.Add(lbl);
                }
                else if (Ques.QUESTION_TYPE == 21)
                {
                    quesLink[i].Text = "V";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = "Video" + "- No Analysis for this Question Type";
                    quesLink[i].Font.Bold = true;
                    quesLink[i].Font.Underline = true;
                    ASPxRoundPanelquestions.Controls.Add(quesLink[i]);
                    ASPxRoundPanelquestions.Controls.Add(lbl);
                }
                else if (Ques.QUESTION_TYPE == 16)
                {
                    quesLink[i].Text = "N";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL) + "- No Analysis for this Question Type";
                    quesLink[i].Font.Bold = true;
                    quesLink[i].Font.Underline = true;
                    ASPxRoundPanelquestions.Controls.Add(quesLink[i]);
                    ASPxRoundPanelquestions.Controls.Add(lbl);
                } 

               
            }
        }
    }

    int labelAngle = 0, ValasPercent = 0, ZoomPercent = 100, Transparency = 45, PesrpAngle = 0, markerSiz = 0, showMarker = 0, invert = 0;
    string lblPos = "", MarkerKid = "", holeRad = "", diagramTyp = "", txtDirection = "", funTyp = "", ExplodedPnt = "";

    public void GetChartOptions(string chrtType)
    {
        labelAngle = 0; ValasPercent = 0; ZoomPercent = 100; Transparency = 45; PesrpAngle = 0; markerSiz = 0; showMarker = 0; invert = 0;
        lblPos = ""; MarkerKid = ""; holeRad = ""; diagramTyp = ""; txtDirection = ""; funTyp = ""; ExplodedPnt = "";
        if (chrtType == ViewType.Bar.ToString())
        {
            lblPos = cboLabelPostioning.Text;
        }
        else if (chrtType == ViewType.FullStackedBar.ToString() || chrtType == ViewType.FullStackedArea.ToString() || chrtType == ViewType.FullStackedSplineArea.ToString())
        {
            ValasPercent = Convert.ToInt32(chkValueAsPercent.Checked);
        }
        else if (chrtType == ViewType.Bar3D.ToString() || chrtType == ViewType.StackedBar3D.ToString() || chrtType == ViewType.FullStackedBar3D.ToString() || chrtType == ViewType.Area3D.ToString() || chrtType == ViewType.StackedArea3D.ToString() || chrtType == ViewType.FullStackedArea3D.ToString() || chrtType == ViewType.SplineArea3D.ToString() || chrtType == ViewType.StackedSplineArea3D.ToString() || chrtType == ViewType.FullStackedSplineArea3D.ToString())
        {
            ZoomPercent = Convert.ToInt32(cboZoomPercent.Text.Trim());
            Transparency = Convert.ToInt32(cboTransparency.Text.Trim());
            PesrpAngle = Convert.ToInt32(cboPerspectiveAngle.Text.Trim());
            if (chrtType == ViewType.FullStackedBar3D.ToString())
                ValasPercent = Convert.ToInt32(chkValueAsPercent.Checked);
        }
        else if (chrtType == ViewType.Line.ToString() || chrtType == ViewType.Point.ToString() || chrtType == ViewType.StepLine.ToString())
        {
            markerSiz = Convert.ToInt32(cboMarkerSize.Text.Trim());
            MarkerKid = cboMarkerKind.Text.Trim();
            if (chrtType == ViewType.Point.ToString())
                labelAngle = Convert.ToInt32(cboLabelAngle.Text.Trim());
            else if (chrtType == ViewType.StepLine.ToString())
                invert = Convert.ToInt32(chkInverted.Checked);

        }
        else if (chrtType == ViewType.Line3D.ToString() || chrtType == ViewType.StepLine3D.ToString() || chrtType == ViewType.Spline3D.ToString())
        {
            ZoomPercent = Convert.ToInt32(cboZoomPercent.Text.Trim());
            PesrpAngle = Convert.ToInt32(cboPerspectiveAngle.Text.Trim());
            if (chrtType == ViewType.StepLine3D.ToString())
                invert = Convert.ToInt32(chkInverted.Checked);
        }
        else if (chrtType == ViewType.Area.ToString() || chrtType == ViewType.SplineArea.ToString())
        {
            labelAngle = Convert.ToInt32(cboLabelAngle.Text.Trim());
            Transparency = Convert.ToInt32(cboTransparency.Text.Trim());
            markerSiz = Convert.ToInt32(cboMarkerSize.Text.Trim());
            MarkerKid = cboMarkerKind.Text.Trim();
        }
        else if (chrtType == ViewType.Pie.ToString() || chrtType == ViewType.Pie3D.ToString() || chrtType == ViewType.Doughnut.ToString() || chrtType == ViewType.Doughnut3D.ToString())
        {
            ValasPercent = Convert.ToInt32(chkValueAsPercent.Checked);
            ExplodedPnt = cboExplodedPoint.Text.Trim();
            lblPos = cboLabelPostioning.Text.Trim();
            if (chrtType == ViewType.Doughnut.ToString() || chrtType == ViewType.Doughnut3D.ToString())
                holeRad = cboHoleRadius.Text.Trim();
        }
        else if (chrtType == ViewType.RadarArea.ToString() || chrtType == ViewType.RadarLine.ToString() || chrtType == ViewType.RadarPoint.ToString() || chrtType == ViewType.PolarArea.ToString() || chrtType == ViewType.PolarLine.ToString() || chrtType == ViewType.PolarPoint.ToString())
        {
            MarkerKid = cboMarkerKind.Text.Trim();
            markerSiz = Convert.ToInt32(cboMarkerSize.Text.Trim());
            diagramTyp = cboDiagramType.Text.Trim();
            if (chrtType == ViewType.RadarArea.ToString() || chrtType == ViewType.RadarLine.ToString() || chrtType == ViewType.PolarArea.ToString() || chrtType == ViewType.PolarLine.ToString())
            {
                showMarker = Convert.ToInt32(chkShowMarker.Checked);
            }

            if (chrtType == ViewType.RadarArea.ToString() || chrtType == ViewType.RadarLine.ToString() || chrtType == ViewType.RadarPoint.ToString())
            {
                txtDirection = cboTextDirection.Text.Trim();
            }
            else
            {
                funTyp = cboFunctionType.Text.Trim();
            }
        }
    }

    protected void cmdsave_Click(object sender, EventArgs e)
    {
        SurveyCore cor = new SurveyCore();
        GetChartOptions(cmbchrttype.Text.Trim());

        Insighto.Business.Charts.Survey sury = cor.UpdateQuestionChart(SurveyBasicInfoView.SURVEY_ID, QuestionID, memcharttitle.Text.Trim(), memoaddcomments.Text.Trim(), cmbchrttype.Text.Trim(), cmbchartappearance.Text.Trim(), cmbpalette.Text.Trim(), cmbalignment.Text.Trim(), cmbDock.Text.Trim(), Convert.ToInt32(chkShowLabel.Checked), labelAngle, ValasPercent, ZoomPercent, Transparency, PesrpAngle, markerSiz, showMarker, invert, lblPos, MarkerKid, holeRad, diagramTyp, txtDirection, funTyp, ExplodedPnt, cboLHorizontalAlign.Text.Trim(), cboLVerticalAlign.Text.Trim(), Convert.ToInt32(cboLMaxHorizontalPercentage.Value), Convert.ToInt32(cboLMaxVerticalPercentage.Value), cboLDirection.Text.Trim(), Convert.ToInt32(chkLEquallySpacedItems.Checked), Convert.ToInt32(chkHideLegands.Checked), Convert.ToInt32(chkHideXaxis.Checked), Convert.ToInt32(chkStaggered.Checked));
        Insighto.Business.Charts.Survey survey = new Insighto.Business.Charts.Survey();
        if (sury.surveyQues != null && sury.surveyQues.Count > 0)
        {
            SelectedSurvey = sury;
        }
        Session["Save"] = "1";

    }
    //protected void cmdApplySettings_Click(object sender, EventArgs e)
    //{
    //    SurveyQuestion quest = new SurveyQuestion();
    //    if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
    //    {
    //        foreach (SurveyQuestion ques in SelectedSurvey.surveyQues)
    //        {
    //            if (QuestionID == ques.QUESTION_ID)
    //            {
    //                quest.surveyAnswers = ques.surveyAnswers;
    //                quest.QUESTION_TYPE = ques.QUESTION_TYPE;
    //                quest.QUESTION_LABEL = ques.QUESTION_LABEL;
    //                quest.QUESTION_ID = ques.QUESTION_ID;
    //                quest.OTHER_ANS = ques.OTHER_ANS;
    //                quest.OTHER_RESPCOUNT = ques.OTHER_RESPCOUNT;
    //                break;
    //            }
    //        }
    //        GetChartOptions(cmbchrttype.Text.Trim());
    //        quest.CHART_ALIGNMENT = cmbalignment.Text.Trim();
    //        quest.CHART_APPEARANCE = cmbchartappearance.Text.Trim();
    //        quest.CHART_DOCK = cmbDock.Text.Trim();
    //        quest.CHART_PALETTE = cmbpalette.Text.Trim();
    //        quest.CHART_TITLE = memcharttitle.Text.Trim();
    //        quest.CHART_TYPE = cmbchrttype.Text.Trim();
    //        quest.COMMENTS = memoaddcomments.Text.Trim();
    //        quest.SHOW_LABEL = Convert.ToInt32(chkShowLabel.Checked);
    //        quest.LABEL_ANGLE = labelAngle;
    //        quest.VALUEAS_PERCENT = ValasPercent;
    //        quest.ZOOMPERCENT = ZoomPercent;
    //        quest.TRANSPARENCY = Transparency;
    //        quest.PERSPECTIVE_ANGLE = PesrpAngle;
    //        quest.MARKER_SIZE = markerSiz;
    //        quest.SHOW_MARKER = showMarker;
    //        quest.INVERTED = invert;
    //        quest.LABEL_POSITION = lblPos;
    //        quest.MARKER_KIND = MarkerKid;
    //        quest.HOLE_RADIUS = holeRad;
    //        quest.DIAGRAM_TYPE = diagramTyp;
    //        quest.TEXT_DIRECTION = txtDirection;
    //        quest.FUNCTION_TYPE = funTyp;
    //        quest.EXPLODED_POINT = ExplodedPnt;
    //        quest.LEGEND_HORIZONTALALIGN = cboLHorizontalAlign.Text.Trim();
    //        quest.LEGEND_VERTICALALIGN = cboLVerticalAlign.Text.Trim();
    //        quest.LEGEND_MAX_HORIZONTALALIGN = Convert.ToInt32(cboLMaxHorizontalPercentage.Value);
    //        quest.LEGEND_MAX_VERTICALALIGN = Convert.ToInt32(cboLMaxVerticalPercentage.Value);
    //        quest.LEGEND_DIRECTION = cboLDirection.Text.Trim();
    //        quest.LEGEND_EQUALLYSPACED_ITEMS = Convert.ToInt32(chkLEquallySpacedItems.Checked);
    //        quest.HIDE_LEGAND = Convert.ToInt32(chkHideLegands.Checked);
    //        quest.HIDE_XAXIS = Convert.ToInt32(chkHideXaxis.Checked);
    //        quest.STAGGERED = Convert.ToInt32(chkStaggered.Checked);
    //        Session["Temp_Chart"] = quest;
    //        string urlstr = EncryptHelper.EncryptQuerystring("Customizereports.aspx", Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID + "&Prev=1&QuesId=" + QuestionID+"&surveyFlag="+SurveyFlag);
    //        Response.Redirect(urlstr);
    //    }
    //}

    protected void cmdApplySettings_Click(object sender, EventArgs e)
    {
        Insighto.Business.Charts.Survey sury = new Insighto.Business.Charts.Survey();
        Insighto.Business.Charts.Survey survey = new Insighto.Business.Charts.Survey();
        if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
        {
            foreach (SurveyQuestion ques in SelectedSurvey.surveyQues)
            {
                GetChartOptions(cmbchrttype.Text.Trim());
                sury = cor.UpdateQuestionChart(SurveyBasicInfoView.SURVEY_ID, ques.QUESTION_ID, memcharttitle.Text.Trim(), memoaddcomments.Text.Trim(), cmbchrttype.Text.Trim(), cmbchartappearance.Text.Trim(), cmbpalette.Text.Trim(), cmbalignment.Text.Trim(), cmbDock.Text.Trim(), Convert.ToInt32(chkShowLabel.Checked), labelAngle, ValasPercent, ZoomPercent, Transparency, PesrpAngle, markerSiz, showMarker, invert, lblPos, MarkerKid, holeRad, diagramTyp, txtDirection, funTyp, ExplodedPnt, cboLHorizontalAlign.Text.Trim(), cboLVerticalAlign.Text.Trim(), Convert.ToInt32(cboLMaxHorizontalPercentage.Value), Convert.ToInt32(cboLMaxVerticalPercentage.Value), cboLDirection.Text.Trim(), Convert.ToInt32(chkLEquallySpacedItems.Checked), Convert.ToInt32(chkHideLegands.Checked), Convert.ToInt32(chkHideXaxis.Checked), Convert.ToInt32(chkStaggered.Checked));
            }
            SelectedSurvey = sury;
            string urlstr = EncryptHelper.EncryptQuerystring("Customizereports.aspx", Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID + "&Prev=1&QuesId=" + QuestionID + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(urlstr);
        }
    }

    protected void cmbchrttype_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (Request.QueryString["key"] != null)
        {
            string key = Request.Params["key"];
            key = key.Replace(' ', '+');
            ht = EncryptHelper.DecryptQuerystringParam(key);

            if (ht != null && ht.Count > 0 && ht.Contains("QuesId"))
            {
                QuestionID = Convert.ToInt32(ht["QuesId"]);
            }
        }
        if (ht != null && ht.Count > 0 && ht.Contains("Prev"))
        {
            Preveiewtyp = Convert.ToInt32(ht["Prev"]);
        }
        int savecustid = 0;
        if (ht != null && ht.Count > 0 && ht.Contains("Saveid"))
        {
            savecustid = Convert.ToInt32(ht["Saveid"]);

        }

        string chrttyp = "";
        SurveyQuestion ques = Session["TempChart_det"] as SurveyQuestion;
        if (ques != null)
            chrttyp = ques.CHART_TYPE;

        if (cmbchrttype.Value != null)
        {
            Session["Temp_ChartType"] = cmbchrttype.Value.ToString();
            Session["TempChart_det"] = null;
            string urlstr = EncryptHelper.EncryptQuerystring("Customizereports.aspx", Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID + "&QuesId=" + QuestionID.ToString() + "&surveyFlag=" + SurveyFlag);
            Page.Response.Redirect(urlstr, true);
        }
    }
    public void GetChartControls()
    {
        if (cmbchrttype != null && cmbchrttype.SelectedItem != null && cmbchrttype.Text.Trim().Length > 0)
        {
            lblLabelAngle.Visible = false;
            cboLabelAngle.Visible = false;
            trlblangle.Visible = false;

            lblMarkerKind.Visible = false;
            cboMarkerKind.Visible = false;
            trlblmarkerkind.Visible = false;
            lblMarkerSize.Visible = false;

            cboMarkerSize.Visible = false;
            trlblmarkersize.Visible = false;
            lblTransparency.Visible = false;
            trlbltransparancey.Visible = false;
            cboTransparency.Visible = false;
            lblPerspectiveAngle.Visible = false;
            tdperspectiveangle.Visible = false;

            cboPerspectiveAngle.Visible = false;
            lblZoomPercent.Visible = false;

            tdlblzoompercent.Visible = false;
            cboZoomPercent.Visible = false;
            lblLabelPosition.Visible = false;

            lbltrpositioning.Visible = false;
            //
            cboLabelPostioning.Visible = false;
            lblValueAsPercent.Visible = false;

            tdlblvalueaspercent.Visible = false;

            chkValueAsPercent.Visible = false;
            lblHoleRadius.Visible = false;
            tdlblholradius.Visible = false;
            cboHoleRadius.Visible = false;
            lblExplodedPoint.Visible = false;
            tdlblexplodedpoint.Visible = false;
            cboExplodedPoint.Visible = false;
            lblInverted.Visible = false;
            tdlblinverted.Visible = false;
            chkInverted.Visible = false;
            lblDiagramType.Visible = false;

            tdlbldiagramtype.Visible = false;
            cboDiagramType.Visible = false;
            lblTextDirection.Visible = false;
            tdlbltextdirection.Visible = false;
            cboTextDirection.Visible = false;
            lblShowMarker.Visible = false;
            tdlblshowmarker.Visible = false;
            chkShowMarker.Visible = false;
            lblFunctionType.Visible = false;
            tdlblfunctiontype.Visible = false;
            cboFunctionType.Visible = false;
            chkHideXaxis.Visible = false;
            lblHideXaxis.Visible = false;
            trlblhidexis.Visible = false;
            chkStaggered.Visible = false;
            lblStaggered.Visible = false;
            trlblstaggered.Visible = false;

            if (cmbchrttype.Text == "FullStackedBar" || cmbchrttype.Text == "FullStackedBar" || cmbchrttype.Text == "FullStackedBar3D" || cmbchrttype.Text == "FullStackedArea" || cmbchrttype.Text == "FullStackedSplineArea")
            {
                lblValueAsPercent.Visible = true;
                tdlblvalueaspercent.Visible = true;
                chkValueAsPercent.Visible = true;
            }
            else if (cmbchrttype.Text == "Area" || cmbchrttype.Text == "SplineArea")
            {
                lblLabelAngle.Visible = true;
                trlblangle.Visible = true;
                cboLabelAngle.Visible = true;
                lblMarkerKind.Visible = true;
                trlblmarkerkind.Visible = true;
                cboMarkerKind.Visible = true;
                lblMarkerSize.Visible = true;
                cboMarkerSize.Visible = true;
                trlblmarkersize.Visible = true;
                lblTransparency.Visible = true;
                trlbltransparancey.Visible = true;
                cboTransparency.Visible = true;
            }
            else if (cmbchrttype.Text == "Area3D" || cmbchrttype.Text == "Bar3D" || cmbchrttype.Text == "Line3D" || cmbchrttype.Text == "StepLine3D" || cmbchrttype.Text == "Spline3D" || cmbchrttype.Text == "SplineArea3D" || cmbchrttype.Text == "StackedArea3D" || cmbchrttype.Text == "StackedSplineArea3D" || cmbchrttype.Text == "StackedBar3D" || cmbchrttype.Text == "FullStackedBar3D" || cmbchrttype.Text == "FullStackedArea3D" || cmbchrttype.Text == "FullStackedSplineArea3D")
            {
                lblPerspectiveAngle.Visible = true;
                tdperspectiveangle.Visible = true;
                cboPerspectiveAngle.Visible = true;
                lblZoomPercent.Visible = true;
                tdlblzoompercent.Visible = true;
                cboZoomPercent.Visible = true;
                if (cmbchrttype.Text == "Area3D" || cmbchrttype.Text == "Bar3D" || cmbchrttype.Text == "SplineArea3D" || cmbchrttype.Text == "StackedArea3D" || cmbchrttype.Text == "StackedSplineArea3D" || cmbchrttype.Text == "StackedBar3D" || cmbchrttype.Text == "FullStackedBar3D" || cmbchrttype.Text == "FullStackedArea3D" || cmbchrttype.Text == "FullStackedSplineArea3D")
                {
                    lblTransparency.Visible = true;
                    trlbltransparancey.Visible = true;
                    cboTransparency.Visible = true;
                    if (cmbchrttype.Text == "FullStackedBar3D")
                    {
                        lblValueAsPercent.Visible = true;
                        tdlblvalueaspercent.Visible = true;
                        chkValueAsPercent.Visible = true;
                    }
                }
                else if (cmbchrttype.Text == "StepLine3D")
                {
                    lblInverted.Visible = true;
                    tdlblinverted.Visible = true;
                    chkInverted.Visible = true;
                }
            }

            else if (cmbchrttype.Text == "Pie" || cmbchrttype.Text == "Pie3D" || cmbchrttype.Text == "Doughnut" || cmbchrttype.Text == "Doughnut3D")
            {
                lblValueAsPercent.Visible = true;
                tdlblvalueaspercent.Visible = true;
                chkValueAsPercent.Visible = true;
                lblLabelPosition.Visible = true;
                lbltrpositioning.Visible = true;
                cboLabelPostioning.Visible = true;
                lblExplodedPoint.Visible = true;
                tdlblexplodedpoint.Visible = true;
                cboExplodedPoint.Visible = true;
                if (cmbchrttype.Text == "Doughnut" || cmbchrttype.Text == "Doughnut3D")
                {
                    lblHoleRadius.Visible = true;
                    tdlblholradius.Visible = true;
                    cboHoleRadius.Visible = true;
                }
            }
            else if (cmbchrttype.Text == "Line" || cmbchrttype.Text == "Point" || cmbchrttype.Text == "StepLine")//|| cmbchrttype.Text == "Spline"
            {
                lblMarkerKind.Visible = true;
                trlblmarkerkind.Visible = true;
                cboMarkerKind.Visible = true;
                lblMarkerSize.Visible = true;
                trlblmarkersize.Visible = true;

                cboMarkerSize.Visible = true;
                if (cmbchrttype.Text == "Point")
                {
                    lblLabelAngle.Visible = true;
                    trlblangle.Visible = true;
                    cboLabelAngle.Visible = true;
                }
                else if (cmbchrttype.Text == "StepLine")
                {
                    lblLabelAngle.Visible = true;
                    trlblangle.Visible = true;
                    cboLabelAngle.Visible = true;
                    lblInverted.Visible = true;
                    tdlblinverted.Visible = true;
                    chkInverted.Visible = true;
                }
            }

            else if (cmbchrttype.Text == "RadarArea" || cmbchrttype.Text == "RadarLine" || cmbchrttype.Text == "RadarPoint" || cmbchrttype.Text == "PolarArea" || cmbchrttype.Text == "PolarLine" || cmbchrttype.Text == "PolarPoint")
            {
                lblMarkerKind.Visible = true;
                trlblmarkerkind.Visible = true;
                cboMarkerKind.Visible = true;
                lblMarkerSize.Visible = true;
                trlblmarkersize.Visible = true;
                cboMarkerSize.Visible = true;
                lblDiagramType.Visible = true;
                tdlbldiagramtype.Visible = true;
                cboDiagramType.Visible = true;
                if (cmbchrttype.Text == "RadarArea" || cmbchrttype.Text == "RadarLine" || cmbchrttype.Text == "RadarPoint")
                {
                    lblTextDirection.Visible = true;
                    tdlbltextdirection.Visible = true;
                    cboTextDirection.Visible = true;
                }
                else if (cmbchrttype.Text == "PolarArea" || cmbchrttype.Text == "PolarLine" || cmbchrttype.Text == "PolarPoint")
                {
                    lblFunctionType.Visible = true;
                    tdlblfunctiontype.Visible = true;
                    cboFunctionType.Visible = true;
                }

                if (cmbchrttype.Text == "RadarLine" || cmbchrttype.Text == "RadarArea" || cmbchrttype.Text == "PolarArea" || cmbchrttype.Text == "PolarLine")
                {
                    lblShowMarker.Visible = true;
                    tdlblshowmarker.Visible = true;
                    chkShowMarker.Visible = true;
                }
            }

            if (cmbchrttype.Text == "Area" || cmbchrttype.Text == "Bar" || cmbchrttype.Text == "Line" || cmbchrttype.Text == "Point" || cmbchrttype.Text == "Spline" || cmbchrttype.Text == "SplineArea" || cmbchrttype.Text == "StackedArea" || cmbchrttype.Text == "StackedBar" || cmbchrttype.Text == "StackedSplineArea" || cmbchrttype.Text == "StepLine" || cmbchrttype.Text == "Area3D" || cmbchrttype.Text == "Bar3D" || cmbchrttype.Text == "Line3D" || cmbchrttype.Text == "ManhattanBar" || cmbchrttype.Text == "Spline3D" || cmbchrttype.Text == "SplineArea3D" || cmbchrttype.Text == "StackedSplineArea3D" || cmbchrttype.Text == "StepLine3D" || cmbchrttype.Text == "StackedArea3D")
            {
                chkHideXaxis.Visible = true;
                lblHideXaxis.Visible = true;
                trlblhidexis.Visible = true;
                chkStaggered.Visible = true;
                lblStaggered.Visible = true;
                trlblstaggered.Visible = true;

                if (cmbchrttype.Text == "Bar")
                {
                    lblLabelPosition.Visible = true;
                    lbltrpositioning.Visible = true;
                    cboLabelPostioning.Visible = true;
                }
                else if (cmbchrttype.Text == "ManhattanBar")
                {
                    lblPerspectiveAngle.Visible = true;
                    tdperspectiveangle.Visible = true;
                    cboPerspectiveAngle.Visible = true;
                    lblZoomPercent.Visible = true;
                    tdlblzoompercent.Visible = true;
                    cboZoomPercent.Visible = true;

                }

            }
            getLegendControls(chkHideLegands.Checked);

        }
    }

    protected void UpdateChartStyle(object sender, EventArgs e)
    {
        bool TitleCheck = false;

        SurveyQuestion ques = Session["TempChart_det"] as SurveyQuestion;
        if (memcharttitle.Text.Trim() != ques.CHART_TITLE)
        {
            TitleCheck = true;
            ques.CHART_TITLE = memcharttitle.Text.Trim();
        }
        if (memoaddcomments.Text.Trim() != ques.COMMENTS)
        {
            TitleCheck = true;
            ques.COMMENTS = memoaddcomments.Text.Trim();
        }
        if (cmbalignment.Text.Trim() != ques.CHART_ALIGNMENT)
        {
            TitleCheck = true;
            ques.CHART_ALIGNMENT = cmbalignment.Text.Trim();
        }
        if (cmbDock.Text.Trim() != ques.CHART_DOCK)
        {
            TitleCheck = true;
            ques.CHART_DOCK = cmbDock.Text.Trim();
        }
        if (cmbchartappearance.Text.Trim().Length > 0 && cmbchartappearance.Text.Trim() != ques.CHART_APPEARANCE)
        {
            ques.CHART_APPEARANCE = cmbchartappearance.Text.Trim();
            webchrtcntrl.AppearanceName = cmbchartappearance.Text.Trim();
        }
        if (cmbpalette.Text.Trim().Length > 0 && cmbpalette.Text.Trim() != ques.CHART_PALETTE)
        {
            ques.CHART_PALETTE = cmbpalette.Text.Trim();
            webchrtcntrl.PaletteName = cmbpalette.Text.Trim();
        }

        Series[] ser1 = webchrtcntrl.Series.ToArray();
        long y = ser1.LongLength;
        if ((chkShowLabel.Checked != Convert.ToBoolean(ques.SHOW_LABEL)) || (y > 0 && chkShowLabel.Checked != webchrtcntrl.Series[0].Label.Visible))
        {
            Series[] ser = webchrtcntrl.Series.ToArray();
            for (int i = 0; i < ser.LongLength; i++)
            {
                ser[i].Label.Visible = Convert.ToBoolean(chkShowLabel.Checked);
            }
            ques.SHOW_LABEL = Convert.ToInt32(chkShowLabel.Checked);
        }
        if ((chkHideLegands.Checked != Convert.ToBoolean(ques.HIDE_LEGAND)) || (chkHideLegands.Checked != webchrtcntrl.Legend.Visible))
        {
            webchrtcntrl.Legend.Visible = Convert.ToBoolean(chkHideLegands.Checked);
            ques.HIDE_LEGAND = Convert.ToInt32(chkHideLegands.Checked);
            getLegendControls(chkHideLegands.Checked);
        }
        if (cboLHorizontalAlign.Text.Trim().Length > 0 && cboLHorizontalAlign.Text.Trim() != ques.LEGEND_HORIZONTALALIGN)
        {
            ques.LEGEND_HORIZONTALALIGN = cboLHorizontalAlign.Text.Trim();
            webchrtcntrl.Legend.AlignmentHorizontal = BuildQuetions.GetLegendHoriAlign(cboLHorizontalAlign.Text.Trim());
        }
        if (cboLVerticalAlign.Text.Trim().Length > 0 && cboLVerticalAlign.Text.Trim() != ques.LEGEND_VERTICALALIGN)
        {
            ques.LEGEND_VERTICALALIGN = cboLVerticalAlign.Text.Trim();
            webchrtcntrl.Legend.AlignmentVertical = BuildQuetions.GetLegendVeriAlign(cboLVerticalAlign.Text.Trim());
        }
        if (cboLDirection.Text.Trim().Length > 0 && cboLDirection.Text.Trim() != ques.LEGEND_DIRECTION)
        {
            bool chek = false;
            if (cboLDirection.Text == "LeftToRight" || cboLDirection.Text == "RightToLeft")
                chek = true;
            chkLEquallySpacedItems.Checked = chek;
            chkLEquallySpacedItems.Enabled = chek;
            ques.LEGEND_DIRECTION = cboLDirection.Text.Trim();
            webchrtcntrl.Legend.Direction = BuildQuetions.GetLegendDirection(cboLDirection.Text.Trim());
        }
        if (chkLEquallySpacedItems.Checked != Convert.ToBoolean(ques.LEGEND_EQUALLYSPACED_ITEMS))
        {
            ques.LEGEND_EQUALLYSPACED_ITEMS = Convert.ToInt32(chkLEquallySpacedItems.Checked);
            if (cboLDirection.Text == "LeftToRight" || cboLDirection.Text == "RightToLeft")
                webchrtcntrl.Legend.EquallySpacedItems = Convert.ToBoolean(chkLEquallySpacedItems.Checked);
        }
        if (cboLMaxHorizontalPercentage.SelectedItem != null && cboLMaxHorizontalPercentage.SelectedItem.Value.ToString() != Convert.ToString(ques.LEGEND_MAX_HORIZONTALALIGN))
        {
            ques.LEGEND_MAX_HORIZONTALALIGN = Convert.ToInt32(cboLMaxHorizontalPercentage.SelectedItem.Value);
            webchrtcntrl.Legend.MaxHorizontalPercentage = Convert.ToDouble(cboLMaxHorizontalPercentage.SelectedItem.Value);
        }
        if (cboLMaxVerticalPercentage.SelectedItem != null && cboLMaxVerticalPercentage.SelectedItem.Value.ToString() != Convert.ToString(ques.LEGEND_MAX_VERTICALALIGN))
        {
            ques.LEGEND_MAX_VERTICALALIGN = Convert.ToInt32(cboLMaxVerticalPercentage.SelectedItem.Value);
            webchrtcntrl.Legend.MaxVerticalPercentage = Convert.ToDouble(cboLMaxVerticalPercentage.SelectedItem.Value);
        }

        if (TitleCheck)
            BuildQuetions.AddChartTitles(webchrtcntrl, ques.CHART_TITLE, ques.COMMENTS, ques.CHART_DOCK, ques.CHART_ALIGNMENT);
        Session["TempChart_det"] = ques;
        if (hdnType.Value == "CHARTOPTIONS")
        {
            Page.RegisterStartupScript("Indshow", "<script>showHide('divChartOptions','spanChartOptions','CHARTOPTIONS')</script>");
        }
        else if (hdnType.Value == "LEGENDOPTIONS")
        {
            Page.RegisterStartupScript("Indshow", "<script>showHide('divLegendOptions','spanLegendOptions','LEGENDOPTIONS')</script>");
        }

    }

    protected void UpdateChartSpecificstyle(object sender, EventArgs e)
    {
        SurveyQuestion ques = Session["TempChart_det"] as SurveyQuestion;
        bool CheckChartUpdates = false;
        bool checkChartXtraoption = false;

        if (chkHideXaxis.Checked != Convert.ToBoolean(ques.HIDE_XAXIS))
        {
            ques.HIDE_XAXIS = Convert.ToInt32(chkHideXaxis.Checked);
            checkChartXtraoption = true;
        }
        if (chkStaggered.Checked != Convert.ToBoolean(ques.STAGGERED))
        {
            ques.STAGGERED = Convert.ToInt32(chkStaggered.Checked);
            checkChartXtraoption = true;
        }
        if (cboZoomPercent.Text.Trim().Length > 0 && Convert.ToInt32(cboZoomPercent.Text) != ques.ZOOMPERCENT)
        {
            ques.ZOOMPERCENT = Convert.ToInt32(cboZoomPercent.Text.Trim());
            checkChartXtraoption = true;
        }
        if (cboPerspectiveAngle.Text.Trim().Length > 0 && Convert.ToInt32(cboPerspectiveAngle.Text) != ques.PERSPECTIVE_ANGLE)
        {
            ques.PERSPECTIVE_ANGLE = Convert.ToInt32(cboPerspectiveAngle.Text.Trim());
            checkChartXtraoption = true;
        }
        if (cboDiagramType.Text.Trim().Length > 0 && cboDiagramType.Text.Trim() != ques.DIAGRAM_TYPE)
        {
            ques.DIAGRAM_TYPE = cboDiagramType.Text.Trim();
            checkChartXtraoption = true;
        }
        if (cboTextDirection.Text.Trim().Length > 0 && cboTextDirection.Text.Trim() != ques.TEXT_DIRECTION)
        {
            ques.TEXT_DIRECTION = cboTextDirection.Text.Trim();
            checkChartXtraoption = true;
        }
        if (cboFunctionType.Text.Trim().Length > 0 && cboFunctionType.Text.Trim() != ques.FUNCTION_TYPE)
        {
            ques.FUNCTION_TYPE = cboFunctionType.Text.Trim();
            checkChartXtraoption = true;
        }
        if (checkChartXtraoption)
            ChartExtraFeatures.SetchartDiagramProp(webchrtcntrl, ques);





        if (chkShowMarker.Checked != Convert.ToBoolean(ques.SHOW_MARKER))
        {
            ques.SHOW_MARKER = Convert.ToInt32(chkShowMarker.Checked);
            CheckChartUpdates = true;
        }
        if (chkInverted.Checked != Convert.ToBoolean(ques.INVERTED))
        {
            ques.INVERTED = Convert.ToInt32(chkInverted.Checked);
            CheckChartUpdates = true;
        }
        if (chkValueAsPercent.Checked != Convert.ToBoolean(ques.VALUEAS_PERCENT))
        {
            ques.VALUEAS_PERCENT = Convert.ToInt32(chkValueAsPercent.Checked);
            CheckChartUpdates = true;
        }

        if (cboLabelAngle.Text.Trim().Length > 0 && Convert.ToInt32(cboLabelAngle.Text.Trim()) != ques.LABEL_ANGLE)
        {
            ques.LABEL_ANGLE = Convert.ToInt32(cboLabelAngle.Text.Trim());
            CheckChartUpdates = true;
        }
        if (cboTransparency.Text.Trim().Length > 0 && Convert.ToInt32(cboTransparency.Text.Trim()) != ques.TRANSPARENCY)
        {
            ques.TRANSPARENCY = Convert.ToInt32(cboTransparency.Text.Trim());
            CheckChartUpdates = true;
        }
        if (cboMarkerKind.Text.Trim().Length > 0 && cboMarkerKind.Text.Trim() != ques.MARKER_KIND)
        {
            ques.MARKER_KIND = cboMarkerKind.Text.Trim();
            CheckChartUpdates = true;
        }
        if (cboMarkerSize.Text.Trim().Length > 0 && Convert.ToInt32(cboMarkerSize.Text.Trim()) != ques.MARKER_SIZE)
        {
            ques.MARKER_SIZE = Convert.ToInt32(cboMarkerSize.Text.Trim());
            CheckChartUpdates = true;
        }
        if (cboExplodedPoint.Text.Trim().Length > 0 && cboExplodedPoint.Text.Trim() != ques.EXPLODED_POINT)
        {
            ques.EXPLODED_POINT = cboExplodedPoint.Text.Trim();
            CheckChartUpdates = true;
        }
        if (cboHoleRadius.Text.Trim().Length > 0 && cboHoleRadius.Text.Trim() != ques.HOLE_RADIUS)
        {
            ques.HOLE_RADIUS = cboHoleRadius.Text.Trim();
            CheckChartUpdates = true;
        }

        if (cboLabelPostioning.Text.Trim().Length > 0 && cboLabelPostioning.Text.Trim() != ques.LABEL_POSITION)
        {
            ques.LABEL_POSITION = cboLabelPostioning.Text.Trim();
            string ctype = cmbchrttype.SelectedItem.Text.Trim();
            if (ctype == "Pie3D" || ctype == "Doughnut3D" || ctype == "Pie" || ctype == "Doughnut" || ctype == "Bar")
            {
                if (ques.LABEL_POSITION != null && ques.LABEL_POSITION.Trim().Length > 0)
                {
                    if (ctype == "Pie3D" || ctype == "Doughnut3D")
                    {
                        Series[] ser = webchrtcntrl.Series.ToArray();
                        for (int i = 0; i < ser.LongLength; i++)
                        {
                            ((PieSeriesLabel)ser[i].Label).Position = BuildQuetions.GetLabelPostion(ques.LABEL_POSITION);
                        }
                    }
                    else if (ctype == "Pie" || ctype == "Doughnut")
                    {
                        Series[] ser = webchrtcntrl.Series.ToArray();
                        for (int i = 0; i < ser.LongLength; i++)
                        {
                            ((PieSeriesLabel)ser[i].Label).Position = BuildQuetions.GetLabelPostion(ques.LABEL_POSITION);
                        }
                    }

                    else if (ctype == "Bar")
                    {
                        Series[] ser = webchrtcntrl.Series.ToArray();
                        for (int i = 0; i < ser.LongLength; i++)
                        {
                            ((SideBySideBarSeriesLabel)ser[i].Label).Position = (ques.LABEL_POSITION == "Center") ? BarSeriesLabelPosition.Center : BarSeriesLabelPosition.Top;
                        }
                    }
                }
            }
        }

        if (CheckChartUpdates)
            BuildQuetions.SetChartPreview(webchrtcntrl, ques, ques.CHART_TYPE);

        Session["TempChart_det"] = ques;
        if (hdnType.Value == "CHARTOPTIONS")
        {
            Page.RegisterStartupScript("Indshow", "<script>showHide('divChartOptions','spanChartOptions','CHARTOPTIONS')</script>");
        }
        else if (hdnType.Value == "LEGENDOPTIONS")

            Page.RegisterStartupScript("Indshow", "<script>showHide('divLegendOptions','spanLegendOptions','LEGENDOPTIONS')</script>");
    }

    private void SetChartClss(SurveyQuestion ques, string ChartTyp)
    {
        SurveyQuestion quest = new SurveyQuestion();
        quest.OTHER_ANS = ques.OTHER_ANS;
        quest.OTHER_RESPCOUNT = ques.OTHER_RESPCOUNT;
        quest.surveyAnswers = ques.surveyAnswers;
        quest.QUESTION_TYPE = ques.QUESTION_TYPE;
        quest.QUESTION_LABEL = ques.QUESTION_LABEL;
        quest.QUESTION_ID = ques.QUESTION_ID;
        quest.CHART_TYPE = ChartTyp;

        quest.CHART_ALIGNMENT = ques.CHART_ALIGNMENT;
        quest.CHART_APPEARANCE = ques.CHART_APPEARANCE;
        quest.CHART_DOCK = ques.CHART_DOCK;
        quest.CHART_PALETTE = ques.CHART_PALETTE;
        quest.CHART_TITLE = ques.CHART_TITLE;
        quest.COMMENTS = ques.COMMENTS;
        quest.SHOW_LABEL = ques.SHOW_LABEL;
        quest.LABEL_ANGLE = ques.LABEL_ANGLE;
        quest.VALUEAS_PERCENT = ques.VALUEAS_PERCENT;
        quest.ZOOMPERCENT = ques.ZOOMPERCENT;
        quest.TRANSPARENCY = ques.TRANSPARENCY;
        quest.PERSPECTIVE_ANGLE = ques.PERSPECTIVE_ANGLE;
        quest.MARKER_SIZE = ques.MARKER_SIZE;
        quest.SHOW_MARKER = ques.SHOW_MARKER;
        quest.INVERTED = ques.INVERTED;
        quest.LABEL_POSITION = ques.LABEL_POSITION;
        quest.MARKER_KIND = ques.MARKER_KIND;
        quest.HOLE_RADIUS = ques.HOLE_RADIUS;
        quest.DIAGRAM_TYPE = ques.DIAGRAM_TYPE;
        quest.TEXT_DIRECTION = ques.TEXT_DIRECTION;
        quest.FUNCTION_TYPE = ques.FUNCTION_TYPE;
        quest.EXPLODED_POINT = ques.EXPLODED_POINT;
        quest.LEGEND_HORIZONTALALIGN = ques.LEGEND_HORIZONTALALIGN;
        quest.LEGEND_VERTICALALIGN = ques.LEGEND_VERTICALALIGN;
        quest.LEGEND_MAX_HORIZONTALALIGN = ques.LEGEND_MAX_HORIZONTALALIGN;
        quest.LEGEND_MAX_VERTICALALIGN = ques.LEGEND_MAX_VERTICALALIGN;
        quest.LEGEND_DIRECTION = ques.LEGEND_DIRECTION;
        quest.LEGEND_EQUALLYSPACED_ITEMS = ques.LEGEND_EQUALLYSPACED_ITEMS;
        quest.HIDE_LEGAND = ques.HIDE_LEGAND;
        quest.HIDE_XAXIS = ques.HIDE_XAXIS;
        quest.STAGGERED = ques.STAGGERED;
        fillchartcontrols(quest);
        Session["TempChart_det"] = quest;

    }

    private void getLegendControls(bool stat)
    {
        lblLHorizontalAlign.Visible = stat;
        cboLHorizontalAlign.Visible = stat;
        trlblhorizontalalign.Visible = stat;

        lblLVerticalAlign.Visible = stat;
        cboLVerticalAlign.Visible = stat;
        trlblverticalalign.Visible = stat;
        lblLDirection.Visible = stat;
        cboLDirection.Visible = stat;
        trlblldirection.Visible = stat;
        lblLEquallySpacedItems.Visible = stat;
        trlblbequallyspaceditems.Visible = stat;
        chkLEquallySpacedItems.Visible = stat;
        lblLMaxHorizontalPercentage.Visible = stat;
        cboLMaxHorizontalPercentage.Visible = stat;
        trlblmaxhorizantalpercent.Visible = stat;
        lblLMaxVerticalPercentage.Visible = stat;
        cboLMaxVerticalPercentage.Visible = stat;
        trlblverticalpercent.Visible = stat;
    }

    public void updateChartsettings()
    {
        SurveyQuestion quest = new SurveyQuestion();
        SurveyQuestion QuesChrt = (SurveyQuestion)Session["Temp_ChartDet"];
        if (QuesChrt != null)
        {
            if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
            {

                GetChartOptions(QuesChrt.CHART_TYPE);

                quest.LABEL_ANGLE = labelAngle;
                quest.VALUEAS_PERCENT = ValasPercent;
                quest.ZOOMPERCENT = ZoomPercent;
                quest.TRANSPARENCY = Transparency;
                quest.PERSPECTIVE_ANGLE = PesrpAngle;
                quest.MARKER_SIZE = markerSiz;
                quest.SHOW_MARKER = showMarker;
                quest.INVERTED = invert;
                quest.LABEL_POSITION = lblPos;
                quest.MARKER_KIND = MarkerKid;
                quest.HOLE_RADIUS = holeRad;
                quest.DIAGRAM_TYPE = diagramTyp;
                quest.TEXT_DIRECTION = txtDirection;
                quest.FUNCTION_TYPE = funTyp;
                quest.EXPLODED_POINT = ExplodedPnt;

                quest.HIDE_XAXIS = Convert.ToInt32(chkHideXaxis.Checked);
                quest.STAGGERED = Convert.ToInt32(chkStaggered.Checked);
                Session["Temp_ChartDet"] = quest;

            }
        }
    }

    private void fillchartcontrols1(SurveyQuestion ques)
    {
        ValidationCore Vcor = new ValidationCore();
        cmbalignment.Value = (ques.CHART_ALIGNMENT.Trim().Length > 0) ? ques.CHART_ALIGNMENT : "Center";
        cmbchartappearance.Value = (ques.CHART_APPEARANCE.Trim().Length > 0) ? ques.CHART_APPEARANCE : "Pastel Kit";
        cmbDock.Value = (ques.CHART_DOCK.Trim().Length > 0) ? ques.CHART_DOCK : "Top";
        cmbpalette.Value = (ques.CHART_PALETTE.Trim().Length > 0) ? ques.CHART_PALETTE : "Nature Colors";
        memcharttitle.Text = (ques.CHART_TITLE.Trim().Length > 0) ? ques.CHART_TITLE : Utilities.ClearHTML(ques.QUESTION_LABEL);

        if (ques.CHART_TYPE.Trim().Length > 0)
        {
            cmbchrttype.Value = ques.CHART_TYPE;
        }
        else
        {
            if (ques.QUESTION_TYPE == 1)
            {
                cmbchrttype.Value = "Bar";
            }
            else if (ques.QUESTION_TYPE == 2)
            {
                cmbchrttype.Value = "Bar3D";
            }
            else if (ques.QUESTION_TYPE == 3)
            {
                cmbchrttype.Value = "Doughnut";
            }
            else if (ques.QUESTION_TYPE == 4)
            {
                cmbchrttype.Value = "Doughnut3D";
            }
            else if (ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11 || ques.QUESTION_TYPE == 12)
            {
                cmbchrttype.Value = "StackedBar";
            }
            else if (ques.QUESTION_TYPE == 15)
            {
                cmbchrttype.Value = "StackedArea";
            }
            else
            {
                cmbchrttype.Value = "Pie";
            }

        }
    }
}