﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="CopyQuestion.aspx.cs" Inherits="CopyQuestion" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="popupHeading">
        <div class="popupTitle">
            <h3><asp:Label ID="lblTitle" runat="server" Text="Copy question" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>
                </h3>
            
        </div>
      <%--  <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick="closeModalSelf(false,'');" alt="Close"
                title="Close">&nbsp;</a>
        </div>--%>
        <!-- //popup heading -->
    </div>
    <div class="popupContentPanel">
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1" /></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" 
                    meta:resourcekey="lblErrMsgResource1" /></div>
        </div>
        <div id="dvReorder" runat="server">
            <div class="leftPad_0">
                <b>
                    <asp:Label runat="server" Text="Choose position to copy the question." 
                    ID="lblreorderNote" meta:resourcekey="lblreorderNoteResource1" />
                </b>
            </div>
            <div class="formPanel" id="dvQuestionReorder" runat="server">
                <asp:Panel runat="server" ID="pnlReorderPopupcc" 
                    meta:resourcekey="pnlReorderPopupccResource1">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>
                               
                                   <div class="listboxDiv">
                                        <asp:ListBox CssClass="copylistboxpnl" runat="server" ID="lbQuesList" 
                                            scroll='false' meta:resourcekey="lbQuesListResource1" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <div class="bottomButtonPanel">
                                        <asp:Button runat="server" Text="Paste" CssClass="dynamicButton" ID="btnSaveReorder"
                                            OnClick="btnSaveReorder_Click" ToolTip="Paste" 
                                            meta:resourcekey="btnSaveReorderResource1" /></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
            </div>
        </div>
        <div id="dvPaidSubscription" runat="server" visible="false">
            <table cellpadding="0" cellspacing="0" width="100%">
              
                <tr>
                    <td>
                        <div class="informationPanelDefault">
                            <div runat="server" id="dvNoOfQuestions" visible="false">
                               
                            </div>
                            <div runat="server" id="dvImagePaidSubscription" visible="false">
                               
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="defaultHeight">
                    </td>
                </tr>
                <tr>
                    <td id="Td1">
                       <div id="dvInformation" runat="server"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="upgradeNowBtnPanel">
                            <ul class="HyperlinkBtn">
                                <li><a href="#" id="hyp_priclink" runat="server" onclick="opener.focus()">Upgrade Now!</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
