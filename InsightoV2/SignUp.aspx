﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignUp.aspx.cs" Inherits="SignUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Insighto SignUp Error</title>
  <style type="text/css" media="screen">
        @import "images/homeimages/thickbox.css";
        @import "images/homeimages/insighto.css";
    </style>
    <link href="Styles/Banner_Style.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Classic/layoutStyle.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Classic/facebox.css" media="screen" rel="stylesheet" type="text/css" />  
    <link href="Styles/notification.css" rel="stylesheet" type="text/css" />  

     <script type="text/javascript">
         function clickButton(e, buttonid) {
             var evt = e ? e : window.event;
             var bt = document.getElementById(buttonid);
             if (bt) {
                 if (evt.keyCode == 13) {
                     bt.click();
                     return false;
                 }
             }
         }

       

</script>



</head>
<body>
    <form id="form1" runat="server">
          <div class="header">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <!-- header -->
                <tr>
                    <td colspan="5">
                        <%--  <uc:TopLinks ID="ucTopLinks" runat="server" />--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <div class="logoHeader">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td width="10">
                                    </td>
                                    <td align="left">
                                        <a id="lnkLogo" runat="server" target="_blank">
                                            <img src="App_Themes/Classic/Images/insighto_logo.png" border="0" alt="" /></a>
                                    </td>
                                    <td align="right" valign="top">
                                        <%--<uc:Login ID="Login1" runat="server" />--%>
                                        <%-- <p class="header-user-login">
                                            New User ?<a href="In/SignUpFree.aspx"> Sign Up Free</a></p>--%>
                                    </td>
                                    <td width="10">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <div id="topMainMenu">
                            <div id="topMainLeftLinks">
                                <div style="margin-left: 20px;">
                                    <ul class="nav">
                                        <li class="activelink"><a href="Home.aspx">
                                            <asp:Label ID="lblHomeLink" runat="server" Text="Home" meta:resourcekey="lblHomeLinkResource1"></asp:Label></a></li>
                                        <li><a id="lnkWhyInsighto" runat="server">
                                            <asp:Label ID="lblWhyInsightoLink" runat="server" Text="Why Insighto" meta:resourcekey="lblWhyInsightoLinkResource1"></asp:Label></a></li>
                                        <%-- <li><a id="lnkAboutUs" runat="server">About Us</a></li>--%>
                                        <li><a id="lnkTakeaTour" href="In/TakeaTour.aspx">
                                            <asp:Label ID="lblTakeaTourLink" runat="server" Text="Take a Tour" meta:resourcekey="lblTakeaTourLinkResource1"></asp:Label></a></li>
                                        <li><a id="lnkAboutUs" href="http://www.insighto.com/pricing/">
                                            <asp:Label ID="lblPlansLink" runat="server" Text="Plans & Pricing" meta:resourcekey="lblPlansLinkResource1"></asp:Label></a></li>
                                         <li><a id="lnkBlog" href="http://insighto.com/blog/">
                                            <asp:Label ID="lblBlogLink" runat="server" Text="Blog" meta:resourcekey="lblBlogResource1"></asp:Label></a></li>

                                                                             
                                    </ul>
                                </div>
                                
                            </div>
                           
                            <div class="topMennuRightPanel" style="position: relative;">
                                <ul class="loginUL">
                                    <li><a href="#" class="signin">LOGIN</a></li>
                                </ul>
                                <div id="signin_menu" runat="server" class="signin_menu">
                                    <!-- login form -->
                                    <div style="position: absolute; right: 5px; top: 5px; cursor: pointer; color: #C424A5;
                                        font-weight: bold; font-size: 14px;" onclick="$('.signin_menu').hide();" title="Close">
                                        X
                                    </div>
                                    <div style="width: 175px; margin: auto;" >
                                        <table align="center" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblInvaildAlert" class="lblRequired" runat="server" Visible="False"
                                                        Text="In valid User Name or Password" meta:resourcekey="lblInvaildAlertResource1"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="loginFormLabel">
                                                    Username
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtEmailAdd" runat="server" class="homeTextbox userName"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" SetFocusOnError="true" Display="Dynamic"
                                                        ErrorMessage="Please enter email address." ControlToValidate="txtEmailAdd" CssClass="lblRequired"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator SetFocusOnError="true" ID="RexEmail" CssClass="lblRequired"
                                                        ControlToValidate="txtEmailAdd" runat="server" Display="Dynamic" ValidationExpression="\s*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\s*"
                                                        ErrorMessage="Please enter valid email address."></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="loginFormLabel">
                                                    Password
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox TextMode="Password" ID="txtPassword" runat="server" class="homeTextbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" SetFocusOnError="true"
                                                        Display="Dynamic" ErrorMessage="Please enter password." ControlToValidate="txtPassword"
                                                        CssClass="lblRequired"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%--<asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="dynamicButtonSmall" OnClick="btnLogin_Click" />--%>
                                                    <asp:Button ID="ibtnLogin" OnClick="ibtnLogin_Click" CssClass="dynamicButtonSmall"
                                                        runat="server" Text="Login" ToolTip="Login" meta:resourcekey="ibtnLoginResource1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="margin-top: 10px;">
                                                        <a href="ForgotPassword.aspx" id="hlkForgotPassword" rel='framebox' w='700' h='300'>
                                                            Forgot Password?</a></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </td>
                </tr>
                <!-- //header -->
            </table>
        </div>
    <div>
        <br />
        <asp:Label ID="lblError" runat="server" Visible="false" Font-Bold="true" Font-Size="Larger"></asp:Label>    
    </div>
    </form>
</body>

<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="images/homeimages/thickbox.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/droid-sans.cufonfonts.js"></script>
<script type="text/javascript" src="Scripts/facebox.js"></script>

    <script src="Scripts/jquery.cookie.js" type="text/javascript"></script>
   <%-- <script src="Scripts/notification.js" type="text/javascript"></script>--%>
<script type="text/javascript">

    Cufon.replace('h2', { fontFamily: 'Droid Sans' });

    Cufon.replace('.cb-style-outer h2', { fontFamily: 'Droid Sans Bold' });

    Cufon.replace('h3', { fontFamily: 'Droid Sans Bold' });
    //Cufon.replace('h4', { fontFamily: 'Droid Sans Bold' });
    //Cufon.replace('h5', { fontFamily: 'Droid Sans Bold' });
</script>
<script type="text/javascript">

    /*** 
    Simple jQuery Slideshow Script
    Released by Jon Raasch (jonraasch.com) under FreeBSD license: free to use or modify, not responsible for anything, etc.  Please link out to me if you like it :)
    ***/

    var slideSwitchCnt = 0;
    function slideSwitchLazyLoad() {

        var divStr = "<div class=\"imgs2\">" +
                        "<h3>Designing a training program</h3>" +
                        "<h3>is a tough job.</h3>" +
                        "<h4 class=\"ex-margin5px\">" +
                        "Knowing how well it was received shouldn\'t be.</h4>" +
                        "</div><div class=\"imgs3\">" +
                        "<h3>Conceptualising an idea</h3>" +
                        "<h3>is strenuous.</h3>" +
                        "<h4 class=\"ex-margin5px\">" +
                        "Validating it with customers shouldn\'t be.</h4>" +
                        "</div> <div class=\"imgs4\">" +
                        "<h3>Ensuring employee delight</h3>" +
                        "<h3>is a demanding task.</h3>" +
                        "<h4 class=\"ex-margin5px\">Measuring it, however, shouldn\'t be.</h4>" +
                        "</div>" +
                        "<div class=\"active imgs1\">" +
                        "<h3>Keeping customers happy</h3>" +
                        "<h3>is a complex job.</h3>" +
                        "<h4 class=\"ex-margin5px\">" +
                        "Assessing their satisfaction shouldn't be.</h4>" +
                        "</div>";

        document.getElementById("slideshow").innerHTML = divStr;
    }

    function slideSwitch() {

        // if (slideSwitchCnt == 0) {
        // slideSwitchLazyLoad();
        activateZopim();
        slideSwitchCnt++;
        // }

        var $active = $('#slideshow DIV.active');

        if ($active.length == 0) $active = $('#slideshow DIV:last');

        // use this to pull the divs in the order they appear in the markup
        var $next = $active.next().length ? $active.next()
        : $('#slideshow DIV:first');

        // uncomment below to pull the divs randomly
        // var $sibs  = $active.siblings();
        // var rndNum = Math.floor(Math.random() * $sibs.length );
        // var $next  = $( $sibs[ rndNum ] );


        $active.addClass('last-active');

        $next.css({ opacity: 0.0 })
        .addClass('active')
        .animate({ opacity: 1.0 }, 1000, function () {
            $active.removeClass('active last-active');
        });
    }


</script>

<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29026379-1']);
    _gaq.push(['_setDomainName', 'insighto.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<script type="text/javascript">
    $(document).ready(function ($) {
        $('a[rel*=framebox]').click(function (e) {
            e.preventDefault();
            ApplyFrameBox($(this));

        });

        $(function () {
            setInterval("slideSwitch()", 7000);
        });
    });

    function ApplyFrameBox(link) {
        var frameWidth = $(link).attr('w');
        var frameHeight = $(link).attr('h');
        var scrolling = $(link).attr('scrolling');
        var href = $(link).attr('href');
        if (!scrolling) {
            scrolling = 'no';
        }

        $.facebox($('<iframe id="modalFrame" name="modalFrame" src="' + href + '" width="' + frameWidth + '" height="' + frameHeight + '" frameborder="0" scrolling="' + scrolling + '"></iframe>'));

        return false;
    }

</script>
<script type="text/javascript">
    $(document).ready(function () {

        $(".signin").click(function (e) {
            e.preventDefault();
            $("div.signin_menu").toggle();
            $(".signin").toggleClass("menu-open");
            $('.userName').focus();
        });

        $("div.signin_menu").mouseup(function () {
            return false
        });



        //        $(document).mouseup(function (e) {
        //            if ($(e.target).parent("a.signin").length == 0) {
        //                alert('hi');
        //                $(".signin").removeClass("menu-open");
        //                $("div#signin_menu").hide();
        //            }
        //        });

    });
</script>

<!--Start of Zopim Live Chat Script-->

<script type="text/javascript">

    function activateZopim() {

        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) { z._.push(c) }, $ = z.s =

d.createElement(s), e = d.getElementsByTagName(s)[0]; z.set = function (o) {
    z.set.

_.push(o)
}; z._ = []; z.set._ = []; $.async = !0; $.setAttribute('charset', 'utf-8');

            $.src = '//cdn.zopim.com/?bYM7XxoDiH1Gv57aQNwmeiatKZbOCdA7'; z.t = +new Date; $.

type = 'text/javascript'; e.parentNode.insertBefore($, e)
        })(document, 'script');

    }
</script>


</html>
