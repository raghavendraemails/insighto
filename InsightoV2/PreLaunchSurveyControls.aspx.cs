﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using App_Code;


public partial class PreLaunchSurveyControls : SurveyPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAlerts();
            BindResourceMessages();
        }
    }

    private void BindResourceMessages()
    {
        //
        rbtnSurveyClose.Items[0].Text = String.Format(Utilities.ResourceMessage("ListItemResource3.Text"), SurveyMode.ToLower());
        chkAlertOnSurveyClose.Text = String.Format(Utilities.ResourceMessage("chkAlertOnSurveyCloseResource1.Text"), SurveyMode);
        chkAlertOnSurveyLaunch.Text = String.Format(Utilities.ResourceMessage("chkAlertOnSurveyLaunchResource1.Text"), SurveyMode);
        lblSelfReminder.Text = String.Format(Utilities.ResourceMessage("lblSelfReminderResource1.Text"), SurveyMode.ToUpper());
        lblCloseSurvey.Text = String.Format(Utilities.ResourceMessage("lblCloseSurveyResource1.Text"), SurveyMode.ToLower());
        lblLaunchStart.Text = String.Format(Utilities.ResourceMessage("lblLaunchStartResource1.Text"), SurveyMode.ToLower());
        lblSurveyScheduler.Text = String.Format(Utilities.ResourceMessage("lblSurveySchedulerResource1.Text"), SurveyMode);
        lblTitle.Text = String.Format(Utilities.ResourceMessage("lblTitleResource1.Text"), SurveyMode);
    
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            string strLaunchUrl = EncryptHelper.EncryptQuerystring("SurveyAlerts.aspx", "Page=PSC&" + Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
            Response.Redirect(strLaunchUrl);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            string strLaunchUrl = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx", "Page=PSC&" + Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
            Response.Redirect(strLaunchUrl);
        }
    }

    private void BindAlerts()
    {
        if (base.SurveyBasicInfoView != null)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            chkAlertOnSurveyLaunch.Checked = Convert.ToBoolean(base.SurveyBasicInfoView.SCHEDULE_ON_ALERT);
            rbtnLaunchSchedule.SelectedValue = base.SurveyBasicInfoView.SCHEDULE_ON.ToString();
            DateTime dtLaunch = Convert.ToDateTime("1/1/1800");
            if (base.SurveyBasicInfoView.LAUNCH_ON_DATE > Convert.ToDateTime("1/1/1800"))
            {
                dtLaunch = CommonMethods.GetConvertedDatetime(base.SurveyBasicInfoView.LAUNCH_ON_DATE, userDetails.StandardBIAS, true);
                txtDateTime.Text = dtLaunch.ToString();
                launchDateTime.Style.Add("display", "block");
                
            }
            else
            {
                launchDateTime.Style.Add("display", "none");
            }
            if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE.ToString() == "0")
            {
                rbtnSurveyClose.ClearSelection();
                rbtnNoofRespondents.Checked = true;
                
                txtNoofRespondents.Text = base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE_ON_NO_RESPONDENTS.ToString();
            }
            else if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE.ToString() == "1")
            {
                rbtnNoofRespondents.Checked = false;
                
                txtNoofRespondents.Text = "0";
                rbtnSurveyClose.ClearSelection();
                rbtnSurveyClose.Items.FindByValue("1").Selected = true;
            }
            else if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE.ToString() == "2")
            {
                rbtnNoofRespondents.Checked = false;
                
                txtNoofRespondents.Text = "0";
                rbtnSurveyClose.ClearSelection();
                rbtnSurveyClose.Items.FindByValue("2").Selected = true;
            }


            DateTime dtClose = Convert.ToDateTime("1/1/1800"); ;
            if (base.SurveyBasicInfoView.CLOSE_ON_DATE > Convert.ToDateTime("1/1/1800"))
            {
                dtClose = CommonMethods.GetConvertedDatetime(base.SurveyBasicInfoView.CLOSE_ON_DATE, userDetails.StandardBIAS, true);
                txtClosingDateTime.Text = dtClose.ToString();
                surveyDateTime.Style.Add("display", "block");
                
            }
            else
            {
                surveyDateTime.Style.Add("display", "none");
            }
            txtAlert1.Text = base.SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES.ToString();
            txtAlert2.Text = base.SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES.ToString();
            DateTime dtMinResp = Convert.ToDateTime("1/1/1800");
            DateTime dtMaxResp = Convert.ToDateTime("1/1/1800");
            DateTime dtRemdate = Convert.ToDateTime("1/1/1800");
            if (base.SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE > Convert.ToDateTime("1/1/1800"))
            {
                dtMaxResp = CommonMethods.GetConvertedDatetime(base.SurveyBasicInfoView.ALERT_ON_MAX_RESPONSES_BY_DATE, userDetails.StandardBIAS, true);
                txtDate2.Text = dtMaxResp.ToString();
            }
            else
            {
                txtDate2.Text = "";
            }
            if (base.SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE.Date > Convert.ToDateTime("1/1/1800").Date)
            {
                dtMinResp = CommonMethods.GetConvertedDatetime(base.SurveyBasicInfoView.ALERT_ON_MIN_RESPONSES_BY_DATE, userDetails.StandardBIAS, true);
                txtDate1.Text = dtMinResp.ToString();
            }
            else
            {
                txtDate1.Text = "";
            }
            txtRemainder.Text = base.SurveyBasicInfoView.REMINDER_TEXT;
            if (base.SurveyBasicInfoView.REMINDER_ON_DATE > Convert.ToDateTime("1/1/1800"))
            {
                dtRemdate = CommonMethods.GetConvertedDatetime(base.SurveyBasicInfoView.REMINDER_ON_DATE, userDetails.StandardBIAS, true);
                txtRemaiderDate.Text = dtRemdate.ToShortDateString();
            }
            else
            {
                txtRemaiderDate.Text = "";
            }
            chkAlertOnSurveyClose.Checked = Convert.ToBoolean(base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE_ALERT);
            //CheckFeaturesRest();
        }

    }

    //private void CheckFeaturesRest()
    //{
    //    var sessionService = ServiceFactory.GetService<SessionStateService>();
    //    var userDetails = sessionService.GetLoginUserDetailsSession();
    //    var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
    //    var listFeatures = ServiceFactory.GetService<FeatureService>();



    //    if (listFeatures.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYLAUNCH).Value == 0)
    //    {
    //        feaLaunch.Value = "0";
    //    }
    //    if (listFeatures.Find(licenceType, FeatureDeploymentType.SCHEDULE_SURVEYCLOSE).Value == 0)
    //    {
    //        feaClose.Value = "0";
    //    }
    //    if (listFeatures.Find(licenceType, FeatureDeploymentType.ALERT_MINMAXNO_RESPONDENTS).Value == 0)
    //    {
    //        txtAlert1.Enabled = false;
    //        txtAlert2.Enabled = false;
    //        txtDate1.Enabled = false;
    //        txtDate2.Enabled = false;
    //    }
    //    if (listFeatures.Find(licenceType, FeatureDeploymentType.SEND_REMAINDERS).Value == 0)
    //    {
    //        txtRemaiderDate.Enabled = false;
    //        txtRemainder.Enabled = false;
    //    }
    //}
}