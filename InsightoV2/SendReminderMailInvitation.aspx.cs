﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using App_Code;


public partial class SendReminderMailInvitation : SurveyPageBase
{
    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int launchid = 0;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains("launchid"))
                    launchid = int.Parse(typeHt["launchid"].ToString());


            }

            if (base.SurveyBasicInfoView.SURVEY_ID > 0)
            {
                var surveyService = ServiceFactory.GetService<SurveyService>();
                var surveyremainderInvitationList = surveyService.GetSendReminderSurveysInvitation(base.SurveyBasicInfoView.SURVEY_ID);

                hfCopyIntro.Value = base.SurveyBasicInfoView.SURVEY_INTRO;

                if (surveyremainderInvitationList.Any())
                {
                    txt_sendersname.Text = surveyremainderInvitationList[0].SENDER_NAME;
                    txtfromAddress.Text = surveyremainderInvitationList[0].FROM_MAILADDRESS;
                    txtEmailSubject.Text = surveyremainderInvitationList[0].MAIL_SUBJECT;
                    txtreplyto.Text = surveyremainderInvitationList[0].REPLY_TO_EMAILADDRESS;
                    EditorEmailInvitation.Value = surveyremainderInvitationList[0].EMAIL_INVITATION;
                }

                CheckFeatureRest();
            }
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        string urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetSendReminderURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&Prev=1&surveyFlag="+base.SurveyFlag);
        Response.Redirect(urlstr);
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool IsBlockedWordExists = false;

        typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
        if (typeHt.Contains("launchid"))
            launchid = int.Parse(typeHt["launchid"].ToString());

        if ((base.SurveyBasicInfoView.SURVEY_ID > 0) && (launchid > 0))
        {

            var surveyService = ServiceFactory.GetService<SurveyService>();
            osm_surveyreminder surveyReminder = new osm_surveyreminder();
            surveyReminder.FROM_MAILADDRESS = txtfromAddress.Text.Trim();
            surveyReminder.MAIL_SUBJECT = txtEmailSubject.Text.Trim();
            surveyReminder.REPLY_TO_EMAILADDRESS = txtreplyto.Text.Trim();
            surveyReminder.SENDER_NAME = txt_sendersname.Text.Trim();
            surveyReminder.EMAIL_INVITATION = EditorEmailInvitation.Value;
            surveyReminder.LAUNCH_ID = launchid;

            var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();
            var contentWords = EditorEmailInvitation.Value.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var existedBlockedWords = blockedWords.Where(b => contentWords.Where(c => c.Trim() == b.BLOCKEDWORD_NAME).Any());
            IsBlockedWordExists = existedBlockedWords.Any();


            if (!IsBlockedWordExists)
            {
                int reminder_id = surveyService.SaveSendReminder(surveyReminder, base.SurveyBasicInfoView.SURVEY_ID);

                if (reminder_id > 0)
                {
                    string urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetReminderConfirmationURL(), "ReminderId=" + reminder_id + "&" + Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&launchid=" + launchid + "&surveyFlag=" + base.SurveyFlag);
                    Response.Redirect(urlstr);
                }
            }
            else
            {
                lblErrMsg.Text = Utilities.ResourceMessage("SendInvitation_Error_BlockedWords"); //"Email invitation contains blocked words.";
                dvErrMsg.Visible = false;
            }
        }
    }



    #region "Method : SetFeatures"
    /// <summary>
    /// This method returns bool value, if the feature flag is one.
    /// </summary>
    /// <param name="strFeature"></param>
    /// <returns></returns>

    public int SetFeatures(string strFeature)
    {
        int iVal = 0;
        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var sessdet = session.GetLoginUserDetailsSession();
        if (sessdet != null)
        {

            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));

            if (sessdet.UserId > 0 && listFeatures != null)
            {
                foreach (var listFeat in listFeatures)
                {

                    if (listFeat.Feature.Contains(strFeature))
                    {
                        iVal = (int)listFeat.Value;
                    }
                }
            }
        }

        else
        {
            Response.Redirect(PathHelper.GetUserLoginPageURL());
        }
        return iVal;
    }

    #endregion

    private void CheckFeatureRest()
    {

        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var sessdet = session.GetLoginUserDetailsSession();

        if (sessdet != null && sessdet.UserId > 0)
        {

            if (SetFeatures("SENDER_NAME") == 5)
            {
                txt_sendersname.Text = sessdet.FirstName;
                txt_sendersname.Enabled = false;
            }
            if (SetFeatures("SENDER_EMAILADDRESS") == 5)
            {
                txtfromAddress.Text = sessdet.LoginName;
                txtfromAddress.Enabled = false;
            }
            if (SetFeatures("REPLYTO_EMAILADDRESS") == 5)
            {
                txtreplyto.Text = sessdet.LoginName;
                txtreplyto.Enabled = false;
            }
            if (SetFeatures("SUBJECT") == 5)
            {
                txtEmailSubject.Text = "Survey Invitation";
                txtEmailSubject.Enabled = false;
            }

        }

    }
    protected void lbtnCopyIntro_Click(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            var surveyService = ServiceFactory.GetService<SurveyService>();
            var surveyremainderInvitationList = surveyService.GetSendReminderSurveysInvitation(base.SurveyBasicInfoView.SURVEY_ID);

            EditorEmailInvitation.Value = base.SurveyBasicInfoView.SURVEY_INTRO;
        }
    }
}