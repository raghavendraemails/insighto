﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using App_Code;
using System.Web.UI.HtmlControls;

public partial class AddIntroduction : SurveyPageBase
{
    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int surveyId = 0;
    string strPage = "";
    string introFlag="";

    #endregion

    #region "Event : Page_Load"
    /// <summary>
    /// page load event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void Page_Load(object sender, EventArgs e)
    {    
        dvSuccessMsg.Visible = false;
        dvErrMsg.Visible = false;
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            if (typeHt.Contains("Flag"))
                introFlag = typeHt["Flag"].ToString();
        }
        if (!IsPostBack)
        {           
            if (introFlag != string.Empty)
            {
                dvErrMsg.Visible = true;
                lblErrMsg.Visible = true;
              //  lblErrMsg.Text = "There is no introduction saved for this survey. Please add introduction and launch again.";
                lblErrMsg.Text = "There is no introduction for this survey. </br> If you wish to go ahead and launch without it, simply click CLEAR and Save.</br> If you want to add introduction, you can do so by modifying the default text below.";
                
            }
            else
            {
                dvErrMsg.Visible = false;
                lblErrMsg.Visible = false;
            }

            var editorControl = Utilities.FindControl<SurveyAnswerControlBase>(Page, "SurveyQuestionControl1");
            Utilities.FindControl<Label>(editorControl, "lblTypeYpurQuestion").Visible = false;
            BindIntro();
            lblIntroHelp.Text = String.Format(Utilities.ResourceMessage("lblIntroHelp"),SurveyMode, SurveyMode.ToLower());
        }
    }
    #endregion


    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            if (typeHt.Contains(""))
                introFlag = typeHt[Constants.SURVEYID].ToString();
        }

        if (surveyId > 0)
        {
            string strLaunchUrl = EncryptHelper.EncryptQuerystring("~/PreLaunchQuestionnaire.aspx", Constants.SURVEYID + "=" + surveyId+"&surveyFlag="+SurveyFlag);
            Response.Redirect(strLaunchUrl);
        }
    }


    #region "Method : BindIntro()"
    /// <summary>
    /// Used to bind survey introduction.
    /// </summary>
    private void BindIntro()
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            if (typeHt.Contains("Page"))
                strPage = typeHt["Page"].ToString();
        }
        var surveyIntroService = ServiceFactory.GetService<SurveyIntroductionService>();
        var surveyIntroList = surveyIntroService.FindBySurveyId(surveyId);

        if (strPage == "PLQI")
        {
            btnBack.Visible = true;
        }
        else
        {
            btnBack.Visible = false;
        }

        if (surveyId > 0)
        {
            btnSave.Visible = true;
            btnClear.Visible = true;
            btnReset.Visible = true;
        }
        else
        {
            btnSave.Visible = false;
            btnClear.Visible = false;
            btnReset.Visible = false;
        }

        if (surveyIntroList.Any())
        {
            EditorQuestionIntro.Value = surveyIntroList[0].SURVEY_INTRO;

            if (surveyIntroList[0].PAGE_BREAK == 1)
            {
                chkPageBreak.Checked = true;
            }
            else
            {
                chkPageBreak.Checked = false;
            }
            btnReset.Visible = true;
        }
        else
        {
            EditorQuestionIntro.Value =  String.Format(Utilities.ResourceMessage("editIntroMsg"),SurveyMode.ToLower());//"<p> You are invited to join a short survey being held among the customers of COMPANY/BRAND X. There are only (X No. of questions) and it will take about X minutes of your time. We will keep all your information confidential and will not transmit it to any third parties. If you have any questions about this survey, please contact me directly at my email address provided below. <br> Your Feedback is important!</br>";
            btnReset.Visible = true;
        }


    }
    #endregion

    #region "Event : btnSave_Click"
    /// <summary>
    /// Used to save or update survey introduction.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool IsBlockedWordExists = false;
        bool isUpdated = default(bool);

        var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();
        var contentWords = EditorQuestionIntro.Value.Trim().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
        var existedBlockedWords = blockedWords.Where(b => contentWords.Where(c => c.Trim() == b.BLOCKEDWORD_NAME).Any());
        IsBlockedWordExists = existedBlockedWords.Any();

        if (!IsBlockedWordExists)
        {

            var content = Utilities.ClearHTML(SurveyQuestionControl1.value);
            //if (content.Trim().Length > 0)
            //{
                isUpdated = IntroSave(isUpdated);

                if (isUpdated)
                {
                    lblSuccMsg.Text = Utilities.ResourceMessage("updatedMsg"); //"Introduction updated successfully.";
                    dvSuccessMsg.Visible = true;                   
                }
                else
                {
                    lblSuccMsg.Text = Utilities.ResourceMessage("saveMsg"); //"Introduction saved successfully.";
                    dvSuccessMsg.Visible = true;
                }
                if (introFlag == string.Empty)
                {
                    Response.Redirect(EncryptHelper.EncryptQuerystring("~/Theme.aspx", Constants.SURVEYID + "=" + base.SurveyID + "&surveyFlag=" + SurveyFlag), false);
                }
                else
                {
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetLaunchSurveyURL(), "SurveyId=" + base.SurveyID + "&surveyFlag=" + base.SurveyFlag));
                }
            //}
            //else
            //{
            //    lblErrMsg.Text = Utilities.ResourceMessage("errIntroMsg");//"Please enter introduction.";
            //    dvErrMsg.Visible = true;
            //}
        }
        else
        {
            lblErrMsg.Text = Utilities.ResourceMessage("errBlockedWords"); //"Introduction contains blocked words.";
            dvErrMsg.Visible = true;
        }
    }

    #endregion

    #region "Method : IntroSave"
    /// <summary>
    /// Used to save or update survey introduction.
    /// </summary>
    /// <param name="isUpdated"></param>
    /// <returns></returns>

    private bool IntroSave(bool isUpdated)
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }

        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();

        osm_surveyIntro surveyIntro = new osm_surveyIntro();
        surveyIntro.SURVEY_ID = surveyId;
        surveyIntro.USERID = userInfo.UserId;
        surveyIntro.SURVEY_INTRO = EditorQuestionIntro.Value.Trim();
        surveyIntro.CREATED_ON = DateTime.UtcNow;
        surveyIntro.STATUS = "Active";
        if (chkPageBreak.Checked)
        {
            surveyIntro.PAGE_BREAK = 1;
        }
        else
        {
            surveyIntro.PAGE_BREAK = 0;
        }

        var surveyIntroService = ServiceFactory.GetService<SurveyIntroductionService>();
        isUpdated = surveyIntroService.Save(surveyIntro);
        return isUpdated;
    }
    #endregion

    #region "Event : btnReset_Click"
    /// <summary>
    /// Used to reset the survey introdcution.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void btnReset_Click(object sender, EventArgs e)
    {
        EditorQuestionIntro.Value = String.Format(Utilities.ResourceMessage("defaultMsg"),SurveyMode.ToLower());//"<p> You are invited to join a short survey being held among the customers of COMPANY/BRAND X. There are only (X No. of questions) and it will take about X minutes of your time. We will keep all your information confidential and will not transmit it to any third parties. If you have any questions about this survey, please contact me directly at my email address provided below. <br> Your Feedback is important!</br>";

        bool isUpdated = default(bool);

        isUpdated = IntroSave(isUpdated);

        lblSuccMsg.Text = Utilities.ResourceMessage("resetMsg");//"Introduction reset successfully.";
        dvSuccessMsg.Visible = true;

    }

    private HtmlTextArea GetTextEditor()
    {
        var editorControl = Utilities.FindControl<SurveyAnswerControlBase>(Page, "SurveyQuestionControl1");
        return Utilities.FindControl<HtmlTextArea>(editorControl, "EditorQuestion");
    }

    public HtmlTextArea EditorQuestionIntro
    {
        get
        {
            return GetTextEditor();
        }
    }

    #endregion
}