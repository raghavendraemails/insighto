﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using Insighto.Business.Services;
using Insighto.Data;
using CovalenseUtilities.Services;
using System.Collections;

public partial class UploadExcel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        lnkSamplePrview.Attributes.Add("Onclick", "javascript: window.open('sampleEmail.htm','SurveyPopup','height=360,width=815,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;");
    }
    protected void iBtnUploadFile_Click(object sender, ImageClickEventArgs e)
    {
        if (fileUpload.HasFile)
        {
            
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            string save = "In/UserImages/" + userInfo.UserId + "/";
            string file = "";
            if (fileUpload.HasFile)
            {
                file = System.Guid.NewGuid().ToString() + "_" + fileUpload.FileName;
                save += file;
                fileUpload.SaveAs(Server.MapPath(save));
            }
            if (rbtnUploadType.Checked == true)
            {
                DataSet xlds = new DataSet();
                DataSet ds = new DataSet();
             
                string str = ("Provider=Microsoft.ACE.OLEDB.12.0;" + ("Data Source=" + Server.MapPath(save) + ";" + "Excel 12.0 Xml;HDR=No;IMEX=1"));
                using (OleDbConnection mycon = new OleDbConnection(str))
                {
                    OleDbConnection objConn = new OleDbConnection(str);
                  
                        objConn.Open();
                        DataTable dt1 = new DataTable();
                        dt1 = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        String[] excelSheets = new String[dt1.Rows.Count];
                        int l = 0;
                        foreach (DataRow row in dt1.Rows)
                        {
                            excelSheets[l] = row["TABLE_NAME"].ToString();
                            l++;
                        }

                        OleDbCommand command = new OleDbCommand();
                        OleDbDataAdapter dbcomm = new OleDbDataAdapter();
                        mycon.Open();
                        for (int j = 0; j <= 0; j++)
                        {
                            string strSelectString = "SELECT * FROM [" + dt1.Rows[j][2] + "]";
                            command = new OleDbCommand(strSelectString, mycon);
                            command.CommandTimeout = 999999;
                            dbcomm = new OleDbDataAdapter(strSelectString, mycon);
                            dbcomm.Fill(ds);
                        }
                        objConn.Close();
                        bool status = false;

                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {


                            if (ds.Tables[0].Rows[0][j].ToString() != "" && ds.Tables[0].Rows[0][j].ToString() != null)
                            {
                                status = true;
                            }

                        }
                        int cust_id = ds.Tables[0].Columns.Count;
                        int cust_id1 = ds.Tables[0].Columns.Count;
                        int temp = 0;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            for (int k = 3; k < ds.Tables[0].Columns.Count; k++)
                            {
                                if (ds.Tables[0].Rows[i][k].ToString() != "")
                                {
                                    if (k > temp)
                                    {
                                        temp = k;
                                        cust_id = k;
                                    }
                                }
                            }

                        }

                        for (int m = 3; m < cust_id; m++)
                        {
                            if (ds.Tables[0].Rows[0][m].ToString() == "")
                                ds.Tables[0].Rows[0][m] = Convert.ToString("Custom" + (m - 2));
                        }

                        if (status == true)
                        {
                            var contacts = new osm_contactlist();
                            if (ds.Tables[0].Rows[0][0].ToString() != "")
                            {
                                SaveContactList(ds);                                         
                            }
                        }
                        else
                        {
                            this.ClientScript.RegisterStartupScript(this.GetType(), "Some Title", "<script language=\"javaScript\">" + "alert('Invalid Excel Format!');" + "window.location.href='CreateEmailList.aspx';" + "<" + "/script>");
                        }                    
                }
            }
            else
            {
                DataSet ds = new DataSet();
                string savecsv = "In/UserImages/" + userInfo.UserId+ "/";            
                string strSql = "SELECT * FROM [" + file + "]";
                string strCSVConnString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath(savecsv) + ";" + "Extended Properties='text;HDR=No;IMEX=1'";
                OleDbDataAdapter oleda = new OleDbDataAdapter(strSql, strCSVConnString);
                try
                {
                    oleda.Fill(ds);
                    SaveContactList(ds); 
                }
                catch (Exception)
                {
                    ClientScript.RegisterStartupScript(GetType(), "Some Title", "<script language=\"javaScript\">" + "alert('File is Not in Correct Format');" + "window.location.href='CreateEmailList.aspx';" + "<" + "/script>");
                }
            }
        }
    }

    protected void  SaveContactList(DataSet ds)
    {
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        var emailService = ServiceFactory.GetService<EmailService>();
        var contactlist = emailService.SaveEmailContactList(userInfo.UserId, txtEmailListName.Text);
       
        if (ds.Tables[0].Rows.Count > 3)
        contactlist.CUSTOM_HEADER1 = ds.Tables[0].Rows[0][3].ToString().Trim();
        if (ds.Tables[0].Rows.Count > 4)
        contactlist.CUSTOM_HEADER2 = ds.Tables[0].Rows[0][4].ToString().Trim();
        if (ds.Tables[0].Rows.Count > 5)
        contactlist.CUSTOM_HEADER3 = ds.Tables[0].Rows[0][5].ToString().Trim();
        if (ds.Tables[0].Rows.Count > 6)
        contactlist.CUSTOM_HEADER4 = ds.Tables[0].Rows[0][6].ToString().Trim();
        if (ds.Tables[0].Rows.Count > 7)
        contactlist.CUSTOM_HEADER5 = ds.Tables[0].Rows[0][7].ToString().Trim();
        if (ds.Tables[0].Rows.Count > 8)
            contactlist.CUSTOM_HEADER6 = ds.Tables[0].Rows[0][8].ToString().Trim();
        if (ds.Tables[0].Rows.Count > 9)
            contactlist.CUSTOM_HEADER7 = ds.Tables[0].Rows[0][9].ToString().Trim();
        if (ds.Tables[0].Rows.Count > 10)
            contactlist.CUSTOM_HEADER8 = ds.Tables[0].Rows[0][10].ToString().Trim();
            emailService.SaveAddressBookColumnsByContactListId(contactlist);
            SaveEmailList(contactlist.CONTACTLIST_ID, ds);
        
    }

    protected void SaveEmailList(int contactListId,DataSet ds)
    {
        string Inval_dupIDs = "";      
        if (ds != null && ds.Tables.Count > 0)
        {
            Hashtable ht = new Hashtable();
            for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i][0].ToString() != null && Convert.ToString(ds.Tables[0].Rows[i][0].ToString()).Trim().Length > 0)
                {
                    string email = Convert.ToString(ds.Tables[0].Rows[i][0]);
                    
                        if (ht.Contains(Convert.ToString(ds.Tables[0].Rows[i][0].ToString()).Trim()))
                        {
                            Inval_dupIDs += "," + ds.Tables[0].Rows[i][0].ToString().Trim();
                        }
                        else
                        {
                            var emails = new osm_emaillist();
                            emails.CONTACTLIST_ID = contactListId;
                            ht.Add(ds.Tables[0].Rows[i][0].ToString().Trim(), i);
                            if (ds.Tables[0].Rows[i][0].ToString() != "")
                            {
                                if (ds.Tables[0].Columns.Count > 0)
                                    emails.EMAIL_ADDRESS = ds.Tables[0].Rows[i][0].ToString();
                                if (ds.Tables[0].Columns.Count > 1)
                                    emails.FIRST_NAME = ds.Tables[0].Rows[i][1].ToString();
                                if (ds.Tables[0].Columns.Count > 2)
                                    emails.LAST_NAME = ds.Tables[0].Rows[i][2].ToString();
                                if (ds.Tables[0].Columns.Count > 3)
                                    emails.CUSTOM_VAR1 = ds.Tables[0].Rows[i][3].ToString();
                                if (ds.Tables[0].Columns.Count > 4)
                                    emails.CUSTOM_VAR2 = ds.Tables[0].Rows[i][4].ToString();
                                if (ds.Tables[0].Columns.Count > 5)
                                    emails.CUSTOM_VAR3 = ds.Tables[0].Rows[i][5].ToString();
                                if (ds.Tables[0].Columns.Count > 6)
                                    emails.CUSTOM_VAR4 = ds.Tables[0].Rows[i][6].ToString();
                                if (ds.Tables[0].Columns.Count > 7)
                                    emails.CUSTOM_VAR5 = ds.Tables[0].Rows[i][7].ToString();
                                if (ds.Tables[0].Columns.Count > 8)
                                    emails.CUSTOM_VAR6 = ds.Tables[0].Rows[i][8].ToString();
                                if (ds.Tables[0].Columns.Count > 9)
                                    emails.CUSTOM_VAR7 = ds.Tables[0].Rows[i][9].ToString();
                                if (ds.Tables[0].Columns.Count > 10)
                                    emails.CUSTOM_VAR8 = ds.Tables[0].Rows[i][10].ToString();
                                var emailService = ServiceFactory.GetService<EmailService>();
                                emailService.SaveEmailsByList(emails);                                
                            }
                        }                   
                }      
            }
        }
    }
}