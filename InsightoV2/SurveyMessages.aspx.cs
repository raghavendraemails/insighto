﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.Enumerations;
using System.IO;
using System.Configuration;
using Resources;
using System.Collections;
using System.Web.UI.HtmlControls;
using App_Code;

namespace Insighto.Pages
{
    public partial class SurveyMessages : SurveyPageBase
    {
        bool addEmail = false;
        Hashtable ht = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);

            //MODIFIED ON 1-JUL-16 AS FOR NEW POLLS CUST LICENSETYPE COULD BE NULL
            hdnLicenseType.Value = licenceType.ToString();// (userDetails.LicenseType.ToString() ?? "FREE");
            
            if (!IsPostBack)
            {
                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);

                    if (ht != null && ht.Count > 0 && ht.Contains("Page"))
                    {
                        if (ht["Page"].ToString() == "PSEM")
                        {
                            hlnkBack.Visible = true;
                            hlnkBack.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPreLaunchSurveyEndMessagesURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
                        }
                        else
                        {
                            hlnkBack.Visible = false;
                        }

                    }
                    else
                    {
                        hlnkBack.Visible = false;
                    }
                }
               
                if (base.SurveyBasicInfoView != null)
                {                    
                    if (base.SurveyBasicInfoView.FINISHOPTIONS_STATUS == 1)
                    {
                        rbtnThankyou.SelectedValue = base.SurveyBasicInfoView.THANKUPAGE_TYPE.ToString();
                        hdnThanku.Value = base.SurveyBasicInfoView.THANKUPAGE_TYPE.ToString();
                        rbtnClose.SelectedValue = base.SurveyBasicInfoView.CLOSEPAGE_TYPE.ToString();
                        hdnClose.Value = base.SurveyBasicInfoView.CLOSEPAGE_TYPE.ToString();
                        rbtnOverQuota.SelectedValue = base.SurveyBasicInfoView.OVERQUOTAPAGE_TYPE.ToString();
                        hdnOverQuota.Value = base.SurveyBasicInfoView.OVERQUOTAPAGE_TYPE.ToString();
                        rbtnScreenOut.SelectedValue = base.SurveyBasicInfoView.SCREENOUTPAGE_TYPE.ToString();
                        hdnScreenOut.Value = base.SurveyBasicInfoView.SCREENOUTPAGE_TYPE.ToString(); 
                        if (base.SurveyBasicInfoView.THANKUPAGE_TYPE.ToString() == "1")
                        {
                            txtThankyouMsg.Text = base.SurveyBasicInfoView.THANKYOU_PAGE;
                        }
                        else
                        {
                            lblDefaultMsg.Text = base.SurveyBasicInfoView.THANKYOU_PAGE;
                        }
                        if (base.SurveyBasicInfoView.CLOSEPAGE_TYPE.ToString() == "1")
                        {
                            txtCloseCustom.Text = base.SurveyBasicInfoView.CLOSE_PAGE;
                        }
                        else
                        {
                            lblCloseDefaultMsg.Text = base.SurveyBasicInfoView.CLOSE_PAGE;
                        }
                        if (base.SurveyBasicInfoView.SCREENOUTPAGE_TYPE.ToString() == "1")
                        {
                            txtScreenoutCustom.Text = base.SurveyBasicInfoView.SCREENOUT_PAGE;
                        }
                        else
                        {
                            lblScreenoutMsg.Text = base.SurveyBasicInfoView.SCREENOUT_PAGE;
                        }
                        if (base.SurveyBasicInfoView.OVERQUOTAPAGE_TYPE.ToString() == "1")
                        {
                            txtOverQuota.Text = base.SurveyBasicInfoView.OVERQUOTA_PAGE;
                        }
                        else
                        {
                            lblOverQuotaMsg.Text = base.SurveyBasicInfoView.OVERQUOTA_PAGE;
                        }
                    }
                    else
                    {
                        rbtnThankyou.SelectedValue = base.SurveyBasicInfoView.THANKUPAGE_TYPE.ToString();
                        hdnThanku.Value = base.SurveyBasicInfoView.THANKUPAGE_TYPE.ToString();
                        rbtnClose.SelectedValue = base.SurveyBasicInfoView.CLOSEPAGE_TYPE.ToString();
                        hdnClose.Value = base.SurveyBasicInfoView.CLOSEPAGE_TYPE.ToString(); 
                        rbtnOverQuota.SelectedValue = base.SurveyBasicInfoView.OVERQUOTAPAGE_TYPE.ToString();
                        hdnOverQuota.Value = base.SurveyBasicInfoView.OVERQUOTAPAGE_TYPE.ToString(); 
                        rbtnScreenOut.SelectedValue = base.SurveyBasicInfoView.SCREENOUTPAGE_TYPE.ToString();
                        hdnScreenOut.Value = base.SurveyBasicInfoView.SCREENOUTPAGE_TYPE.ToString();
                        lblDefaultMsg.Text = base.SurveyBasicInfoView.THANKYOU_PAGE;
                        lblCloseDefaultMsg.Text = base.SurveyBasicInfoView.CLOSE_PAGE;
                        lblScreenoutMsg.Text = base.SurveyBasicInfoView.SCREENOUT_PAGE;
                        lblOverQuotaMsg.Text = base.SurveyBasicInfoView.OVERQUOTA_PAGE;
                    }

                    string fileName = Server.MapPath("~/TemplateFolder/SurveyEndPageContent.txt");



                    if (hdnLicenseType.Value != "PREMIUM_YEARLY")
                    {



                        var dic = new Dictionary<string, string>
                      {
                          {"##LogoImageUrl##", PathHelper.SurveyLogoImageUrl() },
                          {"##ContentImageUrl##", PathHelper.SurveyAlertThankyouUrl() },                         
                          {"##InsightoUrl##",ConfigurationManager.AppSettings["RootURL"].ToString()}
                      };

                        var templateContent = ServiceFactory.GetService<TemplateService>().GetDataFromTempalte(fileName, dic);
                        thankyouLogo.InnerHtml = templateContent[0];
                        closeLogo.InnerHtml = templateContent[0];
                        screenOutLogo.InnerHtml = templateContent[0];
                        overQuotaLogo.InnerHtml = templateContent[0];
                        ThankyouContent.InnerHtml = templateContent[1];
                        CloseContent.InnerHtml = templateContent[1];
                        ScreenoutContent.InnerHtml = templateContent[1];
                        OverQuotaContent.InnerHtml = templateContent[1];
                    }
                    SetThankuPage();
                    SetClosePage();
                    SetOverQuotaPage();
                    SetScreenOutPage();
                }
                else
                {
                    if (base.SurveyBasicInfoView.SURVEY_ID == null || base.SurveyBasicInfoView.SURVEY_ID <= 0)
                    {
                        Response.Redirect(PathHelper.GetSurveyBasicsURL());
                    }
                    else if (base.SurveyBasicInfoView.SURVEYCONTROL_ID == null || base.SurveyBasicInfoView.SURVEYCONTROL_ID <= 0)
                    {
                        Response.Redirect(PathHelper.GetSurveyControlsURL());
                    }
                }
            }
            if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0 && (base.SurveyBasicInfoView.STATUS == "Active" || base.SurveyBasicInfoView.STATUS == "Closed"))
            {
                if (base.SurveyBasicInfoView.STATUS == "Closed")
                {
                    string alertText = "";
                    if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE == 1)
                        alertText = Utilities.ResourceMessage("SurveyMessages_Info_CrossedSchedule"); //"The survey is closed because it has crossed the schedule survey close date. If you want to edit the survey, please activate the survey from your closed survey list and select a future close date from your Survey Controls settings.";
                    else if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE == 0)
                        alertText = Utilities.ResourceMessage("SurveyMessages_Info_AchievementResponse");  //"The survey is closed due to achievement of response quota. If you want to edit the survey, please activate the survey  from your closed survey list and increase the number of response quota from your Survey Controls settings.";
                    this.ClientScript.RegisterStartupScript(this.GetType(), "Some Title", "<script language=\"javaScript\">" + "alert('" + alertText + "');</script>");
                }
            }
            else
            {
                btnSaveContinue.Visible = true;
                btnAddEditEmailList.Visible = false;
                btnSaveChanges.Visible = false;
            }
        }
        protected void rbtnThankyou_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetThankuPage();
        }

        protected void rbtnOverQuota_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetOverQuotaPage();
        }
        protected void rbtnClose_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetClosePage();
        }
        protected void rbtnScreenOut_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetScreenOutPage();
        }

        public void SetThankuPage()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            if (rbtnThankyou.SelectedItem != null)
            {
                if (rbtnThankyou.SelectedItem.Value.ToString() == "1")
                {

                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 1)
                    {

                        if (base.SurveyBasicInfoView.FINISHOPTIONS_STATUS == 1 && base.SurveyBasicInfoView.THANKUPAGE_TYPE == 1)
                        {
                            if (base.SurveyBasicInfoView.THANKYOU_PAGE != "You can put your custom text here")
                            {
                                txtThankyouMsg.ForeColor = System.Drawing.Color.FromName("#F67803");
                            }
                            txtThankyouMsg.Text = base.SurveyBasicInfoView.THANKYOU_PAGE;

                        }
                        else
                        {
                            txtThankyouMsg.Text = Utilities.ResourceMessage("SurveyMessages_Custom_Message"); //"You can put your custom text here";
                        }

                    }
                    divThankyouDefault.Style.Add("display", "none");
                    divThankyouCustom.Style.Add("display", "block");

                }
                else if (rbtnThankyou.SelectedValue == "0")
                {
                    rbtnThankyou.SelectedValue = "0";

                    divThankyouDefault.Style.Add("display", "block");
                    divThankyouCustom.Style.Add("display", "none");                    
                }
            }
            else
            {
                rbtnThankyou.SelectedValue = "0";
                divThankyouDefault.Style.Add("display", "block");
                divThankyouCustom.Style.Add("display", "none");                
            }

            if (!IsPostBack)
            {
                BindResourceMessages();
            }

        }

        private void BindResourceMessages()
        {
            dvSurveyClose.InnerHtml = Utilities.ResourceMessage("dvSurveyClose");
            dvIconOut.InnerHtml = Utilities.ResourceMessage("dvIconOut");
            dvOverQuota.InnerHtml = Utilities.ResourceMessage("dvOverQuota");
            dvThankyou.InnerHtml = Utilities.ResourceMessage("dvThankyou");

        }

        public void SetClosePage()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            if (rbtnClose.SelectedItem != null)
            {
                if (rbtnClose.SelectedItem.Value.ToString() == "1")
                {
                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 1)
                    {
                        if (base.SurveyBasicInfoView.FINISHOPTIONS_STATUS == 1 && base.SurveyBasicInfoView.THANKUPAGE_TYPE == 1)
                        {
                            if (base.SurveyBasicInfoView.CLOSE_PAGE != Utilities.ResourceMessage("SurveyMessages_Custom_Message"))
                            {
                                txtCloseCustom.ForeColor = System.Drawing.Color.FromName("#F67803");
                            }
                            txtCloseCustom.Text = base.SurveyBasicInfoView.CLOSE_PAGE;

                        }
                        else
                        {
                            txtCloseCustom.Text = Utilities.ResourceMessage("SurveyMessages_Custom_Message");//"You can put your custom text here";

                        }

                    }
                    divCloseDefault.Style.Add("display", "none");
                    divCloseCustom.Style.Add("display", "block");

                }
                else if (rbtnClose.SelectedValue == "0")
                {
                    divCloseDefault.Style.Add("display", "block");
                    divCloseCustom.Style.Add("display", "none");
                }
            }
            else
            {
                rbtnClose.SelectedValue = "0";
                divCloseDefault.Style.Add("display", "block");
                divCloseCustom.Style.Add("display", "none");
            }

        }

        public void SetScreenOutPage()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            if (rbtnScreenOut.SelectedItem != null)
            {
                if (rbtnScreenOut.SelectedItem.Value.ToString() == "1")
                {
                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 1)
                    {
                        if (base.SurveyBasicInfoView.FINISHOPTIONS_STATUS == 1 && base.SurveyBasicInfoView.SCREENOUTPAGE_TYPE == 1)
                        {
                            if (base.SurveyBasicInfoView.SCREENOUT_PAGE !=  Utilities.ResourceMessage("SurveyMessages_Custom_Message"))
                            {
                                txtScreenoutCustom.ForeColor = System.Drawing.Color.FromName("#F67803");
                            }
                            txtScreenoutCustom.Text = base.SurveyBasicInfoView.SCREENOUT_PAGE;

                        }
                        else
                        {
                            txtScreenoutCustom.Text =  Utilities.ResourceMessage("SurveyMessages_Custom_Message");
                        }
                    }
                    divScreenoutcustom.Style.Add("display", "block");
                    divScreenoutdefault.Style.Add("display", "none");

                }
                else if (rbtnScreenOut.SelectedValue == "0")
                {
                    divScreenoutcustom.Style.Add("display", "none");
                    divScreenoutdefault.Style.Add("display", "block");
                }
            }
            else
            {
                rbtnScreenOut.SelectedValue = "0";
                divScreenoutdefault.Style.Add("display", "block");
                divThankyouCustom.Style.Add("display", "none");
            }

        }
        protected void BtnSaveContinue_Click(object sender, EventArgs e)
        {
            SaveFinishOptionsUpdateAfterActive();
            lblSuccMsg.Text = Utilities.ResourceMessage("SurveyMessages_Success_Saved"); //"Survey end messages saved successfully.";
            lblSuccMsg.Visible = true;
            dvSuccessMsg.Visible = true;
        }

        public void SetOverQuotaPage()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            if (rbtnOverQuota.SelectedItem != null)
            {
                if (rbtnOverQuota.SelectedItem.Value.ToString() == "1")
                {

                    if (listFeatures.Find(licenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 1)
                    {
                        if (base.SurveyBasicInfoView.FINISHOPTIONS_STATUS == 1 && base.SurveyBasicInfoView.OVERQUOTAPAGE_TYPE == 1)
                        {
                            if (base.SurveyBasicInfoView.OVERQUOTA_PAGE != Utilities.ResourceMessage("SurveyMessages_Custom_Message"))
                            {
                                txtOverQuota.ForeColor = System.Drawing.Color.FromName("#F67803");
                            }
                            txtOverQuota.Text = base.SurveyBasicInfoView.OVERQUOTA_PAGE;

                        }
                        else
                        {
                            txtOverQuota.Text = Utilities.ResourceMessage("SurveyMessages_Custom_Message");

                        }

                    }
                    divOverQuotaCustom.Style.Add("display", "block");
                    divOverQuotaDefault.Style.Add("display", "none");

                }
                else if (rbtnOverQuota.SelectedValue == "0")
                {
                    divOverQuotaCustom.Style.Add("display", "none");
                    divOverQuotaDefault.Style.Add("display", "block");
                }
            }
            else
            {
                rbtnOverQuota.SelectedValue = "0";
                divOverQuotaCustom.Style.Add("display", "none");
                divOverQuotaDefault.Style.Add("display", "block");
            }

        }

        private void SaveFinishOptionsUpdateAfterActive()
        {
            string strThankuContent = "", strCloseContent = "", strScreenContent = "", strOverquote = "";
            bool isBlockedWordExists = false;
            if (base.SurveyBasicInfoView.SURVEY_ID > 0 && base.SurveyBasicInfoView.SURVEYCONTROL_ID > 0)
            {

                if (rbtnThankyou.SelectedValue == "0")
                {
                    string str = Convert.ToString(lblDefaultMsg.Text);
                    str = str.Replace("In/homeimages/", "homeimages/");
                    strThankuContent = str;
                }
                else
                {
                    strThankuContent = txtThankyouMsg.Text.Trim();
                }
                if (rbtnClose.SelectedValue == "0")
                {
                    string str = Convert.ToString(lblCloseDefaultMsg.Text);
                    str = str.Replace("In/homeimages/", "homeimages/");
                    strCloseContent = str;
                }
                else
                {
                    strCloseContent = txtCloseCustom.Text.Trim().ToString();
                }
                if (rbtnScreenOut.SelectedValue == "0")
                {
                    string str = Convert.ToString(lblScreenoutMsg.Text);
                    str = str.Replace("In/homeimages/", "homeimages/");
                    strScreenContent = str;
                }
                else
                {
                    strScreenContent = txtScreenoutCustom.Text.Trim();
                }
                if (rbtnOverQuota.SelectedValue == "0")
                {
                    string str = Convert.ToString(lblOverQuotaMsg.Text);
                    str = str.Replace("In/homeimages/", "homeimages/");
                    strOverquote = str;
                }
                else
                {
                    strOverquote = txtOverQuota.Text.Trim();
                }
                if (base.SurveyBasicInfoView.STATUS == "Active")
                {
                    var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();
                    isBlockedWordExists = blockedWords.Where(b => strThankuContent.Contains(b.BLOCKEDWORD_NAME)).Any();
                    isBlockedWordExists = blockedWords.Where(b => strScreenContent.Contains(b.BLOCKEDWORD_NAME)).Any();
                    isBlockedWordExists = blockedWords.Where(b => strOverquote.Contains(b.BLOCKEDWORD_NAME)).Any();
                    isBlockedWordExists = blockedWords.Where(b => strCloseContent.Contains(b.BLOCKEDWORD_NAME)).Any();

                    if (isBlockedWordExists)
                    {
                        if (base.SurveyBasicInfoView != null && (base.SurveyBasicInfoView.THANKUPAGE_TYPE != ValidationHelper.GetInteger(rbtnThankyou.SelectedValue, 0) || base.SurveyBasicInfoView.CLOSEPAGE_TYPE != ValidationHelper.GetInteger(rbtnClose.SelectedValue, 0) || base.SurveyBasicInfoView.SCREENOUTPAGE_TYPE != ValidationHelper.GetInteger(rbtnScreenOut.SelectedValue, 0) || base.SurveyBasicInfoView.OVERQUOTAPAGE_TYPE != ValidationHelper.GetInteger(rbtnOverQuota.SelectedValue, 0) || base.SurveyBasicInfoView.THANKYOU_PAGE != strThankuContent || base.SurveyBasicInfoView.CLOSE_PAGE != strCloseContent || base.SurveyBasicInfoView.SCREENOUT_PAGE != strScreenContent || base.SurveyBasicInfoView.OVERQUOTA_PAGE != strOverquote))
                            addEmail = true;
                    }
                }
            }
            if (!isBlockedWordExists)
            {
                SurveyBasicInfoView.SURVEY_ID = base.SurveyBasicInfoView.SURVEY_ID;
                SurveyBasicInfoView.THANKYOU_PAGE = strThankuContent;
                SurveyBasicInfoView.CLOSE_PAGE = strCloseContent;
                SurveyBasicInfoView.SCREENOUT_PAGE = strScreenContent;
                SurveyBasicInfoView.OVERQUOTA_PAGE = strOverquote;
                SurveyBasicInfoView.THANKUPAGE_TYPE = ValidationHelper.GetInteger(rbtnThankyou.SelectedValue, 0);
                SurveyBasicInfoView.CLOSEPAGE_TYPE = ValidationHelper.GetInteger(rbtnClose.SelectedValue, 0);
                SurveyBasicInfoView.SCREENOUTPAGE_TYPE = ValidationHelper.GetInteger(rbtnScreenOut.SelectedValue, 0);
                SurveyBasicInfoView.OVERQUOTAPAGE_TYPE = ValidationHelper.GetInteger(rbtnOverQuota.SelectedValue, 0);
                SurveyBasicInfoView.FINISHOPTIONS_STATUS = 1;
                ServiceFactory.GetService<SurveyControls>().UpdateSurveyControlsFromView(SurveyBasicInfoView);
                if (SurveyBasicInfoView != null)
                {

                    SetClosePage();
                    SetOverQuotaPage();
                    SetScreenOutPage();
                    SetThankuPage();
                    //if (SurveyBasicInfoView.STATUS == "Draft")
                    //{
                    //    //Response.Redirect(PathHelper.GetUrlredirectionURL() + "?Key=" + Request.QueryString["key"]);
                    //}
                }
            }
            else
            {
                lblErrMsg.Text = Utilities.ResourceMessage("SurveyMessages_Error_BlockedWords"); //"Survey contains blocked words.";
                lblErrMsg.Visible = true;
                dvErrMsg.Visible = true;
            }

        }

        public string AlertMessageText()
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            string txtMessage = "";
            txtMessage = Utilities.ResourceMessage("SurveyMessages_Message_Saved");//"You have saved changes to an active survey. These changes are effective immediately and your respondents will now see the revised survey";
            if (base.SurveyBasicInfoView.EMAILLIST_TYPE != 2)
            {
                if (base.SurveyBasicInfoView.SCHEDULE_ON == 1 && base.SurveyBasicInfoView.LAUNCH_ON_DATE.AddMinutes(userDetails.StandardBIAS) > DateTime.UtcNow.AddMinutes(userDetails.StandardBIAS))
                {
                    if (base.SurveyBasicInfoView.EMAILLIST_TYPE == 1 && base.SurveyBasicInfoView.CONTACTLIST_ID > 0)
                    {
                        var contactDetails = ServiceFactory.GetService<EmailService>().GetContactListByContactListId(base.SurveyBasicInfoView.CONTACTLIST_ID);
                        if (contactDetails != null)
                        {
                            var contactListName = contactDetails.Select(s => new { s.CONTACTLIST_NAME });
                            txtMessage = String.Format(Utilities.ResourceMessage("SurveyMessages_Changes_Saved"),base.SurveyBasicInfoView.LAUNCH_ON_DATE.AddMinutes(userDetails.StandardBIAS)); //"Your changes have been saved. Your respondents will see the revised changes when the survey gets launched on " + base.SurveyBasicInfoView.LAUNCH_ON_DATE.AddMinutes(userDetails.StandardBIAS);
                            txtMessage += String.Format(Utilities.ResourceMessage("SurveyMessages_Chang_LaunchDate"), contactDetails.Count, contactListName);// ". If you wish to change the date of launch, you may do so from the Survey Controls page. The survey will be sent to " + contactDetails.Count + " email ids that are currently in your email list " + contactListName;
                        }
                    }

                }
                else
                {
                    txtMessage = String.Format(Utilities.ResourceMessage("SurveyMessages_Changes_Revised"), base.SurveyBasicInfoView.LAUNCH_ON_DATE.AddMinutes(userDetails.StandardBIAS));//"Your changes have been saved. Your respondents will see the revised changes when the survey gets launched on" + base.SurveyBasicInfoView.LAUNCH_ON_DATE.AddMinutes(userDetails.StandardBIAS);

                }
            }
            else
            {
               // txtMessage = "Changes have been made in the survey and will reflect in the URL that you have used to ";
               // txtMessage += "launch the survey. In case you want to send the survey to more people, please copy and ";
                //txtMessage += "paste the URL below and send through your own mail system / IM / Social media.                                                                                                                                             ";
                //txtMessage += " URL : " + base.SurveyBasicInfoView.SURVEY_URL;
                txtMessage = String.Format(Utilities.ResourceMessage("SurveyMessages_Launch_URL"), base.SurveyBasicInfoView.SURVEY_URL);
            }

            return txtMessage;
        }

        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {
            SaveFinishOptionsUpdateAfterActive();
            string message = AlertMessageText();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "myscript", "alert('" + message + "');", true);
        }

        protected void btnAddEditEmailList_Click(object sender, EventArgs e)
        {
            SaveFinishOptionsUpdateAfterActive();
            if (lblErrMsg.Visible == false)
            {
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userDetails = sessionService.GetLoginUserDetailsSession();
                var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
                var listFeatures = ServiceFactory.GetService<FeatureService>();
                if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_NEWEMAILLIST).Value == 1)
                {
                    string message = AlertMessageText();
                    string Naurlstr = EncryptHelper.EncryptQuerystring("EditActiveSurveys.aspx", "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
                    if (addEmail)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "myscript", "alert('" + message + "');window.location.href='" + Naurlstr + "';", true);
                    else
                        Response.Redirect(Naurlstr);
                }
                else
                {
                    Page.RegisterStartupScript("Upgrade", @"<script>window.open('"+PathHelper.GetUpgradeLicenseURL()+"','Upgradelicense','height=300,width=600,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');</script>");
                }
            }
        }
    }
}