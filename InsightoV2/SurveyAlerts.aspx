﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true"
    CodeFile="SurveyAlerts.aspx.cs" Inherits="Insighto.Pages.SurveyAlerts" Culture="auto"
    UICulture="auto" %>

<%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" runat="Server">
 <script type="text/javascript" src="Scripts/custom.js" ></script>
 <script type="text/javascript" src="Scripts/jquery.ui.datepicker.js" ></script>
 <script type="text/javascript" src="Scripts/timepicker.js" ></script>    
    <div class="surveyQuestionPanel">
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" Visible="False" 
                    meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" Visible="False" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <!-- survey question panel -->
        <div class="surveyQuestionHeader">
            <div class="surveyQuestionTitle">
                <asp:Label ID="lblTitle" runat="server" Text="SURVEY SCHEDULER" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>                
            </div>
            <div class="previewPanel">
                <asp:Button ID="btnBack" title='Back' CssClass="backbgbtn" ToolTip="Back to Pre Launch Check"
                    runat="server" OnClick="btnBack_Click" Visible="False" Text="Back" 
                    meta:resourcekey="btnBackResource1"></asp:Button>
                &nbsp;&nbsp;<uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
            </div>
        </div>
        <div class="surveyQuestionTabPanel">
            <!-- survey question tabs -->
            <h4>
                <asp:Label ID="lblSurveyStart" runat="server" 
                    Text="When would you like to launch the survey?" 
                    meta:resourcekey="lblSurveyStartResource1"></asp:Label>
                <span> &nbsp<a href="#" class="helpLink1"
                    title="Help" onclick="javascript: window.open('help/2_4_3.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">&nbsp;</a></span></h4>
            <div>
                <p>
                    <asp:RadioButtonList ID="rbtnLaunchSchedule" ValidationGroup="a" runat="server" CssClass="rbtnLaunchSchedule"
                        meta:resourcekey="rbtnLaunchScheduleResource1">
                        <asp:ListItem Value="0" Text="On Submit" meta:resourcekey="ListItemResource1"></asp:ListItem>
                        <asp:ListItem Value="1" Text="On Specific Date" meta:resourcekey="ListItemResource2"></asp:ListItem>
                    </asp:RadioButtonList>
                </p>
                <div id="launchDateTime" class="launchDateTime" style="display: none;" runat="server">
                    <table>
                        <tr>
                            <td>
                                <table border="0" cellpadding="1" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblDate" runat="server" AssociatedControlID="txtDateTime" meta:resourcekey="lblDateResource1" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDateTime" runat="server" CssClass="textBoxDateTime txtDateTime launchdate"
                                                meta:resourcekey="txtDateTimeResource1" />
                                        </td>
                                        <td>
                                            <a id="lnkLaunchDateClear" class="cleartext" href='javascript:void(0)' title="Clear">
                                                Clear</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CustomValidator SetFocusOnError="True" ID="customLaunchDate" runat="server"
                                    ValidateEmptyText="True" ControlToValidate="txtDateTime" Display="Dynamic" CssClass="lblRequired"
                                    OnServerValidate="ValidateLaunchDate_ServerValidate" 
                                    ErrorMessage="Please select survey launch date." 
                                    meta:resourcekey="customLaunchDateResource1"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <p>
                    <asp:CheckBox ID="chkAlertOnSurveyLaunch" runat="server" Text="Send an email alert on
                    survey launch" meta:resourcekey="chkAlertOnSurveyLaunchResource1"/>
                </p>
                <p>
                    &nbsp;</p>
                <h4>
                    <asp:Label ID="lblCloseSurvey" runat="server" 
                        Text="When would you like to close the survey?" 
                        meta:resourcekey="lblCloseSurveyResource1"></asp:Label>
                    <span> &nbsp<a href="#" class="helpLink1"
                        title="Help" onclick="javascript: window.open('help/2_4_2.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">&nbsp;</a></span></h4>
                <p>
                    <asp:RadioButtonList ID="rbtnSurveyClose" runat="server" ValidationGroup="a"
                        CssClass="rbtnSurveyClose" meta:resourcekey="rbtnSurveyCloseResource1">
                        <asp:ListItem Text="I will close the survey myself" Value="2" 
                            meta:resourcekey="ListItemResource3"></asp:ListItem>
                        <asp:ListItem Text="On specific date" Value="1" 
                            meta:resourcekey="ListItemResource4"></asp:ListItem>
                    </asp:RadioButtonList>
                </p>
                <div id="surveyDateTime" class="surveyCloseDateTime" style="display: none;" runat="server">
                    <table>
                        <tr>
                            <td>
                                <table border="0" cellpadding="1" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblSurveyClosingDate" runat="server" 
                                                AssociatedControlID="txtClosingDateTime" 
                                                meta:resourcekey="lblSurveyClosingDateResource1" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtClosingDateTime" runat="server" 
                                                CssClass="textBoxDateTime txtDateTime closeDate" 
                                                meta:resourcekey="txtClosingDateTimeResource1" />
                                        </td>
                                        <td>
                                            <a id="lnkCloseDateClear" class="cleartext" href='javascript:void(0)' title="Clear">
                                                Clear</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CustomValidator SetFocusOnError="True" ID="customClosingDate" runat="server"
                                    ValidateEmptyText="True" ControlToValidate="txtClosingDateTime" Display="Dynamic"
                                    CssClass="lblRequired" OnServerValidate="ValidateCloseDate_ServerValidate" 
                                    ErrorMessage="Please select survey closing date." 
                                    meta:resourcekey="customClosingDateResource1"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbtnNoofRespondents" runat="server" 
                                CssClass="rbtnNoofRespondents" 
                                meta:resourcekey="rbtnNoofRespondentsResource1" />
                        </td>
                        <td>
                            On reaching
                        </td>
                        <td>
                            <asp:TextBox ID="txtNoofRespondents" runat="server" Text="0" MaxLength="10" CssClass="textBoxTextCount txtNoofRespondents"
                                onkeypress="javascript:return onlyNumbers(event);" onpaste="return false" 
                                meta:resourcekey="txtNoofRespondentsResource1"></asp:TextBox>
                        </td>
                        <td>
                            Responses
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left">
                            <asp:CustomValidator SetFocusOnError="True" ID="custNoofRespondents" runat="server"
                                ControlToValidate="txtNoofRespondents" ClientValidationFunction="ValidateRespondentsCount"
                                CssClass="lblRequired" Display="Dynamic" ErrorMessage="Please enter number of respondents."
                                ValidateEmptyText="True" meta:resourcekey="custNoofRespondentsResource1"></asp:CustomValidator>
                        </td>
                    </tr>
                </table>
            </div>
            <p>
                <br />
                <asp:CheckBox ID="chkAlertOnSurveyClose" runat="server"  Text="Send an email alert on
                survey close"
                    meta:resourcekey="chkAlertOnSurveyCloseResource1" />
            </p>
            <!-- //survey question tabs -->
        </div>
        <div class="clear">
        </div>
        <div class="surveyQuestionHeader">
            <!-- alert sheduler header panel -->
            <div class="surveyQuestionTitle">
                <asp:Label ID="lblAlterTitle" runat="server" Text="ALERT SCHEDULER" 
                    meta:resourcekey="lblAlterTitleResource1"></asp:Label>
                </div>
            <!-- //alert sheduler header panel -->
        </div>
        <div class="surveyQuestionTabPanel">
            <!-- survey question tabs -->
            <h4>
                <asp:Label ID="lblAlertsSurveyManager" runat="server" 
                    Text="Displays alert in survey manager page." 
                    meta:resourcekey="lblAlertsSurveyManagerResource1"></asp:Label>
                <span>&nbsp<a href="#" class="helpLink1" title="Help"
                    onclick="javascript: window.open('help/2_4_4.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">&nbsp;</a></span></h4>
            <div>
                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="20%">
                            Minimum Responses
                        </td>
                        <td width="30%">
                        <asp:TextBox ID="txtAlert1" runat="server" MaxLength="10" CssClass="textBoxSmall"
                                onkeypress="javascript:return onlyNumbers(event);" onpaste="return false" 
                                meta:resourcekey="txtAlert1Resource1"></asp:TextBox>
                        </td>                        
                         <td width="15%">
                           on Specific Date
                        </td>
                        <td width="35%">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDate1" runat="server" AssociatedControlID="txtDate1" 
                                            meta:resourcekey="lblDate1Resource1" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDate1" runat="server" 
                                            CssClass="textBoxDateTime txtDateTime alert1" 
                                            meta:resourcekey="txtDate1Resource1" />
                                    </td>
                                    <td>
                                        &nbsp; <a id="lnkAlertClear1" class="cleartext" href='javascript:void(0)' title="Clear">
                                            Clear</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                       
                    </tr>
                    <tr>
                        <td>Maximum Responses</td>
                        <td>
                        <asp:TextBox ID="txtAlert2" runat="server" MaxLength="10" CssClass="textBoxSmall"
                                onkeypress="javascript:return onlyNumbers(event);" onpaste="return false" 
                                meta:resourcekey="txtAlert2Resource1"></asp:TextBox>
                        </td>                       
                        <td>on Specific Date</td>
                        <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDate2" runat="server" AssociatedControlID="txtDate2" 
                                            meta:resourcekey="lblDate2Resource1" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDate2" runat="server" 
                                            CssClass="textBoxDateTime txtDateTime alert2" 
                                            meta:resourcekey="txtDate2Resource1" />
                                    </td>
                                    <td>
                                        &nbsp;<a id="lnkAlertClear2" class="cleartext" href='javascript:void(0)' title="Clear">Clear</a>
                                    </td>
                                </tr>
                            </table>
                        </td>                       
                    </tr>                  
                </table>
                <%--  <asp:HiddenField ID="hdnCloseDate" runat="server" />--%>
            </div>
          
            <!-- //survey question tabs -->
        </div>
        <div class="clear">
        </div>
        <div class="surveyQuestionHeader">
            <!-- alert sheduler header panel -->
            <div class="surveyQuestionTitle" style="width: 90%;">
                <asp:Label ID="lblQuestionTitle" runat="server" 
                    Text="Self Reminder - on any aspects of the Survey" 
                    meta:resourcekey="lblQuestionTitleResource1"></asp:Label></div>
            <!-- //alert sheduler header panel -->
        </div>
        <div class="clear">
        </div>
        <div style="padding-top: 8px;">
            <table width="78%" border="0">
                <tr>
                    <td>
                        Reminder &nbsp<a href="#" class="helpLink1" title="Help" onclick="javascript: window.open('help/2_4_10.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;">&nbsp;</a>
                    </td>
                    <td>
                        Reminder Date
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtRemainder" runat="server" CssClass="textArea-200-40" 
                            TextMode="MultiLine" meta:resourcekey="txtRemainderResource1"></asp:TextBox>
                    </td>
                    <td valign="top">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblDate3" runat="server" AssociatedControlID="txtRemaiderDate" 
                                        meta:resourcekey="lblDate3Resource1" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRemaiderDate" runat="server" 
                                        CssClass="textBoxDate txtRemaiderDate" 
                                        meta:resourcekey="txtRemaiderDateResource1" />
                                </td>
                                <td>
                                    <a id="lnkReminderClear" class="cleartext" href='javascript:void(0)' title="Clear">&nbsp;Clear</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
        </div>
        <div class="bottomButtonPanel">
            <asp:Button ID="btnRestoreDefault" runat="server" Text="Restore Default" CssClass="btn_Restore"
                ToolTip="Restore Default" OnClick="btnRestore_Click" 
                meta:resourcekey="btnRestoreDefaultResource1" />&nbsp;&nbsp;
            <%-- <asp:Button ID="btnSaveActive" runat="server" Text="Save Changes" CssClass="btn_medium_117" ToolTip="Save Changes" OnClick="btnSaveActive_Click" />--%>
            <asp:Button ID="btnSaveContinue" runat="server" Text="Save" CssClass="dynamicButton"
                ToolTip="Save" OnClick="btnSaveContinue_Click" OnClientClick="validatedates();"
                meta:resourcekey="btnSaveContinueResource1" />
            <%--<asp:Button ID="btnSaveEditEmail" runat="server" Text="Add/Edit Email List" CssClass="btn_medium" ToolTip="Add/Edit Email List" OnClick="btnSaveEditEmail_Click" />--%>
            <asp:HiddenField ID="hdnLaunchDate" runat="server" />
            <asp:HiddenField ID="hdnCloseDate" runat="server" />
            <asp:HiddenField ID="feaLaunch" runat="server" />
            <asp:HiddenField ID="feaClose" runat="server" />
            <input type="hidden" runat="server" id="hdnLicenseType" class="hdnLicenseType" />
        </div>
        <!-- //survey question panel -->
    </div>  

    <script type="text/javascript">
        $(document).ready(function () {
            var currentTime = new Date();
            var hours = currentTime.getHours();
            var minutes = currentTime.getMinutes();
            $('.txtDateTime').datetimepicker({
                minDate: currentTime,
                dateFormat: 'dd-M-yy',
                showOn: "button",
                buttonImage: "App_Themes/Classic/Images/icon-calander.gif",
                buttonImageOnly: true,
                buttonText: "Calendar"
                //hourMin: hours,
                //minuteMin: minutes
            });

            $(".txtRemaiderDate").datepicker({
                minDate: 0,
                showOn: "button",
                buttonImage: "App_Themes/Classic/Images/icon-calander.gif",
                buttonImageOnly: true,
                dateFormat: 'dd-M-yy',
                buttonText: "Calendar"
            });

            $('.rbtnLaunchSchedule input').change(function () {
                if ($('.hdnLicenseType').val() == "FREE") {
                    OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                    $('.rbtnLaunchSchedule').find("input[value='0']").attr("checked", "checked");
                }
                else {
                    $('.rbtnLaunchSchedule').find("input[value='0']").attr("checked", "checked");
                    if ((this).value == '1') {
                        $('.rbtnLaunchSchedule').find("input[value='1']").attr("checked", "checked");
                        $('.launchDateTime').show();
                    }
                    else {
                        $('.launchDateTime').hide();
                        $('.launchdate').val('');
                        $('.surveylaunch').html("");

                    }
                }
            });

            $('.rbtnSurveyClose input').change(function () {
                if ((this).value == '2') {
                    $('.surveyCloseDateTime').hide();
                    $('.closeDate').val('');
                    $('.rbtnNoofRespondents input').attr('checked', false);
                    $(".txtNoofRespondents").val('0');
                    $(".txtNoofRespondents").attr("disabled", "disabled");
                }
                else if ((this).value == '1') {
                    $('.surveyCloseDateTime').show();
                    $('.rbtnNoofRespondents input').attr('checked', false);
                    $(".txtNoofRespondents").val('0');
                    $(".txtNoofRespondents").attr("disabled", "disabled");
                }
                else {
                    $('.surveyCloseDateTime').hide();
                    $('.closeDate').val('');
                    OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                    $('.rbtnSurveyClose').find("input[value='2']").attr("checked", "checked");
                    $('.rbtnNoofRespondents input').attr('checked', false);
                    $(".txtNoofRespondents").val('0');
                    $(".txtNoofRespondents").attr("disabled", "disabled");

                }
            });
            $('.rbtnNoofRespondents').click(function () {
                $(".txtNoofRespondents").removeAttr("disabled");
                //$(".txtNoofRespondents").val('');
                //$(".txtNoofRespondents").focus();
                $('.surveyCloseDateTime').hide();
                $('.rbtnSurveyClose input').each(function () {
                    (this).checked = false;
                });
            });
            if ($('.hdnLicenseType').val() == " ") {
                $('.ui-datepicker-trigger').hide();
                $('.cleartext').hide();
            }
        });
        //        function ValidateLaunchDate(sender, args) {
        //            var launchDate = $('.txtDateTime').val();
        //            al(launchDate);
        //            if ($('.rbtnLaunchSchedule input:checked').val() == "1" && launchDate.lenght <= 0) {
        //                args.IsValid = false;
        //            }
        //            else {
        //                args.IsValid = true;
        //            }
        //        }
        function ValidateRespondentsCount(sender, args) {
            if ($('.rbtnNoofRespondents input[type=radio]').is(':checked')) {
                if ($('.txtNoofRespondents').val() == '') {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            }
        }
        function onlyNumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            //            if (type == 'ResponseCount') {
            //                if (key > 31 && (key < 49 || key > 57)) {
            //                    return false;
            //                }
            //            }
            //            else {
            if (key > 31 && (key < 48 || key > 57)) {
                return false;
            }
            //}
            return true;
        }
        $('#lnkLaunchDateClear').click(function () {
            $('.launchdate').val('');
        });
        $('#lnkCloseDateClear').click(function () {
            $('.closeDate').val('');
        });
        $('#lnkReminderClear').click(function () {
            $('.txtRemaiderDate').val('');
        });
        $('#lnkAlertClear2').click(function () {
            $('.alert2').val('');
        });
        $('#lnkAlertClear1').click(function () {
            $('.alert1').val('');
        });      
    </script>
</asp:Content>
