﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using System.Threading;
using System.Configuration;
using App_Code;
using Insighto.Business.ValueObjects;

namespace Insighto.Pages
{
    public partial class ContactList : BasePage
    {
        Hashtable ht = new Hashtable();
        public string Key
        {
            get
            {
                return HttpUtility.UrlEncode(QueryStringHelper.GetString("Key"));
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            //if (loggedInUserInfo.LicenseType == "FREE")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            //}
            //else
            //{


                lblCreate.Visible = true;
                if (Request.QueryString["key"] != null)
                {
                    int contactId = 0;

                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                    {
                        contactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                        lblCreate.Visible = false;
                        lblEdit.Visible = true;
                    }
                    if (!IsPostBack)
                    {
                        lblSuccessMsg.Text = "";
                        var contactService = ServiceFactory.GetService<ContactListService>();
                        var contacts = contactService.FindContactsByContactId(contactId);
                        if (contacts != null)
                        {
                            txtListName.Text = contacts.CONTACTLIST_NAME;
                        }


                    }

                }
          //  }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
           
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userInfo = sessionService.GetLoginUserDetailsSession();
                if (userInfo != null)
                {
                    var contactService = ServiceFactory.GetService<ContactListService>();
                    if (Request.QueryString["key"] != null && ht.Contains("ContactId"))
                    {
                        int contactId = 0;
                        ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                        if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                        {
                            contactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                        }


                        var isUpdated = contactService.UpdateContactList(contactId, userInfo.UserId, txtListName.Text.Trim());
                        if (isUpdated)
                        {
                            dvSuccessMsg.Visible = true;
                            dvErrMsg.Visible = false;
                            lblSuccessMsg.Text = Utilities.ResourceMessage("ContactList_Success_Update"); //"List name updated successfully.";
                            lblSuccessMsg.Visible = true;
                            lblErrMsg.Visible = false;
                            base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring(PathHelper.GetAddressBookURL().Replace("~/", ""), "surveyFlag=" + SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                        }
                        else
                        {
                            dvErrMsg.Visible = true;
                            lblErrMsg.Visible = true;
                            lblSuccessMsg.Visible = false;
                            dvSuccessMsg.Visible = false;
                        }


                    }
                    else
                    {

                        var contactList = contactService.SaveEmailContactList(userInfo.UserId, txtListName.Text.Trim());
                        if (contactList != null)
                        {
                            if (contactList.CONTACTLIST_ID > 0)
                            {
                                dvSuccessMsg.Visible = true;
                                //lblSuccessMsg.Text = " Contact list created successfully.";    
                                lblSuccessMsg.Text = Utilities.ResourceMessage("ContactList_Success_Create"); //" Email list created successfully.";
                                lblSuccessMsg.Visible = true;
                                dvErrMsg.Visible = false;
                                Session["ContactId"] = contactList.CONTACTLIST_ID.ToString();
                                lblErrMsg.Visible = false;
                                base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("CreateEmailList.aspx", "ContactId=" + contactList.CONTACTLIST_ID + "&surveyFlag=" + SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                                base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("CreateEmailList.aspx", "ContactId=" + contactList.CONTACTLIST_ID + "&surveyFlag=" + SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);

                            }
                        }
                        else
                        {
                            dvSuccessMsg.Visible = false;
                            lblSuccessMsg.Visible = false;
                            dvErrMsg.Visible = true;
                            lblErrMsg.Visible = true;
                            lblSuccessMsg.Visible = false;
                        }
                    }

                }
                else
                {

                }
            }
        }
    }
