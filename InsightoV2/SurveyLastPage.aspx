﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SurveyLastPage.aspx.cs" Inherits="SurveyLastPage"
    MasterPageFile="~/SurveyRespondent.master" %>

<%@ Register Src="~/UserControls/RespondentControls/RespondentPopupHeader.ascx" TagName="RespondentPopupHeader"
    TagPrefix="uc" %>
<asp:Content ContentPlaceHolderID="ContentHead" runat="server" ID="headerPlaceHolder">
    <style type="text/css">
        body
        {
            font: normal 12px Arial, Helvetica, sans-serif;
            color: #424242;
        }
        .bannerLastpnl
        {
            width: 90%;
            margin: 20px auto 20px auto;
        }
        .surverylastpagebdr
        {
            border: solid 0px #dcdcdc; /* padding:2% */
        }
        
        .ltbanner_img
        {
            width: 100%;
        }
        .banner_content
        {
            border: solid 1px #B92616;
            font-size: 16px;
            text-align: center;
            width: 100%;
            font-weight: bold;
            margin: auto;
        }
        .bannecontPadding
        {
            padding: 5px;
        }
        .surveyspce
        {
            height: 18px;
        }
        .bttm_banner
        {
            text-align: right;
            width: 100%;
            padding-top: 15px;
        }
        .productimgwidth
        {
            width: 100%;
        }
        .maroon
        {
            color: #800000;
            font-weight: bold;
        }
        .NasscomlogoView
        {
            margin: 3px 0 0 3px;
            font-size: 16px;
        }
        .nasspc_lbl
        {
            padding-left: 5%;
        }
      
    </style>
    <style type="text/css">
 
a:hover 
{
    color:Green;
}

    </style>

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPopupHeader" runat="server" ID="popupHeaderPlaceHolder">
    <uc:RespondentPopupHeader ID="ucRespondentPopupHeader" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentMain" runat="server" ID="Content1">
    <div id="divSurveyLastPage" runat="server" style="display: none;">
        <div class="questionViewpanel">
            <div class="sampleLogoHeader">
                <asp:Literal runat="server" ID="ltlLogo" />
            </div>
            <h2 class="surveyMessage divThankyouDefault" id="defaultthankyoumessage" runat="server">
                <span>
                    <asp:Literal runat="server" ID="ltlMessage" Visible="false" />
                </span>
            </h2>
            <h2 class="surveyMessageTextBox" id="customthankyoumessage" runat="server" >
                <span>
                    <asp:Literal runat="server" ID="ltlMessageCustom" />
                </span>
            </h2>
            <br />
            <br />
            <div style="width:100%;text-align:center;margin:0 auto;">
                <asp:Image ID="customimage1" runat="server" Visible="false" ImageUrl="~/images/Insighto_logo.png"/>
            </div>
             <br />
             <br />
             <div class="pwdbylogopading">
              <a  href="<%=airtimeurl %>"   style="text-decoration:underline;font-size:24px;font-weight: 700;text-align:center"><%=airtimeurl %></a>
              </div>
             <br />
           
            
            <asp:Literal runat="server" ID="ltlContent" />
        </div>
    </div>
    <div class="informationPanelDefault" id="divRedirectMsg" runat="server" style="display: none;">
        <div>
            <asp:Label ID="lblRedirectMsg" runat="server" Visible="true" />
        </div>
    </div>

     <div id="partnerimg" runat="server"></div>

   <script type="text/javascript">

       var _gaq = _gaq || [];
       _gaq.push(['_setAccount', 'UA-29026379-1']);
       _gaq.push(['_setDomainName', 'insighto.com']);
       _gaq.push(['_setAllowLinker', true]);
       _gaq.push(['_trackPageview']);

       (function () {
           var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
           ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
           var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
       })();

</script>

    <script type="text/javascript" src="Scripts/mobilethankyou.js"></script>
</asp:Content>
