﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Services;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Services;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using System.Configuration;
using App_Code;
using Insighto.Business.Enumerations;

namespace Insighto.Pages
{
    public partial class ViewFolderPopUp : BasePage
    {
      
        Hashtable ht = new Hashtable();
        private string _FolderId = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFoldersList();
                lblMoveSurveys.Text = String.Format(Utilities.ResourceMessage("lblMoveSurveys"), SurveyMode);
            }
        }    
       

      

        public string FolderId
        {
            get
            {
                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains("FolderId"))
                    {
                        _FolderId = ht["FolderId"].ToString();
                    }
                }
                return _FolderId;
            }
            set
            {

            }
        }
     

        protected void BindFoldersList()
        {
            FolderService folder = new FolderService();
            var objFolder = new osm_cl_folders();

            var sessionState = ServiceFactory.GetService<SessionStateService>();
            var loginUser = sessionState.GetLoginUserDetailsSession();

            if (loginUser != null)
            {
                objFolder.USERID = loginUser.UserId;
              
                var foldersList = folder.GetFolders(objFolder);
                if (foldersList.Count > 0)
                {
                    ddlFolders.DataSource = foldersList;
                    ddlFolders.DataTextField = Constants.FolderName;//"FOLDER_NAME";
                    ddlFolders.DataValueField = Constants.FolderId;//"FOLDER_ID";
                    ddlFolders.DataBind();
                    ddlFolders.Items.Insert(0, new ListItem(Constants.SELECT, "0"));
                    if (Request.QueryString["key"] != null)
                    {

                        ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                        if (ht != null && ht.Count > 0 && ht.Contains("FolderId"))
                        {
                            //ddlFolders.SelectedValue = ht["FolderId"].ToString();
                            var folderDetails = foldersList.Where(f => f.FOLDER_ID == ValidationHelper.GetInteger(ht["FolderId"].ToString(), 0)).SingleOrDefault();
                            lblTitle.Text = folderDetails.FOLDER_NAME;
                        }
                    }    
                }             

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlFolders_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {

                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);

                if (ddlFolders.SelectedIndex < 1)
                {
                    dvSuccessMsg.Visible = false;
                    dvErrMsg.Visible = true;
                    lblErrMsg.Text = Utilities.ResourceMessage("errSelectFolder");//"Please select a folder to move " + SurveyMode.ToLower() + "(s).";
                    lblErrMsg.Visible = true;
                    lblSuccessMsg.Visible = false;
                    hdntxtValue.Value = "";
                }
                else if (ht != null && ht.Count > 0 && ht.Contains("FolderId"))
                {
                    if (hdntxtValue.Value == null || hdntxtValue.Value == "")
                    {

                        lblErrMsg.Text = String.Format(Utilities.ResourceMessage("errSelectSurvey"), SurveyMode.ToLower()); //"Please select atleast one " + SurveyMode.ToLower() + " to move to another folder.";
                        dvSuccessMsg.Visible = false;
                        dvErrMsg.Visible = true;
                        lblErrMsg.Visible = true;
                        lblSuccessMsg.Visible = false;
                        hdntxtValue.Value = "";
                        ddlFolders.SelectedIndex = 0;

                    }
                    else
                    {
                        if (ddlFolders.SelectedValue == ht["FolderId"].ToString())
                        {
                            lblErrMsg.Text = Utilities.ResourceMessage("errSourceDestination");//"Source & destination folders are same, please choose another Folder.";
                            dvSuccessMsg.Visible = false;
                            dvErrMsg.Visible = true;
                            lblErrMsg.Visible = true;
                            lblSuccessMsg.Visible = false;
                            hdntxtValue.Value = "";
                        }

                        else
                        {
                            if (hdntxtValue.Value != null || hdntxtValue.Value != "")
                            {
                                string selectedSurveys = hdntxtValue.Value;
                                List<osm_survey> survey = new List<osm_survey>();
                                //int USERID =1814; //temp code need to delete
                                var sessionState = ServiceFactory.GetService<SessionStateService>();
                                var loginUser = sessionState.GetLoginUserDetailsSession();
                                if (selectedSurveys != "" && loginUser != null)
                                {
                                    osm_survey objSurvey = new osm_survey();
                                    SurveyService surveyService = new SurveyService();
                                    string[] surveyList = selectedSurveys.Split(',');
                                    if (surveyList.Length > 0)
                                    {
                                        int i = 0;
                                        for (i = 0; i < surveyList.Length; i++)
                                        {
                                            objSurvey.SURVEY_ID =ValidationHelper.GetInteger(surveyList[i],0);
                                            objSurvey.USERID = loginUser.UserId;
                                            objSurvey.FOLDER_ID = Convert.ToInt32(ddlFolders.SelectedValue);
                                            survey.Add(objSurvey);
                                            surveyService.ChangeSurveyFolder(survey);
                                        }
                                        lblSuccessMsg.Text = String.Format(Utilities.ResourceMessage("successMsg"), i, ddlFolders.SelectedItem.Text, SurveyMode.ToLower());
                                    }

                                    hdntxtValue.Value = "";
                                    lblSuccessMsg.Visible = true;
                                    dvSuccessMsg.Visible = true;
                                    dvErrMsg.Visible = false;
                                    lblErrMsg.Visible = false;
                                    ddlFolders.SelectedIndex = 0;
                                    base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("ManageFolders.aspx", "surveyFlag=" + SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);

                                }


                            }
                        }
                    }
                }
               
            }

            
            
        }

        protected void btnMoveSurveys_Click(object sender, EventArgs e)
        {
            
        }
    }
}