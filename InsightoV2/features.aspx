﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true" CodeFile="features.aspx.cs" Inherits="partnersfeaturesall" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Price plan - Insighto</title>
 <link href="../App_Themes/Classic/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="wrapper">
	
    <div class="priceplan_blk">
        	<div class="view_features" align="center">
        		
                <table width="95%" border="0" cellspacing="0" cellpadding="0"  align="center">
                  <tr>
                    <th width="35%"><h3 class="title">Detailed <br /> features</h3></th>
                    <th width="20%">
                        <div class="price_tag_header" id="basicprice_tag_header" runat="server">                   	
            				<h3>BASIC</h3>
                           <%-- <div class="price_tag_free">Free <br /> for<br /> LIFE</div>--%>
                           <div class="price">
                                    <asp:Label ID="lblfree" runat="server" Text="FREE" CssClass="freetextfeature" ></asp:Label></div>
                                    <asp:Label ID="Label1" runat="server" Text="for life" CssClass="freetextfeature1"  ></asp:Label>
                        </div>
                       
                    </th>
                    <th width="20%">
                        <div class="price_tag_header green" id="proprice_tag_header" runat="server">                   	
            				<h3>Pro</h3>
                             <div class="strike_txt" id="divpro_strike" runat="server" visible="false">
                             <div class="strike_bg" id="divpro_strikebg" runat="server"></div>
                         <div class="price"> <asp:Label ID="lblprostrike" runat="server" ></asp:Label></div>   
                             </div>
                        <div class="price"><asp:Label ID="lblpro" runat="server"  
                                     ></asp:Label></div>
                            per annum
                        </div>
                      
                    </th>
                    <th width="20%">
                        <div class="price_tag_header orange" id="premiumprice_tag_header" runat="server">                        	
            				<h3>Premium</h3>
                            <div class="strike_txt" id="divpremium_strike" runat="server" visible="false">
                            <div class="strike_bg" id="divpremium_strikebg" runat="server" ></div>
                           <div class="price"><asp:Label ID="lblpremstrike" runat="server" ></asp:Label></div> 
                           </div>
                            <div class="price"><asp:Label ID="lblpremium" runat="server" ></asp:Label></div>
                            per annum
                        </div>
                        
                    </th>
                  </tr>
                </table>

                <table width="95%" border="0" cellspacing="0" cellpadding="0"  align="center">
                  <tr>
                    <td width="35%" border="0" ></td>
                    <td width="20%" border="0">
                        <asp:Button ID="button" runat="server" CssClass="btn_bg" 
                            Text="Sign Up" onclick="button_Click" />
                    </td>
                    <td width="20%" border="0">
                        <asp:Button ID="btnpro1" Text="Buy Now" runat="server" CssClass="btn_bg" onclick="btnpro1_Click"
                                    />
                    </td>
                    <td width="20%" border="0">
                        <asp:Button ID="btnpremium1" Text="Buy Now" runat="server" CssClass="btn_bg" onclick="btnpremium1_Click" 
                                    />
                    </td>
                  </tr>
                </table>
           <div id="dvPageInfo" class="dvPageInfo" runat="server"></div>
           <table width="95%" border="0" cellspacing="0" cellpadding="0"  align="center">
                  <tr>
                    <td width="35%"></td>
                    <td width="20%">
                        <asp:Button ID="button1" runat="server" CssClass="btn_bg" 
                            Text="Sign Up" onclick="button1_Click" />
                    </td>
                    <td width="20%">
                        <asp:Button ID="btnpro2" Text="Buy Now" runat="server" CssClass="btn_bg" onclick="btnpro2_Click" 
                                    />
                    </td>
                    <td width="20%">
                        <asp:Button ID="btnpremium2" Text="Buy Now" runat="server" CssClass="btn_bg" onclick="btnpremium2_Click" 
                                    />
                    </td>
                  </tr>
                </table>

        	</div>
        

    
  </div>
<div>&nbsp;</div>
  

</body>
</html>

    <script language="javascript" type="text/javascript">
        function showHide(EL, PM) {
            ELpntr = document.getElementById(EL);
            if (ELpntr.style.display == 'none') {
                document.getElementById(PM).innerHTML = "-";
                ELpntr.style.display = 'block';
            }
            else {
                document.getElementById(PM).innerHTML = "+";
                ELpntr.style.display = 'none';
            }
        }
        function RedirectToLoginPage() {
            window.location.href = "../Home.aspx";
            return false;
        }

        function showHideFAQPanel() {
            divContent = document.getElementById('divFaqContent');
            if (divContent.style.display == 'none') {
                // document.getElementById(PM).innerHTML = "-";
                divContent.style.display = 'block';
            }
            else {
                //document.getElementById(PM).innerHTML = "+";
                divContent.style.display = 'none';
            }
        }
        
    </script>

    <!--Start of Zopim Live Chat Script-->

<script type="text/javascript">

    window.$zopim || (function (d, s) {
        var z = $zopim = function (c) { z._.push(c) }, $ = z.s =

d.createElement(s), e = d.getElementsByTagName(s)[0]; z.set = function (o) {
    z.set.

_.push(o)
}; z._ = []; z.set._ = []; $.async = !0; $.setAttribute('charset', 'utf-8');

        $.src = '//cdn.zopim.com/?bYM7XxoDiH1Gv57aQNwmeiatKZbOCdA7'; z.t = +new Date; $.

type = 'text/javascript'; e.parentNode.insertBefore($, e)
    })(document, 'script');

</script>

<!--End of Zopim Live Chat Script-->

    </div>

</asp:Content>

