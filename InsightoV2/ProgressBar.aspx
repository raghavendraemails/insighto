﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true"
    CodeFile="ProgressBar.aspx.cs" Inherits="ProgressBar"%>

<%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" runat="Server">
    <div class="surveyQuestionPanel">
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1" /></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" CssClass="errorMesg" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <!-- survey question panel -->
        <div class="surveyQuestionHeader">
            <div class="surveyQuestionTitle">
                <asp:Label ID="lblTitle" runat="server" Text="Choose Progress Bar" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>
                </div>
            <div class="previewPanel">
                <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
            </div>
        </div>
        <div class="surveyQuestionSubHeader">
            <!-- sub header -->
            <asp:CheckBox ID="chkShowProgresBar" runat="server" 
                Text="Show progress bar for respondents" 
                meta:resourcekey="chkShowProgresBarResource1" />&nbsp;&nbsp;<asp:CheckBox 
                ID="chkProgressBarText" runat="server" Text=" Show progress bar text" 
                meta:resourcekey="chkProgressBarTextResource1" />&nbsp;&nbsp;<asp:CheckBox 
                ID="chkQM" runat="server" Text="Show Questions with asterisk (*)" 
                meta:resourcekey="chkQMTextResource1" Checked />
           

            <!-- //sub header -->
        </div>
        <div class="surveyQuestionTabPanel">
            <!-- survey question tabs -->
          
            <div class="con_login_pnl">
                <div class="loginlbl"><asp:Label ID="lblProgessBar" runat="server" 
                        Text=" Progress bar text:" meta:resourcekey="lblProgessBarResource1"></asp:Label>
                   </div>
                
                <div class="loginControls">
                    <asp:TextBox ID="txtProgressBar" MaxLength="20" CssClass="textBoxLogin" 
                        runat="server" meta:resourcekey="txtProgressBarResource1"></asp:TextBox>
                    <br />
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl"></div>
                <div class="loginControls">&nbsp;</div>
            </div> 
            <div class="clear">
            </div>
            
                <asp:ListView ID="lstProgressbarCategory" runat="server" GroupItemCount="5" ItemPlaceholderID="itemPlaceHolder"
            GroupPlaceholderID="groupPlaceHolder">
            <GroupTemplate>
                <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
            </GroupTemplate>
            <LayoutTemplate>
                <asp:PlaceHolder ID="groupPlaceholder" runat="server" />
            </LayoutTemplate>
            <ItemTemplate>
                <div class="surveyQuestionHeader">
                    <!-- classic theme header -->
                    <div class="surveyQuestionTitle">
                        <%# Eval("PROGRESSBAR_CATEGORY")%></div>
                    <div class="previewPanel">
                        &nbsp;</div>
                    <!-- //classic theme header -->
                </div>
               
                <asp:ListView ID="lstProgressbar" runat="server" DataSource='<%# Eval("osm_progressbar") %>'
                    OnItemDataBound="lstProgressbar_ItemDataBound">
                    <ItemTemplate>
                        <div class="progressbarThumb theme_mn">
                            <p>
                                <input type="radio"  runat="server" id="rbtnProgressbar"  value ='<%# Eval("PROGRESSBAR_NAME") %>'/>
                            &nbsp;</p>
                            <asp:Image ID="imgProgressBar" runat="server" 
                                meta:resourcekey="imgProgressBarResource1" />
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                <p>
                    &nbsp;</p>
                <div class="clear">
                </div>
            </ItemTemplate>
        </asp:ListView>
           
             <div class="clear">
        </div>
        <div class="bottomButtonPanel">
            <asp:Button ID="btnSave" runat="server" Text="Save" ToolTip="Save"
                CssClass="dynamicButton" onclick="btnSave_Click" 
                meta:resourcekey="btnSaveResource1"  />
        </div>
        </div>
        <script language="javascript" type="text/javascript">


            function SetSingleRadioButton(nameregex, current) {
                re = new RegExp(nameregex);
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    elm = document.forms[0].elements[i];
                    if (elm.type == 'radio') {
                        if (elm != current) {
                            elm.checked = false;
                        }
                    }
                }
                current.checked = true;
            }


</script>
</div>
</asp:Content>
