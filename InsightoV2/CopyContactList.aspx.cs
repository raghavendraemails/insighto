﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using System.Collections;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using System.Text.RegularExpressions;
namespace Insigto.Pages
{
    public partial class CopyContactList : BasePage
    {
        private string _errMsg = string.Empty;
        static string ErrorMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected override void OnInit(EventArgs e)
        {
            divValidators.Controls.Add(GetCustomValidator());
            
            divValidators.Controls.Add(GetCustomValidator2());
            base.OnInit(e);
            
        }

        public string ErrMsg
        {
            get {
                return _errMsg;
            }
            set { _errMsg = value; }
        }

        protected void cvMoss_ServerValidate(object source, ServerValidateEventArgs args)
        {
            lblDuplicateEmails.Text = "";
            ErrorMsg = "";
            if (txtMultipleEmails.Text.Trim() != "")
            {
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userInfo = sessionService.GetLoginUserDetailsSession();
                var emailService = ServiceFactory.GetService<EmailService>();
                int ContactId = 0;
                string emails = txtMultipleEmails.Text.Trim();
                string[] emailList = emails.Split(',');
                var osmemail = new osm_emaillist();
                Regex reLenient = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                string dupEmails = "";
                
                bool flag = true;
                if (Request.QueryString["key"] != null)
                {                    
                    Hashtable ht = new Hashtable();
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                    {
                        ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                        foreach (string email in emailList)
                        {
                            if (email.Length <= 50)
                            {
                                bool isEmail = reLenient.IsMatch(email);
                                if (!isEmail)
                                {
                                    hdnError.Value = "List contains invalid email id's.";
                                    flag = false;
                                    break;
                                } 
                            }
                            else
                            {
                                flag = false;
                                hdnError.Value = "List contains invalid email id's.";
                                break;
                            }
                        }
                        
                    }                   
                }

                if (flag)
                {
                    hdnError.Value = "";
                }
                args.IsValid = flag;
                
            }
        }



        protected void cvMoss_ServerValidate1(object source, ServerValidateEventArgs args)
        {
            lblDuplicateEmails.Text = "";
            ErrorMsg = "";
            if (txtMultipleEmails.Text.Trim() != "" && hdnError.Value =="")
            {
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userInfo = sessionService.GetLoginUserDetailsSession();
                var emailService = ServiceFactory.GetService<EmailService>();
                int ContactId = 0;
                string emails = txtMultipleEmails.Text.Trim();
                string[] emailList = emails.Split(',');
                var osmemail = new osm_emaillist();
                Regex reLenient = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                string dupEmails = "";

                bool flag = true;
                if (Request.QueryString["key"] != null )
                {
                    Hashtable ht = new Hashtable();
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                    {
                        ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                       
                            foreach (string email in emailList)
                            {

                                if (email.Length <= 50)
                                {
                                    int count = 0;
                                    foreach (string tempemail in emailList)
                                    {
                                        if (email == tempemail)
                                        {
                                            if (count > 0)
                                            {
                                                string[] dupList = dupEmails.Split(',');
                                                int cnt = 0;
                                                foreach (string dup in dupList)
                                                {
                                                    if (dup == email)
                                                    {
                                                        cnt++;
                                                    }

                                                }
                                                if (cnt < 1)
                                                    dupEmails += email + ",";
                                            }
                                            count++;
                                        }
                                    }

                                }
                                else
                                {
                                    flag = false;
                                    hdnError.Value = "List contains invalid email id's.";
                                    break;
                                }
                            
                            }
                    }
                }
                if (dupEmails != "")
                {
                    flag = false;
                    lblDuplicateEmails.Text = dupEmails;
                    hdnError.Value = "List contains duplicate email id's";

                }
                else
                {
                    ErrorMsg = "";
                }

                args.IsValid = flag;

            }
        }


       private CustomValidator GetCustomValidator()
        {
         
            var revTemp = new CustomValidator
                              {
                                  CssClass = "lblRequired",
                                  ID = "reqFormat",
                                  ErrorMessage =  "List Contains Invalid email ids",
                                  Display = ValidatorDisplay.Dynamic,
                                  Visible = true
                              };
            revTemp.ServerValidate += cvMoss_ServerValidate;
           
            return revTemp;
        }

       private CustomValidator GetCustomValidator2()
       {

           var revTemp = new CustomValidator
           {
               CssClass = "lblRequired",
               ID = "reqFormat1",
               ErrorMessage = "List Contains Duplicate Email ids",
               Display = ValidatorDisplay.Dynamic,
               Visible = true
           };
           revTemp.ServerValidate += cvMoss_ServerValidate1;

           return revTemp;
       }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (txtMultipleEmails.Text.Trim() != "")
                {
                    var sessionService = ServiceFactory.GetService<SessionStateService>();
                    var userInfo = sessionService.GetLoginUserDetailsSession();
                    var emailService = ServiceFactory.GetService<EmailService>();
                    int ContactId = 0;
                    string emails = txtMultipleEmails.Text.Trim();
                    string[] emailList = emails.Split(',');
                    var osmemail = new osm_emaillist();
                    string patternLenient = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Regex reLenient = new Regex(patternLenient);
                  
                 
                    if (Request.QueryString["key"] != null)
                    {
                        Hashtable ht = new Hashtable();
                        ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                        if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                        {
                            ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                            foreach (string email in emailList)
                            {
                                if (email.Length <= 50)
                                {
                                    bool isEmail = reLenient.IsMatch(email);
                                    if (isEmail)
                                    {
                                        osmemail.CONTACTLIST_ID = ValidationHelper.GetInteger(ContactId, 0);
                                        osmemail.DELETED = 0;
                                        osmemail.EMAIL_ADDRESS = email;
                                        osmemail.CREATED_BY = userInfo.UserId;
                                        osmemail.STATUS = "Active";
                                        osmemail.LAST_MODIFIED_BY = userInfo.UserId;
                                        osmemail.MODIFIED_ON = DateTime.Now;
                                        var rtn = emailService.SaveEmailsByList(osmemail);
                                    }
                                }
                            }
                            lblSuccessMsg.Visible = true;
                            dvSuccessMsg.Visible = true;
                            dvErrMsg.Visible = false;

                        }
                    }
                }
            }

             
                 
        }
    }
}