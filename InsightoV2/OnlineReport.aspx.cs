﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts.Web;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using sharp = iTextSharp.text;
using FeatureTypes;
using Insighto.Business.ValueObjects;
using System.Text;
using System.Linq;
using System.Threading;
using DevExpress.Web.ASPxClasses;
using System.Diagnostics;
using InfoSoftGlobal;


public partial class OnlineReport : SurveyPageBase
{

    #region "Variables"
    /// <summary>
    /// class level variables.
    /// </summary>
   
    public int surveyID = 0;
    //int QuesSeq = 0;
    SurveyCore surcore = new SurveyCore();
    SurveyCoreVoice surcorevoice = new SurveyCoreVoice();
    Hashtable ht = new Hashtable();
    //string ViewAllQues = "";
    DateTime dt1 = new DateTime();
    DateTime dt2 = new DateTime();
    public string exportchartfilehandler;
    public string jsstrchartname;
    DataTable colgroupoptions123413;
    #endregion
    int pageno;
    int seqint;
   
    protected void Page_Load(object sender, EventArgs e)
    {


        exportchartfilehandler = ConfigurationSettings.AppSettings["exportchartfile"].ToString();
       // BindReport(ht, ViewAllQues, dt1, dt2);
        if (!IsPostBack)
        {
            pageno = 0;
            if (Session["pageno"] == null)
            {
                Session["pageno"] = 0;
            }
            else
            {
                Session["pageno"] = Session["pageno"];
            }

            if (pageno == 0)
            {
                
                btnprev.Enabled = false;
            }
            else
            {
                btnprev.Enabled = true;
            }
            Session["datatablechart"] = null;
             
        }
     
                surveyID = Convert.ToInt32(Request.QueryString["surveyid"]);
                DateTime start = dt1.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt1;
                DateTime End = dt2.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt2;

                DataSet dssurcorevoice = surcore.voicedataset();
                if (dssurcorevoice.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
                {
                    SelectedSurvey = surcorevoice.GetSurveyWithResponsesCount(surveyID, start, End);
                }
                else
                {
                    // SelectedSurvey = surcore.GetSurveyWithResponsesCount(surveyID, start, End);
                    SelectedSurvey = surcore.GetSurveyWithResponsesOnlineReport(surveyID, start, End);
                }
                if (!IsPostBack)
                {
                    lbldisplay.Text = "1" + " of " + SelectedSurvey.surveyQues.Count;
                    BindReport(dt1, dt2);
                }

                lblsurveyname.Text = "SurveyName: " +  SelectedSurvey.SURVEY_NAME;



                var logoSplit = SelectedSurvey.SURVEY_LOGO.Split(new[] { '<' }, StringSplitOptions.RemoveEmptyEntries);
            var url = "~/" + logoSplit[0].Replace(">", string.Empty);
            var alignment = logoSplit.Length > 1 ? logoSplit[2].Replace(">", string.Empty) : string.Empty;
            imgLogo.ImageUrl = url;

        }

    private void BindReport( DateTime dt1, DateTime dt2)
    {
         seqint = Convert.ToInt32(Session["pageno"].ToString());

        //if (seqint < SelectedSurvey.surveyQues.Count) 
        //{
        //    seqint = Convert.ToInt32(Session["pageno"].ToString());
        //}
        //else if (seqint >= SelectedSurvey.surveyQues.Count)
        //{
        //    seqint = SelectedSurvey.surveyQues.Count - 1;
        //    Session["pageno"] = seqint;
        //    Session["datatablechart"] = "chartready";
        //}
        //else if (seqint == 0)
        //{
        //    seqint = 0;
        //    Session["pageno"] = seqint;
        //}
       

        //if ((seqint >= 0) && (seqint < SelectedSurvey.surveyQues.Count) )
        //{
            
                HtmlTable qt3tableques = new HtmlTable();
                qt3tableques.CellPadding = 1;
                qt3tableques.CellSpacing = 0;
               // qt3tableques.Width = "600";
        qt3tableques.Width = "100%";
        

                HtmlTableRow qt3row = new HtmlTableRow();
                qt3row.BgColor = "#bf92cb";
                HtmlTableCell qt3cell = new HtmlTableCell();
                qt3cell.VAlign = "Top";
                qt3cell.Height = "30px";
                // qt3cell.Width = "3%";                
                qt3cell.Align = "Center";
               
                System.Web.UI.WebControls.Label lblqt3cell = new System.Web.UI.WebControls.Label();
                lblqt3cell.Font.Name = "Arial, Helvetica, sans-serif";
                lblqt3cell.Text =  SelectedSurvey.surveyQues[seqint].QUESTION_SEQ + ".";
                lblqt3cell.ForeColor = System.Drawing.Color.White;
                lblqt3cell.Font.Bold = true;


                HtmlTableCell qt3cell1 = new HtmlTableCell();
                qt3cell1.VAlign = "Top";
                qt3cell1.Height = "30px";
                //  qt3cell1.Width = "97%";
                qt3cell1.Align = "left";
               
                System.Web.UI.WebControls.Label lblqt3cell1 = new System.Web.UI.WebControls.Label();
                lblqt3cell1.Font.Name = "Arial, Helvetica, sans-serif";
                lblqt3cell1.Text = StripTagsRegex(SelectedSurvey.surveyQues[seqint].QUESTION_LABEL);

                lblqt3cell1.ForeColor = System.Drawing.Color.White;
                lblqt3cell1.Font.Bold = true;

                qt3cell.Controls.Add(lblqt3cell);
                qt3cell1.Controls.Add(lblqt3cell1);
                qt3row.Cells.Add(qt3cell);
                qt3row.Cells.Add(qt3cell1);

                qt3tableques.Rows.Add(qt3row);
                rpnl_reportsdata.Controls.Add(qt3tableques);



                HtmlTable qt3table = new HtmlTable();
                qt3table.Border = 0;

                qt3table.CellPadding = 1;
                qt3table.CellSpacing = 0;
               // qt3table.Width = "600";
                qt3table.Width = "100%";
                if (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 1 || SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 2 || SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 3 || SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 4 )
                {

                    jsstrchartname = "chrtobo" + seqint;
                    
                    colgroupoptions123413 = createDataTableQ123413();


                    DataSet dsansoptions = surcore.getAnsweroptionsQID(SelectedSurvey.surveyQues[seqint].QUESTION_ID);
                    
                    for (int rans = 0; rans < dsansoptions.Tables[1].Rows.Count; rans++)
                    {
                        HtmlTableRow qt3rowans1 = new HtmlTableRow();

                        qt3rowans1.Height = "25px";
                        //  qt3rowans1.BgColor = "lightgray";
                        if (rans % 2 == 1)
                        {
                            qt3rowans1.BgColor = "#F4F4F4";
                            qt3rowans1.BorderColor = "#F4F4F4";
                        }
                        else
                        {
                            qt3rowans1.BgColor = "white";
                            qt3rowans1.BorderColor = "white";
                        }

                        HtmlTableCell qt3cellans1 = new HtmlTableCell();
                        qt3cellans1.Align = "left";
                        System.Web.UI.WebControls.Label lblqt3cellans1 = new System.Web.UI.WebControls.Label();
                        lblqt3cellans1.Font.Name = "Arial, Helvetica, sans-serif";
                        lblqt3cellans1.Text = dsansoptions.Tables[1].Rows[rans][1].ToString();
                        qt3cellans1.Width = "60%";
                        qt3cellans1.NoWrap = true;
                        qt3cellans1.Controls.Add(lblqt3cellans1);


                        int intanscnt = Convert.ToInt32(dsansoptions.Tables[0].Rows[0][0].ToString());
                        int ranscnt = Convert.ToInt32(dsansoptions.Tables[1].Rows[rans][2].ToString());

                        Double z2ans = Convert.ToDouble(ranscnt * 100);
                        z2ans = Convert.ToDouble(z2ans / intanscnt);
                        string percentans;
                        if (intanscnt != 0)
                        {
                            percentans = Math.Round(Convert.ToDouble(z2ans), 2).ToString();

                        }
                        else
                        {
                            percentans = "0";
                        }

                        HtmlTableCell qt3cellper1 = new HtmlTableCell();
                        qt3cellper1.Align = "Right";
                        System.Web.UI.WebControls.Label lblqt3cellper1 = new System.Web.UI.WebControls.Label();
                        lblqt3cellper1.Font.Name = "Arial, Helvetica, sans-serif";                        
                        lblqt3cellper1.Text = Math.Round(Convert.ToDouble(percentans)).ToString() + "%";
                       qt3cellper1.Width = "20%";
                        qt3cellper1.Controls.Add(lblqt3cellper1);

                        HtmlTableCell qt3cellnos1 = new HtmlTableCell();
                        qt3cellnos1.Align = "Right";
                        System.Web.UI.WebControls.Label lblqt3cellnos1 = new System.Web.UI.WebControls.Label();
                        lblqt3cellnos1.Font.Name = "Arial, Helvetica, sans-serif";
                        lblqt3cellnos1.Text = dsansoptions.Tables[1].Rows[rans][2].ToString() + "&nbsp;";
                        qt3cellnos1.Width = "20%";
                        qt3cellnos1.Controls.Add(lblqt3cellnos1);

                        qt3rowans1.Cells.Add(qt3cellans1);
                        qt3rowans1.Cells.Add(qt3cellper1);
                        qt3rowans1.Cells.Add(qt3cellnos1);

                        qt3table.Rows.Add(qt3rowans1);



                        AddDatatoTable123413(SelectedSurvey.surveyQues[seqint].QUESTION_ID, System.Web.HttpUtility.HtmlEncode(dsansoptions.Tables[1].Rows[rans][1].ToString()), Math.Round(Convert.ToDouble(percentans)).ToString(), colgroupoptions123413);
                    }
                    HtmlTableRow qt3rowtotal = new HtmlTableRow();
                    qt3rowtotal.BorderColor = "#D8D8D8";
                    qt3rowtotal.Height = "25px";
                    HtmlTableCell qt3cellanstotal = new HtmlTableCell();
                    System.Web.UI.WebControls.Label lblqt3cellanstotal = new System.Web.UI.WebControls.Label();
                    lblqt3cellanstotal.Font.Name = "Arial, Helvetica, sans-serif";
                    lblqt3cellanstotal.Font.Bold = true;
                    lblqt3cellanstotal.ForeColor = System.Drawing.Color.Black;
                    lblqt3cellanstotal.Text = "Total Respondents Count";
                    qt3cellanstotal.Width = "60%";
                    qt3cellanstotal.BgColor = "#D8D8D8";
                    qt3cellanstotal.Controls.Add(lblqt3cellanstotal);

                    HtmlTableCell qt3cellpertotal = new HtmlTableCell();
                    System.Web.UI.WebControls.Label lblqt3cellpertotal = new System.Web.UI.WebControls.Label();
                    lblqt3cellpertotal.Font.Name = "Arial, Helvetica, sans-serif";
                    lblqt3cellpertotal.Font.Bold = true;
                    lblqt3cellpertotal.ForeColor = System.Drawing.Color.Black;
                    lblqt3cellpertotal.Text = " ";
                    qt3cellpertotal.Width = "20%";
                    qt3cellpertotal.BgColor = "#D8D8D8";
                    qt3cellpertotal.Controls.Add(lblqt3cellpertotal);

                    HtmlTableCell qt3cellnosTOTAL = new HtmlTableCell();
                    qt3cellnosTOTAL.Align = "Right";
                    System.Web.UI.WebControls.Label lblqt3cellnosTOTAL = new System.Web.UI.WebControls.Label();
                    lblqt3cellnosTOTAL.Font.Name = "Arial, Helvetica, sans-serif";
                    lblqt3cellnosTOTAL.Font.Bold = true;
                    lblqt3cellnosTOTAL.ForeColor = System.Drawing.Color.Black;
                    lblqt3cellnosTOTAL.Text = dsansoptions.Tables[0].Rows[0][0].ToString() + "&nbsp;";
                    qt3cellnosTOTAL.Width = "20%";
                    qt3cellnosTOTAL.BgColor = "#D8D8D8";
                    qt3cellnosTOTAL.Controls.Add(lblqt3cellnosTOTAL);

                    qt3rowtotal.Cells.Add(qt3cellanstotal);
                    qt3rowtotal.Cells.Add(qt3cellpertotal);
                    qt3rowtotal.Cells.Add(qt3cellnosTOTAL);


                    qt3table.Rows.Add(qt3rowtotal);



                    if ((Session["datatablechart"] == "") || (Session["datatablechart"] == null) || (Session["datatablechart"] == "chartloaded"))
                    {
                        rpnl_reportsdata.Controls.Add(qt3tableques);
                        rpnl_reportsdata.Controls.Add(qt3table);

                        rpnl_reportsdata.Visible = true;
                        Session["datatablechart"] = "datatableloaded";
                    }


                    if (Session["datatablechart"] == "chartready")
                    {
                        Session["datatablechart"] = "chartloaded";

                        StringBuilder strXML = new StringBuilder();
                        string strchartname = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(SelectedSurvey.surveyQues[seqint].QUESTION_LABEL)).Replace("\r\n", "");
                        //string repstrchartname = strchartname.Replace("<p>", "");
                        //string fstrchartname1 = repstrchartname.Replace("</p>", "");

                        //string fstrchartname = System.Web.HttpUtility.HtmlEncode(fstrchartname1);
                        ////////Generate the chart element string

                        if ((SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 1) || (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 2) || (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 3))
                        {
                            //paletteColors='FF0000,0404B4,01DF01,A901DB' bgColor='FFFFFF' plotGradientColor='FFFFFF' plotFillColor='FFFFFF' 
                            strXML.Append("<chart  caption='" + strchartname + "'   canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1'  animate3D='1' animated='1'  xAxisName=''  interactiveLegend='1'  showValues='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='save'  exportFileName='chrtobo" + seqint + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' font='Arial, Helvetica, sans-serif'  >  ");

                        }
                        if ((SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 4) )
                        {
                            strXML.Append("<chart caption='" + strchartname + "'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270'  canvasBorderColor='6e1473' canvasBorderThickness='1' pieSliceDepth='30' showBorder='1' borderColor='6e1473' borderThickness='1'  animated='1' interactiveLegend='1' formatNumberScale='0' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='save'  exportFileName='chrtobo" + seqint + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0'  font='Arial, Helvetica, sans-serif'  >");
                        }


                        foreach (DataRow dr in colgroupoptions123413.Rows)
                        {

                            strXML.AppendFormat("<set label='{0}' value='{1}' />", dr[1].ToString().Replace("\r\n",""), dr[2].ToString());
                        }

                        //Close <chart> element
                        strXML.Append("</chart>");



                        if (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 1)
                        {
                            //Create the chart - Column 2D Chart with data from strXML
                            //LiteralQT1.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/Column3D.swf", "", strXML.ToString(), "chrtobo" + seqint, "600", "300", false, true, false);
                            LiteralQT1.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/Column3D.swf", "", strXML.ToString(), "chrtobo" + seqint, "100%", "500", false, true, false);

                            
                        }

                        if (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 2)
                        {
                            //Create the chart - Column 2D Chart with data from strXML
                            LiteralQT2.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/Bar2D.swf", "", strXML.ToString(), "chrtobo" + seqint, "100%", "500", false, true, false);



                        }
                        if (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 3)
                        {
                            //Create the chart - Column 2D Chart with data from strXML
                            LiteralQT3.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/Bar2D.swf", "", strXML.ToString(), "chrtobo" + seqint, "100%", "500", false, true, false);

                        }
                        if (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 4)
                        {
                            LiteralQT4.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/Pie3D.swf", "", strXML.ToString(), "chrtobo" + seqint, "100%", "500", false, true, false);
                        }
                       

                    }
                }
                if (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 13)
                {
                    jsstrchartname = "chrtobo" + seqint;



                    colgroupoptions123413 = createDataTableQ123413();


                    DataSet dsansoptionsCS = surcore.getAnsweroptionsCSQID(SelectedSurvey.surveyQues[seqint].QUESTION_ID);



                    for (int rans = 0; rans < dsansoptionsCS.Tables[0].Rows.Count; rans++)
                    {



                        HtmlTableRow qt3rowans1 = new HtmlTableRow();

                        qt3rowans1.Height = "25px";
                        //  qt3rowans1.BgColor = "lightgray";
                        if (rans % 2 == 1)
                        {
                            qt3rowans1.BgColor = "#F4F4F4";
                            qt3rowans1.BorderColor = "#F4F4F4";
                        }
                        else
                        {
                            qt3rowans1.BgColor = "white";
                            qt3rowans1.BorderColor = "white";
                        }

                        HtmlTableCell qt3cellans1 = new HtmlTableCell();

                        System.Web.UI.WebControls.Label lblqt3cellans1 = new System.Web.UI.WebControls.Label();
                        lblqt3cellans1.Font.Name = "Arial, Helvetica, sans-serif";
                        lblqt3cellans1.Text = dsansoptionsCS.Tables[0].Rows[rans][1].ToString();
                         qt3cellans1.Width = "60%";
                        qt3cellans1.NoWrap = true;
                        qt3cellans1.Controls.Add(lblqt3cellans1);


                        HtmlTableCell qt3cellper1 = new HtmlTableCell();
                        qt3cellper1.Align = "Right";
                        System.Web.UI.WebControls.Label lblqt3cellper1 = new System.Web.UI.WebControls.Label();
                        lblqt3cellper1.Font.Name = "Arial, Helvetica, sans-serif";
                        lblqt3cellper1.Text = Math.Round(Convert.ToDouble(dsansoptionsCS.Tables[0].Rows[rans][5].ToString())).ToString() + "%";
                        qt3cellper1.Width = "20%";
                        qt3cellper1.Controls.Add(lblqt3cellper1);



                        HtmlTableCell qt3cellnos1 = new HtmlTableCell();
                        qt3cellnos1.Align = "Right";
                        System.Web.UI.WebControls.Label lblqt3cellnos1 = new System.Web.UI.WebControls.Label();
                        lblqt3cellnos1.Font.Name = "Arial, Helvetica, sans-serif";
                        lblqt3cellnos1.Text = Math.Round(Convert.ToDouble(dsansoptionsCS.Tables[0].Rows[rans][4].ToString())).ToString() + "&nbsp;";
                        qt3cellnos1.Width = "20%";
                        qt3cellnos1.Controls.Add(lblqt3cellnos1);

                        qt3rowans1.Cells.Add(qt3cellans1);
                        qt3rowans1.Cells.Add(qt3cellper1);
                        qt3rowans1.Cells.Add(qt3cellnos1);
                       
                            qt3table.Rows.Add(qt3rowans1);
                       


                        AddDatatoTable123413(SelectedSurvey.surveyQues[seqint].QUESTION_ID, System.Web.HttpUtility.HtmlEncode(dsansoptionsCS.Tables[0].Rows[rans][1].ToString()), Math.Round(Convert.ToDouble(dsansoptionsCS.Tables[0].Rows[rans][5].ToString())).ToString(), colgroupoptions123413);


                    }


                    HtmlTableRow qt3rowtotal = new HtmlTableRow();
                    qt3rowtotal.BorderColor = "#D8D8D8";
                    qt3rowtotal.Height = "25px";
                    HtmlTableCell qt3cellanstotal = new HtmlTableCell();
                    System.Web.UI.WebControls.Label lblqt3cellanstotal = new System.Web.UI.WebControls.Label();
                    lblqt3cellanstotal.Font.Name = "Arial, Helvetica, sans-serif";
                    lblqt3cellanstotal.Font.Bold = true;
                    lblqt3cellanstotal.ForeColor = System.Drawing.Color.Black;

                    lblqt3cellanstotal.Text = "Constant Sum";

                    qt3cellanstotal.Width = "60%";
                    qt3cellanstotal.BgColor = "#D8D8D8";
                    qt3cellanstotal.Controls.Add(lblqt3cellanstotal);

                    HtmlTableCell qt3cellpertotal = new HtmlTableCell();
                    System.Web.UI.WebControls.Label lblqt3cellpertotal = new System.Web.UI.WebControls.Label();
                    lblqt3cellpertotal.Font.Name = "Arial, Helvetica, sans-serif";
                    lblqt3cellpertotal.Font.Bold = true;
                    lblqt3cellpertotal.ForeColor = System.Drawing.Color.Black;
                    lblqt3cellpertotal.Text = " ";
                    qt3cellpertotal.Width = "20%";
                    qt3cellpertotal.BgColor = "#D8D8D8";
                    qt3cellpertotal.Controls.Add(lblqt3cellpertotal);

                    HtmlTableCell qt3cellnosTOTAL = new HtmlTableCell();
                    qt3cellnosTOTAL.Align = "Right";
                    System.Web.UI.WebControls.Label lblqt3cellnosTOTAL = new System.Web.UI.WebControls.Label();
                    lblqt3cellnosTOTAL.Font.Name = "Arial, Helvetica, sans-serif";
                    lblqt3cellnosTOTAL.Font.Bold = true;
                    lblqt3cellnosTOTAL.ForeColor = System.Drawing.Color.Black;


                    lblqt3cellnosTOTAL.Text = dsansoptionsCS.Tables[1].Rows[0][0].ToString() + "&nbsp;";

                    qt3cellnosTOTAL.Width = "20%";
                    qt3cellnosTOTAL.BgColor = "#D8D8D8";
                    qt3cellnosTOTAL.Controls.Add(lblqt3cellnosTOTAL);

                    qt3rowtotal.Cells.Add(qt3cellanstotal);
                    qt3rowtotal.Cells.Add(qt3cellpertotal);
                    qt3rowtotal.Cells.Add(qt3cellnosTOTAL);


                    HtmlTableRow qt3rowtotalrpc = new HtmlTableRow();
                    qt3rowtotal.BorderColor = "#D8D8D8";
                    qt3rowtotal.Height = "25px";
                    HtmlTableCell qt3cellanstotalrpc = new HtmlTableCell();
                    System.Web.UI.WebControls.Label lblqt3cellanstotalrpc = new System.Web.UI.WebControls.Label();
                    lblqt3cellanstotalrpc.Font.Name = "Arial, Helvetica, sans-serif";
                    lblqt3cellanstotalrpc.Font.Bold = true;
                    lblqt3cellanstotalrpc.ForeColor = System.Drawing.Color.Black;

                    lblqt3cellanstotalrpc.Text = "Total Respondents Count";

                    qt3cellanstotal.Width = "60%";
                    qt3cellanstotalrpc.BgColor = "#D8D8D8";
                    qt3cellanstotalrpc.Controls.Add(lblqt3cellanstotalrpc);

                    HtmlTableCell qt3cellpertotalrpc = new HtmlTableCell();
                    System.Web.UI.WebControls.Label lblqt3cellpertotalrpc = new System.Web.UI.WebControls.Label();
                    lblqt3cellpertotalrpc.Font.Name = "Arial, Helvetica, sans-serif";
                    lblqt3cellpertotalrpc.Font.Bold = true;
                    lblqt3cellpertotalrpc.ForeColor = System.Drawing.Color.Black;
                    lblqt3cellpertotalrpc.Text = " ";
                    qt3cellpertotal.Width = "20%";
                    qt3cellpertotalrpc.BgColor = "#D8D8D8";
                    qt3cellpertotalrpc.Controls.Add(lblqt3cellpertotalrpc);

                    HtmlTableCell qt3cellnosTOTALrpc = new HtmlTableCell();
                    System.Web.UI.WebControls.Label lblqt3cellnosTOTALrpc = new System.Web.UI.WebControls.Label();
                    lblqt3cellnosTOTALrpc.Font.Name = "Arial, Helvetica, sans-serif";
                    lblqt3cellnosTOTALrpc.Font.Bold = true;
                    lblqt3cellnosTOTALrpc.ForeColor = System.Drawing.Color.Black;


                    lblqt3cellnosTOTALrpc.Text = dsansoptionsCS.Tables[2].Rows[0][0].ToString() + "&nbsp;";

                    qt3cellnosTOTAL.Width = "20%";
                    qt3cellnosTOTALrpc.BgColor = "#D8D8D8";
                    qt3cellnosTOTALrpc.Controls.Add(lblqt3cellnosTOTALrpc);

                    qt3rowtotalrpc.Cells.Add(qt3cellanstotalrpc);
                    qt3rowtotalrpc.Cells.Add(qt3cellpertotalrpc);
                    qt3rowtotalrpc.Cells.Add(qt3cellnosTOTALrpc);


                        qt3table.Rows.Add(qt3rowtotal);
                        qt3table.Rows.Add(qt3rowtotalrpc);
                    

                     if ((Session["datatablechart"] == "") || (Session["datatablechart"] == null) || (Session["datatablechart"] == "chartloaded"))
                    {
                        rpnl_reportsdata.Controls.Add(qt3tableques);
                        rpnl_reportsdata.Controls.Add(qt3table);

                        rpnl_reportsdata.Visible = true;
                        Session["datatablechart"] = "datatableloaded";
                    }


                     if (Session["datatablechart"] == "chartready")
                     {
                         Session["datatablechart"] = "chartloaded";


                         StringBuilder strXML = new StringBuilder();
                         string strchartname = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(SelectedSurvey.surveyQues[seqint].QUESTION_LABEL)).Replace("\r\n", "");
                         //string repstrchartname = strchartname.Replace("<p>", "");
                         //string fstrchartname1 = repstrchartname.Replace("</p>", "");
                         //string fstrchartname = System.Web.HttpUtility.HtmlEncode(fstrchartname1);
                         ////////Generate the chart element string

                         strXML.Append("<chart caption='" + strchartname + "'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270'  canvasBorderColor='6e1473' canvasBorderThickness='1' pieSliceDepth='30' showBorder='1' borderColor='6e1473' borderThickness='1'  animated='1' interactiveLegend='1' formatNumberScale='0' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='chrtobo" + seqint + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif'   >");


                         foreach (DataRow dr in colgroupoptions123413.Rows)
                         {

                             strXML.AppendFormat("<set label='{0}' value='{1}' />", dr[1].ToString().Replace("\r\n",""), dr[2].ToString());
                         }

                         //Close <chart> element
                         strXML.Append("</chart>");



                         LiteralQT13.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/Doughnut3D.swf", "", strXML.ToString(), "chrtobo" + seqint, "100%", "500", false, true, false);


                     }
                    
                }

                if (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 10 || SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 11)
                {
                    jsstrchartname = "chrtobo" + seqint;
                    DataSet dsmtxcols = surcore.getMatrixrowscolsQID(SelectedSurvey.surveyQues[seqint].QUESTION_ID);

                    DataTable mtxcolnames = createMtxColumnDataTable();

                    DataTable mtxrownames = createMtxRowDataTable();

                    for (int imtxrow = 0; imtxrow < dsmtxcols.Tables[0].Rows.Count; imtxrow++)
                    {

                        if (imtxrow == 0)
                        {
                            AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[0].Rows[0][0].ToString()), mtxrownames);
                        }
                        else
                        {
                            DataRow[] dr = mtxrownames.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[0].Rows[imtxrow][0].ToString()) + "'");
                            if (dr.Length == 0)
                            {
                                if (dsmtxcols.Tables[0].Rows[imtxrow][0].ToString() != mtxrownames.Rows[imtxrow - 1][0].ToString())
                                {
                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[0].Rows[imtxrow][0].ToString()), mtxrownames);
                                }
                            }
                        }
                    }



                    for (int imtxcol = 0; imtxcol < dsmtxcols.Tables[1].Rows.Count; imtxcol++)
                    {

                        if (imtxcol == 0)
                        {
                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[1].Rows[0][0].ToString()), mtxcolnames);
                        }
                        else
                        {
                             DataRow[] dr = mtxcolnames.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[1].Rows[imtxcol][0].ToString()) + "'");


                             if (dr.Length == 0)
                             {
                                 if (dsmtxcols.Tables[1].Rows[imtxcol][0].ToString() != dsmtxcols.Tables[1].Rows[imtxcol - 1][0].ToString())
                                 {
                                     AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[1].Rows[imtxcol][0].ToString()), mtxcolnames);
                                 }
                             }
                        }
                    }


                    HtmlTableRow mtxcolrow = new HtmlTableRow();
                    mtxcolrow.BgColor = "#bf92cb";
                    mtxcolrow.BorderColor = "#bf92cb";
                    mtxcolrow.Height = "30px";

                    HtmlTableCell mtxcolscellempty = new HtmlTableCell();
                    System.Web.UI.WebControls.Label lblmtxcolscellempty = new System.Web.UI.WebControls.Label();
                    lblmtxcolscellempty.Font.Name = "Arial, Helvetica, sans-serif";
                    lblmtxcolscellempty.Text = "";
                    mtxcolscellempty.Controls.Add(lblmtxcolscellempty);
                    mtxcolrow.Cells.Add(mtxcolscellempty);

                    for (int mcans = 0; mcans < mtxcolnames.Rows.Count; mcans++)
                    {
                        HtmlTableCell mtxcolscell = new HtmlTableCell();
                        mtxcolscell.Align = "Center";
                        System.Web.UI.WebControls.Label lblmtxcolscell = new System.Web.UI.WebControls.Label();
                        lblmtxcolscell.Font.Name = "Arial, Helvetica, sans-serif";
                        lblmtxcolscell.ForeColor = System.Drawing.Color.Black;
                      //  lblmtxcolscell.Font.Bold = true;
                        lblmtxcolscell.Text = mtxcolnames.Rows[mcans][0].ToString();
                        mtxcolscell.Controls.Add(lblmtxcolscell);
                        mtxcolrow.Cells.Add(mtxcolscell);

                    }

                    qt3table.Rows.Add(mtxcolrow);


                    DataTable colgroupoptions = createDataTable();

                    for (int mcansrows = 0; mcansrows < mtxrownames.Rows.Count; mcansrows++)
                    {
                        HtmlTableRow mtxrow = new HtmlTableRow();
                        mtxrow.Height = "30px";
                        //if (mcansrows % 2 == 1)
                        mtxrow.BgColor = "#F4F4F4";
                        mtxrow.BorderColor = "#F4F4F4";
                        //else
                        //    mtxrow.BgColor = "white";


                        HtmlTableCell mtxrowscell = new HtmlTableCell();
                        System.Web.UI.WebControls.Label lblmtxrowscell = new System.Web.UI.WebControls.Label();
                        lblmtxrowscell.Font.Name = "Arial, Helvetica, sans-serif";
                        //lblmtxrowscell.Font.Bold = true;
                        lblmtxrowscell.Text = mtxrownames.Rows[mcansrows][0].ToString();
                        mtxrowscell.Controls.Add(lblmtxrowscell);
                        mtxrow.Cells.Add(mtxrowscell);

                        int matrixtotalcount = 0;

                        for (int mcans1 = 0; mcans1 < mtxcolnames.Rows.Count; mcans1++)
                        {
                            DataSet dsmtrowscolscnt = surcore.getMatrixansrowscolsCount(SelectedSurvey.surveyQues[seqint].QUESTION_ID,System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[mcansrows][0].ToString()),System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[mcans1][0].ToString()));

                            HtmlTableCell mtxrowscolscnt = new HtmlTableCell();
                            mtxrowscolscnt.Align = "Center";
                            System.Web.UI.WebControls.Label lblmtxrowscolscnt = new System.Web.UI.WebControls.Label();
                            lblmtxrowscolscnt.Font.Name = "Arial, Helvetica, sans-serif";
                            if (dsmtrowscolscnt.Tables[0].Rows.Count > 0)
                            {
                                lblmtxrowscolscnt.Text = dsmtrowscolscnt.Tables[0].Rows[0][2].ToString();
                            }
                            else
                            {
                                lblmtxrowscolscnt.Text = "0";
                            }
                           
                            mtxrowscolscnt.Controls.Add(lblmtxrowscolscnt);
                            mtxrow.Cells.Add(mtxrowscolscnt);

                            string mtxcnt;
                            if (dsmtrowscolscnt.Tables[0].Rows.Count > 0)
                            {
                                mtxcnt = dsmtrowscolscnt.Tables[0].Rows[0][2].ToString();
                            }
                            else
                            {
                                mtxcnt = "0";
                            }

                            matrixtotalcount = matrixtotalcount + Convert.ToInt32(mtxcnt);

                        }
                        HtmlTableRow mtxrowpercntg = new HtmlTableRow();
                        mtxrowpercntg.BorderColor = "white";
                        mtxrowpercntg.Height = "30px";
                        HtmlTableCell mtxcolscellemptypcnt = new HtmlTableCell();
                        System.Web.UI.WebControls.Label lblmtxcolscellemptypcnt = new System.Web.UI.WebControls.Label();
                        lblmtxcolscellemptypcnt.Font.Name = "Arial, Helvetica, sans-serif";
                        lblmtxcolscellemptypcnt.Text = "";
                        mtxcolscellemptypcnt.Controls.Add(lblmtxcolscellemptypcnt);
                        mtxrowpercntg.Cells.Add(mtxcolscellemptypcnt);



                        for (int mcansp = 0; mcansp < mtxcolnames.Rows.Count; mcansp++)
                        {
                            DataSet dsmtrowscolscnt = surcore.getMatrixansrowscolsCount(SelectedSurvey.surveyQues[seqint].QUESTION_ID,System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[mcansrows][0].ToString()),System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[mcansp][0].ToString()));

                            //percentage of matrix
                            int rccntrc;

                            if (dsmtrowscolscnt.Tables[0].Rows.Count > 0)
                            {
                                rccntrc = Convert.ToInt32(dsmtrowscolscnt.Tables[0].Rows[0][2].ToString());
                            }
                            else
                            {
                                rccntrc = 0;
                            }

                            Double z2ansrc = Convert.ToDouble(rccntrc * 100);
                            z2ansrc = Convert.ToDouble(z2ansrc / matrixtotalcount);
                            string percentansrc;
                            if (rccntrc != 0)
                            {
                                percentansrc = Math.Round(Convert.ToDouble(z2ansrc), 2).ToString();
                            }
                            else
                            {
                                percentansrc = "0";
                            }

                            HtmlTableCell mtxrowscolscntrc = new HtmlTableCell();
                            mtxrowscolscntrc.Align = "Center";
                            System.Web.UI.WebControls.Label lblmtxrowscolscntrc = new System.Web.UI.WebControls.Label();
                            // lblmtxrowscolscntrc.Text = "";
                            lblmtxrowscolscntrc.Font.Name = "Arial, Helvetica, sans-serif";
                            lblmtxrowscolscntrc.Text = Math.Round(Convert.ToDouble(percentansrc)).ToString() + "%";
                            mtxrowscolscntrc.Controls.Add(lblmtxrowscolscntrc);
                            mtxrowpercntg.Cells.Add(mtxrowscolscntrc);



                            AddDatatoTable(SelectedSurvey.surveyQues[seqint].QUESTION_ID,System.Web.HttpUtility.HtmlEncode(mtxrownames.Rows[mcansrows][0].ToString()),System.Web.HttpUtility.HtmlEncode(mtxcolnames.Rows[mcansp][0].ToString()), Math.Round(Convert.ToDouble(percentansrc)).ToString(), colgroupoptions);
                        }


                        qt3table.Rows.Add(mtxrow);
                        qt3table.Rows.Add(mtxrowpercntg);


                    }
                    if ((Session["datatablechart"] == "") || (Session["datatablechart"] == null) || (Session["datatablechart"] == "chartloaded"))
                    {
                        rpnl_reportsdata.Controls.Add(qt3tableques);
                        rpnl_reportsdata.Controls.Add(qt3table);

                        rpnl_reportsdata.Visible = true;
                        Session["datatablechart"] = "datatableloaded";
                    }


                    if (Session["datatablechart"] == "chartready")
                    {
                        Session["datatablechart"] = "chartloaded";

                        StringBuilder strXML = new StringBuilder();

                        string strchartnamematrix = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(SelectedSurvey.surveyQues[seqint].QUESTION_LABEL)).Replace("\r\n", "");
                        //string repstrchartnamematrix = strchartnamematrix.Replace("<p>", "");
                        //string fstrchartnamematrix1 = repstrchartnamematrix.Replace("</p>", "");
                        //string fstrchartnamematrix = System.Web.HttpUtility.HtmlEncode(fstrchartnamematrix1);
                        ////////Generate the chart element string

                        strXML.Append("<chart palette='2' caption='" + strchartnamematrix + "' animate3D='1' xAxisName='' showValues='1' labelStep='0' rotateXAxisName ='0'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='save'  exportFileName='chrtobo" + seqint + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' font='Arial, Helvetica, sans-serif' >");
                        strXML.AppendFormat("<categories>");

                        foreach (DataRow dr in mtxrownames.Rows)
                        {

                            strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r\n",""));
                        }
                        strXML.AppendFormat("</categories>");


                        for (int mcans1chart = 0; mcans1chart < mtxcolnames.Rows.Count; mcans1chart++)
                        {

                            strXML.AppendFormat("<dataset seriesName='{0}'>", mtxcolnames.Rows[mcans1chart][0].ToString());
                            //int totalmatrixtotalcountchart = 0;
                            for (int mcansrowschart = 0; mcansrowschart < mtxrownames.Rows.Count; mcansrowschart++)
                            {

                                DataRow[] dr1 = colgroupoptions.Select("QuestionID='" + SelectedSurvey.surveyQues[seqint].QUESTION_ID + "'" + "and rowansweroption='" + System.Web.HttpUtility.HtmlEncode(mtxrownames.Rows[mcansrowschart][0].ToString()) + "'" + "and columnansweroption ='" + System.Web.HttpUtility.HtmlEncode(mtxcolnames.Rows[mcans1chart][0].ToString()) + "'");


                                strXML.AppendFormat("<set value='{0}'/>", dr1[0].ItemArray[3].ToString());

                            }
                            strXML.AppendFormat("</dataset>");
                        }

                        //Close <chart> element
                        strXML.Append("</chart>");

                        if (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 10)
                        {
                            //Create the chart - Column 2D Chart with data from strXML
                            LiteralQT10.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/MSLine.swf", "", strXML.ToString(), "chrtobo" + seqint, "100%", "500", false, true, false);
                        }
                        if (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 11)
                        {
                            //Create the chart - Column 2D Chart with data from strXML
                            LiteralQT11.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/MSLine.swf", "", strXML.ToString(), "chrtobo" + seqint, "100%", "500", false, true, false);
                        }
                    }
                }

                if (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 12)
                {
                    jsstrchartname = "chrtobo" + seqint;

                    DataSet dsmtxcols = surcore.getMatrixsidebysideQID(SelectedSurvey.surveyQues[seqint].QUESTION_ID);

                    DataTable mtxrownames = createMtxRowDataTable();

                    DataTable mtxcolnames = createMtxColumnDataTable();



                    for (int imtxrow = 0; imtxrow < dsmtxcols.Tables[0].Rows.Count; imtxrow++)
                    {

                        if (imtxrow == 0)
                        {
                            AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[0].Rows[0][0].ToString()), mtxrownames);
                        }
                        else
                        {
                            DataRow[] dr = mtxrownames.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[0].Rows[imtxrow][0].ToString()) + "'");
                            if (dr.Length == 0)
                            {
                                if (dsmtxcols.Tables[0].Rows[imtxrow][0].ToString() != mtxrownames.Rows[imtxrow - 1][0].ToString())
                                {
                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[0].Rows[imtxrow][0].ToString()), mtxrownames);
                                }
                            }
                        }
                    }



                    for (int imtxcol = 0; imtxcol < dsmtxcols.Tables[1].Rows.Count; imtxcol++)
                    {

                        if (imtxcol == 0)
                        {
                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[1].Rows[0][0].ToString()), mtxcolnames);
                        }
                        else
                        {
                             DataRow[] dr = mtxcolnames.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[1].Rows[imtxcol][0].ToString()) + "'");


                             if (dr.Length == 0)
                             {
                                 if (dsmtxcols.Tables[1].Rows[imtxcol][0].ToString() != dsmtxcols.Tables[1].Rows[imtxcol - 1][0].ToString())
                                 {
                                     AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[1].Rows[imtxcol][0].ToString()), mtxcolnames);
                                 }
                             }
                        }
                    }



                    HtmlTableRow mtxsbscolrow = new HtmlTableRow();
                    mtxsbscolrow.Height = "30px";
                    mtxsbscolrow.BgColor = "#bf92cb";
                    mtxsbscolrow.BorderColor = "white";

                    HtmlTableCell mtxsbscolscellempty = new HtmlTableCell();
                    mtxsbscolscellempty.Width = "20%";
                    System.Web.UI.WebControls.Label lblmtxsbscolscellempty = new System.Web.UI.WebControls.Label();
                    lblmtxsbscolscellempty.Font.Name = "Arial, Helvetica, sans-serif";
                    lblmtxsbscolscellempty.Text = "";
                    mtxsbscolscellempty.Controls.Add(lblmtxsbscolscellempty);
                    mtxsbscolrow.Cells.Add(mtxsbscolscellempty);

                    for (int drmtxcols = 0; drmtxcols < mtxcolnames.Rows.Count; drmtxcols++)
                    {
                        HtmlTableCell mtxsbsmaincolscellempty = new HtmlTableCell();
                        mtxsbsmaincolscellempty.Align = "Center";
                         mtxsbsmaincolscellempty.Width = "40%";
                        System.Web.UI.WebControls.Label lblmtxsbsmaincolscellempty = new System.Web.UI.WebControls.Label();
                        lblmtxsbsmaincolscellempty.Font.Name = "Arial, Helvetica, sans-serif";
                        lblmtxsbsmaincolscellempty.Text = mtxcolnames.Rows[drmtxcols][0].ToString();
                       // lblmtxsbsmaincolscellempty.Font.Bold = true;
                        lblmtxsbsmaincolscellempty.ForeColor = System.Drawing.Color.Black;


                        HtmlTable tbl1 = new HtmlTable();
                        tbl1.Width = "100%";
                        // tbl1.Border = 1;
                        tbl1.BorderColor = "White";
                        HtmlTableRow tbr1 = new HtmlTableRow();
                        for (int drmtxsubcols = 0; drmtxsubcols < dsmtxcols.Tables[2].Rows.Count; drmtxsubcols++)
                        {
                            HtmlTableCell tbc1 = new HtmlTableCell();
                            tbc1.Align = "Center";
                            System.Web.UI.WebControls.Label lblmtxsbsmaincolscellempty1 = new System.Web.UI.WebControls.Label();
                            lblmtxsbsmaincolscellempty1.Font.Name = "Arial, Helvetica, sans-serif";
                            lblmtxsbsmaincolscellempty1.Text = dsmtxcols.Tables[2].Rows[drmtxsubcols][0].ToString();
                           // lblmtxsbsmaincolscellempty1.Font.Bold = true;
                            lblmtxsbsmaincolscellempty1.ForeColor = System.Drawing.Color.Black;
                                tbc1.Width = "10%";
                            // tbc1.BgColor = "red";
                            tbc1.Controls.Add(lblmtxsbsmaincolscellempty1);
                            tbr1.Cells.Add(tbc1);
                            tbl1.Rows.Add(tbr1);
                            mtxsbsmaincolscellempty.Controls.Add(lblmtxsbsmaincolscellempty);
                            mtxsbsmaincolscellempty.Controls.Add(tbl1);
                        }
                        mtxsbscolrow.Cells.Add(mtxsbsmaincolscellempty);
                    }


                    qt3table.Rows.Add(mtxsbscolrow);


                    DataTable colgroupoptionsQ12 = createDataTableQ12();

                    for (int drmtxrows = 0; drmtxrows < mtxrownames.Rows.Count; drmtxrows++)
                    {
                        HtmlTableRow htr = new HtmlTableRow();
                        htr.Height = "30px";
                        if (drmtxrows % 2 == 1)
                        {
                            htr.BgColor = "#F4F4F4";
                            htr.BorderColor = "#F4F4F4";
                        }
                        else
                        {
                            htr.BgColor = "white";
                            htr.BorderColor = "white";
                        }


                        HtmlTableCell htc = new HtmlTableCell();
                        htc.Width = "30%";
                        // htc.BgColor = "MediumGrey";
                        System.Web.UI.WebControls.Label htcstring = new System.Web.UI.WebControls.Label();
                        htcstring.Font.Name = "Arial, Helvetica, sans-serif";
                        htcstring.Text = mtxrownames.Rows[drmtxrows][0].ToString();
                        htcstring.Font.Bold = true;
                        string strRowValue = mtxrownames.Rows[drmtxrows][0].ToString();
                        htc.Controls.Add(htcstring);
                        htr.Cells.Add(htc);
                        for (int drmtxcols1 = 0; drmtxcols1 < mtxcolnames.Rows.Count; drmtxcols1++)
                        {
                            HtmlTableCell mtxsbsmaincolscellempty1 = new HtmlTableCell();
                            mtxsbsmaincolscellempty1.Align = "Center";
                              mtxsbsmaincolscellempty1.Width = "40%";
                            System.Web.UI.WebControls.Label rowdata = new System.Web.UI.WebControls.Label();
                            rowdata.Font.Name = "Arial, Helvetica, sans-serif";
                            string strColValue = mtxcolnames.Rows[drmtxcols1][0].ToString();

                            int totalsubcolprcntg = 0;

                            HtmlTable tbl2 = new HtmlTable();
                            tbl2.Width = "100%";
                            //tbl2.Border = 1;
                            HtmlTableRow tbr2 = new HtmlTableRow();
                            for (int drmtxsubcols1 = 0; drmtxsubcols1 < dsmtxcols.Tables[2].Rows.Count; drmtxsubcols1++)
                            {
                                HtmlTableCell tbc2 = new HtmlTableCell();
                                tbc2.Align = "Center";
                                System.Web.UI.WebControls.Label rowdata1 = new System.Web.UI.WebControls.Label();
                                rowdata1.Font.Name = "Arial, Helvetica, sans-serif";

                                DataSet dssbsrowscols = surcore.getMatrixsidebysiderowscolsCount(SelectedSurvey.surveyQues[seqint].QUESTION_ID, System.Web.HttpUtility.HtmlDecode(strColValue), dsmtxcols.Tables[2].Rows[drmtxsubcols1][0].ToString(), System.Web.HttpUtility.HtmlDecode(strRowValue));
                                if (dssbsrowscols.Tables[0].Rows.Count > 0)
                                {
                                    rowdata1.Text = dssbsrowscols.Tables[0].Rows[0][2].ToString();
                                }
                                else
                                {
                                    rowdata1.Text = "0";
                                }

                                     tbc2.Width = "10%";
                                //  tbc2.BgColor = "green";
                                tbc2.Controls.Add(rowdata1);
                                tbr2.Cells.Add(tbc2);
                                tbl2.Rows.Add(tbr2);
                                mtxsbsmaincolscellempty1.Controls.Add(tbl2);

                                totalsubcolprcntg = totalsubcolprcntg + Convert.ToInt32(rowdata1.Text);
                            }

                            HtmlTable tbl2tot = new HtmlTable();
                            tbl2tot.Align = "Center";
                            tbl2tot.Width = "100%";
                            // tbl2tot.Border = 1;
                            HtmlTableRow tbr2tot = new HtmlTableRow();

                            for (int drmtxsubcols1tot = 0; drmtxsubcols1tot < dsmtxcols.Tables[2].Rows.Count; drmtxsubcols1tot++)
                            {
                                HtmlTableCell tbc2tot = new HtmlTableCell();
                                System.Web.UI.WebControls.Label rowdata1tot = new System.Web.UI.WebControls.Label();
                                rowdata1tot.Font.Name = "Arial, Helvetica, sans-serif";

                                DataSet dssbsrowscolstot = surcore.getMatrixsidebysiderowscolsCount(SelectedSurvey.surveyQues[seqint].QUESTION_ID, System.Web.HttpUtility.HtmlDecode(strColValue), dsmtxcols.Tables[2].Rows[drmtxsubcols1tot][0].ToString(), System.Web.HttpUtility.HtmlDecode(strRowValue));

                                int rccntrcsisdebyside;
                                if (dssbsrowscolstot.Tables[0].Rows.Count > 0)
                                {

                                    rccntrcsisdebyside = Convert.ToInt32(dssbsrowscolstot.Tables[0].Rows[0][2].ToString());
                                }
                                else
                                {
                                    rccntrcsisdebyside = 0;
                                }

                                Double z2ansrcsidebyside = Convert.ToDouble(rccntrcsisdebyside * 100);
                                z2ansrcsidebyside = Convert.ToDouble(z2ansrcsidebyside / totalsubcolprcntg);

                                if (rccntrcsisdebyside != 0)
                                {

                                    // rowdata1tot.Text = Math.Round(Convert.ToDouble(z2ansrcsidebyside), 2).ToString() + "%";
                                    rowdata1tot.Text = Math.Round(z2ansrcsidebyside).ToString() + "%";
                                }
                                else
                                {
                                    rowdata1tot.Text = "0" + "%";
                                }

                                tbc2tot.Width = "10%";
                                //  tbc2tot.BgColor = "yellow";
                                tbc2tot.Controls.Add(rowdata1tot);
                                tbr2tot.Cells.Add(tbc2tot);
                                tbl2tot.Rows.Add(tbr2tot);
                                mtxsbsmaincolscellempty1.Controls.Add(tbl2tot);



                                AddDatatoTableQ12(SelectedSurvey.surveyQues[seqint].QUESTION_ID,System.Web.HttpUtility.HtmlEncode(strColValue),System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[2].Rows[drmtxsubcols1tot][0].ToString()),System.Web.HttpUtility.HtmlEncode(strRowValue), Math.Round(z2ansrcsidebyside).ToString(), colgroupoptionsQ12);

                            }
                            htr.Cells.Add(mtxsbsmaincolscellempty1);
                        }


                        qt3table.Rows.Add(htr);

                    }

                    if ((Session["datatablechart"] == "") || (Session["datatablechart"] == null) || (Session["datatablechart"] == "chartloaded"))
                    {
                        rpnl_reportsdata.Controls.Add(qt3tableques);
                        rpnl_reportsdata.Controls.Add(qt3table);

                        rpnl_reportsdata.Visible = true;
                        Session["datatablechart"] = "datatableloaded";
                    }


                    if (Session["datatablechart"] == "chartready")
                    {
                        Session["datatablechart"] = "chartloaded";
                        StringBuilder strXML = new StringBuilder();

                        string strchartnamematrixsbs = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(SelectedSurvey.surveyQues[seqint].QUESTION_LABEL)).Replace("\r\n", "");
                        //string repstrchartnamematrixsbs = strchartnamematrixsbs.Replace("<p>", "");
                        //string fstrchartnamematrixsbs1 = repstrchartnamematrixsbs.Replace("</p>", "");
                        //string fstrchartnamematrixsbs = System.Web.HttpUtility.HtmlEncode(fstrchartnamematrixsbs1);
                        ////////Generate the chart element string

                        strXML.Append("<chart palette='2' caption='" + strchartnamematrixsbs + "' animate3D='1' xAxisName='' showValues='1' labelStep='0' rotateXAxisName ='0' labelDisplay='Rotate' slantLabels='1'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='save'  exportFileName='chrtobo" + seqint + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' font='Arial, Helvetica, sans-serif' >");


                        strXML.AppendFormat("<categories>");

                        foreach (DataRow dr in mtxrownames.Rows)
                        {
                            strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r\n","") + "-" + mtxcolnames.Rows[0][0].ToString());

                            for (int i = 1; i <= mtxcolnames.Rows.Count - 1; i++)
                            {

                                strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r\n","") + "-" + mtxcolnames.Rows[i][0].ToString());

                            }

                        }
                        strXML.AppendFormat("</categories>");


                        for (int colval = 0; colval < dsmtxcols.Tables[2].Rows.Count; colval++)
                        {
                            strXML.AppendFormat("<dataset seriesName='{0}'>", dsmtxcols.Tables[2].Rows[colval][0].ToString());

                            for (int colvalx = 0; colvalx < mtxrownames.Rows.Count; colvalx++)
                            {

                                //string rowtot;


                                for (int rot = 0; rot < mtxcolnames.Rows.Count; rot++)
                                {
                                    // int totalsubcolprcntg1 = 0;
                                    DataRow[] dr1 = null;

                                    dr1 = colgroupoptionsQ12.Select("QuestionID='" + SelectedSurvey.surveyQues[seqint].QUESTION_ID + "'" + "and columnoption='" + System.Web.HttpUtility.HtmlEncode(mtxcolnames.Rows[rot][0].ToString()) + "'" + "and subcolumnoption ='" + System.Web.HttpUtility.HtmlEncode(dsmtxcols.Tables[2].Rows[colval][0].ToString()) + "'" + "and rowoption='" + System.Web.HttpUtility.HtmlEncode(mtxrownames.Rows[colvalx][0].ToString()) + "'");


                                    strXML.AppendFormat("<set value='{0}'/>", dr1[0].ItemArray[4].ToString());

                                }
                            }

                            strXML.AppendFormat("</dataset>");
                        }


                        //Close <chart> element
                        strXML.Append("</chart>");

                        //Create the chart - Column 2D Chart with data from strXML
                        LiteralQT12.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/MSColumn3D.swf", "", strXML.ToString(), "chrtobo" + seqint, "100%", "500", false, true, false);
                    }
                }

                if (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 15)
                {
                    jsstrchartname = "chrtobo" + seqint;
                    DataTable colgroupoptions15 = createDataTable();
                    DataSet dsmtxcolsranking = surcore.getRankingrowscolsQID(SelectedSurvey.surveyQues[seqint].QUESTION_ID);
                    HtmlTableRow mtxcolrowranking = new HtmlTableRow();
                    mtxcolrowranking.BgColor = "#bf92cb";
                    mtxcolrowranking.BorderColor = "#bf92cb";

                    HtmlTableCell mtxcolscellemptyranking = new HtmlTableCell();
                    System.Web.UI.WebControls.Label lblmtxcolscellemptyranking = new System.Web.UI.WebControls.Label();
                    lblmtxcolscellemptyranking.Font.Name = "Arial, Helvetica, sans-serif";
                    lblmtxcolscellemptyranking.Text = "";
                    mtxcolscellemptyranking.Controls.Add(lblmtxcolscellemptyranking);
                    mtxcolrowranking.Cells.Add(mtxcolscellemptyranking);

                    for (int mcansrank = 0; mcansrank < dsmtxcolsranking.Tables[1].Rows.Count; mcansrank++)
                    {
                        HtmlTableCell mtxcolscellrank = new HtmlTableCell();
                        System.Web.UI.WebControls.Label lblmtxcolscellrank = new System.Web.UI.WebControls.Label();
                        lblmtxcolscellrank.Font.Name = "Arial, Helvetica, sans-serif";
                        lblmtxcolscellrank.ForeColor = System.Drawing.Color.White;
                        lblmtxcolscellrank.Text = dsmtxcolsranking.Tables[1].Rows[mcansrank][0].ToString();
                        mtxcolscellrank.Controls.Add(lblmtxcolscellrank);
                        mtxcolrowranking.Cells.Add(mtxcolscellrank);

                    }
                    qt3table.Rows.Add(mtxcolrowranking);

                    for (int mcansrowsrank = 0; mcansrowsrank < dsmtxcolsranking.Tables[0].Rows.Count; mcansrowsrank++)
                    {
                        HtmlTableRow mtxrowrank = new HtmlTableRow();

                        mtxrowrank.BgColor = "#F4F4F4";
                        mtxrowrank.BorderColor = "#F4F4F4";


                        HtmlTableCell mtxrowscellrank = new HtmlTableCell();
                        System.Web.UI.WebControls.Label lblmtxrowscellrank = new System.Web.UI.WebControls.Label();
                        lblmtxrowscellrank.Font.Name = "Arial, Helvetica, sans-serif";
                        lblmtxrowscellrank.Text = dsmtxcolsranking.Tables[0].Rows[mcansrowsrank][0].ToString();
                        mtxrowscellrank.Controls.Add(lblmtxrowscellrank);
                        mtxrowrank.Cells.Add(mtxrowscellrank);

                        int matrixtotalcountrank = 0;

                        for (int mcans1rank = 0; mcans1rank < dsmtxcolsranking.Tables[1].Rows.Count; mcans1rank++)
                        {
                            DataSet dsmtrowscolscntrank = surcore.getRankingrowscolsCount(SelectedSurvey.surveyQues[seqint].QUESTION_ID, dsmtxcolsranking.Tables[0].Rows[mcansrowsrank][0].ToString(), dsmtxcolsranking.Tables[1].Rows[mcans1rank][0].ToString());

                            HtmlTableCell mtxrowscolscntrank = new HtmlTableCell();
                            System.Web.UI.WebControls.Label lblmtxrowscolscntrank = new System.Web.UI.WebControls.Label();

                            int totconvert = 0;
                            if (dsmtrowscolscntrank.Tables[0].Rows.Count > 0)
                            {
                                lblmtxrowscolscntrank.Text = dsmtrowscolscntrank.Tables[0].Rows[0][2].ToString();
                                totconvert = Convert.ToInt32(dsmtrowscolscntrank.Tables[0].Rows[0][2].ToString());
                            }
                            else
                            {
                                lblmtxrowscolscntrank.Text = "0";
                                totconvert = 0;
                            }
                            mtxrowscolscntrank.Controls.Add(lblmtxrowscolscntrank);
                            mtxrowrank.Cells.Add(mtxrowscolscntrank);
                            matrixtotalcountrank = matrixtotalcountrank + totconvert;

                        }
                        HtmlTableRow mtxrowpercntgrank = new HtmlTableRow();
                        mtxrowpercntgrank.BorderColor = "white";
                        HtmlTableCell mtxcolscellemptypcntrank = new HtmlTableCell();
                        System.Web.UI.WebControls.Label lblmtxcolscellemptypcntrank = new System.Web.UI.WebControls.Label();
                        lblmtxcolscellemptypcntrank.Font.Name = "Arial, Helvetica, sans-serif";
                        lblmtxcolscellemptypcntrank.Text = "";
                        mtxcolscellemptypcntrank.Controls.Add(lblmtxcolscellemptypcntrank);
                        mtxrowpercntgrank.Cells.Add(mtxcolscellemptypcntrank);



                        for (int mcansprank = 0; mcansprank < dsmtxcolsranking.Tables[1].Rows.Count; mcansprank++)
                        {
                            DataSet dsmtrowscolscntrank = surcore.getRankingrowscolsCount(SelectedSurvey.surveyQues[seqint].QUESTION_ID, dsmtxcolsranking.Tables[0].Rows[mcansrowsrank][0].ToString(), dsmtxcolsranking.Tables[1].Rows[mcansprank][0].ToString());

                            //percentage of matrix
                            int rccntrcrank = 0;
                            if (dsmtrowscolscntrank.Tables[0].Rows.Count > 0)
                            {
                                rccntrcrank = Convert.ToInt32(dsmtrowscolscntrank.Tables[0].Rows[0][2].ToString());
                            }
                            else
                            {
                                rccntrcrank = 0;
                            }

                            Double z2ansrcrank = Convert.ToDouble(rccntrcrank * 100);
                            z2ansrcrank = Convert.ToDouble(z2ansrcrank / matrixtotalcountrank);
                            string percentansrcrank;
                            if (rccntrcrank != 0)
                            {
                                percentansrcrank = Math.Round(Convert.ToDouble(z2ansrcrank), 2).ToString();
                            }
                            else
                            {
                                percentansrcrank = "0";
                            }

                            HtmlTableCell mtxrowscolscntrcrank = new HtmlTableCell();
                            System.Web.UI.WebControls.Label lblmtxrowscolscntrcrank = new System.Web.UI.WebControls.Label();
                            lblmtxrowscolscntrcrank.Font.Name = "Arial, Helvetica, sans-serif";
                            lblmtxrowscolscntrcrank.Text = Math.Round(Convert.ToDouble(percentansrcrank)).ToString() + "%";
                            mtxrowscolscntrcrank.Controls.Add(lblmtxrowscolscntrcrank);
                            mtxrowpercntgrank.Cells.Add(mtxrowscolscntrcrank);

                            AddDatatoTable(SelectedSurvey.surveyQues[seqint].QUESTION_ID,System.Web.HttpUtility.HtmlEncode(dsmtxcolsranking.Tables[0].Rows[mcansrowsrank][0].ToString()),System.Web.HttpUtility.HtmlEncode(dsmtxcolsranking.Tables[1].Rows[mcansprank][0].ToString()), Math.Round(Convert.ToDouble(percentansrcrank)).ToString(), colgroupoptions15);
                        }

                        qt3table.Rows.Add(mtxrowrank);
                        qt3table.Rows.Add(mtxrowpercntgrank);


                    }

                    if ((Session["datatablechart"] == "") || (Session["datatablechart"] == null) || (Session["datatablechart"] == "chartloaded"))
                    {
                        rpnl_reportsdata.Controls.Add(qt3tableques);
                        rpnl_reportsdata.Controls.Add(qt3table);

                        rpnl_reportsdata.Visible = true;
                        Session["datatablechart"] = "datatableloaded";
                    }


                    if (Session["datatablechart"] == "chartready")
                    {
                        Session["datatablechart"] = "chartloaded";
                        StringBuilder strXML = new StringBuilder();

                        string strchartnamematrixsbs = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(SelectedSurvey.surveyQues[seqint].QUESTION_LABEL)).Replace("\r\n", "");
                        //string repstrchartnamematrixsbs = strchartnamematrixsbs.Replace("<p>", "");
                        //string fstrchartnamematrixsbs = repstrchartnamematrixsbs.Replace("</p>", "");
                        ////////Generate the chart element string

                        strXML.Append("<chart palette='2' caption='" + strchartnamematrixsbs + "' animate3D='1' xAxisName='' showValues='1' labelStep='0' rotateXAxisName ='0' labelDisplay='Rotate' slantLabels='1'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='save'  exportFileName='chrtobo" + seqint + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' font='Arial, Helvetica, sans-serif' >");


                        strXML.AppendFormat("<categories>");

                        foreach (DataRow dr in dsmtxcolsranking.Tables[0].Rows)
                        {

                            strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r\n",""));

                        }
                        strXML.AppendFormat("</categories>");

                        for (int mcansprankchart = 0; mcansprankchart < dsmtxcolsranking.Tables[1].Rows.Count; mcansprankchart++)
                        {

                            strXML.AppendFormat("<dataset seriesName='{0}'>", dsmtxcolsranking.Tables[1].Rows[mcansprankchart][0].ToString());
                            for (int mcansrowsrankchart = 0; mcansrowsrankchart < dsmtxcolsranking.Tables[0].Rows.Count; mcansrowsrankchart++)
                            {

                                DataRow[] dr15 = null;

                                dr15 = colgroupoptions15.Select("QuestionID='" + SelectedSurvey.surveyQues[seqint].QUESTION_ID + "'" + "and rowansweroption='" + System.Web.HttpUtility.HtmlEncode(dsmtxcolsranking.Tables[0].Rows[mcansrowsrankchart][0].ToString()) + "'" + "and columnansweroption ='" + System.Web.HttpUtility.HtmlEncode(dsmtxcolsranking.Tables[1].Rows[mcansprankchart][0].ToString()) + "'");



                                strXML.AppendFormat("<set value='{0}'/>", dr15[0].ItemArray[3].ToString());

                            }
                            strXML.AppendFormat("</dataset>");
                        }

                        //Close <chart> element
                        strXML.Append("</chart>");

                        //Create the chart - Column 2D Chart with data from strXML
                        LiteralQT15.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/MSColumn3D.swf", "", strXML.ToString(), "chrtobo" + seqint, "100%", "500", false, true, false);

                    }
                }


                //if ((SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 5) || (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 6) || (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 9) || (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 7) || (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 20) || (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 8) || (SelectedSurvey.surveyQues[seqint].QUESTION_TYPE == 14))
                //{
                //    DataSet dstextresponses = surcore.getTextResponsesQID(SelectedSurvey.surveyQues[seqint].QUESTION_ID);

                //    HtmlTableRow hrtext = new HtmlTableRow();
                //    hrtext.BgColor = "#bf92cb";
                //    hrtext.BorderColor = "white";
                //    hrtext.Height = "30px";
                //    HtmlTableCell hctext = new HtmlTableCell();
                //    // hctext.BgColor = "BurlyWood";
                //    HyperLink hql = new HyperLink();
                //    hql.Font.Bold = true;
                //    hql.Font.Underline = true;
                //    hql.ForeColor = System.Drawing.Color.White;
                //    hql.Text = "You have" + dstextresponses.Tables[0].Rows.Count + "Responses for this Question.";

                //    DateTime starttext = dt1.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt1;
                //    DateTime Endtext = dt2.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt2;
                //    string NavUrl = Constants.SURVEYID + "=" + surveyID + "&QuesID=" + SelectedSurvey.surveyQues[seqint].QUESTION_ID + "&Mode=" + Mode + "&surveyFlag=" + SurveyFlag + "&Date_Start=" + starttext + "&Date_End=" + Endtext;
                //    if (ViewAllQues == "All")
                //        NavUrl += "&Veiw=" + ViewAllQues;
                //    hql.NavigateUrl = EncryptHelper.EncryptQuerystring("DescriptiveResponses.aspx", NavUrl);
                //    hctext.Controls.Add(hql);
                //    hrtext.Cells.Add(hctext);
                //    qt3table.Rows.Add(hrtext);

                //}
                //rpnl_reportsdata.Controls.Add(qt3tableques);
                //rpnl_reportsdata.Controls.Add(qt3table);

                //rpnl_reportsdata.Visible = true;

            
       // }
    }

    private DataTable createMtxColumnDataTable()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();


            myDataColumn.DataType = Type.GetType("System.String");
            myDataColumn.ColumnName = "columnname";
            myDataTable.Columns.Add(myDataColumn);


            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private DataTable createMtxRowDataTable()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();


            myDataColumn.DataType = Type.GetType("System.String");
            myDataColumn.ColumnName = "rowname";
            myDataTable.Columns.Add(myDataColumn);


            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void AddMtxrowsDatatoTable(string rowoption, DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["rowname"] = rowoption;

            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void AddMtxcolsDatatoTable(string columnoption, DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["columnname"] = columnoption;

            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
            

  private DataTable createDataTable()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
            DataColumn myDataColumn3 = new DataColumn();

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "QuestionID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "rowansweroption";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "columnansweroption";
            myDataTable.Columns.Add(myDataColumn2);

            myDataColumn3.DataType = Type.GetType("System.String");
            myDataColumn3.ColumnName = "percentage";
            myDataTable.Columns.Add(myDataColumn3);

            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void AddDatatoTable(int QuestionID, string rowansweroption, string columnansweroption, string percentage, DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = QuestionID;
            dr["rowansweroption"] = rowansweroption;
            dr["columnansweroption"] = columnansweroption;
            dr["percentage"] = percentage;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private DataTable createDataTableQ123413()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
          

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "QuestionID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "answeroption";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "percentage";
            myDataTable.Columns.Add(myDataColumn2);


            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void AddDatatoTable123413(int QuestionID, string answeroption,  string percentage, DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = QuestionID;
            dr["answeroption"] = answeroption;           
            dr["percentage"] = percentage;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private DataTable createDataTableQ12()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
            DataColumn myDataColumn3 = new DataColumn();
            DataColumn myDataColumn4 = new DataColumn();

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "QuestionID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "columnoption";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "subcolumnoption";
            myDataTable.Columns.Add(myDataColumn2);

            myDataColumn3.DataType = Type.GetType("System.String");
            myDataColumn3.ColumnName = "rowoption";
            myDataTable.Columns.Add(myDataColumn3);

            myDataColumn4.DataType = Type.GetType("System.String");
            myDataColumn4.ColumnName = "percentage";
            myDataTable.Columns.Add(myDataColumn4);

            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void AddDatatoTableQ12(int QuestionID, string columnoption, string subcolumnoption,string rowoption, string percentage, DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = QuestionID;
            dr["columnoption"] = columnoption;
            dr["subcolumnoption"] = subcolumnoption;
            dr["rowoption"] = rowoption;
            dr["percentage"] = percentage;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnprev_Click(object sender, EventArgs e)
    {

        LiteralQTInitial.Text = "";
        LiteralQT1.Text = "";
        LiteralQT2.Text = "";
        LiteralQT3.Text = "";
        LiteralQT4.Text = "";
        LiteralQT13.Text = "";
        LiteralQT10.Text = "";
        LiteralQT11.Text = "";
        LiteralQT12.Text = "";
        LiteralQT15.Text = "";



        if (Session["datatablechart"] == "datatableloaded")
        {
            Session["pageno"] = Convert.ToInt32(Session["pageno"].ToString())-1;
            Session["datatablechart"] = "chartready";
        }
        else
        {
            Session["pageno"] = Convert.ToInt32(Session["pageno"].ToString());
        }


        btnnxt.Enabled = true;

        DateTime start = dt1.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt1;
        DateTime End = dt2.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt2;

      
            BindReport(dt1, dt2);

            if ((Convert.ToInt32(Session["pageno"].ToString()) == 0) &&  (Session["datatablechart"] == "datatableloaded"))
            {
                btnprev.Enabled = false;
            }
            int currentpage = Convert.ToInt32(Session["pageno"].ToString()) + 1;
            lbldisplay.Text = currentpage + " of " + SelectedSurvey.surveyQues.Count; 

    }
    protected void btnnxt_Click(object sender, EventArgs e)
    {

        LiteralQTInitial.Text="";
           LiteralQT1.Text="";
           LiteralQT2.Text="";
            LiteralQT3.Text="";
             LiteralQT4.Text="";
              LiteralQT13.Text="";
              LiteralQT10.Text="";
              LiteralQT11.Text="";
              LiteralQT12.Text="";
              LiteralQT15.Text="";
                
        

        if (Session["datatablechart"] == "datatableloaded")
        {
            Session["pageno"] = Convert.ToInt32(Session["pageno"].ToString());
            Session["datatablechart"] = "chartready";
        }
        else
        {
            Session["pageno"] = Convert.ToInt32(Session["pageno"].ToString()) + 1;
        }
       
       
        btnprev.Enabled = true;

        DateTime start = dt1.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt1;
        DateTime End = dt2.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt2;

        
            BindReport(dt1, dt2);
        
        int lastrec = SelectedSurvey.surveyQues.Count - 1;
        if ((Convert.ToInt32(Session["pageno"].ToString()) == lastrec) && (Session["datatablechart"] == "chartloaded"))
        {
            btnnxt.Enabled = false;
        }
        int currentpage = Convert.ToInt32(Session["pageno"].ToString()) + 1;
        lbldisplay.Text = currentpage + " of " + SelectedSurvey.surveyQues.Count; 
    }

    public static string StripTagsRegex(string source)
    {
        return Regex.Replace(source, "<.*?>", string.Empty);
    }

    static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
    public static string StripTagsRegexCompiled(string source)
    {
        return _htmlRegex.Replace(source, string.Empty);
    }
    public static string StripTagsCharArray(string source)
    {
        char[] array = new char[source.Length];
        int arrayIndex = 0;
        bool inside = false;

        for (int i = 0; i < source.Length; i++)
        {
            char let = source[i];
            if (let == '<')
            {
                inside = true;
                continue;
            }
            if (let == '>')
            {
                inside = false;
                continue;
            }
            if (!inside)
            {
                array[arrayIndex] = let;
                arrayIndex++;
            }
        }
        return new string(array, 0, arrayIndex);
    }
}