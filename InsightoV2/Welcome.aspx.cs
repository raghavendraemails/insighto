﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.ValueObjects;
using Insighto.Business.Services;
using Insighto.Data;
using CovalenseUtilities.Extensions.UtcTimeZones;
using CovalenseUtilities.Helpers;
using Insighto.Business.Helpers;

public partial class Welcome : SecurePageBase
{
    LoggedInUserInfo loggedInUserInfo;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) 
        { 
           // getTimeZone();
            SessionStateService sessionStateService = new SessionStateService();
            loggedInUserInfo  = sessionStateService.GetLoginUserDetailsSession();
            if (loggedInUserInfo == null)
            {
                Response.Redirect("Home.aspx");
            }
        }

    }
    protected void getTimeZone()
     {

        TimeZoneInformation[] m_zones = TimeZoneInformation.EnumZones();
        ArrayList disp_name = new ArrayList();
        ArrayList disp_negative = new ArrayList();
        for (int i = 0; i < m_zones.LongLength; i++)
        {
            if (m_zones[i].StandardBias < 0)
                disp_negative.Add(m_zones[i].DisplayName);
            else
                disp_name.Add(m_zones[i].DisplayName);
        }
        disp_name.Sort();
        disp_negative.Sort();

       
        int p = 1;
        for (int k = disp_negative.Count - 1; k >= 0; k--)
        {
            for (int i = 0; i < m_zones.LongLength; i++)
            {
                if (Convert.ToString(disp_negative[k]) == m_zones[i].DisplayName)
                {
                    
                    drpTimeZone.Items.Add(m_zones[i].DisplayName);
                    p = p + 1;
                    break;
                }
            }
        }
        for (int k = 0; k < disp_name.Count; k++)
        {
            for (int i = 0; i < m_zones.LongLength; i++)
            {
                if (Convert.ToString(disp_name[k]) == m_zones[i].DisplayName)
                {
                    drpTimeZone.Items.Add(m_zones[i].DisplayName);                  
                    p = p + 1;
                    break;
                }
            }
        }

        drpTimeZone.Items.Insert(0, new ListItem("--Select--", " "));
        drpTimeZone.SelectedIndex = 0;
       
              
         
         
    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {
       // ListEditItem lt = drpTimeZone.SelectedItem;
        
        string timeZ = drpTimeZone.SelectedValue;
        string[] totalString = timeZ.Split(',');
        string[] tempTime = totalString[0].Split(':');
        string sign = tempTime[0].Substring(0, 1);
        int hours = 0, minutes = 0;
        if (sign != "0")
        {
            hours = ValidationHelper.GetInteger(tempTime[0].Substring(1, (tempTime.Length)), 0) * 60;
            minutes = ValidationHelper.GetInteger(tempTime[1].ToString(), 0);
            minutes = hours + minutes;
            if (sign == "-")
            {
                minutes = -(minutes);
            }
        }
         
        SessionStateService sessionStateService = new SessionStateService();
        loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        TimeZoneInformation[] m_zones = TimeZoneInformation.EnumZones();
        int y = 0;
        //for (int i = 0; i < m_zones.LongLength; i++)
        //{
        //    if (drpTimeZone.SelectedItem.Text == m_zones[i].DisplayName)
        //    {
        //        y = Convert.ToInt32(m_zones[i].StandardBias);
        //        break;
        //    }
        //}
        if (drpTimeZone.SelectedItem != null && drpTimeZone.SelectedItem.Text.Length > 0)
        {
           
           
            if (loggedInUserInfo.UserId > 0)
            {
                //bool test = usrCor.setUserTimeZone(AuthenticatedUser.USER_ID, lt.Text, Convert.ToInt32(lt.Value));

                var userServices = new UsersService();               
                osm_user user = new osm_user();
                user.USERID = loggedInUserInfo.UserId;
           
                user.TIME_ZONE = drpTimeZone.SelectedItem.Text;
                user.STANDARD_BIAS = minutes; //y
                user.UPDATES_FLAG = Convert.ToInt32(chkInformation.Checked);
                
                int retValue = userServices.UserTimeZone(user);
                if (loggedInUserInfo.Reset == 1 && Request.QueryString["accountType"] == null)
                {
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserChangePasswordPageURL(), "surveyFlag=" + base.SurveyFlag));
                } else 
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + base.SurveyFlag));
            }
        }

        
    }
}