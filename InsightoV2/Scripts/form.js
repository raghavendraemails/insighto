

var checkboxHeight = "25";
var radioHeight = "25";
var selectWidth = "";


/* No need to change anything after this */
document.write('<style type="text/css">input[type="checkbox"],input[type="radio"] { display: none; } .disabled { opacity: 0.5; filter: alpha(opacity=50); }</style>');

var Custom = {
    init: function () {
        noBack();
        var inputs = document.getElementsByTagName("input"), span = Array(), textnode, option, active;
        for (a = 0; a < inputs.length; a++) {
            if ((inputs[a].type == "checkbox" || inputs[a].type == "radio")) {
                span[a] = document.createElement("span");
                span[a].className = inputs[a].type;
                //                if ($(".isPageValidClass").val() == "Valid")
                //                    inputs[a].checked = false;

                if (inputs[a].checked == true) {
                    if (inputs[a].type == "checkbox") {
                        position = "0 -" + (checkboxHeight * 2) + "px";
                        span[a].style.backgroundPosition = position;
                    } else {
                        position = "0 -" + (radioHeight * 2) + "px";
                        span[a].style.backgroundPosition = position;
                    }
                }
                inputs[a].parentNode.insertBefore(span[a], inputs[a]);
                inputs[a].onchange = Custom.clear;
                if (!inputs[a].getAttribute("disabled")) {
                    span[a].onmousedown = Custom.pushed;
                    span[a].onmouseup = Custom.check;
                } else {
                    span[a].className = span[a].className += " disabled";
                }
            }
        }

        document.onmouseup = Custom.clear;
    },
    pushed: function () {
        element = this.nextSibling;
        if (element.checked == true && element.type == "checkbox") {
            this.style.backgroundPosition = "0 -" + checkboxHeight * 3 + "px";
        } else if (element.checked == true && element.type == "radio") {
            this.style.backgroundPosition = "0 -" + radioHeight * 3 + "px";
        } else if (element.checked != true && element.type == "checkbox") {
            this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
        } else {
            this.style.backgroundPosition = "0 -" + radioHeight + "px";
        }
    },
    check: function () {
        element = this.nextSibling;
        if (element.checked == true && element.type == "checkbox") {
            this.style.backgroundPosition = "0 0";
            element.checked = false;
            inputs = document.getElementsByTagName("input");
            group = this.nextSibling.name;
            for (a = 0; a < inputs.length; a++) {
                if (inputs[a].name == group && inputs[a].name != this.nextSibling) {
                    var txtothervalue = inputs[a + 1].name;
                    var txtotherelement = document.getElementsByName(txtothervalue);
                    if (txtotherelement.length > 0) {
                        txtotherelement[0].value = "";
                        //   txtotherelement[0].visible = false;
                    }
                }
            }

        }
        else if (element.checked == true && element.type == "radio") {
            this.style.backgroundPosition = "0 0";
            element.checked = false;
        }
        else {
            if (element.type == "checkbox") {
                this.style.backgroundPosition = "0 -" + checkboxHeight * 2 + "px";
                inputs = document.getElementsByTagName("input");
                group = this.nextSibling.name;
                for (a = 0; a < inputs.length; a++) {
                    if (inputs[a].name == group && inputs[a].name != this.nextSibling) {
                        var txtothervalue = inputs[a + 1].name;
                        var txtotherelement = document.getElementsByName(txtothervalue);
                        if (txtotherelement.length > 0) {

                            //  txtotherelement[0].visible = true;
                            txtotherelement[0].focus();
                        }
                    }
                }
            } else {
            
                this.style.backgroundPosition = "0 -" + radioHeight * 2 + "px";
                group = this.nextSibling.name;
                var replacegroupname = group.replace("rbAnswersList", "");
                inputs = document.getElementsByTagName("input");
              
                for (a = 0; a < inputs.length; a++) {
                    if (inputs[a].name == group && inputs[a] != this.nextSibling) {

                        var n = group.search("rbAnswersList");

                        if (n > 0) {

                            var txtothervalue = replacegroupname + "txtOther";
                           
                            var splitVal = element.defaultValue.split("_").reverse();
                         
                            answerOptionId = splitVal[0];
                          
                            //  if (element.defaultValue == "Other") {
                            if (answerOptionId == 0) {
                               
                                var txtotherelement = document.getElementsByName(txtothervalue);

                                txtotherelement[0].disabled = false;
                                txtotherelement[0].focus();


                            }
                            else {
                                var txtotherelement = document.getElementsByName(txtothervalue);
                                if (txtotherelement.length > 0) {
                                    txtotherelement[0].value = "";
                                    txtotherelement[0].disabled = true;
                                }


                            }
                        }
                        inputs[a].previousSibling.style.backgroundPosition = "0 0";
                    }

                }
            }
            element.checked = true;
        }
    },
    clear: function () {
        inputs = document.getElementsByTagName("input");
        for (var b = 0; b < inputs.length; b++) {
            if (inputs[b].type == "checkbox" && inputs[b].checked == true) {
                inputs[b].previousSibling.style.backgroundPosition = "0 -" + checkboxHeight * 2 + "px";
            } else if (inputs[b].type == "checkbox" && inputs[b].className == "styled") {
                inputs[b].previousSibling.style.backgroundPosition = "0 0";
            } else if (inputs[b].type == "radio" && inputs[b].checked == true) {
                inputs[b].previousSibling.style.backgroundPosition = "0 -" + radioHeight * 2 + "px";
            } else if (inputs[b].type == "radio") {
                inputs[b].previousSibling.style.backgroundPosition = "0 0";
            }
        }
    },
    choose: function () {
        option = this.getElementsByTagName("option");
        for (d = 0; d < option.length; d++) {
            if (option[d].selected == true) {
                document.getElementById("select" + this.name).childNodes[0].nodeValue = option[d].childNodes[0].nodeValue;
            }
        }
    }
}
//window.onload = Custom.init;