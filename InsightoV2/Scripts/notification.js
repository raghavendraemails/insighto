$(document).ready(function () {

    $('.slideContent').css({ left: $(window).width(), top: $(window).height() - ($('.slideContent').height()) });
    var desiredLeft = $(window).width() - ($('.slideContent').width());

    if ($.cookie('notification') == null) {
        $('.slideContent').animate({ 'left': desiredLeft },
         {
             duration: 3000,
             complete: function () {
                  $.cookie('notification', ['ReportsDemo', $(location).attr('pathname')], { expires: 7, path: '/' });
              //   $.cookie('notification', ['ReportsDemo', $(location).attr('pathname')], { expires: 0, path: '/' });
                 // alert("Setting Notification: " + $.cookie('notification'));

                 var _gaq = _gaq || [];
                 _gaq.push(['_setAccount', 'UA-29026379-1']);
                 _gaq.push(['_setDomainName', 'insighto.com']);
                 _gaq.push(['_setAllowLinker', true]);
                 _gaq.push(['_trackPageview']);

                 (function () {
                     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                     ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                     // alert("In GA function");
                 })();
             }
        });
    }

    $('.plusIcon').click(function () {
        $('.slideContent').animate({ 'left': $(window).width() }, 3000);
        $('.slideContent').hide("slow");
    });

});
