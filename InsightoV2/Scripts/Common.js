﻿// JScript File

//Constants
var WEGO_SHOWHIDE_PANELS="WegoShowHidePanels";
var MANDATORY_VALIDATION_MESSAGE="MandatoryValidationMessage";
var INTEGER_VALIDATION_MESSAGE="IntegerValidationMessage";
var NUMERIC_VALIDATION_MESSAGE="NumericValidationMessage";
var ALPHABET_VALIDATION_MESSAGE="AlphabetValidationMessage";
var EMAIL_VALIDATION_MESSAGE="EmailValidationMessage";
var DATE_VALIDATION_MESSAGE="DateValidationMessage";
var TIME_VALIDATION_MESSAGE="TimeValidationMessage";
var DIGIT_VALIDATION_MESSAGE="DigitValidationMessage";
var ISACTIV="Are you sure active";
var ISINACTIVE="Are you sure inactive";
var PHONENO_VALIDATION_MESSAGE="PhoneNumberValidationMessage";

//Validates the parameter value and returns a boolean value. 
//true - if the parameter value is not empty
//false - if the parameter value is empty
function IsEmpty(value)
{
    var regExp = /\s*((\S+\s*)*)/;
    var resultValue=value.replace(regExp, "$1");
    
    if(resultValue.length>0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

// replace the &amp; to &
function AmpReplace(text)
        {
          return text.replace(/\&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
        } 
        //end of replacing amp
// if obj value is one return InActive message else return Active Message -- Added by sumanth kuamr
function IsActiveOrInActive(obj)
{
    var value = obj;
    
    if (obj == 1)
    {
    return ISINACTIVE;
    }
    else if (obj == 0)
    {
        return ISACTIV;
    }
}
// validation for exclude other files
function IsImageFile(value)
{
  //var regExp=/^\S+\.(gif|jpg|jpeg|bmp|png)$/
  
  value=value.toLowerCase();
  
  var imageType=value.substring(value.lastIndexOf('.'));
 
  if(imageType=='.jpg' || imageType=='.jpeg' || imageType=='.gif' || imageType=='.bmp' || imageType=='.png')
  {
    return true;
  }
  else
  {
    return false;
  }
}


//Email Validation. Validates the parameter value and returns a boolean value
//true - if the validation is successful.
//false - if the validation fails
function IsValidEmail(value)
{
    var regExp=/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
    
    if(value.search(regExp)==-1)
    {
        return false;
    }
    else
    {
        return true;
    }
}

//Textbox Value Validation. validates the paramete value and returns a boolena value
//true-if the validation is successful.
//fase-if the validation fails.
function IsValidDigit(value)
{
   if(IsEmpty(value)==true)
   {
    return true;
   }
    if(parseInt(value)>99999999.99)
    {
        return false;
        
    }
    else
    {
        return true;
    }
}

//Validating alphabets only Returns true if the given value is alphabet only else false
function IsValidAlphabet(alphabetValue)
{
    var regExp=/^\D*$/;
    
    if (IsEmpty(alphabetValue)==true)
    {
        return true;
    }
    
    if(alphabetValue.search(regExp)==-1)
    {
        return false;
    }
    else
    {
        return true;
    }
}

//Validating integers Returns true if the given value is an integer else false
function IsValidInteger(intValue)
{
    var regExp=/^\d*$/;
    
    if (IsEmpty(intValue)==true)
    {
        return true;
    }
    
    if(intValue.search(regExp)==-1)
    {
        return false;
    }
    else
    {
        return true;
    }
}

//Validating numerics(i.e allows only one decimal) Returns true if the given value is numeric else false
function IsValidNumeric(numericValue)
{
    var regExp=/(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;
    
    if (IsEmpty(numericValue)==true)
    {
        return true;
    }
    
    if(numericValue.search(regExp)==-1)
    {
        return false;
    }
    else
    {
        return true;
    }
}





