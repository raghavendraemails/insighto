
// manually hide navigation overlay
function hideOverlay(overlayToHide, navToHide) {
    document.getElementById(overlayToHide).style.display = 'none';
    document.getElementById(navToHide).style.backgroundColor = 'transparent';
}

function ApplyFrameBox(link) {
    var frameWidth = $(link).attr('w');
    var frameHeight = $(link).attr('h');
    var scrolling = $(link).attr('scrolling');
    var href = $(link).attr('href');
    if (!scrolling) {
        scrolling = 'no';
    }

    //$.facebox($('<iframe id="modalFrame" name="modalFrame" src="' + href + '" width="' + frameWidth + '" height="' + frameHeight + '" frameborder="0" scrolling="' + scrolling + '"></iframe>'));
    $.facebox($('<iframe id="modalFrame" name="modalFrame" src="' + href + '" width="auto" height="auto" frameborder="0" scrolling="' + scrolling + '"></iframe>'));

    return false;
}

function ApplyFrameBoxWithLink(link, href) {
    var frameWidth = $(link).attr('w');
    var frameHeight = parseInt($(link).attr('h')) ;
    var scrolling = $(link).attr('scrolling');
    //var href = $(link).attr('datalink');
    if (!scrolling) {
        scrolling = 'no';
    }

    //console.log(href);
    $.facebox($('<iframe id="modalFrame" name="modalFrame" src="' + href + '" width="' + frameWidth + '" height="' + frameHeight + '" frameborder="0" scrolling="no" style="background-color:white;" allowtransparency="true"></iframe>'));
    
    return false;
}

function OpenModalPopUP(path, width, height, scrolling) {
    $.facebox($('<iframe id="modalFrame" name="modalFrame" src="' + path + '" width="' + width + '" height="' + height + '" frameborder="0" scrolling="' + scrolling + '"></iframe>'));
}


// closes active modal
function closeModalSelf(doParentRefresh, pageTobeRefreshedURL) {
    // ensure we are in a modal
    //console.log(doParentRefresh);
    if (parent.$) {
        parent.$('#facebox .close').click(); // we are using facebox
        if (doParentRefresh == true) {
            if (pageTobeRefreshedURL == undefined || pageTobeRefreshedURL == '') {
                //parent.window.location.reload(true);
                parent.window.location.href = parent.window.location.href;  // navigate instead of reload removes Resend dialog issue
            } else {
                parent.window.location.href = pageTobeRefreshedURL;
            }

        }
        else {
            if (doParentRefresh == "true") {
                parent.window.location.href = pageTobeRefreshedURL;
            }
        }
    }
}

//close Modal  after 5 sec
function closeModelAfterFive(doParentRefresh, pageTobeRefreshedURL, timeOutValue) {
    setTimeout("closeModalSelf('" + doParentRefresh + "', '" + pageTobeRefreshedURL + "')", timeOutValue); // milliseconds
}

$(document).ready(function () {

    $('a[rel*=framebox]').click(function (e) {
        e.preventDefault();
        ApplyFrameBox($(this));
    });

    //grid pagination controls tooltips
    $("#first_ptoolbar").attr('title', 'First');
    $(".jqGrid #prev_ptoolbar").attr('title', 'Previous');

    //Right Side buttons
    $("#next_ptoolbar").attr('title', 'Next');
    $("#last_ptoolbar").attr('title', 'Last');
});

function ClipBoard(objectId) {
    var sourceObject = document.getElementById(objectId.id)
    var holdtext = sourceObject.value;
    if (holdtext == "") {
        sourceObject.innerText = "No Url found";
    }
    Copied = sourceObject.createTextRange();
    Copied.execCommand("Copy");
}