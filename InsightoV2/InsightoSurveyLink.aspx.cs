﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.Enumerations;

public partial class InsightoSurveyLink : SurveyPageBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            string review_url = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewLinkURL(), Constants.SURVEYID + "=" + SurveyID + "&Mode=" + RespondentDisplayMode.PreviewWithPagination.ToString() + "&surveyFlag=" + SurveyFlag);
            txtSurveyUrl.Text = ConfigurationManager.AppSettings["RootURL"].ToString() + review_url;
        }
    }
}
