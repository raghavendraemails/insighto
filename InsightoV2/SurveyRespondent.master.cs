﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;

public partial class SurveyRespondent : MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        var htQueryStrings = EncryptHelper.DecryptQuerystringParam(Request.QueryString["Key"].Replace(' ', '+'));
        if (htQueryStrings != null)
        {
            var surveyId = ValidationHelper.GetInteger(htQueryStrings["SurveyId"], 0);
            if (surveyId <= 0) return;
            var surveyBasicView = ServiceFactory.GetService<RespondentService>().GetSurveyViewInformation(surveyId);

            // A stylsheet for theme
            if (surveyBasicView == null) return;
            var themeUrl = PathHelper.GetThemePath(Utilities.ToEnum<ThemeType>(surveyBasicView.THEME.Replace(" ", "")));
            var hlnkStylesheet = new HtmlLink();
            Page.Header.Controls.Add(hlnkStylesheet);
            hlnkStylesheet.Attributes.Add("rel", "Stylesheet");
            hlnkStylesheet.Attributes.Add("type", "text/css");
            hlnkStylesheet.Href = "~/" + themeUrl;
            hdnBrowserBack.Value =Convert.ToString(surveyBasicView.BROWSER_BACKBUTTON); 
           
            // A stylsheet for progress bar
            if (surveyBasicView.Show_Progressbar == true)
            {
                themeUrl = PathHelper.GetProgressBarPath(Utilities.ToEnum<ProgressBarType>(surveyBasicView.Progressbar_Type.Replace(" ", "")));
                hlnkStylesheet = new HtmlLink();
                Page.Header.Controls.Add(hlnkStylesheet);
                hlnkStylesheet.Attributes.Add("rel", "Stylesheet");
                hlnkStylesheet.Attributes.Add("type", "text/css");
                hlnkStylesheet.Href = "~/" + themeUrl;
            }

            //added this inline to avoid unnecessary space causing popupBody class.
            if (htQueryStrings["Mode"] == null || (htQueryStrings["Mode"] != null && Utilities.ToEnum<RespondentDisplayMode>(htQueryStrings["Mode"].ToString()) == RespondentDisplayMode.Respondent))
            {
                resMobile.Attributes.Add("class", "respondentBg");
            }
            else
            {
                resMobile.Attributes.Add("class", "popupBody");
            }
            if ((surveyId == 10964) || (surveyId == 11028))
            {
                divFooter.Visible = false;
                resMobile.Style.Add("overflow", "hidden");  //removed scrollbar on request from customer
            }
        }
        base.OnInit(e);
    }
      
    protected void Page_Load(object sender, EventArgs e)
    {
      
        if (!IsPostBack)
        {
            lnkFavIcon.Href = "http://" + Request.Url.Authority + "/insighto.ico";
        }
    }
}
