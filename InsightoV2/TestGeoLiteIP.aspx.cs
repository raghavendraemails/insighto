﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.geoip;
using System.Text;
using System.Configuration;
using Insighto.Data;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Net;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Serialization;
public partial class TestGeoLiteIP : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string filepath = Server.MapPath(PathHelper.GetGeoliteDBFileWithPath());
        LookupService lookupservice = new LookupService(filepath);
        Location location = new Location();
        location = lookupservice.getLocation("117.221.60.67");
        lbl1.Text = location.countryCode;
        lbl2.Text = location.city;
        lbl3.Text = location.regionName;
    }
}