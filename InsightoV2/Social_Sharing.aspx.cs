﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.IO.Compression;
using System.Collections.Specialized;
using System.Net;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Web.Services;
using TweetSharp;
using TweetSharp.Model;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using System.Configuration;
using Insighto.Business.ValueObjects;
using App_Code;

public partial class Default3 : System.Web.UI.Page
{
    string twitterlink = ConfigurationManager.AppSettings["twitterurl"];
    string linkedinlink = ConfigurationManager.AppSettings["linkedinurl"];
    LoggedInUserInfo loggedInUserInfo;
    public string strDiscAmt = "";
    public int lnkuserid;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionStateService sessionStateService = new SessionStateService();
        loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

       // lnkuserid = loggedInUserInfo.UserId;

        string strtweetval = Request.Form["twtsharehidden"];
      //  string strtweetval = twtsharehidden.Value;
        //TwitterConnect.API_Key = "jHfU2n3NKkoo694tGLP5aw";
        //TwitterConnect.API_Secret = "sgMFc1MycDfb1KwBKpyEO8aLCZAjhYxZ3scpniHNhk";


        string ipAddress = "";

        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
        {
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        }


        var country = Utilities.GetCountryName(ipAddress);

        if (country.ToUpper() == "INDIA")
        {
            strDiscAmt = "&#8377; 1666";
        }
        else
        {
            strDiscAmt = "US $40";
        }

        NameValueCollection pColl = Request.Params;

       
        
        for (int i = 0; i <= pColl.Count - 1; i++)
        {
            if (pColl.GetKey(i) == "validationresult")
            {
              //  this.lblValidationResult.Text = pColl.Get(i);
                return;
            }
        }

    }
    public void OnConfirm(object sender, EventArgs e)
    {
        string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "Yes")
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Tweeted Successfully!')", true);
        }
        else
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked NO!')", true);
        }
    }
    protected void btnShareTwitter_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            

            string oauth_consumer_key = "jHfU2n3NKkoo694tGLP5aw";
            string oauth_consumer_secret = "sgMFc1MycDfb1KwBKpyEO8aLCZAjhYxZ3scpniHNhk";

            TwitterClientInfo twitterClientInfo = new TwitterClientInfo();
            twitterClientInfo.ConsumerKey = oauth_consumer_key; //Read ConsumerKey out of the app.config
            twitterClientInfo.ConsumerSecret = oauth_consumer_secret; //Read the ConsumerSecret out the app.config
            TwitterService twitterService = new TwitterService(twitterClientInfo);

            //Firstly we need the RequestToken and the AuthorisationUrl
            OAuthRequestToken requestToken = twitterService.GetRequestToken();
            Uri authUrl = twitterService.GetAuthorizationUri(requestToken, twitterlink);

            Response.Redirect(authUrl.AbsoluteUri, false);
        }
    }

    protected void btnShareIn_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string inAuthDlgRedirect = "https://www.linkedin.com/uas/oauth2/authorization?response_type=code";
            string api_client_id = "8f7rrkzepihs";
            string state = Convert.ToBase64String(new System.Text.ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            string redirect_uri = linkedinlink;

            string inRedirectURL = inAuthDlgRedirect + '&' + "client_id=" + api_client_id + '&' +
                                    "state=" + state + '&' + "redirect_uri=" + redirect_uri;

            Response.Redirect(inRedirectURL);
        }
    }

    [WebMethod]
    public static string MyMethod(string name)
    {
        string[] parts = name.Split(new string[] { ";" }, StringSplitOptions.None);
        // Update user account to allow premium trial
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();     
  
        

        NetworkShareService NSS = new NetworkShareService();
        NetworkShareCodes NSC = new NetworkShareCodes();
        if (parts[0].ToString() == "TWITTER_USER")
        {
             NSC = NSS.AddNetworkTrial(userInfo.UserId, "Twitter", parts[0], Convert.ToInt32(parts[1]), 15);
        }
        else
        {
             NSC = NSS.AddNetworkTrial(userInfo.UserId, "Facebook", parts[0], Convert.ToInt32(parts[1]), 15);
        }

        UsersService userService = new UsersService();
        var user = userService.FindUserByUserId(userInfo.UserId);
        ServiceFactory.GetService<SessionStateService>().SaveUserToSession(user);

        string result = "";
        if (NSC == NetworkShareCodes.Reshared)
            if (parts[0].ToString() == "TWITTER_USER")
            {
                result = "errorcredit;" + "You have already availed of our premium offer with your earlier Twitter share";
            }
            else
            {
                result = "errorcredit;" + "You have already availed of our premium offer with your earlier Facebook share";
            }
        else if (NSC == NetworkShareCodes.Error)
            result = "error;" + "An error has occured, please try again.";
        else
        {
            if (parts[0].ToString() == "TWITTER_USER")
            {
                 result = "success;" +
                         "<p>Your update is shared on Twitter successfully<br /><br /><h2>Congratulations!</h2><br />You have won yourself " +
                         "<b>15</b> days of Premium subscription.<br /><br />Details sent through email.</p>";
            }
            
            else
            {
                result = "success;" +
                         "<p>Your update is shared on Facebook successfully<br /><br /><h2>Congratulations!</h2><br />You have won yourself " +
                         "<b>15</b> days of Premium subscription.<br /><br /Details sent through email.</p>";
            }
        }

        return result;
    }

    protected void btnnothanks_Click(object sender, EventArgs e)
    {
        
        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=0"));
    }
    protected void btnproceed_Click(object sender, EventArgs e)
    {
        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=0"));
    }
   
}