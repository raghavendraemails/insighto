﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using System.Text;
using System.Collections;
using Insighto.Business.Helpers;
using Insighto.Data;
using System.Data.OleDb;
using System.IO;
using Insighto.Business.Enumerations;
using System.Xml;
using System.Data;
using App_Code;
using ExportToExcel;
using Insighto.Business.ValueObjects;

namespace Insighto.Pages
{
    public partial class CreateEmailList : BasePage
    {
        private osm_contactlist _contactList = null;
        private string _listId = "0";
        public osm_contactlist ContactList
        {
            get
            {
                if (Request.QueryString["key"] != null)
                {
                    Hashtable ht = new Hashtable();

                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);

                    if (_contactList == null)
                    {
                        if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                        {
                            int ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                            var contactListService = ServiceFactory.GetService<ContactListService>();
                            int contacTListId = ValidationHelper.GetInteger(ContactId, 0);
                            _contactList = contactListService.FindByContactListID(contacTListId);

                        }

                        else
                        {
                            var sessionService = ServiceFactory.GetService<SessionStateService>();
                            var userInfo = sessionService.GetLoginUserDetailsSession();
                            var contactListService = ServiceFactory.GetService<ContactListService>();
                            var list = contactListService.GetListByUserId(userInfo.UserId);
                            if (list.Count > 0 && _listId != "0")
                            {
                                _contactList = list[0];
                            }
                            else
                                _contactList = null;
                        }

                    }

                }
                return _contactList;
            }
        }

        /// <summary>
        /// Gets the headers.
        /// </summary>
        public string Headers
        {
            get
            {
                var colFormat = ",'{0}'";
                var headerBuilder = new StringBuilder();
                headerBuilder.Append("[ 'EMAIL ID', ' ', 'FIRST NAME ', ' ',' LAST NAME '");
                if (ContactList != null)
                {
                    if (!String.IsNullOrEmpty(ContactList.CUSTOM_HEADER1))
                    {
                        if (!String.IsNullOrEmpty(ContactList.CUSTOM_HEADER1.Trim()))
                            headerBuilder.AppendFormat(colFormat, ContactList.CUSTOM_HEADER1);
                    }

                    if (!String.IsNullOrEmpty(ContactList.CUSTOM_HEADER2))
                    {
                        if (!String.IsNullOrEmpty(ContactList.CUSTOM_HEADER2.Trim()))
                            headerBuilder.AppendFormat(colFormat, ContactList.CUSTOM_HEADER2);
                    }                    
                }
                headerBuilder.Append(", '','']");

                return headerBuilder.ToString();
            }
        }

        /// <summary>
        /// Gets the columns.
        /// </summary>
            

        public string Columns
        {
            get
            {
                var colFormat = "  name: '{0}', index: '{0}', width: '100%', align: 'left' ";
                var columnBuilder = new StringBuilder();
                columnBuilder.Append(" [");
                columnBuilder.Append("{ name: 'EMAIL_ADDRESS', index: 'EMAIL_ADDRESS', width: 200, align: 'left',resizable: false},");
                columnBuilder.Append("{ name: '', index: '', width: 5, align: 'left', resizable:false, sortable:false,resizable: false },");
                columnBuilder.Append("{ name: 'FIRST_NAME', index: 'FIRST_NAME', width: 100, align: 'left',resizable: false},");
                columnBuilder.Append("{ name: '', index: '', width: 5, align: 'left' , resizable:false, sortable:false,resizable: false},");
                columnBuilder.Append("{ name: 'LAST_NAME', index: 'LAST_NAME', width: 100, align: 'left',resizable: false },");
                
                if (ContactList != null)
                {
                    if (!String.IsNullOrEmpty(ContactList.CUSTOM_HEADER1))
                    {
                        if (!String.IsNullOrEmpty(ContactList.CUSTOM_HEADER1.Trim()))
                            columnBuilder.Append("{").AppendFormat(colFormat, "CUSTOM_VAR1").Append("},");
                    }

                    if (!String.IsNullOrEmpty(ContactList.CUSTOM_HEADER2))
                    {
                        if (!String.IsNullOrEmpty(ContactList.CUSTOM_HEADER2.Trim()))
                            columnBuilder.Append("{").AppendFormat(colFormat, "CUSTOM_VAR2").Append("},");
                    }
                }

                columnBuilder.Append("{ name: 'EditUrl', index: 'EditUrl', width: 20, align: 'right', formatter: EditLinkFormatter,resizable: false },");
                columnBuilder.Append("{ name: 'EMAIL_ID', index: 'EMAIL_ID', width: 20, align: 'right', formatter: DeleteLinkFormatter,resizable: false }, ]");
                return columnBuilder.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the list id.
        /// </summary>
        /// <value>
        /// The list id.
        /// </value>
        public string ListId
        {
            get
            {
                if (Request.QueryString["key"] != null)
                {
                    Hashtable ht = new Hashtable();

                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                    {
                        int ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                        _listId = ContactId.ToString();

                    }
                }
                return _listId;
            }
            set
            {
                _listId = value;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            divValidators.Controls.Add(GetCustomValidator());
            base.OnInit(e);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
           
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var featureService = ServiceFactory.GetService<FeatureService>();
            var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
            var listFeatures = featureService.Find(GenericHelper.ToEnum<UserType>(userInfo.LicenseType));
            hlnkManageEmail.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetAddressBookURL(), "surveyFlag=" + SurveyFlag);
            //hlnkBack.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetAddressBookURL(), "surveyFlag=" + SurveyFlag);
            if (Request.QueryString["key"] != null)
            {
                Hashtable ht = new Hashtable();
                var contactService = ServiceFactory.GetService<ContactListService>();

                int ContactId = 0;
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                {
                    ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                    string editListUrl = EncryptHelper.EncryptQuerystring("ContactList.aspx", "ContactId=" + ContactId + "&surveyFlag=" + SurveyFlag);

                }
                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    int SurvyeId = ValidationHelper.GetInteger(ht[Constants.SURVEYID].ToString(), 0);
                    Session["ReturnSurveyId"] = SurvyeId.ToString();
                    btnBacktoLaunchPage.Visible = true;
                }
                else
                {
                    btnBacktoLaunchPage.Visible = false;
                }

                if (ContactId > 0)
                {
                    var contact = contactService.FindContactsByContactId(ContactId);
                    lblEmailListText.Text = contact.CONTACTLIST_NAME;
                    lblEmailList.Text = "Email List: " + contact.CONTACTLIST_NAME;
                }
               

                    string strAddColumns = EncryptHelper.EncryptQuerystring("AddAddressBookColumns.aspx", "ContactId=" + ContactId + "&surveyFlag=" + SurveyFlag);
                    string strCopyList = EncryptHelper.EncryptQuerystring("CopyContactList.aspx", "ContactId=" + ContactId + "&surveyFlag=" + SurveyFlag);
                    string strUpload = EncryptHelper.EncryptQuerystring("UploadExcelCSV.aspx", "ContactId=" + ContactId + "&surveyFlag=" + SurveyFlag);
                    string strNewEmail = EncryptHelper.EncryptQuerystring("EmailListPopUp.aspx", "ContactId=" + ContactId + "&surveyFlag=" + SurveyFlag);
                    hlnkCopy.NavigateUrl = strCopyList;
                    hlnkNewEmail.NavigateUrl = strNewEmail;

                    //if (featureService.Find(licenceType, FeatureDeploymentType.EXCEL_UPLOAD).Value == 0 || featureService.Find(licenceType, FeatureDeploymentType.CSV_UPLOAD).Value == 0)
                    //{
                    //    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Upgrade", "OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');", true);
                    //    hlnkUpload.NavigateUrl = "Upgradelicense.aspx";
                    //    liUpload.Attributes.Add("class", "proLinkHyper");
                    //}
                    //else
                    //{
                        hlnkUpload.NavigateUrl = strUpload;
                        liUpload.Attributes.Remove("class");

                   // }

                    if (Session["ReturnSurveyId"] != null)
                    {
                        btnBacktoLaunchPage.Visible = true;
                    }
                    else
                        btnBacktoLaunchPage.Visible = false;

                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);

                    if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                    {
                        ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                    }
                    else
                    {
                        ContactId = ValidationHelper.GetInteger(drpList.SelectedValue, 0);
                    }


                    string uploadUrl = EncryptHelper.EncryptQuerystring("~/CreateEmailList.aspx", "ContactId=" + ContactId + "&Control=Upload&surveyFlag=" + SurveyFlag);
                    hlnkUploadLink.NavigateUrl = uploadUrl;
                    string copylistUrl = EncryptHelper.EncryptQuerystring("~/CreateEmailList.aspx", "ContactId=" + ContactId + "&Control=CopyList&surveyFlag=" + SurveyFlag);
                    hlnkCopyLink.NavigateUrl = copylistUrl;
                    string addUrl = EncryptHelper.EncryptQuerystring("~/CreateEmailList.aspx", "ContactId=" + ContactId + "&Control=AddEmail&surveyFlag=" + SurveyFlag);
                    hlnkCreateEmail.NavigateUrl = addUrl;
                }
            

            else
            {
                string uploadUrl = EncryptHelper.EncryptQuerystring("~/CreateEmailList.aspx", "Control=Upload&surveyFlag=" + SurveyFlag);
                hlnkUploadLink.NavigateUrl = uploadUrl;
                string copylistUrl = EncryptHelper.EncryptQuerystring("~/CreateEmailList.aspx", "Control=CopyList&surveyFlag=" + SurveyFlag);
                hlnkCopyLink.NavigateUrl = copylistUrl;
                string addUrl = EncryptHelper.EncryptQuerystring("~/CreateEmailList.aspx", "Control=AddEmail&surveyFlag=" + SurveyFlag);
                hlnkCreateEmail.NavigateUrl = addUrl;
                btnBacktoLaunchPage.Visible = false;
            }


            if (!IsPostBack)
            {
                SetProFeatures();
                BindEmailList();
            }
            if (drpList.SelectedIndex <= 0)
            {
                tblExcel.Visible = false;
            }
            if (drpList.Items.Count < 2)
            {
                //hlnkAddList.Visible = true;
                // pnlAddressBook.Visible = false;
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Upgrade", "OpenModalPopUP('" + EncryptHelper.EncryptQuerystring("ContactList.aspx", "surveyFlag=" + SurveyFlag) + "', 650, 250, 'yes');", true);

            }
            //else
            //{
            //    hlnkAddList.Visible = false;
            //    pnlAddressBook.Visible = true;
            //}
            lblEmailListText.Text = drpList.SelectedIndex > 0 ? drpList.SelectedItem.Text : "";
            //lblEmailList.Text = drpList.SelectedIndex > 0 ? drpList.SelectedItem.Text : "";
            LoadControls();
        }

        /// <summary>
        /// Sets the pro features.
        /// </summary>
        private void SetProFeatures()
        {
            if (!Utilities.ShowOrHideFeature(Utilities.GetUserType(), (int)FeatureTypes.SurveyCreation.Email_List_Export))
            {
                btnGo.Attributes.Add("onclick", "javascript:OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "',690, 450, 'yes');return false;");
               // chkBrowserBackButton.Checked = false;
            }
        }

        /// <summary>
        /// Handles the Validate event of the DrpList control.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="args">The <see cref="System.Web.UI.WebControls.ServerValidateEventArgs"/> instance containing the event data.</param>
        protected void DrpList_Validate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = drpList.SelectedIndex > 0;
        }

        /// <summary>
        /// Gets the custom validator.
        /// </summary>
        /// <returns></returns>
        private CustomValidator GetCustomValidator()
        {

            var revTemp = new CustomValidator
            {
                CssClass = "lblRequired",
                ID = "reqFormat",
                ErrorMessage = Utilities.ResourceMessage("CreatEmail_Validation_Select"), //"Please select a list.",
                Display = ValidatorDisplay.Dynamic,
                Visible = true
            };
            revTemp.ServerValidate += new ServerValidateEventHandler(DrpList_Validate);
            if (revTemp.IsValid)
            {
                tblExcel.Visible = true;
            }
            return revTemp;
        }
        
        /// <summary>
        /// Binds the email list.
        /// </summary>
        private void BindEmailList()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var contactService = ServiceFactory.GetService<ContactListService>();
            var list = contactService.GetListByUserId(userInfo.UserId);
            if (list != null)
            {
                drpList.DataSource = list;
                drpList.DataTextField = "CONTACTLIST_NAME";
                drpList.DataValueField = "CONTACTLIST_ID";
                drpList.DataBind();
                if (ListId != null)
                {

                    drpList.SelectedValue = ListId.ToString();
                }

            }
            drpList.Items.Insert(0, new ListItem("--Select--", "0"));

        }

        /// <summary>
        /// Loads the controls.
        /// </summary>
        public void LoadControls()
        {
            if (Request.QueryString["key"] != null)
            {
                Hashtable ht = new Hashtable();


                int ContactId = 0;
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                {
                    ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                    string editListUrl = EncryptHelper.EncryptQuerystring("ContactList.aspx", "ContactId=" + ContactId + "&surveyFlag=" + SurveyFlag);

                }
                string Control = string.Empty;

                if (ht != null && ht.Count > 0 && ht.Contains("Control"))
                {
                    Control = ht["Control"].ToString();
                }


                if (Control == "AddEmail" || Control == "" || Control == null)
                {
                    var addControl = Page.LoadControl("UserControls/AddNewEmail.ascx");
                    pnlAddressBook.Controls.Add(addControl);
                    lblTitle.Text = Utilities.ResourceMessage("CreateEmail_Title_AddOneByOne"); //"Add One By One";
                    hlnkCreateEmail.CssClass = "activeLink";
                }

                else if (Control == "CopyList")
                {
                    hlnkCopyLink.CssClass = "activeLink";
                    var addControl = Page.LoadControl("UserControls/CopyContactList.ascx");
                    pnlAddressBook.Controls.Add(addControl);
                    lblTitle.Text = Utilities.ResourceMessage("CreateEmail_Title_AddCSV"); //"Add CSV List";

                }

                else if (Control == "Upload")
                {
                    var addControl = Page.LoadControl("UserControls/UploadExcel.ascx");
                    pnlAddressBook.Controls.Add(addControl);
                    lblTitle.Text = Utilities.ResourceMessage("CreateEmail_Title_Excel"); //"Upload Email list - Excel / CSV";
                    hlnkUploadLink.CssClass = "activeLink";
                }

            }


        }
        
        /// <summary>
        /// Handles the Click event of the btnAddColumn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnAddColumn_Click(object sender, EventArgs e)
        {

            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        if (loggedInUserInfo.LicenseType == "FREE")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
        }
        }

        /// <summary>
        /// Handles the Click event of the btnGo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnGo_Click(object sender, EventArgs e)
        {

            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            //if (loggedInUserInfo.LicenseType == "PRO_YEARLY" || loggedInUserInfo.LicenseType == "PRO_QUATERLY" || loggedInUserInfo.LicenseType == "PRO_MONTHLY")
            //{
              //  ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            //}
            //else
            //{

                int ContactId = 0;
                if (Request.QueryString["key"] != null)
                {
                    Hashtable ht = new Hashtable();
                    var contactService = ServiceFactory.GetService<ContactListService>();


                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                    {
                        ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                    }
                }


                var emailService = ServiceFactory.GetService<EmailService>();
                var emaillist = emailService.FindEmailsByContactListId(ContactId);
                if (rbtnExcel.Checked == true)
                    Export("emaillist", emaillist, ContactId);
                else
                    ExportDataTableToCSV("emaillist", emaillist, ContactId);


           // }
        }

        /// <summary>
        /// Exports the specified file name.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="emailList">The email list.</param>
        /// <param name="ContactId">The contact id.</param>
        public void Export(string fileName, List<osm_emaillist> emailList, int ContactId)
        {
            DataSet dsContacts;
            fileName = GetEmailsIntoTable(fileName, emailList, ContactId, out dsContacts);
            string filePath = Server.MapPath(@"TemplateFolder/Reports/" + fileName);
            CreateExcelFile.CreateExcelDocument(dsContacts, filePath);
            FileInfo file = new FileInfo(filePath);
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name.Replace(" ","_"));
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = Utilities.MimeType(file.Extension);
                Response.WriteFile(file.FullName);
                Response.End();

                file.Delete();
            }
            else
            {
                Response.Write("This file does not exist.");
            }
        }

        /// <summary>
        /// Gets the emails into table.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="emailList">The email list.</param>
        /// <param name="ContactId">The contact id.</param>
        /// <param name="dsContacts">The ds contacts.</param>
        /// <returns></returns>
        private string GetEmailsIntoTable(string fileName, List<osm_emaillist> emailList, int ContactId, out DataSet dsContacts)
        {
            var contactService = ServiceFactory.GetService<ContactListService>();
            var contacts = contactService.FindByContactListID(ContactId);
            fileName = contacts.CONTACTLIST_NAME + ".xls";
            try
            {
                DirectoryInfo dir = new DirectoryInfo(Server.MapPath(@"TemplateFolder/Reports/"));
                foreach (var item in dir.GetFiles())
                {
                    item.Delete();
                }
            }
            catch (Exception) { }

            //get email list into data table
            dsContacts = new DataSet();
            string[] columns = new string[] { "EMAIL_ADDRESS", "FIRST_NAME", "LAST_NAME", "CUSTOM_VAR1", "CUSTOM_VAR2" };
            string[] replaceColumns = new string[] { "Email Address", "First Name", "Last Name", "CUSTOM_VAR1", "CUSTOM_VAR2" };
            DataTable dtTemp = Utilities.ToDataTable(emailList);

            DataTable dtEmails = new DataTable();
            foreach (var columnName in replaceColumns)
            {
                DataColumn dc = new DataColumn();
                dc.ColumnName = columnName;
                dc.DataType = typeof(String);
                dtEmails.Columns.Add(dc);
            }

            int ctr = 0;
            foreach (DataRow row in dtTemp.Rows)
            {
                DataRow dr = dtEmails.NewRow();
                dr[replaceColumns[ctr]] = row[columns[ctr]].ToString();
                ctr++;

                dr[replaceColumns[ctr]] = row[columns[ctr]];
                ctr++;

                dr[replaceColumns[ctr]] = row[columns[ctr]];
                ctr++;

                dr[replaceColumns[ctr]] = row[columns[ctr]];
                ctr++;

                dr[replaceColumns[ctr]] = row[columns[ctr]];

                dtEmails.Rows.Add(dr);
                ctr = 0;
            }
            dtEmails.AcceptChanges();
            dsContacts.Tables.Add(dtEmails);
            return fileName;
        }

        /// <summary>
        /// Exports the data table to CSV.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="emailList">The email list.</param>
        /// <param name="ContactId">The contact id.</param>
        private static void ExportDataTableToCSV(string fileName, List<osm_emaillist> emailList, int ContactId)
        {
            HttpContext context = HttpContext.Current;
            context.Response.Clear();
            var contactService = ServiceFactory.GetService<ContactListService>();
            var contacts = contactService.FindByContactListID(ContactId);
            fileName = contacts.CONTACTLIST_NAME + ".csv";
            StringBuilder sb = new StringBuilder();

            //defining header
            if (!string.IsNullOrEmpty(contacts.CUSTOM_HEADER1) && !string.IsNullOrEmpty(contacts.CUSTOM_HEADER2))
            {
               sb.Append(string.Format("{0},{1},{2},{3},{4}", "Email Address", "First Name", "Last Name", contacts.CUSTOM_HEADER1.Trim(), contacts.CUSTOM_HEADER2.Trim()));
            }
            else if(!string.IsNullOrEmpty(contacts.CUSTOM_HEADER1) && string.IsNullOrEmpty(contacts.CUSTOM_HEADER2))
            {
                sb.Append(string.Format("{0},{1},{2},{3}", "Email Address", "First Name", "Last Name", contacts.CUSTOM_HEADER1.Trim()));
            }
            else
            {
                sb.Append(string.Format("{0},{1},{2}", "Email Address", "First Name", "Last Name"));
            }

            sb.Append(Environment.NewLine);

            //defining data
            foreach (var row in emailList)
            {
                sb.Append(string.Format("{0},{1},{2}", row.EMAIL_ADDRESS.Trim(), !string.IsNullOrEmpty(row.FIRST_NAME) ? row.FIRST_NAME.Trim() : string.Empty,
                   !string.IsNullOrEmpty(row.LAST_NAME) ? row.LAST_NAME.Trim() : string.Empty));

                //check if custom column1 has beed added
                if (!string.IsNullOrEmpty(contacts.CUSTOM_HEADER1) && !string.IsNullOrEmpty(row.CUSTOM_VAR1))
                    sb.Append(string.Format(",{0}", !string.IsNullOrEmpty(row.CUSTOM_VAR1) ? row.CUSTOM_VAR1.Trim() : string.Empty));

                //check if custom column2 has beed added
                if (!string.IsNullOrEmpty(contacts.CUSTOM_HEADER2) && !string.IsNullOrEmpty(row.CUSTOM_VAR2))
                    sb.Append(string.Format(",{0}", !string.IsNullOrEmpty(row.CUSTOM_VAR2) != null ? row.CUSTOM_VAR2.Trim() : string.Empty));

                //row pointer moves to next line
                sb.Append(Environment.NewLine);
            }

            context.Response.ContentType = Utilities.MimeType(Path.GetExtension(fileName));
            context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName.Replace(" ","_") );
            context.Response.Write(sb.ToString());
            context.Response.End();
        }

        /// <summary>
        /// Handles the Click event of the btnBacktoLaunchPage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnBacktoLaunchPage_Click(object sender, EventArgs e)
        {
            Hashtable ht = new Hashtable();
            int ContactId = 0;
            if (Request.QueryString["key"] != null)
            {

                var contactService = ServiceFactory.GetService<ContactListService>();


                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("ContactId"))
                {
                    ContactId = ValidationHelper.GetInteger(ht["ContactId"].ToString(), 0);
                }

                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    int SurveyId = ValidationHelper.GetInteger(ht[Constants.SURVEYID].ToString(), 0);
                    if (SurveyId > 0)
                    {
                        Session["ReturnSurveyId"] = null;
                        string urlstr = "";
                        urlstr = EncryptHelper.EncryptQuerystring("PreLaunchSurveyEmailList.aspx", Constants.SURVEYID + "=" + SurveyId + "&ContID=" + ContactId + "&flag=active&surveyFlag=" + SurveyFlag);
                        Response.Redirect(urlstr);

                    }
                }

            }

            if (Session["ReturnSurveyId"] != null)
            {
                int SurveyID = ValidationHelper.GetInteger(Session["ReturnSurveyId"].ToString(), 0);
                Session["ReturnSurveyId"] = null;
                if (SurveyID > 0)
                {
                    string urlstr = "";
                    if (Session["Flag"] != null)
                        urlstr = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx", "SurveyId=" + SurveyID + "&ContID=" + ContactId + "&Flag=Active&surveyFlag=" + SurveyFlag);
                    else
                        urlstr = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx", "SurveyId=" + SurveyID + "&ContID=" + ContactId + "&surveyFlag=" + SurveyFlag);

                    Session["Flag"] = null;
                    Response.Redirect(urlstr);

                }
            }

        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the drpList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void drpList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                int ContactId = ValidationHelper.GetInteger(drpList.SelectedValue, 0);
                string Control = string.Empty;

                if (ht != null && ht.Count > 0 && ht.Contains("Control"))
                {
                    Control = ht["Control"].ToString();
                }
                string Url = EncryptHelper.EncryptQuerystring("~/CreateEmailList.aspx", "ContactId=" + ContactId + "&Control=" + Control + "&surveyFlag=" + SurveyFlag);
                Response.Redirect(Url);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnBack_Click(object sender, EventArgs e)
        {


            if (Session["ReturnSurveyId"] != null)
            {
                int SurveyID = ValidationHelper.GetInteger(Session["ReturnSurveyId"].ToString(), 0);
                Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetAddressBookURL(), "surveyFlag=" + SurveyFlag + "&SurveyId=" + SurveyID));
            }
            else
            {
                Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetAddressBookURL(), "surveyFlag=" + SurveyFlag));
            }
        }
    }
}