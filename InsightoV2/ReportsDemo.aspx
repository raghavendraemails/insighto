﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true" CodeFile="ReportsDemo.aspx.cs" Inherits="ReportsDemo" %>

    <%@ Register Src="~/UserControls/SingleSeries.ascx" TagName="SingleSeries" TagPrefix="uc" %>
    <%@ Register Src="~/UserControls/MCMS.ascx" TagName="MCMS" TagPrefix="uc1" %>
    <%@ Register Src="~/UserControls/UCYesNo.ascx" TagName="YN" TagPrefix="uc2" %>
     <%@ Register Src="~/UserControls/MenuDD.ascx" TagName="MDD" TagPrefix="uc3" %>
       <%@ Register Src="~/UserControls/MtxSS.ascx" TagName="MtxSS" TagPrefix="uc4" %>
         <%@ Register Src="~/UserControls/MtxMS.ascx" TagName="MtxMS" TagPrefix="uc5" %>
         <%@ Register Src="~/UserControls/MtxSbS.ascx" TagName="MtxSbS" TagPrefix="uc6" %>
             <%@ Register Src="~/UserControls/ConstSum.ascx" TagName="ConstSm" TagPrefix="uc7" %>
             <%@ Register Src="~/UserControls/Ranking.ascx" TagName="Rank" TagPrefix="uc8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <script type="text/javascript" src="FusionCharts/FusionCharts.js"></script>    
   <script type="text/javascript" src="FusionCharts/jquery.min.js"></script>    
   <script type="text/javascript" src="FusionCharts/FusionCharts.jqueryplugin.js"></script>

   <script type="text/javascript">

       var _gaq = _gaq || [];
       _gaq.push(['_setAccount', 'UA-29026379-1']);
       _gaq.push(['_setDomainName', 'insighto.com']);
       _gaq.push(['_setAllowLinker', true]);
       _gaq.push(['_trackPageview']);

       (function () {
           var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
           ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
           var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
       })();

</script>

<style type="text/css">

.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 101;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 607px;
   height: 342px;
   top: 50%;
   left: 35%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #ffffff;
   border: 2px solid Purple;
   padding: 0px;
   z-index: 102;
   font-family: Verdana;
   font-size: 10pt;
}
.web_dialog_title
{
   border-bottom: solid 2px Purple;
   background-color: Purple;
   padding: 4px;
   color: White;
   font-weight:bold;
}
.web_dialog_title a
{
   color: White;
   text-decoration: none;
}
.align_right
{
   text-align: right;
}
.align_left
{
   text-align: left;
}

</style>
    <script src="Scripts/jquery-1.6.2.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#MCSS").click(function (e) {
                ShowDialog(true, "#dialog");
                $("#overlay").show();
                $("#dialog").fadeIn(600);                     
                e.preventDefault();

            });


            $("#btnClose").click(function (e) {
                HideDialog("#dialog");
                e.preventDefault();
            });


        });

        $(document).ready(function () {
            $("#MCMSImg").click(function (e) {                
                ShowDialog(true, "#Div1");
                $("#overlay").show();
                $("#Div1").fadeIn(600);
                e.preventDefault();

            });



            $("#btnClose1").click(function (e) {
                HideDialog("#Div1");
                e.preventDefault();
            });


        });

        $(document).ready(function () {
            $("#YNImg").click(function (e) {               
                ShowDialog(true, "#Div2");
                $("#overlay").show();
                $("#Div2").fadeIn(600);
                e.preventDefault();

            });


            $("#btnClose2").click(function (e) {
                HideDialog("#Div2");
                e.preventDefault();
            });


        });


        $(document).ready(function () {
            $("#MDDImg").click(function (e) {
                ShowDialog(true, "#Div3");
                 $("#overlay").show();
                $("#Div3").fadeIn(600);
                e.preventDefault();

            });


            $("#btnClose3").click(function (e) {
                HideDialog("#Div3");
                e.preventDefault();
            });


        });

        

        $(document).ready(function () {
            $("#MtxSSImg").click(function (e) {
                ShowDialog(true, "#Div4");
                 $("#overlay").show();
                $("#Div4").fadeIn(600);
                e.preventDefault();

            });


            $("#btnClose4").click(function (e) {
                HideDialog("#Div4");
                e.preventDefault();
            });


        });

        

        $(document).ready(function () {
            $("#MtxMSImg").click(function (e) {
                ShowDialog(true, "#Div5");
                $("#overlay").show();
                $("#Div5").fadeIn(600);
                e.preventDefault();

            });


            $("#btnClose5").click(function (e) {
                HideDialog("#Div5");
                e.preventDefault();
            });


        });

        

        $(document).ready(function () {
            $("#MtxSBSImg").click(function (e) {
                ShowDialog(true, "#Div6");
                $("#overlay").show();
                $("#Div6").fadeIn(600);
                e.preventDefault();

            });


            $("#btnClose6").click(function (e) {
                HideDialog("#Div6");
                e.preventDefault();
            });


        });
        

        $(document).ready(function () {
            $("#ConstSumImg").click(function (e) {
                ShowDialog(true, "#Div7");
                $("#overlay").show();
                $("#Div7").fadeIn(600);
                e.preventDefault();

            });


            $("#btnClose7").click(function (e) {
                HideDialog("#Div7");
                e.preventDefault();
            });


        });

        
        $(document).ready(function () {
            $("#RankingImg").click(function (e) {
                ShowDialog(true, "#Div8");
                $("#overlay").show();
                $("#Div8").fadeIn(600);
                e.preventDefault();

            });


            $("#btnClose8").click(function (e) {
                HideDialog("#Div8");
                e.preventDefault();
            });

        });

        function ShowDialog(modal,screenvis) {
            $("#overlay").show();

            $(screenvis).fadeIn(600);
          
            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }

        function HideDialog(screen1) {
            $("#overlay").hide();
           $(screen1).fadeOut(600);
          
        }


   
</script>

<table width="100%" border="0px" style="background-color:White">
<tr>
<td width="75%">

<table width="100%" border="0px" style="background-color:White">
<tr>
<td colspan="3" align="center"><label style="font-family:Arial, Helvetica, sans-serif;font-size:24px;color:Purple">Express your data better with stunning animated graphs
</label></td>
</tr>
<tr>
<td colspan="3" align="center"><label style="font-family:Arial, Helvetica, sans-serif;font-size:20px;color:Purple">Click on the thumbnails to view animation
</label></td>
</tr>


<tr>
<td width="25%">
<table>
    <tr><td><input type="image"  id="MCSS" src="images/MCSS.png" Width="230px" Height="150px" /></td></tr>
    <tr><td align="center"><label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:Purple">Multiple Choice - Single Select</label></td></tr>
</table>
</td>   
   
<td width="25%">
<table>
    <tr><td><input type="image" id="MCMSImg" src="images/MCMS.png"  Width="230px" Height="150px" /></td></tr>
    <tr><td align="center"><label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:Purple">Multiple Choice - Multiple Select</label></td></tr>
</table>
</td>  
 
<td width="25%">
<table>
    <tr><td><input type="image" ID="YNImg" src="images/YNChart.png"  Width="230px"  Height="150px" /></td></tr>
     <tr><td align="center"><label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:Purple">Yes / No</label></td></tr>
</table>
</td>
</tr>

<tr>
<td width="25%">
<table>
    <tr><td><input type="image" id="MDDImg" src="images/MDD.png" Width="230px"  Height="150px"/></td></tr>
     <tr><td align="center"><label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:Purple">Menu - Drop Down</label></td></tr>
</table>
</td>
<td width="25%">
<table>
    <tr><td>
<input type="image" id="MtxSSImg" src="images/MtxSS.png"  Width="230px"  Height="150px" /></td></tr>
     <tr><td align="center"><label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:Purple">Matrix - Single Select</label></td></tr>
</table>
</td>

<td width="25%"><table>
    <tr><td>
<input type="image" id="MtxMSImg" src="images/MtxMS.png"  Width="230px"  Height="150px" /></td></tr>
     <tr><td align="center"><label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:Purple">Matrix - Multiple Select</label></td></tr>
</table>
</td>
</tr>
<tr>
<td width="25%">
<table>
    <tr><td>
<input type="image" id="MtxSBSImg" src="images/MtxSBS.png"  Width="230px"  Height="150px" /></td></tr>
     <tr><td align="center"><label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:Purple">Matrix - Side by Side</label></td></tr>
</table>
</td>
<td width="25%"><table>
    <tr><td>
<input type="image" id="ConstSumImg" src="images/consSUM.png"  Width="230px"  Height="150px" /></td></tr>
     <tr><td align="center"><label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:Purple">Constant Sum</label></td></tr>
</table>
</td>

<td width="25%"><table>
    <tr><td>
<input type="image" id="RankingImg" src="images/Rankingchrt.png"  Width="230px"  Height="150px" /></td></tr>
     <tr><td align="center"><label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:Purple">Ranking</label></td></tr>
</table>
</td>
</tr>
</table>
 
   </td>
   <td width="25%" valign="top">
   <div class="takeATourRightPanel">
        <!-- take a tour right panel -->
        <div class="tATRightTextPanel" width="230px" height="200px">
            <h2>
                Create an online survey - fast and easy</h2>
            <div class="tATText">
                <p>
                    <asp:HyperLink ID="lnkSignupFree" CssClass="dynamicHyperLink" runat="server" NavigateUrl="~/In/SignUpFree.aspx"
                        Text="Sign Up Free"></asp:HyperLink>
                </p>
            </div>
        </div>
        <div class="tATRightTextPanel">
            <h2>
                Do more, Do better with Insighto Pro</h2>
            <div style="border-left: solid 5px #FFD5B6; padding-left: 20px; padding-right: 10px;">
                <p>
                    Ask unlimited questions</p>
                <p>
                    Receive unlimited responses</p>
                <p>
                   Export data to<br /> Excel/PPT/PDF/Word</p>
                <p>
                   Brand your survey</p>
                   <p>View results graphically</p>
            </div>
            <div class="tATText">
                <p>
                    <asp:LinkButton ID="lnkBtnYearly" CssClass="dynamicHyperLink" runat="server"
                        Text="Buy Now" OnClick="lnkBtnYearly_Click"></asp:LinkButton>
                    <span class="pricePanel"><del>&#2352;</del> 4,500/ year</span>
                </p>
            </div>
        </div>
        <input type="hidden" class="hdnPanelIndex" id="hdnPanelIndex" value="1" />
        <!-- //take a tour right panel -->
    </div>
   </td> 
   </tr>
   </table>
<div id="output"></div>
    
<div id="overlay" class="web_dialog_overlay"></div>
  <div id="dialog" class="web_dialog">  
 <table style="width: 100%; border: 0px;" >
      <tr>
      <td style="background-color:Purple;height:25px">
       <table width="100%">
         <tr>
            <td class="web_dialog_title align_left" width="50%">
              <label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:white">Multiple Choice - Single Select</label>
            </td>
         <td class="web_dialog_title align_right" width="50%">
            <a href="#" id="btnClose">Close</a>
         </td>
          </tr>
      </table>
      </td>
      </tr>
    
      <tr>
   <td>
   <uc:SingleSeries ID="sengleseries" runat="server" />

      </td>
   <td>
   </td>
   </tr>
   </table>
    </div>
     <div id="Div1" class="web_dialog">  
 <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
      <tr>
      <td style="background-color:Purple;height:25px">
       <table width="100%">
         <tr>
            <td class="web_dialog_title align_left" width="50%">
              <label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:white">Multiple Choice - Multiple Select</label>
            </td>
         <td class="web_dialog_title align_right" width="50%">
            <a href="#" id="btnClose1">Close</a>
         </td>
          </tr>
      </table>
      </td>
      </tr>
    
      <tr>
   <td>
   <uc1:MCMS ID="mcmsjquery" runat="server" />
      </td>
   <td>
   </td>
   </tr>
   </table>
    </div>

     <div id="Div2" class="web_dialog">  
 <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
     <tr>
      <td style="background-color:Purple;height:25px">
       <table width="100%">
         <tr>
            <td class="web_dialog_title align_left" width="50%">
              <label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:white">Yes / No</label>
            </td>
         <td class="web_dialog_title align_right" width="50%">
            <a href="#" id="btnClose2">Close</a>
         </td>
          </tr>
      </table>
      </td>
      </tr>
    
      <tr>
   <td>
   <uc2:YN id="YNJq" runat="server" />
      </td>
   <td>
   </td>
   </tr>
   </table>
    </div>

     <div id="Div3" class="web_dialog">  
 <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
      <tr>
      <td style="background-color:Purple;height:25px">
       <table width="100%">
         <tr>
            <td class="web_dialog_title align_left" width="50%">
              <label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:white">Menu - Drop Down</label>
            </td>
         <td class="web_dialog_title align_right" width="50%">
            <a href="#" id="btnClose3">Close</a>
         </td>
          </tr>
      </table>
      </td>
      </tr>
    
      <tr>
   <td>
  <uc3:MDD ID="MDDJQ" runat="server" />
      </td>
   <td>
   </td>
   </tr>
   </table>
    </div>

      <div id="Div4" class="web_dialog">  
 <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
       <tr>
      <td style="background-color:Purple;height:25px">
       <table width="100%">
         <tr>
            <td class="web_dialog_title align_left" width="50%">
              <label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:white">Matrix - Single Select</label>
            </td>
         <td class="web_dialog_title align_right" width="50%">
            <a href="#" id="btnClose4">Close</a>
         </td>
          </tr>
      </table>
      </td>
      </tr>
    
      <tr>
   <td>
  <uc4:MtxSS ID="Mtxsscontrol" runat="server" />
      </td>
   <td>
   </td>
   </tr>
   </table>
    </div>


    <div id="Div5" class="web_dialog">  
 <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
      <tr>
      <td style="background-color:Purple;height:25px">
       <table width="100%">
         <tr>
            <td class="web_dialog_title align_left" width="50%">
              <label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:white">Matrix - Multiple Select</label>
            </td>
         <td class="web_dialog_title align_right" width="50%">
            <a href="#" id="btnClose5">Close</a>
         </td>
          </tr>
      </table>
      </td>
      </tr>
    
      <tr>
   <td>
 <uc5:MtxMS id="MtxMSChart" runat="server" />
      </td>
   <td>
   </td>
   </tr>
   </table>
    </div>

     <div id="Div6" class="web_dialog">  
 <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
     <tr>
      <td style="background-color:Purple;height:25px">
       <table width="100%">
         <tr>
            <td class="web_dialog_title align_left" width="50%">
              <label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:white">Matrix - Side by Side</label>
            </td>
         <td class="web_dialog_title align_right" width="50%">
            <a href="#" id="btnClose6">Close</a>
         </td>
          </tr>
      </table>
      </td>
      </tr>
    
      <tr>
   <td>
 <uc6:MtxSbS ID="MtxSBSChart" runat="server" />
      </td>
   <td>
   </td>
   </tr>
   </table>
    </div>

     <div id="Div7" class="web_dialog">  
 <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
      <tr>
      <td style="background-color:Purple;height:25px">
       <table width="100%">
         <tr>
            <td class="web_dialog_title align_left" width="50%">
              <label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:white">Constant Sum</label>
            </td>
         <td class="web_dialog_title align_right" width="50%">
            <a href="#" id="btnClose7">Close</a>
         </td>
          </tr>
      </table>
      </td>
      </tr>
    
      <tr>
   <td>
   <uc7:ConstSm ID="consts" runat="server" />
      </td>
   <td>
   </td>
   </tr>
   </table>
    </div>


      <div id="Div8" class="web_dialog">  
 <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
      <tr>
      <td style="background-color:Purple;height:25px">
       <table width="100%">
         <tr>
            <td class="web_dialog_title align_left" width="50%">
              <label style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:white">Ranking</label>
            </td>
         <td class="web_dialog_title align_right" width="50%">
            <a href="#" id="btnClose8">Close</a>
         </td>
          </tr>
      </table>
      </td>
      </tr>
    
      <tr>
   <td>
  <uc8:Rank ID="rankchart" runat="server" />
      </td>
   <td>
   </td>
   </tr>
   </table>
    </div>

</asp:Content>