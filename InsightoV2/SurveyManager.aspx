﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SurveyReport.master" AutoEventWireup="true" CodeFile="SurveyManager.aspx.cs" Inherits="Insighto.Pages.SurveyManager" meta:resourcekey="PageResource2" %>
<%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.XtraCharts.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.XtraCharts.v9.1.Web, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>

<%@ Register assembly="DevExpress.Web.v9.1" namespace="DevExpress.Web.ASPxPanel" tagprefix="dxp" %>
<%@ Register Src="~/UserControls/SurveyPrint.ascx" TagPrefix="uc" TagName="SurveyPrint" %>
<%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>

 <asp:Content ID="surveyReportHeader" ContentPlaceHolderID="SurveyReportHeader" runat="Server">
    <div class="contentPanelHeader">
        <div class="pageTitle">
   <span id="lblHeading" runat="server" class="lblSurveyHeading"></span>
                    <asp:Label ID="lblSurveyName" runat="server" 
                meta:resourcekey="lblSurveyNameResource2"></asp:Label>
                    <span><a id="lnkEdit" runat="server" href="" rel="framebox" h="480" w="700" class="smallFont">
                        <asp:Label ID="lblEdit" runat="server" Text="Edit" 
                meta:resourcekey="lblEditResource1"></asp:Label></a></span>
                
        </div>
          <div class="previewPanel">
            <!-- preview link -->
               <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
            <!-- //preview link -->
        </div>

    </div>

    </asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="SurveyReportMainContent" Runat="Server">

<div class="launchStatusPanel"><!-- survey status panel -->
    
    <ul class="launchStatusUL">
        <li>
            <b>Status:</b>
            <asp:Label ID="lblStatus" runat="server" CssClass="surveyManagerText" 
                meta:resourcekey="lblStatusResource2" />
        </li>
        <li>
            <b>Category:</b>
            <asp:Label ID="lblCategory" runat="server"  CssClass="surveyManagerText" 
                meta:resourcekey="lblCategoryResource2" />
        </li>
        <li>
            <b>Folder:</b>
            <asp:Label ID="lblFolder" runat="server"  CssClass="surveyManagerText" 
                meta:resourcekey="lblFolderResource2" />
        </li>
        <li>
            <b>Last Modified On:</b>
            <asp:Label ID="lblLastModified" runat="server"  CssClass="surveyManagerText" 
                meta:resourcekey="lblLastModifiedResource2" />
        </li>
        <li class="noborder">
           <b>Created By: </b>
            <asp:Label ID="lblLaunchedBy" runat="server"  CssClass="surveyManagerText" 
                meta:resourcekey="lblLaunchedByResource2"  />
        </li>
    </ul>
    <div class="clear"></div>
    
    
<!-- //survey status panel --></div>


<div class="surveyCreateLeftMenu"><!-- survey left menu -->
        <ul class="reportsMenu">
            <li><a href="#" class="activeLink" style="cursor:default">Survey Stats</a></li>
        </ul>
        <div class="launchExpandPanel">
            <div><asp:Label ID="lblRecentLaunch" runat="server" 
                    meta:resourcekey="lblRecentLaunchResource2" ></asp:Label></div>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="headerRwoGray">
                        <td colspan="2">No of Invites sent:</td>
                    </tr>
                    <tr class="rowStyleGray">
                        <td width="95%">Through Insighto</td>
                        <td width="5%" align="right">
                            <asp:Label ID="lblInsighto" runat="server" 
                                meta:resourcekey="lblInsightoResource2"></asp:Label></td>
                    </tr>
                    <tr class="rowStyleGray">
                        <td>Through Web link</td>
                        <td align="right">
                        <asp:TextBox ID="txtExternal" CssClass="textBoxSmallValueRight" runat="server" onkeypress="return isNumberKey(event)"
                                   Text="0"     AutoPostBack="True" 
                                OnTextChanged="txt_external_ValueChanged" ToolTip="Input value and 'Enter'" 
                                meta:resourcekey="txtExternalResource2"></asp:TextBox>
                        <asp:Label ID="lblExternal" Visible="False" runat="server" 
                                meta:resourcekey="lblExternalResource2"></asp:Label></td>
                    </tr>
                    <tr class="rowStyleGray">
                        <td>Total</td>
                        <td align="right"><asp:Label ID="lblTotal" runat="server" Text="Label" 
                                meta:resourcekey="lblTotalResource2"></asp:Label></td>
                    </tr>
                </table>
            
            <p class="defaultHeight"></p>
           
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="headerRwoGray">
                        <td width="80%"></td>
                        <td width="5%" align="center">No.</td>
                        <td width="15%" align="center">%</td>
                    </tr>
                    <tr class="rowStyleGray">
                        <td >Visits</td>
                        <td align="center"><asp:Label ID="lblVisits" runat="server" Text="0" 
                                meta:resourcekey="lblVisitsResource2"></asp:Label></td>
                        <td align="center"><asp:Label ID="lblVisitPercent" runat="server" Text="0%" 
                                meta:resourcekey="lblVisitPercentResource2"></asp:Label></td>
                    </tr>
                    <tr class="rowStyleGray">
                        <td>Completes</td>
                        <td align="center"><asp:Label ID="lblCompletes" runat="server" Text="0" 
                                meta:resourcekey="lblCompletesResource2"></asp:Label></td>
                        <td align="center"><asp:Label ID="lblCompletesPercent" runat="server" Text="0%" 
                                meta:resourcekey="lblCompletesPercentResource2"></asp:Label></td>
                    </tr>
                    <tr class="rowStyleGray">
                        <td>Partials</td>
                        <td align="center"><asp:Label ID="lblPartials" runat="server" Text="0" 
                                meta:resourcekey="lblPartialsResource2"></asp:Label></td>
                        <td align="center"><asp:Label ID="lblPartialsPercent" runat="server"  Text="0%" 
                                meta:resourcekey="lblPartialsPercentResource2"></asp:Label></td>
                    </tr>
                      <%--<tr class="rowStyleGray">
                        <td>Bounced</td>
                         <td align="center"><asp:Label ID="lblBounced" runat="server" Text="0" 
                                 meta:resourcekey="lblBouncedResource2"></asp:Label></td>
                        <td align="center"><asp:Label ID="lbleBouncedPercent" runat="server" Text="0%" 
                                meta:resourcekey="lbleBouncedPercentResource2"></asp:Label></td>
                    </tr>--%>
                    <tr class="rowStyleGray">
                        <td>Screen Out</td>
                          <td align="center"><asp:Label ID="lblScreenOut" runat="server" Text="0" 
                                  meta:resourcekey="lblScreenOutResource2"></asp:Label></td>
                        <td align="center"><asp:Label ID="lblScreenOutPercent" runat="server" Text="0%" 
                                meta:resourcekey="lblScreenOutPercentResource2"></asp:Label></td>
                    </tr>
                     <tr class="rowStyleGray">
                        <td>Over Quota</td>
                          <td align="center"><asp:Label ID="lblOverQuota" runat="server" Text="0" 
                                  meta:resourcekey="lblOverQuotaResource2"></asp:Label></td>
                        <td align="center"><asp:Label ID="lblOverQuotaPercent" runat="server" Text="0%" 
                                meta:resourcekey="lblOverQuotaPercentResource2"></asp:Label></td>
                    </tr>
                  
                </table>
           
        </div>
        
<!-- //survey left menu --></div>

                

<div class="surveyQuestionPanel"><!-- survey question panel -->


           <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                <div><asp:Label ID="lblSuccessMsg"  runat="server" 
                        meta:resourcekey="lblSuccessMsgResource2"></asp:Label></div>
           </div>
           <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">                
                <div><asp:Label ID="lblErrMsg"  runat="server" 
                        meta:resourcekey="lblErrMsgResource2" ></asp:Label></div>
            </div>


           

        <div class="surveyQuestionHeader">
            <div class="manageLinks" style="border:solid 0 #000;">
            <asp:HyperLink ID="hlnkCopy"  CssClass="copyLink-left" rel="framebox" h="450" 
                    w="700" scrolling='yes' title='Copy' 
                    onclick='javascript: return ApplyFrameBox(this);' ToolTip="Copy" runat="server" 
                    meta:resourcekey="hlnkCopyResource2">[hlnkCopy]</asp:HyperLink>
            
            <asp:LinkButton ID="lBtnCopy" Visible="False"  CssClass="copyLink-left" 
                    runat="server" onclick="lBtnCopy_Click" meta:resourcekey="lBtnCopyResource2">Copy</asp:LinkButton>
            <asp:LinkButton ID="lBtnDelete" CssClass="deleteLink" runat="server" 
                    ToolTip="Delete" OnClientClick="javascript:return DeleteSurvey();" 
                    OnClick="lBtnDelete_Click" meta:resourcekey="lBtnDeleteResource2"></asp:LinkButton>
            <asp:LinkButton ID="lBtnClose" CssClass="closeLink" runat="server" ToolTip="Close" 
                    onclick="lBtnClose_Click" meta:resourcekey="lBtnCloseResource2"></asp:LinkButton>
            <asp:LinkButton ID="lBtnActivate" CssClass="activeIconLink" runat="server" 
                    ToolTip="Activate" onclick="lBtnActivate_Click" 
                    meta:resourcekey="lBtnActivateResource2"></asp:LinkButton>
            <asp:LinkButton ID="lBtnAddEmail" runat="server" CssClass="addEmailLink" 
                    ToolTip="Add Email List" onclick="lBtnAddEmail_Click" 
                    meta:resourcekey="lBtnAddEmailResource2"></asp:LinkButton>
            
            <asp:LinkButton ID ="lBtnReports" runat="server" ToolTip="Reports" 
                    CssClass="top-small-gap icon-Reports-active" OnClick ="lBtnReports_Click" 
                    meta:resourcekey="lBtnReportsResource1"></asp:LinkButton>  
            <asp:HyperLink id="hlnkEmailList" rel="framebox" h="480" w="750" scrolling="yes" 
                    CssClass="top-small-gap icon-Remainder-active" ToolTip="Reminder"   
                    runat="server" meta:resourcekey="hlnkEmailListResource1">[hlnkEmailList]</asp:HyperLink>
            <asp:LinkButton ID="lBtnSurveyEdit" runat="server" ToolTip="Edit" 
                    CssClass="top-small-gap icon-Edit-active" OnClick="lBtnSurveyEdit_Click" 
                    meta:resourcekey="lBtnSurveyEditResource1"></asp:LinkButton>  
            <uc:SurveyPrint ID="ucSurveyPrint" runat="server" Mode="Preview" />
           
            <asp:LinkButton ID="lBtnPrint" visible="False" CssClass="printLink" ToolTip="Print" 
                    runat="server" onclick="lBtnPrint_Click" meta:resourcekey="lBtnPrintResource2">Print</asp:LinkButton> 
            </div>
        </div>
    
        <div class="surveyUrlPanel">
       
      
       <div style="float:left; height:25px; line-height:25px; padding:0 3px 0 3px;">The link to your survey is :</div>
       <div class="clear"></div>
        <div style="float:left; height:35px; line-height:35px; padding:0 3px 0 3px;">
            <asp:TextBox ID="txtSurveyUrl"  width="400px" CssClass="txtUrl textBoxMedium" 
                Enabled="False" runat="server" meta:resourcekey="txtSurveyUrlResource2" /></div>
 <div style="float:left; height:35px; line-height:35px; padding:0 3px 0 3px; "><a id="lnkUrl" style="padding:0; font-weight:normal;" href="javascript:void(0)">Copy</a></div>
        </div>
        <div class="clear"></div>
            <div class="surveyUrlPanel">
               <div style="float:left; height:25px; line-height:25px; padding:0 3px 0 3px;">The Embed code of your survey is :</div>
               <div class="clear"></div>
               <div style="float:left; height:35px; line-height:35px; padding:0 3px 0 3px;">
                    <asp:TextBox ID="txtEmbedCode" TextMode="MultiLine" width="400px" Height="60px" CssClass="txtEmbedCode textBoxMedium" 
                        Enabled="False" runat="server"  />
               </div>
               <div style="float:left; height:35px; line-height:35px; padding:47px 3px 0 3px; "><a id="lnkEmbed" style="padding:0; font-weight:normal;" href="javascript:void(0)">Copy</a>
               </div>
            </div>
         <div class="clear"></div>
          <div style="float:left; margin-left:195px; padding-right:30px;" id="dvSurveyLunchText" runat="server" visible="false">

        <div class="informationPanelDefault">
        <div>
            <asp:Label ID="lblSurveyLaunchText" Visible="False"  runat="server" Text="As survey is launched through Insigto email system, to protect data integrity, 
 only preview URL is given." meta:resourcekey="lblSurveyLaunchTextResource1"></asp:Label></div>
</div>   
        </div>
        <div class="clear"></div>
        <div style="margin-top:20px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr><td class="defaultHeight" colspan="3"></td></tr>
            <tr>
                <td width="49%" valign="top">
               <%-- <dxp:ASPxPanel id="ASPxRoundPanelQuestionreports1" runat="server" Visible=false>
          <PanelCollection>
<dxp:PanelContent ID="PanelContent3" runat="server">
</dxp:PanelContent>
</PanelCollection>
</dxp:ASPxPanel>--%>
 <%--<dxp:ASPxPanel ID="PanelReport" runat="server" Width="100%">
              <PanelCollection>
                  <dxp:PanelContent ID="PanelContent1" runat="server">                  </dxp:PanelContent>
              </PanelCollection>
          </dxp:ASPxPanel>--%>	

          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class="headerRwoGray">
                            <td>Alerts:</td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Panel ID="pnlAlerts" runat="server" Height="100px"  Width="100%" 
                                    meta:resourcekey="pnlAlertsResource2">
                     </asp:Panel>
                            </td>
                        </tr>
          </table>
                </td>
                <td width="2%">&nbsp;</td>
                <td width="49%" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class="headerRwoGray">
                            <td>Reminders:</td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Panel ID="pnlRemainder" runat="server"  Height="100px" Width="100%" 
                                    meta:resourcekey="pnlRemainderResource2">
                     </asp:Panel>
                            </td>
                        </tr>
                    </table>
                   
             
                </td>
            </tr>
        </table>         
        <input type="hidden" runat="server" id="hdnactivation"  />
             
        </div>
 
<!-- //survey question panel --></div>

<script type="text/javascript" src="Scripts/ZeroClipBoard/jquery.zclip.min.js"></script>
    <script type="text/javascript">
        $("#lnkUrl").zclip({
            path: '/Scripts/ZeroClipBoard/ZeroClipboard.swf',
            copy: $('.txtUrl').val(),
            beforeCopy: function () {               
            },
            afterCopy: function () {                      
            }

        });
        $("#lnkEmbed").zclip({
            path: '/Scripts/ZeroClipBoard/ZeroClipboard.swf',
            copy: $('.txtEmbedCode').val(),
            beforeCopy: function () {
            },
            afterCopy: function () {
            }

        });
    </script>
<script type="text/javascript" language="javascript">

    function isNumberKey(evt) {
        //         var charCode = (evt.which) ? evt.which : event.keyCode
        //         if (charCode > 31 && (charCode < 48 || charCode > 57))
        //            return false;

        //         return true;



        var ValidChars = "0123456789.";
        var IsNumber = true;
        var Char;
        var sText = document.getElementById('<%=txtExternal.ClientID%>').value;
        for (i = 0; i < sText.length && IsNumber == true; i++) {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) == -1) {
                IsNumber = false;
                document.getElementById('<%=txtExternal.ClientID%>').value = "";
                alert('Please enter numeric values only.');
                return IsNumber;
            }
        }
        return IsNumber;

    }
    function DeleteSurvey() {
        jConfirm('Are you sure you want to delete?', 'Delete Confirmation', function (r) {
            r.preventDefault();
            if (r == '1') {              
                //                $.post("AjaxService.aspx", { "method": "DeleteSurveyBySurveyId", "Id": surveyId },
                //           function (result) {
                //               if (result == '0') {
                //                   $('#divSuccess').show();
                //                   $('#lblMessage').html('Survey deleted successfully.');
                //                   var status = $('#hdnStatus').val();

                //                   gridReload(status);
                //               }
                //               else {
                //                   $('#divSuccess').hide();
                //                   $('#lblMessage').attr('style', 'disply:none;');
                //               }
                //           });
            }

        });
    }

    function DeleteSurvey() {
        var answer = confirm('Are you sure you want to delete?');
        if (answer) {
           return true
       }
       return false;
   }

   function Activationalert() {
       jConfirm('Are you sure you want to close the survey? Please note that you cannot reactivate the survey once it closed, until you purchase and apply a survey credit ', 'Survey Close', function (r) {

           document.getElementById('<%=hdnactivation.ClientID %>').value = r;

           var f1 = document.forms['Form1'];
           f1.submit();

           return false;
       });
       
   }

   function Activationalertfree() {
       jConfirm('Are you sure you want to close the survey?', 'Survey Close', function (r) {

           document.getElementById('<%=hdnactivation.ClientID %>').value = r;

           var f1 = document.forms['Form1'];
           f1.submit();

           return false;
       });

   }
    </script>
</asp:Content>

