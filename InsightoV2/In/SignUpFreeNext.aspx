﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Login.master" CodeFile="SignUpFreeNext.aspx.cs" Inherits="Insighto.Pages.In.In_SignUpFreeNext"%>

<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <!-- Header and Menu -->
        <div class="messagePanel"><!-- signup panel -->
		<h3>
            <asp:Label ID="lblThankyou" runat="server" 
                Text="Thank you for registering with Insighto.com" 
                meta:resourcekey="lblThankyouResource1"></asp:Label>
		</h3>
		<p>
            <asp:Label ID="lblEmail" runat="server" 
                Text="Your registration needs authentication. An email has been sent to your email box with a confirmation link.
			Please go to your email box and click on the link to complete your registration process and start using Insighto." 
                meta:resourcekey="lblEmailResource1"></asp:Label>
		</p>
		<p>&nbsp;</p>
		<p>
            <asp:Label ID="lblContact" runat="server" 
                Text="If you experience any issue/ difficulty in registration/activation, please contact us at <a href='mailto:support@insighto.com'>support@insighto.com</a>" 
                meta:resourcekey="lblContactResource1"></asp:Label>
		</p>
		<p>&nbsp;</p>
		<p>
            <asp:ImageButton ID="btnContinue" 
                ImageUrl="~/App_Themes/Classic/Images/button_continue.gif" runat="server" 
                onclick="btnContinue_Click" ToolTip="Continue" 
                meta:resourcekey="btnContinueResource1" />
		
		</p>
		
		<div class="clear"></div>
	<!-- //signup panel --></div>
    <script type="text/javascript">
// <![CDATA[
        var google_conversion_id = 1027814833;
        var google_conversion_language = "en";
        var google_conversion_format = "2";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "dEWdCImw7gIQseuM6gM";
        var google_conversion_value = 0;
// ]]> 
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1027814833/?value=0&amp;label=dEWdCImw7gIQseuM6gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</asp:Content> 
