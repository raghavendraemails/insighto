﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Insighto.Pages.In.In_Register"
    Culture="auto" MasterPageFile="~/Home.master" UICulture="auto" %>

<%@ Register TagPrefix="uc" TagName="Footer" Src="~/UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc" TagName="TopLinks" Src="~/UserControls/TopLinks.ascx" %>
<%@ Register TagPrefix="uc" TagName="HeaderMenu" Src="~/In/UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc" TagName="PurchaseMenu" Src="~/In/UserControls/PurchaseDetails.ascx" %>
<%@ Register TagPrefix="uc" TagName="AuthenticatedBy" Src="~/In/UserControls/AuthenticatedBy.ascx" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<%--<%@ Register TagPrefix="uc" TagName="Advertisement" Src="~/In/UserControls/ProAdvertisement.ascx" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="Stylesheet" href="/App_Themes/Classic/WebRupee.css" type="text/css" />
    <link href="/Styles/Banner_Style.css" rel="stylesheet" type="text/css" />
     <link href="/Styles/notification.css" rel="stylesheet" type="text/css" />
    <style type="text/css" media="screen">
        @import "/images/homeimages/insighto.css";
    </style>

        <link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/layoutStyle.css?v=0.0.8.1" />
        <link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/facebox.css?v=0.0.8.1" />
    <table border="0">
        <tr>
            <td valign="top">
                <div class="marginNewInnerPanel">
                <div id="divPremiumNote" class="benefitBannerPanel" style="display: none;" runat="server">
                        <!-- premium benefits panel -->
                        <div class="benefitBannerTop">
                        </div>
                        <div id="dvPremium" runat="server"> <!--Code bind from resuource file -->
                       <!-- //premium benefits panel -->
                        </div>
                    </div>
                    <div class="clear"/>
                    <div id="divMonthlyNote" class="benefitBannerPanel" style="display: none;" runat="server">
                        <!-- monthly benefits panel -->
                        <div class="benefitBannerTop">
                        </div>
                        <div id="dvProMonthly" runat="server"> <!--Code bind from resuource file -->
                       <!-- //monthly benefits panel -->
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div id="divQuarterlyNote" class="benefitBannerPanel" style="display: none;" runat="server">
                        <!-- quarterly benefits panel -->
                        <div class="benefitBannerTop">
                        </div>
                        <div id="dvProQuarterly" runat="server">
                        
                        </div>
                    </div>
                    <!-- //quarterly benefits panel -->
                    <div class="clear">
                    </div>
                    <div id="divAnnualNote" class="benefitBannerPanel" style="display: block;" runat="server">
                        <!-- annual benefits panel -->
                        <div class="benefitBannerTop">
                        </div>
                        <div id="dvProAnnual"  runat="server" >
                        
                        <!-- //annual benefits panel -->
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                     <div class="signUpPnl">
                        <!-- signup panel -->
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <h3>
                                       <asp:Label ID="lblRegistrationText" runat="server" meta:resourcekey="lblRegistrationtext" Visible="false"></asp:Label>  
                                      <asp:Label ID="lblPreRegistrationText" runat="server" meta:resourcekey="lblPreRegistrationtext" Visible="false"></asp:Label> 
                                       <asp:Label ID="lblProRegistrationText" runat="server" meta:resourcekey="lblproRegistrationtext" Visible="false"></asp:Label> 
                                    </h3>
                                </td>
                                <td class="signbg" runat="server" id="trSignIn">
                                <asp:Label ID ="lblhaveAnAccount" runat ="server" meta:resourcekey="lblhaveAnAccount"></asp:Label>                                  
                                    <asp:HyperLink ID="hlnkPreview" runat="server" CssClass="orangeColor" rel="framebox"
                                        Text="Login" h="350" w="600" scrolling='yes' NavigateUrl="~/SignIn.aspx" ToolTip="Sign In"
                                        alt="Login" title="Login" />
                                </td>
                            </tr>
                        </table>
                        <div class="text">
                            <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false" style="padding-top: 0;padding-bottom: 0;margin-bottom: 0;margin-top: 0;">
                                <div style="padding-top: 0;padding-bottom: 0;margin-bottom: 0;margin-top: 0;">
                                    <asp:Label ID="lblErrMsg" runat="server" CssClass="errorMesg" Visible="false" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
                            </div>
                            <div id="ulMemberInformation" runat="server">
                                <div class="mandatoryPanel">
                                <asp:Label ID ="lblRequiredFileds" runat ="server" meta:resourcekey="lblRequiredFiledString"></asp:Label>
                                   <%-- <span>indicates required fields</span>--%>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblName" runat="server" AssociatedControlID="txtName" CssClass="requirelbl"
                                           />
                                    </div>
                                    <div class="loginControls">
                                        <asp:TextBox ID="txtName" runat="server" CssClass="textBoxMedium" MaxLength="30"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:DropDownList ID="drpTimeZone" runat="server" class="dropdownMedium" style="display:none;">
                                            <asp:ListItem Value="-12:00,0">(-12:00) International Date Line West</asp:ListItem>
                                            <asp:ListItem Value="-11:00,0">(-11:00) Midway Island, Samoa</asp:ListItem>
                                            <asp:ListItem Value="-10:00,0">(-10:00) Hawaii</asp:ListItem>
                                            <asp:ListItem Value="-09:00,1">(-09:00) Alaska</asp:ListItem>
                                            <asp:ListItem Value="-08:00,1">(-08:00) Pacific Time (US & Canada)</asp:ListItem>
                                            <asp:ListItem Value="-07:00,0">(-07:00) Arizona</asp:ListItem>
                                            <asp:ListItem Value="-07:00,1">(-07:00) Mountain Time (US & Canada)</asp:ListItem>
                                            <asp:ListItem Value="-06:00,0">(-06:00) Central America, Saskatchewan</asp:ListItem>
                                            <asp:ListItem Value="-06:00,1">(-06:00) Central Time (US & Canada), Guadalajara, Mexico city</asp:ListItem>
                                            <asp:ListItem Value="-05:00,0">(-05:00) Indiana, Bogota, Lima, Quito, Rio Branco</asp:ListItem>
                                            <asp:ListItem Value="-05:00,1">(-05:00) Eastern time (US & Canada)</asp:ListItem>
                                            <asp:ListItem Value="-04:00,1">(-04:00) Atlantic time (Canada), Manaus, Santiago</asp:ListItem>
                                            <asp:ListItem Value="-04:00,0">(-04:00) Caracas, La Paz</asp:ListItem>
                                            <asp:ListItem Value="-03:30,1">(-03:30) Newfoundland</asp:ListItem>
                                            <asp:ListItem Value="-03:00,1">(-03:00) Greenland, Brasilia, Montevideo</asp:ListItem>
                                            <asp:ListItem Value="-03:00,0">(-03:00) Buenos Aires, Georgetown</asp:ListItem>
                                            <asp:ListItem Value="-02:00,1">(-02:00) Mid-Atlantic</asp:ListItem>
                                            <asp:ListItem Value="-01:00,1">(-01:00) Azores</asp:ListItem>
                                            <asp:ListItem Value="-01:00,0">(-01:00) Cape Verde Is.</asp:ListItem>
                                            <asp:ListItem Value="00:00,0">(00:00) Casablanca, Monrovia, Reykjavik</asp:ListItem>
                                            <asp:ListItem Value="00:00,1">(00:00) GMT: Dublin, Edinburgh, Lisbon, London</asp:ListItem>
                                            <asp:ListItem Value="+01:00,1">(+01:00) Amsterdam, Berlin, Rome, Vienna, Prague, Brussels</asp:ListItem>
                                            <asp:ListItem Value="+01:00,0">(+01:00) West Central Africa</asp:ListItem>
                                            <asp:ListItem Value="+02:00,1">(+02:00) Amman, Athens, Istanbul, Beirut, Cairo, Jerusalem</asp:ListItem>
                                            <asp:ListItem Value="+02:00,0">(+02:00) Harare, Pretoria</asp:ListItem>
                                            <asp:ListItem Value="+03:00,1">(+03:00) Baghdad, Moscow, St. Petersburg, Volgograd</asp:ListItem>
                                            <asp:ListItem Value="+03:00,0">(+03:00) Kuwait, Riyadh, Nairobi, Tbilisi</asp:ListItem>
                                            <asp:ListItem Value="+03:30,0">(+03:30) Tehran</asp:ListItem>
                                            <asp:ListItem Value="+04:00,0">(+04:00) Abu Dhadi, Muscat</asp:ListItem>
                                            <asp:ListItem Value="+04:00,1">(+04:00) Baku, Yerevan</asp:ListItem>
                                            <asp:ListItem Value="+04:30,0">(+04:30) Kabul</asp:ListItem>
                                            <asp:ListItem Value="+05:00,1">(+05:00) Ekaterinburg</asp:ListItem>
                                            <asp:ListItem Value="+05:00,0">(+05:00) Islamabad, Karachi, Tashkent</asp:ListItem>
                                            <asp:ListItem Value="+05:30,0">(+05:30) Chennai, Kolkata, Mumbai, New Delhi, Sri Jayawardenepura</asp:ListItem>
                                            <asp:ListItem Value="+05:45,0">(+05:45) Kathmandu</asp:ListItem>
                                            <asp:ListItem Value="+06:00,0">(+06:00) Astana, Dhaka</asp:ListItem>
                                            <asp:ListItem Value="+06:00,1">(+06:00) Almaty, Nonosibirsk</asp:ListItem>
                                            <asp:ListItem Value="+06:30,0">(+06:30) Yangon (Rangoon)</asp:ListItem>
                                            <asp:ListItem Value="+07:00,1">(+07:00) Krasnoyarsk</asp:ListItem>
                                            <asp:ListItem Value="+07:00,0">(+07:00) Bangkok, Hanoi, Jakarta</asp:ListItem>
                                            <asp:ListItem Value="+08:00,0">(+08:00) Beijing, Hong Kong, Singapore, Taipei</asp:ListItem>
                                            <asp:ListItem Value="+08:00,1">(+08:00) Irkutsk, Ulaan Bataar, Perth</asp:ListItem>
                                            <asp:ListItem Value="+09:00,1">(+09:00) Yakutsk</asp:ListItem>
                                            <asp:ListItem Value="+09:00,0">(+09:00) Seoul, Osaka, Sapporo, Tokyo</asp:ListItem>
                                            <asp:ListItem Value="+09:30,0">(+09:30) Darwin</asp:ListItem>
                                            <asp:ListItem Value="+09:30,1">(+09:30) Adelaide</asp:ListItem>
                                            <asp:ListItem Value="+10:00,0">(+10:00) Brisbane, Guam, Port Moresby</asp:ListItem>
                                            <asp:ListItem Value="+10:00,1">(+10:00) Canberra, Melbourne, Sydney, Hobart, Vladivostok</asp:ListItem>
                                            <asp:ListItem Value="+11:00,0">(+11:00) Magadan, Solomon Is., New Caledonia</asp:ListItem>
                                            <asp:ListItem Value="+12:00,1">(+12:00) Auckland, Wellington</asp:ListItem>
                                            <asp:ListItem Value="+12:00,0">(+12:00) Fiji, Kamchatka, Marshall Is.</asp:ListItem>
                                            <asp:ListItem Value="+13:00,0">(+13:00) Nuku'alofa</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="reqlbl btmpadding">
                                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqName" CssClass="lblRequired"
                                            ControlToValidate="txtName" meta:resourcekey="lblReqNameResource1" runat="server"
                                            Display="Dynamic" ValidationGroup="tab1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="regName" CssClass="lblRequired"
                                            ControlToValidate="txtName" runat="server"  meta:resourcekey="lblRegNameResource1" 
                                            ValidationExpression="^[a-zA-Z ]*$" Display="Dynamic"></asp:RegularExpressionValidator></div>
                                </div>
                                  <div class="con_login_pnl" id="divLname" runat="server" style="display:none">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblLname" runat="server" CssClass="requirelbl" AssociatedControlID="txtLname"
                                           Text="Last Name"/>
                                    </div>
                                    <div class="loginControls">
                                        <asp:TextBox ID="txtLname" runat="server" TabIndex="2" CssClass="textBoxMedium"
                                            MaxLength="40"></asp:TextBox>
                                    </div>
                                </div>
                                  <div class="con_login_pnl" id="divValidationLname" runat="server" style="display:none;">
                                    <div class="reqlbl btmpadding">
                                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvLname" CssClass="lblRequired"
                                            ControlToValidate="txtLname" ErrorMessage="Please enter last name." runat="server"
                                            Display="Dynamic" ValidationGroup="tab1" Enabled="false"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="rexLname" CssClass="lblRequired"
                                            ControlToValidate="txtLname" runat="server"  
                                            ValidationExpression="^[a-zA-Z ]*$" Display="Dynamic" Enabled="false"  meta:resourcekey="lblRegNameResource1"></asp:RegularExpressionValidator></div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblUserName" runat="server" CssClass="requirelbl" AssociatedControlID="txtUserName"
                                            meta:resourcekey="lblUserNameResource" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:TextBox ID="txtUserName" runat="server" TabIndex="2" CssClass="textBoxMedium"
                                            MaxLength="40"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="reqlbl btmpadding">
                                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqUsername" meta:resourcekey="lblReqUNameResource"
                                            CssClass="lblRequired" ControlToValidate="txtUserName" runat="server" Display="Dynamic"
                                            ValidationGroup="tab1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="RexUsername" meta:resourcekey="lblvalidUNameResource1"
                                            CssClass="lblRequired" ControlToValidate="txtUserName" runat="server" Display="Dynamic"
                                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="tab1"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblUserNameText" runat="server" AssociatedControlID="userNameText"
                                            meta:resourcekey="Label1Resource1" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:Label ID="userNameText" runat="server" CssClass="note" meta:resourcekey="userNameTextResource" />
                                    </div>
                                </div>
                                <div class="con_login_pnl" id="divPassword" runat="server">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblPassword" runat="server" CssClass="requirelbl" AssociatedControlID="txtPassword"
                                            meta:resourcekey="lblPasswordResource" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:TextBox ID="txtPassword" runat="server" TabIndex="3" TextMode="Password" MaxLength="16"
                                            CssClass="textBoxMedium" meta:resourcekey="txtPasswordResource1"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="reqlbl btmpadding" id="divPasswordValidation" runat="server">
                                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqPassword" meta:resourcekey="lblReqPasswordResource1"
                                            CssClass="lblRequired" ControlToValidate="txtPassword" runat="server" Display="Dynamic"
                                            ValidationGroup="tab1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="regNewPassword" CssClass="lblRequired"
                                            ControlToValidate="txtPassword" runat="server" meta:resourcekey="lblLenPasswordResource2"
                                            ValidationExpression="^.{6,16}$" Display="Dynamic" ValidationGroup="tab1"></asp:RegularExpressionValidator>
                                        <asp:CustomValidator SetFocusOnError="true" ID="cvNewPassword" Display="Dynamic"
                                            runat="server" ControlToValidate="txtPassword" CssClass="lblRequired" meta:resourcekey="lblLenPasswordResource1"
                                            ClientValidationFunction="ValidatePassword">
                                        </asp:CustomValidator>
                                    </div>
                                </div>
                                <div class="con_login_pnl" runat="server" id="divPasswordhelp">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblPasswordHelp" runat="server" AssociatedControlID="lblPasswordHelpText"
                                            meta:resourcekey="lblPasswordHelpResource1" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:Label ID="lblPasswordHelpText" CssClass="note" runat="server" meta:resourcekey="lblPasswordHelpTextResource" />
                                    </div>
                                </div>
                                <div class="con_login_pnl" id="divconfirmpassword" runat="server">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblConfirmPassword" runat="server" CssClass="requirelbl" AssociatedControlID="txtConfirmPassword"
                                            meta:resourcekey="lblConfirmPasswordResource" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:TextBox ID="txtConfirmPassword" TabIndex="4" runat="server" TextMode="Password"
                                            MaxLength="16" CssClass="textBoxMedium" meta:resourcekey="txtConfirmPasswordResource1"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="con_login_pnl" id="divConfirmPasswordValidation" runat="server">
                                    <div class="reqlbl btmpadding">
                                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqConfirmPassword" meta:resourcekey="lblReqConfirmPasswordResource1"
                                            CssClass="lblRequired" ControlToValidate="txtConfirmPassword" runat="server"
                                            Display="Dynamic" ValidationGroup="tab1"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cmpPassword" meta:resourcekey="lblcmpConfirmPasswordResource1"
                                            CssClass="lblRequired" ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword"
                                            Operator="Equal" runat="server" SetFocusOnError="true" ValidationGroup="tab1"></asp:CompareValidator></div>
                                </div>
                                   <div class="con_login_pnl" id="divCompany" runat="server" style="display:none">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblCompanyName" runat="server" CssClass="requirelbl" AssociatedControlID="txtCompanyName" Text="Company Name" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:TextBox ID="txtCompanyName" runat="server" CssClass="textBoxMedium"
                                            MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                  <div class="con_login_pnl" id="divCompanyValidation" runat="server" style="display:none;">
                                    <div class="reqlbl btmpadding">
                                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvCompany" CssClass="lblRequired"
                                            ControlToValidate="txtCompanyName" ErrorMessage="Please enter company name." runat="server"
                                            Display="Dynamic" ValidationGroup="tab1" Enabled="false"></asp:RequiredFieldValidator>                                       
                                </div>
                                </div>

                                <div class="con_login_pnl" id="divUsertype" runat="server">
                                    <div class="loginlbl">
                                        <div class="spacepad">
                                            <asp:Label ID="lblUserType" runat="server" AssociatedControlID="rbtnIndividual" meta:resourcekey="lblUserTypeResource" /></div>
                                    </div>
                                    <div class="loginControls">
                                        <asp:RadioButton ID="rbtnIndividual" TabIndex="5" runat="server" CssClass="userType rbtnIndividual"
                                            GroupName="type" meta:resourcekey="rbtnIndividualResource1" Checked="true" onclick="javascript:ShowHide('hide');" />Individual
                                        <asp:RadioButton ID="rbtnOrganization" TabIndex="6" runat="server" CssClass="userType rbtnOrganization"
                                            GroupName="type" meta:resourcekey="rbtnOrganizationResource1" onclick="javascript:ShowHide('show');" />Organization
                                    </div>
                                </div>
                                <div class="hide userOptions" id="userOptions" style="display: none" runat="server">
                                    <div class="con_login_pnl" id="Organization">
                                        <div class="loginlbl">
                                            <input type="hidden" id="hdnHideFlag" class="hdnHideFlag" value="0" runat="server" />
                                            <asp:Label ID="lblOrganization" runat="server" CssClass="requirelbl" AssociatedControlID="txtOrganization"
                                                meta:resourcekey="lblOrganizationResource" />
                                        </div>
                                        <div class="loginControls">
                                            <asp:TextBox ID="txtOrganization" TabIndex="7" runat="server" MaxLength="50" CssClass="textBoxMedium"
                                                meta:resourcekey="txtOrganizationResource1"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="con_login_pnl">
                                        <div class="reqlbl btmpadding">
                                            <asp:Label ID="lblreqOrganization" AssociatedControlID="custreqOrganization" runat="server" />
                                            <asp:CustomValidator SetFocusOnError="true" ID="custreqOrganization" meta:resourcekey="lblReqOrganizationResource1"
                                                CssClass="lblRequired" ControlToValidate="txtOrganization" ClientValidationFunction="validateOrgTextBox"
                                                runat="server" Display="Dynamic" ValidationGroup="tab1" ValidateEmptyText="true"></asp:CustomValidator></div>
                                    </div>
                                    <div class="con_login_pnl" id="Designation">
                                        <div class="loginlbl">
                                            <asp:Label ID="lblDesignation" runat="server" CssClass="requirelbl" AssociatedControlID="txtDesignation"
                                                meta:resourcekey="lblDesignationResource1" />
                                        </div>
                                        <div class="loginControls">
                                            <asp:TextBox ID="txtDesignation" TabIndex="8" runat="server" MaxLength="50" CssClass="textBoxMedium"
                                                meta:resourcekey="txtDesignationResource1"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="con_login_pnl">
                                        <div class="reqlbl btmpadding">
                                            <asp:Label ID="lblreqDesignation" AssociatedControlID="custreqDesignation" runat="server" />
                                            <asp:CustomValidator SetFocusOnError="true" ID="custreqDesignation" meta:resourcekey="lblReqDesignationResource1"
                                                CssClass="lblRequired" ControlToValidate="txtDesignation" ValidateEmptyText="true"
                                                ClientValidationFunction="validateDesgTextBox" runat="server" Display="Dynamic"
                                                ValidationGroup="tab1"></asp:CustomValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblPhno" runat="server" CssClass="requirelbl" AssociatedControlID="txtCountryCode"
                                            meta:resourcekey="lblPhnoResource" />
                                    </div>
                                    <div class="loginControls">
                                        <table border="0" cellpadding="2" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtCountryCode" runat="server" TabIndex="9" MaxLength="4" CssClass="phoneCountry"
                                                        meta:resourcekey="txtCountryCodeResource1" onkeypress="javascript:return onlyNumbers(event,'Countrycode');"
                                                        onpaste="return false;"></asp:TextBox>
                                                </td>
                                                <td valign="middle">
                                                   <asp:Label ID="dash1" runat="server" Text="&#45;"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAreaCode" runat="server" TabIndex="10" MaxLength="5" CssClass="phoneArea"
                                                        meta:resourcekey="txtAreaCodeResource1" onkeypress="javascript:return onlyNumbers(event,'');"
                                                        onpaste="return false;"></asp:TextBox>
                                                </td>
                                                <td valign="middle">
                                                   <asp:Label ID="dash2" runat="server" Text="&#45;"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPhno" runat="server" MaxLength="10" TabIndex="11" CssClass="phoneNumber_long"
                                                        meta:resourcekey="txtPhnoResource1" onkeypress="javascript:return onlyNumbers(event,'');"
                                                        onpaste="return false;"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="reqlbl btmpadding">
                                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqPhno" CssClass="lblRequired"
                                            ControlToValidate="txtPhno" meta:resourcekey="lblReqPhnoResource1" runat="server" 
                                            Display="Dynamic" ValidationGroup="tab1"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblPhoneHelp" runat="server" AssociatedControlID="lblPhoneHelpText"
                                            meta:resourcekey="lblPhoneHelpResource1" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:Label ID="lblPhoneHelpText" runat="server" CssClass="note" meta:resourcekey="lblPhoneHelpTextResource" />
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblCity" runat="server" CssClass="requirelbl" AssociatedControlID="txtCity"
                                            Text="<span>City</span>" meta:resourcekey="lblCityResource" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:TextBox ID="txtCity" runat="server" TabIndex="12" MaxLength="50" CssClass="textBoxMedium"
                                            meta:resourcekey="txtCityResource1"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblreqCity" runat="server" AssociatedControlID="reqCityName" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqCityName" CssClass="lblRequired"
                                            ControlToValidate="txtCity" meta:resourcekey="lblReqCityResource1" runat="server"
                                            Display="Dynamic" ValidationGroup="tab1"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblCountry" runat="server" CssClass="requirelbl" AssociatedControlID="ddlCountry"
                                            meta:resourcekey="lblCountryResource" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:DropDownList ID="ddlCountry" CssClass="dropdownMedium" runat="server" TabIndex="13"
                                            meta:resourcekey="ddlCountryResource1">
                                            <asp:ListItem meta:resourcekey="ListItemResource1">Select</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="reqlbl btmpadding">
                                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqCountry" CssClass="lblRequired"
                                            ControlToValidate="ddlCountry" meta:resourcekey="lblReqCountryResource1" runat="server"
                                            Display="Dynamic" ValidationGroup="tab1"></asp:RequiredFieldValidator></div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="loginlbl">
                                        <asp:Label ID="Label2" runat="server" AssociatedControlID="reCaptchaimg" />
                                    </div>
                                    <div class="loginControls">
                                        <img src="~/In/Captcha.aspx" id="reCaptchaimg" alt="Catptcha Text" runat="server" />
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="loginlbl">
                                    </div>
                                    <div class="loginControls">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="typecaptcha" runat="server" Text="Type the above number"></asp:Label><asp:Label ID="lblRecaptcha" runat="server" AssociatedControlID="txtCaptcha"
                                                        meta:resourcekey="lblRecaptchaResource1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtCaptcha" CssClass="aspxtextbox textBoxSmall" runat="server"  TabIndex="14" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="con_login_pnl">
                                    <div class="loginlbl">
                                    </div>
                                    <div class="loginControls">
                                        <asp:HiddenField ID="hdnKey" runat="server" Value="" />
                                        <asp:HiddenField ID="hdnPrice" runat="server" Value="" />
                                        <asp:HiddenField ID="hdnTax" runat="server" Value="" />
                                        <asp:HiddenField ID="hdnTaxAmount" runat="server" Value="" />
                                        <asp:HiddenField ID="hdnType" runat="server" Value="" />
                                        <asp:HiddenField ID="hdnOrderNo" runat="server" Value="" />
                                        <asp:HiddenField ID="hdnTotal" runat="server" Value="" />
                                        <asp:Label ID="lblSubmit" runat="server" AssociatedControlID="btnContinue" />
                                        <asp:Button ID="btnContinue" runat="server" TabIndex="15" CssClass="btn_small" ToolTip="Continue"
                                            Text="Continue >" ValidationGroup="tab1" OnClick="btnContinue_Click"  /></div>
                                </div>
                            </div>
                            <div id="ulPaymentSummary" class="formList"  runat="server">
                                <div class="myRegisterPanel" >
                                    <div class="RegisterPanel" style="display: none;"> 
                                        <!-- purchase details -->
                                        <h3>
                                            Purchase Details</h3>
                                        <p>
                                            &nbsp;</p>
                                        <table border="0" width="100%" cellpadding="2">
                                            <tr>
                                                <td class="wid_30">
                                                    Membership Type:
                                                </td>
                                                <td class="wid_70">
                                                    <b>
                                                        <asp:Label ID="lblMemberType" runat="server" meta:resourcekey="lblMemberTypeResource" />
                                                        <%--<asp:Label ID="lblPreMemberType" runat="server" meta:resourcekey="lblPreMemberTypeResource" Visible="false" />--%></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="wid_30">
                                                    Subscription Type:
                                                </td>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblSubscriptiontype" runat="server" /></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="wid_30">
                                                    Price:
                                                </td>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblPrice" runat="server" /></b>
                                                </td>
                                            </tr>
                                            <tr id="rowServiceTax" runat="server">
                                                <td class="wid_30" id="tdServiceTax" runat="server">
                                                    Service Tax (14.5%):
                                                </td>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblServiceTax" runat="server" /></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="wid_30">
                                                    Total Price:
                                                </td>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblTotal" runat="server" /></b>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- //purchase details -->
                                    </div>
                                    <div class="RegisterPanel" id="paymentpanel" runat="server" >
                                        <!-- signup panel -->
                                        <h3>
                                            Payment Options
                                        </h3>
                                        <div class="text">
                                            <table border="0" cellpadding="4" cellspacing="0" width="100%" align="center">
                                                <tr>
                                                    <td width="30%" align="right" valign="top">
                                                        <asp:Label ID="lblPaymentType" runat="server" AssociatedControlID="rbtnListPaymentType"
                                                            meta:resourcekey="lblPaymentTypeResource" />
                                                    </td>
                                                    <td width="70%">
                                                        <asp:RadioButtonList ID="rbtnListPaymentType" TabIndex="16" runat="server" RepeatDirection="Horizontal"
                                                            RepeatLayout="Table" meta:resourcekey="rbtnListPaymentTypeResource1" RepeatColumns="3">
                                                            <asp:ListItem Selected="True" Value="1" Text="Credit Card" meta:resourcekey="ListItemResource2"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="Debit Card" meta:resourcekey="ListItemResource3"></asp:ListItem>
                                                             <asp:ListItem Value="5" Text="Netbanking" meta:resourcekey="ListItemResource6"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="Pay by Cheque" meta:resourcekey="ListItemResource4"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="Direct Deposit" meta:resourcekey="ListItemResource5"></asp:ListItem>
                                                               </asp:RadioButtonList>                                                        
                                                        <asp:HiddenField ID="hdnMerchantId" runat="server" Value="0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                <td width="30%">&nbsp;</td>
                                                <td width="70%">
                                                <asp:Label ID="lblpromand" runat="server" Visible="false" Text="**" ForeColor="Red"></asp:Label>
                                                 <asp:Label ID="lblprovalid" runat="server" Visible="false" Text="Pay by Cheque & Direct Deposit are not available for monthly subscriptions" Font-Size="10px"></asp:Label>
                                                </td>
                                                </tr>
                                               <%-- <tr>
                                                    <td colspan="2" class="defaultHeight">
                                                    </td>
                                                </tr>--%>
                                                <tr valign="top">
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblbtnCreate" runat="server" AssociatedControlID="btnCreateAccount" />
                                                        <asp:Button ID="btnCreateAccount" CssClass="btn_medium" runat="server" TabIndex="17"
                                                            meta:resourcekey="lblbtnCreateTooltip" Text="Proceed to Pay" OnClick="btnCreateAccount_Click"
                                                            ValidationGroup="tab2" />
                                                        <asp:HiddenField ID="hdnSecret" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <td valign="top" style="padding-top: 15px;">
                <uc:PurchaseMenu ID="ucPurchaseMenu" runat="server" />
                <%--<uc:AuthenticatedBy ID="ucAuthenticatedBy" runat="server" />--%>
               <%-- <uc:Advertisement ID="ucAdvertisement" runat="server" />--%>
            </td>
        </tr>
        <asp:HiddenField ID="hdnUpgrade" runat="server" Value="No" />
    </table>
    <script type="text/javascript">
        function ShowHide(flag) {
            if (flag == 'show') {
                $('.userOptions').show();
                $('.hdnHideFlag').val('1');
            }
            else {
                $('.userOptions').hide();
                $('.hdnHideFlag').val('0');
            }
        }
        //});
        function validateOrgTextBox(sender, args) {
            var flag = $('.hdnHideFlag').val();
            var value = args.Value;
            if (flag == '0') {
                args.IsValid = true;
            }
            else {
                if (value.length > 0) {
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }
            }
        }
        function validateDesgTextBox(sender, args) {
            var flag = $('.hdnHideFlag').val();
            var value = args.Value;
            if (flag == '0') {
                args.IsValid = true;
            }
            else {
                if (value.length > 0) {
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }
            }

        }
        //Allows only numerics and incase country code textbox allows + also.
        function onlyNumbers(e, Type) {
            var key = window.event ? e.keyCode : e.which;
            if (Type == 'Countrycode' && key == 43) {
                return true;
            }
            else if (key > 31 && (key < 48 || key > 57)) {
                return false;
            }
            return true;
        }
        //Validate the password that contains length between 6-16 character and special character or Upper case letter or
        //Lower case letter or digit 
        function ValidatePassword(oSrc, args) {

            var l = /^\w*(?=\w*[a-z])\w*$/
            var u = /^\w*(?=\w*[A-Z])\w*$/
            var d = /^\w*(?=\w*\d)\w*$/
            var lower = /^.*(?=.*[a-z]).*$/
            var upper = /^.*(?=.*[A-Z]).*$/
            var digit = /^.*(?=.*\d).*$/
            var spl = /^.*(?=.*[@#$%^&+=]).*$/
            var cnt = 0;
            if (lower.test(args.Value)) { cnt = cnt + 1; }
            if (upper.test(args.Value)) { cnt = cnt + 1; }
            if (digit.test(args.Value)) { cnt = cnt + 1; }
            if (spl.test(args.Value)) { cnt = cnt + 1; }
            if (args.Value.length >= 6) {
                if (cnt < 2) {

                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            }
            else {
                args.IsValid = true;
            }
        }
        function onlyAlphabets(e) {         
            var key = (window.event) ? e.keyCode : e.which         
            if ((key >= 65 && key <= 90) || (key >= 97 && key <= 122) || key == 8 || key == 32) {
                return true;
            }
            return false;
        }    
    </script>
    <script type="text/javascript">
        function calculate_time_zone() {
            var rightNow = new Date();
            var jan1 = new Date(rightNow.getFullYear(), 0, 1, 0, 0, 0, 0);  // jan 1st
            var june1 = new Date(rightNow.getFullYear(), 6, 1, 0, 0, 0, 0); // june 1st
            var temp = jan1.toGMTString();
            var jan2 = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
            temp = june1.toGMTString();
            var june2 = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
            var std_time_offset = (jan1 - jan2) / (1000 * 60 * 60);
            var daylight_time_offset = (june1 - june2) / (1000 * 60 * 60);
            var dst;
            if (std_time_offset == daylight_time_offset) {
                dst = "0"; // daylight savings time is NOT observed
            } else {
                // positive is southern, negative is northern hemisphere
                var hemisphere = std_time_offset - daylight_time_offset;
                if (hemisphere >= 0)
                    std_time_offset = daylight_time_offset;
                dst = "1"; // daylight savings time is observed
            }
            var i;
            // check just to avoid error messages
            if (document.getElementById('ContentPlaceHolder1_drpTimeZone')) {
                for (i = 0; i < document.getElementById('ContentPlaceHolder1_drpTimeZone').options.length; i++) {
                    if (document.getElementById('ContentPlaceHolder1_drpTimeZone').options[i].value == convert(std_time_offset) + "," + dst) {
                        document.getElementById('ContentPlaceHolder1_drpTimeZone').selectedIndex = i;                      
                        break;
                    }
                }
            }
        }
        function convert(value) {
            var hours = parseInt(value);
            value -= parseInt(value);
            value *= 60;

            var mins = parseInt(value);
            value -= parseInt(value);
            value *= 60;
            var secs = parseInt(value);
            var display_hours = hours;
            // handle GMT case (00:00)
            if (hours == 0) {
                display_hours = "00";
            } else if (hours > 0) {
                // add a plus sign and perhaps an extra 0
                display_hours = (hours < 10) ? "+0" + hours : "+" + hours;
            } else {
                // add an extra 0 if needed 
                display_hours = (hours > -10) ? "-0" + Math.abs(hours) : hours;
            }

            mins = (mins < 10) ? "0" + mins : mins;

            return display_hours + ":" + mins;
        }
        onload = calculate_time_zone;
        function window_onunload() {
           
        }

    </script>  
    
           <script type="text/javascript" src="/Scripts/facebox.js?v=0.0.8.1"></script>
        <script type="text/javascript" src="/Scripts/common-functions.js?v=0.0.8.1"></script>

<script language="javascript" type="text/javascript" for="window" event="onunload">
// <![CDATA[
return window_onunload()
// ]]>
</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29026379-1']);
    _gaq.push(['_setDomainName', 'insighto.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</asp:Content>

