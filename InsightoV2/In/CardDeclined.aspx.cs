﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using System.Collections;
using CovalenseUtilities.Helpers;
using App_Code;

public partial class In_CardDeclined : System.Web.UI.Page
{
    Hashtable ht = new Hashtable();
    int orderId;
    int userId = -1, pkOrder = -1;
    double Amount = 0, productAmt = 0, taxAmt = 0, totalAmt = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        var userService = new UsersService();
        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0)
            {
                if (ht.Contains("order_id"))
                    orderId = ValidationHelper.GetInteger(ht["order_id"].ToString(), 0);
                if (ht.Contains("Usrid"))
                    userId = ValidationHelper.GetInteger(ht["Usrid"].ToString(), 0);
                if (ht.Contains("Am"))
                    Amount = ValidationHelper.GetDouble(ht["Am"].ToString(), 0);
                if (ht.Contains("pk_order"))
                    pkOrder = ValidationHelper.GetInteger(ht["pk_order"].ToString(), 0);
            }
        }
        //declineInvoice.InnerHtml = "Your transaction bearing Order/Invoice number " + orderId + " has been declined.";
        //lblTransactionInfo.Text = String.Format(Utilities.ResourceMessage("CardDeclined_Failed_Transaction"), orderId); 
        var returnValue = userService.DeclineOrder(userId, pkOrder, 3);
        var orderInfo = userService.GetUserOrderInfo(userId, pkOrder);
        foreach (var order in orderInfo)
        {
            productAmt = Math.Round(order.productAmont, 2);
            taxAmt = Math.Round(order.taxAmount, 2);
            totalAmt = Math.Round(order.Total, 2);
            if (order.orderType == "N" && order.activationFlag == 0)
            {
                txtMessage.Visible = true;
                auth_linktext.Visible = true;

            }
        }
        string strNavigetUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserCheckoutPageURL(), "order=" + orderId + "&Usrid=" + userId + "&Am=" + Amount + "&pk_order=" + pkOrder + "&in_new_order=2");
        RetryUrl.HRef = strNavigetUrl;
    }
}