﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SaasyReturnPage.aspx.cs" Inherits="In_SaasyReturnPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function calculate_time_zone() {
            var rightNow = new Date();
            var jan1 = new Date(rightNow.getFullYear(), 0, 1, 0, 0, 0, 0);  // jan 1st
            var june1 = new Date(rightNow.getFullYear(), 6, 1, 0, 0, 0, 0); // june 1st
            var temp = jan1.toGMTString();
            var jan2 = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
            temp = june1.toGMTString();
            var june2 = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
            var std_time_offset = (jan1 - jan2) / (1000 * 60 * 60);
            var daylight_time_offset = (june1 - june2) / (1000 * 60 * 60);
            var dst;
            if (std_time_offset == daylight_time_offset) {
                dst = "0"; // daylight savings time is NOT observed
            } else {
                // positive is southern, negative is northern hemisphere
                var hemisphere = std_time_offset - daylight_time_offset;
                if (hemisphere >= 0)
                    std_time_offset = daylight_time_offset;
                dst = "1"; // daylight savings time is observed
            }
            var i;
            // check just to avoid error messages
            if (document.getElementById('ContentPlaceHolder1_drpTimeZone')) {
                for (i = 0; i < document.getElementById('ContentPlaceHolder1_drpTimeZone').options.length; i++) {
                    if (document.getElementById('ContentPlaceHolder1_drpTimeZone').options[i].value == convert(std_time_offset) + "," + dst) {
                        document.getElementById('ContentPlaceHolder1_drpTimeZone').selectedIndex = i;
                        break;
                    }
                }
            }
        }
        function convert(value) {
            var hours = parseInt(value);
            value -= parseInt(value);
            value *= 60;

            var mins = parseInt(value);
            value -= parseInt(value);
            value *= 60;
            var secs = parseInt(value);
            var display_hours = hours;
            // handle GMT case (00:00)
            if (hours == 0) {
                display_hours = "00";
            } else if (hours > 0) {
                // add a plus sign and perhaps an extra 0
                display_hours = (hours < 10) ? "+0" + hours : "+" + hours;
            } else {
                // add an extra 0 if needed 
                display_hours = (hours > -10) ? "-0" + Math.abs(hours) : hours;
            }

            mins = (mins < 10) ? "0" + mins : mins;

            return display_hours + ":" + mins;
        }
        onload = calculate_time_zone;
    </script>
</head>
<body>
    <form id="form1" runat="server" method="POST" action="http://sites.fastspring.com/knowience/api/order">
    <div>
      <asp:DropDownList ID="drpTimeZone" runat="server" class="dropdownMedium" style="display:none;">
                                            <asp:ListItem Value="-12:00,0">(-12:00) International Date Line West</asp:ListItem>
                                            <asp:ListItem Value="-11:00,0">(-11:00) Midway Island, Samoa</asp:ListItem>
                                            <asp:ListItem Value="-10:00,0">(-10:00) Hawaii</asp:ListItem>
                                            <asp:ListItem Value="-09:00,1">(-09:00) Alaska</asp:ListItem>
                                            <asp:ListItem Value="-08:00,1">(-08:00) Pacific Time (US & Canada)</asp:ListItem>
                                            <asp:ListItem Value="-07:00,0">(-07:00) Arizona</asp:ListItem>
                                            <asp:ListItem Value="-07:00,1">(-07:00) Mountain Time (US & Canada)</asp:ListItem>
                                            <asp:ListItem Value="-06:00,0">(-06:00) Central America, Saskatchewan</asp:ListItem>
                                            <asp:ListItem Value="-06:00,1">(-06:00) Central Time (US & Canada), Guadalajara, Mexico city</asp:ListItem>
                                            <asp:ListItem Value="-05:00,0">(-05:00) Indiana, Bogota, Lima, Quito, Rio Branco</asp:ListItem>
                                            <asp:ListItem Value="-05:00,1">(-05:00) Eastern time (US & Canada)</asp:ListItem>
                                            <asp:ListItem Value="-04:00,1">(-04:00) Atlantic time (Canada), Manaus, Santiago</asp:ListItem>
                                            <asp:ListItem Value="-04:00,0">(-04:00) Caracas, La Paz</asp:ListItem>
                                            <asp:ListItem Value="-03:30,1">(-03:30) Newfoundland</asp:ListItem>
                                            <asp:ListItem Value="-03:00,1">(-03:00) Greenland, Brasilia, Montevideo</asp:ListItem>
                                            <asp:ListItem Value="-03:00,0">(-03:00) Buenos Aires, Georgetown</asp:ListItem>
                                            <asp:ListItem Value="-02:00,1">(-02:00) Mid-Atlantic</asp:ListItem>
                                            <asp:ListItem Value="-01:00,1">(-01:00) Azores</asp:ListItem>
                                            <asp:ListItem Value="-01:00,0">(-01:00) Cape Verde Is.</asp:ListItem>
                                            <asp:ListItem Value="00:00,0">(00:00) Casablanca, Monrovia, Reykjavik</asp:ListItem>
                                            <asp:ListItem Value="00:00,1">(00:00) GMT: Dublin, Edinburgh, Lisbon, London</asp:ListItem>
                                            <asp:ListItem Value="+01:00,1">(+01:00) Amsterdam, Berlin, Rome, Vienna, Prague, Brussels</asp:ListItem>
                                            <asp:ListItem Value="+01:00,0">(+01:00) West Central Africa</asp:ListItem>
                                            <asp:ListItem Value="+02:00,1">(+02:00) Amman, Athens, Istanbul, Beirut, Cairo, Jerusalem</asp:ListItem>
                                            <asp:ListItem Value="+02:00,0">(+02:00) Harare, Pretoria</asp:ListItem>
                                            <asp:ListItem Value="+03:00,1">(+03:00) Baghdad, Moscow, St. Petersburg, Volgograd</asp:ListItem>
                                            <asp:ListItem Value="+03:00,0">(+03:00) Kuwait, Riyadh, Nairobi, Tbilisi</asp:ListItem>
                                            <asp:ListItem Value="+03:30,0">(+03:30) Tehran</asp:ListItem>
                                            <asp:ListItem Value="+04:00,0">(+04:00) Abu Dhadi, Muscat</asp:ListItem>
                                            <asp:ListItem Value="+04:00,1">(+04:00) Baku, Yerevan</asp:ListItem>
                                            <asp:ListItem Value="+04:30,0">(+04:30) Kabul</asp:ListItem>
                                            <asp:ListItem Value="+05:00,1">(+05:00) Ekaterinburg</asp:ListItem>
                                            <asp:ListItem Value="+05:00,0">(+05:00) Islamabad, Karachi, Tashkent</asp:ListItem>
                                            <asp:ListItem Value="+05:30,0">(+05:30) Chennai, Kolkata, Mumbai, New Delhi, Sri Jayawardenepura</asp:ListItem>
                                            <asp:ListItem Value="+05:45,0">(+05:45) Kathmandu</asp:ListItem>
                                            <asp:ListItem Value="+06:00,0">(+06:00) Astana, Dhaka</asp:ListItem>
                                            <asp:ListItem Value="+06:00,1">(+06:00) Almaty, Nonosibirsk</asp:ListItem>
                                            <asp:ListItem Value="+06:30,0">(+06:30) Yangon (Rangoon)</asp:ListItem>
                                            <asp:ListItem Value="+07:00,1">(+07:00) Krasnoyarsk</asp:ListItem>
                                            <asp:ListItem Value="+07:00,0">(+07:00) Bangkok, Hanoi, Jakarta</asp:ListItem>
                                            <asp:ListItem Value="+08:00,0">(+08:00) Beijing, Hong Kong, Singapore, Taipei</asp:ListItem>
                                            <asp:ListItem Value="+08:00,1">(+08:00) Irkutsk, Ulaan Bataar, Perth</asp:ListItem>
                                            <asp:ListItem Value="+09:00,1">(+09:00) Yakutsk</asp:ListItem>
                                            <asp:ListItem Value="+09:00,0">(+09:00) Seoul, Osaka, Sapporo, Tokyo</asp:ListItem>
                                            <asp:ListItem Value="+09:30,0">(+09:30) Darwin</asp:ListItem>
                                            <asp:ListItem Value="+09:30,1">(+09:30) Adelaide</asp:ListItem>
                                            <asp:ListItem Value="+10:00,0">(+10:00) Brisbane, Guam, Port Moresby</asp:ListItem>
                                            <asp:ListItem Value="+10:00,1">(+10:00) Canberra, Melbourne, Sydney, Hobart, Vladivostok</asp:ListItem>
                                            <asp:ListItem Value="+11:00,0">(+11:00) Magadan, Solomon Is., New Caledonia</asp:ListItem>
                                            <asp:ListItem Value="+12:00,1">(+12:00) Auckland, Wellington</asp:ListItem>
                                            <asp:ListItem Value="+12:00,0">(+12:00) Fiji, Kamchatka, Marshall Is.</asp:ListItem>
                                            <asp:ListItem Value="+13:00,0">(+13:00) Nuku'alofa</asp:ListItem>
                                        </asp:DropDownList>
    </div>
    </form>
</body>
</html>
