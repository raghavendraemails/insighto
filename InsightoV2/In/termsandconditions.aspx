﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true"
    CodeFile="termsandconditions.aspx.cs" Inherits="In_termsandconditions" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="contentPanelHeader">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div class="pageTitle">
                        <!-- page title -->
                        <asp:Label ID ="lblTermsAndConditions" runat="server"  meta:resourcekey="lblTermsAndConditions"></asp:Label> 
                        <!-- //page title -->
                    </div>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="defaultHeight">
        </div>
        <div id="divContent" runat="server">
          <h2 style="font-size:14px;color: #F67803;font-weight:700;">Terms and Conditions of Use</h2>
          <p>
          Thank you for using products and services of Insighto.com.
          <br />
          <br />
          These Terms and Conditions of use contain the terms under which Knowience Insights Private Limited  (the Company that owns Insighto products and services) and 
          its affiliates and resellers provide the products and services to you and describe how these may be accessed and used.
          <br/>
          <br />
          You indicate your agreement to these Terms by clicking or tapping on a button indicating your acceptance of these Terms, by executing a document that references
          to them, or by using the services. 
          <br/>
          <br />
          If you will be using the Insighto products and services on behalf of an organization, you agree to these Terms on behalf of that organization and you represent 
          that you have the authority to do so. In such case, “you” and “your” will refer to that organization.
          <br/>
          <br />
          </p>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="termwid">
                    <ol>
                        <li><b>Definitions</b><br />
                            <br />
                            &ldquo;Knowience&rdquo; means Knowience Insights Pvt. Ltd. - the company that owns and runs Insighto.com for online survey, polls and other related software.
                            <br />
                            <br />
                            &ldquo;Insighto.com&rdquo; or &ldquo;Web Site&rdquo; means the website that facilitates the use of the online survey, polls and other related software.<br />
                            <br />
                            &quot;Content&quot; means the information, data, text, software, applications, games, messages, videos, songs, photographs, graphics and other material published on the website.<br />
                            <br />
                            &ldquo;Software Application Services&rdquo; or &ldquo;Software+&rdquo; means computer software, database, data storage / related systems, online or electronic documentation<br />
                            <br />
                            &quot;Post&quot; means display, exhibit, publish, distribute, transmit and/or disclose information, details and/or other material on the Website, and the phrases &quot;Posted&quot;
                            and &quot;Posting&quot; shall be interpreted accordingly.<br />
                            <br />
                            &quot;Services&quot; means all software application service or any service provided by us through the Web Site.<br />
                            <br />
                            &ldquo;User&rdquo; means any person who uses the Services or visits the Web Site for any purpose.<br />
                            <br />
                        </li>
                        <li><b>The Agreement</b><br />
                            <br />
                            ANY USER ACCESSING INSIGHTO.COM WEBSITE OR ANY OF THE CONTENT HEREIN AGREES TO AND IS BOUND BY THE FOLLOWING TERMS AND CONDITIONS OF INSIGHTO.COM TERMS OF USE (hereinafter
                            the &ldquo;Agreement&rdquo;).<br />
                            <br />
                            This Agreement is a legal agreement between the USER and KNOWIENCE for use of INSIGHTO.COM and its Software+. By using Insighto.com’s Software+, you agree to be bound by the terms of this Agreement.
                            <br />
                            <br />
                        </li>                       
                        <li><b>Price and Payment</b><br />
                            <br />
                            <ol style="list-style-type:decimal">
                                <li>You agree to pay the applicable subscription fees including any government taxes if any for paid use of Insighto.com.</li>
                                <li>Where applicable, you will be billed using the billing method you select at the appropriate pages of the website.</li>
                                <li>Some of our subscriptions are billed on a recurring subscription basis. This means  that you will be billed in advance on a recurring, periodic basis – and will depend on the subscription plan you select at the time of purchase. Your subscription will automatically renew at the end of each billing cycle unless you cancel auto-renewal through the account management page of payment gateway services provider or by contacting our support team. When you cancel your auto-renewal, your subscription will continue till the end of your billing cycle before terminating.</li>
                                <li>Knowience is entitled to alter, revise or modify the pricing on the Website at any time.</li>
                                <li>You will pay all sums due to Knowience under this Agreement by the means specified without any set-off, deduction or counterclaim.</li>
                                <li>All monies paid by you to us are non-refundable and cancellation and/or termination of this Agreement by User or Knowience at any time for any reason (unless required by law) will not entitle you to a refund of monies paid.</li>
                            </ol>
                            <br />
                            <br />
                        </li>
                        <li><b>Account Activation and Management</b><br />
                            <br />
                            <ol style="list-style-type:decimal">
                             <li>You will set your  Username and password yourelf to complete the sign up process on Insighto.com. You alone are responsible for maintaining the confidentiality and integrity of the password and the account, and are fully responsible for all activities related to the account.</li>
                             <li>You are responsible for any activity occurring in your account (other than activity that Insighto(Knowience Insights) is directly responsible for which is not peformed in accordance with customer’s instructions), whether or not you authorized that activity.</li>
                             <li>You agree to (a) immediately notify Knowience of any unauthorized use of your password or account or any other breach of security, and (b) ensure that you logout from your account at the end of each session. Knowience cannot and will not take responsibility and cannot and will not be liable for any loss or damage arising from your failure to comply with this Section.</li>
                             <li>You must keep your account details including your name, contact details and email address accurate. You will receive notices and occasional emails to the email address registered with your account.</li> 
                             <li>You are responsible for maintaining, protecting and making back ups of your content and your survey/poll data reports. To the extent permitted by applicable law, Insighto will not be liable for any failure to store, or for loss or corruption of your content.</li>
                             <li>Insighto may terminate your account and delete any content contained in it if there is no account activity for over 12 months.</li>
                            </ol>
                            <br />
                        </li>
                        <li><b>Your Content creation and ownership </b><br />
                        <br />
                            <ol style="list-style-type:decimal">
                            <li>You retain the ownership of all of your intellectual property rights in your content. These terms do not grant us any licenses or rights to your content except for the limited rights needed by us to provide the services of Insighto and as otherwise described in these Terms.</li>
                            <li>You grant Knowience, a world wide, royalty fre license to use, reproduce, distribute, modify, adapt, create derivative works, make publicly available and otherwise exploit your content, but only for limited purposes of providing Insighto to you and as otherwise permitted by Insighto’s privacy policy. The license for such limited purposes continues even after you stop using Insighto though you may have the ability to delete your content in relation to certain services such that Insighto has no longer access to it. The license also extends to any trusted third parties we work to the extent necessary to provide Insighto to you. If you provide Insighto with feedback on the product and services, we may use the same without any obligation to you.</li>
                            <li>You acknowledge that in order to ensure compliance with legal obligations, Insighto may be required to review certain content to determine whether it is illegal or whether it violates these Terms (such as when unlawful content is reported to us or when any obscene words are used). We may also modify, prevent access to, delete or refuse to display content that we believe violates the law or these Terms.</li> 
                            <li>Knowience may publish links in Insighto to internet websites maintained by third parties. Knowience does not represent that it has reviewed such third party websites and is not responsible for them or any content appearing on them. Trademarks displayed in conjunction with Insighto are the property of their respective owners.</li>
                            </ol>
                            <br />
                            <br />
                        </li>
                        <li><b>Acceptable use Policy</b><br />
                            <br />
                            You agree to comply with these provisions:<br />
                            <br />
                            <ol>
                                <li>You are legally eligible to use Insighto products and services as per the laws of your country in terms of your age, legal status and contractual ability.</li>
                                <li>You will use Insighto products and services in compliance with, and only as permitted by law.</li>
                                <li>You will not use or allow anyone else to use the Web Site to upload, Post, email, transmit or otherwise publish or make available any Content that infringes any patent, trademark, trade secret, copyright works or other proprietary rights ("Rights") of any party; commercial audio, video or music files; any material which violates the law of any established jurisdiction; unlicensed software; software, which assists in or promotes: emulators, freaking, hacking, password cracking, IP spoofing; links to any of the material specified in this paragraph; pornographic material; any material promoting discrimination or animosity to any person on grounds of gender, race or colour.</li>
                                <li>You will not use the Services for spamming. Spamming includes, but is not limited to: the bulk sending of unsolicited messages, or the sending of unsolicited emails which provoke complaints from recipients; the sending of junk mail; the use of distribution lists that include people who have not given specific permission to be included in such distribution process; excessive and repeated Posting off-topic messages to newsgroups; excessive and repeated cross-posting; email harassment of another Internet user, including but not limited to, transmitting any threatening, libelous or obscene material, or material of any nature which could be deemed to be offensive; the emailing of age inappropriate communications or content to anyone under the age of 18.</li>
                                <li>You will not use Insighto in a manner, which violates any city, local, state, national or international law or regulation, or which fails to comply with accepted Internet protocol. You will not attempt to interfere in any way with our networks or network security, or attempt to use the Services to gain unauthorized access to any other computer system.</li>
                                <li>You will immediately notify us, of any security breach or unauthorised use of your account. You will not interfere in any way with another User(s) use of Insighto.</li>
                                <li>You will not resell, rent, lease, grant a security interest in, or make commercial use of Insighto without our express written consent.</li>
                                <li>You agree not to transfer your email address for gain or otherwise. Transfer of such email address will result in immediate termination of your membership and your contract.</li>
                                <li>You agree not to assign, transfer, or authorise any other person to use, your membership.  If you try to do so, we have the right to terminate your membership.</li>
                                <li>You may not engage in abusive or excessive usage of Insighto products and services, which is usage significantly in excess of average usage patterns that adversely affects the speed, responsiveness, stability, availability or functionality of Insighto  for other users.</li> 
                                <li>You will not probe, scan or test the vulnerability of any Insighto product or systems.</li>
                                <li>Unless permitted by applicable law, you should not reverse engineer Insighto products and services</li>
                                <li>You shall not transmit any viruses, malware, or other types of malicious software or links to such software.</li>
                                <li>You may not use Insighto products and services to infringe the intellectual property rights of others or to commit any unlawful activity.</li>
                                <li>Unless authorized by Knowience Insights in writing, you may not resell or lease Insighto products and services.</li>
                                <li>You are responsible for complying with any industry-specific regulations that are applicable to use while using Insighto products and services.</li>
                            </ol>
                            <br />
                            <br />
                        </li>                       
                        <li><b>System Security</b><br />
                            <br />
                            <ol>
                                <li>You agree that you will not, and will not allow any other person to, violate or attempt to violate any aspect of the security of the Web Site.</li>
                                <li>You agree that you will, in no way, modify, reverse engineer, disassemble, decompile, copy, or cause damage or unintended effect to any portion of the Web Site, or any software used on the Web Site, and that you will not permit any other person to do so.</li>
                                <li>You understand that any such violation is unlawful in many jurisdictions and that any contravention of law may result in criminal prosecution.</li>
                                <li>Examples of violations are:
                                    <ul>
                                        <li>accessing data unlawfully or without consent</li>
                                        <li>attempting to probe, scan or test the vulnerability of a system or network or to
                                            breach security or authentication measures</li>
                                        <li>attempting to interfere with service to any user, host or network, including, without
                                            limitation, via means of overloading, &quot;flooding&quot;, &quot;mail bombing&quot;
                                            or &quot;crashing&quot;</li>
                                        <li>forging any TCP/IP packet header or any part of the header information in any e-mail
                                            or newsgroup Posting</li>
                                        <li>taking any action in order to obtain services to which you are not entitled.</li>
                                    </ul>
                                </li>
                            </ol>
                            <br />
                            <br />
                        </li>
                        <li><b>Intellectual Property Rights</b><br />
                            <br />
                            <ol>
                                <li>Insighto Software Application Services are protected by Indian and international intellectual property laws and treaties. The Software Application Services are licensed, not sold. All title and copyrights in and to Software+ [and the Web Site] are owned by Knowience.</li>
                                <li>Title, ownership rights, and intellectual property rights in the Content whether provided by us or by any other Content provider shall remain the sole property of us and / or the other Content provider. We will strongly protect its rights in all countries.</li>
                                <li>You may not copy, modify, publish, transmit, transfer or sell, reproduce, create derivative works from, distribute, perform, display, or in any way exploit any of the Content, in whole or in part, except as is expressly permitted in this Agreement.</li>
                                <li>You may Post into the Services any Content owned by you. You accept all risk and responsibility for determining whether any Content is in the public domain.</li>
                                <li>You represent that any user name or email address selected by you, when used alone or combined with a second or third level domain name, does not interfere with the rights of any third party and has not been selected for any unlawful purpose. You acknowledge and agree that if such selection does interfere with the rights of any third party or is being selected for any unlawful purpose, we may immediately suspend the use of such name or email address, and you will indemnify us for any claim or demand that arises out of your selection. You acknowledge and agree that we shall not be liable to you in the event that we are ordered or required by a court or judicial authority, to desist from using or permitting the use of a particular domain name as part of a name or email address. If as a result of such action, you lose an email address; your sole remedy shall be the receipt of a replacement.</li>
                            </ol>
                            <br />
                            <br />
                        </li>

                         <li><b>Legal Action</b><br />
                            <br />
                             Knowience may initiate such legal action as it may deem necessary or is advised or shall co-operate with any authority and provide all information, material and or such other material and information as it may have access to, to protect its rights as also of its clients, customers, users, and others who may either directly or indirectly be effected in any way.<br />
                            <br />
                        </li>

                        <li><b>Indemnity</b><br />
                            <br />
                            You agree to indemnify us against any claim or demand, including reasonable lawyers’ fees, made by any third party due to or arising out of your use of the Services, the breach or violation of this Agreement by you, or the infringement by you, or by any other user of the Services using your computer or your username and password, of any intellectual property or other right of any person or entity, or as a result of any threatening, libelous, obscene, harassing or offensive material contained in any of your communications.<br />
                            <br />
                        </li>
                         <li><b>Termination</b><br />
                            <br />
                            <ol>
                                <li>You may terminate this Agreement at any time, for any reason, with immediate effect. You may terminate the agreement either by sending notices to us by email. We reserve the right to check the validity of any request to terminate membership.If you terminate a subscription in the middle of the billing cycle, you will not receive a refund for any period of time you did not use in that billing cycle, unless you are terminating the Agreement for our breach and have notified us in writing, or unless a refund is required by law</li>
                                <li>We may terminate this Agreement at any time, for any reason, with immediate effect by sending you notice to that effect by email. If you sent a spam email or registered unlawfully, your account would be terminated without any notice or information. And Insighto would take measures to prevent the further use of the Products and servces by you, including blocking of your IP address.</li>
                                <li>If we terminate, we shall be under no liability to you whatsoever, and you hereby release us from any such liability.</li>
                                <li>Termination by either party shall have the following effects:
                                    <ul>
                                        <li>your right to use the Services immediately ceases;</li>
                                        <li>we are under no obligation to forward any unread or unsent messages to you or any
                                            third party;</li>
                                    </ul>
                                </li>
                                <li>We reserve the right to terminate your email address in the event that our right to use certain domain names or email addresses terminate or expire.</li>
                                <li>We retain the right, at our sole discretion, to terminate any and all parts of Insighto provided to you, without refunding to you any fees paid if we decide in our absolute discretion that you have failed to comply with any of the terms of this Agreement.</li>
                               
                            </ol>
                            <br />
                            <br />
                        </li>

                        <li><b>Interruption to the Service</b><br />
                            <br />
                            <ol>
                                <li>If it is necessary for us to interrupt the Services then we may do so without telling you first.</li>
                                <li>You acknowledge that the Services may also be interrupted for reasons beyond our control.</li>
                                <li>You agree that we are not liable to you for any loss whether foreseeable or not, arising as a result of interruption to the Services.</li>
                            </ol>
                            <br />
                            <br />
                        </li>
                        <li><b>Storage of Data</b><br />
                            <br />
                            <ol>
                                <li>We assume no responsibility for the deletion or failure to store, deliver or timely delivery of messages/emails.</li>
                                <li>We may, from time to time and without notice, set limit(s) on the number of messages/emails  a User may send, store, or receive through the service, and we retain the right to delete any emails above such limit(s) without any liability whatsoever, and you hereby release	 us from any such liability. Any notice provided by us to you in connection with such limit(s) shall not create any obligation to provide future notification regarding any change(s) to such limit(s).</li>
                            </ol>
                            <br />
                            <br />
                        </li>

                        <li><b>Our liability</b><br />
                            <br />
                            <ol>
                                <li>Your use of the Services and the Web Site is without any warranty or guarantee.</li>
                                <li>Where we provide a service without specific charge, then it is deemed to be provided free of charge, and not to be associated with any other service for which a charge is made. Accordingly, there is no contractual or other obligation upon us in respect of any such service.</li>
                                <li>We or our Content suppliers may make improvements or changes to the Web Site, the content, or to any of the products and services described on the Web Site, at any time and without notice to you.</li>
                                <li>You are advised that content may include technical inaccuracies or typographical errors..</li>
                                <li>We give no warranty and make no representation, express or implied, as to:
                                    <ul>
                                        <li>the truth of any information given on the Web Site by any Associate or third party;</li>
                                        <li>suitability, reliability, availability, timeliness, lack of viruses or other harmful components and accuracy of the information, software, products, Services and related graphics contained within the Web Sites;</li>
                                        <li>any implied warranty or condition as to merchantability or fitness for a particular
                                            purpose;</li>
                                        <li>compliance with any law;</li>
                                        <li>Non-infringement of any right.</li>
                                    </ul>
                                </li>
                                <li>Much of the material provided on the Web Site is Posted (and thereby published) by Users. We are under no obligation to monitor, vet, and check or approve any such material. We disclaim all responsibility for information published on the Web Site by any person.</li>
                                <li>The Web Site contains links to other Internet web sites. We have neither power nor control over any such web site. You acknowledge and agree that presence of such link or other means of accessing such third party web site does not constitute endorsement, recommendation, or acceptance of any responsibility for the content of that linked web site or the operators of that linked web site and that we shall not be liable in any way for the content of any such linked web site, nor for any loss or damage arising from your use of any such web site.</li>
                                <li>In no event shall we be liable for any special, indirect or consequential damages or any damages whatsoever resulting from loss of use, loss of data or loss of revenues or profits, whether in an action of contract, negligence or otherwise, arising out of or in connection with the use of the Web Site or the content available from this Web Site.</li>
                               
                            </ol>
                            <br />
                            <br />
                        </li>
                        
                        <li><b>Disclaimers</b><br />
                            <br />
                           <ol>
                           <li>We try to keep our online products and services up, but they may be unavailable from time to time for various reasons. EXCEPT AS EXPRESSLY PROVIDED IN THESE TERMS AND TO THE EXTENT PERMITTED BY APPLICABLE LAW, THE PRODUCTS AND SERVICES ARE PROVIDED “AS IS” AND  DOES NOT MAKE WARRANTIES OF ANY KIND, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING THOSE OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT OR ANY REGARDING AVAILABILITY, RELIABILITY, OR ACCURACY OF THE SERVICES.</li>
<li>TO THE EXTENT PERMITTED BY APPLICABLE LAW,KNOWIENCE, ITS AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, SUPPLIERS, AND LICENSORS WILL NOT BE LIABLE FOR ANY INDIRECT, CONSEQUENTIAL, SPECIAL, INCIDENTAL, PUNITIVE, OR EXEMPLARY DAMAGES WHATSOEVER, INCLUDING DAMAGES FOR LOST PROFITS, LOSS OF USE, LOSS OF DATA, ARISING OUT OF OR IN CONNECTION WITH THE SERVICES AND THESE TERMS, AND WHETHER BASED ON CONTRACT, TORT, STRICT LIABILITY, OR ANY OTHER LEGAL THEORY, EVEN IF SURVEYMONKEY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES AND EVEN IF A REMEDY FAILS OF ITS ESSENTIAL PURPOSE.</li>
<li>TO THE EXTENT PERMITTED BY APPLICABLE LAW, THE AGGREGATE LIABILITY OF EACH OF KNOWIENCE, ITS AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, SUPPLIERS, AND LICENSORS ARISING OUT OF OR IN CONNECTION WITH THE SERVICES AND THESE TERMS WILL NOT EXCEED THE GREATER OF: (A) THE AMOUNTS PAID BY YOU TO USE OF INSIGHTO AT ISSUE DURING THE 3 MONTHS PRIOR TO THE EVENT GIVING RISE TO THE LIABILITY; AND (B) US$25.00.</li>
<li>Consumers. We acknowledge that the laws of certain jurisdictions provide legal rights to consumers that may not be overridden by contract or waived by those consumers. If you are such a consumer, nothing in these Terms limits any of those consumer rights.</li>
<li>Businesses. If you are a business, you will indemnify and hold harmless Knowience and its affiliates, officers, agents, and employees from all liabilities, damages, and costs (including settlement costs and reasonable attorneys’ fees) arising out of a third party claim regarding or in connection with your use of the Services or a breach of these Terms, to the extent that such liabilities, damages and costs were caused by you.</li>
                           </ol>
                           <br />
                            <br />
                        </li>

                        <li><b>Modification/Changes </b><br />
                            <br />
                            We reserve the right to modify the Changes and to change the terms and conditions of this agreement at any time, without notice. Any changes will be posted to the location at which those terms appear. Insighto may also provide notification of changes on the blog or through email. Your continued use of the Services after such modifications shall be deemed an acceptance by you to be bound by the terms of the modified agreement. <br />
                            <br />
                        </li>
                       
                        <li><b>Severability</b><br />
                            <br />
                            If any of these terms is at any time held by any jurisdiction to be void, invalid or unenforceable, then it shall be treated as changed or reduced, only to the extent minimally necessary to bring it within the laws of that jurisdiction and to prevent it from being void and it shall be binding in that changed or reduced form. Subject to that, each provision shall be interpreted as severable and shall not in any way affect any other of these terms.<br />
                            <br />
                        </li>
                        <li><b>Action Limit</b><br />
                            <br />
                            You and we agree that any cause of action arising out of or related to the Services must commence within one year after the cause of action arose; otherwise, such cause of action is permanently barred.<br />
                            <br />
                        </li>
                        
                        <li><b>Force majeure</b><br />
                            <br />
                            <ol>
                                <li>Neither party shall be liable for any breach of its obligations resulting from causes beyond its reasonable control including acts of God, fire, natural disaster, war or military hostilities and strikes of its own employees.</li>
                                <li>Each of the parties agrees to give notice immediately to the other upon becoming aware of an event of force majeure such notice to contain details of the circumstances giving rise to it.</li>
                               
                            </ol>
                            <br />
                            <br />
                        </li>
                        <li><b>No Waiver</b><br />
                            <br />
                           No waiver by us, in exercising any right shall operate as a waiver of any other right or of that same right at a future time; nor shall any delay in exercise of any power or right be interpreted as a waiver. 
                            <br />
                            <br />
                        </li>
                       
                        <li><b>Dispute Resolution</b><br />
                            <br />
                           In the event of a dispute arising out of or in connection with the terms of this Agreement between you and us, then you agree to attempt to settle the dispute by engaging in good faith with us in a process of mediation before commencing arbitration or litigation.<br />
                            <br />
                        </li>
                        <li><b>Jurisdiction</b><br />
                            <br />
                            This Contract shall be interpreted according to the Laws of Republic of India and the parties agree to submit to the exclusive jurisdiction of the Indian courts at Hyderabad. This Agreement shall not be governed by the United Nations Convention on Contracts for the International Sale of Goods, the application of which is hereby expressly excluded. </li>
                    </ol>
                </td>
             
            </tr>
        </table>
        </div>
       
    </div>
</asp:Content>

