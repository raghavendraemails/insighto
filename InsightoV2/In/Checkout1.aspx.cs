﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using App_Code;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
public partial class In_Checkout : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int userID = 0;
        double amount = 0;
        int newOrder = 0;
        int pkOrder = 0;
        string orderNo = "";
        if (Request.QueryString["key"] != null)
        {
            Hashtable ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 && ht.Contains("Userid"))
            {
                userID = ValidationHelper.GetInteger(ht["Userid"].ToString(), 0);
            }
            if (ht != null && ht.Count > 0 && ht.Contains("Amount"))
            {
                amount = ValidationHelper.GetDouble(ht["Amount"].ToString(), 0);
            }
            if (ht != null && ht.Count > 0 && ht.Contains("Order"))
            {
                orderNo = ht["Order"].ToString();
            }
            if (ht != null && ht.Count > 0 && ht.Contains("newOrder"))
            {
                newOrder = ValidationHelper.GetInteger(ht["newOrder"].ToString(), 0);
            }
            if (ht != null && ht.Count > 0 && ht.Contains("pk_order"))
            {
                pkOrder = ValidationHelper.GetInteger(ht["pk_order"].ToString(), 0);
            }
            if (newOrder > 0)
            {
                var userService = ServiceFactory.GetService<UsersService>();       
                var orderList = new List<osm_orderinfo>();
                var orderInfo = ServiceFactory.GetService<OrderInfoService>().GetOrderInfo(pkOrder, userID);
                string latestOrderNo = "";
                double Tax = 0;
                double taxAmount = 0;
                double Total = 0;
                double Price = 0;
                string orderType = "";
                string workingKey = "";
                string orderID = "";
                foreach (var ordInfo in orderInfo)
                {
                    latestOrderNo = ordInfo.ORDERID;
                    Tax = ValidationHelper.GetDouble(ordInfo.TAX_PERCENT.ToString(), 0);
                    taxAmount = ordInfo.TAX_PRICE;
                    Total = ordInfo.TOTAL_PRICE;
                    Price = ordInfo.PRODUCT_PRICE;
                    orderType = ordInfo.ORDER_TYPE;

                }
                int index = latestOrderNo.IndexOf("-");
                latestOrderNo = latestOrderNo.Substring(0, index + 2);
                osm_orderinfo order = new osm_orderinfo();
                if (DateTime.UtcNow.Month < 10)
                    latestOrderNo += "0" + DateTime.UtcNow.Month;
                else
                    latestOrderNo += DateTime.UtcNow.Month;
                latestOrderNo += Convert.ToString(DateTime.UtcNow.Year).Substring(2);
                order.TAX_PERCENT = Tax;
                order.TAX_PRICE = taxAmount;
                order.TOTAL_PRICE = Total;
                order.USERID = userID;
                order.PRODUCT_PRICE = Price;
                order.MODEOFPAYMENT = 1;
                order.ORDERID = latestOrderNo;
                order.ORDER_TYPE = orderType;
                order.CREATED_DATE = DateTime.UtcNow;
                order.TRANSACTION_STATUS = 0;
                orderList.Add(order);
                order = userService.InsertOrderInfo(order);
                if (order!=null)
                {
                    var historyList = new List<osm_useraccounthistory>();
                    osm_useraccounthistory history = new osm_useraccounthistory();
                    history.USERID = userID;
                    history.ORDERID = order.PK_ORDERID.ToString();
                    history.EMAIL_FLAG = 0;
                    history.CREATED_DATE = DateTime.UtcNow;
                    history.CURRENT_LICENSETYPE = orderType;
                    history.LICENSETYPE_OPTED = orderType;
                    historyList.Add(history);
                    var historyId = userService.InsertUserAccountHistory(history);
                    orderID = latestOrderNo + order.PK_ORDERID.ToString();
                    hdnOrderId.Value = orderID;
                }
                List<string> listConfig = new List<string>();
                listConfig.Add(Constants.MERCHANTID);
                listConfig.Add(Constants.REDIRECTIONURL);
                listConfig.Add(Constants.WORKINGKEY);
                var configService = ServiceFactory.GetService<ConfigService>();
                var configvalue = configService.GetConfigurationValues(listConfig);
                foreach (var cv in configvalue)
                {
                    if (cv.CONFIG_KEY == Constants.MERCHANTID)
                    {
                        hdnMerchantId.Value = cv.CONFIG_VALUE; 
                    }
                    if (cv.CONFIG_KEY == Constants.REDIRECTIONURL)
                    {
                        hdnRedirectUrl.Value = cv.CONFIG_VALUE;
                    }
                    if (cv.CONFIG_KEY == Constants.WORKINGKEY)
                    {
                       workingKey = cv.CONFIG_VALUE;
                    }
                }
                Currency.Value = string.Empty;
                hdnChecksum.Value = Utilities.Getchecksum(hdnMerchantId.Value, hdnOrderId.Value, amount.ToString(), hdnRedirectUrl.Value, workingKey.ToString());
                
            }

        }
    }
}