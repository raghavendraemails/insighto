﻿<%@ Page Title="Personalize Email Invitations – Launch Online Survey" Language="C#"
    MasterPageFile="~/Home.master" AutoEventWireup="true" CodeFile="TakeATour_Launch.aspx.cs"
    Inherits="In_TakeATour_Launch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <meta name="description" content="Preview Email invitations and insert the survey link to your email invitations." />
    <meta name="keywords" content="online survey" />
    <div class="takeATourContentPanel">
        <!-- take a tour main content panel -->
        <div class="faqTopMenuPanel">
            <!-- tab links -->
            <ul id="faqNav">
                <li>
                    <asp:HyperLink ID="lnkCreate" runat="server" ToolTip="Creating Online Web Surveys" NavigateUrl="~/In/TakeaTour.aspx"
                        Text="Create"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkDesign" runat="server" NavigateUrl="~/In/TakeATour_Design.aspx"
                        ToolTip="Logo for Branding – Design Surveys" Text="Design"></asp:HyperLink></li>
                <li class="activelink">
                    <asp:HyperLink ID="lnkLaunch" runat="server" NavigateUrl="~/In/TakeATour_Launch.aspx"
                        ToolTip="Launch Online Surveys" Text="Launch"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkAnalyze" runat="server" NavigateUrl="~/In/TakeATour_Analyze.aspx"
                        ToolTip="Manage Responses and Analyze" Text="Analyze"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkManage" runat="server" NavigateUrl="~/In/TakeATour_Manage.aspx"
                        ToolTip="Manage Email Lists" Text="Manage"></asp:HyperLink></li>
            </ul>
            <div class="clear">
            </div>
            <!-- //tab links -->
        </div>
        <div class="clear">
        </div>
        <div class="takeATourTextPanel">
            <!-- text panel -->
            <div class="takeATourButtonPanel">
                <!-- prev next bubtton -->
                <asp:HyperLink ID="lnkPrevius" runat="server" NavigateUrl="javascript:void(0)" CssClass="active"
                    ToolTip="Previous" Text="Prev"></asp:HyperLink>
                <asp:HyperLink ID="lnkNext" runat="server" NavigateUrl="javascript:void(0)" CssClass="active"
                    ToolTip="Next" Text="Next"></asp:HyperLink>
                <!-- //prev next bubtton -->
            </div>
            <div style="display: block" id="Create_1">
                <!-- gettin started -->
                <h3>
                    Send out personalized email invitations to all your contacts</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/launch-img-03.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>Add first and last names of your contacts from email list</li>
                    <li>Three ways to create email invitation</li>
                    <ul>
                        <li>Include the introduction of your survey into the email invitation with just one
                            click OR</li>
                        <li>Edit the default text given OR</li>
                        <li>Copy and paste new invite from MS Word</li>
                    </ul>
                    <li>Insert the survey link wherever you wish in the email invitation </li>
                </ul>
                <!-- //gettin started -->
            </div>
            <div style="display: none" id="Create_2">
                <!-- question types -->
                <h3>
                    <%--Launch through powerful Insighto’s email system--%>
                    Launch your survey through Insighto’s powerful email system
                    </h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/launch-img-02.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>Create your email list and add contacts to the list</li>
                    <li>Deploy survey to the entire list at one go -Simple</li>
                </ul>
                <!-- //question types -->
            </div>
            <div style="display: none" id="Create_3">
                <!-- answer options -->
                <h3>
                    Launch your survey by generating a unique URL</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/launch-img-01.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>The simplest way to launch your survey</li>
                    <li>Generate a unique URL for your survey</li>
                    <li>Send this link through your email to as many people as you want</li>
                    <li>Start collecting the responses instantly</li>
                </ul>
                <!-- answer options -->
            </div>
            <div style="display: none" id="Create_4">
                <!-- questionnair -->
                <h3>
                    Redirect your respondents to a website of your preference after the survey</h3>
                <p style="padding-top: 10px;">
                    >
                    <img src="../images/homeimages/launch-img-04.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>Just enter the name of the website you want the respondents to be redirected</li>
                    <li>You can also specify the time in seconds after which the survey respondents would
                        be automatically redirected to the website of your choice</li>
                </ul>
                <!-- questionnair -->
            </div>
            <div style="display: none" id="Create_5">
                <!-- preview survey -->
                <h3>
                    Schedule your launch or close of survey for time of your choice</h3>
                <p style="padding-top: 10px;">
                    >
                    <img src="../images/homeimages/launch-img-05.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>Launch or close survey manually or on a specific date or after reaching specific
                        number of responses</li>
                    <li>Set alerts to keep tab on survey responses</li>
                </ul>
                <!-- preview survey -->
            </div>
            <div style="display: none" id="Create_6">
                <!-- review survey -->
                <h3>
                    Password protect your surveys</h3>
                <p style="padding-top: 10px;">
                    >
                    <img src="../images/homeimages/launch-img-06.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <br />
                <ul>
                    <li>Extra security if you are sending a survey to a closed group – For e.g., Employees</li>
                    <li>Include a password along with your email invitation to control who takes the survey</li>
                    <li>Stop unauthorized people from responding to your survey</li>
                </ul>
                <!-- review survey -->
            </div>
            <!-- //text panel -->
        </div>
        <div class="takeATourSubMenuPanel">
            <!-- sub menu -->
            <ul class="takeATourMenu">
                <li><a id="lnkCreate_1" class="active" href="javascript:void(0);" onclick="javascript:ShowSelectedScreen(1);">
                    Personalize Email Invitations</a></li>
                <li><a id="lnkCreate_2" href="javascript:void(0);" onclick="javascript:ShowSelectedScreen(2);">
                    Launch Through Insighto</a></li>
                <li><a id="lnkCreate_3" href="javascript:void(0);" onclick="javascript:ShowSelectedScreen(3);">
                    Launch Through Weblink</a></li>
                <li><a id="lnkCreate_4" href="javascript:void(0);" onclick="javascript:ShowSelectedScreen(4);">
                    Redirect URL</a></li>
                <li><a id="lnkCreate_5" href="javascript:void(0);" onclick="javascript:ShowSelectedScreen(5);">
                    Schedule Alerts / Deadlines</a></li>
                <li><a id="lnkCreate_6" href="javascript:void(0);" onclick="javascript:ShowSelectedScreen(6);">
                    Password Protected Surveys</a></li>
            </ul>
            <!-- //sub menu -->
        </div>
         <input type="hidden" class="hdnPanelIndex" id="hdnPanelIndex" value="1" />
        <!-- // take a tour main content panel -->
    </div>
    
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('.hdnPanelIndex').val() == '1') {
                $('#<%=lnkPrevius.ClientID %>').hide();
            }
        });
        $('#<%=lnkNext.ClientID %>').click(function () {
            var index = $('.hdnPanelIndex').val();
            $('#Create_' + index).hide();
            $('#lnkCreate_' + index).removeClass('active');
            index++;
            $('#Create_' + index).show();
            $('.hdnPanelIndex').val(index);
            if ($('.hdnPanelIndex').val() > 1) {
                $('#<%=lnkPrevius.ClientID %>').show();
                $('#lnkCreate_' + index).addClass('active');
                if (index == 6) {
                    $('#<%=lnkNext.ClientID %>').hide();
                }
            }
        });
        $('#<%=lnkPrevius.ClientID %>').click(function () {
            var index = $('.hdnPanelIndex').val();
            $('#Create_' + index).hide();
            $('#lnkCreate_' + index).removeClass('active');
            index--;
            $('#Create_' + index).show();
            $('.hdnPanelIndex').val(index);
            if ($('.hdnPanelIndex').val() < 6) {
                $('#<%=lnkNext.ClientID %>').show();
                $('#lnkCreate_' + index).addClass('active');
                if (index == 1) {
                    $('#<%=lnkPrevius.ClientID %>').hide();
                }
            }
        });
        function ShowSelectedScreen(index) {
            if (index == 1) {
                $('#<%=lnkPrevius.ClientID %>').hide();
                $('#<%=lnkNext.ClientID %>').show();
            }
            else if (index == 6) {
                $('#<%=lnkNext.ClientID %>').hide();
                $('#<%=lnkPrevius.ClientID %>').show();
            }
            else {
                $('#<%=lnkNext.ClientID %>').show();
                $('#<%=lnkPrevius.ClientID %>').show();
            }
            var prevIndex = $('.hdnPanelIndex').val();
            $('#Create_' + prevIndex).hide();
            $('#lnkCreate_' + prevIndex).removeClass('active');
            $('#Create_' + index).show();
            $('#lnkCreate_' + index).addClass('active');
            $('.hdnPanelIndex').val(index);

        }
    </script>
</asp:Content>
