﻿using System;
using System.Drawing;
using System.IO;
using Insighto.Business.Services;
using CovalenseUtilities.Extensions;
using CovalenseUtilities.Services;

public partial class Captcha : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
     
        Response.ContentType = "text/plain";
        Random random = new Random(DateTime.Now.Millisecond);

        int number = random.Next(100000);
        ServiceFactory.GetService<SessionStateService>().AddCaptchaToSession(number.ToString());       

        System.Drawing.Bitmap bmpOut = new Bitmap(80, 35);

        Graphics graphics = Graphics.FromImage(bmpOut);

        graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bilinear;

        //graphics.FillRectangle(Brushes.Aquamarine, 0, 0, 100, 50);

        graphics.DrawString(number.ToString(), new System.Drawing.Font("TimesNewRoman", 20f), new SolidBrush(Color.Coral), 0, 0);

        MemoryStream memorystream = new MemoryStream();

        bmpOut.Save(memorystream, System.Drawing.Imaging.ImageFormat.Png);

        byte[] bmpBytes = memorystream.GetBuffer();

        bmpOut.Dispose();

        memorystream.Close();

        Response.BinaryWrite(bmpBytes);
        

        Response.End();

    }
}
