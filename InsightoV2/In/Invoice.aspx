﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Invoice.aspx.cs" Inherits="Insighto.Pages.Invoice" MasterPageFile="~/ModelMaster.master" meta:resourcekey="PageResource1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div class="popupBody">
    <div class="popupHeading" id="divClose" runat="server">       
        <div class="popupTitle"></div>
      <%--  <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick="closeModalSelf(false,'');">&nbsp;</a>
        </div>  --%>     
    </div>
      <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">     
        <tr>
          <td><table width="600" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="281"><img src="../App_Themes/Classic/Images/Insighto_logo.png" /></td>
                <td align="right" valign="top" id="knowience_address" runat="server"></td>
              </tr>
            </table>
            <table width="600" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="middle"><img src="images/line.gif" width="260" height="1" /></td>
                <td width="80" align="center"> <strong> <asp:Label ID="lblInvoice" runat="server" 
                        Text="INVOICE" meta:resourcekey="lblInvoiceResource1"></asp:Label></strong></td>
                <td width="260" valign="middle"><img src="images/line.gif" width="260" height="1" /></td>
              </tr>
            </table>
            <br />
            <table width="600" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="275" align="left" valign="top"><table width="275" border="1" cellpadding="2" cellspacing="0" bordercolor="#999999">
                    <tr>
                      <td bgcolor="#CCCCCC"><strong> <asp:Label ID="lblBillTo" runat="server" 
                              Text="Bill To :" meta:resourcekey="lblBillToResource1"></asp:Label></strong> </td>
                    </tr>
                    <tr>
                      <td height="80" valign="top" id=billaddress runat=server></td>
                    </tr>
                  </table></td>
                <td align="right" valign="top"><table width="275" border="1" cellpadding="2" cellspacing="0" bordercolor="#999999">
                    <tr>
                      <td align="left" bgcolor="#CCCCCC"><strong>
                          <asp:Label ID="lblInvoiceDetails" runat="server" Text="Invoice Details  :" 
                              meta:resourcekey="lblInvoiceDetailsResource1"></asp:Label></strong> </td>
                    </tr>
                    <tr>
                      <td height="80" align="left" valign="top" id=invoice_info runat=server></td>
                    </tr>
                  </table></td>
              </tr>
            </table>
            <br />
            <br />
            <table width="600" border="1" cellpadding="2" cellspacing="0" bordercolor="#999999">
              <tr>
                <td width="150" bgcolor="#CCCCCC"><strong>
                    <asp:Label ID="lblItem" runat="server" Text="Item" 
                        meta:resourcekey="lblItemResource1"></asp:Label></strong></td>
                <td width="270" bgcolor="#CCCCCC"><strong>
                    <asp:Label ID="lblDescription" runat="server" Text="Description" 
                        meta:resourcekey="lblDescriptionResource1"></asp:Label></strong></td>
                <td width="45" align="right" bgcolor="#CCCCCC"><strong><asp:Label ID="lblQty" 
                        runat="server" Text="Qty" meta:resourcekey="lblQtyResource1"></asp:Label></strong></td>
                <td width="60" align="right" bgcolor="#CCCCCC"><strong><asp:Label ID="lblRate" 
                        runat="server" Text="Rate" meta:resourcekey="lblRateResource1"></asp:Label>
                        <asp:Label ID="lblRateCurrency" runat="server" ></asp:Label>
                        </strong></td>
                <td width="75" align="right" bgcolor="#CCCCCC"><strong><asp:Label ID="lblAmount" 
                        runat="server" Text="Amount" meta:resourcekey="lblAmountResource1"></asp:Label>
                        <asp:Label ID="lblAmountCurrency" runat="server" ></asp:Label>
                        </strong></td>
              </tr>
              <tr>
                <td width="150" height="220" valign="top" id=item_Name runat=server></td>
                <td width="270" valign="top" id=item_Description runat=server></td>
                <td width="45" align="right" valign="top" runat=server id=item_Qty></td>
                <td width="60" align="right" valign="top" runat=server id=item_Rate></td>
                <td width="75" align="right" valign="top" runat=server id=item_Amt></td>
              </tr>
            </table>
            <table width="600" border="0" cellspacing="0" cellpadding="4">
              <tr>
                <td align="left" valign="top"><em><strong>
                    <asp:Label ID="lblThankyou" runat="server" 
                        Text="Thank you for choosing Insighto.com.<br />   We appreciate your business." 
                        meta:resourcekey="lblThankyouResource1"></asp:Label>  </strong></em></td>
                <td width="290" align="right" valign="top"><table width="270" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="170" height="24" align="right" valign="middle">
                          <asp:Label ID="lblSubTotal" runat="server" Text="Sub Total :" 
                              meta:resourcekey="lblSubTotalResource1"></asp:Label> </td>
                      <td width="100" height="24" align="right" valign="middle" id=subtotal runat=server></td>
                    </tr>
                    <tr>
                      <td width="170" height="24" align="right" valign="middle" id=service_tax runat=server></td>
                      <td width="100" height="24" align="right" valign="middle" id=taxt_amt runat=server></td>
                    </tr>
                    <tr>
                      <td height="18" colspan="2" align="right" valign="middle"><img src="images/line.gif" width="260" height="1" /></td>
                    </tr>
                    <tr>
                      <td width="170" height="24" align="right" valign="middle"><strong>
                          <asp:Label ID="lblTotal" runat="server" Text="Total :" 
                              meta:resourcekey="lblTotalResource1"></asp:Label> </strong></td>
                      <td width="100" height="24" align="right" valign="middle" id=total_amt runat=server></td>
                    </tr>
                    <tr>
                      <td height="18" colspan="2" align="right" valign="middle"><img src="images/line.gif" width="260" height="1" /></td>
                    </tr>
                  </table></td>
              </tr>
            </table>
            <br>
            <br>
            <table width="600" border="1" cellspacing="0" cellpadding="4">
              <tr>
                <td width="100%" align="left" valign="top" id="payment_instructions" runat="server"></td>
              </tr>
            </table>
            <br>
            <table width="600" border="0" cellpadding="2" cellspacing="0" bordercolor="#999999">
              <tr>
                <td valign="top" bgcolor="#F5F5F5" style="padding:5px 0 5px 0"><ul>
                    <li><asp:Label ID="lblInstrSystemGen" runat="server" 
                            Text="This is a system generated invoice and does not require signature." 
                            meta:resourcekey="lblInstrSystemGenResource1"></asp:Label></li>                    
                    <li><asp:Label ID="lblInstrTerms" runat="server" 
                            Text="Making payment for this invoice is accepting the T&amp;C and Privacy policy on" 
                            meta:resourcekey="lblInstrTermsResource1"></asp:Label> <a id="lnkUrl" runat="server" target="_blank">www.insighto.com</a></li>
                    <li><asp:Label ID="lblInstrContact" runat="server" 
                            Text="For any queries and support, use" 
                            meta:resourcekey="lblInstrContactResource1"></asp:Label> <a id="lnkContactus" runat="server" target="_blank">Contact Us</a></li>
                    <li><asp:Label ID="lblInstrServiceTax" runat="server" 
                            Text="Service Tax Code: AADCK4420AST001  - PAN:AADCK4420A" 
                            meta:resourcekey="lblInstrServiceTaxResource1"></asp:Label></li>
                  </ul></td>
              </tr>
            </table></td>
        </tr>        
      </table>
      <p>&nbsp;</p>
    </div>
</asp:Content>
