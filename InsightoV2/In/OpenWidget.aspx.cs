﻿using System;
using System.Collections;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;

public partial class In_OpenWidget : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Hashtable ht = new Hashtable();
        int surveyId = 0, mode = 0, launchId = 0, passFlag = 0,layout=1;
        if (Request.QueryString["key"] != null)
        {
            string key = Request.Params["key"];
            key = key.Replace("@", "");
            key = key.Replace(' ', '+');
            ht = EncryptHelper.DecryptQuerystringParam(key);
            if (ht != null && ht.Count > 0 && ht.Contains("SurveyId"))
            {
                surveyId = Convert.ToInt32(ht["SurveyId"]);
            }
            if (ht.Contains("Mode"))
            {
                mode = Convert.ToInt32(ht["Mode"]);
            }
            if (ht.Contains("LaunchId"))
            {
                launchId = Convert.ToInt32(ht["LaunchId"]);
            }
            if (ht.Contains("PassVerf"))
            {
                passFlag = Convert.ToInt32(ht["PassVerf"]);
            }
             if (ht.Contains("Layout"))
            {
                layout = Convert.ToInt16(ht["Layout"]);
            }
            
        }
        if (Request.QueryString["DomainName"] != null)
        {
            string finalDomainName = "";
            string DomainName = Request.Params["DomainName"];
            //string DomainName = "http://www.covalense.com/";
            string[] arrDomainName = DomainName.Split('/');
            if (arrDomainName.Length > 0)
            {
                DomainName = arrDomainName[2].ToString();
            }

            finalDomainName = "http://" + DomainName;
            string strTitle = "";
            string strItemName = "";
            if (Request.QueryString["Title"] != null)
            {
                strTitle = Request.Params["Title"];
            }
            if (Request.QueryString["ItemName"] != null)
            {
                strItemName = Request.Params["ItemName"];
            }

            if (ValidateDomain(surveyId, finalDomainName))
            {
                string strNavUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL1(), "WidgetValid=1&Title=" + strTitle + "&ItemName=" + strItemName + "&SurveyId=" + surveyId.ToString() + "&Layout=" + layout);
                Response.Redirect(strNavUrl);
            }
            else
            {
                //lblErrMsg.Text = "You are not valid user for this widget.";
                lblErrMsg.Visible = true;
                dvErrMsg.Visible = true;
            }
        }
    }
    private bool ValidateDomain(int surveyId, string DomainName)
    {
        bool flag = false;
        var widget = ServiceFactory.GetService<WidgetService>().GetWidgetDetailsBySurveyId(surveyId);
        string strDomainName = widget.DomainName;
        if (strDomainName == DomainName)
        {
            flag = true;
        }
        else
        {
            flag = false;
        }
        return flag;
    }


}