﻿using System;
using System.Collections;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;



namespace Insighto.Pages.In
{
    public partial class In_Pricing : BasePage
    {
        #region "Variables"

        Hashtable typeHt = new Hashtable();
        int userId = 0;
        string transType = "New";
        string country;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            string ipAddress = "";

           

            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
                }               
                //ipAddress = "125.236.193.250";
                var country = Utilities.GetCountryName(ipAddress);  
                
                ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
                //  ipAddress = "125.236.193.250";
               // country = Utilities.GetCountryName(ipAddress);
                ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
                ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
                country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
            //}
            if (!IsPostBack)
            {     
                if (country.ToUpper() == "INDIA")
                {
                    lblMonthly.Attributes.Add("Class", "priceRs");
                    divFaqOtherContent.Visible = false;
                    divFaqIndiaContent.Visible = true;
                }
                else
                {
                    lnkBtnMonthly.Text = string.Format("{0}{1}{2}{3}", "SAVE ON ANNUAL PLAN - ", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), " ", GetPriceByCountryName(country, "Yearly"));
                    lblMonthly.Text = string.Format("{0}{1}{2}{3}", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), " ", GetPriceByCountryName(country, "MONTHLY"), " / MONTH");
                    lblMonthly.Attributes.Add("Class", "monthlyHeading");
                    divFaqOtherContent.Visible = true;
                    divFaqIndiaContent.Visible = false;
                }

                dvPageInfo.InnerHtml = Utilities.ResourceMessage("dvPageInfo");
                FormatCurrencyAndSymbol();
                SetPermissions();
            }
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
                if (typeHt.Contains(Constants.USERID))
                    userId = ValidationHelper.GetInteger(typeHt[Constants.USERID].ToString(), 0);
                if (typeHt.Contains("Flag"))
                    transType = Convert.ToString(typeHt["Flag"]);
            }
        }

        /// <summary>
        /// Sets the permissions.
        /// </summary>
        private void SetPermissions()
        {
            var userType = Utilities.GetUserType();
            if (userType != UserType.None)
            {
                imgTryItFree.Enabled = false;
                imgTryItFree.Style.Add("cursor", "default");
                if (userType == UserType.PRO_MONTHLY)
                {
                    rbtnMonthly.Checked = true;
                }
                else if (userType == UserType.PRO_QUARTERLY)
                {
                    rbtnQuarterly.Checked = true;
                }
                else if (userType == UserType.PRO_YEARLY)
                {
                    rbtnYear.Checked = true;
                }
            }
            else
            {
                rbtnMonthly.Enabled = true;
                imgTryItFree.Enabled = true;
                rbtnYear.Checked = true;
            }
        }

        /// <summary>
        /// Formats the currency and symbol.
        /// </summary>
        private void FormatCurrencyAndSymbol()
        {
            rbtnMonthly.Text = Utilities.FormatCurrency(Math.Round(450.00, 2), false, true);
            rbtnQuarterly.Text = Utilities.FormatCurrency(Math.Round(1300.00, 2), false, true);
            rbtnYear.Text = Utilities.FormatCurrency(Math.Round(4500.00, 2), false, true);
        }

        protected void imgbtnMonthly_Click(object sender, ImageClickEventArgs e)
        {

            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.USERID))
                    userId = int.Parse(typeHt[Constants.USERID].ToString());
            }

            string Navurl = "";
            string type = "";

            //if (rbtnMonthly.Checked)
            //{
            //    type = "PRO_MONTHLY";
            //    Navurl = EncryptHelper.EncryptQuerystring(PathHelper.GetRegisterURL(), Constants.USERID + "=" + userId + "&Type=" + type + "&Flag=" + transType);
            //}
            //else if (rbtnQuarterly.Checked)
            //{
            //    type = "PRO_QUARTERLY";
            //    Navurl = EncryptHelper.EncryptQuerystring(PathHelper.GetRegisterURL(), Constants.USERID + "=" + userId + "&Type=" + type + "&Flag=" + transType);
            //}
            //else
            //{
            type = "PRO_MONTHLY";
            Navurl = EncryptHelper.EncryptQuerystring(PathHelper.GetRegisterURL(), Constants.USERID + "=" + userId + "&LicenseType=" + type + "&Flag=" + transType);
            //}
            Response.Redirect(Navurl);
        }

        protected void lnkBtnYearly_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.USERID))
                    userId = int.Parse(typeHt[Constants.USERID].ToString());
            }
            string navUrl = "";
            string type = "";
            type = "PRO_YEARLY";
            navUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetRegisterURL(), Constants.USERID + "=" + userId + "&LicenseType=" + type + "&Flag=" + transType);
            Response.Redirect(navUrl);
        }

        /// <summary>
        /// Handles the Click event of the imgTryItFree control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
        protected void imgTryItFree_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/In/SignUpFree.aspx");
        }
        private string GetCurrencySymbol(string countryName)
        {
            var ci = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.EnglishName.Contains(countryName)).FirstOrDefault();
            return ci.NumberFormat.CurrencySymbol;
        }

        private double GetPriceByCountryName(string countryNAme, string subscription)
        {
            var price = ServiceFactory<OrderInfoService>.Instance.GetPriceByCountryName(countryNAme);
            if (subscription == "MONTHLY")
                return Convert.ToDouble(price.MONTHLY_PRICE);
            else
                return Convert.ToDouble(price.YEARLY_PRICE);
        }

    }
}