﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;

public partial class In_UserControls_ProAdvertisement : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string NavurlYear = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", "Type=PRO_YEARLY");
        PayYear.HRef = NavurlYear;
        string NavurlMonth = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", "Type=PRO_MONTHLY");
        PayMonth.HRef = NavurlMonth;       
    }
}