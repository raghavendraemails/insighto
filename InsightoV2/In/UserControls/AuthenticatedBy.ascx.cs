﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Business.Services;
using System.Data;
using Insighto.Data;
using System.Collections;
using System.IO;
using System.Globalization;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using CurrencyExchangeRates;
using Resources;

public partial class In_UserControls_AuthenticatedBy : System.Web.UI.UserControl
{
    string countryName = "";
        SurveyCore surcore = new SurveyCore();
    protected void Page_Load(object sender, EventArgs e)
    {
         countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
            if (countryName == null)
            {
                string ipAddress;
                if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
                }
            
                countryName = Utilities.GetCountryName(ipAddress);
                ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(countryName);
                ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
                countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
               
            }
            if (countryName.ToUpper() == "INDIA")
            {
                imgSecure.Visible = true;
                imgIndian1.Visible = false;
                imgforeign.Visible = false;
                Image1.Visible = false;
            }
            else
            {
                imgSecure.Visible = false;
                imgIndian1.Visible = false;
                imgforeign.Visible = true;
                Image1.Visible = true;
            }
           
    }
}