﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProAdvertisement.ascx.cs" Inherits="In_UserControls_ProAdvertisement" %>
     <div class="buyNowPanel" style="float: left; height: auto; ">
                                            <!-- buy now pannel -->
                                            <div class="buyNowHeader">
                                                <div>
                                                   <asp:Label ID="lblInsightoPro" runat="server" Text="Insighto Pro" 
                                                        meta:resourcekey="lblInsightoProResource1"></asp:Label>
                                                    <asp:Label ID="lblIsForYou" runat="server" Text=" is for you" 
                                                        meta:resourcekey="lblIsForYouResource1"></asp:Label></div>
                                            </div>
                                            <div class="buyNowContentPanel" style="height: auto; padding-bottom:5px;">
                                               <asp:Label ID="lblIfyouneed" runat="server" Text ="If you need" 
                                                    meta:resourcekey="lblIfyouneedResource1"></asp:Label>
                                                <ul class="proList" style="margin-bottom:5px;">
                                                    <li><asp:Label ID="lblMorethan100" runat="server" 
                                                            Text="More than 100 responses per survey" 
                                                            meta:resourcekey="lblMorethan100Resource1"></asp:Label></li>
                                                    <li><asp:Label ID="lblReportsIngraphs" runat="server" Text="Reports in graphs" 
                                                            meta:resourcekey="lblReportsIngraphsResource1"></asp:Label></li>
                                                    <li><asp:Label ID="lblEmailSupport" runat="server" Text="Email support" 
                                                            meta:resourcekey="lblEmailSupportResource1"></asp:Label></li>
                                                </ul>
                                                <b><asp:Label ID="lblJustINR" Text="Just INR 4,500 / annum." runat="server" 
                                                    meta:resourcekey="lblJustINRResource1"></asp:Label></b>
                                                <div style="margin-top:5px;">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left">
                                                                <a id="PayYear" runat="server">
                                                                    <img src="../images/homeimages/button_buy-now.gif" alt="Buy Now" title="Buy Now" /></a>
                                                            </td>
                                                            <td style="padding-left: 10px;">
                                                                <a id="PayMonth" runat="server" class="orangeLinksml" title="See monthly plan">
                                                                <asp:Label ID="lblSeeMonthlyPlan" runat="server" Text="See monthly plan" 
                                                                    meta:resourcekey="lblSeeMonthlyPlanResource1"></asp:Label></a>
                                                            </td>
                                                        </tr>
                                                     
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="buyPanelBottom">
                                            </div>
                                            <!-- //buy now pannel -->
                                        </div>