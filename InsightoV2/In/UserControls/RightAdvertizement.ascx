﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RightAdvertizement.ascx.cs" Inherits="In_UserControls_RightAdvertizement" %>

 <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="top" style="padding-bottom:8px;">
                                        <div class="buyNowPanel" style="float: left; height: auto; margin-top:-5px;">
                                            <!-- buy now pannel -->
                                            <div class="buyNowHeader">
                                                <div><asp:Label ID="lblValuableCustomerInsights" 
                                                        Text="Valuable customer insights. Free!" runat="server" 
                                                        meta:resourcekey="lblValuableCustomerInsightsResource1"></asp:Label></div>
                                            </div>
                                            <div class="buyNowContentPanel" style="height:auto; padding-bottom:5px;">
                                                <ul class="proList" style="margin-bottom:0;">
                                                    <li><asp:Label ID="lblVeryUser" runat="server" Text="Very user-friendly tool" 
                                                            meta:resourcekey="lblVeryUserResource1"></asp:Label></li>
                                                    <li><asp:Label ID="lblSurveyTemplate" runat="server" 
                                                            Text="Survey templates for help" meta:resourcekey="lblSurveyTemplateResource1"></asp:Label></li>
                                                    <li><asp:Label ID="lblNoneed" runat="server" Text="No need to install software" 
                                                            meta:resourcekey="lblNoneedResource1"></asp:Label></li>
                                                    <li><asp:Label ID="lblSimplycreate" runat="server" 
                                                            Text="Simply create, send & analyze" 
                                                            meta:resourcekey="lblSimplycreateResource1"></asp:Label></li>
                                                </ul>
                                                <div>
                                                    
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="padding-bottom:5px;">
                                                                    <b><asp:Label ID="lblStarusing" runat="server" Text="Start using Insighto" 
                                                                        meta:resourcekey="lblStarusingResource1"></asp:Label></b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a id="hlinkFree" runat="server" href="~/In/SignUpFree.aspx">
                                                                        <img src="../images/homeimages/button_free.png" alt="It's Free" title="It's Free" /></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    
                                                </div>
                                            </div>
                                           <div class="buyPanelBottom"></div>
                                            
                                            <!-- //buy now pannel -->
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="buyNowPanel" style="float: left; height: auto; ">
                                            <!-- buy now pannel -->
                                            <div class="buyNowHeader">
                                                <div>
                                                    <asp:Label ID="lblInsightoForAdvance" runat="server" 
                                                        Text="Insighto for advanced users" 
                                                        meta:resourcekey="lblInsightoForAdvanceResource1"></asp:Label>                                                      
                                                        
                                                        </div>
                                            </div>
                                            <div class="buyNowContentPanel" style="height: auto; padding-bottom:5px;">
                                             <%--  <asp:Label ID="lblIfyouneed" runat="server" Text="If you need" 
                                                    meta:resourcekey="lblIfyouneedResource1"></asp:Label>--%>
                                                     <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:HyperLink ID="HyperLink1"   runat="server" NavigateUrl="http://www.insighto.com/pricing/"
                        Text="See Plans"></asp:HyperLink>
                </p>



                                                <ul class="proList" style="margin-bottom:5px;">
                                                    <li><asp:Label ID="lblUnlimited" runat="server" 
                                                            Text="Unlimited questions & responses" meta:resourcekey="lblUnlimitedResource1"></asp:Label> </li>
                                                            <li><asp:Label ID="Label2" runat="server" Text="Email support" 
                                                            meta:resourcekey="lblViewRessultResource1"></asp:Label></li>
                                                    <li><asp:Label ID="lblReports" runat="server" Text="Reports in graphs" 
                                                            meta:resourcekey="lblReportsResource1"></asp:Label></li>
                                                    <li><asp:Label ID="lblEmailSupport" runat="server" Text="Email support" 
                                                            meta:resourcekey="lblEmailSupportResource1"></asp:Label></li>
                                                             <li><asp:Label ID="Label1" runat="server" Text="Email support" 
                                                            meta:resourcekey="lblBrandResource1"></asp:Label></li>                                                             
                                                </ul>
                                                <b><asp:Label ID="lblINR" runat="server" Text="" 
                                                    meta:resourcekey="lblINRResource1"></asp:Label></b>
                                                <div style="margin-top:5px;">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left">
                                                                <a id="PayYear" runat="server">
                                                                    <img src="../../images/homeimages/button_buy-now.png" alt="Buy Now" title="Buy Now" /></a>
                                                            </td>
                                                            <td style="padding-left: 10px;">
                                                                <a id="PayMonth" runat="server" class="orangeLinksml" title="See monthly plan">
                                                                <asp:Label ID="lblSeemonthlyPlan" runat="server" Text="" 
                                                                    meta:resourcekey="lblSeemonthlyPlanResource1"></asp:Label></a>
                                                            </td>
                                                        </tr>
                                                     
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="buyPanelBottom">
                                            </div>
                                            <!-- //buy now pannel -->
                                        </div>
                                    </td>
                                </tr>
                            </table>
