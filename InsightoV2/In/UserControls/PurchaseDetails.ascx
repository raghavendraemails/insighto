﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PurchaseDetails.ascx.cs"
    Inherits="In_UserControls_PurchaseDetails" %>
<div class="buyNowPanel" style="float: left; height: auto; margin-top: -4px; margin-bottom: 10px;">
    <div class="buyNowHeader">
        <div>
           <asp:Label ID="lblPurchaseDetails" runat="server" Text="Purchase Details" 
                meta:resourcekey="lblPurchaseDetailsResource1"></asp:Label></div>
    </div>
    <div class="buyNowContentPanel" style="height: auto; padding-bottom: 5px;padding-left:0px !important;padding-right:0px !important;">
        <div class="text" style="padding:0px !important;">
            <table border="0" width="100%">
                <tr>
                    <td width="35%" class="smallFont">
                     <asp:Label ID="lblMemberShipType" runat="server" Text=" Membership Type" 
                            meta:resourcekey="lblMemberShipTypeResource1"></asp:Label>
                    </td>
                    <td width="3%" align="center">
                        -
                    </td>
                    <td class="smallFont" align="right">
                        <asp:Label ID="lblMemberType" runat="server" meta:resourcekey="lblMemberTypeResource1" Visible="false"></asp:Label>
                             <asp:Label ID="lblPreMemberType" runat="server" meta:resourcekey="lblPreMemberTypeResource1" Visible="false"></asp:Label>
                            </td>
                            
                     <%--<td width="50%" class="smallFont" align="right">
                            <asp:Label ID="lblPreMemberType" runat="server" Text="Premium" 
                            meta:resourcekey="lblPreMemberTypeResource1"></asp:Label>
                    </td>--%>
                </tr>
                <tr>
                    <td class="smallFont">
                     <asp:Label ID="lblSubscriptionTypeText" Text="Subscription Type" runat="server" 
                            meta:resourcekey="lblSubscriptionTypeTextResource1"></asp:Label>
                    </td>
                    <td align="center">
                        -
                    </td>
                    <td class="smallFont" align="right">
                        <asp:Label ID="lblSubscriptiontype" runat="server" 
                            meta:resourcekey="lblSubscriptiontypeResource1"></asp:Label>
                    </td>
                </tr>             

                <tr>
                    <td class="smallFont">
                      <asp:Label ID="lblPricetext" runat="server" Text="Price" 
                            meta:resourcekey="lblPricetextResource1"></asp:Label>
                    </td>
                    <td align="center">
                        -
                    </td>
                    <td class="smallFont" align="right">
                        <asp:Label ID="lblPrice" runat="server" meta:resourcekey="lblPriceResource1"></asp:Label>
                    </td>
                </tr>
                 <tr>
                    <td class="smallFont">
                     <asp:Label ID="lblexistprodtext" Text="Existing Prod. Price" runat="server" 
                            meta:resourcekey="lblexistproduct" Visible="false"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="eprodash" runat="server" Text="-" Visible="false"></asp:Label>
                    </td>
                    <td class="smallFont" align="right">
                        <asp:Label ID="lblexistprod" runat="server" 
                            meta:resourcekey="lblexistproductval" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr id="trServiceTax" runat="server">
                    <td class="smallFont">
                      <asp:Label ID="lblServiceTaxText" runat="server"   Text="Service Tax (10.3%)" 
                            meta:resourcekey="lblServiceTaxTextResource1"></asp:Label>
                    </td>
                    <td align="center">
                      <asp:Label ID="servicedash" runat="server" Text="-"></asp:Label>
                    </td>
                    <td class="smallFont" align="right">
                        <asp:Label ID="lblServiceTax" runat="server" 
                            meta:resourcekey="lblServiceTaxResource1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="smallFont">
                         <b><asp:Label ID="lblTotalPrice" runat ="server" Text="Total Price" 
                             meta:resourcekey="lblTotalPriceResource1"></asp:Label></b>
                    </td>
                    <td align="center">
                        -
                    </td>
                    <td class="smallFont" align="right">
                        <b>
                            <asp:Label ID="lblTotal" runat="server" 
                            meta:resourcekey="lblTotalResource1"></asp:Label></b>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="buyPanelBottom">
    </div>
</div>
<div class="clear">
</div>
