﻿using System;
using Insighto.Business.Helpers;
using App_Code;

public partial class In_UserControls_RightAdvertizement : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //string NavurlYear = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", "LicenseType=PRO_YEARLY");
        string NavurlYear = "http://www.insighto.com/pricing/";
        PayYear.HRef = NavurlYear;
        string NavurlMonth = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", "LicenseType=PRO_MONTHLY");
        PayMonth.HRef = NavurlMonth;
        lblINR.Text = String.Format(GetLocalResourceObject("lblINRResource1.Text").ToString(), Utilities.FormatCurrency(4500.00, false, true));
    }
}
