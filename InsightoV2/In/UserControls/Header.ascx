﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="In_UserControls_Header" %>
<%--<%@ Register Src="~/UserControls/TopLinks.ascx" TagPrefix="uc" TagName="TopLinks" %>--%>
<%@ Register Src="~/UserControls/LoginControl.ascx" TagPrefix="uc" TagName="Login" %>
<div class="header">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <!-- header -->
        <tr>
            <td colspan="5">
                <%-- <uc:TopLinks ID="ucTopLinks" runat="server" />--%>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <div class="logoHeader">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="10">
                            </td>
                            <td align="left">
                                <a href="../../Home.aspx" target="_blank" runat="server" id="lnklogo">
                                    <img src="../../App_Themes/Classic/Images/Insighto_logo.png" border="0" alt="" runat="server" id="imglogo"/></a>
                            </td>
                            <td align="right">
                                <%--   <uc:Login ID="Login1" runat="server" />--%>
                            </td>
                            <td width="10">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="5" id="tdmainmenu" runat="server">
                <div id="topMainMenu" >
                    <div id="topMainLeftLinks" style="padding-left: 20px;">
                        <ul class="nav">
                            <li id="ahome"><a href="../../Home.aspx">Home</a></li>
                            <li id="awhyinsighto"><a id="lnkWhyInsighto" runat="server">Why Insighto</a></li>
                            <li id="atakeatour"><a id="lnkTakeatour" href="~/In/TakeaTour.aspx" runat="server">Take
                                a Tour</a></li>
                            <li id="apricing"><a id="lnkAboutUs" href="http://insighto.com/pricing.aspx" runat="server">Plans &
                                Pricing</a></li>
                                <li id="ablog"><a id="lnkBlog" href="http://insighto.com/blog/" runat="server">Blog</a></li>
                        </ul>
                    </div>
                    <div class="topMennuRightPanel" style="position: relative;">
                        <ul class="loginUL">
                            <li><a href="#" class="signin">LOGIN</a></li>
                        </ul>
                        <div id="signin_menu" runat="server" class="signin_menu">
                        <div style="position:absolute;right:5px;top:5px;cursor:pointer;color:#C424A5; font-weight:bold;font-size:14px;" onclick ="$('.signin_menu').hide();" title="Close">X</div>
                            <div style="width: 180px; margin: auto;">
                                <table align="center" width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblInvaildAlert" class="lblRequired" runat="server" Visible="False"
                                                Text="In valid User Name or Password" meta:resourcekey="lblInvaildAlertResource1"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="loginFormLabel">
                                            Username
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtEmailAdd" runat="server" class="homeTextbox userName"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" SetFocusOnError="true" Display="Dynamic"
                                                ErrorMessage="Please enter email address." ControlToValidate="txtEmailAdd" CssClass="lblRequired"
                                                ValidationGroup="loginVal"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator SetFocusOnError="true" ID="RexEmail" CssClass="lblRequired"
                                                ControlToValidate="txtEmailAdd" runat="server" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ErrorMessage="Please enter valid email address." ValidationGroup="loginVal"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="loginFormLabel">
                                            Password
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox TextMode="Password" ID="txtPassword" runat="server" class="homeTextbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" SetFocusOnError="true"
                                                Display="Dynamic" ErrorMessage="Please enter password." ControlToValidate="txtPassword"
                                                CssClass="lblRequired" ValidationGroup="loginVal"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%--<asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="dynamicButtonSmall" OnClick="btnLogin_Click" />--%>
                                            <asp:Button ID="ibtnLogin" OnClick="ibtnLogin_Click" CssClass="dynamicButtonSmall"
                                                runat="server" Text="Login" ToolTip="Login" meta:resourcekey="ibtnLoginResource1"
                                                ValidationGroup="loginVal" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="margin-top: 10px;">
                                                <a href="/ForgotPassword.aspx" id="hlkForgotPassword" rel='framebox' w='700' h='300'>
                                                    Forgot Password?</a></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </td>
        </tr>
        <!-- //header -->
    </table>
</div>
<script language="javascript" type="text/javascript">
    var url = location.href;
    var url_parts = url.split('?');
    var main_url = url_parts[0];
    var url_split = main_url.split('/');

    if (url_split[4] == 'whyinsighto.aspx') {
        document.getElementById('awhyinsighto').className = 'activelink';
    }
    if (url_split[4] == 'Pricing.aspx')
    { document.getElementById('apricing').className = 'activelink'; }
    if (url_split[4] == 'Home.aspx')
    { document.getElementById('ahome').className = 'activelink'; }
    if (url_split[4] == 'TakeaTour.aspx')
    { document.getElementById('atakeatour').className = 'activelink'; } 
     
    
</script>
<script type="text/javascript">
    $(document).ready(function () {

        $(".signin").click(function (e) {
            e.preventDefault();
            $("div.signin_menu").toggle();
            $(".signin").toggleClass("menu-open");
            $('.userName').focus();
        });

        $("div.signin_menu").mouseup(function () {
            return false
        });

    });
</script>
