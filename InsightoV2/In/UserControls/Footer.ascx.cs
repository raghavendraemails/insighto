﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;

public partial class In_UserControls_Footer : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lnkFooterAboutUs.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetAboutUsUrl(), "Page=AboutUs");
        lnkFooterAntispam.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetAntiSpamUrl(), "Page=AntiSpam");
        lnkFooterPrivacyPolicy.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetPrivacyPolicyUrl(), "Page=PrivacyPolicy");
        lnkFooterWhyInsighto.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetWhyInsightoUrl(), "Page=WhyInsighto");
        if (Request.RawUrl.ToLower().Contains("polls/"))
        {
            lblWhyInsihgto.Visible = false;
        }
    }
}
