﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using App_Code;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Resources;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections;

public partial class In_UserControls_HeaderOld : System.Web.UI.UserControl
{
    Hashtable typeHt = new Hashtable();
    private string vpartner = "";
    private string vpartnername = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string currurl = "";
        string querystring = "";

        string utmsource = "";
        string utmterm = "";

        if (!IsPostBack)
        {
            currurl = HttpContext.Current.Request.RawUrl;
            querystring = HttpContext.Current.Request.QueryString.ToString();
           
           // if (currurl.Contains("Register.aspx"))
           //{
           //    topMainMenu.Visible = false;
           //}
           //else
           //{
           //    topMainMenu.Visible = true;
           //}


           if ((querystring != "") && (!querystring.Contains("key")))
            {
                SurveyCore surcore = new SurveyCore();

                    utmsource = Request.QueryString["utm_source"].ToString();
              
                DataSet dspartnerinfo = surcore.getPartnerInfo(utmsource);

                if ((Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) == 0) || (Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) < 0))
                {
                    tdmainmenu.Visible = true;
                }
                else
                {
                    if (dspartnerinfo.Tables[0].Rows[0][1].ToString().ToUpper() != "INSIGHTO")
                    {
                        tdmainmenu.Visible = false;
                    }
                    else
                    {
                        tdmainmenu.Visible = true;
                    }
                   
                }
            }
            else
            {

                if (querystring.Contains("key"))
                {
                    if (Request.QueryString["key"] != null)
                    {
                        typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                        if (typeHt.Contains("FromPartner"))
                            vpartner = typeHt["FromPartner"].ToString();
                        if (typeHt.Contains("utm_source"))
                            vpartner = "Yes";
                        if (typeHt.Contains("partner"))
                            vpartnername = typeHt["partner"].ToString();
                         if (typeHt.Contains("utm_source"))
                             vpartnername = typeHt["utm_source"].ToString();
                    }
                }
                if (vpartner == "Yes")
                {
                    if (vpartnername.ToUpper() != "INSIGHTO")
                    {
                        tdmainmenu.Visible = false;
                    }
                    else
                    {
                        tdmainmenu.Visible = true;
                    }
                }
                else
                {
                    tdmainmenu.Visible = true;
                }
            }
           // lnkAboutUs.HRef = EncryptHelper.EncryptQuerystring("", "Page=AboutUs");
            lnkWhyInsighto.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetWhyInsightoUrl(), "Page=WhyInsighto");
            lblInvaildAlert.Text = string.Empty;
            lblInvaildAlert.Visible = false;
        }
    }
    protected void ibtnLogin_Click(object sender, EventArgs e)
    {
        if (!IsPostBack)
            return;

        string alrt = "Please enter";
        this.lblInvaildAlert.Visible = true;
        bool flag = false;
        if (string.IsNullOrEmpty(txtEmailAdd.Text.Trim()) || txtEmailAdd.Text.ToLower() == "email address")
        {
            alrt += " email address.";
            lblInvaildAlert.Text = alrt;
        }
        else
        {
            Regex reLenient = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            bool isEmail = reLenient.IsMatch(txtEmailAdd.Text);
            if (!isEmail)
            {
                alrt += " valid email address.";
                lblInvaildAlert.Text = alrt;
                signin_menu.Style.Add("display", "block");
            }
            else if (Convert.ToString(txtPassword.Text).Length == 0)
            {
                alrt += " password.";
                lblInvaildAlert.Text = alrt;
                signin_menu.Style.Add("display", "block");
            }
            flag = true;
        }

        if (flag && Convert.ToString(txtEmailAdd.Text).Length > 0 && Convert.ToString(txtPassword.Text).Length > 0)
        {
            // To check authenticated user       
            var userService = ServiceFactory.GetService<UsersService>();
            var user = userService.getAuthenticatedUser(txtEmailAdd.Text, txtPassword.Text);
            if (user == null)
            {
                lblInvaildAlert.Text = "Invalid Email Address or Password.";
                lblInvaildAlert.Visible = true;
                signin_menu.Style.Add("display", "block");
                return;
            }
            else if (Utilities.ToEnum<SurveyStatus>(user.STATUS) == SurveyStatus.InActive || ValidationHelper.GetInteger(user.DELETED, 0) == 1)
            {
                lblInvaildAlert.Text = "Your Account is de-activated.";
                lblInvaildAlert.Visible = true;
                signin_menu.Style.Add("display", "block");
                return;
            }
            else if (user.ACTIVATION_FLAG == 0 || user.ACTIVATION_FLAG == null)
            {
                lblInvaildAlert.Text = " Your Account is not activated.";
                lblInvaildAlert.Visible = true;
                signin_menu.Style.Add("display", "block");
                return;

            }
            else
            {
                userService.ProcessUserLogin(user);
                if (user.TIME_ZONE == null) //Timezone is null for first user login. hence redirecting to welcome page
                {
                    //create folder with userid 
                    //CreateUserFolders(user); this function is moved to register free user .. as every user must have this.
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                }
                else if (user.RESET == 1) // Password reset value is 1 for first login. hence redirectiong to chanepassword page
                {
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserChangePasswordPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                }
                else // on successful login user is redirected to Myaccounts page
                {
                    Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                }
            }

        }
        else
        {


        }
    }
}
