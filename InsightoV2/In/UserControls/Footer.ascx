﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="In_UserControls_Footer" %>
<div class="clear">
</div>
<!-- footer -->
<div class="footer">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td class="copyRights" nowrap>
              <asp:Label ID="lblCopyRight" runat="server"  
                    Text="Copyright &copy; 2014 Knowience Insights" 
                    meta:resourcekey="lblCopyRightResource1"></asp:Label>
            </td>
            <td align="right" style="padding-top:18px;" id="footerlinks">
                <a href="../../Home.aspx" class="botnav" title="Free Online Survey & Questionnaire Software"><asp:Label ID="lblHome" runat="server" 
                    Text="Home" meta:resourcekey="lblHomeResource1"></asp:Label></a> <a id="lnkFooterWhyInsighto" runat="server" class="botnav" title="Creating Online Web Survey">
                  <asp:Label ID="lblWhyInsihgto" runat="server" Text="Why Insighto" 
                    meta:resourcekey="lblWhyInsihgtoResource1"></asp:Label></a> <a href="/pricing.aspx" class="botnav" title="Plans and Pricing">
                <asp:Label ID="lblPlans" runat="server"  Text="Plans & Pricing" 
                    meta:resourcekey="lblPlansResource1"></asp:Label></a> <a id="lnkFooterAboutUs" runat="server" title="About Us"
                        class="botnav"><asp:Label ID="lblAboutUs" runat="server" 
                    Text="About Us" meta:resourcekey="lblAboutUsResource1"></asp:Label></a> <a href="../In/ContactUs.aspx" class="botnav" title="Contact Us">
                <asp:Label ID="lblContactUs" runat="server" Text="Contact Us" 
                    meta:resourcekey="lblContactUsResource1"></asp:Label></a>
                <a id="lnkFooterAntispam" runat="server" class="botnav" title="Anti Spam Policy">
                <asp:Label ID="lblAntiSpamPolicy" runat="server" Text="Anti spam policy" 
                    meta:resourcekey="lblAntiSpamPolicyResource1"></asp:Label></a><a id="lnkFooterPrivacyPolicy" runat="server" title="Privacy Policy"
                    class="botnav"><asp:Label ID="lblPrivacyPolicy" runat="server" 
                    Text="Privacy policy" meta:resourcekey="lblPrivacyPolicyResource1"></asp:Label></a>
            </td>
        </tr>
    </table>
</div>
