﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AuthenticatedBy.ascx.cs"
    Inherits="In_UserControls_AuthenticatedBy" %>
<div class="buyNowPanel" style="float: left; height: auto; margin-bottom: 10px;">
    <div class="buyNowHeader">
        <div>
            <asp:Label ID="lblAuthenticated" runat="server" Text="Authenticated by" 
                meta:resourcekey="lblAuthenticatedResource1"></asp:Label>
            </div>
    </div>
    <div class="buyNowContentPanel" style="height: auto; padding-bottom: 5px;">
        <p align="center">
          <%--  <a href="#">--%>
           <asp:Image ID="Image1" runat="server" AlternateText="Secure Logo" 
                ImageUrl="~/App_Themes/Classic/Images/fs_secure-ordering.png" 
                meta:resourcekey="imgSecureResource1" style="float:none;"  />
                <asp:Image ID="imgSecure" runat="server" AlternateText="Secure Logo" 
                ImageUrl="~/App_Themes/Classic/Images/Indian.gif" 
                meta:resourcekey="imgSecureResource1" style="float:none;" Visible="false" />

                <asp:Image ID="imgIndian1" runat="server" AlternateText="Secure Logo" 
                ImageUrl="~/App_Themes/Classic/Images/Indian1.png" 
                meta:resourcekey="imgSecureResource2" style="float:none;" Visible="false" />

                 <asp:Image ID="imgforeign" runat="server" AlternateText="Secure Logo" 
                ImageUrl="~/App_Themes/Classic/Images/nortonseal.png"
                meta:resourcekey="imgSecureResource3" style="float:none;" Visible="false" />
        <%--    </a>--%>
        </p>
         <div style="clear:both;"></div>
    </div>
   
    <div class="buyPanelBottom">
    </div>
</div>
<div class="clear">
</div>
