﻿<%@ Page Title="Manage Responses of Online Surveys - Analyze" Language="C#" MasterPageFile="~/Home.master"
    AutoEventWireup="true" CodeFile="TakeATour_Analyze.aspx.cs" Inherits="In_TakeATour_Analyze" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <meta name="description" content="Gather, summarize and analyze individual responses of your online web survey." />
    <meta name="keywords" content="online survey, online web survey, web survey" />
    <div class="takeATourContentPanel">
        <!-- take a tour main content panel -->
        <div class="faqTopMenuPanel">
            <!-- tab links -->
            <ul id="faqNav">
                <li>
                    <asp:HyperLink ID="lnkCreate" runat="server" ToolTip="Creating Online Web Surveys" NavigateUrl="~/In/TakeaTour.aspx"
                        Text="Create"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkDesign" runat="server" NavigateUrl="~/In/TakeATour_Design.aspx"
                        ToolTip="Logo for Branding – Design Surveys" Text="Design"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkLaunch" runat="server" NavigateUrl="~/In/TakeATour_Launch.aspx"
                        ToolTip="Launch Online Surveys" Text="Launch"></asp:HyperLink></li>
                <li class="activelink">
                    <asp:HyperLink ID="lnkAnalyze" runat="server" NavigateUrl="~/In/TakeATour_Analyze.aspx"
                        ToolTip="Manage Responses and Analyze" Text="Analyze"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkManage" runat="server" NavigateUrl="~/In/TakeATour_Manage.aspx"
                        ToolTip="Manage Email Lists" Text="Manage"></asp:HyperLink></li>
            </ul>
            <div class="clear">
            </div>
            <!-- //tab links -->
        </div>
        <div class="clear">
        </div>
        <div class="takeATourTextPanel">
            <!-- text panel -->
            <div class="takeATourButtonPanel">
                <!-- prev next bubtton -->
                <asp:HyperLink ID="lnkPrevius" runat="server" NavigateUrl="javascript:void(0)" CssClass="active"
                    ToolTip="Previous" Text="Prev"></asp:HyperLink>
                <asp:HyperLink ID="lnkNext" runat="server" NavigateUrl="javascript:void(0)" CssClass="active"
                    ToolTip="Next" Text="Next"></asp:HyperLink>
                <!-- //prev next bubtton -->
            </div>
            <%--<div style="display: block" id="Create_1">
                <!-- gettin started -->
                <h3>
                    View the results as soon as your survey is launched.</h3>
                <p style="padding-top:10px;">
                    <img src="../images/homeimages/analyse-img-01.jpg" alt='' /></p>
                    <div style="clear:both;"></div> <br />
                <ul>
                    <li>You can view real-time results of all launched survey at one place</li>
                    <li>Get to know the number of visits, completes and partials too</li>
                </ul>
                <!-- //gettin started -->
            </div>--%>
            <div id="Create_1" style="display: block">
                <!-- question types -->
                <h3>
                    View and analyse survey responses in real time</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/analyse-img-02.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>Survey responses are analyzed in real time i.e., your report keeps getting refreshed
                        as new responses come</li>
                    <li>View only the data or data + charts or open ended responses or individual responses</li>
                </ul>
                <!-- //question types -->
            </div>
            <div style="display: none" id="Create_2">
                <!-- answer options -->
                <h3>
                    Professionally designed graph for each close-ended question</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/analyse-img-03.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>Present your data with the help of professional graphs</li>
                    <li>Customize graph type with just a couple of clicks</li>
                    <li>Add comments and notations for each graph</li>
                </ul>
                <!-- answer options -->
            </div>
            <div style="display: none" id="Create_3">
                <!-- questionnair -->
                <h3>
                    Share your data – export with one click</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/analyse-img-04.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>Export your survey data in formatted PPT, MS Excel, MS Word and PDF.</li>
                    <li>Export raw data for additional offline analysis</li>
                    <li>Open text responses included with export of raw data and export to Excel</li>
                </ul>
                <!-- questionnair -->
            </div>
            <div style="display: none" id="Create_4">
                <!-- preview survey -->
                <h3>
                    Slice and dice your data any way you like
                </h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/analyse-img-05.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>                       
                <ul>
                    <li>Uncover hidden trends of the data with cross tab reports</li>
                    <li>Generate unique insights by plotting relationship between responses of two different
                        questions<</li>
                </ul>
                <!-- preview survey -->
            </div>
            <div style="display: none" id="Create_5">
                <!-- review survey -->
                <h3>
                    View and export open ended responses
                </h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/analyse-img-06.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>View open text comments made by the respondents and export them to Excel</li>
                    <li>Analyze and get insights from the verbatim comments of your respondents</li>
                </ul>
                <!-- review survey -->
            </div>
            <!-- //text panel -->
        </div>
        <div class="takeATourSubMenuPanel">
            <!-- sub menu -->
            <ul class="takeATourMenu">
                <%-- <li><a id="lnkCreate_1" class="active" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(1);">
                    Manage Responses</a></li>--%>
                <li><a id="lnkCreate_1" href="javascript:void(0)" class="active" onclick="javascript:ShowSelectedScreen(1);">
                    Realtime Reports</a></li>
                <li><a id="lnkCreate_2" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(2);">
                    Charts and Graphs</a></li>
                <li><a id="lnkCreate_3" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(3);">
                    Export Reports</a></li>
                <li><a id="lnkCreate_4" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(4);">
                    Generate Cross Tabs</a></li>
                <li><a id="lnkCreate_5" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(5);">
                    Analyze Text</a></li>
            </ul>
            <!-- //sub menu -->
        </div>
         <input type="hidden" class="hdnPanelIndex" id="hdnPanelIndex" value="1" />
        <!-- // take a tour main content panel -->
    </div>
   
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('.hdnPanelIndex').val() == '1') {
                $('#<%=lnkPrevius.ClientID %>').hide();
            }
        });
        $('#<%=lnkNext.ClientID %>').click(function () {
            var index = $('.hdnPanelIndex').val();
            $('#Create_' + index).hide();
            $('#lnkCreate_' + index).removeClass('active');
            index++;
            $('#Create_' + index).show();
            $('.hdnPanelIndex').val(index);
            if ($('.hdnPanelIndex').val() > 1) {
                $('#<%=lnkPrevius.ClientID %>').show();
                $('#lnkCreate_' + index).addClass('active');
                if (index == 5) {
                    $('#<%=lnkNext.ClientID %>').hide();
                }
            }
        });
        $('#<%=lnkPrevius.ClientID %>').click(function () {
            var index = $('.hdnPanelIndex').val();
            $('#Create_' + index).hide();
            $('#lnkCreate_' + index).removeClass('active');
            index--;
            $('#Create_' + index).show();
            $('.hdnPanelIndex').val(index);
            if ($('.hdnPanelIndex').val() < 6) {
                $('#<%=lnkNext.ClientID %>').show();
                $('#lnkCreate_' + index).addClass('active');
                if (index == 1) {
                    $('#<%=lnkPrevius.ClientID %>').hide();
                }
            }
        });
        function ShowSelectedScreen(index) {
            if (index == 1) {
                $('#<%=lnkPrevius.ClientID %>').hide();
                $('#<%=lnkNext.ClientID %>').show();
            }
            else if (index == 5) {
                $('#<%=lnkNext.ClientID %>').hide();
                $('#<%=lnkPrevius.ClientID %>').show();
            }
            else {
                $('#<%=lnkNext.ClientID %>').show();
                $('#<%=lnkPrevius.ClientID %>').show();
            }
            var prevIndex = $('.hdnPanelIndex').val();
            $('#Create_' + prevIndex).hide();
            $('#lnkCreate_' + prevIndex).removeClass('active');
            $('#Create_' + index).show();
            $('#lnkCreate_' + index).addClass('active');
            $('.hdnPanelIndex').val(index);

        }
    </script>
</asp:Content>
