﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using CovalenseUtilities.Services;
using System.Globalization;
using System.Configuration;
using System.Data;
using App_Code;

namespace Insighto.Pages
{
    public partial class Invoice : BasePage
    {
        Hashtable ht = new Hashtable();
        string strsaasypayementorderid = "";
        SurveyCore surcore = new SurveyCore();
        protected void Page_Load(object sender, EventArgs e)
        {
            int orderId = 0, userId = 0,historyId=0;
            int printConf = 0;
            string knowAccountno = "", knowFavouringAddress = "";
            string paymentInstructionsText = "";
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0)
                {
                    if (ht.Contains("OrderId"))
                        orderId = Convert.ToInt32(ht["OrderId"]);
                    if (ht.Contains("UserId"))
                        userId = Convert.ToInt32(ht["UserId"]);
                    if (ht.Contains("HistoryId"))
                        historyId = Convert.ToInt32(ht["HistoryId"]);
                    if (ht.Contains("Print"))
                    {
                        printConf = Convert.ToInt32(ht["Print"]);
                        divClose.Visible = false;
                    }
                }
            }
            if (orderId > 0)
            {
                lnkUrl.HRef = PathHelper.GetUserLoginPageURL();
                lnkContactus.HRef =PathHelper.GetUserContactusURL();
                var userDetails = ServiceFactory.GetService<UsersService>().GetUserDetails(userId).FirstOrDefault();
                var orderDetails = ServiceFactory.GetService<OrderInfoService>().GetOrderInfo(orderId, userId).SingleOrDefault();

                var licenseDetails = ServiceFactory<UsersService>.Instance.GetLicenseDetailsDateHistoryId(historyId).FirstOrDefault();
                if (licenseDetails == null) {
                    licenseDetails = ServiceFactory<UsersService>.Instance.GetLicenseDetailsDateByOrderID(orderId.ToString()).FirstOrDefault();
                }
              

                string order = orderDetails.ORDERID.Substring(4, 1);
                
                lblAmountCurrency.Text = "(" + orderDetails.CurrencyType + ")";
                lblRateCurrency.Text = "(" + orderDetails.CurrencyType + ")";

                string licenseType = "";
                if (order.ToUpper() == "Q")
                {
                    licenseType = "PRO_QUARTERLY";
                }
                else if (order.ToUpper() == "Y")
                {
                    licenseType = "PRO_YEARLY";
                }
                else if (order.ToUpper() == "M")
                {
                    licenseType = "PRO_MONTHLY";
                }
                else
                {
                    licenseType = "PREMIUM_YEARLY";
                }

                DataSet dspps = surcore.getCreditExpirydate(orderId);

                if (dspps.Tables[0].Rows.Count > 0)
                {
                    if (dspps.Tables[0].Rows[0][3].ToString() == "1")
                    {
                        licenseType = "PRO_SINGLE";
                    }
                    else if (dspps.Tables[0].Rows[0][3].ToString() == "2")
                    {
                        licenseType = "PREMIUM_SINGLE";
                    }

                }

                string configkeyName = "ITEM_" + licenseType;
                string configkeyDesc = licenseType + "_DESCRIPTION";
                List<string> listConfig = new List<string>();
                 
                listConfig.Add(configkeyName);
                listConfig.Add(configkeyDesc);
                listConfig.Add(Constants.CURRENTACCOUNTNO);
                listConfig.Add(Constants.KNOWIENCEADDRESS);
                listConfig.Add(Constants.KNOWIENCEFAVOUROFADDRESS);
                var configService = ServiceFactory.GetService<ConfigService>();
                var configvalue = configService.GetConfigurationValues(listConfig);
                foreach (var cv in configvalue)
                {
                    if (cv.CONFIG_KEY.TrimEnd() == Constants.KNOWIENCEADDRESS.TrimEnd())
                    {
                        knowience_address.InnerHtml = cv.CONFIG_VALUE;
                    }
                    if (cv.CONFIG_KEY.TrimEnd() == Constants.KNOWIENCEFAVOUROFADDRESS.TrimEnd())
                    {
                        knowFavouringAddress = cv.CONFIG_VALUE;
                    }
                    if (cv.CONFIG_KEY.TrimEnd() == Constants.CURRENTACCOUNTNO.TrimEnd())
                    {
                        knowAccountno = cv.CONFIG_VALUE;
                    }
                    if (cv.CONFIG_KEY.TrimEnd() == configkeyName.TrimEnd())
                    {
                        item_Name.InnerHtml = cv.CONFIG_VALUE;
                    }
                    if (cv.CONFIG_KEY.TrimEnd() == configkeyDesc.TrimEnd())
                    {
                        item_Description.InnerHtml = cv.CONFIG_VALUE;
                    }
                }
                if (userDetails.USER_TYPE == 1)
                {
                    billaddress.InnerHtml = "<strong> " + userDetails.FIRST_NAME + " " + userDetails.LAST_NAME + " </strong><br />" + userDetails.ADDRESS_LINE1 + "<br/> " + userDetails.ADDRESS_LINE2;
                }
                else if (userDetails.USER_TYPE == 2)
                {
                    billaddress.InnerHtml = String.Format(Utilities.ResourceMessage("Invoice_Bill_Address"), userDetails.COMPANY, userDetails.CITY + ", " + userDetails.COUNTRY, userDetails.PHONE, userDetails.FIRST_NAME + " " + userDetails.LAST_NAME); //"<strong>" + userDetails.COMPANY + "</strong>,<br/>" + "<strong>" + userDetails.CITY + ", " + userDetails.COUNTRY + ".</strong><br/><strong> Phone:" + userDetails.PHONE + "</strong><br/><br/>Kind Attention :<strong> " + userDetails.FIRST_NAME + " " + userDetails.LAST_NAME + " </strong>";
                }
                //if (licenseDetails.LICENSE_STARTDATE != null && licenseDetails.LICENSE_ENDDATE != null)
                if (orderDetails.PAID_DATE != null)
                {
                    int paymentorderid = Convert.ToInt32(orderDetails.PK_ORDERID);
                    DataSet dssaasypaymentorder = surcore.getSaasypaymentorderid(paymentorderid);

                    if (dssaasypaymentorder.Tables[0].Rows.Count > 0)
                    {
                        strsaasypayementorderid = dssaasypaymentorder.Tables[0].Rows[0][0].ToString();
                    }
                    DateTime PaidDate;
                    DateTime LicenseExpiryDate;
                    
                    if (licenseType.Contains("PREMIUM") || licenseType.Contains("PRO"))
                    {
                        PaidDate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(orderDetails.PAID_DATE), userDetails.STANDARD_BIAS, true);
                        LicenseExpiryDate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(orderDetails.PAID_DATE).AddYears(1), userDetails.STANDARD_BIAS, true);
                        invoice_info.InnerHtml = String.Format(Utilities.ResourceMessage("Invoice_Payment_Info"), orderDetails.ORDERID, strsaasypayementorderid, Convert.ToDateTime(userDetails.CREATED_ON).ToString("dd-MMM-yyyy"), PaidDate.ToString("dd-MMM-yyyy"), LicenseExpiryDate.ToString("dd-MMM-yyyy"));
                    }
                    else
                    {
                        PaidDate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(orderDetails.PAID_DATE), userDetails.STANDARD_BIAS, true);
                        LicenseExpiryDate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(licenseDetails.LICENSE_ENDDATE), userDetails.STANDARD_BIAS, true);
                        invoice_info.InnerHtml = String.Format(Utilities.ResourceMessage("Invoice_Payment_Info"), orderDetails.ORDERID, strsaasypayementorderid, Convert.ToDateTime(userDetails.CREATED_ON).ToString("dd-MMM-yyyy"), PaidDate.ToString("dd-MMM-yyyy"), LicenseExpiryDate.ToString("dd-MMM-yyyy"));
                        //invoice_info.InnerHtml = String.Format(Utilities.ResourceMessage("Invoice_Payment_Info"), orderDetails.ORDERID, strsaasypayementorderid, Convert.ToDateTime(userDetails.CREATED_ON).ToString("dd-MMM-yyyy"), Convert.ToDateTime(licenseDetails.LICENSE_STARTDATE).ToString("dd-MMM-yyyy"), Convert.ToDateTime(licenseDetails.LICENSE_ENDDATE).ToString("dd-MMM-yyyy"));
                    }
                }
                else
                {
                    //invoice_info.InnerHtml = String.Format(Utilities.ResourceMessage("Invoice_Payment_Info"), orderDetails.ORDERID, Convert.ToDateTime(userDetails.CREATED_ON).ToString("dd-MMM-yyyy"), "NA", "NA");
                    invoice_info.InnerHtml = String.Format(Utilities.ResourceMessage("Invoice_Payment_Info"), orderDetails.ORDERID, strsaasypayementorderid, Convert.ToDateTime(userDetails.CREATED_ON).ToString("dd-MMM-yyyy"), "NA", "NA");
                }
                item_Qty.InnerHtml = "1";
                item_Rate.InnerHtml = Math.Round(orderDetails.PRODUCT_PRICE, 2).ToString("N", CultureInfo.InvariantCulture);
                item_Amt.InnerHtml = Math.Round(orderDetails.PRODUCT_PRICE, 2).ToString("N", CultureInfo.InvariantCulture);
                subtotal.InnerHtml = Math.Round(orderDetails.PRODUCT_PRICE, 2).ToString("N", CultureInfo.InvariantCulture);
                service_tax.InnerHtml = String.Format(Utilities.ResourceMessage("Invoice_Payment_ServiceTax"), orderDetails.TAX_PERCENT.ToString()); //"Service Tax (" + orderDetails.TAX_PERCENT.ToString() + "%)";
                taxt_amt.InnerHtml = Math.Round(orderDetails.TAX_PRICE, 2).ToString("N", CultureInfo.InvariantCulture);
                //total_amt.InnerHtml = "<strong>" + String.Format("{0} {1} ", new RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name).ISOCurrencySymbol, new RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name).CurrencySymbol) + Math.Round(orderDetails.TOTAL_PRICE, 2).ToString("N") + "</strong>";
               // total_amt.InnerHtml = "<strong>" + "INR" + Math.Round(orderDetails.TOTAL_PRICE, 2).ToString("N") + "</strong>";
                total_amt.InnerHtml ="<strong>"+ Utilities.FormatCurrency(orderDetails.TOTAL_PRICE, false, true)+"</strong>";
                if (userDetails.USER_TYPE == 2)
                {
                    paymentInstructionsText = Utilities.ResourceMessage("Invoice_Payment_Instructions");//"<p><strong>Payment instructions:</strong> </p>";
                    paymentInstructionsText += String.Format(Utilities.ResourceMessage("Invoice_Instruction_ChequeDD"), knowFavouringAddress); //"<p>1.Draw the cheque/DD in favour of &ldquo;" + knowFavouringAddress + " &rdquo; for the amount mentioned above.</p>";
                    paymentInstructionsText += Utilities.ResourceMessage("Invoice_Instruction_Email");    //"  <p>2.Send a mail to <a href='mailto:accounts@insighto.com'>accounts@insighto.com</a> with details of payment.</p>";
                    paymentInstructionsText += Utilities.ResourceMessage("Invoice_Instruction_Options"); //" <p>3.Options for making payment:</p>  <p>Option 1: Mail the Cheque/Demand Draft</p>";
                    paymentInstructionsText += Utilities.ResourceMessage("Invoice_Instruction_AttachChequeDD"); //" <p>Attach the cheque/DD to a copy of invoice generated at the time of registration and send it to the address given at the top right hand corner of the invoice.</p>";
                    paymentInstructionsText += Utilities.ResourceMessage("Invoice_Instruction_DepositHSBC");  //"<p>Option 2: Deposit Cheque/Demand Draft in HSBC Bank</p>";
                    paymentInstructionsText += String.Format(Utilities.ResourceMessage("Invoice_Instruction_DepositAccountNo"),knowFavouringAddress,knowAccountno); //" <p>Deposit the cheque/DD at any of the branches of HSBC Bank favoring " + knowFavouringAddress + " Current Account No." + knowAccountno + "</p>";
                    paymentInstructionsText += Utilities.ResourceMessage("Invoice_Instruction_Ignore"); //" <p style='color: #999999;font-style: italic;'>If you have already made the payment, please ignore the payment instructions.</p>";
                    payment_instructions.InnerHtml = paymentInstructionsText;
                    payment_instructions.Visible = true;
                }
                else
                {
                    payment_instructions.Visible = false;
                }
                if (printConf == 1)
                    Page.ClientScript.RegisterStartupScript(typeof(String), "print", @"<script>window.print();</script>");  //Page.RegisterStartupScript("print", @"<script>window.print();</script>"); 


            }
        }
    }
}