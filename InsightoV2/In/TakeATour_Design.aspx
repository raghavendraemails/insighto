﻿<%@ Page Title="Logo for Branding – Design Surveys" Language="C#" MasterPageFile="~/Home.master"
    AutoEventWireup="true" CodeFile="TakeATour_Design.aspx.cs" Inherits="In_TakeATour_Design" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <meta name="description" content="Upload your logo for better survey branding into your online surveys." />
    <meta name="keywords" content=" design a survey, online survey" />
    <div class="takeATourContentPanel">
        <!-- take a tour main content panel -->
        <div class="faqTopMenuPanel">
            <!-- tab links -->
            <ul id="faqNav">
                <li>
                    <asp:HyperLink ID="lnkCreate" runat="server" ToolTip="Creating Online Web Surveys" NavigateUrl="~/In/TakeaTour.aspx"
                        Text="Create"></asp:HyperLink></li>
                <li class="activelink">
                    <asp:HyperLink ID="lnkDesign" runat="server" NavigateUrl="~/In/TakeATour_Design.aspx"
                        ToolTip="Logo for Branding – Design Surveys" Text="Design"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkLaunch" runat="server" NavigateUrl="~/In/TakeATour_Launch.aspx"
                        ToolTip="Launch Online Surveys" Text="Launch"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkAnalyze" runat="server" NavigateUrl="~/In/TakeATour_Analyze.aspx"
                        ToolTip="Manage Responses and Analyze" Text="Analyze"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkManage" runat="server" NavigateUrl="~/In/TakeATour_Manage.aspx"
                        ToolTip="Manage Email Lists" Text="Manage"></asp:HyperLink></li>
            </ul>
            <div class="clear">
            </div>
            <!-- //tab links -->
        </div>
        <div class="clear">
        </div>
        <div class="takeATourTextPanel">
            <!-- text panel -->
            <div class="takeATourButtonPanel">
                <!-- prev next bubtton -->
                <asp:HyperLink ID="lnkPrevius" runat="server" NavigateUrl="javascript:void(0)" CssClass="active"
                    ToolTip="Previous" Text="Prev"></asp:HyperLink>
                <asp:HyperLink ID="lnkNext" runat="server" NavigateUrl="javascript:void(0)" CssClass="active"
                    ToolTip="Next" Text="Next"></asp:HyperLink>
                <!-- //prev next bubtton -->
            </div>
            <div style="display: block" id="Create_1">
                <!-- gettin started -->
                <h3>
                    Give your survey a professional look – Add your corporate logo</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/design-img-01.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <br />
                <ul>
                    <li>Upload the logo of your company with ease</li>
                    <li>Preview the logo and customize the size</li>
                    <li>Add company name with logo - optional</li>
                    <li>Choose not to show logo – with one click</li>
                </ul>
                <!-- //gettin started -->
            </div>
            <div style="display: none" id="Create_2">
                <!-- question types -->
                <h3>
                    Give your survey a colour theme</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/design-img-02.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>6 colour themes to choose from</li>
                    <li>Select the theme of your choice</li>
                    <li>Set theme as your default for all your surveys or change theme for every survey</li>
                </ul>
                <!-- //question types -->
            </div>
            <div style="display: none" id="Create_3">
                <!-- answer options -->
                <h3>
                    Give your survey a font of your choice</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/design-img-03.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>Choose from the list of frequently used and most popular fonts</li>
                    <li>Customize the size and colour of your fonts</li>
                    <li>Set font as your default for all surveys or keep customizing for every survey</li>
                </ul>
                <!-- answer options -->
            </div>
            <div style="display: none" id="Create_4">
                <!-- questionnair -->
                <h3>
                    Customize the survey end pages as per your requirement</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/design-img-04.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <br />
                <h4>
                    You can customize all your survey end messages:</h4>
                <ul>
                    <li>Thank you page</li>
                    <li>Survey close page</li>
                    <li>Screen out page</li>
                    <li>Over quota page</li>
                </ul>
                <!-- questionnair -->
            </div>
            <!-- //text panel -->
        </div>
        <div class="takeATourSubMenuPanel">
            <!-- sub menu -->
            <ul class="takeATourMenu">
                <li><a id="lnkCreate_1" class="active" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(1);">
                    Survey Branding</a></li>
                <li><a id="lnkCreate_2" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(2);">
                    Themes</a></li>
                <li><a id="lnkCreate_3" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(3);">
                    Customize Font</a></li>
                <li><a id="lnkCreate_4" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(4);">
                    Customize Thank You Page</a></li>
            </ul>
            <!-- //sub menu -->
        </div>
         <input type="hidden" class="hdnPanelIndex" id="hdnPanelIndex" value="1" />
        <!-- // take a tour main content panel -->
    </div>
    
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('.hdnPanelIndex').val() == '1') {
                $('#<%=lnkPrevius.ClientID %>').hide();
            }
        });
        $('#<%=lnkNext.ClientID %>').click(function () {
            var index = $('.hdnPanelIndex').val();
            $('#Create_' + index).hide();
            $('#lnkCreate_' + index).removeClass('active');
            index++;
            $('#Create_' + index).show();
            $('.hdnPanelIndex').val(index);
            if ($('.hdnPanelIndex').val() > 1) {
                $('#<%=lnkPrevius.ClientID %>').show();
                $('#lnkCreate_' + index).addClass('active');
                if (index == 4) {
                    $('#<%=lnkNext.ClientID %>').hide();
                }
            }
        });
        $('#<%=lnkPrevius.ClientID %>').click(function () {
            var index = $('.hdnPanelIndex').val();
            $('#Create_' + index).hide();
            $('#lnkCreate_' + index).removeClass('active');
            index--;
            $('#Create_' + index).show();
            $('.hdnPanelIndex').val(index);
            if ($('.hdnPanelIndex').val() < 4) {
                $('#<%=lnkNext.ClientID %>').show();
                $('#lnkCreate_' + index).addClass('active');
                if (index == 1) {
                    $('#<%=lnkPrevius.ClientID %>').hide();
                }
            }
        });
        function ShowSelectedScreen(index) {
            if (index == 1) {
                $('#<%=lnkPrevius.ClientID %>').hide();
                $('#<%=lnkNext.ClientID %>').show();
            }
            else if (index == 4) {
                $('#<%=lnkNext.ClientID %>').hide();
                $('#<%=lnkPrevius.ClientID %>').show();
            }
            else {
                $('#<%=lnkNext.ClientID %>').show();
                $('#<%=lnkPrevius.ClientID %>').show();
            }
            var prevIndex = $('.hdnPanelIndex').val();
            $('#Create_' + prevIndex).hide();
            $('#lnkCreate_' + prevIndex).removeClass('active');
            $('#Create_' + index).show();
            $('#lnkCreate_' + index).addClass('active');
            $('.hdnPanelIndex').val(index);

        }
    </script>
</asp:Content>
