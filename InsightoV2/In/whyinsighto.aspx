﻿<%@ Page Title="Creating Online Web Survey made Easy - Insighto" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true"
    CodeFile="whyinsighto.aspx.cs" Inherits="In_whyinsighto" %>    
<%@ Register TagPrefix="uc" TagName="Advertizement" Src="~/In/UserControls/RightAdvertizement.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <meta name="description" content="Insighto is an easy to use online survey software that does not require any installation where creating a survey can be done within minutes." />
   <meta name="keywords" content="online survey software, creating online surveys, creating an online survey, creating web survey, online survey software tool, survey tool online" />
    <table border="0" width="100%">
        <tr>
            <td valign="top">
                <div class="marginNewInnerPanel">
                    <div class="whyInsightoPnl">
                      <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="Why Insighto Online Survey software"
                                meta:resourcekey="lblTitleResource1"></asp:Label>
                        </h2>
                        <div class="text content" style="padding: 10px 20px;" id="divContent" runat="server">
                        </div>
                    </div>
                </div>
            </td>
          <%--  <td valign="top" style="padding-top:12px;">
                <%--<uc:Advertizement ID="ucAdvertizement" runat="server" />
                    <div class="whyInsightoRightPanel">
        <!-- take a tour right panel -->
        <div class="tATRightTextPanel">
            <h2>
                Create an online survey - fast and easy</h2>
            <div class="tATText">
                <p>
                    <asp:HyperLink ID="lnkSignupFree" CssClass="dynamicHyperLink" runat="server" NavigateUrl=
                        Text="Sign Up Free"></asp:HyperLink>
                </p>
            </div>
        </div>
        <div class="tATRightTextPanel">
            <h2>
                Do more, Do better </h2>

                <p>&nbsp;&nbsp;&nbsp;
                <asp:HyperLink ID="HyperLink1"   runat="server" NavigateUrl=
                        Text="See Pro and Premium Plans"></asp:HyperLink>
                </p>

            <div style="border-left: solid 5px #FFD5B6; padding-left: 20px; padding-right: 10px;">
                <p>
                    View individual responses</p>
                    <p>
                    Export raw data</p>
                <p>
                    Receive unlimited responses</p>
                <p>
                   Export data to<br /> Excel/PPT/PDF/Word</p>
                <p>
                   Brand your survey</p>
                   
            </div>
            <div class="tATText">
                <table>
                <tr>
                <td>
                    <asp:LinkButton ID="lnkBtnYearly" CssClass="dynamicHyperLink" runat="server" Width="70px" Text="Buy Now" OnClick="lnkBtnYearly_Click"></asp:LinkButton></td>
                    
                <td>
                 <%-- <img id="ImgINR" src="../images/homeimages/pay_button.png" alt="button" runat="server" visible="true" />
                  <img id="ImgUSD" src="../images/homeimages/US_pro_btn.png" alt="button" runat="server" visible="false" />
               </td>
               </tr>
               </table>
            </div>
        </div>
        <input type="hidden" class="hdnPanelIndex" id="hdnPanelIndex" value="1" />
        <!-- //take a tour right panel -->
    </div>
            </td>--%>
        </tr>
    </table>
</asp:Content>
