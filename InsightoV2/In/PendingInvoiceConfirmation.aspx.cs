﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Services;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using App_Code;

namespace Insighto.Pages.In
{
    public partial class In_PendingInvoiceConfirmation : System.Web.UI.Page
    {
        int orderId = 0, userId = 0, succTran = 0;
        string strNavUrl = "";
        string supportEmail = "";
        Hashtable ht = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0)
                {
                    if (ht.Contains("ord_id"))
                        orderId = Convert.ToInt32(ht["ord_id"]);
                    if (ht.Contains("Usrid"))
                        userId = Convert.ToInt32(ht["Usrid"]);
                    if (ht.Contains("tran_status"))
                        succTran = Convert.ToInt32(ht["tran_status"]);
                }
            }
            if (succTran > 0)
            {
                if (succTran == 2)
                {
                    var userService = ServiceFactory.GetService<UsersService>();
                    userService.DeclineOrder(userId, orderId, succTran);
                }
                List<string> listConfg = new List<string>();
                listConfg.Add(Constants.SUPPORTEMAIL);
                var configService = ServiceFactory.GetService<ConfigService>();
                var configvalue = configService.GetConfigurationValues(listConfg);
                foreach (var email in configvalue)
                {
                    supportEmail = email.CONFIG_VALUE;
                }
                strNavUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserContactusURL(), "Usrid=" + userId);
                string strDisplayText = String.Format(Utilities.ResourceMessage("PendingInvoice_Issue_Contact"), strNavUrl, supportEmail);//"If you experience any issue/ difficulty in registration/activation, please contact us at <a href='http://insighto.com/osm/In/" + strNavUrl + "'>" + supportEmail + "</a>";
                contactus_text.InnerHtml = strDisplayText;

            }
        }
    }
}