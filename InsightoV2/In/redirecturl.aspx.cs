﻿using System;
using System.Collections.Generic;
using App_Code;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Data;
using System.Data.SqlClient;

namespace In
{
    public partial class InRedirectUrl : BasePage
    {
        string _workingKey;
        string _orderId;
        string _merchantId;
        string _amount;
        string _authDesc;
        string _checksum;
        string _licensetype;
        int _userId = -1;
        int _pkOrderid = -1;
        
        protected override void OnLoad(EventArgs e)
        {
            Populate();
        }

        #region "Method : Populate"
        void Populate()
        {
            _workingKey = "";
            _merchantId = Request.Form["Merchant_Id"];
            _orderId = Request.Form["Order_Id"];
            _amount = Request.Form["Amount"];
            _authDesc = Request.Form["AuthDesc"];
            _checksum = Request.Form["Checksum"];
            _licensetype = Request.Form["Merchant_Param"];


            PollCreation pc = new PollCreation();
           DataSet dsorder =  pc.getPollOrder(_orderId);

           if (dsorder.Tables[0].Rows.Count > 0)
           {
               _userId = Convert.ToInt32(dsorder.Tables[0].Rows[0]["USERID"].ToString());
               _pkOrderid = Convert.ToInt32(dsorder.Tables[0].Rows[0]["PK_ORDERID"].ToString());

               var listConfig = new List<string> { Constants.WORKINGKEY };
               var configService = ServiceFactory.GetService<ConfigService>();
               var configvalue = configService.GetConfigurationValues(listConfig);

               foreach (var cv in configvalue)
               {
                   _workingKey = cv.CONFIG_VALUE;
               }
               var newCheckSum = Utilities.Verifychecksum(_merchantId, _orderId, _amount, _authDesc, _workingKey, _checksum);
               if ((newCheckSum == "true") && (_authDesc == "Y"))
               {
                   //Message.Text = "<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.";

                   /* 
                       Here you need to put in the routines for a successful 
                        transaction such as sending an email to customer,
                        setting database status, informing logistics etc etc
                   */
                   var urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserInvoiceConfirmationURL(), "ord_id=" + _pkOrderid + "&invoice_id=" + _orderId + "&Usrid=" + _userId + "&tran_status=1" + "&polllicensetype=" + _licensetype);
                   Response.Redirect(urlstr);
               }
               else if ((newCheckSum == "true") && (_authDesc == "N"))
               {
                   //Message.Text =  "<br>Thank you for shopping with us. However,the transaction has been declined.";
                   /*
                       Here you need to put in the routines for a failed
                       transaction such as sending an email to customer
                       setting database status etc etc
                   */

                   var urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserDeclinedCardURL(), "order_id=" + _orderId + "&Usrid=" + _userId + "&Am=" + _amount + "&pk_order=" + _pkOrderid);
                   Response.Redirect(urlstr);
               }
               else if ((newCheckSum == "true") && (_authDesc == "B"))
               {
                   //Message.Text =  "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";
                   /*
                       Here you need to put in the routines/e-mail for a  "Batch Processing" order
                       This is only if payment for this transaction has been made by an American Express Card
                       since American Express authorisation status is available only after 5-6 hours by mail from ccavenue and at the "View Pending Orders"
                */
                   //string urlstr = URLSecurity.EncryptQuerystring("InVoiceConfirmation.aspx", "ord_id=" + pk_orderid + "&Usrid=" + UsrId + "&tran_status=2");
                   string urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetUserPendingInvoiceURL(), "ord_id=" + _pkOrderid + "&Usrid=" + _userId + "&tran_status=2");
                   Response.Redirect(urlstr);
               }
               else
               {
                   //Message.Text = "Merchant Id is:" + Merchant_Id + " Order Id is" + Order_Id + " Check sum is" + Checksum + " AuthDescription is" + AuthDesc + " Amount is:" + Amount + "<br>Security Error. Illegal access detected";
                   lblMessage.Text = Utilities.ResourceMessage("RedirectUrl_Error_Security");//@"<br>Security Error. Illegal access detected";
                   /*
                       Here you need to simply ignore this and dont need
                       to perform any operation in this condition
                   */
               }
           }
           else
           {
               var orderInfo = ServiceFactory.GetService<OrderInfoService>().GetOrderInfoByOrderId(_orderId);
               foreach (var ud in orderInfo)
               {
                   _userId = ud.USERID;
                   _pkOrderid = ud.PK_ORDERID;
               }

               var listConfig = new List<string> { Constants.WORKINGKEY };
               var configService = ServiceFactory.GetService<ConfigService>();
               var configvalue = configService.GetConfigurationValues(listConfig);

               foreach (var cv in configvalue)
               {
                   _workingKey = cv.CONFIG_VALUE;
               }
               var newCheckSum = Utilities.Verifychecksum(_merchantId, _orderId, _amount, _authDesc, _workingKey, _checksum);
               if ((newCheckSum == "true") && (_authDesc == "Y"))
               {
                   //Message.Text = "<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.";

                   /* 
                       Here you need to put in the routines for a successful 
                        transaction such as sending an email to customer,
                        setting database status, informing logistics etc etc
                   */
                   var urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetUserInvoiceConfirmationURL(), "ord_id=" + _pkOrderid + "&invoice_id=" + _orderId + "&Usrid=" + _userId + "&tran_status=1");
                   Response.Redirect(urlstr);
               }
               else if ((newCheckSum == "true") && (_authDesc == "N"))
               {
                   //Message.Text =  "<br>Thank you for shopping with us. However,the transaction has been declined.";
                   /*
                       Here you need to put in the routines for a failed
                       transaction such as sending an email to customer
                       setting database status etc etc
                   */

                   var urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetUserDeclinedCardURL(), "order_id=" + _orderId + "&Usrid=" + _userId + "&Am=" + _amount + "&pk_order=" + _pkOrderid);
                   Response.Redirect(urlstr);
               }
               else if ((newCheckSum == "true") && (_authDesc == "B"))
               {
                   //Message.Text =  "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";
                   /*
                       Here you need to put in the routines/e-mail for a  "Batch Processing" order
                       This is only if payment for this transaction has been made by an American Express Card
                       since American Express authorisation status is available only after 5-6 hours by mail from ccavenue and at the "View Pending Orders"
                */
                   //string urlstr = URLSecurity.EncryptQuerystring("InVoiceConfirmation.aspx", "ord_id=" + pk_orderid + "&Usrid=" + UsrId + "&tran_status=2");
                   string urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetUserPendingInvoiceURL(), "ord_id=" + _pkOrderid + "&Usrid=" + _userId + "&tran_status=2");
                   Response.Redirect(urlstr);
               }
               else
               {
                   //Message.Text = "Merchant Id is:" + Merchant_Id + " Order Id is" + Order_Id + " Check sum is" + Checksum + " AuthDescription is" + AuthDesc + " Amount is:" + Amount + "<br>Security Error. Illegal access detected";
                   lblMessage.Text = Utilities.ResourceMessage("RedirectUrl_Error_Security");//@"<br>Security Error. Illegal access detected";
                   /*
                       Here you need to simply ignore this and dont need
                       to perform any operation in this condition
                   */
               }
           }
        }
        #endregion
    }
}