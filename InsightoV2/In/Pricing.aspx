﻿<%@ Page Title="Plans and Pricing - Insighto" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true"
    CodeFile="Pricing.aspx.cs" Inherits="Insighto.Pages.In.In_Pricing"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<meta name="description" content="Design, build and create an online web survey for free or sign up for a low-cost account to add surveying functionality." />
    <meta name="keywords" content=" design a survey, build a survey, create online survey" />
    <style type="text/css" media="screen">
        @import "../App_Themes/Classic/price.css";
    </style>
  
  <div style="display:none;">
     <div class="informationPanelDefault">
            <div>
                <p>
                <asp:Label ID="lblOnlineFeatures" runat="server" 
                        Text=" <b>Online Survey Features</b> - Empower your survey with advance survey creation,    control and reporting features." 
                        meta:resourcekey="lblOnlineFeaturesResource1"></asp:Label>
                   </p>
            </div>
        </div>
        </div>

         <div class="pricingGrid">
            <div>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <tr>
			            <td style="width:55%;" class="pricingHeader">&nbsp;</td>
			            <td style="width:20%;font-family:Arial, Helvetica, sans-serif;color:Gray;font-size:28px;" class="pricingHeader_free" >Basic</td>
			            <td style="width:25%;font-family:Arial, Helvetica, sans-serif;color:Gray;font-size:28px;"  class="pricingHeaderPro">Pro</td>
		            </tr>
		            <tr>
			            <td class="pricingRow" align="center" style="height:85px;">
			            <img src="../App_Themes/Classic/Images/pro_feature_lbl.jpg" />
			            </td>
			            <td id="headerArrowTop" class="pricingRow shadow_01">
                        <div align="center" style="margin-left:30px">
				            <a href="SignUpFree.aspx"><img src="../App_Themes/Classic/Images/button_sign_up.png" alt="Sign Up" /></a>
                            </div>
			            </td>
			            <td class="pricingRow" align="center" style="padding-top:20px">
				            <ul class="proUl">
                             <li>
                                 <%--<asp:LinkButton ID="lnkBtnMonthly" runat="server" CssClass="orangeLink priceRs"
                                  Text="450 / MONTH" OnClick="lnkBtnMonthly_Click"></asp:LinkButton>--%>
                               <%--   <asp:Label ID="lblMonthly" runat="server" CssClass="orangeLink priceRs" Text="450 / MONTH"></asp:Label>--%>
                                  <asp:Label ID="lblMonthly" runat="server" Text="450 / MONTH" style="font-weight:bold;"></asp:Label>
                               <%-- <a class="orangeLink priceRs">Go Monthly Plan (450)</a>--%></li>					           
					            <li style="padding-top:9px;">
                                <asp:ImageButton ID="imgbtnMonthly" runat="server" ImageUrl="../App_Themes/Classic/Images/button_buy_now_pricing.jpg"
                                OnClick="imgbtnMonthly_Click" meta:resourcekey="ibtnBuynowResource1" /></li>
                                 <li><asp:LinkButton ID="lnkBtnMonthly" runat="server" CssClass="orangeLink" Text="SAVE ON ANNUAL PLAN - <span class='priceRs'></span> 4500"
                                  OnClick="lnkBtnYearly_Click"></asp:LinkButton></li>
					           <%-- <li><span class="priceRs">4500</span> /Year</li>--%>
					           
				            </ul>
			            </td>
		            </tr>
                </table>
            </div>
            <div id="dvPageInfo" class="dvPageInfo" runat="server"></div>
            <div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="40px">
                <tr>
                    <td style="width:55%;" class="priceAlterRow"></td>
                    <td style="width:20%;" class="priceAlterRow shadow_01" align="center"><asp:ImageButton ID="imgTryItFree" runat="server" 
                                ImageUrl="../App_Themes/Classic/Images/button_sign_up.png" 
                                onclick="imgTryItFree_Click" meta:resourcekey="imgTryItFreeResource1" /></td>
                    <td style="width:25%;" class="priceAlterRow" align="center"> <asp:ImageButton ID="ibtnBuynow" runat="server" ImageUrl="../App_Themes/Classic/Images/button_buy_now_pricing.jpg"
                                OnClick="imgbtnMonthly_Click" meta:resourcekey="ibtnBuynowResource1" /></td>
                </tr>
            </table>
            </div>     
            </div>
            <center>
            <div class="prcingFaqHeading" id="divFaq" style="text-align:left;">          
            <a href="javascript:void(0)" id="lnkShowContent" onclick="javascript:showHideFAQPanel();" style="font-size:12px;">Pricing FAQs</a>
            <a href="javascript:void(0)" onclick="showHideFAQPanel()"><span class="plusExpandFAQ">+</span></a>
            </div>
            <div >&nbsp;</div>
  <div class="prcingFaqContent" style="display:none;text-align:left;" id="divFaqContent">  
  <div id="divFaqIndiaContent" runat="server">
  <h4>What’s the validity of the Free subscription?</h4>
  <p>There’s no end date to Insighto Free subscription. In a sense, the Free subscription has lifetime validity!</p>
    <p class="defaultHeight">&nbsp;</p>
         <h4>In short, what is it that I do not get with my Free subscription?</h4>
  <p>The Free subscription is designed to offer you a real feel of the way the survey tool works albeit with a few limitations. (Else, you won’t pay us, will you?)</p>
 <p><b>You will need a Pro subscription, if you have the following needs.</b></p>
 <ul class="listPadding">
 <li>You need more than 150 responses for your survey</li>
  <li>You would like to brand your survey with your logo</li>
  <li>You would like to have a skip logic in your survey</li>
<li>You would like to have schedule your survey launch and survey closure automated</li>
<li>You would like to redirect your respondents to a website of your choice after they complete the response</li>
<li>You would like to have a custom Thank You page</li>
<li>You would like to export your responses into an Excel, PDF, Word or a PowerPoint.</li> 
<li>You would like to view your data in graphs</li>

 </ul>
 <%-- <p class="defaultHeight">&nbsp;</p>--%>
   <h4>If I do not renew my Pro subscription, what would be the status of my account?</h4>
   <p>If for some reason, you do not renew your Pro subscription, you will automatically turn into a Free subscriber. You will be able to see the data with the limitations of a Free subscriber.</p>
     <p class="defaultHeight">&nbsp;</p> 
      <h4>If I do not renew my Pro subscription, what happens to my data?</h4>
        <p>Your data stays secure with us even if you are not a Pro subscriber. However, you will be able to see your data with the limitations of a Free subscriber.</p>
       <p class="defaultHeight">&nbsp;</p>
         <h4>What happens if I launch a survey while having a Pro subscription but do not renew while I still have active survey(s)?</h4>
           <p>Assuming that your Pro subscription is not renewed while you still have active surveys, the following scenarios are applied.</p>
             <ol style="list-style-type:lower-alpha" class="listPadding">            
             <li>If the active survey(s) have any Pro features used (for example, skip logic), such active surveys will be closed for response.</li> 
<li>If the active survey(s) do not have any Pro features used, they will continue to be active.</li>

             </ol>
            <%-- <p class="defaultHeight">&nbsp;</p>--%>
               <h4>What are the different payment options I have?</h4>
               <p><b>There are five different options you can choose from.</b></p>
                <ol style="list-style-type:lower-alpha" class="listPadding">
                <li>	Credit card</li>
<li>Debit Card</li>
<li>Net banking</li>
<li>Pay by cheque (Not available for Pro – Monthly)</li>
<li>Direct deposit (Not available for Pro – Monthly)</li>

                </ol>
              <%--  <p class="defaultHeight">&nbsp;</p>--%>
                  <h4>How do I upgrade? </h4>
                    <p>You can upgrade at any time right from “My Surveys” page. Simply click on the upgrade button and choose the plan that you’d like to upgrade to.</p>
                      <p class="defaultHeight">&nbsp;</p>
                        <h4>Is my data secure and private? </h4>
                          <p>Yes, your data is secure and private. Your collected data is yours and we never use it or share it any way shape or form. Please see our Privacy Policy, Security Information, and Terms of Use for further info.</p>
                           <p class="defaultHeight">&nbsp;</p>
                              <h4>How do I cancel my account?</h4>
                               <p>You can cancel anytime you want. For one, we would be sorry to see you go as we are constantly working to add more features that enhance your experience with Insighto.
 Please let us know how we can improve.</p>
 <p>
 If you would like to cancel your subscription, please do send a mail to Support@insighto.com. We, of course, would be keen to know the reasons for the same.
 </p>
 <p class="defaultHeight">&nbsp;</p>
 <b>Cancellation Terms:</b>
 <p>
 Once your Professional plan cancels, the account converts to the Basic (free) status with all the limitations associated with that account type. Professional features will be deactivated, but you will still have access to your account and surveys. You can reactivate it at a later date and everything will be available to you again. If you have active surveys or need to analyze data, then we recommend waiting to cancel the subscription until you are completely finished.
 </p>
 <p class="defaultHeight">&nbsp;</p>
 <p>
 <b>NOTE - Pro-Refunds:</b> We do not offer pro-rated refunds on plans. If you anticipate needing the plan for a short amount of time, purchase a PRO Monthly plan.
 </p>
 <p>
 <b>Example:</b> If you have a paid monthly subscription, it will cancel at the end of the current billing cycle. If you have an annual subscription, it will cancel at the end of the 12 months period.
 </p> 
 </div>
   <div id="divFaqOtherContent" runat="server">
  Other Countries Content Here
  </div>
  </div> 

  </center>
            <table width="631" border="0" cellpadding="0" cellspacing="0" class="tabme" style="display:none;">
                <tr>
                    <td width="216" height="30" class="leftcol1">
                        &nbsp;Pricing
                    </td>
                    <td width="90" class="shade1  bordown">
                        &nbsp;
                    </td>
                    <td width="105" class="shade2  bordown">
                        <asp:RadioButton ID="rbtnMonthly" runat="server" GroupName="Buy" 
                            meta:resourcekey="rbtnMonthlyResource1" />
                    </td>
                    <td width="115" class="shade2  bordown">
                        <asp:RadioButton ID="rbtnQuarterly" runat="server" GroupName="Buy" 
                            meta:resourcekey="rbtnQuarterlyResource1" />
                    </td>
                    <td width="105" class="shade2  bordown">
                        <asp:RadioButton ID="rbtnYear" runat="server" GroupName="Buy" 
                            meta:resourcekey="rbtnYearResource1"  />
                    </td>
                </tr>
            </table>
       <%-- </div>--%>
        
   
    <script language="javascript" type="text/javascript">
        function showHide(EL, PM) {
            ELpntr = document.getElementById(EL);
            if (ELpntr.style.display == 'none') {
                document.getElementById(PM).innerHTML = "-";
                ELpntr.style.display = 'block';
            }
            else {
                document.getElementById(PM).innerHTML = "+";
                ELpntr.style.display = 'none';
            }
        }
        function RedirectToLoginPage() {
            window.location.href = "../Home.aspx";
            return false;
        }

        function showHideFAQPanel() {
            divContent = document.getElementById('divFaqContent');
            if (divContent.style.display == 'none') {
               // document.getElementById(PM).innerHTML = "-";
                divContent.style.display = 'block';
            }
            else {
                //document.getElementById(PM).innerHTML = "+";
                divContent.style.display = 'none';
            }
        }
        
    </script>

    <!--Start of Zopim Live Chat Script-->

<script type="text/javascript">

    window.$zopim || (function (d, s) {
        var z = $zopim = function (c) { z._.push(c) }, $ = z.s =

d.createElement(s), e = d.getElementsByTagName(s)[0]; z.set = function (o) {
    z.set.

_.push(o)
}; z._ = []; z.set._ = []; $.async = !0; $.setAttribute('charset', 'utf-8');

        $.src = '//cdn.zopim.com/?bYM7XxoDiH1Gv57aQNwmeiatKZbOCdA7'; z.t = +new Date; $.

type = 'text/javascript'; e.parentNode.insertBefore($, e)
    })(document, 'script');

</script>

<!--End of Zopim Live Chat Script-->
</asp:Content>
