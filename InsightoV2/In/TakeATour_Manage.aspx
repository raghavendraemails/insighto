﻿<%@ Page Title="Email Lists - Manage" Language="C#" MasterPageFile="~/Home.master"
    AutoEventWireup="true" CodeFile="TakeATour_Manage.aspx.cs" Inherits="In_TakeATour_Manage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <meta name="description" content=" View all your online web survey stats and settings at one place.." />
    <meta name="keywords" content="online survey, online web survey, web survey" />
    <div class="takeATourContentPanel">
        <!-- take a tour main content panel -->
        <div class="faqTopMenuPanel">
            <!-- tab links -->
            <ul id="faqNav">
                <li>
                    <asp:HyperLink ID="lnkCreate" runat="server" ToolTip="Creating Online Web Surveys" NavigateUrl="~/In/TakeaTour.aspx"
                        Text="Create"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkDesign" runat="server" NavigateUrl="~/In/TakeATour_Design.aspx"
                        ToolTip="Logo for Branding – Design Surveys" Text="Design"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkLaunch" runat="server" NavigateUrl="~/In/TakeATour_Launch.aspx"
                        ToolTip="Launch Online Surveys" Text="Launch"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkAnalyze" runat="server" NavigateUrl="~/In/TakeATour_Analyze.aspx"
                        ToolTip="Manage Responses and Analyze" Text="Analyze"></asp:HyperLink></li>
                <li class="activelink">
                    <asp:HyperLink ID="lnkManage" runat="server" NavigateUrl="~/In/TakeATour_Manage.aspx"
                        ToolTip="Manage Email Lists" Text="Manage"></asp:HyperLink></li>
            </ul>
            <div class="clear">
            </div>
            <!-- //tab links -->
        </div>
        <div class="clear">
        </div>
        <div class="takeATourTextPanel">
            <!-- text panel -->
            <div class="takeATourButtonPanel">
                <!-- prev next bubtton -->
                <asp:HyperLink ID="lnkPrevius" runat="server" NavigateUrl="javascript:void(0)" CssClass="active"
                    ToolTip="Previous" Text="Prev"></asp:HyperLink>
                <asp:HyperLink ID="lnkNext" runat="server" NavigateUrl="javascript:void(0)" CssClass="active"
                    ToolTip="Next" Text="Next"></asp:HyperLink>
                <!-- //prev next bubtton -->
            </div>
            <div style="display: block" id="Create_1">
                <!-- gettin started -->
                <h3>
                    Pit stop for all survey settings and statistics</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/manage-img-01.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>View all your survey stats and settings at one place</li>
                    <li>Check number of email invites sent</li>
                    <li>View number of visits, completes, partials, bounced, screened out and over quota
                        results</li>
                    <li>View your alerts and reminders</li>
                    <li>Activate or close your survey</li>
                </ul>
                <!-- //gettin started -->
            </div>
            <div style="display: none" id="Create_2">
                <!-- question types -->
                <h3>
                    Create as many email lists as you want
                </h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/manage-img-02.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <br />
                <ul>
                    <li>Add contacts individually or bulk upload from MS Excel / CSV</li>
                    <li>Add, edit, delete and view past history of your email lists</li>
                    <li>Check the number of contacts count in each email list</li>
                    <li>Search for a specific email address from the list</li>
                </ul>
                <!-- //question types -->
            </div>
            <div style="display: none" id="Create_3">
                <!-- answer options -->
                <h3>
                    Manage your folders and categorize your surveys</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/manage-img-03.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <br />
                <ul>
                    <li>Manage your folders and categorize surveys at one place</li>
                    <li>Move surveys from one folder / category to another</li>
                </ul>
                <!-- answer options -->
            </div>
            <!-- //text panel -->
        </div>
        <div class="takeATourSubMenuPanel">
            <!-- sub menu -->
            <ul class="takeATourMenu">
                <li><a id="lnkCreate_1" class="active" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(1);">
                    Survey Dashboard</a></li>
                <li><a id="lnkCreate_2" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(2);">
                    Emails Lists</a></li>
                <li><a id="lnkCreate_3" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(3);">
                    Folders And Categories</a></li>
            </ul>
            <!-- //sub menu -->
        </div>
         <input type="hidden" class="hdnPanelIndex" id="hdnPanelIndex" value="1" />
        <!-- // take a tour main content panel -->
    </div>
    
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('.hdnPanelIndex').val() == '1') {
                $('#<%=lnkPrevius.ClientID %>').hide();
            }
        });
        $('#<%=lnkNext.ClientID %>').click(function () {
            var index = $('.hdnPanelIndex').val();
            $('#Create_' + index).hide();
            $('#lnkCreate_' + index).removeClass('active');
            index++;
            $('#Create_' + index).show();
            $('.hdnPanelIndex').val(index);
            if ($('.hdnPanelIndex').val() > 1) {
                $('#<%=lnkPrevius.ClientID %>').show();
                $('#lnkCreate_' + index).addClass('active');
                if (index == 3) {
                    $('#<%=lnkNext.ClientID %>').hide();
                }
            }
        });
        $('#<%=lnkPrevius.ClientID %>').click(function () {
            var index = $('.hdnPanelIndex').val();
            $('#Create_' + index).hide();
            $('#lnkCreate_' + index).removeClass('active');
            index--;
            $('#Create_' + index).show();
            $('.hdnPanelIndex').val(index);
            if ($('.hdnPanelIndex').val() < 3) {
                $('#<%=lnkNext.ClientID %>').show();
                $('#lnkCreate_' + index).addClass('active');
                if (index == 1) {
                    $('#<%=lnkPrevius.ClientID %>').hide();
                }
            }
        });
        function ShowSelectedScreen(index) {
            if (index == 1) {
                $('#<%=lnkPrevius.ClientID %>').hide();
                $('#<%=lnkNext.ClientID %>').show();
            }
            else if (index == 3) {
                $('#<%=lnkNext.ClientID %>').hide();
                $('#<%=lnkPrevius.ClientID %>').show();
            }
            else {
                $('#<%=lnkNext.ClientID %>').show();
                $('#<%=lnkPrevius.ClientID %>').show();
            }
            var prevIndex = $('.hdnPanelIndex').val();
            $('#Create_' + prevIndex).hide();
            $('#lnkCreate_' + prevIndex).removeClass('active');
            $('#Create_' + index).show();
            $('#lnkCreate_' + index).addClass('active');
            $('.hdnPanelIndex').val(index);

        }
    </script>
</asp:Content>
