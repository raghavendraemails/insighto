﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.master" AutoEventWireup="true" CodeFile="CardDeclined.aspx.cs" Inherits="In_CardDeclined" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <div class="InVoice_messagePanel">
    <!-- signup panel -->
    <div class="confirm_middle_pnl">
      <h4 id="declineInvoice" runat="server" class="center"> 
          <asp:Label ID="lblSorry"  class="sorry_lbl" runat="server" Text="SORRY!! " 
              meta:resourcekey="lblSorryResource1"></asp:Label>
           <asp:Label ID="lblTransactionInfo" runat="server" Text="<br/>Your payment for the Insighto plan you chose did not go through."></asp:Label>
          </h4>
    
      <p class="center"> <asp:Label ID="lblReason" runat="server" Text="Click here to "></asp:Label></p>        
    
      <div style="text-align: center;"> <a href="#" target="_blank" id="RetryUrl" runat="server"> 
          <img src="../App_Themes/Classic/Images/button_tryagain.gif" alt="Try Again" 
              border="0" /></a></div><p>&nbsp;</p>
      <p id="txtMessage" runat="server" visible="false" class="center">
          <asp:Label ID="lblFreSubscriber" runat="server"
              Text="   Contact support@insighto.com if you are facing any issue."></asp:Label></p>
      <p id="auth_linktext" class="center" runat="server" visible="false">
          <asp:Label ID="lblActivate" runat="server" 
              Text="Till you purchase the Insighto plan you want, you can explore Insighto as a FREE user." ></asp:Label></p>
      <p>&nbsp;</p>
      
    </div>
    <div id="divHeading" runat="server">
            <h2 class="center" id="heading_activate" runat="server">
           <u><asp:LinkButton ID="lnkActivate" runat="server" Text="Login to Insighto"
                   PostBackUrl="~/Home.aspx">
            </asp:LinkButton></u> 
            
               </h2>
            
        </div>
    <div class="clear"></div>
    </div>
</asp:Content>

