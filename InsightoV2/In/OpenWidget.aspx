﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OpenWidget.aspx.cs" Inherits="In_OpenWidget" meta:resourcekey="PageResource1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Insighto</title>
     <link href="../insighto.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" 
                    Text="You are not valid user for this widget." Visible="False" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
    </form>
</body>
</html>
