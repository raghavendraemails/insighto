﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Insighto.Business.Services;
using System.Data;
using Insighto.Data;
using System.Collections;
using System.IO;
using System.Globalization;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using CurrencyExchangeRates;
using Resources;
using System.Configuration;
using System.Linq.Expressions;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;


#endregion

namespace Insighto.Pages.In
{
    public partial class In_Register : BasePage
    {
        #region Private Member declaration
        Hashtable typeHt = new Hashtable();
        private string licenseType = "";
        private string Type;
        private double price;
        private double tax = 0;
        private double taxPercentage;
        private string orderNo = "",transType="",surveytype="";
        string Revenushare = ConfigurationManager.AppSettings["Revenueshare"];
        private string utm_source;
        private string utm_term;
        public int surveyID;
        string status = "";
       // private string orderNo = "Premium", transType = "";
        int userId = 0;
       string countryName = "";
        SurveyCore surcore = new SurveyCore();
        private MailMessage mailMessage = null;
        private SmtpClient smtpMail = null;

        #endregion

        #region Events

        #region Page_Load
        /// <summary>
        /// Bind the Payment details by license type and
        /// populates user details if userid exists in the query string
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
                   

                string ipAddress="";
                if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
                }
            //ipAddress = "125.236.193.250";
            ipAddress = "62.40.127.255"; //UK
            ipAddress = "149.202.152.144";//FRANCE
            countryName = Utilities.GetCountryName(ipAddress);

           // countryName = "UNITED STATES";
                ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(countryName);
             
                countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();

               

                lblName.Text = "First Name";
                divLname.Style.Add("display", "block");
                divValidationLname.Style.Add("display", "block");
                rfvLname.Enabled = true;
                rexLname.Enabled = true;
                divUsertype.Style.Add("display", "none");
                divCompanyValidation.Style.Add("display", "none");
                rfvCompany.Enabled = false;
                divCompany.Style.Add("display", "none");
                lblPhno.Style.Add("display", "none");
                txtAreaCode.Style.Add("display", "none");
                txtCountryCode.Style.Add("display", "none");
                txtPhno.Style.Add("display", "none");
                lblCity.Style.Add("display", "none");
                txtCity.Style.Add("display", "none");
                lblCountry.Style.Add("display", "none");
                ddlCountry.Style.Add("display", "none");
              lblRecaptcha.Style.Add("display", "none");
                txtCaptcha.Style.Add("display", "none");
                lblPhoneHelpText.Style.Add("display", "none");
                reCaptchaimg.Style.Add("display", "none");
                typecaptcha.Style.Add("display", "none");
                dash1.Style.Add("display", "none");
                dash2.Style.Add("display", "none");
                reqPhno.Style.Add("display", "none");
                reqPhno.Enabled = false;
                reqCityName.Style.Add("display", "none");
                reqCityName.Enabled = false;
                reqCountry.Style.Add("display", "none");
                reqCountry.Enabled = false;
                btnContinue.Visible = true;
                btnCreateAccount.Visible = false;
                paymentpanel.Visible = false;
            

            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (typeHt.Contains("LicenseType"))
                    licenseType = typeHt["LicenseType"].ToString();
                if (typeHt.Contains(Constants.USERID))
                    userId = ValidationHelper.GetInteger(typeHt[Constants.USERID].ToString(), 0);
                 if (typeHt.Contains("Flag"))
                     transType = Convert.ToString(typeHt["Flag"]);
                 if (typeHt.Contains("surveytype"))
                     surveytype = Convert.ToString(typeHt["surveytype"]);
                 if (typeHt.Contains("SurveyId"))
                     surveyID = Convert.ToInt32(typeHt["SurveyId"]);
                 if (typeHt.Contains("status"))
                     status = typeHt["status"].ToString();

                  utm_source =  Convert.ToString(typeHt["utm_source"]);
                  utm_term = Convert.ToString(typeHt["utm_term"]);
                
            }
            if (licenseType == "PPS_PREMIUM")
            {
                dvPremium.InnerHtml = Utilities.ResourceMessage("divpremiumsingle");
                
            }
            else if (licenseType == "PREMIUM_YEARLY")
            {
                dvPremium.InnerHtml = Utilities.ResourceMessage("divPremiumNote");
                
            }
            else if (licenseType == "PPS_PRO")
            {
                dvProAnnual.InnerHtml = Utilities.ResourceMessage("divprosingle"); 
            }
            else if (licenseType == "PRO_YEARLY")
            {
                dvProAnnual.InnerHtml = Utilities.ResourceMessage("divProAnnual");
            }


            dvProMonthly.InnerHtml = Utilities.ResourceMessage("divProMonthlyString");
            dvProQuarterly.InnerHtml = Utilities.ResourceMessage("divProQuarterlyString");
            

            if (Session["userdetails"] != null)
            {
                txtName.Text = Session["username"].ToString();
                txtUserName.Text = Session["emailid"].ToString();
                //txtPassword.Text = ;
                txtPassword.Attributes.Add("value",Session["password"].ToString());
                txtConfirmPassword.Attributes.Add("value",Session["password"].ToString());
            }

            if (!IsPostBack)
            {
                BindCountry();
                string paylicensetype;
                if (licenseType == "")
                {
                    paylicensetype = "PRO_YEARLY";
                   
                }
                
                else 
                {
                    paylicensetype = licenseType;
                   
                }


                //BindPaymentDetails(licenseType);
                BindPaymentDetails(paylicensetype);

              
                if (userId > 0)
                {
                    txtUserName.Enabled = false;
                    divPassword.Style.Add("display", "none");
                    divPasswordhelp.Style.Add("display", "none");
                    divPasswordValidation.Style.Add("display", "none");
                    divconfirmpassword.Style.Add("display", "none");
                    divConfirmPasswordValidation.Style.Add("display", "none");
                    var usersService = ServiceFactory.GetService<UsersService>();
                    var userInfo = usersService.GetUserDetails(userId);

                    if (userInfo.Any())
                    {
                        txtName.Text = userInfo[0].FIRST_NAME;
                        txtUserName.Text = userInfo[0].LOGIN_NAME;
                        txtLname.Text = userInfo[0].LAST_NAME;
                        if (userInfo[0].USER_TYPE == 2)
                        {
                            rbtnIndividual.Checked = false;
                            rbtnOrganization.Checked = true;
                            Page.RegisterStartupScript("Indshow", "<script>ShowHide('show')</script>");
                            txtOrganization.Text = userInfo[0].USER_COMPANY;
                            txtDesignation.Text = userInfo[0].USER_DESIGNATION;

                        }
                        else
                        {
                            rbtnIndividual.Checked = true;
                            rbtnOrganization.Checked = false;
                            Page.RegisterStartupScript("Indshow", "<script>ShowHide('hide')</script>");
                            txtOrganization.Text = "";
                            txtDesignation.Text = "";
                        }

                        reqPassword.Enabled = false;
                        regNewPassword.Enabled = false;
                        cvNewPassword.Enabled = false;
                        reqConfirmPassword.Enabled = false;
                        cmpPassword.Enabled = false;
                        if (userInfo[0].PHONE != null)
                        {
                            string phone = userInfo[0].PHONE;
                            string coutrycode = "";
                            string regioncode = "";
                            int x = phone.IndexOf(Constants.PHONE_SEPARATER);
                            if (x >= 0)
                            {
                                coutrycode = phone.Substring(0, x);
                                phone = phone.Substring(x + 1);
                                x = phone.IndexOf(Constants.PHONE_SEPARATER);
                                regioncode = phone.Substring(0, x);
                                regioncode = phone.Substring(0, x);
                                phone = phone.Substring(x + 1);
                            }
                            txtCountryCode.Text = coutrycode;
                            txtAreaCode.Text = regioncode;
                            txtPhno.Text = phone;
                        }
                        txtCity.Text = userInfo[0].CITY;
                        if (userInfo[0].COUNTRY != null && userInfo[0].COUNTRY != "")
                            ddlCountry.SelectedValue = userInfo[0].COUNTRY;

                    }
                    hdnUpgrade.Value = transType;

                }
                else
                {
                    hdnUpgrade.Value = "New";
                    txtUserName.Enabled = true;
                }
                SetSignLink();
            }

        }

        private void SetSignLink()
        {
            if (Session["UserInfo"] != null)
                trSignIn.Visible = false;
            else
                trSignIn.Visible = true;

        }
        #endregion

        #region "Method : BindCountry"
        /// <summary>
        /// Used to get country list and bind values to dropdownlist.
        /// </summary>

        private void BindCountry()
        {
            // object on osm_picklist class
            osm_picklist osmPickList = new osm_picklist();
            //Assign vaule category
            osmPickList.CATEGORY = Constants.COUNTRY;
            // object on PickListService class
            PickListService pickListService = new PickListService();
            //inovke method GetPickList
            var vPickList = pickListService.GetPickList(osmPickList);

            if (vPickList.Count > 0)
            {
                //Bind list values to dropdownlist
                this.ddlCountry.DataSource = vPickList;
                this.ddlCountry.DataTextField = Constants.PARAMETER;
                this.ddlCountry.DataValueField = Constants.PARAMETER;
                this.ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("--Select--", ""));
            }

        }
        #endregion

        #region BindPaymentDetails
        /// <summary>
        /// bind the payment details based on the license type
        /// </summary>
        /// <param name="licenseType"></param>
        private void BindPaymentDetails(string licenseType)
        {
            if (licenseType.ToUpper() == "PRO_MONTHLY")
            {
                divMonthlyNote.Style.Add("display", "block");
                divQuarterlyNote.Style.Add("display", "none");
                divAnnualNote.Style.Add("display", "none");
                divPremiumNote.Style.Add("display", "none");
               // ucAdvertisement.Visible = true;
                rbtnListPaymentType.Items.FindByValue("2").Enabled = false;
                                rbtnListPaymentType.Items.FindByValue("4").Enabled = false;
                                lblprovalid.Visible = true;
                                lblpromand.Visible = true;
             
            }                

            else if (licenseType.ToUpper() == "PRO_QUARTERLY")
            {
                divQuarterlyNote.Style.Add("display", "block");
                divMonthlyNote.Style.Add("display", "none");
                divAnnualNote.Style.Add("display", "none");
                divPremiumNote.Style.Add("display", "none");
                //ucAdvertisement.Visible = true;
            }
            else if (licenseType.ToUpper() == "PRO_YEARLY")
            {
                divAnnualNote.Style.Add("display", "block");
                divMonthlyNote.Style.Add("display", "none");
                divQuarterlyNote.Style.Add("display", "none");
                divPremiumNote.Style.Add("display", "none");
                //ucAdvertisement.Visible = false;
            }
            else if (licenseType.ToUpper() == "PPS_PRO")
            {
                divAnnualNote.Style.Add("display", "block");
                divMonthlyNote.Style.Add("display", "none");
                divQuarterlyNote.Style.Add("display", "none");
                divPremiumNote.Style.Add("display", "none");
                //ucAdvertisement.Visible = false;
            }
            else if (licenseType.ToUpper() == "PREMIUM_YEARLY")
            {
                divPremiumNote.Style.Add("display", "block");
                divMonthlyNote.Style.Add("display", "none");
                divQuarterlyNote.Style.Add("display", "none");
                divAnnualNote.Style.Add("display", "none");
                
                //ucAdvertisement.Visible = false;
            }
            else if (licenseType.ToUpper() == "PPS_PREMIUM")
            {
                divPremiumNote.Style.Add("display", "block");
                divMonthlyNote.Style.Add("display", "none");
                divQuarterlyNote.Style.Add("display", "none");
                divAnnualNote.Style.Add("display", "none");

                //ucAdvertisement.Visible = false;
            }
            if (countryName.ToUpper() == "INDIA")
            {
                string[] LICINFO = new string[2];
                LICINFO = licenseType.ToUpper().Split('_');
                LICINFO[0] = "ORDER_" + LICINFO[0];
                LICINFO[1] = LICINFO[1] + "_NOTATION";
                List<string> listConfg = new List<string>();
                listConfg.Add(licenseType);
                listConfg.Add(Constants.TAX);
                listConfg.Add(LICINFO[0]);
                listConfg.Add(LICINFO[1]);
                listConfg.Add(Constants.MERCHANTID);
                var configvalue = ServiceFactory.GetService<ConfigService>().GetConfigurationValues(listConfg);
               // ((Label)ucPurchaseMenu.FindControl("lblSubscriptiontype")).Text = licenseType;

                if (licenseType == "PPS_PRO")
                {
                    ((Label)ucPurchaseMenu.FindControl("lblSubscriptiontype")).Text = "Pro-Single";
                    orderNo = "PPSPRO";
                }
                else if (licenseType == "PPS_PREMIUM")
                {
                    ((Label)ucPurchaseMenu.FindControl("lblSubscriptiontype")).Text = "Premium-Single";
                    orderNo = "PPSPREM";

               
                    if (surveytype == "PPS_PRO")
                    {
                        ((Label)ucPurchaseMenu.FindControl("lblexistprodtext")).Visible = true;
                        ((Label)ucPurchaseMenu.FindControl("lblexistprod")).Visible = true;
                        ((Label)ucPurchaseMenu.FindControl("eprodash")).Visible = true;
                    }
                
                }
                else
                {
                    ((Label)ucPurchaseMenu.FindControl("lblSubscriptiontype")).Text = licenseType;
                }

                if (licenseType == "PREMIUM_YEARLY")
                {
                    orderNo = "PREM";
                    lblMemberType.Text = "Premium_Yearly";
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Visible = true;
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Text = "PREMIUM YEARLY";
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Visible = false;
                    lblPreRegistrationText.Visible = true;
                    lblRegistrationText.Visible = false;
                }
                else
                {
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Visible = true;
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Visible = false;
                    if (licenseType == "PPS_PRO")
                    {
                        lblProRegistrationText.Visible = true;
                        lblRegistrationText.Visible = false;
                        lblPreRegistrationText.Visible = false;
                    }
                    else
                    {
                        lblProRegistrationText.Visible = false;
                        lblRegistrationText.Visible = true;
                        lblPreRegistrationText.Visible = false;
                    }
                }

                lblSubscriptiontype.Text = licenseType;
                hdnType.Value = licenseType;
                foreach (var cv in configvalue)
                {
                    if (cv.CONFIG_KEY == licenseType)
                    {

                        if (utm_source != "")
                        {
                            SurveyCore surcore = new SurveyCore();
                            DataSet dspartnerinfo;
                            dspartnerinfo = surcore.getPartnerInfo(utm_source);

                            int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                            int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());


                            if ((Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) == 0) || (Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) < 0))
                            {
                                price = Convert.ToDouble(cv.CONFIG_VALUE);
                            }
                            else
                            {
                                if (licenseType == "PRO_YEARLY")
                                {
                                    price = Convert.ToDouble(cv.CONFIG_VALUE) - (prodisval * Convert.ToDouble(cv.CONFIG_VALUE)) / 100;
                                }
                                else if (licenseType == "PREMIUM_YEARLY")
                                {
                                    price = Convert.ToDouble(cv.CONFIG_VALUE) - (premdisval * Convert.ToDouble(cv.CONFIG_VALUE)) / 100 - 40;
                                }

                            }
                        }
                        else
                        {
                            price = Convert.ToDouble(cv.CONFIG_VALUE);
                        }


                        if (surveytype == "PPS_PRO")
                        {
                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = Utilities.FormatCurrency(Math.Round(price, 2), false, true);
                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).CssClass = "rupeeSmallLbl lblBorder";
                            lblPrice.Text = Utilities.FormatCurrency(Math.Round(price, 2), false, true);
                            hdnPrice.Value = price.ToString();

                            double existproprodprice = 448;
                            ((Label)ucPurchaseMenu.FindControl("lblexistprod")).Text = Utilities.FormatCurrency(Math.Round(existproprodprice, 2), false, true);
                            ((Label)ucPurchaseMenu.FindControl("lblexistprod")).CssClass = "rupeeSmallLbl lblBorder";

                        }
                        else
                        {

                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = Utilities.FormatCurrency(Math.Round(price, 2), false, true);
                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).CssClass = "rupeeSmallLbl lblBorder";
                            lblPrice.Text = Utilities.FormatCurrency(Math.Round(price, 2), false, true);
                            hdnPrice.Value = price.ToString();
                        }
                    }
                    if (cv.CONFIG_KEY == Constants.TAX)
                    {
                        taxPercentage = Convert.ToDouble(cv.CONFIG_VALUE.ToString());
                        hdnTax.Value = taxPercentage.ToString();
                    }
                    if (cv.CONFIG_KEY == Constants.MERCHANTID)
                    {
                        hdnMerchantId.Value = cv.CONFIG_VALUE.ToString();
                    }
                    if (cv.CONFIG_KEY == LICINFO[1])
                    {
                        orderNo += "-" + cv.CONFIG_VALUE.ToString();
                    }
                }


                if (surveytype == "PPS_PRO")
                {
                    price = Convert.ToDouble(Utilities.FormatCurrency(Math.Round(price - 448, 2), false, true));
                }


                tax = Math.Round(Convert.ToDouble(price * Convert.ToDouble(taxPercentage) / 100));
                hdnTaxAmount.Value = tax.ToString();

                ((Label)ucPurchaseMenu.FindControl("lblServiceTax")).Text = Utilities.FormatCurrency(Math.Round(tax, 2), false, true);             
                ((Label)ucPurchaseMenu.FindControl("lblServiceTax")).CssClass = "rupeeSmallLbl lblBorder";
                lblServiceTax.Text = Utilities.FormatCurrency(Math.Round(tax, 2), false, true);

              
                ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = Utilities.FormatCurrency(Math.Round(price + tax, 2), false, true);

                ((Label)ucPurchaseMenu.FindControl("lblServiceTaxText")).Text = "Service Tax(" + hdnTax.Value + "%)";
                tdServiceTax.InnerHtml = "Service Tax(" + hdnTax.Value + "%)";
                ((Label)ucPurchaseMenu.FindControl("lblTotal")).CssClass = "rupeeLbl lblBorder";
                lblTotal.Text = Utilities.FormatCurrency(Math.Round(price + tax, 2), false, true);
                hdnTotal.Value = Math.Round(price + tax, 2).ToString();
                hdnOrderNo.Value = orderNo;
            }
            else
            {
                ((Label)ucPurchaseMenu.FindControl("lblServiceTaxText")).Visible = false;
                ((Label)ucPurchaseMenu.FindControl("servicedash")).Visible = false;
               
                string[] LICINFO = new string[2];
                LICINFO = licenseType.ToUpper().Split('_');
                LICINFO[0] = "ORDER_" + LICINFO[0];
                LICINFO[1] = LICINFO[1] + "_NOTATION";
                List<string> listConfg = new List<string>();
                listConfg.Add(licenseType);
                listConfg.Add(Constants.TAX);
                listConfg.Add(LICINFO[0]);
                listConfg.Add(LICINFO[1]);
                listConfg.Add(Constants.MERCHANTID);
                var configvalue = ServiceFactory.GetService<ConfigService>().GetConfigurationValues(listConfg);
                if (licenseType == "PPS_PRO")
                {
                    ((Label)ucPurchaseMenu.FindControl("lblSubscriptiontype")).Text = "Pro-Single";
                }
                else if (licenseType == "PPS_PREMIUM")
                {
                    ((Label)ucPurchaseMenu.FindControl("lblSubscriptiontype")).Text = "Premium-Single";
                }
                else
                {
                    ((Label)ucPurchaseMenu.FindControl("lblSubscriptiontype")).Text = licenseType;
                }

                if (licenseType == "PREMIUM_YEARLY")
                {
                    orderNo = "PREM";
                    lblMemberType.Text = "Premium_Yearly";
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Visible = true;
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Text = "Premium Annual";
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Visible = false;
                    lblPreRegistrationText.Visible = true;
                    lblRegistrationText.Visible = false;
                    lblProRegistrationText.Visible = false;
                }
                else if (licenseType == "PPS_PRO")
                {
                    orderNo = "PPSPRO";
                    lblMemberType.Text = "Pay Per Survey";
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Visible = true;
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Text = "Pay Per Survey";
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Visible = false;
                    lblPreRegistrationText.Visible = false;
                    lblProRegistrationText.Visible = true;
                    lblRegistrationText.Visible = false;
                }
                else if (licenseType == "PPS_PREMIUM")
                {
                    orderNo = "PPSPREM";
                    lblMemberType.Text = "Pay Per Survey";
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Visible = true;
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Text = "Pay Per Survey";
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Visible = false;
                    lblPreRegistrationText.Visible = false;
                    lblRegistrationText.Visible = true;
                    lblProRegistrationText.Visible = false;

                    if (surveytype == "PPS_PRO")
                    {
                        ((Label)ucPurchaseMenu.FindControl("lblexistprodtext")).Visible = true;
                        ((Label)ucPurchaseMenu.FindControl("lblexistprod")).Visible = true;
                        ((Label)ucPurchaseMenu.FindControl("eprodash")).Visible = true;
                    }
                }
                else
                {
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Visible = true;
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Text = "Pay Per Survey";
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Visible = false;
                    // lblRegistrationText.Visible = true;
                    //  lblPreRegistrationText.Visible = false;
                }
                lblSubscriptiontype.Text = licenseType;

                hdnType.Value = licenseType;


                DataSet dsprice = surcore.getPriceLicenseCountry(countryName, licenseType);

                var priceDetails = ServiceFactory<OrderInfoService>.Instance.GetPriceByCountryName(countryName);

                var ci = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.EnglishName.Contains(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(countryName.ToLower()))).FirstOrDefault();
                if (licenseType == "PPS_PRO")
                {
                    ((Label)ucPurchaseMenu.FindControl("lblSubscriptiontype")).Text = "Pro-Single";

                    lblSubscriptiontype.Text = "Pro-Single";
                }
                else if (licenseType == "PPS_PREMIUM")
                {
                    ((Label)ucPurchaseMenu.FindControl("lblSubscriptiontype")).Text = "Premium-Single";
                    lblSubscriptiontype.Text = "Premium-Single";
                }
                else
                {
                    ((Label)ucPurchaseMenu.FindControl("lblSubscriptiontype")).Text = licenseType;
                    lblSubscriptiontype.Text = licenseType;

                }


                if (licenseType == "PREMIUM_YEARLY")
                {
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Visible = true;
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Text = "Premium Annual";
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Visible = false;
                    lblPreRegistrationText.Visible = true;
                    lblRegistrationText.Visible = false;
                    lblProRegistrationText.Visible = false;
                }
                else if (licenseType == "PPS_PRO")
                {
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Visible = true;
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Text = "Pay Per Survey";
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Visible = false;
                    lblPreRegistrationText.Visible = false;
                    lblRegistrationText.Visible = false;
                    lblProRegistrationText.Visible = true;
                }
                else if (licenseType == "PPS_PREMIUM")
                {
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Visible = true;
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Text = "Pay Per Survey";
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Visible = false;
                    lblPreRegistrationText.Visible = false;
                    lblRegistrationText.Visible = true;
                    lblProRegistrationText.Visible = false;
                }
                else
                {
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Visible = true;
                    ((Label)ucPurchaseMenu.FindControl("lblMemberType")).Text = "Pay Per Survey";
                    ((Label)ucPurchaseMenu.FindControl("lblPreMemberType")).Visible = false;
                    //lblPreRegistrationText.Visible = false;
                    //lblRegistrationText.Visible = true;
                }

                hdnTax.Value = priceDetails.SERVICE_TAX.ToString();

                if (licenseType.ToUpper() == "PRO_MONTHLY")
                {

                    ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString());
                    lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString());


                    hdnPrice.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString();
                    tax = Math.Round(Convert.ToDouble(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100));
                    hdnTaxAmount.Value = tax.ToString();
                    ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                    lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                    orderNo += "-M";

                    hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2) + tax, 2).ToString();
                }
                else if (licenseType.ToUpper() == "PRO_YEARLY")
                {

                    if (utm_source != "")
                    {
                        SurveyCore surcoreusdpro = new SurveyCore();
                        DataSet dspartnerinfo;
                        dspartnerinfo = surcoreusdpro.getPartnerInfo(utm_source);

                        int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                        int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());

                        if ((Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) == 0) || (Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) < 0))
                        {
                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2));
                            lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString());
                            hdnPrice.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString();
                            tax = Math.Round(Convert.ToDouble(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100));
                            hdnTaxAmount.Value = tax.ToString();
                            ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                            lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                            // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                            hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2) + tax, 2).ToString();
                            orderNo += "-Y";
                        }
                        else
                        {
                            double pricedemo = Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) - (prodisval * Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString())) / 100;

                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(pricedemo).ToString());
                            lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(pricedemo).ToString());
                            hdnPrice.Value = Math.Round(pricedemo).ToString();
                            tax = Math.Round(pricedemo) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100;
                            hdnTaxAmount.Value = tax.ToString();
                            ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(pricedemo + tax));
                            lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(pricedemo + tax));
                            // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                            hdnTotal.Value = Math.Round(pricedemo + tax).ToString();
                            orderNo += "-Y";
                        }
                    }
                    else
                    {

                        ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2));
                        lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString());
                        hdnPrice.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString();
                        tax = Math.Round(Convert.ToDouble(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100));
                        hdnTaxAmount.Value = tax.ToString();
                        ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                        lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                        // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                        hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2) + tax, 2).ToString();
                        orderNo += "-Y";
                    }
                }
                else if (licenseType.ToUpper() == "PPS_PRO")
                {

                    if (utm_source != "")
                    {
                        SurveyCore surcoreusdpro = new SurveyCore();
                        DataSet dspartnerinfo;
                        dspartnerinfo = surcoreusdpro.getPartnerInfo(utm_source);

                        int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                        int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());

                        if ((Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) == 0) || (Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) < 0))
                        {
                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2));
                            lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString());

                            hdnPrice.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString();
                            tax = Math.Round(Convert.ToDouble(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100), 2);
                            hdnTaxAmount.Value = tax.ToString();
                            ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                            lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                            // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                            hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2) + tax, 2).ToString();
                            orderNo += "-Y";
                        }
                        else
                        {
                            double pricedemo = Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) - (prodisval * Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString())) / 100;

                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(pricedemo).ToString());
                            lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(pricedemo).ToString());
                            hdnPrice.Value = Math.Round(pricedemo).ToString();
                            tax = Math.Round(pricedemo) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100;
                            hdnTaxAmount.Value = tax.ToString();
                            ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(pricedemo + tax));
                            lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(pricedemo + tax));
                            // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                            hdnTotal.Value = Math.Round(pricedemo + tax).ToString();
                            orderNo += "-Y";
                        }
                    }
                    else
                    {

                        ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2));
                        lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString());
                        hdnPrice.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString();
                        tax = Math.Round(Convert.ToDouble(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100), 2);
                        hdnTaxAmount.Value = tax.ToString();
                        ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                        lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                        // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                        hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2) + tax, 2).ToString();
                        orderNo += "-Y";
                    }
                }
                else if (licenseType.ToUpper() == "PREMIUM_YEARLY")
                {
                    if (utm_source != "")
                    {
                        SurveyCore surcoreusdprem = new SurveyCore();
                        DataSet dspartnerinfo;
                        dspartnerinfo = surcoreusdprem.getPartnerInfo(utm_source);

                        //int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                        //int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());

                        int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                        int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());

                        if ((Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) == 0) || (Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) < 0))
                        {
                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2));
                            lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString());
                            hdnPrice.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString();
                            tax = Math.Round(Convert.ToDouble(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100));
                            hdnTaxAmount.Value = tax.ToString();
                            ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                            lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                            // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                            hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2) + tax, 2).ToString();
                            orderNo += "-Y";
                        }
                        else
                        {
                            double pricepremdemo = Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) - (premdisval * Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString())) / 100;

                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(pricepremdemo));
                            lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(pricepremdemo));
                            hdnPrice.Value = Math.Round(pricepremdemo).ToString();
                            tax = Math.Round(pricepremdemo) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100;
                            hdnTaxAmount.Value = tax.ToString();
                            ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(pricepremdemo + tax));
                            lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(pricepremdemo + tax));
                            // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                            hdnTotal.Value = Math.Round(pricepremdemo + tax).ToString();
                            orderNo += "-Y";
                        }
                    }
                    else
                    {
                        ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2));
                        lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString());
                        hdnPrice.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString();
                        tax = Math.Round(Convert.ToDouble(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100));
                        hdnTaxAmount.Value = tax.ToString();
                        ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                        lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                        // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                        hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2) + tax, 2).ToString();
                        orderNo += "-Y";
                    }
                }
                else if (licenseType.ToUpper() == "PPS_PREMIUM")
                {
                    if (utm_source != "")
                    {
                        SurveyCore surcoreusdprem = new SurveyCore();
                        DataSet dspartnerinfo;
                        dspartnerinfo = surcoreusdprem.getPartnerInfo(utm_source);

                        int prodisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][3].ToString());
                        int premdisval = Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][4].ToString());
                        if ((Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) == 0) || (Convert.ToInt32(dspartnerinfo.Tables[0].Rows[0][6].ToString()) < 0))
                        {
                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2));
                            lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString());
                            hdnPrice.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString();
                            tax = Math.Round(Convert.ToDouble(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100), 2);
                            hdnTaxAmount.Value = tax.ToString();
                            ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                            lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                            // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                            hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2) + tax, 2).ToString();
                            orderNo += "-Y";
                        }
                        else
                        {
                            double pricepremdemo = Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) - (premdisval * Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString())) / 100;

                            ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(pricepremdemo));
                            lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(pricepremdemo));
                            hdnPrice.Value = Math.Round(pricepremdemo).ToString();
                            tax = Math.Round(pricepremdemo) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100;
                            hdnTaxAmount.Value = tax.ToString();
                            ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(pricepremdemo + tax));
                            lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(pricepremdemo + tax));
                            // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                            hdnTotal.Value = Math.Round(pricepremdemo + tax).ToString();
                            orderNo += "-Y";
                        }
                    }
                    else
                    {
                        ((Label)ucPurchaseMenu.FindControl("lblexistprod")).Text = string.Format("{0} {1}", "$", "9");
                        ((Label)ucPurchaseMenu.FindControl("lblPrice")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2));
                        if (surveytype == "PPS_PRO")
                        {
                            lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) - 9, 2).ToString());
                            hdnPrice.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) - 9, 2).ToString();
                            tax = Math.Round(Convert.ToDouble((Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) - 9) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100), 2);
                            hdnTaxAmount.Value = tax.ToString();
                            ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) - 9 + tax, 2));
                            lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) - 9 + tax, 2));
                            // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                            hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) - 9, 2) + tax, 2).ToString();
                            orderNo += "-Y";
                        }
                        else
                        {
                            lblPrice.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString());
                            hdnPrice.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString();

                            tax = Math.Round(Convert.ToDouble(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) * Convert.ToDouble(priceDetails.SERVICE_TAX) / 100), 2);
                            hdnTaxAmount.Value = tax.ToString();
                            ((Label)ucPurchaseMenu.FindControl("lblTotal")).Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                            lblTotal.Text = string.Format("{0} {1}", "$", Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2));
                            // hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(159)*54, 2) + tax, 2).ToString();
                            hdnTotal.Value = Math.Round(Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2) + tax, 2).ToString();
                            orderNo += "-Y";
                        }
                    }
                }

            }
        }
        #endregion

        #region btnContinue_Click
        /// <summary>
        /// checks the email id exist or not and disply the payment details.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        public void sendFreetoPROUpgradeEmail(string templatename, string SurveyName)
        {
            var loggedInUser = Session["UserInfo"] as LoggedInUserInfo;
            string viewData1;
            JavaScriptSerializer js1 = new JavaScriptSerializer();
            js1.MaxJsonLength = int.MaxValue;
            clsjsonmandrill prmjson1 = new clsjsonmandrill();
            //  var prmjson1 = new jsonmandrill();
            prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
            prmjson1.name = templatename;
            viewData1 = js1.Serialize(prmjson1);

            string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";


            WebClient request1 = new WebClient();
            request1.Encoding = System.Text.Encoding.UTF8;
            request1.Headers["Content-Type"] = "application/json";
            byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));

            string response1 = System.Text.Encoding.ASCII.GetString(resp1);


            clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);

            string bodycontent = jsmandrill11.code;


            string viewData;
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = int.MaxValue;
            var prmjson = new clsjsonmandrill.jsonmandrillmerge();
            prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
            prmjson.template_name = templatename;

            string strUrl = ConfigurationManager.AppSettings["RootURL"].ToString();
            string OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + loggedInUser.LoginName);

            string unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";


            List<clsjsonmandrill.template_content> mtags = new List<clsjsonmandrill.template_content>();
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content00", content = "<div> Dear *|FNAME|*,</div>" });

            if (templatename == "Active survey upgrade - free to pro single")
            {
                mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div>Your survey *|SURVEY_NAME|* has been upgraded from Free Survey to Pro - Single Survey. </div>" });
            }
            else if (templatename == "Active Survey Upgrade - Free to Premium - Single")
            {
                mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div>Your survey *|SURVEY_NAME|* has been upgraded from Free Survey to Premium - Single Survey. </div>" });
            }
            else if (templatename == "Active Survey Upgrade - Pro - Single to Premium - Single")
            {
                mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div>Your survey *|SURVEY_NAME|* has been upgraded from Pro - Single Survey to Premium - Single Survey. </div>" });
            }


            mtags.Add(new clsjsonmandrill.template_content { name = "std_content02", content = "<div><p style='font-size:8pt;font-family:Arial,Sans-Serif'> *|UNSUBSCRIBEINSIGHTO|* </p></div>" });

            List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
            mvars.Add(new clsjsonmandrill.merge_vars { name = "FNAME", content = loggedInUser.FirstName });
            mvars.Add(new clsjsonmandrill.merge_vars { name = "SURVEY_NAME", content = SurveyName });
            mvars.Add(new clsjsonmandrill.merge_vars { name = "UNSUBSCRIBEINSIGHTO", content = unsubscribe });

            prmjson.template_content = mtags;
            prmjson.merge_vars = mvars;


            viewData = js.Serialize(prmjson);


            string url = "https://mandrillapp.com/api/1.0/templates/render.json";


            WebClient request = new WebClient();
            request.Encoding = System.Text.Encoding.UTF8;
            request.Headers["Content-Type"] = "application/json";
            byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

            string response = System.Text.Encoding.ASCII.GetString(resp);

            string unesc = System.Text.RegularExpressions.Regex.Unescape(response);

            string first4 = unesc.Substring(0, 9);

            string last2 = unesc.Substring(unesc.Length - 2, 2);


            string unesc1 = unesc.Replace(first4, "");
            string unescf = unesc1.Replace(last2, "");


            clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);

            //   string bodycontent = jsmandrill1.code;


            mailMessage = new MailMessage();
            mailMessage.To.Clear();
            mailMessage.Sender = new MailAddress("Takeastart@Insighto.com", "Insighto.com");
            // mailMessage.Sender = new MailAddress(Sendmails.FROM_MAILADDRESS, "Insighto.com");
            mailMessage.From = new MailAddress("Takeastart@Insighto.com", "Insighto.com");
            if (templatename == "Active survey upgrade - free to pro single")
            {
                mailMessage.Subject = "[From Insighto] - Your survey " + SurveyName + " is now upgraded to Pro - Single survey";
            }
            else if (templatename == "Active Survey Upgrade - Free to Premium - Single")
            {
                mailMessage.Subject = "[From Insighto] - Your survey " + SurveyName + " is now upgraded to Premium - Single";
            }
            else if (templatename == "Active Survey Upgrade - Pro - Single to Premium - Single")
            {
                mailMessage.Subject = "[From Insighto] - Your survey " + SurveyName + "is now upgraded to Premium - Single survey";
            }
            mailMessage.To.Add(loggedInUser.LoginName);
            //  mailMessage.Headers.Add("survey_id", "6666");
            // mailMessage.Headers.Add("X-MC-Tags", "survey_respondent");
            mailMessage.Body = unescf;
            //if (Sendmails.SENDER_TOCCADDRESSES != null && Convert.ToString(Sendmails.SENDER_TOCCADDRESSES.Trim()).Length > 0)
            //{
            //    mailMessage.CC.Add(Sendmails.SENDER_TOCCADDRESSES.Trim());
            //}
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.Normal;
            smtpMail = new SmtpClient();
            smtpMail.Host = "smtp.mandrillapp.com";
            smtpMail.Port = 587;
            smtpMail.EnableSsl = true;
            smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpMail.Credentials = new System.Net.NetworkCredential("ram@knowience.com", "T74TJc3EZPYaIcvdBg--Dg");

            try
            {

                smtpMail.Send(mailMessage);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        //  Console.WriteLine("Delivery failed - retrying in 5 seconds.");


                        System.Threading.Thread.Sleep(5000);
                        smtpMail.Send(mailMessage);
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            //catch (System.Net.Mail.SmtpException ex)
            //{
            //    throw ex;
            //}
            mailMessage.Dispose();
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                
                    bool isEmailExist = false;
                    if (userId <= 0)
                    {
                        var userServices = ServiceFactory.GetService<UsersService>();
                        isEmailExist = userServices.IsEmailIdExists(txtUserName.Text);
                    }
                    if (!isEmailExist)
                    {
                        hdnSecret.Value = EncryptHelper.Encrypt(txtPassword.Text);
                        
                        dvErrMsg.Visible = false;
                        lblErrMsg.Visible = false;
                        ulPaymentSummary.Style.Add("display", "block");
                        ulMemberInformation.Style.Add("display", "none");


                        BindPaymentDetails(licenseType);

                        if (ddlCountry.SelectedItem != null)
                        {
                            if (countryName.ToUpper() != "INDIA")
                            {
                                rbtnListPaymentType.Items.FindByValue("2").Enabled = false;
                                rbtnListPaymentType.Items.FindByValue("4").Enabled = false;
                                lblprovalid.Visible = true;
                                lblpromand.Visible = true;
                            }
                            else if ((ddlCountry.SelectedItem.Text.ToUpper() != "INDIA" && licenseType.ToUpper() == "PRO_MONTHLY"))
                            {
                                rbtnListPaymentType.Items.FindByValue("2").Enabled = false;
                                rbtnListPaymentType.Items.FindByValue("4").Enabled = false;
                                lblprovalid.Visible = true;
                                lblpromand.Visible = true;
                            }
                            else if (ddlCountry.SelectedItem.Text.ToUpper() != "INDIA")
                            {
                                rbtnListPaymentType.Items.FindByValue("2").Enabled = false;
                                lblprovalid.Visible = false;
                                lblpromand.Visible = false;
                            }
                            else if (licenseType.ToUpper() == "PRO_MONTHLY")
                            {
                                rbtnListPaymentType.Items.FindByValue("2").Enabled = false;
                                rbtnListPaymentType.Items.FindByValue("4").Enabled = false;
                                lblprovalid.Visible = true;
                                lblpromand.Visible = true;
                            }
                            else
                            {
                                rbtnListPaymentType.Items.FindByValue("2").Enabled = true;
                                rbtnListPaymentType.Items.FindByValue("4").Enabled = true;
                                lblprovalid.Visible = false;
                                lblpromand.Visible = false;
                            }
                        }


                   

                    var userService = ServiceFactory.GetService<UsersService>();
                    var user = GetUserDetails();
                    user = userService.SaveUser(user);

                    PollCreation Pc = new PollCreation();
                    if (user != null)
                    {
                        Pc.UpdateOnlySubscriptionType(1, "survey", user.USERID);
                    }
                    if (user != null)
                    {
                        if (userId == 0)
                        {
                            //creation of subaccount in mandrill
                            string viewData;
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            js.MaxJsonLength = int.MaxValue;
                            clsjsonmandrill cjmd = new clsjsonmandrill();
                            cjmd.key = "T74TJc3EZPYaIcvdBg--Dg";
                            cjmd.id = txtName.Text.Trim() + user.USERID.ToString();
                            viewData = js.Serialize(cjmd);
                            string url = "https://mandrillapp.com/api/1.0/subaccounts/add.json";
                            WebClient request1 = new WebClient();
                            request1.Encoding = System.Text.Encoding.UTF8;
                            request1.Headers["Content-Type"] = "application/json";
                            byte[] resp = request1.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

                            string response = System.Text.Encoding.ASCII.GetString(resp);
                            clsjsonmandrill Deserlcjmd = js.Deserialize<clsjsonmandrill>(response);

                            DataSet dsmsubacc = surcore.updateSubaccount(user.USERID, Deserlcjmd.id.ToString());

                            //end of subaccount creation
                        }

                        dvErrMsg.Visible = false;
                        lblErrMsg.Visible = false;
                        var order = GetOrderInfoDetails(user.USERID);
                        var prevOrderDetails = ServiceFactory<OrderInfoService>.Instance.CheckOrderInfoDetailsExists(order.USERID, order.MODEOFPAYMENT);
                        if (!prevOrderDetails)
                        {
                            order = userService.InsertOrderInfo(order);

                            DataSet dssurvy = surcore.updateSurveyID(surveyID, order.PK_ORDERID);
                          

                            if (order != null)
                            {

                                var history = GetAccountHistoryInfo(user.USERID, order.PK_ORDERID.ToString());
                                var historyId = userService.InsertUserAccountHistory(history);
                                string orderID = orderNo + order.PK_ORDERID;

                                if (countryName.ToUpper() != "INDIA")
                                {
                                    DataSet dsorderdefualtstatus = surcore.updateSaasyDefaultstatus(order.PK_ORDERID);
                                }

                              // DateTime st1 =  DateTime.Now.AddYears(1);
                                if (licenseType.ToUpper() != "PREMIUM_YEARLY")
                                {
                                    DataSet dscredittransaction = surcore.insertCreditTransaction(licenseType, 1, user.USERID, float.Parse(Math.Round(order.TOTAL_PRICE, 2).ToString()), DateTime.Now, DateTime.Now.AddYears(1), "DeclinedRNotCompleted", order.PK_ORDERID, "", 0, "NO", "NO");
                                }      

                                if ((surveyID != 0) && (licenseType == "PPS_PRO"))
                                {
                                    DataSet dssurveyname = surcore.getSurveyType(surveyID);

                                  //  DataSet dsproupdate = surcore.updatesurveyType(user.USERID, licenseType, surveyID, "PRO_YEARLY", status);
                               //     DataSet dsproupdate = surcore.updatesurveyEmailType(user.USERID, licenseType, surveyID, "PRO_YEARLY", status, "upgradetopro",surveytype);
                                    if ((surveytype == "BASIC") && (licenseType == "PPS_PRO"))
                                    {
                                     //   sendFreetoPROUpgradeEmail("Active survey upgrade - free to pro single", dssurveyname.Tables[0].Rows[0][2].ToString());                                                                          

                                    }
                                   
                                }
                                else if ((surveyID != 0) && (licenseType == "PPS_PREMIUM"))
                                {
                                    DataSet dssurveyname = surcore.getSurveyType(surveyID);

                                   // DataSet dsproupdate = surcore.updatesurveyType(user.USERID, licenseType, surveyID, "PREMIUM_YEARLY", status);
                                //    DataSet dsproupdate = surcore.updatesurveyEmailType(user.USERID, licenseType, surveyID, "PREMIUM_YEARLY", status, "upgradetopremium",surveytype);
                                    if ((surveytype == "BASIC") && (licenseType == "PPS_PREMIUM"))
                                    {
                                       // sendFreetoPROUpgradeEmail("Active Survey Upgrade - Free to Premium - Single", dssurveyname.Tables[0].Rows[0][2].ToString());
                                        
                                    }
                                    else if ((surveytype == "PPS_PRO") && (licenseType == "PPS_PREMIUM"))
                                    {
                                      
                                      //  sendFreetoPROUpgradeEmail("Active Survey Upgrade - Pro - Single to Premium - Single", dssurveyname.Tables[0].Rows[0][2].ToString());
                                    }
                                }
                               
                                        string pno = "";
                                       
                                        if (utm_source != "")
                                        {
                                            //if (surveyID != 0)
                                            //{
                                            //    if (surveytype == "PPS_PRO")
                                            //    {

                                            //        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetNonIndianUserCheckoutPageURL(), "fName=" + user.FIRST_NAME + "&Order=" + orderID + "S" + surveyID + "&lName=" + txtLname.Text.Trim() + "&Pno=" + pno + "&emailId=" + user.LOGIN_NAME + "&company=" + txtCompanyName.Text.Trim() + "&subscriptionPlan=pps_upgrade_pro&onpromo=yes");

                                            //        Response.Redirect(strRedirectUrl, false);
                                            //    }
                                            //    else
                                            //    {
                                            //        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetNonIndianUserCheckoutPageURL(), "fName=" + user.FIRST_NAME + "&Order=" + orderID + "S" + surveyID + "&lName=" + txtLname.Text.Trim() + "&Pno=" + pno + "&emailId=" + user.LOGIN_NAME + "&company=" + txtCompanyName.Text.Trim() + "&subscriptionPlan=" + licenseType.ToUpper() + "&onpromo=yes");

                                            //        Response.Redirect(strRedirectUrl, false);
                                            //    }
                                            //}
                                            //else
                                            //{
                                                if (surveytype == "PPS_PRO")
                                                {
                                                    if (countryName.ToUpper() == "INDIA")
                                                    {
                                                       
                                                        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserCheckoutPageURL(), "Userid=" + user.USERID + "&Order=" + orderID + "&Amount=" + hdnTotal.Value + "&Ren=0" + "&BillCName=" + user.FIRST_NAME + "&email=" + user.LOGIN_NAME + "&planchosen=" + licenseType + "&planamount=" + price + "&taxamount=" + hdnTaxAmount.Value);
                                                        Response.Redirect(strRedirectUrl, false);
                                                        
                                                    }
                                                    else
                                                    {
                                                        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetNonIndianUserCheckoutPageURL(), "fName=" + user.FIRST_NAME + "&Order=" + orderID + "&lName=" + txtLname.Text.Trim() + "&Pno=" + pno + "&emailId=" + user.LOGIN_NAME + "&company=" + txtCompanyName.Text.Trim() + "&subscriptionPlan=pps_upgrade_pro&onpromo=yes");

                                                        Response.Redirect(strRedirectUrl, false);
                                                    }
                                                }
                                                else
                                                {
                                                    if (countryName.ToUpper() == "INDIA")
                                                    {
                                                        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserCheckoutPageURL(), "Userid=" + user.USERID + "&Order=" + orderID + "&Amount=" + hdnTotal.Value + "&Ren=0" + "&BillCName=" + user.FIRST_NAME + "&email=" + user.LOGIN_NAME + "&planchosen=" + licenseType + "&planamount=" + price + "&taxamount=" + hdnTaxAmount.Value);
                                                        Response.Redirect(strRedirectUrl, false);
                                                    }
                                                    else
                                                    {
                                                        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetNonIndianUserCheckoutPageURL(), "fName=" + user.FIRST_NAME + "&Order=" + orderID + "&lName=" + txtLname.Text.Trim() + "&Pno=" + pno + "&emailId=" + user.LOGIN_NAME + "&company=" + txtCompanyName.Text.Trim() + "&subscriptionPlan=" + licenseType.ToUpper() + "&onpromo=yes");

                                                        Response.Redirect(strRedirectUrl, false);
                                                    }
                                                }
                                           // }
                                        }
                                        else
                                        {
                                            //if (surveyID != 0)
                                            //{
                                            //    if (surveytype == "PPS_PRO")
                                            //    {
                                            //        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetNonIndianUserCheckoutPageURL(), "fName=" + user.FIRST_NAME + "&Order=" + orderID + "S" + surveyID + "&lName=" + txtLname.Text.Trim() + "&Pno=" + pno + "&emailId=" + user.LOGIN_NAME + "&company=" + txtCompanyName.Text.Trim() + "&subscriptionPlan=pps_upgrade_pro");

                                            //        Response.Redirect(strRedirectUrl, false);
                                            //    }
                                            //    else
                                            //    {
                                            //        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetNonIndianUserCheckoutPageURL(), "fName=" + user.FIRST_NAME + "&Order=" + orderID + "S" + surveyID + "&lName=" + txtLname.Text.Trim() + "&Pno=" + pno + "&emailId=" + user.LOGIN_NAME + "&company=" + txtCompanyName.Text.Trim() + "&subscriptionPlan=" + licenseType.ToUpper());

                                            //        Response.Redirect(strRedirectUrl, false);
                                            //    }
                                            //}
                                            //else
                                            //{
                                                if (surveytype == "PPS_PRO")
                                                {
                                                    if (countryName.ToUpper() == "INDIA")
                                                    {
                                                        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserCheckoutPageURL(), "Userid=" + user.USERID + "&Order=" + orderID + "&Amount=" + hdnTotal.Value + "&Ren=0" + "&BillCName=" + user.FIRST_NAME + "&email=" + user.LOGIN_NAME + "&planchosen=" + licenseType + "&planamount=" + price + "&taxamount=" + hdnTaxAmount.Value);
                                                        Response.Redirect(strRedirectUrl, false);
                                                    }
                                                    else
                                                    {
                                                        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetNonIndianUserCheckoutPageURL(), "fName=" + user.FIRST_NAME + "&Order=" + orderID + "&lName=" + txtLname.Text.Trim() + "&Pno=" + pno + "&emailId=" + user.LOGIN_NAME + "&company=" + txtCompanyName.Text.Trim() + "&subscriptionPlan=pps_upgrade_pro");
                                                        Response.Redirect(strRedirectUrl, false);
                                                    }
                                                }
                                                else
                                                {
                                                    if (countryName.ToUpper() == "INDIA")
                                                    {
                                                        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserCheckoutPageURL(), "Userid=" + user.USERID + "&Order=" + orderID + "&Amount=" + hdnTotal.Value + "&Ren=0" + "&BillCName=" + user.FIRST_NAME + "&email=" + user.LOGIN_NAME + "&planchosen=" + licenseType + "&planamount=" + price + "&taxamount=" + hdnTaxAmount.Value);
                                                        Response.Redirect(strRedirectUrl, false);
                                                    }
                                                    else
                                                    {
                                                        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetNonIndianUserCheckoutPageURL(), "fName=" + user.FIRST_NAME + "&Order=" + orderID + "&lName=" + txtLname.Text.Trim() + "&Pno=" + pno + "&emailId=" + user.LOGIN_NAME + "&company=" + txtCompanyName.Text.Trim() + "&subscriptionPlan=" + licenseType.ToUpper());
                                                        Response.Redirect(strRedirectUrl, false);
                                                    }
                                                }
                                            //}
                                        }
                                  

                            }
                        }
                        else
                        {
                            dvErrMsg.Visible = true;
                            lblErrMsg.Visible = true;
                            lblErrMsg.Text = CommonMessages.AlreadyRegisteredMsg.ToString();
                        }
                    }
                    else
                    {
                        dvErrMsg.Visible = true;
                        lblErrMsg.Visible = true;
                        lblErrMsg.Text = CommonMessages.EmailExistsMsg.ToString();
                    }
                    }
                    else
                    {

                        lblErrMsg.Text = Utilities.ResourceMessage("lblErrMsg");
                        dvErrMsg.Visible = true;
                        lblErrMsg.Visible = true;

                    }
            }
        }
        #endregion


        

        #region btnCreateAccount_Click
        /// <summary>
        /// Inserts  details into users,orderinfo,useraccounthistory tables
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCreateAccount_Click(object sender, EventArgs e)
        {

           
            string emailid = txtUserName.Text.Trim();

            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);
            strconn.Open();
            SqlCommand scom = new SqlCommand("sp_getpassword", strconn);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@login_name", SqlDbType.VarChar)).Value = emailid;
           
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet pricedetails = new DataSet();
            sda.Fill(pricedetails);
            string passwrd="";
            if (pricedetails.Tables[0].Rows.Count > 0)
            {
                 passwrd = pricedetails.Tables[0].Rows[0][0].ToString();
            }

            if (txtName.Text == "")
            {
                txtName.Focus();
                dvErrMsg.Visible = true;
                lblErrMsg.Visible = true;
                lblErrMsg.Text = Utilities.ResourceMessage("lblReqNameResource1.Text");
                if (rbtnOrganization.Checked)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('show')", true);
                }
                else
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('hide')", true);
                }


            }
            else if (txtUserName.Text == ""  )
            {
                txtUserName.Focus();
                dvErrMsg.Visible = true;
                lblErrMsg.Visible = true;
                lblErrMsg.Text = Utilities.ResourceMessage("lblReqUNameResource.Text");
                if (rbtnOrganization.Checked)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('show')", true);
                }
                else
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('hide')", true);
                }

             

            }

            else if (txtPassword.Text == "" && passwrd=="")
            {
                txtPassword.Focus();
                dvErrMsg.Visible = true;
                lblErrMsg.Visible = true;
                lblErrMsg.Text = Utilities.ResourceMessage("lblReqPasswordResource1.Text");
                if (rbtnOrganization.Checked)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('show')", true);
                }
                else
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('hide')", true);
                }
                               
            }
            else if (txtConfirmPassword.Text == "" && passwrd=="")
            {
                txtConfirmPassword.Focus();
                dvErrMsg.Visible = true;
                lblErrMsg.Visible = true;
                lblErrMsg.Text = Utilities.ResourceMessage("lblReqConfirmPasswordResource1.Text");
                if (rbtnOrganization.Checked)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('show')", true);
                }
                else
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('hide')", true);
                }

            }
            else if (txtCountryCode.Text == "")
            {
                txtCountryCode.Focus();
                dvErrMsg.Visible = true;
                lblErrMsg.Visible = true;
                lblErrMsg.Text = Utilities.ResourceMessage("lblReqCountryCodeResource1.Text");
                if (rbtnOrganization.Checked)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('show')", true);
                }
                else
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('hide')", true);
                }

            }
            else if (txtCity.Text == "")
            {
                txtCity.Focus();
                dvErrMsg.Visible = true;
                lblErrMsg.Visible = true;
                lblErrMsg.Text = Utilities.ResourceMessage("lblReqCityResource1.Text");
                if (rbtnOrganization.Checked)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('show')", true);
                }
                else
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('hide')", true);
                }

            }
            else if (ddlCountry.SelectedValue == "")
            {
                ddlCountry.Focus();
                dvErrMsg.Visible = true;
                lblErrMsg.Visible = true;
                lblErrMsg.Text = Utilities.ResourceMessage("lblReqCountryResource1.Text");
                if (rbtnOrganization.Checked)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('show')", true);
                }
                else
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('hide')", true);
                }

            } 
            else
            {

                if (ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession() != null)
                {
                    if (txtCaptcha.Text == ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession())
                    {
                        bool isEmailExist = false;
                        if (userId <= 0)
                        {
                            var userServices = ServiceFactory.GetService<UsersService>();
                            isEmailExist = userServices.IsEmailIdExists(txtUserName.Text);
                        }
                        if (!isEmailExist)
                        {


                            hdnSecret.Value = EncryptHelper.Encrypt(txtPassword.Text);
                            dvErrMsg.Visible = false;
                            lblErrMsg.Visible = false;
                            ulPaymentSummary.Style.Add("display", "block");
                            ulMemberInformation.Style.Add("display", "none");
                            BindPaymentDetails(licenseType);

                            if (ddlCountry.SelectedItem != null)
                            {
                                if (countryName.ToUpper() != "INDIA")
                                {
                                    rbtnListPaymentType.Items.FindByValue("2").Enabled = false;
                                    rbtnListPaymentType.Items.FindByValue("4").Enabled = false;
                                    lblprovalid.Visible = true;
                                    lblpromand.Visible = true;
                                }
                                else if ((ddlCountry.SelectedItem.Text.ToUpper() != "INDIA" && licenseType.ToUpper() == "PRO_MONTHLY"))
                                {
                                    rbtnListPaymentType.Items.FindByValue("2").Enabled = false;
                                    rbtnListPaymentType.Items.FindByValue("4").Enabled = false;
                                    lblprovalid.Visible = true;
                                    lblpromand.Visible = true;
                                }
                                else if (ddlCountry.SelectedItem.Text.ToUpper() != "INDIA")
                                {
                                    rbtnListPaymentType.Items.FindByValue("2").Enabled = false;
                                    lblprovalid.Visible = false;
                                    lblpromand.Visible = false;
                                }
                                else if (licenseType.ToUpper() == "PRO_MONTHLY")
                                {
                                    rbtnListPaymentType.Items.FindByValue("2").Enabled = false;
                                    rbtnListPaymentType.Items.FindByValue("4").Enabled = false;

                                    lblprovalid.Visible = true;
                                    lblpromand.Visible = true;
                                }
                                else
                                {
                                    rbtnListPaymentType.Items.FindByValue("2").Enabled = true;
                                    rbtnListPaymentType.Items.FindByValue("4").Enabled = true;

                                    lblprovalid.Visible = false;
                                    lblpromand.Visible = false;
                                }
                            }


                        }
                        else
                        {
                            lblErrMsg.Text = Utilities.ResourceMessage("lblErrMsg");
                            dvErrMsg.Visible = true;
                            lblErrMsg.Visible = true;
                        }
                   

                    var userService = ServiceFactory.GetService<UsersService>();
                    var user = GetUserDetails();
                    user = userService.SaveUser(user);



                    if (user != null)
                    {
                        dvErrMsg.Visible = false;
                        lblErrMsg.Visible = false;
                        var order = GetOrderInfoDetails(user.USERID);

                        DataSet dsRefby = surcore.updateReferredby(user.USERID, utm_source, utm_term);

                        var prevOrderDetails = ServiceFactory<OrderInfoService>.Instance.CheckOrderInfoDetailsExists(order.USERID, order.MODEOFPAYMENT);
                        if (!prevOrderDetails)
                        {
                            order = userService.InsertOrderInfo(order);
                            if (order != null)
                            {
                                var history = GetAccountHistoryInfo(user.USERID, order.PK_ORDERID.ToString());
                                var historyId = userService.InsertUserAccountHistory(history);
                                string orderID = orderNo + order.PK_ORDERID;
                                if (rbtnListPaymentType.SelectedItem.Value == "1" || rbtnListPaymentType.SelectedItem.Value == "3" || rbtnListPaymentType.SelectedItem.Value == "5")
                                {
                                    if (countryName.ToUpper() == "INDIA")
                                    {

                                        string txtphvar = txtCountryCode.Text + txtAreaCode.Text + txtPhno.Text;
                                        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserCheckoutPageURL(), "Userid=" + user.USERID + "&Order=" + orderID + "&Amount=" + hdnTotal.Value + "&Ren=0" + "&BillCName=" + txtName.Text + "&email=" + txtUserName.Text + "&City=" + txtCity.Text + "&country=" + ddlCountry.SelectedValue + "&phoneno=" + txtphvar);
                                        Response.Redirect(strRedirectUrl, false);
                                    }
                                    else
                                    {
                                        string pno = "";
                                        if (txtCountryCode.Text.Trim() != string.Empty)
                                        {
                                            pno = txtCountryCode.Text.Trim();
                                        }
                                        if (txtAreaCode.Text.Trim() != string.Empty)
                                        {
                                            pno += txtAreaCode.Text.Trim();
                                        }
                                        pno += user.PHONE.Trim();

                                        //string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserCheckoutPageURL(), "Userid=" + user.USERID + "&Order=" + orderID + "&Amount=" + hdnTotal.Value + "&Ren=0");
                                        //Response.Redirect(strRedirectUrl, false);

                                        //string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetNonIndianUserCheckoutPageURL(), "fName=" + user.FIRST_NAME + "&Order=" + orderID + "&lName=" + txtLname.Text.Trim() + "&Pno=" + pno + "&emailId=" + user.LOGIN_NAME + "&company=" + txtCompanyName.Text.Trim() + "&subscriptionPlan=" + user.LICENSE_OPTED.ToUpper());

                                        string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetNonIndianUserCheckoutPageURL(), "fName=" + user.FIRST_NAME + "&Order=" + orderID + "&lName=" + txtLname.Text.Trim() + "&Pno=" + pno + "&emailId=" + user.LOGIN_NAME + "&company=" + txtCompanyName.Text.Trim() + "&subscriptionPlan=" + user.LICENSE_OPTED.ToUpper());
                                        Response.Redirect(strRedirectUrl, false);
                                    }
                                }
                                else if (rbtnListPaymentType.SelectedItem.Value == "2" || rbtnListPaymentType.SelectedItem.Value == "4")
                                {
                                    string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserInvoiceConfirmationURL(), "ord_id=" + order.PK_ORDERID + "&Usrid=" + user.USERID + "&invoice_id=" + orderID + "&Ren=0");
                                    Response.Redirect(strRedirectUrl, false);
                                }

                            }
                        }
                        else
                        {
                            dvErrMsg.Visible = true;
                            lblErrMsg.Visible = true;
                            lblErrMsg.Text = CommonMessages.AlreadyRegisteredMsg.ToString();
                        }
                    }
                    else
                    {
                        dvErrMsg.Visible = true;
                        lblErrMsg.Visible = true;
                        lblErrMsg.Text = CommonMessages.EmailExistsMsg.ToString();
                    }
                    }
                    else
                    {
                        txtCaptcha.Focus();
                        dvErrMsg.Visible = true;
                        lblErrMsg.Text = Utilities.ResourceMessage("lblCaptchaErrMsg");
                        lblErrMsg.Visible = true;
                        if (rbtnOrganization.Checked)
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('show')", true);
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('hide')", true);
                        }

                    }
                }
                else
                {
                    txtCaptcha.Focus();
                    lblErrMsg.Text = Utilities.ResourceMessage("lblCaptchaErrMsg");
                    if (rbtnOrganization.Checked)
                    {
                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('show')", true);
                    }
                    else
                    {
                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "showorganization", "ShowHide('hide')", true);
                    }

                    lblErrMsg.Visible = true;
                }
            }
        }
        #endregion

        #region GetUserdetails
        public osm_user GetUserDetails()
        {
            
            osm_user user = new osm_user();
            
            user.USERID = userId;
            user.PASSWORD = hdnSecret.Value;
            user.LOGIN_NAME = txtUserName.Text.Trim();
            user.FIRST_NAME = txtName.Text.Trim();
            user.LAST_NAME = txtLname.Text.Trim();
            user.ADDRESS_LINE1 = "";
            user.ADDRESS_LINE2 = "";
            user.CITY = txtCity.Text.Trim();
            user.STATE = "";
            user.COUNTRY = ddlCountry.SelectedItem.Text;
            user.COMPANY = "";
            user.WORK_INDUSTRY = "";
            user.JOB_FUNCTION = "";
            user.DEPT = "";
            user.EMAIL = txtUserName.Text.Trim();
            user.LICENSE_TYPE = "FREE";
            user.STATUS = "Active";            
         //   user.LICENSE_EXPIRY_DATE = DateTime.UtcNow;
            user.LICENSE_EXPIRY_DATE = Convert.ToDateTime("1/1/1800");
            if ((hdnType.Value == "PPS_PRO") || (hdnType.Value == "PPS_PREMIUM"))
            {
                user.LICENSE_OPTED = "FREE";
            }
            else
            {
                user.LICENSE_OPTED = hdnType.Value;
            }
            user.ADMINID = 0;
            user.APPROVAL_FLAG = 0;
            user.UPDATES_FLAG = 0;
            user.CREATED_ON = DateTime.UtcNow;
            user.MODIFIED_ON = DateTime.UtcNow;
            user.DELETED = 0;
            user.EMAIL_FLAG = 0;
            if (rbtnIndividual.Checked)
            {
                user.USER_TYPE = 1;
                user.USER_COMPANY = "";
                user.USER_DESIGNATION = "";
            }
            else
            {
                user.USER_TYPE = 2;
                user.USER_COMPANY = txtOrganization.Text.Trim();
                user.USER_DESIGNATION = txtDesignation.Text.Trim();
            }
            user.PHONE = txtCountryCode.Text.Trim() + Constants.PHONE_SEPARATER + txtAreaCode.Text.Trim() + Constants.PHONE_SEPARATER + txtPhno.Text.Trim();
            user.PAYMENT_TYPE = ValidationHelper.GetInteger(rbtnListPaymentType.SelectedValue, 0);
            user.LICENSE_UPGRADE = 0;
            user.RENEWAL_FLAG = 0;
            user.POSTAL_CODE = "0";
            user.COMPANY_SIZE = "";
            user.RESET = 1;
            user.MAIL_REMINDER_FLAG = 0;
            user.MAIL_REMINDER_DATE = DateTime.UtcNow;
            user.QUICKUSERGUIDE_FLAG = 0;
            user.OPTOUT_STATUS = "";
            user.ACCOUNT_TYPE = "Master";
            user.AUTHLINK_FLAG = 0;
            user.LICENSE_CHANGEFLAG = 0;
            user.EMAIL_COUNT = 0;

            if (hdnType.Value == "PREMIUM_YEARLY")
            {
                user.WALLETPRICEPLAN = "PREMIUM";
            }
            else if ((hdnType.Value == "PPS_PRO") || (hdnType.Value == "PPS_PREMIUM"))
            {
                user.WALLETPRICEPLAN = "PPS";
            }
            else
            {
                user.WALLETPRICEPLAN = "PRO";
            }
            user.STANDARD_BIAS = GetBIASValue();
            string ipAddress = string.Empty;
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
            user.IP_ADDRESS = ipAddress;
            user.SURVEY_FLAG = 0;
            return user;
        }
        #endregion

        #region GetOrderInfoDetails
        public osm_orderinfo GetOrderInfoDetails(int userId)
        {


            orderNo = hdnOrderNo.Value;
            osm_orderinfo order = new osm_orderinfo();
            if (DateTime.UtcNow.Month < 10)
                orderNo += "0" + DateTime.UtcNow.Month;
            else
                orderNo += DateTime.UtcNow.Month;
            orderNo += Convert.ToString(DateTime.UtcNow.Year).Substring(2);
            order.TAX_PERCENT = ValidationHelper.GetDouble(hdnTax.Value, 0);
            order.TAX_PRICE = ValidationHelper.GetDouble(hdnTaxAmount.Value, 0);
            order.TOTAL_PRICE = ValidationHelper.GetDouble(hdnTotal.Value, 0);
            order.USERID = userId;
            order.PRODUCT_PRICE = ValidationHelper.GetDouble(hdnPrice.Value, 0);
            order.MODEOFPAYMENT = ValidationHelper.GetInteger(rbtnListPaymentType.SelectedValue, 0);
            order.ORDERID = orderNo;
            if (hdnUpgrade.Value == "New")
            {
                order.ORDER_TYPE = Constants.ORDERTYPE;
            }
            else if (hdnUpgrade.Value == "Upgrade")
            {
                order.ORDER_TYPE = Constants.UPGRADEORDERTYPE;
            }
            else if (hdnUpgrade.Value == "Renew")
            {
                order.ORDER_TYPE = Constants.RENEWORDERTYPE;
            }
            order.CREATED_DATE = DateTime.UtcNow;
            order.PAID_DATE = DateTime.UtcNow;
            order.TRANSACTION_STATUS = 0;
           // DataSet dsprice = surcore.getPriceLicenseCountry(countryName, licenseType);
           // order.CurrencyType = "INR"; // new RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name).ISOCurrencySymbol;
            if (countryName.ToUpper() == "INDIA")
            {
               order.CurrencyType = "INR";
               // order.CurrencyType = "USD";
            }
            else
            {
                order.CurrencyType = "USD";
            }
            //order.CurrencyType =  new RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name).ISOCurrencySymbol;
            order.ModifiedDate = DateTime.UtcNow;
            //var currency = new CurrencyServiceClient().GetConversionRate(CurrencyCode.USD, CurrencyCode.INR);
            order.ExchangeRate = 0;// currency.Rate;
            return order;
        }
        #endregion

        #region GetAccountHistoryInfo
        /// <summary>
        /// fills data into user account history object and returns user acc 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public osm_useraccounthistory GetAccountHistoryInfo(int userId, string orderId)
        {
            osm_useraccounthistory history = new osm_useraccounthistory();
            history.USERID = userId;
            history.ORDERID = orderId;
            if (rbtnListPaymentType.SelectedValue == Constants.PAYBYCHEQUE)
            {
                history.EMAIL_FLAG = 1;
            }
            else
            {
                history.EMAIL_FLAG = 0;
            }
            history.CREATED_DATE = DateTime.UtcNow;
            history.CURRENT_LICENSETYPE = hdnType.Value;
            history.LICENSETYPE_OPTED = hdnType.Value;
            return history;
        }
        #endregion

        private int GetBIASValue()
        {           
            string timeZ = drpTimeZone.SelectedValue;
            string[] totalString = timeZ.Split(',');
            string[] tempTime = totalString[0].Split(':');
            string sign = tempTime[0].Substring(0, 1);
            int hours = 0, minutes = 0;
            if (sign != "0")
            {
                hours = ValidationHelper.GetInteger(tempTime[0].Substring(1, (tempTime.Length)), 0) * 60;
                minutes = ValidationHelper.GetInteger(tempTime[1].ToString(), 0);
                minutes = hours + minutes;
                if (sign == "-")
                {
                    minutes = -(minutes);
                }
            }
            return minutes;

        }
        #endregion

    }
}