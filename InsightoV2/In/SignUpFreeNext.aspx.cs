﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using System.Collections;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using CovalenseUtilities.Helpers;

namespace Insighto.Pages.In
{
    public partial class In_SignUpFreeNext : System.Web.UI.Page
    {
        Hashtable ht = new Hashtable();
        int userId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht.Contains("userId"))
                {
                    userId=ValidationHelper.GetInteger(ht["userId"].ToString(),0);
                    var user = ServiceFactory<UsersService>.Instance.GetUserDetails(userId).FirstOrDefault();
                    ServiceFactory.GetService<SessionStateService>().SaveUserToSession(user);
                }
            }
        }
        protected void btnContinue_Click(object sender, ImageClickEventArgs e)
        {
           // Response.Redirect("~/Home.aspx");
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=0"));
        }
    }
}