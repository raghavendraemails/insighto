﻿<%@ Page Title="Online Survey Tool: About us - Insighto" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true"
    CodeFile="aboutus.aspx.cs" Inherits="In_aboutus" %>

<%@ Register TagPrefix="uc" TagName="Advertizement" Src="~/In/UserControls/RightAdvertizement.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<meta name="description" content="Insighto.com, a world-class online survey software is the makes it very easy for companies to gain insights by reaching out to their audiences through a survey." />
<meta name="keywords" content="online survey software, survey software, online survey tool"/>
  <table border="0" width="100%">
    <tr>
        <td valign="top"  width="100%">
        <div class="marginNewInnerPanel">
        <div class="whyInsightoPnl">
         <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="About Us" 
                                meta:resourcekey="lblTitleResource1"></asp:Label>
                        </h2>
                    </td>                    
                    
                </tr>
            </table>
        <!-- content panel -->
        <div class="defaultHeight"></div>
        
        <!-- contact form -->
        <div class="text content" style="padding:10px 20px;" id="divContent" runat="server">       
        </div>
        
        <!-- contact form -->
       </div>
        <!-- //content panel -->
        </div>
        
        </td>        
       <%-- <td valign="top" style="padding-top: 15px;">
        <uc:Advertizement ID="ucAdvertizement" runat="server" />
        </td>--%>
    </tr>
  </table>
    
</asp:Content>
