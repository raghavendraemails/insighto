﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true"
    CodeFile="CraftingServices.aspx.cs" Inherits="Insighto.Pages.In.CraftingServices"%>

<%@ Register TagPrefix="uc" TagName="Advertizement" Src="~/In/UserControls/RightAdvertizement.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0">
        <tr>
            <td>
                <div class="marginNewInnerPanel">
                <div class="benefitBannerPanel">
                            <!-- Wallet benefits panel -->
                            <div class="benefitBannerTop">
                            </div>
                            <h1> <asp:Label ID="lblIntro" runat="server" 
                                    Text="Presenting Insighto Survey Crafting service. <br /> Give your survey the professional edge. Sign up now!" 
                                    meta:resourcekey="lblIntroResource1"></asp:Label>
                               </h1>
                   
                            <div class="benefitBannerText craftHeight">
                                <div class="benefitPointsPanel" style="width: 98%;">
                                    <ul>
                                        <li><asp:Label ID="lblBenefits1" runat="server" 
                                                Text="Your survey crafted by an expert with over 8 years of survey crafting experience" 
                                                meta:resourcekey="lblBenefits1Resource1"></asp:Label></li>
                                        <li><asp:Label ID="lblBenefits2" runat="server" 
                                                Text="Professional looking survey, with your logo branding" 
                                                meta:resourcekey="lblBenefits2Resource1"></asp:Label></li>
                                        <li><asp:Label ID="lblBenefits3" runat="server" 
                                                Text="Get free Insighto Pro subscription with your purchase" 
                                                meta:resourcekey="lblBenefits3Resource1"></asp:Label></li>
                                    </ul>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <!-- //Wallet benefits panel -->
                        </div>
                        <div class="clear"></div>
                    <div class="signUpPnl">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <h3>
                                        <asp:Label ID="lblTitle" runat="server" Text="Crafting Services" 
                                            meta:resourcekey="lblTitleResource1"></asp:Label>
                                    </h3>
                                </td>
                                <td class="signbg">
                                    <asp:Label ID="lblAlreadyAccnt" runat="server" Text="Already have an Account?" 
                                        meta:resourcekey="lblAlreadyAccntResource1"></asp:Label>   
                                    <b><asp:HyperLink ID="hlnkHome" runat="server" class="orangeColor" 
                                        NavigateUrl="~/Home.aspx" Text="Sign in" meta:resourcekey="hlnkHomeResource1"></asp:HyperLink></b>
                                </td>
                               
                            </tr>
                        </table>
                       
                      <div class="text">
                        <div class="mandatoryPanel">
                            
                            <asp:Label ID="lblMandatory" runat="server" Text="indicates required fields" 
                                meta:resourcekey="lblMandatoryResource1"></asp:Label>
                        </div>
                        
                            <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                                <div>
                                    <asp:Label ID="lblErrMsg" runat="server" CssClass="errorMesg" Visible="False" 
                                        meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
                            </div>
                            <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                                <div>
                                    <asp:Label ID="lblSuccMsg" runat="server" Visible="False" 
                                        meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
                            </div>
                       
                        <div class="con_login_pnl">
                            <div class="loginlbl">
                                <asp:Label ID="lblName" runat="server" Text="Name" CssClass="requirelbl" 
                                    meta:resourcekey="lblNameResource1" />
                            </div>
                            <div class="loginControls">
                                <asp:TextBox ID="txtName" runat="server" CssClass="textBoxMedium" MaxLength="50"
                                    TabIndex="1" meta:resourcekey="txtNameResource1"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqUserName" 
                                    Display="Dynamic" ControlToValidate="txtName"
                                    runat="server" CssClass="lblRequired" ErrorMessage="Please enter name." 
                                    meta:resourcekey="reqUserNameResource1"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="con_login_pnl">
                            <div class="loginlbl">
                                <asp:Label ID="Label1" runat="server" Text="Email" CssClass="requirelbl" 
                                    meta:resourcekey="Label1Resource1" />
                            </div>
                            <div class="loginControls">
                                <asp:TextBox ID="txtMail" runat="server" CssClass="textBoxMedium" MaxLength="50"
                                    TabIndex="2" meta:resourcekey="txtMailResource1"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqMail" 
                                    ControlToValidate="txtMail" runat="server"
                                    Display="Dynamic" ErrorMessage="Please enter email." 
                                    CssClass="lblRequired" meta:resourcekey="reqMailResource1"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator SetFocusOnError="True"  ID="rexUsername" 
                                    CssClass="lblRequired" ControlToValidate="txtMail"
                                    runat="server" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ErrorMessage="Please enter valid email." 
                                    meta:resourcekey="rexUsernameResource1"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="con_login_pnl">
                            <div class="loginlbl">
                                <asp:Label ID="Label4" runat="server" Text="Organisation" 
                                    meta:resourcekey="Label4Resource1" />
                            </div>
                            <div class="loginControls">
                                <asp:TextBox ID="txtOraganization" runat="server" MaxLength="30" CssClass="textBoxMedium"
                                    TabIndex="3" meta:resourcekey="txtOraganizationResource1"></asp:TextBox>
                            </div>
                        </div>
                        <div class="con_login_pnl">
                            <div class="loginlbl">
                                <asp:Label ID="Label2" runat="server" Text="Phone number to contact" 
                                    CssClass="requirelbl" meta:resourcekey="Label2Resource1" />
                            </div>
                            <div class="loginControls">
                                <asp:TextBox ID="txtPhno" runat="server" MaxLength="10" 
                                    onkeypress="javascript:return onlyNumbers(event,'');" CssClass="phoneNum_long"
                                    onpaste="return false;" TabIndex="4" meta:resourcekey="txtPhnoResource1"></asp:TextBox>
                                <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqPhNo" runat="server" ControlToValidate="txtPhno"
                                    ErrorMessage="Please enter phone number." CssClass="lblRequired" 
                                    meta:resourcekey="reqPhNoResource1"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="con_login_pnl">
                            <div class="loginlbl">
                                <asp:Label ID="Label5" runat="server" Text="Message" 
                                    meta:resourcekey="Label5Resource1" />
                            </div>
                            <div class="loginControls">
                                <asp:TextBox ID="txtMessage" TextMode="MultiLine" CssClass="textAreaMedium" runat="server"
                                    TabIndex="5" meta:resourcekey="txtMessageResource1" />
                            </div>
                        </div>
                        <div class="con_login_pnl">
                            <div class="loginlbl">
                                <asp:Label ID="Label3" runat="server" AssociatedControlID="reCaptchaimg" 
                                    meta:resourcekey="Label3Resource1" />
                            </div>
                            <div class="loginControls">
                                <img src="~/In/Captcha.aspx" id="reCaptchaimg" alt="Catptcha Text" runat="server" />
                            </div>
                        </div>
                        <div class="con_login_pnl">
                            <div class="loginlbl">
                            </div>
                            <div class="loginControls">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCaptcha" runat="server" Text="Type the above number" 
                                                meta:resourcekey="lblCaptchaResource1"></asp:Label>
                                            <asp:Label ID="lblRecaptcha" runat="server" 
                                                meta:resourcekey="lblRecaptchaResource1" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCaptcha" CssClass="aspxtextbox textBoxSmall" runat="server" 
                                                TabIndex="6" meta:resourcekey="txtCaptchaResource1" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="con_login_pnl">
                            <div class="loginlbl">
                            </div>
                            <div class="loginControls">
                                <div class="top_padding">
                                    <asp:Button ID="btnsubmit" runat="server" CssClass="btn_small_65" ToolTip="Submit"
                                        Text="Submit" OnClick="btnsubmit_Click" TabIndex="7" 
                                        meta:resourcekey="btnsubmitResource1" /></div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </td>
          
            <td valign="top" style="padding-top: 15px;">
                <uc:Advertizement ID="ucAdvertizement" runat="server" />
            </td>
        </tr>
    </table>
    <div class="clear">
    </div>
    <script type="text/javascript">
        function onlyNumbers(e, Type) {
            var key = window.event ? e.keyCode : e.which;
            if (Type == 'Countrycode' && key == 43) {
                return true;
            }
            else if (key > 31 && (key < 48 || key > 57)) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
