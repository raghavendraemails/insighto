﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using Insighto.Data;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;

public partial class In_paypersurveycheckout : System.Web.UI.Page
{
    private string subscription = "";
    private string promotion = "";
    SurveyCore surcore = new SurveyCore();
    protected void Page_Load(object sender, EventArgs e)
    {
        string ipAddress = "";
        var country = "";
        if (!IsPostBack)
        {
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            //ipAddress = "125.236.193.250";
            country = Utilities.GetCountryName(ipAddress);
        }

        if (Request.QueryString["key"] != null)
        {
            Hashtable ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
           

            if (ht != null && ht.Count > 0 && ht.Contains("subscriptionPlan"))
            {
                subscription = ht["subscriptionPlan"].ToString();
            }
            if (ht != null && ht.Count > 0 && ht.Contains("onpromo"))
            {
                promotion = ht["onpromo"].ToString();
            }


            DataSet dsprice = surcore.getPriceLicenseCountry(country, subscription);

            if (promotion == "yes")
            {
                if (subscription == "PRO_YEARLY")
                {
                    
                    product_1_path.Value = "/" + dsprice.Tables[0].Rows[0][2].ToString() + "promo30";
                }
                else if (subscription == "PREMIUM_YEARLY")
                {
                    product_1_path.Value = "/" + dsprice.Tables[0].Rows[0][2].ToString() + "promo40";
                }

            }
            else
            {
                product_1_path.Value = "/" + dsprice.Tables[0].Rows[0][2].ToString();
            }

            //if (subscription == "PRO_MONTHLY")
            //{

            //    product_1_path.Value = "/insighto_monthly";
            //}
            //else if (subscription == "PRO_YEARLY")
            //{

            //    product_1_path.Value = "/insighto_yearly";
            //}
            //else if (subscription == "PREMIUM_YEARLY")
            //{

            //    product_1_path.Value = "/Insighto_Premium_Yearly";
            //}
        }
        
    }
}