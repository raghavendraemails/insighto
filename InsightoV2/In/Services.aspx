﻿<%@ Page Title="Online Survey Software Tool - Insighto" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true"
    CodeFile="Services.aspx.cs" Inherits="In_Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<meta name="description" content="Insighto offers solutions to create and conduct web surveys online, which get the feedback you need and help garner maximum responses." />
  <meta name="keywords" content=" online survey software tool, survey software, conduct surveys online" />
   <div class="successPanel" id="divSuccess" style="display: none" runat="server">
            <div>
                <%-- <label id="lblMessage" />--%>
                <asp:Label ID="lblMessage" CssClass="successMsg" runat="server" 
                    Style="display: none;" meta:resourcekey="lblMessageResource1"></asp:Label>
            </div>
        </div>
    <div>
        <div class="innerContentLeft">
            <!-- left content panel -->
            <div class="innerTextPanel">
                <h1>
                    Insighto Survey Solutions</h1>
                <h3>
                    It’s a better way to do surveys.</h3>
                <p>
                    &nbsp;</p>
                <p>
                    Insighto offers professional assistance to create and deploy surveys, which get
                    the feedback you need. With years of experience in Industry, our survey professionals
                    have gained technical expertise in survey creation, deployment and reporting. Using
                    best practices, they help you design and develop surveys that will garner maximum
                    responses. Subsequently, our reporting tools will facilitate in-depth analysis resulting
                    in insights that matter.
                </p>
                <p>
                    &nbsp;</p>
                <p>
                    Our range of services includes:</p>
                <p>
                    &nbsp;</p>
                <div class="textPanelHalf">
                    <h2>
                        Survey Creation</h2>
                    <p>
                        Send us your questions and let our experts develop a professional survey. Our survey
                        professionals work closely with you to develop, review and test your surveys before
                        deployment.</p>
                    <p>
                        &nbsp;</p>
                    <h2>
                        Survey Reports</h2>
                    <p>
                        Upon data collection, our team can drive the analytics and present specialized reports
                        that will make the survey worth more than you had imagined.</p>
                    <p>
                        &nbsp;</p>
                    <h2>
                        Customized Branding</h2>
                    <p>
                        If you require more than a professional survey, we have a solution for you too.
                        Our design team can work with you to brand your survey pages as per your requirement
                        and integrate them seamlessly so you can leverage on your brand for your survey
                        or vice-versa.</p>
                    <p>
                        &nbsp;</p>
                </div>
                <div class="textPanelHalf">
                    <h2>
                        Survey Deployment</h2>
                    <p>
                        After your survey has been designed and developed, we can assist you in deploying
                        the survey. Our delivery team helps you design email invites with survey links and
                        reach your target database.</p>
                    <p>
                        &nbsp;</p>
                    <h2>
                        Survey Project Management</h2>
                    <p>
                        Our team of survey professionals engages in totality to create powerful surveys.
                        They work closely with you from start to finish i.e., from survey creation, deployment,
                        tracking responses, generating real-time reports, to deliver insightful results.</p>
                    <p>
                        &nbsp;</p>
                </div>
            </div>
            <!-- //left content panel -->
        </div>
        <div class="innerContentRight">
            <!-- right content panel -->
            <div class="serviceFormPanel">
                <h2>
                    Get in touch with us</h2>
                <p>
                    To get your surveys done quickly and professionally initiate dialogue with our survey
                    experts today. It’s a smarter way to do your survey.</p>
                <p>
                    &nbsp;</p>
                <ul class="serviceUL">
                    <li><b>Name :</b></li>
                    <li>
                        <asp:TextBox ID="txtName" runat="server" CssClass="textBoxService" MaxLength="50"></asp:TextBox></li>
                    <li>
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" SetFocusOnError="true" Display="Dynamic"
                            ErrorMessage="Please enter name." ControlToValidate="txtName" CssClass="lblRequired"></asp:RequiredFieldValidator>
                    </li>
                    <li>Do you have subscription to Insighto.com?</li>
                    <li>
                        <%-- <asp:RadioButton ID="rbtnYes" runat="server"/> Yes
                   <asp:RadioButton ID="rbtnNo" runat="server"/> No--%>
                        <asp:RadioButtonList ID="rbtnListSubscription" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                        </asp:RadioButtonList>
                    </li>
                    <li>
                        <asp:RequiredFieldValidator ID="refSubscription" runat="server" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="Please select yes/no." ControlToValidate="rbtnListSubscription"
                            CssClass="lblRequired"></asp:RequiredFieldValidator>
                    </li>
                    <li><b>Email Id :</b></li>
                    <li>
                        <asp:TextBox ID="txtEmailId" runat="server" CssClass="textBoxService" MaxLength="200"></asp:TextBox></li>
                    <li>
                        <asp:RequiredFieldValidator ID="rfvEmailId" runat="server" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="Please enter email id." ControlToValidate="txtEmailId"
                            CssClass="lblRequired"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="regEmailId" runat="server"
                            ErrorMessage="Please enter valid email id." ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                            CssClass="lblRequired" ControlToValidate="txtEmailId" Display="Dynamic">
                        </asp:RegularExpressionValidator>
                    </li>
                    <li><b>Phone No :</b></li>
                    <li>
                        <asp:TextBox ID="txtPhoneNo" runat="server" CssClass="textBoxService" MaxLength="20"></asp:TextBox></li>
                    <li>
                        <asp:RequiredFieldValidator ID="rfvPhoneno" runat="server" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="Please enter phone no." ControlToValidate="txtPhoneNo"
                            CssClass="lblRequired"></asp:RequiredFieldValidator>
                    </li>
                    <li><b>Service required :</b></li>
                    <li>
                        <asp:DropDownList ID="ddlService" runat="server" CssClass="dropdownService">
                            <asp:ListItem Value="">Choose any service</asp:ListItem>
                            <asp:ListItem>Survey Creation</asp:ListItem>
                            <asp:ListItem>Survey Reports</asp:ListItem>
                            <asp:ListItem>Customized Branding</asp:ListItem>
                            <asp:ListItem>Survey Deployment</asp:ListItem>
                            <asp:ListItem>Survey Project Management</asp:ListItem>
                        </asp:DropDownList>
                    </li>
                    <li>
                        <asp:RequiredFieldValidator ID="rfvService" runat="server" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="Please select service." ControlToValidate="ddlService"
                            CssClass="lblRequired"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="dynamicButton" OnClick="btnSubmit_Click" />
                    </li>
                </ul>
            </div>
            <!-- //right content panel -->
        </div>
        <div class="clear">
        </div>
    </div>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-29026379-1']);
        _gaq.push(['_setDomainName', 'insighto.com']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script>
</asp:Content>
