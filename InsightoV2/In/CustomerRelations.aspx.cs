﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Collections;
using Insighto.Business.Helpers;

public partial class In_CustomerRelations : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["key"] != null)
            {
                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                var pageContent = ServiceFactory.GetService<HelpPageService>().GetHelpPageContent(ht["Page"].ToString());
                if (pageContent != null)
                {
                    divContent.InnerHtml = pageContent.PageContent;
                }
                else
                {
                    divContent.InnerHtml = "";
                }
            }
        }
    }
}