﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using System.Configuration;

public partial class In_Services : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string subject = "Insighto Survey Solutions – Customer Query";
        string strBody = "<b>Name :</b>" + txtName.Text + "<br/><b>Do you have subscription to Insighto.com :</b>" + rbtnListSubscription.SelectedItem.Value + "<br/><b>Email Id :</b>" + txtEmailId.Text + "<br/><b>Phone No :</b>" + txtPhoneNo.Text + "<br/><b>Service required :</b>" + ddlService.Text;
        //string strTo = "satya@knowience.com,satya@knowience.com";
        string strTo = ConfigurationManager.AppSettings["servicesEmails"].ToString();
        MailHelper.SendMailMessage(txtEmailId.Text, strTo, "", "", subject, strBody);
        divSuccess.Style.Add("display", "block");
        lblMessage.Style.Add("display", "block");
        lblMessage.Text = "Thank you. We will get back to you.";
        ClearControls();
    }
    private void ClearControls()
    {
        txtName.Text = string.Empty;
        rbtnListSubscription.ClearSelection();
        txtEmailId.Text = string.Empty;
        txtPhoneNo.Text = string.Empty;
        ddlService.SelectedIndex = 0;
    }
}