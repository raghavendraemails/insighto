﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.master" AutoEventWireup="true"
    CodeFile="Checkout1.aspx.cs" Inherits="In_Checkout" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input id="hdnMerchantId" type="hidden" runat="server" />
    <input id="hdnOrderId" type="hidden" runat="server" />
    <input id="hdnRedirectUrl" type="hidden" runat="server" />
    <input id="hdnChecksum" type="hidden" runat="server" />
    <input type="hidden" id="Currency" runat="server" />
    <div class="messagePanel">
        <!-- signup panel -->
        <p>
            <asp:Label ID="lblInfo" runat="server" Text="You are being directed through a payment gateway page where you need to give details
            of your information and choose any of the payment options provided." 
                meta:resourcekey="lblInfoResource1"></asp:Label>
        </p>
        <p>&nbsp;</p>
        <p>
            <asp:Label ID="lblCaution" runat="server" Text="Caution: Please ensure that the information such as a your name, address, contact
            number match with your Credit/Debit card service provider." 
                meta:resourcekey="lblCautionResource1"></asp:Label>
        </p>
        <p>&nbsp;</p>
        <p>
            <asp:Button ID="btnContinue" runat="server" Text="Continue" ToolTip="Continue" 
                meta:resourcekey="btnContinueResource1" />
            <%--<a href="index.html"><img src="images/button_continue.gif" alt="Continue" title="Continue" />--%></a>
        </p>
    </div>
</asp:Content>
