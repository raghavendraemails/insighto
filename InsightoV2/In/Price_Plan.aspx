﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true" CodeFile="Price_Plan.aspx.cs" Inherits="In_Price_Plan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Price plan - Insighto</title>
<link href="../App_Themes/Classic/style.css" rel="stylesheet" type="text/css" />
</head>
 
<body>
<div id="wrapper">
    
    <div id="container_main">
    	<div class="priceplan_blk">
        	
            <!--  Basic Price Blk  -->
        
        	<div class="price_1_blk">
            	<h3>BASIC</h3>
                <div class="price_1_blk_cont" >
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header">
                                    <div class="price_tag_free">Free <br /> for<br /> LIFE</div>
                                    <div class="price">
                                        <asp:Label ID="Label2" runat="server" Text=""></asp:Label></div>
                                    per month
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td><asp:Button ID="btnsignup1" Text="Sign Up" runat="server" CssClass="btn_bg" 
                                    onclick="btnsignup1_Click" /></td>
                          </tr>
                          <tr>
                            <td>Unlimited number of surveys</td>
                          </tr>
                          <tr>
                            <td>15 questions per survey</td>
                          </tr>
                          <tr>
                            <td>150 responses per survey</td>
                          </tr>
                          <tr>
                            <td>18 question types</td>
                          </tr>
                          <tr>
                            <td>Survey templates</td>
                          </tr>
                          <tr>
                            <td>Choice of themes</td>
                          </tr>
                          <tr>
                            <td>Launch through web link</td>
                          </tr>
                          <tr>
                            <td>Report in data tables</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><a href="../In/Pricing.aspx">View all features</a></td>
                          </tr>
                          <tr>
                            <td class="no_border">
                                <asp:Button ID="btnsignup2" Text="Sign Up" runat="server" 
                                    CssClass="btn_bg" onclick="btnsignup2_Click" /></td>
                          </tr>
                    </table>
                </div>
 
            </div>
            <!--  Basic Price Blk End  -->
        	
            <!--  Basic Pro Blk  -->
        
        	<div class="price_1_blk">
            	<h3>PRO</h3>
                <div class="price_1_blk_cont">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header green">
                                    <div class="price">
                                        <asp:Label ID="Label1" runat="server" Text="" CssClass="price" ></asp:Label></div>
                                    per annum
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border"><a id="PayMonth" runat="server">
                                <asp:Label ID="lblMonthly" runat="server" Text="Monthly Plan - <span class='font_ruppe'>`</span>450"></asp:Label>
                                </a>
                            </td>
                          </tr>
                          <tr>
                            <td><asp:Button ID="btnbuynow1" Text="Buy Now" runat="server" CssClass="btn_bg" 
                                    onclick="btnbuynow1_Click" /></td>
                          </tr>
                          <tr>
                            <td>Unlimited number of surveys</td>
                          </tr>
                        
                          <tr>
                            <td>Unlimited number of questions</td>
                          </tr>
                          <tr>
                            <td>Unlimited responses</td>
                          </tr>
                          <tr>
                            <td>19 question types</td>
                          </tr>
                           <tr>
                            <td>Survey templates</td>
                          </tr>
                          <tr>
                            <td>Choice of themes</td>
                          </tr>
                          <tr>
                            <td>Insert brand logo</td>
                          </tr>
                          <tr>
                            <td>Personalize buttons</td>
                          </tr>
                          <tr>
                            <td>Personalize survey end message</td>
                          </tr>
                          <tr>
                            <td>Schedule alerts</td>
                          </tr>
                          <tr>
                            <td>Print survey</td>
                          </tr>
                          <tr>
                            <td>Save survey to Word</td>
                          </tr>
                          <tr>
                            <td>Launch through web link</td>
                          </tr>
                          <tr>
                            <td>Launch via Insighto emails</td>
                          </tr>
                          <tr>
                            <td>Report in data and graphs</td>
                          </tr>
                          <tr>
                            <td>Export data to Excel, PPT, PDF and Word</td>
                          </tr>
                          <tr>
                            <td>Cross Tab</td>
                          </tr>
                          <tr>
                            <td><a href="../In/Pricing.aspx">View all features</a></td>
                                </tr>
                          <tr>
                            <td class="no_border">
                                <asp:Button ID="btnbuynow2" Text="Buy Now" runat="server" 
                                    CssClass="btn_bg" onclick="btnbuynow2_Click" /></td>
                          </tr>
                    </table>
                </div>
 
            </div>
            <!--  Basic Pro Blk End  -->
        
        	
   			<div class="clr"></div>
 
        </div>
   		<div class="clr"></div>
        <div>&nbsp;</div>
   
         <center>
            <div class="prcingFaqHeading" id="divFaq" style="text-align:left;">          
            <a href="javascript:void(0)" id="lnkShowContent" onclick="javascript:showHideFAQPanel();" style="font-size:12px;">Pricing FAQs</a>
            <a href="javascript:void(0)" onclick="showHideFAQPanel()"><span class="plusExpandFAQ">+</span></a>
            </div>
            <div >&nbsp;</div>
  <div class="prcingFaqContent" style="display:none;text-align:left;" id="divFaqContent">  
  <div id="divFaqIndiaContent" runat="server">
  <h4>What’s the validity of the Free subscription?</h4>
  <p>There’s no end date to Insighto Free subscription. In a sense, the Free subscription has lifetime validity!</p>
    <p class="defaultHeight">&nbsp;</p>
         <h4>In short, what is it that I do not get with my Free subscription?</h4>
  <p>The Free subscription is designed to offer you a real feel of the way the survey tool works albeit with a few limitations. (Else, you won’t pay us, will you?)</p>
 <p><b>You will need a Pro subscription, if you have the following needs.</b></p>
 <ul class="listPadding">
 <li>You need more than 150 responses for your survey</li>
  <li>You would like to brand your survey with your logo</li>
  <li>You would like to have a skip logic in your survey</li>
<li>You would like to have schedule your survey launch and survey closure automated</li>
<li>You would like to redirect your respondents to a website of your choice after they complete the response</li>
<li>You would like to have a custom Thank You page</li>
<li>You would like to export your responses into an Excel, PDF, Word or a PowerPoint.</li> 
<li>You would like to view your data in graphs</li>

 </ul>
      <%--  <p class="defaultHeight">&nbsp;</p>--%>
   <h4>If I do not renew my Pro subscription, what would be the status of my account?</h4>
   <p>If for some reason, you do not renew your Pro subscription, you will automatically turn into a Free subscriber. You will be able to see the data with the limitations of a Free subscriber.</p>
     <p class="defaultHeight">&nbsp;</p> 
      <h4>If I do not renew my Pro subscription, what happens to my data?</h4>
        <p>Your data stays secure with us even if you are not a Pro subscriber. However, you will be able to see your data with the limitations of a Free subscriber.</p>
       <p class="defaultHeight">&nbsp;</p>
         <h4>What happens if I launch a survey while having a Pro subscription but do not renew while I still have active survey(s)?</h4>
           <p>Assuming that your Pro subscription is not renewed while you still have active surveys, the following scenarios are applied.</p>
             <ol style="list-style-type:lower-alpha" class="listPadding">            
             <li>If the active survey(s) have any Pro features used (for example, skip logic), such active surveys will be closed for response.</li> 
<li>If the active survey(s) do not have any Pro features used, they will continue to be active.</li>

             </ol>
      <%--  <p class="defaultHeight">&nbsp;</p>--%>
               <h4>What are the different payment options I have?</h4>
               <p><b>There are five different options you can choose from.</b></p>
                <ol style="list-style-type:lower-alpha" class="listPadding">
                <li>	Credit card</li>
<li>Debit Card</li>
<li>Net banking</li>
<li>Pay by cheque (Not available for Pro – Monthly)</li>
<li>Direct deposit (Not available for Pro – Monthly)</li>

                </ol>
              <%--  <p class="defaultHeight">&nbsp;</p>--%>
                  <h4>How do I upgrade? </h4>
                    <p>You can upgrade at any time right from “My Surveys” page. Simply click on the upgrade button and choose the plan that you’d like to upgrade to.</p>
                      <p class="defaultHeight">&nbsp;</p>
                        <h4>Is my data secure and private? </h4>
                          <p>Yes, your data is secure and private. Your collected data is yours and we never use it or share it any way shape or form. Please see our Privacy Policy, Security Information, and Terms of Use for further info.</p>
                           <p class="defaultHeight">&nbsp;</p>
                              <h4>How do I cancel my account?</h4>
                               <p>You can cancel anytime you want. For one, we would be sorry to see you go as we are constantly working to add more features that enhance your experience with Insighto.
 Please let us know how we can improve.</p>
 <p>
 If you would like to cancel your subscription, please do send a mail to Support@insighto.com. We, of course, would be keen to know the reasons for the same.
 </p>
 <p class="defaultHeight">&nbsp;</p>
 <b>Cancellation Terms:</b>
 <p>
 Once your Professional plan cancels, the account converts to the Basic (free) status with all the limitations associated with that account type. Professional features will be deactivated, but you will still have access to your account and surveys. You can reactivate it at a later date and everything will be available to you again. If you have active surveys or need to analyze data, then we recommend waiting to cancel the subscription until you are completely finished.
 </p>
 <p class="defaultHeight">&nbsp;</p>
 <p>
 <b>NOTE - Pro-Refunds:</b> We do not offer pro-rated refunds on plans. If you anticipate needing the plan for a short amount of time, purchase a PRO Monthly plan.
 </p>
 <p>
 <b>Example:</b> If you have a paid monthly subscription, it will cancel at the end of the current billing cycle. If you have an annual subscription, it will cancel at the end of the 12 months period.
 </p> 
 </div>
   <div id="divFaqOtherContent" runat="server">
  Other Countries Content Here
  </div>
  </div> 

  </center>
  </div>
    
    
</div>
</body>
</html>

 <script language="javascript" type="text/javascript">
     function showHide(EL, PM) {
         ELpntr = document.getElementById(EL);
         if (ELpntr.style.display == 'none') {
             document.getElementById(PM).innerHTML = "-";
             ELpntr.style.display = 'block';
         }
         else {
             document.getElementById(PM).innerHTML = "+";
             ELpntr.style.display = 'none';
         }
     }
     function RedirectToLoginPage() {
         window.location.href = "../Home.aspx";
         return false;
     }

     function showHideFAQPanel() {
         divContent = document.getElementById('divFaqContent');
         if (divContent.style.display == 'none') {
             // document.getElementById(PM).innerHTML = "-";
             divContent.style.display = 'block';
         }
         else {
             //document.getElementById(PM).innerHTML = "+";
             divContent.style.display = 'none';
         }
     }
        
    </script>
</asp:Content>

    	