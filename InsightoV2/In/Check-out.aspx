﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Check-out.aspx.cs" Inherits="In_Check_out" meta:resourcekey="PageResource1" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc" TagName="TopLinks" Src="~/UserControls/TopLinks.ascx" %>
<%@ Register TagPrefix="uc" TagName="HeaderMenu" Src="~/In/UserControls/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
     <script type="text/javascript" src="../Scripts/jquery-1.6.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
//            $('#btnContinue').click(function () {
                $('#form1').submit();
//            });
        });
    </script>
    <link href="../insighto.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link href="../App_Themes/Classic/layoutStyle.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript">

         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-29026379-1']);
         _gaq.push(['_setDomainName', 'insighto.com']);
         _gaq.push(['_setAllowLinker', true]);
         _gaq.push(['_trackPageview']);

         (function () {
             var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
             ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
             var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();

</script>
</head>
<body>
    <form id="form1" runat="server" method="POST" action="https://sites.fastspring.com/insighto/api/order">   

    <div>
    <input type="hidden" name="operation" value="create"/>
<input type="hidden" name="destination" value="checkout"/>
<input type="hidden" name="product_1_path" id="product_1_path" runat="server" value="/insighto_monthly"/>
<input type="hidden" name="contact_fname" id="contact_fname" runat="server"/>
<input type="hidden" name="contact_lname" id="contact_lname" runat="server"/>
<input type="hidden" name="contact_company" id="contact_company" runat="server"/>
<input type="hidden" name="contact_email" id="contact_email" runat="server"/>
<input type="hidden" name="contact_phone" id="contact_phone" runat="server"/>
<input type="hidden" name="referrer" id="referrer" runat="server"/>
<%--<input type="hidden" runat="server" name="product_1_quantity" id="product_1_quantity" value="2"/> paypersurvey--%>


<div id="wrapper">
        <!-- main wrapper -->
        <!-- Top Links -->
        <uc:TopLinks ID="ucTopLinks" runat="server" />
        <!-- Top Links -->
        <!-- Header -->
        <uc:HeaderMenu ID="ucHeader" runat="server" />
        <!-- Header -->
        <div class="messagePanel">
            <!-- signup panel -->
             <p>
                <asp:Label ID="lblInfo" runat="server"   Font-Size="Large" Font-Names="Arial" Text=" You are now being redirected to the payments page. 
                                                               Please submit your address & payment details." 
                    meta:resourcekey="lblInfoResource1"></asp:Label>
            </p>
            <p>
                &nbsp;</p>
            <p>
                <asp:Label ID="lblCaution" runat="server"  Font-Size="Large" Font-Names="Arial" Text="Note:Your information such as Name, Address, Phone number and EmailId must match with that on file with your Credit/Debit	 card issuer." 
                    meta:resourcekey="lblCautionResource1"></asp:Label>
            </p>
            <p>
                &nbsp;</p>
           <%-- <p>
                <asp:Button ID="btnContinue" runat="server" Text="Continue" CssClass="dynamicButton"
                    ToolTip="Continue" meta:resourcekey="btnContinueResource1" />
            </p>--%>
        </div>
        <uc:Footer ID="ucFooter" runat="server" />
        <!-- //main wrapper -->
    </div>
    </div>
    </form>
  
     <script type="text/javascript">
         $(document).ready(function () {
             $('.alreadySignIn').hide();
         });
    </script>
       <!-- Google Code for Lead_signup Conversion Page -->
<script type="text/javascript">
// <![CDATA[
    var google_conversion_id = 1027814833;
    var google_conversion_language = "en";
    var google_conversion_format = "2";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "dEWdCImw7gIQseuM6gM";
    var google_conversion_value = 0;
// ]]> 
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1027814833/?value=0&amp;label=dEWdCImw7gIQseuM6gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>
