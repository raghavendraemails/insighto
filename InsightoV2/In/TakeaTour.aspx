﻿<%@ Page Title="Online Questionnaire Tool | Survey Software– Getting Started" Language="C#"
    MasterPageFile="~/Home.master" AutoEventWireup="true" CodeFile="TakeaTour.aspx.cs"
    Inherits="In_TakeaTour" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <meta name="description" content="Create simple surveys using Insighto’s questionnaire software tool. Design using various survey templates." />
    <meta name="keywords" content="online survey software tool, online questionnaire tool, online survey tool, online survey templates" />
    <div class="takeATourContentPanel">
        <!-- take a tour main content panel -->
        <div class="faqTopMenuPanel">
            <!-- tab links -->
            <ul id="faqNav">
                <li class="activelink">
                    <asp:HyperLink ID="lnkCreate" runat="server" ToolTip="Creating Online Web Surveys" NavigateUrl="~/In/TakeaTour.aspx"
                        Text="Create"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkDesign" runat="server" NavigateUrl="~/In/TakeATour_Design.aspx"
                        ToolTip="Logo for Branding – Design Surveys" Text="Design"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkLaunch" runat="server" NavigateUrl="~/In/TakeATour_Launch.aspx"
                        ToolTip="Launch Online Surveys" Text="Launch"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkAnalyze" runat="server" NavigateUrl="~/In/TakeATour_Analyze.aspx"
                        ToolTip="Manage Responses and Analyze" Text="Analyze"></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkManage" runat="server" NavigateUrl="~/In/TakeATour_Manage.aspx"
                        ToolTip="Manage Email Lists" Text="Manage"></asp:HyperLink></li>
            </ul>
            <div class="clear">
            </div>
            <!-- //tab links -->
        </div>
        <div class="clear">
        </div>
        <div class="takeATourTextPanel">
            <!-- text panel -->
            <div class="takeATourButtonPanel">
                <!-- prev next bubtton -->
                <asp:HyperLink ID="lnkPrevius" runat="server" NavigateUrl="javascript:void(0);" CssClass="active"
                    ToolTip="Previous" Text="Prev"></asp:HyperLink>
                <asp:HyperLink ID="lnkNext" runat="server" NavigateUrl="javascript:void(0);" CssClass="active"
                    ToolTip="Next" Text="Next"></asp:HyperLink>
                <!-- //prev next bubtton -->
            </div>
            <div style="display: block;" id="Create_1">
                <!-- gettin started -->
                <h3>
                    Creating a professional survey is simple and swift! Experience this with Insighto’s
                    user friendly and intuitive software</h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/create-img-01.jpg" alt='' />
                </p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>No software to install. Just open a browser and you are ready to go</li>
                    <li>Basic features - free for life time</li>
                    <li>Advanced features for pro users</li>
                </ul>
                <!-- //gettin started -->
            </div>
            <div style="display: none" id="Create_2">
                <!-- question types -->
                <h3>
                    Creating a survey questionnaire has never been so simple
                </h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/create-img-02.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>Choose from over 20 question types</li>
                    <li>Create questions with ease</li>
                    <ul>
                        <li>Randomize answer options</li>
                        <li>Make responses mandatory</li>                     
                        <li>Add page breaks or reorder your questions with a single click of your mouse</li>
                        <li>View all questions and answer options as you build your survey</li>
                    </ul>
                </ul>
                <%-- <h4>
                    Format with ease</h4>
                <ul>
                    <li>Randomize answer options</li>
                    <li>Force responses</li>
                    <li>Add comment fields</li>
                    <li>Add page breaks or reorder your questions with a single click of your mouse</li>
                    <li>View all questions and answer options as you build your survey</li>
                </ul>--%>
                <!-- //question types -->
            </div>
            <div style="display: none" id="Create_3">
                <h3>
                    Input the answer options of your choice
                </h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/create-img-03.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <br />
                <ul>
                    <li>Keep adding answer options with a single click</li>
                    <li>Control the look of your answer options – select the number of columns – from 1
                        to 4 – to display the answer options to your respondents</li>
                </ul>
            </div>
            <div style="display: none" id="Create_4">
                <!-- answer options -->
                <h3>
                    Insighto’s professional survey templates will save you time and effort
                </h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/create-img-04.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <br />
                <h4>
                    Get a jumpstart with popular survey templates such as</h4>
                <ul>
                    <li>Customer Satisfaction</li>
                    <li>Employee Satisfaction</li>
                    <li>Event / Conference Feedback</li>
                    <li>Training Evaluation</li>
                    <li>Product Feedback and many more</li>
                </ul>
                <!-- answer options -->
            </div>
            <div style="display: none" id="Create_5">
                <!-- questionnair -->
                <h3>
                    Target right questions to the right respondents with skip logic
                </h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/create-img-05.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <br />
                <h4>
                    Skip logic allows you create custom path for respondents.</h4>
                <ul>
                    <li>Skip to specific questions</li>
                    <li>Terminate surveys, show customized survey end messages etc., based on responses
                        to previous questions</li>
                </ul>
                <!-- questionnair -->
            </div>
            <div style="display: none" id="Create_6">
                <!-- preview survey -->
                <h3>
                    Keep a tab on how your survey is shaping up by previewing
                </h3>
                <p style="padding-top: 10px;">
                    <img src="../images/homeimages/create-img-06.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>
                <ul>
                    <li>Preview the survey on the same screen</li>
                    <li>Or copy the link and view it in a different browser too</li>
                </ul>
                <!-- preview survey -->
            </div>
            <div style="display: none" id="Create_7">
                <!-- review survey -->
                <h3>
                    Share the survey for a second opinion easily. With the preview link
                </h3>
                <br />
                <p style="padding-top: 5px;">
                    <img src="../images/homeimages/create-img-07.jpg" alt='' /></p>
                <div style="clear: both;">
                </div>             
                <ul>
                    <li> Copy the survey preview URL and forward to any of your colleagues or superiors for
                    their opinion or inputs</li>                    
                </ul>
                <!-- review survey -->
            </div>
            <!-- //text panel -->
        </div>
        <div class="takeATourSubMenuPanel">
            <!-- sub menu -->
            <ul class="takeATourMenu">
                <li><a id="lnkCreate_1" class="active" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(1);">
                    Getting Started</a></li>
                <li><a id="lnkCreate_2" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(2);">
                    Question Types</a></li>
                <li><a id="lnkCreate_3" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(3);">
                    Answer Options</a></li>
                <li><a id="lnkCreate_4" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(4);">
                    Survey Templates</a></li>
                <li><a id="lnkCreate_5" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(5);">
                    Apply Logic</a></li>
                <li><a id="lnkCreate_6" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(6);">
                   Survey Preview</a></li>
                <li><a id="lnkCreate_7" href="javascript:void(0)" onclick="javascript:ShowSelectedScreen(7);">
                    Review Survey URL</a></li>
            </ul>
            <!-- //sub menu -->
        </div>

        <!-- // take a tour main content panel -->
         <input type="hidden" class="hdnPanelIndex" id="hdnPanelIndex" value="1" />
    </div>
    
    <%-- <div class="contentPanel">
        <div class="defaultHeight">
        </div>
        <div>
        <h2><center>Create smart and professional surveys</center></h2>
        <p class="defaultHeight"></p>
        <h2><center>Insighto makes it possible with an easy-to-use interface</center></h2>
        <p class="defaultHeight"></p>
         <p align="center"><b>Insighto’s user friendly interface will guide all through and give you an enchanting survey experience</b></p>
          <p class="defaultHeight"></p>
         <p class="defaultHeight"></p>
         <p align="center">
            <a href="#Create">Create</a> | 
            <a href="#Design">Design</a> | 
            <a href="#Launch">Launch</a> | 
            <a href="#Analyze">Analyze</a> | 
            <a href="#Manage">Manage</a>
         </p>
         <p class="defaultHeight"></p>
         <p class="defaultHeight"></p>
         <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td colspan="2">
                    <div class="takeAtourRow">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                            <div class="text">
                    

                   
                    </div>
                            </td>    
                            <td valign="top"> </td>
                        </tr>
                        <tr>
                    <td colspan="2">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top"></td>
                            <td valign="top">
                            <div class="text">
                    
                    </div>
                            </td>
                        </tr>
                    </table>
                    
                    </td>
                </tr>
                    </table>
                    </div>
                </td>
                
            </tr>
          
            <tr><td colspan="2" align="right"><a href="#" style="font-size:11px;">Top</a></td></tr>
            <tr>
                <td colspan="2">
                <div class="takeAtourRow">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                            <div class="text">
                    <h2><a name="Design">Design</a></h2>
                    <p class="defaultHeight"></p>
                    <p>Insighto provides all the required tools to enhance the look and feel of your survey for better visual presentation which can gain better responses.</p>
                    <ul class="tickUL">
                        <li>Choose from different themes</li>
                        <li>Upload your logo for better branding </li>
                        <li>Select fonts of your choice – which would get applied all through the survey</li>
                        <li>Create a header and footer for your survey </li>
                        <li>Select the buttons that guide the respondent towards end of each page</li>
                        <li>Decide on the appearance of Survey Progress bar </li>
                    </ul>
                    </div>
                            </td>
                            <td valign="top"><img src="../images/homeimages/design-img.jpg" /></td>
                        </tr>
                    </table>
                </div>
                </td>
            </tr>
            <tr><td colspan="2" align="right"><a href="#" style="font-size:11px;">Top</a></td></tr>
            <tr>
                <td colspan="2">
                <div class="takeAtourRow">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <img src="../images/homeimages/launch-img.jpg" />
                            </td>
                            <td valign="top">
                            <div class="text">
                    <h2><a name="Launch"></a>Launch</h2>
                    <p class="defaultHeight"></p>
                    <h3>Schedule surveys</h3>
                    <ul class="tickUL">
                        <li>Time your survey launch or close per your choice</li>
                        <li>Close survey after reaching specific number of  
responses
</li>
                        <li>Set alerts</li>
                        <li>Set a self reminder to keep tab on survey responses</li>
                    </ul>
                    <h3>Thank You & Re-direction</h3>
                    <ul class="tickUL">
                        <li>Set/customise survey end messages</li>
                        <li>Re-direct respondents to your website</li>
                    </ul>
                    <h3>Launch through</h3>
                    <ul class="tickUL">
                        <li>Insighto’s email system  OR</li>
                        <li>Generate a weblink and send /share through your 
email or website or social networking sites
</li>
                        <li>Customize survey end pages</li>
                        <li>Redirect the respondents to the website of your choice</li>                        
                    </ul>
                    </div>
                            </td>
                        </tr>
                    </table>
                </div>
                </td>
               
            </tr>
            <tr><td colspan="2" align="right"><a href="#" style="font-size:11px;">Top</a></td></tr>
            <tr>
                <td>
                <div class="takeAtourRow">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                            <div class="text">
                    <h2><a name="Analyze">Analyze</a></h2>
                    <p class="defaultHeight"></p>
                    <h3>Real time reports</h3>
                    <ul class="tickUL">
                        <li>Automatic data compilation & report generation </li>
                        <li>Analytical reports in real time </li>
                        <li>Automatic chart/graph builder</li>
                        <li>View valid/completed and partial responses</li>
                        <li>See each individual response</li>
                        <li>Flexibility to move responses from valid to partials and vice versa</li>
                        <li>Customize charts per your choice</li>
                        <li>Add comments interpreting the chart data</li>
                        <li>Generate cross tab reports</li>
                        <li>Export reports to MS PPT, PDF, MS Excel and MS Word</li>
                    </ul>
                    <h3>Sample PPT report</h3>
                    </div>
                            </td>
                            <td valign="top"><img src="../images/homeimages/analyze-img.jpg" /></td>
                        </tr>
                    </table>
                </div>
                </td>
               
            </tr>
            <tr><td colspan="2" align="right"><a href="#" style="font-size:11px;">Top</a></td></tr>
            <tr>
                <td>
                <div class="takeAtourRow">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top"><img src="../images/homeimages/manage-img.jpg" />
                                
                            </td>
                            <td valign="top">
                            <div class="text">
                    <h2><a name="Manage">Manage</a></h2>
                    <p class="defaultHeight"></p>
                    <h3>Survey Dashboards</h3>
                    <ul class="tickUL">
                        <li>See all surveys you created – categorised into 
                            <ul>
                                <li>All</li>
                                <li>Active (launched surveys)</li>
                                <li>Draft (surveys being built)</li>
                                <li>Closed (which could be activated any time)</li>
                            </ul>
                        </li>
                        <li>
                            Track your survey from stats such as
                            <ul class="tickUL">
                                <li>
                                    % of responses
                                </li>
                                <li>Visits (number of people clicked)</li>
                                <li>Completes</li>
                                <li>Partials</li>
                                <li>Bounced</li>
                            </ul>
                        </li>
                        <li>Send reminders to those who have not yet responded 
or completed the survey

                        </li>
                        <li>Take all survey control actions from one place</li>
                    </ul>
                    </div>
                            </td>
                        </tr>
                    </table>
                </div>
                </td>
               
            </tr>
            <tr><td colspan="2" align="right"><a href="#" style="font-size:11px;">Top</a></td></tr>
           
         </table>
        </div>
    </div>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('.hdnPanelIndex').val() == '1') {
                $('#<%=lnkPrevius.ClientID %>').hide();
            }
        });
        $('#<%=lnkNext.ClientID %>').click(function () {
            var index = $('.hdnPanelIndex').val();
            $('#Create_' + index).hide();
            $('#lnkCreate_' + index).removeClass('active');
            index++;
            $('#Create_' + index).show();
            $('.hdnPanelIndex').val(index);
            if ($('.hdnPanelIndex').val() > 1) {
                $('#<%=lnkPrevius.ClientID %>').show();
                $('#lnkCreate_' + index).addClass('active');
                if (index == 7) {
                    $('#<%=lnkNext.ClientID %>').hide();
                }
            }
        });
        $('#<%=lnkPrevius.ClientID %>').click(function () {
            var index = $('.hdnPanelIndex').val();
            $('#Create_' + index).hide();
            $('#lnkCreate_' + index).removeClass('active');
            index--;
            $('#Create_' + index).show();
            $('.hdnPanelIndex').val(index);
            if ($('.hdnPanelIndex').val() < 7) {
                $('#<%=lnkNext.ClientID %>').show();
                $('#lnkCreate_' + index).addClass('active');
                if (index == 1) {
                    $('#<%=lnkPrevius.ClientID %>').hide();
                }
            }
        });
        function ShowSelectedScreen(index) {
            if (index == 1) {
                $('#<%=lnkPrevius.ClientID %>').hide();
                $('#<%=lnkNext.ClientID %>').show();
            }
            else if (index == 7) {
                $('#<%=lnkNext.ClientID %>').hide();
                $('#<%=lnkPrevius.ClientID %>').show();
            }
            else {
                $('#<%=lnkNext.ClientID %>').show();
                $('#<%=lnkPrevius.ClientID %>').show();
            }
            var prevIndex = $('.hdnPanelIndex').val();
            $('#Create_' + prevIndex).hide();
            $('#lnkCreate_' + prevIndex).removeClass('active');
            $('#Create_' + index).show();
            $('#lnkCreate_' + index).addClass('active');
            $('.hdnPanelIndex').val(index);

        }
    </script>
    <!--Start of Zopim Live Chat Script-->

<script type="text/javascript">

    window.$zopim || (function (d, s) {
        var z = $zopim = function (c) { z._.push(c) }, $ = z.s =

d.createElement(s), e = d.getElementsByTagName(s)[0]; z.set = function (o) {
    z.set.

_.push(o)
}; z._ = []; z.set._ = []; $.async = !0; $.setAttribute('charset', 'utf-8');

        $.src = '//cdn.zopim.com/?bYM7XxoDiH1Gv57aQNwmeiatKZbOCdA7'; z.t = +new Date; $.

type = 'text/javascript'; e.parentNode.insertBefore($, e)
    })(document, 'script');

</script>

<!--End of Zopim Live Chat Script-->
</asp:Content>
