﻿<%@ Page Language="C#" MasterPageFile="~/Login.master" AutoEventWireup="true" CodeFile="InVoiceConfirmation.aspx.cs" Inherits="Insighto.Pages.In.In_InVoiceConfirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="InVoice_messagePanel"><!-- signup panel -->
		<div class="confirm_middle_pnl">
       
        <h2 class="center"><asp:Label ID="lblThankyouInsighto" runat="server"                 
                meta:resourcekey="lblThankyouInsightoResource1"></asp:Label>
            </h2>
            
        <p>&nbsp;</p>
        <p class="center" id="txtConfirm" runat="server">
            <asp:Label ID="lblConfirm" runat="server" 
                Text="Invoice PRO-Y0811197 has been generated for your PRO account." 
                meta:resourcekey="lblConfirmResource1"></asp:Label>
            </p>
			
		<p>&nbsp;</p>
        <div class="center">
            <a href="#" id="hlk_viewinvoice" runat="server">
                <img src="../App_Themes/Classic/Images/invoice-view.gif" width="113" 
                height="24" hspace="4" border="0" alt=""/></a>                
            <a href="#" id="hlk_printinvoice" runat="server">
                <img src="../App_Themes/Classic/Images/invoice-print.gif" width="113" 
                height="24" hspace="4" border="0" alt=""/></a></div>
        <p>&nbsp;</p>
        <p class="center" id="invoice_seperatetext" runat="server" visible="false">		
</p>

        <div id="divHeading" runat="server">
            <h2 class="center" id="heading_activate" runat="server">
           <u><asp:LinkButton ID="lnkActivate" runat="server" Text="Login to Insighto"
                   PostBackUrl="~/Home.aspx">
            </asp:LinkButton></u> 
            
               </h2>
            
        </div>
		<%--<p class="center" id="txtActivate" runat="server">--%><%--To activate your account and experience Insighto, we would need you to authenticate your registration.
An email has been sent to your email box with a confirmation link.--%> 
</p>
<%--<p class="center">Please visit your email box and click on the link to complete your registration processIf you experience any issue/ difficulty in registration/activation, please contact us at <b><a href="#">support@insighto.com</a></b></p>--%>
 
<p>&nbsp;</p>
    </div>
		<div class="clear"></div>
	<!-- //signup panel --></div>
</asp:Content>

