﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using System.Configuration;
using App_Code;
using System.Data;
using System.Data.SqlClient;

namespace Insighto.Pages.In
{
    public partial class In_InVoiceConfirmation : System.Web.UI.Page
    {
        SurveyCore sc = new SurveyCore();
        string surveylicensetype = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Hashtable ht = new Hashtable();
                var userService = ServiceFactory.GetService<UsersService>();
                int orderId = 0, userId = 0, succTrans = 0;
                string invoice = "", strNavurl = "", supportEmail = "", upgradeFlag = "", accounttype = "";
                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0)
                    {
                        if (ht.Contains("ord_id"))
                            orderId = Convert.ToInt32(ht["ord_id"]);
                        if (ht.Contains("Usrid"))
                            userId = Convert.ToInt32(ht["Usrid"]);
                        if (ht.Contains("tran_status"))
                            succTrans = Convert.ToInt32(ht["tran_status"]);
                        if (ht.Contains("invoice_id"))
                        {
                            invoice = Convert.ToString(ht["invoice_id"]);
                            if (invoice.Contains("PPSPREM"))
                            {
                                accounttype = "PREM";
                                lblThankyouInsighto.Text = "Thank you for purchasing Insighto Premium-Single Survey plan.";
                            }
                            else if (invoice.Contains("PREM"))
                            {
                                accounttype = "PREM";
                                lblThankyouInsighto.Text = "Thank you!! Welcome to Insighto Premium.";
                            }
                            else if (invoice.Contains("PRO"))
                            {
                                accounttype = "PRO";
                                lblThankyouInsighto.Text = "Thank you for purchasing Insighto Pro-Single Survey plan.";

                            }

                        }
                        if (ht.Contains("surveylicensetype"))
                        {
                            surveylicensetype = Convert.ToString(ht["surveylicensetype"]);
                            if (surveylicensetype.Contains("PREMIUM-YEARLY"))
                            {
                                accounttype = "PREM";
                                lblThankyouInsighto.Text = "Thank you!! Welcome to Insighto Premium Annual.";
                            }
                            else if (surveylicensetype.Contains("PPS-PREM"))
                            {
                                accounttype = "PREM";
                                lblThankyouInsighto.Text = "Thank you for purchasing Insighto Premium-Single Survey plan.";
                            }
                            else if (surveylicensetype.Contains("PPS-PRO"))
                            {
                                accounttype = "PRO";
                                lblThankyouInsighto.Text = "Thank you for purchasing Insighto Pro-Single Survey plan.";

                            }
                        }                        
                    }
                }
                var orderInfo = ServiceFactory.GetService<OrderInfoService>().GetOrderInfo(orderId, userId).FirstOrDefault();
                List<string> listConfg = new List<string>();
                listConfg.Add(Constants.SUPPORTEMAIL);
                var configService = ServiceFactory.GetService<ConfigService>();
                var configvalue = configService.GetConfigurationValues(listConfg).FirstOrDefault();
                if (configvalue != null)
                    supportEmail = configvalue.CONFIG_VALUE;
               
                if (succTrans > 0)
                {
                    if (succTrans == 1)
                    {



                        var returnValue = userService.UpgradeUserInvoice(orderId);

                        DataSet dsbzcode = sc.getBizcode(orderId);

                        if (dsbzcode.Tables.Count > 0)
                        {
                            if (dsbzcode.Tables[0].Rows.Count > 0)
                            { 
                                if (dsbzcode.Tables[0].Rows[0][2].ToString() == "PPSPROREG")
                                {
                                    DataSet dsproupdate = sc.updatesurveyEmailType(userId, "PPS_PRO", Convert.ToInt32(dsbzcode.Tables[0].Rows[0][1].ToString()), "PRO_YEARLY", dsbzcode.Tables[0].Rows[0][3].ToString(), "upgradetopro", "PPS_PRO");
                                }
                                else if (dsbzcode.Tables[0].Rows[0][2].ToString() == "PPSPREMREG")
                                {
                                    DataSet dsproupdate = sc.updatesurveyEmailType(userId, "PPS_PREMIUM", Convert.ToInt32(dsbzcode.Tables[0].Rows[0][1].ToString()), "PREMIUM_YEARLY", dsbzcode.Tables[0].Rows[0][3].ToString(), "upgradetopremium", "PPS_PREMIUM");
                                }
                                //else if (dsbzcode.Tables[0].Rows[0][2].ToString() == "PPSPRESOC")
                                //{
                                //}
                            }
                        }                          

                        txtConfirm.InnerHtml = String.Format(Utilities.ResourceMessage("InvoiceConfirmation_Success_Payment"),invoice,accounttype); //"Your payment has been received by us. Invoice " + invoice + " has been generated for your PRO account.";
                       
                        string strDisplaytext = "";
                        if (orderInfo.ORDER_TYPE == "N")
                        {
                         //   strDisplaytext = Utilities.ResourceMessage("InvoiceConfirmation_Activation_Process"); //"To activate your account and experience Insighto, we would need you to authenticate your registration.</br> An email has been sent to your email box with a confirmation link. </br> Please visit your email box and click on the link to complete your registration process";
                            if (supportEmail != null && supportEmail.Length > 0)
                            {
                                strNavurl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserContactusURL().Replace("~/In/", ""), "UserId=" + userId);
                              //  strDisplaytext += String.Format(Utilities.ResourceMessage("InvoiceConfirmation_Support_Email"), strNavurl, supportEmail); //"If you experience any issue/ difficulty in registration/activation, please contact us at <a href='" + strNavurl + "'>" + supportEmail + "</a>";
                            }
                        }
                        else if ((orderInfo.ORDER_TYPE == "U"))
                        {
                            invoice_seperatetext.Visible = false;
                            divHeading.Visible = false;
                           // strDisplaytext = String.Format(Utilities.ResourceMessage("InvoiceConfiramtion_Renew_Acknowledge"), ConfigurationManager.AppSettings["RootURL"].ToString()); //"Thank you for renewing / upgrading your subscription to <a href='" + ConfigurationManager.AppSettings["RootURL"].ToString() + "' target='_blank'> Insighto.com</a>.<br/>We have received your payment and have sent you an email confirming the same.";
                        }
                       // txtActivate.InnerHtml = strDisplaytext;
                    }
                }
                else
                {
                   var returnValue = userService.UpgradeUserAccountHistory(userId, orderId, 1);
                    txtConfirm.InnerHtml = String.Format(Utilities.ResourceMessage("InvoiceConfirmation_ProAccount_Generated"),invoice,accounttype); //"Invoice " + invoice + " has been generated for your PRO account.";
                    string strDisplayText = "";
                    if (orderInfo.ORDER_TYPE == "N")
                    {
                        divHeading.Visible = true;
                   //     invoice_seperatetext.InnerHtml = Utilities.ResourceMessage("InvoiceConfirmation_ProAccount_Cheque"); //"An email has been sent to your email address along with instructions for making the payment.</br> Your Insighto PRO membership will be activated within 12 hours of receipt of cheque.</br></br> In the interim you can test-drive Insighto as a FREE user";
                        invoice_seperatetext.Visible = true;
                   //     strDisplayText = Utilities.ResourceMessage("InvoiceConfirmation_Activate_Email"); //"To activate your account and experience Insighto, we would need you to authenticate your registration.</br> An email has been sent to your email box with a confirmation link. </br> Please visit your email box and click on the link to complete your registration process";
                        if (supportEmail != null && supportEmail.Length > 0)
                        {
                            strNavurl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserContactusURL().Replace("~/In/", ""), "UserId=" + userId);
                   //         strDisplayText += String.Format(Utilities.ResourceMessage("InvoiceConfirmation_Issues_Support"), strNavurl, supportEmail); //"If you experience any issue/ difficulty in registration/activation, please contact us at <a href='" + strNavurl + "'>" + supportEmail + "</a>";
                        }
                    }
                    else if (orderInfo.ORDER_TYPE == "U")
                    {
                        invoice_seperatetext.Visible = false;
                        divHeading.Visible = false;
                     //   strDisplayText = Utilities.ResourceMessage("InvoiceConfirmation_Success_Thankyou"); //"Thank you for renewing / upgrading your subscription to <a href='" + ConfigurationManager.AppSettings["RootURL"].ToString() + "' target='_blank'> Insighto.com</a>.<br/>We have received your payment and have sent you an email confirming the same.";
                    }
                  //  txtActivate.InnerHtml = strDisplayText;
                    string fromEmail = ConfigurationManager.AppSettings["emailActivation"];
                    var sendEmailService = ServiceFactory.GetService<SendEmailService>();
                    string toMail = supportEmail;
                    sendEmailService.RegIstrationEmailAlert("Registration_Email_Alert", userId, toMail, fromEmail);    
                }


                if (succTrans == 1)
                {
                    DataSet dstxnstatus = sc.UpdateCCTxnStatus("completed", orderId);
                }

                string viewNavurl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserInvoiceURL(), "UserId=" + userId + "&OrderId=" + orderId);
                string printNavurl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserInvoiceURL(), "UserId=" + userId + "&OrderId=" + orderId + "&Print=1");
                hlk_viewinvoice.Attributes.Add("Onclick", "javascript: OpenModalPopUP('" + viewNavurl + "',690, 515, 'yes');");
                hlk_printinvoice.Attributes.Add("Onclick", "javascript:  window.open('" + printNavurl + "', 'Help', 'height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');");
            }
        }

        private void EmailAlert()
        {
 
        }
    }
}