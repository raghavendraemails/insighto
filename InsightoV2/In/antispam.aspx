﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true"
    CodeFile="antispam.aspx.cs" Inherits="In_antispam" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="contentPanelHeader">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div class="pageTitle">
                        <!-- page title -->
                        <asp:Label ID="lblTitle" runat="server" Text="Knowience Anti Spam Policy" 
                            meta:resourcekey="lblTitleResource1"></asp:Label>
                        <!-- //page title -->
                        
                    </div>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="defaultHeight">
        </div>
        <div id="divContent" runat="server"></div>
        <%--<table width="95%" border="0" cellpadding="0" cellspacing="0" class="textalignment"
            align="center">
            <tr>
                <td>
                    <p>
                        <strong>1. Spamming defined:</strong></p>
                    <p>
                        Spamming is an activity whereby email messages are directly or indirectly transmitted
                        to any email address that has not solicited such email and does not consent to such
                        transmission or receipts.
                    </p>
                    <p>
                        Knowience also consider spamming to constitute posting advertisements in newsgroups
                        in violation of the terms of participation in such newsgroup that are off topic,
                        or in newsgroups that do not specifically permit advertisements.
                    </p>
                    <p>
                        Knowience also consider it spamming when advertisements are placed on message boards
                        or in chat rooms when they are not permitted by the terms of participation in such
                        message boards and chat rooms.</p>
                    <p>
                        <strong>2. Anti Spam Policy</strong></p>
                    <p>
                        Any e-mail or message sent, or caused to be sent, to Knowience Users or through
                        the Knowience Services may not:
                    </p>
                    <ol>
                        <li>use or contain invalid or forged headers; </li>
                        <li>use or contain invalid or non-existent domain names; </li>
                        <li>employ any technique to otherwise misrepresent, hide or obscure any information
                            in identifying the point of origin or the transmission path; </li>
                        <li>use other means of deceptive addressing; </li>
                        <li>use a third party's internet domain name, or be relayed from or through a third
                            party's equipment, without permission of the third party; or</li>
                        <li>contain false or misleading information in the subject line or otherwise contain
                            false or misleading content.<br />
                        </li>
                    </ol>
                    <p>
                        Knowience does not authorize the harvesting, mining or collection of e-mail addresses
                        or other information from or through the Knowience Services. Knowience does not
                        permit or authorize others to use the Knowience Services to collect, compile or
                        obtain any information about its customers or subscribers, including but not limited
                        to subscriber e-mail addresses, which are Knowience's confidential and proprietary
                        information. Use of the Knowience Services is also subject to the Knowience Privacy
                        Statement and the Knowience Website Terms of Use and Notices.
                    </p>
                    <p>
                        Knowience does not permit or authorize any attempt to use the Knowience Services
                        in a manner that could damage, disable, overburden or impair any aspect of any of
                        the Knowience Services, or that could interfere with any other party's use and enjoyment
                        of any Knowience Service.
                    </p>
                    <p>
                        Knowience forbids the sending of unsolicited mass Emails or unsolicited Emails of
                        any kind in connection with use of Insighto.com.</p>
                    <p>
                        In the event that Knowience deems you to be in violation of these policies, Knowience
                        shall immediately revoke your membership rights and close any active account.
                    </p>
                    <p>
                        Knowience also reserve the right to suspend your account and participation pending
                        review upon receipt of any complaint or other evidence that you may be engaged in
                        any spamming activity. Knowience may initiate such legal action as it may deem necessary
                        or is advised or shall co-operate with any authority and provide all information,
                        material and or such other material and information as it may have access to, to
                        protect its rights as also of its clients, customers, users, and others who may
                        either directly or indirectly be effected in any way.<br />
                    </p>
                    <p>
                        <strong>3. Report</strong></p>
                    <p>
                        If you are &ldquo;spammed&rdquo; by anyone regarding our/their products, services,
                        website, or any other matters, please report this activity to <a href="mailto:compliance@insighto.com">
                            compliance@insighto.com</a>.
                    </p>
                    <p>
                        <br />
                    </p>
                </td>
            </tr>
        </table>--%>
        <div class="clear">
        </div>
        <!-- //content panel -->
    </div>
</asp:Content>
