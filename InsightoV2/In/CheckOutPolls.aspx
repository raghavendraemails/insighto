﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckOutPolls.aspx.cs" Inherits="Insighto.Pages.In.In_CheckOutPolls" meta:resourcekey="PageResource2" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc" TagName="TopLinks" Src="~/UserControls/TopLinks.ascx" %>
<%@ Register TagPrefix="uc" TagName="HeaderMenu" Src="~/In/UserControls/Header.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.6.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btnContinue').click(function () {
                $('#form1').submit();
            });
        });
    </script>
    <link href="../insighto.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link rel="Stylesheet" href="App_Themes/Classic/WebRupee.css" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/layoutStyle.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/facebox.css" />
    <link href="Styles/Banner_Style.css" rel="stylesheet" type="text/css" />
     <link href="Styles/notification.css" rel="stylesheet" type="text/css" />
    <style type="text/css" media="screen">
        @import "../images/homeimages/insighto.css";
    </style>
        <script type="text/javascript" src="../Scripts/facebox.js?v=0.0.8.1"></script>
        <script type="text/javascript" src="../Scripts/common-functions.js?v=0.0.8.1"></script>
    <title></title>
     <script type="text/javascript">

         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-29026379-1']);
         _gaq.push(['_setDomainName', 'insighto.com']);
         _gaq.push(['_setAllowLinker', true]);
         _gaq.push(['_trackPageview']);

         (function () {
             var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
             ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
             var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();

</script>
</head>
<body>
    <%--<form method="post" id="form1" name="form1" action="https://www.ccavenue.com/shopzone/cc_details.jsp" runat="server">--%>
    <form method="get" id="form1" name="form1" action="https://test.instamojo.com/insightotest/" runat="server">

	    <input type="hidden" id="intent" name="intent" value="buy"/> 
        <input type="hidden" id="data_Field_61558" name="data_Field_61558" value="" runat="server"/>
	    <input type="hidden" id="data_name" name="data_name" value="" runat="server"/> 
	    <input type="hidden" id="data_email" name="data_email" value="" runat="server"/> 
	    <input type="hidden" id="data_readonly2" name="data_readonly" value="Field_61558"/> 
	    <input type="hidden" id="data_readonly1" name="data_readonly" value="data_name"/> 
	    <input type="hidden" id="data_readonly" name="data_readonly" value="data_email"/> 


 	
    <div id="wrapper">
        <!-- main wrapper -->
        <!-- Top Links -->
        <uc:TopLinks ID="ucTopLinks" runat="server" />
        <!-- Top Links -->
        <!-- Header -->
        <uc:HeaderMenu ID="ucHeader" runat="server" />
        <!-- Header -->
        <div class="messagePanel">
            <!-- signup panel -->
            <div style="font-size:14px;font-weight:bold;font-family:Arial;margin-left:200px;">
                Purchase Information
                </div>
                <div>&nbsp;</div>
                <div >
                <table style="margin-left:130px;border:1px;">
                <tr><td style="font-size:14px;font-family:Arial;">Plan chosen :</td><td style="font-size:14px;font-family:Arial;"><asp:Label ID="lblpc" runat="server" ></asp:Label></td></tr>
                <tr><td style="font-size:14px;font-family:Arial;">Plan amount :</td><td style="font-size:14px;font-family:Arial;"><asp:Label ID="lblpa" runat="server" ></asp:Label></td></tr>
                <tr><td style="font-size:14px;font-family:Arial;">Service Tax(@<asp:Label ID="lbltaxpercentage" runat="server"></asp:Label>) :</td><td style="font-size:14px;font-family:Arial;"><asp:Label ID="lblst" runat="server" ></asp:Label></td></tr>
                <tr><td style="font-size:14px;font-family:Arial;">Total :</td><td style="font-size:14px;font-family:Arial;"><asp:Label ID="lbltotal" runat="server" ></asp:Label></td></tr>
                </table>                
             </div>
             <div>&nbsp;</div>
             <p>
                <asp:Label ID="lblInfo" runat="server" Text=" You are now being redirected to a Secured Payment page. You may choose any of the 
                payment methods given and provide the necessary details." 
                    meta:resourcekey="lblInfoResource1"></asp:Label>
            </p>
         
            <br />
          
            <p style="text-align:center;">
                <asp:HyperLink ID="btnContinue" runat="server" Text="Continue" CssClass="dynamicButton"
                    ToolTip="Continue" meta:resourcekey="btnContinueResource1"/>
            </p>
        </div>
        <uc:Footer ID="ucFooter" runat="server" />
        <!-- //main wrapper -->
    </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.alreadySignIn').hide();

            $('a[rel*=dropmenu1_a]').click(function (e) {
            e.preventDefault();
            ApplyFrameBox($(this));
        });
    });

    function ApplyFrameBox(link) {
        var frameWidth = $(link).attr('w');
        var frameHeight = $(link).attr('h');
        var scrolling = $(link).attr('scrolling');
        var href = $(link).attr('href');
        if (!scrolling) {
            scrolling = 'no';
        }

        $.facebox($('<iframe id="modalFrame" name="modalFrame" src="' + href + '" width="' + frameWidth + '" height="' + frameHeight + '" frameborder="0" scrolling="' + scrolling + '"></iframe>'));

        return false;
    }

</script>


<!-- Google Code for Purchase intent - Cheque / Direct Deposit Conversion Page --> 
<script type="text/javascript">
// <![CDATA[
    var google_conversion_id = 1027814833;
    var google_conversion_language = "en";
    var google_conversion_format = "2";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "dEWdCImw7gIQseuM6gM";
    var google_conversion_value = 0;
// ]]> 
</script>
<script type="text/javascript"  src="https://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  src="https://www.googleadservices.com/pagead/conversion/1027814833/?value=0&amp;label=b0t1CJnf8wYQseuM6gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript">
    $(document).ready(function () {
        $("#ucHeader_tdmainmenu").hide();
        $("#ucHeader_signin_menu").hide();
        $(".signin").click(function (e) {
            e.preventDefault();
            $("div.signin_menu").toggle();
            $(".signin").toggleClass("menu-open");
            $('.userName').focus();
        });

        $("div.signin_menu").mouseup(function () {
            return false
        });
    });
</script>
</body>
</html>