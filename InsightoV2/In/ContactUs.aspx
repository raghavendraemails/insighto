﻿<%@ Page Title="Online Survey Questionnaire Software – Contact Us" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true"
    CodeFile="ContactUs.aspx.cs" Inherits="Insighto.Pages.In.ContactUs" %>

<%@ Register TagPrefix="uc" TagName="Advertizement" Src="~/In/UserControls/RightAdvertizement.ascx" %>
<%@ Register Src="~/UserControls/ContactUsControl.ascx" TagName="ContactUsControl"
    TagPrefix="uc1" %>
   
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<meta name="description" content="Contact us for the best online survey questionnaire software and tools. Signup for free." />
<meta name="keywords" content="online survey questionnaire software, online survey software, survey software" />
    <table border="0" width="100%">
        <tr>
            <td>
                <div >
                    <uc1:ContactUsControl ID="ContactUsControl1" runat="server" />
                </div>
            </td>
         
        </tr>
    </table>
    <div class="clear">
    </div>
     <style type="text/css">
    
    .takeATourRightPanel1 
{
	width:195px; 
	float:right; 
	margin:20px 20px 0 0;
}
    </style>
</asp:Content>
