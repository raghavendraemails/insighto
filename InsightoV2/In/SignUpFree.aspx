﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignUpFree.aspx.cs" Inherits="Insighto.Pages.In.In_SignUpFree"
    Culture="auto" UICulture="auto" %>

<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<%--<%@ Register TagPrefix="uc" TagName="Advertizement" Src="~/In/UserControls/RightAdvertizement.ascx" %>--%>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">--%>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/In/UserControls/HeaderOld.ascx" %>
<%--<%@ Register TagPrefix="uc" TagName="Footer" Src="~/In/UserControls/Footer.ascx" %>--%>
<%--<%@ Register Src="~/UserControls/LoginControl.ascx" TagPrefix="uc" TagName="Login"  %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Free Online Survey and Questionnaire Software for Customer Research and Feedback
    </title>
    <meta id="meta1" name="description" content="Smart and Professional Online Survey Software Tool - Create Online 
Surveys – Insighto." />
    <meta id="meta2" name="keywords" content="Insighto, online survey, create online surveys, online survey tool, 
free surveys, internet survey, web survey software, Free Online Survey, Free Online Surveys, survey site, survey website, 
e-survey, customer satisfaction questionnaire, employee satisfaction, conduct survey, market research, analyze survey 
results" />
    <link rel="shortcut icon" runat="server" id="lnkFavIcon" type="image/vnd.microsoft.icon" />
    <link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/layoutStyle.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/facebox.css" />
    <link href="../Styles/Banner_Style.css" rel="stylesheet" type="text/css" />
    <style type="text/css" media="screen">
        @import "../images/homeimages/insighto.css";
    </style>
    <script type="text/javascript" src="/Scripts/jquery-1.6.2.js"></script>
    <script type="text/javascript" src="/Scripts/facebox.js"></script>
    <script type="text/javascript" src="/Scripts/common-functions.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".signin").click(function (e) {
                e.preventDefault();
                $("div.signin_menu").toggle();
                $(".signin").toggleClass("menu-open");
            });

            $("div.signin_menu").mouseup(function () {
                return false
            });

        });
    </script>
   <script type="text/javascript">

       var _gaq = _gaq || [];
       _gaq.push(['_setAccount', 'UA-29026379-1']);
       _gaq.push(['_setDomainName', 'insighto.com']);
       _gaq.push(['_setAllowLinker', true]);
       _gaq.push(['_trackPageview']);

       (function () {
           var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
           ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
           var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
       })();

</script>

</head>
<body>
    <div id="wrapper">
        <form id="form1" runat="server">
        <div>
            <div class="header">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <!-- header -->
                    <tr>
                        <td colspan="5">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <div class="logoHeader">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="10">
                                        </td>
                                        <td align="left" width="185">
                                            <a href="../../Home.aspx" target="_blank">
                                                <img src="../../App_Themes/Classic/Images/Insighto_logo.png" border="0" alt="" /></a>
                                        </td>
                                        <td align="right">
                                            <%--   <uc:Login ID="Login1" runat="server" />--%>
                                        </td>
                                        <td width="10">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <!-- //header -->
                </table>
            </div>
            <div>
                <link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/layoutStyle.css?v=0.0.8.1" />
                <link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/facebox.css?v=0.0.8.1" />
                <script type="text/javascript" src="../Scripts/facebox.js?v=0.0.8.1"></script>
                <script type="text/javascript" src="../Scripts/common-functions.js?v=0.0.8.1"></script>
            </div>
            <table border="0">
                <tr>
                    <td valign="top">
                        <div class="marginNewInnerPanel">
                            <div class="benefitBannerPanel">
                                <!-- free benefits panel -->
                                <div class="benefitBannerTop">
                                </div>
                                <h1>
                                    <asp:Label ID="lblSignup" runat="server" Text=" Sign up now for your subscription. Takes just one minute and it's <b>Free!</b>"></asp:Label>
                                </h1>
                                <div class="benefitBannerText benefitHeight">
                                    <div class="benefitPointsPanel">
                                        <ul>
                                            <li>
                                                <asp:Label ID="lblUnlimitedSurveys" runat="server" meta:resourcekey="lblUnlimitedSurveys"></asp:Label></li>
                                            <li>
                                                <asp:Label ID="lblResponsesPerSurvey" runat="server" meta:resourcekey="lblResponsesPerSurvey"></asp:Label></li>
                                            <li>
                                                <asp:Label ID="lblQuestionTypes" runat="server" meta:resourcekey="lblQuestionTypes"></asp:Label></li>
                                        </ul>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="benefitPointsPanelTwo">
                                        <ul>
                                         <li>
                                                <asp:Label ID="useskip" runat="server"  Text="Use skip logic"></asp:Label></li>
                                            <li>
                                                <asp:Label ID="lblFreeSurvey" runat="server" meta:resourcekey="lblFreeSurvey"></asp:Label></li>
                                            <li>
                                                <asp:Label ID="lblNoCreditCard" runat="server" meta:resourcekey="lblNoCreditCard"></asp:Label></li>
                                        </ul>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <!-- //free benefits panel -->
                            </div>
                            <div clas="clear">
                            </div>
                            <!-- signup panel -->
                            <div class="signUpPnl">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <h3>
                                                <asp:Label ID="lblSignupSubscrption" runat="server" meta:resourcekey="lblSignupSubscrption"></asp:Label>
                                            </h3>
                                        </td>
                                        <td class="signbg">
                                            <asp:Label ID="lblAlreadyAccount" runat="server" meta:resourcekey="lblAlreadyAccount"></asp:Label>
                                            <%--<a href="../Home.aspx" class="orangeColor"><b>Sign in</b></a>--%>
                                            <asp:HyperLink ID="hlnkPreview" runat="server" CssClass="orangeColor" rel="framebox"
                                                Text="Sign In" h="400" w="600" scrolling='yes' NavigateUrl="~/SignIn.aspx" ToolTip="Sign In"
                                                alt="Sign In" title="Sign In" />
                                        </td>
                                    </tr>
                                </table>
                                <div >
                                    <div class="clear">
                                    </div>
                                    <div class="lt_padding">
                                        <asp:Label ID="requiredfield" CssClass="left_requirelbl" meta:resourcekey="lblrequiredfield"
                                            runat="server"></asp:Label></div>
                                    <div style="margin: 0;">
                                        <!-- sign up free -->
                                        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                                            <div>
                                                <asp:Label ID="lblErrorMsg" runat="server"></asp:Label></div>
                                        </div>
                                        <div class="con_login_pnl">
                                            <div class="loginlbl">
                                                <asp:Label ID="Label2" runat="server" AssociatedControlID="lblErrMsg" />
                                            </div>
                                            <div class="loginControls">
                                                <asp:Label ID="lblErrMsg" runat="server" CssClass="lblRequired" Visible="False" meta:resourcekey="lblErrMsgResource1">
                                                </asp:Label>
                                            </div>
                                        </div>
                                        <div class="con_login_pnl">
                                            <div class="loginlbl">
                                                <asp:Label ID="lblName" runat="server" CssClass="requirelbl" AssociatedControlID="txtName"
                                                    meta:resourcekey="lblNameResource1" />
                                            </div>
                                            <div class="loginControls">
                                                <asp:TextBox ID="txtName" class="textBoxMedium" MaxLength="30" runat="server"
                                                    Style="color: #888;" />
                                            </div>
                                        </div>
                                        <div class="con_login_pnl">
                                            <div class="reqlbl btmpadding" id="reqlastname" runat="server">
                                                <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqName" CssClass="lblRequired"
                                                    ControlToValidate="txtName" meta:resourcekey="lblReqNameResource1" runat="server"
                                                    Font-Underline="False" Font-Strikeout="False" Display="Dynamic">
                                                </asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator SetFocusOnError="true" ID="regName" CssClass="lblRequired"
                                                    ControlToValidate="txtName" runat="server" meta:resourceKey="lblRegNameResource"
                                                    ValidationExpression="^[a-zA-Z ]*$" Display="Dynamic">
                                                </asp:RegularExpressionValidator></div>
                                        </div>
                                    <div class="con_login_pnl" id="divlastname" runat="server">
                                    <div class="loginlbl">
                                        <asp:Label ID="lblUserName" runat="server" CssClass="requirelbl" AssociatedControlID="txtUserName"
                                            meta:resourcekey="lblUserNameResource" />
                                    </div>
                                    <div class="loginControls">
                                        <asp:TextBox ID="txtUserName" runat="server" TabIndex="2" CssClass="textBoxMedium"
                                            MaxLength="40"></asp:TextBox>
                                    </div>
                                </div> 
                                        <div class="con_login_pnl">
                                    <div class="reqlbl btmpadding">


                                     <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqUsername" CssClass="lblRequired"
                                                    ControlToValidate="txtUserName"  meta:resourcekey="lblReqNameResource2" runat="server"
                                                    Font-Underline="False" Font-Strikeout="False" Display="Dynamic">
                                                </asp:RequiredFieldValidator>
                                              
                                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="RexUsername" meta:resourceKey="lblRegNameResource2"
                                            CssClass="lblRequired" ControlToValidate="txtUserName" runat="server" Display="Dynamic"
                                             ValidationExpression="^[a-zA-Z ]*$" ></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                        <div class="con_login_pnl">
                                            <div class="loginlbl">
                                                <asp:Label ID="lblEmailId" CssClass="requirelbl" runat="server" AssociatedControlID="txtEmailId"
                                                    meta:resourcekey="lblEmailIdResource1" />
                                            </div>
                                            <div class="loginControls">
                                                <asp:TextBox ID="txtEmailId" class="textBoxMedium" MaxLength="40" runat="server"
                                                    Style="color: #888;"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="con_login_pnl">
                                            <div class="reqlbl btmpadding">
                                                <div id="Nogap">
                                                    <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqEmailId" ControlToValidate="txtEmailId"
                                                        CssClass="lblRequired" meta:resourcekey="lblReqEmailIdResource1" runat="server"
                                                        Display="Dynamic">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator SetFocusOnError="true" ID="regEmailId" runat="server"
                                                        ErrorMessage="" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        CssClass="lblRequired" ControlToValidate="txtEmailId" meta:resourcekey="lblRegEmailIdResource1"
                                                        Display="Dynamic">
                                                    </asp:RegularExpressionValidator></div>
                                                <asp:Label ID="lblEmailRequired" runat="server" class="tip" meta:resourcekey="lblEmailRequired"></asp:Label>
                                            </div>
                                            <div class="con_login_pnl" id="divPassword" runat="server">
                                                <div class="loginlbl">
                                                    <asp:Label ID="lblPassword" runat="server" CssClass="requirelbl" AssociatedControlID="txtPassword"
                                                        Text="Password" />
                                                </div>
                                                <div class="loginControls">
                                                    <asp:TextBox ID="txtPassword" runat="server" TabIndex="3" TextMode="Password" MaxLength="16"
                                                        CssClass="textBoxMedium" Style="color: #888;" ></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="con_login_pnl">
                                                <div class="reqlbl btmpadding" id="divPasswordValidation" runat="server">
                                                    <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqPassword" meta:resourcekey="lblReqPasswordResource1"
                                                        CssClass="lblRequired" ControlToValidate="txtPassword" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator SetFocusOnError="true" ID="regNewPassword" CssClass="lblRequired"
                                                        ControlToValidate="txtPassword" runat="server" meta:resourcekey="lblLenPasswordResource2"
                                                        ValidationExpression="^.{6,16}$" Display="Dynamic"></asp:RegularExpressionValidator>
                                                    <asp:CustomValidator SetFocusOnError="true" ID="cvNewPassword" Display="Dynamic"
                                                        runat="server" ControlToValidate="txtPassword" CssClass="lblRequired" meta:resourcekey="lblLenPasswordResource1"
                                                        ClientValidationFunction="ValidatePassword">
                                                    </asp:CustomValidator>
                                                </div>
                                            </div>
                                            <div class="con_login_pnl" runat="server" id="divPasswordhelp">
                                                <div class="loginlbl">
                                                    <asp:Label ID="lblPasswordHelp" runat="server" AssociatedControlID="lblPasswordHelpText"
                                                        meta:resourcekey="lblPasswordHelpResource1" />
                                                </div>
                                                <div class="loginControls">
                                                    <asp:Label ID="lblPasswordHelpText" CssClass="note" runat="server" meta:resourcekey="lblPasswordHelpTextResource" />
                                                </div>
                                            </div>
                                            <div class="con_login_pnl" id="divconfirmpassword" runat="server">
                                                <div class="loginlbl">
                                                    <asp:Label ID="lblConfirmPassword" runat="server" CssClass="requirelbl" AssociatedControlID="txtConfirmPassword"
                                                        Text="Re-type Password" />
                                                </div>
                                                <div class="loginControls">
                                                    <asp:TextBox ID="txtConfirmPassword" TabIndex="4" runat="server" TextMode="Password"
                                                        MaxLength="16" CssClass="textBoxMedium" Style="color: #888;" ></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="con_login_pnl" id="divConfirmPasswordValidation" runat="server">
                                                <div class="reqlbl btmpadding">
                                                    <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqConfirmPassword" meta:resourcekey="lblReqConfirmPasswordResource1"
                                                        CssClass="lblRequired" ControlToValidate="txtConfirmPassword" runat="server"
                                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="cmpPassword" meta:resourcekey="lblcmpConfirmPasswordResource1"
                                                        CssClass="lblRequired" ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword"
                                                        Operator="Equal" runat="server" SetFocusOnError="true"></asp:CompareValidator></div>
                                            </div>
                                            <%--  <div class="con_login_pnl">
                                        <div class="loginlbl">
                                            <asp:Label ID="Label1" runat="server" AssociatedControlID="reCaptchaimg" /><asp:Label
                                                ID="lblRecaptcha" runat="server" AssociatedControlID="txtCaptcha" meta:resourcekey="lblRecaptchaResource1" /></div>
                                        <div class="loginControls">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <img src="~/In/Captcha.aspx" id="reCaptchaimg" alt="Catptcha Text" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblTypeAbove" runat="server" meta:resourcekey="lblTypeAbove"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtCaptcha" CssClass="textBoxSmall" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>--%>
                                            <div class="con_login_pnl">
                                                <div class="reqlbl btmpadding">
                                                    By signing up, you agree to the <a id="lnktermsandcondtions" runat="server" class="defaultlink">
                                                        Terms &amp; Conditions</a> and <a id="lnkPrivacyPolicy" runat="server" class="defaultlink">
                                                            Privacy Policy.</a>
                                                </div>
                                            </div>
                                            
                                            <div class="con_login_pnl">
                                                <div class="loginlbl">
                                                    <asp:Label ID="lblCreateAmt" runat="server" AssociatedControlID="btnCreateAccount" /></div>
                                                <div class="loginControls">
                                                    <asp:ImageButton ID="btnCreateAccount" runat="server" OnClick="btnCreateAccount_Click"
                                                        ImageUrl="~/App_Themes/Classic/Images/button_sign_up.png" />
                                                </div>
                                            </div>
                                             <div class="clear">
                                        </div>
                                           
                                            <!-- //div row -->

                                        </div>
                                        <div class="clear">
                                        </div>
                                        <!-- sign up free -->
                                    </div>
                                    <div class="clear">
                                    </div>
                                     
                                   

                                    <%--<div class="socialMediaPanel">
				<h3>Sign Up with Your Favorite Account</h3>
				<p>&nbsp;</p>
				<p align="center">
					Now you can link your accounts and sign up to <b>Insighto</b> using your Facebook or Google Account.
				</p>
				<div style="width:40%; margin:20px auto 0 auto;">
				
				<div class="div_row"><!-- div row -->
					<div class="fLt div_lbl">
					
					</div>
					<div class="fLt">
						<table>
							<tr>
								<td>
								 <asp:ImageButton ID="imgbtnFBLogin" runat="server" ImageUrl="~/App_Themes/Classic/Images/icon-facebook.gif" OnClick="imgbtnFBLogin_Click" ValidationGroup="img"/></td>
								<td>&nbsp;&nbsp;<a href="#"><b>Facebook</b></a></td>
							</tr>
						</table>
						<div></div>
					</div>
					<div class="clr"></div>
				<!-- //div row --></div>
				
				<div class="div_row"><!-- div row -->
					<div class="fLt div_lbl">
					
					</div>
					<div class="fLt">
						<table>
							<tr>
								<td>
								<asp:ImageButton ID="imgbtnGoogleLogin" runat="server" ImageUrl="~/App_Themes/Classic/Images/icon-google.gif" ValidationGroup="img"  OnCommand="imgbtnGoodleLogin_Click" CommandArgument="https://www.google.com/accounts/o8/id" /></td>
								<td>&nbsp;&nbsp;<a href="#"><b>Google</b></a></td>
							</tr>
						</table>						
						<div></div>
					</div>
					<div class="clr"></div>
				<!-- //div row --></div>				
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			</div>--%>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td valign="top" style="padding-top: 15px;">
                        <%--  <uc:Advertizement ID="ucAdvertizement" runat="server" />--%>
                    </td>
                </tr>
            </table>
            <div class="clear">
            </div>
            <!-- footer -->
            <div class="footer">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="copyRights" nowrap>
                            <asp:Label ID="lblCopyRight" runat="server" Text="Copyright &copy; 2014 Knowience Insights"
                                meta:resourcekey="lblCopyRightResource1"></asp:Label>
                        </td>
                        <td align="right" style="padding-top: 18px;">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        </form>
    </div>
</body>
</html>
<script type="text/javascript">
    function onlyAlphabets(e) {
        var key = window.event ? e.keyCode : e.which;
        if ((key >= 65 && key <= 90) || (key >= 97 && key <= 122) || key == 8 || key == 32) {
            return true;
        }
        return false;
    }
    function ValidatePassword(oSrc, args) {

        var l = /^\w*(?=\w*[a-z])\w*$/
        var u = /^\w*(?=\w*[A-Z])\w*$/
        var d = /^\w*(?=\w*\d)\w*$/
        var lower = /^.*(?=.*[a-z]).*$/
        var upper = /^.*(?=.*[A-Z]).*$/
        var digit = /^.*(?=.*\d).*$/
        var spl = /^.*(?=.*[@#$%^&+=]).*$/
        var cnt = 0;
        if (lower.test(args.Value)) { cnt = cnt + 1; }
        if (upper.test(args.Value)) { cnt = cnt + 1; }
        if (digit.test(args.Value)) { cnt = cnt + 1; }
        if (spl.test(args.Value)) { cnt = cnt + 1; }
        if (args.Value.length >= 6) {
            if (cnt < 2) {

                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }
        else {
            args.IsValid = true;
        }
    }
    function inputFocus(i) {
        if (i.value == i.defaultValue) { i.value = ""; i.style.color = "#000"; }
    }
    function inputBlur(i) {
        if (i.value == "") { i.value = i.defaultValue; i.style.color = "#888"; }
    }

</script>
<!--Start of Zopim Live Chat Script-->

<script type="text/javascript">

    window.$zopim || (function (d, s) {
        var z = $zopim = function (c) { z._.push(c) }, $ = z.s =

d.createElement(s), e = d.getElementsByTagName(s)[0]; z.set = function (o) {
    z.set.

_.push(o)
}; z._ = []; z.set._ = []; $.async = !0; $.setAttribute('charset', 'utf-8');

        $.src = '//cdn.zopim.com/?bYM7XxoDiH1Gv57aQNwmeiatKZbOCdA7'; z.t = +new Date; $.

type = 'text/javascript'; e.parentNode.insertBefore($, e)
    })(document, 'script');

</script>

<!--End of Zopim Live Chat Script-->
<%--</asp:Content>--%>
