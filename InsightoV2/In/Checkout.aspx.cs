﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using App_Code;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Data;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using System.Globalization;
using System.Data;
using System.Configuration;

namespace Insighto.Pages.In
{
    public partial class In_Checkout : BasePage
    {
        int userID = 0;
        double amount = 0;
        int newOrder = 0;
        int pkOrder = 0;
        string orderNo = "";
        string workingKey = "";
        string chkBillCName = "";
        string chkemail = "";
        string chkCity = "";
        string chkcountry = "";
        string chkphoneno = "";
        string strplanchosen = "";
        string strplanamt = "";
        string strtaxamount = "";
        string strtaxpercentage = "14.5%";

        string strmerchantplan = "";
        string latestOrderNo = "";
        double Tax = 0;
        double taxAmount = 0;
        double Total = 0;
        double Price = 0;
        string orderType = "";
        string orderID = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            strtaxpercentage = ConfigurationManager.AppSettings["taxpercentage"].ToString();
            if (Request.QueryString["key"] != null)
            {
                Hashtable ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("Userid"))
                {
                    userID = ValidationHelper.GetInteger(ht["Userid"].ToString(), 0);
                }
                if (ht != null && ht.Count > 0 && ht.Contains("Amount"))
                {
                    amount = ValidationHelper.GetDouble(ht["Amount"].ToString(), 0);
                }
                if (ht != null && ht.Count > 0 && ht.Contains("Order"))
                {
                    orderNo = ht["Order"].ToString();
                }
                if (ht != null && ht.Count > 0 && ht.Contains("newOrder"))
                {
                    newOrder = ValidationHelper.GetInteger(ht["newOrder"].ToString(), 0);
                }
                if (ht != null && ht.Count > 0 && ht.Contains("pk_order"))
                {
                    pkOrder = ValidationHelper.GetInteger(ht["pk_order"].ToString(), 0);
                }
                 if (ht != null && ht.Count > 0 && ht.Contains("BillCName"))
                {
                     chkBillCName = ht["BillCName"].ToString();
                 }

                 if (ht != null && ht.Count > 0 && ht.Contains("email"))
                {
                    chkemail = ht["email"].ToString();
                 }
                 if (ht != null && ht.Count > 0 && ht.Contains("City"))
                 {
                     chkCity = ht["City"].ToString();
                 }
                 if (ht != null && ht.Count > 0 && ht.Contains("country"))
                 {
                     chkcountry = ht["country"].ToString();
                 }
                 if (ht != null && ht.Count > 0 && ht.Contains("phoneno"))
                 {
                     chkphoneno = ht["phoneno"].ToString();
                 }
                 if (ht != null && ht.Count > 0 && ht.Contains("planchosen"))
                 {
                     strplanchosen = ht["planchosen"].ToString();
                 }
                 if (ht != null && ht.Count > 0 && ht.Contains("planamount"))
                 {
                     strplanamt = ht["planamount"].ToString();
                 }
                 if (ht != null && ht.Count > 0 && ht.Contains("taxamount"))
                 {
                     strtaxamount = ht["taxamount"].ToString();
                 }

                 if (ht != null && ht.Count > 0 && ht.Contains("taxpercentage"))
                 {
                     if (!String.IsNullOrWhiteSpace(ht["taxpercentage"].ToString()))
                     {
                         strtaxpercentage = ht["taxpercentage"].ToString();
                     }
                 }

                 lblpc.Text = strplanchosen;
                 lblpa.Text = "&#8377;" + strplanamt;
                 lblst.Text = "&#8377;" + strtaxamount;
                 lbltotal.Text = "&#8377;" + amount.ToString();
                 lbltaxpercentage.Text = strtaxpercentage;
                
                if (newOrder > 0)
                {
                    var userService = ServiceFactory.GetService<UsersService>();
                    var orderService = ServiceFactory.GetService<OrderInfoService>();
                    var orderList = new List<osm_orderinfo>();
                    var orderInfo = orderService.GetOrderInfo(pkOrder, userID);
                    foreach (var ordInfo in orderInfo)
                    {
                        latestOrderNo = ordInfo.ORDERID;
                        Tax = ValidationHelper.GetDouble(ordInfo.TAX_PERCENT.ToString(), 0);
                        taxAmount = ordInfo.TAX_PRICE;
                        Total = ordInfo.TOTAL_PRICE;
                        Price = ordInfo.PRODUCT_PRICE;
                        orderType = ordInfo.ORDER_TYPE;
                    }
                    int index = latestOrderNo.IndexOf("-");
                    latestOrderNo = latestOrderNo.Substring(0, index + 2);
                    osm_orderinfo order = new osm_orderinfo();
                    if (DateTime.UtcNow.Month < 10)
                        latestOrderNo += "0" + DateTime.UtcNow.Month;
                    else
                        latestOrderNo += DateTime.UtcNow.Month;
                    latestOrderNo += Convert.ToString(DateTime.UtcNow.Year).Substring(2);
                    order.TAX_PERCENT = Tax;
                    order.TAX_PRICE = taxAmount;
                    order.TOTAL_PRICE = Total;
                    order.USERID = userID;
                    order.PRODUCT_PRICE = Price;
                    order.MODEOFPAYMENT = 1;
                    order.ORDERID = latestOrderNo;
                    order.ORDER_TYPE = orderType;
                    order.CREATED_DATE = DateTime.UtcNow;
                    order.TRANSACTION_STATUS = 0;
                    orderList.Add(order);
                    order = userService.InsertOrderInfo(order);
                    if (order != null)
                    {
                        var historyList = new List<osm_useraccounthistory>();
                        osm_useraccounthistory history = new osm_useraccounthistory();
                        history.USERID = userID;
                        history.ORDERID = order.PK_ORDERID.ToString();
                        history.EMAIL_FLAG = 0;
                        history.CREATED_DATE = DateTime.UtcNow;
                        history.CURRENT_LICENSETYPE = orderType;
                        history.LICENSETYPE_OPTED = orderType;
                        pkOrder = order.PK_ORDERID;
                        historyList.Add(history);
                        var historyId = userService.InsertUserAccountHistory(history);
                        orderID = latestOrderNo + order.PK_ORDERID.ToString();
                        Order_Id.Value = orderID;
                    }
                }
                else
                {
                    Order_Id.Value = orderNo;
                    //BELOW CODE AS WE NEED THE ORDER ID WHEN PASSING TO INSTAMOJO
                    var orderService = ServiceFactory.GetService<OrderInfoService>();
                    var orderList = new List<osm_orderinfo>();
                    orderList = orderService.GetOrderInfoByUserId(userID);
                    var thisorder = orderList.Where(a => a.ORDERID == orderNo).ToList();
                    if (thisorder.Count() > 0) {
                        pkOrder = thisorder[0].PK_ORDERID;
                    }
                }
                List<string> listConfig = new List<string>();
                listConfig.Add(Constants.MERCHANTID);
                listConfig.Add(Constants.REDIRECTIONURL);
                listConfig.Add(Constants.WORKINGKEY);
                var configService = ServiceFactory.GetService<ConfigService>();
                var configvalue = configService.GetConfigurationValues(listConfig);
                foreach (var cv in configvalue)
                {
                    if (cv.CONFIG_KEY == Constants.MERCHANTID.ToUpper())
                    {
                        Merchant_Id.Value = cv.CONFIG_VALUE;
                    }
                    if (cv.CONFIG_KEY == Constants.REDIRECTIONURL)
                    {
                        Redirect_Url.Value = cv.CONFIG_VALUE;
                    }
                    if (cv.CONFIG_KEY == Constants.WORKINGKEY)
                    {
                        workingKey = cv.CONFIG_VALUE;
                    }
                }
                Amount1.Value = Convert.ToString(Math.Round(Convert.ToDouble(amount), 2));
             
                billing_cust_name.Value = chkBillCName;
                billing_cust_address.Value = "";
                billing_cust_country.Value = chkcountry;
                billing_cust_city.Value = chkCity;
                billing_cust_state.Value = "";
                billing_cust_tel.Value = chkphoneno;
                billing_cust_email.Value = chkemail;
                delivery_cust_name.Value = "";
                delivery_cust_address.Value = "";
                delivery_cust_country.Value = "";
                delivery_cust_state.Value = "";
                delivery_cust_tel.Value = "";
                delivery_cust_notes.Value = "";
                Merchant_Param.Value = strplanchosen;
                billing_zip_code.Value = "";
                delivery_zip_code.Value = "";               
                delivery_cust_city.Value = "";

                string paymentgatewaylive = ConfigurationManager.AppSettings["PaymentGatewayLive"].ToString();
                string paymentgatewaytest = ConfigurationManager.AppSettings["PaymentGatewayTest"].ToString();
                string paymentgatewaymode = ConfigurationManager.AppSettings["PaymentGatewayMode"].ToString();
                string navigatelinkquerystring = "";//"https://test.instamojo.com/insightotest/" + strmerchantplan + "/?";
                string navigatelink = "";//"https://test.instamojo.com/insightotest/" + strmerchantplan + "/?";
                if (paymentgatewaymode == "LIVE")
                {
                    navigatelink = paymentgatewaylive;
                }
                else
                {
                    navigatelink = paymentgatewaytest;
                }

                navigatelinkquerystring = "intent=buy";
                navigatelinkquerystring += "&data_readonly=data_email&data_readonly=data_name";
                navigatelinkquerystring += "&data_email=" + chkemail + "&data_name=" + chkBillCName;

                PollCreation pc = new PollCreation();
                DataSet ds = pc.getInstaMojoForPolls(strplanchosen);
                strmerchantplan = ds.Tables[0].Rows[0]["InstaMojoProductSlug"].ToString();
                for (int x = 0; x < ds.Tables[1].Rows.Count; x++)
                {
                    var thiscustomfieldname = ds.Tables[1].Rows[x]["CustomFieldName"].ToString();
                    var thiscustomfieldvariable = ds.Tables[1].Rows[x]["CustomFieldVariable"].ToString();
                    navigatelinkquerystring += "&data_readonly=data_" + thiscustomfieldname;
                    if (thiscustomfieldvariable == "pkOrder")
                    {
                        navigatelinkquerystring += "&data_" + thiscustomfieldname + "=" + pkOrder.ToString();
                    }
                    else if (thiscustomfieldvariable == "strplanamt")
                    {
                        navigatelinkquerystring += "&data_" + thiscustomfieldname + "=" + strplanamt.ToString();
                    }
                    else if (thiscustomfieldvariable == "strtaxamount")
                    {
                        navigatelinkquerystring += "&data_" + thiscustomfieldname + "=" + strtaxamount.ToString();
                    }

                    else if (thiscustomfieldvariable == "amount")
                    {
                        navigatelinkquerystring += "&data_" + thiscustomfieldname + "=" + amount.ToString();
                    }

                }
                btnContinue.NavigateUrl = navigatelink + strmerchantplan + "?" + navigatelinkquerystring;


                //    delivery_cust_name = "Satya";
                //                User Id / Merchant Id :    M_Insighto_10177 
                //Current Working Key :    0ti3w0309y32avqr3a 

                // Currency.Value = "INR"; // new RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name).ISOCurrencySymbol;

                //Currency.Value =  new RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name).ISOCurrencySymbol;

                //Checksum.Value = Utilities.Getchecksum(Merchant_Id.Value, Order_Id.Value, amount.ToString(), Redirect_Url.Value, workingKey.ToString());
                //if (newOrder > 0)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "submitform", "document.form1.submit()", true);
                //}

            }
        }
    }
}