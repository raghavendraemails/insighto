﻿using System;
using System.IO;
using App_Code;
using Insighto.Business.Services;
using System.Data;
using Insighto.Data;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using CurrencyExchangeRates;
using Resources;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;


public partial class In_SaasyReturnPage : System.Web.UI.Page
{
    string SubscriptionType;
    string countryName;
    string orderNo = "PRO";
    string orderId = "";
    string saasyOrderReference = "";
    string saasyStatus = "";
    string strAddressCountry = "";
    string strCustomerPhone = "";
    string strAddressStreet1 = "";
    string strAddressStreet2 = "";
    string strAddressCity = "";
    string strAddressRegion = "";
    string strCustomerCompany = "";
    string strAddressPostalCode = "";
    string strCustomerLastName = "";
    string strOrderItemSubscriptionReference = "";
    string strordersubscriptionstatus = "";
    string strproductname = "";

    SurveyCore surcore = new SurveyCore();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Params.Keys.Count > 0)
            {
             //   using (StreamWriter outfile =
             //new StreamWriter(Server.MapPath("~/") + "Remotecall.txt"))
             //   {
             //       foreach (string s in Request.Params.Keys)
             //       {
             //           outfile.Write(s.ToString() + ":" + Request.Params[s] + "<br>");
             //       }

             //   }
              //  SubscriptionType = Convert.ToString(Request.Params["OrderReferrer"]);
               // countryName = Convert.ToString(Request.Params["AddressCountry"]);
                //var user = GetUserDetails(Convert.ToString(Request.Params["Status"]));
                //user = ServiceFactory.GetService<UsersService>().SaveUser(user);
                PollCreation Pc = new PollCreation();
                if (Convert.ToString(Request.Params["Status"]) == "completed")
                {
                    using (StreamWriter outfile =
           new StreamWriter(Server.MapPath("~/") + "Remotecall.txt"))
                    {
                        foreach (string s in Request.Params.Keys)
                        {
                            outfile.Write(s.ToString() + ":" + Request.Params[s] + "<br>");
                        }

                    }

                    orderId = Convert.ToString(Request.Params["OrderReferrer"]);

                  
                  //  var orderInfo = ServiceFactory.GetService<OrderInfoService>().GetOrderInfoByOrderId(orderId).FirstOrDefault();
                  //  var returnValue = ServiceFactory<UsersService>.Instance.UpgradeUserInvoice(orderInfo.PK_ORDERID);

                    saasyOrderReference = Convert.ToString(Request.Params["OrderReference"]);
                    saasyStatus = Convert.ToString(Request.Params["Status"]);
                    strAddressCountry = Convert.ToString(Request.Params["AddressCountry"]);
                    strCustomerPhone = Convert.ToString(Request.Params["CustomerPhone"]);
                    strAddressStreet1 = Convert.ToString(Request.Params["AddressStreet1"]);
                    strAddressStreet2 = Convert.ToString(Request.Params["AddressStreet2"]);
                    strAddressCity = Convert.ToString(Request.Params["AddressCity"]);
                    strAddressRegion = Convert.ToString(Request.Params["AddressRegion"]);
                    strCustomerCompany = Convert.ToString(Request.Params["CustomerCompany"]);
                    strAddressPostalCode = Convert.ToString(Request.Params["AddressPostalCode"]);
                    strCustomerLastName = Convert.ToString(Request.Params["CustomerLastName"]);
                    strOrderItemSubscriptionReference = Convert.ToString(Request.Params["subscriptionurl"]);
                    strordersubscriptionstatus = Convert.ToString(Request.Params["ordersubscriptionstatus"]);
                    strproductname = Convert.ToString(Request.Params["productName"]);

                    using (StreamWriter outfile =
           new StreamWriter(Server.MapPath("~/") + "Remotecall.txt"))
                    {
                        outfile.Write("< br > before update");
                    }

                    if ((strproductname == "Business_Monthly") || (strproductname == "Business_Yearly") || (strproductname == "Professional_Monthly") || (strproductname == "Professional_Yearly") || (strproductname == "Publisher_Monthly") || (strproductname == "Publisher_Yearly"))
                    {
                      DataSet dsprice = updatePollSaasyOrderRef(orderId, saasyOrderReference, saasyStatus, strAddressCountry, strCustomerPhone, strAddressStreet1, strAddressStreet2, strAddressCity, strAddressRegion, strCustomerCompany, strAddressPostalCode, strCustomerLastName, strOrderItemSubscriptionReference);
                      using (StreamWriter outfile =
                                   new StreamWriter(Server.MapPath("~/") + "Remotecall.txt"))
                      {
                          outfile.Write("< br > update orderinfo");
                      }

                      Pc.UpdatePollLicenseType(Convert.ToInt32(dsprice.Tables[0].Rows[0][0].ToString()), strproductname.ToUpper(), 2);
                      using (StreamWriter outfile =
           new StreamWriter(Server.MapPath("~/") + "Remotecall.txt"))
                      {
                          outfile.Write("< br > update license type");
                      }

                      Pc.UpdatePollPaymentType(Convert.ToInt32(dsprice.Tables[0].Rows[0][0].ToString()), strproductname.ToUpper(), Convert.ToInt32(dsprice.Tables[1].Rows[0][0].ToString()));
                      using (StreamWriter outfile =
           new StreamWriter(Server.MapPath("~/") + "Remotecall.txt"))
                      {
                          outfile.Write("< br > update poll payment type");
                      }
                    }
                    else
                    {

                        DataSet dsprice = updateSaasyOrderRef(orderId, saasyOrderReference, saasyStatus, strAddressCountry, strCustomerPhone, strAddressStreet1, strAddressStreet2, strAddressCity, strAddressRegion, strCustomerCompany, strAddressPostalCode, strCustomerLastName, strOrderItemSubscriptionReference);
                    }

                    using (StreamWriter outfile =
          new StreamWriter(Server.MapPath("~/") + "Remotecall.txt"))
                    {
                        outfile.Write("< br > updated successfully");
                    }
                    
                    //var order = GetOrderInfoDetails(user.USERID);
                    //order = ServiceFactory.GetService<UsersService>().InsertOrderInfo(order);
                    //if (order != null)
                    //{
                    //    var history = GetAccountHistoryInfo(user.USERID, order.PK_ORDERID.ToString());
                    //    var historyId = ServiceFactory.GetService<UsersService>().InsertUserAccountHistory(history);
                    //    ServiceFactory.GetService<UsersService>().UpgradeUserInvoice(order.PK_ORDERID);
                    //}

                    
                }
                else
                {
                    using (StreamWriter outfile =
           new StreamWriter(Server.MapPath("~/") + "Remotecalldeclined.txt"))
                    {
                        foreach (string s in Request.Params.Keys)
                        {
                            outfile.Write(s.ToString() + ":" + Request.Params[s] + "<br>");
                        }

                    }

                    orderId = Convert.ToString(Request.Params["OrderReferrer"]);


                    //var orderInfo = ServiceFactory.GetService<OrderInfoService>().GetOrderInfoByOrderId(orderId).FirstOrDefault();
                    //var returnValue = ServiceFactory<UsersService>.Instance.UpgradeUserInvoice(orderInfo.PK_ORDERID);

                    saasyOrderReference = Convert.ToString(Request.Params["OrderReference"]);
                    saasyStatus = Convert.ToString(Request.Params["Status"]);
                    strAddressCountry = Convert.ToString(Request.Params["AddressCountry"]);
                    strCustomerPhone = Convert.ToString(Request.Params["CustomerPhone"]);
                    strAddressStreet1 = Convert.ToString(Request.Params["AddressStreet1"]);
                    strAddressStreet2 = Convert.ToString(Request.Params["AddressStreet2"]);
                    strAddressCity = Convert.ToString(Request.Params["AddressCity"]);
                    strAddressRegion = Convert.ToString(Request.Params["AddressRegion"]);
                    strCustomerCompany = Convert.ToString(Request.Params["CustomerCompany"]);
                    strAddressPostalCode = Convert.ToString(Request.Params["AddressPostalCode"]);
                    strCustomerLastName = Convert.ToString(Request.Params["CustomerLastName"]);
                    strOrderItemSubscriptionReference = Convert.ToString(Request.Params["subscriptionurl"]);
                    strordersubscriptionstatus = Convert.ToString(Request.Params["ordersubscriptionstatus"]);
                    strproductname = Convert.ToString(Request.Params["productName"]);

                    using (StreamWriter outfile =
          new StreamWriter(Server.MapPath("~/") + "Remotecalldeclined.txt"))
                    {
                        outfile.Write("< br > before update");
                    }

                    if ((strproductname == "Business_Monthly") || (strproductname == "Business_Yearly") || (strproductname == "Professional_Monthly") || (strproductname == "Professional_Yearly") || (strproductname == "Publisher_Monthly") || (strproductname == "Publisher_Yearly"))
                    {
                        DataSet dsprice = updatePollSaasyOrderRef(orderId, saasyOrderReference, saasyStatus, strAddressCountry, strCustomerPhone, strAddressStreet1, strAddressStreet2, strAddressCity, strAddressRegion, strCustomerCompany, strAddressPostalCode, strCustomerLastName, strOrderItemSubscriptionReference);


                        using (StreamWriter outfile =
              new StreamWriter(Server.MapPath("~/") + "Remotecalldeclined.txt"))
                        {
                            outfile.Write("< br > update orderinfo");
                        }

                        Pc.UpdatePollLicenseType(Convert.ToInt32(dsprice.Tables[0].Rows[0][0].ToString()), strproductname.ToUpper(), 2);

                        using (StreamWriter outfile =
             new StreamWriter(Server.MapPath("~/") + "Remotecalldeclined.txt"))
                        {
                            outfile.Write("< br > update licensetype");
                        }

                        Pc.UpdatePollPaymentType(Convert.ToInt32(dsprice.Tables[0].Rows[0][0].ToString()), strproductname.ToUpper(), Convert.ToInt32(dsprice.Tables[1].Rows[0][0].ToString()));

                        using (StreamWriter outfile =
             new StreamWriter(Server.MapPath("~/") + "Remotecalldeclined.txt"))
                        {
                            outfile.Write("< br > update payment type");
                        }
                    }
                    else
                    {

                        DataSet dsprice = updateSaasyOrderRef(orderId, saasyOrderReference, saasyStatus, strAddressCountry, strCustomerPhone, strAddressStreet1, strAddressStreet2, strAddressCity, strAddressRegion, strCustomerCompany, strAddressPostalCode, strCustomerLastName, strOrderItemSubscriptionReference);
                    }

                    using (StreamWriter outfile =
        new StreamWriter(Server.MapPath("~/") + "Remotecalldeclined.txt"))
                    {
                        outfile.Write("< br > updated successfully");
                    }
                    
                }
            }
        }
        catch (Exception ex)
        {
            using (StreamWriter outfile =
              new StreamWriter(Server.MapPath("~/") + "exception.txt"))
            {
                outfile.Write("PageLoad"+ex.Message+"\n"+ex.Source+"\n"+ex.StackTrace);
            }
        }
    }
    private int GetBIASValue()
    {
        string timeZ = drpTimeZone.SelectedValue;
        string[] totalString = timeZ.Split(',');
        string[] tempTime = totalString[0].Split(':');
        string sign = tempTime[0].Substring(0, 1);
        int hours = 0, minutes = 0;
        if (sign != "0")
        {
            hours = ValidationHelper.GetInteger(tempTime[0].Substring(1, (tempTime.Length)), 0) * 60;
            minutes = ValidationHelper.GetInteger(tempTime[1].ToString(), 0);
            minutes = hours + minutes;
            if (sign == "-")
            {
                minutes = -(minutes);
            }
        }
        return minutes;
    }

    public osm_user GetUserDetails(string status)
    {
        osm_user user = new osm_user();
        try
        {        
            user.PASSWORD = GetRandomPasswordUsingGUID(5);
            user.LOGIN_NAME = Convert.ToString(Request.Params["CustomerEmail"]);
            user.FIRST_NAME = string.Format("{0} {1}{2}", Convert.ToString(Request.Params["CustomerFirstName"]), " ", Convert.ToString(Request.Params["CustomerLastName"]));
            user.LAST_NAME = "";
            user.ADDRESS_LINE1 = Convert.ToString(Request.Params["AddressStreet1"]);
            user.ADDRESS_LINE2 = Convert.ToString(Request.Params["AddressStreet2"]);
            user.CITY = Convert.ToString(Request.Params["AddressCity"]);
            user.STATE = "";
            user.COUNTRY = Convert.ToString(Request.Params["AddressCountry"]);
            user.COMPANY = Convert.ToString(Request.Params["CustomerCompany"]);
            user.WORK_INDUSTRY = "";
            user.JOB_FUNCTION = "";
            user.DEPT = "";
            user.EMAIL = Convert.ToString(Request.Params["CustomerEmail"]);
            if (Request.Params["Status"].ToString() == "completed")
            {
                user.LICENSE_TYPE = Convert.ToString(Request.Params["OrderReferrer"]);
            }
            else
            {
                user.LICENSE_TYPE = "FREE";
            }
            user.STATUS = "Active";
            user.LICENSE_EXPIRY_DATE = DateTime.UtcNow;
            user.LICENSE_OPTED = Convert.ToString(Request.Params["OrderReferrer"]);
            user.ADMINID = 0;
            user.APPROVAL_FLAG = 0;
            user.UPDATES_FLAG = 0;
            user.CREATED_ON = DateTime.UtcNow;
            user.MODIFIED_ON = DateTime.UtcNow;
            user.DELETED = 0;
            user.EMAIL_FLAG = 0;
            user.USER_TYPE = 1;
            user.USER_COMPANY = "";
            user.USER_DESIGNATION = "";

            user.PHONE = Convert.ToString(Request.Params["CustomerPhone"]);
            user.PAYMENT_TYPE = 1;
            user.LICENSE_UPGRADE = 0;
            user.RENEWAL_FLAG = 0;
            user.POSTAL_CODE = "0";
            user.COMPANY_SIZE = "";
            user.RESET = 1;
            user.MAIL_REMINDER_FLAG = 0;
            user.MAIL_REMINDER_DATE = DateTime.UtcNow;
            user.QUICKUSERGUIDE_FLAG = 0;
            user.OPTOUT_STATUS = "";
            user.ACCOUNT_TYPE = "Master";
            user.AUTHLINK_FLAG = 0;
            user.LICENSE_CHANGEFLAG = 0;
            user.EMAIL_COUNT = 0;
            user.WALLETPRICEPLAN = "PRO";
            user.STANDARD_BIAS = GetBIASValue();
            string ipAddress = string.Empty;
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
            user.IP_ADDRESS = ipAddress;
            user.SURVEY_FLAG = 0;         
        }
        catch (Exception ex)
        {
            using (StreamWriter outfile =
             new StreamWriter(Server.MapPath("~/") + "exception.txt"))
            {
                outfile.Write("userdetails:"+ex.Message + "\n" + ex.Source + "\n" + ex.StackTrace);
            }
        }
        return user;
    }

    public osm_orderinfo GetOrderInfoDetails(int userId)
    {
        osm_orderinfo order = new osm_orderinfo();
        try
        {
            orderNo += Convert.ToString(DateTime.UtcNow.Year).Substring(2);
        order.TAX_PERCENT = 0;
        order.TAX_PRICE = 0;
        order.TOTAL_PRICE = GetPrice(countryName, SubscriptionType);
        order.USERID = userId;
        order.PRODUCT_PRICE = GetPrice(countryName, SubscriptionType);
        order.MODEOFPAYMENT = 1;
        order.ORDERID = GetOrderId(SubscriptionType);
        order.ORDER_TYPE = Constants.ORDERTYPE;
        order.CREATED_DATE = DateTime.UtcNow;
        order.PAID_DATE = DateTime.UtcNow;
        order.TRANSACTION_STATUS = 0;
        order.CurrencyType = "USD"; // new RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name).ISOCurrencySymbol;
        order.ModifiedDate = DateTime.UtcNow;
        //var currency = new CurrencyServiceClient().GetConversionRate(CurrencyCode.USD, CurrencyCode.INR);
        order.ExchangeRate = 0;// currency.Rate;      
         }
        catch (Exception ex)
        {
            using (StreamWriter outfile =
             new StreamWriter(Server.MapPath("~/") + "exception.txt"))
            {
                outfile.Write("order:"+ex.Message + "\n" + ex.Source + "\n" + ex.StackTrace);
            }
        }
        return order;
    }

    public osm_useraccounthistory GetAccountHistoryInfo(int userId, string orderId)
    {
        osm_useraccounthistory history = new osm_useraccounthistory();
        try
        {          
            history.USERID = userId;
            history.ORDERID = orderId;
            history.EMAIL_FLAG = 0;
            history.CREATED_DATE = DateTime.UtcNow;
            history.CURRENT_LICENSETYPE = SubscriptionType;
            history.LICENSETYPE_OPTED = SubscriptionType;
        }
        catch (Exception ex)
        {
            using (StreamWriter outfile =
             new StreamWriter(Server.MapPath("~/") + "exception.txt"))
            {
                outfile.Write("histroy:" + ex.Message + "\n" + ex.Source + "\n" + ex.StackTrace);
            }
        }

        return history;
    }

    private string GetRandomPasswordUsingGUID(int length)
    {
        // Get the GUID
        string guidResult = System.Guid.NewGuid().ToString();
        // Remove the hyphens
        guidResult = guidResult.Replace("-", string.Empty);
        // Make sure length is valid
        if (length <= 0 || length > guidResult.Length)
            throw new ArgumentException("Length must be between 1 and " + guidResult.Length);
        // Return the first length bytes        
        return guidResult.Substring(0, length);
    }

    private double GetPrice(string country, string subscriptionPlan)
    {

        var price = ServiceFactory<OrderInfoService>.Instance.GetPriceByCountryName(country);
        return subscriptionPlan == "PRO_MONTHLY" ? Convert.ToDouble(price.MONTHLY_PRICE) : Convert.ToDouble(price.YEARLY_PRICE);
    }
    private string GetOrderId(string subscription)
    {
        string order = "PRO";
        if (subscription == "PRO_MONTHLY")
        {
            order += order + "-" + "M";
        }
        else
        {
            order += order + "-" + "Y";
        }
        if (DateTime.UtcNow.Month < 10)
            orderNo += "0" + DateTime.UtcNow.Month;
        else
            orderNo += DateTime.UtcNow.Month;
        orderNo += Convert.ToString(DateTime.UtcNow.Year).Substring(2);
        return orderNo;
    }
    public SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public DataSet updateSaasyOrderRef(string InsOrderID, string SaasyOrderRef, string TransStatus, string strAddressCountry, string strCustomerPhone, string strAddressStreet1, string strAddressStreet2, string strAddressCity, string strAddressRegion, string strCustomerCompany, string strAddressPostalCode, string strCustomerLastName, string strOrderItemSubscriptionReference)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updateSaasyorderRef", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@InsOrderID", SqlDbType.VarChar)).Value = InsOrderID;
            scom.Parameters.Add(new SqlParameter("@SaasyOrderRef", SqlDbType.VarChar)).Value = SaasyOrderRef;
            scom.Parameters.Add(new SqlParameter("@TransStatus", SqlDbType.VarChar)).Value = TransStatus;

            scom.Parameters.Add(new SqlParameter("@strAddressCountry", SqlDbType.VarChar)).Value = strAddressCountry;
            scom.Parameters.Add(new SqlParameter("@strCustomerPhone", SqlDbType.VarChar)).Value = strCustomerPhone;
            scom.Parameters.Add(new SqlParameter("@strAddressStreet1", SqlDbType.VarChar)).Value = strAddressStreet1;
            scom.Parameters.Add(new SqlParameter("@strAddressStreet2", SqlDbType.VarChar)).Value = strAddressStreet2;
            scom.Parameters.Add(new SqlParameter("@strAddressCity", SqlDbType.VarChar)).Value = strAddressCity;
            scom.Parameters.Add(new SqlParameter("@strAddressRegion", SqlDbType.VarChar)).Value = strAddressRegion;
            scom.Parameters.Add(new SqlParameter("@strCustomerCompany", SqlDbType.VarChar)).Value = strCustomerCompany;
            scom.Parameters.Add(new SqlParameter("@strAddressPostalCode", SqlDbType.VarChar)).Value = strAddressPostalCode;
            scom.Parameters.Add(new SqlParameter("@strCustomerLastName", SqlDbType.VarChar)).Value = strCustomerLastName;
            scom.Parameters.Add(new SqlParameter("@strOrderItemSubscriptionReference", SqlDbType.VarChar)).Value = strOrderItemSubscriptionReference;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdateorderef = new DataSet();
            sda.Fill(dsupdateorderef);
            con.Close();
            return dsupdateorderef;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataSet updatePollSaasyOrderRef(string InsOrderID, string SaasyOrderRef, string TransStatus, string strAddressCountry, string strCustomerPhone, string strAddressStreet1, string strAddressStreet2, string strAddressCity, string strAddressRegion, string strCustomerCompany, string strAddressPostalCode, string strCustomerLastName, string strOrderItemSubscriptionReference)
    {
        try
        {
            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_updatePollSaasyorderRef", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@InsOrderID", SqlDbType.VarChar)).Value = InsOrderID;
            scom.Parameters.Add(new SqlParameter("@SaasyOrderRef", SqlDbType.VarChar)).Value = SaasyOrderRef;
            scom.Parameters.Add(new SqlParameter("@TransStatus", SqlDbType.VarChar)).Value = TransStatus;

            scom.Parameters.Add(new SqlParameter("@strAddressCountry", SqlDbType.VarChar)).Value = strAddressCountry;
            scom.Parameters.Add(new SqlParameter("@strCustomerPhone", SqlDbType.VarChar)).Value = strCustomerPhone;
            scom.Parameters.Add(new SqlParameter("@strAddressStreet1", SqlDbType.VarChar)).Value = strAddressStreet1;
            scom.Parameters.Add(new SqlParameter("@strAddressStreet2", SqlDbType.VarChar)).Value = strAddressStreet2;
            scom.Parameters.Add(new SqlParameter("@strAddressCity", SqlDbType.VarChar)).Value = strAddressCity;
            scom.Parameters.Add(new SqlParameter("@strAddressRegion", SqlDbType.VarChar)).Value = strAddressRegion;
            scom.Parameters.Add(new SqlParameter("@strCustomerCompany", SqlDbType.VarChar)).Value = strCustomerCompany;
            scom.Parameters.Add(new SqlParameter("@strAddressPostalCode", SqlDbType.VarChar)).Value = strAddressPostalCode;
            scom.Parameters.Add(new SqlParameter("@strCustomerLastName", SqlDbType.VarChar)).Value = strCustomerLastName;
            scom.Parameters.Add(new SqlParameter("@strOrderItemSubscriptionReference", SqlDbType.VarChar)).Value = strOrderItemSubscriptionReference;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dsupdateorderef = new DataSet();
            sda.Fill(dsupdateorderef);
            con.Close();
            return dsupdateorderef;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
}