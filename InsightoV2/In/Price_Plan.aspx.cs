﻿using System;
using System.Collections;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;

public partial class In_Price_Plan : System.Web.UI.Page
{

    Hashtable typeHt = new Hashtable();
    int userId = 0;
    string transType = "New";
    string country;
    LoggedInUserInfo loggedInUserInfo;
    private string licenseType = "";
   
    protected void Page_Load(object sender, EventArgs e)
    {
      
   
            string ipAddress="";
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
               // ipAddress = "93.186.23.87"; 
            }
           
           var country = Utilities.GetCountryName(ipAddress);
            ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
         //   ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
            country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
      

      
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (typeHt.Contains("Type"))
                licenseType = typeHt["Type"].ToString();
            if (typeHt.Contains(Constants.USERID))
                userId = ValidationHelper.GetInteger(typeHt[Constants.USERID].ToString(), 0);
            if (typeHt.Contains("Flag"))
                transType = Convert.ToString(typeHt["Flag"]);

        }
        if (!IsPostBack)
        {
            SessionStateService sessionStateService = new SessionStateService();
            loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            //if (loggedInUserInfo == null)
            //{
            //    Response.Redirect("Home.aspx");
            //}
        }


        string NavUrl = Constants.LicenseType + "=" + "PRO_MONTHLY" + "&UserId=" + userId;


       // Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));

        PayMonth.HRef = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl);

            if (country.ToUpper() == "INDIA")
            {
                Label1.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "4,500");
                Label1.Attributes.Add("Class", "priceRs");
                // lblMonthly.Text = string.Format("{0}{1}{2}{3}", "","", "","450");
                //  lblMonthly.Attributes.Add("Class", "priceRs");


                Label2.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");



                //  lblMonthly.Attributes.Add("Class", "priceRs");

                // divFaqOtherContent.Visible = false;

                //  divFaqIndiaContent.Visible = true;

            }

            else
            {

                Label1.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "159");


                //  Label1.Attributes.Add("Class", "monthlyHeading");
                // lnkBtnMonthly.Text = string.Format("{0}{1}{2}{3}", "SAVE ON ANNUAL PLAN - ", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), " ", GetPriceByCountryName(country, "Yearly"));

                lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $15");

                // lblMonthly.Attributes.Add("Class", "monthlyHeading");

                Label2.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");
            }



            // dvPageInfo.InnerHtml = Utilities.ResourceMessage("dvPageInfo");

            //FormatCurrencyAndSymbol();

            //    SetPermissions();


        }

    

    private string GetCurrencySymbol(string countryName)
    {
        var ci = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.EnglishName.Contains(countryName)).FirstOrDefault();
        return ci.NumberFormat.CurrencySymbol;
    }

    private double GetPriceByCountryName(string countryNAme, string subscription)
    {
        var price = ServiceFactory<OrderInfoService>.Instance.GetPriceByCountryName(countryNAme);
        if (subscription == "MONTHLY")
            return Convert.ToDouble(price.MONTHLY_PRICE);
        else
            return Convert.ToDouble(price.YEARLY_PRICE);
    }


    protected void btnsignup1_Click(object sender, EventArgs e)
    {
        if (userId == 0)
        {
            Response.Redirect("~/In/SignUpFree.aspx");

        }
    }
    protected void btnsignup2_Click(object sender, EventArgs e)
    {
        if (userId == 0)
        {
            Response.Redirect("~/In/SignUpFree.aspx");
        }
    }
    protected void btnbuynow1_Click(object sender, EventArgs e)
    {
        //string navUrl = "";
        //navUrl = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", "LicenseType==PRO_YEARLY");
        //Response.Redirect(navUrl);
        string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId;
       

        Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));

    }
    protected void btnbuynow2_Click(object sender, EventArgs e)
    {
        string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId;


        Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));
    }
}