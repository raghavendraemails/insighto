﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;

public partial class In_antispam : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Contains("Page"))
            {
                var pageContent = ServiceFactory.GetService<HelpPageService>().GetHelpPageContent(ht["Page"].ToString());
                if (pageContent != null)
                {                    
                    divContent.InnerHtml = pageContent.PageContent;//.Replace("src=" + '"' + "/", "src=" + '"');
                }
                else
                {
                    divContent.InnerHtml = "";
                }
            }
        }
    }
}