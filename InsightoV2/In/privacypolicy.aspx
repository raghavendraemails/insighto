﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true"
    CodeFile="privacypolicy.aspx.cs" Inherits="In_privacypolicy" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="contentPanelHeader">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div class="pageTitle">
                        <!-- page title -->
                        <asp:Label ID="lblTitle" runat="server" Text="Privacy Policy" 
                            meta:resourcekey="lblTitleResource1"></asp:Label>                      
                        <!-- //page title -->
                    </div>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="defaultHeight">
        </div>
        <div id="divContent" runat="server">
        
        </div>
        <%--<table width="95%" border="0" cellpadding="0" cellspacing="0" class="textalignment" align="center">
            <tr>
                <td>
                    <p>
                        <strong>General</strong></p>
                    <p>
                        Insighto.com is a website of Knowience Insights Private Limited.
                    </p>
                    <p>
                        As an organization that aims to provide tools, methods and processes to improve
                        the customer engagement experience of organizations/users, we at Knowience Insights
                        fully appreciate and respect the need of every user/customer of our website/product
                        www.insighto.com for privacy while being online as well as offline. We stand committed
                        to protect any information that you may share with us, in an appropriate manner.</p>
                    <p>
                        Given below are our practices &amp; policies of use of the information that is provided/
                        stored in website/product.</p>
                    <p>
                        As a pre-requisite to use Insighto.com, you consent to the terms of the Insighto
                        Privacy Policy as it may be updated from time to time.</p>
                    <p>
                        <strong>Information &nbsp;stored and used in www.insighto.com</strong></p>
                    <p>
                        <u>Personal Information</u>
                        <br />
                        During the Registration Process for creating your Insighto Account, we request for
                        your name and email address, telephone number, organization you work for, contact
                        information, billing information any other information from which your identity
                        is discernible. We use this information to contact you about the services on our
                        site in which you have expressed interest and also enable us to make your experience
                        in using our product more personalized and useful.
                        <br />
                        Your email id would act as your User Id for your Insighto Account. In order to authenticate
                        your request, &nbsp;a system generated welcome mail would be sent to your Mail Inbox
                        giving the password. You may log in to your Insighto Account with the given User
                        Id and the password. We recommend that you change the password at the earliest instance.
                        Your name and email address will be used to inform you regarding new services, releases,
                        upcoming events and changes in this privacy policy.<br />
                    </p>
                    <p>
                        <u>Details of usage of your Account Usage</u><br />
                        Details of&nbsp; the usage of your Insighto Account such as time, frequency, duration
                        and pattern of use, features used and the amount of storage used will be recorded
                        by us in order to enhance your experience of the services and to help us provide
                        you the best possible service.<br />
                    </p>
                    <p>
                        <u>Contents of your Account </u>
                        <br />
                        We store and maintain the surveys you created, email lists, images, logos along
                        with the responses to your surveys, reports pertaining to them, alerts, reminders,
                        messages and all other data stored in your Account at our facilities in any country.
                        Use of Insighto.com signifies your consent to such transfer of your data outside
                        of your country. In order to prevent loss of data due to errors or system failures,
                        we also keep backup copies of data including the contents of your Account. Hence
                        your files and data may remain on our servers even after deletion or termination
                        of your Account. We assure you that the contents of your Account will not be disclosed
                        to anyone and will not be accessible to employees of Knowience Insights. We also
                        would not process the contents of your Account for serving targeted advertisements.
                        We will not use the information collected from your surveys in any way, shape, or
                        form.</p>
                    <p>
                        <u>Financial Information </u>
                    </p>
                    <p>
                        In order to avail the paid products of Insighto.com after giving the contact information
                        during the registration process, you would have to choose between making payments
                        through your credit/debit card/online banking mode or to pay by sending a physical
                        cheque.<br />
                        &nbsp;If you choose to make the payment through your credit/debit card/online banking
                        mode, you would be directed to the encrypted form on secure servers of a reputed
                        Payment Gateway Service Provider that Knowience Insights had contracted with. You
                        would be requested to provide the appropriate information. The information that
                        you would provide would be used solely for processing payments. Your financial information
                        would not be stored by Knowience Insights or Insighto.com. In case you are renewing
                        your account, you would have to fill in the financial information once again as
                        we would not be storing any of your financial information.<br />
                        &nbsp;If you choose to make the payment by way of sending a physical cheque, you
                        would be sent an email giving the invoice for the same and you would be requested
                        to send the physical cheque to our office address given below. In both the above
                        mentioned payment methods, you would be able to view or print the invoice from the
                        Profile page of your Insighto Account.
                        <br />
                        All this financial information would be used for billing purposes and to fill your
                        orders. If we have trouble processing an order, we will use this information to
                        contact you.<br />
                        <p>
                            <u>Respondent Information</u></p>
                        All the general information/data/responses of the respondents of the surveys you
                        had created/used would be stored for the purpose of generating appropriate reports
                        for you and your organization. This data would not be used for or accessed by any
                        other user at Insighto.com. Time to time, we may be inviting the respondents of
                        your surveys to refer Insighto.com to anyone who may use the same for creating their
                        own surveys.&nbsp; And if any of your respondent chooses to use this referral, we
                        will&nbsp; ask your respondent to furnish his/her name, email address and&nbsp;
                        the&nbsp; name of their reference and email address of that person. On the basis
                        of this information, we will automatically send the referred person a one-time email
                        inviting him or her to visit www.insighto.com. We do not store this information.</p>
                    <p>
                        <u>Visitor Details </u>
                    </p>
                    Insighto.com reserves the right to perform statistical analyses of user behavior
                    and characteristics. We would be using &nbsp;the data/information relating to but
                    not limiting to Internet Protocol address, type of browser used, language used/preferred,
                    any other URLs referred/used, files accessed, errors generated, time zone, date/time
                    stamp, clickstream data, operating system referring/exit pages, and all details
                    of the visitors, users including the customers and their respondents. All these
                    details are collected and stored in order to analyze the trends, administer the
                    websites, and track visitor's movements and to improve our website/product and its
                    services.We do this in order to measure interest in and use of the various areas
                    of the website.</p>
                    <p>
                        <u>Referrals</u></p>
                    <p>
                        If you choose to use our referral service to tell a friend about Insighto.com or
                        the surveys you had created using it, we will ask you for your friend's name and
                        email address. We will automatically send your friend a one-time email inviting
                        him or her to visit <a href="http://www.insighto.com/">www.insighto.com</a>. We
                        do not store this information.</p>
                    <p>
                        <strong>Cookies </strong>
                    </p>
                    <p>
                        www.insighto.com uses &quot;cookies&quot; to help personalize and maximize your
                        online experience.. A cookie is a text file that is placed on your hard drive by
                        a Web page server use to recognize repeat users. Cookies are not used to run programs
                        or deliver viruses to your computer. Cookies are uniquely assigned to your computer,
                        and can only be read by a Web server in the domain that issued the cookie to you.<br />
                        Information gathered through cookies also helps us measure use of our website. Cookie
                        data allow us to track usage behaviour and compile data that we can use to improve
                        the site. This data will be used in aggregate form; no specific users will be tracked.<br />
                        <br />
                        One of the primary purposes of cookies is to provide a convenience feature to save
                        you time. The purpose of a cookie is to tell the Web server that you have returned
                        to a specific page. For example, if you personalize Insighto &nbsp;pages, or register
                        for services, a cookie helps us to recall your specific information (such as user
                        name, password and preferences). Because of our use of cookies, we can deliver faster
                        and more accurate results and a more personalized site experience. When you return
                        to Insighto, the information you previously provided can be retrieved, so you can
                        easily use the features that you customized. We also use cookies to track click
                        streams and for load balancing.<br />
                        <br />
                        You may have the ability to accept or decline cookies. Most Web browsers automatically
                        accept cookies, but you can usually modify your browser setting to decline all cookies
                        if you prefer. Alternatively, you may be able to modify your browser setting to
                        notify you each time a cookie is tendered and permit you to accept or decline cookies
                        on an individual basis. If you choose to decline cookies, however, that may hinder
                        performance and negatively impact your experience on Insighto. Enabling cookies
                        ensures a smooth, efficient visit to our website.<br />
                        <br />
                        This privacy policy only covers the use of cookies by <a href="http://www.insighto.com/">
                            www.insighto.com</a>&nbsp; and does not cover the use of cookies by any third
                        party.</p>
                    <p>
                        <strong>Communications from www.insighto.com</strong></p>
                    <p>
                        <u>Customer Service</u></p>
                    Based upon the personally identifiable information you provide us, we will send
                    you a welcoming email to verify your username and password. We will also communicate
                    with you in response to your inquiries, to provide the services you request, and
                    to manage your account
                    <p>
                        <u>Service-related Announcements</u></p>
                    <p>
                        We will send you strictly service-related announcements on occasions when it is
                        necessary to do so. For instance, if our service is temporarily suspended for maintenance,
                        we might send you an email.</p>
                    <p>
                        <u>Newsletters</u></p>
                    <p>
                        If you wish to subscribe to our newsletter(s), we will use your name and email address
                        to send the newsletter to you. Out of respect for your privacy, we provide you a
                        way to unsubscribe. Please see the &ldquo;Opting out&rdquo; section.</p>
                    <u>Sending Emails on your behalf</u>
                    <p>
                        We store the email lists of our customers and send the survey invitations to their
                        audience through our system. The emails sent on our customer's behalf appear to
                        come from the customer's email address.</p>
                    <p>
                        <strong>Surveys or Contests</strong></p>
                    <p>
                        From time-to-time we may provide you the opportunity to participate in contests
                        or surveys on Insighto.com. If you participate, we will request certain personally
                        identifiable information from you. Participation in these surveys or contests is
                        completely voluntary and you therefore have a choice whether or not to disclose
                        this information. The requested information typically includes contact information
                        (such as name and shipping address), and demographic information (such as zip code).<br />
                        We use this information to notify contest winners and to monitor site traffic or
                        personalize the site (in the case of anonymous information collected in surveys).</p>
                    <p>
                        <strong>Testimonials</strong></p>
                    <p>
                        We post testimonials from time to time. We do always take permission from the persons
                        who have given the testimonials - prior to posting.<br />
                    </p>
                    <p>
                        <strong>Access to and modification of your information</strong></p>
                    <p>
                        You may review, correct, update or change your account information at any time.<br />
                        <br />
                        To change your account information, simply log into your account, go to your Account
                        Profile, review your account information and, if you wish, edit it with the options
                        provided. You could also contact Insighto support at support@insighto.com.
                    </p>
                    <p>
                        <strong>Opting Out</strong></p>
                    <p>
                        Upon request, Insighto.com will allow any user to opt out of the newsletter or any
                        such material sent to the users through email. You can contact us through our Support
                        at <a href="mailto:support@insighto.com">support@insighto.com</a> or &nbsp;follow
                        the unsubscribe instructions included in each promotional email sent to you including
                        the newsletter.</p>
                    <p>
                        <strong>Legal Disclaimer</strong></p>
                    <p>
                        We reserve the right to disclose your personally identifiable information as required
                        by law and when we believe that disclosure is necessary to protect our rights and/or
                        to comply with a judicial proceeding, court order, or legal process served on our
                        Web site</p>
                    <p>
                        <strong>General Security Policy</strong></p>
                    <p>
                        We at Knowience are quite aware of your privacy concerns and so will continuously
                        strive &nbsp;to collect only as much data as is required to make you use your Insighto
                        account &nbsp;as efficient and satisfying as possible.<br />
                        We follow generally accepted industry standards to protect the personal information
                        submitted to us, both during transmission and once we receive it. No method of transmission
                        over the Internet, or method of electronic storage, is 100% secure, however. Therefore,
                        while we strive to use commercially acceptable means to protect your personal information,
                        we cannot guarantee its absolute security. However, we will make every effort to
                        ensure that whatever information you provide will be maintained in a secure environment.</p>
                    <p>
                        The security of your personal information is important to us. When you enter sensitive
                        information (such as credit card number and/or social security number) on our registration
                        or order forms, the site directs you to the encrypted pages of a reputed&nbsp; Payment
                        Gateway Service Provider who would be using secure socket layer technology (SSL).<br />
                        Should you have any questions about security on our Web site, you can send email
                        us at <a href="mailto:support@insighto.com">support@insighto.com</a></p>
                    <p>
                        <strong>Changes in this Privacy Policy</strong></p>
                    <p>
                        If we decide to change our privacy policy, we will post those changes to this privacy
                        statement, the home page, and other places we deem appropriate so that you are aware
                        of what information we collect, how we use it, and under what circumstances, if
                        any, we disclose it.<br />
                        We reserve the right to modify this privacy statement at any time, so please review
                        it frequently. If we make material changes to this policy, we will notify you here,
                        by email, or by means of a prominent notice on our home page.</p>
                    <p>
                        <strong>Enforcement of Privacy Policy </strong>
                    </p>
                    <p>
                        We make every effort including periodic reviews to ensure that Personal Information
                        provided by you is used in conformity with this privacy policy. If you have any
                        concerns regarding our adherence to this policy or the manner in which Personal
                        Information is used for the purpose of providing the services, kindly contact us.
                        We will contact you to address your concerns and we will also co-operate with regulatory
                        authorities in this regard if needed.</p>
                    <p>
                        <strong>Contact Us</strong></p>
                    <p>
                        If you have any questions or suggestions regarding our privacy policy, please contact
                        us at:</p>
                    <table  border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td width="100" align="left" valign="top">
                                Support
                            </td>
                            <td width="5" align="left" valign="top">
                                :
                            </td>
                            <td>
                                <a href="http://www.insighto.com/osm/in/contactus.aspx">http://www.insighto.com/osm/in/contactus.aspx</a>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                Email
                            </td>
                            <td width="5" align="left" valign="top">
                                :
                            </td>
                            <td>
                                <a href="mailto:support@insighto.com">support@insighto.com</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>--%>
        <div class="clear">
        </div>
        <!-- //content panel -->
    </div>
</asp:Content>
