﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Collections;
using App_Code;

namespace Insighto.Pages.In
{
    public partial class CraftingServices : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblErrMsg.Visible = false;
            dvErrMsg.Visible = false;
            lblSuccMsg.Visible = false;
            dvSuccessMsg.Visible = false;
        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession() != null)
            {
                if (txtCaptcha.Text == ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession())
                {
                    InsertContactDetails();
                }
                else
                {
                    lblErrMsg.Text = Utilities.ResourceMessage("CraftingService_Error_Captcha"); //"Please enter the numbers displayed.";
                    lblErrMsg.Visible = true;
                    dvErrMsg.Visible = true;
                    txtCaptcha.Focus();
                }
            }
            else
            {
                txtCaptcha.Focus();
                lblErrMsg.Text = Utilities.ResourceMessage("CraftingService_Error_Captcha"); //"Please enter the numbers displayed.";
                lblErrMsg.Visible = true;
                dvErrMsg.Visible = true;
            }

        }
        #region "Method:InsertContactDetails"
        public void InsertContactDetails()
        {
            osm_contactusdetails osmContactUs = new osm_contactusdetails();
            osmContactUs.USERNAME = txtName.Text.Trim();
            osmContactUs.EMAILADDRESS = txtMail.Text.Trim();
            osmContactUs.PHONENO = txtPhno.Text.Trim();
            osmContactUs.MAILMESSAGE = txtMessage.Text.Trim();
            osmContactUs.ATTENTIONVALUE = "CraftService";
            osmContactUs.Organisation = txtOraganization.Text.Trim();
            var contactusService = ServiceFactory.GetService<ContactUsService>();
            contactusService.InsertContactUs(osmContactUs);
            if (osmContactUs.CONTACTUSUSERID > 0)
            {
                var configService = ServiceFactory.GetService<ConfigService>();
                List<string> configValue = new List<string>();
                configValue.Add("ContactusEmailAddress");
                var configDet = configService.GetConfigurationValues(configValue);
                string subject = String.Format(Utilities.ResourceMessage("CraftingService_Mail_Subject"), osmContactUs.USERNAME, osmContactUs.EMAILADDRESS, osmContactUs.PHONENO); //"UserName: " + osmContactUs.USERNAME + " - Request for: Craft Service " + " - EmailID: " + osmContactUs.EMAILADDRESS + " - Phone No: " + osmContactUs.PHONENO;
                Hashtable ht = new Hashtable();
                MailHelper.SendMailMessage(osmContactUs.EMAILADDRESS, configDet[0].CONFIG_VALUE, "", "", subject, osmContactUs.MAILMESSAGE);

                ClearControls();
            }
        }

        public void ClearControls()
        {
            lblSuccMsg.Text = Utilities.ResourceMessage("CraftingService_Success_Thankyou"); //"Thank you for choose Craft Service. We will get back to you shortly.";
            txtCaptcha.Text = "";
            txtMail.Text = "";
            txtMessage.Text = "";
            txtPhno.Text = "";
            txtName.Text = "";
            txtOraganization.Text = "";
            lblSuccMsg.Visible = true;
            dvSuccessMsg.Visible = true;
        }
        #endregion

    }
}
