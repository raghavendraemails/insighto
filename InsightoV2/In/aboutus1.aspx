﻿<%@ Page Title="Online Survey Tool: About us - Insighto" Language="C#" AutoEventWireup="true"
    CodeFile="aboutus1.aspx.cs" Inherits="In_aboutus" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Free Online Survey and Questionnaire Software for Customer Research and Feedback
    </title>
    <link rel="shortcut icon" runat="server" id="lnkFavIcon" type="image/vnd.microsoft.icon" />
    <link rel="Stylesheet" href="App_Themes/Classic/WebRupee.css" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/layoutStyle.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/App_Themes/Classic/facebox.css" />
    <link href="Styles/Banner_Style.css" rel="stylesheet" type="text/css" />
     <link href="Styles/notification.css" rel="stylesheet" type="text/css" />
    <style type="text/css" media="screen">
        @import "../images/homeimages/insighto.css";
    </style>
    <script type="text/javascript" src="/Scripts/jquery-1.6.2.js"></script>
    <script type="text/javascript" src="/Scripts/facebox.js"></script>
    <script type="text/javascript" src="/Scripts/common-functions.js"></script>
   <%-- <script type="text/javascript" src="/Scripts/notification.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.cookie.js"></script>--%>
   
</head>
<body>
       
    <div id="wrapper">
        <form id="form1" runat="server"  >
        <div>

<meta name="description" content="Insighto.com, a world-class online survey software is the makes it very easy for companies to gain insights by reaching out to their audiences through a survey." />
<meta name="keywords" content="online survey software, survey software, online survey tool"/>
  <table border="0" width="100%">
    <tr>
        <td valign="top"  width="100%">
        <div class="marginNewInnerPanel">
        <div class="whyInsightoPnl">
         <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="About Us" 
                                meta:resourcekey="lblTitleResource1"></asp:Label>
                        </h2>
                    </td>                    
                    
                </tr>
            </table>
        <!-- content panel -->
        <div class="defaultHeight"></div>
        
        <!-- contact form -->
        <div class="text content" style="padding:10px 20px;" id="divContent" runat="server">       
        </div>
        
        <!-- contact form -->
       </div>
        <!-- //content panel -->
        </div>
        
        </td>        
       <%-- <td valign="top" style="padding-top: 15px;">
        <uc:Advertizement ID="ucAdvertizement" runat="server" />
        </td>--%>
    </tr>
  </table>
           <script type="text/javascript">

               var _gaq = _gaq || [];
               _gaq.push(['_setAccount', 'UA-29026379-1']);
               _gaq.push(['_setDomainName', 'insighto.com']);
               _gaq.push(['_setAllowLinker', true]);
               _gaq.push(['_trackPageview']);

               (function () {
                   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                   ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
               })();

</script>

            <script type="text/javascript" language="javascript">
                $(document).ready(function () {
                    $('input:text:first').focus();
                });
            </script>
        </div>
        </form>

    </div>

</body>
</html>
