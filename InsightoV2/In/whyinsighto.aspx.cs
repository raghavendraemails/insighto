﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using CovalenseUtilities.Services;
using App_Code;

public partial class In_whyinsighto : System.Web.UI.Page
{
    int userId = 0;
    string transType = "New";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Contains("Page"))
            {
                var pageContent = ServiceFactory.GetService<HelpPageService>().GetHelpPageContent(ht["Page"].ToString());
                if (pageContent != null)
                {
                    divContent.InnerHtml = pageContent.PageContent;//.Replace("src=" + '"' + "/", "src=" + '"');
                }
                else
                {
                    divContent.InnerHtml = "";
                }
            }
        }

        string ipAddress = "";
        if (!IsPostBack)
        {


            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            //ipAddress = "125.236.193.250";
            var country = Utilities.GetCountryName(ipAddress);

            //if (country.ToUpper() == "INDIA")
            //{
            //    ImgINR.Visible = true;
            //    ImgUSD.Visible = false;
            //}
            //else
            //{
            //    ImgINR.Visible = false;
            //    ImgUSD.Visible = true;
            //}

            ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);

            SessionStateService sessionStateService = new SessionStateService();

            sessionStateService.EndUserSession();

        }

    }

    protected void lnkBtnYearly_Click(object sender, EventArgs e)
    {       
        //string navUrl = "";
        //string type = "";
        //type = "PRO_YEARLY";
        //navUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetRegisterURL(), Constants.USERID + "=" + userId + "&Type=" + type + "&Flag=" + transType);
        //Response.Redirect(navUrl);
     //   string NavurlYear = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", "LicenseType=PRO_YEARLY&utm_source=insighto");
        string NavurlYear = "http://www.insighto.com/pricing/";
        Response.Redirect(NavurlYear);
    }
}