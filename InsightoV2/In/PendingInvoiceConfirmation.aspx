﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.master" AutoEventWireup="true" CodeFile="PendingInvoiceConfirmation.aspx.cs" Inherits="Insighto.Pages.In.In_PendingInvoiceConfirmation" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="InVoice_messagePanel">
        <!-- signup panel -->
        <div class="confirm_middle_pnl">
            <h2 class="center"><asp:Label ID="lblThankyou" runat="server" 
                    Text=" Thank you for registering with Insighto.com" 
                    meta:resourcekey="lblThankyouResource1"></asp:Label>
               </h2>
            <p class="center">
                
                <asp:Label ID="lblPendingClearance" runat="server" Text=" Your Credit Card transaction is PENDING clearance from your bank. You would be intimated
                of the change in the status as soon as we hear the same.Your Insighto PRO membership
                will be activated after transaction is successful. In the interim you can test-drive
                Insighto as a FREE user" meta:resourcekey="lblPendingClearanceResource1"></asp:Label>
            </p>
            <p> </p>
            <h2 class="center">
                <asp:Label ID="lblActivate" runat="server" Text="Activate your Insighto" 
                    meta:resourcekey="lblActivateResource1"></asp:Label>
                </h2>
            <p class="center">
                <asp:Label ID="lblExperience" runat="server" Text=" To experience Insighto, we would need you to authenticate your registration. An
                email has been sent to your email box with a confirmation link. Please visit your
                email box and click on the link to complete your registration process …………..and
                start using Insighto as FREE user." meta:resourcekey="lblExperienceResource1"></asp:Label>
               </p>
            
            <p id="contactus_text" runat="server">
            </p>
        </div>
    </div>      	
</asp:Content>

