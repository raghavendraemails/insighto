﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Price.aspx.cs" Inherits="Price" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Price plan - Insighto</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
 
<body>
<div id="wrapper">
	<div id="header_main">
    	<a href="http://insighto.com" class="header_logo"></a>
    </div>
    <div class="clr"></div>
    <div id="nav_main">
      	<ul>
            <li><a href="../../Home.aspx">Home</a></li>
            <li><a href="whyinsighto.aspx?key=Ghp2rQFUHwIqTRm%2fsyV7zcJen%2fadfzIY">Why Insighto</a></li>
            <li><a href="TakeaTour.aspx">Take a Tour</a></li>
            <li><a href="price_plan.html" class="selected">Plans &amp; Pricing</a></li>
            <li><a href="http://blog.insighto.com:8021/">Blog</a></li>
		</ul>
        <div id="nav_rgt">
        	<a href="#" class="selected">Login</a>
        </div>
   		<div class="clr"></div>
    </div>
    
    <div id="container_main">
    	<div class="priceplan_blk">
        	
            <!--  Basic Price Blk  -->
        
        	<div class="price_1_blk">
            	<h3>BASIC</h3>
                <div class="price_1_blk_cont">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header">
                                    <div class="price_tag_free">Free <br /> for<br /> LIFE</div>
                                    <div class="price"><span>`</span>0</div>
                                    per month
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td><input type="submit" name="button" id="button" value="Sign Up" class="btn_bg" /></td>
                          </tr>
                          <tr>
                            <td>Unlimited number of surveys</td>
                          </tr>
                          <tr>
                            <td>15 questions per survey</td>
                          </tr>
                          <tr>
                            <td>150 responses per survey</td>
                          </tr>
                          <tr>
                            <td>18 question types</td>
                          </tr>
                          <tr>
                            <td>Survey templates</td>
                          </tr>
                          <tr>
                            <td>Choice of themes</td>
                          </tr>
                          <tr>
                            <td>Launch through web link</td>
                          </tr>
                          <tr>
                            <td>Report in data tables</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><a href="../In/Pricing.aspx">View all features</a></td>
                          </tr>
                          <tr>
                            <td class="no_border"><input type="submit" name="button" id="button" value="Sign Up" class="btn_bg" /></td>
                          </tr>
                    </table>
                </div>
 
            </div>
            <!--  Basic Price Blk End  -->
        	
            <!--  Basic Pro Blk  -->
        
        	<div class="price_1_blk">
            	<h3>PRO</h3>
                <div class="price_1_blk_cont">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header green">
                                    <div class="price"><span>`</span>4,500</div>
                                   	per annum
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border txt_underline"><a id="PayMonth" runat="server"><span id="PayMonth">Monthly Plan - </span></a><span class="font_ruppe">`</span>450</td>
                          </tr>
                          <tr>
                            <td><input type="submit" name="button" id="lnkBtnMonthly" value="Buy Now" class="btn_bg" /></td>
                          </tr>
                          <tr>
                            <td>Unlimited number of surveys</td>
                          </tr>
                        
                          <tr>
                            <td>Unlimited number of questions</td>
                          </tr>
                          <tr>
                            <td>Unlimited responses</td>
                          </tr>
                          <tr>
                            <td>19 question types</td>
                          </tr>
                          <tr>
                            <td>Survey templates</td>
                          </tr>
                          <tr>
                            <td>Choice of themes</td>
                          </tr>
                          <tr>
                            <td>Insert brand logo</td>
                          </tr>
                          <tr>
                            <td>Personalize buttons</td>
                          </tr>
                          <tr>
                            <td>Personalize survey end message</td>
                          </tr>
                          <tr>
                            <td>Schedule alerts</td>
                          </tr>
                          <tr>
                            <td>Print survey</td>
                          </tr>
                          <tr>
                            <td>Save survey to Word</td>
                          </tr>
                          <tr>
                            <td>Launch through web link</td>
                          </tr>
                          <tr>
                            <td>Launch via Insighto emails</td>
                          </tr>
                          <tr>
                            <td>Report in data and graphs</td>
                          </tr>
                          <tr>
                            <td>Export data to Excel, PPT, PDF and Word</td>
                          </tr>
                          <tr>
                            <td>Cross Tab</td>
                          </tr>
                          <tr>
                            <td><a href="../In/Pricing.aspx">View all features</a></td>
                                </tr>
                          <tr>
                            <td class="no_border"><input type="submit" name="button" id="button" value="Buy Now" class="btn_bg" /></td>
                          </tr>
                    </table>
                </div>
 
            </div>
            <!--  Basic Pro Blk End  -->
        
        	
   			<div class="clr"></div>
 
        </div>
   		<div class="clr"></div>
  </div>
    
    <div id="footer_blk">
    	<div id="footerL">Copyright &copy; 2014 Knowience Insights</div>
        <div id="footerR">
            <ul>
                <li><a href="../../Home.aspx">Home</a></li>
                <li><a href="whyinsighto.aspx?key=Ghp2rQFUHwIqTRm%2fsyV7zcJen%2fadfzIY">Why Insighto</a></li>
                <li><a href="price_plan.html" class="selected">Plans &amp; Pricing</a></li>
                <li><a href="aboutus.aspx?key=JUQ5UePXAPzI3eI%2b40zxvA%3d%3d">About Us</a></li>
                <li><a href="../In/ContactUs.aspx">Contact Us</a></li>
                <li><a href="antispam.aspx?key=6auQ29rICLYtdOGhX9UHsQ%3d%3d">Anti spam policy</a></li>
                <li><a href="privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy policy</a></li>
            </ul>
        </div>
   		<div class="clr"></div>
    </div>
</div>
</body>
</html>

