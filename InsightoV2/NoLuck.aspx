﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="NoLuck.aspx.cs"
    MasterPageFile="~/Login.master" Inherits="NoLuck"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:HiddenField ID="hdnMyAccount" runat="server" />
     <div class="defaultH">
            </div>
             <div class="clear">
            </div>
    <div class="PageNotFound" id="PageNotFound" runat="server">
    </div>
    <script type="text/javascript">      
        function closePage() {
            if (parent.$) {
                parent.$('#facebox .close').click(); // we are using facebox        
                parent.window.location.href = document.getElementById("<%= hdnMyAccount.ClientID %>").value; //"Home.aspx";  // navigate instead of reload removes Resend dialog issue
            } else {
                window.location.href = pageTobeRefreshedURL;
            }
        }

        function closePageAndRedirectToMyAccount() {
            if (parent.$) {
                parent.$('#facebox .close').click(); // we are using facebox
                parent.window.location.href = document.getElementById("<%= hdnMyAccount.ClientID %>").value;  // navigate instead of reload removes Resend dialog issue
            } else {
                window.location.href = pageTobeRefreshedURL;
            }
        }
    </script>
</asp:Content>
