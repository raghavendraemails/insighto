﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using System.Collections;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;

public partial class SurveyTemplate : SecurePageBase
{
    int surveyId;

    public int SurveyID
    {
        
        get
        {
            
            Hashtable ht = new Hashtable();
            ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
            if (ht.Contains("SurveyId"))
            {
                surveyId = ValidationHelper.GetInteger(ht["SurveyId"],0);
            }
            return surveyId;

        }
    }

    public UserType GetLicenceType
    {
        get
        {
           var serviceSession =  ServiceFactory.GetService<SessionStateService>();
            var userDet = serviceSession.GetLoginUserDetailsSession();
            return GenericHelper.ToEnum<UserType>(userDet.LicenseType);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (GetLicenceType.ToString() == "FREE")
        //{
        //    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "test", "confiramtionModal();", true);
        //}
       
    }

}