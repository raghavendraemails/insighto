﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SurveyReport.master" AutoEventWireup="true"
    CodeFile="CustomizeReports.aspx.cs" Inherits="CustomizeReports" %>

<%@ Register Src="~/UserControls/SurveyReportHeader.ascx" TagName="SurveyReportHeader"
    TagPrefix="uc" %>
<%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.XtraCharts.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.XtraCharts.v9.1.Web, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<asp:Content ID="surveyReportHeader" ContentPlaceHolderID="SurveyReportHeader" runat="Server">
    <uc:SurveyReportHeader ID="ucSurveyReportHeader" runat="server" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyReportMainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel runat="server" ID="pnlLoading" Style="height: 450px; vertical-align: middle;"
        HorizontalAlign="Center">
        <div style="width: 100%; height: 220px;">
        </div>
       <%-- <img src="App_Themes/Classic/Images/ajax-loader.gif" alt="LOADING..." /><br />--%>
        <div style="width: 100%; height: 220px;">
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlMain" Style="display: none;">
        <div class="surveyCreateLeftMenu">
            <!-- survey left menu -->
            <ul class="reportsMenu">
                <li><a href="Reports.aspx?Key=<%= Key %>">Reports</a></li>
            </ul>
            <ul class="reportsMenu">
                <li><a class="activeLink" id="cust_reports" runat="server">Customise Charts</a></li>
            </ul>
            <div class="reportExpandPanel">
                <p>
                    <dxe:ASPxLabel runat="server" Text="Chart Type :" ID="lblchrttype">
                    </dxe:ASPxLabel>
                </p>
                <p>
                    <dxe:ASPxComboBox runat="server" CssClass="dropDown-200" ID="cmbchrttype" AutoPostBack="True"
                        ValueType="System.String" OnSelectedIndexChanged="cmbchrttype_SelectedIndexChanged" />
                </p>
            </div>
            <div class="reportExpandPanel">
                <p>
                    <a href="#" onclick="showHide('divChartOptions','spanChartOptions','CHARTOPTIONS')"><span id="spanChartOptions"
                        style="font-size: 10px; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: normal;
                        color: #666666">[+]</span><b> Chart Options</b></a></p>
                <div id="divChartOptions" style="display: none;">
                    <p>
                        <table  height="30" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="middle">
                                    <dxe:ASPxLabel runat="server" Text="Show Label" ID="lblshowLabel">
                                    </dxe:ASPxLabel>
                                </td>
                                <td>&nbsp;&nbsp;</td>
                                <td  align="left" valign="middle">
                                    <dxe:ASPxCheckBox runat="server" Text="" AutoPostBack="True" ID="chkShowLabel" OnCheckedChanged="UpdateChartStyle">
                                    </dxe:ASPxCheckBox>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p class="defaultHeight">
                    </p>
                    <p>
                        <dxe:ASPxLabel runat="server" Text="Appearance" ID="lblchrtappearance">
                        </dxe:ASPxLabel>
                    </p>
                    <p>
                        <dxe:ASPxComboBox runat="server" CssClass="dropDown-200" ID="cmbchartappearance"
                            AutoPostBack="True" ValueType="System.String" OnCheckedChanged="UpdateChartStyle" />
                    </p>
                    <p class="defaultHeight">
                    </p>
                    <p>
                        <dxe:ASPxLabel runat="server" Text="Palette" ID="lblpalette">
                        </dxe:ASPxLabel>
                    </p>
                    <p>
                        <dxe:ASPxComboBox runat="server" CssClass="dropDown-200" ID="cmbpalette" AutoPostBack="True"
                            ValueType="System.String" OnCheckedChanged="UpdateChartStyle" />
                    </p>
                    <p class="defaultHeight">
                    </p>
                    <p>
                        <dxe:ASPxLabel runat="server" Text="Title" ID="lblshowTitle" />
                    </p>
                    <p>
                        <dxe:ASPxMemo runat="server" CssClass="textArea-200-40" AutoPostBack="True" ID="memcharttitle"
                            OnTextChanged="UpdateChartStyle" />
                    </p>
                    <p class="defaultHeight">
                    </p>
                    <p>
                        <dxe:ASPxLabel runat="server" Text="Align : " ID="lblalignment" />
                    </p>
                    <p>
                        <dxe:ASPxComboBox runat="server" CssClass="dropdownSmall" ID="cmbalignment" AutoPostBack="True"
                            ValueType="System.String" OnSelectedIndexChanged="UpdateChartStyle" />
                    </p>
                    <p class="defaultHeight">
                    </p>
                    <p>
                        <dxe:ASPxLabel runat="server" Text="Dock : " ID="lbldock" />
                    </p>
                    <p>
                        <dxe:ASPxComboBox runat="server" CssClass="dropdownSmall" ID="cmbDock" AutoPostBack="True"
                            ValueType="System.String" OnSelectedIndexChanged="UpdateChartStyle" />
                    </p>
                    <p class="defaultHeight">
                    </p>
                    <p id="trlblhidexis" visible="false" runat="server">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="80">
                                    <dxe:ASPxLabel runat="server" Text="Show Xaxis :" ID="lblHideXaxis" Visible="false" />
                                </td>
                                <td>
                                    <dxe:ASPxCheckBox runat="server" Text=" " AutoPostBack="True" ID="chkHideXaxis" Visible="false"
                                        OnCheckedChanged="UpdateChartSpecificstyle" />
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p id="trlblstaggered" runat="server" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="80">
                                    <dxe:ASPxLabel runat="server" Text="Staggered :" ID="lblStaggered" Visible="false" />
                                </td>
                                <td>
                                    <dxe:ASPxCheckBox runat="server" Text=" " AutoPostBack="True" ID="chkStaggered" Visible="false"
                                        OnCheckedChanged="UpdateChartSpecificstyle" />
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p class="defaultHeight">
                    </p>
                    <p id="lbltrpositioning" runat="server" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p>
                                        <dxe:ASPxLabel runat="server" Text="Label Positioning :" ID="lblLabelPosition" Visible="false" />
                                    </p>
                                    <p>
                                        <dxe:ASPxComboBox runat="server" CssClass="dropdownSmall" Visible="true" ID="cboLabelPostioning"
                                            AutoPostBack="false" ValueType="System.String" OnSelectedIndexChanged="UpdateChartSpecificstyle" />
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p id="trlblangle" runat="server" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p>
                                        <dxe:ASPxLabel runat="server" Text="Label Angle :" ID="lblLabelAngle" Visible="false" />
                                    </p>
                                    <p>
                                        <dxe:ASPxComboBox runat="server" Visible="false" CssClass="dropDown-200" ID="cboLabelAngle"
                                            AutoPostBack="True" ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartSpecificstyle">
                                            <Items>
                                                <dxe:ListEditItem Value="0" Text="0"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="45" Text="45"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="90" Text="90"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="135" Text="135"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="180" Text="180"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="225" Text="225"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="270" Text="270"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="315" Text="315"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p id="tdlblvalueaspercent" runat="server" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="80">
                                    <dxe:ASPxLabel runat="server" Text="Value As Percent" ID="lblValueAsPercent" Visible="false" />
                                </td>
                                <td>
                                    <dxe:ASPxCheckBox runat="server" Text=" " AutoPostBack="True" ID="chkValueAsPercent"
                                        Visible="false" OnCheckedChanged="UpdateChartSpecificstyle" />
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p id="tdlblzoompercent" runat="server" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p>
                                        <dxe:ASPxLabel runat="server" Text="Zoom Percent :" ID="lblZoomPercent" Visible="false" />
                                    </p>
                                    <p>
                                        <dxe:ASPxComboBox runat="server" Visible="false" CssClass="dropDown-200" ID="cboZoomPercent"
                                            AutoPostBack="True" ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartSpecificstyle">
                                            <Items>
                                                <dxe:ListEditItem Value="50" Text="50"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="75" Text="75"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="100" Text="100"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="120" Text="120"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="140" Text="140"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="170" Text="170"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="200" Text="200"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="250" Text="250"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="300" Text="300"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p id="trlbltransparancey" runat="server" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p>
                                        <dxe:ASPxLabel runat="server" Text="Transparency :" ID="lblTransparency" Visible="false" />
                                    </p>
                                    <p>
                                        <dxe:ASPxComboBox runat="server" Visible="false" CssClass="dropDown-200" ID="cboTransparency"
                                            AutoPostBack="True" ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartSpecificstyle">
                                            <Items>
                                                <dxe:ListEditItem Value="0" Text="0"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="45" Text="45"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="90" Text="90"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="135" Text="135"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="180" Text="180"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="225" Text="225"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="255" Text="255"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p id="tdperspectiveangle" runat="server" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p>
                                        <dxe:ASPxLabel runat="server" Text="Perspective Angle :" ID="lblPerspectiveAngle"
                                            Visible="false" />
                                    </p>
                                    <p>
                                        <dxe:ASPxComboBox runat="server" Visible="false" CssClass="dropDown-200" ID="cboPerspectiveAngle"
                                            AutoPostBack="True" ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartSpecificstyle">
                                            <Items>
                                                <dxe:ListEditItem Value="0" Text="0"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="30" Text="30"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="45" Text="45"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="60" Text="60"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="90" Text="90"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="120" Text="120"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="135" Text="135"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="150" Text="150"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p runat="server" id="tdlblshowmarker" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="80">
                                    <dxe:ASPxLabel runat="server" Text="Show Marker :" ID="lblShowMarker" Visible="false" />
                                </td>
                                <td>
                                    <dxe:ASPxCheckBox runat="server" Text=" " AutoPostBack="True" ID="chkShowMarker"
                                        Visible="false" OnCheckedChanged="UpdateChartSpecificstyle" />
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p id="trlblmarkerkind" visible="false" runat="server">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p>
                                        <dxe:ASPxLabel runat="server" Text="Marker Kind" ID="lblMarkerKind" Visible="false" />
                                    </p>
                                    <p>
                                        <dxe:ASPxComboBox runat="server" Visible="false" CssClass="dropDown-200" ID="cboMarkerKind"
                                            AutoPostBack="True" ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartSpecificstyle">
                                            <Items>
                                                <dxe:ListEditItem Value="Circle" Text="Circle"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Cross" Text="Cross"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Diamond" Text="Diamond"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Hexagon" Text="Hexagon"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="InvertedTriangle" Text="InvertedTriangle"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Pentagon" Text="Pentagon"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Plus" Text="Plus"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Square" Text="Square"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Star" Text="Star"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Triangle" Text="Triangle"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p id="trlblmarkersize" runat="server" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p>
                                        <dxe:ASPxLabel runat="server" Text="Marker Size :" ID="lblMarkerSize" Visible="False" />
                                    </p>
                                    <p>
                                        <dxe:ASPxComboBox runat="server" Visible="False" CssClass="dropDown-200" ID="cboMarkerSize"
                                            AutoPostBack="True" ValueType="System.String" SelectedIndex="1" OnSelectedIndexChanged="UpdateChartSpecificstyle">
                                            <Items>
                                                <dxe:ListEditItem Value="8" Text="8"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="10" Text="10"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="12" Text="12"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="14" Text="14"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="16" Text="16"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="18" Text="18"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="20" Text="20"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="22" Text="22"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="24" Text="24"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="26" Text="26"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="28" Text="28"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="30" Text="30"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p runat="server" id="tdlblinverted" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="80">
                                    <dxe:ASPxLabel runat="server" Text="Inverted :" ID="lblInverted" Visible="False" />
                                </td>
                                <td>
                                    <dxe:ASPxCheckBox runat="server" Text=" " AutoPostBack="True" ID="chkInverted" Visible="False"
                                        OnCheckedChanged="UpdateChartSpecificstyle" />
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p runat="server" id="tdlblexplodedpoint" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p>
                                        <dxe:ASPxLabel runat="server" Text="Exploded Point :" ID="lblExplodedPoint" Visible="False" />
                                    </p>
                                    <p>
                                        <dxe:ASPxComboBox runat="server" Visible="False" CssClass="dropDown-200" ID="cboExplodedPoint"
                                            AutoPostBack="True" ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartSpecificstyle">
                                            <Items>
                                                <dxe:ListEditItem Value="None" Text="None"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="All" Text="All"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Min Value" Text="Min Value"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Max Value" Text="Max Value"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p id="tdlblholradius" visible="false" runat="server">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p>
                                        <dxe:ASPxLabel runat="server" Text="Hole Radius :" ID="lblHoleRadius" Visible="False" />
                                    </p>
                                    <p>
                                        <dxe:ASPxComboBox runat="server" Visible="False" CssClass="dropDown-200" ID="cboHoleRadius"
                                            AutoPostBack="True" ValueType="System.String" SelectedIndex="4" OnSelectedIndexChanged="UpdateChartSpecificstyle">
                                            <Items>
                                                <dxe:ListEditItem Value="0" Text="0"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="15" Text="15"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="30" Text="30"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="50" Text="50"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="60" Text="60"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="75" Text="75"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="90" Text="90"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="100" Text="100"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p runat="server" id="tdlbldiagramtype" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p>
                                        <dxe:ASPxLabel runat="server" Text="Diagram Type :" ID="lblDiagramType" Visible="False" />
                                    </p>
                                    <p>
                                        <dxe:ASPxComboBox runat="server" Visible="False" CssClass="dropDown-200" ID="cboDiagramType"
                                            AutoPostBack="True" ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartSpecificstyle">
                                            <Items>
                                                <dxe:ListEditItem Value="Circle" Text="Circle"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Polygon" Text="Polygon"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p id="tdlblfunctiontype" runat="server" visible="false">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p>
                                        <dxe:ASPxLabel runat="server" Text="Function Type:" ID="lblFunctionType" Visible="False" />
                                    </p>
                                    <p>
                                        <dxe:ASPxComboBox runat="server" Visible="False" CssClass="dropDown-200" ID="cboFunctionType"
                                            AutoPostBack="True" ValueType="System.String" SelectedIndex="2" OnSelectedIndexChanged="UpdateChartSpecificstyle">
                                            <Items>
                                                <dxe:ListEditItem Value="Circles" Text="Circles"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Cardioid" Text="Cardioid"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Lemniscate" Text="Lemniscate"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </p>
                </div>
            </div>
            <div class="reportExpandPanel">
                <p>
                    <dxp:ASPxPanel ID="ASPxRoundPanelEditconsole" runat="server">
                        <PanelCollection>
                            <dxp:PanelContent ID="PanelContent1" runat="server" _designerRegion="0">
                                <p>
                                    <a href="#" onclick="showHide('divLegendOptions','spanLegendOptions','LEGENDOPTIONS')"><span id="spanLegendOptions"
                                        style="font-size: 10px; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: normal;
                                        color: #666666">[+]</span><b> <span runat="server" id="lbllegandOpt">Legend Options</span></b></a></p>
                                <div id="divLegendOptions" style="display: none;">
                                    <p>
                                        <table height="30" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="middle">
                                                    <dxe:ASPxLabel runat="server" Text="Show Legends" ID="lblHideLegands">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td valign="middle">
                                                    <dxe:ASPxCheckBox runat="server" Text=" " AutoPostBack="True" ID="chkHideLegands"
                                                        OnCheckedChanged="UpdateChartStyle">
                                                    </dxe:ASPxCheckBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </p>
                                    <p class="defaultHeight">
                                    </p>
                                    <panel id="trlblhorizontalalign" runat="server">                                            
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <p>
                                            <dxe:ASPxLabel runat="server" Text="Horizontal :" ID="lblLHorizontalAlign" />
                                            </p>
                                            <p>
                                            <dxe:ASPxComboBox runat="server" CssClass="dropDown-200" ID="cboLHorizontalAlign" AutoPostBack="True"
                                            ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartStyle">
                                            <Items>
                                                <dxe:ListEditItem Value="LeftOutside" Text="LeftOutside"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Left" Text="Left"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Center" Text="Center"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Right" Text="Right"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="RightOutside" Text="RightOutside"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </panel>
                                    <p class="defaultHeight">
                                    </p>
                                    <panel id="trlblverticalalign" runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <p> <dxe:ASPxLabel runat="server" Text="Vertical :" ID="lblLVerticalAlign" /></p>
                                            <p>
                                            <dxe:ASPxComboBox runat="server" CssClass="dropDown-200" ID="cboLVerticalAlign" AutoPostBack="True"
                                            ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartStyle">
                                            <Items>
                                                <dxe:ListEditItem Value="TopOutside" Text="TopOutside"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Top" Text="Top"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Center" Text="Center"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Bottom" Text="Bottom"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="BottomOutside" Text="BottomOutside"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </panel>
                                    <p class="defaultHeight">
                                    </p>
                                    <panel id="trlblldirection" runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <p><dxe:ASPxLabel runat="server" Text="Direction :" ID="lblLDirection" /></p> 
                                            <p>
                                            <dxe:ASPxComboBox runat="server" CssClass="dropDown-200" ID="cboLDirection" AutoPostBack="True"
                                            ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartStyle">
                                            <Items>
                                                <dxe:ListEditItem Value="TopToBottom" Text="TopToBottom"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="BottomToTop" Text="BottomToTop"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="LeftToRight" Text="LeftToRight"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="RightToLeft" Text="RightToLeft"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </panel>
                                    <p class="defaultHeight">
                                    </p>
                                    <panel id="trlblbequallyspaceditems" runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" >
                                    <tr>
                                        <td >
                                            <dxe:ASPxLabel runat="server" Text="Equally Spaced Items :" ID="lblLEquallySpacedItems" />
                                        </td>
                                        <td>
                                        <dxe:ASPxCheckBox runat="server" Text="  " AutoPostBack="True" ID="chkLEquallySpacedItems" OnCheckedChanged="UpdateChartStyle">
                                        </dxe:ASPxCheckBox>
                                        </td>
                                    </tr>
                                </table>
                            </panel>
                                    <p class="defaultHeight">
                                    </p>
                                    <panel id="trlblmaxhorizantalpercent" runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <p>
                                            <dxe:ASPxLabel runat="server" Text=" Horizontal % :" ID="lblLMaxHorizontalPercentage">
                                        </dxe:ASPxLabel>
                                            </p>
                                            <p>
                                            <dxe:ASPxComboBox runat="server" CssClass="dropdownSmall" ID="cboLMaxHorizontalPercentage" AutoPostBack="True"
                                            ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartStyle">
                                            <Items>
                                                <dxe:ListEditItem Value="25" Text="25%"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="50" Text="50%"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="75" Text="75%"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="100" Text="100%"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </panel>
                                    <p class="defaultHeight">
                                    </p>
                                    <panel id="trlblverticalpercent" runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <p>
                                            <dxe:ASPxLabel runat="server" Text="Vertical % :" ID="lblLMaxVerticalPercentage">
                                        </dxe:ASPxLabel>
                                            </p>
                                            <p>
                                            <dxe:ASPxComboBox runat="server" CssClass="dropdownSmall" ID="cboLMaxVerticalPercentage" AutoPostBack="True"
                                            ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartStyle">
                                            <Items>
                                                <dxe:ListEditItem Value="25" Text="25%"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="50" Text="50%"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="75" Text="75%"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="100" Text="100%"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </panel>
                                    <p class="defaultHeight">
                                    </p>
                                    <panel id="tdlbltextdirection" runat="server" visible="false">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <p>
                                            <dxe:ASPxLabel runat="server" Text="Text Direction :" ID="lblTextDirection" Visible="False">
                                        </dxe:ASPxLabel>
                                            </p> 
                                            <p>
                                            <dxe:ASPxComboBox runat="server" Visible="False" CssClass="dropdownSmall" ID="cboTextDirection"
                                            AutoPostBack="True" ValueType="System.String" SelectedIndex="0" OnSelectedIndexChanged="UpdateChartSpecificstyle">
                                            <Items>
                                                <dxe:ListEditItem Value="LeftToRight" Text="LeftToRight"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="TopToBottom" Text="TopToBottom"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="BottomToTop" Text="BottomToTop"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Radial" Text="Radial"></dxe:ListEditItem>
                                                <dxe:ListEditItem Value="Tangent" Text="Tangent"></dxe:ListEditItem>
                                            </Items>
                                        </dxe:ASPxComboBox>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </panel>
                            </dxp:PanelContent>
                        </PanelCollection>
                    </dxp:ASPxPanel>
                </p>
            </div>
            <div class="reportExpandPanel">
                <p>
                    <dxe:ASPxButton ID="cmdApplySettings" CssClass="devxButton" runat="server" BackColor="Transparent"
                        Text="Apply" Visible="False">
                        <Border BorderStyle="None"></Border>
                    </dxe:ASPxButton>
                </p>
                <p>
                    <dxe:ASPxButton ID="cmdsave" CssClass="devxButton" runat="server" BackColor="Transparent"
                        Text="Save your customization" OnClick="cmdApplySettings_Click">
                        <Border BorderStyle="None"></Border>
                    </dxe:ASPxButton>
                </p>
            </div>
        </div>
        <ul class="reportsMenu">
            <li><a id="CrossTabRpts" runat="server">Cross Tab Report</a></li>
            <li><a id="IndividualRes" runat="server">Individual Responses</a></li>
        </ul>
        <!-- //survey left menu -->
        </div>
        <div class="surveyQuestionPanel">
            <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblSuccMsg" runat="server"></asp:Label></div>
            </div>
            <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label></div>
            </div>
            <!-- survey question panel -->
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="surveyQuestionHeader">
                        <div class="surveyQuestionTitle">
                            CUSTOMISE CHARTS</div>
                    </td>
                    <td class="quation_btn_pnl">
                        <dxe:ASPxButton ID="cmdSaveAgain" CssClass="devxButton" OnClick="cmdsave_Click" runat="server"
                            BackColor="Transparent" Text="Save your customization">
                            <Border BorderStyle="None"></Border>
                        </dxe:ASPxButton>
                    </td>
                </tr>
            </table>
            <!-- //survey question panel -->
            <div>
                <asp:UpdatePanel ID="upCustomControls" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin: 0 0 8px 0">
                            <tr>
                                <td align="left" valign="top">
                                    <span class="rightheads">Questions: </span><span id="sidebysidetext" runat="server"
                                        visible="false">Customization will auto-apply to all charts in this question</span><br />
                                    <dxp:ASPxPanel ID="ASPxRoundPanelquestions" runat="server">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent11" runat="server" _designerRegion="0">
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxp:ASPxPanel>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="top">
                                    <table width="95%" border="0" cellspacing="0" cellpadding="0">
                                        <tr align="right">
                                            <td width="750" align="right">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dxp:ASPxPanel ID="ASPxRoundPanelwebchart" runat="server" Width="650px">
                                                    <PanelCollection>
                                                        <dxp:PanelContent ID="PanelContent2" runat="server" _designerRegion="0">
                                                        </dxp:PanelContent>
                                                    </PanelCollection>
                                                </dxp:ASPxPanel>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center">
                                                <dxe:ASPxLabel runat="server" Text="Add Comments" CssClass="HyperLink" ID="lblAddcomments">
                                                </dxe:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dxe:ASPxMemo runat="server" Height="70px" Width="100%" AutoPostBack="True" ID="memoaddcomments"
                                                    OnTextChanged="UpdateChartStyle">
                                                </dxe:ASPxMemo>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <asp:HiddenField ID="hdnType" runat="server" />
      
    </asp:Panel>
    <script type="text/javascript" language="javascript">

        function pageLoad(sender, args) {
            //alert('hai');
            TogglePannel('<%= pnlMain.ClientID %>', true);
            TogglePannel('<%= pnlLoading.ClientID %>', false);
        }

        function TogglePannel(target, OnOff) {
            obj = document.getElementById(target);

            if (obj) {
                obj.style.display = (OnOff) ? 'inline' : 'none';
            }
        }

        function showHide(EL, PM,Type) {
            ELpntr = document.getElementById(EL);
            document.getElementById('<%=hdnType.ClientID %>').value = Type;
            if (ELpntr.style.display == 'none') {
                document.getElementById(PM).innerHTML = "[-] ";
                ELpntr.style.display = 'block';
            }
            else {
                document.getElementById(PM).innerHTML = "[+]";
                ELpntr.style.display = 'none';
            }
        }

    </script>
</asp:Content>
