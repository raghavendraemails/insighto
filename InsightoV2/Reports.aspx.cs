﻿
#region "NameSpaces"

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts.Web;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using sharp = iTextSharp.text;
using FeatureTypes;
using Insighto.Business.ValueObjects;
using System.Text;
using System.Linq;
using System.Threading;
using DevExpress.Web.ASPxClasses;
using System.Diagnostics;
using InfoSoftGlobal;
using System.Security.Cryptography;
using iTextSharp.text.html.simpleparser;
using System.Reflection;
using OfficeOpenXml;
using Office = Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.Runtime.InteropServices;
using Word = Microsoft.Office.Interop.Word;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
//using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Packaging.Extensions;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Threading.Tasks;
using System.Net;
using System.Web;

//using P = DocumentFormat.OpenXml.Presentation;
//using D = DocumentFormat.OpenXml.Drawing;
//using A = DocumentFormat.OpenXml.Drawing;
//using Drawing = DocumentFormat.OpenXml.Drawing;
//using DocumentFormat.OpenXml.Drawing;
//using P14 = DocumentFormat.OpenXml.Office2010.PowerPoint;

//using InsightoV2.ExportSurveyReports;
#endregion

#region "Class : Reports"
/// <summary>
/// This class used to show raw, data , chart and data+chart reports.
/// </summary>

public partial class Reports : SurveyPageBase
{
    #region "Variables"
    /// <summary>
    /// class level variables.
    /// </summary>
    int QuestID = -1, Mode = 0, check_flag = 0, SMode = 0, QType_count = 0;
    public int surveyID = 0;
    int QuesSeq = 0;
    SurveyCore surcore = new SurveyCore();
    SurveyCoreVoice surcorevoice = new SurveyCoreVoice();
    PPTXopenXML pptxml = new PPTXopenXML();

    Hashtable ht = new Hashtable();
    string ViewAllQues = "";
    DateTime dt1 = new DateTime();
    DateTime dt2 = new DateTime();
    bool isDateFilter = false;
    //int pageIndex = 0;
    //int pageSize = 10;

    string strquestionid;
    string tempquestionid = "";
    string firstview = "";
    public string jsstrchartname;
    public string jsstrchartnameppt;
    public string SAVE_PATH;
    public string SAVE_PATHRET;
    public int fcdelaytime;
    public string exportchartfilehandler;
    public string strlicensetype;
   System.Data.DataTable colgroupoptions123413;
   System.Data.DataTable colgroupoptions123413Export;
   System.Data.DataTable dtchart;
   System.Data.DataTable colgroupoptions123413ppt;
   System.Data.DataTable colgroupoptions1011ppt;
   System.Data.DataTable colgroupoptions12ppt;
   System.Data.DataTable colgroupoptions15ppt;
    double maxAllowedResponseLimit = 0;

    #endregion

    #region "Property"
    /// <summary>
    /// This property used to set query string.
    /// </summary>
    public string Key
    {
        get
        {
            return HttpUtility.UrlEncode(QueryStringHelper.GetString("Key"));
        }
    }
    #endregion
      

    #region "Event : Page_Load"
    /// <summary>
    /// Page load event show the default data charts.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            strlicensetype = loggedInUserInfo.LicenseType;
            SAVE_PATH = ConfigurationManager.AppSettings["downloadcharts"].ToString();
            SAVE_PATHRET = ConfigurationManager.AppSettings["downloadchartsret"].ToString();
            // fcdelaytime = Convert.ToInt32(ConfigurationManager.AppSettings["fusionchartcapturingdelaytime"].ToString());
            exportchartfilehandler = ConfigurationManager.AppSettings["exportchartfile"].ToString();
            string Path = ConfigurationManager.AppSettings["RootPath"].ToString();
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.cmdExport);
            scriptManager.RegisterPostBackControl(this.lbn_rawdataexport);
            dvErrMsg.Visible = false;
            dvSuccessMsg.Visible = false;
            divSendReport.Visible = false;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alertScript", "return GetRadioButtonListSelectedValue('" + rblgrp.ClientID + "')", true);
            if (Session["viewallclick"] == "viewallclick")
            {
                btnviewall.Visible = false;

            }
            else
            {
                btnviewall.Visible = true;

            }


            Session["TempChart_det"] = null;



            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);


                if (Session["defaultchart"] == null)
                {

                    rblgrp.SelectedValue = "Chart";
                    Session["searchclick"] = "";

                }

                if (ht != null && ht.Count > 0)
                {
                    if (ht.Contains("QuesID"))
                        QuestID = Convert.ToInt32(ht["QuesID"]);
                    if (ht.Contains("Veiw"))
                        ViewAllQues = Convert.ToString(ht["Veiw"]);
                    if (ht.Contains("Mode"))
                        Mode = Convert.ToInt32(ht["Mode"]);
                    if (ht.Contains("QuesSeq"))
                        QuesSeq = Convert.ToInt32(ht["QuesSeq"]);
                    if (ht.Contains("ddlsidetext"))
                        SMode = Convert.ToInt32(ht["ddlsidetext"]);
                    if (ht.Contains("Date_Start"))
                        dt1 = Convert.ToDateTime(ht["Date_Start"]);

                    if (ht.Contains("Date_End"))
                        dt2 = Convert.ToDateTime(ht["Date_End"]);


                    if (ht.Contains("Date_Start") && (ht.Contains("Date_End")))
                    {
                        if (Convert.ToDateTime(ht["Date_Start"]) > Convert.ToDateTime("1/1/0001 12:00:00 AM") && Convert.ToDateTime(ht["Date_End"]) > Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                        {
                            Page.RegisterStartupScript("Indshow", "<script>showHide('divDate','spanDate')</script>");
                        }
                    }
                }
            }

            if (Session["searchclick"] == "Clicked")
            {
                dedstartdate.Date = Convert.ToDateTime(Session["dedstartdate"]);
                dedEnddate.Date = Convert.ToDateTime(Session["dedEnddate"]);
            }


            //if (!IsPostBack)
            //{

            SetPremiumPermissions();

            //}

            if (rblgrp.SelectedValue == "DataChart")
            {

                Mode = 0;
            }
            else if (rblgrp.SelectedValue == "Data")
            {
                Mode = 1;
            }
            else if (rblgrp.SelectedValue == "Chart")
            {
                Mode = 2;
            }

            BindReportgrid(ht, ViewAllQues, dt1, dt2);

            if (check_flag != 1)
            {
                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    surveyID = Convert.ToInt32(ht[Constants.SURVEYID]);
                    DateTime start = dt1.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt1;
                    DateTime End = dt2.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt2;

                    DataSet dssurcorevoice = surcore.voicedataset();
                    if (dssurcorevoice.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
                    {
                        SelectedSurvey = surcorevoice.GetSurveyWithResponsesCount(surveyID, start, End);
                    }
                    else
                    {

                        SelectedSurvey = surcore.GetSurveyWithResponsesCountFusion(surveyID, start, End);

                    }
                }


            }
            lnkClose.Attributes.Add("onclick", "javascript:closeModalSelf(true,'" + EncryptHelper.EncryptQuerystring(PathHelper.GetReportsURL().Replace("~/", ""), "SurveyId=" + surveyID + "&surveyFlag=" + base.SurveyFlag) + "')");

            string renew_link = "";
            renew_link = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + SelectedSurvey.USER_ID + "&utm_source=insighto&Prem=" + ht.Contains("pre"));

            hyp_priclink.Target = "_blank";
            hyp_priclink.HRef = renew_link;
        }
        catch (Exception ex)
        {
            throw ex;
        }
 }

    private void ClearChartSessions()
    {
        foreach (var key in Session.Keys)
        {
            if (key.ToString().StartsWith("Chart_") && key.ToString() != "Chart_" + ht[Constants.SURVEYID].ToString())
            {
                Session["Chart_" + ht[Constants.SURVEYID].ToString()] = 0;
            }
        }
    }

    /// <summary>
    /// Sets the premium permissions.
    /// </summary>
    private void SetPremiumPermissions()
    {
        UserType userType=0;
        surveyID = Convert.ToInt32(ht[Constants.SURVEYID]);


        DataSet dssurveytype = surcore.getSurveyType(surveyID);

        if (dssurveytype.Tables[0].Rows[0][1].ToString() != "")
        {
            userType = (UserType)Enum.Parse(typeof(UserType) , dssurveytype.Tables[0].Rows[0][1].ToString());
        }
        else
        {
             userType = Utilities.GetUserType();
        }
        bool showIndivisualResponse = Utilities.ShowOrHideFeature(userType, (int)RealTimeReports.Individual_responses);
        bool showCustomCharts = Utilities.ShowOrHideFeature(userType, (int)RealTimeReports.Customization_of_Charts);
        bool showRawDataExport = Utilities.ShowOrHideFeature(userType, (int)RealTimeReports.Raw_Data_Export);

        IndividualRes.Visible = true;
        lbn_rawdataexport.Visible = showRawDataExport;
    }

    private void OnlyCapture(string ChartPath, string ChartXMLURL, string FileName)
    {
        string ReturnValue = "";
        FusionCharts.ServerSideImageHandler ssh = new FusionCharts.ServerSideImageHandler(ChartPath, 850, 500, ChartXMLURL, "", FusionCharts.ServerSideImageHandler.ImageType.PNG, 96.0f);
        // Setting chart SWF version
        ssh.SetChartVersion("3.2.2.0");
       // ssh.SetChartVersion("3.2.1.0");
       // ssh.SetCapturingDelayTime(fcdelaytime);
        // Begins the capture process.
        // An image will be created at the specified location after execution of this statement.
        ReturnValue = ssh.BeginCapture();
        if (!ReturnValue.ToLower().Equals("success"))
        {
            this.Response.Write("<b>Error:&nbsp;&nbsp;</b>" + ReturnValue.Replace(Environment.NewLine, "<br/>"));
        }
        else
        {
            Bitmap bmp = ssh.GetImage();           
            bmp.Save(FileName);
        }
    }

    public static string StripTagsRegex(string source)
    {
        return Regex.Replace(source, "<.*?>", string.Empty);
    }
  
    static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
    public static string StripTagsRegexCompiled(string source)
    {
        return _htmlRegex.Replace(source, string.Empty);
    }
    public static string StripTagsCharArray(string source)
    {
        char[] array = new char[source.Length];
        int arrayIndex = 0;
        bool inside = false;

        for (int i = 0; i < source.Length; i++)
        {
            char let = source[i];
            if (let == '<')
            {
                inside = true;
                continue;
            }
            if (let == '>')
            {
                inside = false;
                continue;
            }
            if (!inside)
            {
                array[arrayIndex] = let;
                arrayIndex++;
            }
        }
        return new string(array, 0, arrayIndex);
    }

    private string GetUniqueKey()
    {
        int maxSize = 8;
        int minSize = 5;
        char[] chars = new char[62];
        string a;
        a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        chars = a.ToCharArray();
        int size = maxSize;
        byte[] data = new byte[1];
        RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
        crypto.GetNonZeroBytes(data);
        size = maxSize;
        data = new byte[size];
        crypto.GetNonZeroBytes(data);
        StringBuilder result = new StringBuilder(size);
        foreach (byte b in data)
        {
            result.Append(chars[b % (chars.Length - 1)]);
        }
        return result.ToString();
    }

   
  
    #endregion


    protected void grdvall_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[0].Width = 240;
        e.Row.Cells[1].Width = 180;
        e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right;
        e.Row.Cells[2].Width = 180;
        e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
        e.Row.Font.Name = "Arial";

        if (e.Row.Cells[0].Text == "# of Respondents")
        {
            e.Row.BackColor = System.Drawing.Color.FromName("#D8D8D8");
            e.Row.Font.Bold = true;

        }

    }

    protected void grdvall1011_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Font.Name = "Arial";


        if (e.Row.Cells[0].Text == "# of Respondents")
        {
            e.Row.Cells[0].Width = 240;
            e.Row.BackColor = System.Drawing.Color.FromName("#D8D8D8");
            e.Row.Font.Bold = true;
        }


        e.Row.Cells[0].Width = 240;
        int rowcount = e.Row.Cells.Count;
        int rowwidth = 360 / (rowcount - 1);
        for (int i = 1; i <= rowcount - 1; i++)
        {
            e.Row.Cells[i].Width = rowwidth;
            e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
        }


    }
    protected void grdvall12_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Font.Name = "Arial";

        if (e.Row.Cells[0].Text == "# of Respondents")
        {
            e.Row.BackColor = System.Drawing.Color.FromName("#D8D8D8");
            e.Row.Font.Bold = true;
            e.Row.Cells[0].Width = 240;
        }

        e.Row.Cells[0].Width = 240;

        int rowcount = e.Row.Cells.Count;
        int rowwidth = 360 / (rowcount - 1);
        for (int i = 1; i <= rowcount - 1; i++)
        {
            e.Row.Cells[i].Width = rowwidth;
            e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
        }

    }
    protected void grdvall13_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Font.Name = "Arial";
        if (e.Row.Cells[0].Text == "# of Respondents")
        {
            e.Row.BackColor = System.Drawing.Color.FromName("#D8D8D8");
            e.Row.Font.Bold = true;
            e.Row.Cells[0].Width = 240;

        }
        e.Row.Cells[0].Width = 240;

        int rowcount = e.Row.Cells.Count;
        int rowwidth = 360 / (rowcount - 1);
        for (int i = 1; i <= rowcount - 1; i++)
        {
            e.Row.Cells[i].Width = rowwidth;
            e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
        }


    }
    protected void grdvall15_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Font.Name = "Arial";
        if (e.Row.Cells[0].Text == "# of Respondents")
        {
            e.Row.BackColor = System.Drawing.Color.FromName("#D8D8D8");
            e.Row.Font.Bold = true;
            e.Row.Cells[0].Width = 240;

        }
       
        e.Row.Cells[0].Width = 240;
        int rowcount = e.Row.Cells.Count;
        int rowwidth = 360 / (rowcount - 1);
        for (int i = 1; i <= rowcount - 1; i++)
        {
            e.Row.Cells[i].Width = rowwidth;
            e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
        }

    }

    
    private void BindReportgrid(Hashtable ht, string ViewAllQues, DateTime dt1, DateTime dt2)
    {
        try
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            if (userDetails != null && userDetails.UserId > 0)
            {

                if (Mode == 0)
                {
                    rblgrp.Items[0].Selected = true;
                }
                else if (Mode == 1)
                {
                    rblgrp.Items[1].Selected = true;
                }
                else if (Mode == 2)
                {
                    rblgrp.Items[2].Selected = true;
                    rpnl_reportsdata.ScrollBars = ScrollBars.None;
                }

                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    surveyID = Convert.ToInt32(ht[Constants.SURVEYID]);
                    DateTime start = dt1.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt1;
                    DateTime End = dt2.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt2;

                    DataSet dssurcorevoice1 = surcore.voicedataset();
                    if (dssurcorevoice1.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
                    {
                        SelectedSurvey = surcorevoice.GetSurveyWithResponsesCount(surveyID, start, End);
                    }
                    else
                    {
                        SelectedSurvey = surcore.GetSurveyWithResponsesCountFusion(surveyID, start, End);
                    }
                }


                int ques_seq = 1;
                int Ques_Raw = 0;
                int seqint = 0;

                SelectedSurvey.surveyQues.Sort();
                QuestID = SelectedSurvey.surveyQues[seqint].QUESTION_ID;

                if (dt1 != null && ht != null && ht.Count > 0 && ht.Contains("Date_Start"))
                {
                    dedstartdate.Date = dt1;
                    isDateFilter = true;

                }
                if (dt2 != null && ht != null && ht.Count > 0 && ht.Contains("Date_End"))
                {
                    dedEnddate.Date = dt2;

                }

                if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
                {
                    if (QuestID > 0)
                    {


                        if (QuesSeq == 0)
                        {
                            seqint = QuesSeq;
                        }
                        else
                        {
                            seqint = QuesSeq - 1;
                        }


                        DataSet mxreslimit = surcore.getSurveyType(SelectedSurvey.SURVEY_ID);

                        DataSet dssrvyquestions = surcore.getReportquestions(SelectedSurvey.SURVEY_ID);

                        string strsurvytype = mxreslimit.Tables[0].Rows[0][0].ToString();

                        dtchart = createDataTableChartID();
                        DataSet dsansoptionsall = null;
                        //THIS IS STILL HARDCODED IN THE QUERIES STARTING WITH SP_REPORT...
                        //NOT CHANGED THERE BECAUSE THIS MIGHT NOT CHANGE VERY REGULARLY
                        //SATISH 15-OCT-15
                        maxAllowedResponseLimit = RespondentService.GetNoOfResponses(Utilities.ToEnum<UserType>(SurveyBasicInfoView.LICENSE_TYPE));

                        if (((strsurvytype == null) || (strsurvytype == "") || (strsurvytype == "FREE")) && (SurveyBasicInfoView.ResponseCount > maxAllowedResponseLimit))
                        {


                            if (loggedInUserInfo.LicenseType == "PREMIUM_YEARLY")
                            {
                                if ((Session["viewallclick"] != "viewallclick") || (Session["viewallclick"] == "viewonebyone"))
                                {
                                    dsansoptionsall = surcore.getIndvQuestAnsweroptions(SelectedSurvey.SURVEY_ID, Convert.ToInt32(dssrvyquestions.Tables[0].Rows[seqint][1].ToString()));
                                }
                                else
                                {
                                    dsansoptionsall = surcore.getAnsweroptionsQIDySurveOpt(SelectedSurvey.SURVEY_ID);
                                }
                            }
                            else
                            {
                                if ((Session["viewallclick"] != "viewallclick") || (Session["viewallclick"] == "viewonebyone"))
                                {
                                    NotificationControl.AddSuccessMessagemaxAllowedResponseLimit(string.Format(" You have received more than " + maxAllowedResponseLimit.ToString() + " responses.To view all the responses, please upgrade.", ""), false);
                                }
                                if ((Session["viewallclick"] != "viewallclick") || (Session["viewallclick"] == "viewonebyone"))
                                {
                                    dsansoptionsall = surcore.getFreeIndvQuestAnsweroptions(SelectedSurvey.SURVEY_ID, Convert.ToInt32(dssrvyquestions.Tables[0].Rows[seqint][1].ToString()));
                                }
                                else
                                {
                                    dsansoptionsall = surcore.getAnsweroptionsQIDySurveOptFree(SelectedSurvey.SURVEY_ID);
                                }
                            }
                        }
                        else if ((strsurvytype == "PPS_PRO") && (SurveyBasicInfoView.ResponseCount > maxAllowedResponseLimit))
                        {
                            if ((Session["viewallclick"] != "viewallclick") || (Session["viewallclick"] == "viewonebyone"))
                            {
                                NotificationControl.AddSuccessMessagemaxAllowedResponseLimit(string.Format(" You have received more than " + maxAllowedResponseLimit.ToString() + " responses.To view all the responses, please upgrade.", ""), false);
                            }
                            if ((Session["viewallclick"] != "viewallclick") || (Session["viewallclick"] == "viewonebyone"))
                            {
                                dsansoptionsall = surcore.get500IndvQuestAnsweroptions(SelectedSurvey.SURVEY_ID, Convert.ToInt32(dssrvyquestions.Tables[0].Rows[seqint][1].ToString()));
                            }
                            else
                            {
                                dsansoptionsall = surcore.getAnsweroptionsQIDySurveOpt500(SelectedSurvey.SURVEY_ID);
                            }
                        }
                        else if ((strsurvytype == "PPS_PREMIUM") && (SurveyBasicInfoView.ResponseCount > maxAllowedResponseLimit))
                        {
                            if ((Session["viewallclick"] != "viewallclick") || (Session["viewallclick"] == "viewonebyone"))
                            {
                                NotificationControl.AddSuccessMessagemaxAllowedResponseLimit(string.Format(" You have received more than " + maxAllowedResponseLimit.ToString() + " responses.To view all the responses, please upgrade.", ""), false);
                            }
                            if ((Session["viewallclick"] != "viewallclick") || (Session["viewallclick"] == "viewonebyone"))
                            {
                                dsansoptionsall = surcore.get1000IndvQuestAnsweroptions(SelectedSurvey.SURVEY_ID, Convert.ToInt32(dssrvyquestions.Tables[0].Rows[seqint][1].ToString()));
                            }
                            else
                            {
                                dsansoptionsall = surcore.getAnsweroptionsQIDySurveOpt1000(SelectedSurvey.SURVEY_ID);
                            }
                        }
                        else
                        {

                            if (Session["searchclick"] == "Clicked")
                            {
                                //dsansoptionsall = surcore.getAnsweroptionsQIDySurveOptDateRange(SelectedSurvey.SURVEY_ID, dt1, dt2);    
                                dsansoptionsall = surcore.getAnsweroptionsQIDySurveOptDateRange(SelectedSurvey.SURVEY_ID, dedstartdate.Date, dedEnddate.Date);
                            }
                            else
                            {
                                if ((Session["viewallclick"] != "viewallclick") || (Session["viewallclick"] == "viewonebyone"))
                                {
                                    dsansoptionsall = surcore.getIndvQuestAnsweroptions(SelectedSurvey.SURVEY_ID, Convert.ToInt32(dssrvyquestions.Tables[0].Rows[seqint][1].ToString()));
                                }                             
                                else
                                {
                                dsansoptionsall = surcore.getAnsweroptionsQIDySurveOpt(SelectedSurvey.SURVEY_ID);
                                }
                            }
                        }

                        SetQuestionNos1(dsansoptionsall.Tables[3], SelectedSurvey.SURVEY_ID);
                        

                        for (int iseq = 0; iseq < dsansoptionsall.Tables[1].Rows.Count; iseq++)
                        {

                            if (QuesSeq > 1)
                            {
                                if ((Session["viewallclick"] != "viewallclick") || (Session["viewallclick"] == "viewonebyone"))
                                {
                                    seqint = iseq;
                                    iseq = seqint;
                                }
                                else
                                {
                                    iseq = seqint;
                                }
                            }

                            System.Data.DataTable colgroupoptions123413 = createDataTableQ123413();
                            System.Data.DataTable colgroupoptions = createDataTable();
                            System.Data.DataTable colgroupoptionsQ12 = createDataTableQ12();
                            System.Data.DataTable colgroupoptions15 = createDataTable();


                            HtmlTable qt3tableques1 = new HtmlTable();
                            qt3tableques1.CellPadding = 1;
                            qt3tableques1.CellSpacing = 0;
                            qt3tableques1.Width = "600";


                            HtmlTableRow qt3row = new HtmlTableRow();
                            qt3row.BgColor = "#bf92cb";


                            HtmlTableCell qt3cell1 = new HtmlTableCell();
                            qt3cell1.VAlign = "Middle";
                            qt3cell1.Height = "30px";
                            qt3cell1.ColSpan = 2;
                            // qt3cell1.Align = "left";

                            System.Web.UI.WebControls.Label lblqt3cell1 = new System.Web.UI.WebControls.Label();
                            lblqt3cell1.Font.Name = "Arial, Helvetica, sans-serif";

                            if ((Session["viewallclick"] != "viewallclick") || (Session["viewallclick"] == "viewonebyone"))
                            {
                                if (QuesSeq == 0)
                                {
                                    QuesSeq = QuesSeq + 1;
                                }
                                

                                lblqt3cell1.Text = QuesSeq + "." + " " + StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseq][2].ToString()).Trim();
                            }
                            else
                            {
                                lblqt3cell1.Text = dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "." + " " + StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseq][2].ToString()).Trim();
                            }
                            lblqt3cell1.ForeColor = System.Drawing.Color.White;
                            lblqt3cell1.Font.Bold = true;

                            qt3cell1.Controls.Add(lblqt3cell1);

                            qt3row.Cells.Add(qt3cell1);

                            if ((rblgrp.SelectedValue == "DataChart") || (rblgrp.SelectedValue == "Data") || (rblgrp.SelectedValue == "Chart"))
                            {
                                qt3tableques1.Rows.Add(qt3row);
                                rpnl_reportsdata.Controls.Add(qt3tableques1);
                            }


                            HtmlTable qt3tableques = new HtmlTable();
                            qt3tableques.CellPadding = 1;
                            qt3tableques.CellSpacing = 0;
                            //   qt3tableques.Width = "100%";

                            HtmlTableRow htchartrow = new HtmlTableRow();

                            HtmlTableCell htchartcell = new HtmlTableCell();

                            HtmlTable qt3table = new HtmlTable();
                            qt3table.Border = 0;
                            //qt3table.Align = "left";
                            qt3table.CellPadding = 1;
                            qt3table.CellSpacing = 0;
                            //  qt3table.Width = "100%";

                            HtmlTableRow htrgx = new HtmlTableRow();
                            HtmlTableCell htcgx = new HtmlTableCell();


                            if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "1" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "2" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "3" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "4")
                            {

                                DataRow[] dr1234 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                                GridView grdvall = new GridView();
                                grdvall.ID = "gridQT" + iseq;
                                grdvall.Width = 598;
                                grdvall.RowStyle.Height = 25;
                                grdvall.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                                grdvall.GridLines = 0;
                                grdvall.RowDataBound += new GridViewRowEventHandler(grdvall_RowDataBound);

                                DataTable dtg = new DataTable();
                                DataRow dtgr;
                                DataRow dtgresp;

                                string lnkother = "";
                                string lnkothercount = "";

                                dtg.Columns.Add("Answeroption");
                                dtg.Columns.Add("percentage");
                                dtg.Columns.Add("count");
                                SurveyCore othertext = new SurveyCore();
                                for (int irg1 = 0; irg1 < dr1234.Length; irg1++)
                                {
                                    dtgr = dtg.NewRow();

                                    if (dr1234[irg1][2].ToString() == "Other")
                                    {                                       
                                        dtgr[0] = othertext.Selectsurveyques_other(Convert.ToInt32(dr1234[irg1][8].ToString()));
                                        if (dtgr[0] == "")
                                        {
                                            dtgr[0] = dr1234[irg1][2].ToString();
                                        }
                                    }
                                    else
                                    {
                                        dtgr[0] = dr1234[irg1][2].ToString();
                                    }

                                   // dtgr[0] = dr1234[irg1][2].ToString();
                                    dtgr[1] = dr1234[irg1][6].ToString() + "%";
                                    dtgr[2] = dr1234[irg1][5].ToString();

                                    dtg.Rows.Add(dtgr);

                              //      if ((dtgr[0].ToString().Contains("Other")) && (dr1234[irg1][5].ToString() != "0"))
                                    if ((dr1234[irg1][8].ToString() != "") && (dr1234[irg1][5].ToString() != "0"))
                                    {
                                      //  lnkother = "Other";
                                      //  lKnkothercount = dtgr[2].ToString();

                                        lnkother = othertext.Selectsurveyques_other(Convert.ToInt32(dr1234[irg1][8].ToString()));
                                        if (lnkother == "")
                                        {
                                            lnkother = "Other";
                                        }
                                        else
                                        {
                                            lnkother = lnkother;
                                        }
                                        lnkothercount = dtgr[2].ToString();
                                    }

                                    string rowname = dtgr[0].ToString();

                                    AddDatatoTable123413(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), System.Web.HttpUtility.HtmlEncode(rowname), dr1234[irg1][6].ToString(), colgroupoptions123413);
                                }


                                dtgresp = dtg.NewRow();
                                dtgresp[0] = "# of Respondents";
                                dtgresp[1] = " ";
                                dtgresp[2] = dsansoptionsall.Tables[1].Rows[iseq][7].ToString();
                                dtg.Rows.Add(dtgresp);


                                grdvall.DataSource = dtg;

                                grdvall.DataBind();

                                grdvall.HeaderRow.Cells[0].Visible = false;
                                grdvall.HeaderRow.Cells[1].Visible = false;
                                grdvall.HeaderRow.Cells[2].Visible = false;


                                htcgx.Controls.Add(grdvall);

                                htrgx.Cells.Add(htcgx);



                                if ((rblgrp.SelectedValue == "DataChart") || (rblgrp.SelectedValue == "Data"))
                                {

                                    grdvall.Visible = true;
                                    qt3table.Rows.Add(htrgx);

                                   // if (lnkother == "Other")
                                    if (lnkother != "")
                                    {

                                        HtmlTableRow hrtext = new HtmlTableRow();
                                        hrtext.BorderColor = "white";
                                        hrtext.Height = "30px";
                                        HtmlTableCell hctext = new HtmlTableCell();

                                        HyperLink hql = new HyperLink();
                                        hql.Font.Bold = true;
                                        hql.Font.Underline = true;
                                        hql.ForeColor = System.Drawing.Color.Purple;
                                        hql.Text = "View" + " " + lnkothercount + " " + "Other Responses";

                                        DateTime starttext = dt1.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt1;
                                        DateTime Endtext = dt2.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt2;
                                        string NavUrl = Constants.SURVEYID + "=" + surveyID + "&QuesID=" + dsansoptionsall.Tables[1].Rows[iseq][1].ToString() + "&Mode=" + Mode + "&surveyFlag=" + SurveyFlag + "&Date_Start=" + starttext + "&Date_End=" + Endtext;
                                        if (ViewAllQues == "All")
                                            NavUrl += "&Veiw=" + ViewAllQues;
                                        hql.NavigateUrl = EncryptHelper.EncryptQuerystring("DescriptiveResponses.aspx", NavUrl);
                                        hctext.Controls.Add(hql);
                                        hrtext.Cells.Add(hctext);
                                        qt3table.Rows.Add(hrtext);

                                    }
                                }
                                if (loggedInUserInfo.LicenseType != "FREE" || loggedInUserInfo.LicenseType == "FREE")
                                {
                                    if ((rblgrp.SelectedValue == "DataChart") || (rblgrp.SelectedValue == "Chart"))
                                    {

                                        HtmlTableRow htrcharttemp = new HtmlTableRow();
                                        HtmlTableCell htccharttemp = new HtmlTableCell();
                                        htccharttemp.ColSpan = 3;
                                        //   htccharttemp.Width = "100%";
                                        System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp = new System.Web.UI.WebControls.Label();

                                        string strunique1 = GetUniqueKey();
                                        string uniquestr = SelectedSurvey.SURVEY_ID.ToString() + dsansoptionsall.Tables[1].Rows[iseq][1].ToString();

                                        jsstrchartname = "pptimg" +SelectedSurvey.SURVEY_ID.ToString() ;// uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");
                                        jsstrchartname = "pptimg" + dsansoptionsall.Tables[1].Rows[iseq][1].ToString();


                                        StringBuilder strXML = new StringBuilder();
                                        string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseq][2].ToString())).Replace("\r", "").Replace("\n", "");
                                        //  string strchartname = strchartname1.Replace("&amp;nbsp;", "").Trim();
                                        string strchartname = "";


                                        if ((dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "1") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "2") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "3"))
                                        {
                                            strXML.Append("<chart caption='" + strchartname + "'  animate3D='1' xAxisName='' showValues='1' labelStep='0'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='save'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif'  >");
                                        }
                                        if ((dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "4"))
                                        {
                                            strXML.Append("<chart caption='" + strchartname + "'  pieSliceDepth='30' showBorder='1' animated='1' interactiveLegend='1' formatNumberScale='0' numberSuffix='%'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='save'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif' >");
                                        }

                                        foreach (DataRow dr in colgroupoptions123413.Rows)
                                        {

                                            strXML.AppendFormat("<set label='{0}' value='{1}' />", dr[1].ToString().Replace("\r", "").Replace("\n", ""), dr[2].ToString());
                                        }

                                        //Close <chart> element
                                        strXML.Append("</chart>");
                                        //reportcontent.InnerHtml = strXML.ToString();
                                        string charttypename = "";
                                        if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "1")
                                        {
                                            charttypename = "../Charts/Column3D.swf";
                                        }
                                        else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "2")
                                        {
                                            charttypename = "../Charts/Bar2D.swf";
                                        }
                                        else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "3")
                                        {
                                            charttypename = "../Charts/Bar2D.swf";
                                        }
                                        else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "4")
                                        {
                                            charttypename = "../Charts/Pie3D.swf";
                                        }

                                        LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart(charttypename, "", strXML.ToString(), strunique1 + uniquestr, "598", "300", false, true, false);
                                        lblqt3cellpertotalcharttemp.Text = LiteralQTInitial.Text;
                                        htccharttemp.Controls.Add(lblqt3cellpertotalcharttemp);
                                        htrcharttemp.Cells.Add(htccharttemp);
                                        qt3table.Rows.Add(htrcharttemp);
                                        
                                        AddDatatoTablechartid(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), dtchart);
                                    }
                                }
                                HtmlTableRow htrchartbreak = new HtmlTableRow();
                                HtmlTableCell htcchartbreak = new HtmlTableCell();
                                htcchartbreak.ColSpan = 3;
                                htcchartbreak.Width = "100%";
                                htcchartbreak.Height = "10px";
                                htrchartbreak.Cells.Add(htcchartbreak);
                                qt3table.Rows.Add(htrchartbreak);

                            }

                            if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "13")
                            {
                                DataRow[] dr13 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                                GridView grdvall13 = new GridView();
                                grdvall13.ID = "gridQT" + iseq;
                                grdvall13.Width = 598;
                                grdvall13.RowStyle.Height = 25;
                                grdvall13.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                                grdvall13.GridLines = 0;
                                grdvall13.RowDataBound += new GridViewRowEventHandler(grdvall13_RowDataBound);


                                DataTable dtg13 = new DataTable();
                                DataRow dtgr13;
                                DataRow dtgrcs;
                                DataRow dtgresp13;

                                dtg13.Columns.Add("Answeroption");
                                dtg13.Columns.Add("percentage");
                                dtg13.Columns.Add("count");

                                int constsum = 0;


                                for (int irg1 = 0; irg1 < dr13.Length; irg1++)
                                {
                                    dtgr13 = dtg13.NewRow();

                                    dtgr13[0] = dr13[irg1][2].ToString();
                                    dtgr13[1] = dr13[irg1][6].ToString() + "%";
                                    dtgr13[2] = dr13[irg1][5].ToString();

                                    constsum = constsum + Convert.ToInt32(dr13[irg1][5].ToString());

                                    dtg13.Rows.Add(dtgr13);

                                    AddDatatoTable123413(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), System.Web.HttpUtility.HtmlEncode(dr13[irg1][2].ToString()), dr13[irg1][6].ToString(), colgroupoptions123413);

                                }

                                dtgrcs = dtg13.NewRow();
                                dtgrcs[0] = "Constant Sum";
                                dtgrcs[1] = " ";
                                dtgrcs[2] = constsum;
                                dtg13.Rows.Add(dtgrcs);

                                dtgresp13 = dtg13.NewRow();
                                dtgresp13[0] = "# of Respondents";
                                dtgresp13[1] = " ";
                                int respcount = Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][4]) * Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][7]);
                                dtgresp13[2] = respcount.ToString();
                                dtg13.Rows.Add(dtgresp13);

                                grdvall13.DataSource = dtg13;

                                grdvall13.DataBind();

                                grdvall13.HeaderRow.Cells[0].Visible = false;
                                grdvall13.HeaderRow.Cells[1].Visible = false;
                                grdvall13.HeaderRow.Cells[2].Visible = false;

                                htcgx.Controls.Add(grdvall13);

                                htrgx.Cells.Add(htcgx);

                                if ((rblgrp.SelectedValue == "DataChart") || (rblgrp.SelectedValue == "Data"))
                                {
                                    grdvall13.Visible = true;
                                    qt3table.Rows.Add(htrgx);
                                }
                                if (loggedInUserInfo.LicenseType != "FREE" || loggedInUserInfo.LicenseType == "FREE")
                                {
                                    if ((rblgrp.SelectedValue == "DataChart") || (rblgrp.SelectedValue == "Chart"))
                                    {
                                        HtmlTableRow htrcharttemp13 = new HtmlTableRow();
                                        HtmlTableCell htccharttemp13 = new HtmlTableCell();
                                        htccharttemp13.Width = "100%";
                                        htccharttemp13.ColSpan = 3;
                                        System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp13 = new System.Web.UI.WebControls.Label();

                                        string strunique113 = GetUniqueKey();
                                        string uniquestr13 = SelectedSurvey.SURVEY_ID.ToString() + dsansoptionsall.Tables[1].Rows[iseq][1].ToString();

                                        jsstrchartname = uniquestr13 + DateTime.Now.ToString("ddMMyyyyhhMMss");


                                        StringBuilder strXML = new StringBuilder();
                                        string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseq][2].ToString())).Replace("\r", "").Replace("\n", "");
                                        //   string strchartname = strchartname1.Replace("&amp;nbsp;", "").Trim();
                                        string strchartname = "";
                                        strXML.Append("<chart caption='" + strchartname + "'  pieSliceDepth='30' showBorder='1' animated='1' interactiveLegend='1' formatNumberScale='0' numberSuffix='%'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif' >");
                                        //  }

                                        foreach (DataRow dr in colgroupoptions123413.Rows)
                                        {

                                            strXML.AppendFormat("<set label='{0}' value='{1}' />", dr[1].ToString().Replace("\r", "").Replace("\n", ""), dr[2].ToString());
                                        }

                                        //Close <chart> element
                                        strXML.Append("</chart>");


                                        LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/Doughnut3D.swf", "", strXML.ToString(), strunique113 + uniquestr13, "598", "300", false, true, false);

                                        lblqt3cellpertotalcharttemp13.Text = LiteralQTInitial.Text;

                                        htccharttemp13.Controls.Add(lblqt3cellpertotalcharttemp13);
                                        htrcharttemp13.Cells.Add(htccharttemp13);
                                        qt3table.Rows.Add(htrcharttemp13);

                                    }
                                }
                                HtmlTableRow htrchartbreak13 = new HtmlTableRow();
                                HtmlTableCell htcchartbreak13 = new HtmlTableCell();
                                htcchartbreak13.ColSpan = 3;
                                htcchartbreak13.Width = "100%";
                                htcchartbreak13.Height = "10px";
                                htrchartbreak13.Cells.Add(htcchartbreak13);
                                qt3table.Rows.Add(htrchartbreak13);
                            }
                            if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "10" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "11")
                            {
                                // DataSet dsmtxcols = surcore.getMatrixrowscolsQID(SelectedSurvey.surveyQues[iseq].QUESTION_ID);

                                DataRow[] dr1011 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                                System.Data.DataTable mtxcolnames = createMtxColumnDataTable();

                                System.Data.DataTable mtxrownames = createMtxRowDataTable();

                                for (int imtxrow = 0; imtxrow < dr1011.Length; imtxrow++)
                                {

                                    if (imtxrow == 0)
                                    {
                                        AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][2].ToString()), mtxrownames);
                                    }
                                    else
                                    {
                                        DataRow[] dr = mtxrownames.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()) + "'");
                                        if (dr.Length == 0)
                                        {

                                            AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()), mtxrownames);

                                        }
                                    }
                                }

                                for (int imtxcol = 0; imtxcol < dr1011.Length; imtxcol++)
                                {

                                    if (imtxcol == 0)
                                    {
                                        AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][3].ToString().TrimStart()), mtxcolnames);
                                    }
                                    else
                                    {
                                        DataRow[] dr = mtxcolnames.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()) + "'");
                                        if (dr.Length == 0)
                                        {
                                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()), mtxcolnames);
                                        }
                                    }
                                }

                                GridView grdvall1011 = new GridView();
                                grdvall1011.ID = "gridQT" + iseq;
                                grdvall1011.Width = 598;
                                grdvall1011.RowStyle.Height = 25;
                                grdvall1011.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                                grdvall1011.GridLines = 0;
                                grdvall1011.RowDataBound += new GridViewRowEventHandler(grdvall1011_RowDataBound);


                                DataTable dtg1011 = new DataTable();
                                DataRow dtgr1011;
                                DataRow dtgrp1011;
                                DataRow dtgresp;

                                dtg1011.Columns.Add(" ");
                                for (int irg = 0; irg < mtxcolnames.Rows.Count; irg++)
                                {

                                    dtg1011.Columns.Add(System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[irg][0].ToString()));

                                }

                                for (int irg1 = 0; irg1 < mtxrownames.Rows.Count; irg1++)
                                {
                                    dtgr1011 = dtg1011.NewRow();
                                    dtgrp1011 = dtg1011.NewRow();
                                    DataRow[] dr1011f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                                    dtgr1011[0] = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString());
                                    dtgrp1011[0] = " ";
                                    for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                                    {
                                        dtgr1011[cgv + 1] = dr1011f[cgv][5].ToString();
                                        dtgrp1011[cgv + 1] = dr1011f[cgv][6].ToString() + "%";


                                        AddDatatoTable(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), mtxrownames.Rows[irg1][0].ToString(), System.Web.HttpUtility.HtmlEncode(dr1011f[cgv][3].ToString()), dr1011f[cgv][6].ToString(), colgroupoptions);
                                    }


                                    dtg1011.Rows.Add(dtgr1011);
                                    dtg1011.Rows.Add(dtgrp1011);


                                }

                                dtgresp = dtg1011.NewRow();
                                dtgresp[0] = "# of Respondents";
                                dtgresp[mtxcolnames.Rows.Count] = dsansoptionsall.Tables[1].Rows[iseq][7].ToString();
                                dtg1011.Rows.Add(dtgresp);


                                grdvall1011.DataSource = dtg1011;
                                grdvall1011.DataBind();
                                htcgx.Controls.Add(grdvall1011);
                                htrgx.Cells.Add(htcgx);

                                if ((rblgrp.SelectedValue == "DataChart") || (rblgrp.SelectedValue == "Data"))
                                {
                                    grdvall1011.Visible = true;
                                    qt3table.Rows.Add(htrgx);
                                }
                                if (loggedInUserInfo.LicenseType != "FREE" || loggedInUserInfo.LicenseType == "FREE")
                                {
                                    if ((rblgrp.SelectedValue == "DataChart") || (rblgrp.SelectedValue == "Chart"))
                                    {
                                        HtmlTableRow htrcharttemp1011 = new HtmlTableRow();
                                        HtmlTableCell htccharttemp1011 = new HtmlTableCell();
                                        htccharttemp1011.Width = "100%";
                                        htccharttemp1011.ColSpan = mtxcolnames.Rows.Count + 1;
                                        System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp1011 = new System.Web.UI.WebControls.Label();

                                        string strunique11011 = GetUniqueKey();
                                        string uniquestr1011 = SelectedSurvey.SURVEY_ID.ToString() + dsansoptionsall.Tables[1].Rows[iseq][1].ToString();

                                        jsstrchartname = uniquestr1011 + DateTime.Now.ToString("ddMMyyyyhhMMss");


                                        StringBuilder strXML = new StringBuilder();

                                        string strchartnamematrix1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseq][2].ToString())).Replace("\r", "").Replace("\n", "");
                                        //  string strchartnamematrix = strchartnamematrix1.Replace("&amp;nbsp;", "").Trim();
                                        string strchartnamematrix = "";
                                        ////////Generate the chart element string

                                        strXML.Append("<chart  caption='" + strchartnamematrix + "' animate3D='1' xAxisName='' showValues='1' labelStep='0' rotateXAxisName ='0'   canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif' >");
                                        strXML.AppendFormat("<categories>");

                                        foreach (DataRow dr in mtxrownames.Rows)
                                        {

                                            strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r", "").Replace("\n", ""));
                                        }
                                        strXML.AppendFormat("</categories>");

                                        for (int mcans1chart = 0; mcans1chart < mtxcolnames.Rows.Count; mcans1chart++)
                                        {

                                            strXML.AppendFormat("<dataset seriesName='{0}'>", mtxcolnames.Rows[mcans1chart][0].ToString().Replace("\r", "").Replace("\n", ""));

                                            for (int mcansrowschart = 0; mcansrowschart < mtxrownames.Rows.Count; mcansrowschart++)
                                            {

                                                DataRow[] dr1 = colgroupoptions.Select("QuestionID='" + dsansoptionsall.Tables[1].Rows[iseq][1].ToString() + "'" + "and rowansweroption='" + mtxrownames.Rows[mcansrowschart][0].ToString() + "'" + "and columnansweroption ='" + mtxcolnames.Rows[mcans1chart][0].ToString() + "'");

                                                if (dr1.Length > 0)
                                                {
                                                    strXML.AppendFormat("<set value='{0}'/>", dr1[0].ItemArray[3].ToString());
                                                }
                                                else
                                                {
                                                    strXML.AppendFormat("<set value='{0}'/>", "0");
                                                }

                                            }
                                            strXML.AppendFormat("</dataset>");
                                        }

                                        //Close <chart> element
                                        strXML.Append("</chart>");


                                        //Create the chart - Column 2D Chart with data from strXML
                                        LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/MSLine.swf", "", strXML.ToString(), strunique11011 + uniquestr1011, "598", "300", false, true, false);
                                        lblqt3cellpertotalcharttemp1011.Text = LiteralQTInitial.Text;

                                        htccharttemp1011.Controls.Add(lblqt3cellpertotalcharttemp1011);
                                        htrcharttemp1011.Cells.Add(htccharttemp1011);
                                        qt3table.Rows.Add(htrcharttemp1011);

                                    }
                                }
                                HtmlTableRow htrchartbreak1011 = new HtmlTableRow();
                                HtmlTableCell htcchartbreak1011 = new HtmlTableCell();
                                htcchartbreak1011.ColSpan = 3;
                                htcchartbreak1011.Width = "100%";
                                htcchartbreak1011.Height = "10px";
                                htrchartbreak1011.Cells.Add(htcchartbreak1011);
                                qt3table.Rows.Add(htrchartbreak1011);

                                mtxrownames.Clear();
                                mtxcolnames.Clear();
                            }

                            if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "12")
                            {

                                // DataSet dsmtxcols = surcore.getMatrixsidebysideQID(SelectedSurvey.surveyQues[iseq].QUESTION_ID);
                                DataRow[] dr12 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                                System.Data.DataTable mtxrownames = createMtxRowDataTable();

                                System.Data.DataTable mtxcolnames = createMtxColumnDataTable();

                                System.Data.DataTable mtxsubcolnames = createMtxColumnDataTable();

                                for (int imtxrow = 0; imtxrow < dr12.Length; imtxrow++)
                                {

                                    if (imtxrow == 0)
                                    {
                                        AddMtxrowsDatatoTable(dr12[0][2].ToString(), mtxrownames);
                                    }
                                    else
                                    {
                                        DataRow[] dr = mtxrownames.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()) + "'");
                                        if (dr.Length == 0)
                                        {

                                            AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()), mtxrownames);

                                        }
                                    }
                                }

                                for (int imtxcol = 0; imtxcol < dr12.Length; imtxcol++)
                                {

                                    if (imtxcol == 0)
                                    {
                                        AddMtxcolsDatatoTable(dr12[0][3].ToString(), mtxcolnames);
                                    }
                                    else
                                    {
                                        DataRow[] dr = mtxcolnames.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()) + "'");
                                        if (dr.Length == 0)
                                        {

                                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()), mtxcolnames);

                                        }
                                    }
                                }

                                for (int imtxsubcol = 0; imtxsubcol < dr12.Length; imtxsubcol++)
                                {

                                    if (imtxsubcol == 0)
                                    {
                                        AddMtxcolsDatatoTable(dr12[0][4].ToString(), mtxsubcolnames);
                                    }
                                    else
                                    {

                                        DataRow[] dr = mtxsubcolnames.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()) + "'");
                                        if (dr.Length == 0)
                                        {

                                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()), mtxsubcolnames);

                                        }
                                    }
                                }


                                GridView grdvall12 = new GridView();
                                grdvall12.ID = "gridQT" + iseq;
                                grdvall12.Width = 598;
                                grdvall12.RowStyle.Height = 25;
                                grdvall12.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                                grdvall12.GridLines = 0;
                                grdvall12.RowDataBound += new GridViewRowEventHandler(grdvall12_RowDataBound);


                                DataTable dtg12 = new DataTable();
                                DataRow dtgr12;
                                DataRow dtgr12data;
                                DataRow dtgr12pcntg;
                                DataRow dtgresp;
                                dtg12.Columns.Add(" ");
                                int cval = 0;
                                StringBuilder ColumnName = new StringBuilder(" ");

                                for (int irg12 = 0; irg12 < mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count; irg12++)
                                {
                                    if ((irg12 == 0) || (irg12 == mtxsubcolnames.Rows.Count))
                                    {
                                        dtg12.Columns.Add(System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[cval][0].ToString()));
                                        cval = cval + 1;
                                    }
                                    else
                                    {

                                        try
                                        {
                                            dtg12.Columns.Add(System.Web.HttpUtility.HtmlDecode(ColumnName.ToString()));
                                        }
                                        catch (Exception ex)
                                        {
                                            ColumnName.Append(" ");
                                            dtg12.Columns.Add(System.Web.HttpUtility.HtmlDecode(ColumnName.ToString()));
                                        }

                                    }

                                }

                                dtgr12 = dtg12.NewRow();
                                for (int irg112 = 0; irg112 < mtxrownames.Rows.Count; irg112++)
                                {

                                    DataRow[] dr12sub = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg112][0].ToString()).Replace("'", "''") + "' and seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");
                                    for (int cgv12 = 0; cgv12 < dr12sub.Length; cgv12++)
                                    {
                                        dtgr12[cgv12 + 1] = System.Web.HttpUtility.HtmlDecode(dr12sub[cgv12][4].ToString());
                                    }

                                }
                                dtg12.Rows.Add(dtgr12);


                                for (int irg12data = 0; irg12data < mtxrownames.Rows.Count; irg12data++)
                                {
                                    dtgr12data = dtg12.NewRow();
                                    dtgr12pcntg = dtg12.NewRow();
                                    DataRow[] dr12f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg12data][0].ToString()).Replace("'", "''") + "' and seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                                    dtgr12data[0] = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg12data][0].ToString());
                                    dtgr12pcntg[0] = " ";
                                    for (int cgv12 = 0; cgv12 < dr12f.Length; cgv12++)
                                    {
                                        dtgr12data[cgv12 + 1] = dr12f[cgv12][5].ToString();
                                        dtgr12pcntg[cgv12 + 1] = dr12f[cgv12][6].ToString() + "%";


                                        AddDatatoTableQ12(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), System.Web.HttpUtility.HtmlEncode(dr12f[cgv12][3].ToString()), System.Web.HttpUtility.HtmlEncode(dr12f[cgv12][4].ToString()), mtxrownames.Rows[irg12data][0].ToString(), dr12f[cgv12][6].ToString(), colgroupoptionsQ12);
                                    }

                                    dtg12.Rows.Add(dtgr12data);
                                    dtg12.Rows.Add(dtgr12pcntg);


                                }

                                dtgresp = dtg12.NewRow();
                                dtgresp[0] = "# of Respondents";
                                dtgresp[mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count] = dsansoptionsall.Tables[1].Rows[iseq][7].ToString();
                                dtg12.Rows.Add(dtgresp);


                                grdvall12.DataSource = dtg12;
                                grdvall12.DataBind();

                                htcgx.Controls.Add(grdvall12);
                                htrgx.Cells.Add(htcgx);


                                if ((rblgrp.SelectedValue == "DataChart") || (rblgrp.SelectedValue == "Data"))
                                {

                                    grdvall12.Visible = true;
                                    qt3table.Rows.Add(htrgx);
                                }
                                if (loggedInUserInfo.LicenseType != "FREE" || loggedInUserInfo.LicenseType == "FREE")
                                {
                                    if ((rblgrp.SelectedValue == "DataChart") || (rblgrp.SelectedValue == "Chart"))
                                    {
                                        HtmlTableRow htrcharttemp12 = new HtmlTableRow();
                                        HtmlTableCell htccharttemp12 = new HtmlTableCell();
                                        htccharttemp12.Width = "100%";
                                        htccharttemp12.ColSpan = mtxsubcolnames.Rows.Count + 1;
                                        System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp12 = new System.Web.UI.WebControls.Label();

                                        string strunique112 = GetUniqueKey();
                                        string uniquestr12 = SelectedSurvey.SURVEY_ID.ToString() + dsansoptionsall.Tables[1].Rows[iseq][1].ToString();

                                        jsstrchartname = uniquestr12 + DateTime.Now.ToString("ddMMyyyyhhMMss");
                                        StringBuilder strXML = new StringBuilder();

                                        string strchartnamematrixsbs1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseq][2].ToString())).Replace("\r", "").Replace("\n", "");
                                        //     string strchartnamematrixsbs = strchartnamematrixsbs1.Replace("&amp;nbsp;", "").Trim();
                                        string strchartnamematrixsbs = "";

                                        ////////Generate the chart element string

                                        strXML.Append("<chart  caption='" + strchartnamematrixsbs + "' animate3D='1' xAxisName='' showValues='1' labelStep='0' rotateXAxisName ='0' labelDisplay='Rotate' slantLabels='1'   canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1'  font='Arial, Helvetica, sans-serif' >");


                                        strXML.AppendFormat("<categories>");

                                        foreach (DataRow dr in mtxrownames.Rows)
                                        {
                                            strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r", "").Replace("\n", "") + "-" + mtxcolnames.Rows[0][0].ToString());

                                            for (int i = 1; i <= mtxcolnames.Rows.Count - 1; i++)
                                            {

                                                strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r", "").Replace("\n", "") + "-" + mtxcolnames.Rows[i][0].ToString());

                                            }

                                        }
                                        strXML.AppendFormat("</categories>");


                                        for (int colval = 0; colval < mtxsubcolnames.Rows.Count; colval++)
                                        {
                                            strXML.AppendFormat("<dataset seriesName='{0}'>", mtxsubcolnames.Rows[colval][0].ToString().Replace("\r", "").Replace("\n", ""));

                                            for (int colvalx = 0; colvalx < mtxrownames.Rows.Count; colvalx++)
                                            {

                                                string rowtot;


                                                for (int rot = 0; rot < mtxcolnames.Rows.Count; rot++)
                                                {
                                                    DataRow[] dr1 = null;

                                                    dr1 = colgroupoptionsQ12.Select("QuestionID='" + dsansoptionsall.Tables[1].Rows[iseq][1].ToString() + "'" + "and columnoption='" + System.Web.HttpUtility.HtmlEncode(mtxcolnames.Rows[rot][0].ToString()) + "'" + "and subcolumnoption ='" + mtxsubcolnames.Rows[colval][0].ToString() + "'" + "and rowoption='" + System.Web.HttpUtility.HtmlEncode(mtxrownames.Rows[colvalx][0].ToString()) + "'");

                                                    if (dr1.Length > 0)
                                                    {
                                                        strXML.AppendFormat("<set value='{0}'/>", dr1[0].ItemArray[4].ToString());
                                                    }
                                                    else
                                                    {
                                                        strXML.AppendFormat("<set value='{0}'/>", "0");
                                                    }

                                                }
                                            }

                                            strXML.AppendFormat("</dataset>");
                                        }


                                        //Close <chart> element
                                        strXML.Append("</chart>");


                                        //Create the chart - Column 2D Chart with data from strXML
                                        LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/MSColumn3D.swf", "", strXML.ToString(), strunique112 + uniquestr12, "598", "300", false, true, false);

                                        lblqt3cellpertotalcharttemp12.Text = LiteralQTInitial.Text;

                                        htccharttemp12.Controls.Add(lblqt3cellpertotalcharttemp12);
                                        htrcharttemp12.Cells.Add(htccharttemp12);
                                        qt3table.Rows.Add(htrcharttemp12);
                                    }
                                }
                                HtmlTableRow htrchartbreak12 = new HtmlTableRow();
                                HtmlTableCell htcchartbreak12 = new HtmlTableCell();
                                htcchartbreak12.ColSpan = 3;
                                htcchartbreak12.Width = "100%";
                                htcchartbreak12.Height = "10px";
                                htrchartbreak12.Cells.Add(htcchartbreak12);
                                qt3table.Rows.Add(htrchartbreak12);

                                mtxcolnames.Clear();
                                mtxrownames.Clear();
                                mtxsubcolnames.Clear();
                            }

                            if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "15")
                            {
                                DataRow[] dr15 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                                System.Data.DataTable mtxcolnames15 = createMtxColumnDataTable();

                                System.Data.DataTable mtxrownames15 = createMtxRowDataTable();

                                for (int imtxrow = 0; imtxrow < dr15.Length; imtxrow++)
                                {

                                    if (imtxrow == 0)
                                    {
                                        AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][2].ToString()), mtxrownames15);
                                    }
                                    else
                                    {

                                        DataRow[] dr = mtxrownames15.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()) + "'");


                                        if (dr.Length == 0)
                                        {

                                            AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()), mtxrownames15);

                                        }
                                    }
                                }


                                for (int imtxcol = 0; imtxcol < dr15.Length; imtxcol++)
                                {

                                    if (imtxcol == 0)
                                    {
                                        AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][3].ToString().TrimStart()), mtxcolnames15);
                                    }
                                    else
                                    {
                                        DataRow[] dr = mtxcolnames15.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()) + "'");


                                        if (dr.Length == 0)
                                        {

                                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()), mtxcolnames15);

                                        }
                                    }
                                }

                                GridView grdvall15 = new GridView();
                                grdvall15.ID = "gridQT" + iseq;
                                grdvall15.Width = 598;
                                grdvall15.RowStyle.Height = 25;
                                grdvall15.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                                grdvall15.GridLines = 0;
                                grdvall15.RowDataBound += new GridViewRowEventHandler(grdvall15_RowDataBound);


                                DataTable dtg15 = new DataTable();
                                DataRow dtgr15;
                                DataRow dtgr115;
                                DataRow dtgresp;

                                dtg15.Columns.Add(" ");
                                for (int irg = 0; irg < mtxcolnames15.Rows.Count; irg++)
                                {

                                    dtg15.Columns.Add(System.Web.HttpUtility.HtmlDecode(mtxcolnames15.Rows[irg][0].ToString()));

                                }

                                for (int irg1 = 0; irg1 < mtxrownames15.Rows.Count; irg1++)
                                {
                                    dtgr15 = dtg15.NewRow();
                                    dtgr115 = dtg15.NewRow();
                                    DataRow[] dr15f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames15.Rows[irg1][0].ToString()).Replace("'", "''") + "'" + "and seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                                    dtgr15[0] = System.Web.HttpUtility.HtmlDecode(mtxrownames15.Rows[irg1][0].ToString());
                                    dtgr115[0] = " ";
                                    for (int cgv = 0; cgv < dr15f.Length; cgv++)
                                    {
                                        dtgr15[cgv + 1] = dr15f[cgv][5].ToString();
                                        dtgr115[cgv + 1] = dr15f[cgv][6].ToString() + "%";



                                        AddDatatoTable(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), System.Web.HttpUtility.HtmlEncode(mtxrownames15.Rows[irg1][0].ToString()), System.Web.HttpUtility.HtmlEncode(dr15f[cgv][3].ToString()), dr15f[cgv][6].ToString(), colgroupoptions15);

                                    }

                                    //  dtg.Rows.Add(mtxrownames.Rows[irg1][0].ToString(),dr[irg1][4].ToString());

                                    dtg15.Rows.Add(dtgr15);
                                    dtg15.Rows.Add(dtgr115);


                                }



                                dtgresp = dtg15.NewRow();
                                dtgresp[0] = "# of Respondents";
                                dtgresp[mtxcolnames15.Rows.Count] = dsansoptionsall.Tables[1].Rows[iseq][7].ToString();
                                dtg15.Rows.Add(dtgresp);

                                grdvall15.DataSource = dtg15;
                                grdvall15.DataBind();
                                htcgx.Controls.Add(grdvall15);
                                htrgx.Cells.Add(htcgx);


                                if ((rblgrp.SelectedValue == "DataChart") || (rblgrp.SelectedValue == "Data"))
                                {

                                    grdvall15.Visible = true;
                                    qt3table.Rows.Add(htrgx);

                                }
                                if (loggedInUserInfo.LicenseType != "FREE" || loggedInUserInfo.LicenseType == "FREE")
                                {
                                    if ((rblgrp.SelectedValue == "DataChart") || (rblgrp.SelectedValue == "Chart"))
                                    {
                                        HtmlTableRow htrcharttemp15 = new HtmlTableRow();
                                        HtmlTableCell htccharttemp15 = new HtmlTableCell();
                                        htccharttemp15.Width = "100%";
                                        htccharttemp15.ColSpan = mtxcolnames15.Rows.Count + 1;
                                        System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp15 = new System.Web.UI.WebControls.Label();

                                        string strunique115 = GetUniqueKey();
                                        string uniquestr15 = SelectedSurvey.SURVEY_ID.ToString() + dsansoptionsall.Tables[1].Rows[iseq][1].ToString();


                                        jsstrchartname = uniquestr15 + DateTime.Now.ToString("ddMMyyyyhhMMss");


                                        StringBuilder strXML = new StringBuilder();

                                        string strchartnamematrixsbs1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseq][2].ToString())).Replace("\r", "").Replace("\n", "");
                                        //  string strchartnamematrixsbs = strchartnamematrixsbs1.Replace("&amp;nbsp;", "").Trim();

                                        string strchartnamematrixsbs = "";
                                        strXML.Append("<chart  caption='" + strchartnamematrixsbs + "' animate3D='1' xAxisName='' showValues='1' labelStep='0' rotateXAxisName ='0' labelDisplay='Rotate' slantLabels='1'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1' showBorder='1' borderColor='6e1473' borderThickness='1' paletteColors='5bbcf0,62d955,6e1473,fafa0a,23e868,eb101b,c94b77,941515,91a3ed,c73acf,158a7e'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  numberSuffix='%' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif' >");

                                        strXML.AppendFormat("<categories>");

                                        foreach (DataRow dr in mtxrownames15.Rows)
                                        {

                                            strXML.AppendFormat("<category label='{0}'/>", dr[0].ToString().Replace("\r", "").Replace("\n", ""));

                                        }
                                        strXML.AppendFormat("</categories>");

                                        for (int mcansprankchart = 0; mcansprankchart < mtxcolnames15.Rows.Count; mcansprankchart++)
                                        {

                                            strXML.AppendFormat("<dataset seriesName='{0}'>", mtxcolnames15.Rows[mcansprankchart][0].ToString().Replace("\r", "").Replace("\n", ""));
                                            for (int mcansrowsrankchart = 0; mcansrowsrankchart < mtxrownames15.Rows.Count; mcansrowsrankchart++)
                                            {

                                                DataRow[] dr15cht = null;

                                                dr15cht = colgroupoptions15.Select("QuestionID='" + dsansoptionsall.Tables[1].Rows[iseq][1].ToString() + "'" + "and rowansweroption='" + System.Web.HttpUtility.HtmlEncode(mtxrownames15.Rows[mcansrowsrankchart][0].ToString()) + "'" + "and columnansweroption ='" + System.Web.HttpUtility.HtmlEncode(mtxcolnames15.Rows[mcansprankchart][0].ToString()) + "'");
                                                if (dr15cht.Length > 0)
                                                {
                                                    strXML.AppendFormat("<set value='{0}'/>", dr15cht[0].ItemArray[3].ToString());
                                                }
                                                else
                                                {
                                                    strXML.AppendFormat("<set value='{0}'/>", "0");
                                                }

                                            }
                                            strXML.AppendFormat("</dataset>");
                                        }
                                        strXML.Append("</chart>");

                                        //Create the chart - Column 2D Chart with data from strXML
                                        LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart("../Charts/MSColumn3D.swf", "", strXML.ToString(), strunique115 + uniquestr15, "598", "300", false, true, false);
                                        lblqt3cellpertotalcharttemp15.Text = LiteralQTInitial.Text;

                                        htccharttemp15.Controls.Add(lblqt3cellpertotalcharttemp15);
                                        htrcharttemp15.Cells.Add(htccharttemp15);
                                        qt3table.Rows.Add(htrcharttemp15);

                                    }
                                }
                                HtmlTableRow htrchartbreak15 = new HtmlTableRow();
                                HtmlTableCell htcchartbreak15 = new HtmlTableCell();
                                htcchartbreak15.ColSpan = 3;
                                htcchartbreak15.Width = "100%";
                                htcchartbreak15.Height = "10px";
                                htrchartbreak15.Cells.Add(htcchartbreak15);
                                qt3table.Rows.Add(htrchartbreak15);
                            }


                            if ((dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "5") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "6") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "9") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "7") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "20") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "8") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "14"))
                            {
                                // DataSet dstextresponses = surcore.getTextResponsesQID(SelectedSurvey.surveyQues[iseq].QUESTION_ID);
                                DataRow[] drtext = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");


                                HtmlTableRow hrtext = new HtmlTableRow();
                                hrtext.BgColor = "#bf92cb";
                                hrtext.BorderColor = "white";
                                hrtext.Height = "30px";
                                HtmlTableCell hctext = new HtmlTableCell();
                                // hctext.BgColor = "BurlyWood";
                                HyperLink hql = new HyperLink();
                                hql.Font.Bold = true;
                                hql.Font.Underline = true;
                                hql.ForeColor = System.Drawing.Color.Blue;
                                hql.Text = "You have" + " " + drtext[0][5].ToString() + " " + "responses for this question.";

                                DateTime starttext = dt1.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt1;
                                DateTime Endtext = dt2.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt2;
                                string NavUrl = Constants.SURVEYID + "=" + surveyID + "&QuesID=" + dsansoptionsall.Tables[1].Rows[iseq][1].ToString() + "&Mode=" + Mode + "&surveyFlag=" + SurveyFlag + "&Date_Start=" + starttext + "&Date_End=" + Endtext;
                                if (ViewAllQues == "All")
                                    NavUrl += "&Veiw=" + ViewAllQues;
                                hql.NavigateUrl = EncryptHelper.EncryptQuerystring("DescriptiveResponses.aspx", NavUrl);
                                hctext.Controls.Add(hql);
                                hrtext.Cells.Add(hctext);
                                qt3table.Rows.Add(hrtext);

                                HtmlTableRow htrchartbreaktext = new HtmlTableRow();
                                HtmlTableCell htcchartbreaktext = new HtmlTableCell();
                                htcchartbreaktext.ColSpan = 3;
                                htcchartbreaktext.Width = "100%";
                                htcchartbreaktext.Height = "10px";
                                htrchartbreaktext.Cells.Add(htcchartbreaktext);
                                qt3table.Rows.Add(htrchartbreaktext);
                            }
                            if ((dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "16") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "17") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "18") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "19") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "21") || (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "22"))
                            {
                                HtmlTableRow htrchartbreaknar = new HtmlTableRow();
                                HtmlTableCell htcchartbreaknar = new HtmlTableCell();
                                htcchartbreaknar.ColSpan = 3;
                                htcchartbreaknar.Width = "100%";
                                htcchartbreaknar.Height = "10px";
                                htrchartbreaknar.Cells.Add(htcchartbreaknar);
                                qt3table.Rows.Add(htrchartbreaknar);

                            }

                            Panel p1 = new Panel();
                            p1.ScrollBars = ScrollBars.Horizontal;
                            p1.Visible = true;
                            p1.Controls.Add(qt3table);

                            htchartcell.Controls.Add(qt3table);
                            htchartrow.Cells.Add(htchartcell);
                            qt3tableques.Rows.Add(htchartrow);

                            p1.Controls.Add(qt3tableques);
                            rpnl_reportsdata.Controls.Add(p1);
                            chartContainerParent.InnerText = "";



                            if (ViewAllQues == "All")
                            {
                                continue;
                            }
                            else
                            {
                                ASPxRoundPanelQuestionreports.Visible = true;
                                btnviewall.Visible = true;
                                Session["viewallclick"] = "viewonebyone";
                                Session["defaultchart"] = "notdefault";

                                break;
                            }

                        }

                    }


                }

                if (Mode == 0)
                {
                    //rbtnDataChart.Checked = true;

                }
                else if (Mode == 1)
                {
                    // rbtnData.Checked = true;
                }
                else if (Mode == 2)
                {
                    //rbtnChart.Checked = true;
                }

                CheckUserFeatureLinks();

            }
        }
        catch (Exception ex)
        {
            string toEmail = ConfigurationManager.AppSettings["ToEmail"];
            string ccEmail = ConfigurationManager.AppSettings["CCEmail"];

            Insighto.Business.Helpers.MailHelper.SendMailMessage(toEmail, toEmail, "", ccEmail, ex.Message, ex.ToString());
          
        }
    }
    #endregion

    private void FillRawDataForColumn(int colval, ExcelWorksheet worksheet, string QT, DataSet dsgetRawdataAnswersForQues,
                                         string AnswerId, DataTable dtRespId, string AnswerOption, string OtherOption)
    {
        // Get specific data for Question, Answer option and answer id
        DataRow[] drForRespId;
        int rownum = 2; // start from 3, reducing 1 for offset from rownum value
        string respondentId;
        string strRespIdQuery;

        if (QT == "6" || QT == "5" || QT == "7" || QT == "8" || QT == "14") // no answer options just a question and answers
        {
            DataRow[] drForAnswerId;
            string colName = "ANSWER_ID=" + AnswerId;
            drForAnswerId = dsgetRawdataAnswersForQues.Tables[0].Select(colName);
            for (int i = 0; i < drForAnswerId.Count(); i++)
            {
                // Get Rownum for respondent id
                respondentId = drForAnswerId[i][2].ToString();
                strRespIdQuery = "RESPONDENT_ID=" + respondentId;
                drForRespId = dtRespId.Select(strRespIdQuery);
                rownum = rownum + Convert.ToInt32(drForRespId[0][7].ToString());

                worksheet.Cells[rownum, colval].Value = drForAnswerId[i][1];

                // reset row num
                rownum = 2;
            }
        }
        else if (QT == "10" || QT == "11" || QT == "12" || QT == "15" || QT == "13")
        {
            // Increase optimization, too many rows iterated reduce rows
            string AnswerOptionwithPadding = "";
            DataRow[] dtTmpRawdataAnswersForOption = null;
            if (QT == "10")
            {
                AnswerOptionwithPadding = "$--$" + AnswerOption;

                string repsquoteAnswerOptionwithPadding = AnswerOptionwithPadding.Replace("'", "''");
                string expression = "ANSWER_OPTIONS LIKE '%" + repsquoteAnswerOptionwithPadding + "%'";
              //  string expression = "ANSWER_OPTIONS LIKE '%" + AnswerOptionwithPadding + "%'";
                dtTmpRawdataAnswersForOption = dsgetRawdataAnswersForQues.Tables[0].Select(expression);

            }
            else if (QT == "11")// replace "--" with "$--$"
            {
                AnswerOptionwithPadding = AnswerOption.Replace("--", "$--$");

                string repsquoteAnswerOptionwithPadding = AnswerOptionwithPadding.Replace("'", "''");
                string expression = "ANSWER_OPTIONS LIKE '%" + repsquoteAnswerOptionwithPadding + "%'";

                dtTmpRawdataAnswersForOption = dsgetRawdataAnswersForQues.Tables[0].Select(expression);
            }
            else if (QT == "12")
            {
                AnswerOptionwithPadding = AnswerOption.Replace("-", "$--$");


                string repsquoteAnswerOptionwithPadding = AnswerOptionwithPadding.Replace("'", "''");

                string expression = "ANSWER_OPTIONS LIKE '%" + repsquoteAnswerOptionwithPadding.Substring(0, repsquoteAnswerOptionwithPadding.IndexOf("$--$")) + "%'";
                dtTmpRawdataAnswersForOption = dsgetRawdataAnswersForQues.Tables[0].Select(expression);

            }
            else if (QT == "13" || QT == "15")
            {
                string expression = "ANSWER_OPTIONS LIKE '%" + AnswerOption + "%'";
                dtTmpRawdataAnswersForOption = dsgetRawdataAnswersForQues.Tables[0].Select(expression);
            }

            for (int i = 0; i < dtTmpRawdataAnswersForOption.Length; i++)
            {
                string Answer_String = dtTmpRawdataAnswersForOption[i][3].ToString();
                bool matchFound = false;
                int strIndex = 0;

                if (QT == "12")
                {
                    // Strip off answer from the middle and only compare with option string
                    // Find first location of $--$

                    int firstIdx = Answer_String.IndexOf("$--$");
                    int secondIdx = (Answer_String).IndexOf("$--$", firstIdx + 4);
                    string tmpStr = Answer_String.Substring(0, firstIdx) + Answer_String.Substring(secondIdx,
                                          Answer_String.Length - secondIdx);

                    if (AnswerOptionwithPadding.Equals(tmpStr))
                        matchFound = true;

                }
                if (QT == "11")
                    if (Answer_String.Equals(AnswerOptionwithPadding))
                        matchFound = true;
                if (QT == "10")
                {
                    if ((strIndex = Answer_String.IndexOf(AnswerOptionwithPadding)) > 0)
                        matchFound = true;
                }
                if (QT == "15" || QT == "13")
                    if (Answer_String.Equals(AnswerOption))
                        matchFound = true;

                if (matchFound)
                {
                    // Get rownum based on respondent id
                    string str_resp_id = "RESPONDENT_ID=" + dtTmpRawdataAnswersForOption[i][2].ToString();
                    drForRespId = dtRespId.Select(str_resp_id);
                    rownum = rownum + Convert.ToInt32(drForRespId[0][7].ToString());
                    if (QT == "11")
                        worksheet.Cells[rownum, colval].Value = "Yes";
                    else if (QT == "10")
                        worksheet.Cells[rownum, colval].Value = Answer_String.Substring(0, strIndex);
                    else if (QT == "15" || QT == "13")
                        worksheet.Cells[rownum, colval].Value = dtTmpRawdataAnswersForOption[i][1].ToString();
                    else // QT = 12
                    {
                        // Find first location of $--$
                        int firstIdx = Answer_String.IndexOf("$--$");
                        int secondIdx = (Answer_String).IndexOf("$--$", firstIdx + 4);
                        worksheet.Cells[rownum, colval].Value = Answer_String.Substring(firstIdx + 4, secondIdx - (firstIdx + 4));
                    }

                    rownum = 2;
                }
            }
        }
        else
        {
            DataRow[] drForAnswerId;

            // Other
            if ((QT == "1" || QT == "2" || QT == "3" || QT == "4") && OtherOption == "1")
            {

                for (int i = 0; i < dsgetRawdataAnswersForQues.Tables[1].Rows.Count; i++)
                {
                    // Get Rownum for respondent id
                    respondentId = dsgetRawdataAnswersForQues.Tables[1].Rows[i][1].ToString();
                    strRespIdQuery = "RESPONDENT_ID=" + respondentId;
                    drForRespId = dtRespId.Select(strRespIdQuery);
                    rownum = rownum + Convert.ToInt32(drForRespId[0][7].ToString());
                    worksheet.Cells[rownum, colval].Value = dsgetRawdataAnswersForQues.Tables[1].Rows[i][0].ToString();
                    // reset row num
                    rownum = 2;
                }
            }
            else
            {
                string colName = "ANSWER_ID=" + AnswerId;
                drForAnswerId = dsgetRawdataAnswersForQues.Tables[0].Select(colName);
                for (int i = 0; i < drForAnswerId.Count(); i++)
                {
                    // Get Rownum for respondent id
                    respondentId = drForAnswerId[i][2].ToString();
                    strRespIdQuery = "RESPONDENT_ID=" + respondentId;
                    drForRespId = dtRespId.Select(strRespIdQuery);
                    //if (drForRespId.Length == 0)
                    //    return; // Possibly an optional question for which respondent was found

                    rownum = rownum + Convert.ToInt32(drForRespId[0][7].ToString());

                    if (QT == "2" && OtherOption == "1")
                        worksheet.Cells[rownum, colval].Value = drForAnswerId[i][3];
                    else if (QT == "2" && OtherOption == "0")
                        worksheet.Cells[rownum, colval].Value = "Yes";
                    else if (QT == "1" || QT == "3" || QT == "4")
                        worksheet.Cells[rownum, colval].Value = drForAnswerId[i][3];
                    else
                        worksheet.Cells[rownum, colval].Value = drForAnswerId[i][1];

                    // reset row num
                    rownum = 2;
                }
            }
        }
    }


    #region "Method : SetFeatures"
    /// <summary>
    /// This method returns bool value, if the feature flag is one.
    /// </summary>
    /// <param name="strFeature"></param>
    /// <returns></returns>

    public int SetFeatures(string strFeature)
    {
        int iVal = 0;
        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var sessdet = session.GetLoginUserDetailsSession();
        if (sessdet != null)
        {

            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            string strlicensetype;
            DataSet dssurveytype = surcore.getSurveyType(surveyID);
            if (dssurveytype.Tables[0].Rows[0][1].ToString() != "")
            {
                strlicensetype = dssurveytype.Tables[0].Rows[0][1].ToString();
            }
            else
            {
                strlicensetype = sessdet.LicenseType;
            }
            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(strlicensetype));

            if (sessdet.UserId > 0 && listFeatures != null)
            {
                foreach (var listFeat in listFeatures)
                {

                    if (listFeat.Feature.Contains(strFeature))
                    {
                        iVal = (int)listFeat.Value;
                        break;
                    }
                }
            }
        }

        else
        {
            Response.Redirect(PathHelper.GetUserLoginPageURL(), false);
        }
        return iVal;
    }

    #endregion

    #region "Event : OnSelectedIndexChanged"
    /// <summary>
    /// Radio button changed event show the different types of data and charts.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void OnSelectedIndexChanged(object sender, EventArgs e)
    {
        string temp_id;
        RadioButtonList ddl_sidebysidelist = (RadioButtonList)sender;
        temp_id = ddl_sidebysidelist.ID;
        temp_id = temp_id.Substring(10);

        string Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&QuesID=" + Convert.ToInt32(temp_id) + "&Mode=" + Mode + "&ddlsidetext=" + ddl_sidebysidelist.SelectedIndex + "&surveyFlag=" + SurveyFlag);
        Response.Redirect(Navurlstr);
    }
    #endregion

    #region "Method : CheckUserFeatureLinks"
    /// <summary>
    /// Bind data based on features.
    /// </summary>

    public void CheckUserFeatureLinks()
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();

        if (userDetails != null && userDetails.UserId > 0)
        {
            if (SetFeatures("DATACHART") == 0)
            {
                var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            }
            else
            {
               
            }
            if (SetFeatures("CHART") == 0)
            {
                
            }
            else
            {
               
            }
            if (SetFeatures("CUSTOMCHART") == 0)
            {
               
            }
            else
            {

            }
            if (SetFeatures("CROSS_TAB") == 0)
            {
                CrossTabRpts.HRef = "#";
                CrossTabRpts.Attributes.Add("onclick", "javascript:return CheckPermissions();");            
                imgCrossTabReports.Visible = true;
            }
            else
            {
                string strCrossTab = EncryptHelper.EncryptQuerystring("CrossTabReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + base.SurveyFlag);
                CrossTabRpts.HRef = strCrossTab;
                imgCrossTabReports.Visible = false;
            }
            if (SetFeatures("INDIVIDUAL_RESPONSES") == 0)
            {
                IndividualRes.HRef = "#";
                IndividualRes.Attributes.Add("onclick", "javascript:return CheckPermissions();");
                imgIndividualResponses.Visible = true;
            }
            else
            {
                DataSet dssurcorevoice = surcore.voicedataset();


                DataSet dssurveytype = surcore.getSurveyType(surveyID);

                if (dssurcorevoice.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
                {

                    string strIndividual = EncryptHelper.EncryptQuerystring("IndividualResponseVoice.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + base.SurveyFlag);
                    IndividualRes.HRef = strIndividual;
                    imgIndividualResponses.Visible = false;
                }
                else if((userDetails.LicenseType=="PREMIUM_YEARLY") || (dssurveytype.Tables[0].Rows[0][1].ToString() == "PREMIUM_YEARLY"))
                {

                    string strIndividual = EncryptHelper.EncryptQuerystring("IndividualResponse.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + base.SurveyFlag);
                    IndividualRes.HRef = strIndividual;
                    imgIndividualResponses.Visible = false;
                }
                else
                {

                    string strIndividual = EncryptHelper.EncryptQuerystring("IndividualResponse.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + base.SurveyFlag);
                    IndividualRes.HRef = strIndividual;
                    imgIndividualResponses.Visible = true;
                }
            }

            if (SetFeatures("FILTER") == 0)
            {
                cmdSearch.Attributes.Add("onclick", "javascript:return CheckPermissions();");
            }
            if (SetFeatures("RAWDATA_EXPORT") == 0)
            {
                lbn_rawdataexport.Attributes.Add("onclick", "javascript:return CheckPermissions();");
            }
            if (SetFeatures("EXPORT_PPT") == 0)
            {
                fea_exppt.Value = "0";
            }
            if (SetFeatures("EXPORT_EXCEL") == 0)
            {
                fea_exexc.Value = "0";
            }
            if (SetFeatures("EXPORT_WORD") == 0)
            {
                fea_exwor.Value = "0";
            }
            if (SetFeatures("EXPORT_PDF") == 0)
            {
                fea_expdf.Value = "0";
            }
            if (SetFeatures("DATACHART") == 0)
            {
                fea_datacharts.Value = "0";
            }
            if (SetFeatures("CHART") == 0)
            {
                fea_charts.Value = "0";
            }

        }
    }
    #endregion

    #region "Method : SetSubNaviLinks"
    /// <summary>
    /// Set the navigation links.
    /// </summary>
    public void SetSubNaviLinks()
    {
        string strCrossTab = EncryptHelper.EncryptQuerystring("CrossTabReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + base.SurveyFlag);
        string strCustomTab = EncryptHelper.EncryptQuerystring("CustomizeReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + base.SurveyFlag);

        string strCrossIndvResponse = EncryptHelper.EncryptQuerystring("IndividualResponse.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + base.SurveyFlag);

        CrossTabRpts.HRef = strCrossTab;
        IndividualRes.HRef = strCrossIndvResponse;
       
    }
    #endregion

    #region "Method : FindControlRecursive"
    /// <summary>
    /// Find the control.
    /// </summary>
    /// <param name="Root"></param>
    /// <param name="Id"></param>
    /// <returns></returns>

    public static System.Web.UI.Control FindControlRecursive(System.Web.UI.Control Root, string Id)
    {
        if (Root.ID == Id)
            return Root;
        foreach (System.Web.UI.Control Ctl in Root.Controls)
        {
            System.Web.UI.Control FoundCtl = FindControlRecursive(Ctl, Id);
            if (FoundCtl != null)
                return FoundCtl;
        }
        return null;
    }

    #endregion

    #region "Method : SetQuestionNos"
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sur"></param>

    public void SetQuestionNos(Insighto.Business.Charts.Survey sur)
    {
     
        int i = 0;
        int qnsCount = 1;
        List<SurveyQuestion> QuesList = sur.surveyQues;
        if (QuesList != null && QuesList.Count > 0)
        {
            QuesList.Sort();
            ASPxRoundPanelquestions.Controls.Clear();
            ASPxHyperLink[] quesLink = new ASPxHyperLink[QuesList.Count];
            foreach (SurveyQuestion Ques in QuesList)
            {
                quesLink[i] = new ASPxHyperLink();
                quesLink[i].EncodeHtml = false;
                ASPxLabel lbl = new ASPxLabel();
                lbl.Text = "&nbsp;";
                lbl.EncodeHtml = false;

                if (Ques.QUESTION_TYPE == 17)
                {
                    quesLink[i].Text = "P";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL) + "- No Analysis for this Question Type";
                }
                else if (Ques.QUESTION_TYPE == 18)
                {
                    quesLink[i].Text = "Q";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL) + "- No Analysis for this Question Type";
                }
                else if (Ques.QUESTION_TYPE == 19)
                {
                    quesLink[i].Text = "I";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = "Image" + "- No Analysis for this Question Type";
                }
                else if (Ques.QUESTION_TYPE == 21)
                {
                    quesLink[i].Text = "V";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = "Video" + "- No Analysis for this Question Type";
                }
                else if (Ques.QUESTION_TYPE == 16)
                {
                    quesLink[i].Text = "N";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL) + "- No Analysis for this Question Type";
                }
                else
                {
                    if (i >= 9)
                    {
                        quesLink[i].Text = Convert.ToString(i + 1);
                        Ques.QUESTION_no = Convert.ToInt32(quesLink[i].Text);
                    }
                    else
                    {
                        quesLink[i].Text = "0" + Convert.ToString(i + 1);
                        Ques.QUESTION_no = Convert.ToInt32(quesLink[i].Text);
                    }
                }
                if (Ques.QUESTION_TYPE != 19 && Ques.QUESTION_TYPE != 16 && Ques.QUESTION_TYPE != 17 && Ques.QUESTION_TYPE != 18 && Ques.QUESTION_TYPE != 21)
                    quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL);

                string Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + Ques.SURVEY_ID + "&QuesID=" + Ques.QUESTION_ID + "&Mode=" + Mode + "&surveyFlag=" + SurveyFlag + "&QuesSeq=" + Ques.QUESTION_SEQ);
                quesLink[i].NavigateUrl = Navurlstr;
                quesLink[i].Font.Bold = true;
                quesLink[i].Font.Underline = true;
                ASPxRoundPanelquestions.Controls.Add(quesLink[i]);
                if (qnsCount % 30 != 0)
                {
                    ASPxRoundPanelquestions.Controls.Add(lbl);
                }
                else
                {
                    lbl.Text = "<br/>";
                    ASPxRoundPanelquestions.Controls.Add(lbl);
                }
                qnsCount++;
                if (Ques.QUESTION_TYPE != 16 && Ques.QUESTION_TYPE != 17 && Ques.QUESTION_TYPE != 18 && Ques.QUESTION_TYPE != 19 && Ques.QUESTION_TYPE != 21)
                    i++;
            }
        }
    }
    #endregion

    public void SetQuestionNos1(DataTable dtQues,int surveyid)
    {

        int i = 0;
        int qnsCount = 1;
       // List<SurveyQuestion> QuesList = sur.surveyQues;
        List<object> QuesList = new List<object>();

        foreach (DataRow drq in dtQues.Rows)
        {
            QuesList.Add(drq);
        }
       
        if (QuesList != null && QuesList.Count > 0)
        {
           // QuesList.Sort();
            ASPxRoundPanelquestions.Controls.Clear();
            ASPxHyperLink[] quesLink = new ASPxHyperLink[QuesList.Count];

            //foreach (SurveyQuestion Ques in QuesList)
            //{
            foreach (DataRow Ques in QuesList)
            {
                quesLink[i] = new ASPxHyperLink();
                quesLink[i].EncodeHtml = false;
                ASPxLabel lbl = new ASPxLabel();
                lbl.Text = "&nbsp;";
                lbl.EncodeHtml = false;

                //if (Ques.QUESTION_TYPE == 17)
                if (Ques.ItemArray[3].ToString() == "17")
                {
                    quesLink[i].Text = "P";
                    quesLink[i].Enabled = false;
                  //  quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL) + "- No Analysis for this Question Type";
                    quesLink[i].ToolTip = Utilities.ClearHTML(Ques.ItemArray[2].ToString()) + "- No Analysis for this Question Type";
                }
                //else if (Ques.QUESTION_TYPE == 18)
                else if (Ques.ItemArray[3].ToString() == "18")
                {
                    quesLink[i].Text = "Q";
                    quesLink[i].Enabled = false;
                   // quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL) + "- No Analysis for this Question Type";
                    quesLink[i].ToolTip = Utilities.ClearHTML(Ques.ItemArray[2].ToString()) + "- No Analysis for this Question Type";
                }
               // else if (Ques.QUESTION_TYPE == 19)
                else if (Ques.ItemArray[3].ToString() == "19")
                {
                    quesLink[i].Text = "I";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = "Image" + "- No Analysis for this Question Type";
                }
               // else if (Ques.QUESTION_TYPE == 21)
                else if (Ques.ItemArray[3].ToString() == "21")
                {
                    quesLink[i].Text = "V";
                    quesLink[i].Enabled = false;
                    quesLink[i].ToolTip = "Video" + "- No Analysis for this Question Type";
                }
              //  else if (Ques.QUESTION_TYPE == 16)
                 else if (Ques.ItemArray[3].ToString() == "16")
                
                {
                    quesLink[i].Text = "N";
                    quesLink[i].Enabled = false;
                  //  quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL) + "- No Analysis for this Question Type";
                    quesLink[i].ToolTip = Utilities.ClearHTML(Ques.ItemArray[2].ToString()) + "- No Analysis for this Question Type";
                }
                else
                {
                    if (i >= 9)
                    {
                        quesLink[i].Text = Convert.ToString(i + 1);
                      //  Ques.QUESTION_no = Convert.ToInt32(quesLink[i].Text);
                        Ques.ItemArray[1] = Convert.ToInt32(quesLink[i].Text);
                    }
                    else
                    {
                        quesLink[i].Text = "0" + Convert.ToString(i + 1);
                      //  Ques.QUESTION_no = Convert.ToInt32(quesLink[i].Text);
                        Ques.ItemArray[1] = Convert.ToInt32(quesLink[i].Text);
                    }
                }

                
                //if (Ques.QUESTION_TYPE != 19 && Ques.QUESTION_TYPE != 16 && Ques.QUESTION_TYPE != 17 && Ques.QUESTION_TYPE != 18 && Ques.QUESTION_TYPE != 21)
                if (Ques.ItemArray[3].ToString() != "19" && Ques.ItemArray[3].ToString() != "16" && Ques.ItemArray[3].ToString() != "17" && Ques.ItemArray[3].ToString() != "18" && Ques.ItemArray[3].ToString() != "21")
                 //   quesLink[i].ToolTip = Utilities.ClearHTML(Ques.QUESTION_LABEL);
                    quesLink[i].ToolTip = Utilities.ClearHTML(Ques.ItemArray[2].ToString());

               
                string Navurlstr = "";
                if (Session["searchclick"] == "Clicked")
                {
                    dedstartdate.Date = Convert.ToDateTime(Session["dedstartdate"]);
                    dedEnddate.Date = Convert.ToDateTime(Session["dedEnddate"]);
                    Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + surveyid + "&QuesID=" + Ques.ItemArray[1].ToString() + "&Mode=" + Mode + "&surveyFlag=" + SurveyFlag + "&QuesSeq=" + Ques.ItemArray[0].ToString() + "&Date_Start=" + Convert.ToDateTime(Session["dedstartdate"]) + "&Date_End=" + Convert.ToDateTime(Session["dedEnddate"]));
                }
                else
                {
                     Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + surveyid + "&QuesID=" + Ques.ItemArray[1].ToString() + "&Mode=" + Mode + "&surveyFlag=" + SurveyFlag + "&QuesSeq=" + Ques.ItemArray[0].ToString());
                }
                    
                quesLink[i].NavigateUrl = Navurlstr;
                quesLink[i].Font.Bold = true;
                quesLink[i].Font.Underline = true;
                ASPxRoundPanelquestions.Controls.Add(quesLink[i]);
                if (qnsCount % 30 != 0)
                {
                    ASPxRoundPanelquestions.Controls.Add(lbl);
                }
                else
                {
                    lbl.Text = "<br/>";
                    ASPxRoundPanelquestions.Controls.Add(lbl);
                }
                try
                {
                    CreatChartForQuestion(Ques.ItemArray[1].ToString(), Navurlstr);
                }
                catch (Exception)
                {
                }
                qnsCount++;
               // if (Ques.QUESTION_TYPE != 16 && Ques.QUESTION_TYPE != 17 && Ques.QUESTION_TYPE != 18 && Ques.QUESTION_TYPE != 19 && Ques.QUESTION_TYPE != 21)
                if (Ques.ItemArray[3].ToString() != "16" && Ques.ItemArray[3].ToString() != "17" && Ques.ItemArray[3].ToString() != "18" && Ques.ItemArray[3].ToString() != "19" && Ques.ItemArray[3].ToString() != "21")
                    i++;
            }
        }
    }

    public void CreatChartForQuestion(string questionid, string questionURL) {
        string strRootURL = ConfigurationSettings.AppSettings["RootURL"].ToString();
        HttpWebRequest req;
        HttpWebResponse resp;
        string filename = "pptimg" + questionid + "" + surveyID.ToString() + ".jpg";
        filename = "pptimg" + questionid + ".jpg";
        try
        {
            
            if (!File.Exists(Server.MapPath("//images//ExportImages//") + filename))
            {
                req = (HttpWebRequest)WebRequest.Create(strRootURL + questionURL);
                resp = (HttpWebResponse)req.GetResponse();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void rdgdisplayset1_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (userDetails != null && userDetails.UserId > 0)
        {
            
        }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

              

        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (userDetails != null && userDetails.UserId > 0)
        {
            if (SetFeatures("FILTER") == 0)
            {
                
            }
            else
            {

                SurveyCore cor = new SurveyCore();
                SurveyCoreVoice corvoice = new SurveyCoreVoice();
                if (dedEnddate != null && dedEnddate.Date > Convert.ToDateTime("1/1/1900 12:00:00 AM") && dedstartdate != null && dedstartdate.Date > Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                {
                    dedEnddate.Date = dedEnddate.Date.AddHours(23.999);
                    DateTime start = dt1.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt1;
                    DateTime End = dt2.ToString() == "1/1/0001 12:00:00 AM" ? Convert.ToDateTime("1/1/1800 12:00:00 AM") : dt2;

                    DataSet dsvoice = cor.voicedataset();
                    if (dsvoice.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
                    {
                        SelectedSurvey = corvoice.GetSurveyWithResponsesCount(SurveyBasicInfoView.SURVEY_ID, start, End);
                    }
                    else
                    {
                        SelectedSurvey = cor.GetSurveyWithResponsesCount(SurveyBasicInfoView.SURVEY_ID, start, End);
                    }
                    string temp = "", urlstr = "";
                    if (SurveyBasicInfoView.SURVEY_ID != 0)
                        temp += Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID.ToString();
                    if (temp.Length > 0)
                        temp += "&";
                    if (dedstartdate != null)
                        temp += "Date_Start=" + dedstartdate.Date;
                    if (temp.Length > 0)
                        temp += "&";
                    if (dedEnddate != null)
                        temp += "Date_End=" + dedEnddate.Date;
                    if (temp.Length > 0)
                    {
                        if (Session["viewallclick"] == "viewallclick")
                        {
                            urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", temp + "&surveyFlag=" + SurveyFlag + "&Veiw=All");
                        }
                        else
                        {
                            urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", temp + "&surveyFlag=" + SurveyFlag + "&Veiw=viewonebyone");
                        }
                    }
                    else
                    {
                       // urlstr = "Reports.aspx";
                        if (Session["viewallclick"] == "viewallclick")
                        {
                            urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", "&surveyFlag=" + SurveyFlag + "&Veiw=All");
                        }
                        else
                        {
                            urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", "&surveyFlag=" + SurveyFlag + "&Veiw=viewonebyone");
                        }
                    }

                    Session["dedstartdate"] = dedstartdate.Date;
                    Session["dedEnddate"] = dedEnddate.Date;
                    Session["searchclick"] = "Clicked";

                    Response.Redirect(urlstr);
                }
                else
                    ClientScript.RegisterStartupScript(typeof(System.Web.UI.Page), "SearchonDate", "<script language='javascript'>alert('Please Enter valid Start date and End date for search');</script>");
            }
        }
              //}
    }

    protected void btn_crosstab_Click(object sender, EventArgs e)
    {
    }

    protected void btn_individualrep_Click(object sender, EventArgs e)
    {

        DataSet dssurcorevoice = surcore.voicedataset();
        if (dssurcorevoice.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
        {
            string urlstr = EncryptHelper.EncryptQuerystring("IndividualResponseVoice.aspx", Constants.SURVEYID + "=" + SelectedSurvey.SURVEY_ID + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(urlstr);
        }
        else
        {
            string urlstr = EncryptHelper.EncryptQuerystring("IndividualResponse.aspx", Constants.SURVEYID + "=" + SelectedSurvey.SURVEY_ID + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(urlstr);
        }
    }

    private string EscapeLikeValue(string value)
    {
        StringBuilder sb = new StringBuilder(value.Length);
        for (int i = 0; i < value.Length; i++)
        {
            char c = value[i];
            switch (c)
            {
                case ']':
                case '[':
                case '%':
                case '*':
                    sb.Append("[").Append(c).Append("]");
                    break;
                case '\'':
                    sb.Append("''");
                    break;
                default:
                    sb.Append(c);
                    break;
            }
        }
        return sb.ToString();
    }
    private static Object locker = new Object();

    public static void GenrateSendmailLog(string Content, string path)
    {
        lock (locker)
        {
            StreamWriter sw = null;
            try
            {
                if (File.Exists(path))
                {
                    FileStream file = new FileStream(path, FileMode.Append, FileAccess.Write);
                    sw = new StreamWriter(file);
                }
                else
                {
                    sw = File.CreateText(path);
                }
                sw.WriteLine(DateTime.Now.ToString() + " ***** " + Content.Trim());
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (sw != null) sw.Close();
            }
        }

    }

    public void exportdocx()
    {
        try
        {
            string sourcename = SAVE_PATH + "openxmldocxfile.docx";
            string destinationname = SAVE_PATH + "openxmldocxfile" + SelectedSurvey.SURVEY_ID.ToString() + ".docx";

            if (File.Exists(destinationname))
            {
                File.Delete(destinationname);
            }

            File.Copy(sourcename, destinationname);

            using (WordprocessingDocument wordDocumnet = WordprocessingDocument.Open(destinationname, true))
            {
               
             //   int pcnt = presentationDocument.PresentationPart.SlideParts.Count();

                // Pass the source document and the position and title of the slide to be inserted to the next method.

                int surveyid = Convert.ToInt32(SelectedSurvey.SURVEY_ID.ToString());
                DataSet dsansoptionsall = surcore.getAnsweroptionsQIDySurveOptPPTX(surveyid);


                Task[] tasks = new Task[dsansoptionsall.Tables[1].Rows.Count];

                try
                {
                    for (int iseq = 0; iseq < dsansoptionsall.Tables[1].Rows.Count; iseq++)
                    {

                        if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "1" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "2" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "3" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "4")
                        {
                            string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                            DataRow[] dr1234 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                            CaptureFCImage CFCI = new CaptureFCImage();
                            CFCI.drQT1234 = dr1234;
                            CFCI.QuestionType = seqtype;
                            CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                            CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                            CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();

                            tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt1234Img());

                        }
                        else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "13")
                        {
                            string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                            DataRow[] dr13 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                            CaptureFCImage CFCI = new CaptureFCImage();
                            CFCI.drQT1234 = dr13;
                            CFCI.QuestionType = seqtype;
                            CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                            CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                            CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                            tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt13Img());
                        }
                        else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "10")
                        {
                            string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                            DataRow[] dr10 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                            System.Data.DataTable mtxcolnames10 = createMtxColumnDataTable();

                            System.Data.DataTable mtxrownames10 = createMtxRowDataTable();

                            for (int imtxrow = 0; imtxrow < dr10.Length; imtxrow++)
                            {

                                if (imtxrow == 0)
                                {
                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[0][2].ToString()), mtxrownames10);
                                }
                                else
                                {
                                    DataRow[] dr = mtxrownames10.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr10[imtxrow][2].ToString()) + "'");
                                    if (dr.Length == 0)
                                    {

                                        AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[imtxrow][2].ToString()), mtxrownames10);

                                    }
                                }
                            }

                            for (int imtxcol = 0; imtxcol < dr10.Length; imtxcol++)
                            {

                                if (imtxcol == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[0][3].ToString().TrimStart()), mtxcolnames10);
                                }
                                else
                                {
                                    DataRow[] dr = mtxcolnames10.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr10[imtxcol][3].ToString().TrimStart()) + "'");
                                    if (dr.Length == 0)
                                    {
                                        AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[imtxcol][3].ToString().TrimStart()), mtxcolnames10);
                                    }
                                }
                            }
                            System.Data.DataTable colgroupoptions10 = createDataTable();
                            for (int irg1 = 0; irg1 < mtxrownames10.Rows.Count; irg1++)
                            {

                                DataRow[] dr1011f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames10.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                                for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                                {

                                    AddDatatoTable(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), mtxrownames10.Rows[irg1][0].ToString(), System.Web.HttpUtility.HtmlEncode(dr1011f[cgv][3].ToString()), dr1011f[cgv][6].ToString(), colgroupoptions10);
                                }


                            }


                            CaptureFCImage CFCI = new CaptureFCImage();
                            CFCI.drQT10 = dr10;
                            CFCI.QuestionType = seqtype;
                            CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                            CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                            CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                            CFCI.dtmtxrows10 = mtxrownames10;
                            CFCI.dtmtxcols10 = mtxcolnames10;
                            CFCI.dt10 = colgroupoptions10;
                            tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt10Img());


                        }
                        else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "11")
                        {
                            string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                            DataRow[] dr11 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                            System.Data.DataTable mtxcolnames11 = createMtxColumnDataTable();

                            System.Data.DataTable mtxrownames11 = createMtxRowDataTable();

                            for (int imtxrow = 0; imtxrow < dr11.Length; imtxrow++)
                            {

                                if (imtxrow == 0)
                                {
                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[0][2].ToString()), mtxrownames11);
                                }
                                else
                                {
                                    DataRow[] dr = mtxrownames11.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr11[imtxrow][2].ToString()) + "'");
                                    if (dr.Length == 0)
                                    {

                                        AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[imtxrow][2].ToString()), mtxrownames11);

                                    }
                                }
                            }

                            for (int imtxcol = 0; imtxcol < dr11.Length; imtxcol++)
                            {

                                if (imtxcol == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[0][3].ToString().TrimStart()), mtxcolnames11);
                                }
                                else
                                {
                                    DataRow[] dr = mtxcolnames11.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr11[imtxcol][3].ToString().TrimStart()) + "'");
                                    if (dr.Length == 0)
                                    {
                                        AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[imtxcol][3].ToString().TrimStart()), mtxcolnames11);
                                    }
                                }
                            }
                            System.Data.DataTable colgroupoptions11 = createDataTable();
                            for (int irg1 = 0; irg1 < mtxrownames11.Rows.Count; irg1++)
                            {

                                DataRow[] dr1011f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames11.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                                for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                                {

                                    AddDatatoTable(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), mtxrownames11.Rows[irg1][0].ToString(), System.Web.HttpUtility.HtmlEncode(dr1011f[cgv][3].ToString()), dr1011f[cgv][6].ToString(), colgroupoptions11);
                                }


                            }


                            CaptureFCImage CFCI = new CaptureFCImage();
                            CFCI.drQT11 = dr11;
                            CFCI.QuestionType = seqtype;
                            CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                            CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                            CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                            CFCI.dtmtxrows11 = mtxrownames11;
                            CFCI.dtmtxcols11 = mtxcolnames11;
                            CFCI.dt11 = colgroupoptions11;
                            tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt11Img());



                        }
                        else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "12")
                        {
                            string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                            DataRow[] dr12 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                            System.Data.DataTable mtxrownames12 = createMtxRowDataTable();

                            System.Data.DataTable mtxcolnames12 = createMtxColumnDataTable();

                            System.Data.DataTable mtxsubcolnames12 = createMtxColumnDataTable();

                            for (int imtxrow = 0; imtxrow < dr12.Length; imtxrow++)
                            {

                                if (imtxrow == 0)
                                {
                                    AddMtxrowsDatatoTable(dr12[0][2].ToString(), mtxrownames12);
                                }
                                else
                                {
                                    DataRow[] dr = mtxrownames12.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()) + "'");
                                    if (dr.Length == 0)
                                    {

                                        AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()), mtxrownames12);

                                    }
                                }
                            }

                            for (int imtxcol = 0; imtxcol < dr12.Length; imtxcol++)
                            {
                                if (imtxcol == 0)
                                {
                                    AddMtxcolsDatatoTable(dr12[0][3].ToString(), mtxcolnames12);
                                }
                                else
                                {
                                    DataRow[] dr = mtxcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()) + "'");
                                    if (dr.Length == 0)
                                    {

                                        AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()), mtxcolnames12);

                                    }
                                }
                            }

                            for (int imtxsubcol = 0; imtxsubcol < dr12.Length; imtxsubcol++)
                            {

                                if (imtxsubcol == 0)
                                {
                                    AddMtxcolsDatatoTable(dr12[0][4].ToString(), mtxsubcolnames12);
                                }
                                else
                                {

                                    DataRow[] dr = mtxsubcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()) + "'");
                                    if (dr.Length == 0)
                                    {

                                        AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()), mtxsubcolnames12);

                                    }
                                }
                            }
                            System.Data.DataTable colgroupoptionsQ12 = createDataTableQ12();
                            for (int irg12data = 0; irg12data < mtxrownames12.Rows.Count; irg12data++)
                            {
                                DataRow[] dr12f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames12.Rows[irg12data][0].ToString()).Replace("'", "''") + "' and seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");
                                for (int cgv12 = 0; cgv12 < dr12f.Length; cgv12++)
                                {
                                    AddDatatoTableQ12(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), System.Web.HttpUtility.HtmlEncode(dr12f[cgv12][3].ToString()), System.Web.HttpUtility.HtmlEncode(dr12f[cgv12][4].ToString()), mtxrownames12.Rows[irg12data][0].ToString(), dr12f[cgv12][6].ToString(), colgroupoptionsQ12);
                                }

                            }

                            CaptureFCImage CFCI = new CaptureFCImage();
                            CFCI.drQT12 = dr12;
                            CFCI.QuestionType = seqtype;
                            CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                            CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                            CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                            CFCI.dtmtxrows12 = mtxrownames12;
                            CFCI.dtmtxcols12 = mtxcolnames12;
                            CFCI.dtmtxsubcols12 = mtxsubcolnames12;
                            CFCI.dt12 = colgroupoptionsQ12;
                            tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt12Img());

                        }
                        else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "15")
                        {
                            string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();
                            DataRow[] dr15 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                            System.Data.DataTable mtxcolnames15 = createMtxColumnDataTable();

                            System.Data.DataTable mtxrownames15 = createMtxRowDataTable();

                            for (int imtxrow = 0; imtxrow < dr15.Length; imtxrow++)
                            {

                                if (imtxrow == 0)
                                {
                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][2].ToString()), mtxrownames15);
                                }
                                else
                                {

                                    DataRow[] dr = mtxrownames15.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()) + "'");


                                    if (dr.Length == 0)
                                    {

                                        AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()), mtxrownames15);

                                    }
                                }
                            }


                            for (int imtxcol = 0; imtxcol < dr15.Length; imtxcol++)
                            {

                                if (imtxcol == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][3].ToString().TrimStart()), mtxcolnames15);
                                }
                                else
                                {
                                    DataRow[] dr = mtxcolnames15.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()) + "'");


                                    if (dr.Length == 0)
                                    {

                                        AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()), mtxcolnames15);

                                    }
                                }
                            }
                            DataTable dtg15 = new DataTable();

                            for (int irg = 0; irg < mtxcolnames15.Rows.Count; irg++)
                            {
                                dtg15.Columns.Add(System.Web.HttpUtility.HtmlDecode(mtxcolnames15.Rows[irg][0].ToString()));
                            }
                            System.Data.DataTable colgroupoptions15 = createDataTable();
                            for (int irg1 = 0; irg1 < mtxrownames15.Rows.Count; irg1++)
                            {
                                DataRow[] dr15f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames15.Rows[irg1][0].ToString()).Replace("'", "''") + "'" + "and seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                                for (int cgv = 0; cgv < dr15f.Length; cgv++)
                                {

                                    AddDatatoTable(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), System.Web.HttpUtility.HtmlEncode(mtxrownames15.Rows[irg1][0].ToString()), System.Web.HttpUtility.HtmlEncode(dr15f[cgv][3].ToString()), dr15f[cgv][6].ToString(), colgroupoptions15);

                                }
                            }

                            CaptureFCImage CFCI = new CaptureFCImage();
                            CFCI.drQT15 = dr15;
                            CFCI.QuestionType = seqtype;
                            CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                            CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                            CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                            CFCI.dtmtxrows15 = mtxrownames15;
                            CFCI.dtmtxcols15 = mtxcolnames15;
                            CFCI.dt15 = colgroupoptions15;
                            tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt15Img());

                        }
                    }
                    Task.WaitAll(tasks);

                }
                catch (Exception ex)
                {
                    throw ex;
                }

                DOCXopenXML docxml = new DOCXopenXML();

                DocumentFormat.OpenXml.Wordprocessing.Paragraph firstpageparagraph = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));
                docxml.CreateFirstTable(destinationname, wordDocumnet, SelectedSurvey.SURVEY_NAME);
                wordDocumnet.MainDocumentPart.Document.Body.Append(firstpageparagraph);

               // docxml.CreateTable(destinationname, wordDocumnet);
                for (int iseqppt = 0; iseqppt < dsansoptionsall.Tables[1].Rows.Count; iseqppt++)
                {

                    string seqtype = dsansoptionsall.Tables[1].Rows[iseqppt][3].ToString();
                                    //// wordDocumnet.AddMainDocumentPart();
                    
                    if ((seqtype == "1") || (seqtype == "2") || (seqtype == "3") || (seqtype == "4") || (seqtype == "13"))
                    {

                       
                       
                        DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph2 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));
                          
                        
                        DataRow[] dr1234 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                         docxml.CreateTable(destinationname, wordDocumnet, seqtype, dr1234, dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));
                    
                        wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph2);

                        DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph1 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));
                        

                        docxml.InsertAPicture(wordDocumnet, SAVE_PATH + "pptimg" + dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString() + ".jpg", StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));
                     wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph1);



                    }
                    else if ((seqtype == "10"))
                    {
                        DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph2 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));
                       
                        DataRow[] dr1011 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                        System.Data.DataTable mtxcolnames = createMtxColumnDataTable();

                        System.Data.DataTable mtxrownames = createMtxRowDataTable();

                        for (int imtxrow = 0; imtxrow < dr1011.Length; imtxrow++)
                        {
                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][2].ToString()), mtxrownames);
                            }
                            else
                            {
                                DataRow[] dr = mtxrownames.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()), mtxrownames);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr1011.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][3].ToString().TrimStart()), mtxcolnames);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()), mtxcolnames);
                                }
                            }
                        }

                      
                          docxml.CreateTable(destinationname, wordDocumnet, seqtype, dr1011,  dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), mtxrownames, mtxcolnames, dsansoptionsall.Tables[2], dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString(), StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));

                        mtxrownames.Clear();
                        mtxcolnames.Clear();

                        wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph2);


                        DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph1 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));
                        docxml.InsertAPicture(wordDocumnet, SAVE_PATH + "pptimg" + dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString() + ".jpg", StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));
                        wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph1);
                    }
                    else if ((seqtype == "11"))
                    {
                        DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph2 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));

                        DataRow[] dr1011 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                        System.Data.DataTable mtxcolnames11 = createMtxColumnDataTable();

                        System.Data.DataTable mtxrownames11 = createMtxRowDataTable();

                        for (int imtxrow = 0; imtxrow < dr1011.Length; imtxrow++)
                        {
                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][2].ToString()), mtxrownames11);
                            }
                            else
                            {
                                DataRow[] dr = mtxrownames11.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()), mtxrownames11);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr1011.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][3].ToString().TrimStart()), mtxcolnames11);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames11.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()), mtxcolnames11);
                                }
                            }
                        }


                        docxml.CreateTable(destinationname, wordDocumnet, seqtype, dr1011, dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), mtxrownames11, mtxcolnames11, dsansoptionsall.Tables[2], dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString(), StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));



                        mtxrownames11.Clear();
                        mtxcolnames11.Clear();
                        wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph2);


                        DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph1 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));
                        docxml.InsertAPicture(wordDocumnet, SAVE_PATH + "pptimg" + dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString() + ".jpg", StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));
                        wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph1);
                    }
                    else if (seqtype == "12")
                    {
                        DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph2 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));

                        DataRow[] dr12 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                        System.Data.DataTable mtxrownames12 = createMtxRowDataTable();

                        System.Data.DataTable mtxcolnames12 = createMtxColumnDataTable();

                        System.Data.DataTable mtxsubcolnames12 = createMtxColumnDataTable();

                        for (int imtxrow = 0; imtxrow < dr12.Length; imtxrow++)
                        {

                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(dr12[0][2].ToString(), mtxrownames12);
                            }
                            else
                            {
                                DataRow[] dr = mtxrownames12.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()), mtxrownames12);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr12.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(dr12[0][3].ToString(), mtxcolnames12);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()), mtxcolnames12);

                                }
                            }
                        }

                        for (int imtxsubcol = 0; imtxsubcol < dr12.Length; imtxsubcol++)
                        {

                            if (imtxsubcol == 0)
                            {
                                AddMtxcolsDatatoTable(dr12[0][4].ToString(), mtxsubcolnames12);
                            }
                            else
                            {

                                DataRow[] dr = mtxsubcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()), mtxsubcolnames12);

                                }
                            }
                        }

                        docxml.CreateTable(destinationname, wordDocumnet, seqtype, dr12, dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), mtxrownames12, mtxcolnames12,mtxsubcolnames12, dsansoptionsall.Tables[2], dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString(), StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));
                       
                        mtxcolnames12.Clear();
                        mtxrownames12.Clear();
                        mtxsubcolnames12.Clear();

                        wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph2);

                        DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph1 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));
                        //Run run = new Run();
                        //DocumentFormat.OpenXml.Wordprocessing.Text text = new DocumentFormat.OpenXml.Wordprocessing.Text() { Text = StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()) };
                        //run.Append(text);
                        //PageBreakParagraph1.Append(run);
                        docxml.InsertAPicture(wordDocumnet, SAVE_PATH + "pptimg" + dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString() + ".jpg", StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));
                        wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph1);
                    }
                    else if (seqtype == "15")
                    {
                        DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph2 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));
                        DataRow[] dr15 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                        System.Data.DataTable mtxcolnames15 = createMtxColumnDataTable();

                        System.Data.DataTable mtxrownames15 = createMtxRowDataTable();

                        for (int imtxrow = 0; imtxrow < dr15.Length; imtxrow++)
                        {

                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][2].ToString()), mtxrownames15);
                            }
                            else
                            {

                                DataRow[] dr = mtxrownames15.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()) + "'");


                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()), mtxrownames15);

                                }
                            }
                        }


                        for (int imtxcol = 0; imtxcol < dr15.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][3].ToString().TrimStart()), mtxcolnames15);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames15.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()) + "'");


                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()), mtxcolnames15);

                                }
                            }
                        }


                        docxml.CreateTable15(destinationname, wordDocumnet, seqtype, dr15, dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), mtxrownames15, mtxcolnames15, dsansoptionsall.Tables[2], dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString(), StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));

                        mtxcolnames15.Clear();
                        mtxrownames15.Clear();

                        wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph2);

                        DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph1 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));

                        docxml.InsertAPicture(wordDocumnet, SAVE_PATH + "pptimg" + dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString() + ".jpg", StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));
                        wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph1);
                    }
                   
           
                    wordDocumnet.MainDocumentPart.Document.Save();

            
                }
            }
            string name_survey = SelectedSurvey.SURVEY_NAME.Replace(" ", "");
            name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
            name_survey = name_survey.Replace("/", "");

            Response.ContentType = "application/vnd.openxmlformats-office";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + ".docx");
            Response.TransmitFile(destinationname);
            Response.End();
 
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void exportpdf()
    {
        
        int surveyid = Convert.ToInt32(SelectedSurvey.SURVEY_ID.ToString());
        DataSet dsansoptionsall = surcore.getAnsweroptionsQIDySurveOptPPTX(surveyid);


        Task[] tasks = new Task[dsansoptionsall.Tables[1].Rows.Count];

        try
        {
            for (int iseq = 0; iseq < dsansoptionsall.Tables[1].Rows.Count; iseq++)
            {

                if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "1" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "2" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "3" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "4")
                {
                    string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                    DataRow[] dr1234 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                    CaptureFCImage CFCI = new CaptureFCImage();
                    CFCI.drQT1234 = dr1234;
                    CFCI.QuestionType = seqtype;
                    CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                    CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                    CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();

                    tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt1234Img());

                }
                else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "13")
                {
                    string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                    DataRow[] dr13 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                    CaptureFCImage CFCI = new CaptureFCImage();
                    CFCI.drQT1234 = dr13;
                    CFCI.QuestionType = seqtype;
                    CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                    CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                    CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                    tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt13Img());
                }
                else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "10")
                {
                    string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                    DataRow[] dr10 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                    System.Data.DataTable mtxcolnames10 = createMtxColumnDataTable();

                    System.Data.DataTable mtxrownames10 = createMtxRowDataTable();

                    for (int imtxrow = 0; imtxrow < dr10.Length; imtxrow++)
                    {

                        if (imtxrow == 0)
                        {
                            AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[0][2].ToString()), mtxrownames10);
                        }
                        else
                        {
                            DataRow[] dr = mtxrownames10.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr10[imtxrow][2].ToString()) + "'");
                            if (dr.Length == 0)
                            {

                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[imtxrow][2].ToString()), mtxrownames10);

                            }
                        }
                    }

                    for (int imtxcol = 0; imtxcol < dr10.Length; imtxcol++)
                    {

                        if (imtxcol == 0)
                        {
                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[0][3].ToString().TrimStart()), mtxcolnames10);
                        }
                        else
                        {
                            DataRow[] dr = mtxcolnames10.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr10[imtxcol][3].ToString().TrimStart()) + "'");
                            if (dr.Length == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[imtxcol][3].ToString().TrimStart()), mtxcolnames10);
                            }
                        }
                    }
                    System.Data.DataTable colgroupoptions10 = createDataTable();
                    for (int irg1 = 0; irg1 < mtxrownames10.Rows.Count; irg1++)
                    {

                        DataRow[] dr1011f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames10.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                        for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                        {

                            AddDatatoTable(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), mtxrownames10.Rows[irg1][0].ToString(), System.Web.HttpUtility.HtmlEncode(dr1011f[cgv][3].ToString()), dr1011f[cgv][6].ToString(), colgroupoptions10);
                        }


                    }


                    CaptureFCImage CFCI = new CaptureFCImage();
                    CFCI.drQT10 = dr10;
                    CFCI.QuestionType = seqtype;
                    CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                    CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                    CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                    CFCI.dtmtxrows10 = mtxrownames10;
                    CFCI.dtmtxcols10 = mtxcolnames10;
                    CFCI.dt10 = colgroupoptions10;
                    tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt10Img());


                }
                else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "11")
                {
                    string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                    DataRow[] dr11 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                    System.Data.DataTable mtxcolnames11 = createMtxColumnDataTable();

                    System.Data.DataTable mtxrownames11 = createMtxRowDataTable();

                    for (int imtxrow = 0; imtxrow < dr11.Length; imtxrow++)
                    {

                        if (imtxrow == 0)
                        {
                            AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[0][2].ToString()), mtxrownames11);
                        }
                        else
                        {
                            DataRow[] dr = mtxrownames11.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr11[imtxrow][2].ToString()) + "'");
                            if (dr.Length == 0)
                            {

                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[imtxrow][2].ToString()), mtxrownames11);

                            }
                        }
                    }

                    for (int imtxcol = 0; imtxcol < dr11.Length; imtxcol++)
                    {

                        if (imtxcol == 0)
                        {
                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[0][3].ToString().TrimStart()), mtxcolnames11);
                        }
                        else
                        {
                            DataRow[] dr = mtxcolnames11.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr11[imtxcol][3].ToString().TrimStart()) + "'");
                            if (dr.Length == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[imtxcol][3].ToString().TrimStart()), mtxcolnames11);
                            }
                        }
                    }
                    System.Data.DataTable colgroupoptions11 = createDataTable();
                    for (int irg1 = 0; irg1 < mtxrownames11.Rows.Count; irg1++)
                    {

                        DataRow[] dr1011f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames11.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                        for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                        {

                            AddDatatoTable(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), mtxrownames11.Rows[irg1][0].ToString(), System.Web.HttpUtility.HtmlEncode(dr1011f[cgv][3].ToString()), dr1011f[cgv][6].ToString(), colgroupoptions11);
                        }


                    }


                    CaptureFCImage CFCI = new CaptureFCImage();
                    CFCI.drQT11 = dr11;
                    CFCI.QuestionType = seqtype;
                    CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                    CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                    CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                    CFCI.dtmtxrows11 = mtxrownames11;
                    CFCI.dtmtxcols11 = mtxcolnames11;
                    CFCI.dt11 = colgroupoptions11;
                    tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt11Img());



                }
                else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "12")
                {
                    string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                    DataRow[] dr12 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                    System.Data.DataTable mtxrownames12 = createMtxRowDataTable();

                    System.Data.DataTable mtxcolnames12 = createMtxColumnDataTable();

                    System.Data.DataTable mtxsubcolnames12 = createMtxColumnDataTable();

                    for (int imtxrow = 0; imtxrow < dr12.Length; imtxrow++)
                    {

                        if (imtxrow == 0)
                        {
                            AddMtxrowsDatatoTable(dr12[0][2].ToString(), mtxrownames12);
                        }
                        else
                        {
                            DataRow[] dr = mtxrownames12.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()) + "'");
                            if (dr.Length == 0)
                            {

                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()), mtxrownames12);

                            }
                        }
                    }

                    for (int imtxcol = 0; imtxcol < dr12.Length; imtxcol++)
                    {
                        if (imtxcol == 0)
                        {
                            AddMtxcolsDatatoTable(dr12[0][3].ToString(), mtxcolnames12);
                        }
                        else
                        {
                            DataRow[] dr = mtxcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()) + "'");
                            if (dr.Length == 0)
                            {

                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()), mtxcolnames12);

                            }
                        }
                    }

                    for (int imtxsubcol = 0; imtxsubcol < dr12.Length; imtxsubcol++)
                    {

                        if (imtxsubcol == 0)
                        {
                            AddMtxcolsDatatoTable(dr12[0][4].ToString(), mtxsubcolnames12);
                        }
                        else
                        {

                            DataRow[] dr = mtxsubcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()) + "'");
                            if (dr.Length == 0)
                            {

                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()), mtxsubcolnames12);

                            }
                        }
                    }
                    System.Data.DataTable colgroupoptionsQ12 = createDataTableQ12();
                    for (int irg12data = 0; irg12data < mtxrownames12.Rows.Count; irg12data++)
                    {
                        DataRow[] dr12f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames12.Rows[irg12data][0].ToString()).Replace("'", "''") + "' and seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");
                        for (int cgv12 = 0; cgv12 < dr12f.Length; cgv12++)
                        {
                            AddDatatoTableQ12(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), System.Web.HttpUtility.HtmlEncode(dr12f[cgv12][3].ToString()), System.Web.HttpUtility.HtmlEncode(dr12f[cgv12][4].ToString()), mtxrownames12.Rows[irg12data][0].ToString(), dr12f[cgv12][6].ToString(), colgroupoptionsQ12);
                        }

                    }

                    CaptureFCImage CFCI = new CaptureFCImage();
                    CFCI.drQT12 = dr12;
                    CFCI.QuestionType = seqtype;
                    CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                    CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                    CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                    CFCI.dtmtxrows12 = mtxrownames12;
                    CFCI.dtmtxcols12 = mtxcolnames12;
                    CFCI.dtmtxsubcols12 = mtxsubcolnames12;
                    CFCI.dt12 = colgroupoptionsQ12;
                    tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt12Img());

                }
                else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "15")
                {
                    string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();
                    DataRow[] dr15 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                    System.Data.DataTable mtxcolnames15 = createMtxColumnDataTable();

                    System.Data.DataTable mtxrownames15 = createMtxRowDataTable();

                    for (int imtxrow = 0; imtxrow < dr15.Length; imtxrow++)
                    {

                        if (imtxrow == 0)
                        {
                            AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][2].ToString()), mtxrownames15);
                        }
                        else
                        {

                            DataRow[] dr = mtxrownames15.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()) + "'");


                            if (dr.Length == 0)
                            {

                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()), mtxrownames15);

                            }
                        }
                    }


                    for (int imtxcol = 0; imtxcol < dr15.Length; imtxcol++)
                    {

                        if (imtxcol == 0)
                        {
                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][3].ToString().TrimStart()), mtxcolnames15);
                        }
                        else
                        {
                            DataRow[] dr = mtxcolnames15.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()) + "'");


                            if (dr.Length == 0)
                            {

                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()), mtxcolnames15);

                            }
                        }
                    }
                    DataTable dtg15 = new DataTable();

                    for (int irg = 0; irg < mtxcolnames15.Rows.Count; irg++)
                    {
                        dtg15.Columns.Add(System.Web.HttpUtility.HtmlDecode(mtxcolnames15.Rows[irg][0].ToString()));
                    }
                    System.Data.DataTable colgroupoptions15 = createDataTable();
                    for (int irg1 = 0; irg1 < mtxrownames15.Rows.Count; irg1++)
                    {
                        DataRow[] dr15f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames15.Rows[irg1][0].ToString()).Replace("'", "''") + "'" + "and seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                        for (int cgv = 0; cgv < dr15f.Length; cgv++)
                        {

                            AddDatatoTable(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), System.Web.HttpUtility.HtmlEncode(mtxrownames15.Rows[irg1][0].ToString()), System.Web.HttpUtility.HtmlEncode(dr15f[cgv][3].ToString()), dr15f[cgv][6].ToString(), colgroupoptions15);

                        }
                    }

                    CaptureFCImage CFCI = new CaptureFCImage();
                    CFCI.drQT15 = dr15;
                    CFCI.QuestionType = seqtype;
                    CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                    CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                    CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                    CFCI.dtmtxrows15 = mtxrownames15;
                    CFCI.dtmtxcols15 = mtxcolnames15;
                    CFCI.dt15 = colgroupoptions15;
                    tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt15Img());

                }
            }
            Task.WaitAll(tasks);

        }
        catch (Exception ex)
        {
            throw ex;
        }

        sharp.Document document = new sharp.Document();
        string name_survey = SurveyBasicInfoView.SURVEY_NAME.Replace(" ", "");
        name_survey = name_survey.Replace("/", "");
        name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");

        string strDirector = ConfigurationManager.AppSettings["RootPath"];
        if (!Directory.Exists(strDirector + SurveyBasicInfoView.USERID + @"\" + SurveyBasicInfoView.SURVEY_ID))
        {
            Directory.CreateDirectory(strDirector + SurveyBasicInfoView.USERID + @"\" + SurveyBasicInfoView.SURVEY_ID);
        }
        string path = strDirector + SurveyBasicInfoView.USERID + "\\" + SurveyBasicInfoView.SURVEY_ID + "\\" + name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".pdf";


        PdfWriter.GetInstance(document, new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite));
        document.Open();     
      
        sharp.Table tablesrvyname = new iTextSharp.text.Table(1, 1);       
        tablesrvyname.Padding = 4;
        tablesrvyname.Width = 100;
        iTextSharp.text.Cell cellsrvyname = new iTextSharp.text.Cell(new Phrase("Survey Title: " + SurveyBasicInfoView.SURVEY_NAME, FontFactory.GetFont(FontFactory.HELVETICA, 20)));
        cellsrvyname.HorizontalAlignment = sharp.Element.ALIGN_CENTER;
        cellsrvyname.VerticalAlignment = sharp.Element.ALIGN_CENTER;      
        tablesrvyname.AddCell(cellsrvyname);
        tablesrvyname.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;       
        tablesrvyname.BorderWidth = 1;
        document.Add(tablesrvyname);
        document.NewPage();

        for (int iseqppt = 0; iseqppt < dsansoptionsall.Tables[1].Rows.Count; iseqppt++)
        {
            string seqtype = dsansoptionsall.Tables[1].Rows[iseqppt][3].ToString();
        
            if ((seqtype == "1") || (seqtype == "2") || (seqtype == "3") || (seqtype == "4") || (seqtype == "13"))
            {
                
                sharp.Table tablequeslbl = new iTextSharp.text.Table(1, 1);
                tablequeslbl.Padding = 4;
                tablequeslbl.Width = 100;
                iTextSharp.text.Cell cellqueslbl = new iTextSharp.text.Cell(new Phrase(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD)));
                cellqueslbl.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cellqueslbl.VerticalAlignment = sharp.Element.ALIGN_CENTER;               
                tablequeslbl.AddCell(cellqueslbl);
                tablequeslbl.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;
                tablequeslbl.BorderWidth = 1;
                document.Add(tablequeslbl);

                System.Web.UI.WebControls.Label quesLabel1234 = new System.Web.UI.WebControls.Label();
                quesLabel1234.Text = "";
                iTextSharp.text.Font font22B = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD);
                document.Add(new sharp.Paragraph(quesLabel1234.Text, font22B));
                
                
                DataRow[] dr1234 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");
                sharp.Table table = new iTextSharp.text.Table(3, dr1234.Length+2);              
                table.Padding = 4;
                table.Width = 100;                
                iTextSharp.text.Cell cell = new iTextSharp.text.Cell(new Phrase("Answer options", FontFactory.GetFont(FontFactory.HELVETICA, 10,1,new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                cell.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cell.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD")); 
                table.AddCell(cell);
                iTextSharp.text.Cell cell1 = new iTextSharp.text.Cell(new Phrase("In%", FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                cell1.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cell1.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                cell1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD")); 
                table.AddCell(cell1);
                iTextSharp.text.Cell cell2 = new iTextSharp.text.Cell(new Phrase("In nos.", FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                cell2.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cell2.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                cell2.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD")); 
                table.AddCell(cell2);


                for (int irg1 = 0; irg1 < dr1234.Length; irg1++)
                {
                    iTextSharp.text.Cell cellAns = new iTextSharp.text.Cell(new Phrase(dr1234[irg1][2].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                    cellAns.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cellAns.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                    if (irg1 == 1)
                    {
                        cellAns.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                    }
                    else
                    {
                        if ((irg1 % 2) == 1)
                        {
                            cellAns.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            cellAns.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                        }
                    }
                    table.AddCell(cellAns);
                    iTextSharp.text.Cell cellAns1 = new iTextSharp.text.Cell(new Phrase(dr1234[irg1][6].ToString() + "%", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                    cellAns1.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cellAns1.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                    if (irg1 == 1)
                    {
                        cellAns1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                    }
                    else
                    {
                        if ((irg1 % 2) == 1)
                        {
                            cellAns1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            cellAns1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                        }
                    }
                    table.AddCell(cellAns1);
                    iTextSharp.text.Cell cellAns2 = new iTextSharp.text.Cell(new Phrase(dr1234[irg1][5].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                    cellAns2.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cellAns2.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                    if (irg1 == 1)
                    {
                        cellAns2.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                    }
                    else
                    {
                        if ((irg1 % 2) == 1)
                        {
                            cellAns2.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            cellAns2.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                        }
                    }
                    table.AddCell(cellAns2);
                }
                
                iTextSharp.text.Cell rcell = new iTextSharp.text.Cell(new Phrase("# of Respondents", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                rcell.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                rcell.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                table.AddCell(rcell);
                iTextSharp.text.Cell rcell1 = new iTextSharp.text.Cell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                rcell1.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                rcell1.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                table.AddCell(rcell1);
                iTextSharp.text.Cell rcell2 = new iTextSharp.text.Cell(new Phrase(dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                rcell2.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                rcell2.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                table.AddCell(rcell2);
                document.Add(table);
                document.NewPage();
                               
                sharp.Table tablequeslblimg = new iTextSharp.text.Table(1, 1);
                tablequeslblimg.Padding = 4;
                tablequeslblimg.Width = 83;
                iTextSharp.text.Cell cellqueslblimg = new iTextSharp.text.Cell(new Phrase(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD)));
                cellqueslblimg.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cellqueslblimg.VerticalAlignment = sharp.Element.ALIGN_CENTER;
                tablequeslblimg.AddCell(cellqueslblimg);
                tablequeslblimg.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;
                tablequeslblimg.BorderWidth = 1;
                document.Add(tablequeslblimg);
                string PicPath = SAVE_PATH + "pptimg" + dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString() + ".jpg";

                document.Add(new sharp.Paragraph());
                sharp.Image img = sharp.Image.GetInstance(PicPath);
                img.Alignment = sharp.Element.ALIGN_CENTER;
                img.ScalePercent(51);
                img.Border = sharp.Image.LEFT_BORDER | sharp.Image.RIGHT_BORDER | sharp.Image.TOP_BORDER | sharp.Image.BOTTOM_BORDER;
                img.BorderWidth = 1;               
                document.Add(img);
                document.NewPage();
            }
            else if ((seqtype == "10") || (seqtype == "11"))
            {
                DataRow[] dr1011 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                System.Data.DataTable mtxcolnames = createMtxColumnDataTable();

                System.Data.DataTable mtxrownames = createMtxRowDataTable();

                for (int imtxrow = 0; imtxrow < dr1011.Length; imtxrow++)
                {
                    if (imtxrow == 0)
                    {
                        AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][2].ToString()), mtxrownames);
                    }
                    else
                    {
                        DataRow[] dr = mtxrownames.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()) + "'");
                        if (dr.Length == 0)
                        {

                            AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()), mtxrownames);

                        }
                    }
                }

                for (int imtxcol = 0; imtxcol < dr1011.Length; imtxcol++)
                {

                    if (imtxcol == 0)
                    {
                        AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][3].ToString().TrimStart()), mtxcolnames);
                    }
                    else
                    {
                        DataRow[] dr = mtxcolnames.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()) + "'");
                        if (dr.Length == 0)
                        {
                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()), mtxcolnames);
                        }
                    }
                }

                
                sharp.Table tablequeslbl = new iTextSharp.text.Table(1, 1);
                tablequeslbl.Padding = 4;
                tablequeslbl.Width = 100;
                iTextSharp.text.Cell cellqueslbl = new iTextSharp.text.Cell(new Phrase(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD)));
                cellqueslbl.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cellqueslbl.VerticalAlignment = sharp.Element.ALIGN_CENTER;
                tablequeslbl.AddCell(cellqueslbl);
                tablequeslbl.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;
                tablequeslbl.BorderWidth = 1;
                document.Add(tablequeslbl);


                System.Web.UI.WebControls.Label quesLabel1011 = new System.Web.UI.WebControls.Label();
                quesLabel1011.Text = "";
                iTextSharp.text.Font font22B = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD);
                document.Add(new sharp.Paragraph(quesLabel1011.Text, font22B));



                sharp.Table table = new iTextSharp.text.Table(mtxcolnames.Rows.Count + 1, mtxrownames.Rows.Count * 2 + 2);        
                table.Padding = 4;
                table.Width = 100;

                iTextSharp.text.Cell cell10 = new iTextSharp.text.Cell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                cell10.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cell10.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                cell10.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
                table.AddCell(cell10);

                for (int irg = 0; irg < mtxcolnames.Rows.Count; irg++)                
                {
                    
                    iTextSharp.text.Cell cellcol = new iTextSharp.text.Cell(new Phrase(System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[irg][0].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                    cellcol.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cellcol.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                    cellcol.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
                    table.AddCell(cellcol);
                }
          
                for (int irg1 = 0; irg1 < mtxrownames.Rows.Count; irg1++)
                {

                    DataRow[] dr1011f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");


                    iTextSharp.text.Cell cellrow = new iTextSharp.text.Cell(new Phrase(System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                    cellrow.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cellrow.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                    if (irg1 == 1)
                    {
                        cellrow.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                    }
                    else
                    {
                        if ((irg1 % 2) == 1)
                        {
                            cellrow.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            cellrow.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                        }
                    }
                  
                    table.AddCell(cellrow);

                    
                    for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                    {
                        iTextSharp.text.Cell cellrow1 = new iTextSharp.text.Cell(new Phrase(dr1011f[cgv][5].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                        cellrow1.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                        cellrow1.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                       
                        if (irg1 == 1)
                        {
                            cellrow1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            if ((irg1 % 2) == 1)
                            {
                                cellrow1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                            }
                            else
                            {
                                cellrow1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                            }
                        }
                        table.AddCell(cellrow1);
                    }

                    iTextSharp.text.Cell cell10b = new iTextSharp.text.Cell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                    cell10b.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cell10b.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                    if (irg1 == 1)
                    {
                        cell10b.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                    }
                    else
                    {
                        if ((irg1 % 2) == 1)
                        {
                            cell10b.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            cell10b.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                        }
                    }
                    table.AddCell(cell10b);

                    for (int cgv1 = 0; cgv1 < dr1011f.Length; cgv1++)
                    {
                        iTextSharp.text.Cell cellrowb1 = new iTextSharp.text.Cell(new Phrase(dr1011f[cgv1][6].ToString() + "%", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                        cellrowb1.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                        cellrowb1.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                       
                        if (irg1 == 1)
                        {
                            cellrowb1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            if ((irg1 % 2) == 1)
                            {
                                cellrowb1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                            }
                            else
                            {
                                cellrowb1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                            }
                        }
                        table.AddCell(cellrowb1);
                    }
                   
                }

                iTextSharp.text.Cell cell10resp = new iTextSharp.text.Cell(new Phrase("# of Respondents", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                cell10resp.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cell10resp.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
               
                table.AddCell(cell10resp);

                for (int irg = 0; irg < mtxcolnames.Rows.Count-1; irg++)
                {

                    iTextSharp.text.Cell cellcolempy = new iTextSharp.text.Cell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                    cellcolempy.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cellcolempy.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
               
                    table.AddCell(cellcolempy);
                }

                iTextSharp.text.Cell cellnoresp = new iTextSharp.text.Cell(new Phrase(dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                cellnoresp.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cellnoresp.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
               
                table.AddCell(cellnoresp);
                document.Add(table);

                mtxrownames.Clear();
                mtxcolnames.Clear();

                document.NewPage();

                sharp.Table tablequeslblimg = new iTextSharp.text.Table(1, 1);
                tablequeslblimg.Padding = 4;
                tablequeslblimg.Width = 83;
                iTextSharp.text.Cell cellqueslblimg = new iTextSharp.text.Cell(new Phrase(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD)));
                cellqueslblimg.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cellqueslblimg.VerticalAlignment = sharp.Element.ALIGN_CENTER;
                tablequeslblimg.AddCell(cellqueslblimg);
                tablequeslblimg.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;
                tablequeslblimg.BorderWidth = 1;
                document.Add(tablequeslblimg);

                string PicPath = SAVE_PATH + "pptimg" + dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString() + ".jpg";

                document.Add(new sharp.Paragraph());
                sharp.Image img = sharp.Image.GetInstance(PicPath);
                img.Alignment = sharp.Element.ALIGN_CENTER;
                img.ScalePercent(51);
                img.Border = sharp.Image.LEFT_BORDER | sharp.Image.RIGHT_BORDER | sharp.Image.TOP_BORDER | sharp.Image.BOTTOM_BORDER;
                img.BorderWidth = 1;               
                document.Add(img);
                document.NewPage();
            }
            else if (seqtype == "12")
            {
                DataRow[] dr12 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                System.Data.DataTable mtxrownames12 = createMtxRowDataTable();

                System.Data.DataTable mtxcolnames12 = createMtxColumnDataTable();

                System.Data.DataTable mtxsubcolnames12 = createMtxColumnDataTable();

                for (int imtxrow = 0; imtxrow < dr12.Length; imtxrow++)
                {

                    if (imtxrow == 0)
                    {
                        AddMtxrowsDatatoTable(dr12[0][2].ToString(), mtxrownames12);
                    }
                    else
                    {
                        DataRow[] dr = mtxrownames12.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()) + "'");
                        if (dr.Length == 0)
                        {

                            AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()), mtxrownames12);

                        }
                    }
                }

                for (int imtxcol = 0; imtxcol < dr12.Length; imtxcol++)
                {

                    if (imtxcol == 0)
                    {
                        AddMtxcolsDatatoTable(dr12[0][3].ToString(), mtxcolnames12);
                    }
                    else
                    {
                        DataRow[] dr = mtxcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()) + "'");
                        if (dr.Length == 0)
                        {

                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()), mtxcolnames12);

                        }
                    }
                }

                for (int imtxsubcol = 0; imtxsubcol < dr12.Length; imtxsubcol++)
                {

                    if (imtxsubcol == 0)
                    {
                        AddMtxcolsDatatoTable(dr12[0][4].ToString(), mtxsubcolnames12);
                    }
                    else
                    {

                        DataRow[] dr = mtxsubcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()) + "'");
                        if (dr.Length == 0)
                        {

                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()), mtxsubcolnames12);

                        }
                    }
                }
               
                sharp.Table tablequeslbl = new iTextSharp.text.Table(1, 1);
                tablequeslbl.Padding = 4;
                tablequeslbl.Width = 100;
                iTextSharp.text.Cell cellqueslbl = new iTextSharp.text.Cell(new Phrase(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD)));
                cellqueslbl.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cellqueslbl.VerticalAlignment = sharp.Element.ALIGN_CENTER;
                tablequeslbl.AddCell(cellqueslbl);
                tablequeslbl.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;
                tablequeslbl.BorderWidth = 1;
                document.Add(tablequeslbl);

                System.Web.UI.WebControls.Label quesLabel12 = new System.Web.UI.WebControls.Label();
                quesLabel12.Text = "";
                iTextSharp.text.Font font22B = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD);
                document.Add(new sharp.Paragraph(quesLabel12.Text, font22B));



                sharp.Table table = new iTextSharp.text.Table(mtxcolnames12.Rows.Count * mtxsubcolnames12.Rows.Count + 1, mtxrownames12.Rows.Count * 2 + 2);
                
                table.Padding = 4;
                table.Width = 100;

                iTextSharp.text.Cell cell12 = new iTextSharp.text.Cell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                cell12.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cell12.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                cell12.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
                table.AddCell(cell12);

                int cval = 0;
                for (int irg12 = 0; irg12 < mtxcolnames12.Rows.Count * mtxsubcolnames12.Rows.Count; irg12++)
                {
                    if ((irg12 == 0) || (irg12 == mtxsubcolnames12.Rows.Count))
                    {
                       
                        iTextSharp.text.Cell cellcol12 = new iTextSharp.text.Cell(new Phrase(System.Web.HttpUtility.HtmlDecode(mtxcolnames12.Rows[cval][0].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                        cellcol12.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                        cellcol12.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                        cellcol12.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
                        table.AddCell(cellcol12);

                        cval = cval + 1;
                    }
                    else
                    {
                        iTextSharp.text.Cell cellcol12 = new iTextSharp.text.Cell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                        cellcol12.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                        cellcol12.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                        cellcol12.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
                        table.AddCell(cellcol12);
                    }
                }


                iTextSharp.text.Cell cell12b = new iTextSharp.text.Cell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                cell12b.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cell12b.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                cell12b.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
                table.AddCell(cell12b);

                int cvalsubcol = 0;
                for (int irg112 = 0; irg112 < mtxcolnames12.Rows.Count * mtxsubcolnames12.Rows.Count; irg112++)
                {
                    if ((irg112 == 0) || (irg112 == mtxsubcolnames12.Rows.Count))
                    {
                        cvalsubcol = 0;
                    }                     
                        iTextSharp.text.Cell cell12bsubcol = new iTextSharp.text.Cell(new Phrase(System.Web.HttpUtility.HtmlDecode(mtxsubcolnames12.Rows[cvalsubcol][0].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                        cell12bsubcol.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                        cell12bsubcol.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                        cell12bsubcol.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
                        table.AddCell(cell12bsubcol);
                        cvalsubcol = cvalsubcol + 1;
                }


                for (int irg12data = 0; irg12data < mtxrownames12.Rows.Count; irg12data++)
                {

                    DataRow[] dr12f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames12.Rows[irg12data][0].ToString()).Replace("'", "''") + "' and seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");


                    iTextSharp.text.Cell cellrow12 = new iTextSharp.text.Cell(new Phrase(System.Web.HttpUtility.HtmlDecode(mtxrownames12.Rows[irg12data][0].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                    cellrow12.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cellrow12.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                    if (irg12data == 1)
                    {
                        cellrow12.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                    }
                    else
                    {
                        if ((irg12data % 2) == 1)
                        {
                            cellrow12.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            cellrow12.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                        }
                    }
                    
                    table.AddCell(cellrow12);


                    for (int cgv12 = 0; cgv12 < dr12f.Length; cgv12++)
                    {
                        iTextSharp.text.Cell cellrow112 = new iTextSharp.text.Cell(new Phrase(dr12f[cgv12][5].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                        cellrow112.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                        cellrow112.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                        
                        if (irg12data == 1)
                        {
                            cellrow112.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            if ((irg12data % 2) == 1)
                            {
                                cellrow112.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                            }
                            else
                            {
                                cellrow112.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                            }
                        }
                        table.AddCell(cellrow112);
                    }

                    iTextSharp.text.Cell cell12blw = new iTextSharp.text.Cell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                    cell12blw.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cell12blw.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                    if (irg12data == 1)
                    {
                        cell12blw.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                    }
                    else
                    {
                        if ((irg12data % 2) == 1)
                        {
                            cell12blw.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            cell12blw.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                        }
                    }
                    table.AddCell(cell12blw);

                    for (int cgv12 = 0; cgv12 < dr12f.Length; cgv12++)
                    {
                        iTextSharp.text.Cell cellrowblw1 = new iTextSharp.text.Cell(new Phrase(dr12f[cgv12][6].ToString() + "%", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                        cellrowblw1.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                        cellrowblw1.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                        
                        if (irg12data == 1)
                        {
                            cellrowblw1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            if ((irg12data % 2) == 1)
                            {
                                cellrowblw1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                            }
                            else
                            {
                                cellrowblw1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                            }
                        }
                        table.AddCell(cellrowblw1);
                    }

                }
                iTextSharp.text.Cell cell12resp = new iTextSharp.text.Cell(new Phrase("# of Respondents", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                cell12resp.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cell12resp.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                
                table.AddCell(cell12resp);

                for (int irg = 0; irg < mtxcolnames12.Rows.Count * mtxsubcolnames12.Rows.Count - 1; irg++)
                {

                    iTextSharp.text.Cell cellcolempy12 = new iTextSharp.text.Cell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                    cellcolempy12.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cellcolempy12.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                
                    table.AddCell(cellcolempy12);
                }

                iTextSharp.text.Cell cellnoresp12 = new iTextSharp.text.Cell(new Phrase(dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                cellnoresp12.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cellnoresp12.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                
                table.AddCell(cellnoresp12);
                
                document.Add(table);

                mtxrownames12.Clear();
                mtxcolnames12.Clear();
                mtxsubcolnames12.Clear();

                document.NewPage();

                
                sharp.Table tablequeslblimg = new iTextSharp.text.Table(1, 1);
                tablequeslblimg.Padding = 4;
                tablequeslblimg.Width = 83;
                iTextSharp.text.Cell cellqueslblimg = new iTextSharp.text.Cell(new Phrase(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD)));
                cellqueslblimg.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cellqueslblimg.VerticalAlignment = sharp.Element.ALIGN_CENTER;
                tablequeslblimg.AddCell(cellqueslblimg);
                tablequeslblimg.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;
                tablequeslblimg.BorderWidth = 1;
                document.Add(tablequeslblimg);

                string PicPath = SAVE_PATH + "pptimg" + dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString() + ".jpg";

                document.Add(new sharp.Paragraph());
                sharp.Image img = sharp.Image.GetInstance(PicPath);
                img.Alignment = sharp.Element.ALIGN_CENTER;
                img.ScalePercent(51);
                img.Border = sharp.Image.LEFT_BORDER | sharp.Image.RIGHT_BORDER | sharp.Image.TOP_BORDER | sharp.Image.BOTTOM_BORDER;
                img.BorderWidth = 1;
                
                document.Add(img);
                document.NewPage();
            }
            else if (seqtype == "15")
            {
                DataRow[] dr15 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                System.Data.DataTable mtxcolnames15 = createMtxColumnDataTable();

                System.Data.DataTable mtxrownames15 = createMtxRowDataTable();

                for (int imtxrow = 0; imtxrow < dr15.Length; imtxrow++)
                {

                    if (imtxrow == 0)
                    {
                        AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][2].ToString()), mtxrownames15);
                    }
                    else
                    {

                        DataRow[] dr = mtxrownames15.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()) + "'");


                        if (dr.Length == 0)
                        {

                            AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()), mtxrownames15);

                        }
                    }
                }


                for (int imtxcol = 0; imtxcol < dr15.Length; imtxcol++)
                {

                    if (imtxcol == 0)
                    {
                        AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][3].ToString().TrimStart()), mtxcolnames15);
                    }
                    else
                    {
                        DataRow[] dr = mtxcolnames15.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()) + "'");


                        if (dr.Length == 0)
                        {

                            AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()), mtxcolnames15);

                        }
                    }
                }

                
                sharp.Table tablequeslbl = new iTextSharp.text.Table(1, 1);
                tablequeslbl.Padding = 4;
                tablequeslbl.Width = 100;
                iTextSharp.text.Cell cellqueslbl = new iTextSharp.text.Cell(new Phrase(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD)));
                cellqueslbl.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cellqueslbl.VerticalAlignment = sharp.Element.ALIGN_CENTER;
                tablequeslbl.AddCell(cellqueslbl);
                tablequeslbl.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;
                tablequeslbl.BorderWidth = 1;
                document.Add(tablequeslbl);

                System.Web.UI.WebControls.Label quesLabel15 = new System.Web.UI.WebControls.Label();
                quesLabel15.Text = "";
                iTextSharp.text.Font font22B = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD);
                document.Add(new sharp.Paragraph(quesLabel15.Text, font22B));


                sharp.Table table = new iTextSharp.text.Table(mtxcolnames15.Rows.Count + 1, mtxrownames15.Rows.Count * 2 + 2);
                table.Padding = 4;
                table.Width = 100;

                iTextSharp.text.Cell cell15 = new iTextSharp.text.Cell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                cell15.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cell15.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                cell15.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
                table.AddCell(cell15);

                for (int irg = 0; irg < mtxcolnames15.Rows.Count; irg++)
                {
                    iTextSharp.text.Cell cellcol15 = new iTextSharp.text.Cell(new Phrase(System.Web.HttpUtility.HtmlDecode(mtxcolnames15.Rows[irg][0].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                    cellcol15.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cellcol15.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                    cellcol15.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
                    table.AddCell(cellcol15);
                }

                for (int irg1 = 0; irg1 < mtxrownames15.Rows.Count; irg1++)
                {

                    DataRow[] dr1011f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames15.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");


                    iTextSharp.text.Cell cellrow15 = new iTextSharp.text.Cell(new Phrase(System.Web.HttpUtility.HtmlDecode(mtxrownames15.Rows[irg1][0].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                    cellrow15.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cellrow15.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                    if (irg1 == 1)
                    {
                        cellrow15.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                    }
                    else
                    {
                        if ((irg1 % 2) == 1)
                        {
                            cellrow15.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            cellrow15.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                        }
                    }
                
                    table.AddCell(cellrow15);


                    for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                    {
                        iTextSharp.text.Cell cellrow115 = new iTextSharp.text.Cell(new Phrase(dr1011f[cgv][5].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                        cellrow115.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                        cellrow115.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                
                        if (irg1 == 1)
                        {
                            cellrow115.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            if ((irg1 % 2) == 1)
                            {
                                cellrow115.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                            }
                            else
                            {
                                cellrow115.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                            }
                        }
                        table.AddCell(cellrow115);
                    }

                    iTextSharp.text.Cell cell15b = new iTextSharp.text.Cell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
                    cell15b.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cell15b.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                    if (irg1 == 1)
                    {
                        cell15b.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                    }
                    else
                    {
                        if ((irg1 % 2) == 1)
                        {
                            cell15b.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            cell15b.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                        }
                    }
                    table.AddCell(cell15b);

                    for (int cgv1 = 0; cgv1 < dr1011f.Length; cgv1++)
                    {
                        iTextSharp.text.Cell cellrowb15 = new iTextSharp.text.Cell(new Phrase(dr1011f[cgv1][6].ToString() + "%", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                        cellrowb15.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                        cellrowb15.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
                
                        if (irg1 == 1)
                        {
                            cellrowb15.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                        }
                        else
                        {
                            if ((irg1 % 2) == 1)
                            {
                                cellrowb15.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                            }
                            else
                            {
                                cellrowb15.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                            }
                        }
                        table.AddCell(cellrowb15);
                    }

                }

                iTextSharp.text.Cell cell15resp = new iTextSharp.text.Cell(new Phrase("# of Respondents", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                cell15resp.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cell15resp.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
             
                table.AddCell(cell15resp);

                for (int irg = 0; irg < mtxcolnames15.Rows.Count - 1; irg++)
                {

                    iTextSharp.text.Cell cellcolempy15 = new iTextSharp.text.Cell(new Phrase("", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                    cellcolempy15.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                    cellcolempy15.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
             
                    table.AddCell(cellcolempy15);
                }

                iTextSharp.text.Cell cellnoresp15 = new iTextSharp.text.Cell(new Phrase(dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
                cellnoresp15.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cellnoresp15.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
             
                table.AddCell(cellnoresp15);
                
                document.Add(table);
                
                mtxcolnames15.Clear();
                mtxrownames15.Clear();

                document.NewPage();

             
                sharp.Table tablequeslblimg = new iTextSharp.text.Table(1, 1);
                tablequeslblimg.Padding = 4;
                tablequeslblimg.Width = 83;
                iTextSharp.text.Cell cellqueslblimg = new iTextSharp.text.Cell(new Phrase(StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD)));
                cellqueslblimg.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
                cellqueslblimg.VerticalAlignment = sharp.Element.ALIGN_CENTER;
                tablequeslblimg.AddCell(cellqueslblimg);
                tablequeslblimg.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;
                tablequeslblimg.BorderWidth = 1;
                document.Add(tablequeslblimg);

                string PicPath = SAVE_PATH + "pptimg" + dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString() + ".jpg";

                document.Add(new sharp.Paragraph());
                sharp.Image img = sharp.Image.GetInstance(PicPath);
                img.Alignment = sharp.Element.ALIGN_CENTER;
                img.ScalePercent(51);
                img.Border = sharp.Image.LEFT_BORDER | sharp.Image.RIGHT_BORDER | sharp.Image.TOP_BORDER | sharp.Image.BOTTOM_BORDER;
                img.BorderWidth = 1;
             
                document.Add(img);
                document.NewPage();
            }
        }
        
        document.NewPage();
        document.Add(new sharp.Paragraph("Thank you for using Insighto"));
        document.Close();
        string targetFile = SurveyBasicInfoView.SURVEY_NAME;
        string Extension = ".pdf";
        SaveDialogBox(path, targetFile, Extension);
    }

    public void exportpptx()
    {
      
        string sourcename = SAVE_PATH + "openxmlpptxfile.pptx";
        string destinationname = SAVE_PATH+ "openxmlpptxfile" + SelectedSurvey.SURVEY_ID.ToString() + ".pptx";

        if (File.Exists(destinationname))
        {
            File.Delete(destinationname);
        }

        File.Copy(sourcename, destinationname);

        using (PresentationDocument presentationDocument = PresentationDocument.Open(destinationname, true))
        {

            int pcnt = presentationDocument.PresentationPart.SlideParts.Count();

            // Pass the source document and the position and title of the slide to be inserted to the next method.

            int surveyid = Convert.ToInt32(SelectedSurvey.SURVEY_ID.ToString());
            DataSet dsansoptionsall = surcore.getAnsweroptionsQIDySurveOptPPTX(surveyid);
         

            Task[] tasks = new Task[dsansoptionsall.Tables[1].Rows.Count];

            try
            {
                for (int iseq = 0; iseq < dsansoptionsall.Tables[1].Rows.Count; iseq++)
                {
                   
                    if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "1" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "2" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "3" || dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "4")
                    {
                        string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                        DataRow[] dr1234 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                        CaptureFCImage CFCI = new CaptureFCImage();
                        CFCI.drQT1234 = dr1234;
                        CFCI.QuestionType = seqtype;
                        CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                        CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                        CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                      
                        tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt1234Img());

                    }
                    else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "13")
                    {
                        string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                        DataRow[] dr13 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                        CaptureFCImage CFCI = new CaptureFCImage();
                        CFCI.drQT1234 = dr13;
                        CFCI.QuestionType = seqtype;
                        CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                        CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                        CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                        tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt13Img());
                    }
                    else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "10")
                    {
                        string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                        DataRow[] dr10 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                        System.Data.DataTable mtxcolnames10 = createMtxColumnDataTable();

                        System.Data.DataTable mtxrownames10 = createMtxRowDataTable();

                        for (int imtxrow = 0; imtxrow < dr10.Length; imtxrow++)
                        {

                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[0][2].ToString()), mtxrownames10);
                            }
                            else
                            {
                                DataRow[] dr = mtxrownames10.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr10[imtxrow][2].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[imtxrow][2].ToString()), mtxrownames10);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr10.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[0][3].ToString().TrimStart()), mtxcolnames10);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames10.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr10[imtxcol][3].ToString().TrimStart()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr10[imtxcol][3].ToString().TrimStart()), mtxcolnames10);
                                }
                            }
                        }
                        System.Data.DataTable colgroupoptions10 = createDataTable();
                        for (int irg1 = 0; irg1 < mtxrownames10.Rows.Count; irg1++)
                        {

                            DataRow[] dr1011f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames10.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                            for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                            {

                                AddDatatoTable(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), mtxrownames10.Rows[irg1][0].ToString(), System.Web.HttpUtility.HtmlEncode(dr1011f[cgv][3].ToString()), dr1011f[cgv][6].ToString(), colgroupoptions10);
                            }


                        }


                        CaptureFCImage CFCI = new CaptureFCImage();
                        CFCI.drQT10 = dr10;
                        CFCI.QuestionType = seqtype;
                        CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                        CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                        CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                        CFCI.dtmtxrows10 = mtxrownames10;
                        CFCI.dtmtxcols10 = mtxcolnames10;
                        CFCI.dt10 = colgroupoptions10;
                        tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt10Img());

                      
                    }
                    else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "11")
                    {
                        string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                        DataRow[] dr11 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                        System.Data.DataTable mtxcolnames11 = createMtxColumnDataTable();

                        System.Data.DataTable mtxrownames11 = createMtxRowDataTable();

                        for (int imtxrow = 0; imtxrow < dr11.Length; imtxrow++)
                        {

                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[0][2].ToString()), mtxrownames11);
                            }
                            else
                            {
                                DataRow[] dr = mtxrownames11.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr11[imtxrow][2].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[imtxrow][2].ToString()), mtxrownames11);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr11.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[0][3].ToString().TrimStart()), mtxcolnames11);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames11.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr11[imtxcol][3].ToString().TrimStart()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr11[imtxcol][3].ToString().TrimStart()), mtxcolnames11);
                                }
                            }
                        }
                        System.Data.DataTable colgroupoptions11 = createDataTable();
                        for (int irg1 = 0; irg1 < mtxrownames11.Rows.Count; irg1++)
                        {

                            DataRow[] dr1011f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames11.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                            for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                            {

                                AddDatatoTable(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), mtxrownames11.Rows[irg1][0].ToString(), System.Web.HttpUtility.HtmlEncode(dr1011f[cgv][3].ToString()), dr1011f[cgv][6].ToString(), colgroupoptions11);
                            }


                        }


                        CaptureFCImage CFCI = new CaptureFCImage();
                        CFCI.drQT11 = dr11;
                        CFCI.QuestionType = seqtype;
                        CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                        CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                        CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                        CFCI.dtmtxrows11 = mtxrownames11;
                        CFCI.dtmtxcols11 = mtxcolnames11;
                        CFCI.dt11 = colgroupoptions11;
                        tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt11Img());

                        

                    }
                    else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "12")
                    {
                        string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();

                        DataRow[] dr12 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                        System.Data.DataTable mtxrownames12 = createMtxRowDataTable();

                        System.Data.DataTable mtxcolnames12 = createMtxColumnDataTable();

                        System.Data.DataTable mtxsubcolnames12 = createMtxColumnDataTable();

                        for (int imtxrow = 0; imtxrow < dr12.Length; imtxrow++)
                        {

                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(dr12[0][2].ToString(), mtxrownames12);
                            }
                            else
                            {
                                DataRow[] dr = mtxrownames12.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()), mtxrownames12);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr12.Length; imtxcol++)
                        {
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(dr12[0][3].ToString(), mtxcolnames12);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()), mtxcolnames12);

                                }
                            }
                        }

                        for (int imtxsubcol = 0; imtxsubcol < dr12.Length; imtxsubcol++)
                        {

                            if (imtxsubcol == 0)
                            {
                                AddMtxcolsDatatoTable(dr12[0][4].ToString(), mtxsubcolnames12);
                            }
                            else
                            {

                                DataRow[] dr = mtxsubcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()), mtxsubcolnames12);

                                }
                            }
                        }
                        System.Data.DataTable colgroupoptionsQ12 = createDataTableQ12();
                        for (int irg12data = 0; irg12data < mtxrownames12.Rows.Count; irg12data++)
                        {                          
                            DataRow[] dr12f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames12.Rows[irg12data][0].ToString()).Replace("'", "''") + "' and seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");
                            for (int cgv12 = 0; cgv12 < dr12f.Length; cgv12++)
                            {
                               AddDatatoTableQ12(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), System.Web.HttpUtility.HtmlEncode(dr12f[cgv12][3].ToString()), System.Web.HttpUtility.HtmlEncode(dr12f[cgv12][4].ToString()), mtxrownames12.Rows[irg12data][0].ToString(), dr12f[cgv12][6].ToString(), colgroupoptionsQ12);
                            }

                        }

                        CaptureFCImage CFCI = new CaptureFCImage();
                        CFCI.drQT12 = dr12;
                        CFCI.QuestionType = seqtype;
                        CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                        CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                        CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                        CFCI.dtmtxrows12 = mtxrownames12;
                        CFCI.dtmtxcols12 = mtxcolnames12;
                        CFCI.dtmtxsubcols12 = mtxsubcolnames12;
                        CFCI.dt12 = colgroupoptionsQ12;
                        tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt12Img());

                    }
                    else if (dsansoptionsall.Tables[1].Rows[iseq][3].ToString() == "15")
                    {
                        string seqtype = dsansoptionsall.Tables[1].Rows[iseq][3].ToString();
                        DataRow[] dr15 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                        System.Data.DataTable mtxcolnames15 = createMtxColumnDataTable();

                        System.Data.DataTable mtxrownames15 = createMtxRowDataTable();

                        for (int imtxrow = 0; imtxrow < dr15.Length; imtxrow++)
                        {

                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][2].ToString()), mtxrownames15);
                            }
                            else
                            {

                                DataRow[] dr = mtxrownames15.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()) + "'");


                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()), mtxrownames15);

                                }
                            }
                        }


                        for (int imtxcol = 0; imtxcol < dr15.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][3].ToString().TrimStart()), mtxcolnames15);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames15.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()) + "'");


                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()), mtxcolnames15);

                                }
                            }
                        }
                        DataTable dtg15 = new DataTable();

                        for (int irg = 0; irg < mtxcolnames15.Rows.Count; irg++)
                        {
                            dtg15.Columns.Add(System.Web.HttpUtility.HtmlDecode(mtxcolnames15.Rows[irg][0].ToString()));
                        }
                        System.Data.DataTable colgroupoptions15 = createDataTable();
                        for (int irg1 = 0; irg1 < mtxrownames15.Rows.Count; irg1++)
                        {
                            DataRow[] dr15f = dsansoptionsall.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames15.Rows[irg1][0].ToString()).Replace("'", "''") + "'" + "and seq_num = '" + dsansoptionsall.Tables[1].Rows[iseq][0].ToString() + "'");

                            for (int cgv = 0; cgv < dr15f.Length; cgv++)
                            {

                                AddDatatoTable(Convert.ToInt32(dsansoptionsall.Tables[1].Rows[iseq][1].ToString()), System.Web.HttpUtility.HtmlEncode(mtxrownames15.Rows[irg1][0].ToString()), System.Web.HttpUtility.HtmlEncode(dr15f[cgv][3].ToString()), dr15f[cgv][6].ToString(), colgroupoptions15);

                            }
                        }

                        CaptureFCImage CFCI = new CaptureFCImage();
                        CFCI.drQT15 = dr15;
                        CFCI.QuestionType = seqtype;
                        CFCI.Questionseq = dsansoptionsall.Tables[1].Rows[iseq][1].ToString();
                         CFCI.srvyQuestion = dsansoptionsall.Tables[1].Rows[iseq][2].ToString();
                        CFCI.SurveyID = SelectedSurvey.SURVEY_ID.ToString();
                        CFCI.dtmtxrows15 = mtxrownames15;
                        CFCI.dtmtxcols15 = mtxcolnames15;                  
                        CFCI.dt15 = colgroupoptions15;
                        tasks[iseq] = Task.Factory.StartNew(() => CFCI.generateQt15Img());

                    }
                }
                Task.WaitAll(tasks);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                pptxml.InsertNewfirstSlide(presentationDocument, 0, "firstslide",SelectedSurvey.SURVEY_NAME);

              
                for (int iseqppt = 0; iseqppt < dsansoptionsall.Tables[1].Rows.Count; iseqppt++)
                {

                    string seqtype = dsansoptionsall.Tables[1].Rows[iseqppt][3].ToString();


                    if ((seqtype == "1") || (seqtype == "2") || (seqtype == "3") || (seqtype == "4") || (seqtype == "13"))
                    {

                        DataRow[] dr1234 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                        pptxml.InsertNewSlide(presentationDocument, iseqppt, (iseqppt + 1).ToString(), iseqppt, seqtype, dr1234, dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString(), dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(),StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));

                    }
                    else if ((seqtype == "10") )
                    {
                        DataRow[] dr1011 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                        System.Data.DataTable mtxcolnames = createMtxColumnDataTable();

                        System.Data.DataTable mtxrownames = createMtxRowDataTable();

                        for (int imtxrow = 0; imtxrow < dr1011.Length; imtxrow++)
                        {
                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][2].ToString()), mtxrownames);
                            }
                            else
                            {
                                DataRow[] dr = mtxrownames.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()), mtxrownames);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr1011.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][3].ToString().TrimStart()), mtxcolnames);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()), mtxcolnames);
                                }
                            }
                        }
                        
                        pptxml.InsertNewSlide(presentationDocument, iseqppt, (iseqppt + 1).ToString(), iseqppt, seqtype, dr1011, dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString(), dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), mtxrownames, mtxcolnames, dsansoptionsall.Tables[2], dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString(),StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));

                        mtxrownames.Clear();
                        mtxcolnames.Clear();

                    }
                    else if ((seqtype == "11"))
                    {
                        DataRow[] dr1011 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                        System.Data.DataTable mtxcolnames11 = createMtxColumnDataTable();

                        System.Data.DataTable mtxrownames11 = createMtxRowDataTable();

                        for (int imtxrow = 0; imtxrow < dr1011.Length; imtxrow++)
                        {
                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][2].ToString()), mtxrownames11);
                            }
                            else
                            {
                                DataRow[] dr = mtxrownames11.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()), mtxrownames11);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr1011.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][3].ToString().TrimStart()), mtxcolnames11);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames11.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()), mtxcolnames11);
                                }
                            }
                        }


                        pptxml.InsertNewSlide(presentationDocument, iseqppt, (iseqppt + 1).ToString(), iseqppt, seqtype, dr1011, dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString(), dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), mtxrownames11, mtxcolnames11, dsansoptionsall.Tables[2], dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString(),StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));

                        mtxrownames11.Clear();
                        mtxcolnames11.Clear();
                    }
                    else if (seqtype == "12")
                    {
                        DataRow[] dr12 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                        System.Data.DataTable mtxrownames12 = createMtxRowDataTable();

                        System.Data.DataTable mtxcolnames12 = createMtxColumnDataTable();

                        System.Data.DataTable mtxsubcolnames12 = createMtxColumnDataTable();

                        for (int imtxrow = 0; imtxrow < dr12.Length; imtxrow++)
                        {

                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(dr12[0][2].ToString(), mtxrownames12);
                            }
                            else
                            {
                                DataRow[] dr = mtxrownames12.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()), mtxrownames12);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr12.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(dr12[0][3].ToString(), mtxcolnames12);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()), mtxcolnames12);

                                }
                            }
                        }

                        for (int imtxsubcol = 0; imtxsubcol < dr12.Length; imtxsubcol++)
                        {

                            if (imtxsubcol == 0)
                            {
                                AddMtxcolsDatatoTable(dr12[0][4].ToString(), mtxsubcolnames12);
                            }
                            else
                            {

                                DataRow[] dr = mtxsubcolnames12.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()), mtxsubcolnames12);

                                }
                            }
                        }

                        pptxml.InsertNewSlide(presentationDocument, iseqppt, (iseqppt + 1).ToString(), iseqppt, seqtype, dr12, dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString(), dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), mtxrownames12, mtxcolnames12, mtxsubcolnames12, dsansoptionsall.Tables[2], dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString(), StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));

                        mtxcolnames12.Clear();
                        mtxrownames12.Clear();
                        mtxsubcolnames12.Clear();
                    }
                    else if (seqtype == "15")
                    {
                        DataRow[] dr15 = dsansoptionsall.Tables[2].Select("seq_num = '" + dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString() + "'");

                        System.Data.DataTable mtxcolnames15 = createMtxColumnDataTable();

                        System.Data.DataTable mtxrownames15 = createMtxRowDataTable();

                        for (int imtxrow = 0; imtxrow < dr15.Length; imtxrow++)
                        {

                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][2].ToString()), mtxrownames15);
                            }
                            else
                            {

                                DataRow[] dr = mtxrownames15.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()) + "'");


                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()), mtxrownames15);

                                }
                            }
                        }


                        for (int imtxcol = 0; imtxcol < dr15.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][3].ToString().TrimStart()), mtxcolnames15);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames15.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()) + "'");


                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()), mtxcolnames15);

                                }
                            }
                        }


                        pptxml.InsertNewSlide15(presentationDocument, iseqppt, (iseqppt + 1).ToString(), iseqppt, seqtype, dr15, dsansoptionsall.Tables[1].Rows[iseqppt][1].ToString(), dsansoptionsall.Tables[1].Rows[iseqppt][7].ToString(), mtxrownames15, mtxcolnames15, dsansoptionsall.Tables[2], dsansoptionsall.Tables[1].Rows[iseqppt][0].ToString(), StripTagsRegex(dsansoptionsall.Tables[1].Rows[iseqppt][2].ToString()));

                        mtxcolnames15.Clear();
                        mtxrownames15.Clear();
                    }
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

           pptxml.DeleteSlide(presentationDocument, 0);
        }

   

        string name_survey = SelectedSurvey.SURVEY_NAME.Replace(" ", "");
        name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
        name_survey = name_survey.Replace("/", "");

        Response.ContentType = "application/vnd.openxmlformats-office";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + ".pptx");
        Response.TransmitFile(destinationname);
        Response.End();

    }

    protected void cmdExport_Click(object sender, EventArgs e)
    {

        try
        {
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();



        string strlicensetype;
        DataSet dssurveytype = surcore.getSurveyType(surveyID);
        if (dssurveytype.Tables[0].Rows[0][1].ToString() != "")
        {
            strlicensetype = dssurveytype.Tables[0].Rows[0][1].ToString();
        }
        else
        {
            strlicensetype = loggedInUserInfo.LicenseType;
        }


       // if (loggedInUserInfo.LicenseType == "FREE")
        if (strlicensetype == "FREE")
        {
            trgtDiv.Visible = true;
            lbn_rawdataexport.Visible = false;
        }
        else
        {

            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0)
                {
                    if (ht.Contains("QuesID"))
                        QuestID = Convert.ToInt32(ht["QuesID"]);
                    if (ht.Contains("Veiw"))
                        ViewAllQues = Convert.ToString(ht["Veiw"]);
                    if (ht.Contains("Mode"))
                        Mode = Convert.ToInt32(ht["Mode"]);
                    if (ht.Contains("ddlsidetext"))
                        SMode = Convert.ToInt32(ht["ddlsidetext"]);
                    if (ht.Contains("Date_Start"))
                        dt1 = Convert.ToDateTime(ht["Date_Start"]);
                    if (ht.Contains("Date_End"))
                        dt2 = Convert.ToDateTime(ht["Date_End"]);
                    // ddlsidetext
                }
            }


            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            if (userDetails != null && userDetails.UserId > 0)
            {

                if (SetFeatures("EXPORT") == 0 || (rdgdisplayset2.SelectedIndex == 0 && SetFeatures("EXPORT_PPT") == 0) || (rdgdisplayset2.SelectedIndex == 1 && SetFeatures("EXPORT_PDF") == 0) || (rdgdisplayset2.SelectedIndex == 2 && SetFeatures("EXPORT_EXCEL") == 0) || (rdgdisplayset2.SelectedIndex == 3 && SetFeatures("EXPORT_WORD") == 0))
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Upgrade", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", true);
                }
                else
                {
                    string ExportMode = "";

                    if (rblgrp.SelectedValue == "DataChart")                    
                    {
                        ExportMode = "DC";
                    }
                    if (rblgrp.SelectedValue == "Data")                   
                    {
                        ExportMode = "Data";
                    }
                    if (rblgrp.SelectedValue == "Chart")                 
                    {
                        ExportMode = "Charts";

                    }
                    
         
                    if (rdgdisplayset2.SelectedIndex == 1)
                    {
                       
                      //  CreatePDF(ExportMode);

                        exportpdf();
                        BindReportgrid(ht, ViewAllQues, dt1, dt2);

                    }
                    else if (rdgdisplayset2.SelectedIndex == 2)
                    {


                        WriteOpenCloseQToExcelSheet();
                       
                        BindReportgrid(ht, ViewAllQues, dt1, dt2);
                    }
                    else if (rdgdisplayset2.SelectedIndex == 0)
                    {
                       
                        exportpptx();

                        BindReportgrid(ht, ViewAllQues, dt1, dt2);
                    }
                    else if (rdgdisplayset2.SelectedIndex == 3)
                    {
                        exportdocx();                
                        BindReportgrid(ht, ViewAllQues, dt1, dt2);
                    }
                    //else
                    //{
                    //    CreateExportFile(rdgdisplayset2.SelectedIndex, ExportMode);
                    //    BindReportgrid(ht, ViewAllQues, dt1, dt2);
                    //}
                }
            }
         }
       
        }
        catch (Exception ex)
        {
            throw ex;
        }
        Page.RegisterStartupScript("Indshow", "<script>showHide('divExportTo','spanExportTo')</script>");

    }


    //to read file from you project folder you can use following.



  /*the method below takes two arguments the path of our excel file and the data to write in the excel file which is stored in a DataTable */

    public void WriteRawDataToExcelSheet()
    {
        try
        {
            //create FileInfo object  to read you ExcelWorkbook
            string strDirector = ConfigurationManager.AppSettings["RootPath"].ToString();
            if (!Directory.Exists(strDirector + SelectedSurvey.USER_ID + @"\" + SelectedSurvey.SURVEY_ID))
            {
                Directory.CreateDirectory(strDirector + SelectedSurvey.USER_ID + @"\" + SelectedSurvey.SURVEY_ID);
            }

          //  string strXLFileName = strDirector + SelectedSurvey.USER_ID + "\\" + SelectedSurvey.SURVEY_ID + "\\" + "RawDataExportComplete_" + SelectedSurvey.SURVEY_NAME.ToString() + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";

            string surveynamechar = RemoveSpecialCharacters(SelectedSurvey.SURVEY_NAME.ToString());
            string strXLFileName = strDirector + SelectedSurvey.USER_ID + "\\" + SelectedSurvey.SURVEY_ID + "\\" + "RawDataExportComplete_" + surveynamechar + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
                
                
            var file = new FileInfo(strXLFileName);

            using (ExcelPackage xlPackage = new ExcelPackage(file))
            {

                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Sheet1");

                //Select a range of cells to insert the data and use the LoadFromDataTable method to write the
                //data                

                worksheet.Cells[1, 1, 600, 600].Clear();

                DataSet mxreslimit = surcore.getSurveyType(SelectedSurvey.SURVEY_ID);
                string strsurvytype = mxreslimit.Tables[0].Rows[0][0].ToString();
                DataSet dsquestions = null;

                if ((strsurvytype == "PPS_PREMIUM") && (SurveyBasicInfoView.ResponseCount > maxAllowedResponseLimit))
                {

                    dsquestions = surcore.getRawdataexportquestions1000(surveyID);
                }
                else
                {
                    dsquestions = surcore.getRawdataexportquestions(surveyID);
                }


                System.Data.DataTable dtquestions = dsquestions.Tables[0];
                // Srini, Build a 2 column table containing respondent id and rownum
                System.Data.DataTable dtRespId = dsquestions.Tables[1];

                System.Data.DataTable mtxcolnames10 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames11 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames12 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames15 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames13 = createMtxColumnDataTable();
                System.Data.DataTable mtxcolnames20 = createMtxColumnDataTable();

                int colval = 0;

                worksheet.Cells[2, colval + 1].Value = "Email";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                colval = colval + 1;

                worksheet.Cells[2, colval + 1].Value = "Submit Time";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                worksheet.Cells[2, colval + 1].Value = "Serial number";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                // Srini, get all responses for a question across all options, give tpm boost
                DataSet[] dsgetRawdataAnswersForQues = new DataSet[dtquestions.Rows.Count];
                DataSet[] dsmultiselectques = new DataSet[dtquestions.Rows.Count];
                Task[] tasks = new Task[dtquestions.Rows.Count]; ;
                for (int i = 0; i < dtquestions.Rows.Count; i++)
                {
                    int quest_id = Convert.ToInt32(dtquestions.Rows[i][1].ToString());
                    int index = i;
                    tasks[i] = Task.Factory.StartNew(() => getRawdataAnswersForQuesFunc(quest_id, index, dsgetRawdataAnswersForQues,
                                                           dsmultiselectques));

                }
                Task.WaitAll(tasks);

                for (int i = 0; i < dtquestions.Rows.Count; i++)
                {
                    //getting questions
                    worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + HttpUtility.HtmlDecode(dtquestions.Rows[i][2].ToString());
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                    worksheet.Column(colval + 1).Width = 30;

                    // Draw thick black border line for the entire column
                    worksheet.Cells[2, colval + 1, dsquestions.Tables[1].Rows.Count + 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[2, colval + 1, dsquestions.Tables[1].Rows.Count + 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);


                    if ((dtquestions.Rows[i][3].ToString() == "1") || (dtquestions.Rows[i][3].ToString() == "3") || (dtquestions.Rows[i][3].ToString() == "4"))
                    {
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                        if (dtquestions.Rows[i][5].ToString() == "1")
                        {
                            worksheet.Cells[2, colval + 2, 2, colval + 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[2, colval + 2, 2, colval + 2].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        }
                        else
                        {
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        }

                        // Srini, fill data
                        for (int i134 = 0; i134 < dsmultiselectques[i].Tables[0].Rows.Count; i134++)
                        {
                            FillRawDataForColumn(colval + 1, worksheet, dtquestions.Rows[i][3].ToString(), dsgetRawdataAnswersForQues[i], dsmultiselectques[i].Tables[0].Rows[i134][2].ToString(), dtRespId,
                                                  dsmultiselectques[i].Tables[0].Rows[i134][3].ToString(), "0");
                        }

                    }
                    else
                    {
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    }


                    colval = colval + 1;

                    //  DataTable dtmultiselectques;
                    if (dtquestions.Rows[i][3].ToString() == "2")
                    {

                        for (int i2 = 0; i2 < dsmultiselectques[i].Tables[0].Rows.Count; i2++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + dsmultiselectques[i].Tables[0].Rows[i2][3].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            // Srini, fill data for this column
                            FillRawDataForColumn(colval + 1, worksheet, "2", dsgetRawdataAnswersForQues[i], dsmultiselectques[i].Tables[0].Rows[i2][2].ToString(), dtRespId,
                                                  dsmultiselectques[i].Tables[0].Rows[i2][3].ToString(), "0");

                            colval = colval + 1;
                        }
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "9")
                    {
                        for (int i11 = 0; i11 < dsmultiselectques[i].Tables[0].Rows.Count; i11++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + dsmultiselectques[i].Tables[0].Rows[i11][3].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            // Srini, fill data for this column
                            FillRawDataForColumn(colval + 1, worksheet, "9", dsgetRawdataAnswersForQues[i], dsmultiselectques[i].Tables[0].Rows[i11][2].ToString(), dtRespId,
                                                  dsmultiselectques[i].Tables[0].Rows[i11][3].ToString(), "0");

                            colval = colval + 1;
                        }
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "10")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques[i].Tables[1].Rows.Count; imtxcol++)
                        {
                            //CHANGED BY SATISH TO FIX SINGLE QUOTE ENCODING ERROR 10-SEP-15
                            var ques = dsmultiselectques[i].Tables[1].Rows[imtxcol][0].ToString();
                            //ques = ques.Replace("'", "'");
                            var cond = "columnname = '" + ques.Replace("'", "''") + "'";
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlDecode(ques), mtxcolnames10);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames10.Select(cond);
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlDecode(ques), mtxcolnames10);
                                }
                            }
                        }
                        for (int i10 = 0; i10 < mtxcolnames10.Rows.Count; i10++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames10.Rows[i10][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            FillRawDataForColumn(colval + 1, worksheet, "10", dsgetRawdataAnswersForQues[i], dsmultiselectques[i].Tables[0].Rows[i10][2].ToString(),
                                                   dtRespId, mtxcolnames10.Rows[i10][0].ToString(), "0");

                            colval = colval + 1;
                        }
                        mtxcolnames10.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "11")
                    {
                        for (int i11 = 0; i11 < dsmultiselectques[i].Tables[0].Rows.Count; i11++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + dsmultiselectques[i].Tables[0].Rows[i11][3].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            FillRawDataForColumn(colval + 1, worksheet, "11", dsgetRawdataAnswersForQues[i], dsmultiselectques[i].Tables[0].Rows[i11][2].ToString(),
                                                   dtRespId, dsmultiselectques[i].Tables[0].Rows[i11][3].ToString(), "0");

                            colval = colval + 1;
                        }
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "12")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques[i].Tables[2].Rows.Count; imtxcol++)
                        {
                            //CHANGED BY SATISH TO FIX SINGLE QUOTE ENCODING ERROR 10-SEP-15
                            var ques = dsmultiselectques[i].Tables[1].Rows[imtxcol][0].ToString();
                            ques = ques.Replace("'", "''");
                            var cond = "columnname = '" + System.Web.HttpUtility.HtmlDecode(ques) + "'";
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlDecode(ques), mtxcolnames12);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames12.Select(cond);
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlDecode(ques), mtxcolnames12);
                                }
                            }
                        }
                        for (int i12 = 0; i12 < mtxcolnames12.Rows.Count; i12++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ":" + mtxcolnames12.Rows[i12][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            //Srini
                            FillRawDataForColumn(colval + 1, worksheet, "12", dsgetRawdataAnswersForQues[i], "",
                                                   dtRespId, mtxcolnames12.Rows[i12][0].ToString(), "0");

                            colval = colval + 1;
                        }
                        mtxcolnames12.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "15")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques[i].Tables[3].Rows.Count; imtxcol++)
                        {
                            //CHANGED BY SATISH TO FIX SINGLE QUOTE ENCODING ERROR 10-SEP-15
                            var ques = dsmultiselectques[i].Tables[1].Rows[imtxcol][0].ToString();
                            ques = ques.Replace("'", "''");
                            var cond = "columnname = '" + System.Web.HttpUtility.HtmlDecode(ques) + "'";
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlDecode(ques), mtxcolnames15);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames15.Select(cond);
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlDecode(ques), mtxcolnames15);
                                }
                            }
                        }
                        for (int i15 = 0; i15 < mtxcolnames15.Rows.Count; i15++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames15.Rows[i15][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            //Srini
                            FillRawDataForColumn(colval + 1, worksheet, "15", dsgetRawdataAnswersForQues[i], "",
                                                   dtRespId, mtxcolnames15.Rows[i15][0].ToString(), "0");

                            colval = colval + 1;
                        }
                        mtxcolnames15.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "13")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques[i].Tables[3].Rows.Count; imtxcol++)
                        {
                            //CHANGED BY SATISH TO FIX SINGLE QUOTE ENCODING ERROR 10-SEP-15
                            var ques = dsmultiselectques[i].Tables[1].Rows[imtxcol][0].ToString();
                            ques = ques.Replace("'", "''");
                            var cond = "columnname = '" + System.Web.HttpUtility.HtmlDecode(ques) + "'";
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlDecode(ques), mtxcolnames13);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames13.Select(cond);
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(ques), mtxcolnames13);
                                }
                            }
                        }
                        for (int i13 = 0; i13 < mtxcolnames13.Rows.Count; i13++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames13.Rows[i13][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            //Srini
                            FillRawDataForColumn(colval + 1, worksheet, "13", dsgetRawdataAnswersForQues[i], "",
                                                   dtRespId, mtxcolnames13.Rows[i13][0].ToString(), "0");

                            colval = colval + 1;
                        }
                        mtxcolnames13.Clear();
                    }
                    else if (dtquestions.Rows[i][3].ToString() == "20")
                    {
                        for (int imtxcol = 0; imtxcol < dsmultiselectques[i].Tables[3].Rows.Count; imtxcol++)
                        {

                            //CHANGED BY SATISH TO FIX SINGLE QUOTE ENCODING ERROR 10-SEP-15
                            var ques = dsmultiselectques[i].Tables[1].Rows[imtxcol][0].ToString();
                            ques = ques.Replace("'", "''");
                            var cond = "columnname = '" + System.Web.HttpUtility.HtmlDecode(ques) + "'";
                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlDecode(ques), mtxcolnames20);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames20.Select(cond);
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlDecode(ques), mtxcolnames20);

                                }
                            }
                        }
                        for (int i20 = 0; i20 < mtxcolnames20.Rows.Count; i20++)
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + dtquestions.Rows[i][0].ToString() + ": " + mtxcolnames20.Rows[i20][0].ToString();
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            FillRawDataForColumn(colval + 1, worksheet, "20", dsgetRawdataAnswersForQues[i], dsmultiselectques[i].Tables[0].Rows[i20][2].ToString(),
                                                 dtRespId, "", "0");

                            colval = colval + 1;
                        }
                        mtxcolnames20.Clear();
                    }
                    else if ((dtquestions.Rows[i][3].ToString() == "6") ||
                             (dtquestions.Rows[i][3].ToString() == "5") ||
                             (dtquestions.Rows[i][3].ToString() == "7") ||
                             (dtquestions.Rows[i][3].ToString() == "8") ||
                             (dtquestions.Rows[i][3].ToString() == "14"))
                    {
                        FillRawDataForColumn(colval, worksheet, dtquestions.Rows[i][3].ToString(), dsgetRawdataAnswersForQues[i], "0", dtRespId, "", "0");
                    }

                    //displaying other option
                    if ((dtquestions.Rows[i][3].ToString() == "1") || (dtquestions.Rows[i][3].ToString() == "2") || (dtquestions.Rows[i][3].ToString() == "3") || (dtquestions.Rows[i][3].ToString() == "4"))
                    {
                        if (dtquestions.Rows[i][5].ToString() == "1")
                        {
                            worksheet.Cells[2, colval + 1].Value = "Question" + HttpUtility.HtmlDecode(dtquestions.Rows[i][0].ToString()) + ": Other, Please specify";
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                            worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                            worksheet.Column(colval + 1).Width = 30;
                            // Srini
                            FillRawDataForColumn(colval + 1, worksheet, dtquestions.Rows[i][3].ToString(), dsgetRawdataAnswersForQues[i], "0", dtRespId, "", "1");

                            colval = colval + 1;
                        }
                    }

                    // Border for Column headers
                    worksheet.Cells[2, 1, 2, colval].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[2, 1, 2, colval].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                    worksheet.Cells[2, 1, 2, colval].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[2, 1, 2, colval].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                    // Border for last question column
                    if (i == dtquestions.Rows.Count - 1)
                    {
                        worksheet.Cells[2, colval, dsquestions.Tables[1].Rows.Count + 2, colval].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet.Cells[2, colval, dsquestions.Tables[1].Rows.Count + 2, colval].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                    }
                }

                // Border for header
                worksheet.Cells[2, colval, 2, colval].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval, 2, colval].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                // Border for top of sheet
                worksheet.Cells[1, 1, 1, colval].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[1, 1, 1, colval].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, 1, 2, colval].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, 1, 2, colval].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                // Border for bottom of sheet
                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, 1, dsquestions.Tables[1].Rows.Count + 2, colval].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[dsquestions.Tables[1].Rows.Count + 2, 1, dsquestions.Tables[1].Rows.Count + 2, colval].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                int rowvaldata = 3;
                  var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
                for (int resrow = 0; resrow < dsquestions.Tables[1].Rows.Count; resrow++)
                {
                    int colvaldata = 0;
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsquestions.Tables[1].Rows[resrow][5].ToString();
                    colvaldata = colvaldata + 1;

                    DateTime dt = Convert.ToDateTime(dsquestions.Tables[1].Rows[resrow][3].ToString());
                    dt = BuildQuetions.GetConvertedDatetime(dt, userDetails.StandardBIAS, SurveyBasicInfoView.TIME_ZONE, true);
                  //  worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsquestions.Tables[1].Rows[resrow][3].ToString();
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dt.ToString();
                    colvaldata = colvaldata + 1;
                   // colvaldata = colvaldata + 1;
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = resrow + 1;
                    colvaldata = colvaldata + 1;

                    worksheet.Cells[rowvaldata, 1, rowvaldata, colvaldata].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[rowvaldata, 1, rowvaldata, colvaldata].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);

                    rowvaldata = rowvaldata + 1;
                }


                //Then we save our changes so that they are reflected in ou excel sheet.
                xlPackage.Save();

                if (File.Exists(strXLFileName))
                {

                    string name_survey = SelectedSurvey.SURVEY_NAME.Replace(" ", "");
                    name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                    name_survey = name_survey.Replace("/", "");
                    Response.ContentType = "application/ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + "_Rawdataexport" + ".xlsx");
                    Response.TransmitFile(strXLFileName);
                    Response.End();

                }

            }

        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            //set workbook object to null
            //if (workBook != null)
            //    workBook = null;
        }
    }

    private void getRawdataAnswersForQuesFunc(int question_id, int index, DataSet[] dsgetRawdataAnswersForQues, DataSet[] dsmultiselectques)
    // private void getRawdataAnswersForQuesFunc(int question_id)
    {
        DataSet dsMSQ = null;
        DataSet dsGRAFQ = null;
        //getting answeroptions as columns
        dsMSQ = surcore.getRawdataexportansweroptions(question_id);
        dsmultiselectques[index] = dsMSQ;
        // Srini, get all responses for a question across all options
        dsGRAFQ = surcore.getRawdataAnswersForQues(question_id);
        dsgetRawdataAnswersForQues[index] = dsGRAFQ;
    }



    private string SetNumberFormat(Type type)
    {
        string result = "@";

        if (type == typeof(string))
            result = "@";
        else if (type == typeof(sbyte))
            result = "##0";
        else if (type == typeof(byte))
            result = "##0";
        else if (type == typeof(bool))
            result = "0";
        else if (type == typeof(short))
            result = "####0";
        else if (type == typeof(ushort))
            result = "####0";
        else if (type == typeof(int))
            result = "#,##0";
        else if (type == typeof(uint))
            result = "#,##0";
        else if (type == typeof(long))
            result = "#,##0";
        else if (type == typeof(ulong))
            result = "#,##0";
        else if (type == typeof(float))
            result = ".#######";
        else if (type == typeof(double))
            result = ".################";
        else if (type == typeof(decimal))
            result = "₪ .00##########################";
        else if (type == typeof(DateTime))
            result = "dd/mm/yyyy";

        return result;
    }
    public static string RemoveSpecialCharacters(string str)
    {
        return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
    }
    public void WriteOpenCloseQToExcelSheet()
    {
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        try
        {

            //create FileInfo object  to read you ExcelWorkbook
            DataSet dssurveydata = surcore.getSurveyAnalyticsData(Convert.ToInt32(SelectedSurvey.SURVEY_ID));

            string strDirector = ConfigurationManager.AppSettings["RootPath"].ToString();
            if (!Directory.Exists(strDirector + SelectedSurvey.USER_ID + @"\" + SelectedSurvey.SURVEY_ID))
            {
                Directory.CreateDirectory(strDirector + SelectedSurvey.USER_ID + @"\" + SelectedSurvey.SURVEY_ID);
            }


            string surveynamechar = RemoveSpecialCharacters(SelectedSurvey.SURVEY_NAME.ToString());
                //SelectedSurvey.SURVEY_NAME.ToString().Replace("@", "").Replace("/", "").Replace("*", "");
            string strXLFileName = strDirector + SelectedSurvey.USER_ID + "\\" + SelectedSurvey.SURVEY_ID + "\\" + surveynamechar + "_Summary_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";


            var file = new FileInfo(strXLFileName);


            using (ExcelPackage xlPackage = new ExcelPackage(file))
            {

                //Fetch the worksheet to insert the data
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Survey Profile and Statistics");

                //Select a range of cells to insert the data and use the LoadFromDataTable method to write the
                //data                 
                string ycolor = "#FFBF00";
                string yblue = "#C6CFEB";

                worksheet.Cells[1, 1, 800, 800].Clear();
                worksheet.Column(1).Width = 30;
                worksheet.Column(2).Width = 30;
                worksheet.Column(4).Width = 30;
                worksheet.Column(5).Width = 20;
                worksheet.Column(6).Width = 20;
                worksheet.Cells[1, 1, 1, 2].Merge = true;
                worksheet.Cells[1, 1, 1, 2].Value = "Survey Fact Profile";
                worksheet.Cells[1, 1, 1, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[1, 1, 1, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                worksheet.Cells[1, 1, 1, 2].Style.WrapText = true;
                worksheet.Cells[1, 1, 1, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[1, 1, 1, 2].Style.Font.Bold = true;


                worksheet.Cells[3, 1].Value = "Survey Title";
                worksheet.Cells[3, 1].Style.Font.Bold = true;
                worksheet.Cells[3, 2].Value = SelectedSurvey.SURVEY_NAME;

                worksheet.Cells[4, 1].Value = "User Name";
                worksheet.Cells[4, 1].Style.Font.Bold = true;
        
                worksheet.Cells[4, 2].Value = loggedInUserInfo.Email;

                worksheet.Cells[5, 1].Value = "Launch Date";
                worksheet.Cells[5, 1].Style.Font.Bold = true;
                if (dssurveydata.Tables[5].Rows.Count > 0)
                {
                    string launchdate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(dssurveydata.Tables[5].Rows[0][0].ToString()), loggedInUserInfo.StandardBIAS, true).ToString("dd-MMM-yyyy hh:mm tt");
                    worksheet.Cells[5, 2].Value = launchdate;
                    //worksheet.Cells[5, 2].Value = dssurveydata.Tables[5].Rows[0][0].ToString();
                }
                else
                {
                    worksheet.Cells[5, 2].Value = " ";
                }


                worksheet.Cells[6, 1].Value = "Date of Export";
                worksheet.Cells[6, 1].Style.Font.Bold = true;

                string dateofexport = Convert.ToString(Convert.ToDateTime(CommonMethods.GetConvertedDatetime(DateTime.Now, loggedInUserInfo.StandardBIAS, true)).ToString("dd-MMM-yyyy") + " at " + DateTime.Now.ToShortTimeString());
                //worksheet.Cells[6, 2].Value = Convert.ToString(Convert.ToDateTime(DateTime.Now).ToString("dd-MMM-yyyy") + " at " + DateTime.Now.ToShortTimeString());
                worksheet.Cells[6, 2].Value = dateofexport;

                worksheet.Cells[1, 4, 1, 6].Merge = true;
                worksheet.Cells[1, 4, 1, 6].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[1, 4, 1, 6].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                worksheet.Cells[1, 4, 1, 6].Style.WrapText = true;
                worksheet.Cells[1, 4, 1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[1, 4, 1, 6].Style.Font.Bold = true;
                worksheet.Cells[1, 4, 1, 6].Value = "Survey Name:" + SelectedSurvey.SURVEY_NAME;

                worksheet.Cells[2, 4, 2, 6].Merge = true;
                worksheet.Cells[2, 4, 2, 6].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;

                worksheet.Cells[2, 4, 2, 6].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(ycolor));
                worksheet.Cells[2, 4, 2, 6].Style.WrapText = true;
                worksheet.Cells[2, 4, 2, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[2, 4, 2, 6].Style.Font.Bold = true;
                worksheet.Cells[2, 4, 2, 6].Value = "Survey Statistics Report";


                worksheet.Cells[3, 5].Value = "Count";
                worksheet.Cells[3, 5].Style.Font.Bold = true;

                worksheet.Cells[3, 6].Value = "Response in %";
                worksheet.Cells[3, 6].Style.Font.Bold = true;


                worksheet.Cells[4, 4].Value = "# of Visits";
                worksheet.Cells[4, 4].Style.Font.Bold = true;
                worksheet.Cells[4, 5].Value = Convert.ToInt32(dssurveydata.Tables[0].Rows[0][0].ToString());
                worksheet.Cells[4, 6].Value = Convert.ToInt32(dssurveydata.Tables[0].Rows[0][1].ToString());

                worksheet.Cells[5, 4].Value = "Completes";
                worksheet.Cells[5, 4].Style.Font.Bold = true;
                worksheet.Cells[5, 5].Value = Convert.ToInt32(dssurveydata.Tables[1].Rows[0][0].ToString());
                worksheet.Cells[5, 6].Value = Convert.ToInt32(dssurveydata.Tables[1].Rows[0][1].ToString());

                worksheet.Cells[6, 4].Value = "Partials";
                worksheet.Cells[6, 4].Style.Font.Bold = true;
                worksheet.Cells[6, 5].Value = Convert.ToInt32(dssurveydata.Tables[2].Rows[0][0].ToString());
                worksheet.Cells[6, 6].Value = Convert.ToInt32(dssurveydata.Tables[2].Rows[0][1].ToString());


                worksheet.Cells[7, 4].Value = "Bounced";
                worksheet.Cells[7, 4].Style.Font.Bold = true;
                worksheet.Cells[7, 5].Value = 0;
                worksheet.Cells[7, 6].Value = 0;

                worksheet.Cells[8, 4].Value = "Screened out";
                worksheet.Cells[8, 4].Style.Font.Bold = true;
                worksheet.Cells[8, 5].Value = Convert.ToInt32(dssurveydata.Tables[3].Rows[0][0].ToString());
                worksheet.Cells[8, 6].Value = Convert.ToInt32(dssurveydata.Tables[3].Rows[0][1].ToString());

                worksheet.Cells[9, 4].Value = "Over-quota";
                worksheet.Cells[9, 4].Style.Font.Bold = true;
                worksheet.Cells[9, 5].Value = Convert.ToInt32(dssurveydata.Tables[4].Rows[0][0].ToString());
                worksheet.Cells[9, 6].Value = Convert.ToInt32(dssurveydata.Tables[4].Rows[0][1].ToString());


                worksheet.Cells[1, 4, 1, 6].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[1, 4, 1, 6].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[1, 4, 1, 6].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[1, 4, 1, 6].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, 4, 2, 6].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, 4, 2, 6].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[3, 4, 3, 6].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[3, 4, 3, 6].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[9, 4, 9, 6].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[9, 4, 9, 6].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                worksheet.Cells[1, 4, 9, 6].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[1, 4, 9, 6].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[1, 4, 9, 6].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[1, 4, 9, 6].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                worksheet.Cells[10, 1].Value = "Survey URL";
                worksheet.Cells[10, 2].Value = dssurveydata.Tables[6].Rows[0][0].ToString();


                int TRid = 1;

                // ExcelWorksheet worksheet1 = xlPackage.Workbook.Worksheets["CloseEndedResponses"];
                ExcelWorksheet worksheet1 = xlPackage.Workbook.Worksheets.Add("Close-ended Responses");

                worksheet1.Cells[1, 1, 800, 800].Clear();

                worksheet1.Column(1).Width = 30;
                worksheet1.Column(2).Width = 10;
                worksheet1.Column(3).Width = 10;


                worksheet1.Cells[2, 1, 2, 3].Merge = true;
                worksheet1.Cells[2, 1, 2, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet1.Cells[2, 1, 2, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                worksheet1.Cells[2, 1, 2, 3].Style.WrapText = true;
                worksheet1.Cells[2, 1, 2, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet1.Cells[2, 1, 2, 3].Style.Font.Bold = true;
                worksheet1.Cells[2, 1, 2, 3].Value = "Survey Name:" + SelectedSurvey.SURVEY_NAME;

                worksheet1.Cells[2, 3, 2, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[2, 3, 2, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                worksheet1.Cells[2, 1, 2, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[2, 1, 2, 3].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);


                //DataSet dsquestions = surcore.getRawdataexportquestions(surveyID);

                DataSet mxreslimit = surcore.getSurveyType(SelectedSurvey.SURVEY_ID);
                string strsurvytype = mxreslimit.Tables[0].Rows[0][0].ToString();
                DataSet dsquestions = null;
                if ((strsurvytype == "PPS_PRO") && (SurveyBasicInfoView.ResponseCount > maxAllowedResponseLimit))
                {
                    dsquestions = surcore.getAnsweroptionsQIDySurveOpt500(surveyID);
                }
                else if ((strsurvytype == "PPS_PREMIUM") && (SurveyBasicInfoView.ResponseCount > maxAllowedResponseLimit))
                {
                    dsquestions = surcore.getAnsweroptionsQIDySurveOpt1000(surveyID);
                }
                else
                {
                    if (Session["searchclick"] == "Clicked")
                    {
                        //dsansoptionsall = surcore.getAnsweroptionsQIDySurveOptDateRange(SelectedSurvey.SURVEY_ID, dt1, dt2);    
                        dsquestions = surcore.getAnsweroptionsQIDySurveOptDateRange(surveyID, dedstartdate.Date, dedEnddate.Date);
                    }
                    else
                    {
                        dsquestions = surcore.getAnsweroptionsQIDySurveOpt(surveyID);
                    }


                }

                int intQrowdata = 4;
                for (int i = 0; i < dsquestions.Tables[1].Rows.Count; i++)
                {
                    if ((dsquestions.Tables[1].Rows[i][3].ToString() == "1") || (dsquestions.Tables[1].Rows[i][3].ToString() == "2") || (dsquestions.Tables[1].Rows[i][3].ToString() == "3") || (dsquestions.Tables[1].Rows[i][3].ToString() == "4"))
                    {

                        worksheet1.Cells[intQrowdata, 1].Value = dsquestions.Tables[1].Rows[i][8].ToString();
                        intQrowdata = intQrowdata + 1;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Merge = true;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Font.Bold = true;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Value = "Q" + dsquestions.Tables[1].Rows[i][0].ToString() + ":" + StripTagsRegex(dsquestions.Tables[1].Rows[i][2].ToString()).Trim();

                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, 3, intQrowdata, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 3, intQrowdata, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        intQrowdata = intQrowdata + 1;

                        DataRow[] dr1234 = dsquestions.Tables[2].Select("seq_num = '" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");
                        DataTable dtg = new DataTable();
                        dtg.TableName = "dtQT" + i;
                        DataRow dtgr;
                        DataRow dtgresp;

                        dtg.Columns.Add("Answer Options");
                        dtg.Columns.Add("In %");
                        dtg.Columns.Add("In nos.");


                        for (int irg1 = 0; irg1 < dr1234.Length; irg1++)
                        {
                            dtgr = dtg.NewRow();
                            dtgr[0] = dr1234[irg1][2].ToString();
                            dtgr[1] = dr1234[irg1][6].ToString() + "%";
                            dtgr[2] = (int)dr1234[irg1][5];

                            dtg.Rows.Add(dtgr);
                        }

                        dtgresp = dtg.NewRow();
                        dtgresp[0] = "Total";
                        dtgresp[1] = " ";
                        dtgresp[2] = dsquestions.Tables[1].Rows[i][7].ToString();
                        dtg.Rows.Add(dtgresp);

                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(ycolor));
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        /***    CHANGED BY SATISH ON 11-03-15****************/
                        //ADDING EACH CELL WITH THE FIGURES OF THE SURVEY ANSWER OPTIONS SO THAT THEY CAN BE CONVERTED INTO INTEGER
                        //WHERE REQUIRED RATHER THAN BEING LOADED FROM DATA TABLE
                        //worksheet1.Cells[intQrowdata, 1].LoadFromDataTable(dtg, true);
                        worksheet1.Cells[intQrowdata, 1].Value = "Answer Options";
                        worksheet1.Cells[intQrowdata, 2].Value = "In %";
                        worksheet1.Cells[intQrowdata, 3].Value = "In nos.";
                        for (int irg1 = 0; irg1 < dr1234.Length; irg1++)
                        {
                            worksheet1.Cells[intQrowdata + 1 + irg1, 1].Value = dr1234[irg1][2];
                            worksheet1.Cells[intQrowdata + 1 + irg1, 2].Value = Convert.ToDecimal(dr1234[irg1][6]) / 100;
                            worksheet1.Cells[intQrowdata + 1 + irg1, 2].Style.Numberformat.Format = "0%";
                            worksheet1.Cells[intQrowdata + 1 + irg1, 3].Value = Convert.ToInt32(dr1234[irg1][5]);
                        }
                        worksheet1.Cells[intQrowdata + dr1234.Length + 1, 1].Value = "Total";
                        worksheet1.Cells[intQrowdata + dr1234.Length + 1, 2].Value = "";
                        worksheet1.Cells[intQrowdata + dr1234.Length + 1, 3].Value = Convert.ToInt32(dsquestions.Tables[1].Rows[i][7]);
                        /***************************************************/

                        worksheet1.Cells[intQrowdata, 1, intQrowdata + dr1234.Length + 1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata + dr1234.Length + 1, 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata + dr1234.Length + 1, 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        worksheet1.Cells[intQrowdata, 2, intQrowdata + dr1234.Length + 1, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        worksheet1.Cells[intQrowdata, 2, intQrowdata + dr1234.Length + 1, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet1.Cells[intQrowdata, 2, intQrowdata + dr1234.Length + 1, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        worksheet1.Cells[intQrowdata + dr1234.Length + 1, 1, intQrowdata + dr1234.Length + 1, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata + dr1234.Length + 1, 1, intQrowdata + dr1234.Length + 1, 3].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, 3, intQrowdata + dr1234.Length + 1, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 3, intQrowdata + dr1234.Length + 1, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        intQrowdata = intQrowdata + dtg.Rows.Count + 2;


                        dtg.Clear();

                    }
                    else if (dsquestions.Tables[1].Rows[i][3].ToString() == "13")
                    {

                        worksheet1.Cells[intQrowdata, 1].Value = dsquestions.Tables[1].Rows[i][8].ToString();

                        intQrowdata = intQrowdata + 1;

                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Merge = true;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.WrapText = true;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Font.Bold = true;

                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Value = "Q" + dsquestions.Tables[1].Rows[i][0].ToString() + ":" + StripTagsRegex(dsquestions.Tables[1].Rows[i][2].ToString()).Trim();

                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, 3, intQrowdata, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 3, intQrowdata, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        intQrowdata = intQrowdata + 1;

                        DataRow[] dr13 = dsquestions.Tables[2].Select("seq_num = '" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");

                        DataTable dtg13 = new DataTable();
                        dtg13.TableName = "dtQT" + i;
                        DataRow dtgr13;
                        DataRow dtgrcs;
                        DataRow dtgresp13;

                        dtg13.Columns.Add("Answer Options");
                        dtg13.Columns.Add("In %");
                        dtg13.Columns.Add("In nos.");

                        int constsum = 0;


                        for (int irg1 = 0; irg1 < dr13.Length; irg1++)
                        {
                            dtgr13 = dtg13.NewRow();

                            dtgr13[0] = dr13[irg1][2].ToString();
                            dtgr13[1] = dr13[irg1][6].ToString() + "%";
                            dtgr13[2] = dr13[irg1][5].ToString();

                            constsum = constsum + Convert.ToInt32(dr13[irg1][5].ToString());

                            dtg13.Rows.Add(dtgr13);
                        }


                        dtgrcs = dtg13.NewRow();
                        dtgrcs[0] = "Constant Sum";
                        dtgrcs[1] = " ";
                        dtgrcs[2] = constsum;
                        dtg13.Rows.Add(dtgrcs);

                        dtgresp13 = dtg13.NewRow();
                        dtgresp13[0] = "Total";
                        dtgresp13[1] = " ";
                        int respcount = Convert.ToInt32(dsquestions.Tables[1].Rows[i][4]) * Convert.ToInt32(dsquestions.Tables[1].Rows[i][7]);
                        dtgresp13[2] = respcount.ToString();
                        dtg13.Rows.Add(dtgresp13);

                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(ycolor));
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                        /***    CHANGED BY SATISH ON 11-03-15****************/
                        //ADDING EACH CELL WITH THE FIGURES OF THE SURVEY ANSWER OPTIONS SO THAT THEY CAN BE CONVERTED INTO INTEGER
                        //WHERE REQUIRED RATHER THAN BEING LOADED FROM DATA TABLE

                        //worksheet1.Cells[intQrowdata, 1].LoadFromDataTable(dtg13, true);
                        worksheet1.Cells[intQrowdata, 1].Value = "Answer Options";
                        worksheet1.Cells[intQrowdata, 2].Value = "In %";
                        worksheet1.Cells[intQrowdata, 3].Value = "In nos.";
                        for (int irg1 = 0; irg1 < dr13.Length; irg1++)
                        {
                            worksheet1.Cells[intQrowdata + 1 + irg1, 1].Value = dr13[irg1][2];
                            worksheet1.Cells[intQrowdata + 1 + irg1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            worksheet1.Cells[intQrowdata + 1 + irg1, 2].Value = Convert.ToDecimal(dr13[irg1][6]) / 100;
                            worksheet1.Cells[intQrowdata + 1 + irg1, 2].Style.Numberformat.Format = "0%";
                            worksheet1.Cells[intQrowdata + 1 + irg1, 3].Value = Convert.ToInt32(dr13[irg1][5]);
                        }
                        worksheet1.Cells[intQrowdata + dr13.Length + 1, 1].Value = "Total";
                        worksheet1.Cells[intQrowdata + dr13.Length + 1, 2].Value = "";
                        worksheet1.Cells[intQrowdata + dr13.Length + 1, 3].Value = respcount;
                        /***************************************************/


                        worksheet1.Cells[intQrowdata, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet1.Cells[intQrowdata, 2, intQrowdata + dr13.Length + 2, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata + dr13.Length + 2, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata + dr13.Length + 2, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        worksheet1.Cells[intQrowdata + dr13.Length + 2, 1, intQrowdata + dr13.Length + 2, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata + dr13.Length + 2, 1, intQrowdata + dr13.Length + 2, 3].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, 3, intQrowdata + dr13.Length + 2, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 3, intQrowdata + dr13.Length + 2, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                        intQrowdata = intQrowdata + dtg13.Rows.Count + 2;

                        dtg13.Clear();


                    }
                    else if ((dsquestions.Tables[1].Rows[i][3].ToString() == "10") || (dsquestions.Tables[1].Rows[i][3].ToString() == "11"))
                    {

                        DataRow[] dr1011 = dsquestions.Tables[2].Select("seq_num = '" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");

                        System.Data.DataTable mtxcolnames = createMtxColumnDataTable();

                        System.Data.DataTable mtxrownames = createMtxRowDataTable();

                        for (int imtxrow = 0; imtxrow < dr1011.Length; imtxrow++)
                        {

                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][2].ToString()), mtxrownames);
                            }
                            else
                            {
                                DataRow[] dr = mtxrownames.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxrow][2].ToString()), mtxrownames);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr1011.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[0][3].ToString().TrimStart()), mtxcolnames);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()) + "'");
                                if (dr.Length == 0)
                                {
                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr1011[imtxcol][3].ToString().TrimStart()), mtxcolnames);
                                }
                            }
                        }


                        worksheet1.Cells[intQrowdata, 1].Value = dsquestions.Tables[1].Rows[i][8].ToString();
                        intQrowdata = intQrowdata + 1;

                        int colcnt = mtxcolnames.Rows.Count + 1;
                        worksheet1.Column(colcnt + 3).Width = 20;

                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Merge = true;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.WrapText = true;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Font.Bold = true;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Value = "Q" + dsquestions.Tables[1].Rows[i][0].ToString() + ":" + StripTagsRegex(dsquestions.Tables[1].Rows[i][2].ToString()).Trim();
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                        intQrowdata = intQrowdata + 1;

                        DataTable dtg1011 = new DataTable();
                        dtg1011.TableName = "dtQT" + i;
                        DataRow dtgr1011;
                        DataRow dtgrp1011;
                        DataRow dtgresp;

                        dtg1011.Columns.Add("Answer Options");
                        for (int irg = 0; irg < mtxcolnames.Rows.Count; irg++)
                        {
                            dtg1011.Columns.Add(System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[irg][0].ToString()));
                        }

                        //for (int irg1 = 0; irg1 < mtxrownames.Rows.Count; irg1++)
                        //{
                        //    dtgr1011 = dtg1011.NewRow();
                        //    dtgrp1011 = dtg1011.NewRow();
                        //    DataRow[] dr1011f = dsquestions.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");

                        //    dtgr1011[0] = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString());
                        //    dtgrp1011[0] = " ";
                        //    for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                        //    {
                        //        dtgr1011[cgv + 1] = dr1011f[cgv][5].ToString();
                        //        dtgrp1011[cgv + 1] = dr1011f[cgv][6].ToString() + "%";

                        //    }
                        //    dtg1011.Rows.Add(dtgr1011);
                        //    dtg1011.Rows.Add(dtgrp1011);
                        //}

                        //dtgresp = dtg1011.NewRow();
                        //dtgresp[0] = "Total";
                        //dtgresp[mtxcolnames.Rows.Count] = dsquestions.Tables[1].Rows[i][7].ToString();
                        //dtg1011.Rows.Add(dtgresp);


                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(ycolor));
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        /***    CHANGED BY SATISH ON 16-03-15****************/
                        //ADDING EACH CELL WITH THE FIGURES OF THE SURVEY ANSWER OPTIONS SO THAT THEY CAN BE CONVERTED INTO INTEGER
                        //WHERE REQUIRED RATHER THAN BEING LOADED FROM DATA TABLE ONLY LOADING THE HEADERS FROM DATATABLE
                        worksheet1.Cells[intQrowdata, 1].LoadFromDataTable(dtg1011, true);
                        intQrowdata = intQrowdata + 1;
                        for (int irg1 = 0; irg1 < mtxrownames.Rows.Count; irg1++)
                        {
                            dtgr1011 = dtg1011.NewRow();
                            dtgrp1011 = dtg1011.NewRow();
                            DataRow[] dr1011f = dsquestions.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num='" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");

                            worksheet1.Cells[intQrowdata, 1].Value = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg1][0].ToString());
                            worksheet1.Cells[intQrowdata + 1, 1].Value = " ";
                            for (int cgv = 0; cgv < dr1011f.Length; cgv++)
                            {
                                worksheet1.Cells[intQrowdata, 2 + cgv].Value = Convert.ToInt32(dr1011f[cgv][5]);
                                worksheet1.Cells[intQrowdata + 1, 2 + cgv].Value = Convert.ToDecimal(dr1011f[cgv][6]) / 100;
                                worksheet1.Cells[intQrowdata + 1, 2 + cgv].Style.Numberformat.Format = "0%";
                            }
                            intQrowdata = intQrowdata + 2;
                        }

                        worksheet1.Cells[intQrowdata, 1].Value = "Total";
                        worksheet1.Cells[intQrowdata, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet1.Cells[intQrowdata, mtxcolnames.Rows.Count + 1].Value = Convert.ToDecimal(dsquestions.Tables[1].Rows[i][7]);

                        int finalrow = intQrowdata;
                        int finalcol = mtxcolnames.Rows.Count + 1;
                        worksheet1.Cells[intQrowdata - (2 * mtxrownames.Rows.Count), 1, finalrow, finalcol].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet1.Cells[intQrowdata - (2 * mtxrownames.Rows.Count), 1, finalrow, finalcol].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[finalrow, 1, finalrow, finalcol].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[finalrow, 1, finalrow, finalcol].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata - (2 * mtxrownames.Rows.Count) - 2, finalcol, finalrow, finalcol].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata - (2 * mtxrownames.Rows.Count) - 2, finalcol, finalrow, finalcol].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        //int finalrow = intQrowdata + (mtxrownames.Rows.Count * 2) + 1;

                        //worksheet1.Cells[intQrowdata, 1, finalrow, mtxcolnames.Rows.Count + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        //worksheet1.Cells[intQrowdata, 1, finalrow, mtxcolnames.Rows.Count + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        //worksheet1.Cells[intQrowdata, 1, finalrow, mtxcolnames.Rows.Count + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        //worksheet1.Cells[finalrow, 1, finalrow, mtxcolnames.Rows.Count + 1].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        //worksheet1.Cells[finalrow, 1, finalrow, mtxcolnames.Rows.Count + 1].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        //worksheet1.Cells[intQrowdata, mtxcolnames.Rows.Count + 1, finalrow, mtxcolnames.Rows.Count + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        //worksheet1.Cells[intQrowdata, mtxcolnames.Rows.Count + 1, finalrow, mtxcolnames.Rows.Count + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        //intQrowdata = intQrowdata + dtg1011.Rows.Count + 2;
                        intQrowdata = intQrowdata + 2;

                        dtg1011.Clear();

                    }
                    else if (dsquestions.Tables[1].Rows[i][3].ToString() == "12")
                    {
                        DataRow[] dr12 = dsquestions.Tables[2].Select("seq_num = '" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");

                        System.Data.DataTable mtxrownames = createMtxRowDataTable();

                        System.Data.DataTable mtxcolnames = createMtxColumnDataTable();

                        System.Data.DataTable mtxsubcolnames = createMtxColumnDataTable();

                        for (int imtxrow = 0; imtxrow < dr12.Length; imtxrow++)
                        {

                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(dr12[0][2].ToString(), mtxrownames);
                            }
                            else
                            {
                                DataRow[] dr = mtxrownames.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxrow][2].ToString()), mtxrownames);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr12.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(dr12[0][3].ToString(), mtxcolnames);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxcol][3].ToString()), mtxcolnames);

                                }
                            }
                        }

                        for (int imtxsubcol = 0; imtxsubcol < dr12.Length; imtxsubcol++)
                        {

                            if (imtxsubcol == 0)
                            {
                                AddMtxcolsDatatoTable(dr12[0][4].ToString(), mtxsubcolnames);
                            }
                            else
                            {

                                DataRow[] dr = mtxsubcolnames.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()) + "'");
                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr12[imtxsubcol][4].ToString()), mtxsubcolnames);

                                }
                            }
                        }


                        int colcnt12 = mtxsubcolnames.Rows.Count + 1;


                        //    worksheet1.Column(colcnt12 + 3).Width = 20;

                        worksheet1.Cells[intQrowdata, 1].Value = dsquestions.Tables[1].Rows[i][8].ToString();
                        intQrowdata = intQrowdata + 1;


                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Merge = true;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Style.WrapText = true;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Style.Font.Bold = true;

                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Value = "Q" + dsquestions.Tables[1].Rows[i][0].ToString() + ":" + StripTagsRegex(dsquestions.Tables[1].Rows[i][2].ToString()).Trim();

                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt12].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                        intQrowdata = intQrowdata + 1;

                        DataTable dtg12 = new DataTable();
                        dtg12.TableName = "dtQT" + i;
                        DataRow dtgr12;
                        DataRow dtgr12data;
                        DataRow dtgr12pcntg;
                        DataRow dtgresp;
                        dtg12.Columns.Add(" ");
                        int cval = 0;
                        StringBuilder ColumnName = new StringBuilder(" ");

                        for (int irg12 = 0; irg12 < mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count; irg12++)
                        {
                            if ((irg12 == 0) || (irg12 == mtxsubcolnames.Rows.Count))
                            {
                                dtg12.Columns.Add(System.Web.HttpUtility.HtmlDecode(mtxcolnames.Rows[cval][0].ToString()));
                                cval = cval + 1;
                            }
                            else
                            {

                                try
                                {
                                    dtg12.Columns.Add(System.Web.HttpUtility.HtmlDecode(ColumnName.ToString()));
                                }
                                catch (Exception ex)
                                {
                                    ColumnName.Append(" ");
                                    dtg12.Columns.Add(System.Web.HttpUtility.HtmlDecode(ColumnName.ToString()));
                                }

                            }

                        }

                        dtgr12 = dtg12.NewRow();
                        for (int irg112 = 0; irg112 < mtxrownames.Rows.Count; irg112++)
                        {

                            DataRow[] dr12sub = dsquestions.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg112][0].ToString()).Replace("'", "''") + "' and seq_num = '" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");
                            dtgr12[0] = "Answer Options";
                            for (int cgv12 = 0; cgv12 < dr12sub.Length; cgv12++)
                            {
                                dtgr12[cgv12 + 1] = System.Web.HttpUtility.HtmlDecode(dr12sub[cgv12][4].ToString());
                            }

                        }
                        dtg12.Rows.Add(dtgr12);


                        /***    CHANGED BY SATISH ON 11-03-15. 
                         *      WE ONLY NEED THE TABLE HEADERS FOR THAT QUESTION TYPE AND NOT DATA
                         *      ********/
                        //for (int irg12data = 0; irg12data < mtxrownames.Rows.Count; irg12data++)
                        //{
                        //    dtgr12data = dtg12.NewRow();
                        //    dtgr12pcntg = dtg12.NewRow();
                        //    DataRow[] dr12f = dsquestions.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg12data][0].ToString()).Replace("'", "''") + "' and seq_num = '" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");

                        //    dtgr12data[0] = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg12data][0].ToString());
                        //    dtgr12pcntg[0] = " ";
                        //    for (int cgv12 = 0; cgv12 < dr12f.Length; cgv12++)
                        //    {
                        //        dtgr12data[cgv12 + 1] = dr12f[cgv12][5].ToString();
                        //        dtgr12pcntg[cgv12 + 1] = dr12f[cgv12][6].ToString() + "%";

                        //    }

                        //    dtg12.Rows.Add(dtgr12data);
                        //    dtg12.Rows.Add(dtgr12pcntg);
                        //}
                        /***************************************************************/

                        dtgresp = dtg12.NewRow();
                        dtgresp[0] = "Total";
                        dtgresp[mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count] = dsquestions.Tables[1].Rows[i][7].ToString();
                        dtg12.Rows.Add(dtgresp);

                        worksheet1.Cells[intQrowdata, 1, intQrowdata + 1, mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata + 1, mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(ycolor));
                        worksheet1.Cells[intQrowdata, 1, intQrowdata + 1, mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata + 1, mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                        /***    CHANGED BY SATISH ON 11-03-15****************/
                        //ADDING EACH CELL WITH THE FIGURES OF THE SURVEY ANSWER OPTIONS SO THAT THEY CAN BE CONVERTED INTO INTEGER
                        //WHERE REQUIRED RATHER THAN BEING LOADED FROM DATA TABLE ONLY LOADING THE HEADERS FROM DATATABLE
                        worksheet1.Cells[intQrowdata, 1].LoadFromDataTable(dtg12, true);
                        intQrowdata = intQrowdata + 2;
                        for (int irg12data = 0; irg12data < mtxrownames.Rows.Count; irg12data++)
                        {
                            DataRow[] dr12f = dsquestions.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg12data][0].ToString()).Replace("'", "''") + "' and seq_num = '" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");

                            worksheet1.Cells[intQrowdata, 1].Value = System.Web.HttpUtility.HtmlDecode(mtxrownames.Rows[irg12data][0].ToString());
                            worksheet1.Cells[intQrowdata, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            worksheet1.Cells[intQrowdata + 1, 1].Value = "";
                            for (int cgv12 = 0; cgv12 < dr12f.Length; cgv12++)
                            {
                                worksheet1.Cells[intQrowdata, cgv12 + 2].Value = Convert.ToInt32(dr12f[cgv12][5].ToString());
                                worksheet1.Cells[intQrowdata + 1, cgv12 + 2].Value = Convert.ToDecimal(dr12f[cgv12][6].ToString()) / 100;
                                worksheet1.Cells[intQrowdata + 1, cgv12 + 2].Style.Numberformat.Format = "0%";
                            }
                            intQrowdata = intQrowdata + 2;
                        }
                        worksheet1.Cells[intQrowdata, 1].Value = "Total";
                        worksheet1.Cells[intQrowdata, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet1.Cells[intQrowdata, mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count + 1].Value = Convert.ToDecimal(dsquestions.Tables[1].Rows[i][7]);

                        int finalrow12 = intQrowdata;
                        int finalcol12 = mtxcolnames.Rows.Count * mtxsubcolnames.Rows.Count + 1;

                        worksheet1.Cells[intQrowdata - (2 * mtxrownames.Rows.Count), 1, finalrow12, finalcol12].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet1.Cells[intQrowdata - (2 * mtxrownames.Rows.Count), 1, finalrow12, finalcol12].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[finalrow12, 1, finalrow12, finalcol12].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[finalrow12, 1, finalrow12, finalcol12].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata - (2 * mtxrownames.Rows.Count) - 2, finalcol12, finalrow12, finalcol12].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata - (2 * mtxrownames.Rows.Count) - 2, finalcol12, finalrow12, finalcol12].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        intQrowdata = intQrowdata + 2;
                        dtg12.Clear();
                    }
                    else if (dsquestions.Tables[1].Rows[i][3].ToString() == "15")
                    {

                        DataRow[] dr15 = dsquestions.Tables[2].Select("seq_num = '" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");

                        System.Data.DataTable mtxcolnames15 = createMtxColumnDataTable();

                        System.Data.DataTable mtxrownames15 = createMtxRowDataTable();

                        for (int imtxrow = 0; imtxrow < dr15.Length; imtxrow++)
                        {

                            if (imtxrow == 0)
                            {
                                AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][2].ToString()), mtxrownames15);
                            }
                            else
                            {

                                DataRow[] dr = mtxrownames15.Select("rowname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()) + "'");


                                if (dr.Length == 0)
                                {

                                    AddMtxrowsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxrow][2].ToString()), mtxrownames15);

                                }
                            }
                        }

                        for (int imtxcol = 0; imtxcol < dr15.Length; imtxcol++)
                        {

                            if (imtxcol == 0)
                            {
                                AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[0][3].ToString().TrimStart()), mtxcolnames15);
                            }
                            else
                            {
                                DataRow[] dr = mtxcolnames15.Select("columnname = '" + System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()) + "'");


                                if (dr.Length == 0)
                                {

                                    AddMtxcolsDatatoTable(System.Web.HttpUtility.HtmlEncode(dr15[imtxcol][3].ToString().TrimStart()), mtxcolnames15);

                                }
                            }
                        }

                        int colcnt15 = mtxcolnames15.Rows.Count + 1;
                        // worksheet1.Column(colcnt15 + 3).Width = 20;

                        worksheet1.Cells[intQrowdata, 1].Value = dsquestions.Tables[1].Rows[i][8].ToString();
                        intQrowdata = intQrowdata + 1;


                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Merge = true;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.WrapText = true;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.Font.Bold = true;

                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Value = "Q" + dsquestions.Tables[1].Rows[i][0].ToString() + ":" + StripTagsRegex(dsquestions.Tables[1].Rows[i][2].ToString()).Trim();

                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata, colcnt15, intQrowdata, colcnt15].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata, colcnt15, intQrowdata, colcnt15].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        intQrowdata = intQrowdata + 1;

                        DataTable dtg15 = new DataTable();
                        dtg15.TableName = "dtQT" + i;
                        DataRow dtgr15;
                        DataRow dtgr115;
                        DataRow dtgresp;

                        dtg15.Columns.Add("Answer Options");
                        for (int irg = 0; irg < mtxcolnames15.Rows.Count; irg++)
                        {

                            dtg15.Columns.Add(System.Web.HttpUtility.HtmlDecode(mtxcolnames15.Rows[irg][0].ToString()));

                        }

                        /***    CHANGED BY SATISH ON 16-03-15. 
                         *      WE ONLY NEED THE TABLE HEADERS FOR THAT QUESTION TYPE AND NOT DATA
                         *      ********/
                        //for (int irg1 = 0; irg1 < mtxrownames15.Rows.Count; irg1++)
                        //{
                        //    dtgr15 = dtg15.NewRow();
                        //    dtgr115 = dtg15.NewRow();
                        //    DataRow[] dr15f = dsquestions.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames15.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num = '" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");


                        //    dtgr15[0] = System.Web.HttpUtility.HtmlDecode(mtxrownames15.Rows[irg1][0].ToString());
                        //    dtgr115[0] = " ";
                        //    for (int cgv = 0; cgv < dr15f.Length; cgv++)
                        //    {
                        //        dtgr15[cgv + 1] = dr15f[cgv][5].ToString();
                        //        dtgr115[cgv + 1] = dr15f[cgv][6].ToString() + "%";

                        //    }

                        //    dtg15.Rows.Add(dtgr15);
                        //    dtg15.Rows.Add(dtgr115);

                        //}

                        //dtgresp = dtg15.NewRow();
                        //dtgresp[0] = "Total";
                        //dtgresp[mtxcolnames15.Rows.Count] = dsquestions.Tables[1].Rows[i][7].ToString();
                        //dtg15.Rows.Add(dtgresp);


                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(ycolor));
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet1.Cells[intQrowdata, 1, intQrowdata, colcnt15].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        /***    CHANGED BY SATISH ON 11-03-15****************/
                        //ADDING EACH CELL WITH THE FIGURES OF THE SURVEY ANSWER OPTIONS SO THAT THEY CAN BE CONVERTED INTO INTEGER
                        //WHERE REQUIRED RATHER THAN BEING LOADED FROM DATA TABLE ONLY LOADING THE TABLE HEADERS
                        worksheet1.Cells[intQrowdata, 1].LoadFromDataTable(dtg15, true);
                        intQrowdata = intQrowdata + 1;
                        for (int irg1 = 0; irg1 < mtxrownames15.Rows.Count; irg1++)
                        {
                            DataRow[] dr15f = dsquestions.Tables[2].Select("row_name = '" + System.Web.HttpUtility.HtmlDecode(mtxrownames15.Rows[irg1][0].ToString()).Replace("'", "''") + "' and seq_num = '" + dsquestions.Tables[1].Rows[i][0].ToString() + "'");
                            worksheet1.Cells[intQrowdata, 1].Value = System.Web.HttpUtility.HtmlDecode(mtxrownames15.Rows[irg1][0].ToString());
                            worksheet1.Cells[intQrowdata + 1, 1].Value = " ";
                            for (int cgv = 0; cgv < dr15f.Length; cgv++)
                            {
                                worksheet1.Cells[intQrowdata, 2 + cgv].Value = Convert.ToInt32(dr15f[cgv][5]);
                                worksheet1.Cells[intQrowdata + 1, 2 + cgv].Value = Convert.ToDecimal(dr15f[cgv][6]) / 100;
                                worksheet1.Cells[intQrowdata + 1, 2 + cgv].Style.Numberformat.Format = "0%";
                            }
                            intQrowdata = intQrowdata + 2;
                        }
                        worksheet1.Cells[intQrowdata, 1].Value = "Total";
                        worksheet1.Cells[intQrowdata, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet1.Cells[intQrowdata, mtxcolnames15.Rows.Count + 1].Value = Convert.ToDecimal(dsquestions.Tables[1].Rows[i][7]);

                        int finalrow15 = intQrowdata;
                        int finalcol15 = mtxcolnames15.Rows.Count + 1;

                        worksheet1.Cells[intQrowdata - (2 * mtxrownames15.Rows.Count), 1, finalrow15, finalcol15].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet1.Cells[intQrowdata - (2 * mtxrownames15.Rows.Count), 1, finalrow15, finalcol15].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[finalrow15, 1, finalrow15, finalcol15].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[finalrow15, 1, finalrow15, finalcol15].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        worksheet1.Cells[intQrowdata - (2 * mtxrownames15.Rows.Count) - 1, finalcol15, finalrow15, finalcol15].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet1.Cells[intQrowdata - (2 * mtxrownames15.Rows.Count) - 1, finalcol15, finalrow15, finalcol15].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        //worksheet1.Cells[intQrowdata, 1, finalrow15, finalcol15].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        //worksheet1.Cells[intQrowdata, 1, finalrow15, finalcol15].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        //worksheet1.Cells[intQrowdata, 1, finalrow15, finalcol15].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                        //worksheet1.Cells[finalrow15, 1, finalrow15, finalcol15].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        //worksheet1.Cells[finalrow15, 1, finalrow15, finalcol15].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        //worksheet1.Cells[intQrowdata, finalcol15, finalrow15, finalcol15].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        //worksheet1.Cells[intQrowdata, finalcol15, finalrow15, finalcol15].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);



                        intQrowdata = intQrowdata + dtg15.Rows.Count + 2;

                        dtg15.Clear();


                    }
                    else if ((dsquestions.Tables[1].Rows[i][3].ToString() == "5") || (dsquestions.Tables[1].Rows[i][3].ToString() == "6") || (dsquestions.Tables[1].Rows[i][3].ToString() == "7") || (dsquestions.Tables[1].Rows[i][3].ToString() == "8") || (dsquestions.Tables[1].Rows[i][3].ToString() == "14"))
                    {
                        int intQTextrowdata = 1;
                        //ExcelWorksheet worksheetTR;
                        // worksheetTR = xlPackage.Workbook.Worksheets["TR" + TRid];
                        //if (worksheetTR == null)
                        //{
                        //     worksheetTR = xlPackage.Workbook.Worksheets.Add("TR" + TRid);
                        //}

                        ExcelWorksheet worksheetTR = xlPackage.Workbook.Worksheets.Add("TR-" + TRid);
                        worksheetTR.Cells[1, 1, 800, 800].Clear();

                        worksheetTR.Column(1).Width = 10;
                        worksheetTR.Column(2).Width = 70;

                        DataSet dsmatrxtxt = surcore.getMatrixSSMS(Convert.ToInt32(dsquestions.Tables[1].Rows[i][1].ToString()), surveyID);
                        DataTable dtmatrxtxt = dsmatrxtxt.Tables[3];


                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Merge = true;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.WrapText = true;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.Font.Bold = true;

                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Value = SelectedSurvey.SURVEY_NAME.ToString();
                        intQTextrowdata = intQTextrowdata + 2;


                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Merge = true;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.WrapText = true;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.Font.Bold = true;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Value = "You have " + dsmatrxtxt.Tables[2].Rows[0][0].ToString() + " responses for this question as listed below:";
                        intQTextrowdata = intQTextrowdata + 2;


                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Merge = true;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.WrapText = true;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Style.Font.Bold = true;
                        worksheetTR.Cells[intQTextrowdata, 1, intQTextrowdata, 2].Value = "Q" + dsquestions.Tables[1].Rows[i][0].ToString() + ":" + StripTagsRegex(dsquestions.Tables[1].Rows[i][2].ToString()).Trim();
                        intQTextrowdata = intQTextrowdata + 1;

                        for (int txtrow = 0; txtrow < dsmatrxtxt.Tables[4].Rows.Count; txtrow++)
                        {
                            worksheetTR.Cells[intQTextrowdata, 1].Value = txtrow + 1;

                            DataRow[] drmtxtext = dtmatrxtxt.Select("respondentid='" + dsmatrxtxt.Tables[4].Rows[txtrow][0] + "'");

                            if (drmtxtext.Length > 0)
                            {
                                worksheetTR.Cells[intQTextrowdata, 2].Value = drmtxtext[0].ItemArray[0].ToString();
                                worksheetTR.Cells[intQTextrowdata, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                intQTextrowdata = intQTextrowdata + 1;

                            }
                            else
                            {
                                worksheetTR.Cells[intQTextrowdata, 2].Value = "";
                                worksheetTR.Cells[intQTextrowdata, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                intQTextrowdata = intQTextrowdata + 1;
                            }


                        }

                        TRid = TRid + 1;
                        intQTextrowdata = intQTextrowdata + 1;
                    }
                    else if (dsquestions.Tables[1].Rows[i][3].ToString() == "9")
                    {
                        int intQTextrowdata9 = 1;

                        ExcelWorksheet worksheetTR9 = xlPackage.Workbook.Worksheets.Add("TR" + TRid);

                        worksheetTR9.Cells[1, 1, 800, 800].Clear();


                        DataSet dsmatrxtxt = surcore.getMatrixSSMS(Convert.ToInt32(dsquestions.Tables[1].Rows[i][1].ToString()), surveyID);
                        DataTable dtmatrxtxt = dsmatrxtxt.Tables[3];

                        int colcnt9 = dsmatrxtxt.Tables[0].Rows.Count + 1;

                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Merge = true;
                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Style.WrapText = true;
                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Style.Font.Bold = true;

                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Value = SelectedSurvey.SURVEY_NAME.ToString();
                        intQTextrowdata9 = intQTextrowdata9 + 2;

                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Merge = true;
                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Value = "You have " + dsmatrxtxt.Tables[2].Rows[0][0].ToString() + " responses for this question as listed below:";
                        intQTextrowdata9 = intQTextrowdata9 + 2;

                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Merge = true;
                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Value = "Q" + dsquestions.Tables[1].Rows[i][0].ToString() + ":" + StripTagsRegex(dsquestions.Tables[1].Rows[i][2].ToString()).Trim();
                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheetTR9.Cells[intQTextrowdata9, 1, intQTextrowdata9, colcnt9].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));

                        intQTextrowdata9 = intQTextrowdata9 + 2;

                        int txtcoldata = 2;
                        for (int txtcol = 0; txtcol < dsmatrxtxt.Tables[0].Rows.Count; txtcol++)
                        {
                            worksheetTR9.Cells[intQTextrowdata9, txtcoldata].Value = dsmatrxtxt.Tables[0].Rows[txtcol][3].ToString();
                            worksheetTR9.Cells[intQTextrowdata9, txtcoldata].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheetTR9.Cells[intQTextrowdata9, txtcoldata].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(ycolor));
                            worksheetTR9.Column(txtcoldata).Width = 10;
                            txtcoldata = txtcoldata + 1;
                        }
                        intQTextrowdata9 = intQTextrowdata9 + 1;

                        for (int txtrow = 0; txtrow < dsmatrxtxt.Tables[4].Rows.Count; txtrow++)
                        {
                            worksheetTR9.Cells[intQTextrowdata9, 1].Value = txtrow + 1;
                            int txtcoldata1 = 2;

                            DataRow[] drmtxtext = dtmatrxtxt.Select("respondentid='" + dsmatrxtxt.Tables[4].Rows[txtrow][0] + "'");

                            if (drmtxtext.Length > 0)
                            {
                                if (drmtxtext.Length < dsmatrxtxt.Tables[0].Rows.Count)
                                {
                                    for (int txtcol = 0; txtcol < drmtxtext.Length; txtcol++)
                                    {
                                        worksheetTR9.Cells[intQTextrowdata9, txtcoldata1].Value = drmtxtext[txtcol].ItemArray[0].ToString();
                                        worksheetTR9.Cells[intQTextrowdata9, txtcoldata1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                        txtcoldata1 = txtcoldata1 + 1;
                                    }
                                }
                                else
                                {
                                    for (int txtcol = 0; txtcol < dsmatrxtxt.Tables[0].Rows.Count; txtcol++)
                                    {
                                        worksheetTR9.Cells[intQTextrowdata9, txtcoldata1].Value = drmtxtext[txtcol].ItemArray[0].ToString();
                                        worksheetTR9.Cells[intQTextrowdata9, txtcoldata1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                        txtcoldata1 = txtcoldata1 + 1;
                                    }
                                }
                            }
                            else
                            {
                                for (int txtcol = 0; txtcol < dsmatrxtxt.Tables[0].Rows.Count; txtcol++)
                                {
                                    worksheetTR9.Cells[intQTextrowdata9, 2].Value = "";
                                    worksheetTR9.Cells[intQTextrowdata9, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    txtcoldata1 = txtcoldata1 + 1;
                                }
                            }


                            intQTextrowdata9 = intQTextrowdata9 + 1;
                        }

                        TRid = TRid + 1;
                        intQTextrowdata9 = intQTextrowdata9 + 1;


                    }
                    else if (dsquestions.Tables[1].Rows[i][3].ToString() == "20")
                    {
                        int intQTextrowdata20 = 1;

                        ExcelWorksheet worksheetTR20 = xlPackage.Workbook.Worksheets.Add("TR" + TRid);
                        worksheetTR20.Cells[1, 1, 800, 800].Clear();

                        DataSet dsmatrxtxt = surcore.getMatrixSSMS(Convert.ToInt32(dsquestions.Tables[1].Rows[i][1].ToString()), surveyID);

                        int colcnt20 = dsmatrxtxt.Tables[0].Rows.Count + 1;

                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Merge = true;
                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Style.WrapText = true;
                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Style.Font.Bold = true;

                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Value = SelectedSurvey.SURVEY_NAME.ToString();
                        intQTextrowdata20 = intQTextrowdata20 + 2;

                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Merge = true;
                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Value = "You have " + dsmatrxtxt.Tables[2].Rows[0][0].ToString() + " responses for this question as listed below:";
                        intQTextrowdata20 = intQTextrowdata20 + 2;

                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Merge = true;
                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheetTR20.Cells[intQTextrowdata20, 1, intQTextrowdata20, colcnt20].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                        worksheetTR20.Cells[intQTextrowdata20, 1].Value = "Q" + dsquestions.Tables[1].Rows[i][0].ToString() + ":" + StripTagsRegex(dsquestions.Tables[1].Rows[i][2].ToString()).Trim();
                        intQTextrowdata20 = intQTextrowdata20 + 2;

                        int txtcoldata = 2;
                        for (int txtcol = 0; txtcol < dsmatrxtxt.Tables[0].Rows.Count; txtcol++)
                        {
                            worksheetTR20.Cells[intQTextrowdata20, txtcoldata].Value = dsmatrxtxt.Tables[0].Rows[txtcol][3].ToString();
                            worksheetTR20.Cells[intQTextrowdata20, txtcoldata].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheetTR20.Cells[intQTextrowdata20, txtcoldata].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(ycolor));
                            worksheetTR20.Column(txtcoldata).Width = 10;
                            txtcoldata = txtcoldata + 1;
                        }
                        intQTextrowdata20 = intQTextrowdata20 + 1;

                        for (int txtrow = 0; txtrow < dsmatrxtxt.Tables[4].Rows.Count; txtrow++)
                        {
                            worksheetTR20.Cells[intQTextrowdata20, 1].Value = txtrow + 1;
                            int txtcoldata1 = 2;

                            DataSet dscontinfo = surcore.getRawdataexportcontactinfo(Convert.ToInt32(dsquestions.Tables[1].Rows[i][1].ToString()), Convert.ToInt32(dsmatrxtxt.Tables[4].Rows[txtrow][0].ToString()));

                            for (int continfo = 0; continfo < dscontinfo.Tables[0].Rows.Count; continfo++)
                            {
                                worksheetTR20.Cells[intQTextrowdata20, txtcoldata1].Value = dscontinfo.Tables[0].Rows[continfo][1].ToString();
                                worksheetTR20.Cells[intQTextrowdata20, txtcoldata1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                txtcoldata1 = txtcoldata1 + 1;
                            }

                            intQTextrowdata20 = intQTextrowdata20 + 1;
                        }

                        TRid = TRid + 1;
                        intQTextrowdata20 = intQTextrowdata20 + 1;


                    }
                }

                xlPackage.Save();

                if (File.Exists(strXLFileName))
                {

                    string name_survey = SelectedSurvey.SURVEY_NAME.Replace(" ", "");
                    name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                    name_survey = name_survey.Replace("/", "");
                    Response.ContentType = "application/ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + "_Report" + ".xlsx");
                    Response.TransmitFile(strXLFileName);
                    Response.End();

                }
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            //set workbook object to null
            //if (workBook != null)
            //    workBook = null;
        }
    }


    private void CreateExportFile(int rep_type, string exp_type)
    {
        

        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (exp_type != "" && SelectedSurvey != null && SurveyBasicInfoView.SURVEY_ID > 0 && userDetails != null && userDetails.UserId > 0)
        {
            int exp_id;
            DataSet dssurcorevoice2 = surcore.voicedataset();
            if (dssurcorevoice2.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
            {
                exp_id = surcorevoice.InsertExportInfo(SurveyBasicInfoView.SURVEY_ID, rep_type, exp_type, userDetails.UserId);
            }
            else
            {
                exp_id = surcore.InsertExportInfo(SurveyBasicInfoView.SURVEY_ID, rep_type, exp_type, userDetails.UserId);
            }



            if (exp_id > 0)
            {
                DataSet ds = new DataSet();
                int flag = 0;
             
                for (int i = 0; i < 6; i++)
                {
                   System.Threading.Thread.Sleep(5000 * 10);
                    DataSet dssurcorevoice3 = surcore.voicedataset();
                    if (dssurcorevoice3.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
                    {
                        ds = surcorevoice.GetExportInfo(exp_id);
                    }
                    else
                    {
                        ds = surcore.GetExportInfo(exp_id);
                    }
                     if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        
                        if ((Convert.ToInt32(ds.Tables[0].Rows[0]["REPORT_STATUS"]) == 2) || (Convert.ToInt32(ds.Tables[0].Rows[0]["REPORT_STATUS"]) == 3))
                        {
                            string strDirector = ConfigurationManager.AppSettings["RootPath"].ToString();
                            if (!Directory.Exists(strDirector + SelectedSurvey.USER_ID + @"\" + SelectedSurvey.SURVEY_ID))
                            {
                                Directory.CreateDirectory(strDirector + SelectedSurvey.USER_ID + @"\" + SelectedSurvey.SURVEY_ID);
                            }

                            string strXLFileName = strDirector + SelectedSurvey.USER_ID + "\\" + SelectedSurvey.SURVEY_ID + "\\" + Convert.ToString(ds.Tables[0].Rows[0]["REPORT_FILENAME"]);
                            string ext = "";
                            if (rep_type == 0)
                                ext = ".ppt";
                            else if (rep_type == 2 || rep_type == 4)
                                ext = ".xls";
                            else if (rep_type == 3)
                                ext = ".doc";
                            if (ext.Length > 0)
                            {


                                SaveDialogBox(strXLFileName, SelectedSurvey.SURVEY_NAME, ext);
                                flag = 1;
                            }
                            break;

                        }

                    }


                }
               

            }


        }


    }

    public DataSet GetDataReport(SurveyQuestion ques)
    {
        int qid = Convert.ToInt32(ques.QUESTION_ID);
        DataSet ds = new DataSet();
        System.Data.DataTable tab = new System.Data.DataTable();
        ds.Tables.Add(tab);
        int i = 0;
        if (ques.surveyAnswers != null && ques.surveyAnswers.Count > 0)
        {
            if (!(ques.QUESTION_TYPE == 10 || ques.QUESTION_TYPE == 11 || ques.QUESTION_TYPE == 12 || ques.QUESTION_TYPE == 13))
            {
                int ansoptcnt = 0;
                int YesResponsecount = 0, NoresponseCount = 0;
                int TotalResponsCount = 0;
                if (ques.QUESTION_TYPE == 2)
                {
                    TotalResponsCount += ques.RESPONSE_COUNT;
                }
                else
                {
                    foreach (SurveyAnswers answ in ques.surveyAnswers)
                    {
                        TotalResponsCount += answ.RESPONSE_COUNT;
                    }
                }
                if (ques.QUESTION_TYPE == 1 || ques.QUESTION_TYPE == 3 || ques.QUESTION_TYPE == 4)
                {
                    TotalResponsCount += ques.OTHER_RESPCOUNT;
                }
                ds.Tables[0].Columns.Add("AnswerOptions");
                ds.Tables[0].Columns.Add("ResponseCount");
                ds.Tables[0].Columns.Add("ResponsePercent");
                foreach (SurveyAnswers answe in ques.surveyAnswers)
                {
                    ds.Tables[0].Rows.Add();
                    string str1 = Utilities.ClearHTML(Convert.ToString(answe.ANSWER_OPTIONS));
                    str1 = str1.Replace("\r", " ");
                    str1 = str1.Replace("\n", " ");
                    ds.Tables[0].Rows[i]["AnswerOptions"] = str1;
                    ds.Tables[0].Rows[i]["ResponseCount"] = Convert.ToString(answe.RESPONSE_COUNT);
                    if (!(TotalResponsCount == 0))
                    {
                        Double z = answe.RESPONSE_COUNT * 100;
                        ds.Tables[0].Rows[i]["ResponsePercent"] = Math.Round(z / TotalResponsCount, 2) + " %";
                    }
                    else
                    {
                        ds.Tables[0].Rows[i]["ResponsePercent"] = Convert.ToString(0) + " %";

                    }

                    i++;
                }
                if (ques.OTHER_ANS == 1)
                {
                    if (ques.QUESTION_TYPE == 1 || ques.QUESTION_TYPE == 2 || ques.QUESTION_TYPE == 3 || ques.QUESTION_TYPE == 4)
                    {
                        ds.Tables[0].Rows.Add();
                        ds.Tables[0].Rows[i]["AnswerOptions"] = "Others";
                        ds.Tables[0].Rows[i]["ResponseCount"] = Convert.ToString(ques.OTHER_RESPCOUNT);
                        if (!(TotalResponsCount == 0))
                        {
                            Double z = Convert.ToDouble(Convert.ToInt32(ques.OTHER_RESPCOUNT) * 100);
                            ds.Tables[0].Rows[i]["ResponsePercent"] = Math.Round(z / TotalResponsCount, 2) + " %";
                        }
                        else
                        {
                            ds.Tables[0].Rows[i]["ResponsePercent"] = Convert.ToString(0) + " %";

                        }
                    }
                }
            }
            else if (ques.QUESTION_TYPE == 13)
            {

                int ansoptcnt = 0;
                int YesResponsecount = 0, NoresponseCount = 0;
                int TotalResponsCount = 0;
                ArrayList ans_options = new ArrayList();
                ArrayList ans_option_sum = new ArrayList();
                foreach (SurveyAnswers ans in ques.surveyAnswers)
                {
                    TotalResponsCount += ans.RESPONSE_COUNT;
                    ans_options.Add(ans.ANSWER_ID);
                }
                DataSet ds_constantsum = new DataSet();

                DataSet dssurcorevoice4 = surcore.voicedataset();
                if (dssurcorevoice4.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
                {
                    ds_constantsum = surcorevoice.GetConstantSumForReports(ques.QUESTION_ID, ans_options, dt1, dt2);
                }
                else
                {
                    ds_constantsum = surcore.GetConstantSumForReports(ques.QUESTION_ID, ans_options, dt1, dt2);
                }
                ds.Tables[0].Columns.Add("AnswerOptions");
                ds.Tables[0].Columns.Add("ResponseCount");
                ds.Tables[0].Columns.Add("ResponsePercent");
                if (ds_constantsum != null && ds_constantsum.Tables.Count > 0)
                {
                    double ResPercentdividedby = 0;
                    string RespondentIDS;
                    DataSet dssurcorevoice5 = surcore.voicedataset();
                    if (dssurcorevoice5.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
                    {
                        RespondentIDS = surcorevoice.GetRespondentsForReports(ques.QUESTION_ID);
                    }
                    else
                    {
                        RespondentIDS = surcore.GetRespondentsForReports(ques.QUESTION_ID);
                    }
                    string[] arr = RespondentIDS.Split(',');
                    double z = Convert.ToDouble(ques.CHAR_LIMIT);
                    if (z == 0.0)
                    {
                        z = 100;

                    }

                    ResPercentdividedby = Math.Round(Convert.ToDouble(z) / 100, 2);
                    double totalsum = 0;
                    int y = 0;
                    foreach (SurveyAnswers ans in ques.surveyAnswers)
                    {
                        int temp_total = 0;
                        ds.Tables[0].Rows.Add();
                        string str1 = Utilities.ClearHTML(Convert.ToString(ans.ANSWER_OPTIONS));
                        str1 = str1.Replace("\r", " ");
                        str1 = str1.Replace("\n", " ");
                        ds.Tables[0].Rows[i]["AnswerOptions"] = str1;

                        for (int m = 0; m < ds_constantsum.Tables[0].Rows.Count; m++)
                        {

                            if (Convert.ToString(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) != null && ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"] != null && Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_ID"]) == ans.ANSWER_ID)
                            {
                                temp_total += Convert.ToInt32(ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"] == "" ? 0 : ds_constantsum.Tables[0].Rows[m]["ANSWER_TEXT"]);

                            }
                        }
                        double avg = 0;
                        avg = Math.Round(Convert.ToDouble(temp_total) / (arr.Length), 2);
                        ds.Tables[0].Rows[i]["ResponseCount"] = Convert.ToString(avg);
                        Double ResPercent = 0;
                        if (Convert.ToInt32(temp_total) > 0 && ResPercentdividedby > 0 && arr.Length != null && arr.Length > 0)
                            ResPercent = (temp_total) / (ResPercentdividedby * arr.Length);
                        ds.Tables[0].Rows[i]["ResponsePercent"] = Math.Round(ResPercent, 2) + " %";
                        y = y + 1;
                        i++;
                    }

                }

            }
            
         
            else
            {

                ds.Clear();
                ds.Tables[0].Columns.Add("ColumnOptions");
                ds.Tables[0].Columns.Add("RowOptions");
                ds.Tables[0].Columns.Add("ResponseCount");
                ds.Tables[0].Columns.Add("ResponsePercent");
                ds.Tables[0].Columns.Add("Rowcount");
                ds.Tables[0].Columns.Add("ColCount");
                ds.Tables[0].Columns.Add("DynamicPercentCnt");
                ds.Tables[0].Columns.Add("ColumnSubOptions");
                ds.Tables[0].Columns.Add("ColumnSubCount");
                ArrayList colHead = new ArrayList();
                ArrayList ColSubHead = new ArrayList();
                ArrayList rowHead = new ArrayList();
                Hashtable headerList = BuildQuetions.GetAnsItemsList(ques);
                if (headerList.Contains("ColHead"))
                    colHead = (ArrayList)headerList["ColHead"];
                //if (headerList.Contains("ColSubHead"))
                //    ColSubHead = (ArrayList)headerList["ColSubHead"];
                if (headerList.Contains("RowHead"))
                    rowHead = (ArrayList)headerList["RowHead"];
                ArrayList column_values = new ArrayList();
                // int m = 0, b = 0;
                int col = 0, row = 0, totalResponse = 0;
                //if(ques.QUESTION_TYPE ==10 || ques.QUESTION_TYPE ==11)
                //{
                foreach (SurveyAnswers ans in ques.surveyAnswers)
                {
                    
                    string optionValue = string.Format("{0}$--${1}", colHead[col], rowHead[row]);
                    var anserOption = ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Trim() == optionValue.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Trim()).FirstOrDefault();
                    totalResponse += anserOption.RESPONSE_COUNT;
                    col++;
                    if (col == colHead.Count)
                    {
                        column_values.Add(totalResponse);
                        row++;
                        col = 0;
                        totalResponse = 0;
                    }
                }
                int row_count = 0;
                //, g = 0;
                //int l = 0;
                col = 0;
                row = 0;
                foreach (SurveyAnswers answe in ques.surveyAnswers)
                {
                    string optionValue = string.Format("{0}$--${1}", colHead[col], rowHead[row]);
                    var anserOption = ques.surveyAnswers.Where(ao => ao.ANSWER_OPTIONS.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Trim() == optionValue.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Trim()).FirstOrDefault();
                    ds.Tables[0].Rows.Add();
                    string str2 = "";
                    if (colHead.Count > col)
                    {
                        str2 = Utilities.ClearHTML(Convert.ToString(colHead[col]));
                        str2 = str2.Replace("\r", " ");
                        str2 = str2.Replace("\n", " ");
                        ds.Tables[0].Rows[i]["ColumnOptions"] = str2;
                        //ds.Tables[0].Rows[i]["ColumnOptions"] = Utilities.ClearHTML(Convert.ToString(colHead[col]));

                    }
                    if (rowHead.Count > row)
                    {
                        str2 = Utilities.ClearHTML(Convert.ToString(rowHead[row]));
                        str2 = str2.Replace("\r", " ");
                        str2 = str2.Replace("\n", " ");
                        ds.Tables[0].Rows[i]["RowOptions"] = str2;

                    }
                 
                    ds.Tables[0].Rows[i]["ResponseCount"] = Convert.ToString(anserOption.RESPONSE_COUNT);
                    if (column_values[row_count] != null && Convert.ToInt32(column_values[row_count]) > 0)
                        ds.Tables[0].Rows[i]["ResponsePercent"] = Math.Round(Convert.ToDouble(anserOption.RESPONSE_COUNT * 100) / Convert.ToInt32(column_values[row_count]), 2);
                    else
                        ds.Tables[0].Rows[i]["ResponsePercent"] = Convert.ToString(0) + " %";
                    
                    col++;
                    if (col == colHead.Count)
                    {
                        row_count++;
                        row++;
                        col = 0;
                    }
                    ds.Tables[0].Rows[0]["DynamicPercentCnt"] = i + 1;
                    i++;
                }
                ds.Tables[0].Rows[0]["Rowcount"] = rowHead.Count;
                ds.Tables[0].Rows[0]["ColCount"] = colHead.Count;
              
            }


        }
        return (ds);
    }
       
   
    public void SaveDialogBox(string sourcePath, string SurveyName, string fileExtension)
    {
        
        if (File.Exists(sourcePath))
        {
                        
            string name_survey = SurveyName.Replace(" ", "");
            name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
            name_survey = name_survey.Replace("/", "");

        
            if (fileExtension == ".ppt")
                Response.ContentType = "application/ms-powerpoint";
            else if (fileExtension == ".pdf")
                Response.ContentType = "application/pdf";
            else if (fileExtension == ".xls")
                Response.ContentType = "application/ms-excel";
            else if (fileExtension == ".doc")
                Response.ContentType = "application/msword";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey  + fileExtension);
            Response.TransmitFile(sourcePath);
            Response.End();
            
        }
    }


    public string GetQuesType(int QUESTION_TYPE)
    {
        string QUES_MODE = "";
        if (QUESTION_TYPE == 1 || QUESTION_TYPE == 2 || QUESTION_TYPE == 3 || QUESTION_TYPE == 4 || QUESTION_TYPE == 10 || QUESTION_TYPE == 11 || QUESTION_TYPE == 12 || QUESTION_TYPE == 13 || QUESTION_TYPE == 15)
        { QUES_MODE = "Desc"; }
        else if (!(QUESTION_TYPE == 16 || QUESTION_TYPE == 17 || QUESTION_TYPE == 18 || QUESTION_TYPE == 19))
        { QUES_MODE = "NonDesc"; }
        return (QUES_MODE);
    }
    protected void lbn_rawdataexport_Click(object sender, EventArgs e)
    {

        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0)
            {
                if (ht.Contains("QuesID"))
                    QuestID = Convert.ToInt32(ht["QuesID"]);
                if (ht.Contains("Veiw"))
                    ViewAllQues = Convert.ToString(ht["Veiw"]);
                if (ht.Contains("Mode"))
                    Mode = Convert.ToInt32(ht["Mode"]);
                if (ht.Contains("ddlsidetext"))
                    SMode = Convert.ToInt32(ht["ddlsidetext"]);
                if (ht.Contains("Date_Start"))
                    dt1 = Convert.ToDateTime(ht["Date_Start"]);
                if (ht.Contains("Date_End"))
                    dt2 = Convert.ToDateTime(ht["Date_End"]);
            }
        }


        if (SelectedSurvey != null && SelectedSurvey.SURVEY_ID > 0)
        {
            bool flag = false;
            DataSet dssurcorevoice7 = surcore.voicedataset();
            if (dssurcorevoice7.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
            {
                flag = surcorevoice.GetRawDataExportResponses(SelectedSurvey.SURVEY_ID, "Complete");
            }
            else
            {

                flag = surcore.GetRawDataExportResponses(SelectedSurvey.SURVEY_ID, "Complete");
            }
            if (flag)
            {
             //  CreateExportFile(4, "RawDataComplete");               
               WriteRawDataToExcelSheet();            

                BindReportgrid(ht, ViewAllQues, dt1, dt2);
            }
            else
            {
                string Naurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + SelectedSurvey.SURVEY_ID + "&surveyFlag=" + SurveyFlag);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "myscript", "alert('No Responses has been Recorded for this survey');window.location.href='" + Naurlstr + "';", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "myscript", "alert('No Responses has been Recorded for this survey');", true);

            }
        }

    }

    protected override void OnError(EventArgs e)
    {

        string enableEmarilToError = ConfigurationManager.AppSettings["EnableEmailToError"];
        if (Convert.ToInt32(enableEmarilToError) > 0)
        {
            // Code that runs when an unhandled error occurs
            var lastException = Server.GetLastError();
            var exceptionMessage = new StringBuilder();
            exceptionMessage.AppendLine("Message:");
            exceptionMessage.AppendLine(lastException.Message);
            exceptionMessage.AppendLine(string.Empty);
            exceptionMessage.AppendLine(string.Empty);
            exceptionMessage.AppendLine(string.Empty);

            exceptionMessage.AppendLine("Source:");
            exceptionMessage.AppendLine(lastException.InnerException.Source);
            exceptionMessage.AppendLine(string.Empty);
            exceptionMessage.AppendLine(string.Empty);
            exceptionMessage.AppendLine(string.Empty);

            exceptionMessage.AppendLine("Stack trace:");
            exceptionMessage.AppendLine(lastException.StackTrace);
            exceptionMessage.AppendLine(string.Empty);
            exceptionMessage.AppendLine(string.Empty);
            exceptionMessage.AppendLine(string.Empty);

            exceptionMessage.AppendLine("Base Stack trace:");
            exceptionMessage.AppendLine(lastException.GetBaseException().StackTrace);
            exceptionMessage.AppendLine(string.Empty);
            exceptionMessage.AppendLine(string.Empty);
            exceptionMessage.AppendLine(string.Empty);
            // Clear the error from the server


            string toEmail = ConfigurationManager.AppSettings["ToEmail"];
            string ccEmail = ConfigurationManager.AppSettings["CCEmail"];

            Insighto.Business.Helpers.MailHelper.SendMailMessage(toEmail, toEmail, "", ccEmail, lastException.Message, exceptionMessage.ToString());
            Server.ClearError();
            base.OnError(e);
        }
    }
    
    protected void lnkNextButton_Click(object sender, EventArgs e)
    {
        int index = ValidationHelper.GetInteger(Session["pageIndex"] != null ? Session["pageIndex"].ToString() : "0", 0);
        index = index + 1;
        Session["pageIndex"] = index;
        BindReportgrid(ht, ViewAllQues, dt1, dt2);
    }

    private System.Data.DataTable createDataTable()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
            DataColumn myDataColumn3 = new DataColumn();

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "QuestionID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "rowansweroption";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "columnansweroption";
            myDataTable.Columns.Add(myDataColumn2);

            myDataColumn3.DataType = Type.GetType("System.String");
            myDataColumn3.ColumnName = "percentage";
            myDataTable.Columns.Add(myDataColumn3);

            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private System.Data.DataTable createDataTableexp()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
            DataColumn myDataColumn3 = new DataColumn();
            DataColumn myDataColumn4 = new DataColumn();

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "QuestionID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "rowansweroption";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "columnansweroption";
            myDataTable.Columns.Add(myDataColumn2);

            myDataColumn3.DataType = Type.GetType("System.String");
            myDataColumn3.ColumnName = "percentage";
            myDataTable.Columns.Add(myDataColumn3);

            myDataColumn4.DataType = Type.GetType("System.String");
            myDataColumn4.ColumnName = "responseval";
            myDataTable.Columns.Add(myDataColumn4);

            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private System.Data.DataTable createMtxColumnDataTable()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
           
            
            myDataColumn.DataType = Type.GetType("System.String");
            myDataColumn.ColumnName = "columnname";
            myDataTable.Columns.Add(myDataColumn);

           
            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private System.Data.DataTable createMtxRowDataTable()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();


            myDataColumn.DataType = Type.GetType("System.String");
            myDataColumn.ColumnName = "rowname";
            myDataTable.Columns.Add(myDataColumn);


            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void AddMtxrowsDatatoTable(string rowoption, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["rowname"] = rowoption;

            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void AddMtxcolsDatatoTable(string columnoption, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["columnname"] = columnoption;
     
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void AddDatatoTable(int QuestionID, string rowansweroption, string columnansweroption, string percentage, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = QuestionID;
            dr["rowansweroption"] = rowansweroption;
            dr["columnansweroption"] = columnansweroption;
            dr["percentage"] = percentage;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void AddDatatoTable(int QuestionID, string rowansweroption, string columnansweroption, string percentage,string responseval, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = QuestionID;
            dr["rowansweroption"] = rowansweroption;
            dr["columnansweroption"] = columnansweroption;
            dr["percentage"] = percentage;
            dr["responseval"] = responseval;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private System.Data.DataTable createDataTableQ123413()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
          

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "QuestionID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "answeroption";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "percentage";
            myDataTable.Columns.Add(myDataColumn2);



            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private System.Data.DataTable createDataTableQ123413exp()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
            DataColumn myDataColumn3 = new DataColumn();

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "QuestionID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "answeroption";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "percentage";
            myDataTable.Columns.Add(myDataColumn2);

            myDataColumn3.DataType = Type.GetType("System.String");
            myDataColumn3.ColumnName = "responseval";
            myDataTable.Columns.Add(myDataColumn3);


            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void AddDatatoTable123413(int QuestionID, string answeroption,  string percentage, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = QuestionID;
            dr["answeroption"] = answeroption;           
            dr["percentage"] = percentage;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void AddDatatoTable123413(int QuestionID, string answeroption, string percentage,string responseval, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = QuestionID;
            dr["answeroption"] = answeroption;
            dr["percentage"] = percentage;
            dr["responseval"] = responseval;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private System.Data.DataTable createDataTableQ123413Export()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
            DataColumn myDataColumn3 = new DataColumn();

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "QuestionID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "answeroption";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "percentage";
            myDataTable.Columns.Add(myDataColumn2);

            myDataColumn3.DataType = Type.GetType("System.Int32");
            myDataColumn3.ColumnName = "AnsCnt";
            myDataTable.Columns.Add(myDataColumn3);

            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void AddDatatoTable123413Export(int QuestionID, string answeroption, string percentage,int anscount, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = QuestionID;
            dr["answeroption"] = answeroption;
            dr["percentage"] = percentage;
            dr["AnsCnt"] = anscount;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private System.Data.DataTable createDataTableQ12()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
            DataColumn myDataColumn3 = new DataColumn();
            DataColumn myDataColumn4 = new DataColumn();

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "QuestionID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "columnoption";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "subcolumnoption";
            myDataTable.Columns.Add(myDataColumn2);

            myDataColumn3.DataType = Type.GetType("System.String");
            myDataColumn3.ColumnName = "rowoption";
            myDataTable.Columns.Add(myDataColumn3);

            myDataColumn4.DataType = Type.GetType("System.String");
            myDataColumn4.ColumnName = "percentage";
            myDataTable.Columns.Add(myDataColumn4);

            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private System.Data.DataTable createDataTableQ12exp()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();
            DataColumn myDataColumn1 = new DataColumn();
            DataColumn myDataColumn2 = new DataColumn();
            DataColumn myDataColumn3 = new DataColumn();
            DataColumn myDataColumn4 = new DataColumn();
            DataColumn myDataColumn5 = new DataColumn();

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "QuestionID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn1.DataType = Type.GetType("System.String");
            myDataColumn1.ColumnName = "columnoption";
            myDataTable.Columns.Add(myDataColumn1);

            myDataColumn2.DataType = Type.GetType("System.String");
            myDataColumn2.ColumnName = "subcolumnoption";
            myDataTable.Columns.Add(myDataColumn2);

            myDataColumn3.DataType = Type.GetType("System.String");
            myDataColumn3.ColumnName = "rowoption";
            myDataTable.Columns.Add(myDataColumn3);

            myDataColumn4.DataType = Type.GetType("System.String");
            myDataColumn4.ColumnName = "percentage";
            myDataTable.Columns.Add(myDataColumn4);

            myDataColumn5.DataType = Type.GetType("System.String");
            myDataColumn5.ColumnName = "responseval";
            myDataTable.Columns.Add(myDataColumn5);

            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void AddDatatoTableQ12(int QuestionID, string columnoption, string subcolumnoption,string rowoption, string percentage, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = QuestionID;
            dr["columnoption"] = columnoption;
            dr["subcolumnoption"] = subcolumnoption;
            dr["rowoption"] = rowoption;
            dr["percentage"] = percentage;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void AddDatatoTableQ12(int QuestionID, string columnoption, string subcolumnoption, string rowoption, string percentage,string responseval, System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = QuestionID;
            dr["columnoption"] = columnoption;
            dr["subcolumnoption"] = subcolumnoption;
            dr["rowoption"] = rowoption;
            dr["percentage"] = percentage;
            dr["responseval"] = responseval;
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private System.Data.DataTable createDataTableChartID()
    {
        try
        {
            DataTable myDataTable = new DataTable();
            DataColumn myDataColumn = new DataColumn();

            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "QuestionID";
            myDataTable.Columns.Add(myDataColumn);
            return myDataTable;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void AddDatatoTablechartid(int chartid,  System.Data.DataTable mytable)
    {
        try
        {
            DataRow dr;
            dr = mytable.NewRow();
            dr["QuestionID"] = chartid;
         
            mytable.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    

    protected void rblgrp_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblgrp.SelectedValue == "DataChart")
        {
          
            string Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&QuestID=" + QuestID + "&Mode=" + 0 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(Navurlstr);
        }
        else if (rblgrp.SelectedValue == "Data")
        {
            string Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&QuestID=" + QuestID + "&Mode=" + 1 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(Navurlstr);
        }
        else if (rblgrp.SelectedValue == "Chart")
        {
            string Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&QuestID=" + QuestID + "&Mode=" + 2 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(Navurlstr);
        }
    }
    protected void btnviewall_Click(object sender, EventArgs e)
    {
        //btnviewall.Enabled = false;
        Session["viewallclick"] = "viewallclick";
        Session["defaultchart"] = "notdefault";
        if (rblgrp.SelectedValue == "DataChart")
        {
            string urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&Veiw=All&Mode=" + 0 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(urlstr);

        }
        else if (rblgrp.SelectedValue == "Data")
        {
            string urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&Veiw=All&Mode=" + 1 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(urlstr);
        }
        else if (rblgrp.SelectedValue == "Chart")
        {
            string urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&Veiw=All&Mode=" + 2 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(urlstr);
        }




    }
    protected void btnviewonebyone_Click(object sender, EventArgs e)
    {
        
       
       
        //btnviewonebyone.Enabled = false;
        if (rblgrp.SelectedValue == "DataChart")
        {

            string Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&QuestID=" + QuestID + "&Mode=" + 0 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(Navurlstr);
        }
        else if (rblgrp.SelectedValue == "Data")
        {
            string Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&QuestID=" + QuestID + "&Mode=" + 1 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(Navurlstr);
        }
        else if (rblgrp.SelectedValue == "Chart")
        {
            string Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&QuestID=" + QuestID + "&Mode=" + 2 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(Navurlstr);
        }
    }
    protected void viewalllnk_Click(object sender, EventArgs e)
    {


        if (rblgrp.SelectedValue == "DataChart")
        {
            string urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&Veiw=All&Mode=" + 0 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(urlstr);
        }
        else if (rblgrp.SelectedValue == "Data")
        {
            string urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&Veiw=All&Mode=" + 1 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(urlstr);
        }
        else if (rblgrp.SelectedValue == "Chart")
        {
            string urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&Veiw=All&Mode=" + 2 + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(urlstr);
        }
    }



    
}

