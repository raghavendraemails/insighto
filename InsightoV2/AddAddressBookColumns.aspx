﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="AddAddressBookColumns.aspx.cs" Inherits="AddAddressBookColumns" meta:resourcekey="PageResource1" %>

<%@ Register Src="UserControls/AddressbookColumns.ascx" TagName="AddressbookColumns"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                Add Columns</h3>
        </div>
        <%--<div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick="closeModalSelf(false,''); " title="Close">&nbsp;</a>
        </div>--%>
        <!-- //popup heading -->
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupContentPanel">
    <p class="defaultHeight"></p>
    <div class="informationPanelDefault">
        <div>
            You can add one or two columns to your contacts.<br />
(Eg: Organization and Phone Number)
        </div>
    </div>
        <!-- popup content panel -->
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccessMsg" runat="server" Visible="False" 
                   meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" Visible="False" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label>
            </div>
        </div>
        <div class="formPanel">
            <!-- form -->
            <div class="gridTopPanel">
                <uc1:AddressbookColumns ID="ucAddressbookColumns" runat="server" />
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="staticwidth">
                            <asp:Label ID="lblbtnNewFolder" runat="server" AssociatedControlID="ibtnSave" meta:resourcekey="lblbtnNewFolderResource1" />
                        </td>
                        <td class="top_padding">
                            <asp:Button ID="ibtnSave" runat="server" Text="Save" CssClass="btn_small_65"
                                meta:resourcekey="ibtnSaveResource1" OnClick="ibtnSave_Click" />                               
                            <asp:HiddenField ID="hdnContactListId" runat="server" />
                        </td>
                    </tr>
                </table>
                <div class="clear">
                </div>
            </div>
            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>
</asp:Content>
