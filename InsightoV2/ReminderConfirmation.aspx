﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReminderConfirmation.aspx.cs" Inherits="ReminderConfirmation"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="clear">
    </div>
    <div class="contentPanelHeader">
        <div class="pageTitle">
            <asp:Label ID="lblReminder" runat="server" Text="Sent Reminder" 
                meta:resourcekey="lblReminderResource1"></asp:Label>
            <!-- //survey question panel -->
        </div>
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <p class="defaultHeight">
        </p>
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
        </div>
         <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div><asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <p class="defaultHeight">
        </p>
        <p id="tr_messagesent" runat="server">
            <asp:Literal ID="litMessagesent" runat="server" 
                meta:resourcekey="litMessagesentResource1" /></p>
        <p class="defaultHeight">
        </p>
        <p id="tr_messagecount" runat="server">
            <asp:Label ID="lbl_messagecount" runat="server" 
                meta:resourcekey="lbl_messagecountResource1" /></p>
        <p class="defaultHeight">
        </p>
        <table width="100%" cellpadding="4" cellspacing="0">
            <tr id="tr_recipients" runat="server">
                <td align="right" width="30%">
                    <asp:Label ID="lbl_recipients" runat="server" Text="Receipients:" 
                        meta:resourcekey="lbl_recipientsResource1" />
                </td>
                <td width="70%">
                    <asp:Label ID="lblrecpcount" Font-Bold="True" runat="server" Text="Receipients" 
                        meta:resourcekey="lblrecpcountResource1" />
                </td>
            </tr>
            <tr id="tr_remdate" runat="server">
                <td align="right">
                    <asp:Label ID="ASPxLabel1" runat="server" Text="Reminder Sent:" 
                        meta:resourcekey="ASPxLabel1Resource1" />
                </td>
                <td>
                    <asp:Label ID="lbl_remdate" runat="server" Font-Bold="True" Text=" " 
                        meta:resourcekey="lbl_remdateResource1" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnMyInsighto" runat="server" Text="Go to My Surveys" wrap="false"
                        CssClass="btn_medium" OnClick="btnMyInsighto_Click" 
                        ToolTip="Go to My Surveys" meta:resourcekey="btnMyInsightoResource1" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
