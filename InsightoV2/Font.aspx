﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true"
    CodeFile="Font.aspx.cs" Inherits="Font"%>

 <%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" runat="Server">
<asp:ScriptManager ID="smFonts" runat="server"></asp:ScriptManager>
    <div class="surveyQuestionPanel">
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div><asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1"></asp:Label></div>
        </div>
         <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div><asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <!-- survey question panel -->
        <div class="surveyQuestionHeader">
            <div class="surveyQuestionTitle">
               <asp:Label ID ="lblChangeFontStyle" runat="server" meta:resourcekey="lblChangeFontStyle"></asp:Label></div>
            <div class="previewPanel">
                <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
            </div>
        </div>
        <div class="surveyQuestionSubHeader">
            <!-- sub header -->
            <asp:CheckBox ID="chkSetDefault" runat="server" 
                meta:resourcekey="chkSetDefaultResource1" />
            <asp:Label ID ="lblSetDefault" runat="server"  
                meta:resourcekey="lblSetDefaultResource1"></asp:Label>
            <!-- //sub header -->
        </div>

        <div class="surveyQuestionTabText">
            <!-- survey question tabs -->
            <asp:UpdatePanel ID="uPnlFonts" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="lbl_rgt">
                        Type
                    </td>
                    <td class="lbl_rgt">
                        <asp:DropDownList ID="ddlType" runat="server" CssClass="dropdownSmall" 
                            meta:resourcekey="ddlTypeResource1" AutoPostBack="true" OnSelectedIndexChanged="ChangeFontAttributes">
                        </asp:DropDownList>
                    </td>
                    <td class="lbl_rgt">
                        Size
                    </td>
                    <td class="lbl_rgt">
                        <asp:DropDownList ID="ddlSize" runat="server" CssClass="dropDown" 
                            meta:resourcekey="ddlSizeResource1" AutoPostBack="true" OnSelectedIndexChanged="ChangeFontAttributes">
                        </asp:DropDownList>
                    </td>
                    <td class="lbl_rgt">
                        Color
                    </td>
                    <td class="lbl_rgt">
                        <asp:DropDownList ID="ddlColor" runat="server" CssClass="dropDown" 
                            meta:resourcekey="ddlColorResource1" AutoPostBack="true" OnSelectedIndexChanged="ChangeFontAttributes">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                <td>
                &nbsp;
                </td>
                </tr>                
            </table cellpadding="0" cellspacing="0">
            <table>
            <tr>
                <td>
                <b>Preview :</b>
                </td>
                <td>
                <asp:Label ID="lblPreview" runat="server" Text="Insighto"></asp:Label>
                </td>
                </tr>
            </table>
            </ContentTemplate>
            <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlType" EventName="SelectedIndexChanged" />
             <asp:AsyncPostBackTrigger ControlID="ddlSize" EventName="SelectedIndexChanged" />
              <asp:AsyncPostBackTrigger ControlID="ddlColor" EventName="SelectedIndexChanged" />
            </Triggers>
            </asp:UpdatePanel>
            
            <!-- //survey question tabs -->
        </div>
        <div id="div_font" runat="server" visible="false" class="errorMesg">
           <asp:Label ID ="lblPaidFeature" runat="server" Text="Paid feature" 
                meta:resourcekey="lblPaidFeatureResource1"></asp:Label></div>

        <div class="clear">
        </div>
        <div class="bottomButtonPanel">
            <asp:Button ID="btnSave" runat="server" Text="Save" ToolTip="Save" CssClass="dynamicButton"
                OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" />
        </div>
        <!-- //survey question panel -->
    </div>
</asp:Content>
