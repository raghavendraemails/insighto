﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ManageLogin.aspx.cs" Inherits="Insighto.Pages.ManageLogin" Culture="auto"
    UICulture="auto" %>

<%@ Register Src="UserControls/ProfileRightPanel.ascx" TagName="ProfileRightPanel"
    TagPrefix="uc" %>
<%@ Register Src="UserControls/TopMenu.ascx" TagName="TopMenu" TagPrefix="uc" %>
<%@ Register Src="UserControls/ManageLogin.ascx" TagName="ManageLogin" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="myProfileLeftPanel">
        <div class="marginNewPanel">
            <div class="signUpPnl">
                <!-- change password -->
                <h3>
                    Manage Email
                </h3>
                <div class="messagePanelProfile">
                    <div class="informationPanelDefault">
                        <div>
                            Your email Address is your login to Insighto. If you change your email address here,
                            remember to use the new email address the next time you log into Insighto.</div>
                    </div>
                    <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                        <div>
                            <asp:Label ID="lblSuccessMsg" runat="server" Text="Saved Successfully."></asp:Label></div>
                    </div>
                    <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                        <div>
                            <asp:Label ID="lblErrorMsg" runat="server" Text="<%$ Resources:CommonMessages, EmailExistsMsg %>"></asp:Label></div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <fieldset>
                    <legend>Primary Email</legend>
                    <div class="con_login_pnl">
                        <div class="loginlbl" style="width: 119px">
                            Login Email Id
                        </div>
                        <div class="loginControls">
                            <asp:TextBox ID="txtEmail" runat="server" meta:resourcekey="txtEmailResource1" CssClass="textBoxMedium200"
                                Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Alternate Email</legend>
                    <ul class="formList" id="alternateMail">
                        <li>
                            <uc1:ManageLogin ID="ucManageLogin" runat="server" />
                        </li>
                    </ul>
                </fieldset>
                <div class="con_login_pnl">
                    <div class="loginlbl">
                    </div>
                    <div class="loginControls">
                        <asp:Button ID="btnSave" CssClass="dynamicButton" runat="server" Text="Save" ToolTip="Save"
                            OnClick="btnSave_Click" />&nbsp;&nbsp;
                        <asp:Button ID="btnCancel" runat="server" ValidationGroup="Cancel" CssClass="dynamicButton"
                            ToolTip="Cancel" Text="Cancel" OnClick="btnCancel_Click" /></div>
                </div>
                <div class="clear">
                </div>
                <!-- //change password -->
            </div>
        </div>
    </div>
    <uc:ProfileRightPanel ID="ucProfileRightPanel" runat="server" />
    <div class="clear">
    </div>
</asp:Content>
