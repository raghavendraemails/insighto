﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Configuration;
using CovalenseUtilities.Helpers;
using Insighto.Business;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
using App_Code;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.IO;
using SendGrid;
using SendGrid.SmtpApi;

public partial class errorpage : System.Web.UI.Page
{
    string errormessage = "";
    string templatename = "ErrorNotification";
    private MailMessage mailMessage = null;
    private SmtpClient smtpMail = null;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        string iserror = "";
        try
        {
            if (Request.QueryString["iserror"] != null)
            {
                iserror = Request.QueryString["iserror"].ToString();
            }
            if (iserror == "true")
            {
                errormsg.InnerHtml = "Your session has expired.";
            }
            if (ex != null)
            {
                if (ex.GetBaseException() != null)
                {
                    errormessage = ex.ToString();
                    if (!String.IsNullOrWhiteSpace(errormessage))
                    {
                        try
                        {
                            errormessage = "<b>ERROR:</b> " + ex.ToString();
                            errormessage = "<b>STACK TRACE:</b> " + ex.StackTrace.ToString();
                            errormessage += "<br/><b>Remote IP:</b> " + Request.ServerVariables["REMOTE_HOST"].ToString();
                            errormessage += "<br/><b>Remote PORT:</b> " + Request.ServerVariables["REMOTE_PORT"].ToString();
                            errormessage += "<br/><b>Remote USER:</b> " + Request.ServerVariables["REMOTE_USER"].ToString();
                            errormessage += "<br/><b>Query string:</b> " + Request.QueryString;
                            errormessage += "<br/><b>Page:</b> " + Request.RawUrl;
                            errormessage += "<br/><b>Referer:</b> " + Request.UrlReferrer.ToString();
                            if (userDetails != null && userDetails.UserId > 0)
                            {
                                errormessage += "<br/><b>Username</b>: " + userDetails.LoginName;
                            }
                        }
                        catch (Exception)
                        {
                        }
                        sendEmail();
                    }
                }
            }

        }
        catch (Exception)
        {
        }
    }

    protected void sendEmail1() { 
    
        string viewData1;
        JavaScriptSerializer js1 = new JavaScriptSerializer();
        js1.MaxJsonLength = int.MaxValue;
        clsjsonmandrill prmjson1 = new clsjsonmandrill();
        //  var prmjson1 = new jsonmandrill();
        prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
        prmjson1.name = templatename;
        viewData1 = js1.Serialize(prmjson1);

        string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";


        WebClient request1 = new WebClient();
        request1.Encoding = System.Text.Encoding.UTF8;
        request1.Headers["Content-Type"] = "application/json";
        byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));

        string response1 = System.Text.Encoding.ASCII.GetString(resp1);


        clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);

        string bodycontent = jsmandrill11.code;


        string viewData;
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = int.MaxValue;
        var prmjson = new clsjsonmandrill.jsonmandrillmerge();
        prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
        prmjson.template_name = templatename;



        List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
        mvars.Add(new clsjsonmandrill.merge_vars { name = "ERRORDETAILS", content = errormessage });
        prmjson.merge_vars = mvars;


        viewData = js.Serialize(prmjson);


        string url = "https://mandrillapp.com/api/1.0/templates/render.json";


        WebClient request = new WebClient();
        request.Encoding = System.Text.Encoding.UTF8;
        request.Headers["Content-Type"] = "application/json";
        byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

        string response = System.Text.Encoding.ASCII.GetString(resp);

        string unesc = System.Text.RegularExpressions.Regex.Unescape(response);

        string first4 = unesc.Substring(0, 9);

        string last2 = unesc.Substring(unesc.Length - 2, 2);


        string unesc1 = unesc.Replace(first4, "");
        string unescf = unesc1.Replace(last2, "");


        clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);

        mailMessage = new MailMessage();
        mailMessage.To.Clear();
        mailMessage.Sender = new MailAddress("alerts@insighto.com", "Insighto.com");
        mailMessage.From = new MailAddress("alerts@insighto.com", "Insighto.com");

        mailMessage.Subject = "ERROR ON INSIGHTO.COM " + DateTime.Now.ToString();

        mailMessage.To.Add(ConfigurationManager.AppSettings["ErrorEmailTo"].ToString());
        mailMessage.Body = unescf;
        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;
        smtpMail = new SmtpClient();
        smtpMail.Host = "smtp.mandrillapp.com";
        smtpMail.Port = 587;
        smtpMail.EnableSsl = true;
        smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
        smtpMail.Credentials = new System.Net.NetworkCredential("ram@knowience.com", "T74TJc3EZPYaIcvdBg--Dg");

        try
        {

            smtpMail.Send(mailMessage);
        }
        catch (SmtpFailedRecipientsException ex)
        {
        }
        mailMessage.Dispose();
    }

    protected void sendEmail()
    {

        try
        {
            SendGridMessage mailmessage = new SendGridMessage();
            mailmessage.From = new MailAddress("satish@knowience.com", "Dev Team");
            mailmessage.Subject = "ERROR ON INSIGHTO.COM at " + DateTime.UtcNow.ToString() + " UTC";
            mailmessage.Html = errormessage;
            List<String> recipients = new List<String>
                                {
                                    @"Satish Kumar <satish@knowience.com>",
                                    @"Dasarath Ram <ram@knowience.com>",
                                };
            mailmessage.AddTo(recipients);

            var creds = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SendGridUID"].ToString(), ConfigurationManager.AppSettings["SendGridPWD"].ToString());
            var transportWeb = new Web(creds);
            transportWeb.Deliver(mailmessage);

        }
        catch (Exception ex)
        {
        }
    }
}