﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <link href="css/StyleSheet.css" rel="stylesheet" type="text/css" />
  <div class="reportsmain">
     <div class="bgbar">
       <div class="report">Report : Election</div>
       <div class="divreportssep"><div class="report">Status : </div><div class="reportnormal">Active</div></div>
       <div class="divreportssep"><div class="report">Views : </div><div class="reportnormal">10,000</div></div>
       <div class="divreportssep"><div class="report">Votes : </div><div class="reportnormal">10,000</div></div>
       <div class="divreportssep"><div class="report">Last Modified : </div><div class="reportnormal">01-jul-2014</div></div>
     </div>
     <div>
        <div class="surveyCreateLeftMenu">
            <ul class="reportsMenu">
                <li><a href="#" class="activeLink">Reports</a></li>
            </ul>
            <div class="reportExpandPanel">
                <p>
                    <a href="#" onclick="showHide('divDisplay','spanDisplay')"><span id="spanDisplay"
                        style="font-size: 10px; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: normal;
                        color: #666666">[+]</span><b> Display:</b></a></p>  
                <div id="divDisplay" style="display: block;">
                 <asp:RadioButtonList ID="rblgrp" runat="server"  AutoPostBack="true" >
                  <asp:ListItem Value="DataChart" Text="Data+Chart" ></asp:ListItem>
                  <asp:ListItem Value="Data" Text="Data"></asp:ListItem>
                  <asp:ListItem Value="Chart" Text="Chart"></asp:ListItem>
                 </asp:RadioButtonList>
                </div>
                   
            </div>
            <div class="reportExpandPanel">
                <p>
                    <a href="#" onclick="showHide('divExportTo','spanExportTo')"><span id="spanExportTo"
                        style="font-size: 10px; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: normal;
                        color: #666666">[+]</span><b> Export to:</b></a></p>
                <div id="divExportTo" style="display: none;">
                <div class="exportpollreports"><asp:LinkButton ID="LinkButton1" runat="server" Text="Excel" 
                                   ></asp:LinkButton></div>
                <div class="exportpollreports"><asp:LinkButton ID="lnkWord" runat="server" Text="Word" 
                                   ></asp:LinkButton></div>
                <div class="exportpollreports"><asp:LinkButton ID="lnlPdf" runat="server" Text="PDF" 
                                   ></asp:LinkButton></div>
                <div class="exportpollreports">   <asp:LinkButton ID="lnlPowerPoint" runat="server" Text="PowerPoint" 
                                   ></asp:LinkButton></div>
                <div class="exportrawdata"><asp:LinkButton ID="lbn_rawdataexport" runat="server" Text="Export Raw Data to Excel" 
                                   ></asp:LinkButton></div>
                <div class="exportrawdata"><asp:LinkButton ID="lnlGraphImage" runat="server" Text="Convert graph to image" 
                                   ></asp:LinkButton></div>
                </div>
            </div>
            <ul class="reportsMenu">
                <li><a href="#" class="activeLink">SOURCE OF VOTES</a></li>
            </ul>
            <div class="reportExpandPanel">
               <div class="exportrawdata"><asp:LinkButton ID="lnkLocation" runat="server" Text="Location" 
                                   ></asp:LinkButton></div>
               <div class="exportrawdata"><asp:LinkButton ID="lnkDevice" runat="server" Text="Device" 
                                   ></asp:LinkButton></div>
               <div class="exportrawdata"><asp:LinkButton ID="lnkPage" runat="server" Text="Page" 
                                   ></asp:LinkButton></div>
            </div>
        </div>
        <div class="surveyQuestionPanel">
        <div class="bgbarquestion question">Question</div>
        <div class="reportview">Report</div>
        <div class="fullwidth">
        <div class="tablecssreports"><table style="width: 600px" cellpadding="3" >
                <tr>
                    <td class="countrywidth">
                        Country
                    </td>
                    <td>
                       Views
                    </td>
                    <td>
                       % of Views
                    </td>
                        <td>
                       Votes
                    </td>
                    <td>
                       % of Votes
                    </td>
                </tr>
                <tr class="labels">
                    <td class="countrywidth">
                        India
                    </td>
                    <td>
                        5,000
                    </td>
                    <td>
                        10
                    </td>
                       <td>
                        2000
                    </td>
                    <td>
                        35
                    </td>
                </tr>
                <tr class="labels">
                    <td class="countrywidth">
                        China
                    </td>
                    <td>
                        5,000
                    </td>
                    <td style="width: 103px">
                        10
                    </td>
                       <td>
                        2000
                    </td>
                    <td>
                        35
                    </td>
                </tr>
            </table></div>
        <div class="exportpollreports floatR"><asp:LinkButton ID="lnkExport" runat="server" Text="Export" 
                                   ></asp:LinkButton></div>
        </div>
        </div>
     </div>
  </div>
      <script type="text/javascript">
          function showHide(EL, PM) {
              ELpntr = document.getElementById(EL);
              if (ELpntr.style.display == 'none') {
                  document.getElementById(PM).innerHTML = "[-] ";
                  ELpntr.style.display = 'block';
              }
              else {
                  document.getElementById(PM).innerHTML = "[+]";
                  ELpntr.style.display = 'none';
              }
          }

    </script>
</asp:Content>

