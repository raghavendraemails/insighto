﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="SendReminder.aspx.cs" Inherits="SendReminder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="clear">
    </div>
    <div class="contentPanelHeader">
        <div class="pageTitle">
            <asp:Label ID="lblTitle" runat="server" Text="Send Reminder" 
                meta:resourcekey="lblTitleResource1"></asp:Label>
            <!-- //survey question panel -->
        </div>
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
    <p class="defaultHeight"></p>
    <table border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td width="30%" align="right">
            <asp:Label ID="ASPxLabel6" runat="server" AssociatedControlID="ddlSurvey" 
                    Text="Select the survey to send reminder : " 
                    meta:resourcekey="ASPxLabel6Resource1" />
            <asp:Label ID="lbl_customerid" runat="server" CssClass="bcolor" 
                    meta:resourcekey="lbl_customeridResource1" />
            </td>
            <td width="70%">
             <asp:DropDownList ID="ddlSurvey" CssClass="dropDown" runat="server" AutoPostBack="True" 
        onselectedindexchanged="ddlSurvey_SelectedIndexChanged" 
                    meta:resourcekey="ddlSurveyResource1">
    </asp:DropDownList>
            </td>
        </tr>
    </table>
<p class="defaultHeight"></p>
     
        <div>
            <asp:ListView ID="lvSurveyReminders" runat="server" OnItemDataBound="lvSurveyReminders_ItemDataBound">
                        <LayoutTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr class="headerRwoGray"> 
                                    <td align="left">
                                        <asp:Label ID="lblDateProcessed" runat="server" Text="Date Processed" 
                                            meta:resourcekey="lblDateProcessedResource1"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblList" runat="server" Text="List" 
                                            meta:resourcekey="lblListResource1"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblUniqueRespondent" runat="server" Text="Unique Respondent" 
                                            meta:resourcekey="lblUniqueRespondentResource2"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblInviteSent" runat="server" Text="Invites Sent" 
                                            meta:resourcekey="lblInviteSentResource1"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblCompletes" runat="server" Text="Completes" 
                                            meta:resourcekey="lblCompletesResource2"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblNoofReminders" runat="server" Text="No of Reminders" 
                                            meta:resourcekey="lblNoofRemindersResource1"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblReminders" runat="server" Text="Reminders" 
                                            meta:resourcekey="lblRemindersResource1"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr class="rowStyleGray">
                                <td>
                                    <asp:Label runat="server" ID="lblLAUNCHEDON" 
                                        ><%# Convert.ToDateTime(Eval("LAUNCHED_ON")).ToString("dd-MMM-yyyy")%></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblCName"><%#Eval("CONLIST_CONTACTLIST_NAME")%></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblUNIQUERESPONDENT" 
                                       ></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblSENDTO" ><%#Eval("EmailCount")%></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblCOMPLETES" 
                                        ><%#Eval("COMPLETES")%></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblTOTAL_RESPONSES" 
                                        ><%#Eval("REMINDER_COUNT")%></asp:Label>
                                </td>
                                <td>
                                <asp:HyperLink ID="hlnkReminder" runat="server" Text="Send Reminders" 
                                        meta:resourcekey="hlnkReminderResource1"></asp:HyperLink>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
        </div>
        
    </div>
</asp:Content>
