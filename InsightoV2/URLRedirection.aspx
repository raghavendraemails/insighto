﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true" CodeFile="URLRedirection.aspx.cs" Inherits="Insighto.Pages.URLRedirection" culture="auto" uiculture="auto" %>
 <%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" Runat="Server">

	<div class="surveyQuestionPanel"><!-- survey question panel -->
    <div class="successPanel" runat="server" visible="false" id="sucessMsg">
        <div><asp:Label ID="lblMessage" runat="server" meta:resourcekey="lblMessageResource1" /></div>
    </div>
     <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
        <div>
            <asp:Label ID="lblErrMsg" runat="server" Visible="False" 
                meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
    </div>
    <div class="surveyQuestionHeader">
					<div class="surveyQuestionTitle">
                        <asp:Label ID="lblTitle" runat="server"
                            meta:resourcekey="lblTitleResource1"></asp:Label></div>
					<div class="previewPanel">
						 <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
					</div>
				</div>
				<div class="surveyQuestionTabPanel"><!-- survey question tabs -->
					<h4><span></span>
                        <asp:Label ID="lblHelpText" runat="Server" 
                            Text="Redirect survey end page to another website of your choice. (For eg. your company website)" 
                            meta:resourcekey="lblHelpTextResource1"></asp:Label></h4>
                    <div class="con_login_pnl">
                        <div class="loginlbl"> <asp:Label ID="lblUrl" runat="server" AssociatedControlID="txtUrl" Text="http://"
                                meta:resourcekey="lblUrlResource1"/>
                        </div>
                        <div class="loginControls"> <asp:TextBox ID="txtUrl" runat="server" CssClass="textBoxMedium txtUrl" MaxLength="500"
                                meta:resourcekey="txtUrlResource1"></asp:TextBox>
                        </div>                        
                    </div>              
                       
                    <div class="con_login_pnl">
                        <div class="reqlbl btmpadding"><asp:Label ID="lblreqUrl" runat="server" AssociatedControlID="regUrl"
                                meta:resourcekey="lblreqUrlResource1"></asp:Label><%--<asp:RequiredFieldValidator SetFocusOnError="true" ID="reqUrl" runat="server" CssClass="lblRequired"
                                ControlToValidate="txtUrl" Display="Dynamic" ErrorMessage="Please enter Url."
                                meta:resourcekey="reqUrlResource2" ></asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator SetFocusOnError="True"  ID="regUrl" 
                                ControlToValidate="txtUrl" CssClass="lblRequired" 
                                ValidationExpression="^www.[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$" 
                                ErrorMessage="Please enter url in a valid format. www.domain.com" 
                                Display="Dynamic" runat="server" meta:resourcekey="regUrlResource1"></asp:RegularExpressionValidator>
                        </div>                       
                    </div>
                    <div class="con_login_pnl">
                        <div class="loginlbl">Redirect after
                        </div>
                        <div class="loginControls"><table cellpadding="0" cellspacing="0"><tr><td><asp:TextBox ID="txtTime" runat="server" MaxLength="2" Text="0" CssClass="textBoxSmall txtTime"
                                meta:resourcekey="txtTimeResource1"  onkeypress="javascript:return onlyNumbers();" onpaste="return false" onblur="javascript:return setdefaultvalue()"/>&nbsp;</td><td> seconds</td></tr></table>
                        </div>
                    </div> 
                      <div class="con_login_pnl">
                        <div class="reqlbl btmpadding">
                       <%-- <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqSedconds" runat="server" CssClass="lblRequired"
                                ControlToValidate="txtTime" Display="Dynamic" ErrorMessage="Please enter seconds to redirect."></asp:RequiredFieldValidator>--%>
                        <asp:RangeValidator SetFocusOnError="True"  ID="rangeSec" runat="server" 
                                ControlToValidate="txtTime" Display="Dynamic" CssClass="lblRequired" 
                                ErrorMessage="Please enter 0 to 60 seconds only." MaximumValue="60" 
                                MinimumValue="0" Type="Integer" meta:resourcekey="rangeSecResource1"></asp:RangeValidator>
                        </div>
                        </div>             
                    <div class="con_login_pnl">
                        <div class="reqlbl btmpadding"><asp:Button ID="btnSaveContinue" runat="server" Text="Save" CssClass="dynamicButton"
                            ToolTip="Save" OnClick="btnSaveContinue_Click"
                            meta:resourcekey="btnSaveContinueResource1" />
                            
                        </div>
                    </div>

				<!-- //survey question tabs --></div>
                <div class="clear"></div>

                </div>
                <script type="text/javascript">
                    function onlyNumbers(evt) {
                        var e = event || evt; // for trans-browser compatibility
                        var charCode = e.which || e.keyCode;

                        if (charCode > 31 && (charCode < 48 || charCode > 57))
                            return false;

                        return true;

                    }
                    function setdefaultvalue() {
                        if ($('.txtTime').val() == "") {
                            $('.txtTime').val('0');
                        }
                    }
//                    $(document).ready(function () {
//                    $('.txtUrl').mask('www.? x99999');
//                    });
                </script>
</asp:Content>

