﻿using System;
using System.Collections.Generic;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using System.Linq;
using App_Code;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;

public partial class SurveyDrafts : SurveyPageBase
{

    private List<SurveyQuestionAndAnswerOptions> surveyoptionsList = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (SurveyID == null)
            return;

        GetRespondentUrl();
        var questionService = ServiceFactory.GetService<QuestionService>();
        
        var questionSeq = questionService.GetAllSurveyQuestionsBySurveyId(base.SurveyID);
        surveyoptionsList = Cache["SurveyQuestionsList"] as List<SurveyQuestionAndAnswerOptions>;
        string strAddLink = EncryptHelper.EncryptQuerystring("AddNewQuestion.aspx", "SurveyId=" + base.SurveyID + "&Page=Questionnaire&surveyFlag=" + base.SurveyFlag + "&QuestionSeq=" + questionSeq.Count.ToString());
        lnkAddNew.HRef = strAddLink;
        if (base.IsSurveyActive)
        {
            dvAddQuestion.Visible = false;
        }
        if (!IsPostBack)
        {
            lblSurveyPreviewURL.Text = String.Format(Utilities.ResourceMessage("lblSurveyPreviewURL"), SurveyMode);
            //dvInformation1.InnerHtml = Utilities.ResourceMessage("dvInformation1");   
            //dvInformation.InnerHtml = Utilities.ResourceMessage("dvInformation");
        }
    }


    private void GetSurveyQuestionsAndOptionsList()
    {
        int SurveyId = 0;
        surveyoptionsList = Cache["SurveyQuestionsList"] == null ? ServiceFactory.GetService<RespondentService>().GetSurveyQuestionAndAnswerOptionsList(SurveyId, false)
            : Cache["SurveyQuestionsList"] as List<SurveyQuestionAndAnswerOptions>;

        Cache["SurveyQuestionsList"] = surveyoptionsList != null ? surveyoptionsList : new List<SurveyQuestionAndAnswerOptions>();
    }



    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (base.SurveyID > 0)
        {
            string strNextLink = EncryptHelper.EncryptQuerystring(PathHelper.GetAddIntroductionURL(), Constants.SURVEYID + "=" + base.SurveyID + "&surveyFlag=" + SurveyFlag);
            Response.Redirect(strNextLink);
        }
    }

    protected void Page_Error(object source, EventArgs e)
    {

    }

    private void GetRespondentUrl()
    {
        //txtUrl.Text = string.Empty;
        //var questionService = ServiceFactory.GetService<QuestionService>();
        //var surveyControls = ServiceFactory.GetService<SurveyControls>().GetSurveycontrolsBySurveyId(SurveyID).FirstOrDefault();

        //txtUrl.Text = "http://" + Request.Url.Authority + "/" + EncryptHelper.EncryptQuerystring("SurveyRespondentReview.aspx", Constants.SURVEYID + "=" + SurveyID + "&Mode=" + RespondentDisplayMode.RespondentWithRedirection.ToString() + "&surveyFlag=" + base.SurveyFlag + "&Layout=" + surveyControls.SURVEY_LAYOUT);

        txtUrl.Text = string.Empty;
        var questionService = ServiceFactory.GetService<QuestionService>();
        var surveyControls = ServiceFactory.GetService<SurveyControls>().GetSurveycontrolsBySurveyId(SurveyID).FirstOrDefault();

        txtUrl.Text = "http://" + Request.Url.Authority + "/" + EncryptHelper.EncryptQuerystring("SurveyRespondentReview.aspx", Constants.SURVEYID + "=" + SurveyID + "&Mode=" + RespondentDisplayMode.RespondentWithRedirection.ToString() + "&surveyFlag=" + base.SurveyFlag + "&Layout=" + surveyControls.SURVEY_LAYOUT);

        // srini, 10/5/2013 URL shortening
        //txtUrl.Text = "http://" + Request.Url.Authority + "/s/" +
        //               SurveyID + "-" +
        //               Convert.ToInt32(RespondentDisplayMode.RespondentWithRedirection) +
        //               "-" + base.SurveyFlag + "-" + surveyControls.SURVEY_LAYOUT;  

        // srini, 10/22/2013, url shortening using bitly, if it fails then go ahead with long url, sob!
        //IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
        //for preview on 5/11/2013
        //IBitlyService s = new BitlyService("satyakolli", "R_8a13c903550fc8f80a8a2efe1e481271");
        IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
        string insightoLong = txtUrl.Text;
        string shortened = "";
        try
        {
            shortened = s.Shorten(insightoLong);
            if (shortened != null)
            {
                txtUrl.Text = shortened;
            }

        }
        catch (Exception)
        {
        }

    }

}