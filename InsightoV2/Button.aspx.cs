﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using App_Code;
using System.Data;


public partial class Button : SurveyPageBase
{
    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int surveyId = 0;
    SurveyCore scapp = new SurveyCore();

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = false;

        if (!IsPostBack)
        {

            BindColor();

            BindExistingData();
            SetProFeatures();
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            //if (loggedInUserInfo.LicenseType == "FREE")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", false);
            //}
        }
    }
    private void SetProFeatures()
    {
        if (!Utilities.ShowOrHideFeature(Utilities.GetUserType(), (int)FeatureTypes.Personalization.Browser_Back_Button))
        {
            chkBrowserBackButton.Attributes.Add("onclick", "javascript:OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "',690, 450, 'yes');return false;");
            chkBrowserBackButton.Checked = false;
        }
    }
    private void BindExistingData()
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }

        if (surveyId > 0)
        {

            if (SetFeatures("BUTTONSETTINGS") == 0)
            {
                rbtnArrow.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                chkClearLabels.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                rbtnCustom.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }

            var surveySettingsService = ServiceFactory.GetService<SurveySetting>();
            var surveySettings = surveySettingsService.GetSurveySettings(surveyId);
            if (surveySettings[0].BROWSER_BACKBUTTON == 1)

                chkBrowserBackButton.Checked = true;
            else
                chkBrowserBackButton.Checked = false;

            if (SetFeatures("BUTTONSETTINGS") == 1)
            {
                if (surveySettings[0].SURVEY_BUTTON_TYPE.Trim() == "Plain")
                {
                    rbtnArrow.Checked = true;
                    chkClearLabels.Checked = true;
                    if (surveySettings[0].BUTTON_BG_COLOR != "")
                        ddlArrowBC.SelectedValue = surveySettings[0].BUTTON_BG_COLOR;

                }
                else if (surveySettings[0].SURVEY_BUTTON_TYPE.Trim() == "Label")
                {
                    rbtnArrow.Checked = true;
                    chkClearLabels.Checked = false;
                    if (surveySettings[0].BUTTON_BG_COLOR != "")
                        ddlArrowBC.SelectedValue = surveySettings[0].BUTTON_BG_COLOR;
                }
                else if (surveySettings[0].SURVEY_BUTTON_TYPE.Trim() == "Custom")
                {
                    rbtnCustom.Checked = true;

                    if (surveySettings[0].BUTTON_BG_COLOR != "")
                        ddlCustomBC.SelectedValue = surveySettings[0].BUTTON_BG_COLOR;
                }
                else if (surveySettings[0].SURVEY_BUTTON_TYPE.Trim() == "")
                {
                    rbtnDefault.Checked = true;
                    if (surveySettings[0].BUTTON_BG_COLOR != "")
                        ddlDefaultBC.SelectedValue = surveySettings[0].BUTTON_BG_COLOR;
                }

                if (surveySettings[0].CUSTOMSTART_TEXT != null)
                {
                    btnStart.Text = "Start Survey";
                    //btnStart.Text = (surveySettings[0].CUSTOMSTART_TEXT == "") ? "Start" : surveySettings[0].CUSTOMSTART_TEXT;
                    txtStart.Text = (surveySettings[0].CUSTOMSTART_TEXT == "") ? "Start Survey" : surveySettings[0].CUSTOMSTART_TEXT;
                }
                else
                {
                    txtStart.Text = "Start Survey";
                    btnStart.Text = "Start Survey";
                }
                if (surveySettings[0].CUSTOMSUBMITTEXT != null)
                {
                    btnSubmit.Text = "Submit";
                    //btnSubmit.Text = (surveySettings[0].CUSTOMSUBMITTEXT == "") ? "Submit" : surveySettings[0].CUSTOMSUBMITTEXT;
                    txtSubmit.Text = (surveySettings[0].CUSTOMSUBMITTEXT == "") ? "Submit" : surveySettings[0].CUSTOMSUBMITTEXT;
                }
                else
                {
                    txtSubmit.Text = "Submit";
                    btnSubmit.Text = "Submit";
                }
                if (surveySettings[0].CUSTOM_CONTINUE_TEXT != null)
                {
                    btnContinue.Text = "Continue";
                    //btnContinue.Text = (surveySettings[0].CUSTOM_CONTINUE_TEXT == "") ? "Continue" : surveySettings[0].CUSTOM_CONTINUE_TEXT;
                    txtContinue.Text = (surveySettings[0].CUSTOM_CONTINUE_TEXT == "") ? "Continue" : surveySettings[0].CUSTOM_CONTINUE_TEXT;
                }
                else
                {
                    txtContinue.Text = "Continue";
                    btnContinue.Text = "Continue";
                }


                DataSet dssrvysettings = scapp.getSurveySettings(surveyId);

                if (dssrvysettings.Tables[0].Rows.Count > 0)
                {
                    if (dssrvysettings.Tables[0].Rows[0]["CUSTOMEXIT_TEXT"].ToString() != "") 
                    {
                        txtExitbtn.Text = dssrvysettings.Tables[0].Rows[0]["CUSTOMEXIT_TEXT"].ToString();                        
                    }
                    else
                    {
                        txtExitbtn.Text = "Exit this survey";                       
                    }
                    if (dssrvysettings.Tables[0].Rows[0]["CUSTOMSAVELATER_TEXT"].ToString() != "")
                    {
                        txtsavelaterbtn.Text = dssrvysettings.Tables[0].Rows[0]["CUSTOMSAVELATER_TEXT"].ToString();
                    }
                    else
                    {
                        txtsavelaterbtn.Text = "Save for later";
                    }
                }
                else
                {
                     txtExitbtn.Text ="Exit this survey";
                     txtsavelaterbtn.Text = "Save for later";
                }

            }
            else
            {
                rbtnDefault.Checked = true;
                rbtnArrow.Checked = false;
                chkClearLabels.Checked = false;
                rbtnCustom.Checked = false;
                txtStart.Text = "Start";
                btnStart.Text = "Start";
                txtSubmit.Text = "Submit";
                btnSubmit.Text = "Submit";
                txtContinue.Text = "Continue";
                btnContinue.Text = "Continue";
                ddlArrowBC.SelectedValue = "Pink";
                ddlCustomBC.SelectedValue = "Pink";
                ddlDefaultBC.SelectedValue = "Pink";
            }


        }
    }

    #region "Method : BindColor"
    /// <summary>
    /// used to bind font colors.
    /// </summary>

    public void BindColor()
    {
        var pickListService = ServiceFactory.GetService<PickListService>();
        osm_picklist osmPicklist = new osm_picklist();
        osmPicklist.CATEGORY = Constants.FONTCOLOR;
        List<osm_picklist> picklist = pickListService.GetPickList(osmPicklist);


        ddlDefaultBC.DataSource = picklist;
        ddlDefaultBC.DataTextField = Constants.PARAMETER;
        ddlDefaultBC.DataValueField = Constants.PARAMETER;
        ddlDefaultBC.DataBind();

        ddlArrowBC.DataSource = picklist;
        ddlArrowBC.DataTextField = Constants.PARAMETER;
        ddlArrowBC.DataValueField = Constants.PARAMETER;
        ddlArrowBC.DataBind();

        ddlCustomBC.DataSource = picklist;
        ddlCustomBC.DataTextField = Constants.PARAMETER;
        ddlCustomBC.DataValueField = Constants.PARAMETER;
        ddlCustomBC.DataBind();

        ddlDefaultBC.SelectedValue = "Pink";
        ddlArrowBC.SelectedValue = "Pink";
        ddlCustomBC.SelectedValue = "Pink";
    }

    #endregion


    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains(Constants.SURVEYID))
                surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        }

        if (surveyId > 0)
        {
            osm_surveysetting osmSurveySetting = new osm_surveysetting();
            osmSurveySetting.SURVEY_ID = surveyId;


            if (SetFeatures("BUTTONSETTINGS") == 1)
            {
                if ((rbtnArrow.Checked == true) && (chkClearLabels.Checked == true))
                {
                    osmSurveySetting.SURVEY_BUTTON_TYPE = "Plain";
                    osmSurveySetting.CUSTOMSTART_TEXT = "";
                    osmSurveySetting.CUSTOMSUBMITTEXT = "";
                    osmSurveySetting.CUSTOM_CONTINUE_TEXT = "";
                    osmSurveySetting.BUTTON_BG_COLOR = ddlArrowBC.SelectedValue;
                }

                if ((rbtnArrow.Checked == true) && (chkClearLabels.Checked == false))
                {
                    osmSurveySetting.SURVEY_BUTTON_TYPE = "Label";
                    osmSurveySetting.CUSTOMSTART_TEXT = "";
                    osmSurveySetting.CUSTOMSUBMITTEXT = "";
                    osmSurveySetting.CUSTOM_CONTINUE_TEXT = "";
                    osmSurveySetting.BUTTON_BG_COLOR = ddlArrowBC.SelectedValue;
                }

                if (rbtnCustom.Checked)
                {
                    osmSurveySetting.SURVEY_BUTTON_TYPE = "Custom";
                    osmSurveySetting.CUSTOMSTART_TEXT = txtStart.Text.Trim();
                    osmSurveySetting.CUSTOMSUBMITTEXT = txtSubmit.Text.Trim();
                    osmSurveySetting.CUSTOM_CONTINUE_TEXT = txtContinue.Text.Trim();
                    osmSurveySetting.BUTTON_BG_COLOR = ddlCustomBC.SelectedValue;
                }

                if (rbtnDefault.Checked)
                {
                    osmSurveySetting.SURVEY_BUTTON_TYPE = "";
                    osmSurveySetting.CUSTOMSTART_TEXT = "";
                    osmSurveySetting.CUSTOMSUBMITTEXT = "";
                    osmSurveySetting.CUSTOM_CONTINUE_TEXT = "";
                    osmSurveySetting.BUTTON_BG_COLOR = ddlDefaultBC.SelectedValue;
                }

            }
            else
            {
                osmSurveySetting.SURVEY_BUTTON_TYPE = "";
                osmSurveySetting.CUSTOMSTART_TEXT = "";
                osmSurveySetting.CUSTOMSUBMITTEXT = "";
                osmSurveySetting.CUSTOM_CONTINUE_TEXT = "";
                osmSurveySetting.BUTTON_BG_COLOR = ddlDefaultBC.SelectedValue;
            }
            osmSurveySetting.BROWSER_BACKBUTTON = chkBrowserBackButton.Checked ? 1 : 0;  

            var surveySettingsService = ServiceFactory.GetService<SurveySetting>();
            surveySettingsService.SaveOrUpdateSurveySetting(osmSurveySetting, "B");

            DataSet dsappsrvysetting = scapp.updateSurveySettings(txtExitbtn.Text, txtsavelaterbtn.Text, surveyId);

            if (SetFeatures("BUTTONSETTINGS") == 1)
            {
                lblSuccMsg.Text = Utilities.ResourceMessage("lblSuccMsgResource1.Text"); ;
                dvSuccessMsg.Visible = true;
                BindExistingData();
            }
        }
    }


    #region "Method : SetFeatures"
    /// <summary>
    /// This method returns bool value, if the feature flag is one.
    /// </summary>
    /// <param name="strFeature"></param>
    /// <returns></returns>

    public int SetFeatures(string strFeature)
    {
        int iVal = 0;
        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var sessdet = session.GetLoginUserDetailsSession();
        if (sessdet != null)
        {

            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));

            if (sessdet.UserId > 0 && listFeatures != null)
            {
                foreach (var listFeat in listFeatures)
                {

                    if (listFeat.Feature.Contains(strFeature))
                    {
                        iVal = (int)listFeat.Value;
                    }
                }
            }
        }

        else
        {
            Response.Redirect(PathHelper.GetUserLoginPageURL());
        }
        return iVal;
    }

    #endregion
}