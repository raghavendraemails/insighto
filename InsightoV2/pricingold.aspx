﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true" CodeFile="pricingold.aspx.cs" Inherits="pricing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Price plan - Insighto</title>
<link href="../App_Themes/Classic/style.css" rel="stylesheet" type="text/css"/>
 <script type="text/javascript">

     var _gaq = _gaq || [];
     _gaq.push(['_setAccount', 'UA-29026379-1']);
     _gaq.push(['_setDomainName', 'insighto.com']);
     _gaq.push(['_setAllowLinker', true]);
     _gaq.push(['_trackPageview']);

     (function () {
         var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
     })();

</script>
</head>
<body>

<div id="container_main" class="clr">
<div class="title_blk pad clr" id="topinspromo" runat="server">
            <h2 >Make your online feedback surveys swifter and easier.</h2>
           <%-- <h4>Now, for a lot less!</h4>--%>
        </div>
<table style="background:white" width="100%">
<tr>
<td >
    	<div class="container_mainL">
        	<div class="title_blk clr" id="topleftmsg" runat="server">
                <h2>Make your online employee surveys <br /> swifter and easier.</h2>
               <%-- <h4>Now, for a lot less!</h4>--%>
            </div>
    	<div class="priceplan_blk" id="pricemain" runat="server">        	
            <!--  Basic Price Blk  -->

    	
        <table width="100%">
        

        <tr>
        <td  id="td1" runat="server">&nbsp;</td>
        <td  id="td2" runat="server" valign="top">        
        	<div id="divbasic"  runat="server" align="center">
            	<h3>Basic</h3>
                <div id="divbasiccont" runat="server">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header">
                                    <div class="price_tag_free">Free <br /> for<br /> LIFE</div>
                                    <div class="price">
                                 <asp:Label ID="lblfree" runat="server" Text="FREE" class="freetext" ></asp:Label></div>
                                 <asp:Label ID="Label1" runat="server" Text="for life" class="freetext1"  ></asp:Label>
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border" id="tdfreespace" runat="server">&nbsp;</td>
                          </tr>
                           <tr>
                            <td class="no_border" id="td6" runat="server">&nbsp;                          
                            </td>
                          </tr>
                          <tr>
                            <td><asp:Button ID="button" runat="server" Text="Sign Up" CssClass="btn_bg" 
                                    onclick="button_Click" />
                                <%--<input type="submit" name="button" id="button" value="Buy Now" class="btn_bg" />--%></td>
                          </tr>
                          <tr>
                            <td style=" color:#217DC4; font-weight:bold">The new awesome Basic<br /> Plan<br />                               
                                <a id="lnkcheckitout" runat="server">Check it out</a>
                                </td>
                          </tr>
 
                          <tr>
                            <td>Unlimited number of surveys<br id="basicbr1" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                            <td>Unlimited number of questions  </td>
                          </tr>
                          <tr>
                            <td>100 responses per survey<br id="basicbr2" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                            <td>21 question types<br id="basicbr3" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                            <td>Survey templates<br id="basicbr4" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                            <td>Skip logic / branching<br id="basicbr5" runat="server" visible="false" /></td>
                          </tr>
                            <tr>
                            <td>Print survey<br id="basicbr6" runat="server" visible="false" /></td>
                          </tr>
                           <tr>
                            <td>Save survey to Word<br id="brbasicviresp" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                            <td>Choice of themes</td>
                          </tr>
                           <tr>
                            <td>Brand logo on survey</td>
                          </tr>
                          <tr>
                            <td>Customize survey end messages</td>
                          </tr>
                           <tr>
                            <td>Launch through weblink</td>
                          </tr>
                          <tr>
                            <td>Results display in animated graphs</td>
                          </tr>
                          <tr>
                            <td>Cross-tab</td>
                          </tr>
                         
                          <tr>
                            <td class="no_border" style="height: 34px"><br id="basicfree1" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border" id="basictr" runat="server" visible="false">&nbsp;</td>
                          </tr>
                          <tr>
                            <td><a id="basicviewall" runat="server">View all features</a></td>
                          </tr>
                          <tr>
                            <td class="no_border"><asp:Button ID="button1" runat="server" Text="Sign Up" 
                                    CssClass="btn_bg" onclick="button1_Click" />
                                <%--<input type="submit" name="button" id="button" value="Buy Now" class="btn_bg" />--%></td>
                          </tr>
                    </table>
                </div>

            </div>
          </td>
        <td  id="td3" runat="server" valign="top">
                   
            <!--  Pro Blk  -->
         
        	<div id="divpro" runat="server">
                
            	<h3>PPS_Pro</h3>
                <div id="divprocont" runat="server">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header green">
                                   <div id="dvpricetagfree" runat="server" class="price_tag_free">30%<br />OFF</div>
                                     <div class="strike_txt" id="divstrikepro" runat="server">
                                        	<div class="strike_bg"></div> 
                                            <asp:Label ID="lblorgpro" runat="server" Text=""  ></asp:Label>
                                        </div>
                                         <div class="price">
                                    <asp:Label ID="lblpro" runat="server" Text="" ></asp:Label>
                                       </div>
                                   	per annum
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border txt_underline" id="tdpromonth" runat="server">&nbsp; <%--<a id="PayMonth" runat="server" >
                            <asp:Label ID="lblMonthly" runat="server" Text="Monthly Plan - <span class='font_ruppe'>`</span>450"></asp:Label>
                            </a>--%>
                                <%--<input type="submit" name="button" id="button" value="Buy Now" class="btn_bg" />--%>
                            </td>
                          </tr>
                          <tr>
                            <td><asp:Button ID="btnpro1" Text="Buy Now" runat="server" CssClass="btn_bg" onclick="btnpro1_Click" 
                                    />
                                <%--<input type="submit" name="button" id="button" value="Buy Now" class="btn_bg" />--%></td>
                          </tr>
                          <tr>
                            <td class="no_border txt_green">All features of Basic </td>
                          </tr>
                          <tr>
                            <td   class="txt_green">+</td>
                          </tr>
                          <tr>
                            <td>Unlimited number of responses per survey</td>
                          </tr>
                          <tr>
                            <td>Schedule alerts<br id="probr2" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                            <td>Launch through Insighto emails</td>
                          </tr>
                          <tr>
                            <td>Personalize email invitations<br id="probr3" runat="server" visible="false" /></td>
                          </tr>
                          
                          <tr>
                            <td>Protect survey link with password</td>
                          </tr>
                          <tr>
                            <td>Send reminder emails to get more responses</td>
                          </tr>
                          <tr>
                            <td>Export data to Excel, PPT, PDF and Word</td>
                          </tr>
                          <tr>
                            <td>Export cross-tab report to Excel<br id="probr4" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;<br id="Br1" runat="server" visible="true" /></td>                         
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                        
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                        
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                        
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                        
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                         
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                            
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                        
                          </tr>
                          <tr >
                          <td class="no_border">&nbsp;</td>                      
                          </tr>
                          
                          <tr>
                            <td><a id="proviewall" runat="server">View all features</a></td>
                          </tr>
                          <tr>
                            <td class="no_border">
                                <asp:Button ID="btnpro2" Text="Buy Now" runat="server" 
                                    CssClass="btn_bg" onclick="btnpro2_Click"  />
                                <%--<div id="footer_blk">
    	<div id="footerL">Copyright &copy; 2013 Knowience Insights</div>
        <div id="footerR">
            <ul>
                <li><a href="../../Home.aspx">Home</a></li>
                <li><a href="whyinsighto.aspx?key=Ghp2rQFUHwIqTRm%2fsyV7zcJen%2fadfzIY">Why Insighto</a></li>
                <li><a href="price_plan.html" class="selected">Plans &amp; Pricing</a></li>
                <li><a href="aboutus.aspx?key=JUQ5UePXAPzI3eI%2b40zxvA%3d%3d">About Us</a></li>
                <li><a href="../In/ContactUs.aspx">Contact Us</a></li>
                <li><a href="antispam.aspx?key=6auQ29rICLYtdOGhX9UHsQ%3d%3d">Anti spam policy</a></li>
                <li><a href="privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy policy</a></li>
            </ul>
        </div>
   		<div class="clr"></div>
    </div>--%></td>
                          </tr>
                    </table>
                </div>

            </div>
            <!--  Pro Blk End  -->
        	</td>


             <td  id="tdppsprem" runat="server" valign="top">
                   
            <!--  Pro Blk  -->
         
        	<div id="divppsprem" runat="server">
                
            	<h3>PPS_Prem</h3>
                <div id="divppspremcont" runat="server">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header green">
                                   <div id="dvpricetagfreepps" runat="server" class="price_tag_free">30%<br />OFF</div>
                                     <div class="strike_txt" id="divstrikeprepps" runat="server">
                                        	<div class="strike_bg"></div> 
                                            <asp:Label ID="lblorgpppsprem" runat="server" Text=""  ></asp:Label>
                                        </div>
                                         <div class="price">
                                    <asp:Label ID="lblppsprem" runat="server" Text="" ></asp:Label>
                                       </div>
                                   	per annum
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border txt_underline" id="td9" runat="server">&nbsp;<%--<a id="A1" runat="server" >
                            <asp:Label ID="Label4" runat="server" Text="Monthly Plan - <span class='font_ruppe'>`</span>450"></asp:Label>
                            </a>--%>
                                <%--<input type="submit" name="button" id="button" value="Buy Now" class="btn_bg" />--%>
                            </td>
                          </tr>
                          <tr>
                            <td><asp:Button ID="btnppsprem" Text="Buy Now" runat="server" CssClass="btn_bg" 
                                    onclick="btnppsprem_Click" />
                                <%--<input type="submit" name="button" id="button" value="Buy Now" class="btn_bg" />--%></td>
                          </tr>
                          <tr>
                            <td class="no_border txt_green">All features of Basic </td>
                          </tr>
                          <tr>
                            <td   class="txt_green">+</td>
                          </tr>
                          <tr>
                            <td>Unlimited number of responses per survey</td>
                          </tr>
                          <tr>
                            <td>Schedule alerts<br id="Br2" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                            <td>Launch through Insighto emails</td>
                          </tr>
                          <tr>
                            <td>Personalize email invitations<br id="Br3" runat="server" visible="false" /></td>
                          </tr>
                          
                          <tr>
                            <td>Protect survey link with password</td>
                          </tr>
                          <tr>
                            <td>Send reminder emails to get more responses</td>
                          </tr>
                          <tr>
                            <td>Export data to Excel, PPT, PDF and Word</td>
                          </tr>
                          <tr>
                            <td>Export cross-tab report to Excel<br id="Br4" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;<br id="Br5" runat="server" visible="true" /></td>                         
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                        
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                        
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                        
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                        
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                         
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                            
                          </tr>
                          <tr>
                          <td class="no_border">&nbsp;</td>                        
                          </tr>
                          <tr >
                          <td class="no_border">&nbsp;</td>                      
                          </tr>
                          
                          <tr>
                            <td><a id="A2" runat="server">View all features</a></td>
                          </tr>
                          <tr>
                            <td class="no_border">
                                <asp:Button ID="btnppsprem2" Text="Buy Now" runat="server" CssClass="btn_bg" 
                                    onclick="btnppsprem2_Click"   />
                                <%--<div id="footer_blk">
    	<div id="footerL">Copyright &copy; 2013 Knowience Insights</div>
        <div id="footerR">
            <ul>
                <li><a href="../../Home.aspx">Home</a></li>
                <li><a href="whyinsighto.aspx?key=Ghp2rQFUHwIqTRm%2fsyV7zcJen%2fadfzIY">Why Insighto</a></li>
                <li><a href="price_plan.html" class="selected">Plans &amp; Pricing</a></li>
                <li><a href="aboutus.aspx?key=JUQ5UePXAPzI3eI%2b40zxvA%3d%3d">About Us</a></li>
                <li><a href="../In/ContactUs.aspx">Contact Us</a></li>
                <li><a href="antispam.aspx?key=6auQ29rICLYtdOGhX9UHsQ%3d%3d">Anti spam policy</a></li>
                <li><a href="privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy policy</a></li>
            </ul>
        </div>
   		<div class="clr"></div>
    </div>--%></td>
                          </tr>
                    </table>
                </div>

            </div>
            <!--  Pro Blk End  -->
        	</td>


        <td  id="td4" runat="server" valign="top">            
            <!--  Premium Blk  -->
        
        	<div  id="divpremium" runat="server">
            	<h3>Premium</h3>
                <div id="divpremiumcont" runat="server" >
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header orange">
                                <div id="dvprempricetagfree" runat="server" class="price_tag_free">40%<br />OFF</div>                                   
                                     <div class="strike_txt" id="divstrikeprem" runat="server">
                                        	<div class="strike_bg"></div>                                        
                                           <asp:Label ID="lblorgpremium" runat="server" Text="" CssClass="price" />
                                        </div>
                                        <div class="price">
                                    <asp:Label ID="lblpremium" runat="server" Text="" CssClass="price" /></div>
                                   	per annum
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border" id="tdonlyaplan" runat="server">Only Annual Plan</td>
                          </tr>
                            <tr>
                            <td class="no_border" id="td7" runat="server">&nbsp;                          
                            </td>
                          </tr>
                          <tr>
                            <td><asp:Button ID="btnpremium1" Text="Buy Now" runat="server" CssClass="btn_bg" onclick="btnpremium1_Click" 
                                    />
                                <%-- <p class="defaultHeight">&nbsp;</p>--%></td>
                          </tr>
                          <tr>
                            <td class="no_border txt_green">All features of Pro </td>
                          </tr>
                          <tr>
                            <td class="txt_green">+</td>
                          </tr>
                          <tr>
                            <td>Export survey raw data to Excel</td>
                          </tr>
                          <tr>
                            <td>View individual responses<br id="brviresp" runat="server" visible="false" /></td>
                          </tr>                         
                          <tr>
                            <td>View partial / incomplete responses</td>
                          </tr>
                          <tr>
                            <td>Exclude / restore completed responses</td>
                          </tr>
                           <tr>
                            <td >Convert partials to completes</td>   
                          </tr>
                          <tr>
                            <td >Export partials data<br id="prembr1" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                            <td >Customize URL redirection<br id="brpremreportindata" runat="server" visible="false" /></td>
                          </tr>
                          <tr>
                            <td >Remove Insighto branding from survey end messages</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;<br id="prembr2" runat="server" visible="true" /></td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr >
                            <td class="no_border">&nbsp;</td>
                          </tr>   
                          
                          <tr>
                            <td><a id="premviewall" runat="server">View all features</a></td>
                          </tr>
                          <tr>
                            <td class="no_border">
                                <asp:Button ID="btnpremium2" Text="Buy Now" runat="server" 
                                    CssClass="btn_bg" onclick="btnpremium2_Click"  />
                                <%--  <p class="defaultHeight">&nbsp;</p>--%></td>
                          </tr>
                    </table>
                </div>

            </div>
         <%--  </div>--%>
            <!--  Premium Blk End  -->
        </td>
        <td id="td5" runat="server" valign="middle">
        	    
        </td>
        </tr>
        </table>  
        	
   			<div class="clr"></div>

        </div>
   		
  </div>
        
        </td>
<td valign="top"  id="tdrightpanel" runat="server" >

    	<div class="container_mainR"  runat="server">
        	<div class="special_offer_blk clr" id="toprightmsg" runat="server">
            	<p class="title">Special Offer </p>
                <p class="title">&nbsp;&nbsp;in association with&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
            	<p class="title"><img src="images/client_logo.png" alt="" /></p>                
                <p class="">&nbsp;</p>
                <p class="">&nbsp;</p>
                <p class="">&nbsp;</p>
                <p class="">&nbsp;</p>
                <p class="">Valid until 15th September 2013</p>
            </div>
            <div class="points_blk clr" id="shinebullet" runat="server">
            	<ul>	
                	<li>The swiftest way to collect and analyze employee feedback</li>
                	<li>Use 15 survey templates to create employee surveys yourself</li>
                	<li id="payinrupeesshine" runat="server">Pay in Indian Rupees</li>
                	<li id="paymethodshine" runat="server">Pay by Credit Card Or Net Banking Or Wire Transfer Or Demand Draft Or Cheque</li>
                	<li id="phonesupportshine" runat="server">Phone support – 9 am to 6 pm – Monday to Friday – India time</li>
                	<li>Free webinar training </li>
                    <li id="pricesUSDshine" runat="server" visible="false">Prices in US dollars</li>
                </ul>
            </div>
             <div class="points_blk clr"  id="insightobullet" runat="server" style="position:absolute;margin:auto;z-index:1000;top:313px;left:656px; width:240px" >
            	<ul>	
                   
                	<li> The swiftest way to collect and analyze online feedback</li>
                	<li>Use 15 survey templates to create online surveys yourself</li>
                	<li id="payinrupeesinsighto" runat="server">Pay in Indian Rupees</li>
                	<li id="paymethodinsighto" runat="server">Pay by Credit Card Or Net Banking Or Wire Transfer Or Demand Draft Or Cheque</li>
                	<li id="phonesupportinsighto" runat="server">Email support </li>
                	<li>Free webinar training </li>
                    <li id="pricesUSDinsighto" runat="server" visible="false">Prices in US dollars</li>
                </ul>
            </div>
            <div id="shinesignup" runat="server" style="position:absolute;margin:auto;z-index:1000;top:650px;">
            <p>
            	Not ready for a paid plan yet? <br />                
				<a href="http://www.insighto.com/In/SignUpFree.aspx" style="position:relative;" ><strong><u>Sign up FREE</u></strong></a> for a Basic Subscription.
            </p>
            <p>&nbsp;</p>
            <p>
            Want to know more about Insighto? <a href="http://www.insighto.com/Home.aspx" style="position:relative;"><strong><u>Click Here</u></strong></a>
            </p>
            </div>

             <div id="insightosignup" runat="server" style="position:absolute;margin:auto;z-index:1000;top:650px;">            
            <p>
         <%--   Want to know more about Insighto? <a href="http://www.insighto.com/Home.aspx" style="position:relative;"><strong><u>Click Here</u></strong></a>--%>
            </p>
            </div>

        </div> 
        </td>   	
    </tr>
<tr>
<td colspan="2" id="tdpricefaq" runat="server">
    <%--  <p class="defaultHeight">&nbsp;</p>--%>

    <center>
            <div class="prcingFaqHeading" id="divFaq" style="text-align:left;">          
            <a href="javascript:void(0)" id="lnkShowContent" onclick="javascript:showHideFAQPanel();" style="font-size:12px;">Pricing FAQs</a>
            <a href="javascript:void(0)" onclick="showHideFAQPanel()"><span class="plusExpandFAQ">+</span></a>
            </div>
            <div >&nbsp;</div>
  <div class="prcingFaqContent" style="display:none;text-align:left;" id="divFaqContent">  
  <div id="divFaqIndiaContent" runat="server">
  <h4>What’s the validity of the Free subscription?</h4>
  <p>There’s no end date to Insighto Free subscription. In a sense, the Free subscription has lifetime validity!</p>
    <p class="defaultHeight">&nbsp;</p>
         <h4>In short, what is it that I do not get with my Free subscription?</h4>
  <p>The Free subscription is designed to offer you a real feel of the way the survey tool works albeit with a few limitations. (Else, you won’t pay us, will you?)</p>
 <p><b>You will need a Paid subscription, if you have the following needs.</b></p>
 <ul class="listPadding">
 <li>You need more than 100 responses for your survey</li>
<li>You would like to schedule your survey launch</li>
<li>You would like to export your responses into an Excel, PDF, Word or a PowerPoint.</li> 
<li>You would like to view individual responses</li>

 </ul>
      <%--  <p class="defaultHeight">&nbsp;</p>--%>
   <h4>If I do not renew my Paid subscription, what would be the status of my account?</h4>
   <p>If for some reason, you do not renew your Paid subscription, you will automatically turn into a Free subscriber.</p>
     <p class="defaultHeight">&nbsp;</p> 
      <h4>If I do not renew my Paid subscription, what happens to my data?</h4>
        <p>Your data stays secure with us even if you are not a Paid subscriber. </p>
       <p class="defaultHeight">&nbsp;</p>
         <h4>What happens if I launch a survey while having a Paid subscription but do not renew while I still have active survey(s)?</h4>
           <p>Assuming that your paid subscription is not renewed while you still have active surveys, the following scenarios are applied.</p>
             <ol style="list-style-type:lower-alpha" class="listPadding">            
             <li>If the active survey(s) have any Pro features used, such active surveys will be closed for response.</li> 
<li>If the active survey(s) do not have any Paid features used, they will continue to be active.</li>

             </ol>
      <%--  <p class="defaultHeight">&nbsp;</p>--%>
               <h4>What are the different payment options I have?</h4>
               <p><b>There are five different options you can choose from.</b></p>
                <ol style="list-style-type:lower-alpha" class="listPadding">
                <li>	Credit card</li>
<li>Debit Card</li>
<li>Net banking</li>
<li>Pay by cheque (Not available for Pro – Monthly)</li>
<li>Direct deposit (Not available for Pro – Monthly)</li>

                </ol>
              <%--  <p class="defaultHeight">&nbsp;</p>--%>
                  <h4>How do I upgrade? </h4>
                    <p>You can upgrade at any time right from “My Surveys” page. Simply click on the upgrade button and choose the plan that you’d like to upgrade to.</p>
                      <p class="defaultHeight">&nbsp;</p>
                        <h4>Is my data secure and private? </h4>
                          <p>Yes, your data is secure and private. Your collected data is yours and we never use it or share it any way shape or form. Please see our Privacy Policy, Security Information, and Terms of Use for further info.</p>
                           <p class="defaultHeight">&nbsp;</p>
                              <h4>How do I cancel my account?</h4>
                               <p>You can cancel anytime you want. For one, we would be sorry to see you go as we are constantly working to add more features that enhance your experience with Insighto.
 Please let us know how we can improve.</p>
 <p>
 If you would like to cancel your subscription, please do send a mail to Support@insighto.com. We, of course, would be keen to know the reasons for the same.
 </p>
 <p class="defaultHeight">&nbsp;</p>
 <b>Cancellation Terms:</b>
 <p>
 Once your Paid plan cancels, the account converts to the Basic (free) status with all the limitations associated with that account type. Paid features will be deactivated, but you will still have access to your account and surveys. You can reactivate it at a later date and everything will be available to you again. If you have active surveys or need to analyze data, then we recommend waiting to cancel the subscription until you are completely finished.
 </p>
 <p class="defaultHeight">&nbsp;</p>
 <p>
 <b>NOTE - Refunds:</b> We do not offer pro-rated refunds on plans. If you anticipate needing the plan for a short amount of time, purchase a PRO Monthly plan.
 </p>
 <p>
 <b>Example:</b> If you have a paid monthly subscription, it will cancel at the end of the current billing cycle. If you have an annual subscription, it will cancel at the end of the 12 months period.
 </p> 
 </div>
   <div id="divFaqOtherContent" runat="server">
 
  </div>
  </div> 

  </center>
  </td>
</tr>
</table>
  </div>
 
</body>
</html>


<script language="javascript" type="text/javascript">
    function showHide(EL, PM) {
        ELpntr = document.getElementById(EL);
        if (ELpntr.style.display == 'none') {
            document.getElementById(PM).innerHTML = "-";
            ELpntr.style.display = 'block';
        }
        else {
            document.getElementById(PM).innerHTML = "+";
            ELpntr.style.display = 'none';
        }
    }
    function RedirectToLoginPage() {
        window.location.href = "../Home.aspx";
        return false;
    }

    function showHideFAQPanel() {
        divContent = document.getElementById('divFaqContent');
        if (divContent.style.display == 'none') {
            // document.getElementById(PM).innerHTML = "-";
            divContent.style.display = 'block';
        }
        else {
            //document.getElementById(PM).innerHTML = "+";
            divContent.style.display = 'none';
        }
    }
        
    </script>

</asp:Content>

