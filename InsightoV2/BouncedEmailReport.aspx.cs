﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using App_Code;
using Insighto.Data;

public partial class BouncedEmailReport : SurveyPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (userDetails != null)
        {
            var surveyList = ServiceFactory.GetService<SurveyService>().GetSurveysByUserId(userDetails.UserId);
            if (surveyList.Count > 0)
            {
                Utilities.FillDropDownList<osm_survey>(ddlSurvey, surveyList, "SURVEY_NAME", "SURVEY_ID");
                ddlSurvey.Items[0].Text = "-- ALL --";
            }
        }
        ddlSurvey.SelectedValue =Convert.ToString(SurveyID);
        ddlSurvey.Enabled = false;
        hdnSurveyId.Value = Convert.ToString(SurveyID);
    }
}