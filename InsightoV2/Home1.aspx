﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home1.aspx.cs" Inherits="Insighto.Pages.Home1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>:: Insighto ::</title>
    <link href="homeStyle.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript" src="images/homeimages/jquery.js"></script>
    <script type="text/javascript" src="images/homeimages/thickbox.js"></script> 
    <script type="text/javascript" src="JQuery/banner_ jquery.js"></script>
    <script type="text/javascript" src="jQuery/banner_jquery-ui.js"></script>
    <script type="text/javascript" src="jQuery/banner_slider.js"></script>
     
    <script type="text/javascript" src="Scripts/facebox.js"></script>
     <script type="text/javascript" src="Scripts/common-functions.js" ></script>
    <style type="text/css" media="screen">
        @import "images/homeimages/thickbox.css";
        @import "images/homeimages/insighto.css";
    </style>
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="images/homeimages/insighto.ico" />
</head>
<body>
    <form id="form1" runat="server">
    <table width="980" border="0" cellspacing="0" cellpadding="0" align="center">
        <!-- header -->
        <tr>
            <td width="281" align="left" valign="bottom">
                <%--<a href="index.html">--%><img src="images/homeimages/logo.gif" width="281" height="112"
                    border="0" /><%--</a>--%>
            </td>
            <td align="left" valign="bottom">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="41" align="right" valign="bottom">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="7">
                                    </td>
                                </tr>
                            </table>
                            <table width="699" height="37" border="0" cellpadding="0" cellspacing="0" id="loginid">
                                <tr>
                                    <td width="272" rowspan="2" style="padding-right: 5px; padding-top: 5px;" align="right"
                                        valign="top">
                                        <asp:Label ID="lblInvaildAlert" class="errorMesg" runat="server" Visible="false"  Text="In valid User Name or Password"></asp:Label>
                                      
                                    </td>
                                    <td width="132" align="left">
                                        <input id="txtEmailAdd" runat="server" width="124" name="emailaddress" type="text"
                                            value="Email Address" onfocus="if(this.value==this.defaultValue) this.value='';"
                                            onblur="if(this.value=='') this.value=this.defaultValue;" />
                                    </td>
                                    <td width="130" align="left">
                                        
                                        <input id="txtPassword" runat="server" width="124" name="password" type="password"
                                            value="password" onfocus="if(this.value==this.defaultValue) this.value='';" onblur="if(this.value=='') this.value=this.defaultValue;" />
                                    </td>
                                    <td width="100" align="left">
                                        <asp:ImageButton ID="ibtnLogin" ImageUrl="images/homeimages/login_button.gif" OnClick="ibtnLogin_Click" runat="server" />
                                       
                                        <%--<input type="submit" name="Submit" value="Login" ID="cmdLogIn" class="logbut" runat="server" OnClick="cmdLogIn_Click" ValidationGroup="grplogin" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="132" align="left">
                                        <%--<dxe:ASPxHyperLink ID="hlkSignupNow" runat="server" Text="Sign Up Now !" ForeColor="#6c59a2" NavigateUrl="~/In/Registration.aspx" CssClass="pra"> </dxe:ASPxHyperLink>--%>
                                        <a href="In/pricing.aspx" id="hlkSignupNow" runat="server" title="Sign Up Now !"
                                            style="color: #6c59a2; font-family: 'Trebuchet MS'; font-size: 13px">Sign Up Now
                                            !</a>
                                    </td>
                                    <td width="130" align="left">

                                        <a href="ForgotPassword.aspx" id="hlkForgotPassword"
                                            rel='framebox' w='700' h='300'  style="color: #6c59a2; font-family: 'Trebuchet MS';
                                            font-size: 13px">Forgot Password?</a>


                                        <%--<dxe:ASPxHyperLink ID="ASPxHyperLink1"  runat="server" Text="Forgot Password?" Width="111px"  ForeColor="#6c59a2" Cursor="pointer">
</dxe:ASPxHyperLink>--%>
                                    </td>
                                    <td width="100" align="left">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="images/homeimages/logo_tag_line.gif" width="242" height="24" hspace="4" alt="Online surveys" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">
                            <ul id="topnavigation">
                                <li id="topnavigation-01"><a href="Home.aspx"><span>Home</span></a></li>
                                <li id="topnavigation-02"><a href="In/whyinsighto.aspx"><span>Why Insighto</span></a></li>
                                <li id="topnavigation-03"><a href="In/Pricing.aspx"><span>Pricing</span></a></li>
                                <li id="topnavigation-04"><a href="In/aboutus.aspx"><span>About Us</span></a></li>
                                <li id="topnavigation-05"><a href="In/ContactUs.aspx"><span>Conatct Us</span></a></li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!-- //header -->
    </table>
    <div style="height: 5px; background: #AF7BB7;">
    </div>
    <!-- body -->
    <div class="mainPanel">
        <div>
            <!-- flash top panel -->
            <div class="preface-top-wrapper-home">
                <!-- flash banner -->
                <div class="block-home-slider-wrapper">
                    <div>
                        <div class="block-home-text-slide">
                            <div id="txt_1" style="display: block;">
                                <div class="bannerTextPanel">
                                    <span class="block-home-sub-title-wrapper">Never created a survey before? <span>No problem.</span></span>
                                    <div class="sub-text-panel">
                                        <span class="home-span-title-second">Easy-to-use
                                            <br />
                                            survey templates</span></div>
                                    <div class="block-home-slider-a">
                                        <table border="0">
                                            <tr>
                                                <td align="right">
                                                    <span>Start using Insighto</span>
                                                </td>
                                                <td>
                                                    <a href="In/SignUpFree.aspx">
                                                        <img src="images/homeimages/button_free.png" alt="It's Free" title="It's Free" /></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="txt_2" style="display: none;">
                                <div class="bannerTextPanel">
                                    <span class="block-home-sub-title-wrapper">Survey customers, employees, students, friends,
                                        anyone.</span>
                                    <div class="sub-text-panel">
                                        <span class="home-span-title-second">Ask questions.
                                            <br />
                                            Get answers. Simple. </span>
                                    </div>
                                    <div class="block-home-slider-a">
                                        <table border="0">
                                            <tr>
                                                <td align="right">
                                                    <span>Start using Insighto</span>
                                                </td>
                                                <td>
                                                    <a href="In/SignUpFree.aspx">
                                                        <img src="images/homeimages/button_free.png" alt="It's Free" title="It's Free" /></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="txt_3" style="display: none;">
                                <div class="bannerTextPanel">
                                    <span class="block-home-sub-title-wrapper">19 question types to choose from.</span>
                                    <div class="sub-text-panel">
                                        <span class="home-span-title-second">Now, you can ask
                                            <br />
                                            any kind of question.</span></div>
                                    <div class="block-home-slider-a">
                                        <table border="0">
                                            <tr>
                                                <td align="right">
                                                    <span>Start using Insighto</span>
                                                </td>
                                                <td>
                                                    <a href="In/SignUpFree.aspx">
                                                        <img src="images/homeimages/button_free.png" alt="It's Free" title="It's Free" /></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="txt_4" style="display: none;">
                                <div class="bannerTextPanel">
                                    <span class="block-home-sub-title-wrapper">1. Create survey &nbsp;&nbsp;2. Send link
                                        &nbsp;&nbsp;3. Analyse results</span>
                                    <div class="sub-text-panel">
                                        <span class="home-span-title-second">Keeping it simple.</span></div>
                                    <div class="block-home-slider-a">
                                        <table border="0">
                                            <tr>
                                                <td align="right">
                                                    <span>Start using Insighto</span>
                                                </td>
                                                <td>
                                                    <a href="In/SignUpFree.aspx">
                                                        <img src="images/homeimages/button_free.png" alt="It's Free" title="It's Free" /></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-home-slide-menu">
                            <ul>
                                <li><a onclick="javascript:showSlider('1');" id="a_1"><span id="s_1" class="block-pink-dot">
                                    &nbsp;</span></a></li>
                                <li><a onclick="javascript:showSlider('2');" id="a_2"><span id="s_2" class="block-white-dot">
                                    &nbsp;</span></a></li>
                                <li><a onclick="javascript:showSlider('3');" id="a_3"><span id="s_3" class="block-white-dot">
                                    &nbsp;</span></a></li>
                                <li><a onclick="javascript:showSlider('4');" id="a_4"><span id="s_4" class="block-white-dot">
                                    &nbsp;</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- //flash banner -->
            </div>
            <div class="buyNowPanel">
                <!-- buy now pannel -->
                <div class="buyNowHeader">
                    <div>
                        <span>Insighto Pro</span> is for you</div>
                </div>
                <div class="buyNowContentPanel buyNowContentPanelHeight">
                    If you need
                    <ul class="proListSmallFont">
                        <li>Unlimited no. of questions</li>
                        <li>Unlimited survey responses</li>
                        <li>Export data to Excel, PPT, PDF and Word</li>
                        <li>Generate reports with charts / graphs</li>
                    </ul>
                    <p>
                        <b>Just INR 4,500 / annum.</b></p>
                    <p>
                        &nbsp;</p>
                    <div style="margin-top: 6px;">
                        <table border="0">
                            <tr>
                                <td>
                                    <a id="PayYear" runat="server">
                                        <img src="images/homeimages/button_buy-now.gif" alt="Buy Now" title="Buy Now" /></a>
                                </td>
                                <td style="padding-left: 10px;">
                                    <a id="PayMonth" runat="server" class="orangeLink"><span class="smallFont">See monthly
                                        plan</span></a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="buyPanelBottom">
                </div>
                <!-- //buy now pannel -->
            </div>
            <!-- //flash top panel -->
        </div>
        <div>
            <!-- bottom panel -->
            <div class="bottomPanel">
                <!-- with insighto -->
                <div class="bottomPanelHeader">
                    <div>
                        See what you can do with Insighto</div>
                </div>
                <div class="buyNowContentPanel" style="height: 150px;">
                    <table border="0">
                        <tr>
                            <td style="height: 48px;">
                                <img src="images/homeimages/icon_marketing.jpg" alt="Marketing" title="Marketing" width="37"
                                    height="30" />
                            </td>
                            <td valign="middle">
                                <a href="images/homeimages/insighto_marketing.html?TB_iframe=true&amp;height=300&amp;width=420"
                                    title="Insighto for Marketing -" class="thickbox">Marketing</a>
                            </td>
                            <td>
                                <img src="images/homeimages/icon_customer.jpg" alt="Customer Relations" title="Customer Relations"
                                    width="37" height="30" />
                            </td>
                            <td valign="middle">
                                <a href="images/homeimages/insighto_cr.html?TB_iframe=true&amp;height=285&amp;width=420"
                                    title="Insighto for Customer Relations -" class="thickbox">Customer Relations</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 48px;" align="left">
                                <img src="images/homeimages/icon_sales.jpg" alt="Sales" title="Sales" width="37" height="30" />
                            </td>
                            <td valign="middle" align="left">
                                <a href="images/homeimages/insighto_sales.html?TB_iframe=true&amp;height=240&amp;width=420"
                                    title="Insighto for Sales -" class="thickbox">Sales</a>
                            </td>
                            <td align="left">
                                <img src="images/homeimages/icon_educational.jpg" alt="Educational Institutions" title="Educational Institutions"
                                    width="37" height="30" />
                            </td>
                            <td valign="middle" align="left">
                                <a href="images/homeimages/insighto_ei.html?TB_iframe=true&amp;height=295&amp;width=420"
                                    title="Insighto for Educational Institutions -" class="thickbox">Educational Institutions</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 48px;" align="left">
                                <img src="images/homeimages/icon_human.jpg" alt="Human Resources" title="Human Resources"
                                    width="37" height="30" />
                            </td>
                            <td valign="middle" align="left">
                                <a href="images/homeimages/insighto_hr.html?TB_iframe=true&amp;height=345&amp;width=420"
                                    title="Insighto for Human Resources -" class="thickbox">Human Resources</a>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="bottomPanelBottom">
                </div>
                <!-- //with insighto -->
            </div>
            <div class="bottomPanel">
                <!-- check out -->
                <div class="bottomPanelHeader">
                    <div>
                        Check out sample surveys</div>
                </div>
                <div class="buyNowContentPanel" style="height: 150px;">
                    <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 18px;">
                        <tr>
                            <td colspan="3">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="surveyBox1">
                                    <div>
                                        <a id="aCustSatisfaction" runat="server">Customer
                                            <br />
                                            Satisfaction
                                            <br />
                                            Survey </a>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="surveyBox2">
                                    <div>
                                        <a id="aEmpSatisfaction" runat="server">Employee
                                            <br />
                                            Satisfaction
                                            <br />
                                            Survey</a></div>
                                </div>
                            </td>
                            <td>
                                <div class="surveyBox3">
                                    <div>
                                        <a id="aMarketResearch" runat="server" style="cursor: pointer;">Product
                                            <br />
                                            Satisfaction Survey<br />
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="bottomPanelBottom">
                </div>
                <!-- //check out -->
            </div>
            <div class="bottomRightPanel">
                <!-- would you like -->
                <div class="bTop">
                </div>
                <div class="bContent">
                    <h2>
                        Would you like Insighto experts to craft your survey?</h2>
                    <p>
                        &nbsp;</p>
                    <p>
                        &nbsp;</p>
                    <p>
                        <a id="hlinkCraft" runat="server">
                            <img src="images/homeimages/button_yes.gif" alt="Yes" title="Yes" /></a></p>
                </div>
                <div class="clear">
                </div>
                <div class="bBottom">
                </div>
                <!-- would you like -->
            </div>
            <!-- //bottom panel -->
        </div>
    </div>
    <!-- //body -->
    <div class="clear">
    </div>
    <!-- footer -->
    <table width="980" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td style="height: 15px;">
            </td>
        </tr>
        <tr>
            <td>
                <div style="background: #818181; height: 1px;">
                </div>
            </td>
        </tr>
    </table>
    <table width="980" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="150">
                &nbsp;
            </td>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <a href="Home.aspx" class="botnav">Home</a> <a href="In/whyinsighto.aspx" class="botnav">
                                Why Insighto</a> <a href="In/Pricing.aspx" class="botnav">Pricing</a> <a href="In/aboutus.aspx"
                                    class="botnav">About Us</a> <a href="In/ContactUs.aspx" class="botnav">Contact Us</a>
                            <a href="In/antispam.aspx" class="botnav">Anti spam policy</a><a href="In/privacypolicy.aspx"
                                class="botnav">Privacy policy</a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="copyright">
                            &copy;2011 Knowience Insights
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td width="150" align="center" class="botnav">
                <a href="#" target="_blank">
                    <img src="images/homeimages/knowince_logo.gif" alt="Knowience website" height="26" border="0" /></a>
            </td>
        </tr>
    </table>
    </form>
</body>
<script type="text/javascript">
    function ApplyFrameBox(link) {
        var frameWidth = $(link).attr('w');
        var frameHeight = $(link).attr('h');
        var scrolling = $(link).attr('scrolling');
        var href = $(link).attr('href');
        if (!scrolling) {
            scrolling = 'no';
        }
        $.facebox($('<iframe id="modalFrame" name="modalFrame" src="' + href + '" width="' + frameWidth + '" height="' + frameHeight + '" frameborder="0" scrolling="' + scrolling + '"></iframe>'));

        return false;
    }
</script>
</html>
