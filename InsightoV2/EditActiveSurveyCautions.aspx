﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="EditActiveSurveyCautions.aspx.cs" Inherits="EditActiveSurveyCautions"
    EnableEventValidation="true"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="wrapper">
        <div class="contentPanelHeader">
            <!-- form header panel -->
            <div class="pageTitle">
                <!-- page title -->
                <%--<span>Edit Active Survey</span>--%>
                <asp:Label ID="lblEditActiveSurvey" meta:resourcekey="lblEditActiveSurvey" runat="server"></asp:Label>  
                <!-- //page title -->
            </div>
            <div class="previewPanel">
                <!-- preview link -->
                <!-- //preview link -->
            </div>
            <!-- //form header panel -->
        </div>
        <div class="clear">
        </div>
        <div class="contentPanel">
            <!-- content panel -->
            <p class="defaultHeight">
            </p>
            <div style="text-align: right">
                <asp:Button ID="Button1" runat="server" Text="Proceed" CssClass="dynamicButton" OnClick="btnProceedUp_Click"
                    ToolTip="Proceed" meta:resourcekey="Button1Resource1" />
                <asp:Button ID="Button2" Text="Cancel" runat="server" CssClass="dynamicButton" OnClick="btnCancelUp_Click"
                    ToolTip="Cancel" meta:resourcekey="Button2Resource1" />
            </div>
            <p class="defaultHeight">
            </p>
            <div class="informationPanelDefault" id="dvInformationPanelDefult"  runat="server">
                
            </div>
            <div class="clear">
            </div>
            <div class="surveyEditLeftPanel" id="dvEditLefPanel" runat="server" >
              
            </div>
            <div class="surveyEditRightPanel" id="dvEditRightPanel" runat ="server">
               
            </div>
            <div class="clear">
            </div>
            <div class="bottomButtonPanel" style="text-align: right">
                <asp:Button ID="btnProceedBottom" runat="server" Text="Proceed" CssClass="dynamicButton"
                    OnClick="btnProceedBottom_Click" ToolTip="Proceed" 
                    meta:resourcekey="btnProceedBottomResource1" />
                <asp:Button ID="btnCancelBottom" Text="Cancel" runat="server" CssClass="dynamicButton"
                    OnClick="btnCancelBottom_Click" ToolTip="Cancel" 
                    meta:resourcekey="btnCancelBottomResource1" />
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</asp:Content>
