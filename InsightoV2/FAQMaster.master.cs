﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Helpers;

public partial class FAQMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        NavigateURLS();
        Hashtable ht1 = new Hashtable();
        ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
        string page=string.Empty;

        if (Request.QueryString["page"] != null)
            page = Request.QueryString["page"];
        else if(ht1 != null)
             page = ht1["page"].ToString(); 

            if (page == "Create")
            {
                DesignLeftMenu.Visible = false;
                LaunchLeftMenu.Visible = false;
                AnalyzeLeftMenu.Visible = false;
                ManageLeftMenu.Visible = false;
                CreateLeftMenu.Visible = true;
                PpsLeftMenu.Visible = false;
               lnkCreate.CssClass = "activelink";  
            }
            else if (page == "Design")
            {
                PpsLeftMenu.Visible = false;
                DesignLeftMenu.Visible = true;
                LaunchLeftMenu.Visible = false;
                AnalyzeLeftMenu.Visible = false;
                ManageLeftMenu.Visible = false;
                CreateLeftMenu.Visible = false;
                lnkDesign.CssClass = "activelink";
            }

            else if (page == "Launch")
            {
                PpsLeftMenu.Visible = false;
                DesignLeftMenu.Visible = false;
                LaunchLeftMenu.Visible = true;
                AnalyzeLeftMenu.Visible = false;
                ManageLeftMenu.Visible = false;
                CreateLeftMenu.Visible = false;
                lnkLaunch.CssClass = "activelink";
            }
            else if (page == "Analyze")
            {
                PpsLeftMenu.Visible = false;
                DesignLeftMenu.Visible = false;
                LaunchLeftMenu.Visible = false;
                AnalyzeLeftMenu.Visible = true;
                ManageLeftMenu.Visible = false;
                CreateLeftMenu.Visible = false;
                lnkAnalyze.CssClass = "activelink";
            }
            else if (page == "Manage")
            {
                PpsLeftMenu.Visible = false;
                DesignLeftMenu.Visible = false;
                LaunchLeftMenu.Visible = false;
                AnalyzeLeftMenu.Visible = false;
                ManageLeftMenu.Visible = true;
                CreateLeftMenu.Visible = false;
                lnkManage.CssClass = "activelink";
            }
            else if (page == "Pps")
            {
                PpsLeftMenu.Visible = true;
                DesignLeftMenu.Visible = false;
                LaunchLeftMenu.Visible = false;
                AnalyzeLeftMenu.Visible = false;
                ManageLeftMenu.Visible = false;
                CreateLeftMenu.Visible = false;
                lnkPps.CssClass = "activelink";
            }
            else
            {
                PpsLeftMenu.Visible = false;
                DesignLeftMenu.Visible = false;
                LaunchLeftMenu.Visible = false;
                AnalyzeLeftMenu.Visible = false;
                ManageLeftMenu.Visible = false;
                CreateLeftMenu.Visible = true;
                lnkCreate.CssClass = "activelink";
            }
    }
    protected void NavigateURLS()
    {
        lnkCreate.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Faqcms/CNS_AboutSurvey.aspx", "page=Create&pageName=About survey creation&parentNode=Create New Survey");
        lnkDesign.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Faqcms/Design_Theme.aspx", "page=Design&pageName=Theme&parentNode=Choose look and feel");
        lnkAnalyze.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Faqcms/Analyze_Reports.aspx", "page=Analyze&pageName=Reports&parentNode=View Reports");
        lnkLaunch.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Faqcms/Launch_ScheduleLaunch.aspx", "page=Launch&pageName=Schedule launch&parentNode=Survey / Alerts Scheduler");
        lnkManage.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Faqcms/Manage_SurveyOverview.aspx", "page=Manage&pageName=Overview&parentNode=My Surveys");
        lnkPps.NavigateUrl = EncryptHelper.EncryptQuerystring("~/Faqcms/Pps survey_Overview.aspx", "page=Pps&pageName=Overview&parentNode=Single surveys");
    }
}
