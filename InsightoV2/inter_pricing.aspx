﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.master" AutoEventWireup="true" CodeFile="inter_pricing.aspx.cs" Inherits="inter_pricing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Price plan - Insighto</title>
<link href="../App_Themes/Classic/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">
	<%--<div id="header_main">
    	<a href="http://insighto.com" class="header_logo"></a>
    </div>--%>
    <div class="clr"></div>
    <%--<div id="nav_main">
      	<ul>
            <li><a href="../../Home.aspx">Home</a></li>
            <li><a href="whyinsighto.aspx?key=Ghp2rQFUHwIqTRm%2fsyV7zcJen%2fadfzIY">Why Insighto</a></li>
            <li><a href="TakeaTour.aspx">Take a Tour</a></li>
            <li><a href="price_plan.html" class="selected">Plans &amp; Pricing</a></li>
            <li><a href="http://blog.insighto.com:8021/">Blog</a></li>
		</ul>
        <div id="nav_rgt">
        	<a href="#" class="selected">Login</a>
        </div>
   		<div class="clr"></div>
    </div>--%>
    
    <div id="container_main">
        <div class="thx_txt">
            Thank you for registering with Insighto.com. <br />
            Choose one of the following options and proceed.
        </div>
    	<div class="priceplan_blk">
        	
            <!--  Basic Price Blk  -->
        
        	<div class="price_1_blk">
            	<h3>Basic</h3>
                <div class="price_1_blk_cont">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header">
                                    <div class="price_tag_free">Free <br /> for<br /> LIFE</div>
                                    <div class="price"><%--<span>`</span>0--%>
                                    <asp:Label ID="lblfree" runat="server" Text="" /></div>
                                    per month
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td><asp:Button ID="button1" runat="server" Text="Continue" CssClass="btn_bg" onclick="button1_Click" 
                                    />
                            <%--<input type="submit" name="button" id="button" value="Continue" class="btn_bg" />--%></td>
                          </tr>
                          <tr>
                            <td class="no_border txt_green">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="txt_green">&nbsp;</td>
                          </tr>
                          <tr>
                            <td>Unlimited number of surveys</td>
                          </tr>
                          <tr>
                            <td>15 questions per survey</td>
                          </tr>
                          <tr>
                            <td>150 responses per survey</td>
                          </tr>
                          <tr>
                            <td>18 question types</td>
                          </tr>
                          <tr>
                            <td>Survey templates</td>
                          </tr>
                          <tr>
                            <td>Choice of themes</td>
                          </tr>
                          <tr>
                            <td>Launch through weblink</td>
                          </tr>
                          <tr>
                            <td>Report in data tables</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><a href="features.aspx">View all features</a></td>
                          </tr>
                          <tr>
                            <td class="no_border">
                                <asp:Button ID="button2" runat="server" Text="Continue" 
                                    CssClass="btn_bg" onclick="button2_Click"  />
                            <%--<input type="submit" name="button" id="button" value="Continue" class="btn_bg" />--%></td>
                          </tr>
                    </table>
                </div>

            </div>
            <!--  Basic Price Blk End  -->
        	
            <!--  Pro Blk  -->
        
        	<div class="price_1_blk">
            	<h3>Pro</h3>
                <div class="price_1_blk_cont">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header green">
                                    <div class="price"><asp:Label ID="lblpro" runat="server" Text="" CssClass="price" /><%--<span>`</span>4,500--%></div>
                                   	per annum
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border txt_underline"><a id="PayMonth" runat="server" >
                            <asp:Label ID="lblMonthly" runat="server" Text="Monthly Plan - <span class='font_ruppe'>`</span>450"/></a>
                            <%--Monthly Plan - <span class="font_ruppe">`</span>450--%></td>
                          </tr>
                          <tr>
                            <td><asp:Button ID="button3" runat="server" Text="Buy Now" CssClass="btn_bg" onclick="button3_Click" 
                                    />
                            <%--<input type="submit" name="button" id="button" value="Buy Now" class="btn_bg" />--%></td>
                          </tr>
                          <tr>
                            <td class="no_border txt_green">All features of Basic </td>
                          </tr>
                          <tr>
                            <td class="txt_green">+</td>
                          </tr>
                          <tr>
                            <td>Unlimited number of surveys</td>
                          </tr>
                          <tr>
                            <td>Unlimited number of questions</td>
                          </tr>
                          <tr>
                            <td>Unlimited responses</td>
                          </tr>
                          <tr>
                            <td>20 question types</td>
                          </tr>
                          <tr>
                            <td>Insert brand logo</td>
                          </tr>
                          <tr>
                            <td>Personalize buttons</td>
                          </tr>
                          <tr>
                            <td>Personalize survey end message</td>
                          </tr>
                          <tr>
                            <td>Schedule alerts</td>
                          </tr>
                          <tr>
                            <td>Print survey</td>
                          </tr>
                          <tr>
                            <td>Save survey to Word</td>
                          </tr>
                          <tr>
                            <td>Launch via Insighto emails</td>
                          </tr>
                          <tr>
                            <td>Report in data and animated graphs</td>
                          </tr>
                          <tr>
                            <td>Export data to Excel, PPT, PDF and Word</td>
                          </tr>
                          <tr>
                            <td>Cross Tab</td>
                          </tr>
                          <tr>
                            <td><a href="features.aspx">View all features</a></td>
                          </tr>
                          <tr>
                            <td class="no_border">
                                <asp:Button ID="button4" runat="server" Text="Buy Now" 
                                    CssClass="btn_bg" onclick="button4_Click"  />
                            <%--<input type="submit" name="button" id="button" value="Buy Now" class="btn_bg" />--%></td>
                          </tr>
                    </table>
                </div>

            </div>
            <!--  Pro Blk End  -->
        	
            
            <!--  Premium Blk  -->
        
        	<div class="price_1_blk">
            	<h3>Premium</h3>
                <div class="price_1_blk_cont">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="no_border">
                                <div class="price_tag_header orange">
                                    <div class="price"><asp:Label ID="lblpremium" runat="server" Text="" CssClass="price" /><%--<span>`</span>9,900--%></div>
                                   	per annum
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="no_border">Only Annual Plan</td>
                          </tr>
                          <tr>
                            <td><asp:Button ID="button5" runat="server" Text="Buy Now" CssClass="btn_bg" 
                                    onclick="button5_Click" />
                            <%--<input type="submit" name="button" id="button" value="Buy Now" class="btn_bg" />--%></td>
                          </tr>
                          <tr>
                            <td class="no_border txt_green">All features of Pro </td>
                          </tr>
                          <tr>
                            <td class="txt_green">+</td>
                          </tr>
                          <tr>
                            <td>Export raw data</td>
                          </tr>
                          <tr>
                            <td>View individual responses</td>
                          </tr>
                          <tr>
                            <td>Video and image questions</td>
                          </tr>
                          <tr>
                            <td>21 question types</td>
                          </tr>
                          <tr>
                            <td>URL redirection</td>
                          </tr>
                          <tr>
                            <td>Online slideshow of animated graphs</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="no_border">&nbsp;</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><a href="features.aspx">View all features</a></td>
                          </tr>
                          <tr>
                            <td class="no_border"><asp:Button ID="button6" runat="server" Text="Buy Now" 
                                    CssClass="btn_bg" onclick="button6_Click" />
                            <%--<input type="submit" name="button" id="button" value="Buy Now" class="btn_bg" />--%></td>
                          </tr>
                    </table>
                </div>

            </div>
            
            <!--  Premium Blk End  -->
        
        	
   			<div class="clr"></div>

        </div>
   		<div class="clr"></div>
  </div>
    
    <%--<div id="footer_blk">
    	<div id="footerL">Copyright &copy; 2013 Knowience Insights</div>
        <div id="footerR">
            <ul>
                <li><a href="../../Home.aspx">Home</a></li>
                <li><a href="whyinsighto.aspx?key=Ghp2rQFUHwIqTRm%2fsyV7zcJen%2fadfzIY">Why Insighto</a></li>
                <li><a href="price_plan.html" class="selected">Plans &amp; Pricing</a></li>
                <li><a href="aboutus.aspx?key=JUQ5UePXAPzI3eI%2b40zxvA%3d%3d">About Us</a></li>
                <li><a href="../In/ContactUs.aspx">Contact Us</a></li>
                <li><a href="antispam.aspx?key=6auQ29rICLYtdOGhX9UHsQ%3d%3d">Anti spam policy</a></li>
                <li><a href="privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy policy</a></li>
            </ul>
        </div>
   		<div class="clr"></div>
    </div>--%>
</div>
</body>

<!-- Google Code for Lead_signup Conversion Page --> 
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1027814833;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "dEWdCImw7gIQseuM6gM"; var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript"  src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  src="//www.googleadservices.com/pagead/conversion/1027814833/?value=0&amp;label=dEWdCImw7gIQseuM6gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


</html>

</asp:Content>

