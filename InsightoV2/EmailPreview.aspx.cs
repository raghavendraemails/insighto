﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.ValueObjects;
using System.Web.Script.Serialization;
using System.Net;
using System.Text;

public partial class EmailPreview : SurveyPageBase
{
    Hashtable typeHt = new Hashtable();
    string review_url = "";
    string image = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
           
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (typeHt.Contains("image"))
            {
                image = Convert.ToString(typeHt["image"]);
            } 

        }

        string templatename = "SurveysEmailInvitationPreview";
        string emailinvitation="";
        string imagepath="";
        string strEmailInvitation = "";
        if (image == "available")
        {
            try
            {
                string surveylogo = base.SurveyBasicInfoView.SURVEY_LOGO;
                int getlength = surveylogo.IndexOf(">");
                string geturl = surveylogo.Substring(1, getlength - 1);

                imagepath = ConfigurationManager.AppSettings["RootURL"].ToString() + geturl;
                //  imagepath = "http://127.0.0.1:8010/in/UserImages/6808/9098/16806885-ce32-4ad2-9f3a-ede13f497464_medium_PPS Premium Apply.png";
            }
            catch 
            {
                imagepath = "";
            }
          
        }
        if (image == "notavailable")
        {
            imagepath = "";
        }
      
       

        if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
             review_url = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewLinkURL(), Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag);
           
            strEmailInvitation += base.SurveyBasicInfoView.EMAIL_INVITATION;
            if (!strEmailInvitation.Contains("$$Start_Survey"))
            {
                strEmailInvitation += "<p> <a  target='_blank' href='" + ConfigurationManager.AppSettings["RootURL"].ToString() + review_url + "'><span style='font-weight: bold;'><img src='http://insighto.com/images/ButtonStartSurvey.png' /><span></a></p>";
            }

            //divEmailContent.InnerHtml = strEmailInvitation;
            //divEmailContent.Style.Add("family", base.SurveyBasicInfoView.SURVEY_FONT_TYPE);
            //divEmailContent.Style.Add("font-size", BuildQuestions.GetFontSizeValue(base.SurveyBasicInfoView.SURVEY_FONT_SIZE));
            //divEmailContent.Style.Add("color",BuildQuestions.GetFontColor(base.SurveyBasicInfoView.SURVEY_FONT_COLOR));
        }
        emailinvitation = strEmailInvitation;
        var loggedInUser = Session["UserInfo"] as LoggedInUserInfo;
        string viewData1;
        JavaScriptSerializer js1 = new JavaScriptSerializer();
        js1.MaxJsonLength = int.MaxValue;
        clsjsonmandrill prmjson1 = new clsjsonmandrill();
        //  var prmjson1 = new jsonmandrill();
        prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
        prmjson1.name = templatename;
        viewData1 = js1.Serialize(prmjson1);

        string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";


        WebClient request1 = new WebClient();
        request1.Encoding = System.Text.Encoding.UTF8;
        request1.Headers["Content-Type"] = "application/json";
        byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));

        string response1 = System.Text.Encoding.ASCII.GetString(resp1);


        clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);

        string bodycontent = jsmandrill11.code;


        string viewData;
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = int.MaxValue;
        var prmjson = new clsjsonmandrill.jsonmandrillmerge();
        prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
        prmjson.template_name = templatename;


        List<clsjsonmandrill.template_content> mtags = new List<clsjsonmandrill.template_content>();
        mtags.Add(new clsjsonmandrill.template_content { name = "std_content00", content = "<div><img alt='' src='*|image_path|*' style='max-width: 150px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;' class='mcnImage' align='middle' width='150' height='100'></div>" });

        mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div> *|EMAIL_INVITE|*</div>" });

        List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
        mvars.Add(new clsjsonmandrill.merge_vars { name = "image_path", content = imagepath });
        Hashtable ht = new Hashtable();
        ht.Add("$$Start_Survey", "<p> <a  target='_blank' href='" + ConfigurationManager.AppSettings["RootURL"].ToString() + review_url + "'><span style='font-weight: bold;'><img src='http://insighto.com/images/ButtonStartSurvey.png' /><span></a></p>");
        //ht.Add("$$ACTIVATION_LINK$$", urlstr);http://127.0.0.1:8010/LaunchSurvey.aspx?key=2k9Zm%2fj8T%2bTb5eUjbOF33FMCM6q8JlccPjgUfgbg5gU%3d#
        //ht.Add("$$PASSWORD$$", "Password: " + Decrypt(Password));
        emailinvitation = ContentParser(emailinvitation, ht);
     


        mvars.Add(new clsjsonmandrill.merge_vars { name = "EMAIL_INVITE", content = emailinvitation });

        prmjson.template_content = mtags;
        prmjson.merge_vars = mvars;


        viewData = js.Serialize(prmjson);


        string url = "https://mandrillapp.com/api/1.0/templates/render.json";


        WebClient request = new WebClient();
        request.Encoding = System.Text.Encoding.UTF8;
        request.Headers["Content-Type"] = "application/json";
        byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

        string response = System.Text.Encoding.ASCII.GetString(resp);

        string unesc = System.Text.RegularExpressions.Regex.Unescape(response);

        string first4 = unesc.Substring(0, 9);

        string last2 = unesc.Substring(unesc.Length - 2, 2);


        string unesc1 = unesc.Replace(first4, "");
        string unescf = unesc1.Replace(last2, "");
       

        // clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);

        //   string bodycontent = jsmandrill1.code;


        divEmailContent.InnerHtml = unescf;



    }
    public Hashtable GetGeneralLinks(Hashtable ht, int userid)
    {

        string urlstr_contactus = EncryptHelper.EncryptQuerystring(review_url + "/In/contactus.aspx", "UserId=" + userid + "&Qugm=1");
        string faqsurl = string.Format("{0}{1}", review_url, "/Faqcms/CNS_AboutSurvey.aspx?key=O1IiHYYZiuqe67HGRNzwF7j%2btdpCU6IQFvQJRPSFZFVdz1jysLODHqPiBlwB3os07lCVCHakwMBdvY2JVZRBS7CpbynHlOEZ");
        ht.Add("$$FEEDBACKURL$$", urlstr_contactus);
        ht.Add("$$FAQURL$$", faqsurl);
        return ht;
    }

    public string ContentParser(string Contnt, Hashtable ht)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(Contnt);
        if (ht.Count > 0 && Contnt.Trim().Length > 0)
        {
            foreach (string key in ht.Keys)
            {
                while (sb.ToString().Contains(key))
                {
                    sb.Replace(key, Convert.ToString(ht[key]));
                }
            }
        }
        return sb.ToString();
    }
}