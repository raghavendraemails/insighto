﻿using System;
using System.Configuration;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using CovalenseUtilities.Helpers;

namespace Insighto.Admin.Pages
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        #region Events
        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region ResetPassword Click Event
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            var userService = ServiceFactory.GetService<UsersService>();
            var userInfo = userService.FindUserByAdminEmailId(txtEmail.Text.Trim());
            if (userInfo != null)
            {
                var sendEmailService = ServiceFactory.GetService<SendEmailService>();
                string password = GetRandomPasswordUsingGUID(6);
                password = password.Substring(0, 6);
                string fromEmail = ConfigurationManager.AppSettings["emailActivation"];
                userService.UpdateAdminPassword(Convert.ToDecimal(userInfo.ADMINUSERID), password, 1);
                sendEmailService.ForgotPassword("Forgot Password", userInfo.FIRST_NAME, fromEmail, password, txtEmail.Text.Trim());

                dvSuccessMsg.Visible = true;
                dvErrMsg.Visible = false;

            }
            else
            {
                dvSuccessMsg.Visible = false;
                dvErrMsg.Visible = true;



            }
        }
        #endregion
        #endregion
       
        #region Methods
        #region GetRandomPasswordUsingGUID
        /// <summary>
        /// 
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public string GetRandomPasswordUsingGUID(int length)
        {
            // Get the GUID
            string guidResult = System.Guid.NewGuid().ToString();
            // Remove the hyphens
            guidResult = guidResult.Replace("-", string.Empty);
            // Make sure length is valid
            if (length <= 0 || length > guidResult.Length)
                throw new ArgumentException("Length must be between 1 and " + guidResult.Length);
            // Return the first length bytes        
            return guidResult.Substring(0, length);
        }
        #endregion
        #endregion
    }
}