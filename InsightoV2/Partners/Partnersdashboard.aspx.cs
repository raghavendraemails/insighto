﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class partners_Partnersdashboard : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
               
    }

    protected void btnpartnerlist_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
        }
        else
        {
            partnerslist.Visible = true;
            create.Visible = false;
            salessummary.Visible = false;
        }
    }
    protected void btncreatepartner_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
        }
        else
        {
            create.Visible = true;
            partnerslist.Visible = false;
            salessummary.Visible = false;
        }
    }
    protected void btnsalessummary_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
        }
        else
        {
            salessummary.Visible = true;
            create.Visible = false;
            partnerslist.Visible = false;
        }
         }
}