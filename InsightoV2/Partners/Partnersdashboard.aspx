﻿<%@ Page Title="" Language="C#" MasterPageFile="~/partners/ShineMaster.master" AutoEventWireup="true" CodeFile="Partnersdashboard.aspx.cs" Inherits="partners_Partnersdashboard" %>
<%@ Register TagPrefix="uc" TagName="Create" Src="~/partners/usercontros/Createpartners.ascx" %>
<%@ Register TagPrefix="uc" TagName="partnerslist" Src="~/partners/usercontros/Partnerslist.ascx" %>
<%@ Register TagPrefix="uc" TagName="salessummary" Src="~/partners/usercontros/Salessummary.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
  <div id="topMainMenu">
            <!-- top main menu -->
            <!-- top main menu -->
        </div>
<table width="100%">
<tr>
<td width="20%">
</td>
<td width="20%">
    <asp:Button ID="btnpartnerlist" runat="server" Text="Partner List" Width="187px" 
        Height="42px"  Font-Bold="True" onclick="btnpartnerlist_Click" />
</td>
<td width="20%">
    <asp:Button ID="btncreatepartner" runat="server" Text="Create Partner" Width="187px" 
        Height="42px"  Font-Bold="True" onclick="btncreatepartner_Click" />
</td>
<td width="20%">
    <asp:Button ID="btnsalessummary" runat="server" Text="Sales Summary" Width="187px" 
        Height="42px" Font-Bold="True" onclick="btnsalessummary_Click" />
</td>
<td width="20%">
</td>
</tr>
</table>
<uc:Create ID="create" runat="server" Visible="false" />
<uc:partnerslist ID="partnerslist" runat="server" Visible="false" />
<uc:salessummary ID="salessummary" runat="server" Visible="false" />
</asp:Content>

