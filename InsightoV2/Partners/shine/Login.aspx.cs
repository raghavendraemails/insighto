﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business;
using Insighto.Business.Services;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
namespace Insighto.Pages.Admin
{
    public partial class Login : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);
            strconn.Open();
            SqlCommand scom = new SqlCommand("sp_insighto_partnerslogin", strconn);
            scom.CommandType = CommandType.StoredProcedure;
            scom.Parameters.Add(new SqlParameter("@Login_name", SqlDbType.NVarChar)).Value = txtUserName.Text.Trim();
            scom.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar)).Value = txtPassword.Text.Trim();
            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet ds = new DataSet();
            sda.SelectCommand = scom;
            sda.Fill(ds, "partnerdetails");
            int data = ds.Tables["partnerdetails"].Rows.Count;
            if (data != 0)
            {
                string partnerorg = ds.Tables[0].Rows[0][1].ToString();
                HttpCookie c1 = new HttpCookie("UserInfo");
                c1.Values.Add("UserName", txtUserName.Text);
                c1.Values.Add("Password", txtPassword.Text);
                c1.Values.Add("Organization", partnerorg);
                DateTime dt = DateTime.Now;
                c1.Expires = dt.AddMinutes(10);
                Response.Cookies.Add(c1);
                Session["Organization"] = partnerorg;
               // Server.Transfer("Shine_dashboard.aspx");

                Response.Redirect("Shine_dashboard.aspx");
            }
            else
            {
                dvErrMsg.Visible = true;
            }
        }
}
    
}