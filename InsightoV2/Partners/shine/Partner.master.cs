﻿using System;
using App_Code;

public partial class Home : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lnkFavIcon.Href = Utilities.GetAbsoluteUrl("~/insighto.ico", Page);
           
        }
    }

   
}
