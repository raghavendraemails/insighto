<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Insighto.Pages.Admin.Login" meta:resourcekey="PageResource1" %>
<%@ Register TagPrefix="uc" TagName="TopLinks" Src="~/UserControls/AdminControls/TopLinks.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
     <title>Free Online Survey and Questionnaire Software for Customer Research and Feedback</title>
    <link href="../../App_Themes/Classic/layoutStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../App_Themes/Classic/adminStyle.css" rel="stylesheet" type="text/css" />
      <link href="../../App_Themes/Classic/facebox.css" media="screen" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../images/homeimages/jquery.js"></script>
    <script type="text/javascript" src="../../images/homeimages/thickbox.js"></script>
    <script type="text/javascript" src="../../JQuery/banner_ jquery.js"></script>
    <script type="text/javascript" src="../../jQuery/banner_jquery-ui.js"></script>
    
    <script type="text/javascript" src="../../jQuery/banner_slider.js"></script>
    <script type="text/javascript" src="../../Scripts/facebox.js"></script>
    <script type="text/javascript" src="../../Scripts/common-functions.js"></script>
    <style type="text/css" media="screen">
        @import "../../images/homeimages/thickbox.css";
        @import "../../images/homeimages/insighto.css";
    </style>
 
 <link rel="icon" type="image/icon" href="../../insighto.ico" />
    <!--[if lte IE 6]>
<link href="css/IE6.css" rel="stylesheet" type="text/css" />
<![endif]-->

</head>
<body>
    <form runat="server">
    <div id="wrapper">
        <!-- main wrapper -->
       <uc:TopLinks ID="ucTopLinks" runat="server" />
        <div id="header">
            <!-- header -->
            <div class="insightoLogoPanel">
                <!-- insighto logo -->
                <a href="login.aspx">
                    <img src="../../App_Themes/Classic/Images/logo-insighto.jpg" width="184" height="60"
                        alt="Insighto" title="Insighto" /></a>
                <!-- insighto logo -->
            </div>
            <div class="insightoTagLine">
            </div>
            <!-- //header -->
        </div>
        <div id="topMainMenu">
            <!-- top main menu -->
            <!-- top main menu -->
        </div>
        <div class="adminLoginPanel">
            <!-- signup panel -->
            <h3>
            <asp:Label ID="lblHeading" runat="server" Text="Administrator Login"  meta:resourcekey="lblHeading"></asp:Label>
               </h3>
               <p class="defaultHeight"></p>
            <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" Text="Invalid user name or password."></asp:Label></div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="lblUserName" runat="server" AssociatedControlID="txtUserName" text="User Name"></asp:Label>
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="txtUserName"  class="textBoxLogin" runat="server" 
                        meta:resourcekey="txtUserNameResource1" MaxLength="50" ></asp:TextBox>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqEmailId" 
                        ControlToValidate="txtUserName" CssClass="lblRequired"
                        runat="server" Display="Dynamic" 
                        Text="Enter EmailId"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator SetFocusOnError="True"  ID="regEmailId" 
                        runat="server" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                        CssClass="lblRequired" ControlToValidate="txtUserName"
                        Display="Dynamic" Text="Enter valid Email"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    <asp:Label ID="lblPassword" runat="server"
                        AssociatedControlID="txtPassword" text="PassWord"></asp:Label>
                </div>
                <div class="loginControls">
                    <asp:TextBox ID="txtPassword" TextMode="Password" class="textBoxLogin" 
                        runat="server" meta:resourcekey="txtPasswordResource1" MaxLength="50"></asp:TextBox>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:RequiredFieldValidator SetFocusOnError="True" ID="req" 
                        ControlToValidate="txtPassword" CssClass="lblRequired"
                        runat="server" Display="Dynamic" 
                        Text="Enter password"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                </div>
                <div class="loginControls">
                    <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" CssClass="dynamicButton"
                        Text="Login" meta:resourcekey="btnLoginResource1" />
                </div>
            </div>
            <div class="con_login_pnl">
        <div class="reqlbl btmpadding">
        </div>
    </div>
             <div class="con_login_pnl">
                <div class="reqlbl btmpadding">        
                 <%--<asp:HyperLink ID="hlkForgotPassword"  style="color: #6c59a2; font-size: 13px"  rel="framebox" h="300" w="700"
                            scrolling='yes' title='Copy' onclick='javascript: return ApplyFrameBox(this);' NavigateUrl="ForgotPassword.aspx"
                            ToolTip="Forgot Password?" runat="server" Text="Forgot Password?" 
                            meta:resourcekey="hlnkAddListResource1"></asp:HyperLink>--%>
               
                                        </div>
            </div>
            <div class="clear">
            </div>
            <!-- //signup panel -->
        </div>
        <div class="clear">
        </div>
        <div id="footer">
            <!-- footer -->
            <div class="copyRights">
            <asp:Label ID="lblFooter" runat="server" meta:resourcekey="lblFooterResource1"></asp:Label>               
            </div>
            <!-- //footer -->
        </div>
        <!-- //main wrapper -->
    </div>
    </form>
</body>
</html>
