﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class partners_shine_Default : System.Web.UI.Page
{
    string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
    string Revenushare = ConfigurationManager.AppSettings["Revenueshare"];
    
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
        {
            HttpCookie c1 = Request.Cookies["UserInfo"];
            if (c1 == null)
            {
                Server.Transfer("Login.aspx");
            }
            else
            {
                string org_name = Session["Organization"].ToString();
                
                lbladmin.Text = "Welcome " + org_name + " admin";
                
            }
        }
        gridviewsearch();
    }

    public void gridviewsearch()
    {
        string org_name = Session["Organization"].ToString();

        SqlConnection strconn = new SqlConnection(connStr);
        strconn.Open();
        SqlCommand scom = new SqlCommand("sp_partnerdetails", strconn);
        scom.CommandType = CommandType.StoredProcedure;
        scom.Parameters.Add(new SqlParameter("@Partner_name", SqlDbType.NVarChar)).Value = txtsearch.Text;
        scom.Parameters.Add(new SqlParameter("@Organization", SqlDbType.NVarChar)).Value = org_name;
        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet ds = new DataSet();
        sda.SelectCommand = scom;
        sda.Fill(ds, "partnersdetails");
        int data = ds.Tables["partnersdetails"].Rows.Count;
        if (data == 0)
        {
            lblnodata.Visible = true;
        }
        partnerdetails.DataSource = ds.Tables["partnersdetails"];
        partnerdetails.DataBind();
        strconn.Close();
        SqlConnection strconn1 = new SqlConnection(connStr);
        strconn1.Open();
        SqlCommand scom1 = new SqlCommand("sp_adminpartnerstotal", strconn1);
        scom1.CommandType = CommandType.StoredProcedure;
        scom1.Parameters.Add(new SqlParameter("@FIRST_NAME", SqlDbType.NVarChar)).Value = txtsearch.Text;
        scom1.Parameters.Add(new SqlParameter("@Organization", SqlDbType.NVarChar)).Value = org_name;
        SqlDataAdapter sda1 = new SqlDataAdapter(scom1);
        DataSet ds1 = new DataSet();
        sda1.SelectCommand = scom1;
        sda1.Fill(ds1, "shinetotal");
        string TotalpriceinINR = "";
        string TotalpriceinUSD = "";
        if (ds1.Tables[0].Rows.Count == 1 || ds1.Tables[0].Rows.Count == 2)
        {
            TotalpriceinUSD = ds1.Tables[0].Rows[0][1].ToString();
            if (TotalpriceinUSD == "")
                TotalpriceinUSD = "0";
        }
        else
        {
            TotalpriceinUSD = "0";
        }
        if (ds1.Tables[0].Rows.Count == 2)
        {
            TotalpriceinINR = ds1.Tables[0].Rows[1][0].ToString();
        }
        else
        {
            TotalpriceinINR = "0";
        }
        txtamount.Text = TotalpriceinINR;
        int revenue = (Convert.ToInt32(TotalpriceinINR) * Convert.ToInt32(Revenushare)) / 100;
        txttotalinusd.Text = TotalpriceinUSD;
       
    }
    public void gridviewsearchfordate()
    {
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        gridviewsearch();
    }
   
    protected void datesearchto_Click(object sender, EventArgs e)
    {
        string org_name = Session["Organization"].ToString();
        if (txtdate1.Text == "" || txtdate2.Text == "")
        {

            txtdate1.Text = "01/01/1880";
            txtdate2.Text = DateTime.Now.ToString();
        }
        string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
        SqlConnection strconn = new SqlConnection(connStr);
        strconn.Open();
        SqlCommand scom = new SqlCommand("sp_adminshinedatesearch", strconn);
        scom.CommandType = CommandType.StoredProcedure;
        scom.Parameters.Add(new SqlParameter("@startdate", SqlDbType.NVarChar)).Value = txtdate1.Text;
        scom.Parameters.Add(new SqlParameter("@enddate", SqlDbType.NVarChar)).Value = txtdate2.Text;
        scom.Parameters.Add(new SqlParameter("@Organization", SqlDbType.NVarChar)).Value = org_name; 

        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet ds = new DataSet();
        sda.SelectCommand = scom;
        sda.Fill(ds, "shinedetailsdate");
        int data = ds.Tables["shinedetailsdate"].Rows.Count;
        if (data == 0)
        {
            lblnodata.Visible = true;
        }
        else
        {
            lblnodata.Visible = false;
        }
        partnerdetails.DataSource = ds.Tables["shinedetailsdate"];
        partnerdetails.DataBind();
        SqlCommand scom2 = new SqlCommand("sp_partnerstotal", strconn);
        scom2.CommandType = CommandType.StoredProcedure;
        scom2.Parameters.Add(new SqlParameter("@startdate", SqlDbType.NVarChar)).Value = txtdate1.Text;
        scom2.Parameters.Add(new SqlParameter("@enddate", SqlDbType.NVarChar)).Value = txtdate2.Text;
        scom2.Parameters.Add(new SqlParameter("@Organization", SqlDbType.NVarChar)).Value = org_name;
        SqlDataAdapter sda1 = new SqlDataAdapter(scom2);
        DataSet ds1 = new DataSet();
        sda1.SelectCommand = scom2;
        sda1.Fill(ds1, "shinetotal");
        string TotalpriceinINR = "";
        string TotalpriceinUSD = "";
        if (ds1.Tables[0].Rows.Count == 1 || ds1.Tables[0].Rows.Count == 2)
        {
            TotalpriceinUSD = ds1.Tables[0].Rows[0][1].ToString();
            if (TotalpriceinUSD == "")
                TotalpriceinUSD = "0";
        }
        else
        {
            TotalpriceinUSD = "0";
        }
        if (ds1.Tables[0].Rows.Count == 2)
        {
            TotalpriceinINR = ds1.Tables[0].Rows[1][0].ToString();
        }
        else
        {
            TotalpriceinINR = "0";
        }
        txtamount.Text = TotalpriceinINR;
        int revenue = (Convert.ToInt32(TotalpriceinINR) * Convert.ToInt32(Revenushare)) / 100;
       
        txttotalinusd.Text = TotalpriceinUSD;

        if (txtdate1.Text == "01/01/1880" || txtdate2.Text == DateTime.Now.ToString())
        {
            txtdate1.Text = "";
            txtdate2.Text = "";
        }
    }
   
}