<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Shine_dashboard.aspx.cs" Inherits="partners_shine_Default" %>
<%@ Register TagPrefix="uc" TagName="TopLinks" Src="~/UserControls/AdminControls/TopLinks.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
     <title>Free Online Survey and Questionnaire Software for Customer Research and Feedback</title>
    <link href="../../App_Themes/Classic/layoutStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../App_Themes/Classic/adminStyle.css" rel="stylesheet" type="text/css" />
      <link href="../../App_Themes/Classic/facebox.css" media="screen" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../images/homeimages/jquery.js"></script>
    <script type="text/javascript" src="../../images/homeimages/thickbox.js"></script>
    <script type="text/javascript" src="../../JQuery/banner_ jquery.js"></script>
    <script type="text/javascript" src="../../jQuery/banner_jquery-ui.js"></script>
    <script type="text/javascript" src="../../jQuery/banner_slider.js"></script>
    <script type="text/javascript" src="../../Scripts/facebox.js"></script>
    <script type="text/javascript" src="../../Scripts/common-functions.js"></script>
    <style type="text/css" media="screen">
        @import "../images/homeimages/thickbox.css";
        @import "../images/homeimages/insighto.css";
    </style>
 <link rel="icon" type="image/icon" href="../../insighto.ico" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#txtdate1").datepicker();
        $("#txtdate2").datepicker();
    });
</script>


    <!--[if lte IE 6]>
<link href="css/IE6.css" rel="stylesheet" type="text/css" />
<![endif]-->

</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <!-- main wrapper -->
       <uc:TopLinks ID="ucTopLinks" runat="server" />
       <table id="header">
       <tr>
       <td>
       <div class="insightoLogoPanel">
                <!-- insighto logo -->
                <a href="login.aspx">
                    <img src="../../App_Themes/Classic/Images/logo-insighto.jpg" width="184" height="60"
                        alt="Insighto" title="Insighto" /></a>
                <!-- insighto logo -->
            </div>
       </td>
       <td>
       <div class="insightoTagLine">
            </div>
       </td>
       <td width="360px">&nbsp;</td>
       <td align="right">
       <div id="header" align="right">       
         <asp:Label ID="lbladmin" runat="server" ForeColor="Red" Text=""></asp:Label>
            &nbsp;&nbsp; |&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HyperLink1" runat="server" ForeColor="#CC00CC" 
                NavigateUrl="~/partners/shine/Login.aspx">Logout</asp:HyperLink>
        </div>
       </td>
       
       </tr>
       </table>
        
           
  <div id="topMainMenu">
            <!-- top main menu -->
            <!-- top main menu -->
        </div>
<div>

</div>
   
<table width="100%">
<tr>
<td width="5%">
</td>
<td width="90%">
<table align="center">
<tr>
<td>
 <asp:TextBox ID="txtsearch" runat="server" Width="248px" class="textBoxAdminSearch"></asp:TextBox>
</td>

<td>
<asp:Button ID="btnsearch" runat="server" Text="Search" ToolTip="Search" CssClass="dynamicButton btnAdvanceSearch" Height="36px" onclick="btnsearch_Click" />
</td>
<td>&nbsp;</td>
<td>
<asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" Text="Filter By"></asp:Label>
</td>
<td>&nbsp;</td>
<td>
<asp:TextBox ID="txtdate1" runat="server" CssClass="textBoxDate txtCreatedDateFrom" meta:resourcekey="txtCreatedDateFromResource1" />
</td>
<td>
To
</td>
<td>
<asp:TextBox ID="txtdate2" runat="server" CssClass="textBoxDate txtCreatedDateTo" meta:resourcekey="txtCreatedDateToResource1" />
</td>
<td>
 <asp:Button ID="datesearchto" runat="server" Text="Search" ToolTip="Search" CssClass="dynamicButton btnAdvanceSearch" meta:resourcekey="btnAdvanceSearchResource1" Height="36px" onclick="datesearchto_Click"/>
</td>
</tr>
</table>
</td>
<td width="5%">
</td>
</tr>
<tr>
<td width="5%">
</td>
<td align="center">
<div>&nbsp;</div>
<div align="center">
    <asp:Label ID="lblnodata" runat="server" Text="....No Data found..." Visible="false"></asp:Label>
</div>

<div style="overflow-y: scroll; height: 350px">
  <asp:GridView ID="partnerdetails" runat="server"  CellPadding="4" 
            AutoGenerateColumns="False" Width="767px" 
        ForeColor="#333333" GridLines="None" 
            >
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
            <asp:TemplateField HeaderText="Name">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtreferredby" runat="server" Text='<%# Bind("FIRST_NAME") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblreferredby" runat="server" Text='<%# Bind("FIRST_NAME") %>'></asp:Label>
                    </ItemTemplate>
                     
                </asp:TemplateField>
               <%-- <asp:TemplateField HeaderText="Login ID">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtname" runat="server" Text='<%# Bind("LOGIN_NAME") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblname" runat="server" Text='<%# Bind("LOGIN_NAME") %>'></asp:Label>
                    </ItemTemplate>
                     
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Created on" >
                <EditItemTemplate>
                <asp:TextBox ID="txtloginid" runat="server" Text='<%# Bind("CREATED_ON") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblloginid" runat="server" Text='<%# Bind("CREATED_ON") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField >
                <asp:TemplateField HeaderText="Licence Type">
                <EditItemTemplate>
                <asp:TextBox ID="txtcreatedon" runat="server" Text='<%# Bind("LICENSE_TYPE") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblcreatedon" runat="server" Text='<%# Bind("LICENSE_TYPE") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Company">
                <EditItemTemplate>
                <asp:TextBox ID="txtproductpuchased" runat="server" Text='<%# Bind("COMPANY") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblproductpurchased" runat="server" Text='<%# Bind("COMPANY") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Product price in INR">
                <EditItemTemplate>
                <asp:TextBox ID="txtcompanyname" runat="server" Text='<%# Bind("priceinINR") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblcompanyname" runat="server" Text='<%# Bind("priceinINR") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Product price in USD">
                <EditItemTemplate>
                <asp:TextBox ID="txtcompanyname" runat="server" Text='<%# Bind("priceinUSD") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblcompanyname" runat="server" Text='<%# Bind("priceinUSD") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                 
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
            <HeaderStyle BackColor="#c3c3c3" Font-Bold="True" ForeColor="#B765C8" />
            <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="#284775" />
             <RowStyle BackColor="#F7F6F3" ForeColor="#333333"/>
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#B765C8" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    </div>
</td>
<td width="5%">
</td>

</tr>
<tr>
<td width="5%">
</td>
<td width="90%">
<div align="center">
    <asp:Label ID="lblamont" runat="server" Text="Total Amount" Font-Bold="True" 
        Font-Size="Small"></asp:Label>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtamount" runat="server"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Total Amount($)"></asp:Label>
&nbsp;
    <asp:TextBox ID="txttotalinusd" runat="server"></asp:TextBox>
    <br />
    <br />
    &nbsp;&nbsp;&nbsp;
    
&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;
    
</div>

</td>
<td width="5%">
</td>
</tr>



</table>
    
    </div>




    </form>
</body>
</html>
