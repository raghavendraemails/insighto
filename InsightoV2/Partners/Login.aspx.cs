﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business;
using Insighto.Business.Services;
namespace Insighto.Pages.Admin
{
    public partial class Login : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtUserName.Focus();
        }      
        protected void btnLogin_Click(object sender,EventArgs e)
        {
            if (Page.IsValid)
            {
                string userName = txtUserName.Text.Trim();
                string password = txtPassword.Text.Trim();
                var adminUserService = ServiceFactory.GetService<AdminUserService>();
                var adminUser = adminUserService.GetAuthenticatedUser(userName, password);
                if (adminUser != null)
                {
                    var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                    sessionStateService.SaveAdminUserToSession(adminUser);
                    if (adminUser.RESET == 0)
                    {

                        Response.Redirect("~/Partnersdashboard.aspx");
                    }
                    else
                    {
                        Response.Redirect("Changepassword.aspx");
                    }
                }
                else
                {
                   // lblErrMsg.Text = "Email id or password is incorrect. Please try again!";
                    dvErrMsg.Visible = true;
                }
            }
            else
            {
                dvErrMsg.Visible = false;
            }

        }
    }
}