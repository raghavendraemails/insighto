﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Partnerslist.ascx.cs" Inherits="partners_usercontros_Partnerslist" %>
 <table width="100%">
 <tr>
 <td width="5%"></td>
 <td width="90%">
&nbsp;&nbsp;
     &nbsp;
     </td>
 <td width="5%"></td>
 </tr>
 <tr>
 <td width="5%"></td>
 <td width="90%"></td>
 <td width="5%"></td>
 </tr>
 <tr>
 <td width="5%">
 </td>
 <td width="90%" align="center">
 <div align="center">
    <asp:Label ID="lblnodata" runat="server" Text="....No Data found..." Visible="false"></asp:Label>
</div>
 <div style="overflow-y: scroll; height: 350px">
 <asp:GridView ID="Grddetails" runat="server"     
         AutoGenerateColumns="False" style="height: 185px; width: 900px"
         onrowdeleting="Grddetails_RowDeleting" 
         onrowcancelingedit="Grddetails_RowCancelingEdit" 
         onrowediting="Grddetails_RowEditing" onrowcommand="Grddetails_RowCommand" 
         onselectedindexchanging="Grddetails_SelectedIndexChanging" 
         onselectedindexchanged="Grddetails_SelectedIndexChanged" CellPadding="4"
         ForeColor="#333333" GridLines="None"
            >
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                
                <asp:TemplateField HeaderText="ORGANIZATION">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtorganization" runat="server" Text='<%# Bind("Organization_name") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblorganization" runat="server" Text='<%# Bind("Organization_name") %>'></asp:Label>
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NAME">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtname" runat="server" Text='<%# Bind("Partner_name") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblname" runat="server" Text='<%# Bind("Partner_name") %>'></asp:Label>
                    </ItemTemplate>
                     
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LOGIN ID" >
                <EditItemTemplate>
                <asp:TextBox ID="txtloginid" runat="server" Text='<%# Bind("Login_name") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblloginid" runat="server" Text='<%# Bind("Login_name") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField >
                <%--<asp:TemplateField HeaderText="PASSWORD">
                <EditItemTemplate>
                <asp:TextBox ID="txtpassword" runat="server" Text='<%# Bind("Password") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblpassword" runat="server" Text='<%# Bind("Password") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>--%>

                <asp:TemplateField HeaderText="CREATED ON">
                <EditItemTemplate>
                <asp:TextBox ID="txtcreateddate" runat="server" Text='<%# Bind("Created_date") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblcreateddate" runat="server" Text='<%# Bind("Created_date") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="MODIFIED ON">
                <EditItemTemplate>
                <asp:TextBox ID="txtmodifieddate" runat="server" Text='<%# Bind("Modified_date") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblmodifieddate" runat="server" Text='<%# Bind("Modified_date") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
              <%--  <asp:TemplateField HeaderText="REVENUE EARNED">
                     <EditItemTemplate>
                <asp:TextBox ID="txtrevenue" runat="server" Text='<%# Bind("Modified_date") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblrvenue" runat="server" Text='<%# Bind("Modified_date") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>--%>
                 <asp:TemplateField HeaderText="STATUS">
                <EditItemTemplate>
                <asp:TextBox ID="txtstatus" runat="server" Text='<%# Bind("Status") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkactive" runat="server" CausesValidation="False" 
                             CommandName="Select" Text='<%# Bind("Status_type") %>'></asp:LinkButton>
                    </ItemTemplate>
                   
                </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="linkbtndelete" runat="server" CausesValidation="False" 
                            CommandName="Delete" Text="Delete"></asp:LinkButton>

                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnktotal" runat="server" CausesValidation="False" 
                            CommandName="Total" Text="Total"></asp:LinkButton>

                    </ItemTemplate>
                   
                </asp:TemplateField>
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#c3c3c3" Font-Bold="True" ForeColor="#B765C8" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#B765C8" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
   </div>    
<div align="center">
<div>&nbsp;</div>
<div>&nbsp;</div>
<table align="center">
<tr>
<td>
 <asp:Label ID="Label1" runat="server" Text="Total Amount"></asp:Label>&nbsp;&nbsp; <asp:TextBox ID="txtamountinr" runat="server"></asp:TextBox><br />
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
    <asp:Label ID="Label2" runat="server" Text="Total Amount($)"></asp:Label>&nbsp;&nbsp; <asp:TextBox ID="txtamountusd" runat="server"></asp:TextBox><br />

</td>
</tr>
</table>
   
</div>
 </td>
 <td width="5%">
 </td>
 </tr>
 
</table>
