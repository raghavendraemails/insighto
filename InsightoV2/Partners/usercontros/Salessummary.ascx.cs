﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class partners_usercontros_Salessummary : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            gridviewsearch();
        }
    }
    public void gridviewsearch()
    {
        string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
        SqlConnection strconn = new SqlConnection(connStr);
        strconn.Open();
        SqlCommand scom = new SqlCommand("sp_adminpartnersales", strconn);
        scom.CommandType = CommandType.StoredProcedure;
        scom.Parameters.Add(new SqlParameter("@FIRST_NAME", SqlDbType.NVarChar)).Value = txtsearch.Text;
        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet ds = new DataSet();
        sda.SelectCommand = scom;
        sda.Fill(ds, "salesdetails");
        int data = ds.Tables["salesdetails"].Rows.Count;
        if (data == 0)
        {
            lblnodata.Visible = true;
        }
        else
        {
            lblnodata.Visible = false;
        }
        Grddetails.DataSource = ds.Tables["salesdetails"];
        Grddetails.DataBind();
        strconn.Close();
        SqlConnection strconn1 = new SqlConnection(connStr);
        strconn1.Open();
        SqlCommand scom1 = new SqlCommand("sp_admintotal", strconn1);
        scom1.CommandType = CommandType.StoredProcedure;
        scom1.Parameters.Add(new SqlParameter("@FIRST_NAME", SqlDbType.NVarChar)).Value = txtsearch.Text;
        SqlDataAdapter sda1 = new SqlDataAdapter(scom1);
        DataSet ds1 = new DataSet();
        sda1.SelectCommand = scom1;
        sda1.Fill(ds1, "shinetotal");
        string TotalpriceinINR = "";
        string TotalpriceinUSD = "";

        if (ds1.Tables[0].Rows.Count == 1 || ds1.Tables[0].Rows.Count == 2)
        {
            TotalpriceinUSD = ds1.Tables[0].Rows[0][1].ToString();
            if (TotalpriceinUSD == "")
                TotalpriceinUSD = "0";
        }
        else
        {
            TotalpriceinUSD = "0";
        }
        if (ds1.Tables[0].Rows.Count == 2)
        {
            TotalpriceinINR = ds1.Tables[0].Rows[1][0].ToString();
        }
        else
        {
            TotalpriceinINR = "0";
        }

        txtamount.Text = TotalpriceinINR;
        txttotalusd.Text = TotalpriceinUSD;
       
        
        
    }
    public void gridviewsearchfordate()
    {
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        gridviewsearch();
    }

    protected void datesearchto_Click(object sender, EventArgs e)
    {

        if (txtstartdate.Text == "" || txtenddate.Text == "")
        {
            txtenddate.Text = DateTime.Now.ToShortDateString();
            txtstartdate.Text = "01/01/1880";

        }
        else
        {
            
        }
        string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
        SqlConnection strconn = new SqlConnection(connStr);
        strconn.Open();
        SqlCommand scom = new SqlCommand("sp_adminsalesdatesearch", strconn);
        scom.CommandType = CommandType.StoredProcedure;
        scom.Parameters.Add(new SqlParameter("@startdate", SqlDbType.NVarChar)).Value = txtstartdate.Text;
        scom.Parameters.Add(new SqlParameter("@enddate", SqlDbType.NVarChar)).Value = txtenddate.Text;

        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet ds = new DataSet();
        sda.SelectCommand = scom;
        sda.Fill(ds, "shinedetailsdate");
        
        int data = ds.Tables["shinedetailsdate"].Rows.Count;
        if (data == 0)
        {
            lblnodata.Visible = true;
        }
        else
        {
            lblnodata.Visible = false;
        }
        Grddetails.DataSource = ds.Tables["shinedetailsdate"];
        Grddetails.DataBind();
        SqlCommand scom2 = new SqlCommand("sp_salespartnerstotal", strconn);
        scom2.CommandType = CommandType.StoredProcedure;
        scom2.Parameters.Add(new SqlParameter("@startdate", SqlDbType.NVarChar)).Value = txtstartdate.Text;
        scom2.Parameters.Add(new SqlParameter("@enddate", SqlDbType.NVarChar)).Value = txtenddate.Text;
        SqlDataAdapter sda1 = new SqlDataAdapter(scom2);
        DataSet ds1 = new DataSet();
        sda1.SelectCommand = scom2;
        sda1.Fill(ds1, "shinetotal");
        string TotalpriceinINR = "";
        string TotalpriceinUSD = "";
        if (ds1.Tables[0].Rows.Count == 1 || ds1.Tables[0].Rows.Count == 2)
        {
            TotalpriceinUSD = ds1.Tables[0].Rows[0][1].ToString();
            if (TotalpriceinUSD == "")
                TotalpriceinUSD = "0";
        }
        else
        {
            TotalpriceinUSD = "0";
        }
        if (ds1.Tables[0].Rows.Count == 2)
        {
            TotalpriceinINR = ds1.Tables[0].Rows[1][0].ToString();
        }
        else
        {
            TotalpriceinINR = "0";
        }
        txtamount.Text = TotalpriceinINR;
       
        txttotalusd.Text = TotalpriceinUSD;

        if (txtstartdate.Text == "1/1/1880" || txtenddate.Text == DateTime.Now.ToShortDateString())
        {
            txtstartdate.Text = "";
            txtenddate.Text = "";
        }
    }
    protected void Grddetails_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}