﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Salessummary.ascx.cs" Inherits="partners_usercontros_Salessummary" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<style type="text/css">
    .style1
    {
        height: 278px;
    }
    .style2
    {
        width: 80px;
    }
    .style3
    {
        width: 205px;
    }
</style>
<div>

</div>
   <div>
   </div>
<table width="100%">
<tr>
<td width="5%">
</td>
<td width="90%">
<table>
<tr>
<td class="style3">
<asp:TextBox ID="txtsearch" runat="server" Width="195px" class="textBoxAdminSearch" 
        ></asp:TextBox>
</td>
<td>
<asp:Button ID="Button1" runat="server" Text="Search" ToolTip="Search"  CssClass="dynamicButton btnAdvanceSearch" Height="36px" onclick="btnsearch_Click"/>
</td>
<td class="style2">
<asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="#CC0000" Text="Filter By"></asp:Label>
</td>
<td>
<dxe:ASPxDateEdit ID="txtstartdate"  runat="server"></dxe:ASPxDateEdit>
</td>
<td>
To
</td>
<td>
<dxe:ASPxDateEdit ID="txtenddate"  runat="server"></dxe:ASPxDateEdit>
</td>
<td>
<asp:Button ID="Button2" runat="server" Text="Search" ToolTip="Search" CssClass="dynamicButton btnAdvanceSearch" meta:resourcekey="btnAdvanceSearchResource1" Height="36px" onclick="datesearchto_Click" />
</td>
</tr>
</table>
</td>
<td width="5%">
</td>
</tr>
<tr>
<td width="5%" class="style1">
</td>
<td align="center" >
<div>&nbsp;</div>
<div align="center">
    <asp:Label ID="lblnodata" runat="server" Text="....No Data found..." Visible="false"></asp:Label>
</div>
<div style="overflow-y: scroll; height: 350px">
    <asp:GridView ID="Grddetails" runat="server"  CellPadding="4" 
             
            AutoGenerateColumns="False" 
           
             AllowSorting="True" Width="767px" 
       
         ForeColor="#333333" GridLines="None" onselectedindexchanged="Grddetails_SelectedIndexChanged"
         
        
            >
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
            <asp:TemplateField HeaderText="Organization">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtreferredby" runat="server" Text='<%# Bind("REFERREDBY") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblreferredby" runat="server" Text='<%# Bind("REFERREDBY") %>'></asp:Label>
                    </ItemTemplate>
                     
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtname" runat="server" Text='<%# Bind("FIRST_NAME") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblname" runat="server" Text='<%# Bind("FIRST_NAME") %>'></asp:Label>
                    </ItemTemplate>
                     
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Login Id" >
                <EditItemTemplate>
                <asp:TextBox ID="txtloginid" runat="server" Text='<%# Bind("LOGIN_NAME") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblloginid" runat="server" Text='<%# Bind("LOGIN_NAME") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField >
                <asp:TemplateField HeaderText="Date of Purchase">
                <EditItemTemplate>
                <asp:TextBox ID="txtcreatedon" runat="server" Text='<%# Bind("CREATED_ON") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblcreatedon" runat="server" Text='<%# Bind("CREATED_ON") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Product Purchased">
                <EditItemTemplate>
                <asp:TextBox ID="txtproductpuchased" runat="server" Text='<%# Bind("LICENSE_TYPE") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblproductpurchased" runat="server" Text='<%# Bind("LICENSE_TYPE") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Company Name">
                <EditItemTemplate>
                <asp:TextBox ID="txtcompanyname" runat="server" Text='<%# Bind("COMPANY") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblcompanyname" runat="server" Text='<%# Bind("COMPANY") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Price in INR">
                <EditItemTemplate>
                <asp:TextBox ID="txtpriceininr" runat="server" Text='<%# Bind("priceinINR") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblpricininr" runat="server" Text='<%# Bind("priceinINR") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="Price in USD($)">
                <EditItemTemplate>
                <asp:TextBox ID="txtpriceinusa" runat="server" Text='<%# Bind("priceinUSD") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblpricinusa" runat="server" Text='<%# Bind("priceinUSD") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>      
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
             <HeaderStyle BackColor="#c3c3c3" Font-Bold="True" ForeColor="#B765C8" />
            <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="#284775" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#B765C8" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
   </div>
</td>
<td width="5%" class="style1">
   
</td>

</tr>
<tr>
<td width="5%">
</td>
<td width="90%">
<div>&nbsp;</div>
<table align="center">
<tr >
<td>
<asp:Label ID="lblamont" runat="server" Text="Total Amount" Font-Bold="True" Font-Size="Small"></asp:Label>
</td>
<td>
<asp:TextBox ID="txtamount" runat="server"></asp:TextBox>  
</td>
<td>
<asp:Label ID="Label1" runat="server" Text="Total Amount($)" Font-Bold="True" Font-Size="Small"></asp:Label>
</td>
<td>
 <asp:TextBox ID="txttotalusd" runat="server"></asp:TextBox> 
</td>
</tr>
</table>
<div>&nbsp;</div>
</td>
<td width="5%">
</td>
</tr>



</table>

<%--<table width="100%">
<tr>
<td width="10%">
</td>
<td width="40%">
</td>
<td width="40%">
</td>

<td width="10%">
</td>
</tr>
<tr>
<td width="10%">
</td>
<td width="40%">
<asp:TextBox ID="txtsearch" runat="server" Width="248px" class="textBoxAdminSearch"></asp:TextBox>
     <asp:Button ID="btnsearch" runat="server" 
            Text="Search" ToolTip="Search" 
                    CssClass="dynamicButton btnAdvanceSearch" 
                    Height="36px" onclick="btnsearch_Click"  
            />
</td>
<td width="40%">
       <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Medium" 
            ForeColor="#CC0000" Text="Filter By"></asp:Label>
    
          <asp:TextBox ID="TextBox1" runat="server" 
                            CssClass="textBoxDate txtCreatedDateFrom" 
                            meta:resourcekey="txtCreatedDateFromResource1" />
             <asp:CalendarExtender ID="txtcalender" runat="server" 
            Enabled="True" TargetControlID="TextBox1">
        </asp:CalendarExtender>
                        To
                        <asp:TextBox ID="TextBox2" runat="server" 
                            CssClass="textBoxDate txtCreatedDateTo" 
                            meta:resourcekey="txtCreatedDateToResource1" />
     <asp:CalendarExtender ID="txtcalender1" runat="server" Enabled="True" 
            TargetControlID="TextBox2">
        </asp:CalendarExtender>
        <asp:Button ID="datesearchto" runat="server" 
            Text="Search" ToolTip="Search" 
                    CssClass="dynamicButton btnAdvanceSearch" 
                    meta:resourcekey="btnAdvanceSearchResource1" Height="36px" onclick="datesearchto_Click" 
           />
</td>
<td width="10%">
</td>
</tr>
<tr>
<td width="10%">
</td>
<td width="40%" colspan="2" style="width: 80%">
<div>
<asp:GridView ID="Grddetails" runat="server" BackColor="#DEBA84" 
            BorderColor="#DEBA84" BorderStyle="None"  CellPadding="3" 
            CellSpacing="2" 
             
            AutoGenerateColumns="False" 
            ShowFooter="True" 
             AllowSorting="True" Width="767px" style="z-index: 1; left: 36px; top: 162px; position: absolute; height: 199px; width: 767px" 
         
        
            >
            <Columns>
            <asp:TemplateField HeaderText="Name">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtreferredby" runat="server" Text='<%# Bind("REFERREDBY") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblreferredby" runat="server" Text='<%# Bind("REFERREDBY") %>'></asp:Label>
                    </ItemTemplate>
                     
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtname" runat="server" Text='<%# Bind("FIRST_NAME") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblname" runat="server" Text='<%# Bind("FIRST_NAME") %>'></asp:Label>
                    </ItemTemplate>
                     
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Login Id" >
                <EditItemTemplate>
                <asp:TextBox ID="txtloginid" runat="server" Text='<%# Bind("LOGIN_NAME") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblloginid" runat="server" Text='<%# Bind("LOGIN_NAME") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField >
                <asp:TemplateField HeaderText="Date of Purchase">
                <EditItemTemplate>
                <asp:TextBox ID="txtcreatedon" runat="server" Text='<%# Bind("CREATED_ON") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblcreatedon" runat="server" Text='<%# Bind("CREATED_ON") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Product Purchased">
                <EditItemTemplate>
                <asp:TextBox ID="txtproductpuchased" runat="server" Text='<%# Bind("LICENSE_TYPE") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblproductpurchased" runat="server" Text='<%# Bind("LICENSE_TYPE") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Company Name">
                <EditItemTemplate>
                <asp:TextBox ID="txtcompanyname" runat="server" Text='<%# Bind("COMPANY") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblcompanyname" runat="server" Text='<%# Bind("COMPANY") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Price in INR">
                <EditItemTemplate>
                <asp:TextBox ID="txtpriceininr" runat="server" Text='<%# Bind("PRODUCT_PRICE") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                 <asp:Label ID="lblpricininr" runat="server" Text='<%# Bind("PRODUCT_PRICE") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>    
            </Columns>
            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FFF1D4" />
            <SortedAscendingHeaderStyle BackColor="#B95C30" />
            <SortedDescendingCellStyle BackColor="#F1E5CE" />
            <SortedDescendingHeaderStyle BackColor="#93451F" />
        </asp:GridView>
        </div>
</td>
<td width="10%">
</td>
</tr>
<tr>
<td width="10%">
</td>
<td width="40%">
</td>
<td width="40%">
 <asp:Label ID="Label2" runat="server" Text="Total Amount" Font-Bold="True" 
        Font-Size="Small"></asp:Label>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtamount" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label3" runat="server" Text="Revenue earned" Font-Bold="True" Font-Size="Small"></asp:Label>&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtrevenue" runat="server"></asp:TextBox>

</td>
<td width="10%">
</td>
</tr>

</table>--%>