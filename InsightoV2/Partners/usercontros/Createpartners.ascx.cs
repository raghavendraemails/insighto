﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

public partial class partners_usercontros_Createpartners : System.Web.UI.UserControl
{
    public void Textfields()
    {
        txtorganization.Text = "";
        txtname.Text = "";
        txtloginid.Text = "";
        txtpassword.Text = "";
        txtconformpwd.Text = "";
        txtrevenueshr.Text = "";
    
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Textfields();
        }

    }
    public bool IsPasswordStrong(string p)
    {
        if (string.IsNullOrEmpty(p))
            return false;
        else
        {
            var regex = new Regex("^.{6,16}$");
            return regex.IsMatch(p) && !p.EndsWith(".");
        }
    }

    public bool Isvalidname(string a)
    {
        if (string.IsNullOrEmpty(a))
            return false;
        else
        {
            var regex = new Regex("^[a-zA-Z ]*$");
            return regex.IsMatch(a) && !a.EndsWith(".");
        }

    }

    public bool IsValidEmailAddress(string s)
    {
        if (string.IsNullOrEmpty(s))
            return false;
        else
        {
            var regex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            return regex.IsMatch(s) && !s.EndsWith(".");
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (txtorganization.Text == "")
        {
            reqorganization.Text = "Enter Organization";
            ReqName.Text = "";
            reqpassword.Text = "";
            reqconformpswd.Text = "";
            reqloginid.Text="";
            reqrevenue.Text="";
            

        }
        else if (!Isvalidname(txtorganization.Text))
        {
            reqorganization.Text = "Please enter alphabets only";
            ReqName.Text = "";
            reqpassword.Text = "";
            reqconformpswd.Text = "";
            reqloginid.Text = "";
            reqrevenue.Text = "";
        }
        else if (txtname.Text=="")
        {
            reqorganization.Text = "";
            ReqName.Text = "Enter Name";
            reqpassword.Text = "";
            reqconformpswd.Text = "";
            reqloginid.Text = "";
            reqrevenue.Text = "";

        }
        else if (!Isvalidname(txtorganization.Text))
        {
            reqorganization.Text = "";
            ReqName.Text = "Please enter alphabets only";
            reqpassword.Text = "";
            reqconformpswd.Text = "";
            reqloginid.Text = "";
            reqrevenue.Text = "";
        }

        else if (txtloginid.Text=="")
        {
            reqorganization.Text = "";
            ReqName.Text = "";
            reqpassword.Text = "";
            reqconformpswd.Text = "";
            reqloginid.Text = "Enter EmailId";
            reqrevenue.Text = "";
        }
        else if (!IsValidEmailAddress(txtloginid.Text))
        {
            reqorganization.Text = "";
            ReqName.Text = "";
            reqpassword.Text = "";
            reqconformpswd.Text = "";
            reqloginid.Text = "Enter valid Email";
            reqrevenue.Text = "";
        }
        else if (!IsPasswordStrong(txtpassword.Text))
        {

            reqorganization.Text = "";
            ReqName.Text = "";
            reqpassword.Text = "Password should contain 6-16 characters";
            reqconformpswd.Text = "";
            reqloginid.Text = "";
            reqrevenue.Text = "";
        }
        else if (txtpassword.Text=="")
        {
           reqorganization.Text = "";
           ReqName.Text = "";
           reqpassword.Text = "Enter Password";
           reqconformpswd.Text = "";
           reqloginid.Text = "";
           reqrevenue.Text = "";
        }

        else if (txtconformpwd.Text.Trim() != txtpassword.Text.Trim())
        {
            reqorganization.Text = "";
            ReqName.Text = "";
            reqpassword.Text = "";
            reqconformpswd.Text = "Enter Re-typePassword";
            reqloginid.Text = "";
            reqrevenue.Text = "";
        }
        else if (txtrevenueshr.Text.Trim() == "")
        {
            reqorganization.Text = "";
            ReqName.Text = "";
            reqpassword.Text = "";
            reqconformpswd.Text = "";
            reqloginid.Text = "";
            reqrevenue.Text = "Enter Revenueshare";
        }

        else 
            {
                try
                {
                    string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                    SqlConnection con = new SqlConnection(connStr);
                    SqlCommand cmd = new SqlCommand("sp_insertpartnerdetails", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Partner_name", SqlDbType.NVarChar)).Value = txtname.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@Organization_name", SqlDbType.NVarChar)).Value = txtorganization.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@Login_name", SqlDbType.NVarChar)).Value = txtloginid.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar)).Value = txtpassword.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@Revenue_status", SqlDbType.Int)).Value = Convert.ToInt32(txtrevenueshr.Text);
                    con.Open();
                    int ans = cmd.ExecuteNonQuery();
                    con.Close();
                    if (ans == 1)
                    {
                        lblsmsg.Text = "Partner Created Suceessfully";
                        txtorganization.Text = "";
                        txtname.Text = "";
                        txtloginid.Text = "";
                        txtpassword.Text = "";
                        txtconformpwd.Text = "";
                        txtrevenueshr.Text = "";
                    }
                    else
                        lblsmsg.Text = "Partner Not Created";
                }
                catch
                {
                    lblsmsg.Text = "Partner Not Created";
                }
                
            }
    }
 
}