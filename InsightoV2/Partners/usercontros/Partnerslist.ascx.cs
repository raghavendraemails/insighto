﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class partners_usercontros_Partnerslist : System.Web.UI.UserControl
{
    string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
    SqlConnection strconn;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            gridview();
        }

    }
    public void gridview()
    {
        string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
        SqlConnection strconn = new SqlConnection(connStr);
        strconn.Open();
        SqlCommand scom = new SqlCommand("sp_adminpartnerslist", strconn);
        scom.CommandType = CommandType.StoredProcedure;
        //scom.Parameters.Add(new SqlParameter("@FIRST_NAME", SqlDbType.NVarChar)).Value = txtsearch.Text;
        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet ds = new DataSet();
        sda.SelectCommand = scom;
        sda.Fill(ds, "shinedetails");
        int data = ds.Tables["shinedetails"].Rows.Count;
        if (data == 0)
        {
            lblnodata.Visible = true;
        }
        else
        {
            lblnodata.Visible = false;
        }
        Grddetails.DataSource = ds.Tables["shinedetails"];
        Grddetails.DataBind();
       

    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        gridview();
    }
    protected void Grddetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        string textdelete = ((LinkButton)(Grddetails.Rows[e.RowIndex].FindControl("linkbtndelete"))).Text;
        if (textdelete == "Delete")
        {
            string connStr1 = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn1 = new SqlConnection(connStr1);

            string name = ((Label)(Grddetails.Rows[e.RowIndex].FindControl("lblorganization"))).Text;

            SqlCommand scom1 = new SqlCommand("sp_adminpartnersdelete", strconn1);
            scom1.CommandType = CommandType.StoredProcedure;
            scom1.Parameters.Add(new SqlParameter("@ORGANIZATION", SqlDbType.NVarChar)).Value = name;
            strconn1.Open();
            int ans = scom1.ExecuteNonQuery();
            strconn1.Close();
        }
        //else if (textactive == "Active")
        //{

        //}
        gridview();


    }
    protected void Grddetails_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Grddetails.EditIndex = e.NewEditIndex;
        gridview();
    }
    protected void Grddetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        Grddetails.EditIndex = -1;
        gridview();
    }
    protected void Grddetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Total"))
          {

             strconn = new SqlConnection(connStr);

             GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
             Label lblname = (Label)(row.Cells[0].FindControl("lblorganization"));

             //string totallink = ((Label)(Grddetails.Rows[e.NewSelectedIndex].FindControl("lblorganization"))).Text;
             SqlCommand scom2 = new SqlCommand("sp_organizationtotal", strconn);
             scom2.CommandType = CommandType.StoredProcedure;
             scom2.Parameters.Add(new SqlParameter("@Organization_name", SqlDbType.NVarChar)).Value = lblname.Text.Trim();
             SqlDataAdapter sda1 = new SqlDataAdapter(scom2);
             DataSet ds1 = new DataSet();
             sda1.SelectCommand = scom2;
             sda1.Fill(ds1, "shinetotal");
             string TotalpriceinINR = "";
             string TotalpriceinUSD = "";
             if (ds1.Tables[0].Rows.Count == 1 || ds1.Tables[0].Rows.Count == 2)
             {
                 TotalpriceinUSD = ds1.Tables[0].Rows[0][1].ToString();
                 if (TotalpriceinUSD == "")
                     TotalpriceinUSD = "0";
             }
             else
             {
                 TotalpriceinUSD = "0";
             }
             if (ds1.Tables[0].Rows.Count == 2)
             {
                 TotalpriceinINR = ds1.Tables[0].Rows[1][0].ToString();
             }
             else
             {
                 TotalpriceinINR = "0";
             }

             txtamountinr.Text = TotalpriceinINR;
            txtamountusd.Text=TotalpriceinUSD;
            
          }
    }
    protected void Grddetails_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        //int empno = int.Parse(((Label)(gvemp.Rows[e.NewSelectedIndex].FindControl("lnkactive"))).Text);
        //cmd = new SqlCommand("sp_emp_getemp", con);
        //cmd.CommandType = CommandType.StoredProcedure;
        //cmd.Parameters.AddWithValue("@empno", empno);


        string connStr1 = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
        SqlConnection strconn1 = new SqlConnection(connStr1);

        string activation = ((LinkButton)(Grddetails.Rows[e.NewSelectedIndex].FindControl("lnkactive"))).Text;
        string organization = ((Label)(Grddetails.Rows[e.NewSelectedIndex].FindControl("lblorganization"))).Text;
        SqlCommand scom1 = new SqlCommand("sp_adminactivation", strconn1);
        scom1.CommandType = CommandType.StoredProcedure;
        scom1.Parameters.Add(new SqlParameter("@STATUS", SqlDbType.NVarChar)).Value = activation;
        scom1.Parameters.Add(new SqlParameter("@ORGANIZATION", SqlDbType.NVarChar)).Value = organization;

        strconn1.Open();
        scom1.ExecuteReader();
        strconn1.Close();

        gridview();

    }

    protected void Grddetails_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}