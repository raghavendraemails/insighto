﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HomeNew.aspx.cs" Inherits="Polls_HomeNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Insighto Polls</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

   

    <!-- CSS Global Compulsory -->
   
    <link rel="stylesheet" href="WebCss/style.css"/>

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="WebCss/line-icons/line-icons.css"/>
    <link rel="stylesheet" href="WebCss/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="WebCss/flexslider/flexslider.css"/>     
    <link rel="stylesheet" href="WebCss/parallax-slider/css/parallax-slider.css"/>
 

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="WebCss/default.css" id="style_color"/>

    <!-- CSS Customization -->
    <link rel="stylesheet" href="WebCss/custom.css"/>
    
   
    
    
    
     <!-- External CSS -->
    <link rel="stylesheet" href="popup/bootstrap.min.css"/>
    <link rel="stylesheet" href="http://alexgorbatchev.com/pub/sh/current/styles/shThemeDefault.css"/>

    <!-- jQuery Custombox CSS -->
    <link rel="stylesheet" href="popup/jquery.custombox.css"/>

    <!-- Demo page CSS -->
    <link rel="stylesheet" href="popup/demo.css"/>    
    
    
    <!-- Owl Carousel Assets -->
    <link href="owl-carousel/owl.carousel.css" rel="stylesheet"/>
    <link href="owl-carousel/owl.theme.css" rel="stylesheet"/>	
	 <link rel="stylesheet" href="WebCss/bootstrap/css/bootstrap.min.css"/>
	 <style>
	 .carousel-control{
		height:63px;
	 }	 
	 .carousel-control a{
		height:362px;
	 }	 
	 .carousel-control img{
		top: 9%;
		position: relative;
	}
	 </style>
</head>	

<body>


<div class="wrapper">
<form id="head" runat="server">
    <!--=== Header ===-->    
    <div class="header">
       
    
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                    <a class="navbar-brand" href="Home.aspx">
                        <img id="logo-header" src="WebImages/insighto_logo.png" alt="Logo">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav">
                        <li class="highlight"> <a href="Home.aspx">Home</a></li>
                        <li> <a href="Pricing.aspx">Pricing</a></li>
                        <li> <a href="signup.aspx">Sign Up</a></li>
                        <li> <a href="#modal1" id="fadein1">Sign In</a></li>
                        <li> <a href="http://insighto.com/">Surveys</a></li>

                       
                    </ul>
	
                </div><!--/navbar-collapse-->
            </div>    
        </div>            
        <!-- End Navbar -->
    </div>
    <!--=== End Header ===-->    

    <!--=== Slider ===-->
    <div class="slider-inner" style="margin-top:0px;">
		 <!-- BootStrap Banner Starts-->
<div id="myCarousel" class="carousel slide">
   <!-- Carousel indicators -->
   <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
   </ol>   
   <!-- Carousel items -->
   <div class="carousel-inner" >
      <div class="item active">
       <img src="images/bg1.png" alt="First slide">	
			<div class="carousel-caption" style="left:5%;right:5%;top:5%;">
				<h3 style="color:#000;font-size:3em;">Say goodbye to boring polls.<br>

<span style="font-size:0.5em;">Insighto Video Polls are here.<img src="images/play-button.png"></span></h3>
				<p style="color:#8b0189;text-shadow:none;font-size:1.8em;">Try free for 14 days</p>
				<p><a href="signup.aspx" class="btn-u btn-u-lg" style="font-weight:bold;text-shadow:none;">Start Free Trial</a></p>
			</div>
      </div>
      <div class="item">
         <img src="images/bg1.png" alt="Second slide">
		 <div class="carousel-caption" style="left:5%;right:5%;top:5%;">
				<h3 style="color:#000;font-size:3em;">Presenting Insighto Polls.<br>

<span style="font-size:0.5em;">Because your website deserves a conversation piece.<img src="images/button2.png"></span></h3>
				<p style="color:#8b0189;text-shadow:none;font-size:1.8em;">Try free for 14 days</p>
				<p><a href="signup.aspx" class="btn-u btn-u-lg" style="font-weight:bold;text-shadow:none;">Start Free Trial</a></p>
			</div>
      </div>
      <div class="item">
         <img src="images/bg1.png" alt="Third slide">
		<div class="carousel-caption" style="left:5%;right:5%;top:5%;">
				<h3 style="color:#000;font-size:3em;">Monetize your website polls<br>

<span style="font-size:0.5em;">With Insighto Polls’ unique ‘Sponsor’ feature.<img src="images/button3.png"></span></h3>
				<p style="color:#8b0189;text-shadow:none;font-size:1.8em;">Try free for 14 days</p>
				<p><a href="signup.aspx" class="btn-u btn-u-lg" style="font-weight:bold;text-shadow:none;">Start Free Trial</a></p>
			</div>
      </div>
   </div>
   <!-- Carousel nav >
   <a class="carousel-control left" style="background-image:none;" href="#myCarousel" 
      data-slide="prev"><img src="images/previous1.png"></a>
   <a class="carousel-control right" style="background-image:none;"  href="#myCarousel" 
      data-slide="next"><img src="images/next1.png"></a-->
</div> 
		
	 <!-- BootStrap Banner Ends -->	
    </div><!--/slider-->
    <!--=== End Slider ===-->

    

    <!--=== Content Part ===-->
      <div class="container content">	
    	<!-- Service Blocks -->
    	<div class="row margin-bottom-30">
        <div class="headline"><h2>Customizable Polls for your website
</h2></div>
        	<div class="col-md-4">
        		<div class="service">
                    
        			<div class="desc">
        				<h4>Text Poll</h4>
                        <div class="textpolltheme">
                     <div class="questiontheme">Was Dhoni right in announcing Fletcher as the Boss?</div>
                     
                     <div class="floatL marginleft"><img src="images/yesorno.png" /></div>
                     <div class="clear"></div>
                     <div class="floatL">
                      <div class="commentstxt" >Comments</div>
                     <div><input type="text" class="textBoxMedium txtAns txtcomments" /></div>
                     </div>
                        <div class="reportviewbasics">
                           <div class="viewandvoteimage">Vote <div style="font-size:10px">and View Results</div></div>
                           <div class="sponsorimage"> 
                               <div><img src="images/logosam.png"  /></div></div>
                       </div>
                       <div class="clear"></div>
                       <div id="poweredby" class="poweredbybasics">Powered by <a href="http://www.insighto.com" target="_blank">Insighto.com</a></div>
                 </div>
        			</div>
        		</div>	
        	</div>
        	<div class="col-md-4">
        		<div class="service">
                  
        			<div class="desc">
        				<h4>Image Poll</h4>
                        <div class="textpolltheme">
                        <div><img src="images/imagepo.png" /></div>
                     <div class="questionthemeimage">What does this image tell u?</div>
                     
                     <div class="floatL marginleft"><img src="images/imageforimagpo.png" /></div>
                     <div class="clear"></div>
                     
                        <div class="viewandvotetext">Vote and View Results</div>
                       <div class="clear"></div>
                       <div id="Div1" class="poweredbybasics">Powered by <a href="http://www.insighto.com" target="_blank">Insighto.com</a></div>
                 </div>
        			</div>
        		</div>	
        	</div>
        	<div class="col-md-4">
        		<div class="service">
                    
        			<div class="desc">
        				<h4>Video Poll</h4>
                        <p><img src="images/video-poll.jpg"></p>
        			</div>
        		</div>	
        	</div>			    
    	</div>
    	<!-- End Service Blokcs -->
        	<div class="headline"><h2>Features</h2></div>
            <!-- Begin Content -->
            <div class="col-md-9" style="width:100%;">
           	
                <div class="row margin-bottom-10">
                    <div class="col-md-6" style="width:50%;">
                        <div class="funny-boxes funny-boxes-top-sea">
                    <div class="row">
                        <div class="col-md-4 funny-boxes-img">
                            <img class="img-responsive" src="assets/img/new/Easytouse.png" alt="">
                           
                        </div>
                        <div class="col-md-8">
                            <h2><a href="#">Easy To Use</a></h2>
                           
                            <p>It’s time to take the simplicity road. Creating, designing and launching a poll for your website now takes less than 3 minutes. Embed the code into your website and the poll goes live. Simple!</p>
                        </div>
                    </div>                            
                </div>
                
                <div class="funny-boxes funny-boxes-top-sea">
		                    <div class="row">
        		                 <div class="col-md-4 funny-boxes-img">
                		           <img class="img-responsive" src="assets/img/new/VideoPolls.png" alt="">
                                 </div>
                        		 <div class="col-md-8">
		                           <h2><a href="#">Image / Video based polls</a></h2>
                                   <p>From the classic text poll to the new image and video polls, you can do it all. It’s time to say good bye to the mundane and bring zing to your polls.</p>
          		                 </div>
                   		    </div>                            
                        </div>
                
                
                
                <div class="funny-boxes funny-boxes-top-sea">
                    <div class="row">
                        <div class="col-md-4 funny-boxes-img">
                            <img class="img-responsive" src="assets/img/new/Monetize.png" alt="">
                           
                        </div>
                        <div class="col-md-8">
                            <h2><a href="#">Monetize your website polls</a></h2>
                           
                            <p>When you have great traffic on your website, you should monetize it in new ways. With Insighto Polls’ ‘Sponsor’ feature you can do just that. Seamlessly inject a brand logo into your polls, without taking help from your busy tech team.</p>
                        </div>
                    </div>                            
                </div>
                
                
                    </div>
                    <div class="col-md-6" style="width:50%;">
                        <div class="funny-boxes funny-boxes-top-sea">
		                    <div class="row">
        		                 <div class="col-md-4 funny-boxes-img">
                		           <img class="img-responsive" src="assets/img/new/Graphs.png" alt="">
                                 </div>
                        		 <div class="col-md-8">
		                           <h2><a href="#">Stunning Graphs</a></h2>
                                   <p>When you want to see the poll results, wouldn’t you like to see a little magic? Yes – you will - it takes just one click to see your poll results in stunning animated graphs.</p>
          		                 </div>
                   		    </div>                            
                        </div>
                        
                        <div class="funny-boxes funny-boxes-top-sea">
                    <div class="row">
                        <div class="col-md-4 funny-boxes-img">
                            <img class="img-responsive" src="assets/img/new/AdvancedReports.png" alt="">
                           
                        </div>
                        <div class="col-md-8">
                            <h2><a href="#">Advanced Reports</a></h2>
                           
                            <p>Besides the animated graphs, you can export poll results to Excel and PPT besides being able to work with raw data. Besides, you will be able to track and drill down results by geographic location, by device and by page location.</p>
                        </div>
                    </div>                            
                </div>
                        
                        <div class="funny-boxes funny-boxes-top-sea">
		                    <div class="row">
        		                 <div class="col-md-4 funny-boxes-img">
                		           <img class="img-responsive" src="assets/img/new/Multi-lingual.png" alt="">
                                 </div>
                        		 <div class="col-md-8">
		                           <h2><a href="#">Multi-lingual Polls</a></h2>
                                   <p>The good part is that you can create polls not just in English but the language of your choice. So, your poll will be in the same language as the rest of your content.</p>
          		                 </div>
                   		    </div>                            
                        </div>
                        
                        
                    </div>
                </div><!--/row-->            
            </div>
            <!-- End Content -->

    	<!-- Recent Works -->
        <div class="headline"><h2>How easy it is to launch Insighto Polls</h2></div>
        <div class="row margin-bottom-20">
            <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn rounded1">
                	<div class="thumbnail-img">
                        <img class="img-responsive" style="text-align:center;" src="images/icon1.png" alt="" />					
                    </div>
                    <div class="caption" style="height:165px;">
                        <h3 style="text-align:center;"><a class="hover-effect" href="#">Choose Poll Type</a></h3>
                        <p>After signing in, give your poll a name and select a poll type from one of the 3 options – text, image or video. You can also copy the design of an earlier poll done by you and jumpstart your new poll.</p>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn rounded1">
                	<div class="thumbnail-img">
                        <img class="img-responsive" style="text-align:center;" src="images/icon2.png" alt="" />					
                    </div>
                    <div class="caption" style="height:165px;">
                        <h3 style="text-align:center;"><a class="hover-effect" href="#">Create Poll </a></h3>
                        <p>Here’s where you frame your question. If your poll type happens to be media (image or video) based, you can upload the media and frame the question based on the media.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
               <div class="thumbnails thumbnail-style thumbnail-kenburn rounded1">
                	<div class="thumbnail-img">
                        <img class="img-responsive" style="text-align:center;" src="images/icon3.png" alt="" />					
                    </div>
                    <div class="caption" style="height:165px;">
                        <h3 style="text-align:center;"><a class="hover-effect" href="#">Customize Poll Design</a></h3>
                        <p>The Design section gives you the freedom to choose the colors for your question background, vote button background etc. It also gives you one-click options to add a sponsor’s logo, add social sharing and keep results private.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn rounded1">
                	<div class="thumbnail-img">
                        <img class="img-responsive" style="text-align:center;" src="images/icon4.png" alt="" />					
                    </div>
                    <div class="caption" style="height:165px;">
                        <h3 style="text-align:center;"><a class="hover-effect" href="#">Launch Poll</a></h3>
                        <p>After you have finalized your poll content and design, all you have to do is to generate the poll code and embed it in your website. Your poll is now live. Simple!</p>
                    </div>
                </div>
            </div>
        </div>
    	<!-- End Recent Works --> 	

        <!-- Our Clients -->
       <!-- Owl Carousel v2 -->
       <div class="headline"><h2>Trusted By Users In</h2></div>
               <div id="owl-demo" class="owl-carousel">
                
                <div><img src="images/IMRB.png" alt=""/></div>
                <div><img src="images/Knowience.png" alt=""/></div>
                <div><img src="images/Krea.png" alt=""/></div>
                <div><img src="images/NBW.png" alt=""/></div>
                <div><img src="images/PeopleMatters.png" alt=""/></div>
                <div><img src="images/QuickHeal.png" alt=""/></div>
                <div><img src="images/SchoolPage.png" alt=""/></div>
                <div><img src="images/Shine.png" alt=""/></div>
                <div><img src="images/UnivOfManchester.png" alt=""/></div>
                <div><img src="images/Wipro.png" alt=""/></div>
              
              </div>
              <div class="customNavigation">
                <a class="btn prev"><img src="images/previous.png" alt=""/></a>
                <a class="btn next"><img src="images/next.png" alt=""/></a>
                <a class="btn play" style="display:none">Autoplay</a>
                <a class="btn stop" style="display:none">Stop</a>
              </div>
        <!-- End Our Clients -->
    </div>
    <!--/container-->		
    <!-- End Content Part -->
 <!--=== Purchase Block ===-->
    <div class="purchase">
        <div class="container">
            <div class="row">
               
                    <span style="text-align:center;">Try free for 14 days<br>

<a href="signup.aspx" class="btn-u btn-u-lg" style="font-weight:bold;">Start Free Trial</a></span>
                  
                    
                
                    
               
                
            </div>
        </div>
    </div><!--/row-->
    <!-- End Purchase Block -->
   

    <!--=== Copyright ===-->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <p style="text-align:center;">
                        2014 &copy; Insighto Polls. ALL Rights Reserved. 

                    </p>
            </div>
        </div> 
    </div><!--/copyright--> 
    <!--=== End Copyright ===-->
    <div id="modal1" style="display: none;" class="modal-example-content">
        <div class="modal-example-header">
            <button type="button" class="close" onClick="$.fn.custombox('close');">&times;</button>
            <h4>Sign In Form</h4>
        </div>
        <div class="modal-example-body">
           
                    
                    <input type="text" placeholder="Username*" class="form-control margin-bottom-20">
                    
                    <input type="password" placeholder="Password*" class="form-control margin-bottom-20">
                    
                     
                    <button style="width:100%;font-size:18px;font-weight:bold;" class="btn-u" type="submit">Sign In</button> 
                     
					
                 
                    
        </div>
    </div>
    </form>
</div><!--/wrapper-->

<!-- JS Global Compulsory -->			
<script type="text/javascript" src="WebScripts/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="WebScripts/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="WebCss/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="WebScripts/back-to-top.js"></script>
<script type="text/javascript" src="WebCss/flexslider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="WebScripts/flowtype.js"></script>

<script type="text/javascript" src="WebCss/parallax-slider/js/modernizr.js"></script>
<script type="text/javascript" src="WebCss/parallax-slider/js/jquery.cslider.js"></script>
<!-- JS Page Level -->           
<script src="WebCss/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="WebScripts/owl-carousel.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $('.carousel-inner').flowtype({
            minimum: 700,
            maximum: 1200,
            fontRatio: 55,
            lineRatio: 1.45
        });
        App.init();
        OwlCarousel.initOwlCarousel();
    });
</script>
<script type="text/javascript" src="WebScripts/app.js"></script>
<script type="text/javascript" src="WebScripts/index.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init();
        App.initSliders();
        Index.initParallaxSlider();
    });
</script>
<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
<![endif]-->


    <!-- jQuery Custombox JS -->
    <script src="popup/jquery.custombox.js"></script>

    <!-- Demo page JS -->
    <script src="popup/demo.js"></script>

    <script>
        if ($(window).width() > 360) {
            //            SyntaxHighlighter.all();
        }

        $(function () {

            $('#fadein1').on('click', function () {
                $.fn.custombox(this);
                e.preventDefault();
                return false;
            });

            $('#fadein21').on('click', function () {
                $.fn.custombox(this);
                e.preventDefault();
                return false;
            });


            $('#fadein3').on('click', function () {
                $.fn.custombox(this);
                e.preventDefault();
                return false;
            });


            $('#fadein4').on('click', function () {
                $.fn.custombox(this);
                e.preventDefault();
                return false;
            });

            $('#fadein5').on('click', function () {
                $.fn.custombox(this);
                e.preventDefault();
                return false;
            });

            $('#fadein6').on('click', function () {
                $.fn.custombox(this);
                e.preventDefault();
                return false;
            });




        });
</script>	

   <script src="owl-carousel/owl.carousel.js"></script>


    <!-- Demo -->

    <style>
    #owl-demo .item{
        background: #3fbf79;
        padding: 30px 0px;
        margin: 10px;
        color: #FFF;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }
    .customNavigation{
      text-align: center;
    }
    .customNavigation a{
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }
    </style>


    <script>
        $(document).ready(function () {

            var owl = $("#owl-demo");

            owl.owlCarousel({

                items: 7, //10 items above 1000px browser width
                itemsDesktop: [1000, 5], //5 items between 1000px and 901px
                itemsDesktopSmall: [900, 3], // 3 items betweem 900px and 601px
                itemsTablet: [600, 2], //2 items between 600 and 0;
                itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option

            });
            owl.trigger('owl.play', 1500);
            // Custom Navigation Events
            $(".next").click(function () {
                owl.trigger('owl.next');
            })
            $(".prev").click(function () {
                owl.trigger('owl.prev');
            })
            $(".play").click(function () {
                owl.trigger('owl.play', 1000);
            })
            $(".stop").click(function () {
                owl.trigger('owl.stop');
            })

            $(".owl-pagination").hide();



        });
    </script>  	
    <!-- Bootstrap Core JavaScript -->
	

       	
<script>    $('#noscript').remove();</script>


</body>
</html>	
