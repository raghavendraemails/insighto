﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master"  CodeFile="PollBasics.aspx.cs" Inherits="PollBasics" EnableEventValidation="true" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<link rel="stylesheet" type="text/css" href="https://insightopollsssl-a59.kxcdn.com/css/poll.css" />

<script src="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
<link href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/start/jquery-ui.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript">
        function ShowPopup(pollid) {
            $(function () {
                $("#dialog").dialog({
                    title: "Preview",
                    buttons: {
                        Close: function () {
                            $(this).dialog('close');
                        }
                    },
                    open: function (event, ui) {
                        var s = $(document).scrollTop() + 80;
                        $(".ui-dialog").css("top", s + "px");
                        var height = $("#MainContent_" + pollid).height();
                        $("#dialog").height(height);
                        var width = $("#MainContent_Id_" + pollid).width();
                        $("#dialogContent").width(width);
                        $("#dialog").width(width);
                        $(".ui-dialog").css("width", width + "px");
                        width = $("#MainContent_Id_" + pollid).width() - 30;
                        $(".txtcommentsbyme").css("width", width + "px");
                        $("#dialog").css("padding", "0px");
                        $("#dialog").css("overflow", "hidden");
                        $("#dialogContent").html(document.getElementById('MainContent_' + pollid).innerHTML);
                    },
                    resizable: false,
                    modal: true
                });
            });
        };
    </script>
<script type="text/javascript" src="https://insightopollsssl-a59.kxcdn.com/scripts/jquery.alerts.js"></script>
<script src="https://insightopollsssl-a59.kxcdn.com/scripts/customscripts.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#customthemes").hide();
        $("#MainContent_defaultthemes").show();
        $("#MainContent_txtPollName").keyup(function () {
            var text = $("#MainContent_txtPollName").val();
            var count = text.length;
            if (count > 50) {
                $("#MainContent_lblErrMsg").text('Exceeds maximum number of characters');
                $("#MainContent_lblErrMsg").show();
                return false;
            }
        });
    });
</script>
<div id="dialog" style="display: none">
<div id="dialogContent"></div>
</div>
  <div class="contentPanelPoll">
    <div class="createpollbg"><asp:Label runat="server" Text="Create Poll" ID="lblCreate"></asp:Label>
    </div>
         <div class="clear"></div>
    <div class="CreatePoll">
         <div><asp:Label ID="lblErrMsg" runat="server" CssClass="errorMesg" 
                            Visible="False" ></asp:Label>
          </div>
          <div class="floatL">
         <div class="iframemargin fontweightbold">Name for the New Poll*</div>
         <div class="clear"></div>
         <div><asp:TextBox ID="txtPollName" MaxLength="50" runat="server" class="textBoxMedium"></asp:TextBox></div>
         <div class="clear"><!-- validation -->
                        <div class="btmpadding">
                            <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqSurveyName" 
                                ControlToValidate="txtPollName"  ErrorMessage ="Please enter poll name."
							CssClass="lblRequired"  runat="server" 
                                Display="Dynamic" ></asp:RequiredFieldValidator>
                         </div>
                   <!-- //row --> 
          </div></div>
          <div class="createpollright">
         <div class="choosepolltype">Choose your Poll type
         </div>
         <div class="clear"></div>
         <div class="PollDiv">
             <div class="PollDivsub"><a href="#" onclick="ShowDefaultThemes();">Templates</a></div>
             <div class="PollDivsubinner"><a href="#" onclick="ShowCustomThemes();">Polls created by me</a></div>
         </div>
         <div class="clear"></div>
         <div class="polltype" id="defaultthemes" runat="server">
             <div class="texttype">
             <div class="headingpolltheme">The Text poll</div>
                 <div class="textpolltheme">
                     <div class="questiontheme">Your Question</div>
                     <div class="clear"></div>
                     <img src="App_Themes/images/imageoptions.jpg" />
                       <div class="viewandvotetext">Vote and View Results</div>
                       <div id="poweredby" class="poweredbybasics">Powered by <a href="https://www.insighto.com/Polls/" target="_blank">Insighto Polls</a></div>
                 </div>
                 <div class="SavePoll"> <asp:Button ID="btnTextType" runat="server" CssClass="selectbg" Text="Select" 
                         onclick="btnTextType_Click" /></div>
                 </div>
             <div class="imagetype">
             <div class="headingpolltheme">The Image poll</div>
                 <div class="imagepolltheme">
                 <div style="margin-top: 2px;"><img src="App_Themes/images/ImagePoll.jpg" /></div>
                     <div class="imagequestiontheme">Your Question</div>
                     <div class="clear"></div>
                     <img src="App_Themes/images/options.jpg" />
                      <div class="reportviewbasics">
                           <div class="viewandvoteimage">Vote<div style="font-size:10px">and View Results</div></div>
                           <div class="sponsorimage">
                               <div>Sponsored By</div>
                               <div><img src="App_Themes/images/logoPlaceholder2.png"  style="width:60px;"/></div>
                           </div>
                       </div>
                       <div class="clear"></div>
                       <div id="Div1" class="poweredbybasics">Powered by <a href="https://www.insighto.com/Polls/" target="_blank">Insighto Polls</a></div>
                 </div>
                 <div class="SavePoll"> <asp:Button ID="btnImageType" runat="server" CssClass="selectbg" Text="Select" 
                         onclick="btnImageType_Click" /></div>
                 </div>
             <div class="videotype">
                   <div class="headingpolltheme">The Video poll</div>
                 <div class="imagepolltheme">
                 <div style="margin-top: 2px;"><img src="App_Themes/images/video.jpg" /></div>
                     <div class="imagequestiontheme">Your Question</div>
                     <div class="clear"></div>
                    <img src="App_Themes/images/options.jpg" />
                      <div class="reportviewbasics">
                           <div class="viewandvoteimage">Vote <div style="font-size:10px">and View Results</div></div>
                           <div class="sponsorimage">   <div>Sponsored By</div>
                               <div><img src="App_Themes/images/logoPlaceholder2.png"  style="width:60px;" /></div></div>
                       </div>
                       <div class="clear"></div>
                       <div id="Div2" class="poweredbybasics">Powered by <a href="https://www.insighto.com/Polls/" target="_blank">Insighto Polls</a></div>
                 </div>
                 <div class="SavePoll"><asp:Button ID="btnVideoType" runat="server" CssClass="selectbg" Text="Select" 
                         onclick="btnVideoType_Click" /></div>
                 </div>
         </div>
         <div id="customthemes">
             <asp:PlaceHolder ID="plcCustomThemes" runat="server">
             </asp:PlaceHolder>
         </div>
         </div>
         <div class="clear"></div>
   </div>
  </div>
      <script type="text/javascript">
          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-55613473-1', 'auto');
          ga('send', 'pageview');

</script>
</asp:Content>