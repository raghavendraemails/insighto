﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using System.Web.Script.Serialization;
public partial class Polls_paymentwebhook : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //CovalenseUtilities.Extensions.ObjectJsonExtensions objJson = new CovalenseUtilities.Extensions.ObjectJsonExtensions();
        //string postvars = "";
        string paymentid = "";
        decimal amount = 0M;
        int orderid = 0;
        string productslug = "";
        string producttitle = "";
        string status = "";
        string customfields = "";

        PollCreation pc;
        pc = new PollCreation();
        if (Request.Form.Count > 0)
        {
            paymentid = Request.Form["payment_id"];
            productslug = Request.Form["offer_slug"];
            producttitle = Request.Form["offer_title"];
            status = Request.Form["status"];
            Decimal.TryParse(Request.Form["amount"], out amount);
            customfields = Request.Form["custom_fields"];
        }
        else if (Request.QueryString.Count > 0)
        {
            paymentid = Request.QueryString["payment_id"];
            productslug = Request.QueryString["offer_slug"];
            producttitle = Request.QueryString["offer_title"];
            status = Request.QueryString["status"];
            Decimal.TryParse(Request.QueryString["amount"], out amount);
            customfields = Request.QueryString["custom_fields"];
        }
        JavaScriptSerializer js = new JavaScriptSerializer();
        if (productslug == "insighto-polls-publisher-monthly")
        {
            clscustomfields str1 = js.Deserialize<clscustomfields>(customfields);
            Int32.TryParse((str1.field_50041 != null) ? str1.field_50041.value : "0", out orderid);
        }
        else if (productslug == "insighto-polls-publisher-yearly")
        {
            clscustomfields str1 = js.Deserialize<clscustomfields>(customfields);
            Int32.TryParse((str1.field_2275 != null) ? str1.field_2275.value : "0", out orderid);
        }
        else if (productslug == "insighto-polls-business-monthly")
        {
            clscustomfields str1 = js.Deserialize<clscustomfields>(customfields);
            Int32.TryParse((str1.field_34087 != null) ? str1.field_34087.value : "0", out orderid);
        }
        else if (productslug == "insighto-polls-business-yearly")
        {
            clscustomfields str1 = js.Deserialize<clscustomfields>(customfields);
            Int32.TryParse((str1.field_61431 != null) ? str1.field_61431.value : "0", out orderid);
        }
        else if (productslug == "insighto-polls-professional-monthly")
        {
            clscustomfields str1 = js.Deserialize<clscustomfields>(customfields);
            Int32.TryParse((str1.field_58468 != null) ? str1.field_58468.value : "0", out orderid);
        }
        else if (productslug == "insighto-polls-professional-yearly")
        {
            clscustomfields str1 = js.Deserialize<clscustomfields>(customfields);
            Int32.TryParse((str1.field_67761 != null) ? str1.field_67761.value : "0", out orderid);
        }
        lbl1.Text = orderid.ToString();
        pc.SavePollPaymentInformation(paymentid, amount, productslug, producttitle, orderid, status, "Poll");
    }

}

public class clscustomfields
{
    public clscustomfields() { }
    public Field_50041 field_50041 { get; set; } //PUB M
    public Field_2275 field_2275 { get; set; } //PUB Y
    public Field_34087 field_34087 { get; set; } //BUS M
    public Field_61431 field_61431 { get; set; } //BUS Y
    public Field_58468 field_58468 { get; set; } //PRO M
    public Field_67761 field_67761 { get; set; } //PRO Y
}

/// <summary>
/// DETAILS OF PUBLISHER MONTHLY AND YEARLY CLASSES FOR JSON
/// </summary>
public class Field_50041 : Fields { }
public class Field_2275 : Fields { }
//////////////////////////////////////////////////////////////////////////////

/// <summary>
/// DETAILS OF BUSINESS MONTHLY AND YEARLY CLASSES FOR JSON
/// </summary>
public class Field_34087 : Fields { }
public class Field_61431 : Fields { }
//////////////////////////////////////////////////////////////////////////////

/// <summary>
/// DETAILS OF PROFESSIONAL MONTHLY AND YEARLY CLASSES FOR JSON
/// </summary>
public class Field_58468 : Fields { }
public class Field_67761 : Fields { }
//////////////////////////////////////////////////////////////////////////////

//THIS IS THE CUSTOM FIELD FOR THE ORDER ID. TO BE CHANGED WHEN THIS FIELD ID AS GIVEN BY INSTAMOJO CHANGES IN THE PAYMENT LINK
public class Fields
{
    public bool required { get; set; }
    public string type { get; set; }
    public string label { get; set; }
    public string value { get; set; }
}

