﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using System.Data;
using App_Code;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Media;
using System.Web.UI.HtmlControls;
using FeatureTypes;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

public partial class Poll_EditPollName : SecurePageBase
{
    int PollId = 0;
    Hashtable typeHt = new Hashtable();
    PollCreation Pc = new PollCreation();
    DataSet pollInfo = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
        if (typeHt.Contains("PollId"))
            PollId = Convert.ToInt32(typeHt["PollId"].ToString());
        if(!IsPostBack)
        {
        pollInfo = Pc.getPollInfo(PollId);
        txtPollName.Text = pollInfo.Tables[0].Rows[0][2].ToString();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Pc.updatePollNameOnly(PollId, txtPollName.Text);
        base.CloseModal(EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + PollId));
    }
}