﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="InvoiceHistory.aspx.cs" Inherits="Insighto.Pages.InvoiceHistory"%>
    
<%@ Register Src="~/UserControls/ProfileRightPanel.ascx" TagName="ProfileRightPanel"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="myProfileLeftPanel">
        <div class="marginNewPanel" style="margin-top: 0;">
            <div class="signUpPnl">
                <!-- customer details -->
                <h3>
                    <asp:Label ID="lblTitle" runat="server" Text="Invoice History" 
                        meta:resourcekey="lblTitleResource1"></asp:Label>
                </h3>
                
                <div class="surveysGridPanel">
                    <div id="ptoolbar">
                    </div>
                    <table id="tblJobKits">
                    </table>
                </div>
                <asp:HiddenField ID="hdnUrl" runat="server" />
                <!-- //customer details -->
            </div>
        </div>
    </div>
    <uc:ProfileRightPanel ID="ucProfileRightPanel" runat="server" />
    <div class="clear">
    </div>
    <script type="text/javascript">
        var serviceUrl = 'AjaxService.aspx';

        $(document).ready(function () {
            $("#tblJobKits").jqGrid({
                url: serviceUrl,
                postData: { method: "GetOrderInofByUserId" },
                datatype: 'json',
                colNames: ['Invoice No', 'Product', 'Purchased on', 'Valid upto', 'Amount', 'Actions'],
                colModel: [
                { name: 'ORDERID', index: 'ORDERID', width: 125, align: 'left', editable: false, resizable: false, size: 100 },
                { name: 'Product', index: 'Product', width: 150, align: 'left', editable: false, size: 100, resizable: false },
                { name: 'PaidDate', index: 'PAID_DATE', width: 125, align: 'center', editable: false, resizable: false, size: 100, sortable: true },
                { name: 'LICENSEEXPIRYDATE', index: 'LICENSE_EXPIRY_DATE', width: 125, align: 'center', editable: false, resizable: false, size: 100, sortable: true },
                { name: 'Amount', index: 'Amount', width: 100, align: 'center', editable: false, resizable: false, size: 100 },               
                { name: 'Url', index: 'Url', width: 80, align: 'right', editable: false, resizable: false, size: 100, formatter: EditLinkFormatter, sortable: false }
                ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'Product',
                sortorder: "asc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 630,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });

            $("#tblJobKits").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false, refresh: false });
        });

        function EditLinkFormatter(cellvalue, options, rowObject) {
            var mailUrls = cellvalue.split('~');
            //$('#<%=hdnUrl.ClientID %>').val(mailUrls[1]);
            str = "<a href='" + mailUrls[0] + "' title='View' class='button-small-gap icon-View-active' rel='framebox' h='480' w='700' scrolling='true' ></a>";
            var url = "'" + mailUrls[0] + "'";            
            str += '<a href="#"class="button-small-gap icon-print-active" onclick="ShowPopop('+url+')" title="Print"></a>';
            return str;
           

        }
        function gridReload(keyword) {
            $("#tblJobKits").jqGrid('setGridParam', { url: serviceUrl + "?mode=" + keyword, page: 1 }).trigger("reloadGrid");
        }

        function ShowPopop(url) {
            //var url = $('#<%=hdnUrl.ClientID %>').val();          
            window.open(url, 'Help', 'height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');
            return false;
        }
    </script>
        <script type="text/javascript">
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-55613473-1', 'auto');
            ga('send', 'pageview');

</script>
</asp:Content>
