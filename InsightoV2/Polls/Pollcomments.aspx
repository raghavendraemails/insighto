﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SurveyReport.master" AutoEventWireup="true" CodeFile="Pollcomments.aspx.cs" Inherits="Pollcomments" %>

<%@ Register Src="~/Polls/UserControls/PollReportHeader.ascx" TagName="SurveyReportHeader" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SurveyReportHeader" runat="Server">
    <uc:SurveyReportHeader ID="ucSurveyReportHeader" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SurveyReportMainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel runat="server" ID="pnlLoading" Style="height: 450px; vertical-align: middle;"
        HorizontalAlign="Center">
        <div style="width: 100%; height: 220px;">
        </div>  
        <img src="../App_Themes/Classic/Images/ajax-loader.gif" alt="LOADING..." /><br />
        <div style="width: 100%; height: 220px;">
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlMain" Style="display: none;">
        <div id="intercontainer">
            <table width="100%">
                <tr>
                    <td align="left">                     
                        <asp:Label ID="lblheader" runat="server" Text="Responses"></asp:Label>
                    </td>
                </tr>
                <tr><td class="defaultH"></td></tr>
                <tr>
                    <td align="left">
                    <div style="overflow: auto; width: 900px; height: 600px">
                    <asp:GridView ID="grdResponses" AutoGenerateColumns="false" runat="server" HeaderStyle-BackColor="#F4F4F4" Width="95%" RowStyle-Height="25px" HeaderStyle-Height="30px">
                    <columns>  
                <asp:BoundField DataField="rownm" HeaderText="Sno" ReadOnly="True" /> 
                <asp:BoundField DataField="commresp" HeaderText="Comment" ReadOnly="True"  />   
                    </columns>
                    </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr valign="top">
                    <td align="right">                   
                     <asp:Button ID="btnBack" runat="server" ToolTip="Back" Text="Back" CssClass="dynamicButton" OnClick="cmdBack_Click" runat="server" />
                       
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <script type="text/javascript" language="javascript">

        function pageLoad(sender, args) {
            //alert('hai');
            TogglePannel('<%= pnlMain.ClientID %>', true);
            TogglePannel('<%= pnlLoading.ClientID %>', false);
        }

        function TogglePannel(target, OnOff) {
            obj = document.getElementById(target);

            if (obj) {
                obj.style.display = (OnOff) ? 'inline' : 'none';
            }
        }
 
    </script>
        <script type="text/javascript">
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-55613473-1', 'auto');
            ga('send', 'pageview');

</script>
</asp:Content>


