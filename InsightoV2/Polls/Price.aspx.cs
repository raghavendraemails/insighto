﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using Resources;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;
using System.Net;
using System.Web.UI;

public partial class Polls_Price : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int UserId = 0;
    
    string countryName = "";
    private string orderNo = "";    
    private double price;
    private double taxPercentage;
    private double tax = 0;
    public string AnnualProf;
    public string AnnualBus;
    public string AnnualPub;
    SurveyCore surcore = new SurveyCore();
    PollCreation Pc = new PollCreation();

    protected void Page_Load(object sender, EventArgs e)
    {
        var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
        if (userSessionDetails == null)
        {
            Response.Redirect("/Polls/pricing.aspx", false);
        }


        string ipAddress = "";
        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
        {
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        }

        countryName = Utilities.GetCountryName(ipAddress);
        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(countryName);

        countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
      //  countryName = "UNITED STATES";
        if (countryName.ToUpper() != "INDIA")
        {
           
           // countryName = "AC";
            spprof.InnerHtml = "$9";
            spBus.InnerHtml = "$29";
            spPub.InnerHtml = "$79";
            AnnualProf = "Annual Plan - $99";
            AnnualBus = "Annual Plan - $299";
            AnnualPub = "Annual Plan - $799";
        }
        else
        {
            spprof.InnerHtml = "&#8377;" + "399";
            spBus.InnerHtml = "&#8377;" + "1,299";
            spPub.InnerHtml = "&#8377;" + "3,499";
            AnnualProf = "Annual Plan - " + "&#8377;" + "3,990";
            AnnualBus = "Annual Plan - " + "&#8377;" + "12,990";
            AnnualPub = "Annual Plan - " + "&#8377;" + "34,990";
        }
        

        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            if (typeHt.Contains("UserId"))
                UserId = Convert.ToInt32(typeHt["UserId"].ToString());
           
        }

      //  ancProf.ServerClick += new EventHandler(btnprof1_Click);
       
    }

    #region GetOrderInfoDetails
   
    public osm_orderinfo GetOrderInfoDetails(int userId)
    {


        orderNo = hdnOrderNo.Value;
        osm_orderinfo order = new osm_orderinfo();
        if (DateTime.UtcNow.Month < 10)
            orderNo += "0" + DateTime.UtcNow.Month;
        else
            orderNo += DateTime.UtcNow.Month;
        orderNo += Convert.ToString(DateTime.UtcNow.Year).Substring(2);
        order.TAX_PERCENT = ValidationHelper.GetDouble(hdnTax.Value, 0);
        order.TAX_PRICE = ValidationHelper.GetDouble(hdnTaxAmount.Value, 0);
        order.TOTAL_PRICE = ValidationHelper.GetDouble(hdnTotal.Value, 0);
        order.USERID = userId;
        order.PRODUCT_PRICE = ValidationHelper.GetDouble(hdnPrice.Value, 0);
        order.MODEOFPAYMENT = ValidationHelper.GetInteger(1, 0);
        order.ORDERID = orderNo;
        //if (hdnUpgrade.Value == "New")
        //{
        //    order.ORDER_TYPE = Constants.ORDERTYPE;
        //}
        //else if (hdnUpgrade.Value == "Upgrade")
        //{
            order.ORDER_TYPE = Constants.UPGRADEORDERTYPE;
        //}
        //else if (hdnUpgrade.Value == "Renew")
        //{
        //    order.ORDER_TYPE = Constants.RENEWORDERTYPE;
        //}
        order.CREATED_DATE = DateTime.UtcNow;
        order.PAID_DATE = DateTime.UtcNow;
        order.TRANSACTION_STATUS = 0;
        // DataSet dsprice = surcore.getPriceLicenseCountry(countryName, licenseType);
        // order.CurrencyType = "INR"; // new RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name).ISOCurrencySymbol;
        if (countryName.ToUpper() == "INDIA")
        {
             order.CurrencyType = "INR";
            //order.CurrencyType = "USD";
        }
        else
        {
            order.CurrencyType = "USD";
        }
        //order.CurrencyType =  new RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name).ISOCurrencySymbol;
        order.ModifiedDate = DateTime.UtcNow;
        //var currency = new CurrencyServiceClient().GetConversionRate(CurrencyCode.USD, CurrencyCode.INR);
        order.ExchangeRate = 0;// currency.Rate;
        return order;
    }
    #endregion


    #region GetAccountHistoryInfo
    /// <summary>
    /// fills data into user account history object and returns user acc 
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="orderId"></param>
    /// <returns></returns>
    public osm_useraccounthistory GetAccountHistoryInfo(int userId, string orderId)
    {
        osm_useraccounthistory history = new osm_useraccounthistory();
        history.USERID = userId;
        history.ORDERID = orderId;
        
            history.EMAIL_FLAG = 0;
        
        history.CREATED_DATE = DateTime.UtcNow;
        history.CURRENT_LICENSETYPE = hdnType.Value;
        history.LICENSETYPE_OPTED = hdnType.Value;
        return history;
    }
    #endregion
  

    public void bindpaymentdetails(string licenseType)
    {
        if (countryName.ToUpper() == "INDIA")
        {
            string[] LICINFO = new string[2];
            LICINFO = licenseType.ToUpper().Split('_');
            LICINFO[0] = "ORDER_" + LICINFO[0];
            LICINFO[1] = LICINFO[1] + "_NOTATION";
            List<string> listConfg = new List<string>();
            listConfg.Add(licenseType);
            listConfg.Add(Constants.TAX);
            listConfg.Add(LICINFO[0]);
            listConfg.Add(LICINFO[1]);
            listConfg.Add(Constants.MERCHANTID);
            var configvalue = ServiceFactory.GetService<ConfigService>().GetConfigurationValues(listConfg);
            hdnType.Value = licenseType;
            orderNo = "POLL";
            foreach (var cv in configvalue)
            {
                if (cv.CONFIG_KEY == licenseType)
                {
                    price = Convert.ToDouble(cv.CONFIG_VALUE);
                }
                if (cv.CONFIG_KEY == Constants.TAX)
                {
                    taxPercentage = Convert.ToDouble(cv.CONFIG_VALUE.ToString());
                    hdnTax.Value = taxPercentage.ToString();
                }
                if (cv.CONFIG_KEY == Constants.MERCHANTID)
                {
                    hdnMerchantId.Value = cv.CONFIG_VALUE.ToString();
                }
                if (cv.CONFIG_KEY == LICINFO[1])
                {
                    orderNo += "-" + cv.CONFIG_VALUE.ToString();
                }

            }
            hdnPrice.Value = price.ToString();
            tax = Math.Round(Convert.ToDouble(price * Convert.ToDouble(taxPercentage) / 100));
            hdnTaxAmount.Value = tax.ToString();
            hdnTotal.Value = Math.Round(price + tax, 2).ToString();
            hdnOrderNo.Value = orderNo;

            var userService = ServiceFactory.GetService<UsersService>();
            var user = userService.FindUserByUserId(UserId);
            var order = GetOrderInfoDetails(user.USERID);

           // Pc.UpdatePollLicenseType(user.USERID, licenseType, 2);

            

            DataSet dspolloinfo = surcore.getpollorderinfo(order.USERID, order.MODEOFPAYMENT);
            DataSet dsinsOrder = surcore.insertpollinfo(user.USERID, order.ORDERID, order.MODEOFPAYMENT, order.CREATED_DATE, order.PRODUCT_PRICE, order.TAX_PRICE, order.TOTAL_PRICE, Convert.ToDateTime(order.PAID_DATE), order.TRANSACTION_STATUS, Convert.ToDouble(order.TAX_PERCENT), order.ORDER_TYPE, order.CurrencyType, Convert.ToDouble(order.ExchangeRate), order.ModifiedDate);

            order.PK_ORDERID = Convert.ToInt32(dsinsOrder.Tables[0].Rows[0][0].ToString());
            
            //var prevOrderDetails = ServiceFactory<OrderInfoService>.Instance.CheckOrderInfoDetailsExists(order.USERID, order.MODEOFPAYMENT);
            //if (!prevOrderDetails)
            //{
            //    order = userService.InsertOrderInfo(order);
            //    if (order != null)
            //    {
                  //  var history = GetAccountHistoryInfo(user.USERID, order.PK_ORDERID.ToString());
                  //  var historyId = userService.InsertUserAccountHistory(history);
            DataSet dsinsertuserhistory = surcore.InsertPolluserhistory(order.PK_ORDERID, UserId, licenseType);

                    string orderID = orderNo + order.PK_ORDERID;

                    DataSet dsorderno = surcore.updatePollOrderinfo(order.PK_ORDERID, orderID);


                    string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollsUserCheckoutPageURL(), "Userid=" + user.USERID + "&Order=" + orderID + "&Amount=" + hdnTotal.Value + "&Ren=0" + "&BillCName=" + user.FIRST_NAME + "&email=" + user.LOGIN_NAME + "&planchosen=" + licenseType + "&planamount=" + price + "&taxamount=" + hdnTaxAmount.Value + "&taxpercentage=" + taxPercentage.ToString() + "&pk_order=" + order.PK_ORDERID.ToString());
                    Response.Redirect(strRedirectUrl, false);
            //    }

            //}


        }
        else
        {
            hdnType.Value = licenseType;
            orderNo = "POLL";

            var userService = ServiceFactory.GetService<UsersService>();
            var user = userService.FindUserByUserId(UserId);

            DataSet dsprice = surcore.getPriceLicenseCountry(countryName, licenseType);


            hdnPrice.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()), 2).ToString();
            tax = 0;
            hdnTaxAmount.Value = "0";
            hdnTotal.Value = Math.Round(Convert.ToDouble(dsprice.Tables[0].Rows[0][4].ToString()) + tax, 2).ToString();
            hdnOrderNo.Value = orderNo;

            var order = GetOrderInfoDetails(user.USERID);
            
          //  Pc.UpdatePollLicenseType(user.USERID, licenseType, 2);

            DataSet dspolloinfo = surcore.getpollorderinfo(order.USERID, order.MODEOFPAYMENT);
            DataSet dsinsOrder = surcore.insertpollinfo(user.USERID, order.ORDERID, order.MODEOFPAYMENT, order.CREATED_DATE, order.PRODUCT_PRICE, order.TAX_PRICE, order.TOTAL_PRICE, Convert.ToDateTime(order.PAID_DATE), order.TRANSACTION_STATUS, Convert.ToDouble(order.TAX_PERCENT), order.ORDER_TYPE, order.CurrencyType, Convert.ToDouble(order.ExchangeRate), order.ModifiedDate);

            order.PK_ORDERID = Convert.ToInt32(dsinsOrder.Tables[0].Rows[0][0].ToString());

            //var history = GetAccountHistoryInfo(user.USERID, order.PK_ORDERID.ToString());
            //var historyId = userService.InsertUserAccountHistory(history);

            DataSet dsinsertuserhistory = surcore.InsertPolluserhistory(order.PK_ORDERID, UserId, licenseType);

            string orderID = orderNo + order.PK_ORDERID;

            DataSet dsorderno = surcore.updatePollOrderinfo(order.PK_ORDERID, orderID);


            DataSet dsorderdefualtstatus = surcore.updatePollSaasyDefaultstatus(order.PK_ORDERID);
           // DataSet dsupdatepolllicense = surcore.updatePollLicense(user.USERID, licenseType, DateTime.Now);
            

            string strRedirectUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetNonIndianUserCheckoutPageURL(), "fName=" + user.FIRST_NAME + "&Order=" + orderID + "&emailId=" + user.LOGIN_NAME + "&subscriptionPlan=" + licenseType.ToUpper());
            Response.Redirect(strRedirectUrl, false);
        }
    }

    protected void btnprof2_Click(object sender, EventArgs e)
    {
        string licenseType = "PROFESSIONAL_MONTHLY";
        bindpaymentdetails(licenseType);
    }
    
    protected void btnbus2_Click(object sender, EventArgs e)
    {
        string licenseType = "BUSINESS_MONTHLY";
        bindpaymentdetails(licenseType);
    }
    
    protected void btnpub2_Click(object sender, EventArgs e)
    {
        string licenseType = "PUBLISHER_MONTHLY";
        bindpaymentdetails(licenseType);
    }
    
    protected void lnkProf_Click(object sender, EventArgs e)
    {
        string licenseType = "PROFESSIONAL_YEARLY";
        bindpaymentdetails(licenseType);
        
    }
    
    protected void lnkBus_Click(object sender, EventArgs e)
    {
        string licenseType = "BUSINESS_YEARLY";
        bindpaymentdetails(licenseType);
    }
    
    protected void lnkPub_Click(object sender, EventArgs e)
    {
        string licenseType = "PUBLISHER_YEARLY";
        bindpaymentdetails(licenseType);
    }
}