﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Text;
using System.Configuration;
using Insighto.Data;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;

public partial class Poll_PollPreview : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int pollId = 0;
    string Printop = "";
    DataSet pollInfo = new DataSet();
    DataTable dtSettingsInfo = new DataTable();
    PollCreation Pc = new PollCreation();
    string resultAccess;
    DataTable tbAnswerOption = new DataTable();
    int respondentId;
    int answerId = 0, questionType = 1;
    string device;
    ListItem answerOption;
    string foldername = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            if (typeHt.Contains("PollId"))
                pollId = Convert.ToInt32(typeHt["PollId"].ToString());
            if (typeHt.Contains("Print"))
                Printop = typeHt["Print"].ToString();
        }
        foldername = @"images\" + Convert.ToString(pollId);
        if (Printop == "")
        {
            print.Style.Add("display", "none");
        }
        else
        {
            print.Style.Add("display", "block");
        }
        if (!IsPostBack)
        {
            BindDesignStyles();
        }
    }
    protected void BindDesignStyles()
    {
        pollInfo = Pc.getPollInfo(pollId);
        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
        {
            imagepreview.Style.Add("display", "block");
            if (pollInfo.Tables[0].Rows[0][19].ToString() != "")
            {
                imgPreview.Attributes.Add("src", foldername + @"\" + pollInfo.Tables[0].Rows[0][19].ToString());
            }
            hdnWidth.Value = "300";
            hdnHeight.Value = "319";
        }
        else if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
        {
            videopreview.Style.Add("display", "block");
            if (pollInfo.Tables[0].Rows[0][18].ToString() != "")
            {
                videopreview.InnerHtml= pollInfo.Tables[0].Rows[0][18].ToString();
            }
            hdnWidth.Value = "300";
            hdnHeight.Value = "350";
        }
        else
        {
            hdnWidth.Value = "300";
            hdnHeight.Value = "184";
        }
        lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
        if (pollInfo.Tables[0].Rows[0][10].ToString() == "private")
        {
            resultAccess = "private";
        }
        else
        {
            resultAccess = "public";
        }
        if (pollInfo.Tables[0].Rows[0][14].ToString() == "no")
        {
            poweredby.Style.Add("display", "none");
        }
        else
        {
            poweredby.Style.Add("display", "block");
        }
        if (Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()) == 1)
        {
            singleradbtns.Style.Add("display", "block");
            multipleckbbtns.Style.Add("display", "none");
            tbAnswerOption = Pc.getAnswerOptions(pollId);
            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                foreach (DataRow row in tbAnswerOption.Rows)
                {
                    answerOption = new ListItem(row["answeroption"].ToString(), row["pk_answerid"].ToString());
                    radAnswerOptions.Items.Add(answerOption);
                }
            }
            questionType = 1;
        }
        else if (Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()) == 2)
        {
            questionType = 2;
            singleradbtns.Style.Add("display", "none");
            multipleckbbtns.Style.Add("display", "block");
            tbAnswerOption = Pc.getAnswerOptions(pollId);
            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                foreach (DataRow row in tbAnswerOption.Rows)
                {
                    answerOption = new ListItem(row["answeroption"].ToString(), row["pk_answerid"].ToString());
                    ckbAnswerOptions.Items.Add(answerOption);
                }
            }
        }
        if (Convert.ToBoolean(pollInfo.Tables[0].Rows[0][17].ToString()) == true)
        {
            commentbox.Style.Add("display", "block");
        }
        else
        {
            commentbox.Style.Add("display", "none");
        }

        dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
        if (dtSettingsInfo != null && dtSettingsInfo.Rows.Count > 0)
        {
            if (dtSettingsInfo.Rows[0]["pollbgcolor"].ToString() != "")
            {
                hdnPollBgColor.Value = dtSettingsInfo.Rows[0]["pollbgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionfont"].ToString() != "")
            {
                hdnQuestionFont.Value = dtSettingsInfo.Rows[0]["questionfont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfont"].ToString() != "")
            {
                hdnAnswerFont.Value = dtSettingsInfo.Rows[0]["answerfont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefont"].ToString() != "")
            {
                hdnMsgFont.Value = dtSettingsInfo.Rows[0]["messagefont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefont"].ToString() != "")
            {
                hdnVoteFont.Value = dtSettingsInfo.Rows[0]["votefont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questioncolor"].ToString() != "")
            {
                hdnQuestColor.Value = dtSettingsInfo.Rows[0]["questioncolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionbgcolor"].ToString() != "")
            {
                hdnQuestBgColor.Value = dtSettingsInfo.Rows[0]["questionbgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answercolor"].ToString() != "")
            {
                hdnAnswerColor.Value = dtSettingsInfo.Rows[0]["answercolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votebgcolor"].ToString() != "")
            {
                hdnVoteBgColor.Value = dtSettingsInfo.Rows[0]["votebgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votecolor"].ToString() != "")
            {
                hdnVoteColor.Value = dtSettingsInfo.Rows[0]["votecolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionfontweight"].ToString() != "")
            {
                hdnQuestionBold.Value = dtSettingsInfo.Rows[0]["questionfontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionstyle"].ToString() != "")
            {
                hdnQuestionItalic.Value = dtSettingsInfo.Rows[0]["questionstyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionunderline"].ToString() != "")
            {
                hdnQuestionUL.Value = dtSettingsInfo.Rows[0]["questionunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionalign"].ToString() != "")
            {
                hdnQuestionAlign.Value = dtSettingsInfo.Rows[0]["questionalign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfontweight"].ToString() != "")
            {
                hdnAnswerBold.Value = dtSettingsInfo.Rows[0]["answerfontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerstyle"].ToString() != "")
            {
                hdnAnswerItalic.Value = dtSettingsInfo.Rows[0]["answerstyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerunderline"].ToString() != "")
            {
                hdnAnswerUL.Value = dtSettingsInfo.Rows[0]["answerunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answeralign"].ToString() != "")
            {
                hdnAnswerAlign.Value = dtSettingsInfo.Rows[0]["answeralign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefontweight"].ToString() != "")
            {
                hdnVoteBold.Value = dtSettingsInfo.Rows[0]["votefontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votestyle"].ToString() != "")
            {
                hdnVoteItalic.Value = dtSettingsInfo.Rows[0]["votestyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["voteunderline"].ToString() != "")
            {
                hdnVoteUL.Value = dtSettingsInfo.Rows[0]["voteunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votealign"].ToString() != "")
            {
                hdnVoteAlign.Value = dtSettingsInfo.Rows[0]["votealign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefontweight"].ToString() != "")
            {
                hdnMsgBold.Value = dtSettingsInfo.Rows[0]["messagefontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagestyle"].ToString() != "")
            {
                hdnMsgItalic.Value = dtSettingsInfo.Rows[0]["messagestyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messageunderline"].ToString() != "")
            {
                hdnMsgUL.Value = dtSettingsInfo.Rows[0]["messageunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagealign"].ToString() != "")
            {
                hdnMsgAlign.Value = dtSettingsInfo.Rows[0]["messagealign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionfontsize"].ToString() != "")
            {
                hdnQuestionFSize.Value = dtSettingsInfo.Rows[0]["questionfontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfontsize"].ToString() != "")
            {
                hdnAnswerFSize.Value = dtSettingsInfo.Rows[0]["answerfontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefontsize"].ToString() != "")
            {
                hdnVoteFSize.Value = dtSettingsInfo.Rows[0]["votefontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefontsize"].ToString() != "")
            {
                hdnMsgFSize.Value = dtSettingsInfo.Rows[0]["messagefontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["iframewidth"].ToString() != "")
            {
                hdnWidth.Value = dtSettingsInfo.Rows[0]["iframewidth"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["iframeheight"].ToString() != "")
            {
                hdnHeight.Value = dtSettingsInfo.Rows[0]["iframeheight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["recmheight"].ToString() != "")
            {
                hdnRecHeight.Value = dtSettingsInfo.Rows[0]["recmheight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["recmwidth"].ToString() != "")
            {
                hdnRecWidth.Value = dtSettingsInfo.Rows[0]["recmwidth"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "" && dtSettingsInfo.Rows[0]["sponsortext"].ToString() != "")
            {
                lblSponsor.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                sponsorpreviw.Style.Add("display", "block");
                logo.Attributes.Add("src", foldername + @"\" + dtSettingsInfo.Rows[0]["sponsorlogo"].ToString());
                if (dtSettingsInfo.Rows[0]["logolink"].ToString() != "")
                {
                    logolink.Attributes.Add("href", dtSettingsInfo.Rows[0]["logolink"].ToString());
                }
                else
                {
                    logolink.Attributes.Add("href", "#");
                }
                onlyvote.Style.Add("display", "none");
                voteandview.Style.Add("display", "block");
            }
            else
            {
                sponsorpreviw.Style.Add("display", "none");
            }
        }
        else
        {
            hdnAnswerFSize.Value = "12";
            hdnQuestionFSize.Value = "12";
            hdnVoteFSize.Value = "13";
            hdnMsgFSize.Value = "12";
            hdnQuestionBold.Value = "normal";
            hdnQuestionItalic.Value = "normal";
            hdnQuestionUL.Value = "none";
            hdnQuestionAlign.Value = "left";
            hdnAnswerBold.Value = "normal";
            hdnAnswerItalic.Value = "normal";
            hdnAnswerUL.Value = "none";
            hdnAnswerAlign.Value = "left"; ;
            hdnVoteBold.Value = "bold";
            hdnVoteItalic.Value = "normal";
            hdnVoteUL.Value = "none";
            hdnVoteAlign.Value = "center";
            hdnMsgAlign.Value = "left";
            hdnMsgBold.Value = "normal";
            hdnMsgItalic.Value = "normal";
            hdnMsgUL.Value = "none";
            hdnPollBgColor.Value = "ffffff";
            hdnAnswerColor.Value = "000000";
            hdnQuestBgColor.Value = "e7e7e7";
            hdnQuestColor.Value = "000000";
            hdnVoteBgColor.Value = "6C89E1";
            hdnVoteColor.Value = "ffffff";
        }
    }
   
}