﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Helpers;

public partial class Poll_LastPage : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    string Unique;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            if (typeHt.Contains("Unique"))
            {
                lblMsg.Text = "You have already voted for this Poll.";
            }
        }
    }
}