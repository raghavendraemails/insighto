﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="ActivateSubType.aspx.cs" Inherits="ActivateSubType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div class="popupheadsurvey" id="SurveyActivation" runat="server">
<div>
<div class="questionpopup" >You are currently not signed into Insighto Surveys.</div>
<div class="bgactivatesurveys"><asp:Button ID="btnActivate" runat="server" 
        CssClass="btnactivatesurveys" Text="Activate Free Subscription" 
        onclick="btnActivate_Click" /></div>
        </div>
    </div>
    
<div class="popupheadsurvey" id="PollActivation" runat="server">
        <div>
<div class="questionpopup">You are currently not signed into Insighto Polls.</div>
<div class="bgactivatesurveys"><asp:Button ID="btnActivatePoll" runat="server" 
        CssClass="btnactivatesurveys" Text="Activate Free Subscription" 
        onclick="btnActivatePoll_Click"/></div>
        </div>
    </div>
    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-55613473-1', 'auto');
        ga('send', 'pageview');

</script>
</asp:Content>

