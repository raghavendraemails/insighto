﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PollPreview.aspx.cs" Inherits="Poll_PollPreview" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="https://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, maximum-scale=1.0" />
<script src="https://code.jquery.com/jquery-1.8.2.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../App_Themes/Classic/layoutStyle.css" />
<link rel="stylesheet" type="text/css" href="https://insightopollsssl-a59.kxcdn.com/css/poll.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="scripts/pollpreview.js" type="text/javascript"></script>
    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-55613473-1', 'auto');
        ga('send', 'pageview');

</script>
</head>
<body class="popupBody" alink="#ffffff" style="background-color:transparent;margin-top:-30px;">
    <form id="form1" runat="server">
    <div>
    <asp:HiddenField ID="hdnWidth" runat="server" />
<asp:HiddenField ID="hdnHeight" runat="server" />
    <asp:HiddenField ID="hdnRecHeight" runat="server" />
<asp:HiddenField ID="hdnRecWidth" runat="server" />
<asp:HiddenField ID="hdnQuestionFont" runat="server" />
<asp:HiddenField ID="hdnQuestionBold" runat="server" />
<asp:HiddenField ID="hdnQuestionItalic" runat="server" />
<asp:HiddenField ID="hdnQuestionUL" runat="server" />
<asp:HiddenField ID="hdnQuestionAlign" runat="server" />
<asp:HiddenField ID="hdnQuestionFSize" runat="server" />
<asp:HiddenField ID="hdnAnswerFont" runat="server" />
<asp:HiddenField ID="hdnAnswerBold" runat="server" />
<asp:HiddenField ID="hdnAnswerItalic" runat="server" />
<asp:HiddenField ID="hdnAnswerUL" runat="server" />
<asp:HiddenField ID="hdnAnswerAlign" runat="server" />
<asp:HiddenField ID="hdnAnswerFSize" runat="server" />
<asp:HiddenField ID="hdnVoteFont" runat="server" />
<asp:HiddenField ID="hdnVoteBold" runat="server" />
<asp:HiddenField ID="hdnVoteItalic" runat="server" />
<asp:HiddenField ID="hdnVoteUL" runat="server" />
<asp:HiddenField ID="hdnVoteFSize" runat="server" />
<asp:HiddenField ID="hdnVoteAlign" runat="server" />
<asp:HiddenField ID="hdnMsgFont" runat="server" />
<asp:HiddenField ID="hdnMsgBold" runat="server" />
<asp:HiddenField ID="hdnMsgItalic" runat="server" />
<asp:HiddenField ID="hdnMsgUL" runat="server" />
<asp:HiddenField ID="hdnMsgFSize" runat="server" />
<asp:HiddenField ID="hdnMsgAlign" runat="server" />
<asp:HiddenField ID="hdnPollBgColor" runat="server" />
<asp:HiddenField ID="hdnQuestBgColor" runat="server" />
<asp:HiddenField ID="hdnQuestColor" runat="server" />
<asp:HiddenField ID="hdnAnswerColor" runat="server" />
<asp:HiddenField ID="hdnVoteBgColor" runat="server" />
<asp:HiddenField ID="hdnVoteColor" runat="server" />
<asp:HiddenField ID="hdnCountry" runat="server" />
<asp:HiddenField ID="hdnState" runat="server" />
<asp:HiddenField ID="hdnLongitude" runat="server" />
<asp:HiddenField ID="hdnLatitude" runat="server" />
<asp:HiddenField ID="hdnIpAddress" runat="server" />
<asp:HiddenField ID="hdnCity" runat="server" />
<!--<div class="printpoll" id="print" runat="server"><asp:HyperLink ID="hlnkPrint" runat="server" NavigateUrl="#" CssClass="printLink"
            onclick="Javascript:window.print();return false;" Text="Print" ToolTip="Print"></asp:HyperLink></div>-->
    <div class="textpollthemeprint">
                 <div id="imagepreview" runat="server" style="display:none"><img src="App_Themes/images/ImagePoll.jpg" id="imgPreview" runat="server" /></div>
                 <div id="videopreview" runat="server" class="videopreview" style="display:none"></div>
                     <div class="questionpreviewcss">
                         <asp:Label ID="lblQuestion" runat="server"></asp:Label></div>
                     <div class="clear"></div>
                     <div id="singleradbtns" runat="server">
                     <div class="radiopolltheme"><asp:RadioButtonList ID="radAnswerOptions" runat="server">
                         </asp:RadioButtonList></div>
                     </div>
                     <div id="multipleckbbtns" style="display:none" runat="server">
                    <div class="radiopolltheme"><asp:CheckBoxList ID="ckbAnswerOptions" runat="server">
                    </asp:CheckBoxList></div>
                     </div>
                      <div id="commentbox" style="display:none" runat="server">
                      <div class="commentstxt" >Comments</div>
                     <div><asp:TextBox ID="txtComments" CssClass="textBoxMedium txtAns txtcomments" runat="server"></asp:TextBox></div>
                     </div >
                      <div style="padding-top: 10px;" class="voteres">
                       <div class="viewandvoteimageresponse" id="onlyvote" runat="server"><div class="vote"><asp:Button ID="btnVoteAn" 
                                   CssClass="viewandvoteimageresponsebtn" runat="server" Text="Vote and View Results" 
             /></div></div>
                           <div class="viewandvoteimageresponse" style="display:none" id="voteandview" runat="server">
                           <div class="vote"><asp:Button ID="btnVote" 
                                   CssClass="viewandvoteimageresponsebtn" runat="server" Text="Vote " /></div><div class="view"> and View Results</div></div>
                           <div class="sponsorimage" id="sponsorpreviw" runat="server" style="display:none">
                               <div><asp:Label ID="lblSponsor" Text="Sponsored By" runat="server"></asp:Label></div>
                               <div><a href="#" id="logolink" runat="server"><img src="App_Themes/images/logoPlaceholder2.png" runat="server" id="logo" /></a></div>
                           </div>
                       </div>
                       <div class="clear"></div>
                     <div id="poweredby" class="poweredbyrep" runat="server">Powered by <a href="https://www.insighto.com/Polls/" target="_blank">Insighto Polls</a></div>
                 </div>
    </div>
    </form>
</body>
</html>

