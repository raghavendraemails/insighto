﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Services;
using System.Web.Script.Serialization;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;
using System.Collections;
using Insighto.Business.Helpers;
using System.Text;
using Insighto.Business.ValueObjects;
using App_Code;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class Polls_new_design_MyPolls : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PollCreation Pc = new PollCreation(); 
        int userId = 0;
        int activation = 0;
        var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
        if (userSessionDetails != null)
        {
            Int32.TryParse(userSessionDetails.ActivationFlag.ToString(), out activation);
            activationflag.Value = activation.ToString();
            userId = userSessionDetails.UserId;
            Pc.UpdateUserLastActivity("Poll", userId);
            if (Session["activationalert"] == null)
            {
                if (userSessionDetails.ActivationFlag == null)
                {
                    Session["activationalert"] = "1";
                }
                else
                {
                    Session["activationalert"] = "0";
                }
            }
            else {
                Session["activationalert"] = "0";
            }
            activationalert.Value = Session["activationalert"].ToString();
            //var userDetails = ServiceFactory<UsersService>.Instance.GetUserDetails(userId);
        }
        else {
            Response.Redirect("/Home.aspx");
        }
    }
}