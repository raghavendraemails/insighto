﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditPollName.aspx.cs"  MasterPageFile="~/ModelMaster.master" Inherits="Poll_EditPollName" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/poll.css" />
<script src="scripts/customscripts.js" type="text/javascript"></script>
    <div >
    <div class="floatL">
         <div class="popupTitle" style="padding-left:20px">Edit Poll Name*</div>
         <div style="padding-left:20px"><asp:TextBox ID="txtPollName" MaxLength="50" Width="200" runat="server" class="textBoxMedium"></asp:TextBox></div>
         <div class="clear"><!-- validation -->
                        <div class="btmpadding">
                            <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqSurveyName" 
                                ControlToValidate="txtPollName"  ErrorMessage ="Please enter poll name."
							CssClass="lblRequired"  runat="server" 
                                Display="Dynamic" ></asp:RequiredFieldValidator>
                         </div>
                   <!-- //row --> 
          </div></div>
          <div class="clear"></div>
          <div class="floatR" style="padding-right:10px"><asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                  onclick="btnSubmit_Click" /></div>
        
    </div>
        <script type="text/javascript">
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-55613473-1', 'auto');
            ga('send', 'pageview');

</script>
</asp:Content>
