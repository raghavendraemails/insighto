﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.geoip;
using System.Text;
using System.Configuration;
using Insighto.Data;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Net;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Serialization;
//using FiftyOne.Foundation.Mobile.Detection;


public partial class Polls_new_design_PollRespondentIframe : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int pollId = 0;
    int parentpollId = 0;
    DataSet pollInfo = new DataSet();
    DataTable dtSettingsInfo = new DataTable();
    PollCreation Pc = new PollCreation();
    string resultaccess;
    DataTable tbAnswerOption = new DataTable();
    DataSet licInfo = new DataSet();
    int respondentId;
    int answerId = 0, questionType = 1;
    string device = "";
    string mobileBrand = "";
    ListItem answerOption;
    string platform = "", mobilePlatform = "";
    public String statusCode { get; set; }
    public String statusMessage { get; set; }
    public String IpAddress { get; set; }
    public String countryCode { get; set; }
    public String countryName { get; set; }
    public String regionName { get; set; }
    public String cityName { get; set; }
    public String zipCode { get; set; }
    public String latitude { get; set; }
    public String longitude { get; set; }
    public String timeZone { get; set; }
    // string globalurl = "http://insighto.com:8010";
    string globalurl;
    string foldername = "";
    bool isPoweredByShowing = false;
    bool isSponsorLogoShowing = false;
    bool isSocialSharingShowing = false;
    int chartheight = 0;
    int chartwidth = 0;
    string pollsource = "";
    public string facebooktitle = "Insighto Polls";
    public string facebookimage = "/polls/images/insighto-social-media-2.jpg";
    string rootURL = "";
    string pollsURL = "";
    public string questiontext = "";

    string source = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //Request.Headers.Add("X-XSS-Protection", "allow");
        string ipAddress = "";
        string querystring = "";
        globalurl = ConfigurationSettings.AppSettings["RootURL"].ToString();

        rootURL = ConfigurationSettings.AppSettings["RootURL"].ToString();
        pollsURL = ConfigurationSettings.AppSettings["PollsURL"].ToString();
        try
        {
            if (rootURL.Contains("localhost"))
            {
                rootURL = "https://test.insighto.com/polls";
            }
            if (pollsURL.Contains("localhost"))
            {
                pollsURL = "https://test.insighto.com/polls";
            }
            if (Request.QueryString["key"] != null)
            {
                querystring = Request.QueryString["key"].ToString();
                //querystring = WebUtility.HtmlDecode(querystring);
                typeHt = EncryptHelper.DecryptQuerystringParam(querystring);

                if (typeHt.Contains("ParentPollId"))
                {
                    parentpollId = Convert.ToInt32(typeHt["ParentPollId"].ToString());
                }
                else
                {
                    parentpollId = Convert.ToInt32(typeHt["PollId"].ToString());
                }
                if (typeHt.Contains("pollsource"))
                {
                    pollsource = typeHt["pollsource"].ToString();
                }
                if (typeHt.Contains("source"))
                {
                    source = typeHt["source"].ToString();
                }
                DataSet dspollid = Pc.getPollInfofromparent(parentpollId);
                if (dspollid.Tables[0].Rows.Count > 0)
                {
                    pollId = Convert.ToInt32(dspollid.Tables[0].Rows[0][0].ToString());
                }
                else
                {
                    pollId = parentpollId;
                }
            }

        }
        catch (Exception e1)
        {
            //Response.Write(e1.ToString());
            //Response.End();
        }
        hdn.Value = pollId.ToString();
        foldername = @"images\" + Convert.ToString(pollId);
        form1.Action = globalurl + Request.RawUrl;

        Session["respondentId"] = "";
        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
        {
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        }
        //ipAddress = "183.83.200.83";

        HttpRequest httpRequest = HttpContext.Current.Request;
        if (httpRequest.Browser.IsMobileDevice)
        {
            device = "mobile";
            mobileBrand = httpRequest.Browser.MobileDeviceManufacturer;
            mobilePlatform = httpRequest.Browser.Platform;
        }
        else
        {
            device = "desktop";
            platform = httpRequest.Browser.Platform;
        }

        string pollURL = "";
        
        IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
        string shortened = "";
        string absurl = Request.Url.AbsoluteUri.ToString();
        try
        {
            if (absurl.Contains("localhost")) {
                absurl = "https://test.insighto.com/polls?key=" + Request.QueryString["key"];
            }
            shortened = s.Shorten(absurl);
            if (shortened != null)
            {
                pollURL = shortened;
            }

        }
        catch (Exception)
        {
        }
        hdnSrcUrl.Value = pollURL;
        hdnLogo.Value = facebookimage;
        hdnSource.Value = source;
        if (!IsPostBack)
        {
            Session["respondentId"] = "";
            if (Request.Cookies["respondent" + pollId.ToString()] == null)
            {
                HttpCookie respCookie = new HttpCookie("respondent" + pollId.ToString());
                respCookie.Value = respondentId.ToString();
                respCookie.Expires = DateTime.Now.AddDays(10);
                //respCookie.Domain = "127.0.0.1";
                respCookie.Path = "/";
                respCookie.HttpOnly = false;
                Response.Cookies.Add(respCookie);
            }

            BindDesignStyles();
            try
            {
                Location location = getIPDetails(ipAddress);
                if (location != null)
                {
                    IpAddress = (ipAddress ?? "");
                    countryCode = (location.countryCode ?? "");
                    countryName = (location.countryName ?? "");
                    regionName = (location.regionName ?? "");
                    cityName = (location.city ?? "");
                    zipCode = (location.postalCode ?? "");
                    latitude = (location.latitude.ToString() ?? "");
                    longitude = (location.longitude.ToString() ?? "");
                    timeZone = "";
                }
                else {
                    IpAddress = "";
                    countryCode = "";
                    countryName = "NOT FOUND";
                    regionName = "";
                    cityName = "";
                    zipCode = "";
                    latitude = "";
                    longitude = "";
                    timeZone = "";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //if (res != null)
                //{ res.Close(); }
            }
            licInfo = Pc.getPollLicenceInfo(pollId);

            if (licInfo.Tables[2].Rows.Count > 0)
            {
                // string domainname = Request.UrlReferrer.Host.ToString();
                // string domainname = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.ToString();
                //var uri = new Uri(Request.RawUrl); // to get the url from request or replace by your own
                //var domainname = uri.GetLeftPart(UriPartial.Authority);

                string domainname = Request.Url.Host;

                if (licInfo.Tables[2].Rows[0][0].ToString() == "")
                {
                    Pc.updateDomainname(Convert.ToInt32(licInfo.Tables[2].Rows[0][1].ToString()), domainname);
                }
                else if ((licInfo.Tables[2].Rows[0][0].ToString() != domainname) && (pollsource != "weblink"))
                {
                    Response.Write("SORRY! You would not be able to embed your Poll with another domain than the one you have already used earlier. Create another account in Insighto Polls to embed this poll in this domain. For any queries - support@insighto.com");
                    Response.End();
                }
            }


            if (licInfo.Tables[1].Rows.Count > 0)
            {
                if (licInfo.Tables[1].Rows[0][0].ToString() == "Professional_freetrial" || licInfo.Tables[1].Rows[0][0].ToString() == "Professional")
                {
                    if (Convert.ToInt32(licInfo.Tables[0].Rows[0][0].ToString()) > 3000)
                    {
                        Response.Redirect("lastpage.aspx");
                    }
                }
                else if (licInfo.Tables[0].Rows[0][0].ToString() == "Business_freetrial" || licInfo.Tables[0].Rows[0][0].ToString() == "Business")
                {
                    if (Convert.ToInt32(licInfo.Tables[0].Rows[0][0].ToString()) > 10000)
                    {
                        Response.Redirect("lastpage.aspx");
                    }
                }
            }
            pollInfo = Pc.getPollInfo(pollId);
            hdnPollType.Value = pollInfo.Tables[0].Rows[0]["type"].ToString().ToLower();
            facebooktitle = pollInfo.Tables[0].Rows[0]["questiontext"].ToString();

            hdnQuestionText.Value = facebooktitle;

            string facebookdescription = "Click to vote";
            if (hdnPollType.Value == "video")
            {
                facebookdescription = "WATCH THE VIDEO and give your opinion";
            }

            HtmlMeta metaTag = new HtmlMeta();
            metaTag.Attributes.Add("property", "og:title");
            metaTag.Content = facebooktitle;// +" (" + facebookdescription + ")";
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("itemprop", "og:title");
            metaTag.Content = facebooktitle;// +" (" + facebookdescription + ")";
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("property", "og:description");
            metaTag.Content = facebookdescription;
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("itemprop", "og:description");
            metaTag.Content = facebookdescription;
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("property", "og:image");
            metaTag.Content = ConfigurationManager.AppSettings["socialmediashareimage"].ToString();
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("itemprop", "og:image");
            metaTag.Content = ConfigurationManager.AppSettings["socialmediashareimage"].ToString();
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("property", "og:url");
            metaTag.Content = absurl + "&v=" + DateTime.Now.Ticks.ToString();
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("itemprop", "og:url");
            metaTag.Content = absurl + "&v=" + DateTime.Now.Ticks.ToString();
            this.Header.Controls.Add(metaTag);

            hdnPollURLLong.Value = absurl + "&v=" + DateTime.Now.Ticks.ToString();

            hdnFBImage.Value = facebookimage;

            string sourceURL = "";
            sourceURL = hdnSrcUrl.Value;
            if (!String.IsNullOrWhiteSpace(Request.QueryString["utm_source"]))
            {
                sourceURL = Request.QueryString["utm_source"];
            }
            if (sourceURL.Length > 100)
            {
                sourceURL = sourceURL.Substring(0, 100);
            }
            hdnSrcUrl.Value = sourceURL;
            if (!hdnSrcUrl.Value.Contains("CreatePoll.aspx"))
            {
                if (pollInfo.Tables[0].Rows[0]["status"].ToString() == "Active")
                {
                    //DOING THIS CHECK SO THAT WHEN COOKIE IS CHECKED THE VISIT COUNT DOES NOT INCREASE TWICE
                    ///BUT THIS IS ALSO TO TAKE CARE FROM THE SAME USER BEING ABLE TO VOTE TWICE BY USING THE BACK BUTTON ON THE FRAME.
                    //if ((Request.Cookies["respondent" + pollId.ToString()].Value == "0") || (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["uniquerespondent"]) == 1))
                    if (Request.Cookies["respondent" + pollId.ToString()].Value == "0")
                    {
                        respondentId = Pc.InsertIntoRespondent(pollId, "visits", countryName, regionName, cityName, IpAddress, longitude, latitude, device, platform, mobileBrand, mobilePlatform, hdnSrcUrl.Value);
                        hdnRespondentId.Value = respondentId.ToString();
                        Session["respondentId"] = respondentId.ToString();
                        respid.Value = respid.ToString();
                    }
                    else //if(Convert.ToInt32(pollInfo.Tables[0].Rows[0]["uniquerespondent"]) == 0)
                    {
                        //Pc.InsertIntoRespondent(pollId, "visits", countryName, regionName, cityName, IpAddress, longitude, latitude, device, platform, mobileBrand, mobilePlatform, hdnSrcUrl.Value);
                        respid.Value = Request.Cookies["respondent" + pollId.ToString()].Value;
                        hdnRespondentId.Value = Convert.ToString(Request.Cookies["respondent" + pollId.ToString()].Value);
                        Session["respondentId"] = Request.Cookies["respondent" + pollId.ToString()].Value;
                    }
                }
            }
            else {
                hdnRespondentId.Value = "0";
            }
            respid.Value = Request.Cookies["respondent" + pollId.ToString()].Value;
        }
    }

    public class ResponseDetails
    {
        public string Ip { get; set; }
        public string Countrycode { get; set; }
        public string CountryName { get; set; }
        public string RegionCode { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string MetroCode { get; set; }
    }
    protected void BindDesignStyles()
    {
        pollInfo = Pc.getPollInfo(pollId);
        dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
        hdnUniqueRes.Value = pollInfo.Tables[0].Rows[0]["uniquerespondent"].ToString();
        hdnPollID.Value = pollId.ToString();
        if (pollInfo.Tables[0].Rows[0][5].ToString() == "Closed" || Convert.ToInt32(pollInfo.Tables[0].Rows[0][4].ToString()) == 1)
        {
            Response.Redirect("lastpage.aspx");
        }
        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
        {
            if (pollInfo.Tables[0].Rows[0][19].ToString() != "")
            {
                imagepreview.Style.Add("display", "block");
                imgPreview.Attributes.Add("src", foldername + @"/" + pollInfo.Tables[0].Rows[0]["imagelink"].ToString());
            }
        }
        else if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
        {
            if (pollInfo.Tables[0].Rows[0][18].ToString() != "")
            {
                videopreview.Style.Add("display", "block");
                videopreview.InnerHtml = pollInfo.Tables[0].Rows[0][18].ToString();
            }
        }
        if (Convert.ToBoolean(dtSettingsInfo.Rows[0]["countvotes"].ToString()) == true)
        {
            DataSet dspollInfo = Pc.getpollchartinfo(pollId);
            lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
        }
        else
        {
            lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
        }
        if (pollInfo.Tables[0].Rows[0][9].ToString() == "private")
        {
            resultaccess = "private";
            lblMessage.Text = pollInfo.Tables[0].Rows[0][10].ToString();
            btnVoteAn.Text = "Vote";
        }
        else
        {
            resultaccess = "public";
        }
        if (Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()) == 1)
        {
            singleradbtns.Style.Add("display", "block");
            multipleckbbtns.Style.Add("display", "none");
            tbAnswerOption = Pc.getAnswerOptions(pollId);
            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                foreach (DataRow row in tbAnswerOption.Rows)
                {
                    answerOption = new ListItem(HttpUtility.HtmlEncode(row["answeroption"].ToString()), row["pk_answerid"].ToString());
                    radAnswerOptions.Items.Add(answerOption);
                }
            }
            questionType = 1;
        }
        else if (Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()) == 2)
        {
            questionType = 2;
            singleradbtns.Style.Add("display", "none");
            multipleckbbtns.Style.Add("display", "block");
            tbAnswerOption = Pc.getAnswerOptions(pollId);
            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                foreach (DataRow row in tbAnswerOption.Rows)
                {
                    answerOption = new ListItem(HttpUtility.HtmlEncode(row["answeroption"].ToString()), row["pk_answerid"].ToString());
                    ckbAnswerOptions.Items.Add(answerOption);
                }
            }
        }
        if (Convert.ToBoolean(pollInfo.Tables[0].Rows[0][17].ToString()) == true)
        {
            commentrow.Style.Add("display", "block");
            commentbox.Style.Add("display", "block");
        }
        else
        {
            commentrow.Style.Add("display", "none");
            commentbox.Style.Add("display", "none");
        }

        if (dtSettingsInfo != null && dtSettingsInfo.Rows.Count > 0)
        {
            if (dtSettingsInfo.Rows[0]["pollbgcolor"].ToString() != "")
            {
                hdnPollBgColor.Value = dtSettingsInfo.Rows[0]["pollbgcolor"].ToString();
            }
            else
            {
                hdnPollBgColor.Value = "ffffff";
            }
            if (dtSettingsInfo.Rows[0]["sharetext"].ToString() != "")
            {
                lblShare.Text = dtSettingsInfo.Rows[0]["sharetext"].ToString();
            }

            if (dtSettingsInfo.Rows[0]["questionfont"].ToString() != "")
            {
                hdnQuestionFont.Value = dtSettingsInfo.Rows[0]["questionfont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfont"].ToString() != "")
            {
                hdnAnswerFont.Value = dtSettingsInfo.Rows[0]["answerfont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefont"].ToString() != "")
            {
                hdnMsgFont.Value = dtSettingsInfo.Rows[0]["messagefont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["googletext"].ToString() != "")
            {
                hdnGoogleText.Value = dtSettingsInfo.Rows[0]["googletext"].ToString();
                googleicon.Style.Add("display", "block");
            }
            if (dtSettingsInfo.Rows[0]["twittertext"].ToString() != "")
            {
                hdnTwitterText.Value = dtSettingsInfo.Rows[0]["twittertext"].ToString();
                twittericon.Style.Add("display", "block");
            }
            if (dtSettingsInfo.Rows[0]["fbtext"].ToString() != "")
            {
                hdnFbText.Value = dtSettingsInfo.Rows[0]["fbtext"].ToString();
                fbicon.Style.Add("display", "block");
            }
            if (dtSettingsInfo.Rows[0]["messagebgcolor"].ToString() != "")
            {
                hdnMsgBgColor.Value = dtSettingsInfo.Rows[0]["messagebgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagecolor"].ToString() != "")
            {
                hdnMsgColor.Value = dtSettingsInfo.Rows[0]["messagecolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefont"].ToString() != "")
            {
                hdnVoteFont.Value = dtSettingsInfo.Rows[0]["votefont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questioncolor"].ToString() != "")
            {
                hdnQuestColor.Value = dtSettingsInfo.Rows[0]["questioncolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionbgcolor"].ToString() != "")
            {
                hdnQuestBgColor.Value = dtSettingsInfo.Rows[0]["questionbgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answercolor"].ToString() != "")
            {
                hdnAnswerColor.Value = dtSettingsInfo.Rows[0]["answercolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votebgcolor"].ToString() != "")
            {
                hdnVoteBgColor.Value = dtSettingsInfo.Rows[0]["votebgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votecolor"].ToString() != "")
            {
                hdnVoteColor.Value = dtSettingsInfo.Rows[0]["votecolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionfontweight"].ToString() != "")
            {
                hdnQuestionBold.Value = dtSettingsInfo.Rows[0]["questionfontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionstyle"].ToString() != "")
            {
                hdnQuestionItalic.Value = dtSettingsInfo.Rows[0]["questionstyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionunderline"].ToString() != "")
            {
                hdnQuestionUL.Value = dtSettingsInfo.Rows[0]["questionunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionalign"].ToString() != "")
            {
                hdnQuestionAlign.Value = dtSettingsInfo.Rows[0]["questionalign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfontweight"].ToString() != "")
            {
                hdnAnswerBold.Value = dtSettingsInfo.Rows[0]["answerfontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerstyle"].ToString() != "")
            {
                hdnAnswerItalic.Value = dtSettingsInfo.Rows[0]["answerstyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerunderline"].ToString() != "")
            {
                hdnAnswerUL.Value = dtSettingsInfo.Rows[0]["answerunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answeralign"].ToString() != "")
            {
                hdnAnswerAlign.Value = dtSettingsInfo.Rows[0]["answeralign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefontweight"].ToString() != "")
            {
                hdnVoteBold.Value = dtSettingsInfo.Rows[0]["votefontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votestyle"].ToString() != "")
            {
                hdnVoteItalic.Value = dtSettingsInfo.Rows[0]["votestyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["voteunderline"].ToString() != "")
            {
                hdnVoteUL.Value = dtSettingsInfo.Rows[0]["voteunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votealign"].ToString() != "")
            {
                hdnVoteAlign.Value = dtSettingsInfo.Rows[0]["votealign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefontweight"].ToString() != "")
            {
                hdnMsgBold.Value = dtSettingsInfo.Rows[0]["messagefontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagestyle"].ToString() != "")
            {
                hdnMsgItalic.Value = dtSettingsInfo.Rows[0]["messagestyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messageunderline"].ToString() != "")
            {
                hdnMsgUL.Value = dtSettingsInfo.Rows[0]["messageunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagealign"].ToString() != "")
            {
                hdnMsgAlign.Value = dtSettingsInfo.Rows[0]["messagealign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionfontsize"].ToString() != "")
            {
                hdnQuestionFSize.Value = dtSettingsInfo.Rows[0]["questionfontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfontsize"].ToString() != "")
            {
                hdnAnswerFSize.Value = dtSettingsInfo.Rows[0]["answerfontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefontsize"].ToString() != "")
            {
                hdnVoteFSize.Value = dtSettingsInfo.Rows[0]["votefontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefontsize"].ToString() != "")
            {
                hdnMsgFSize.Value = dtSettingsInfo.Rows[0]["messagefontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["iframewidth"].ToString() != "")
            {
                hdnWidth.Value = dtSettingsInfo.Rows[0]["iframewidth"].ToString();
            }

            if (dtSettingsInfo.Rows[0]["iframeheight"].ToString() != "")
            {
                if (hdnSrcUrl.Value.Contains("CreatePoll.aspx"))
                {
                    if (Request.Cookies["iframeheight"] != null)
                    {
                        hdnHeight.Value = Request.Cookies["iframeheight"].Value;
                    }
                    else
                    {
                        hdnHeight.Value = dtSettingsInfo.Rows[0]["iframeheight"].ToString();
                    }
                }
                else
                {
                    hdnHeight.Value = dtSettingsInfo.Rows[0]["iframeheight"].ToString();
                }
            }
            if (dtSettingsInfo.Rows[0]["recmheight"].ToString() != "")
            {
                hdnRecHeight.Value = dtSettingsInfo.Rows[0]["recmheight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["recmwidth"].ToString() != "")
            {
                hdnRecWidth.Value = dtSettingsInfo.Rows[0]["recmwidth"].ToString();
            }

            if (pollInfo.Tables[0].Rows[0][14].ToString() == "no")
            {
                isPoweredByShowing = false;
                poweredby.Style.Add("display", "none");
            }
            else
            {
                isPoweredByShowing = true;
                poweredby.Style.Add("display", "block");
                poweredby.Style.Add("color", "#" + hdnAnswerColor.Value);
            }
            
            if (dtSettingsInfo.Rows[0]["googletext"].ToString() != "" || dtSettingsInfo.Rows[0]["twittertext"].ToString() != "" || dtSettingsInfo.Rows[0]["fbtext"].ToString() != "")
            {
                hdnIsSocialSharingShowing.Value = "true";
            }
            else
            {
                hdnIsSocialSharingShowing.Value = "false";
            }
            
            btnVoteAn.Text = pollInfo.Tables[0].Rows[0]["votebuttontext"].ToString();
            btnSubmitLeadGen.InnerText = pollInfo.Tables[0].Rows[0]["submitbuttontext"].ToString();
            if (pollInfo.Tables[0].Rows[0]["invitationmessage"].ToString() != "")
            {
                invitationmessagepreview.InnerHtml = pollInfo.Tables[0].Rows[0]["invitationmessage"].ToString();
                invitationmessagepreview.Visible = true;
                invitationmessagepreview.Style.Add("display", "block");
            }
            if (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isemailrequired"].ToString()) == 1)
            {
                emaildiv.Visible = true;
                emaildiv.Style.Add("display", "block");
                contactdetailscollectiondiv.Visible = true;
                contactdetailscollectiondiv.Style.Add("display", "block");
                contactdetailscollectiondiv.Style.Add("display", "block");
            }
            if (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isphonerequired"].ToString()) == 1)
            {
                phonenumberdiv.Visible = true;
                phonenumberdiv.Style.Add("display", "block");
                contactdetailscollectiondiv.Visible = true;
                contactdetailscollectiondiv.Style.Add("display", "block");
            }
            if ((Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isemailrequired"].ToString()) == 1) || (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isphonerequired"].ToString()) == 1))
            {
                submitbuttonpreviewdiv.Visible = true;
                submitbuttonpreviewdiv.Style.Add("display", "block");
                btnSubmitLeadGen.Style.Add("display", "inline");
                btnSubmitLeadGen.Visible = true;
            }
            
            if (pollInfo.Tables[0].Rows[0]["redirecturl"].ToString() != "")
            {
                string redurl = pollInfo.Tables[0].Rows[0]["redirecturl"].ToString();
                if (!redurl.Contains("http"))
                {
                    redurl = "http://" + pollInfo.Tables[0].Rows[0]["redirecturl"].ToString();
                }
                submitbuttonpreviewdiv.Visible = true;
                submitbuttonpreviewdiv.Style.Add("display", "block");
                btnSubmitLeadGen.Style.Add("display", "inline");
                btnSubmitLeadGen.Visible = true;
                hdnHasRedirectUrl.Value = "1";
                hdnRedirectUrl.Value = redurl;
            }
            thankyouoverlaymessage.Text = pollInfo.Tables[0].Rows[0]["resulttext"].ToString();
            votebuttonwithsponsor.Style.Add("display", "none");
            votebuttonwithnosponsor.Style.Add("display", "block");
            if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "" && dtSettingsInfo.Rows[0]["sponsortext"].ToString() != "")
            {
                isSponsorLogoShowing = true;
                lblSpoRes.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                logo.Attributes.Add("src", foldername + @"\" + dtSettingsInfo.Rows[0]["sponsorlogo"].ToString());

                if (dtSettingsInfo.Rows[0]["logolink"].ToString() != "")
                {
                    string sponsorlogolink = dtSettingsInfo.Rows[0]["logolink"].ToString();
                    if (sponsorlogolink.IndexOf("http") < 0)
                    {
                        sponsorlogolink = "http://" + sponsorlogolink;
                    }
                    logolink.Attributes.Add("href", sponsorlogolink);
                }
                else
                {
                    logolink.Attributes.Add("href", "#");
                }
            }
            else
            {
                isSponsorLogoShowing = false;
            }
            hdnIsSponsorLogoShowing.Value = isSponsorLogoShowing.ToString().ToLower();
            hdnIsPoweredByShowing.Value = isPoweredByShowing.ToString().ToLower();
        }
        else
        {
            hdnAnswerFSize.Value = "12";
            hdnQuestionFSize.Value = "12";
            hdnVoteFSize.Value = "12";
            hdnMsgFSize.Value = "12";
            hdnQuestionBold.Value = "normal";
            hdnQuestionItalic.Value = "normal";
            hdnQuestionUL.Value = "none";
            hdnQuestionAlign.Value = "left";
            hdnAnswerBold.Value = "normal";
            hdnAnswerItalic.Value = "normal";
            hdnAnswerUL.Value = "none";
            hdnAnswerAlign.Value = "left"; ;
            hdnVoteBold.Value = "bold";
            hdnVoteItalic.Value = "normal";
            hdnVoteUL.Value = "none";
            hdnVoteAlign.Value = "center";
            hdnMsgAlign.Value = "left";
            hdnMsgBold.Value = "normal";
            hdnMsgItalic.Value = "normal";
            hdnMsgUL.Value = "none";
            hdnPollBgColor.Value = "ffffff";
            hdnAnswerColor.Value = "000000";
            hdnQuestBgColor.Value = "e7e7e7";
            hdnQuestColor.Value = "000000";
            hdnVoteBgColor.Value = "6C89E1";
            hdnVoteColor.Value = "ffffff";
        }
    }

    protected void WriteClicked()
    {
        HttpCookie acookie = new HttpCookie("PollUniqueRespondent");
        acookie.Values["PollId"] = pollId.ToString();
        acookie.Values["RespondentId"] = "Responded";
        Response.Cookies.Add(acookie);
    }

    protected void btnVote_Click(object sender, EventArgs e)
    {

        string jstring = "";
        string chartheightstring = "";
        string ipAddress = "";
        HttpRequest httpRequest = HttpContext.Current.Request;
        if (httpRequest.Browser.IsMobileDevice)
        {
            device = "mobile";
            mobileBrand = httpRequest.Browser.MobileDeviceManufacturer;
            mobilePlatform = httpRequest.Browser.Platform;
        }
        else
        {
            device = "desktop";
            platform = httpRequest.Browser.Platform;
        }

        if (Session["respondentId"].ToString() == "createvisit")
        {
            BindDesignStyles();

            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }

            var objClient = new System.Net.WebClient();
            var downloadedString = objClient.DownloadString("http://freegeoip.net/xml/" + ipAddress);
            XmlDocument xob = new XmlDocument();
            xob.LoadXml(downloadedString);

            try
            {

                if (xob != null)
                {
                    var x = xob.SelectSingleNode("Response");
                    if (x != null)
                    {
                        IpAddress = x["IP"].InnerText;
                        countryCode = x["CountryCode"].InnerText;
                        countryName = x["CountryName"].InnerText;
                        regionName = x["RegionName"].InnerText;
                        cityName = x["City"].InnerText;
                        zipCode = x["ZipCode"].InnerText;
                        latitude = x["Latitude"].InnerText;
                        longitude = x["Longitude"].InnerText;
                        timeZone = x["TimeZone"].InnerText;
                    }
                }
            }
            catch { }
            finally
            {
            }
            licInfo = Pc.getPollLicenceInfo(pollId);
            if (licInfo.Tables[1].Rows.Count > 0)
            {
                if (licInfo.Tables[1].Rows[0][0].ToString() == "Free" || Convert.ToDateTime(licInfo.Tables[1].Rows[0][1].ToString()) < DateTime.Now)
                {
                    if (Convert.ToInt32(licInfo.Tables[0].Rows[0][0].ToString()) > 500)
                    {
                        Response.Redirect("lastpage.aspx");
                    }
                }
                else if (licInfo.Tables[0].Rows[0][0].ToString() == "PremiumOne")
                {
                    if (Convert.ToInt32(licInfo.Tables[0].Rows[0][0].ToString()) > 5000)
                    {
                        Response.Redirect("lastpage.aspx");
                    }
                }
                else if (licInfo.Tables[0].Rows[0][0].ToString() == "PremiumTwo")
                {
                    if (Convert.ToInt32(licInfo.Tables[0].Rows[0][0].ToString()) > 10000)
                    {
                        Response.Redirect("lastpage.aspx");
                    }
                }
            }

            respondentId = Pc.InsertIntoRespondent(pollId, "visits", countryName, regionName, cityName, IpAddress, longitude, latitude, device, platform, mobileBrand, mobilePlatform, hdnSrcUrl.Value);
            hdnRespondentId.Value = Convert.ToString(respondentId);
            Session["respondentId"] = respondentId.ToString();
        }
        pollInfo = Pc.getPollInfo(pollId);
        dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
        if (pollInfo.Tables[0].Rows[0][9].ToString() == "private")
        {
            resultaccess = "private";
            lblMessage.Text = pollInfo.Tables[0].Rows[0][10].ToString();
        }
        else
        {
            resultaccess = "public";
        }
        if (Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()) == 1)
        {
            questionType = 1;
            if (radAnswerOptions.SelectedIndex == -1)
            {
                return;
            }
            else
            {
            }
        }
        else
        {
            questionType = 2;
            if (ckbAnswerOptions.SelectedIndex == -1)
            {
                return;
            }
            else
            {
            }
        }
        respondentId = Convert.ToInt32(hdnRespondentId.Value);
        Location location = getIPDetails(ipAddress);
        if (location != null)
        {
            IpAddress = (ipAddress ?? "");
            countryCode = (location.countryCode ?? "");
            countryName = (location.countryName ?? "");
            regionName = (location.regionName ?? "");
            cityName = (location.city ?? "");
            zipCode = (location.postalCode ?? "");
            latitude = (location.latitude.ToString() ?? "");
            longitude = (location.longitude.ToString() ?? "");
            timeZone = "";
        }
        else
        {
            IpAddress = "";
            countryCode = "";
            countryName = "";
            regionName = "";
            cityName = "";
            zipCode = "";
            latitude = "";
            longitude = "";
            timeZone = "";

        }

        if (!hdnSrcUrl.Value.Contains("CreatePoll.aspx"))
        {
            respondentId = Pc.InsertIntoRespondent(pollId, "complete", countryName, regionName, cityName, IpAddress, longitude, latitude, device, platform, mobileBrand, mobilePlatform, hdnSrcUrl.Value);
            Pc.UpdateIntoRespondent(respondentId, "complete", txtComments.Value);
        }

        HttpCookie respCookie = new HttpCookie("respondentanswered" + pollId.ToString());
        Response.Cookies.Remove("respondent" + pollId.ToString());
        respCookie.Value = respondentId.ToString();
        respCookie.Expires = DateTime.Now.AddDays(10);
        respCookie.Domain = "127.0.0.1";
        respCookie.Path = "/";
        respCookie.HttpOnly = false;
        Response.Cookies.Add(respCookie);

        if (questionType == 2)
        {
            for (int i = 0; i < ckbAnswerOptions.Items.Count; i++)
            {
                if (ckbAnswerOptions.Items[i].Selected == true)
                {
                    answerId = Convert.ToInt32(ckbAnswerOptions.Items[i].Value);
                    Pc.InsertIntoResponse(respondentId, answerId);
                }
            }
        }
        else
        {
            answerId = Convert.ToInt32(radAnswerOptions.SelectedItem.Value);
            Pc.InsertIntoResponse(respondentId, answerId);
        }
        singleradbtns.Style.Add("display", "none");
        multipleckbbtns.Style.Add("display", "none");
        
        if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "" && dtSettingsInfo.Rows[0]["sponsortext"].ToString() != "")
        {
            //sponsornav.Style.Add("display", "block");
            isSponsorLogoShowing = true;
            //sponsormaindiv.Style.Add("display", "block");
        }
        else
        {
            //sponsornav.Style.Add("display", "none")
            isSponsorLogoShowing = false;
            //sponsormaindiv.Style.Add("display", "none");
        }

        if (pollInfo.Tables[0].Rows[0][14].ToString() == "no")
        {
            isPoweredByShowing = false;
            //poweredbydiv.Style.Add("display", "none");
        }
        else
        {
            isPoweredByShowing = true;
            //poweredbydiv.Style.Add("display", "block");
        }

        if (resultaccess == "public")
        {
            DataSet dspollInfo = Pc.getpollchartinfo(pollId);
            commentbox.Style.Add("display", "none");
            if (dtSettingsInfo.Rows[0]["googletext"].ToString() != "" || dtSettingsInfo.Rows[0]["twittertext"].ToString() != "" || dtSettingsInfo.Rows[0]["fbtext"].ToString() != "")
            {
                isSocialSharingShowing = true;
            }
            else
            {
                isSocialSharingShowing = false;
                //social.Style.Add("display", "none");
                //socialshares.Style.Add("display", "none");
            }
            if (Convert.ToBoolean(dtSettingsInfo.Rows[0]["countvotes"].ToString()) == true)
            {
                lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString() + " (" + dspollInfo.Tables[4].Rows[0][0].ToString() + " votes)";
            }
            else
            {
                lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
            }
        }
        else
        {
            messagebox.Style.Add("display", "block");
            commentbox.Style.Add("display", "none");
            DataSet dspollInfo = Pc.getpollchartinfo(pollId);
            if (dtSettingsInfo.Rows[0]["googletext"].ToString() != "" || dtSettingsInfo.Rows[0]["twittertext"].ToString() != "" || dtSettingsInfo.Rows[0]["fbtext"].ToString() != "")
            {
                isSocialSharingShowing = true;
            }
            else
            {
                isSocialSharingShowing = false;
            }
            if (Convert.ToBoolean(dtSettingsInfo.Rows[0]["countvotes"].ToString()) == true)
            {
                lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString() + " (Votes : " + dspollInfo.Tables[4].Rows[0][0].ToString() + ")";
            }
            else
            {
                lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
                //  votescount.Style.Add("display", "none");
            }
            //voteres.Style.Add("display", "block");
            //voteres.Style.Add("height", "100px");
        }
        hdnIsSponsorLogoShowing.Value = isSponsorLogoShowing.ToString().ToLower();
        hdnIsPoweredByShowing.Value = isPoweredByShowing.ToString().ToLower();
        Session["respondentId"] = "createvisit";
    }

    public static string StripTagsRegex(string source)
    {
        return Regex.Replace(source, "<.*?>", string.Empty);
    }

    public Location getIPDetails(string IPAddress)
    {
        string filepath = ConfigurationManager.AppSettings["geolitepath"].ToString();
        //string filepath = "D://webapps//insighto//GeoLiteCity.dat";
        LookupService lookupservice = new LookupService(filepath);
        Location location = new Location();
        try
        {
            location = lookupservice.getLocation(IPAddress);
        }
        catch (Exception e1)
        {
            return null;
        }
        return location;
    }

}