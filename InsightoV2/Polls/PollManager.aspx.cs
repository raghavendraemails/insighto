﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Text;
using System.Configuration;
using Insighto.Data;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;

public partial class Poll_PollManager : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int pollId = 0;
    DataSet pollInfo = new DataSet();
    DataTable dtSettingsInfo = new DataTable();
    PollCreation Pc = new PollCreation();
    DataTable tbAnswerOption = new DataTable();
    ListItem answerOption;
    string foldername = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            if (typeHt.Contains("PollId"))
                pollId = Convert.ToInt32(typeHt["PollId"].ToString());
        }
        foldername = @"images\" + Convert.ToString(pollId);
        if (!IsPostBack)
        {
            UserControl header = (UserControl)this.Master.FindControl("ucHeaderMenu");
            UserControl upgrade = (UserControl)header.FindControl("UpgradeNowNew1");
            HtmlGenericControl divpolls = (HtmlGenericControl)upgrade.FindControl("polls");
            HtmlGenericControl divfree = (HtmlGenericControl)upgrade.FindControl("divFreeUser");
            divfree.Style.Add("display", "none");
            divpolls.Style.Add("display", "block");
            BindDesignStyles();
        }
    }
    protected void BindDesignStyles()
    {
         pollInfo = Pc.getPollInfo(pollId);
         dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
         lblStatus.Text = pollInfo.Tables[0].Rows[0][5].ToString();
         lblStatus.Visible = false;

         if (pollInfo.Tables[0].Rows[0]["polltype"].ToString() == "slider")
         {
             lblembedslider.Text = "The Script code of your slider poll is :";
         }
         else
         {
             lblembedslider.Text = "The Embed code of your poll is :";
         }

        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
        {
            imagepreview.Style.Add("display", "block");
            if (pollInfo.Tables[0].Rows[0][19].ToString() != "")
            {
                imgPreview.Attributes.Add("src", foldername + @"\" + pollInfo.Tables[0].Rows[0][19].ToString());
            }
        }
        else if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
        {
            if (pollInfo.Tables[0].Rows[0][18].ToString() != "")
            {
                videopreview.Style.Add("display", "block");
                videopreview.InnerHtml = pollInfo.Tables[0].Rows[0][18].ToString();
            }
        }
        if (pollInfo.Tables[0].Rows[0][7].ToString() != null && pollInfo.Tables[0].Rows[0][7].ToString() != "")
        {
            if (pollInfo.Tables[0].Rows[0]["polltype"].ToString() == "slider")
            {
                txtEmbedCode.Text = "<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js' type='text/javascript'></script><link href='http://insighto.com/Polls/css/SliderStyleSheet.css' rel='stylesheet' type='text/css' /><script src='" + "http://insighto.com/Polls/WebScripts/insightoslider" + pollId + ".js" + "' type='text/javascript'></script><script src='http://insighto.com/Polls/WebScripts/jquery.easing.min.js' type='text/javascript'></script>"; 

            }
            else
            {
                txtEmbedCode.Text = pollInfo.Tables[0].Rows[0][7].ToString();
            }
        }
        lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
        if (pollInfo.Tables[0].Rows[0][14].ToString() == "no")
        {
            poweredby.Style.Add("display", "none");
        }
        else
        {
            poweredby.Style.Add("display", "block");
        }
        if (Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()) == 1)
        {
            singleradbtns.Style.Add("display", "block");
            multipleckbbtns.Style.Add("display", "none");
            tbAnswerOption = Pc.getAnswerOptions(pollId);
            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                foreach (DataRow row in tbAnswerOption.Rows)
                {
                    answerOption = new ListItem(row["answeroption"].ToString(), row["pk_answerid"].ToString());
                    radAnswerOptions.Items.Add(answerOption);
                }
            }
        }
        else if (Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()) == 2)
        {
            singleradbtns.Style.Add("display", "none");
            multipleckbbtns.Style.Add("display", "block");
            tbAnswerOption = Pc.getAnswerOptions(pollId);
            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                foreach (DataRow row in tbAnswerOption.Rows)
                {
                    answerOption = new ListItem(row["answeroption"].ToString(), row["pk_answerid"].ToString());
                    ckbAnswerOptions.Items.Add(answerOption);
                }
            }
        }
        if (Convert.ToBoolean(pollInfo.Tables[0].Rows[0][17].ToString()) == true)
        {
            commentbox.Style.Add("display", "block");
        }
        else
        {
            commentbox.Style.Add("display", "none");
        }
        if (pollInfo.Tables[0].Rows[0][5].ToString() != "Active") {
            lBtnClose.Visible = false;
        }
        else { 
        }
       
        if (dtSettingsInfo != null && dtSettingsInfo.Rows.Count > 0)
        {
            if (dtSettingsInfo.Rows[0]["pollbgcolor"].ToString() != "")
            {
                hdnPollBgColor.Value = dtSettingsInfo.Rows[0]["pollbgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionfont"].ToString() != "")
            {
                //  ddlQuestionFont.SelectedItem.Value = dtSettingsInfo.Rows[0]["questionfont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questioncolor"].ToString() != "")
            {
                hdnQuestColor.Value = dtSettingsInfo.Rows[0]["questioncolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionbgcolor"].ToString() != "")
            {
                hdnQuestBgColor.Value = dtSettingsInfo.Rows[0]["questionbgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answercolor"].ToString() != "")
            {
                hdnAnswerColor.Value = dtSettingsInfo.Rows[0]["answercolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votebgcolor"].ToString() != "")
            {
                hdnVoteBgColor.Value = dtSettingsInfo.Rows[0]["votebgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votecolor"].ToString() != "")
            {
                hdnVoteColor.Value = dtSettingsInfo.Rows[0]["votecolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionfontweight"].ToString() != "")
            {
                hdnQuestionBold.Value = dtSettingsInfo.Rows[0]["questionfontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionstyle"].ToString() != "")
            {
                hdnQuestionItalic.Value = dtSettingsInfo.Rows[0]["questionstyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionunderline"].ToString() != "")
            {
                hdnQuestionUL.Value = dtSettingsInfo.Rows[0]["questionunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionfont"].ToString() != "")
            {
                hdnQuestionFont.Value = dtSettingsInfo.Rows[0]["questionfont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionalign"].ToString() != "")
            {
                hdnQuestionAlign.Value = dtSettingsInfo.Rows[0]["questionalign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfontweight"].ToString() != "")
            {
                hdnAnswerBold.Value = dtSettingsInfo.Rows[0]["answerfontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerstyle"].ToString() != "")
            {
                hdnAnswerItalic.Value = dtSettingsInfo.Rows[0]["answerstyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["iframeheight"].ToString() != "")
            {
                hdnHeight.Value = dtSettingsInfo.Rows[0]["iframeheight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["iframewidth"].ToString() != "")
            {
                hdnWidth.Value = dtSettingsInfo.Rows[0]["iframewidth"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["recmheight"].ToString() != "")
            {
                hdnRecHeight.Value = dtSettingsInfo.Rows[0]["recmheight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["recmwidth"].ToString() != "")
            {
                hdnRecWidth.Value = dtSettingsInfo.Rows[0]["recmwidth"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerunderline"].ToString() != "")
            {
                hdnAnswerUL.Value = dtSettingsInfo.Rows[0]["answerunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answeralign"].ToString() != "")
            {
                hdnAnswerAlign.Value = dtSettingsInfo.Rows[0]["answeralign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefontweight"].ToString() != "")
            {
                hdnVoteBold.Value = dtSettingsInfo.Rows[0]["votefontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votestyle"].ToString() != "")
            {
                hdnVoteItalic.Value = dtSettingsInfo.Rows[0]["votestyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["voteunderline"].ToString() != "")
            {
                hdnVoteUL.Value = dtSettingsInfo.Rows[0]["voteunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votealign"].ToString() != "")
            {
                hdnVoteAlign.Value = dtSettingsInfo.Rows[0]["votealign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefontweight"].ToString() != "")
            {
                hdnMsgBold.Value = dtSettingsInfo.Rows[0]["messagefontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagestyle"].ToString() != "")
            {
                hdnMsgItalic.Value = dtSettingsInfo.Rows[0]["messagestyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messageunderline"].ToString() != "")
            {
                hdnMsgUL.Value = dtSettingsInfo.Rows[0]["messageunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagealign"].ToString() != "")
            {
                hdnMsgAlign.Value = dtSettingsInfo.Rows[0]["messagealign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionfontsize"].ToString() != "")
            {
                hdnQuestionFSize.Value = dtSettingsInfo.Rows[0]["questionfontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfontsize"].ToString() != "")
            {
                hdnAnswerFSize.Value = dtSettingsInfo.Rows[0]["answerfontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfont"].ToString() != "")
            {
                hdnAnswerFont.Value = dtSettingsInfo.Rows[0]["answerfont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefontsize"].ToString() != "")
            {
                hdnVoteFSize.Value = dtSettingsInfo.Rows[0]["votefontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefontsize"].ToString() != "")
            {
                hdnMsgFSize.Value = dtSettingsInfo.Rows[0]["messagefontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "" && dtSettingsInfo.Rows[0]["sponsortext"].ToString() != "")
            {
                lblSponsor.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                sponsorpreviw.Style.Add("display", "block");
                logo.Attributes.Add("src", foldername + @"\" + dtSettingsInfo.Rows[0]["sponsorlogo"].ToString());
                if (dtSettingsInfo.Rows[0]["logolink"].ToString() != "")
                {
                    logolink.Attributes.Add("href", dtSettingsInfo.Rows[0]["logolink"].ToString());
                }
                else
                {
                    logolink.Attributes.Add("href", "#");
                }
            }
            else
            {
                sponsorpreviw.Style.Add("display", "none");
            }
        }
        else
        {
            hdnAnswerFSize.Value = "12";
            hdnQuestionFSize.Value = "12";
            hdnVoteFSize.Value = "12";
            hdnMsgFSize.Value = "12";
            hdnQuestionBold.Value = "normal";
            hdnQuestionItalic.Value = "normal";
            hdnQuestionUL.Value = "none";
            hdnQuestionAlign.Value = "left";
            hdnAnswerBold.Value = "normal";
            hdnAnswerItalic.Value = "normal";
            hdnAnswerUL.Value = "none";
            hdnAnswerAlign.Value = "left"; ;
            hdnVoteBold.Value = "bold";
            hdnVoteItalic.Value = "normal";
            hdnVoteUL.Value = "none";
            hdnVoteAlign.Value = "center";
            hdnMsgAlign.Value = "left";
            hdnMsgBold.Value = "normal";
            hdnMsgItalic.Value = "normal";
            hdnMsgUL.Value = "none";
            hdnPollBgColor.Value = "ffffff";
            hdnAnswerColor.Value = "000000";
            hdnQuestBgColor.Value = "e7e7e7";
            hdnQuestColor.Value = "000000";
            hdnVoteBgColor.Value = "6C89E1";
            hdnVoteColor.Value = "ffffff";
        }
    }
    protected void lBtnDelete_Click(object sender, EventArgs e)
    {
                if (pollId > 0)
                {
                    Pc.DeletePoll(pollId);
                    dvSuccessMsg.Visible = true;
                    dvErrMsg.Visible = false;
                    lblSuccessMsg.Text = "Poll successfully deleted. ";
                }
    }
    protected void lBtnClose_Click(object sender, EventArgs e)
    {
        if (lblStatus.Text == "Active")
        {
            Pc.UpdateStatus(pollId, "Closed");
            string Navurlstr = EncryptHelper.EncryptQuerystring("PollManager.aspx", "PollId=" + pollId);
            Response.Redirect(Navurlstr);
        }
        else
        {
            lblErrMsg.Text = "Only active polls can be closed.";
            dvSuccessMsg.Visible = false;
            dvErrMsg.Visible = true;
        }
    }
    protected void lBtnActivate_Click(object sender, EventArgs e)
    {
        if (lblStatus.Text == "Closed")
        {
            Pc.UpdateStatus(pollId, "Active");

            lblSuccessMsg.Text = "Poll has been activated successfully.";
            dvSuccessMsg.Visible = true;
            dvErrMsg.Visible = false;
            string Navurlstr = EncryptHelper.EncryptQuerystring("PollManager.aspx", "PollId=" + pollId);
            Response.Redirect(Navurlstr);
        }
        else
        {
            lblErrMsg.Text = "Poll will be activated only for closed Polls";
            dvSuccessMsg.Visible = false;
            dvErrMsg.Visible = true;
        }
    }
    protected void lBtnPrint_Click(object sender, EventArgs e)
    {
        if (pollId > 0)
        {
            string Navurlstr = EncryptHelper.EncryptQuerystring("PrintSurveyQuestions.aspx", "PollId=" + pollId);
            Response.Redirect(Navurlstr);
        }
    }
    protected void lBtnReports_Click(object sender, EventArgs e)
    {
        if (pollId > 0)
        {
           string Navurlstr = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + pollId );
           Response.Redirect(Navurlstr);

         }
    }
    protected void lBtnPollEdit_Click(object sender, EventArgs e)
    {
        if (pollId > 0)
        {
            string Navurlstr = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollId);
            Response.Redirect(Navurlstr);
        }
    }
    protected void lBtnCopy_Click(object sender, EventArgs e)
    {
         if (pollId > 0)
         {
            string Navurlstr = EncryptHelper.EncryptQuerystring("PollBasics.aspx", "PollId=" + pollId + "&Mode=Copy");
            Response.Redirect(Navurlstr);
         }
    }
}