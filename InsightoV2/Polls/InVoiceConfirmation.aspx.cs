﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using System.Configuration;
using App_Code;
using System.Data;

namespace Insighto.Pages.In
{
    public partial class In_InVoiceConfirmation : System.Web.UI.Page
    {
        public string strPolllicensetype;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Hashtable ht = new Hashtable();
                var userService = ServiceFactory.GetService<UsersService>();
                int orderId = 0, userId = 0, succTrans = 0;
                string invoice = "", strNavurl = "", supportEmail = "", upgradeFlag = "", accounttype = "";
                PollCreation Pc = new PollCreation();

                if (Request.QueryString["key"] != null)
                {
                    ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0)
                    {
                        if (ht.Contains("ord_id"))
                            orderId = Convert.ToInt32(ht["ord_id"]);
                        if (ht.Contains("Usrid"))
                            userId = Convert.ToInt32(ht["Usrid"]);
                        if (ht.Contains("tran_status"))
                            succTrans = Convert.ToInt32(ht["tran_status"]);
                        if (ht.Contains("polllicensetype"))
                            strPolllicensetype = Convert.ToString(ht["polllicensetype"]);
                        if (ht.Contains("invoice_id"))
                        {
                            invoice = Convert.ToString(ht["invoice_id"]);                           
                        }
                        
                    }
                }

                PollCreation pc = new PollCreation();
                DataSet dsorder = pc.getPollOrderInfo(orderId,userId);

                List<string> listConfg = new List<string>();
                listConfg.Add(Constants.SUPPORTEMAIL);
                var configService = ServiceFactory.GetService<ConfigService>();
                var configvalue = configService.GetConfigurationValues(listConfg).FirstOrDefault();
                if (configvalue != null)
                    supportEmail = configvalue.CONFIG_VALUE;
               
                if (succTrans > 0)
                {
                    if (succTrans == 1)
                    {
                        Pc.UpdatePollLicenseType(userId, strPolllicensetype, 2);
                        pc.UpdatePollPaymentType(userId, strPolllicensetype, orderId);
                        lblThankyouInsighto.Text = "Thank you for your payment <br/> for Insighto Polls " + strPolllicensetype + " plan";

                        //COMMENTED OUT by satish 26AUG16 AS LINK TO SEND USER TO MYPOLLS WAS ADDED 
                        //lblactivated.Text = "Your Insighto Polls "+ strPolllicensetype +" plan has been activated.<br/>Go ahead and create your Polls.";
                        string thankyoumessage = "Your Insighto Polls " + strPolllicensetype + " plan has been activated.<br/>";
                        thankyoumessage += "<a href='" + EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyPollPageURL(), "UserId=" + userId) + "'>Click here</a> to create your Polls.";
                        lblactivated.Text = thankyoumessage;

                        if (dsorder.Tables[0].Rows[0]["ORDER_TYPE"].ToString() == "N")
                        {
                            if (supportEmail != null && supportEmail.Length > 0)
                            {
                                strNavurl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserContactusURL().Replace("~/In/", ""), "UserId=" + userId);
                            }
                        }
                        else if ((dsorder.Tables[0].Rows[0]["ORDER_TYPE"].ToString() == "U"))
                        {
                            invoice_seperatetext.Visible = false;
                            divHeading.Visible = false;
                        }
                    }
                }
                else
                {
                    var returnValue = userService.UpgradeUserAccountHistory(userId, orderId, 1);
                    txtConfirm.InnerHtml = String.Format(Utilities.ResourceMessage("InvoiceConfirmation_ProAccount_Generated"),invoice,accounttype); //"Invoice " + invoice + " has been generated for your PRO account.";
                    if (dsorder.Tables[0].Rows[0]["ORDER_TYPE"].ToString() == "N")
                    {
                        divHeading.Visible = true;
                        invoice_seperatetext.Visible = true;
                        if (supportEmail != null && supportEmail.Length > 0)
                        {
                            strNavurl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserContactusURL().Replace("~/In/", ""), "UserId=" + userId);
                        }
                    }
                    else if (dsorder.Tables[0].Rows[0]["ORDER_TYPE"].ToString() == "U")
                    {
                        invoice_seperatetext.Visible = false;
                        divHeading.Visible = false;
                    }
                    string fromEmail = ConfigurationManager.AppSettings["emailActivation"];
                    var sendEmailService = ServiceFactory.GetService<SendEmailService>();
                    string toMail = supportEmail;
                    sendEmailService.RegIstrationEmailAlert("Registration_Email_Alert", userId, toMail, fromEmail);    
                }

                string viewNavurl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollsInvoiceUrl(), "UserId=" + userId + "&OrderId=" + orderId);
                string printNavurl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollsInvoiceUrl(), "UserId=" + userId + "&OrderId=" + orderId + "&Print=1");
                hlk_viewinvoice.Attributes.Add("Onclick", "javascript: OpenModalPopUP('" + viewNavurl + "',690, 515, 'yes');");
                hlk_printinvoice.Attributes.Add("Onclick", "javascript:  window.open('" + printNavurl + "', 'Help', 'height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');");
            }
        }

        private void EmailAlert()
        {
 
        }


    }
}