﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollSurveyReportHeader.ascx.cs" Inherits="UserControls_PollSurveyReportHeader" %>
<div class="pageTitle createpollbg">
    <table width="100%">
    <tr>
    <td width="20%">    
      <asp:Label ID="lblReport" runat="server" Text="Report:" meta:resourcekey="lblReportResource1"></asp:Label>
        <asp:Label ID="lblPollName" runat="server" meta:resourcekey="lblSurveyNameResource1"></asp:Label>
        </td>
        <td width="20%" >
             <asp:Label ID="lblstatus" runat="server" Text="Status:" Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            <asp:Label ID="lblstatusValue" runat="server"  Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            </td>
            <td width="20%" >
            <asp:Label ID="lblvisits" runat="server" Text="Views:" Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            <asp:Label ID="lblvisitsValue" runat="server"  Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>  
            </td>      
            <td width="20%" >     
              <asp:Label ID="lblvotes" runat="server" Text="Votes:" Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            <asp:Label ID="lblvotesvalue" runat="server"  Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            </td>
            <td width="20%">
            <asp:Label ID="lblModified" runat="server" Text="LastModified:" Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            <asp:Label ID="lblModifiedValue" runat="server" Font-Names="Arial, Helvetica, sans-serif" ForeColor="Blue"></asp:Label>
            </td>
            </tr>
            </table>
</div>