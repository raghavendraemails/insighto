﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeaderPolls.ascx.cs" Inherits="Polls_new_design_UserControls_HeaderPolls" %>

  <nav class="white" role="navigation" style="height:57px;line-height:57px;">
    <div class="nav-wrapper nav-bar" style="padding:0px;">
    <ul id="nav-mobile" class="left hide-on-med-and-down">
            <span class="brand-logo left" id="Span1" runat="server">
                <img src="https://test.insighto.com/Polls/img/apple_touch.png" style="background: #FFEB3B;padding: 9.5px 18px;margin-right: 3%;">
            </span>
      </ul>
        <ul class="center align-center brand-logo" id="ulcompanyname">
            <li>
                <span id="companyname" runat="server" style="font-size:28px;color:white;"></span>
            </li>
            <li>
                <a id="btnOpenCompanyNameDialog" runat="server" class="modal-trigger" data-target="editcompanyname" style="padding-left:8px;">
                    <i class="material-icons" style="font-size:12px;color:white; cursor:pointer;">border_color</i>
                </a>
            </li>
        </ul>
      <ul class="right hide-on-med-and-down">
        <li><a class="upgrade-btn" href="#" id="upgradenow" runat="server">Upgrade</a></li>
        <li><a href="#" style="padding-right: 0;color: #fff;"><asp:Label ID="username" runat="server">PK</asp:Label></a></li>
        <li>
            <a class="btn-floating btn-small dropdown-button center-align" href="#!" data-activates="dropdown1" style="margin: 0 15px;" data-beloworigin="true">
                <asp:Label ID="userinitials" runat="server">PK</asp:Label>
            </a>
          </li>
        <li><a href="/contact.aspx?mode=hide" class="fancybox fancybox.ajax contact" id="contacttop"><i class="material-icons left white-text" style="height:50px;">help</i></a></li>
      </ul>
      <!-- Dropdown Structure -->
        <ul id="dropdown1" class="dropdown-content carddropdown" style="min-width: 125px !important;">
          <li><asp:HyperLink ID="mysurveys" Text="Surveys" runat="server" meta:resourcekey="hlMyProfileResource1"></asp:HyperLink></li>
          <li><asp:HyperLink ID="myprofilelink" Text="My Profile" runat="server" meta:resourcekey="hlMyProfileResource1"></asp:HyperLink></li>
          <li class="divider"></li>
          <li><asp:LinkButton ID="btnLogOut" runat="server" Text="Logout" OnClick="btnLogOut_Click" ValidationGroup="Logout" meta:resourcekey="lbtnLogOutResource1" /></li>
        </ul>
       <ul class="side-nav" id="helpicon">
      </ul>

      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>

       <ul class="side-nav" id="mobile-demo">
        <li><a href="#"><i class="material-icons left">update</i>Upgrade</a></li>
        <li><a href="#"><i class="material-icons left">face</i>User</a></li>
      </ul>


    </div>
  </nav>
<script>
    $(document).ready(function () {
        $('.contact').fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            href: '/contact.aspx?mode=hide',
            type: 'iframe',
            width: '50%',
            height: '80%',
            maxWidth: '50%',
            maxHeight: '80%',
            overlay: false,
            helpers: {
                overlay: {
                    closeClick: true,  // if true, fancyBox will be closed when user clicks on the overlay
                    speedOut: 200,   // duration of fadeOut animation
                    showEarly: true,  // indicates if should be opened immediately or wait until the content is ready
                    css: {},    // custom CSS properties
                    locked: true   // if true, the content will be locked into overlay
                },
                title: {
                    type: 'float' // 'float', 'inside', 'outside' or 'over'
                }
            },
            scrolling: 'no',
            scrollOutside: true,
            margin: [0,0,0,0]
        });
        $("#ucHeader_btnOpenCompanyNameDialog").click(function () {
            $("#editcompanyname").css("height", "100px !important");
            $('#editcompanyname').openModal();
            $("#ucHeader_txtEditCompanyName").val($("#ucHeader_companyname").html());
        });
        $("#ulcompanyname").on("mouseover", function () {
            $("#ucHeader_btnOpenCompanyNameDialog").css("display","block");
        });
        $("#ulcompanyname").on("mouseout", function () {
            $("#ucHeader_btnOpenCompanyNameDialog").css("display", "none");
        });
        $("#btnEditCompanyName").click(function () {
            var newcompanyname = $("#ucHeader_txtEditCompanyName").val();
            if (newcompanyname.trim().length == 0) {
                $('#editcompanyname').closeModal();
                return;
            }
            $.get(
                "/Polls/AjaxService.aspx",
                { 'method': 'SaveCompanyName', 'companyname':  newcompanyname}
            )
            .done(function (data) {
                if (data == "Error") {
                    Materialize.toast("Error saving the company name. Try again later", 3000);
                }
                else {
                    $("#ucHeader_companyname").html(unescape(data));
                }
                $('#editcompanyname').closeModal();
            });
        });
    });
</script>

<div id="editcompanyname" class="modal" style="height:40% !important;max-height:40% !important;min-height:300px !important;width:30%; max-width:30%;">
    <div class="modal-content text-center col m12 s12">
            <%--<h4 class="center-align">Edit company name</h4>--%>
            <p class="col m12 center-align">
                <span class="col m12  center-align">
                    Enter new name for company
                </span>
                <asp:TextBox runat="server" ID="txtEditCompanyName" MaxLength="100" BorderColor="Black" BorderStyle="Solid" BorderWidth="1"  CssClass="col m6 nomar no-pad" Width="100%" ></asp:TextBox>
            </p>
    </div>
    <div class="modal-footer center-align col m12 s12" style="text-align:center;">
        <div class="col m6 s6 left" style="text-align:left;width:60%;">
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-large red  white-text" style="float:none;width:60% !important;">Cancel</a>
        </div>
        <div class="col m6 s6" style="text-align:left;">
            <a href="#!" class="modal-action waves-effect waves-green btn-large green white-text" style="float:none;width:40% !important;" ID="btnEditCompanyName">Save</a>
        </div>
    </div>
</div>

