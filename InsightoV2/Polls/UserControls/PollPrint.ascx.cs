﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using App_Code;

public partial class Poll_UserControls_PollPrint : System.Web.UI.UserControl
{
    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int pollId = 0;

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains("PollId"))
                pollId = int.Parse(typeHt["PollId"].ToString());
            string strTemplatePreviewLink = EncryptHelper.EncryptQuerystring("~/Polls/PollPreview.aspx", "PollId=" + pollId + "&Print=" + "Yes");
                hlnkPrint.NavigateUrl = strTemplatePreviewLink;
                hlnkPrint.Attributes.Add("onclick", "javascript: return ApplyFrameBox(this);");
                hlnkPrint.Attributes.Add("rel", "framebox");
        }
    }
}