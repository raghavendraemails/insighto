﻿using System;
using System.Data;
using Insighto.Business.Services;
using CovalenseUtilities.Services;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
using System.Collections;

public partial class Polls_new_design_UserControls_HeaderPolls : UserControlBase
{
    int userId = 0;
    PollCreation pc;
    string firstnameinitial = "";
    string lastnameinitial = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        pc = new PollCreation();
        var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
        if (userSessionDetails != null)
        {
            userId = userSessionDetails.UserId;
            DataTable dtpoll = pc.getLicencePoll(userId);
            var userDetails = ServiceFactory<UsersService>.Instance.GetUserDetails(userId);
            //var sessionStateService = ServiceFactory.GetService<SessionStateService>();

            if (!String.IsNullOrWhiteSpace(userDetails[0].COMPANY))
            {
                companyname.InnerHtml = userDetails[0].COMPANY;
                //txtEditCompanyName.Text = userDetails[0].COMPANY;
            }
            else if (!String.IsNullOrWhiteSpace(userDetails[0].USER_COMPANY))
            {
                companyname.InnerHtml = userDetails[0].USER_COMPANY;
                //txtEditCompanyName.Text = userDetails[0].USER_COMPANY;
            }

            else 
            {
                companyname.InnerHtml = "Your company name here";
                //txtEditCompanyName.Text = "Your company name here";
            }

            if (!String.IsNullOrWhiteSpace(userDetails[0].FIRST_NAME))
            {
                firstnameinitial = userDetails[0].FIRST_NAME.Substring(0, 1);
            }
            if (!String.IsNullOrWhiteSpace(userDetails[0].LAST_NAME))
            {
                lastnameinitial = userDetails[0].LAST_NAME.Substring(0, 1);
            }
            username.Text = userDetails[0].FIRST_NAME + " " + userDetails[0].LAST_NAME;
            userinitials.Text = firstnameinitial + lastnameinitial;
            //LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            myprofilelink.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetMyProfileURL(), "surveyFlag=" + SurveyFlag);
            mysurveys.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + SurveyFlag);

            upgradenow.Visible = false;
            if (dtpoll.Rows.Count > 0)
            {
                if ((dtpoll.Rows[0]["polllicencetype"].ToString().ToLower() == "free") || (dtpoll.Rows[0]["polllicencetype"].ToString().ToLower() == "publisher_freetrial"))
                {
                    upgradenow.Visible = true;
                }
            }

            string upgradepolls = "UserId=" + userId.ToString() + "&licensetype=Polls";
            upgradenow.HRef = EncryptHelper.EncryptQuerystring("~/Polls/price.aspx", upgradepolls);
        }
    }

    protected void btnLogOut_Click(object sender, EventArgs e)
    {
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        sessionStateService.EndUserSession();
        Session.Clear();

        Response.Redirect("~/Home.aspx");
    }
    
    protected void btnEditCompanyName_Click(object sender, EventArgs e)
    {
        try
        {
            var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
            if (userSessionDetails != null)
            {
                userId = userSessionDetails.UserId;
                var userDetails = ServiceFactory<UsersService>.Instance.GetUserDetails(userId);
                bool savecompanyname = ServiceFactory<UsersService>.Instance.UpdateCompanyName(userId, txtEditCompanyName.Text);
                companyname.InnerHtml = txtEditCompanyName.Text;
            }
        }
        catch (Exception)
        {
        }
    }
}