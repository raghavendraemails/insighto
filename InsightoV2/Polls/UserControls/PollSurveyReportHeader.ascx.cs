﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using DevExpress.Web.ASPxEditors;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using sharp = iTextSharp.text;
using FeatureTypes;
using Insighto.Business.ValueObjects;
using System.Text;


public partial class UserControls_PollSurveyReportHeader : System.Web.UI.UserControl
{
    int pollId = 0;
    Hashtable typeHt = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            if (typeHt.Contains("PollId"))
                pollId = Convert.ToInt32(typeHt["PollId"].ToString());
        }
        DataSet dsAnalytics = getpollSurveyAnalytics(pollId);

        lblPollName.Text = dsAnalytics.Tables[0].Rows[0][0].ToString();
        lblstatusValue.Text = dsAnalytics.Tables[0].Rows[0][1].ToString();
        lblvisitsValue.Text = dsAnalytics.Tables[1].Rows[0][0].ToString();
        lblvotesvalue.Text = dsAnalytics.Tables[2].Rows[0][0].ToString();
        lblModifiedValue.Text = dsAnalytics.Tables[0].Rows[0][2].ToString();

    }
    public DataSet getpollSurveyAnalytics(int pollid)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_PollSurveyAnalytics", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollID", SqlDbType.Int)).Value = pollid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
}