﻿$(document).ready(function () {
    var pollBg = $("#hdnPollBgColor").val();
    var pollQuestion = $("#hdnQuestColor").val();
    var pollQuestionBg = $("#hdnQuestBgColor").val();
    var pollAnswer = $("#hdnAnswerColor").val();
    var pollVote = $("#hdnVoteColor").val();
    var pollVoteBg = $("#hdnVoteBgColor").val();
    var width = $("#hdnWidth").val();
    var height = $("#hdnHeight").val();
    var heightrec = $("#hdnRecHeight").val();
    var pollQuestionFont = $("#hdnQuestionFont").val();
    var answerfont = $("#hdnAnswerFont").val();
    var votefont = $("#hdnVoteFont").val();
    var msgfont = $("#hdnMsgFont").val();
    $(".lblRequired").css("display", "none");
    $(".textpollthemeresponse").css("background-color", "#" + pollBg);
    $(".textpollthemeprint").css("background-color", "#" + pollBg);
    $(".textpollthemeresponse").css("width", width);
    $(".textpollthemeprint").css("width", width);
    $(".videopreview").css("width", width); 
    var comwidth = width - 30;
    $(".txtcomments").css("width", comwidth + 'px');
    $(".textpollthemeresponse").css("height", height);
    $(".textpollthemeprint").css("height", height);
    if (height < heightrec) {
        $(".poweredby").css("position", '');
        $(".voteres").css("position", '');
        $(".voteresview").css("position", '');
    }
    else {
        $(".poweredby").css("position", 'absolute');
        $(".poweredby").css("bottom", '10px');
        $(".poweredby").css("text-align", 'center');
        $(".poweredby").css("width", '100%');
        $(".poweredby").css("margin-top", '0px');
        $(".poweredby").css("margin-bottom", '0px');
        $(".poweredby").css("padding-bottom", '0px');
        $(".voteres").css("position", 'absolute');
        $(".voteresview").css("position", 'absolute');
        if ($('#poweredby').is(':hidden')) {
            $(".voteres").css("bottom", '10px');
        }
        else {
            $(".voteres").css("bottom", '35px');
        }
    }
    $(".questionpreviewcss").css("background-color", "#" + pollQuestionBg);
    $(".questionpreviewcss").css("font-family", pollQuestionFont);
    $(".questionpreviewcss").css("color", "#" + pollQuestion);
    $(".radiopolltheme").css("color", "#" + pollAnswer);
    $(".sponsorimagepreview").css("color", "#" + pollAnswer);
    $(".sponsorimagepreview").css("font-size", answersize + 'px');
    $(".sponsorimagepreview").css("font-family", answerfont);
    $(".viewandvoteimagepreview").css("background-color", "#" + pollVoteBg);
    $(".viewandvoteimageresponse").css("background-color", "#" + pollVoteBg);
    $(".viewandvoteimagepreview").css("font-family", votefont);
    $(".viewandvoteimageresponse").css("font-family", votefont);
    $(".viewandvoteimageresponsebtn").css("font-family", votefont);
    $(".viewandvoteimageresponsebtn").css("background-color", "#" + pollVoteBg);
    $(".viewandvoteimagepreview").css("color", "#" + pollVote);
    $(".poweredbyres").css("color", "#" + pollAnswer);
    $(".poweredbyres a").css("color", "#" + pollAnswer);
    var questweight = $("#hdnQuestionBold").val();
    var answerweight = $("#hdnAnswerBold").val();
    var voteweight = $("#hdnVoteBold").val();
    var msgweight = $("#hdnMsgBold").val();
    var queststyle = $("#hdnQuestionItalic").val();
    var answerstyle = $("#hdnAnswerItalic").val();
    var votestyle = $("#hdnVoteItalic").val();
    var msgstyle = $("#hdnMsgItalic").val();
    var questul = $("#hdnQuestionUL").val();
    var answerul = $("#hdnAnswerUL").val();
    var voteul = $("#hdnVoteUL").val();
    var msgul = $("#hdnMsgUL").val();
    var questalign = $("#hdnQuestionAlign").val();
    var answeralign = $("#hdnAnswerAlign").val();
    var votealign = $("#hdnVoteAlign").val();
    var msgalign = $("#hdnMsgAlign").val();
    var questsize = $("#hdnQuestionFSize").val();
    var answersize = $("#hdnAnswerFSize").val();
    var votesize = $("#hdnVoteFSize").val();
    var msgsize = $("#hdnMsgFSize").val();
    var msgbgcolor = $("#hdnMsgBgColor").val();
    var msgcolor = $("#hdnMsgColor").val();
    var pollbgcolor = $("#hdnPollBgColor").val();
    $('.messageprivate').css({ 'font-weight': msgweight });
    $('.messageprivate').css({ 'font-family': msgfont });
    $('.messageprivate').css({ 'font-style': msgstyle });
    $('.messageprivate').css({ 'font-size': msgsize + 'px' });
    $('.messageprivate').css({ 'background-color': "#" + msgbgcolor });
    $('.messageprivate').css({ 'color': "#" + msgcolor });
    $('.questionpreviewcss').css({ 'font-weight': questweight });
    $('.radiopolltheme').css({ 'font-weight': answerweight });
    $('.viewandvoteimagepreview').css({ 'font-weight': voteweight });
    $('.questionpreviewcss').css({ 'font-style': queststyle });
    $('.radiopolltheme').css({ 'font-style': answerstyle });
    $('.radiopolltheme').css({ 'font-family': answerfont });
    $('.viewandvoteimagepreview').css({ 'font-style': votestyle });
    $('.questionpreviewcss').css({ 'text-decoration': questul });
    $('.radiopolltheme').css({ 'text-decoration': answerul });
    $('.viewandvoteimagepreview').css({ 'text-decoration': voteul });
    $('.questionpreviewcss').css({ 'text-align': questalign });
    $('.radiopolltheme').css({ 'text-align': answeralign });
    $('.sponsorimage').css({ 'font-family': answerfont });
    $('.sponsorimage').css({ 'font-size': answersize + 'px' });
    $('.sponsorimage').css({ 'color': "#" + pollAnswer });
    $(".commentstxt").css("color", "#" + pollAnswer); 
    $('.commentstxt').css({ 'font-size': answersize + 'px' });
    $('.poweredby').css({ 'color': "#" + pollAnswer });
    $('.poweredby').css({ 'font-size': answersize + 'px' });
    $('.poweredby a').css({ 'color': "#" + pollAnswer });
    $('.poweredby').css({ 'font-family': answerfont });
    $('.viewandvoteimagepreview').css({ 'text-align': votealign });
    $('.questionpreviewcss').css({ 'font-size': questsize + 'px' });
    $('.radiopolltheme').css({ 'font-size': answersize + 'px' });
    $('.commentstxt').css({ 'font-family': answerfont });
    $('.viewandvoteimagepreview').css({ 'font-size': votesize + 'px' });
    if ($('#sponsorpreviw').is(':hidden')) {
        $('.viewandvoteimageresponse').css({ 'width': '65%' });
        $('.viewandvoteimageresponse').css({ 'margin-top': '10px' });
        $('.viewandvoteimageresponse').css({ 'margin-left': '50px' });
        $('.view').css({ 'font-size': '13px' });
        $('.viewandvoteimageresponsebtn').css({ 'font-size': '13px' });
        $('.view').css({ 'text-align': 'left' });
        $('.vote').css({ 'text-align': 'center' });
        $('.vote').css({ 'float': 'none' });
        $('.vote').css({ 'width': '100%' });
    }
    else {
        $('.viewandvoteimageresponse').css({ 'width': '45%' });
        $('.viewandvoteimageresponse').css({ 'float': 'left' });
        $('.viewandvoteimageresponse').css({ 'margin-left': '10px' });
        $('.viewandvoteimageresponse').css({ 'margin-top': '6px' });
        $('.view').css({ 'font-size': '10px' });
        $('.view').css({ 'text-align': 'center' });
        $('.vote').css({ 'text-align': 'center' });
        $('.vote').css({ 'width': '100%' });
        $(".sponsorimage").css("margin-top", '0px');
    }
    if ($('#messagebox').is(':hidden')) {

    }
    else {
        $(".poweredbyres").css("position", 'absolute');
        $(".poweredbyres").css("bottom", '10px');
        $(".poweredbyres").css("text-align", 'center');
        $(".poweredbyres").css("width", '100%');
        $(".poweredbyres").css("margin-top", '');
        $(".poweredbyres").css("margin-bottom", '');
        $(".poweredbyres").css("padding-bottom", '');
        $(".voteres").css("position", 'absolute');
        $(".voteresview").css("position", 'absolute');
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '10px');
        }
        else {
            $(".viewtest").css("bottom", '35px');
        }
        $(".sponsorimage").css("margin-left", '0px');
    }
    var alert = $("#lblError").text();
    if (alert == "") {

    }
    else {
        jAlert('Please select answer option', 'Activation Alert', function (r) {
            return false;
        });
    }
});


