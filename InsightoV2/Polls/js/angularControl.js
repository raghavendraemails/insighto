﻿var pollsList = angular.module('pollsList', ['ngSanitize', 'ngAnimate']);
var allpolls;

pollsList.controller('pollsListController', function ($scope, $http, $filter) {
    $scope.createNewPoll = function () {
        $("#loader").toggle();
        var polltype = $("#polltype").val();
        var pollname = $("#newpollname").val();
        if (pollname == "") {
            $(".addpool").hide();
            $("#addcmt").show();
            $("#loader").toggle();
            return;
        }
        $.ajax({
            type: "GET",
            url: "/Polls/AjaxService.aspx?method=SavePollNameAndType",
            data: { Name: pollname, Type: polltype }
        })
            .done(function (data) {
                if (data == "Duplicate") {
                    $("#newpollmessage").html('A poll with this name already exists.');
                    $("#newpollname").focus();
                    $("#loader").toggle();
                    return;
                }
                else if (data == "Error") {
                    $("#newpollmessage").html('Error creating the poll. Please try after a while.');
                    $("#newpollname").focus();
                    $("#loader").toggle();
                    return;
                }
                else{
                    var poll = jQuery.parseJSON(data);

                    if ($scope.polls != null) {
                        $scope.polls.push(poll[0]);
                        $scope.$apply();
                        $("#polltypebuttons").hide();
                        $(".addpool").hide();
                        $("#newpollmessage").html("");
                        updateCardsCount($scope, $filter);
                        $("#loader").toggle();
                    }
                    else {
                        window.location.reload(true);
                    }
                }
            }
        );
    };

    //$http.get('data.html?method=GetAllPolls&_search=false&nd=1444334231188&rows=1000&page=1&sidx=modified_on&sord=desc')
    $http.get('/Polls/AjaxService.aspx?method=GetAllPolls&_search=false&nd=1444334231188&rows=1000&page=1&sidx=modified_on&sord=desc')
        .success(function (data) {
            if (($("#ContentPlaceHolder1_activationflag").val() == "0") && ($("#ContentPlaceHolder1_activationalert").val() == "1")) {
                //$("#loader").toggle();
                $("#modalfornewuser").openModal();
            }
            if (data.records == 0) {
                //$("#loader").toggle();
                showFirstPollCreateBubble();
            }
            else if (data.total > 0) {
                $scope.polls = data.rows;

                allpolls = $scope.polls;
                var draftcount = $filter('filter')($scope.polls, { Status: 'Draft' }, true).length;
                var activecount = $filter('filter')($scope.polls, { Status: 'Active' }, true).length;
                var closedcount = $filter('filter')($scope.polls, { Status: 'Closed' }, true).length;
                $("#draftcount").html(draftcount.toString());
                $("#activecount").html(activecount.toString());
                $("#closedcount").html(closedcount.toString());
                ///$("#loader").toggle();
                if (data.records == 1) {
                    var q = data.rows[0].question;
                    if (q == null) {
                        q = "";
                    }
                    showFirstPollEditBubble(q);
                }
            }
            $("#loader").toggle();

        });
});

pollsList.config(function ($sceDelegateProvider, $compileProvider) {
    $compileProvider.debugInfoEnabled(false);
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'iframe',
        'https://www.youtube.com'
    ]);
});

pollsList.directive('youtube', function ($sce) {
    return {
        restrict: 'A',
        //scope: { youtube: '=' },
        replace: true,
        controller: function ($scope) {
            $scope.videoFrame = $sce.trustAsHtml($scope.poll.VideoLink);
        }
    };
});

pollsList.directive('cardcount', function () {
    return {
        restrict: 'A',
        replace: true,
        controller: function ($scope) {
            var draftcount = $filter('filter')($scope.polls, { Status: 'Draft' }, true).length;
            var activecount = $filter('filter')($scope.polls, { Status: 'Active' }, true).length;
            var closedcount = $filter('filter')($scope.polls, { Status: 'Closed' }, true).length;
            $("#draftcount").html(draftcount.toString());
            $("#activecount").html(activecount.toString());
            $("#closedcount").html(closedcount.toString());
            $("#loader").toggle();

        }
    };
});

pollsList.directive('fancybox', function ($sce, $filter) {

    $('#newpollvideolink').val('');
    $('#newpollimagelink').val('');
    $('#newpollstatus').val('');
    $('#newpollname').val('');
    $('#newpollquestion').val('');

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            ////console.log(attrs["fancybox"]);
            $('#newpollvideolink').val('');
            $('#newpollimagelink').val('');
            $('#newpollstatus').val('');
            $('#newpollname').val('');
            $('#newpollquestion').val('');
            var pollstatus = scope.poll.Status;
            var polllaunchurl = scope.poll.LaunchUrl;
            var pollediturl = scope.poll.PollEditUrl;
            var pollreporturl = scope.poll.ReportUrl;
            var pollvideolink = scope.poll.VideoLink;
            var pollimagelink = scope.poll.ImageLink;
            var pollname = scope.poll.name;
            var pollquestion = scope.poll.question;

            var url = '';
            var changemade = false;
            $("#pollid").val(scope.poll.pk_pollid.toString());
            if (attrs["fancybox"] == 'edit') {
                url = pollediturl;
            }
            else if (attrs["fancybox"] == 'launch') {
                url = polllaunchurl;
            }
            else if (attrs["fancybox"] == 'report') {
                url = pollreporturl;
            }
            else {
                url = pollediturl;
            }
            ////console.log($sce.trustAsHtml('<b>ok</b>'));
            $.fancybox.hideLoading();
            $(element).fancybox({
                href: url,
                type: 'iframe',
                scrolling: 'no',
                iframe: {
                    scrolling: 'no',
                    preload: false
                },
                fitToView: true,
                margin:0,
                topRatio: -0.5,
                leftRatio:-0.5,
                width: '100%',
                height: '100%',
                autoSize: false,
                closeBtn: false,
                closeClick: false,
                openEffect: 'elastic',
                closeEffect: 'elastic',
                //openMethod: 'horizontalflipIn',
                //closeMethod: 'horizontalflipOut',
                openSpeed: 'slow',
                closeSpeed: 500,
                openEasing:'swing',
                transitionIn: 'none',
                padding:0,
                helpers: {
                    title: {
                        type: 'float'
                    }
                ,
                    overlay: null
                    //overlay: {
                    //    showEarly: false,
                    //    locked: true
                    //}
                }
                ,
                beforeLoad: function () {
                    $("#preloader2").removeClass("hide");
                    $("#preloader2").addClass("active");
                    $("#preloader2").css("position", "absolute");
                    $("#preloader2").css("top", "50%");
                    $("#preloader2").css("left", "50%");
                    //window.parent.document.getElementById("preloader2").style.display = "block";
                    $("#preloader2").show();
                },
                afterLoad: function (current, previous) {
                    $("#preloader2").removeClass("active");
                    $("#preloader2").addClass("hide");
                    $("#preloader2").hide();
                },
                onUpdate: function () {
                },
                beforeShow: function () {
                    $("#preloader2").removeClass("hide");
                    $("#preloader2").addClass("active");
                    $("#preloader2").css("position", "absolute");
                    $("#preloader2").css("top", "50%");
                    $("#preloader2").css("left", "50%");
                    $("#preloader2").show();
                },
                afterShow: function () {
                },
                afterClose: function () {
                    var newpollimagelink = $('#newpollimagelink').val();
                    var newpollvideolink = $('#newpollvideolink').val();
                    var newpollstatus = $('#newpollstatus').val();
                    var newpollname = $('#newpollname').val();
                    var newpollquestion = $('#newpollquestion').val();

                    //if (scope.poll.pk_pollid.toString() == $("#pollid").val()) {
                        if ((newpollquestion != pollquestion) && (newpollquestion != '')) {
                            scope.poll.question = newpollquestion;
                            changemade = true;
                        }
                        if ((newpollname != pollname) && (newpollname != '')) {
                            scope.poll.name = newpollname;
                            changemade = true;
                        }
                        if ((newpollimagelink != pollimagelink) && (newpollimagelink != '')) {
                            scope.poll.ImageLink = newpollimagelink;
                            scope.poll.IsImagePoll = "true";
                            scope.poll.IsVideoPoll = "false";
                            scope.poll.type = "Image";
                            changemade = true;
                        }
                        if ((newpollvideolink != pollvideolink) && (newpollvideolink != '')) {
                            scope.poll.VideoLink = newpollvideolink;
                            scope.poll.IsVideoPoll = "true";
                            scope.poll.IsImagePoll = "false";
                            scope.poll.type = "Video";
                            //scope.VideoFrame = $sce.trustAsHtml(newpollvideolink);

                            changemade = true;
                            pollsList.directive('youtube', function ($sce,$scope) {
                                return {
                                    restrict: 'A',
                                    //scope: { youtube: '=' },
                                    replace: true,
                                    controller: function (scope) {
                                        $scope.videoFrame = $sce.trustAsHtml($scope.poll.VideoLink);
                                    }
                                };
                            });
                        }
                        if ((newpollstatus != pollstatus) && (newpollstatus == 'Active')) {
                            updateCardCount(1, "active");
                            updateCardCount(-1, "draft");
                            scope.poll.Status = newpollstatus;
                            console.log(scope.poll);
                            console.log(scope.polls);
                            changemade = true;

                            if ($("#parentpollid").val() != "" && $("#parentpollid").val() != scope.poll.pk_pollid) {
                                //window.location.reload(true);
                            }
                        }
                        $('#parentpollid').val('');
                        $('#newpollvideolink').val('');
                        $('#newpollimagelink').val('');
                        $('#newpollstatus').val('');
                        $('#newpollname').val('');
                        $('#newpollquestion').val('');
                        $('#pollid').val('');
                        if (changemade == true) {
                            scope.$apply(function () {
                                if ((newpollvideolink != pollvideolink) && (newpollvideolink != ''))
                                {
                                    window.location.reload(true);
                                }
                            });
                        }
                    //}
                    $('#newpollvideolink').val('');
                    $('#newpollimagelink').val('');
                    $('#newpollstatus').val('');
                    $('#newpollname').val('');
                    $('#newpollquestion').val('');
                    $('#pollid').val('');
                }
            });
        }
    }
});
    
pollsList.directive('menudropdown', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            $(element).bind('hover', function () {
                $(element).dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrain_width: false, // Does not change width of dropdown to that of the activator
                    hover: false, // Activate on hover
                    gutter: 0, // Spacing from edge
                    belowOrigin: true, // Displays dropdown below the button
                    alignment: 'right' // Displays dropdown with edge aligned to the left of button
                });
            });
            $(element).bind('mouseover', function () {
                //alert('1');
                $(element).dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrain_width: true, // Does not change width of dropdown to that of the activator
                    hover: false, // Activate on hover
                    gutter: 0, // Spacing from edge
                    belowOrigin: true, // Displays dropdown below the button
                    alignment: 'right' // Displays dropdown with edge aligned to the left of button
                });
            });
        }
    }
});

pollsList.directive('showaddpollbuttons', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).bind('click', function () {
                $(element).collapsible(
                    {
                        accordion: true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
                    }
                );
            });
        }
    }
});

var chartsnewcount = 0;

pollsList.directive('resultschart', function ($sce) {
    return {
        restrict: 'A',
        //scope: { resultschart: '=resultschart' },
        controller: function ($scope) {
            var chartanswerrowcountnew = 0;
            var arrColours = ['#AFD8F8', '#F7D671', '#8BBA00', '#A66EDD', '#F984A1', '#AFD8F8', '#F6BD0F', '#8BBA00', '#A66EDD', '#F984A1'];
            var poll = $scope.poll;
            var pollanswers = $scope.poll.pollansweroptions;
            var totalresponses = parseInt($scope.poll.TOTAL_RESPONSES);

            var resultschartwidth = $(".resultsclass").width();
            var purejschart = "<div class='black-text col m12 s12' style='font-size:12;border-style:none; width:100%;margin-left:0px;margin-right:0px;padding-bottom:10px;position:relative;background-color:#F7F6F6;overflow:hidden !important;'>";
            var adjustedchartwidth = "75%";
            if (totalresponses > 0) {
                chartanswerrowcountnew = pollanswers.length;
                for (var x = 0; x < pollanswers.length; x++) {
                    var thisanswer = pollanswers[x].answeroption;
                    var thisanswerid = pollanswers[x].answerid;
                    var thisanswercount = parseInt(pollanswers[x].answercount);
                    var thisrowpercentage = Math.round(thisanswercount * 100 / totalresponses);

                    thisrowwidth = Math.round(thisrowpercentage * 0.70 * resultschartwidth / 100);
                    purejschart += "<div style='width:100%;text-align:left;font-size:9pt;float:left;padding-left:0px;padding-top:5px;border-style:none;'>" + thisanswer + "</div>";
                    purejschart += "<div style='width:" + adjustedchartwidth + ";float:left;background-color:lightgray;border-style:none;'>";
                    if (thisrowpercentage >= 100) {
                        purejschart += "<div style='height:15px;width:100%;max-width:100%;min-width:100%;background-color:" + arrColours[x] + ";float:left;margin-left:0px;' id='div" + thisanswerid + ";'></div>";
                    }
                    else {
                        purejschart += "<div style='height:15px;width:" + thisrowpercentage + "%;background-color:" + arrColours[x] + ";margin-left:0px;float:left;' id='div" + thisanswerid + "'></div>";
                    }
                    purejschart += "</div>";
                    purejschart += "<div style='float:right;width:15%;vertical-align:top;font-size:9pt;'>" + thisrowpercentage + "%</div>";
                }
                purejschart += "</div>";
                $scope.chartsdata = $sce.trustAsHtml(purejschart);
                for (var x = 0; x < chartanswerrowcountnew; x++) {
                    var thisanswerid = pollanswers[x].answerid;
                    var thisanswercount = parseInt(pollanswers[x].answercount);
                    var thisrowpercentage = Math.round(thisanswercount * 100 / totalresponses);

                    if (thisrowpercentage > 100) {
                        thisrowpercentage = 100;
                    }
                    thisrowwidth = Math.round(thisrowpercentage * 150 / 100);
                }
            }
        }
    }
});

pollsList.directive('imagedata', function () {
        return {
            restrict: 'A',
            controller: function ($scope) {
                var imagelink = $scope.poll.ImageLink;
                var polltype = $scope.poll.type;
                var pollid = $scope.poll.pk_pollid;

                if ((polltype == "Image") && (imagelink == "")) {
                    $scope.image = "/polls/images/ImagePoll.jpg";
                }
                else if ((polltype == "Image") && (imagelink != "")) {

                    imagelink = "/polls/images/" + pollid + "/" + imagelink;
                    try {
                        $.ajax({
                            url: imagelink,
                            type: "HEAD",
                            error: function () {
                                imagelink = "/polls/images/ImagePoll.jpg";
                            }
                        });

                    }
                    catch (e) {
                        imagelink = "/polls/images/ImagePoll.jpg";
                    }
                    $scope.image = imagelink;
                }
            }
        }
    });

pollsList.directive('polldeletion', function () {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {
                    var pollid = scope.poll.pk_pollid;
                    $('#pollactiontype').val('delete');
                    $('#pollid').val(pollid);
                    $("#p" + pollid).hide();
                    $('#modalheader-' + pollid).html('Delete poll');
                    $('#modalbody-' + pollid).html('Are you sure you want to delete this poll?');
                    $('.modal-trigger').leanModal();
                    $('#modal-' + pollid).openModal();
                });
            }
        }
    });

pollsList.directive('pollcopying', function () {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {
                    var pollid = scope.poll.pk_pollid;
                    $('#pollactiontype').val('copy');
                    $('#pollid').val(pollid);
                    $("#p" + pollid).hide();
                    $('#modalheader-' + pollid).html('Copy poll');
                    $('#modalbody-' + pollid).html('Are you sure you want to copy this poll? A new poll will be created in drafts.');
                    $('.modal-trigger').leanModal();
                    $('#modal-' + pollid).openModal();
                });
            }
        }
    });

pollsList.directive('pollclosing', function () {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {
                    var pollid = scope.poll.pk_pollid;
                    $('#pollactiontype').val('close');
                    $('#pollid').val(pollid);
                    $("#p" + pollid).hide();
                    $('#modalheader-' + pollid).html('Close poll');
                    $('#modalbody-' + pollid).html('Are you sure you want to close this poll?');
                    $('.modal-trigger').leanModal();
                    $('#modal-' + pollid).openModal();

                });
            }
        }
    });

pollsList.directive('pollreplacing', function () {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {

                    var pollid = scope.poll.pk_pollid;
                    $('#pollactiontype').val('replace');
                    $('#pollid').val(pollid);
                    $('#modalheader-' + pollid).html('Replace poll');
                    $('#p' + pollid).show();
                    $('#modalbody-' + pollid).html('Are you sure you want to replace this poll? When it is launched the parent poll will be closed.');
                    $('.modal-trigger').leanModal();
                    $('#modal-' + pollid).openModal();

                });
            }
        }
    });

pollsList.directive('pollaction', function ($filter) {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {
                    var pollid = $('#pollid').val();
                    var pollactiontype = $("#pollactiontype").val();
                    if (pollactiontype == "delete") {
                        $("#loader").toggle();
                        $.post('/Polls/AjaxService.aspx', { 'method': 'DeletePollByPollId', 'Id': pollid }).success(function (data) {
                            if (data == '0') {
                                updateCardCount(-1, scope.poll.Status.toLowerCase());
                                scope.poll.Status = 'Deleted';
                                scope.$apply();
                            }
                            else if (data == "loggedout") {
                                $('#modallogout').openModal();
                            }
                            else {
                                $('#modalerror').openModal();
                            }
                            $("#loader").toggle();
                        });
                    }
                    else if (pollactiontype == "close") {
                        $("#loader").show();
                        $.post('/Polls/AjaxService.aspx', { 'method': 'ClosePollByPollId', 'Id': pollid }).success(function (data) {
                            if (data == '0') {
                                updateCardCount(-1, scope.poll.Status.toString().toLowerCase());
                                updateCardCount(1, "closed");
                                scope.poll.Status = 'Closed';
                                scope.$apply();
                            }
                            else if (data == "loggedout") {
                                $('#modallogout').openModal();
                            }
                            else {
                                $('#modalerror').openModal();
                            }
                            $("#loader").hide();
                        });
                    }
                    else if (pollactiontype == "copy") {
                        $("#loader").show();
                        $.post('/Polls/AjaxService.aspx', { 'method': 'CopyPollByPollId', 'Id': pollid }).success(function (data) {
                            if (data == 'Error') {
                                $('#modalerror').openModal();
                            }
                            else if (data == "loggedout") {
                                $('#modallogout').openModal();
                            }
                            else {
                                updateCardCount(1, "draft");

                                var poll = jQuery.parseJSON(data);
                                scope.polls.push(poll[0]);
                                scope.$apply();
                            }
                            $("#loader").hide();
                        });
                    }
                    else if (pollactiontype == "replace") {
                        $("#loader").show();
                        $("#p" + pollid).show();
                        var replaceURL = scope.poll.ReplaceUrl;
                        var replacepollname = $("#replacepollname" + pollid).val();
                        var checkPollNameURL = "/Polls/AjaxService.aspx";
                        var updatePollNameURL = "/Polls/AjaxService.aspx";
                        if (replacepollname == "") {
                            $('#modalerrorforreplacepollname').openModal();
                            $('#errormessagereplacepollname').html('Please provide a name for the poll to replace. This name should not have been given to any of your other polls.');
                        }
                        else {
                            $.get(checkPollNameURL, { 'method': 'CheckPollNameExists', 'pollname': replacepollname })
                            .error(function () {
                                $('#modalerrorforreplacepollname').openModal();
                                $('#errormessagereplacepollname').html('Please provide a name for the poll to replace. This name should not have been given to any of your other polls.');
                            })
                            .success(function (data) {
                                if (data == "Success") {
                                    $.get(replaceURL, {}).success(function (data) {
                                        $.get(updatePollNameURL, { 'method': 'ReplacePollByPollId', 'pollname': replacepollname, 'pollid': pollid })
                                        .done(function (data) {
                                            updateCardCount(1, "draft");

                                            var poll = jQuery.parseJSON(data);
                                            scope.polls.push(poll[0]);
                                            scope.$apply();
                                            $("#loader").hide();
                                        });
                                    })
                                    .error(function (data) {
                                        if (data == "loggedout") {
                                            $('#modallogout').openModal();
                                        }
                                        else {
                                            $('#modalerror').openModal();
                                        }
                                        $("#loader").hide();
                                    });
                                }
                                else if (data == "loggedout") {
                                    $('#modallogout').openModal();
                                }
                                else {
                                    $('#modalerrorforreplacepollname').openModal();
                                    $('#errormessagereplacepollname').html('The poll name already exists. Please choose a different name.');
                                }
                                //$("#loader").hide();
                            });
                        }
                    }
                });
            }
        }
    });

pollsList.directive('pollactioncancel', function () {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {
                    alert($scope.poll.pk_pollid);
                });
            }
        }
    });

function updateCardCount(diff, cardtype) {
    var cardcount = parseInt($("#" + cardtype + "count").html());
    if (diff == 1) {
        cardcount++;
    }
    else {
        cardcount--;
    }
    //console.log(cardcount);
    $("#" + cardtype + "count").html(cardcount.toString());
}

function updateCardsCount($scope, $filter) {
    allpolls = $scope.polls;
    var draftcount = $filter('filter')($scope.polls, { Status: 'Draft' }, true).length;
    var activecount = $filter('filter')($scope.polls, { Status: 'Active' }, true).length;
    var closedcount = $filter('filter')($scope.polls, { Status: 'Closed' }, true).length;
    $("#draftcount").html(draftcount.toString());
    $("#activecount").html(activecount.toString());
    $("#closedcount").html(closedcount.toString());
}