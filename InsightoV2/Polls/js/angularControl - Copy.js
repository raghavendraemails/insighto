﻿var pollsList = angular.module('pollsList', ['ngSanitize', 'ngAnimate']);

    pollsList.controller('pollsListController', function ($scope, $http,$filter) {
        //$http.get('data.html?method=GetAllPolls&_search=false&nd=1444334231188&rows=1000&page=1&sidx=modified_on&sord=desc')
        $http.get('/Polls/AjaxService.aspx?method=GetAllPolls&_search=false&nd=1444334231188&rows=1000&page=1&sidx=modified_on&sord=desc')
            .success(function (data) {
                if (data.total > 0) {
                    $scope.polls = data.rows;
                    var draftcount = $filter('filter')($scope.polls, { Status: 'Draft' }, true).length;
                    var activecount = $filter('filter')($scope.polls, { Status: 'Active' }, true).length;
                    var closedcount = $filter('filter')($scope.polls, { Status: 'Closed' }, true).length;
                    $("#draftcount").html(draftcount.toString());
                    $("#activecount").html(activecount.toString());
                    $("#closedcount").html(closedcount.toString());
                    $("#loader").toggle();
                }
                else {
                    $("#loader").toggle();
                    $("#modalfornewuser").openModal();
                }
            });
    });


    pollsList.config(function ($sceDelegateProvider, $compileProvider) {
        $compileProvider.debugInfoEnabled(false);
        $sceDelegateProvider.resourceUrlWhitelist([
          // Allow same origin resource loads.
          'self',
          // Allow loading from our assets domain.  Notice the difference between * and **.
          'iframe',
          'https://www.youtube.com'
        ]);
    });

    pollsList.directive('youtube', function ($sce) {
        return {
            restrict: 'A',
            //scope: { youtube: '=' },
            replace: true,
            controller: function ($scope) {
                $scope.videoFrame = $sce.trustAsHtml($scope.poll.VideoLink);
            }
        };
    });

    pollsList.directive('cardcount', function () {
        return {
            restrict: 'A',
            replace: true,
            controller: function ($scope) {
                var draftcount = $filter('filter')($scope.polls, { Status: 'Draft' }, true).length;
                var activecount = $filter('filter')($scope.polls, { Status: 'Active' }, true).length;
                var closedcount = $filter('filter')($scope.polls, { Status: 'Closed' }, true).length;
                $("#draftcount").html(draftcount.toString());
                $("#activecount").html(activecount.toString());
                $("#closedcount").html(closedcount.toString());
                $("#loader").toggle();

            }
        };
    });

    pollsList.directive('fancybox', function ($sce) {

        $('#newpollvideolink').val('');
        $('#newpollimagelink').val('');
        $('#newpollstatus').val('');
        $('#newpollname').val('');
        $('#newpollquestion').val('');

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                $('#newpollvideolink').val('');
                $('#newpollimagelink').val('');
                $('#newpollstatus').val('');
                $('#newpollname').val('');
                $('#newpollquestion').val('');
                var pollstatus = scope.poll.Status;
                var polllaunchurl = scope.poll.LaunchUrl;
                var pollediturl = scope.poll.PollEditUrl;
                var pollreporturl = scope.poll.ReportUrl;
                var pollvideolink = scope.poll.VideoLink;
                var pollimagelink = scope.poll.ImageLink;
                var pollname = scope.poll.name;
                var pollquestion = scope.poll.question;

                var url = '';
                var changemade = false;
                $("#pollid").val(scope.poll.pk_pollid.toString());
                if (pollstatus == 'Draft') {
                    url = pollediturl;
                }
                else {
                    url = pollreporturl;
                }
                //console.log($sce.trustAsHtml('<b>ok</b>'));
                $.fancybox.hideLoading();
                $(element).fancybox({
                    href: url,
                    type: 'iframe',
                    scrolling: 'no',
                    iframe: {
                        scrolling: 'auto',
                        preload: false
                    },
                    wrapCSS: {
                        background: 'green'
                    },
                    maxWidth: '100%',
                    //content:'Loading...',
                    maxHeight: '100%',
                    fitToView: true,
                    width: '100%',
                    height: '100%',
                    autoSize: false,
                    closeClick: true,
                    openEffect: 'elastic',
                    closeEffect: 'elastic',
                    openSpeed: 200,
                    closeSpeed: 500,
                    openEasing:'swing',
                    transitionIn: 'none',
                    padding:0,
                    helpers: {
                        title: {
                            type: 'float'
                        }
                    ,
                        overlay: {
                            showEarly: true,
                            locked: true
                        }
                    }
                    ,
                    beforeLoad: function () {
                        $(this).content = 'Loading...';
                        //$("#iframeloader").show();
                        console.log("loading");
                        //performance.now();
                    },
                    afterLoad: function (current, previous) {
                        console.log("loading complete");
                        //current.content = null;
                        //current.href = 'http://www.yahoo.com/';
                        //$(this).href = 'http://www.yahoo.com/';
                        //$("#iframeloader").hide();

                    },
                    afterClose: function () {
                        var newpollimagelink = $('#newpollimagelink').val();
                        var newpollvideolink = $('#newpollvideolink').val();
                        var newpollstatus = $('#newpollstatus').val();
                        var newpollname = $('#newpollname').val();
                        var newpollquestion = $('#newpollquestion').val();
                        //console.log(newpollimagelink + ' , ' + pollimagelink);
                        //console.log(scope.poll.pk_pollid.toString() + ' , ' + $("#pollid").val());
                        //console.log(newpollstatus + ' , ' + pollstatus);
                        //console.log(newpollimagelink + ' , ' + pollimagelink);
                        //console.log(newpollvideolink + ' , ' + pollvideolink);
                        //if (scope.poll.pk_pollid.toString() == $("#pollid").val()) {
                            if ((newpollquestion != pollquestion) && (newpollquestion != '')) {
                                scope.poll.question = newpollquestion;
                                changemade = true;
                            }
                            if ((newpollname != pollname) && (newpollname != '')) {
                                scope.poll.name = newpollname;
                                changemade = true;
                            }
                            if ((newpollimagelink != pollimagelink) && (newpollimagelink != '')) {
                                scope.poll.ImageLink = newpollimagelink;
                                scope.poll.IsImagePoll = "true";
                                scope.poll.IsVideoPoll = "false";
                                scope.poll.type = "Image";
                                changemade = true;
                            }
                            if ((newpollvideolink != pollvideolink) && (newpollvideolink != '')) {
                                scope.poll.VideoLink = newpollvideolink;
                                scope.poll.IsVideoPoll = "true";
                                scope.poll.IsImagePoll = "false";
                                scope.poll.type = "Video";
                                //scope.VideoFrame = $sce.trustAsHtml(newpollvideolink);
                                //console.log('Video has changed');
                                changemade = true;
                                pollsList.directive('youtube', function ($sce,$scope) {
                                    return {
                                        restrict: 'A',
                                        //scope: { youtube: '=' },
                                        replace: true,
                                        controller: function (scope) {
                                            $scope.videoFrame = $sce.trustAsHtml($scope.poll.VideoLink);
                                        }
                                    };
                                });
                            }
                            if ((newpollstatus != pollstatus) && (newpollstatus == 'Active')) {
                                updateCardCount(1, "active");
                                updateCardCount(-1, "draft");
                                scope.poll.Status = newpollstatus;
                                changemade = true;
                            }
                            $('#newpollvideolink').val('');
                            $('#newpollimagelink').val('');
                            $('#newpollstatus').val('');
                            $('#newpollname').val('');
                            $('#newpollquestion').val('');
                            $('#pollid').val('');
                            if (changemade == true) {
                                scope.$apply(function () {
                                    if (((newpollvideolink != pollvideolink) && (newpollvideolink != '')) || ((newpollimagelink != pollimagelink) && (newpollimagelink != ''))) {
                                        window.location.reload(true);
                                    }
                                });
                            }
                        //}
                        $('#newpollvideolink').val('');
                        $('#newpollimagelink').val('');
                        $('#newpollstatus').val('');
                        $('#newpollname').val('');
                        $('#newpollquestion').val('');
                        $('#pollid').val('');
                    }
                });
            }
        }
    });

    pollsList.directive('fancyboxlaunch', function () {
        $('#newpollvideolink').val('');
        $('#newpollimagelink').val('');
        $('#newpollstatus').val('');
        $('#newpollname').val('');
        $('#newpollquestion').val('');

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                $('#newpollvideolink').val('');
                $('#newpollimagelink').val('');
                $('#newpollstatus').val('');
                $('#newpollname').val('');
                $('#newpollquestion').val('');

                var url = '';
                var changemade = false;
                var pollstatus = scope.poll.Status;
                var polllaunchurl = scope.poll.LaunchUrl;
                var pollediturl = scope.poll.PollEditUrl;
                var pollreporturl = scope.poll.ReportUrl;
                var pollvideolink = scope.poll.VideoLink;
                var pollimagelink = scope.poll.ImageLink;
                var pollname = scope.poll.name;
                var pollquestion = scope.poll.question;
                $("#pollid").val(scope.poll.pk_pollid);
                url = polllaunchurl;

                $(element).fancybox({
                    href: url,
                    type: 'iframe',
                    scrolling: 'no',
                    maxWidth: 1200,
                    maxHeight: 1200,
                    fitToView: true,
                    width: '95%',
                    height: '95%',
                    autoSize: false,
                    closeClick: true,
                    openEffect: 'none',
                    closeEffect: 'elastic',
                    openSpeed: 200,
                    closeSpeed: 500,
                    transitionIn: 'none',
                    padding: 0,
                    helpers: {
                        title: {
                            type: 'float'
                        }
                    ,
                        overlay: {
                            showEarly: false,
                            locked: true
                        }
                    }
                    //,
                    //'onClosed': function () {
                    //    alert('On Close');
                    //}
                    ,
                    afterClose: function () {
                    var newpollimagelink = $('#newpollimagelink').val();
                    var newpollvideolink = $('#newpollvideolink').val();
                    var newpollstatus = $('#newpollstatus').val();
                    var newpollname = $('#newpollname').val();
                    var newpollquestion = $('#newpollquestion').val();
                    //console.log(newpollimagelink + ' , ' + pollimagelink);
                    //console.log(scope.poll.pk_pollid.toString() + ' , ' + $("#pollid").val());
                    //console.log(newpollstatus + ' , ' + pollstatus);
                    //console.log(newpollimagelink + ' , ' + pollimagelink);
                    //console.log(newpollvideolink + ' , ' + pollvideolink);
                        //if (scope.poll.pk_pollid.toString() == $("#pollid").val()) {
                    if ((newpollquestion != pollquestion) && (newpollquestion != '')) {
                        scope.poll.question = newpollquestion;
                        changemade = true;
                    }
                    if ((newpollname != pollname) && (newpollname != '')) {
                        scope.poll.name = newpollname;
                        changemade = true;
                    }
                    if ((newpollimagelink != pollimagelink) && (newpollimagelink != '')) {
                        scope.poll.ImageLink = newpollimagelink;
                        scope.poll.IsImagePoll = "true";
                        scope.poll.IsVideoPoll = "false";
                        scope.poll.type = "Image";
                        changemade = true;
                    }
                    if ((newpollvideolink != pollvideolink) && (newpollvideolink != '')) {
                        scope.poll.VideoLink = newpollvideolink;
                        scope.poll.IsVideoPoll = "true";
                        scope.poll.IsImagePoll = "false";
                        scope.poll.type = "Video";
                        changemade = true;
                    }
                    if ((newpollstatus != pollstatus) && (newpollstatus == 'Active')) {
                        scope.poll.Status = newpollstatus;
                        updateCardCount(1, "active");
                        updateCardCount(-1, "draft");
                        changemade = true;
                    }
                    $('#newpollvideolink').val('');
                    $('#newpollimagelink').val('');
                    $('#newpollstatus').val('');
                    $('#newpollname').val('');
                    $('#newpollquestion').val('');
                    $('#pollid').val('');
                    if (changemade == true) {
                        scope.$apply(function () {
                            if (((newpollvideolink != pollvideolink) && (newpollvideolink != '')) || ((newpollimagelink != pollimagelink) && (newpollimagelink != ''))) {
                                window.location.reload(true);
                            }
                        });
                    }
                        //}
                    $('#newpollvideolink').val('');
                    $('#newpollimagelink').val('');
                    $('#newpollstatus').val('');
                    $('#newpollname').val('');
                    $('#newpollquestion').val('');
                    $('#pollid').val('');
                }
            });
            }
        }
    });

    pollsList.directive('fancyboxreport', function () {
        $('#newpollvideolink').val('');
        $('#newpollimagelink').val('');
        $('#newpollstatus').val('');
        $('#newpollname').val('');
        $('#newpollquestion').val('');

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                $('#newpollvideolink').val('');
                $('#newpollimagelink').val('');
                $('#newpollstatus').val('');
                $('#newpollname').val('');
                $('#newpollquestion').val('');

                var url = '';
                var changemade = false;
                var pollstatus = scope.poll.Status;
                var polllaunchurl = scope.poll.LaunchUrl;
                var pollediturl = scope.poll.PollEditUrl;
                var pollreporturl = scope.poll.ReportUrl;
                var pollvideolink = scope.poll.VideoLink;
                var pollimagelink = scope.poll.ImageLink;
                var pollname = scope.poll.name;
                var pollquestion = scope.poll.question;
                $("#pollid").val(scope.poll.pk_pollid);
                url = pollreporturl;

                $(element).fancybox({
                    href: url,
                    type: 'iframe',
                    scrolling: 'no',
                    maxWidth: 1200,
                    maxHeight: 1200,
                    fitToView: true,
                    width: '95%',
                    height: '95%',
                    autoSize: false,
                    closeClick: true,
                    openEffect: 'elastic',
                    closeEffect: 'elastic',
                    openSpeed: 500,
                    closeSpeed: 500,
                    transitionIn: 'none',
                    padding: 0,
                    helpers: {
                        title: {
                            type: 'float'
                        }
                    ,
                        overlay: {
                            showEarly: true,
                            locked: true
                        }
                    }
                    //,
                    //'onClosed': function () {
                    //    alert('On Close');
                    //}
                    ,
                    afterClose: function () {
                        var newpollimagelink = $('#newpollimagelink').val();
                        var newpollvideolink = $('#newpollvideolink').val();
                        var newpollstatus = $('#newpollstatus').val();
                        var newpollname = $('#newpollname').val();
                        var newpollquestion = $('#newpollquestion').val();
                        if (scope.poll.pk_pollid.toString() == $("#pollid").val()) {
                            if ((newpollquestion != pollquestion) && (newpollquestion != '')) {
                                scope.poll.question = newpollquestion;
                                changemade = true;
                            }
                            if ((newpollname != pollname) && (newpollname != '')) {
                                scope.poll.name = newpollname;
                                changemade = true;
                            }
                            if ((newpollimagelink != pollimagelink) && (newpollimagelink != '')) {
                                scope.poll.ImageLink = newpollimagelink;
                                changemade = true;
                            }
                            if ((newpollvideolink != pollvideolink) && (newpollvideolink != '')) {
                                scope.poll.VideoLink = newpollvideolink;
                                changemade = true;
                            }
                            if ((newpollstatus != pollstatus) && (newpollstatus == 'Active')) {
                                scope.poll.Status = newpollstatus;
                                changemade = true;
                            }
                            if (changemade == true) {
                                scope.$apply(function () {
                                    if (((newpollvideolink != pollvideolink) && (newpollvideolink != '')) || ((newpollimagelink != pollimagelink) && (newpollimagelink != ''))) {
                                        window.location.reload(true);
                                    }
                                });
                            }
                        }
                        $('#newpollvideolink').val('');
                        $('#newpollimagelink').val('');
                        $('#newpollstatus').val('');
                        $('#newpollname').val('');
                        $('#newpollquestion').val('');
                        $('#pollid').val('');
                    }
                });
            }
        }
    });

    pollsList.directive('fancyboxpoll', function () {
        $('#newpollvideolink').val('');
        $('#newpollimagelink').val('');
        $('#newpollstatus').val('');
        $('#newpollname').val('');
        $('#newpollquestion').val('');

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                $('#newpollvideolink').val('');
                $('#newpollimagelink').val('');
                $('#newpollstatus').val('');
                $('#newpollname').val('');
                $('#newpollquestion').val('');

                var url = '';
                var changemade = false;
                var pollstatus = scope.poll.Status;
                var polllaunchurl = scope.poll.LaunchUrl;
                var pollediturl = scope.poll.PollEditUrl;
                var pollreporturl = scope.poll.ReportUrl;
                var pollvideolink = scope.poll.VideoLink;
                var pollimagelink = scope.poll.ImageLink;
                var pollname = scope.poll.name;
                var pollquestion = scope.poll.question;
                $("#pollid").val(scope.poll.pk_pollid);
                url = pollediturl;

                $(element).fancybox({
                    href: url,
                    type: 'iframe',
                    scrolling: 'no',
                    maxWidth: 1200,
                    maxHeight: 1200,
                    fitToView: true,
                    width: '95%',
                    height: '95%',
                    autoSize: false,
                    closeClick: true,
                    openEffect: 'elastic',
                    closeEffect: 'elastic',
                    openSpeed: 500,
                    closeSpeed: 500,
                    transitionIn: 'none',
                    padding: 0,
                    helpers: {
                        title: {
                            type: 'float'
                        }
                    ,
                        overlay: {
                            showEarly: true,
                            locked: true
                        }
                    }
                    ,
                    afterClose: function () {
                        var newpollimagelink = $('#newpollimagelink').val();
                        var newpollvideolink = $('#newpollvideolink').val();
                        var newpollstatus = $('#newpollstatus').val();
                        var newpollname = $('#newpollname').val();
                        var newpollquestion = $('#newpollquestion').val();
                        //console.log(newpollimagelink + ' , ' + pollimagelink);
                        //console.log(scope.poll.pk_pollid.toString() + ' , ' + $("#pollid").val());
                        //console.log(newpollstatus + ' , ' + pollstatus);
                        //console.log(newpollimagelink + ' , ' + pollimagelink);
                        //console.log(newpollvideolink + ' , ' + pollvideolink);
                        //if (scope.poll.pk_pollid.toString() == $("#pollid").val()) {
                        if ((newpollquestion != pollquestion) && (newpollquestion != '')) {
                            scope.poll.question = newpollquestion;
                            changemade = true;
                        }
                        if ((newpollname != pollname) && (newpollname != '')) {
                            scope.poll.name = newpollname;
                            changemade = true;
                        }
                        if ((newpollimagelink != pollimagelink) && (newpollimagelink != '')) {
                            scope.poll.ImageLink = newpollimagelink;
                            scope.poll.IsImagePoll = "true";
                            scope.poll.IsVideoPoll = "false";
                            scope.poll.type = "Image";
                            changemade = true;
                        }
                        if ((newpollvideolink != pollvideolink) && (newpollvideolink != '')) {
                            scope.poll.VideoLink = newpollvideolink;
                            scope.poll.IsVideoPoll = "true";
                            scope.poll.IsImagePoll = "false";
                            scope.poll.type = "Video";
                            changemade = true;
                        }
                        if ((newpollstatus != pollstatus) && (newpollstatus == 'Active')) {
                            scope.poll.Status = newpollstatus;
                            updateCardCount(1, "active");
                            updateCardCount(-1, "draft");
                            changemade = true;
                        }
                        $('#newpollvideolink').val('');
                        $('#newpollimagelink').val('');
                        $('#newpollstatus').val('');
                        $('#newpollname').val('');
                        $('#newpollquestion').val('');
                        $('#pollid').val('');
                        if (changemade == true) {
                            scope.$apply(function () {
                                if ((newpollvideolink != pollvideolink) && (newpollvideolink != '')) {
                                    window.location.reload(true);
                                }
                            });
                        }
                        //}
                        $('#newpollvideolink').val('');
                        $('#newpollimagelink').val('');
                        $('#newpollstatus').val('');
                        $('#newpollname').val('');
                        $('#newpollquestion').val('');
                        $('#pollid').val('');
                    }

                });
            }
        }
    });

    pollsList.directive('menudropdown', function () {
        return {
            restrict: 'A',
            //    controller: function ($scope) {
            //        var imagelink = $scope.poll.ImageLink;
            //        var polltype = $scope.poll.type;
            //        var pollid = $scope.poll.pk_pollid;
            //        //$(element).attr('data-activates', 'carddrop' + scope.poll.pk_pollid);
            //        //$(element).attr('id', 'card' + scope.poll.pk_pollid);
            //        //$(element).attr('class', 'dropdown-button');
            //        //$scope.dataactivate = 'carddrop' + pollid;
            //        //$scope.id = 'card' + pollid;
            //        $scope.class = 'dropdown-button';
            //        $('#card' + pollid).dropdown({
            //        ////$('.dropdown-button').dropdown({
            //            inDuration: 300,
            //            outDuration: 225,
            //            constrain_width: true, // Does not change width of dropdown to that of the activator
            //            hover: true, // Activate on hover
            //            gutter: 0, // Spacing from edge
            //            belowOrigin: false, // Displays dropdown below the button
            //            alignment: 'left' // Displays dropdown with edge aligned to the left of button
            //        });
            //    }
            //}
            link: function (scope, element, attrs) {
                //$(element).bind('click',function () { 
                //    $(element).dropdown({
                //        inDuration: 300,
                //        outDuration: 225,
                //        constrain_width: true, // Does not change width of dropdown to that of the activator
                //        hover: false, // Activate on hover
                //        gutter: 0, // Spacing from edge
                //        belowOrigin: true, // Displays dropdown below the button
                //        alignment: 'right' // Displays dropdown with edge aligned to the left of button
                //    });
                //});
                $(element).bind('hover', function () {
                    $(element).dropdown({
                        inDuration: 300,
                        outDuration: 225,
                        constrain_width: false, // Does not change width of dropdown to that of the activator
                        hover: false, // Activate on hover
                        gutter: 0, // Spacing from edge
                        belowOrigin: true, // Displays dropdown below the button
                        alignment: 'right' // Displays dropdown with edge aligned to the left of button
                    });
                });
                $(element).bind('mouseover', function () {
                    //alert('1');
                    $(element).dropdown({
                        inDuration: 300,
                        outDuration: 225,
                        constrain_width: true, // Does not change width of dropdown to that of the activator
                        hover: false, // Activate on hover
                        gutter: 0, // Spacing from edge
                        belowOrigin: true, // Displays dropdown below the button
                        alignment: 'right' // Displays dropdown with edge aligned to the left of button
                    });
                });
            }
        }
    });

    pollsList.directive('showaddpollbuttons', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                $(element).bind('click', function () {
                    $(element).collapsible(
                        {
                            accordion: true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
                        }
                    );
                });

                //$(element).bind('hover', function () {
                //    $(element).collapsible(
                //        {
                //            accordion: true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
                //        }
                //    );
                //});

                //$(element).bind('mouseover', function () {
                //    $(element).collapsible(
                //        {
                //            accordion: true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
                //        }
                //    );
                //});
            }
        }
    });

    var chartsnewcount = 0;

    pollsList.directive('resultschart', function ($sce) {
        return {
            restrict: 'A',
            //scope: { resultschart: '=resultschart' },
            controller: function ($scope) {
                var chartanswerrowcountnew = 0;
                var arrColours = ['#AFD8F8', '#F7D671', '#8BBA00', '#A66EDD', '#F984A1', '#AFD8F8', '#F6BD0F', '#8BBA00', '#A66EDD', '#F984A1'];
                var poll = $scope.poll;
                var pollanswers = $scope.poll.pollansweroptions;
                var totalresponses = parseInt($scope.poll.TOTAL_RESPONSES);
                ////console.log("here-" + $(".resultsclass").width());
                var resultschartwidth = $(".resultsclass").width();
                var purejschart = "<div class='black-text col m12 s12' style='font-size:12;border-style:none; width:100%;margin-left:0px;margin-right:0px;padding-bottom:10px;position:relative;background-color:#F7F6F6;overflow:hidden !important;'>";
                var adjustedchartwidth = "75%";
                if (totalresponses > 0) {
                    chartanswerrowcountnew = pollanswers.length;
                    for (var x = 0; x < pollanswers.length; x++) {
                        var thisanswer = pollanswers[x].answeroption;
                        var thisanswerid = pollanswers[x].answerid;
                        var thisanswercount = parseInt(pollanswers[x].answercount);
                        var thisrowpercentage = Math.round(thisanswercount * 100 / totalresponses);
                        thisrowwidth = Math.round(thisrowpercentage * 0.70 * resultschartwidth / 100);
                        purejschart += "<div style='width:100%;text-align:left;font-size:9pt;float:left;padding-left:0px;padding-top:5px;border-style:none;'>" + thisanswer + "</div>";
                        purejschart += "<div style='width:" + adjustedchartwidth + ";float:left;background-color:lightgray;border-style:none;'>";
                        if (thisrowpercentage >= 100) {
                            purejschart += "<div style='height:15px;width:100%;max-width:100%;min-width:100%;background-color:" + arrColours[x] + ";float:left;margin-left:0px;' id='div" + thisanswerid + ";'></div>";
                        } else {
                            //purejschart += "<div style='height:15px;width:" + thisrowwidth + "px;max-width:" + thisrowwidth + "px;min-width:" + thisrowwidth + "px;background-color:" + arrColours[x] + ";float:left;margin-left:0px;' id='div" + thisanswerid + ";'></div>";
                            purejschart += "<div style='height:15px;width:" + thisrowpercentage + "%;background-color:" + arrColours[x] + ";margin-left:0px;float:left;' id='div" + thisanswerid + "'></div>";
                        }
                        purejschart += "</div>";
                        //purejschart += "<div class='card-content black-text' style='float:right;width:25%;vertical-align:top;font-size:9pt;'>" + thisrowpercentage + thisrowwidth + "%</div>";
                        purejschart += "<div style='float:right;width:15%;vertical-align:top;font-size:9pt;'>" + thisrowpercentage + "%</div>";
                        //purejschart += "<div class='card-content black-text' style='width:" + adjustedchartwidth + ";height:26px;border-style:solid;'></div>";

                    }
                    purejschart += "</div>";
                    $scope.chartsdata = $sce.trustAsHtml(purejschart);
                    for (var x = 0; x < chartanswerrowcountnew; x++) {
                        var thisanswerid = pollanswers[x].answerid;
                        var thisanswercount = parseInt(pollanswers[x].answercount);
                        var thisrowpercentage = Math.round(thisanswercount * 100 / totalresponses);

                        if (thisrowpercentage > 100) {
                            thisrowpercentage = 100;
                        }
                        thisrowwidth = Math.round(thisrowpercentage * 150 / 100);
                        ////console.log(thisrowpercentage.toString() + '--' + thisrowwidth.toString());
                        
                        //$("#div" + thisanswerid).animate({
                        //    backgroundColor: arrColours[x],
                        //    color: "#fff",
                        //    width: thisrowwidth
                        //}, 10000);
                    }
                }
            }
        }
    });

    pollsList.directive('imagedata', function () {
        return {
            restrict: 'A',
            controller: function ($scope) {
                var imagelink = $scope.poll.ImageLink;
                var polltype = $scope.poll.type;
                var pollid = $scope.poll.pk_pollid;
                ////console.log(imagelink);
                if ((polltype == "Image")&&(imagelink == "")) {
                    $scope.image = "/polls/images/ImagePoll.jpg";
                }
                else if ((polltype == "Image") && (imagelink != "")) {
                    //$scope.image = "/polls/images/" + pollid + "/" + imagelink;
                    imagelink = "/polls/images/" + pollid + "/" + imagelink;
                    try {
                        $.ajax({
                            url: imagelink,
                            type: "HEAD",
                            error: function () {
                                ////console.log('12');
                                imagelink = "/polls/images/ImagePoll.jpg";
                            }
                        });

                    }
                    catch (e) {
                        imagelink = "/polls/images/ImagePoll.jpg";
                        ////console.log('124');
                    }
                    $scope.image = imagelink;
                }
            }
        }
    });

    pollsList.directive('polldeletion', function () {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {
                    var pollid = scope.poll.pk_pollid;
                    $('#pollactiontype').val('delete');
                    $('#pollid').val(pollid);
                    $("#p" + pollid).hide();
                    $('#modalheader-' + pollid).html('Delete poll');
                    $('#modalbody-' + pollid).html('Are you sure you want to delete this poll?');
                    $('.modal-trigger').leanModal();
                    $('#modal-' + pollid).openModal();
                });
            }
        }
    });

    pollsList.directive('pollcopying', function () {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {
                    var pollid = scope.poll.pk_pollid;
                    $('#pollactiontype').val('copy');
                    $('#pollid').val(pollid);
                    $("#p" + pollid).hide();
                    $('#modalheader-' + pollid).html('Copy poll');
                    $('#modalbody-' + pollid).html('Are you sure you want to copy this poll? A new poll will be created in drafts.');
                    $('.modal-trigger').leanModal();
                    $('#modal-' + pollid).openModal();
                });
            }
        }
    });

    pollsList.directive('pollclosing', function () {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {
                    //if (confirm("Are you sure you want to close this poll")) {
                    //    var pollid = scope.poll.pk_pollid;

                    //    $.post('/Polls/AjaxService.aspx', { 'method': 'ClosePollByPollId', 'Id': pollid }).success(function (data) {
                    //        if (data == '0') {
                    //            scope.poll.Status = 'Closed';
                    //            scope.$apply();
                    //        }
                    //        else {
                    //            alert("Error closing this poll. Please try after a while.");
                    //        }
                    //    });
                    //}
                    var pollid = scope.poll.pk_pollid;
                    $('#pollactiontype').val('close');
                    $('#pollid').val(pollid);
                    $("#p" + pollid).hide();
                    $('#modalheader-' + pollid).html('Close poll');
                    $('#modalbody-' + pollid).html('Are you sure you want to close this poll?');
                    $('.modal-trigger').leanModal();
                    $('#modal-' + pollid).openModal();

                });
            }
        }
    });

    pollsList.directive('pollreplacing', function () {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {

                    var pollid = scope.poll.pk_pollid;
                    $('#pollactiontype').val('replace');
                    $('#pollid').val(pollid);
                    $('#modalheader-' + pollid).html('Replace poll');
                    $('#p' + pollid).show();
                    $('#modalbody-' + pollid).html('Are you sure you want to replace this poll? When it is launched the parent poll will be closed.');
                    $('.modal-trigger').leanModal();
                    $('#modal-' + pollid).openModal();

                });
            }
        }
    });

    pollsList.directive('pollaction', function () {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {
                    var pollid = $('#pollid').val();
                    var pollactiontype = $("#pollactiontype").val();
                    if (pollactiontype == "delete") {
                        $.post('/Polls/AjaxService.aspx', { 'method': 'DeletePollByPollId', 'Id': pollid }).success(function (data) {
                            if (data == '0') {
                                updateCardCount(-1, scope.poll.Status.toLowerCase());
                                scope.poll.Status = 'Deleted';
                                scope.$apply();
                            }
                            else {
                                $('#modalerror').openModal();
                            }
                        });
                    }
                    else if (pollactiontype == "close") {
                        $.post('/Polls/AjaxService.aspx', { 'method': 'ClosePollByPollId', 'Id': pollid }).success(function (data) {
                            if (data == '0') {
                                updateCardCount(-1, scope.poll.Status.toString().toLowerCase());
                                updateCardCount(1, "closed");
                                scope.poll.Status = 'Closed';
                                scope.$apply();
                            }
                            else {
                                $('#modalerror').openModal();
                            }
                        });
                    }
                    else if (pollactiontype == "copy") {
                        $.post('/Polls/AjaxService.aspx', { 'method': 'CopyPollByPollId', 'Id': pollid }).success(function (data) {
                            if (data == 'Success') {
                                window.location.reload(true);
                            }
                            else {
                                $('#modalerror').openModal();
                            }
                        });
                    }
                    else if (pollactiontype == "replace") {
                            $("#p" + pollid).show();
                            var replaceURL = scope.poll.ReplaceUrl;
                            var replacepollname = $("#replacepollname" + pollid).val();
                            var checkPollNameURL = "/Polls/AjaxService.aspx";
                            var updatePollNameURL = "/Polls/AjaxService.aspx";
                            if (replacepollname == "") {
                                $('#modalerrorforreplacepollname').openModal();
                                $('#errormessagereplacepollname').html('Please provide a name for the poll to replace. This name should not have been given to any of your other polls.');
                            }
                            else {
                                $.get(checkPollNameURL, { 'method': 'CheckPollNameExists', 'pollname': replacepollname })
                                .error(function () {
                                    $('#modalerrorforreplacepollname').openModal();
                                    $('#errormessagereplacepollname').html('Please provide a name for the poll to replace. This name should not have been given to any of your other polls.');
                                })
                                .success(function (data) {
                                    if (data == "Success") {
                                        $.get(replaceURL, {}).success(function (data) {
                                            $.get(updatePollNameURL, { 'method': 'UpdateReplacedPollName', 'pollname': replacepollname, 'pollid': pollid })
                                            .done(function () {

                                                window.location.reload(true);
                                            });
                                        })
                                        .error(function () {
                                            $('#modalerror').openModal();
                                        });
                                    }
                                    else {
                                        $('#modalerrorforreplacepollname').openModal();
                                        $('#errormessagereplacepollname').html('The poll name already exists. Please choose a different name.');
                                        //$('#modalerror').openModal();
                                    }
                                });
                            }
                        }
                });
            }
        }
    });

    pollsList.directive('pollactioncancel', function () {
        return {
            restrict: 'AEC',
            link: function (scope, element) {
                $(element).bind('click', function () {
                    alert($scope.poll.pk_pollid);
                });
            }
        }
    });

    function updateCardCount(diff, cardtype) {
        console.log(cardtype);
        var cardcount = parseInt($("#" + cardtype + "count").html());
        console.log(cardcount);
        if (diff == 1) {
            cardcount++;
        }
        else {
            cardcount--;
        }
        console.log(cardcount);
        $("#" + cardtype + "count").html(cardcount.toString());
    }