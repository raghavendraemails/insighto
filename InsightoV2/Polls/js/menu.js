function initMenu() {
    $('.menu ul').hide();
    $('.menu ul:first').show();
    $('.menu li a').click(function () {
        var checkElement = $(this).next();        
        if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            return false;
        }
        if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('.menu ul:visible').slideUp('normal').addClass('activeLink');
            checkElement.slideDown('normal');
            return false;
        }
    });
    $('.addrMenu ul').show();
    $('.addrMenu ul:first').hide();
    $('.addrMenu li a').click(function () {
        var checkElement = $(this).next();
        if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            return false;
        }
        if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('.addrMenu ul:visible').slideUp('normal').addClass('activeLink');
            checkElement.slideDown('normal');
            return false;
        }
    });
}

$(document).ready(function () {
    initMenu();    
    $('.innerMenu a').each(function () {		
        if (document.URL.indexOf($(this).attr('rel')) > 0 || $(this).hasClass('current')) {
            $(this).parentsUntil(".menu").addClass('activeLink');
            $('.menu ul').hide();
            $('.menu ul.activeLink').show();
            $('.addrMenu ul').hide();
            $('.addrMenu ul.activeLink').show();
        }
    });
});



