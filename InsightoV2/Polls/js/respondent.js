﻿$(document).ready(function () {

    $("#btnVoteAn").click(function () {
        var element = document.getElementById("radAnswerOptions");
        if (typeof (element) != 'undefined' && element != null) {
            var no = document.getElementById("radAnswerOptions").rows.length - 1;
            console.log("here 0");
            if ($("input[name='radAnswerOptions']:checked").length < 1) {
                jAlert('Please select answer option', 'Polls', function (r) {
                    return false;
                });
                return false;
            }
            else {
                var resid = getCookie("RespondentId");
                if (resid != "") {
                    var unique = $("#hdnUniqueRes").val();
                    if (parseInt(unique) == 0) {
                        $(".textpollthemeresponse").css("display", "none");
                        $(".classmsg").css("display", "block");
                        return false;
                    }
                    else {
                        console.log("here1");
                        submitResults();
                        return false;
                        //return true;
                    }
                }
                else {
                    resid = $("#hdnRespondentId").val();
                    setCookie("RespondentId", resid);
                }
                console.log("here2");

                submitResults();
                return false;
                //return true;
            }
        }
        else {
            var count = 0;
            no = document.getElementById("ckbAnswerOptions").rows.length - 1;
            var selectedValues = [];
            if ($("[id*=ckbAnswerOptions] input:checkbox:checked").length < 1) {
                jAlert('Please select answer option', 'Polls', function (r) {
                    return false;
                });
                return false;
            }
            else {
                var resid = getCookie("RespondentId");
                if (resid != "") {
                    var unique = $("#hdnUniqueRes").val();
                    if (parseInt(unique) == 0) {
                        $(".textpollthemeresponse").css("display", "none");
                        $(".classmsg").css("display", "block");
                        return false;
                    }
                    else {
                        submitResults();
                        return false;
                        //return true;
                    }
                }
                else {
                    resid = $("#hdnRespondentId").val();
                    setCookie("RespondentId", resid);
                }
                submitResults();
                return false;
                //return true;
            }
        }


    });

    $("#MyLink").click(function () {
        var element = document.getElementById("radAnswerOptions");
        if (typeof (element) != 'undefined' && element != null) {
            var no = document.getElementById("radAnswerOptions").rows.length - 1;
            if ($("input[name='radAnswerOptions']:checked").length < 1) {
                jAlert('Please select answer option', 'Polls', function (r) {
                    return false;
                });
                return false;
            }
            else {
                var resid = getCookie("RespondentId");
                if (resid != "") {
                    var unique = $("#hdnUniqueRes").val();
                    if (parseInt(unique) == 0) {
                        $(".textpollthemeresponse").css("display", "none");
                        $(".classmsg").css("display", "block");
                        return false;
                    }
                    else {
                        submitResults();
                        return false;
                        //return true;
                    }
                }
                else {
                    resid = $("#hdnRespondentId").val();
                    setCookie("RespondentId", resid);
                }
                submitResults();
                return false;
                //return true;
            }
        }
        else {
            var count = 0;
            no = document.getElementById("ckbAnswerOptions").rows.length - 1;
            var selectedValues = [];
            if ($("[id*=ckbAnswerOptions] input:checkbox:checked").length < 1) {
                jAlert('Please select answer option', 'Polls', function (r) {
                    return false;
                });
                return false;
            }
            else {

                var resid = getCookie("RespondentId");
                if (resid != "") {
                    var unique = $("#hdnUniqueRes").val();
                    if (parseInt(unique) == 0) {
                        $(".textpollthemeresponse").css("display", "none");
                        $(".classmsg").css("display", "block");
                        return false;
                    }
                    else {
                        submitResults();
                        return false;
                        //return true;
                    }
                }
                else {
                    resid = $("#hdnRespondentId").val();
                    setCookie("RespondentId", resid);
                }
                submitResults();
                return false;
                //return true;
            }
        }
    });

    $("#LinkNew").click(function () {
        var element = document.getElementById("radAnswerOptions");
        if (typeof (element) != 'undefined' && element != null) {
            var no = document.getElementById("radAnswerOptions").rows.length - 1;
            if ($("input[name='radAnswerOptions']:checked").length < 1) {
                jAlert('Please select answer option', 'Polls', function (r) {
                    return false;
                });
                return false;
            }
            else {
                var resid = getCookie("RespondentId");
                if (resid != "") {
                    var unique = $("#hdnUniqueRes").val();
                    if (parseInt(unique) == 0) {
                        $(".textpollthemeresponse").css("display", "none");
                        $(".classmsg").css("display", "block");
                        return false;
                    }
                    else {
                        submitResults();
                        return false;
                        //return true;
                    }
                }
                else {
                    resid = $("#hdnRespondentId").val();
                    setCookie("RespondentId", resid);
                }
                submitResults();
                return false;
                //return true;
            }
        }
        else {
            var count = 0;
            no = document.getElementById("ckbAnswerOptions").rows.length - 1;
            var selectedValues = [];
            if ($("[id*=ckbAnswerOptions] input:checkbox:checked").length < 1) {
                jAlert('Please select answer option', 'Polls', function (r) {
                    return false;
                });
                return false;
            }
            else {

                var resid = getCookie("RespondentId");
                if (resid != "") {
                    var unique = $("#hdnUniqueRes").val();
                    if (parseInt(unique) == 0) {
                        $(".textpollthemeresponse").css("display", "none");
                        $(".classmsg").css("display", "block");
                        return false;
                    }
                    else {
                        submitResults();
                        return false;
                        //return true;
                    }
                }
                else {
                    resid = $("#hdnRespondentId").val();
                    setCookie("RespondentId", resid);
                }
                submitResults();
                return false;
                //return true;
            }
        }
    });

    var numberofrows = parseInt($("tr").length);
    var answerrowheight = 35;
    var tableheight = numberofrows * answerrowheight;
    $("#radAnswerOptions").css("height", tableheight.toString() + "px");
    $("#radAnswerOptions").css("margin-top", "-2px");
    //$("td").css("margin-bottom", "10px");
    var pollBg = $("#hdnPollBgColor").val();
    var pollQuestion = $("#hdnQuestColor").val();
    var pollQuestionBg = $("#hdnQuestBgColor").val();
    var pollAnswer = $("#hdnAnswerColor").val();
    var pollVote = $("#hdnVoteColor").val();
    var pollVoteBg = $("#hdnVoteBgColor").val();
    var width = $("#hdnWidth").val();
    var height = $("#hdnHeight").val();
    var heightrec = $("#hdnRecHeight").val();
    var pollQuestionFont = $("#hdnQuestionFont").val();
    var answerfont = $("#hdnAnswerFont").val();
    var votefont = $("#hdnVoteFont").val();
    var msgfont = $("#hdnMsgFont").val();
    var questweight = $("#hdnQuestionBold").val();
    var answerweight = $("#hdnAnswerBold").val();
    var voteweight = $("#hdnVoteBold").val();
    var msgweight = $("#hdnMsgBold").val();
    var queststyle = $("#hdnQuestionItalic").val();
    var answerstyle = $("#hdnAnswerItalic").val();
    var votestyle = $("#hdnVoteItalic").val();
    var msgstyle = $("#hdnMsgItalic").val();
    var questul = $("#hdnQuestionUL").val();
    var answerul = $("#hdnAnswerUL").val();
    var voteul = $("#hdnVoteUL").val();
    var msgul = $("#hdnMsgUL").val();
    var questalign = $("#hdnQuestionAlign").val();
    var answeralign = $("#hdnAnswerAlign").val();
    var votealign = $("#hdnVoteAlign").val();
    var msgalign = $("#hdnMsgAlign").val();
    var questsize = $("#hdnQuestionFSize").val();
    var answersize = $("#hdnAnswerFSize").val();
    var votesize = $("#hdnVoteFSize").val();
    var msgsize = $("#hdnMsgFSize").val();
    var msgbgcolor = $("#hdnMsgBgColor").val();
    var msgcolor = $("#hdnMsgColor").val();
    var pollbgcolor = $("#hdnPollBgColor").val();

    $(".lblRequired").css("display", "none");
    $(".classmsg").css("background-color", "#ffffff");
    $(".classmsg").css("width", width);
    var heightcl = parseInt(height) - 17;
    $(".classmsg").css("height", heightcl);
    $(".textpollthemeresponse").css("background-color", "#" + pollBg);
    $(".textpollthemeresponse").css("width", width);
    $(".textpollthemeprint").css("width", width);
    $(".videopreview").css("width", width);
    var comwidth = width - 30;
    $(".txtcomments").css("width", comwidth + 'px');
    $(".commentstxt").css("color", "#" + pollAnswer);
    $(".textpollthemeresponse").css("height", height);
    $(".textpollthemeprint").css("height", height);
    if (height < heightrec) {
        $(".poweredbyres").css("position", 'absolute');
        $(".poweredbyres").css("bottom", '10px');
        $(".poweredbyres").css("text-align", 'center');
        $(".poweredbyres").css("width", '100%');
        $(".poweredbyres").css("margin-top", '');
        $(".poweredbyres").css("margin-bottom", '');
        $(".poweredbyres").css("padding-bottom", '');
        $(".voteres").css("position", 'absolute');
        $(".voteresview").css("position", 'absolute');
        if ($('#poweredby').is(':hidden')) {
            $(".voteres").css("bottom", '7px');
            $(".voteresview").css("bottom", '7px');
        }
        else {
            $(".voteres").css("bottom", '7px');
            $(".voteresview").css("bottom", '7px');
        }
    }
    else {
        $(".poweredbyres").css("position", 'absolute');
        $(".poweredbyres").css("bottom", '10px');
        $(".poweredbyres").css("text-align", 'center');
        $(".poweredbyres").css("width", '100%');
        $(".poweredbyres").css("margin-top", '');
        $(".poweredbyres").css("margin-bottom", '');
        $(".poweredbyres").css("padding-bottom", '');
        $(".voteres").css("position", 'absolute');
        $(".voteresview").css("position", 'absolute');
        if ($('#poweredby').is(':hidden')) {
            $(".voteres").css("bottom", '7px');
            $(".voteresview").css("bottom", '7px');
        }
        else {
            $(".voteres").css("bottom", '7px');
            $(".voteresview").css("bottom", '7px');
        }
    }
    $(".questionpreviewcss").css("background-color", "#" + pollQuestionBg);
    $(".questionpreviewcss").css("font-family", pollQuestionFont);
    $(".questionpreviewcss").css("color", "#" + pollQuestion);
    $(".radiopolltheme").css("color", "#" + pollAnswer);
    $(".sponsorimagepreview").css("color", "#" + pollAnswer);
    $(".socialshares").css("color", "#" + pollAnswer);
    $(".sponsorimagepreview").css("font-family", answerfont);
    $(".socialshares").css("font-family", answerfont);

    $(".viewandvoteimagepreview").css("background-color", "#" + pollVoteBg);
    $(".viewandvoteimagepreview").css("font-family", votefont);
    $(".viewandvoteimagepreview").css("text-decoration", voteul);

    $(".viewandvoteimageresponse").css("background-color", "#" + pollVoteBg);
    $(".viewandvoteimageresponse").css("font-family", votefont);
    $(".viewandvoteimageresponse").css("text-decoration", voteul);

    $(".viewandvoteimageresponsebtn").css("font-family", votefont);
    $(".viewandvoteimageresponsebtn").css("background-color", "#" + pollVoteBg);
    $(".viewandvoteimageresponsebtn").css("text-decoration", voteul);

    $(".viewandvoteimagepreview").css("color", "#" + pollVote);

    $(".poweredbyres").css("color", "#" + pollAnswer);
    $(".poweredbyres a").css("color", "#" + pollAnswer);

    $(".sponsorimagepreview").css("font-size", answersize + 'px');
    $(".socialshares").css("font-size", answersize + 'px');
    $('.messageprivate').css({ 'font-weight': msgweight });
    $('.messageprivate').css({ 'font-family': msgfont });
    $('.messageprivate').css({ 'font-style': msgstyle });
    $('.messageprivate').css({ 'font-size': msgsize + 'px' });
    $('.messageprivate').css({ 'background-color': "#" + msgbgcolor });
    $('.messageprivate').css({ 'color': "#" + msgcolor });
    $('.questionpreviewcss').css({ 'font-weight': questweight });
    $('.radiopolltheme').css({ 'font-weight': answerweight });
    $('.viewandvoteimagepreview').css({ 'font-weight': voteweight });
    $('.questionpreviewcss').css({ 'font-style': queststyle });
    $('.radiopolltheme').css({ 'font-style': answerstyle });
    $('.radiopolltheme').css({ 'font-family': answerfont });
    $('.viewandvoteimagepreview').css({ 'font-style': votestyle });
    $('.questionpreviewcss').css({ 'text-decoration': questul });
    $('.radiopolltheme').css({ 'text-decoration': answerul });
    $('.viewandvoteimagepreview').css({ 'text-decoration': voteul });
    $('.questionpreviewcss').css({ 'text-align': questalign });
    $('.radiopolltheme').css({ 'text-align': answeralign });
    $('.sponsorimage').css({ 'font-family': answerfont });
    $('.sponsorimage').css({ 'font-size': answersize + 'px' });
    $('.sponsorimage').css({ 'color': "#" + pollAnswer });
    $('.viewandvoteimagepreview').css({ 'text-align': votealign });
    $('.questionpreviewcss').css({ 'font-size': questsize + 'px' });
    $('.radiopolltheme').css({ 'font-size': answersize + 'px' });
    $('.viewandvoteimagepreview').css({ 'font-size': votesize + 'px' });
    if ($('#social').is(':hidden')) {
        $(".sponsorimage").css("margin-right", '2%');
        $(".sponsorimage").css("float", 'right');
    }
    else {
        $(".sponsorimage").css("margin-right", '1%');
        $(".sponsorimage").css("float", 'right');
    }
    if ($('#sponsorpreviw').is(':hidden')) {
        if ($('#social').is(':hidden')) {
            $('.votecounres').css({ 'position': 'relative' });
        }
    }

    if ($('#sponsorpreviw').is(':hidden')) {
        $('.viewandvoteimageresponse').css({ 'width': '70%' });
        $('.viewandvoteimageresponse').css({ 'margin-top': '10px' });
        $('.viewandvoteimageresponse').css({ 'margin-left': '15%' });
        $('.view').css({ 'font-size': '13px' });
        $('.viewandvoteimageresponsebtn').css({ 'font-size': '13px' });
        $('.view').css({ 'text-align': 'left' });
        $('.vote').css({ 'text-align': 'center' });
        $('.vote').css({ 'float': 'none' });
        $('.vote').css({ 'width': '100%' });
        $('.classresultsocial').css({ 'width': '100%' });
        var count = 0;
        if ($('#twittericon').is(':hidden')) {
        }
        else {
            count++;
        }
        if ($('#fbicon').is(':hidden')) {

        }
        else {
            count++;
        }
        if ($('#googleicon').is(":hidden")) {

        }
        else {
            count++;
        }

        if ($('#sponsorres').is(':hidden')) {
            $('.viewandvoteimageresponse').css({ 'width': '70%' });
            $('.viewandvoteimageresponse').css({ 'margin-top': '10px' });
            $('.viewandvoteimageresponse').css({ 'margin-left': '15%' });
            $('.view').css({ 'font-size': '13px' });
            $('.viewandvoteimageresponsebtn').css({ 'font-size': '13px' });
            $('.view').css({ 'text-align': 'left' });
            $('.vote').css({ 'text-align': 'center' });
            $('.vote').css({ 'float': 'none' });
            $('.vote').css({ 'width': '100%' });

        }
        else {
            $('.viewandvoteimageresponse').css({ 'width': '45%' });
            $('.viewandvoteimageresponse').css({ 'float': 'left' });
            $('.viewandvoteimageresponse').css({ 'margin-left': '10px' });
            $('.viewandvoteimageresponse').css({ 'margin-top': '6px' });
            $('.view').css({ 'font-size': '10px' });
            $('.view').css({ 'text-align': 'center' });
            $('.vote').css({ 'text-align': 'center' });
            $('.vote').css({ 'width': '100%' });
            $(".sponsorimage").css("margin-top", '3px');

            $('.classresultsocial').css({ 'width': '31%' });
            var count = 0;
            if ($('#twittericon').is(':hidden')) {
            }
            else {
                count++;
            }
            if ($('#fbicon').is(':hidden')) {

            }
            else {
                count++;
            }
            if ($('#googleicon').is(":hidden")) {

            }
            else {
                count++;
            }
            $('.socialshares').css({ 'width': '31%' });
            $('.socialshares').css({ 'margin-left': '2%' });
        }
    }
    else {
        $('.viewandvoteimageresponse').css({ 'width': '45%' });
        $('.viewandvoteimageresponse').css({ 'float': 'left' });
        $('.viewandvoteimageresponse').css({ 'margin-left': '10px' });
        $('.viewandvoteimageresponse').css({ 'margin-top': '6px' });
        //$('.classresultsocial').css({ 'margin-left': '13%' });
        $('.classresultsocial').css({ 'width': '100%' });
        $('.view').css({ 'font-size': '10px' });
        $('.view').css({ 'text-align': 'center' });
        $('.vote').css({ 'text-align': 'center' });
        $('.vote').css({ 'width': '100%' });
        $(".sponsorimage").css("margin-top", '3px');
        $('.classresultsocial').css({ 'width': '31%' });
        var count = 0;
        if ($('#twittericon').is(':hidden')) {
        }
        else {
            count++;
        }
        if ($('#fbicon').is(':hidden')) {

        }
        else {
            count++;
        }
        if ($('#googleicon').is(":hidden")) {

        }
        else {
            count++;
        }
        $('.socialshares').css({ 'width': '31%' });
        $('.socialshares').css({ 'margin-left': '2%' });
    }
    if ($('#messagebox').is(':hidden')) {

    }
    else {
        $(".poweredbyres").css("position", 'absolute');
        $(".poweredbyres").css("bottom", '10px');
        $(".poweredbyres").css("text-align", 'center');
        $(".poweredbyres").css("width", '100%');
        $(".poweredbyres").css("margin-top", '');
        $(".poweredbyres").css("margin-bottom", '');
        $(".poweredbyres").css("padding-bottom", '');
        $(".voteres").css("position", 'absolute');
        $(".voteresview").css("position", 'absolute');
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '7px');
        }
        else {
            $(".viewtest").css("bottom", '30px');
        }
        $(".sponsorimage").css("margin-left", '0px');
    }

});

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) != -1) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue) {
    document.cookie = cname + "=" + cvalue + ";";
}

function submitResults() {
    $("#voteandview").hide();
    $("#votebutton").hide();
    $("#onlyvote").hide();
    $("#onlyvoteres").hide();
    $("#voteviewres").hide();
    $("#sponsorres").hide();
    var respondentid = $("#hdnRespondentId").val();
    var comments = $("#txtComments").val();
    var answeroptions = "";
    var pollid = $("#hdn").val();
    var answerlength = 0;

    var element = document.getElementById("radAnswerOptions");
    if (typeof (element) != 'undefined' && element != null) {
        answerlength = $("input[name='radAnswerOptions']").length;
        element = $("input[name='radAnswerOptions']");
    }
    else {
        answerlength = $("[id*=ckbAnswerOptions] input:checkbox").length;
        element = $("[id*=ckbAnswerOptions] input:checkbox");
    }
    for (var p = 0; p < answerlength; p++) {
        if (element[p].checked) {
            answeroptions += element[p].value + ",";
        }
    }
    //console.log(answeroptions);
    $.ajax({
        type: "GET",
        url: "AjaxService.aspx?method=SavePollResponse",
        data: { pollid: pollid, respondentid: respondentid, answeroptions: answeroptions, comments: comments },
        dataType: "json",
        success: function (data) {
            $("#commentbox").hide();
            $("#singleradbtns").hide();
            $("#multipleckbbtns").hide();
            $("#voteandview").hide();
            $("#votebutton").hide();
            $("#onlyvote").hide();
            $("#onlyvoteres").hide();
            $("#voteviewres").hide();
            $("#sponsorres").hide();
            $("#lblQuestion").append(" (" + data.votecount + " votes)");
            if (data.resultaccess == "private") {
                $("#messagebox").show();
                if (data.privateresultsmessage.length == 0) {
                    $("#lblMessage").html("Thank you for voting!");
                }
                else {
                    $("#lblMessage").html(unescape(data.privateresultsmessage));
                }
            }
            else {
                $("#messagebox").hide();
            }
            if (data.showsocialsharing == "1") {
                $("#voteres").show();
                $("#socialshares").show();
                $("#social").show();
                if (data.showtwitter == "1") {
                    $("#twittericon").show();
                }
                else {
                    $("#twittericon").hide();
                }
                if (data.showgoogleplus == "1") {
                    $("#googleicon").show();
                }
                else {
                    $("#googleicon").hide();
                }
                if (data.showfacebook == "1") {
                    $("#fbicon").show();
                }
                else {
                    $("#fbicon").hide();
                }
            }
            else {
                $("#socialshares").hide();
                $("#social").hide();
                $("#twittericon").hide();
                $("#googleicon").hide();
                $("#fbicon").hide();
            }
            //var obj = JSON.parse(data);
            console.log(data.votecount);
            renderChart(data.answeroptions);
        }
    });
}

var arrColours = ['#AFD8F8', '#F6BD0F', '#8BBA00', '#A66EDD', '#F984A1', '#AFD8F8', '#F6BD0F', '#8BBA00', '#A66EDD', '#F984A1'];
var chartwidth = 0;
var thisrowpercentage = 0;
var thisrowwidth = 0;
var purejschartdata = "";
var baseFont = "";
var baseFontSize = "";
var baseFontBold = "0";
var baseFontItalic = "0";
var baseFontUnderline = "0";

var chartanswerrowcountnew = 0;
var adjustedchartwidth = 0;
var adjustedanswerpercentage = 0;

function renderChart(allanswers) {
    purejschartdata = "";
    chartwidth = parseInt($("#hdnWidth").val());

    baseFontBold = $("#hdnAnswerBold").val();
    baseFontUnderline = $("#hdnAnswerUL").val();
    baseFontItalic = $("#hdnAnswerItalic").val()
    adjustedchartwidth = Math.round(chartwidth * 0.8);
    adjustedanswerpercentage = chartwidth - adjustedchartwidth - 20;
    baseFont = $("#hdnAnswerFont").val();
    var answercolour = $("#hdnAnswerColor").val();
    if (baseFont == "") {
        baseFont = "Arial";
    }
    baseFontSize = $("#hdnAnswerFSize").val();

    chartanswerrowcountnew = allanswers.length;


    for (var row1 = 0; row1 < chartanswerrowcountnew; row1++) {

        thisrowpercentage = parseInt(allanswers[row1].answerpercentage);
        thisrowwidth = Math.round(thisrowpercentage * chartwidth / 100);

        thisanswer = allanswers[row1].answertext;

        thisanswer = escape(thisanswer);
        while (thisanswer.indexOf("%22") >= 0) {
            thisanswer = thisanswer.replace("%22", "");
        }
        while (thisanswer.indexOf("%22") >= 0) {
            thisanswer = thisanswer.replace("%22", "");
        }

        while (thisanswer.indexOf("%3C") >= 0) {
            console.log(thisanswer);
            thisanswer = thisanswer.replace("%3C", "&lt;");
        }

        while (thisanswer.indexOf("%3E") >= 0) {
            thisanswer = thisanswer.replace("%3E", "&gt;");
        }
        thisanswer = unescape(thisanswer);
        while (thisanswer.indexOf("'") >= 0) {
            thisanswer = thisanswer.replace("'", "`");
        }
        thisrowpercentage = parseInt(allanswers[row1].answerpercentage);
        thisrowwidth = Math.round(thisrowpercentage * adjustedchartwidth / 100);
        purejschartdata += "<div style='width:" + adjustedchartwidth + "px;text-align:left;font-size:" + baseFontSize + "px;font-weight:" + baseFontBold + ";font-family:" + baseFont + ";color:#" + answercolour + ";'>" + thisanswer + "</div>";
        purejschartdata += "<div style='width:" + adjustedchartwidth + "px;float:left;background-color:lightgray;'>";
        purejschartdata += "<div style='height:15px;width:0px;background-color:" + arrColours[row1] + ";' id='div" + row1 + "'></div>";
        purejschartdata += "</div>";
        purejschartdata += "<div style='float:right;width:" + adjustedanswerpercentage + "px;vertical-align:top;font-size:" + baseFontSize + "px;font-weight:" + baseFontBold + ";font-family:" + baseFont + ";color:#" + answercolour + ";'>" + thisrowpercentage + "%</div>";
        purejschartdata += "<div style='width:" + (adjustedchartwidth) + "px;height:26px;'></div>";
    }
    $('#purejschart').show();

    $('#purejschart').html(purejschartdata);
    console.log(chartanswerrowcountnew);
    for (var x = 0; x < chartanswerrowcountnew; x++) {
        //console.log(chartanswerrowcountnew);
        thisrowpercentage = parseInt(allanswers[x].answerpercentage);
        if (thisrowpercentage > 100) {
            thisrowpercentage = 100;
        }
        thisrowwidth = Math.round(thisrowpercentage * adjustedchartwidth / 100);
        console.log(thisrowpercentage.toString() + '--' + thisrowwidth.toString());
        $("#div" + x.toString()).animate({
            backgroundColor: "red",
            color: "#fff",
            width: thisrowwidth
        }, 1000);
    }

}