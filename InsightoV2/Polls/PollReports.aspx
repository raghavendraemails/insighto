﻿<%@ Page Title="" Language="C#" MasterPageFile="PollsMaster.master" AutoEventWireup="true" CodeFile="PollReports.aspx.cs" Inherits="Polls_new_design_PollReports" EnableViewState="false" %>
<%@ Register Src="UserControls/PollReportHeader.ascx" TagName="SurveyReportHeader"
    TagPrefix="uc" %>
 <%@ Register Src="~/UserControls/NotificationControl.ascx" TagName="NotificationControl"
    TagPrefix="uc1" %>
       

<asp:Content ID="surveyReportHeader" ContentPlaceHolderID="head" runat="Server">
    <script src="js/materialize.js"></script>
    <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
     <script src="../Charts/FusionCharts.js" type="text/javascript"></script>
      <script type="text/javascript" src="../FusionCharts/FusionCharts.js"></script>
    <script src="../Charts/FusionChartsExportComponent.js" type="text/javascript"></script>
    
    <uc:SurveyReportHeader ID="ucSurveyReportHeader" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

   
    <style>
        @import url(https://fonts.googleapis.com/css?family=Montserrat);

        *{
            font-family: 'Montserrat', sans-serif;
        }

    </style>

    <div >
        <!-- notification panel -->
        <uc1:NotificationControl ID="NotificationControl1" runat="server" />
        <!-- //notification panel -->
    </div>


    <script type="text/javascript">


        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_initializeRequest(prm_InitializeRequest);

        prm.add_endRequest(prm_EndRequest);



        // Called when async postback begins

        function prm_InitializeRequest(sender, args) {

            // get the divImage and set it to visible

            var panelProg = $get('divImage');

            panelProg.style.display = '';


            // reset label text

            var lbl = $get('<%= this.lblText.ClientID %>');

            lbl.innerHTML = '';



            // Disable button that caused a postback

            $get(args._postBackElement.id).disabled = true;

        }



        // Called when async postback ends

        function prm_EndRequest(sender, args) {

            // get the divImage and hide it again

            var panelProg = $get('divImage');

            panelProg.style.display = 'none';


            // Enable button that caused a postback

            $get(sender._postBackSettings.sourceElement.id).disabled = false;

        }


         </script>

    <script type="text/javascript">

        function upgradelicence() {
            OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
            return false;
        }
        function CheckAccess() {
            OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
            $('.rbtnData').find("input[value='rbtnData']").attr("checked", "checked");
            return false;
        }
        function alertmessage(s, e) {

            var index = rdgdisplayset2.GetSelectedIndex();

            if (index == 0 && document.getElementById("MainContent_SurveyReportMainContent_fea_exppt").value == "0") {
                //e.processOnServer=false;
                OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                e.processOnServer = false;

                //return false;
            }
            else if (index == 1 && document.getElementById("MainContent_SurveyReportMainContent_fea_expdf").value == "0") {
                //e.processOnServer=false;
                OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                e.processOnServer = false;
                //return false;
            }
            else if (index == 2 && document.getElementById("MainContent_SurveyReportMainContent_fea_exexc").value == "0") {
                OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                e.processOnServer = false;
                //return false;
            }
            else if (index == 3 && document.getElementById("MainContent_SurveyReportMainContent_fea_exwor").value == "0") {
                OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
                e.processOnServer = false;
                //return false;
            }
            if (licensetype != "FREE") {
                if (index != 2 && document.getElementById("MainContent_SurveyReportMainContent_fea_exexc").value == "1") {
                    var x = "This action exports only closed ended questions and responses. For exporting open ended responses, please use “Export to Excel” feature.\nExport of report will take several seconds. You will be notified once the report is generated.Please wait....";
                    jAlert(x);
                    //return true;
                }


                if (index == 2) {
                    var x = "Export of report will take several seconds. You will be notified once the report is generated.Please wait....";
                    jAlert(x);
                    //alert(x);
                    //return true;
                }
            }

        }
        function OnExportChanged(s, e) {


        }

        function showHide(EL, PM) {
            ELpntr = document.getElementById(EL);
            if (ELpntr.style.display == 'none') {
                document.getElementById(PM).innerHTML = "[-] ";
                ELpntr.style.display = 'block';


            }
            else {
                document.getElementById(PM).innerHTML = "[+]";
                ELpntr.style.display = 'none';

            }
        }

    </script>
    <script type='text/javascript'>

        function centerVertically(objectID) {


            var thisObj = document.getElementById(objectID);

            var height = (window.innerHeight) ? window.innerHeight : document.documentElement.clientHeight;
            var objectHeight = parseInt(thisObj.style.height);
            var newLocation = (height - objectHeight) / 2;


            thisObj.style.top = newLocation + 'px';




        }
        function centerHorizontally(objectID) {
            var thisObj = document.getElementById(objectID);
            var width = (window.innerWidth) ? window.innerWidth : document.body.clientWidth;
            var objectWidth = parseInt(thisObj.style.width);
            var newLocation = (width - objectWidth) / 2;
            thisObj.style.left = newLocation + 'px';
        }


        function GetRadioButtonListSelectedValue(radioButtonList) {

            for (var i = 0; i < radioButtonList.rows.length; ++i) {

                if (radioButtonList.rows[i].cells[0].firstChild.checked) {

                    radioButtonList.rows[i].cells[0].firstChild.checked = true;
                    document.getElementsByName('<%=HiddenField1.ClientID %>').value = radioButtonList.rows[i].cells[0].firstChild.value;

                    return true;
                }

            }

        }


</script>

	<script type="text/javascript">
	    //The Preloader is loaded here by checking the Initialization of JavaScript Class.
	  //  var chartnamerepeat = "chart,";
	    FusionCharts.addEventListener('initialized', function (e, a) {
	   
	        $('#chartContainerParent #floader').show();
	      
	    });

	    function showExportOptions() {
	        $("#MainContent_exportdiv").show();
	        //$("#MainContent_exportdiv").css("display","block");
	    }
        
	    //$(document).ready(function () {
	    //    console.log("here 1" + $("#MainContent_drpSource :selected").text());
	    //    console.log("here 2" + $("#MainContent_drpSource :selected").val());
	    //    console.log("here 1" + $("#MainContent_drpSource").text());
	    //    console.log("here 2" + $("#MainContent_drpSource").val());
	    //    $("#MainContent_drpSource").change(function () { console.log($("#MainContent_drpSource :selected").text()); });
	    //});
</script>



    <asp:ScriptManager ID="ScriptMgr1" runat="server" AsyncPostBackTimeout="100000">
    <Scripts>
   <asp:ScriptReference Path="../UpdatePanel/updatepanelhook.fusioncharts.js" />
    </Scripts>

    </asp:ScriptManager>


    <asp:Panel runat="server" ID="pnlLoading" Style="height: 450px; vertical-align: middle;"
        HorizontalAlign="Center">
        <div style="width: 100%; height: 220px;">
        </div>
      <br />
        <div style="width: 100%; height: 220px;">
        </div>
    </asp:Panel>

    <asp:UpdatePanel ID="updpan1" runat="server">

    <ContentTemplate>
    <asp:Label ID="lblText" runat="server" Text=""></asp:Label>
  <div id="divImage" style="display:none;position:absolute;margin:auto;z-index:1000;top:185px;left:600px" >

                   <table border="0px" width="100%" >
                   <tr>
                   <td align="center">
                   <table border="1px" style="border-bottom-color:Purple;border-left-color:Purple;border-right-color:Purple;border-top-color:Purple;border:1px;width:100px;color:Purple;">
                   <tr><td align="center">Processing.....</td></tr></table>
                    </td>
                   </tr>
                   </table>

                </div>

    <asp:Panel runat="server" ID="pnlMain" Style="display: none;">

        <input id="fea_exppt" type="hidden" value="1" runat="server" />
        <input id="fea_exexc" type="hidden" value="1" runat="server" />
        <input id="fea_exwor" type="hidden" value="1" runat="server" />
        <input id="fea_expdf" type="hidden" value="1" runat="server" />
        <input id="fea_datacharts" type="hidden" value="1" runat="server" />
        <input id="fea_charts" type="hidden" value="1" runat="server" />

        <div id="trgtDiv" runat="server" style="position: absolute;margin:auto;z-index:1000;top:100px;left:400px; width: 600px; height: 400px; background-color: white;"  visible="false">

          <div class="popupClosePanel">
            <a href="#"  alt="Close" title="Close" runat="server" id="lnkClose"
                onclick="closeModalSelf(false,'');">X</a>

    </div>
         <div class="popupContentPanel" >
        <table width="100%" align="center" border="0" cellspacing="0">
            <tr>
                <td align="left" valign="middle">
                    <%--<ucheader:Header ID="Header1" runat="server" />--%>
                </td>
            </tr>
            <tr>
                <td height="1" bgcolor="#994ea3">
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td>
                    <div class="informationPanelDefault">
                        <div>
                            <p>
                                <b>
                        <asp:Label ID="lblFeature" runat="server"
                                    Text="This is a paid feature!"
                                    meta:resourcekey="lblFeatureResource1"></asp:Label>
                                    <asp:Label ID="lblPreFeature" runat="server"
                                    Text="This feature is available only for Insighto Premium Single / premium Annual subscriptions."
                                    meta:resourcekey="lblPreFeatureResource1" Visible="false"></asp:Label></b></p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-left:40px;">
                    <h4>
                        <asp:Label ID="lblExperience" runat="server"
                            Text="Experience Insighto Paid plans and get access to these Powerful features:"
                            meta:resourcekey="lblExperienceResource1"></asp:Label>
                            <asp:Label ID="lblPreExperience" runat="server"
                            Text="Buy Insighto Premium and get access to these powerful features:"
                            meta:resourcekey="lblPreExperienceResource1" Visible="false"></asp:Label>
                        </h4>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td id="Td1">
                    <asp:Label ID="lblDetails" runat="server"
                        Text="<ul class='liInsighoIcon'> <li><b>Download results to PPT, Excel, Word or PDF</b></li> <li><b>Launch survey through insighto's email system</b></li>  <li><b>Password protected survey</b></li> <li><b>Schedule survey launch and close time and more </b></li></ul>"
                        meta:resourcekey="lblDetailsResource1"></asp:Label>
                        <asp:Label ID="lblPreDetails" runat="server"
                        Text="<ul class='liInsighoIcon'> <li><b>All features of Pro +</b></li> <li><b>Export raw data</b></li><li><b>View individual responses</b></li><li><b>Redirection of URL</b></li> <li><b>Remove Insighto branding on your survey end page </b></li></ul>"
                        meta:resourcekey="lblPreDetailsResource1" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="upgradeNowBtnPanel">
                        <ul class="HyperlinkBtn">
                            <li><a href="#" id="hyp_priclink" runat="server" onclick="opener.focus()">
                                <asp:Label ID="lblUpgrade" runat="server" Text="Upgrade Now!"
                                    meta:resourcekey="lblUpgradeResource1"></asp:Label></a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>

        </div>

<div class="row">
    <div class="col m12 s12" id="managepoll" runat="server" style="padding:10px;">
        <div class="col m4 s4" style="font-size:16px;font-weight:bold;"><span id="reportname" runat="server"></span></div>
        <div class="col m8 s8 right-align">
            <a id="lnkmanagepoll" runat="server" style="color: #8ABA3C;border: 1px solid #8ABA3C;padding: 4px 8px;border-radius: 7px;background: #fff;font-size:16px;font-weight:bold;">Manage Poll</a>
            &nbsp;
            <a id="closeicon" runat="server" style="color: #8ABA3C; border: 1px solid #8ABA3C; padding: 4px 8px; border-radius: 7px; background: #fff !important; font-size: 16px; font-weight: bold;cursor:pointer;" onclick="closeIframe()" >Close X</a>

        </div>
        
    </div>
</div>


            <div class="row">
        <div class="col m12 white nopad">
      <div class="col m12 s12">
        <div class="col m12 s12">
            <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblSuccMsg" runat="server"></asp:Label></div>
            </div>
            <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
                <div>
                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label></div>
            </div>
            <div class="quationtxtPanel" id="divSendReport" runat="server" visible="false">

            </div>
            <!-- survey question panel -->
            <div class="col m12 s12">
                <h5>
                   <asp:Label ID="lblQuestionHeader" runat="server"></asp:Label>
                </h5>
            </div>

     <div class="row">
        <div class="col s8 m8">
            <div class="col s12 m12 l3">
                <div class="card-panel teal col s12 m12 l12">
                    <div class="col l4 m12" style="padding-left:0px;">
                            <i class="large material-icons white-text" style="font-size:70px;">touch_app</i>
                        </div>
                    <div class="col m12 l8 right-align nopad">
                            <h5 class="white-text right-align">
                                <span id="votecount" runat="server">15</span>
                                <br />
                                <small class="white-text" style="font-size: 60%;"> votes</small>
                            </h5>
                        </div>
                </div>
            </div>
            <div class="col s12 m12 l3">
                <div class="card-panel teal col s12 m12 l12">
                    <div class="col l4 m12" style="padding-left:0px;">
                        <i class="large material-icons white-text" style="font-size:70px;">face</i>
                    </div>
                    <div class="col m12 l8 right-align nopad">
                        <h5 class="white-text">
                            <span id="visitcount" runat="server"></span>
                            <br/>
                            <small class="white-text" style="font-size: 60%;">  visits</small>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col s12 m12 l3">
                <div class="card-panel teal col s12 m12 l12">
                    <div class="col l4 m12" style="padding-left:0px;">
                        <i class="large material-icons white-text" style="font-size:70px;">perm_phone_msg</i>
                    </div>
                    <div class="col m12 l8 right-align nopad">
                        <h5 class="white-text">
                            <span id="leadscount" runat="server"></span>
                            <br/>
                            <small class="white-text" style="font-size: 60%;">  leads</small>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col s12 m12 l3">
                <div class="card-panel teal col s12 m12 l12">
                    <div class="col l4 m12" style="padding-left:0px;">
                        <i class="large material-icons white-text" style="font-size:70px;">open_in_new</i>
                    </div>
                    <div class="col l8 m12 right-align nopad">
                        <h5 class="white-text">
                            <span id="redirectscount" runat="server"></span>
                            <br/>
                            <small class="white-text" style="font-size: 60%;">  redirects</small>
                        </h5>
                    </div>
                </div>
            </div>

        </div>
    <div class="col s4 m4">
        <div class="col m12 s12" style="padding: 10px 0;display:none;" id="exportdiv" runat="server">

        <div class="col m8 s8  offset- offset-m1 nopad ">
<h6 class="left-align">Export data to</h6>
        </div>    
            
            <div class="col m12 s12">
            <div class="col m2 s2  offset-m1 center-align">
            <asp:LinkButton ID="lnkExcel" runat="server" Text="Excel" OnClick="lnkExcel_Click">
                <img src="images/microsoft-excel.png" class="responsive-img">
            </asp:LinkButton>
        </div>
        <div class="col m2 s2 center-align">
            <asp:LinkButton ID="lnkPDF" runat="server" Text="PDF" OnClick="lnkPDF_Click" >
                <img src="images/pdf.png" class="responsive-img">
            </asp:LinkButton>
        </div>
        <div class="col m2 s2 center-align">
            <asp:LinkButton ID="lnkWord" runat="server" Text="Word" OnClick="lnkWord_Click">
                <img src="images/doc.png" class="responsive-img">
            </asp:LinkButton>
        </div>
        <div class="col m2 s2 center-align">
            <asp:LinkButton ID="lnkPPT" runat="server" Text="PowerPoint" OnClick="lnkPPT_Click" >
                <img src="images/ppt.png" class="responsive-img">
            </asp:LinkButton>
        </div>
        <div class="col m2 s2 center-align">
            
            <asp:LinkButton ID="lbn_rawdataexport" runat="server" Text="PowerPoint" OnClick="lbn_rawdataexport_Click" >
                <img src="images/raw.png" class="responsive-img">
            </asp:LinkButton>
        </div>
        
        
        </div>
        
        </div>
        
         </div>

            </div>


            
            

        <div class="col m7 s7 offset-m2">
            

            
 
            <!-- survey left menu -->
<!--            <ul class="reportsMenu" style="height:40px;">
                <li><a href="#" class="activeLink" style="height:40px;">Reports</a></li>
            </ul>-->
          <div class="col s8 m8 offset-m4">
            <div class="col s12 m12" style="width:60%;">
                <div id="divDisplay" style="display: block;">
                    <div class="col m12 s12">
                      <div class="col m4 s4">
                    
                    </div>
                      <div class="col m4 s4">
                    
                    </div>
                      <div class="col m4 s4">
                    
                    </div>
                    </div>
                    
                    
                    
      <asp:RadioButtonList ID="rblgrp" runat="server"  onclick="return GetRadioButtonListSelectedValue(this);"  AutoPostBack="true"  RepeatDirection="Horizontal">
                  <asp:ListItem Value="Chart" Text="Chart"></asp:ListItem>
                  <asp:ListItem Value="Data" Text="Data"></asp:ListItem>
                  <asp:ListItem Value="DataChart" Text="Data+Chart"></asp:ListItem>
          </asp:RadioButtonList>

          <asp:HiddenField ID="HiddenField1" runat="server"  />

                </div>
            

            </div>
            </div>
             <div class="col s4 m4">
                          

             <div class="col s12 m12">
                 
        
            <!-- //survey left menu -->
        </div>
            </div>
        </div>
                <!-- //survey question panel -->

    <div class="row">
    <div class="col m8 s8 offset-m4" id="pollcontentdiv2">
        <div class="col m12 s12 center-align" >
            <div class="col m2 s2 input-field">Filter By:</div>
            <div class="col m4 s4 input-field">
                <asp:DropDownList ID="drpSource" runat="server" Visible="true" CssClass="browser-default green-text small" OnSelectedIndexChanged="drpSource_Click" AutoPostBack="true" Width="150" Height="30">
                     <asp:ListItem Text="" Value="">Source of votes</asp:ListItem>
                     <asp:ListItem Text="Location" Value="location">Location</asp:ListItem>
                     <asp:ListItem Text="Device" Value="device">Device</asp:ListItem>
                     <asp:ListItem Text="Page" Value="urlpage">Page</asp:ListItem>
                 </asp:DropDownList>
            </div>
            <div class="col m4 s4 input-field">
                <asp:DropDownList runat="server" ID="ddlcountry" CssClass="browser-default green-text small" Visible="false" onselectedindexchanged="ddlcountry_Changed" AutoPostBack="true"  Width="150" Height="30">
                </asp:DropDownList>
            </div>
        </div>        
    </div>
    <div class="col m8 s8 offset-m2">
        <div class="mCustomScrollbar row" id="pollcontentdiv1" runat="server" style="width:60%;">
       
                    <!--<asp:LinkButton ID="lnkallquestions" runat="server"  OnClick="lnkallquestions_Click" Visible="false" ></asp:LinkButton>
                <asp:Label ID="lblarrow" runat="server" Text="->" Visible="false" ></asp:Label>-->
           
                     <div id="chartContainerParent" style="margin-top:10px;" runat="server"><div id="floader"></div>
           <asp:Literal ID="LiteralQTInitial" runat="server"></asp:Literal>
                 <div class="clear"></div>
         </div>

                         <asp:Panel ID="rpnl_reportsdata" runat="server"   CssClass="padno">
                         </asp:Panel>
  </div>          </div>

            </div>

        </div>
        </div>
                </div>
        </div>

</asp:Panel>



</ContentTemplate>

<Triggers>
<asp:PostBackTrigger ControlID="lnkPPT" />
<asp:PostBackTrigger ControlID="lnkWord" />
<asp:PostBackTrigger ControlID="lnkPDF" />
<asp:PostBackTrigger ControlID="lnkExcel" />
<asp:PostBackTrigger ControlID="lbn_rawdataexport" />
<%--<asp:PostBackTrigger ControlID="lnklocation" />
<asp:PostBackTrigger ControlID="lnkDevice" />
<asp:PostBackTrigger ControlID="lnkPage" />--%>

</Triggers>
</asp:UpdatePanel>

    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <link href="css/jquery.mCustomScrollbar.min.css" type="text/css" rel="stylesheet" media="screen,projection" />

    <script type="text/javascript">
        function OpenNewTabPage() {

            window.open('OnlineReport.aspx?surveyid=' + surveyid, '', 'directories=no,location=no,resize=yes,addressbar=no,status=no,scrollbars=yes,titlebar=no,toolbar=no,fullscreen=yes,menubar=no');

        }
    </script>

    <script type="text/javascript" language="javascript">


        function pageLoad(sender, args) {
            //alert('hai');
            //  TogglePannel('<%= pnlMain.ClientID %>', true);
            TogglePannel('<%= pnlMain.ClientID %>', true);
            TogglePannel('<%= pnlLoading.ClientID %>', false);
        }

        function TogglePannel(target, OnOff) {
            obj = document.getElementById(target);

            if (obj) {
                obj.style.display = (OnOff) ? 'inline' : 'none';
            }
        }
        function CheckPermissions() {
            OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');
            return false;
        }

    </script>
    <script type="text/javascript">

        //Confirmation box sample

        function ConfirmationBox() {

            var result = confirm("Are you sure you want to continue?");

            if (result == true) {

                return true;

            }

            else {

                var lbl = document.getElementById('lbltxt');

                lbl.innerHTML = "";

                return false;

            }

        }

        $(document).ready(function ($) {
            $('a[rel*=framebox]').click(function (e) {
                e.preventDefault();
                ApplyFrameBox($(this));
            });
            window.parent.document.getElementById("preloader2").style.display = "none";
            //$.mCustomScrollbar.defaults.scrollButtons.enable = true; //enable scrolling buttons by default
            //$.mCustomScrollbar.defaults.axis = "yx"; //enable 2 axis scrollbars by default


            $("#MainContent_pollcontentdiv1").mCustomScrollbar({
                scrollButtons: { enable: true },
                theme: "rounded-dark",
                autoExpandScrollbar: false,
                autoHideScrollbar: false,
                scrollbarPosition: "inside",
                alwaysShowScrollbar: 2
            });
            //$("#pollcontentdiv1").css("height", "250px !important");
            //$("#pollcontentdiv1").css("max-height", "250px !important");
            //var windowheight = parseInt(window.innerHeight);
            //console.log(window.innerHeight);
            //$("#pollcontentdiv1").css("height", "" + parseInt(10 * window.innerHeight / 100) + "px");
            //$("#pollcontentdiv1").css("max-height", "" + parseInt(10 * window.innerHeight / 100) + "px");
            load();
            //$(document).css("overflow", "auto !important");
        });
        function closeIframe() {
            //window.parent.document.getElementsByClassName("fancybox-iframe").close();
            window.parent.closeFancybox();
        }
        function load() {

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(
                function () {
                    var thiswindowheight = parseInt(window.innerHeight);

                    if (thiswindowheight < 500) {
                        thiswindowheight = 700;
                    }
                    console.log(thiswindowheight);

                    $("#MainContent_pollcontentdiv1").mCustomScrollbar({
                        scrollButtons: { enable: true },
                        theme: "rounded-dark",
                        autoExpandScrollbar: false,
                        autoHideScrollbar: false,
                        scrollbarPosition: "inside"
                    });
                    $("#MainContent_pollcontentdiv1").css("height", "" + parseInt(50 * thiswindowheight / 100) + "px");
                    $("#MainContent_pollcontentdiv1").css("max-height", "" + parseInt(50* thiswindowheight / 100) + "px");
                });
        }
</script>
    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-55613473-1', 'auto');
            ga('send', 'pageview');

    </script>
       <script type="text/javascript" src="/Scripts/facebox.js?v=0.0.7.4"></script>
       <script type="text/javascript" src="/Scripts/common-functions.js?v=0.0.7.4"></script>
</asp:Content>