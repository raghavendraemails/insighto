﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using iTextSharp.text;
using iTextSharp.text.pdf;
using sharp = iTextSharp.text;
using Insighto.Business.ValueObjects;
using System.Text;
using System.Linq;
using System.Security.Cryptography;
using OfficeOpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Data.SqlClient;

public partial class Polls_new_design_PollReports : System.Web.UI.Page
{
    int QuestID = -1, Mode = 0, check_flag = 0, SMode = 0, QType_count = 0;
    public int surveyID = 0;
    public string strcountryname = "";
    public string strdevicename = "";
    public string strmobiledevice = "";
    public string strmobdevos = "";
    public string strurlpage = "";
    public string strExport = "";


    SurveyCore surcore = new SurveyCore();
    SurveyCoreVoice surcorevoice = new SurveyCoreVoice();
    PPTXopenXML pptxml = new PPTXopenXML();

    Hashtable ht = new Hashtable();


    public string jsstrchartname;
    public string jsstrchartnameppt;
    public string SAVE_PATH;
    public string SAVE_PATHRET;
    public int fcdelaytime;
    public string exportchartfilehandler;
    public string strlicensetype;


    #region "Property"
    /// <summary>
    /// This property used to set query string.
    /// </summary>
    public string Key
    {
        get
        {
            return HttpUtility.UrlEncode(QueryStringHelper.GetString("Key"));
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        exportchartfilehandler = "";
        if (!IsPostBack)
        {

            Session["defaultchart"] = null;
            Session["SOVotes"] = null;

        }
        string Path = ConfigurationManager.AppSettings["RootPath"].ToString();

        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        if (loggedInUserInfo != null)
        {
        }
        else
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "RedirectToLogin" + DateTime.Now.Millisecond.ToString(), "<script>window.parent.location.href = '/errorpage.aspx?iserror=true';</script>");
        }
        try
        {
            strlicensetype = loggedInUserInfo.LicenseType;
            SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();
            SAVE_PATHRET = ConfigurationSettings.AppSettings["downloadchartsret"].ToString();

            Page.ClientScript.RegisterStartupScript(this.GetType(), "alertScript", "$(document).ready(function(){GetRadioButtonListSelectedValue('" + rblgrp.ClientID + "');});", true);
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);


                if (ht != null && ht.Count > 0)
                {

                    if (ht.Contains("Mode"))
                        Mode = Convert.ToInt32(ht["Mode"]);
                    if (ht.Contains("PollId"))
                        surveyID = Convert.ToInt32(ht["PollId"]);
                    if (ht.Contains("Country"))
                        strcountryname = Convert.ToString(ht["Country"]);
                    if (ht.Contains("Device"))
                        strdevicename = Convert.ToString(ht["Device"]);
                    if (ht.Contains("mobiledevice"))
                        strmobiledevice = Convert.ToString(ht["mobiledevice"]);
                    if (ht.Contains("mobileop"))
                        strmobdevos = Convert.ToString(ht["mobileop"]);
                    if (ht.Contains("urlpage"))
                        strurlpage = Convert.ToString(ht["urlpage"]);
                    if (ht.Contains("Export"))
                        strExport = Convert.ToString(ht["Export"]);
                    // Export=Country

                    if (ht.Contains("Date_Start") && (ht.Contains("Date_End")))
                    {
                        if (Convert.ToDateTime(ht["Date_Start"]) > Convert.ToDateTime("1/1/0001 12:00:00 AM") && Convert.ToDateTime(ht["Date_End"]) > Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                        {
                            Page.RegisterStartupScript("Indshow", "<script>showHide('divDate','spanDate')</script>");
                        }
                    }

                }
                SetTopBar(surveyID);

                if (Session["defaultchart"] == null)
                {

                    if ((strcountryname == "") || (strdevicename == "") || (strurlpage == ""))
                    {
                        rblgrp.SelectedValue = "Chart";
                    }

                }

                if ((strcountryname != "") || (strdevicename != "") || (strurlpage != ""))
                {
                    if (Mode == 0)
                    {
                        rblgrp.SelectedValue = "DataChart";
                    }
                    else if (Mode == 1)
                    {
                        rblgrp.SelectedValue = "Data";
                    }
                    else if (Mode == 2)
                    {
                        rblgrp.SelectedValue = "Chart";
                    }
                }
                if (rblgrp.SelectedValue == "DataChart")
                {
                    Mode = 0;
                }
                else if (rblgrp.SelectedValue == "Data")
                {
                    Mode = 1;
                }
                else if (rblgrp.SelectedValue == "Chart")
                {
                    Mode = 2;
                }


                DataSet dsAnalytics = getpollSurveyAnalytics(surveyID);

                visitcount.InnerHtml = dsAnalytics.Tables[1].Rows[0][0].ToString();
                votecount.InnerHtml = dsAnalytics.Tables[2].Rows[0][0].ToString();
                leadscount.InnerHtml = dsAnalytics.Tables[3].Rows[0][0].ToString();
                redirectscount.InnerHtml = dsAnalytics.Tables[4].Rows[0][0].ToString();

                string selectedvalue = "";
                selectedvalue = drpSource.SelectedValue;
                if (selectedvalue != "")
                {
                    ddlcountry.Visible = true;
                }
                else
                {
                    ddlcountry.Visible = false;
                }
                if ((selectedvalue == "location") || (strcountryname != ""))
                {
                    if (selectedvalue == Session["SOVotes"].ToString())
                    {
                        if (ddlcountry.SelectedIndex != -1)
                        {
                            strcountryname = ddlcountry.SelectedItem.Value.ToString();
                        }
                    }
                    BindReportLocation();
                }
                else if ((selectedvalue == "device") || (strdevicename != ""))
                {
                    if (selectedvalue == Session["SOVotes"].ToString())
                    {
                        if (ddlcountry.SelectedIndex != -1)
                        {
                            strdevicename = ddlcountry.SelectedItem.Value.ToString();
                        }
                    }
                    BindReportDevice();
                }
                else if ((selectedvalue == "urlpage") || (strurlpage != ""))
                {
                    if (selectedvalue == Session["SOVotes"].ToString())
                    {
                        if (ddlcountry.SelectedIndex != -1)
                        {
                            strurlpage = ddlcountry.SelectedItem.Value.ToString();
                        }
                    }
                    BindReportPage();
                }
                else
                {
                    BindReportgrid();
                }
                Session["SOVotes"] = selectedvalue;


                if (strcountryname != "")
                {
                    lnkallquestions.Visible = true;
                    lnkallquestions.Text = "All Locations";
                    lblarrow.Visible = true;
                    ddlcountry.Visible = true;

                    //ddlcountry.SelectedValue = strcountryname;


                }

                if (strdevicename != "")
                {
                    lnkallquestions.Visible = true;
                    lnkallquestions.Text = "All Devices";
                    lblarrow.Visible = true;
                    ddlcountry.Visible = true;

                    //ddlcountry.SelectedValue = strdevicename;


                }

                if (strurlpage != "")
                {
                    lnkallquestions.Visible = true;
                    lnkallquestions.Text = "All Pages";
                    lblarrow.Visible = true;
                    ddlcountry.Visible = true;

                    //ddlcountry.SelectedValue = strurlpage;


                }

                if (strExport != "")
                {
                    if (strExport == "Country")
                    {
                        ExportVotesToExcelSheet(strExport, "");
                    }
                    else if (strExport == "State")
                    {
                        ExportVotesToExcelSheet(strExport, strcountryname);
                    }
                    else if (strExport == "device")
                    {
                        ExportVotesToExcelSheet(strExport, "");
                    }
                    else if (strExport == "OS")
                    {
                        ExportVotesToExcelSheet(strExport, strdevicename);
                    }
                    else if (strExport == "mobiledevice")
                    {
                        ExportVotesToExcelSheet(strExport, strdevicename);
                    }
                    else if (strExport == "mobdevos")
                    {
                        ExportVotesToExcelSheet(strExport, strdevicename);
                    }
                    else if (strExport == "mobile")
                    {
                        ExportVotesToExcelSheet(strExport, strdevicename);
                    }
                    else if (strExport == "urlpage")
                    {
                        if (strurlpage != "")
                        {
                            ExportVotesToExcelSheet(strExport, strurlpage);
                        }
                        else
                        {
                            ExportVotesToExcelSheet(strExport, "");
                        }
                    }
                }


                DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);
                int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());
                DataRow[] drrespondentscnt = dsRespondentLimit.Tables[1].Select("fk_pollid=" + surveyID);

                int respondentscnt;
                if (drrespondentscnt.Length > 0)
                {
                    respondentscnt = Convert.ToInt32(drrespondentscnt[0][0]);
                }
                else
                {
                    respondentscnt = 0;
                }

                if (respondentscnt > responselimit)
                {
                    NotificationControl.AddSuccessMessagemaxAllowedResponseLimit(string.Format("This Poll has received " + respondentscnt + " votes.  But due to the limitation on the total number of responses of your Plan, you are not able to view all of them. To view all votes please upgrade your account.", ""), false);
                }

            }
            exportdiv.Style.Add("display", "block");
            exportdiv.Style.Add("visibility", "visible");
            
            if (!IsPostBack)
            {
                string exportChartJS = "$(document).ready(function(){FusionCharts.addEventListener('rendered', function(e, a) { FusionCharts(e.sender.id).exportChart(); });});";
                exportChartJS = "FusionCharts.addEventListener('rendered', function(e, a) { FusionCharts(e.sender.id).exportChart(); });";
        
                if (drpSource.SelectedValue != "")
                {
                }
                else
                {
                    exportdiv.Style.Add("display", "block");
                    exportdiv.Style.Add("visibility", "visible");
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "exportChartJS2" + DateTime.Now.Millisecond.ToString(), "<script>" + exportChartJS + "</script>");
                }
            }
            else
            {
                if (drpSource.SelectedValue != "")
                {
                    exportdiv.Style.Add("display", "block");
                    exportdiv.Style.Add("visibility", "visible");
                    Session["defaultchart"] = "notdefault";

                }
            }
        }
        catch (Exception)
        {
        }
    }

    private void SetTopBar(int pollid)
    {
        SurveyCore appsurveycore = new SurveyCore();

        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        DataSet dsRespondentLimit = appsurveycore.getPollRespondentLimit(loggedInUserInfo.UserId);


        int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

        DataSet dspollinfo = appsurveycore.getpollinfo(pollid, responselimit);


        string strEditSurvey = "" + EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollid);
        string strLaunchSurvey = "" + EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollid + "&launch=launchedpoll");

        reportname.InnerHtml = dspollinfo.Tables[0].Rows[0]["name"].ToString();
        if (dspollinfo.Tables[0].Rows[0]["status"].ToString() == "Closed")
        {
            reportname.Style.Add("background-color", "#BD3A47");
            managepoll.Style.Add("background-color", "#BD3A47");
            reportname.Style.Add("color", "white");
            managepoll.Style.Add("color", "white");
            lnkmanagepoll.Style.Add("color", "#BD3A47");
            lnkmanagepoll.Style.Add("border-color", "#BD3A47");
            lnkmanagepoll.InnerHtml = "View Poll";
            if (dspollinfo.Tables[0].Rows[0]["launchstatus"].ToString() == "0")

            closeicon.Style.Add("background-color", "#BD3A47");
            closeicon.Style.Add("color", "#BD3A47");
            closeicon.Style.Add("border-color", "#BD3A47");

            if (dspollinfo.Tables[0].Rows[0]["launchstatus"].ToString() == "0")
            {
                lnkmanagepoll.Attributes.Add("href", strEditSurvey);
            }
            else
            {
                lnkmanagepoll.Attributes.Add("href", strLaunchSurvey);
            }
        }
        else if (dspollinfo.Tables[0].Rows[0]["status"].ToString() == "Active")
        {
            //li1.Style.Add("background-color", "#BD3A47");
            reportname.Style.Add("background-color", "#8ABA3C");
            managepoll.Style.Add("background-color", "#8ABA3C");
            reportname.Style.Add("color", "white");
            managepoll.Style.Add("color", "white");
            //lnkmanagepoll.Style.Add("color", "white");
            lnkmanagepoll.Style.Add("color", "#8ABA3C");
            lnkmanagepoll.Style.Add("border-color", "#8ABA3C");
            lnkmanagepoll.Attributes.Add("href", strLaunchSurvey);
        }
        else if (dspollinfo.Tables[0].Rows[0]["status"].ToString() == "Draft")
        {
            //li1.Style.Add("background-color", "#319AFA");
            reportname.Style.Add("background-color", "#319AFA");
            managepoll.Style.Add("background-color", "#319AFA");
            reportname.Style.Add("color", "white");
            managepoll.Style.Add("color", "white");
            lnkmanagepoll.Style.Add("color", "#319AFA");
            lnkmanagepoll.Style.Add("border-color", "#319AFA");
            lnkmanagepoll.Attributes.Add("href", strEditSurvey);

            closeicon.Style.Add("background-color", "#319AFA");
            closeicon.Style.Add("color", "#319AFA");
            closeicon.Style.Add("border-color", "#319AFA");
        }
    }

    private void BindReportgrid()
    {
        StringBuilder strXML = new StringBuilder();

        exportchartfilehandler = ConfigurationSettings.AppSettings["exportchartfile"].ToString();

        Session["SOVotes"] = "";
        strcountryname = "";

        SurveyCore appsurveycore = new SurveyCore();
        //   surveyID = 1;



        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        DataSet dsRespondentLimit = appsurveycore.getPollRespondentLimit(loggedInUserInfo.UserId);

        jsstrchartname = "pptimg" + surveyID.ToString();
        int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

        DataSet dspollinfo = appsurveycore.getpollinfo(surveyID, responselimit);
        if (!IsPostBack)
        {
            strXML.Append("<chart caption='' subcaption='' yaxisname='NoOfRespondents'  xaxisname='Answers' alternatevgridcolor='AFD8F8' basefontcolor='114B78' tooltipbordercolor='114B78' tooltipbgcolor='E7EFF6' useroundedges='1'  showborder='1' bgcolor='FFFFFF' numbersuffix='%' font='Arial, Helvetica, sans-serif'  exportEnabled='1' exportAtClient='1' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportCallback='showExportOptions' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'   exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>");
        }
        else
        {
            strXML.Append("<chart caption='' subcaption='' yaxisname='NoOfRespondents'  xaxisname='Answers' alternatevgridcolor='AFD8F8' basefontcolor='114B78' tooltipbordercolor='114B78' tooltipbgcolor='E7EFF6' useroundedges='1'  showborder='1' bgcolor='FFFFFF' numbersuffix='%' font='Arial, Helvetica, sans-serif'  exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'   exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>");
        }


        if (dspollinfo.Tables.Count > 0)
        {
            if (dspollinfo.Tables[0].Rows.Count > 0)
            {
                lblQuestionHeader.Text = dspollinfo.Tables[0].Rows[0][1].ToString();
            }
            HtmlTable qt3tableques = new HtmlTable();
            //chartContainerParent.Controls.Clear();
            if ((rblgrp.SelectedValue == "Chart"))
            {
                HtmlTableRow htrcharttemp = new HtmlTableRow();
                HtmlTableCell htccharttemp = new HtmlTableCell();

                System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp = new System.Web.UI.WebControls.Label();

                string strunique1 = GetUniqueKey();
                string uniquestr = strunique1 + surveyID.ToString();
                //jsstrchartname = uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");
                jsstrchartname = "pptimg" + surveyID.ToString();
                string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString())).Replace("\r", "").Replace("\n", "");
                //exportchartfilehandler = "http://localhost:88/FcExporter.aspx";
                // int intchartrowlength;
                DataRow[] drchart = dspollinfo.Tables[2].Select("pollid = " + surveyID);
                for (int ic = 0; ic < drchart.Length; ic++)
                {

                    strXML.AppendFormat("<set label='{0}' value='{1}' />", System.Web.HttpUtility.HtmlEncode(StripTagsRegex(drchart[ic][1].ToString())).Replace("\r", "").Replace("\n", ""), drchart[ic][3].ToString());
                }
                strXML.Append("</chart>");

                string charttypename = "../Charts/Bar2D.swf";
                string chartwidth = "598";
                string chartheight = "300";

                LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart(charttypename, "", strXML.ToString(), strunique1 + uniquestr, chartwidth, chartheight, false, true, false);
                lblqt3cellpertotalcharttemp.Text = LiteralQTInitial.Text;
                htccharttemp.Controls.Add(lblqt3cellpertotalcharttemp);
                htrcharttemp.Cells.Add(htccharttemp);
                qt3tableques.Rows.Add(htrcharttemp);
                if (Session["defaultchart"] != null)
                {
                    chartContainerParent.Controls.Add(qt3tableques);
                }
            }
            if ((rblgrp.SelectedValue == "Data"))
            {
                HtmlTableRow htrgx = new HtmlTableRow();
                HtmlTableCell htcgx = new HtmlTableCell();
                GridView grdvall = new GridView();
                grdvall.ID = "gridQT" + surveyID;
                grdvall.Width = 598;
                grdvall.RowStyle.Height = 25;
                grdvall.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                grdvall.GridLines = 0;
                grdvall.RowDataBound += new GridViewRowEventHandler(grdvall_RowDataBound);

                DataTable dtg = new DataTable();
                DataRow dtgr;
                DataRow dtgresp;

                string lnkother = "";
                string lnkothercount = "";

                dtg.Columns.Add("Answer options");
                dtg.Columns.Add("Percentage");
                dtg.Columns.Add("Count");

                DataRow[] drdata = dspollinfo.Tables[2].Select("pollid = " + surveyID);

                for (int irg1 = 0; irg1 < drdata.Length; irg1++)
                {
                    dtgr = dtg.NewRow();
                    dtgr[0] = drdata[irg1][1].ToString();
                    dtgr[1] = drdata[irg1][3].ToString() + "%";
                    dtgr[2] = drdata[irg1][2].ToString();

                    dtg.Rows.Add(dtgr);
                }

                dtgresp = dtg.NewRow();
                dtgresp[0] = "# of Respondents";
                dtgresp[1] = " ";
                dtgresp[2] = dspollinfo.Tables[4].Rows[0][0].ToString();
                dtg.Rows.Add(dtgresp);

                grdvall.DataSource = dtg;
                grdvall.DataBind();

                grdvall.HeaderRow.Cells[0].Height = 25;
                grdvall.HeaderRow.Cells[1].Height = 25;
                grdvall.HeaderRow.Cells[2].Height = 25;
                grdvall.HeaderRow.Cells[0].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[0].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[1].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[1].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[2].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[2].ForeColor = System.Drawing.Color.White;

                htcgx.Controls.Add(grdvall);
                htrgx.Cells.Add(htcgx);
                qt3tableques.Rows.Add(htrgx);

                if ((dspollinfo.Tables[0].Rows[0][5].ToString() == "True") || (dspollinfo.Tables[0].Rows[0][5].ToString() == "1"))
                {
                    HtmlTableRow hrtext = new HtmlTableRow();
                    hrtext.BorderColor = "white";
                    hrtext.Height = "30px";
                    HtmlTableCell hctext = new HtmlTableCell();

                    HyperLink hql = new HyperLink();
                    hql.Font.Bold = true;
                    hql.Font.Underline = true;
                    hql.ForeColor = System.Drawing.Color.Purple;
                    if (dspollinfo.Tables[5].Rows.Count == 1)
                    {
                        hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                    }
                    else if (dspollinfo.Tables[5].Rows.Count > 1)
                    {
                        hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                    }
                    else {
                        hql.Text = "No comments";
                    }

                    string NavUrl = "PollId=" + surveyID;
                    if (dspollinfo.Tables[5].Rows.Count > 0)
                    {
                        hql.Attributes.Add("href", "javascript:;");
                        hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                        hql.Attributes.Add("id", "commentsbox");
                        hql.Attributes.Add("h", "600");
                        hql.Attributes.Add("w", "600");
                        hql.Attributes.Add("scrolling", "true");
                    }
                    else {
                        hql.Attributes.Add("href", "javascript:;");
                    }
                    hctext.Controls.Add(hql);
                    hrtext.Cells.Add(hctext);
                    qt3tableques.Rows.Add(hrtext);
                }

                rpnl_reportsdata.Controls.Add(qt3tableques);
                chartContainerParent.InnerText = "";
            }
            if (rblgrp.SelectedValue == "DataChart")
            {
                chartContainerParent.InnerText = "";

                HtmlTableRow htrcharttempDC = new HtmlTableRow();
                HtmlTableCell htccharttempDC = new HtmlTableCell();

                System.Web.UI.WebControls.Label lblqt3cellpertotalcharttempDC = new System.Web.UI.WebControls.Label();

                string strunique1 = GetUniqueKey();
                string uniquestr = "datachart" + surveyID.ToString();
                jsstrchartname = uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");

                string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString())).Replace("\r", "").Replace("\n", "");

                //strXML.Append("<chart caption='' subcaption='' yaxisname='NoOfRespondents'  xaxisname='Answers' alternatevgridcolor='AFD8F8' basefontcolor='114B78' tooltipbordercolor='114B78' tooltipbgcolor='E7EFF6' useroundedges='1'  showborder='1' bgcolor='FFFFFF' numbersuffix='%' font='Arial, Helvetica, sans-serif' exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'   exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>");


                DataRow[] drchart = dspollinfo.Tables[2].Select("pollid = " + surveyID);
                for (int ic = 0; ic < drchart.Length; ic++)
                {

                    strXML.AppendFormat("<set label='{0}' value='{1}' />", System.Web.HttpUtility.HtmlEncode(StripTagsRegex(drchart[ic][1].ToString())).Replace("\r", "").Replace("\n", ""), drchart[ic][3].ToString());
                }
                strXML.Append("</chart>");

                string charttypename = "../Charts/Bar2D.swf";
                string chartwidth = "598";
                string chartheight = "300";

                LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart(charttypename, "", strXML.ToString(), strunique1 + uniquestr, chartwidth, chartheight, false, true, false);
                lblqt3cellpertotalcharttempDC.Text = LiteralQTInitial.Text;
                htccharttempDC.Controls.Add(lblqt3cellpertotalcharttempDC);
                htrcharttempDC.Cells.Add(htccharttempDC);
                qt3tableques.Rows.Add(htrcharttempDC);


                HtmlTableRow htrgxDC = new HtmlTableRow();
                HtmlTableCell htcgxDC = new HtmlTableCell();
                GridView grdvallDC = new GridView();
                grdvallDC.ID = "gridQT" + surveyID;
                grdvallDC.Width = 598;
                grdvallDC.RowStyle.Height = 25;
                grdvallDC.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                grdvallDC.GridLines = 0;
                grdvallDC.RowDataBound += new GridViewRowEventHandler(grdvall_RowDataBound);

                DataTable dtgDC = new DataTable();
                DataRow dtgrDC;
                DataRow dtgrespDC;

                string lnkother = "";
                string lnkothercount = "";

                dtgDC.Columns.Add("Answer options");
                dtgDC.Columns.Add("Percentage");
                dtgDC.Columns.Add("Count");

                DataRow[] drdatachart = dspollinfo.Tables[2].Select("pollid = " + surveyID);

                for (int irg1 = 0; irg1 < drdatachart.Length; irg1++)
                {
                    dtgrDC = dtgDC.NewRow();
                    dtgrDC[0] = drdatachart[irg1][1].ToString();
                    dtgrDC[1] = drdatachart[irg1][3].ToString() + "%";
                    dtgrDC[2] = drdatachart[irg1][2].ToString();

                    dtgDC.Rows.Add(dtgrDC);
                }

                dtgrespDC = dtgDC.NewRow();
                dtgrespDC[0] = "# of Respondents";
                dtgrespDC[1] = " ";
                dtgrespDC[2] = dspollinfo.Tables[4].Rows[0][0].ToString();
                dtgDC.Rows.Add(dtgrespDC);

                grdvallDC.DataSource = dtgDC;
                grdvallDC.DataBind();

                grdvallDC.HeaderRow.Cells[0].Height = 25;
                grdvallDC.HeaderRow.Cells[1].Height = 25;
                grdvallDC.HeaderRow.Cells[2].Height = 25;
                grdvallDC.HeaderRow.Cells[0].BackColor = System.Drawing.Color.Purple;
                grdvallDC.HeaderRow.Cells[0].ForeColor = System.Drawing.Color.White;
                grdvallDC.HeaderRow.Cells[1].BackColor = System.Drawing.Color.Purple;
                grdvallDC.HeaderRow.Cells[1].ForeColor = System.Drawing.Color.White;
                grdvallDC.HeaderRow.Cells[2].BackColor = System.Drawing.Color.Purple;
                grdvallDC.HeaderRow.Cells[2].ForeColor = System.Drawing.Color.White;

                htcgxDC.Controls.Add(grdvallDC);
                htrgxDC.Cells.Add(htcgxDC);
                qt3tableques.Rows.Add(htrgxDC);

                if ((dspollinfo.Tables[0].Rows[0][5].ToString() == "True") || (dspollinfo.Tables[0].Rows[0][5].ToString() == "1"))
                {
                    HtmlTableRow hrtext = new HtmlTableRow();
                    hrtext.BorderColor = "white";
                    hrtext.Height = "30px";
                    HtmlTableCell hctext = new HtmlTableCell();

                    HyperLink hql = new HyperLink();
                    hql.Font.Bold = true;
                    hql.Font.Underline = true;
                    hql.ForeColor = System.Drawing.Color.Purple;
                    if (dspollinfo.Tables[5].Rows.Count == 1)
                    {
                        hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                    }
                    else if (dspollinfo.Tables[5].Rows.Count > 1)
                    {
                        hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                    }
                    else
                    {
                        hql.Text = "No comments";
                    }

                    string NavUrl = "PollId=" + surveyID;
                    if (dspollinfo.Tables[5].Rows.Count > 0)
                    {
                        hql.Attributes.Add("href", "javascript:;");
                        hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                        hql.Attributes.Add("id", "commentsbox");
                        hql.Attributes.Add("h", "600");
                        hql.Attributes.Add("w", "600");
                        hql.Attributes.Add("scrolling", "true");
                    }
                    else
                    {
                        hql.Attributes.Add("href", "javascript:;");
                    }
                    hctext.Controls.Add(hql);
                    hrtext.Cells.Add(hctext);
                    qt3tableques.Rows.Add(hrtext);
                }


                chartContainerParent.Controls.Add(qt3tableques);


            }
        }
        Session["defaultchart"] = "notdefault";
    }

    private void BindReportLocation()
    {
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "showExportOptionsForLocation" + DateTime.Now.Ticks.ToString(), "<script>showExportOptions();</script>");
        chartContainerParent.Controls.Clear();

        if (Mode == 0)
        {
            rblgrp.Items[0].Selected = true;
        }
        else if (Mode == 1)
        {
            rblgrp.Items[1].Selected = true;
        }
        else if (Mode == 2)
        {
            rblgrp.Items[2].Selected = true;
        }

        SurveyCore appsurveycore = new SurveyCore();
        // surveyID = 1;
        DataSet dspollinfo = null;

        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        DataSet dsRespondentLimit = appsurveycore.getPollRespondentLimit(loggedInUserInfo.UserId);

        int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

        dspollinfo = appsurveycore.getpollcountryinfo(surveyID, strcountryname, responselimit);

        ddlcountry.DataSource = dspollinfo.Tables[5];
        ddlcountry.DataTextField = "country";
        ddlcountry.DataValueField = "country";
        ddlcountry.DataBind();

        ddlcountry.Items.Insert(0, new System.Web.UI.WebControls.ListItem("All locations", ""));
        if ((strcountryname == "") || (strcountryname == "All Locations"))
        {
            dspollinfo = appsurveycore.getpollinfo(surveyID, responselimit);
            //ddlcountry.SelectedValue = strcountryname;
        }
        else {
        }
        ddlcountry.SelectedValue = strcountryname;
        if (dspollinfo.Tables.Count > 0)
        {
            if (dspollinfo.Tables[0].Rows.Count > 0)
            {
                lblQuestionHeader.Text = dspollinfo.Tables[0].Rows[0][1].ToString();
            }
            HtmlTable qt3tableques = new HtmlTable();
            //qt3tableques.Width = "640";
            qt3tableques.CellPadding = 1;
            qt3tableques.CellSpacing = 0;
            //  qt3tableques.Border = 1;

            if ((rblgrp.SelectedValue == "Chart"))
            {


                HtmlTableRow htrcharttemp = new HtmlTableRow();
                HtmlTableCell htccharttemp = new HtmlTableCell();

                System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp = new System.Web.UI.WebControls.Label();

                string strunique1 = GetUniqueKey();
                string uniquestr = surveyID.ToString();
                jsstrchartname = uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");

                StringBuilder strXML = new StringBuilder();
                string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString())).Replace("\r", "").Replace("\n", "");

                strXML.Append("<chart caption='' subcaption='' yaxisname='NoOfRespondents'  xaxisname='Answers' alternatevgridcolor='AFD8F8' basefontcolor='114B78' tooltipbordercolor='114B78' tooltipbgcolor='E7EFF6' useroundedges='1'  showborder='1' bgcolor='FFFFFF' numbersuffix='%' font='Arial, Helvetica, sans-serif' exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>");


                DataRow[] drchart = dspollinfo.Tables[2].Select("pollid = " + surveyID);
                for (int ic = 0; ic < drchart.Length; ic++)
                {
                    strXML.AppendFormat("<set label='{0}' value='{1}' />", System.Web.HttpUtility.HtmlEncode(StripTagsRegex(drchart[ic][1].ToString())).Replace("\r", "").Replace("\n", ""), drchart[ic][3].ToString());
                }
                strXML.Append("</chart>");

                string charttypename = "../Charts/Bar2D.swf";
                string chartwidth = "598";
                string chartheight = "300";

                LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart(charttypename, "", strXML.ToString(), strunique1 + uniquestr, chartwidth, chartheight, false, true, false);
                lblqt3cellpertotalcharttemp.Text = LiteralQTInitial.Text;
                htccharttemp.Controls.Add(lblqt3cellpertotalcharttemp);
                htrcharttemp.Cells.Add(htccharttemp);
                qt3tableques.Rows.Add(htrcharttemp);

                if (Session["defaultchart"] != null)
                {
                    chartContainerParent.Controls.Add(qt3tableques);
                }

            }
            else if (rblgrp.SelectedValue == "DataChart")
            {
                chartContainerParent.InnerText = "";
                HtmlTableRow htrcharttemp = new HtmlTableRow();
                HtmlTableCell htccharttemp = new HtmlTableCell();

                System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp = new System.Web.UI.WebControls.Label();

                string strunique1 = GetUniqueKey();
                string uniquestr = surveyID.ToString();
                jsstrchartname = uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");

                StringBuilder strXML = new StringBuilder();
                string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString())).Replace("\r", "").Replace("\n", "");

                strXML.Append("<chart caption='' subcaption='' yaxisname='NoOfRespondents'  xaxisname='Answers' alternatevgridcolor='AFD8F8' basefontcolor='114B78' tooltipbordercolor='114B78' tooltipbgcolor='E7EFF6' useroundedges='1'  showborder='1' bgcolor='FFFFFF' numbersuffix='%' font='Arial, Helvetica, sans-serif' exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'   exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>");


                DataRow[] drchart = dspollinfo.Tables[2].Select("pollid = " + surveyID);
                for (int ic = 0; ic < drchart.Length; ic++)
                {
                    strXML.AppendFormat("<set label='{0}' value='{1}' />", System.Web.HttpUtility.HtmlEncode(StripTagsRegex(drchart[ic][1].ToString())).Replace("\r", "").Replace("\n", ""), drchart[ic][3].ToString());
                }
                strXML.Append("</chart>");

                string charttypename = "../Charts/Bar2D.swf";
                string chartwidth = "598";
                string chartheight = "300";

                LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart(charttypename, "", strXML.ToString(), strunique1 + uniquestr, chartwidth, chartheight, false, true, false);
                lblqt3cellpertotalcharttemp.Text = LiteralQTInitial.Text;
                htccharttemp.Controls.Add(lblqt3cellpertotalcharttemp);
                htrcharttemp.Cells.Add(htccharttemp);
                qt3tableques.Rows.Add(htrcharttemp);


                HtmlTableRow htrgx = new HtmlTableRow();
                HtmlTableCell htcgx = new HtmlTableCell();
                GridView grdvall = new GridView();
                grdvall.ID = "gridQT" + surveyID;
                grdvall.Width = 598;
                grdvall.RowStyle.Height = 25;
                grdvall.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                grdvall.GridLines = 0;
                grdvall.RowDataBound += new GridViewRowEventHandler(grdvall_RowDataBound);

                DataTable dtg = new DataTable();
                DataRow dtgr;
                DataRow dtgresp;

                dtg.Columns.Add("Answer options");
                dtg.Columns.Add("Percentage");
                dtg.Columns.Add("Count");

                DataRow[] drdatachartloc = dspollinfo.Tables[2].Select("pollid = " + surveyID);

                for (int irg1 = 0; irg1 < drdatachartloc.Length; irg1++)
                {
                    dtgr = dtg.NewRow();

                    dtgr[0] = drdatachartloc[irg1][1].ToString();
                    dtgr[1] = drdatachartloc[irg1][3].ToString() + "%";
                    dtgr[2] = drdatachartloc[irg1][2].ToString();

                    dtg.Rows.Add(dtgr);
                }

                dtgresp = dtg.NewRow();
                dtgresp[0] = "# of Respondents";
                dtgresp[1] = " ";
                dtgresp[2] = dspollinfo.Tables[4].Rows[0][0].ToString();
                dtg.Rows.Add(dtgresp);

                grdvall.DataSource = dtg;
                grdvall.DataBind();


                grdvall.HeaderRow.Cells[0].Height = 25;
                grdvall.HeaderRow.Cells[1].Height = 25;
                grdvall.HeaderRow.Cells[2].Height = 25;
                grdvall.HeaderRow.Cells[0].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[0].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[1].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[1].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[2].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[2].ForeColor = System.Drawing.Color.White;

                htcgx.Controls.Add(grdvall);
                htrgx.Cells.Add(htcgx);
                qt3tableques.Rows.Add(htrgx);

                if ((dspollinfo.Tables[0].Rows[0][5].ToString() == "True") || (dspollinfo.Tables[0].Rows[0][5].ToString() == "1"))
                {
                    HtmlTableRow hrtext = new HtmlTableRow();
                    hrtext.BorderColor = "white";
                    hrtext.Height = "30px";
                    HtmlTableCell hctext = new HtmlTableCell();

                    HyperLink hql = new HyperLink();
                    hql.Font.Bold = true;
                    hql.Font.Underline = true;
                    hql.ForeColor = System.Drawing.Color.Purple;

                    string NavUrl = "PollId=" + surveyID;
                    if (strcountryname != "")
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }
                    else
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }
                    hctext.Controls.Add(hql);
                    hrtext.Cells.Add(hctext);
                    qt3tableques.Rows.Add(hrtext);
                }

                chartContainerParent.Controls.Add(qt3tableques);


            }
            if ((rblgrp.SelectedValue == "Data"))
            {
                HtmlTableRow htrgx = new HtmlTableRow();
                HtmlTableCell htcgx = new HtmlTableCell();
                GridView grdvall = new GridView();
                grdvall.ID = "gridQT" + surveyID;
                grdvall.Width = 598;
                grdvall.RowStyle.Height = 25;
                grdvall.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                grdvall.GridLines = 0;
                grdvall.RowDataBound += new GridViewRowEventHandler(grdvall_RowDataBound);

                DataTable dtg = new DataTable();
                DataRow dtgr;
                DataRow dtgresp;

                string lnkother = "";
                string lnkothercount = "";

                dtg.Columns.Add("Answer options");
                dtg.Columns.Add("Percentage");
                dtg.Columns.Add("Count");



                DataRow[] drdataloc = dspollinfo.Tables[2].Select("pollid = " + surveyID);

                for (int irg1 = 0; irg1 < drdataloc.Length; irg1++)
                {
                    dtgr = dtg.NewRow();

                    dtgr[0] = drdataloc[irg1][1].ToString();
                    dtgr[1] = drdataloc[irg1][3].ToString() + "%";
                    dtgr[2] = drdataloc[irg1][2].ToString();

                    dtg.Rows.Add(dtgr);
                }

                dtgresp = dtg.NewRow();
                dtgresp[0] = "# of Respondents";
                dtgresp[1] = " ";
                dtgresp[2] = dspollinfo.Tables[4].Rows[0][0].ToString();
                dtg.Rows.Add(dtgresp);

                grdvall.DataSource = dtg;
                grdvall.DataBind();


                grdvall.HeaderRow.Cells[0].Height = 25;
                grdvall.HeaderRow.Cells[1].Height = 25;
                grdvall.HeaderRow.Cells[2].Height = 25;
                grdvall.HeaderRow.Cells[0].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[0].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[1].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[1].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[2].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[2].ForeColor = System.Drawing.Color.White;

                htcgx.Controls.Add(grdvall);
                htrgx.Cells.Add(htcgx);
                qt3tableques.Rows.Add(htrgx);


                string NavUrl = "PollId=" + surveyID;

                if ((dspollinfo.Tables[0].Rows[0][5].ToString() == "True") || (dspollinfo.Tables[0].Rows[0][5].ToString() == "1"))
                {
                    HtmlTableRow hrtext = new HtmlTableRow();
                    hrtext.BorderColor = "white";
                    hrtext.Height = "30px";
                    HtmlTableCell hctext = new HtmlTableCell();

                    HyperLink hql = new HyperLink();
                    hql.Font.Bold = true;
                    hql.Font.Underline = true;
                    hql.ForeColor = System.Drawing.Color.Purple;
                    if (strcountryname != "")
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }
                    else
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }

                    hctext.Controls.Add(hql);
                    hrtext.Cells.Add(hctext);
                    qt3tableques.Rows.Add(hrtext);
                }

                rpnl_reportsdata.Controls.Add(qt3tableques);
                chartContainerParent.InnerText = "";
            }

            HtmlTableRow htrexport = new HtmlTableRow();
            HtmlTableCell htcexport = new HtmlTableCell();
            htcexport.Align = "Right";
            HtmlAnchor aexport = new HtmlAnchor();
            aexport.InnerHtml = "Export";
            aexport.Style.Add("font-size", "12px");
            aexport.Style.Add("font-weight", "bold");
            aexport.Style.Add("font-underline", "true");
            aexport.Style.Add("ForeColor", "Purple");

            if (Session["SOVotes"] == "location")
            {
                aexport.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Export=Country&Mode=" + Mode);
            }
            else
            {
                aexport.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Export=State&Mode=" + Mode + "&Country=" + strcountryname);
            }
            //htcexport.Controls.Add(aexport);
            htrexport.Cells.Add(htcexport);
            qt3tableques.Rows.Add(htrexport);


            HtmlTableRow htrgxcntry = new HtmlTableRow();
            HtmlTableCell htcgxcntry = new HtmlTableCell();

            HtmlTable qt3tableques1 = new HtmlTable();
            qt3tableques1.Width = "598";
            qt3tableques1.CellPadding = 1;
            qt3tableques1.CellSpacing = 0;
            // qt3tableques1.Border = 1;

            HtmlTableRow htrgxlocheader = new HtmlTableRow();
            htrgxlocheader.Height = "25px";
            htrgxlocheader.BgColor = "Purple";

            HtmlTableCell htcgxloch1 = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh1 = new System.Web.UI.WebControls.Label();
            if (Session["SOVotes"] == "location")
            {
                lblqt3cellh1.Text = "Country";
            }
            else
            {
                lblqt3cellh1.Text = "State";
            }
            lblqt3cellh1.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh1.ForeColor = System.Drawing.Color.White;
            htcgxloch1.Controls.Add(lblqt3cellh1);
            HtmlTableCell htcgxloch2 = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh2 = new System.Web.UI.WebControls.Label();
            lblqt3cellh2.Text = "Views";
            lblqt3cellh2.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh2.ForeColor = System.Drawing.Color.White;
            htcgxloch2.Controls.Add(lblqt3cellh2);
            HtmlTableCell htcgxloch3 = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh3 = new System.Web.UI.WebControls.Label();
            lblqt3cellh3.Text = "% of Views";
            lblqt3cellh3.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh3.ForeColor = System.Drawing.Color.White;
            htcgxloch3.Controls.Add(lblqt3cellh3);
            HtmlTableCell htcgxloch4 = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh4 = new System.Web.UI.WebControls.Label();
            lblqt3cellh4.Text = "Votes";
            lblqt3cellh4.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh4.ForeColor = System.Drawing.Color.White;
            htcgxloch4.Controls.Add(lblqt3cellh4);
            HtmlTableCell htcgxloch5 = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh5 = new System.Web.UI.WebControls.Label();
            lblqt3cellh5.Text = "% of Votes";
            lblqt3cellh5.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh5.ForeColor = System.Drawing.Color.White;
            htcgxloch5.Controls.Add(lblqt3cellh5);

            htrgxlocheader.Cells.Add(htcgxloch1);
            htrgxlocheader.Cells.Add(htcgxloch2);
            htrgxlocheader.Cells.Add(htcgxloch3);
            htrgxlocheader.Cells.Add(htcgxloch4);
            htrgxlocheader.Cells.Add(htcgxloch5);
            qt3tableques1.Rows.Add(htrgxlocheader);


            HtmlTableRow htrgxloc1;
            for (int irg1 = 0; irg1 < dspollinfo.Tables[3].Rows.Count; irg1++)
            {
                htrgxloc1 = new HtmlTableRow();

                if (irg1 % 2 == 0)
                {
                    htrgxloc1.BgColor = "#FFFFFF";
                }
                else
                {
                    htrgxloc1.BgColor = "#F4F4F4";
                }


                htrgxloc1.Height = "25px";
                HtmlTableCell htcgxloc1 = new HtmlTableCell();
                htcgxloc1.Width = "128";
                HtmlTableCell htcgxloc2 = new HtmlTableCell();
                htcgxloc2.Width = "128";
                HtmlTableCell htcgxloc3 = new HtmlTableCell();
                htcgxloc3.Width = "128";
                HtmlTableCell htcgxloc4 = new HtmlTableCell();
                htcgxloc4.Width = "128";
                HtmlTableCell htcgxloc5 = new HtmlTableCell();
                htcgxloc5.Width = "128";

                HtmlAnchor a = new HtmlAnchor();
                a.InnerHtml = dspollinfo.Tables[3].Rows[irg1][0].ToString();
                if (Session["SOVotes"] == "location")
                {
                    a.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Country=" + dspollinfo.Tables[3].Rows[irg1][0].ToString() + "&Mode=" + Mode);
                }
                htcgxloc1.Controls.Add(a);

                System.Web.UI.WebControls.Label lblqt3cell1 = new System.Web.UI.WebControls.Label();
                lblqt3cell1.Text = dspollinfo.Tables[3].Rows[irg1][1].ToString();
                htcgxloc2.Controls.Add(lblqt3cell1);

                System.Web.UI.WebControls.Label lblqt3cell2 = new System.Web.UI.WebControls.Label();
                lblqt3cell2.Text = dspollinfo.Tables[3].Rows[irg1][2].ToString();
                htcgxloc3.Controls.Add(lblqt3cell2);

                System.Web.UI.WebControls.Label lblqt3cell3 = new System.Web.UI.WebControls.Label();
                lblqt3cell3.Text = dspollinfo.Tables[3].Rows[irg1][3].ToString();
                htcgxloc4.Controls.Add(lblqt3cell3);

                System.Web.UI.WebControls.Label lblqt3cell4 = new System.Web.UI.WebControls.Label();
                lblqt3cell4.Text = dspollinfo.Tables[3].Rows[irg1][4].ToString();
                htcgxloc5.Controls.Add(lblqt3cell4);

                htrgxloc1.Cells.Add(htcgxloc1);
                htrgxloc1.Cells.Add(htcgxloc2);
                htrgxloc1.Cells.Add(htcgxloc3);
                htrgxloc1.Cells.Add(htcgxloc4);
                htrgxloc1.Cells.Add(htcgxloc5);
                qt3tableques1.Rows.Add(htrgxloc1);

                //  rpnl_reportsdata.Controls.Add(qt3tableques1);

            }

            htcgxcntry.Controls.Add(qt3tableques1);
            htrgxcntry.Cells.Add(htcgxcntry);

            qt3tableques.Rows.Add(htrgxcntry);

        }
        Session["defaultchart"] = "notdefault";
    }


    private void BindReportDevice()
    {
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "showExportOptionsForDevice", "<script>showExportOptions();</script>");

        if (Mode == 0)
        {
            rblgrp.Items[0].Selected = true;
        }
        else if (Mode == 1)
        {
            rblgrp.Items[1].Selected = true;
        }
        else if (Mode == 2)
        {
            rblgrp.Items[2].Selected = true;
        }

        SurveyCore appsurveycore = new SurveyCore();
        //surveyID = 1;
        DataSet dspollinfo = null;

        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        DataSet dsRespondentLimit = appsurveycore.getPollRespondentLimit(loggedInUserInfo.UserId);
        int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

        if (strmobiledevice != "")
        {
            dspollinfo = appsurveycore.getpollmobiledeviceinfo(surveyID, strdevicename, responselimit);
            ddlcountry.DataSource = dspollinfo.Tables[5];
            ddlcountry.DataTextField = "device";
            ddlcountry.DataValueField = "device";
            ddlcountry.DataBind();
        }
        else if (strmobdevos != "")
        {

            dspollinfo = appsurveycore.getpollmobileosinfo(surveyID, strdevicename, responselimit);
            ddlcountry.DataSource = dspollinfo.Tables[5];
            ddlcountry.DataTextField = "device";
            ddlcountry.DataValueField = "device";
            ddlcountry.DataBind();
        }
        else
        {
            dspollinfo = appsurveycore.getpolldeviceinfo(surveyID, strdevicename, responselimit);
            ddlcountry.DataSource = dspollinfo.Tables[5];
            ddlcountry.DataTextField = "device";
            ddlcountry.DataValueField = "device";
            ddlcountry.DataBind();
        }
        //ddlcountry.Items.Insert(0, "All devices");
        ddlcountry.Items.Insert(0, new System.Web.UI.WebControls.ListItem("All Devices", ""));
        if (strdevicename == "")
        {
            dspollinfo = appsurveycore.getpolldevice(surveyID, responselimit);
        }
        ddlcountry.SelectedValue = strdevicename;
        if (dspollinfo.Tables.Count > 0)
        {
            if (dspollinfo.Tables[0].Rows.Count > 0)
            {
                lblQuestionHeader.Text = dspollinfo.Tables[0].Rows[0][1].ToString();
            }
            HtmlTable qt3tableques = new HtmlTable();
            //  qt3tableques.Width = "640";
            qt3tableques.CellPadding = 1;
            qt3tableques.CellSpacing = 0;
            //qt3tableques.Border = 1;

            if ((rblgrp.SelectedValue == "Chart"))
            {

                HtmlTableRow htrcharttemp = new HtmlTableRow();
                HtmlTableCell htccharttemp = new HtmlTableCell();

                System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp = new System.Web.UI.WebControls.Label();

                string strunique1 = GetUniqueKey();
                string uniquestr = surveyID.ToString();
                jsstrchartname = uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");

                StringBuilder strXML = new StringBuilder();
                string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString())).Replace("\r", "").Replace("\n", "");

                strXML.Append("<chart caption='' subcaption='' yaxisname='NoOfRespondents'  xaxisname='Answers' alternatevgridcolor='AFD8F8' basefontcolor='114B78' tooltipbordercolor='114B78' tooltipbgcolor='E7EFF6'  useroundedges='1' showborder='1' bgcolor='FFFFFF' numbersuffix='%' font='Arial, Helvetica, sans-serif' exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>");


                DataRow[] drchart = dspollinfo.Tables[2].Select("pollid = " + surveyID);
                for (int ic = 0; ic < drchart.Length; ic++)
                {
                    strXML.AppendFormat("<set label='{0}' value='{1}' />", System.Web.HttpUtility.HtmlEncode(StripTagsRegex(drchart[ic][1].ToString())).Replace("\r", "").Replace("\n", ""), drchart[ic][3].ToString());
                }
                strXML.Append("</chart>");

                string charttypename = "../Charts/Bar2D.swf";
                string chartwidth = "598";
                string chartheight = "300";

                LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart(charttypename, "", strXML.ToString(), strunique1 + uniquestr, chartwidth, chartheight, false, true, false);
                lblqt3cellpertotalcharttemp.Text = LiteralQTInitial.Text;
                htccharttemp.Controls.Add(lblqt3cellpertotalcharttemp);
                htrcharttemp.Cells.Add(htccharttemp);
                qt3tableques.Rows.Add(htrcharttemp);

                if (Session["defaultchart"] != null)
                {
                    chartContainerParent.Controls.Add(qt3tableques);
                }

            }
            else if (rblgrp.SelectedValue == "DataChart")
            {
                chartContainerParent.InnerText = "";
                HtmlTableRow htrcharttemp = new HtmlTableRow();
                HtmlTableCell htccharttemp = new HtmlTableCell();

                System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp = new System.Web.UI.WebControls.Label();

                string strunique1 = GetUniqueKey();
                string uniquestr = surveyID.ToString();
                jsstrchartname = uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");

                StringBuilder strXML = new StringBuilder();
                string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString())).Replace("\r", "").Replace("\n", "");

                strXML.Append("<chart caption='' subcaption='' yaxisname='NoOfRespondents'  xaxisname='Answers' alternatevgridcolor='AFD8F8' basefontcolor='114B78' tooltipbordercolor='114B78' tooltipbgcolor='E7EFF6' useroundedges='1'  showborder='1' bgcolor='FFFFFF' numbersuffix='%' font='Arial, Helvetica, sans-serif' exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>");


                DataRow[] drchart = dspollinfo.Tables[2].Select("pollid = " + surveyID);
                for (int ic = 0; ic < drchart.Length; ic++)
                {
                    strXML.AppendFormat("<set label='{0}' value='{1}' />", System.Web.HttpUtility.HtmlEncode(StripTagsRegex(drchart[ic][1].ToString())).Replace("\r", "").Replace("\n", ""), drchart[ic][3].ToString());
                }
                strXML.Append("</chart>");

                string charttypename = "../Charts/Bar2D.swf";
                string chartwidth = "598";
                string chartheight = "300";

                LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart(charttypename, "", strXML.ToString(), strunique1 + uniquestr, chartwidth, chartheight, false, true, false);
                lblqt3cellpertotalcharttemp.Text = LiteralQTInitial.Text;
                htccharttemp.Controls.Add(lblqt3cellpertotalcharttemp);
                htrcharttemp.Cells.Add(htccharttemp);
                qt3tableques.Rows.Add(htrcharttemp);


                HtmlTableRow htrgx = new HtmlTableRow();
                HtmlTableCell htcgx = new HtmlTableCell();
                GridView grdvall = new GridView();
                grdvall.ID = "gridQT" + surveyID;
                grdvall.Width = 598;
                grdvall.RowStyle.Height = 25;
                grdvall.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                grdvall.GridLines = 0;
                grdvall.RowDataBound += new GridViewRowEventHandler(grdvall_RowDataBound);

                DataTable dtg = new DataTable();
                DataRow dtgr;
                DataRow dtgresp;

                dtg.Columns.Add("Answer options");
                dtg.Columns.Add("Percentage");
                dtg.Columns.Add("Count");

                DataRow[] drdatachart = dspollinfo.Tables[2].Select("pollid = " + surveyID);

                for (int irg1 = 0; irg1 < drdatachart.Length; irg1++)
                {
                    dtgr = dtg.NewRow();

                    dtgr[0] = drdatachart[irg1][1].ToString();
                    dtgr[1] = drdatachart[irg1][3].ToString() + "%";
                    dtgr[2] = drdatachart[irg1][2].ToString();

                    dtg.Rows.Add(dtgr);
                }

                dtgresp = dtg.NewRow();
                dtgresp[0] = "# of Respondents";
                dtgresp[1] = " ";
                dtgresp[2] = dspollinfo.Tables[4].Rows[0][0].ToString();
                dtg.Rows.Add(dtgresp);

                grdvall.DataSource = dtg;
                grdvall.DataBind();


                grdvall.HeaderRow.Cells[0].Height = 25;
                grdvall.HeaderRow.Cells[1].Height = 25;
                grdvall.HeaderRow.Cells[2].Height = 25;
                grdvall.HeaderRow.Cells[0].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[0].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[1].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[1].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[2].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[2].ForeColor = System.Drawing.Color.White;

                htcgx.Controls.Add(grdvall);
                htrgx.Cells.Add(htcgx);
                qt3tableques.Rows.Add(htrgx);

                if ((dspollinfo.Tables[0].Rows[0][5].ToString() == "True") || (dspollinfo.Tables[0].Rows[0][5].ToString() == "1"))
                {
                    HtmlTableRow hrtext = new HtmlTableRow();
                    hrtext.BorderColor = "white";
                    hrtext.Height = "30px";
                    HtmlTableCell hctext = new HtmlTableCell();

                    HyperLink hql = new HyperLink();
                    hql.Font.Bold = true;
                    hql.Font.Underline = true;
                    hql.ForeColor = System.Drawing.Color.Purple;

                    string NavUrl = "PollId=" + surveyID;

                    if (strdevicename != "")
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }
                    else
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }

                    hctext.Controls.Add(hql);
                    hrtext.Cells.Add(hctext);
                    qt3tableques.Rows.Add(hrtext);
                }
                chartContainerParent.Controls.Add(qt3tableques);


            }
            if ((rblgrp.SelectedValue == "Data"))
            {
                HtmlTableRow htrgx = new HtmlTableRow();
                HtmlTableCell htcgx = new HtmlTableCell();
                GridView grdvall = new GridView();
                grdvall.ID = "gridQT" + surveyID;
                grdvall.Width = 598;
                grdvall.RowStyle.Height = 25;
                grdvall.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                grdvall.GridLines = 0;
                grdvall.RowDataBound += new GridViewRowEventHandler(grdvall_RowDataBound);

                DataTable dtg = new DataTable();
                DataRow dtgr;
                DataRow dtgresp;

                string lnkother = "";
                string lnkothercount = "";

                dtg.Columns.Add("Answer options");
                dtg.Columns.Add("Percentage");
                dtg.Columns.Add("Count");

                DataRow[] drdata = dspollinfo.Tables[2].Select("pollid = " + surveyID);

                for (int irg1 = 0; irg1 < drdata.Length; irg1++)
                {
                    dtgr = dtg.NewRow();

                    dtgr[0] = drdata[irg1][1].ToString();
                    dtgr[1] = drdata[irg1][3].ToString() + "%";
                    dtgr[2] = drdata[irg1][2].ToString();

                    dtg.Rows.Add(dtgr);
                }

                dtgresp = dtg.NewRow();
                dtgresp[0] = "# of Respondents";
                dtgresp[1] = " ";
                dtgresp[2] = dspollinfo.Tables[4].Rows[0][0].ToString();
                dtg.Rows.Add(dtgresp);

                grdvall.DataSource = dtg;
                grdvall.DataBind();

                grdvall.HeaderRow.Cells[0].Height = 25;
                grdvall.HeaderRow.Cells[1].Height = 25;
                grdvall.HeaderRow.Cells[2].Height = 25;
                grdvall.HeaderRow.Cells[0].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[0].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[1].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[1].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[2].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[2].ForeColor = System.Drawing.Color.White;

                htcgx.Controls.Add(grdvall);
                htrgx.Cells.Add(htcgx);
                qt3tableques.Rows.Add(htrgx);

                if ((dspollinfo.Tables[0].Rows[0][5].ToString() == "True") || (dspollinfo.Tables[0].Rows[0][5].ToString() == "1"))
                {
                    HtmlTableRow hrtext = new HtmlTableRow();
                    hrtext.BorderColor = "white";
                    hrtext.Height = "30px";
                    HtmlTableCell hctext = new HtmlTableCell();

                    HyperLink hql = new HyperLink();
                    hql.Font.Bold = true;
                    hql.Font.Underline = true;
                    hql.ForeColor = System.Drawing.Color.Purple;
                    string NavUrl = "PollId=" + surveyID;
                    if (strdevicename != "")
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }
                    else
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }

                    hctext.Controls.Add(hql);
                    hrtext.Cells.Add(hctext);
                    qt3tableques.Rows.Add(hrtext);
                }

                rpnl_reportsdata.Controls.Add(qt3tableques);
                chartContainerParent.InnerText = "";
            }

            if (strdevicename == "mobile")
            {
                HtmlTableRow htrbandlnk = new HtmlTableRow();
                HtmlTableCell htcdevicelnk = new HtmlTableCell();
                HtmlAnchor a = new HtmlAnchor();
                a.InnerHtml = "Brand";
                a.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Device=mobile" + "&Mode=" + Mode + "&brand=mobilebrand");
                htcdevicelnk.Controls.Add(a);

                HtmlTableCell htcblnk = new HtmlTableCell();
                HtmlAnchor a1 = new HtmlAnchor();
                a1.InnerHtml = "Device";
                a1.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Device=mobile" + "&Mode=" + Mode + "&mobiledevice=mobiledevice");
                htcdevicelnk.Controls.Add(a1);

                HtmlTableCell htcoplnk = new HtmlTableCell();
                HtmlAnchor a2 = new HtmlAnchor();
                a2.InnerHtml = "Operating System";
                a2.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Device=mobile" + "&Mode=" + Mode + "&mobileop=mobdevos");
                htcdevicelnk.Controls.Add(a2);

                htrbandlnk.Cells.Add(htcdevicelnk);
                htrbandlnk.Cells.Add(htcblnk);
                qt3tableques.Rows.Add(htrbandlnk);
            }


            HtmlTableRow htrexport = new HtmlTableRow();
            HtmlTableCell htcexport = new HtmlTableCell();
            htcexport.Align = "Right";
            HtmlAnchor aexportdevice = new HtmlAnchor();
            aexportdevice.Style.Add("font-size", "12px");
            aexportdevice.Style.Add("font-weight", "bold");
            aexportdevice.Style.Add("font-underline", "true");
            aexportdevice.Style.Add("ForeColor", "Purple");
            aexportdevice.InnerHtml = "Export";

            if (Session["SOVotes"] == "device")
            {
                aexportdevice.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Export=device&Mode=" + Mode);
            }
            else
            {
                if (strdevicename == "desktop")
                {
                    aexportdevice.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Export=OS&Mode=" + Mode + "&Device=" + strdevicename);
                }
                else if (strdevicename == "mobile")
                {
                    if (strmobiledevice == "mobiledevice")
                    {
                        aexportdevice.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Export=mobiledevice&Mode=" + Mode + "&Device=" + strdevicename + "&mobiledevice=" + strmobiledevice);
                    }
                    else if (strmobdevos == "mobdevos")
                    {
                        aexportdevice.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Export=mobdevos&Mode=" + Mode + "&Device=" + strdevicename + "&mobdevos=" + strmobdevos);
                    }
                    else
                    {
                        aexportdevice.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Export=mobile&Mode=" + Mode + "&Device=" + strdevicename);
                    }
                }

            }
            //htcexport.Controls.Add(aexportdevice);
            htrexport.Cells.Add(htcexport);
            qt3tableques.Rows.Add(htrexport);

            HtmlTableRow htrgxcntry = new HtmlTableRow();
            HtmlTableCell htcgxcntry = new HtmlTableCell();

            HtmlTable qt3tableques1 = new HtmlTable();
            qt3tableques1.Width = "598";
            qt3tableques1.CellPadding = 1;
            qt3tableques1.CellSpacing = 0;
            //qt3tableques1.Border = 1;

            HtmlTableRow htrgxlocheader = new HtmlTableRow();
            htrgxlocheader.Height = "25px";
            htrgxlocheader.BgColor = "Purple";

            HtmlTableCell htcgxloch1 = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh1 = new System.Web.UI.WebControls.Label();
            if (Session["SOVotes"] == "device")
            {
                lblqt3cellh1.Text = "Device";
            }
            else
            {
                if (strdevicename == "desktop")
                {
                    lblqt3cellh1.Text = "Operating System";
                }
                else if (strdevicename == "mobile")
                {
                    if (strmobiledevice == "mobiledevice")
                    {
                        lblqt3cellh1.Text = "Device";
                    }
                    else if (strmobdevos == "mobdevos")
                    {
                        lblqt3cellh1.Text = "Operating System";
                    }
                    else
                    {
                        lblqt3cellh1.Text = "Brand";
                    }
                }
            }
            lblqt3cellh1.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh1.ForeColor = System.Drawing.Color.White;
            htcgxloch1.Controls.Add(lblqt3cellh1);
            HtmlTableCell htcgxloch2 = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh2 = new System.Web.UI.WebControls.Label();
            lblqt3cellh2.Text = "Views";
            lblqt3cellh2.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh2.ForeColor = System.Drawing.Color.White;
            htcgxloch2.Controls.Add(lblqt3cellh2);
            HtmlTableCell htcgxloch3 = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh3 = new System.Web.UI.WebControls.Label();
            lblqt3cellh3.Text = "% of Views";
            lblqt3cellh3.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh3.ForeColor = System.Drawing.Color.White;
            htcgxloch3.Controls.Add(lblqt3cellh3);
            HtmlTableCell htcgxloch4 = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh4 = new System.Web.UI.WebControls.Label();
            lblqt3cellh4.Text = "Votes";
            lblqt3cellh4.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh4.ForeColor = System.Drawing.Color.White;
            htcgxloch4.Controls.Add(lblqt3cellh4);
            HtmlTableCell htcgxloch5 = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh5 = new System.Web.UI.WebControls.Label();
            lblqt3cellh5.Text = "% of Votes";
            lblqt3cellh5.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh5.ForeColor = System.Drawing.Color.White;
            htcgxloch5.Controls.Add(lblqt3cellh5);

            htrgxlocheader.Cells.Add(htcgxloch1);
            htrgxlocheader.Cells.Add(htcgxloch2);
            htrgxlocheader.Cells.Add(htcgxloch3);
            htrgxlocheader.Cells.Add(htcgxloch4);
            htrgxlocheader.Cells.Add(htcgxloch5);
            qt3tableques1.Rows.Add(htrgxlocheader);



            HtmlTableRow htrgxloc1;
            for (int irg1 = 0; irg1 < dspollinfo.Tables[3].Rows.Count; irg1++)
            {
                htrgxloc1 = new HtmlTableRow();
                htrgxloc1.Height = "25px";
                if (irg1 % 2 == 0)
                {
                    htrgxloc1.BgColor = "#FFFFFF";
                }
                else
                {
                    htrgxloc1.BgColor = "#F4F4F4";
                }
                HtmlTableCell htcgxloc1 = new HtmlTableCell();
                htcgxloc1.Width = "128";
                HtmlTableCell htcgxloc2 = new HtmlTableCell();
                htcgxloc2.Width = "128";
                HtmlTableCell htcgxloc3 = new HtmlTableCell();
                htcgxloc3.Width = "128";
                HtmlTableCell htcgxloc4 = new HtmlTableCell();
                htcgxloc4.Width = "128";
                HtmlTableCell htcgxloc5 = new HtmlTableCell();
                htcgxloc5.Width = "128";

                HtmlAnchor a = new HtmlAnchor();
                a.InnerHtml = dspollinfo.Tables[3].Rows[irg1][0].ToString();
                if ((lblqt3cellh1.Text == "Operating System") || (strmobiledevice == "mobiledevice") || (lblqt3cellh1.Text == "Brand"))
                {
                }
                else
                {
                    a.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Device=" + dspollinfo.Tables[3].Rows[irg1][0].ToString() + "&Mode=" + Mode);
                }
                htcgxloc1.Controls.Add(a);

                System.Web.UI.WebControls.Label lblqt3cell1 = new System.Web.UI.WebControls.Label();
                lblqt3cell1.Text = dspollinfo.Tables[3].Rows[irg1][1].ToString();
                htcgxloc2.Controls.Add(lblqt3cell1);

                System.Web.UI.WebControls.Label lblqt3cell2 = new System.Web.UI.WebControls.Label();
                lblqt3cell2.Text = dspollinfo.Tables[3].Rows[irg1][2].ToString();
                htcgxloc3.Controls.Add(lblqt3cell2);

                System.Web.UI.WebControls.Label lblqt3cell3 = new System.Web.UI.WebControls.Label();
                lblqt3cell3.Text = dspollinfo.Tables[3].Rows[irg1][3].ToString();
                htcgxloc4.Controls.Add(lblqt3cell3);

                System.Web.UI.WebControls.Label lblqt3cell4 = new System.Web.UI.WebControls.Label();
                lblqt3cell4.Text = dspollinfo.Tables[3].Rows[irg1][4].ToString();
                htcgxloc5.Controls.Add(lblqt3cell4);

                htrgxloc1.Cells.Add(htcgxloc1);
                htrgxloc1.Cells.Add(htcgxloc2);
                htrgxloc1.Cells.Add(htcgxloc3);
                htrgxloc1.Cells.Add(htcgxloc4);
                htrgxloc1.Cells.Add(htcgxloc5);
                qt3tableques1.Rows.Add(htrgxloc1);

                //  rpnl_reportsdata.Controls.Add(qt3tableques1);

            }

            htcgxcntry.Controls.Add(qt3tableques1);
            htrgxcntry.Cells.Add(htcgxcntry);

            qt3tableques.Rows.Add(htrgxcntry);




        }
        Session["defaultchart"] = "notdefault";
    }

    private void BindReportPage()
    {
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "showExportOptionsForPage", "<script>showExportOptions();</script>");

        if (Mode == 0)
        {
            rblgrp.Items[0].Selected = true;
        }
        else if (Mode == 1)
        {
            rblgrp.Items[1].Selected = true;
        }
        else if (Mode == 2)
        {
            rblgrp.Items[2].Selected = true;
        }

        SurveyCore appsurveycore = new SurveyCore();
        //  surveyID = 1;
        DataSet dspollinfo = null;
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        DataSet dsRespondentLimit = appsurveycore.getPollRespondentLimit(loggedInUserInfo.UserId);

        int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

        dspollinfo = appsurveycore.getpollpageinfo(surveyID, strurlpage, responselimit);
        ddlcountry.DataSource = dspollinfo.Tables[5];
        ddlcountry.DataTextField = "pageurl";
        ddlcountry.DataValueField = "pageurl";
        ddlcountry.DataBind();
        ddlcountry.Items.Insert(0, new System.Web.UI.WebControls.ListItem("All Pages", ""));
        if (strurlpage == "")
        {
            dspollinfo = appsurveycore.getpollPage(surveyID, responselimit);
        }
        ddlcountry.SelectedValue = strurlpage;
        if (dspollinfo.Tables.Count > 0)
        {
            if (dspollinfo.Tables[0].Rows.Count > 0)
            {
                lblQuestionHeader.Text = dspollinfo.Tables[0].Rows[0][1].ToString();
            }
            HtmlTable qt3tablequesurlpg = new HtmlTable();
            // qt3tablequesurlpg.Width = "640";
            qt3tablequesurlpg.CellPadding = 1;
            qt3tablequesurlpg.CellSpacing = 0;
            // qt3tablequesurlpg.Border = 1;

            if ((rblgrp.SelectedValue == "Chart"))
            {


                HtmlTableRow htrcharttempupg = new HtmlTableRow();
                HtmlTableCell htccharttempupg = new HtmlTableCell();

                System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp = new System.Web.UI.WebControls.Label();

                string strunique1 = GetUniqueKey();
                string uniquestr = surveyID.ToString();
                jsstrchartname = uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");

                StringBuilder strXML = new StringBuilder();
                string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString())).Replace("\r", "").Replace("\n", "");

                strXML.Append("<chart caption='' subcaption='' yaxisname='NoOfRespondents'  xaxisname='Answers' alternatevgridcolor='AFD8F8' basefontcolor='114B78' tooltipbordercolor='114B78' tooltipbgcolor='E7EFF6'  useroundedges='1' showborder='1' bgcolor='FFFFFF' numbersuffix='%' font='Arial, Helvetica, sans-serif' exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'   exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>");


                DataRow[] drchart = dspollinfo.Tables[2].Select("pollid = " + surveyID);
                for (int ic = 0; ic < drchart.Length; ic++)
                {
                    strXML.AppendFormat("<set label='{0}' value='{1}' />", System.Web.HttpUtility.HtmlEncode(StripTagsRegex(drchart[ic][1].ToString())).Replace("\r", "").Replace("\n", ""), drchart[ic][3].ToString());
                }
                strXML.Append("</chart>");

                string charttypename = "../Charts/Bar2D.swf";
                string chartwidth = "598";
                string chartheight = "300";

                LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart(charttypename, "", strXML.ToString(), strunique1 + uniquestr, chartwidth, chartheight, false, true, false);
                lblqt3cellpertotalcharttemp.Text = LiteralQTInitial.Text;
                htccharttempupg.Controls.Add(lblqt3cellpertotalcharttemp);
                htrcharttempupg.Cells.Add(htccharttempupg);
                qt3tablequesurlpg.Rows.Add(htrcharttempupg);

                if (Session["defaultchart"] != null)
                {
                    chartContainerParent.Controls.Add(qt3tablequesurlpg);
                }

            }
            else if (rblgrp.SelectedValue == "DataChart")
            {
                chartContainerParent.InnerText = "";
                HtmlTableRow htrcharttempupg1 = new HtmlTableRow();
                HtmlTableCell htccharttempupg1 = new HtmlTableCell();

                System.Web.UI.WebControls.Label lblqt3cellpertotalcharttempupg1 = new System.Web.UI.WebControls.Label();

                string strunique1 = GetUniqueKey();
                string uniquestr = surveyID.ToString();
                jsstrchartname = uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");

                StringBuilder strXML = new StringBuilder();
                string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString())).Replace("\r", "").Replace("\n", "");

                strXML.Append("<chart caption='' subcaption='' yaxisname='NoOfRespondents'  xaxisname='Answers' alternatevgridcolor='AFD8F8' basefontcolor='114B78' tooltipbordercolor='114B78' tooltipbgcolor='E7EFF6' useroundedges='1'  showborder='1' bgcolor='FFFFFF' numbersuffix='%' font='Arial, Helvetica, sans-serif' exportEnabled='1' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='download'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a'  exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>");


                DataRow[] drchart = dspollinfo.Tables[2].Select("pollid = " + surveyID);
                for (int ic = 0; ic < drchart.Length; ic++)
                {
                    strXML.AppendFormat("<set label='{0}' value='{1}' />", System.Web.HttpUtility.HtmlEncode(StripTagsRegex(drchart[ic][1].ToString())).Replace("\r", "").Replace("\n", ""), drchart[ic][3].ToString());
                }
                strXML.Append("</chart>");

                string charttypename = "../Charts/Bar2D.swf";
                string chartwidth = "598";
                string chartheight = "300";

                LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart(charttypename, "", strXML.ToString(), strunique1 + uniquestr, chartwidth, chartheight, false, true, false);
                lblqt3cellpertotalcharttempupg1.Text = LiteralQTInitial.Text;
                htccharttempupg1.Controls.Add(lblqt3cellpertotalcharttempupg1);
                htrcharttempupg1.Cells.Add(htccharttempupg1);
                qt3tablequesurlpg.Rows.Add(htrcharttempupg1);


                HtmlTableRow htrgxupg = new HtmlTableRow();
                HtmlTableCell htcgxupg = new HtmlTableCell();
                GridView grdvall = new GridView();
                grdvall.ID = "gridQT" + surveyID;
                grdvall.Width = 598;
                grdvall.RowStyle.Height = 25;
                grdvall.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                grdvall.GridLines = 0;
                grdvall.RowDataBound += new GridViewRowEventHandler(grdvall_RowDataBound);

                DataTable dtg = new DataTable();
                DataRow dtgr;
                DataRow dtgresp;

                dtg.Columns.Add("Answer options");
                dtg.Columns.Add("Percentage");
                dtg.Columns.Add("Count");

                DataRow[] drdatacht = dspollinfo.Tables[2].Select("pollid = " + surveyID);
                for (int irg1 = 0; irg1 < drdatacht.Length; irg1++)
                {
                    dtgr = dtg.NewRow();

                    dtgr[0] = drdatacht[irg1][1].ToString();
                    dtgr[1] = drdatacht[irg1][3].ToString() + "%";
                    dtgr[2] = drdatacht[irg1][2].ToString();

                    dtg.Rows.Add(dtgr);
                }

                dtgresp = dtg.NewRow();
                dtgresp[0] = "# of Respondents";
                dtgresp[1] = " ";
                dtgresp[2] = dspollinfo.Tables[4].Rows[0][0].ToString();
                dtg.Rows.Add(dtgresp);

                grdvall.DataSource = dtg;
                grdvall.DataBind();


                grdvall.HeaderRow.Cells[0].Height = 25;
                grdvall.HeaderRow.Cells[1].Height = 25;
                grdvall.HeaderRow.Cells[2].Height = 25;
                grdvall.HeaderRow.Cells[0].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[0].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[1].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[1].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[2].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[2].ForeColor = System.Drawing.Color.White;

                htcgxupg.Controls.Add(grdvall);
                htrgxupg.Cells.Add(htcgxupg);
                qt3tablequesurlpg.Rows.Add(htrgxupg);


                string NavUrl = "PollId=" + surveyID;

                if ((dspollinfo.Tables[0].Rows[0][5].ToString() == "True") || (dspollinfo.Tables[0].Rows[0][5].ToString() == "1"))
                {
                    HtmlTableRow hrtext = new HtmlTableRow();
                    hrtext.BorderColor = "white";
                    hrtext.Height = "30px";
                    HtmlTableCell hctext = new HtmlTableCell();

                    HyperLink hql = new HyperLink();
                    hql.Font.Bold = true;
                    hql.Font.Underline = true;
                    hql.ForeColor = System.Drawing.Color.Purple;
                    if (strurlpage != "")
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }
                    else
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }
                    hctext.Controls.Add(hql);
                    hrtext.Cells.Add(hctext);
                    qt3tablequesurlpg.Rows.Add(hrtext);
                }
                chartContainerParent.Controls.Add(qt3tablequesurlpg);


            }
            if ((rblgrp.SelectedValue == "Data"))
            {
                HtmlTableRow htrgxupg1 = new HtmlTableRow();
                HtmlTableCell htcgxupg1 = new HtmlTableCell();
                GridView grdvall = new GridView();
                grdvall.ID = "gridQT" + surveyID;
                grdvall.Width = 598;
                grdvall.RowStyle.Height = 25;
                grdvall.AlternatingRowStyle.BackColor = System.Drawing.Color.FromName("#F4F4F4");
                grdvall.GridLines = 0;
                grdvall.RowDataBound += new GridViewRowEventHandler(grdvall_RowDataBound);

                DataTable dtg = new DataTable();
                DataRow dtgr;
                DataRow dtgresp;

                string lnkother = "";
                string lnkothercount = "";

                dtg.Columns.Add("Answer options");
                dtg.Columns.Add("Percentage");
                dtg.Columns.Add("Count");

                DataRow[] drdata = dspollinfo.Tables[2].Select("pollid = " + surveyID);
                for (int irg1 = 0; irg1 < drdata.Length; irg1++)
                {
                    dtgr = dtg.NewRow();

                    dtgr[0] = drdata[irg1][1].ToString();
                    dtgr[1] = drdata[irg1][3].ToString() + "%";
                    dtgr[2] = drdata[irg1][2].ToString();

                    dtg.Rows.Add(dtgr);
                }

                dtgresp = dtg.NewRow();
                dtgresp[0] = "# of Respondents";
                dtgresp[1] = " ";
                dtgresp[2] = dspollinfo.Tables[4].Rows[0][0].ToString();
                dtg.Rows.Add(dtgresp);

                grdvall.DataSource = dtg;
                grdvall.DataBind();


                grdvall.HeaderRow.Cells[0].Height = 25;
                grdvall.HeaderRow.Cells[1].Height = 25;
                grdvall.HeaderRow.Cells[2].Height = 25;
                grdvall.HeaderRow.Cells[0].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[0].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[1].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[1].ForeColor = System.Drawing.Color.White;
                grdvall.HeaderRow.Cells[2].BackColor = System.Drawing.Color.Purple;
                grdvall.HeaderRow.Cells[2].ForeColor = System.Drawing.Color.White;

                htcgxupg1.Controls.Add(grdvall);
                htrgxupg1.Cells.Add(htcgxupg1);
                qt3tablequesurlpg.Rows.Add(htrgxupg1);

                if ((dspollinfo.Tables[0].Rows[0][5].ToString() == "True") || (dspollinfo.Tables[0].Rows[0][5].ToString() == "1"))
                {
                    HtmlTableRow hrtext = new HtmlTableRow();
                    hrtext.BorderColor = "white";
                    hrtext.Height = "30px";
                    HtmlTableCell hctext = new HtmlTableCell();

                    HyperLink hql = new HyperLink();
                    hql.Font.Bold = true;
                    hql.Font.Underline = true;
                    hql.ForeColor = System.Drawing.Color.Purple;

                    string NavUrl = "PollId=" + surveyID;

                    if (strurlpage != "")
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }
                    else
                    {
                        if (dspollinfo.Tables[5].Rows.Count == 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comment";
                        }
                        else if (dspollinfo.Tables[5].Rows.Count > 1)
                        {
                            hql.Text = "View" + " " + dspollinfo.Tables[5].Rows.Count + " " + "comments";
                        }
                        else
                        {
                            hql.Text = "No comments";
                        }

                        if (dspollinfo.Tables[5].Rows.Count > 0)
                        {
                            hql.Attributes.Add("href", "javascript:;");
                            hql.Attributes.Add("onclick", "javascript:ApplyFrameBoxWithLink($(this),'" + EncryptHelper.EncryptQuerystring("/Polls/PollCommentsSimple.aspx", NavUrl) + "')");
                            hql.Attributes.Add("id", "commentsbox");
                            hql.Attributes.Add("h", "600");
                            hql.Attributes.Add("w", "600");
                            hql.Attributes.Add("scrolling", "true");
                        }
                        else
                        {
                            hql.Attributes.Add("href", "javascript:;");
                        }
                    }
                    hctext.Controls.Add(hql);
                    hrtext.Cells.Add(hctext);
                    qt3tablequesurlpg.Rows.Add(hrtext);
                }

                rpnl_reportsdata.Controls.Add(qt3tablequesurlpg);
                chartContainerParent.InnerText = "";
            }


            HtmlTableRow htrexportpage = new HtmlTableRow();
            htrexportpage.Align = "Right";
            HtmlTableCell htcexportpage = new HtmlTableCell();
            HtmlAnchor aexportpage = new HtmlAnchor();
            aexportpage.InnerHtml = "Export";
            aexportpage.Style.Add("font-size", "12px");
            aexportpage.Style.Add("font-weight", "bold");
            aexportpage.Style.Add("font-underline", "true");
            aexportpage.Style.Add("ForeColor", "Purple");
            if (Session["SOVotes"] == "urlpage")
            {
                aexportpage.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Export=urlpage&Mode=" + Mode);
            }
            else
            {
                aexportpage.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&Export=urlpage&Mode=" + Mode + "&urlpage=" + strurlpage);
            }
            //htcexportpage.Controls.Add(aexportpage);
            htrexportpage.Cells.Add(htcexportpage);
            qt3tablequesurlpg.Rows.Add(htrexportpage);

            HtmlTableRow htrgxcntryupg = new HtmlTableRow();
            HtmlTableCell htcgxcntryupg = new HtmlTableCell();

            HtmlTable qt3tableques1upg = new HtmlTable();
            qt3tableques1upg.Width = "598";
            qt3tableques1upg.CellPadding = 1;
            qt3tableques1upg.CellSpacing = 0;
            //  qt3tableques1upg.Border = 1;

            HtmlTableRow htrgxlocheaderupg = new HtmlTableRow();
            htrgxlocheaderupg.Height = "25px";
            htrgxlocheaderupg.BgColor = "Purple";

            HtmlTableCell htcgxloch1upg = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh1upg = new System.Web.UI.WebControls.Label();
            if (Session["SOVotes"] == "urlpage")
            {
                lblqt3cellh1upg.Text = "Page";
            }
            else
            {
                lblqt3cellh1upg.Text = "Page";
            }
            lblqt3cellh1upg.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh1upg.ForeColor = System.Drawing.Color.White;
            htcgxloch1upg.Controls.Add(lblqt3cellh1upg);
            HtmlTableCell htcgxloch2upg = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh2upg = new System.Web.UI.WebControls.Label();
            lblqt3cellh2upg.Text = "Views";
            lblqt3cellh2upg.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh2upg.ForeColor = System.Drawing.Color.White;
            htcgxloch2upg.Controls.Add(lblqt3cellh2upg);

            HtmlTableCell htcgxloch3upg = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh3upg = new System.Web.UI.WebControls.Label();
            lblqt3cellh3upg.Text = "% of Views";
            lblqt3cellh3upg.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh3upg.ForeColor = System.Drawing.Color.White;
            htcgxloch3upg.Controls.Add(lblqt3cellh3upg);

            HtmlTableCell htcgxloch4upg = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh4upg = new System.Web.UI.WebControls.Label();
            lblqt3cellh4upg.Text = "Votes";
            lblqt3cellh4upg.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh4upg.ForeColor = System.Drawing.Color.White;
            htcgxloch4upg.Controls.Add(lblqt3cellh4upg);
            HtmlTableCell htcgxloch5upg = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellh5upg = new System.Web.UI.WebControls.Label();
            lblqt3cellh5upg.Text = "% of Votes";
            lblqt3cellh5upg.Font.Name = "Arial, Helvetica, sans-serif";
            lblqt3cellh5upg.ForeColor = System.Drawing.Color.White;
            htcgxloch5upg.Controls.Add(lblqt3cellh5upg);

            htrgxlocheaderupg.Cells.Add(htcgxloch1upg);
            htrgxlocheaderupg.Cells.Add(htcgxloch2upg);
            htrgxlocheaderupg.Cells.Add(htcgxloch3upg);
            htrgxlocheaderupg.Cells.Add(htcgxloch4upg);
            htrgxlocheaderupg.Cells.Add(htcgxloch5upg);
            qt3tableques1upg.Rows.Add(htrgxlocheaderupg);


            HtmlTableRow htrgxloc1upg;
            for (int irg1 = 0; irg1 < dspollinfo.Tables[3].Rows.Count; irg1++)
            {
                htrgxloc1upg = new HtmlTableRow();
                htrgxloc1upg.Height = "25px";
                if (irg1 % 2 == 0)
                {
                    htrgxloc1upg.BgColor = "#FFFFFF";
                }
                else
                {
                    htrgxloc1upg.BgColor = "#F4F4F4";
                }
                HtmlTableCell htcgxloc1upg = new HtmlTableCell();
                htcgxloc1upg.Width = "128";
                HtmlTableCell htcgxloc2upg = new HtmlTableCell();
                htcgxloc2upg.Width = "128";
                HtmlTableCell htcgxloc3upg = new HtmlTableCell();
                htcgxloc3upg.Width = "128";
                HtmlTableCell htcgxloc4upg = new HtmlTableCell();
                htcgxloc4upg.Width = "128";
                HtmlTableCell htcgxloc5upg = new HtmlTableCell();
                htcgxloc5upg.Width = "128";

                HtmlAnchor aupg = new HtmlAnchor();
                aupg.InnerHtml = dspollinfo.Tables[3].Rows[irg1][0].ToString();
                if (Session["SOVotes"] == "urlpage")
                {
                    aupg.HRef = EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + surveyID + "&urlpage=" + dspollinfo.Tables[3].Rows[irg1][0].ToString() + "&Mode=" + Mode);
                }
                htcgxloc1upg.Controls.Add(aupg);

                System.Web.UI.WebControls.Label lblqt3cell1upg = new System.Web.UI.WebControls.Label();
                lblqt3cell1upg.Text = dspollinfo.Tables[3].Rows[irg1][1].ToString();
                htcgxloc2upg.Controls.Add(lblqt3cell1upg);

                System.Web.UI.WebControls.Label lblqt3cell2upg = new System.Web.UI.WebControls.Label();
                lblqt3cell2upg.Text = dspollinfo.Tables[3].Rows[irg1][2].ToString();
                htcgxloc3upg.Controls.Add(lblqt3cell2upg);

                System.Web.UI.WebControls.Label lblqt3cell3upg = new System.Web.UI.WebControls.Label();
                lblqt3cell3upg.Text = dspollinfo.Tables[3].Rows[irg1][3].ToString();
                htcgxloc4upg.Controls.Add(lblqt3cell3upg);

                System.Web.UI.WebControls.Label lblqt3cell4upg = new System.Web.UI.WebControls.Label();
                lblqt3cell4upg.Text = dspollinfo.Tables[3].Rows[irg1][4].ToString();
                htcgxloc5upg.Controls.Add(lblqt3cell4upg);

                htrgxloc1upg.Cells.Add(htcgxloc1upg);
                htrgxloc1upg.Cells.Add(htcgxloc2upg);
                htrgxloc1upg.Cells.Add(htcgxloc3upg);
                htrgxloc1upg.Cells.Add(htcgxloc4upg);
                htrgxloc1upg.Cells.Add(htcgxloc5upg);
                qt3tableques1upg.Rows.Add(htrgxloc1upg);

                //  rpnl_reportsdata.Controls.Add(qt3tableques1);

            }

            htcgxcntryupg.Controls.Add(qt3tableques1upg);
            htrgxcntryupg.Cells.Add(htcgxcntryupg);

            qt3tablequesurlpg.Rows.Add(htrgxcntryupg);

        }
        Session["defaultchart"] = "notdefault";
    }

    protected void grdvall_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[0].Width = 240;
        e.Row.Cells[1].Width = 180;
        e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right;
        e.Row.Cells[2].Width = 180;
        e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
        e.Row.Font.Name = "Arial";

        if (e.Row.Cells[0].Text == "# of Respondents")
        {
            e.Row.BackColor = System.Drawing.Color.FromName("#D8D8D8");
            e.Row.Font.Bold = true;

        }

    }

    protected void grdvallloc_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[0].Width = 120;
        e.Row.Cells[1].Width = 120;
        e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right;
        e.Row.Cells[2].Width = 120;
        e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
        e.Row.Cells[3].Width = 120;
        e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
        e.Row.Cells[4].Width = 120;
        e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
        e.Row.Font.Name = "Arial";

        if (e.Row.Cells[0].Text == "# of Respondents")
        {
            e.Row.BackColor = System.Drawing.Color.FromName("#D8D8D8");
            e.Row.Font.Bold = true;

        }

    }

    private string GetUniqueKey()
    {
        int maxSize = 8;
        int minSize = 5;
        char[] chars = new char[62];
        string a;
        a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        chars = a.ToCharArray();
        int size = maxSize;
        byte[] data = new byte[1];
        RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
        crypto.GetNonZeroBytes(data);
        size = maxSize;
        data = new byte[size];
        crypto.GetNonZeroBytes(data);
        StringBuilder result = new StringBuilder(size);
        foreach (byte b in data)
        {
            result.Append(chars[b % (chars.Length - 1)]);
        }
        return result.ToString();
    }

    public static string StripTagsRegex(string source)
    {
        return Regex.Replace(source, "<.*?>", string.Empty);
    }

    protected void lnklocation_Click(object sender, EventArgs e)
    {
        Session["SOVotes"] = "location";
        lnkallquestions.Text = "All Locations";
        ddlcountry.Visible = true;
        lnkallquestions.Visible = false;
        lblarrow.Visible = false;
        ddlcountry.Items.Clear();
        chartContainerParent.InnerHtml = "";
        rpnl_reportsdata.Controls.Clear();
        BindReportLocation();
    }

    protected void lnkdevice_Click(object sender, EventArgs e)
    {
        Session["SOVotes"] = "device";
        lnkallquestions.Text = "All Devices";
        ddlcountry.Visible = true;
        lnkallquestions.Visible = false;
        lblarrow.Visible = false;
        ddlcountry.Items.Clear();
        chartContainerParent.InnerHtml = "";
        rpnl_reportsdata.Controls.Clear();
        BindReportDevice();
    }

    protected void lnkPage_Click(object sender, EventArgs e)
    {
        Session["SOVotes"] = "urlpage";
        lnkallquestions.Text = "All Pages";
        ddlcountry.Visible = true;
        lnkallquestions.Visible = false;
        lblarrow.Visible = false;
        //lblurlpage.Visible = true;
        //lbllocation.Visible = false;
        //lbldevice.Visible = false;
        ddlcountry.Items.Clear();
        chartContainerParent.InnerHtml = "";
        rpnl_reportsdata.Controls.Clear();
        BindReportPage();
    }

    protected void lnkPPT_Click(object sender, EventArgs e)
    {
        exportpptx();
    }

    protected void lnkPDF_Click(object sender, EventArgs e)
    {
        exportpdf();
    }

    protected void lnkWord_Click(object sender, EventArgs e)
    {
        exportdocx();
    }

    protected void lnkExcel_Click(object sender, EventArgs e)
    {
        WriteOpenCloseQToExcelSheet();
    }

    protected void lnkallquestions_Click(object sender, EventArgs e)
    {
        chartContainerParent.InnerHtml = "";
        lblarrow.Visible = false;
        ddlcountry.Visible = false;

        if (strcountryname != "")
        {
            strcountryname = "";
            Session["SOVotes"] = "location";
            //lbllocation.Visible = true;
            BindReportLocation();
        }
        else if (strdevicename != "")
        {
            strdevicename = "";
            Session["SOVotes"] = "device";
            //lbllocation.Visible = true;
            BindReportDevice();
        }
        else if (strurlpage != "")
        {
            strurlpage = "";
            Session["SOVotes"] = "urlpage";
            //lbllocation.Visible = true;
            BindReportPage();
        }

    }

    protected void ddlcountry_Changed(object sender, EventArgs e)
    {
        // Session["strcountryname"] = ddlcountry.SelectedItem.Value.ToString();
        // BindReportLocation();

        //string navurl = EncryptHelper.EncryptQuerystring("PollReports.aspx", "Country=" + ddlcountry.SelectedItem.Value + "&Mode=" + Mode);
        //Response.Redirect(navurl);
    }

    [STAThread]
    public void OnlyCapture(string ChartPath, string ChartXMLURL, string FileName)
    {
        //try
        //{
        string ReturnValue = "";
        FusionCharts.ServerSideImageHandler ssh = new FusionCharts.ServerSideImageHandler(ChartPath, 850, 500, ChartXMLURL, "", FusionCharts.ServerSideImageHandler.ImageType.PNG, 96.0f);
        // Setting chart SWF version
        ssh.SetChartVersion("3.2.2.0");
        // ssh.SetChartVersion("3.2.1.0");
        ssh.SetCapturingDelayTime(10000);
        // Begins the capture process.
        // An image will be created at the specified location after execution of this statement.
        ReturnValue = ssh.BeginCapture();
        //System.Web.HttpContext.Current.Response.Write("<br/>" + FileName);
        //System.Web.HttpContext.Current.Response.Write("<br/>" + ChartPath);
        //System.Web.HttpContext.Current.Response.Write("<br/>" + ChartXMLURL);
        //System.Web.HttpContext.Current.Response.Write("<b>Error:&nbsp;&nbsp;</b>" + ReturnValue.Replace(Environment.NewLine, "<br/>"));
        //System.Web.HttpContext.Current.Response.End();
        if (!ReturnValue.ToLower().Equals("success"))
        {
            System.Web.HttpContext.Current.Response.Write("<b>Error:&nbsp;&nbsp;</b>" + ReturnValue.Replace(Environment.NewLine, "<br/>"));
            //System.Web.HttpContext.Current.Response.End();
        }
        else
        {
            //System.Web.HttpContext.Current.Response.End();
            Bitmap bmp = ssh.GetImage();
            bmp.Save(FileName);
        }
        //}
        //catch (Exception ex)
        //{
        //    throw ex;
        //}
    }

    public void generatePollImage()
    {
        // int surveyid = surveyID;
        //  surveyid = 1;
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

        int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());
        DataSet dspollinfo = surcore.getpollinfo(surveyID, responselimit);

        string strunique1 = GetUniqueKey();
        string uniquestr = strunique1 + surveyID.ToString();
        jsstrchartname = uniquestr + DateTime.Now.ToString("ddMMyyyyhhMMss");

        StringBuilder strXML = new StringBuilder();
        string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString())).Replace("\r", "").Replace("\n", "");
        exportchartfilehandler = "http://localhost:88/FcExporter.aspx";
        strXML.Append("<chart caption='" + strchartname1 + "'  animate3D='1' xAxisName='' showValues='1' labelStep='0'  canvasBgColor='f7eed2' canvasbgAlpha='0' canvasBgAngle='270' canvasBorderColor='6e1473' canvasBorderThickness='1'  showBorder='1'  borderColor='6e1473' borderThickness='1' paletteColors='AFD8F8,F6BD0F,8BBA00,A66EDD,F984A1'   bgColor='FFFFFF' bgAlpha='0' subCaption=''  exportEnabled='0' exportAtClient='0' exportHandler='" + exportchartfilehandler + "' exportAction='save'  exportFileName='" + jsstrchartname + "' exportDialogColor='6e1473' exportDialogBorderColor='62d955' exportDialogFontColor='5bbcf0' exportDialogPBColor='fafa0a' numberSuffix='%'  exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF' showAboutMenuItem='0' allowRotation='1' font='Arial, Helvetica, sans-serif' >");

        //foreach (DataRow drm in dspollinfo.Tables[2].Rows)
        //{
        //    strXML.AppendFormat("<set label='{0}' value='{1}' />", drm[1].ToString().Replace("\r", "").Replace("\n", ""), drm[3].ToString());
        //}
        DataRow[] drchart = dspollinfo.Tables[2].Select("pollid = " + surveyID);
        for (int ic = 0; ic < drchart.Length; ic++)
        {
            strXML.AppendFormat("<set label='{0}' value='{1}' />", System.Web.HttpUtility.HtmlEncode(StripTagsRegex(drchart[ic][1].ToString())).Replace("\r", "").Replace("\n", ""), drchart[ic][3].ToString());
        }
        strXML.Append("</chart>");

        string charttypename = "..\\Charts\\Bar2D.swf";
        charttypename = "//Charts//Bar2D.swf";

        // LiteralQTInitial.Text = InfoSoftGlobal.FusionCharts.RenderChart(charttypename, "", strXML.ToString(), strunique1 + uniquestr, chartwidth, chartheight, false, true, false);
        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();
        string cpath = ConfigurationSettings.AppSettings["Fchartpath"].ToString();
        // string ChartPath = System.Web.HttpContext.Current.Server.MapPath(charttypename);  
        string ChartPath = cpath + charttypename;
        string ChartXMLURL = strXML.ToString();
        string fileNameppt = SAVE_PATH + "pptimg" + surveyID + ".png";


        OnlyCapture(ChartPath, ChartXMLURL, fileNameppt);
    }

    public void exportpptx()
    {

        string sourcename = SAVE_PATH + "openxmlpptxfile.pptx";
        string destinationname = SAVE_PATH + "openxmlpptxfile" + surveyID.ToString() + ".pptx";

        if (File.Exists(destinationname))
        {
            File.Delete(destinationname);
        }

        File.Copy(sourcename, destinationname);
        string surveyname = "";
        using (PresentationDocument presentationDocument = PresentationDocument.Open(destinationname, true))
        {

            int pcnt = presentationDocument.PresentationPart.SlideParts.Count();

            // Pass the source document and the position and title of the slide to be inserted to the next method.

            //   int surveyid = surveyID;
            // surveyid = 1;

            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

            int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());
            DataSet dspollinfo = surcore.getpollinfo(surveyID, responselimit);

            try
            {

                generatePollImage();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //generatePollImage();


            try
            {
                surveyname = dspollinfo.Tables[0].Rows[0][2].ToString();

                pptxml.InsertNewPollfirstSlide(presentationDocument, 0, "firstslide", surveyname);


                DataRow[] drdata = dspollinfo.Tables[2].Select("pollid = " + surveyID);


                //  pptxml.InsertNewPollSlide(presentationDocument,1,"1",dspollinfo.Tables[2],surveyid.ToString(),dspollinfo.Tables[4].Rows[0][0].ToString(),dspollinfo.Tables[0].Rows[0][1].ToString());

                pptxml.InsertNewPollSlide(presentationDocument, 1, "1", drdata, surveyID.ToString(), dspollinfo.Tables[4].Rows[0][0].ToString(), dspollinfo.Tables[0].Rows[0][1].ToString());


            }
            catch (Exception ex)
            {
                throw ex;
            }

            pptxml.DeleteSlide(presentationDocument, 0);
        }


        //try
        //{
        string name_survey = surveyname.Replace(" ", "");
        name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
        name_survey = name_survey.Replace("/", "");
        name_survey = name_survey.Replace("/", "");
        name_survey = name_survey.Replace("'", "");
        name_survey = name_survey.Replace("<", "");
        name_survey = name_survey.Replace(">", "");
        name_survey = name_survey.Replace("\"", "");

        Response.ContentType = "Application/vnd.ms-powerpoint";
        // Response.ContentType = "application/octet-stream";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + ".pptx");
        Response.TransmitFile(destinationname);

        // Context.ApplicationInstance.CompleteRequest();
        Response.End();

    }


    public void exportdocx()
    {
        try
        {
            string sourcename = SAVE_PATH + "openxmldocxfile.docx";
            string destinationname = SAVE_PATH + "openxmldocxfile" + surveyID.ToString() + ".docx";

            if (File.Exists(destinationname))
            {
                File.Delete(destinationname);
            }

            File.Copy(sourcename, destinationname);

            //  int surveyid = surveyID;
            //  surveyid = 1;

            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

            int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

            DataSet dspollinfo = surcore.getpollinfo(surveyID, responselimit);

            using (WordprocessingDocument wordDocumnet = WordprocessingDocument.Open(destinationname, true))
            {

                //   int pcnt = presentationDocument.PresentationPart.SlideParts.Count();

                // Pass the source document and the position and title of the slide to be inserted to the next method.



                try
                {

                    generatePollImage();
                }
                catch (Exception ex)
                {
                    throw ex;
                }




                DOCXopenXML docxml = new DOCXopenXML();

                DocumentFormat.OpenXml.Wordprocessing.Paragraph firstpageparagraph = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));
                docxml.CreatePollFirstTable(destinationname, wordDocumnet, dspollinfo.Tables[0].Rows[0][2].ToString());
                wordDocumnet.MainDocumentPart.Document.Body.Append(firstpageparagraph);

                // docxml.CreateTable(destinationname, wordDocumnet);


                //// wordDocumnet.AddMainDocumentPart();




                DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph2 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));


                DataRow[] drdata = dspollinfo.Tables[2].Select("pollid = " + surveyID);


                docxml.CreatePollTable(destinationname, wordDocumnet, drdata, dspollinfo.Tables[4].Rows[0][0].ToString(), StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString()));

                wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph2);

                DocumentFormat.OpenXml.Wordprocessing.Paragraph PageBreakParagraph1 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));


                docxml.InsertAPicture(wordDocumnet, SAVE_PATH + "pptimg" + dspollinfo.Tables[0].Rows[0][0].ToString() + ".png", StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString()));
                wordDocumnet.MainDocumentPart.Document.Body.Append(PageBreakParagraph1);



                wordDocumnet.MainDocumentPart.Document.Save();



            }
            string name_survey = dspollinfo.Tables[0].Rows[0][2].ToString().Replace(" ", "");
            name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
            name_survey = name_survey.Replace("/", "");
            name_survey = name_survey.Replace("/", "");
            name_survey = name_survey.Replace("'", "");
            name_survey = name_survey.Replace("<", "");
            name_survey = name_survey.Replace(">", "");
            name_survey = name_survey.Replace("\"", "");

            Response.ContentType = "application/vnd.openxmlformats-office";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + ".docx");
            Response.TransmitFile(destinationname);
            Response.End();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //protected void LinkButtonDownload_Click(object sender, EventArgs e)
    //{
    //    Response.ContentType = "Application/msword";
    //    Response.AppendHeader("Content-Disposition", "attachment; filename=Test.docx");
    //    Response.TransmitFile("D://InsightoDevCode/InsightoV2/images/ExportImages/openxmldocxfile1.docx");
    //    Response.End();
    //}

    protected void lbn_rawdataexport_Click(object sender, EventArgs e)
    {
        WriteRawDataToExcelSheet();
    }

    protected void lnkCnvGrpTImg_Click(object sender, EventArgs e)
    {

    }

    public void exportpdf()
    {

        //  int surveyid = surveyID;
        // surveyid = 1;
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

        DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

        int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

        DataSet dspollinfo = surcore.getpollinfo(surveyID, responselimit);


        try
        {
            generatePollImage();

        }
        catch (Exception ex)
        {
            throw ex;
        }

        sharp.Document document = new sharp.Document();
        string name_survey = dspollinfo.Tables[0].Rows[0][2].ToString().Replace(" ", "");
        name_survey = name_survey.Replace("/", "");
        name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
        name_survey = name_survey.Replace("/", "");
        name_survey = name_survey.Replace("'", "");
        name_survey = name_survey.Replace("<", "");
        name_survey = name_survey.Replace(">", "");
        name_survey = name_survey.Replace("\"", "");

        string strDirector = ConfigurationManager.AppSettings["RootPath"];
        if (!Directory.Exists(strDirector + dspollinfo.Tables[0].Rows[0][3].ToString() + @"\" + dspollinfo.Tables[0].Rows[0][0].ToString()))
        {
            Directory.CreateDirectory(strDirector + dspollinfo.Tables[0].Rows[0][3].ToString() + @"\" + dspollinfo.Tables[0].Rows[0][0].ToString());
        }
        string path = strDirector + dspollinfo.Tables[0].Rows[0][3].ToString() + "\\" + dspollinfo.Tables[0].Rows[0][0].ToString() + "\\" + name_survey + String.Format("{0:MM-dd-yy_hh-mm-ss}", DateTime.UtcNow) + ".pdf";


        PdfWriter.GetInstance(document, new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite));
        document.Open();

        sharp.Table tablesrvyname = new iTextSharp.text.Table(1, 1);
        tablesrvyname.Padding = 4;
        tablesrvyname.Width = 100;
        iTextSharp.text.Cell cellsrvyname = new iTextSharp.text.Cell(new Phrase("Poll Title: " + dspollinfo.Tables[0].Rows[0][2].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 20)));
        cellsrvyname.HorizontalAlignment = sharp.Element.ALIGN_CENTER;
        cellsrvyname.VerticalAlignment = sharp.Element.ALIGN_CENTER;
        tablesrvyname.AddCell(cellsrvyname);
        tablesrvyname.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;
        tablesrvyname.BorderWidth = 1;
        document.Add(tablesrvyname);
        document.NewPage();


        sharp.Table tablequeslbl = new iTextSharp.text.Table(1, 1);
        tablequeslbl.Padding = 4;
        tablequeslbl.Width = 100;
        iTextSharp.text.Cell cellqueslbl = new iTextSharp.text.Cell(new Phrase(StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD)));
        cellqueslbl.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
        cellqueslbl.VerticalAlignment = sharp.Element.ALIGN_CENTER;
        tablequeslbl.AddCell(cellqueslbl);
        tablequeslbl.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;
        tablequeslbl.BorderWidth = 1;
        document.Add(tablequeslbl);

        System.Web.UI.WebControls.Label quesLabel1234 = new System.Web.UI.WebControls.Label();
        quesLabel1234.Text = "";
        iTextSharp.text.Font font22B = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD);
        document.Add(new sharp.Paragraph(quesLabel1234.Text, font22B));

        sharp.Table table = new iTextSharp.text.Table(3, dspollinfo.Tables[2].Rows.Count + 2);
        table.Padding = 4;
        table.Width = 100;
        iTextSharp.text.Cell cell = new iTextSharp.text.Cell(new Phrase("Answer options", FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
        cell.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
        cell.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
        cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
        table.AddCell(cell);
        iTextSharp.text.Cell cell1 = new iTextSharp.text.Cell(new Phrase("In%", FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
        cell1.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
        cell1.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
        cell1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
        table.AddCell(cell1);
        iTextSharp.text.Cell cell2 = new iTextSharp.text.Cell(new Phrase("In nos.", FontFactory.GetFont(FontFactory.HELVETICA, 10, 1, new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#FFFFFF")))));
        cell2.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
        cell2.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
        cell2.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#4F81BD"));
        table.AddCell(cell2);


        DataRow[] drdata = dspollinfo.Tables[2].Select("pollid = " + surveyID);

        for (int irg1 = 0; irg1 < drdata.Length; irg1++)
        {
            iTextSharp.text.Cell cellAns = new iTextSharp.text.Cell(new Phrase(drdata[irg1][1].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
            cellAns.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
            cellAns.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
            if (irg1 == 1)
            {
                cellAns.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
            }
            else
            {
                if ((irg1 % 2) == 1)
                {
                    cellAns.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                }
                else
                {
                    cellAns.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                }
            }
            table.AddCell(cellAns);
            iTextSharp.text.Cell cellAns1 = new iTextSharp.text.Cell(new Phrase(drdata[irg1][3].ToString() + "%", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
            cellAns1.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
            cellAns1.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
            if (irg1 == 1)
            {
                cellAns1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
            }
            else
            {
                if ((irg1 % 2) == 1)
                {
                    cellAns1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                }
                else
                {
                    cellAns1.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                }
            }
            table.AddCell(cellAns1);
            iTextSharp.text.Cell cellAns2 = new iTextSharp.text.Cell(new Phrase(drdata[irg1][2].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
            cellAns2.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
            cellAns2.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
            if (irg1 == 1)
            {
                cellAns2.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
            }
            else
            {
                if ((irg1 % 2) == 1)
                {
                    cellAns2.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#E9EDF4"));
                }
                else
                {
                    cellAns2.BackgroundColor = new iTextSharp.text.Color(System.Drawing.ColorTranslator.FromHtml("#D0D8E8"));
                }
            }
            table.AddCell(cellAns2);
        }

        iTextSharp.text.Cell rcell = new iTextSharp.text.Cell(new Phrase("# of Respondents", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
        rcell.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
        rcell.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
        table.AddCell(rcell);
        iTextSharp.text.Cell rcell1 = new iTextSharp.text.Cell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 10)));
        rcell1.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
        rcell1.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
        table.AddCell(rcell1);
        iTextSharp.text.Cell rcell2 = new iTextSharp.text.Cell(new Phrase(dspollinfo.Tables[4].Rows[0][0].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)));
        rcell2.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
        rcell2.VerticalAlignment = sharp.Element.ALIGN_MIDDLE;
        table.AddCell(rcell2);
        document.Add(table);
        document.NewPage();

        sharp.Table tablequeslblimg = new iTextSharp.text.Table(1, 1);
        tablequeslblimg.Padding = 4;
        tablequeslblimg.Width = 83;
        iTextSharp.text.Cell cellqueslblimg = new iTextSharp.text.Cell(new Phrase(StripTagsRegex(dspollinfo.Tables[0].Rows[0][1].ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD)));
        cellqueslblimg.HorizontalAlignment = sharp.Element.ALIGN_LEFT;
        cellqueslblimg.VerticalAlignment = sharp.Element.ALIGN_CENTER;
        tablequeslblimg.AddCell(cellqueslblimg);
        tablequeslblimg.Border = sharp.Table.LEFT_BORDER | sharp.Table.RIGHT_BORDER | sharp.Table.TOP_BORDER | sharp.Table.BOTTOM_BORDER;
        tablequeslblimg.BorderWidth = 1;
        document.Add(tablequeslblimg);
        string PicPath = SAVE_PATH + "pptimg" + dspollinfo.Tables[0].Rows[0][0].ToString() + ".png";

        document.Add(new sharp.Paragraph());
        sharp.Image img = sharp.Image.GetInstance(PicPath);
        img.Alignment = sharp.Element.ALIGN_CENTER;
        img.ScalePercent(51);
        img.Border = sharp.Image.LEFT_BORDER | sharp.Image.RIGHT_BORDER | sharp.Image.TOP_BORDER | sharp.Image.BOTTOM_BORDER;
        img.BorderWidth = 1;
        document.Add(img);
        document.NewPage();






        document.NewPage();
        document.Add(new sharp.Paragraph("Thank you for using Insighto"));
        document.Close();
        string targetFile = dspollinfo.Tables[0].Rows[0][2].ToString();
        string Extension = ".pdf";
        SaveDialogBox(path, targetFile, Extension);
    }

    public void SaveDialogBox(string sourcePath, string SurveyName, string fileExtension)
    {

        if (File.Exists(sourcePath))
        {

            string name_survey = SurveyName.Replace(" ", "");
            name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
            name_survey = name_survey.Replace("/", "");
            name_survey = name_survey.Replace("'", "");
            name_survey = name_survey.Replace("<", "");
            name_survey = name_survey.Replace(">", "");
            name_survey = name_survey.Replace("\"", "");


            Response.ContentType = "application/pdf";

            Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + fileExtension);
            Response.TransmitFile(sourcePath);
            Response.End();

        }
    }

    public void WriteOpenCloseQToExcelSheet()
    {
        var sessionStateService = ServiceFactory.GetService<SessionStateService>();
        LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
        try
        {

            //create FileInfo object  to read you ExcelWorkbook
            // int surveyid = 1;
            DataSet dssurveydata = surcore.getpollSurveyAnalyticsData(Convert.ToInt32(surveyID));

            string strDirector = ConfigurationManager.AppSettings["RootPath"].ToString();
            if (!Directory.Exists(strDirector + dssurveydata.Tables[3].Rows[0][4].ToString() + @"\" + dssurveydata.Tables[3].Rows[0][5].ToString()))
            {
                Directory.CreateDirectory(strDirector + dssurveydata.Tables[3].Rows[0][4].ToString() + @"\" + dssurveydata.Tables[3].Rows[0][5].ToString());
            }

            string surveyname = "";
            surveyname = dssurveydata.Tables[3].Rows[0][0].ToString() + "_Summary_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
            surveyname = Regex.Replace(surveyname, "[^\\w\\.@-]", "");
            surveyname = surveyname.Replace(" ", "");
            surveyname = surveyname.Replace("/", "");
            surveyname = surveyname.Replace("'", "");
            surveyname = surveyname.Replace("<", "");
            surveyname = surveyname.Replace(">", "");
            surveyname = surveyname.Replace("\"", "");

            string strXLFileName = strDirector + dssurveydata.Tables[3].Rows[0][4].ToString() + "\\" + dssurveydata.Tables[3].Rows[0][5].ToString() + "\\" + surveyname;


            var file = new FileInfo(strXLFileName);


            using (ExcelPackage xlPackage = new ExcelPackage(file))
            {

                //Fetch the worksheet to insert the data
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Poll Profile and Statistics");

                //Select a range of cells to insert the data and use the LoadFromDataTable method to write the
                //data                 
                string ycolor = "#FFBF00";
                string yblue = "#C6CFEB";

                worksheet.Cells[1, 1, 800, 800].Clear();
                worksheet.Column(1).Width = 30;
                worksheet.Column(2).Width = 30;
                worksheet.Column(4).Width = 30;
                worksheet.Column(5).Width = 20;
                worksheet.Column(6).Width = 20;
                worksheet.Cells[1, 1, 1, 2].Merge = true;
                worksheet.Cells[1, 1, 1, 2].Value = "Poll Fact Profile";
                worksheet.Cells[1, 1, 1, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[1, 1, 1, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                worksheet.Cells[1, 1, 1, 2].Style.WrapText = true;
                worksheet.Cells[1, 1, 1, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[1, 1, 1, 2].Style.Font.Bold = true;


                worksheet.Cells[3, 1].Value = "Poll Title";
                worksheet.Cells[3, 1].Style.Font.Bold = true;
                worksheet.Cells[3, 2].Value = dssurveydata.Tables[3].Rows[0][0].ToString();

                worksheet.Cells[4, 1].Value = "User Name";
                worksheet.Cells[4, 1].Style.Font.Bold = true;
                worksheet.Cells[4, 2].Value = dssurveydata.Tables[3].Rows[0][3].ToString();

                worksheet.Cells[5, 1].Value = "Launch Date";
                worksheet.Cells[5, 1].Style.Font.Bold = true;
                if (dssurveydata.Tables[2].Rows.Count > 0)
                {
                    //string launchdate = "";
                    //launchdate += Convert.ToDateTime(dssurveydata.Tables[2].Rows[0][0]).Day.ToString();
                    //launchdate += "-";
                    //launchdate += System.Globalization.DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(Convert.ToDateTime(dssurveydata.Tables[2].Rows[0][0]).Month);
                    //launchdate += "-";
                    //launchdate += Convert.ToDateTime(dssurveydata.Tables[2].Rows[0][0]).Year.ToString();
                    //launchdate += " ";
                    //launchdate +=  Convert.ToDateTime(dssurveydata.Tables[2].Rows[0][0]).ToShortTimeString();
                    string launchdate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(dssurveydata.Tables[2].Rows[0][0]), loggedInUserInfo.StandardBIAS, true).ToString("dd-MMM-yyyy hh:mm tt");
                    worksheet.Cells[5, 2].Value = launchdate;
                }
                else
                {
                    worksheet.Cells[5, 2].Value = " ";
                }


                worksheet.Cells[6, 1].Value = "Date of Export";
                worksheet.Cells[6, 1].Style.Font.Bold = true;

                worksheet.Cells[6, 2].Value = Convert.ToString(Convert.ToDateTime(CommonMethods.GetConvertedDatetime(DateTime.Now, loggedInUserInfo.StandardBIAS, true)).ToString("dd-MMM-yyyy") + " at " + DateTime.Now.ToShortTimeString());

                worksheet.Cells[1, 4, 1, 6].Merge = true;
                worksheet.Cells[1, 4, 1, 6].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[1, 4, 1, 6].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                worksheet.Cells[1, 4, 1, 6].Style.WrapText = true;
                worksheet.Cells[1, 4, 1, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[1, 4, 1, 6].Style.Font.Bold = true;
                worksheet.Cells[1, 4, 1, 6].Value = "Poll Name:" + dssurveydata.Tables[3].Rows[0][0].ToString();

                worksheet.Cells[2, 4, 2, 6].Merge = true;
                worksheet.Cells[2, 4, 2, 6].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;

                worksheet.Cells[2, 4, 2, 6].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(ycolor));
                worksheet.Cells[2, 4, 2, 6].Style.WrapText = true;
                worksheet.Cells[2, 4, 2, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells[2, 4, 2, 6].Style.Font.Bold = true;
                worksheet.Cells[2, 4, 2, 6].Value = "Poll Statistics Report";


                worksheet.Cells[3, 5].Value = "Count";
                worksheet.Cells[3, 5].Style.Font.Bold = true;

                worksheet.Cells[3, 6].Value = "Response in %";
                worksheet.Cells[3, 6].Style.Font.Bold = true;


                worksheet.Cells[4, 4].Value = "# of Visits";
                worksheet.Cells[4, 4].Style.Font.Bold = true;
                worksheet.Cells[4, 5].Value = Convert.ToInt32(dssurveydata.Tables[0].Rows[0][0].ToString());
                worksheet.Cells[4, 6].Value = Convert.ToInt32(dssurveydata.Tables[0].Rows[0][1].ToString());

                worksheet.Cells[5, 4].Value = "Votes";
                worksheet.Cells[5, 4].Style.Font.Bold = true;
                worksheet.Cells[5, 5].Value = Convert.ToInt32(dssurveydata.Tables[1].Rows[0][0].ToString());
                worksheet.Cells[5, 6].Value = Convert.ToInt32(dssurveydata.Tables[1].Rows[0][1].ToString());



                worksheet.Cells[1, 4, 1, 6].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[1, 4, 1, 6].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[1, 4, 1, 6].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[1, 4, 1, 6].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, 4, 2, 6].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, 4, 2, 6].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[3, 4, 3, 6].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[3, 4, 3, 6].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[5, 4, 5, 6].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[5, 4, 5, 6].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                worksheet.Cells[1, 4, 5, 6].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[1, 4, 5, 6].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[1, 4, 5, 6].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[1, 4, 5, 6].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                worksheet.Cells[6, 1].Value = "Poll URL";
                worksheet.Cells[6, 2].Value = dssurveydata.Tables[3].Rows[0][1].ToString();


                int TRid = 1;

                // ExcelWorksheet worksheet1 = xlPackage.Workbook.Worksheets["CloseEndedResponses"];
                ExcelWorksheet worksheet1 = xlPackage.Workbook.Worksheets.Add("Close-ended Responses");

                worksheet1.Cells[1, 1, 800, 800].Clear();

                worksheet1.Column(1).Width = 30;
                worksheet1.Column(2).Width = 10;
                worksheet1.Column(3).Width = 10;


                worksheet1.Cells[2, 1, 2, 3].Merge = true;
                worksheet1.Cells[2, 1, 2, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet1.Cells[2, 1, 2, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                worksheet1.Cells[2, 1, 2, 3].Style.WrapText = true;
                worksheet1.Cells[2, 1, 2, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet1.Cells[2, 1, 2, 3].Style.Font.Bold = true;
                worksheet1.Cells[2, 1, 2, 3].Value = "Poll Name:" + dssurveydata.Tables[3].Rows[0][0].ToString();

                worksheet1.Cells[2, 3, 2, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[2, 3, 2, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                worksheet1.Cells[2, 1, 2, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[2, 1, 2, 3].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);


                //var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                //LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

                int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

                DataSet dsquestions = surcore.getpollinfo(surveyID, responselimit);

                int intQrowdata = 4;


                worksheet1.Cells[intQrowdata, 1].Value = dsquestions.Tables[0].Rows[0][4].ToString();
                intQrowdata = intQrowdata + 1;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Merge = true;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Font.Bold = true;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Value = StripTagsRegex(dsquestions.Tables[0].Rows[0][1].ToString()).Trim();

                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                worksheet1.Cells[intQrowdata, 3, intQrowdata, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[intQrowdata, 3, intQrowdata, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                intQrowdata = intQrowdata + 1;




                DataTable dtg = new DataTable();
                dtg.TableName = "dtQT" + dsquestions.Tables[0].Rows[0][4].ToString();
                DataRow dtgr;
                DataRow dtgresp;

                dtg.Columns.Add("Answer Options");
                dtg.Columns.Add("In %");
                dtg.Columns.Add("In nos.");


                DataRow[] drdata = dsquestions.Tables[2].Select("pollid = " + surveyID);

                for (int irg1 = 0; irg1 < drdata.Length; irg1++)
                {
                    dtgr = dtg.NewRow();
                    dtgr[0] = drdata[irg1][1].ToString();
                    dtgr[1] = drdata[irg1][3].ToString() + "%";
                    dtgr[2] = drdata[irg1][2].ToString();
                    dtg.Rows.Add(dtgr);
                }

                dtgresp = dtg.NewRow();
                dtgresp[0] = "Total";
                dtgresp[1] = " ";
                dtgresp[2] = dsquestions.Tables[4].Rows[0][0].ToString();
                dtg.Rows.Add(dtgresp);


                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(ycolor));
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                worksheet1.Cells[intQrowdata, 1].LoadFromDataTable(dtg, true);


                worksheet1.Cells[intQrowdata, 1, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet1.Cells[intQrowdata, 1, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet1.Cells[intQrowdata, 1, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                worksheet1.Cells[intQrowdata, 2, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                worksheet1.Cells[intQrowdata, 2, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet1.Cells[intQrowdata, 2, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                worksheet1.Cells[intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 1, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 1, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 3].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                worksheet1.Cells[intQrowdata, 3, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[intQrowdata, 3, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);



                intQrowdata = intQrowdata + dtg.Rows.Count + 2;



                dtg.Clear();



                xlPackage.Save();

                if (File.Exists(strXLFileName))
                {

                    string name_survey = dsquestions.Tables[0].Rows[0][2].ToString().Replace(" ", "");
                    name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                    name_survey = name_survey.Replace("/", "");
                    name_survey = name_survey.Replace("'", "");
                    name_survey = name_survey.Replace("<", "");
                    name_survey = name_survey.Replace(">", "");
                    name_survey = name_survey.Replace("\"", "");
                    Response.ContentType = "application/ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + "_Report" + ".xlsx");
                    Response.TransmitFile(strXLFileName);
                    Response.End();

                }
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            //set workbook object to null
            //if (workBook != null)
            //    workBook = null;
        }
    }

    public void WriteRawDataToExcelSheet()
    {
        try
        {
            // int surveyid = 1;
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

            int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

            DataSet dssurveydata = surcore.getpollinfo(surveyID, responselimit);

            //create FileInfo object  to read you ExcelWorkbook
            string strDirector = ConfigurationManager.AppSettings["RootPath"].ToString();
            if (!Directory.Exists(strDirector + dssurveydata.Tables[0].Rows[0][3].ToString() + @"\" + dssurveydata.Tables[0].Rows[0][0].ToString()))
            {
                Directory.CreateDirectory(strDirector + dssurveydata.Tables[0].Rows[0][3].ToString() + @"\" + dssurveydata.Tables[0].Rows[0][0].ToString());
            }

            string surveyname = "";
            surveyname = "RawDataExportComplete_" + dssurveydata.Tables[0].Rows[0][2].ToString() + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
            surveyname = surveyname.Replace(" ", "");
            surveyname = Regex.Replace(surveyname, "[^\\w\\.@-]", "");
            surveyname = surveyname.Replace("/", "");
            surveyname = surveyname.Replace("'", "");
            surveyname = surveyname.Replace("<", "");
            surveyname = surveyname.Replace(">", "");
            surveyname = surveyname.Replace("\"", "");

            string strXLFileName = strDirector + dssurveydata.Tables[0].Rows[0][3].ToString() + "\\" + dssurveydata.Tables[0].Rows[0][0].ToString() + "\\" + surveyname;

            var file = new FileInfo(strXLFileName);

            using (ExcelPackage xlPackage = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Sheet1");

                //Select a range of cells to insert the data and use the LoadFromDataTable method to write the
                //data               

                worksheet.Cells[1, 1, 600, 600].Clear();

                int colval = 0;

                worksheet.Cells[2, colval + 1].Value = "Submit Time";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                worksheet.Cells[2, colval + 1].Value = "Serial number";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                worksheet.Cells[2, colval + 1].Value = "Country";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                worksheet.Cells[2, colval + 1].Value = "Statename";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                worksheet.Cells[2, colval + 1].Value = "Device";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                worksheet.Cells[2, colval + 1].Value = "Pageurl";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                colval = colval + 1;

                //getting questions
                worksheet.Cells[2, colval + 1].Value = "Question: " + dssurveydata.Tables[0].Rows[0][1].ToString();
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                colval = colval + 1;


                //getting leads
                worksheet.Cells[2, colval + 1].Value = "Email";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                colval = colval + 1;

                //getting leads
                worksheet.Cells[2, colval + 1].Value = "Phone";
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                worksheet.Column(colval + 1).Width = 30;
                colval = colval + 1;


                if ((dssurveydata.Tables[0].Rows[0][4].ToString() == "1"))
                {
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    if (dssurveydata.Tables[0].Rows[0][5].ToString() == "1")
                    {
                        worksheet.Cells[2, colval + 2, 2, colval + 2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet.Cells[2, colval + 2, 2, colval + 2].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                    }
                    else
                    {
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                    }
                }
                else
                {
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                }

                colval = colval + 1;

                //  DataTable dtmultiselectques;
                if (dssurveydata.Tables[0].Rows[0][4].ToString() == "2")
                {

                    for (int i2 = 0; i2 < dssurveydata.Tables[1].Rows.Count; i2++)
                    {
                        worksheet.Cells[2, colval + 1].Value = dssurveydata.Tables[1].Rows[i2][1].ToString();
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                        worksheet.Column(colval + 1).Width = 30;
                        colval = colval + 1;
                    }
                }


                //displaying other option
                if ((dssurveydata.Tables[0].Rows[0][4].ToString() == "1") || (dssurveydata.Tables[0].Rows[0][4].ToString() == "2"))
                {
                    if (dssurveydata.Tables[0].Rows[0][5].ToString() == "1")
                    {
                        worksheet.Cells[2, colval + 1].Value = "Comments, Please specify";
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue);
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.WrapText = true;
                        worksheet.Cells[2, colval + 1, 2, colval + 1].Style.Font.Bold = true;
                        worksheet.Column(colval + 1).Width = 30;
                        colval = colval + 1;
                    }
                }

                worksheet.Cells[2, 1, 2, colval].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, 1, 2, colval].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                worksheet.Cells[2, 1, 2, colval].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, 1, 2, colval].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);




                worksheet.Cells[2, colval, 2, colval].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[2, colval, 2, colval].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                //loading data

                DataSet dsrespdata = surcore.getPollRawdatarespondents(surveyID);
                int rowvaldata = 3;
                int linecolvaldata = 0;
                for (int resrow = 0; resrow < dsrespdata.Tables[0].Rows.Count; resrow++)
                {
                    int colvaldata = 0;
                    string submitdateandtime = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(dsrespdata.Tables[0].Rows[resrow][2].ToString()), loggedInUserInfo.StandardBIAS, true).ToString("dd-MMM-yyyy hh:mm tt");
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = submitdateandtime;
                    colvaldata = colvaldata + 1;
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsrespdata.Tables[0].Rows[resrow][0].ToString();
                    colvaldata = colvaldata + 1;
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsrespdata.Tables[0].Rows[resrow][3].ToString();
                    colvaldata = colvaldata + 1;
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsrespdata.Tables[0].Rows[resrow][4].ToString();
                    colvaldata = colvaldata + 1;
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsrespdata.Tables[0].Rows[resrow][5].ToString();
                    colvaldata = colvaldata + 1;
                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsrespdata.Tables[0].Rows[resrow][6].ToString();
                    colvaldata = colvaldata + 1;

                    System.Data.DataTable dtQ1 = dsrespdata.Tables[1];

                    System.Data.DataTable dtQ2 = dsrespdata.Tables[2];

                    if (dssurveydata.Tables[0].Rows[0][4].ToString() == "1")
                    {

                        DataRow[] dr1 = dtQ1.Select("fk_respondentid='" + dsrespdata.Tables[0].Rows[resrow][1] + "'");

                        if (dr1.Length > 0)
                        {
                            worksheet.Cells[rowvaldata, colvaldata + 1].Value = dr1[0].ItemArray[2].ToString();
                        }
                        else
                        {
                            worksheet.Cells[rowvaldata, colvaldata + 1].Value = "";
                        }


                        if (dssurveydata.Tables[0].Rows[0][5].ToString() == "1")
                        {
                            colvaldata = colvaldata + 1;
                            DataRow[] dr2 = dtQ2.Select("pk_respondentid='" + dsrespdata.Tables[0].Rows[resrow][1] + "'");

                            if (dr2.Length > 0)
                            {
                                worksheet.Cells[rowvaldata, colvaldata + 1].Value = dr2[0].ItemArray[1].ToString();
                            }
                            else
                            {
                                worksheet.Cells[rowvaldata, colvaldata + 1].Value = "";
                            }
                        }
                    }
                    else if (dssurveydata.Tables[0].Rows[0][4].ToString() == "2")
                    {
                        colvaldata = colvaldata + 1;

                        for (int drv = 0; drv < dssurveydata.Tables[1].Rows.Count; drv++)
                        {
                            DataRow[] dr1 = dtQ1.Select("answerid='" + dssurveydata.Tables[1].Rows[drv][0] + "' and fk_respondentid = '" + dsrespdata.Tables[0].Rows[resrow][1] + "'");

                            if (dr1.Length > 0)
                            {
                                worksheet.Cells[rowvaldata, colvaldata + 1].Value = "Yes";
                            }
                            else
                            {
                                worksheet.Cells[rowvaldata, colvaldata + 1].Value = "";
                            }
                            colvaldata = colvaldata + 1;
                        }

                        if (dssurveydata.Tables[0].Rows[0][5].ToString() == "1")
                        {
                            DataRow[] dr2 = dtQ2.Select("pk_respondentid='" + dsrespdata.Tables[0].Rows[resrow][1] + "'");

                            if (dr2.Length > 0)
                            {
                                worksheet.Cells[rowvaldata, colvaldata + 1].Value = dr2[0].ItemArray[1].ToString();
                            }
                            else
                            {
                                worksheet.Cells[rowvaldata, colvaldata + 1].Value = "";
                            }
                        }

                    }
                    worksheet.Cells[rowvaldata, 1, rowvaldata, colvaldata].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells[rowvaldata, 1, rowvaldata, colvaldata].Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    colvaldata = colvaldata + 1;

                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsrespdata.Tables[0].Rows[resrow]["Phone"].ToString();
                    colvaldata = colvaldata + 1;

                    worksheet.Cells[rowvaldata, colvaldata + 1].Value = dsrespdata.Tables[0].Rows[resrow]["Email"].ToString();
                    linecolvaldata = colvaldata + 1;
                    
                    rowvaldata = rowvaldata + 1;
                }

                worksheet.Cells[dsrespdata.Tables[0].Rows.Count + 2, 1, dsrespdata.Tables[0].Rows.Count + 2, linecolvaldata].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[dsrespdata.Tables[0].Rows.Count + 2, 1, dsrespdata.Tables[0].Rows.Count + 2, linecolvaldata].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);

                worksheet.Cells[3, linecolvaldata, dsrespdata.Tables[0].Rows.Count + 2, linecolvaldata].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet.Cells[3, linecolvaldata, dsrespdata.Tables[0].Rows.Count + 2, linecolvaldata].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                //Then we save our changes so that they are reflected in ou excel sheet.

                xlPackage.Save();

                if (File.Exists(strXLFileName))
                {

                    string name_survey = dssurveydata.Tables[0].Rows[0][1].ToString().Replace(" ", "");
                    name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                    name_survey = name_survey.Replace("/", "");
                    name_survey = name_survey.Replace("'", "");
                    name_survey = name_survey.Replace("<", "");
                    name_survey = name_survey.Replace(">", "");
                    name_survey = name_survey.Replace("\"", "");
                    Response.ContentType = "application/ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + "_Rawdataexport" + ".xlsx");
                    Response.TransmitFile(strXLFileName);
                    Response.End();

                }
            }

        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            //set workbook object to null
            //if (workBook != null)
            //    workBook = null;
        }
    }

    private void FillRawDataForColumn(int colval, ExcelWorksheet worksheet, string QT, DataSet dsgetRawdataAnswersForQues, string AnswerId, DataTable dtRespId, string AnswerOption, string OtherOption)
    {
        // Get specific data for Question, Answer option and answer id
        DataRow[] drForRespId;
        int rownum = 2; // start from 3, reducing 1 for offset from rownum value
        string respondentId;
        string strRespIdQuery;

        if (QT == "6" || QT == "5" || QT == "7" || QT == "8" || QT == "14") // no answer options just a question and answers
        {
            DataRow[] drForAnswerId;
            string colName = "ANSWER_ID=" + AnswerId;
            drForAnswerId = dsgetRawdataAnswersForQues.Tables[0].Select(colName);
            for (int i = 0; i < drForAnswerId.Count(); i++)
            {
                // Get Rownum for respondent id
                respondentId = drForAnswerId[i][2].ToString();
                strRespIdQuery = "RESPONDENT_ID=" + respondentId;
                drForRespId = dtRespId.Select(strRespIdQuery);
                rownum = rownum + Convert.ToInt32(drForRespId[0][7].ToString());

                worksheet.Cells[rownum, colval].Value = drForAnswerId[i][1];

                // reset row num
                rownum = 2;
            }
        }
        else if (QT == "10" || QT == "11" || QT == "12" || QT == "15" || QT == "13")
        {
            // Increase optimization, too many rows iterated reduce rows
            string AnswerOptionwithPadding = "";
            DataRow[] dtTmpRawdataAnswersForOption = null;
            if (QT == "10")
            {
                AnswerOptionwithPadding = "$--$" + AnswerOption;
                string expression = "ANSWER_OPTIONS LIKE '%" + AnswerOptionwithPadding + "%'";
                dtTmpRawdataAnswersForOption = dsgetRawdataAnswersForQues.Tables[0].Select(expression);

            }
            else if (QT == "11")// replace "--" with "$--$"
            {
                AnswerOptionwithPadding = AnswerOption.Replace("--", "$--$");
                string expression = "ANSWER_OPTIONS LIKE '%" + AnswerOptionwithPadding + "%'";
                dtTmpRawdataAnswersForOption = dsgetRawdataAnswersForQues.Tables[0].Select(expression);
            }
            else if (QT == "12")
            {
                AnswerOptionwithPadding = AnswerOption.Replace("-", "$--$");
                string expression = "ANSWER_OPTIONS LIKE '%" + AnswerOptionwithPadding.Substring(0, AnswerOptionwithPadding.IndexOf("$--$")) + "%'";
                dtTmpRawdataAnswersForOption = dsgetRawdataAnswersForQues.Tables[0].Select(expression);

            }
            else if (QT == "13" || QT == "15")
            {
                string expression = "ANSWER_OPTIONS LIKE '%" + AnswerOption + "%'";
                dtTmpRawdataAnswersForOption = dsgetRawdataAnswersForQues.Tables[0].Select(expression);
            }

            for (int i = 0; i < dtTmpRawdataAnswersForOption.Length; i++)
            {
                string Answer_String = dtTmpRawdataAnswersForOption[i][3].ToString();
                bool matchFound = false;
                int strIndex = 0;

                if (QT == "12")
                {
                    // Strip off answer from the middle and only compare with option string
                    // Find first location of $--$

                    int firstIdx = Answer_String.IndexOf("$--$");
                    int secondIdx = (Answer_String).IndexOf("$--$", firstIdx + 4);
                    string tmpStr = Answer_String.Substring(0, firstIdx) + Answer_String.Substring(secondIdx,
                                          Answer_String.Length - secondIdx);

                    if (AnswerOptionwithPadding.Equals(tmpStr))
                        matchFound = true;

                }
                if (QT == "11")
                    if (Answer_String.Equals(AnswerOptionwithPadding))
                        matchFound = true;
                if (QT == "10")
                {
                    if ((strIndex = Answer_String.IndexOf(AnswerOptionwithPadding)) > 0)
                        matchFound = true;
                }
                if (QT == "15" || QT == "13")
                    if (Answer_String.Equals(AnswerOption))
                        matchFound = true;

                if (matchFound)
                {
                    // Get rownum based on respondent id
                    string str_resp_id = "RESPONDENT_ID=" + dtTmpRawdataAnswersForOption[i][2].ToString();
                    drForRespId = dtRespId.Select(str_resp_id);
                    rownum = rownum + Convert.ToInt32(drForRespId[0][7].ToString());
                    if (QT == "11")
                        worksheet.Cells[rownum, colval].Value = "Yes";
                    else if (QT == "10")
                        worksheet.Cells[rownum, colval].Value = Answer_String.Substring(0, strIndex);
                    else if (QT == "15" || QT == "13")
                        worksheet.Cells[rownum, colval].Value = dtTmpRawdataAnswersForOption[i][1].ToString();
                    else // QT = 12
                    {
                        // Find first location of $--$
                        int firstIdx = Answer_String.IndexOf("$--$");
                        int secondIdx = (Answer_String).IndexOf("$--$", firstIdx + 4);
                        worksheet.Cells[rownum, colval].Value = Answer_String.Substring(firstIdx + 4, secondIdx - (firstIdx + 4));
                    }

                    rownum = 2;
                }
            }
        }
        else
        {
            DataRow[] drForAnswerId;

            // Other
            if ((QT == "1" || QT == "2" || QT == "3" || QT == "4") && OtherOption == "1")
            {

                for (int i = 0; i < dsgetRawdataAnswersForQues.Tables[1].Rows.Count; i++)
                {
                    // Get Rownum for respondent id
                    respondentId = dsgetRawdataAnswersForQues.Tables[1].Rows[i][1].ToString();
                    strRespIdQuery = "RESPONDENT_ID=" + respondentId;
                    drForRespId = dtRespId.Select(strRespIdQuery);
                    rownum = rownum + Convert.ToInt32(drForRespId[0][7].ToString());
                    worksheet.Cells[rownum, colval].Value = dsgetRawdataAnswersForQues.Tables[1].Rows[i][0].ToString();
                    // reset row num
                    rownum = 2;
                }
            }
            else
            {
                string colName = "ANSWER_ID=" + AnswerId;
                drForAnswerId = dsgetRawdataAnswersForQues.Tables[0].Select(colName);
                for (int i = 0; i < drForAnswerId.Count(); i++)
                {
                    // Get Rownum for respondent id
                    respondentId = drForAnswerId[i][2].ToString();
                    strRespIdQuery = "RESPONDENT_ID=" + respondentId;
                    drForRespId = dtRespId.Select(strRespIdQuery);
                    //if (drForRespId.Length == 0)
                    //    return; // Possibly an optional question for which respondent was found

                    rownum = rownum + Convert.ToInt32(drForRespId[0][7].ToString());

                    if (QT == "2" && OtherOption == "1")
                        worksheet.Cells[rownum, colval].Value = drForAnswerId[i][3];
                    else if (QT == "2" && OtherOption == "0")
                        worksheet.Cells[rownum, colval].Value = "Yes";
                    else if (QT == "1" || QT == "3" || QT == "4")
                        worksheet.Cells[rownum, colval].Value = drForAnswerId[i][3];
                    else
                        worksheet.Cells[rownum, colval].Value = drForAnswerId[i][1];

                    // reset row num
                    rownum = 2;
                }
            }
        }
    }

    public void ExportVotesToExcelSheet(string strExport, string optionname)
    {
        try
        {
            //create FileInfo object  to read you ExcelWorkbook
            //  int surveyid = 1;
            DataSet dsquestions = null;

            if (strExport == "Country")
            {
                var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

                int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());
                dsquestions = surcore.getpollinfo(surveyID, responselimit);
            }
            else if (strExport == "State")
            {
                var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

                int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

                dsquestions = surcore.getpollcountryinfo(surveyID, optionname, responselimit);
            }
            else if (strExport == "device")
            {
                var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

                int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());
                dsquestions = surcore.getpolldevice(surveyID, responselimit);
            }
            else if (strExport == "OS")
            {
                var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

                int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

                dsquestions = surcore.getpolldeviceinfo(surveyID, optionname, responselimit);
            }
            else if (strExport == "mobiledevice")
            {
                var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

                int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

                dsquestions = surcore.getpollmobiledeviceinfo(surveyID, optionname, responselimit);
            }
            else if (strExport == "mobdevos")
            {
                var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

                int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

                dsquestions = surcore.getpollmobileosinfo(surveyID, optionname, responselimit);
            }
            else if (strExport == "urlpage")
            {
                if (strurlpage != "")
                {
                    var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                    LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                    DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

                    int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

                    dsquestions = surcore.getpollpageinfo(surveyID, optionname, responselimit);
                }
                else
                {
                    var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                    LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                    DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

                    int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());

                    dsquestions = surcore.getpollPage(surveyID, responselimit);
                }
            }
            else
            {
                var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

                int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());
                dsquestions = surcore.getpolldeviceinfo(surveyID, optionname, responselimit);
            }

            string strDirector = ConfigurationManager.AppSettings["RootPath"].ToString();
            if (!Directory.Exists(strDirector + dsquestions.Tables[0].Rows[0][3].ToString() + @"\" + dsquestions.Tables[0].Rows[0][0].ToString()))
            {
                Directory.CreateDirectory(strDirector + dsquestions.Tables[0].Rows[0][3].ToString() + @"\" + dsquestions.Tables[0].Rows[0][0].ToString());
            }

            string surveyname = "";
            surveyname = dsquestions.Tables[0].Rows[0][2].ToString() + "_locationSummary_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
            surveyname = surveyname.Replace(" ", "");
            surveyname = Regex.Replace(surveyname, "[^\\w\\.@-]", "");
            surveyname = surveyname.Replace("/", "");
            surveyname = surveyname.Replace("'", "");
            surveyname = surveyname.Replace("<", "");
            surveyname = surveyname.Replace(">", "");
            surveyname = surveyname.Replace("\"", "");

            string strXLFileName = strDirector + dsquestions.Tables[0].Rows[0][3].ToString() + "\\" + dsquestions.Tables[0].Rows[0][0].ToString() + "\\" + surveyname;


            var file = new FileInfo(strXLFileName);


            using (ExcelPackage xlPackage = new ExcelPackage(file))
            {

                string ycolor = "#FFBF00";
                string yblue = "#C6CFEB";

                int TRid = 1;

                ExcelWorksheet worksheet1 = xlPackage.Workbook.Worksheets.Add("Location data");

                worksheet1.Cells[1, 1, 800, 800].Clear();

                worksheet1.Column(1).Width = 30;
                worksheet1.Column(2).Width = 10;
                worksheet1.Column(3).Width = 10;


                worksheet1.Cells[2, 1, 2, 3].Merge = true;
                worksheet1.Cells[2, 1, 2, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet1.Cells[2, 1, 2, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                worksheet1.Cells[2, 1, 2, 3].Style.WrapText = true;
                worksheet1.Cells[2, 1, 2, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet1.Cells[2, 1, 2, 3].Style.Font.Bold = true;
                worksheet1.Cells[2, 1, 2, 3].Value = "Poll Name:" + dsquestions.Tables[0].Rows[0][2].ToString();

                worksheet1.Cells[2, 3, 2, 3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[2, 3, 2, 3].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                worksheet1.Cells[2, 1, 2, 3].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[2, 1, 2, 3].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);


                //  DataSet dsquestions = surcore.getpollinfo(surveyid);

                int intQrowdata = 4;


                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Merge = true;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(yblue));
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Style.Font.Bold = true;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Value = StripTagsRegex(dsquestions.Tables[0].Rows[0][1].ToString()).Trim();

                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                worksheet1.Cells[intQrowdata, 3, intQrowdata, 5].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[intQrowdata, 3, intQrowdata, 5].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                intQrowdata = intQrowdata + 1;


                DataTable dtg = new DataTable();
                dtg.TableName = "dtQT" + dsquestions.Tables[0].Rows[0][4].ToString();
                DataRow dtgr;
                DataRow dtgresp;

                if (strExport == "Country")
                {
                    dtg.Columns.Add("Country");
                }
                else if (strExport == "State")
                {
                    dtg.Columns.Add("State");
                }
                else if (strExport == "device")
                {
                    dtg.Columns.Add("Device");
                }
                else if (strExport == "OS")
                {
                    dtg.Columns.Add("Operating System");
                }
                else if (strExport == "mobiledevice")
                {
                    dtg.Columns.Add("Device");
                }
                else if (strExport == "mobdevos")
                {
                    dtg.Columns.Add("Operating System");
                }
                else
                {
                    dtg.Columns.Add("Brand");
                }

                dtg.Columns.Add("Views");
                dtg.Columns.Add("% of Views");
                dtg.Columns.Add("Votes");
                dtg.Columns.Add("% of Votes");


                //DataRow[] drdata = dsquestions.Tables[3].Select("pollid = " + surveyID);

                //for (int irg1 = 0; irg1 < drdata.Length; irg1++)
                //{
                //    dtgr = dtg.NewRow();
                //    dtgr[0] = drdata[irg1][0].ToString();
                //    dtgr[1] = drdata[irg1][1].ToString(); 
                //    dtgr[2] = drdata[irg1][2].ToString()+ "%";
                //    dtgr[3] = drdata[irg1][3].ToString();
                //    dtgr[4] = drdata[irg1][4].ToString() + "%";
                //    dtg.Rows.Add(dtgr);
                //}


                for (int irg1 = 0; irg1 < dsquestions.Tables[3].Rows.Count; irg1++)
                {
                    dtgr = dtg.NewRow();
                    dtgr[0] = dsquestions.Tables[3].Rows[irg1][0].ToString();
                    dtgr[1] = dsquestions.Tables[3].Rows[irg1][1].ToString();
                    dtgr[2] = dsquestions.Tables[3].Rows[irg1][2].ToString() + "%";
                    dtgr[3] = dsquestions.Tables[3].Rows[irg1][3].ToString();
                    dtgr[4] = dsquestions.Tables[3].Rows[irg1][4].ToString() + "%";
                    dtg.Rows.Add(dtgr);
                }


                dtgresp = dtg.NewRow();
                dtgresp[0] = "Total";
                dtgresp[1] = " ";
                dtgresp[2] = " ";
                dtgresp[3] = " ";
                dtgresp[4] = dsquestions.Tables[4].Rows[0][0].ToString();
                dtg.Rows.Add(dtgresp);


                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(ycolor));
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet1.Cells[intQrowdata, 1, intQrowdata, 5].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);


                worksheet1.Cells[intQrowdata, 1].LoadFromDataTable(dtg, true);


                worksheet1.Cells[intQrowdata, 1, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet1.Cells[intQrowdata, 1, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet1.Cells[intQrowdata, 1, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 1].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                worksheet1.Cells[intQrowdata, 2, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                worksheet1.Cells[intQrowdata, 2, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 5].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet1.Cells[intQrowdata, 2, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 5].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);

                worksheet1.Cells[intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 1, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 5].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 1, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 5].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                worksheet1.Cells[intQrowdata, 5, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 5].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                worksheet1.Cells[intQrowdata, 5, intQrowdata + dsquestions.Tables[2].Rows.Count + 1, 5].Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);



                intQrowdata = intQrowdata + dtg.Rows.Count + 2;



                dtg.Clear();



                xlPackage.Save();

                if (File.Exists(strXLFileName))
                {

                    string name_survey = dsquestions.Tables[0].Rows[0][2].ToString().Replace(" ", "");
                    name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
                    name_survey = name_survey.Replace("/", "");
                    name_survey = name_survey.Replace("'", "");
                    name_survey = name_survey.Replace("<", "");
                    name_survey = name_survey.Replace(">", "");
                    name_survey = name_survey.Replace("\"", "");
                    Response.ContentType = "application/ms-excel";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + "_LocationReport" + ".xlsx");
                    Response.TransmitFile(strXLFileName);
                    Response.End();

                }
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            //set workbook object to null
            //if (workBook != null)
            //    workBook = null;
        }
    }

    public DataSet getpollSurveyAnalytics(int pollid)
    {
        try
        {

            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_pollsurveyAnalytics", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@pollID", SqlDbType.Int)).Value = pollid;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dssurveyprofile = new DataSet();
            sda.Fill(dssurveyprofile);
            con.Close();
            return dssurveyprofile;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    protected void drpSource_Click(object sender, EventArgs e)
    {
        //string selectedvalue = "";
        //selectedvalue = drpSource.SelectedValue;
        //Session["SOVotes"] = selectedvalue;
        //strcountryname = "";
        //strurlpage = "";
        //strdevicename = "";
        //if (selectedvalue == "location")
        //{
        //    ddlcountry.Items.Clear();
        //    lnklocation_Click(sender, e);
        //}
        //else if (selectedvalue == "device")
        //{
        //    ddlcountry.Items.Clear();
        //    lnkdevice_Click(sender, e);
        //}
        //else if (selectedvalue == "urlpage")
        //{
        //    ddlcountry.Items.Clear();
        //    lnkPage_Click(sender, e);
        //}
        //else
        //{
        //    ddlcountry.Items.Clear();
        //    ddlcountry.Visible = false;
        //    chartContainerParent.InnerHtml = "";
        //    rpnl_reportsdata.Controls.Clear();
        //    BindReportPage();
        //}
        if (drpSource.SelectedValue == "")
        {
            Session["defaultchart"] = "default";
        }
    }

}