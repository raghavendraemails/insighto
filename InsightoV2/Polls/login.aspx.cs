﻿using System;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Resources;
using System.Text.RegularExpressions;
using System.Data;
using System.Web.UI;

public partial class new_polls_login : System.Web.UI.Page
{
    string country;
    string strsource;
    string strterm;
    SurveyCore surcore = new SurveyCore();
    PollCreation Pc = new PollCreation();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ibtnLogin_Click(object sender, EventArgs e)
    {
        if (!IsPostBack)
            return;

        string alrt = "Please enter";
        this.lblInvaildAlert.Visible = true;
        bool flag = false;
        if (string.IsNullOrEmpty(txtEmailAdd.Text.Trim()) || txtEmailAdd.Text.ToLower() == "email address")
        {
            alrt += " email address.";
            lblInvaildAlert.Text = alrt;
        }
        else
        {
            Regex reLenient = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            bool isEmail = reLenient.IsMatch(txtEmailAdd.Text);
            if (!isEmail)
            {
                alrt += " valid email address.";
                lblInvaildAlert.Text = alrt;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "key001", "openModal('#dialog')", true);
            }
            else if (Convert.ToString(txtPassword.Text).Length == 0)
            {
                alrt += " password.";
                lblInvaildAlert.Text = alrt;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "key001", "openModal('#dialog')", true);
            }
            flag = true;
        }

        if (flag && Convert.ToString(txtEmailAdd.Text).Length > 0 && Convert.ToString(txtPassword.Text).Length > 0)
        {
            // To check authenticated user       
            var userService = ServiceFactory.GetService<UsersService>();
            var user = userService.getAuthenticatedUser(txtEmailAdd.Text, txtPassword.Text);

            if (user == null)
            {
                lblInvaildAlert.Text = "Invalid Email Address or Password.";
                lblInvaildAlert.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "key001", "openModal('#dialog')", true);
            }
            else if (Utilities.ToEnum<SurveyStatus>(user.STATUS) == SurveyStatus.InActive || ValidationHelper.GetInteger(user.DELETED, 0) == 1)
            {
                lblInvaildAlert.Text = "Your Account is de-activated.";
                lblInvaildAlert.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "key001", "openModal('#dialog')", true);
            }
            else
            {
                userService.ProcessUserLogin(user);

                DataSet dsteleuser = surcore.getteleuser(user.USERID);

                if (dsteleuser.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
                {
                    //Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                    Response.Redirect("MyPolls.aspx");
                }
                else
                {
                    if (user.TIME_ZONE == null) //Timezone is null for first user login. hence redirecting to welcome page
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    }
                    else if (user.RESET == 1) // Password reset value is 1 for first login. hence redirectiong to chanepassword page
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserChangePasswordPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    }
                    else // on successful login user is redirected to Myaccounts page
                    {
                        DataSet dsactivity = surcore.getPollRespondentLimit(user.USERID);
                        if (dsactivity.Tables[3].Rows[0][0].ToString() == "survey")
                        {
                            //Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                            string RedirectUrl = EncryptHelper.EncryptQuerystring("MyAccounts.aspx", "surveyFlag=" + user.SURVEY_FLAG);
                            string strurl = "https://www.insighto.com/" + RedirectUrl;
                            Response.Redirect(strurl);

                        }
                        else
                        {
                            //Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyPollPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                        }
                    }
                }
            }
        }
        else
        {


        }
    }
}