﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.geoip;
using System.Text;
using System.Configuration;
using Insighto.Data;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Net;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Serialization;

public partial class Polls_CheckPollAnswered : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int pollId = 0;
    int parentpollId = 0;
    DataSet pollInfo = new DataSet();
    DataTable dtSettingsInfo = new DataTable();
    PollCreation Pc = new PollCreation();
    string resultaccess;
    DataTable tbAnswerOption = new DataTable();
    DataSet licInfo = new DataSet();
    int respondentId;
    int answerId = 0, questionType = 1;
    string device;
    string mobileBrand = "";
    ListItem answerOption;
    string platform = "", mobilePlatform = "";
    public String statusCode { get; set; }
    public String statusMessage { get; set; }
    public String IpAddress { get; set; }
    public String countryCode { get; set; }
    public String countryName { get; set; }
    public String regionName { get; set; }
    public String cityName { get; set; }
    public String zipCode { get; set; }
    public String latitude { get; set; }
    public String longitude { get; set; }
    public String timeZone { get; set; }
    // string globalurl = "http://insighto.com:8010";
    string globalurl;
    string foldername = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        globalurl = ConfigurationSettings.AppSettings["RootURL"].ToString();


        //if (Request.QueryString["key"] != null)
        //{
        //    typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

        //    if (typeHt.Contains("ParentPollId"))
        //        parentpollId = Convert.ToInt32(typeHt["ParentPollId"].ToString());
        //    else
        //        parentpollId = Convert.ToInt32(typeHt["PollId"].ToString());

        //    DataSet dspollid = Pc.getPollInfofromparent(parentpollId);
        //    if (dspollid.Tables[0].Rows.Count > 0)
        //    {
        //        pollId = Convert.ToInt32(dspollid.Tables[0].Rows[0][0].ToString());
        //    }
        //    else
        //    {
        //        pollId = parentpollId;
        //    }
        //}
        if (Request.QueryString["pollid"] != null) {
            Int32.TryParse(Request.QueryString["pollid"], out pollId);
            if (pollId > 0) {
                if (Request.Cookies["respondent" + pollId.ToString()] != null) {
                    Int32.TryParse(Request.Cookies["respondent" + pollId.ToString()].Value, out respondentId);
                    if (respondentId > 0)
                    {
                        pollanswered.Text = "answered";
                        respondentid.Text = respondentId.ToString();
                        //HttpCookie respCookie = new HttpCookie("respondent" + pollId.ToString());
                        //respCookie.Value = respondentId.ToString();
                        //respCookie.Expires = DateTime.Now.AddDays(10);
                        ////respCookie.Domain = "127.0.0.1";
                        //Response.Cookies.Add(respCookie);
                    }
                }
            }
        }

        string url = ConfigurationManager.AppSettings["RootURL"].ToString() + EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId + "&Embed=" + "Emb");
        Response.RedirectPermanent(url);
        Response.End();
        //HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
        //req.Referer = ConfigurationManager.AppSettings["RootURL"].ToString();
        
        //HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
        //Stream stream = resp.GetResponseStream();
        //StreamReader reader = new StreamReader(stream);
        //string responsefromserver = reader.ReadToEnd();
        //pollanswered.Text = responsefromserver;
        //respondentid.Text =  resp.GetResponseHeader("Set-Cookie");

    }
}