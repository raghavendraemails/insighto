﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="registration.aspx.cs" Inherits="new_polls_registration" MasterPageFile="~/Home.Master"%>
<%@ Register TagPrefix="uc" TagName="PollsHeader" Src="~/UserControls/PollsHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc:PollsHeader ID="PollsHeader" runat="server" />
    <div> 

      <div class="breadcrumbs-v3 img-v1 text-center">
        <div class="container">
       <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;">
            <span>SIGN UP </span></h2>
        </div>
      </div>
      <div>

        <div class="container content">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="reg-header"> 
                  <!--<h2 style="color:#333;font-weight:bold;margin: 20px 0px 37px 0px; text-align:center;">Get a 14-Day Free Trial</h2>-->
                  <h2 style="color:#1035A2 !important;">Register a new account</h2>
                  <!--<p>(No Credits required)</p>-->
                  <p>Already Signed Up? Click <a href="login.aspx" class="color-green">Sign In</a> to login your account.</p>
                </div>
                <label>Name <span class="color-red">*</span></label>
                <asp:TextBox ID="txtName" class="form-control" runat="server"></asp:TextBox>
                <asp:Label ID="lblvalidmsg" Visible="false" runat="server" ForeColor="Red" Font-Size="12" Font-Bold="false"></asp:Label>

                <label class="margin-top-20">Email Address <span class="color-red">*</span></label>
                <input type="text" class="form-control " runat="server" id="txtEmail"/>
                <label id="lblvalidmsgemail" runat="server" style="color:Red;font-family:Arial;font-size:12px; font-weight:normal;"></label>
                
                <div class="row">
                  <div class="col-sm-6">
                    <label class="margin-top-20">Password <span class="color-red">*</span></label>

                    <input type="password" class="form-control " runat="server" id="txtpwd"/>
                    <label id="lblvalidmsgpwd" runat="server" style="color:Red;font-family:Arial;font-size:12px;font-weight:normal "></label>

                  </div>
                  <div class="col-sm-6">
                    <label class="margin-top-20">Confirm Password <span class="color-red">*</span></label>
                    <input type="password" class="form-control " runat="server" id="txtcpwd"/>
                    <label id="lblvalidmsgrepwd" runat="server" style="color:Red;font-family:Arial;font-size:12px;font-weight:normal"></label>
                  </div>
                </div>
                <div class="row">
            	    <div class="col-md-12">
                    <div>By signing up, you agree to the <a href="../termsofuse.aspx" target="_blank">Terms &amp; Conditions</a> and the <a href="../privacy-policy.aspx" target="_blank">Privacy Policy</a></div>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <label>
                      <asp:CheckBox runat="server" ID="ckbFreeTrial" Checked="true"></asp:CheckBox>
                      <span style="font-weight:normal !important;">Get a 14 day Free Trial</span> </label>
                    <br><br>
                    <asp:Button ID="btnSFT" OnClick="btnSFT_Click" CausesValidation="false" runat="server" Text="Go to  Insighto Polls" class="btn-u btn btn-lg" />
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 alert-danger">
                    <asp:Label ID="lblErrorMsg" Visible="false" runat="server"></asp:Label>
                      </div>
                    </div>
                <br>
            </div>
          </div>
        </div>
        <!--/container--> 
        <script type="text/javascript">
            $(document).ready(function () { $("#lisignup").attr("class", "active"); });
        </script> 
        <!-- CSS Page Style -->
        <link rel="stylesheet" href="../assets/css/pages/page_pricing.css">
      </div>
    </div>
<!--/wrapper--> 
</asp:Content>