﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="new_polls_login"  MasterPageFile="~/Home.master"%>
<%@ Register TagPrefix="uc" TagName="PollsHeader" Src="~/UserControls/PollsHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc:PollsHeader ID="PollsHeader" runat="server" />
    <div> 

      <div class="breadcrumbs-v3 img-v1 text-center">
        <div class="container">
       <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;">
            <span>Login </span></h2>
        </div>
      </div>
      <div class="container content">
        <div class="row">
          <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
              <div class="reg-header">
                <h2 style="color:#1035A2 !important;">GO TO YOUR ACCOUNT</h2>
              </div>
              <div class="input-group margin-bottom-20"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <asp:TextBox ID="txtEmailAdd" runat="server" placeholder="Email" class="form-control"></asp:TextBox>
              </div>
              <div class="input-group margin-bottom-20"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <%--<input type="password" placeholder="Password" class="form-control">--%>
                  <asp:TextBox TextMode="Password" ID="txtPassword" runat="server" placeholder="Password" class="form-control"></asp:TextBox>
              </div>
              <div class="row">
                <div class="col-md-12 checkbox">
                  <p>
                    By signing up, you agree to the <a href="../termsofuse.aspx" target="_blank">Terms & Conditions</a> and the <a href="../privacy-policy.aspx" target="_blank">Privacy Policy</a></p>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12" align="left">
                  <%--<button class="btn-u" type="submit">Login</button>--%>
                    <asp:Button ID="ibtnLogin" OnClick="ibtnLogin_Click" class="btn-u" Text="Login" ToolTip="Login" runat="server"/>
                </div>
              </div>
                <div class="row">
                <div class="col-md-12 center-block alert-danger">
                <asp:Label ID="lblInvaildAlert" class="lblRequired " runat="server" Visible="False"
                                                        Text="In valid User Name or Password" meta:resourcekey="lblInvaildAlertResource1"></asp:Label></div> 
                </div>
                  
              <hr>
              <h4>Forgot your Password ?</h4>
                <%--<p>No worries, <a class="color-green" data-toggle="modal" data-target="#modal" href="/ForgotPassword.aspx">Click here</a> to reset your password.</p>--%>
                <p>No worries, <a class="color-green" href="/ForgotPassword.aspx">Click here</a> to reset your password.</p>
          </div>
        </div>
        <!--/row--> 
      </div>

    </div>
    <!--/wrapper--> 
</asp:Content>