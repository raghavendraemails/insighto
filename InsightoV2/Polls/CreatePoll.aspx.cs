﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Text;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;

public partial class Polls_new_design_CreatePoll : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    public int pollId = 0;
    string strpolltype = "";
    PollCreation Pc = new PollCreation();
    DataSet dsAnswerOptions = null;
    DataTable dtAnswerOptions = null;
    DataTable dtAnswerids = new DataTable();
    List<string> list = new List<string>();
    DataTable tbAnswerOption = new DataTable();
    DataSet pollInfo = new DataSet();
    DataTable dtSettingsInfo = new DataTable();
    string videoText = "";
    string imageText = "";
    public string strfilename, strfileextension;
    int questionType = 1;
    int comments = 0;
    int answerId, uniqueRespondent;
    int phonerequired = 0;
    int emailrequired = 0;
    int showsubmitbutton = 0;
    int count = 0;
    string mode;
    string logoFileName = "";
    string googletext = "", fbtext = "", twittertext = "";
    DataTable questionInfo = new DataTable();
    DataTable imag = new DataTable();
    string resUrl;
    string insightoLink, pollLink, pollLaunchUrl;
    private MailMessage mailMessage = null;
    private SmtpClient smtpMail = null;
    string questionbold = "", questionitalic = "", questionul = "", questionalign = "", questionfsize = "12", pollbgcolor = "ffffff", questioncolor = "000000", questionbgcolor = "E7E7E7";
    string answercolor = "000000", answeritalic = "", answerul = "", answeralign = "", answerfsize = "12", answerbold = "";
    string votebgcolor = "2196F3", votecolor = "ffffff", voteitalic = "", votebold = "bold", voteul = "", votefsize = "13", votealign = "";
    string msgbgcolor = "", resultaccess = "", msgitalic = "", msgbold = "", msgul = "", msgfsize = "", msgalign = "", msgcolor = "";
    string launchto;
    int votecount = 0;
    int arrlength;
    string strRootURL;
    string foldername = "";
    string pollType = "";
    public string previewurl = "";
    int chartwidth = 0;
    int chartheight = 0;
    string rootURL = ConfigurationManager.AppSettings["RootURL"].ToString();
    string parentpolltype = "";
    string bgcolour = "";
    string pollstatusicon = "";
    int autoadjustdimensions = 0;
    int issamethankyoumessageused = 1;
    public string bgcolourstandard = "#F5F7F9";
    [Flags]
    enum LaunchMethod
    {
        Embedthr = 1
    }

    string pollImagesFolder = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        pollImagesFolder = ConfigurationManager.AppSettings["pollimagesfolder"].ToString();
        Session.Timeout = 90;

        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        if (userInfo == null)
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "RedirectToLogin" + DateTime.Now.Millisecond.ToString(), "<script>window.parent.location.href = '/errorpage.aspx?iserror=true';</script>");
        }
        try
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
                if (typeHt.Contains("polltype"))
                    strpolltype = typeHt["polltype"].ToString();
                if (typeHt.Contains("PollId"))
                    if (ViewState["pollId"] == null)
                    {
                        pollId = Convert.ToInt32(typeHt["PollId"].ToString());
                    }
                    else
                    {
                        pollId = Convert.ToInt32(ViewState["pollId"]);
                    }
                hdnPollID.Value = pollId.ToString();
                ImageDiv.Attributes.Add("pollid", pollId.ToString());
                fuImage.Attributes.Add("pollid", pollId.ToString());
                //fuImage.SkinID = pollId.ToString();
                hdnPollType.Value = strpolltype.ToLower();
                if (typeHt.Contains("launch"))
                    launchto = typeHt["launch"].ToString();
            }

            foldername = @"/polls/images/" + Convert.ToString(pollId);

            dsAnswerOptions = new DataSet();
            dtAnswerOptions = new DataTable();

            HiddenField3.Value = "";
            if (!Page.IsPostBack)
            {
                ckbVotes.Checked = false;
                string script = "$(document).ready(function () { $('[id*=btnSubmit]').click(); });";
                ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);
                AddColumns();
                tbAnswerOption = Pc.getAnswerOptions(pollId);
                hdnRepeaterTxts.Value = Convert.ToString(tbAnswerOption.Rows.Count);
                if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
                {
                    DataSet dspollInfo = Pc.getpollchartinfo(pollId);
                    hdnRepeaterTxts.Value = Convert.ToString(tbAnswerOption.Rows.Count);
                    int i = 0;
                    foreach (DataRow row in tbAnswerOption.Rows)
                    {
                        dtAnswerOptions.Rows.Add(i, System.Web.HttpUtility.HtmlEncode(row["answeroption"].ToString().Replace("\r", "").Replace("\n", "")), "Remove", "Add", Convert.ToInt32(row["pk_answerid"].ToString()), row["InvitationThankYouMessage"].ToString());
                        count++;
                        i++;
                    }


                    if (count == 1)
                    {
                        dtAnswerOptions.Rows.Add(1, "", "Remove", "Add", "");
                    }
                }
                else
                {
                    for (int i = 0; i < 2; i++)
                    {
                        dtAnswerOptions.Rows.Add(i, "", "Remove", "Add", "");
                    }
                    hdnRepeaterTxts.Value = "2";
                    strRootURL = ConfigurationSettings.AppSettings["RootURL"].ToString();
                }
                BindWithRepeater();
                BindDesignStyles();

            }
            getCountries();
            if (hdnactivation.Value == "true")
            {
                var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
                SurveyCore surcore = new SurveyCore();
                DataSet dsauth = surcore.updatelaunchactivationemail(userDetails.UserId);

                lblSuccMsg.Text = "An activation email has been resent to your registered email id.";
                dvSuccessMsg.Visible = true;
            }

            pollInfo = Pc.getPollInfo(pollId);

            if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
            {
                pollType = "Image";
            }
            else if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
            {
                pollType = "Video";
            }
            else
            {
                pollType = "Text";
            }
            hdnPollStatus.Value = pollInfo.Tables[0].Rows[0]["status"].ToString();
            setPollCode(pollInfo, pollId.ToString());
            if (pollInfo.Tables[0].Rows[0]["parentpollid"].ToString() != "")
            {
                hdnParentPollID.Value = pollInfo.Tables[0].Rows[0]["parentpollid"].ToString();
            }
            else
            {
                hdnParentPollID.Value = pollId.ToString();
            }
            hdnPollType.Value = pollType.ToLower();
            DataTable licence = new DataTable();
            licence = Pc.getLicencePoll(userInfo.UserId);
            if (licence.Rows[0]["polllicencetype"].ToString() != "")
            {
                var polllicencetype = licence.Rows[0]["polllicencetype"].ToString().ToLower();
                if (polllicencetype == "professional_freetrial" || polllicencetype == "professional" || polllicencetype == "professional_yearly" || polllicencetype == "professional_monthly" || polllicencetype == "free")
                {
                    //resultsshow.Style.Add("display", "none");
                    sponsorshow.Style.Add("display", "none");
                }
            }


            if (hdnPollStatus.Value == "Draft")
            {
                pollanalytics.Style.Add("display", "none");
            }
            else
            {
                pollanalytics.InnerHtml = "Poll Analytics";
                pollanalytics.Style.Add("display", "inline");
                pollanalytics.Attributes.Add("href", EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + pollId + ""));
            }
            if (pollInfo.Tables[0].Rows[0]["status"].ToString() == "Closed")
            {
                bgcolour = "#BD3A47";
                btnOpenModalDialog.Style.Add("display", "none");
                txtQuestion.ReadOnly = true;
                pollstatusicon = "check_circle";
            }
            else if (pollInfo.Tables[0].Rows[0]["status"].ToString() == "Active")
            {
                bgcolour = "#8ABA3C";
                pollstatusicon = "favorite";
            }
            else if (pollInfo.Tables[0].Rows[0]["status"].ToString() == "Draft")
            {
                bgcolour = "#319AFA";
                pollstatusicon = "drafts";
            }

            headerbar.Style.Add("background-color", bgcolour);
            pollanalyticsbar.Style.Add("background-color", bgcolour + " !important");
            footer.Style.Add("background-color", "#000");
            if (launchto != null)
            {
                divcreatebutton.Style.Add("background-color", "#FFF");
                divdesignbutton.Style.Add("background-color", "#FFF");
                divlaunchbutton.Style.Add("background-color", bgcolourstandard);
                CreateNew.ForeColor = Color.Red;
                DesignNew.ForeColor = Color.Red;
                LaunchNew.ForeColor = Color.Black;
                create.Style.Add("display", "none");
                design.Style.Add("display", "none");
                if (launchto == "launchedpoll")
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "LaunchAfter" + DateTime.Now.Millisecond.ToString(), "<script>$(window).load(function() { $('#MainContent_launchafter').show(); $('#launchoptions').show(); $('#hrefweb').addClass('active'); } );</script>");
                    //launchafter.Style.Add("display", "block");
                    launch.Style.Add("display", "none");
                }
                else
                {
                    launch.Style.Add("display", "block");
                    if (pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() != pollId.ToString())
                    {
                        btnActivateParentPoll.Style.Add("display", "block");
                        btnActivatePoll.Visible = false;
                    }
                    else
                    {
                        btnActivateParentPoll.Style.Add("display", "none");
                        btnActivatePoll.Visible = true;
                    }
                }
                setPollCode(pollInfo, pollId.ToString());
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "ShowTabs" + DateTime.Now.Millisecond.ToString(), "<script>$(document).ready(function() { tabDisplay('launch'); } );</script>");
            }
            else
            {
                divcreatebutton.Style.Add("background-color", bgcolourstandard);
                divdesignbutton.Style.Add("background-color", "#FFF");
                divlaunchbutton.Style.Add("background-color", "#FFF");
                create.Style.Add("display", "block");
                design.Style.Add("display", "none");
                launch.Style.Add("display", "none");
                launchafter.Style.Add("display", "none");

                CreateNew.ForeColor = Color.Black;
                DesignNew.ForeColor = Color.Red;
                LaunchNew.ForeColor = Color.Red;
            }

            string thispollname = lblPollName.Text;
            lblPollName.Text = "" + thispollname;
            //leftsidediv.Style.Add("background", "" + bgcolour + "");
            leftsidediv.Style.Add("background", bgcolourstandard);
            //polltitlediv.Style.Add("background-color", "" + bgcolour + "");
            lblPollName.Style.Add("color", "white");
            pollanalytics.Style.Add("color", "" + bgcolour + "");
            pollanalytics.Style.Add("border-color", "" + bgcolour + "");
            closeicon.Style.Add("color", "" + bgcolour + "");
            closeicon.Style.Add("border-color", "" + bgcolour + "");
            //pollstatus.InnerHtml = pollstatusicon;

            string EmbedUrl1 = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId + "&Embed=Emb&pollsource=slider");
            //EmbedUrl1 = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId + "");
            EmbedUrl1 = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "source=embed&PollId=" + pollId + "");
            string pollLaunchUrl1 = rootURL + EmbedUrl1;

            Session["polldisplaytype"] = pollType;

            previewurl = "<iframe id='IframeLaunch' height='" + txtHeightIframe.Text + "' width='" + txtWidthIframe.Text + "'  scrolling='auto' src='" + pollLaunchUrl1 + "' frameborder='0'   style='-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px; -moz-box-shadow: 4px 4px 14px #000; -webkit-box-shadow: 4px 4px 14px #000; box-shadow: 4px 4px 14px #000;'  ></iframe>";

        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
            //Response.Redirect("/errorpage.aspx");
        }
    }

    public void getCountries()
    {
        ArrayList Arraycountries = new ArrayList();

        DataSet dscountries = Pc.getPollCountries();

        for (int i = 0; i < dscountries.Tables[0].Rows.Count; i++)
        {
            Arraycountries.Add(dscountries.Tables[0].Rows[i][1].ToString());
        }

        HiddenField1.Value = ArrayListToString(ref Arraycountries);
    }

    private string ArrayListToString(ref ArrayList _ArrayList)
    {
        int intCount;
        string strFinal = "";

        for (intCount = 0; intCount <= _ArrayList.Count - 1; intCount++)
        {
            if (intCount > 0)
            {
                strFinal += ",";
            }

            strFinal += _ArrayList[intCount].ToString();
        }

        return strFinal;
    }

    //public static List<string> Countrylist()
    //{
    //    List<string> culturelist = new List<string>();
    //    CultureInfo[] getcultureinfo = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
    //    foreach (CultureInfo getculture in getcultureinfo)
    //    {
    //        RegionInfo Getregioninfo = new RegionInfo(getculture.LCID);

    //        if (!culturelist.Contains(Getregioninfo.EnglishName))
    //        {
    //            culturelist.Add(Getregioninfo.EnglishName);
    //        }
    //    }
    //    culturelist.Sort();
    //    return culturelist;
    //}

    public void buttonClick(int selPollId)
    {

        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        dtSettingsInfo = Pc.getPollSettingsInfo(selPollId);
        pollInfo = Pc.getPollInfo(selPollId);
        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
        {
            pollType = "Image";
        }
        else if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
        {
            pollType = "Video";
        }
        else
        {
            pollType = "Text";
        }
        setPollCode(pollInfo, selPollId.ToString());
        DataTable tbAnswerOption = new DataTable();
        pollInfo = Pc.getPollInfo(selPollId);
        tbAnswerOption = Pc.getAnswerOptions(selPollId);

        if (pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() != "")
        {
            selPollId = Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString());
        }


        pollId = Pc.AddNameandType(pollInfo.Tables[0].Rows[0][2].ToString(), pollType, userDetails.UserId, selPollId);
        ViewState["pollId"] = pollId;
        if (ckbSingleVote.Checked)
        {
            uniqueRespondent = 1;
        }
        else
        {
            uniqueRespondent = 0;
        }
        if (chkEmail.Checked)
        {
            emailrequired = 1;
        }
        if (chkPhone.Checked)
        {
            phonerequired = 1;
        }
        if (ckbSubmit.Checked)
        {
            showsubmitbutton = 1;
        }
        if (chkSameAnswer.Checked)
        {
            issamethankyoumessageused = 1;
        }
        else
        {
            issamethankyoumessageused = 0;
        }
        
        //PollId, Question, VideoCode, Imagelink, Questiontype, Commentreq, uniqueRespondent, votebuttontext, invitationmessage, isemailrequired, isphonerequired, redirecturl, showsubmitbutton, submitbuttontext, issamethankyoumessageused
        Pc.InsertintoPollInfo(pollId, pollInfo.Tables[0].Rows[0][16].ToString(), pollInfo.Tables[0].Rows[0][18].ToString(), pollInfo.Tables[0].Rows[0][19].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()), 0, Convert.ToInt32(pollInfo.Tables[0].Rows[0]["uniquerespondent"].ToString()), txtVoteButtonLabel.Text, txtInvitationMessage.Text, emailrequired, phonerequired, txtRedirectURL.Text, showsubmitbutton, txtSubmitButtonLabel.Text, issamethankyoumessageused);
        if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
        {
            foreach (DataRow row in tbAnswerOption.Rows)
            {
                string answeroption = row["answeroption"].ToString();
                Pc.SaveAnswerOptions(answeroption, pollId, "");
            }
        }


        if (pollType == "Image")
        {
            string sourcefolder = @"images\" + selPollId;
            string destinationfolder = @"images\" + pollId;

            if (!System.IO.Directory.Exists(Server.MapPath(destinationfolder)))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath(destinationfolder));
            }

            string[] files = System.IO.Directory.GetFiles(Server.MapPath(sourcefolder));

            foreach (string file in files)
            {
                string filename = System.IO.Path.GetFileName(file);
                string destFile = System.IO.Path.Combine(Server.MapPath(destinationfolder), filename);
                System.IO.File.Copy(file, destFile, true);
            }

        }


        int countvotes;
        if (dtSettingsInfo.Rows[0]["countvotes"].ToString() == "True")
        {
            countvotes = 1;
        }
        else
        {
            countvotes = 0;
        }

        //  Pc.InsertintoPollSettings(pollId, dtSettingsInfo.Rows[0]["pollbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questionfont"].ToString(), dtSettingsInfo.Rows[0]["questionbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questioncolor"].ToString(), dtSettingsInfo.Rows[0]["questionstyle"].ToString(), dtSettingsInfo.Rows[0]["questionfontweight"].ToString(), dtSettingsInfo.Rows[0]["questionunderline"].ToString(), dtSettingsInfo.Rows[0]["questionalign"].ToString(), dtSettingsInfo.Rows[0]["questionfontsize"].ToString(), dtSettingsInfo.Rows[0]["answerfont"].ToString(), dtSettingsInfo.Rows[0]["answercolor"].ToString(), dtSettingsInfo.Rows[0]["answerfontweight"].ToString(), dtSettingsInfo.Rows[0]["answerunderline"].ToString(), dtSettingsInfo.Rows[0]["answerstyle"].ToString(), dtSettingsInfo.Rows[0]["answeralign"].ToString(), dtSettingsInfo.Rows[0]["answerfontsize"].ToString(), dtSettingsInfo.Rows[0]["votebgcolor"].ToString(), dtSettingsInfo.Rows[0]["votefont"].ToString(), dtSettingsInfo.Rows[0]["votefontsize"].ToString(), dtSettingsInfo.Rows[0]["votestyle"].ToString(), dtSettingsInfo.Rows[0]["voteunderline"].ToString(), dtSettingsInfo.Rows[0]["votecolor"].ToString(), dtSettingsInfo.Rows[0]["votefontweight"].ToString(), dtSettingsInfo.Rows[0]["votealign"].ToString(), dtSettingsInfo.Rows[0]["sponsorlogo"].ToString(), dtSettingsInfo.Rows[0]["sponsortext"].ToString(), dtSettingsInfo.Rows[0]["logolink"].ToString(), dtSettingsInfo.Rows[0]["messagebgcolor"].ToString(), dtSettingsInfo.Rows[0]["messagecolor"].ToString(), dtSettingsInfo.Rows[0]["messagefont"].ToString(), dtSettingsInfo.Rows[0]["messagestyle"].ToString(), dtSettingsInfo.Rows[0]["messageunderline"].ToString(), dtSettingsInfo.Rows[0]["messagealign"].ToString(), dtSettingsInfo.Rows[0]["messagefontsize"].ToString(), dtSettingsInfo.Rows[0]["messagefontweight"].ToString(), dtSettingsInfo.Rows[0]["googletext"].ToString(), dtSettingsInfo.Rows[0]["twittertext"].ToString(), dtSettingsInfo.Rows[0]["fbtext"].ToString(), Convert.ToInt32(dtSettingsInfo.Rows[0]["countvotes"].ToString()), dtSettingsInfo.Rows[0]["sharetext"].ToString(), dtSettingsInfo.Rows[0]["votetext"].ToString());
        //InsertintoPollSettings(pollid, pollbgcolor, questionfont, questionbgcolor, questioncolor, questionstyle, questionfw, questionuline, questionalign,
        //questionfs, answerfont, answercolor, answerfw, answeruline,  answerstyle, answeralign, answerfs, votebgcolor, votefont, votefs, votestyle, voteul,
        //votecolor, votefw, votealign, sponsorlogo, sponsortext,logolink, messagebgcolor, messagecolor, messagefont, messagestyle, messageul,  messagealign, 
        // messagefs, messagefw, googletext, twittertext, fbtext,votecount,sharetext,  votetext

        chartwidth = 0;
        chartheight = 0;
        Int32.TryParse(hdnChartWidth.Value, out chartwidth);
        Int32.TryParse(hdnChartHeight.Value, out chartheight);
        Pc.InsertintoPollSettings(pollId, dtSettingsInfo.Rows[0]["pollbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questionfont"].ToString(), dtSettingsInfo.Rows[0]["questionbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questioncolor"].ToString(), dtSettingsInfo.Rows[0]["questionstyle"].ToString(), dtSettingsInfo.Rows[0]["questionfontweight"].ToString(), dtSettingsInfo.Rows[0]["questionunderline"].ToString(), dtSettingsInfo.Rows[0]["questionalign"].ToString(), dtSettingsInfo.Rows[0]["questionfontsize"].ToString(), dtSettingsInfo.Rows[0]["answerfont"].ToString(), dtSettingsInfo.Rows[0]["answercolor"].ToString(), dtSettingsInfo.Rows[0]["answerfontweight"].ToString(), dtSettingsInfo.Rows[0]["answerunderline"].ToString(), dtSettingsInfo.Rows[0]["answerstyle"].ToString(), dtSettingsInfo.Rows[0]["answeralign"].ToString(), dtSettingsInfo.Rows[0]["answerfontsize"].ToString(), dtSettingsInfo.Rows[0]["votebgcolor"].ToString(), dtSettingsInfo.Rows[0]["votefont"].ToString(), dtSettingsInfo.Rows[0]["votefontsize"].ToString(), dtSettingsInfo.Rows[0]["votestyle"].ToString(), dtSettingsInfo.Rows[0]["voteunderline"].ToString(), dtSettingsInfo.Rows[0]["votecolor"].ToString(), dtSettingsInfo.Rows[0]["votefontweight"].ToString(), dtSettingsInfo.Rows[0]["votealign"].ToString(), "", "", dtSettingsInfo.Rows[0]["logolink"].ToString(), dtSettingsInfo.Rows[0]["messagebgcolor"].ToString(), dtSettingsInfo.Rows[0]["messagecolor"].ToString(), dtSettingsInfo.Rows[0]["messagefont"].ToString(), dtSettingsInfo.Rows[0]["messagestyle"].ToString(), dtSettingsInfo.Rows[0]["messageunderline"].ToString(), dtSettingsInfo.Rows[0]["messagealign"].ToString(), dtSettingsInfo.Rows[0]["messagefontsize"].ToString(), dtSettingsInfo.Rows[0]["messagefontweight"].ToString(), dtSettingsInfo.Rows[0]["googletext"].ToString(), dtSettingsInfo.Rows[0]["twittertext"].ToString(), dtSettingsInfo.Rows[0]["fbtext"].ToString(), countvotes, dtSettingsInfo.Rows[0]["sharetext"].ToString(), dtSettingsInfo.Rows[0]["votetext"].ToString(), chartwidth, chartheight, dtSettingsInfo.Rows[0]["weblinkbgcolor"].ToString());
        if (dtSettingsInfo.Rows[0]["iframewidth"].ToString() != "" && dtSettingsInfo.Rows[0]["iframeheight"].ToString() != "")
        {
            Pc.SaveIframeInfo(pollId, Convert.ToInt32(dtSettingsInfo.Rows[0]["iframewidth"].ToString()), Convert.ToInt32(dtSettingsInfo.Rows[0]["iframeheight"].ToString()), Convert.ToInt32(dtSettingsInfo.Rows[0]["recmwidth"].ToString()), Convert.ToInt32(dtSettingsInfo.Rows[0]["recmheight"].ToString()));
            if (pollType == "Image")
            {
                Pc.SaveImageSpec(pollId, Convert.ToInt32(dtSettingsInfo.Rows[0]["imgrecwidth"].ToString()), Convert.ToInt32(dtSettingsInfo.Rows[0]["imgrecmheight"].ToString()));
            }
        }

    }

    protected void BindDesignStyles()
    {
        string polllink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId + "");
        polllink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "source=embed&PollId=" + pollId + "");
        pollInfo = Pc.getPollInfo(pollId);
        dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
        var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
        var userActivationDetails = ServiceFactory<UsersService>.Instance.GetUserActivationFlag(userSessionDetails.UserId);

        hdnActivationFlag.Value = (userActivationDetails.ACTIVATION_FLAG ?? 0).ToString();

        if (pollInfo.Tables[0].Rows[0]["status"].ToString() != "Draft")
        {
            txtVideo.ReadOnly = true;
        }
        if (pollInfo.Tables[0].Rows[0]["status"].ToString() == "Active")
        {
            hdnPollLink.Value = rootURL + polllink;
        }
        if ((pollInfo.Tables[0].Rows[0]["status"].ToString() == "Active") && (pollInfo.Tables[0].Rows[0]["polltype"].ToString() == "slider"))
        {
            txtVideo.ReadOnly = true;
        }

        imageDivBorder.Style.Add("display", "none");
        videoDivBorder.Style.Add("display", "none");

        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
        {
            ImageDiv.Style.Add("display", "block");
            imagepreview.Style.Add("display", "block");
            imgResults.Style.Add("display", "block");
            if (txtWidthPoll.Text == "")
            {
                txtWidthPoll.Text = "300";
            }
            if (lblRecThemeWidth.Text == "")
            {
                lblRecThemeWidth.Text = "300";
                hdnImageRecWidth.Value = "300";
            }
            if (txtHeightPoll.Text == "")
            {
                txtHeightPoll.Text = "310";
                hdnImageRecHeight.Value = "124";
            }
            if (lblRecThemeHeight.Text == "")
            {
                lblRecThemeHeight.Text = "310";
                hdnRecHeightne.Value = "310";
            }
            if (pollInfo.Tables[0].Rows[0]["imagelink"].ToString() != "")
            {
                imgPreview.Attributes.Add("src", foldername + "\\" + pollInfo.Tables[0].Rows[0][19].ToString());
                imgPreviewResults.Attributes.Add("src", foldername + "\\" + pollInfo.Tables[0].Rows[0][19].ToString());
                fileUploadName.Value = pollInfo.Tables[0].Rows[0]["imagelink"].ToString();
                hdnImageLink.Value = "exist";
                string imagefilelocation = pollImagesFolder + pollId.ToString() + "//" + pollInfo.Tables[0].Rows[0]["imagelink"].ToString();
                System.Drawing.Bitmap bmpPostedImage = new System.Drawing.Bitmap(imagefilelocation);
                hdnImageHeight.Value = bmpPostedImage.Height.ToString();
                hdnImageName.Value = pollInfo.Tables[0].Rows[0]["imagelink"].ToString();
            }
        }
        else if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
        {
            videoDiv.Style.Add("display", "block");
            imagepreview.Style.Add("display", "block");
            imgResults.Style.Add("display", "block");
            if (pollInfo.Tables[0].Rows[0]["videolink"].ToString() != "")
            {
                txtVideo.Text = pollInfo.Tables[0].Rows[0]["videolink"].ToString();
            }
            else
            {
                imgPreview.Attributes.Add("src", "App_Themes/images/video.jpg");
                imgPreviewResults.Attributes.Add("src", "App_Themes/images/video.jpg");
                imgPreview.Attributes.Add("height", "169");
                imgPreview.Attributes.Add("width", "300");
                imgPreviewResults.Attributes.Add("height", "169");
                imgPreviewResults.Attributes.Add("width", "300");
            }
            if (txtWidthPoll.Text == "")
            {
                txtWidthPoll.Text = "300";
            }
            if (lblRecThemeWidth.Text == "")
            {
                lblRecThemeWidth.Text = "300";
            }
            if (txtHeightPoll.Text == "")
            {
                txtHeightPoll.Text = "350";
            }
            if (lblRecThemeHeight.Text == "")
            {
                lblRecThemeHeight.Text = "350";
                hdnRecHeightne.Value = "350";
            }
        }
        else
        {
            if (txtWidthPoll.Text == "")
            {
                txtWidthPoll.Text = "300";
            }
            if (lblRecThemeWidth.Text == "")
            {
                lblRecThemeWidth.Text = "300";
            }
            if (txtHeightPoll.Text == "")
            {
                txtHeightPoll.Text = "180";
                hdnansHeight.Value = "54";
            }
            if (lblRecThemeHeight.Text == "")
            {
                lblRecThemeHeight.Text = "180";
                hdnRecHeightne.Value = "170";
            }
        }

        if (pollInfo.Tables[0].Rows[0]["polltype"].ToString() == "slider")
        {

            if (!IsPostBack)
            {
                tbCountries.Text = dtSettingsInfo.Rows[0]["countryname"].ToString();
                tbcities.Text = dtSettingsInfo.Rows[0]["cityname"].ToString();

                txtFreqvisit.Text = dtSettingsInfo.Rows[0]["freqofvisit"].ToString();

                if (dtSettingsInfo.Rows[0]["devicename"].ToString() == "Mobile")
                {
                    chklstDevice.Items[1].Selected = true;
                }
                else if (dtSettingsInfo.Rows[0]["devicename"].ToString() == "Desktop")
                {
                    chklstDevice.Items[0].Selected = true;
                }
                else if (dtSettingsInfo.Rows[0]["devicename"].ToString() == "Tablet")
                {
                    chklstDevice.Items[2].Selected = true;
                }

                if (dtSettingsInfo.Rows[0]["position"].ToString() == "BC")
                {
                    rblposition.Items[0].Selected = true;
                }
                else if (dtSettingsInfo.Rows[0]["position"].ToString() == "BL")
                {
                    rblposition.Items[1].Selected = true;
                }
                else if (dtSettingsInfo.Rows[0]["position"].ToString() == "BR")
                {
                    rblposition.Items[2].Selected = true;
                }
                else if (dtSettingsInfo.Rows[0]["position"].ToString() == "CL")
                {
                    rblposition.Items[3].Selected = true;
                }
                else if (dtSettingsInfo.Rows[0]["position"].ToString() == "CR")
                {
                    rblposition.Items[4].Selected = true;
                }

                if (dtSettingsInfo.Rows[0]["sliderdisplay"].ToString() == "Slow")
                {
                    rblspeed.Items[0].Selected = true;
                }
                else if (dtSettingsInfo.Rows[0]["sliderdisplay"].ToString() == "Medium")
                {
                    rblspeed.Items[1].Selected = true;
                }
                else if (dtSettingsInfo.Rows[0]["sliderdisplay"].ToString() == "Fast")
                {
                    rblspeed.Items[2].Selected = true;
                }
            }
        }

        if (pollInfo.Tables[0].Rows[0][16].ToString() != "")
        {
            txtQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
            lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
        }
        else
        {
            lblQuestion.Text = "Question?";
        }
        lblPollName.Text = pollInfo.Tables[0].Rows[0][2].ToString();
        
        txtEditName.Text = pollInfo.Tables[0].Rows[0][2].ToString();
        if (pollInfo.Tables[0].Rows[0][9].ToString() == "private")
        {
            radLeadGen.Checked = true;
            ResultBox.Style.Add("display", "block");
            txtResultText.Text = pollInfo.Tables[0].Rows[0][10].ToString();
            results.Style.Add("display", "none");
            resulttextpreview.Style.Add("display", "block");
            hdnPollResultsOption.Value = "leadgen";
        }
        else
        {
            radLeadGen.Checked = false;
            radShowResults.Checked = true;
            ResultBox.Style.Add("display", "none");
            results.Style.Add("display", "block");
            resulttextpreview.Style.Add("display", "none");
            hdnPollResultsOption.Value = "results";
        }
        if (pollInfo.Tables[0].Rows[0][14].ToString() == "no")
        {
            ckbInsightoBrand.Checked = true;
            poweredby.Style.Add("display", "none");
            poweredbyResults.Style.Add("display", "none");
        }
        else
        {
            ckbInsightoBrand.Checked = false;
            poweredby.Style.Add("display", "block");
            poweredbyResults.Style.Add("display", "block");
        }
        if (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["questiontype"].ToString()) == 1)
        {
            radSingleAnswer.Checked = true;
        }
        else if (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["questiontype"].ToString()) == 2)
        {
            radMultipleAnswer.Checked = true;
        }
        else
        {
            radSingleAnswer.Checked = true;
        }
        if (Convert.ToBoolean(pollInfo.Tables[0].Rows[0]["commentreq"].ToString()) == true)
        {
            chkComments.Checked = true;
            commentbox.Style.Add("display", "block");
        }
        else
        {
            chkComments.Checked = false;
        }
        if (pollInfo.Tables[0].Rows[0]["uniquerespondent"].ToString() == "0")
        {
            ckbSingleVote.Checked = false;
        }
        else
        {
            ckbSingleVote.Checked = true;
        }
        
        if (pollInfo.Tables[0].Rows[0]["issamethankyoumessageused"].ToString() == "1")
        {
            chkSameAnswer.Checked = true;
        }
        else
        {
            chkSameAnswer.Checked = false;
        }

        if (pollInfo.Tables.Count > 1)
        {
            if (pollInfo.Tables[1].Rows.Count > 0)
            {
                if (pollInfo.Tables[1].Rows[0]["autoadjustdimensions"].ToString() == "1")
                {
                    ckbAutoAdjust.Checked = true;
                    ckbAutoAdjustDesign.Checked = true;
                }
                else
                {
                    ckbAutoAdjust.Checked = false;
                    ckbAutoAdjustDesign.Checked = false;
                }
            }
            else
            {
                ckbAutoAdjust.Checked = true;
                ckbAutoAdjustDesign.Checked = true;
            }
        }
        else
        {
            ckbAutoAdjust.Checked = true;
            ckbAutoAdjustDesign.Checked = true;
        }
        if (pollInfo.Tables[0].Rows[0]["resultaccess"].ToString() == "private")
        {
            radLeadGen.Checked = true;
            radShowResults.Checked = false;
        }
        else
        {
            radLeadGen.Checked = false;
            radShowResults.Checked = true;
        }
        if (pollInfo.Tables[0].Rows[0]["invitationmessage"].ToString() != "")
        {
            txtInvitationMessage.Text = pollInfo.Tables[0].Rows[0]["invitationmessage"].ToString();
        }
        else
        {
            txtInvitationMessage.Text = "Thank you for your vote";
        }
        if (pollInfo.Tables[0].Rows[0]["submitbuttontext"].ToString().Trim() != "")
        {
            txtSubmitButtonLabel.Text = pollInfo.Tables[0].Rows[0]["submitbuttontext"].ToString();
            btnSubmitLeadGen.Value = pollInfo.Tables[0].Rows[0]["submitbuttontext"].ToString();
        }
        else
        {
            txtSubmitButtonLabel.Text = "Submit";
            btnSubmitLeadGen.Value = "Submit";
        }

        txtRedirectURL.Text = pollInfo.Tables[0].Rows[0]["redirecturl"].ToString();
        customvote.InnerHtml = pollInfo.Tables[0].Rows[0]["votebuttontext"].ToString();
        votebutton.Text = pollInfo.Tables[0].Rows[0]["votebuttontext"].ToString();
        if (pollInfo.Tables[0].Rows[0]["resulttext"].ToString() != "")
        {
            txtResultText.Text = pollInfo.Tables[0].Rows[0]["resulttext"].ToString();
        }
        else
        {
            txtResultText.Text = "Thank You!";
        }
        if (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isemailrequired"].ToString()) == 1)
        {
            chkEmail.Checked = true;
        }
        if (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isphonerequired"].ToString()) == 1)
        {
            chkPhone.Checked = true;
        }
        if (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["showsubmitbutton"].ToString()) == 1)
        {
            ckbSubmit.Checked = true;
        }

        if (pollInfo.Tables[0].Rows[0]["status"].ToString() == "Active")
        {
            launcharrow.Style.Add("display", "none");
            ckbAddSponsor.Enabled = false;
            ckbInsightoBrand.Enabled = false;
            chkComments.Enabled = false;
            chkComments.Text = " ";
            txtHeightPoll.Enabled = false;
            txtWidthPoll.Enabled = false;
            lblRecThemeHeight.Enabled = false;
            lblRecThemeWidth.Enabled = false;
            radMultipleAnswer.Enabled = false;
            radSingleAnswer.Enabled = false;
            recheightincreate.Style.Add("display", "none");
        }
        if (dtSettingsInfo != null && dtSettingsInfo.Rows.Count > 0)
        {
            if (dtSettingsInfo.Rows[0]["iframewidth"].ToString() != "")
            {
                txtWidthIframe.Text = dtSettingsInfo.Rows[0]["iframewidth"].ToString();
                txtWidthPoll.Text = dtSettingsInfo.Rows[0]["iframewidth"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["recmwidth"].ToString() != "")
            {
                lblWidth.Text = dtSettingsInfo.Rows[0]["recmwidth"].ToString();
                lblRecThemeWidth.Text = dtSettingsInfo.Rows[0]["recmwidth"].ToString();
            }
            if (Convert.ToBoolean(dtSettingsInfo.Rows[0]["countvotes"].ToString()) == true || dtSettingsInfo.Rows[0]["countvotes"].ToString() == "")
            {
                ckbVotes.Checked = true;
            }
            else
            {
                ckbVotes.Checked = false;
            }
            if (dtSettingsInfo.Rows[0]["imgrecmheight"].ToString() != "")
            {
                hdnImageRecHeight.Value = dtSettingsInfo.Rows[0]["imgrecmheight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["imgrecwidth"].ToString() != "")
            {
                hdnImageRecWidth.Value = dtSettingsInfo.Rows[0]["imgrecwidth"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["recmheight"].ToString() != "" && dtSettingsInfo.Rows[0]["recmheight"].ToString() != null)
            {
                lblHeight.Text = dtSettingsInfo.Rows[0]["recmheight"].ToString();
                hdnRecHeight.Value = dtSettingsInfo.Rows[0]["recmheight"].ToString();
                lblRecThemeHeight.Text = dtSettingsInfo.Rows[0]["recmheight"].ToString();
                hdnRecHeightne.Value = dtSettingsInfo.Rows[0]["recmheight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["iframeheight"].ToString() != "")
            {
                txtHeightIframe.Text = dtSettingsInfo.Rows[0]["iframeheight"].ToString();
                txtHeightPoll.Text = dtSettingsInfo.Rows[0]["iframeheight"].ToString();
            }
            mode = "edit";

            if (dtSettingsInfo.Rows[0]["pollbgcolor"].ToString() != "")
            {
                txtPollBgColor.Value = dtSettingsInfo.Rows[0]["pollbgcolor"].ToString();
            }
            else
            {
                txtPollBgColor.Value = "ffffff";
            }

            if (dtSettingsInfo.Rows[0]["weblinkbgcolor"].ToString() != "")
            {
                txtWebLinkBgColor.Value = dtSettingsInfo.Rows[0]["weblinkbgcolor"].ToString();
            }
            else
            {
                txtPollBgColor.Value = "9CCC65";
            }

            if (dtSettingsInfo.Rows[0]["questioncolor"].ToString() != "")
            {
                txtQuestionColor.Value = dtSettingsInfo.Rows[0]["questioncolor"].ToString();
            }
            else
            {
                txtQuestionColor.Value = "000000";
            }
            if (dtSettingsInfo.Rows[0]["questionfont"].ToString() != "")
            {
                ddlQuestionFont.ClearSelection();
                ddlQuestionFont.Items.FindByValue(dtSettingsInfo.Rows[0]["questionfont"].ToString()).Selected = true;
            }
            if (dtSettingsInfo.Rows[0]["answercolor"].ToString() != "")
            {
                txtAnswerColor.Value = dtSettingsInfo.Rows[0]["answercolor"].ToString();
            }
            else
            {
                txtAnswerColor.Value = "000000";
            }
            if (dtSettingsInfo.Rows[0]["votecolor"].ToString() != "")
            {
                txtVoteColor.Value = dtSettingsInfo.Rows[0]["votecolor"].ToString();
            }
            else
            {
                txtVoteColor.Value = "ffffff";
            }
            if (dtSettingsInfo.Rows[0]["votebgcolor"].ToString() != "")
            {
                txtVoteBgColor.Value = dtSettingsInfo.Rows[0]["votebgcolor"].ToString();
            }
            else
            {
                txtVoteBgColor.Value = "2196F3";
            }
            if (dtSettingsInfo.Rows[0]["messagebgcolor"].ToString() != "")
            {
                txtMessageBgColor.Text = dtSettingsInfo.Rows[0]["messagebgcolor"].ToString();
            }
            else
            {
                txtMessageBgColor.Text = "E7E7E7";
            }
            if (dtSettingsInfo.Rows[0]["googletext"].ToString() != "")
            {

                ckbGoogle.Checked = true;
            }
            if (dtSettingsInfo.Rows[0]["twittertext"].ToString() != "")
            {
                txtTwitter.Text = dtSettingsInfo.Rows[0]["twittertext"].ToString();
                ckbTwitter.Checked = true;
            }
            if (dtSettingsInfo.Rows[0]["fbtext"].ToString() != "")
            {

                ckbFb.Checked = true;
            }
            if (dtSettingsInfo.Rows[0]["googletext"].ToString() != "" || dtSettingsInfo.Rows[0]["twittertext"].ToString() != "" || dtSettingsInfo.Rows[0]["fbtext"].ToString() != "")
            {
                social.Style.Add("display", "block");
                socialsha.Style.Add("display", "block");
            }
            else
            {
                social.Style.Add("display", "none");
                socialsha.Style.Add("display", "none");
            }
            if (dtSettingsInfo.Rows[0]["messagecolor"].ToString() != "")
            {
                txtMessageColor.Text = dtSettingsInfo.Rows[0]["messagecolor"].ToString();
            }
            else
            {
                txtMessageColor.Text = "000000";
            }
            if (dtSettingsInfo.Rows[0]["questionbgcolor"].ToString() != "")
            {
                txtQuestionBgColor.Value = dtSettingsInfo.Rows[0]["questionbgcolor"].ToString();
            }
            else
            {
                txtQuestionBgColor.Value = "E7E7E7";
            }
            if (dtSettingsInfo.Rows[0]["questionfontweight"].ToString() == "bold")
            {
                imgBoldActive.Style.Add("display", "block");
                imgBold.Style.Add("display", "none");
                hdnQuestionBold.Value = "bold";
            }
            else
            {
                imgBoldActive.Style.Add("display", "none");
                imgBold.Style.Add("display", "block");
                hdnQuestionBold.Value = "normal";
            }
            if (dtSettingsInfo.Rows[0]["questionstyle"].ToString() == "italic")
            {
                imgItalicActive.Style.Add("display", "block");
                imgItalic.Style.Add("display", "none");
                hdnQuestionItalic.Value = "italic";
            }
            else
            {
                imgItalicActive.Style.Add("display", "none");
                imgItalic.Style.Add("display", "block");
                hdnQuestionItalic.Value = "normal";
            }
            if (dtSettingsInfo.Rows[0]["questionunderline"].ToString() == "underline")
            {
                imgUnderlineActive.Style.Add("display", "block");
                imgUnderline.Style.Add("display", "none");
                hdnQuestionUL.Value = "underline";
            }
            else
            {
                imgUnderlineActive.Style.Add("display", "none");
                imgUnderline.Style.Add("display", "block");
                hdnQuestionUL.Value = "none";
            }
            if (dtSettingsInfo.Rows[0]["questionalign"].ToString() == "left")
            {
                qleftinactive.Style.Add("display", "none");
                qrightactive.Style.Add("display", "none");
                qcenteractive.Style.Add("display", "none");
                qleftactive.Style.Add("display", "block");
                hdnQuestionAlign.Value = "left";
            }
            else if (dtSettingsInfo.Rows[0]["questionalign"].ToString() == "right")
            {
                qleftinactive.Style.Add("display", "none");
                qrightactive.Style.Add("display", "block");
                qcenteractive.Style.Add("display", "none");
                qleftactive.Style.Add("display", "none");
                hdnQuestionAlign.Value = "right";
            }
            else if (dtSettingsInfo.Rows[0]["questionalign"].ToString() == "center")
            {
                qleftinactive.Style.Add("display", "none");
                qrightactive.Style.Add("display", "none");
                qcenteractive.Style.Add("display", "block");
                qleftactive.Style.Add("display", "none");
                hdnQuestionAlign.Value = "center";
            }
            else
            {
                qleftinactive.Style.Add("display", "block");
                qrightactive.Style.Add("display", "none");
                qcenteractive.Style.Add("display", "none");
                qleftactive.Style.Add("display", "none");
                hdnQuestionAlign.Value = "left";
            }
            if (dtSettingsInfo.Rows[0]["answerfontweight"].ToString() == "bold")
            {
                imgABoldActive.Style.Add("display", "block");
                imgABold.Style.Add("display", "none");
                hdnAnswerBold.Value = "bold";
            }
            else
            {
                imgABoldActive.Style.Add("display", "none");
                imgABold.Style.Add("display", "block");
                hdnAnswerBold.Value = "normal";
            }
            if (dtSettingsInfo.Rows[0]["answerstyle"].ToString() == "italic")
            {
                imgAItalicActive.Style.Add("display", "block");
                imgAItalic.Style.Add("display", "none");
                hdnAnswerItalic.Value = "italic";
            }
            else
            {
                imgAItalicActive.Style.Add("display", "none");
                imgAItalic.Style.Add("display", "block");
                hdnAnswerItalic.Value = "normal";
            }
            if (dtSettingsInfo.Rows[0]["answerunderline"].ToString() == "underline")
            {
                imgAULActive.Style.Add("display", "block");
                imgAUL.Style.Add("display", "none");
                hdnAnswerUL.Value = "underline";
            }
            else
            {
                imgAULActive.Style.Add("display", "none");
                imgAUL.Style.Add("display", "block");
                hdnAnswerUL.Value = "none";
            }
            if (dtSettingsInfo.Rows[0]["answerfont"].ToString() != "")
            {
                ddlAnswerFont.ClearSelection();
                ddlAnswerFont.Items.FindByValue(dtSettingsInfo.Rows[0]["answerfont"].ToString()).Selected = true;
            }
            if (dtSettingsInfo.Rows[0]["votefont"].ToString() != "")
            {
                ddlVoteFont.ClearSelection();
                ddlVoteFont.Items.FindByValue(dtSettingsInfo.Rows[0]["votefont"].ToString()).Selected = true;
            }
            if (dtSettingsInfo.Rows[0]["messagefont"].ToString() != "")
            {
                ddlMsgFont.ClearSelection();
                ddlMsgFont.Items.FindByValue(dtSettingsInfo.Rows[0]["messagefont"].ToString()).Selected = true;
            }
            if (dtSettingsInfo.Rows[0]["answeralign"].ToString() == "left")
            {
                aleftinactive.Style.Add("display", "none");
                arightactive.Style.Add("display", "none");
                acenteractive.Style.Add("display", "none");
                aleftactive.Style.Add("display", "block");
                hdnAnswerAlign.Value = "left";
            }
            else if (dtSettingsInfo.Rows[0]["answeralign"].ToString() == "right")
            {
                aleftinactive.Style.Add("display", "none");
                arightactive.Style.Add("display", "block");
                acenteractive.Style.Add("display", "none");
                aleftactive.Style.Add("display", "none");
                hdnAnswerAlign.Value = "right";
            }
            else if (dtSettingsInfo.Rows[0]["answeralign"].ToString() == "center")
            {
                aleftinactive.Style.Add("display", "none");
                arightactive.Style.Add("display", "none");
                acenteractive.Style.Add("display", "block");
                aleftactive.Style.Add("display", "none");
                hdnAnswerAlign.Value = "center";
            }
            else
            {
                aleftinactive.Style.Add("display", "block");
                arightactive.Style.Add("display", "none");
                acenteractive.Style.Add("display", "none");
                aleftactive.Style.Add("display", "none");
                hdnAnswerAlign.Value = "left";
            }
            if (dtSettingsInfo.Rows[0]["votefontweight"].ToString() == "bold")
            {
                imgVBoldActive.Style.Add("display", "block");
                imgVBold.Style.Add("display", "none");
                hdnVoteBold.Value = dtSettingsInfo.Rows[0]["votefontweight"].ToString();
            }
            else
            {
                imgVBoldActive.Style.Add("display", "none");
                imgVBold.Style.Add("display", "block");
                hdnVoteBold.Value = dtSettingsInfo.Rows[0]["votefontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votestyle"].ToString() == "italic")
            {
                imgVItalicActive.Style.Add("display", "block");
                imgVItalic.Style.Add("display", "none");
                hdnVoteItalic.Value = "italic";
            }
            else
            {
                imgVItalicActive.Style.Add("display", "none");
                imgVItalic.Style.Add("display", "block");
                hdnVoteItalic.Value = "normal";
            }
            if (dtSettingsInfo.Rows[0]["voteunderline"].ToString() == "underline")
            {
                imgVULActive.Style.Add("display", "block");
                imgVUL.Style.Add("display", "none");
                hdnVoteUL.Value = "underline";
            }
            else
            {
                imgVULActive.Style.Add("display", "none");
                imgVUL.Style.Add("display", "block");
                hdnVoteUL.Value = "none";
            }
            if (dtSettingsInfo.Rows[0]["votealign"].ToString() == "left")
            {
                vleftinactive.Style.Add("display", "none");
                vrightactive.Style.Add("display", "none");
                vcenteractive.Style.Add("display", "none");
                vleftactive.Style.Add("display", "block");
                hdnVoteAlign.Value = "left";
            }
            else if (dtSettingsInfo.Rows[0]["votealign"].ToString() == "right")
            {
                vleftinactive.Style.Add("display", "none");
                vrightactive.Style.Add("display", "block");
                vcenteractive.Style.Add("display", "none");
                vleftactive.Style.Add("display", "none");
                hdnVoteAlign.Value = "right";
            }
            else if (dtSettingsInfo.Rows[0]["votealign"].ToString() == "center")
            {
                vleftinactive.Style.Add("display", "none");
                vrightactive.Style.Add("display", "none");
                vcenteractive.Style.Add("display", "block");
                vleftactive.Style.Add("display", "none");
                hdnVoteAlign.Value = "center";
            }
            else
            {
                vleftinactive.Style.Add("display", "block");
                vrightactive.Style.Add("display", "none");
                vcenteractive.Style.Add("display", "none");
                vleftactive.Style.Add("display", "none");
                hdnVoteAlign.Value = "center";
            }
            if (dtSettingsInfo.Rows[0]["messagefontweight"].ToString() == "bold")
            {
                imgMBoldActive.Style.Add("display", "block");
                imgMBold.Style.Add("display", "none");
                hdnMsgBold.Value = "bold";
            }
            else
            {
                imgMBoldActive.Style.Add("display", "none");
                imgMBold.Style.Add("display", "block");
                hdnMsgBold.Value = "normal";
            }
            if (dtSettingsInfo.Rows[0]["messagestyle"].ToString() == "italic")
            {
                imgMItalicActive.Style.Add("display", "block");
                imgMItalic.Style.Add("display", "none");
                hdnMsgItalic.Value = "italic";
            }
            else
            {
                imgMItalicActive.Style.Add("display", "none");
                imgMItalic.Style.Add("display", "block");
                hdnMsgItalic.Value = "normal";
            }
            if (dtSettingsInfo.Rows[0]["messageunderline"].ToString() == "underline")
            {
                imgMULActive.Style.Add("display", "block");
                imgMUL.Style.Add("display", "none");
                hdnMsgUL.Value = "underline";
            }
            else
            {
                imgMULActive.Style.Add("display", "none");
                imgMUL.Style.Add("display", "block");
                hdnMsgUL.Value = "none";
            }
            if (dtSettingsInfo.Rows[0]["messagealign"].ToString() == "left")
            {
                mleftinactive.Style.Add("display", "none");
                mrightactive.Style.Add("display", "none");
                mcenteractive.Style.Add("display", "none");
                mleftactive.Style.Add("display", "block");
                hdnMsgAlign.Value = "left";
            }
            else if (dtSettingsInfo.Rows[0]["messagealign"].ToString() == "right")
            {
                mleftinactive.Style.Add("display", "none");
                mrightactive.Style.Add("display", "block");
                mcenteractive.Style.Add("display", "none");
                mleftactive.Style.Add("display", "none");
                hdnMsgAlign.Value = "right";
            }
            else if (dtSettingsInfo.Rows[0]["messagealign"].ToString() == "center")
            {
                mleftinactive.Style.Add("display", "none");
                mrightactive.Style.Add("display", "none");
                mcenteractive.Style.Add("display", "block");
                mleftactive.Style.Add("display", "none");
                hdnMsgAlign.Value = "center";
            }
            else
            {
                mleftinactive.Style.Add("display", "block");
                mrightactive.Style.Add("display", "none");
                mcenteractive.Style.Add("display", "none");
                mleftactive.Style.Add("display", "none");
                hdnMsgAlign.Value = "center";
            }

            if (dtSettingsInfo.Rows[0]["questionfontsize"].ToString() != "")
            {
                hdnQuestionFSize.Value = dtSettingsInfo.Rows[0]["questionfontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfontsize"].ToString() != "")
            {
                hdnAnswerFSize.Value = dtSettingsInfo.Rows[0]["answerfontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefontsize"].ToString() != "")
            {
                hdnVoteFSize.Value = dtSettingsInfo.Rows[0]["votefontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefontsize"].ToString() != "")
            {
                hdnMsgFSize.Value = dtSettingsInfo.Rows[0]["messagefontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["sharetext"].ToString() != "")
            {
                lblShare.Text = dtSettingsInfo.Rows[0]["sharetext"].ToString();
                txtSharePoll.Text = dtSettingsInfo.Rows[0]["sharetext"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votetext"].ToString() != "")
            {
                txtVoteButtonLabel.Text = dtSettingsInfo.Rows[0]["votetext"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "" || dtSettingsInfo.Rows[0]["sponsortext"].ToString() != "")
            {
                ckbAddSponsor.Checked = true;
                if (dtSettingsInfo.Rows[0]["sponsortext"].ToString() != "")
                {
                    txtSponsorText.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                    lblSponsor.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                    lblSponText.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                }
                else
                {
                    txtSponsorText.Text = "Sponsored By";
                }
                if (dtSettingsInfo.Rows[0]["logolink"].ToString() != "")
                {
                    txtSponsorLink.Text = dtSettingsInfo.Rows[0]["logolink"].ToString();
                }
                else
                {
                    txtSponsorLink.Text = "";
                }
                logolink.Attributes.Add("href", txtSponsorLink.Text);
                SponsorBox.Style.Add("display", "block");
                sponsorpreviw.Style.Add("display", "block");
                sponsorResults.Style.Add("display", "block");
                if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "")
                {
                    logo.Attributes.Add("src", foldername + "\\" + dtSettingsInfo.Rows[0]["sponsorlogo"].ToString());
                    imgSpoResults.Attributes.Add("src", foldername + "\\" + dtSettingsInfo.Rows[0]["sponsorlogo"].ToString());
                    lblLogoName.Value = dtSettingsInfo.Rows[0]["sponsorlogo"].ToString();
                    lblLogoName.Visible = true;
                    hdnLogoLink.Value = "exist";
                }
            }
            else
            {
                ckbAddSponsor.Checked = false;
                SponsorBox.Style.Add("display", "none");
                sponsorpreviw.Style.Add("display", "none");
            }



        }
        else
        {
            mode = "add";
            imgBoldActive.Style.Add("display", "none");
            imgItalicActive.Style.Add("display", "none");
            imgUnderlineActive.Style.Add("display", "none");
            imgABoldActive.Style.Add("display", "none");
            imgAItalicActive.Style.Add("display", "none");
            imgAULActive.Style.Add("display", "none");
            imgVBoldActive.Style.Add("display", "none");
            imgVItalicActive.Style.Add("display", "none");
            imgVULActive.Style.Add("display", "none");
            imgMBoldActive.Style.Add("display", "none");
            imgMItalicActive.Style.Add("display", "none");
            imgMULActive.Style.Add("display", "none");
            qleftinactive.Style.Add("display", "block");
            qrightactive.Style.Add("display", "none");
            qcenteractive.Style.Add("display", "none");
            qleftactive.Style.Add("display", "none");
            aleftinactive.Style.Add("display", "block");
            aleftactive.Style.Add("display", "none");
            arightactive.Style.Add("display", "none");
            acenteractive.Style.Add("display", "none");
            vleftinactive.Style.Add("display", "block");
            vleftactive.Style.Add("display", "none");
            vrightactive.Style.Add("display", "none");
            vcenteractive.Style.Add("display", "none");
            mleftinactive.Style.Add("display", "block");
            mleftactive.Style.Add("display", "none");
            mrightactive.Style.Add("display", "none");
            mcenteractive.Style.Add("display", "none");
            hdnAnswerFSize.Value = "12";
            hdnQuestionFSize.Value = "12";
            hdnVoteFSize.Value = "13";
            hdnMsgFSize.Value = "12";
            hdnQuestionBold.Value = "normal";
            hdnQuestionItalic.Value = "normal";
            hdnQuestionUL.Value = "none";
            hdnQuestionAlign.Value = "left";
            hdnAnswerBold.Value = "normal";
            hdnAnswerItalic.Value = "normal";
            hdnAnswerUL.Value = "normal";
            hdnAnswerAlign.Value = "left"; ;
            hdnVoteBold.Value = "bold";
            hdnVoteItalic.Value = "normal";
            hdnVoteUL.Value = "none";
            hdnVoteAlign.Value = "center";
            hdnMsgAlign.Value = "center";
            hdnMsgBold.Value = "normal";
            hdnMsgItalic.Value = "normal";
            hdnMsgUL.Value = "none";
            txtPollBgColor.Value = "ffffff";
            txtAnswerColor.Value = "000000";
            txtQuestionColor.Value = "000000";
            txtQuestionBgColor.Value = "E7E7E7";
            txtVoteBgColor.Value = "2196F3";
            txtVoteColor.Value = "ffffff";
            txtMessageBgColor.Text = "cccccc";
            txtMessageColor.Text = "000000";
            txtSponsorText.Text = "";
            txtSharePoll.Text = "Share this poll";
            txtWebLinkBgColor.Value = "9ccc65";

            chartwidth = 0;
            chartheight = 0;
            Int32.TryParse(hdnChartWidth.Value, out chartwidth);
            Int32.TryParse(hdnChartHeight.Value, out chartheight);

            //Pc.InsertintoPollSettings(pollId, pollbgcolor, ddlQuestionFont.SelectedItem.Value, questionbgcolor, questioncolor, questionitalic, questionbold, questionul, questionalign, questionfsize, ddlAnswerFont.SelectedItem.Value, answercolor, answerbold, answerul, answeritalic, answeralign, answerfsize, votebgcolor, ddlVoteFont.SelectedItem.Text, votefsize, voteitalic, voteul, votecolor, votebold, votealign, logoFileName, txtSponsorText.Text, txtSponsorLink.Text, msgbgcolor, msgcolor, ddlMsgFont.SelectedItem.Value, msgitalic, msgul, msgalign, msgfsize, msgbold, googletext, twittertext, fbtext, votecount, txtSharePoll.Text, txtVoteButtonLabel.Text, chartwidth, chartheight);
        }

    }

    private void AddColumns()
    {
        if (!dtAnswerOptions.Columns.Contains("AnswerId"))
            dtAnswerOptions.Columns.Add("AnswerId");

        if (!dtAnswerOptions.Columns.Contains("Answer"))
            dtAnswerOptions.Columns.Add("Answer");

        if (!dtAnswerOptions.Columns.Contains("ButtonRemove"))
            dtAnswerOptions.Columns.Add("ButtonRemove");

        if (!dtAnswerOptions.Columns.Contains("Button"))
            dtAnswerOptions.Columns.Add("Button");

        if (!dtAnswerOptions.Columns.Contains("pkAnswerId"))
           dtAnswerOptions.Columns.Add("pkAnswerId");

        if (!dtAnswerOptions.Columns.Contains("InvitationThankYouMessage"))
            dtAnswerOptions.Columns.Add("InvitationThankYouMessage");
    }

    private void BindWithRepeater()
    {
        if (dsAnswerOptions.Tables.Contains("Table1"))
        {
            dsAnswerOptions.Tables.Remove(dtAnswerOptions);
        }
        dsAnswerOptions.Tables.Add(dtAnswerOptions);

        addRemoveControls.DataSource = dsAnswerOptions;
        addRemoveControls.DataBind();

        addThankYouOptions.DataSource = dsAnswerOptions;
        addThankYouOptions.DataBind();

        AnswerPreview.DataSource = dsAnswerOptions;
        AnswerPreview.DataBind();
    }

    private void BindWithRepeaterForThankYouOptions()
    {
        DataTable dtAnswerThankYouOptions = new DataTable();
        dtAnswerThankYouOptions.Columns.Add("AnswerId");
        dtAnswerThankYouOptions.Columns.Add("Answer");
        dtAnswerThankYouOptions.Columns.Add("ButtonRemove");
        dtAnswerThankYouOptions.Columns.Add("Button");
        dtAnswerThankYouOptions.Columns.Add("pkAnswerId");
        dtAnswerThankYouOptions.Columns.Add("InvitationThankYouMessage");

        DataTable answerThankYouOptions = new DataTable();
        answerThankYouOptions = Pc.getAnswerOptions(Convert.ToInt32(hdnPollID.Value));
        if (answerThankYouOptions != null && answerThankYouOptions.Rows.Count > 0)
        {
            int i = 0;
            foreach (DataRow row in answerThankYouOptions.Rows)
            {
                
                dtAnswerThankYouOptions.Rows.Add(i, HttpUtility.HtmlEncode(row["answeroption"].ToString().Replace("\r", "").Replace("\n", "")), "", "", Convert.ToInt32(row[0].ToString()), row["InvitationThankYouMessage"].ToString());
                count++;
                i++;
            }
        }
        DataSet dsAnswerThankYouOptions = new DataSet();
        dsAnswerThankYouOptions.Tables.Add(dtAnswerThankYouOptions);

        addThankYouOptions.DataSource = dsAnswerThankYouOptions;
        addThankYouOptions.DataBind();
    }

    public static System.Drawing.Image ScaleImage(System.Drawing.Image image, int maxWidth, int maxHeight)
    {
        var newWidth = image.Width;
        var newHeight = image.Height;
        if (image.Height > maxHeight)
        {
            var ratio = (double)maxHeight / image.Height;
            newWidth = (int)(image.Width * ratio);
            newHeight = (int)(image.Height * ratio);
        }
        if (newWidth > maxWidth)
        {
            var ratio = (double)maxWidth / image.Width;
            newWidth = (int)(image.Width * ratio);
            newHeight = (int)(image.Height * ratio);
        }

        var newImage = new Bitmap(newWidth, newHeight);
        using (var g = Graphics.FromImage(newImage))
        {
            g.DrawImage(image, 0, 0, newWidth, newHeight);
        }
        return newImage;
    }

    public void LaunchMail(int pollid)
    {
        pollInfo = Pc.getPollInfo(pollId);
        string name = pollInfo.Tables[0].Rows[0][16].ToString();
        string viewData1;
        JavaScriptSerializer js1 = new JavaScriptSerializer();
        js1.MaxJsonLength = int.MaxValue;
        clsjsonmandrill prmjson1 = new clsjsonmandrill();
        prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
        prmjson1.name = "Welcome Email";
        viewData1 = js1.Serialize(prmjson1);
        string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";
        WebClient request1 = new WebClient();
        request1.Encoding = System.Text.Encoding.UTF8;
        request1.Headers["Content-Type"] = "application/json";
        byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));
        string response1 = System.Text.Encoding.ASCII.GetString(resp1);
        clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);
        string bodycontent = jsmandrill11.code;
        string viewData;
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = int.MaxValue;
        var prmjson = new clsjsonmandrill.jsonmandrillmerge();
        prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
        prmjson.template_name = "Welcome Email";
        List<clsjsonmandrill.template_content> mtags = new List<clsjsonmandrill.template_content>();
        mtags.Add(new clsjsonmandrill.template_content { name = "std_content00", content = "<div> Dear *|FNAME|*,</div>" });
        mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div>You have successfully launched your poll. Here are the details of your recent poll launched: </div>" });

        List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userInfo = sessionService.GetLoginUserDetailsSession();
        mvars.Add(new clsjsonmandrill.merge_vars { name = "FNAME", content = userInfo.FirstName });
        mvars.Add(new clsjsonmandrill.merge_vars { name = "Launch Type", content = "Embed" });
        prmjson.template_content = mtags;
        prmjson.merge_vars = mvars;
        viewData = js.Serialize(prmjson);
        string url = "https://mandrillapp.com/api/1.0/templates/render.json";
        WebClient request = new WebClient();
        request.Encoding = System.Text.Encoding.UTF8;
        request.Headers["Content-Type"] = "application/json";
        byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));
        string response = System.Text.Encoding.ASCII.GetString(resp);
        string unesc = System.Text.RegularExpressions.Regex.Unescape(response);
        string first4 = unesc.Substring(0, 9);
        string last2 = unesc.Substring(unesc.Length - 2, 2);
        string unesc1 = unesc.Replace(first4, "");
        string unescf = unesc1.Replace(last2, "");
        clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);
        mailMessage = new MailMessage();
        mailMessage.To.Clear();
        mailMessage.Sender = new MailAddress("getstarted@insighto.com", "Insighto.com");
        mailMessage.From = new MailAddress("getstarted@insighto.com", "Insighto.com");

        mailMessage.Subject = "Launch Confirmation";

        mailMessage.To.Add(userInfo.LoginName);
        mailMessage.Body = unescf;

        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;
        smtpMail = new SmtpClient();
        smtpMail.Host = "smtp.mandrillapp.com";
        smtpMail.Port = 587;
        smtpMail.EnableSsl = true;
        smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
        smtpMail.Credentials = new System.Net.NetworkCredential("ram@knowience.com", "T74TJc3EZPYaIcvdBg--Dg");
    }

    protected void lnkEmbedCode_Click(object sender, EventArgs e)
    {
        resUrl = EncryptHelper.EncryptQuerystring("PollManager.aspx", "PollId=" + pollId);
        Response.Redirect(resUrl);
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        pollInfo = Pc.getPollInfo(pollId);
        Pc.updatePollNameOnly(pollId, txtEditName.Text);
        pollInfo = Pc.getPollInfo(pollId);
        txtEditName.Text = pollInfo.Tables[0].Rows[0][2].ToString();
        lblPollName.Text = pollInfo.Tables[0].Rows[0][2].ToString();
        updatePollNameParentWindowJavascript(pollInfo, pollInfo.ToString());
    }

    //WHEN YOU CLICK THE DESIGN TAB ON THE TOP
    protected void SaveAndClose_Click(object sender, EventArgs e)
    {
        if (txtWidthPoll.Text == "")
        {
            launchafter.Style.Add("display", "none");
            launch.Style.Add("display", "none");
            create.Style.Add("display", "block");
            return;
        }
        if (txtHeightPoll.Text == "")
        {
            launchafter.Style.Add("display", "none");
            launch.Style.Add("display", "none");
            create.Style.Add("display", "block");
            return;
        }

        if (ckbVotes.Checked == true)
        {
            votecount = 1;
        }
        else
        {
            votecount = 0;
        }
        questionbold = hdnQuestionBold.Value;
        questionitalic = hdnQuestionItalic.Value;
        questionul = hdnQuestionUL.Value;
        if (ckbGoogle.Checked == true)
        {
            googletext = "google";
        }
        if (ckbTwitter.Checked == true)
        {
            twittertext = "twitter";
        }
        if (ckbFb.Checked == true)
        {
            fbtext = "fb";
        }
        if (hdnQuestionAlign.Value != "")
        {
            questionalign = hdnQuestionAlign.Value;
        }
        else
        {
            questionalign = "left";
        }
        if (ckbGoogle.Checked == true)
        {
            googletext = "google";
        }
        if (ckbTwitter.Checked == true)
        {
            twittertext = "twitter";
        }
        if (ckbFb.Checked == true)
        {
            fbtext = "fb";
        }
        questionfsize = hdnQuestionFSize.Value;
        answerbold = hdnAnswerBold.Value;
        answeritalic = hdnAnswerItalic.Value;
        answerul = hdnAnswerUL.Value;
        answerfsize = hdnAnswerFSize.Value;
        if (hdnAnswerAlign.Value != "")
        {
            answeralign = hdnAnswerAlign.Value;
        }
        else
        {
            answeralign = "left";
        }
        votebold = hdnVoteBold.Value;
        voteitalic = hdnVoteItalic.Value;
        voteul = hdnVoteUL.Value;
        votefsize = hdnVoteFSize.Value;
        if (hdnVoteAlign.Value != "")
        {
            votealign = hdnVoteAlign.Value;
        }
        else
        {
            votealign = "center";
        }
        if (hdnMsgAlign.Value != "")
        {
            msgalign = hdnMsgAlign.Value;
        }
        else
        {
            msgalign = "left";
        }
        msgbold = hdnMsgBold.Value;
        if (hdnMsgFSize.Value != "")
        {
            msgfsize = hdnMsgFSize.Value;
        }
        else
        {
            msgfsize = "12";
        }
        msgitalic = hdnMsgItalic.Value;
        msgul = hdnMsgUL.Value;
        if (txtPollBgColor.Value != "")
        {
            pollbgcolor = txtPollBgColor.Value;
        }
        else
        {
            pollbgcolor = "ffffff";
        }
        if (txtQuestionBgColor.Value != "")
        {
            questionbgcolor = txtQuestionBgColor.Value;
        }
        else
        {
            questionbgcolor = "E7E7E7";
        }
        if (txtQuestionColor.Value != "")
        {
            questioncolor = txtQuestionColor.Value;
        }
        else
        {
            questioncolor = "000000";
        }
        if (txtVoteColor.Value != "")
        {
            votecolor = txtVoteColor.Value;
        }
        else
        {
            votecolor = "ffffff";
        }
        if (txtVoteBgColor.Value != "")
        {
            votebgcolor = txtVoteBgColor.Value;
        }
        else
        {
            votebgcolor = "6C89E1";
        }
        if (txtAnswerColor.Value != "")
        {
            answercolor = txtAnswerColor.Value;
        }
        else
        {
            answercolor = "000000";
        }
        if (txtMessageBgColor.Text != "")
        {
            msgbgcolor = txtMessageBgColor.Text;
        }
        else
        {
            msgbgcolor = "cccccc";
        }
        if (txtMessageColor.Text != "")
        {
            msgcolor = txtMessageColor.Text;
        }
        else
        {
            msgcolor = "000000";
        }
        if (ckbInsightoBrand.Checked == true)
        {
            insightoLink = "no";
        }
        else
        {
            insightoLink = "yes";
        }
        if (radLeadGen.Checked == true)
        {
            resultaccess = "private";
        }
        else
        {
            resultaccess = "public";
        }
        dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
        if (ckbAddSponsor.Checked == true)
        {
            if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "")
            {
                logoFileName = dtSettingsInfo.Rows[0]["sponsorlogo"].ToString();
                lblLogoName.Visible = true;
                lblLogoName.Value = logoFileName;
                hdnLogoLink.Value = "exists";
                txtSponsorText.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
            }
        }
        else
        {
            txtSponsorLink.Text = "";
            txtSponsorText.Text = "";
        }
        if (dtSettingsInfo != null && dtSettingsInfo.Rows.Count > 0)
        {
            mode = "edit";
        }
        else
        {
            mode = "add";
        }

        chartwidth = 0;
        chartheight = 0;
        Int32.TryParse(hdnChartWidth.Value, out chartwidth);
        Int32.TryParse(hdnChartHeight.Value, out chartheight);

        if ((ckbAutoAdjustDesign.Checked == true) && (ckbAutoAdjust.Checked == true))
        {
            autoadjustdimensions = 1;
        }
        else
        {
            autoadjustdimensions = 0;
        }

        if (chkSameAnswer.Checked == true)
        {
            issamethankyoumessageused = 1;
        }
        else
        {
            issamethankyoumessageused= 0;
        }

        if (mode == "add")
        {
            Pc.InsertintoPollSettings(pollId, pollbgcolor, ddlQuestionFont.SelectedItem.Value, questionbgcolor, questioncolor, questionitalic, questionbold, questionul, questionalign, questionfsize, ddlAnswerFont.SelectedItem.Value, answercolor, answerbold, answerul, answeritalic, answeralign, answerfsize, votebgcolor, ddlVoteFont.SelectedItem.Text, votefsize, voteitalic, voteul, votecolor, votebold, votealign, logoFileName, txtSponsorText.Text, txtSponsorLink.Text, msgbgcolor, msgcolor, ddlMsgFont.SelectedItem.Value, msgitalic, msgul, msgalign, msgfsize, msgbold, googletext, twittertext, fbtext, votecount, txtSharePoll.Text, txtVoteButtonLabel.Text, chartwidth, chartheight, txtWebLinkBgColor.Value);
        }
        else
        {
            Pc.UpdatePollSettings(pollId, pollbgcolor, ddlQuestionFont.SelectedItem.Value, questionbgcolor, questioncolor, questionitalic, questionbold, questionul, questionalign, questionfsize, ddlAnswerFont.SelectedItem.Value, answercolor, answerbold, answerul, answeritalic, answeralign, answerfsize, votebgcolor, ddlVoteFont.SelectedItem.Text, votefsize, voteitalic, voteul, votecolor, votebold, votealign, logoFileName, txtSponsorText.Text, txtSponsorLink.Text, msgbgcolor, msgcolor, ddlMsgFont.SelectedItem.Value, msgitalic, msgul, msgalign, msgfsize, msgbold, googletext, twittertext, fbtext, votecount, txtSharePoll.Text, txtVoteButtonLabel.Text, chartwidth, chartheight, autoadjustdimensions, txtWebLinkBgColor.Value);
        }
        if (ckbVotes.Checked == true)
        {
            votecount = 1;
        }
        else
        {
            votecount = 0;
        }
        if (ckbGoogle.Checked == true)
        {
            googletext = "google";
        }
        if (ckbTwitter.Checked == true)
        {
            twittertext = "twitter";
        }
        if (ckbFb.Checked == true)
        {
            fbtext = "fb";
        }
        txtQuestion.Attributes.Add("Style", "border-color:#CDCDCD");
        pollInfo = Pc.getPollInfo(pollId);
        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
        {
            if (txtVideo.Text != "")
            {
                videopreviewcreate.Style.Add("display", "block");
                List<string> iframe = new List<string>();
                string pattern = @"<(iframe)\b[^>]*>";
                string style = @"width=""(.*?)""";
                string styleheight = @"height=""(.*?)""";
                string iframewidth = txtWidthPoll.Text;

                string content = txtVideo.Text;
                Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                MatchCollection matches = rgx.Matches(content);
                if (matches.Count > 0)
                {
                    for (int i = 0, l = matches.Count; i < l; i++)
                    {
                        iframe.Add(matches[i].Value);
                        if (i > 0)
                            content = content.Replace(iframe[i], string.Empty);
                    }
                }
                if (iframe.Count > 0)
                {
                    string value = string.Empty;
                    string valueheight = string.Empty;
                    Regex styleRgx = new Regex(style, RegexOptions.IgnoreCase);
                    Regex styleRgxHeight = new Regex(styleheight, RegexOptions.IgnoreCase);
                    MatchCollection styleMatches = styleRgx.Matches(iframe[0]);
                    MatchCollection styleMatchesHeight = styleRgxHeight.Matches(iframe[0]);
                    string width = "";
                    if (styleMatches.Count > 0)
                    {
                        value = styleMatches[0].Value;
                        int widthif = Convert.ToInt32(Regex.Replace(value, @"\D", ""));
                        width = "width=" + "\"" + 850 + "\"";
                        content = content.Replace(iframe[0], iframe[0].Replace(value, width));

                    }
                    if (styleMatchesHeight.Count > 0)
                    {
                        int widthnew = Convert.ToInt32(Regex.Replace(width, @"\D", ""));
                        valueheight = styleMatchesHeight[0].Value;
                        int heightif = Convert.ToInt32(Regex.Replace(valueheight, @"\D", ""));
                        var ratio = (double)Convert.ToDouble(widthnew) / Convert.ToDouble(16);
                        var newHeight = (int)(ratio * 9);
                        string height = "height=" + "\"" + 480 + "\"";
                        content = content.Replace(valueheight, height);
                    }
                }
                videopreviewcreate.InnerHtml = content;
                videoText = content;
                txtVideo.Text = videoText;
            }
        }
        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
        {

            imageText = pollInfo.Tables[0].Rows[0]["imagelink"].ToString();
            hdnImageLink.Value = imageText;
            hdnImageName.Value = imageText;
        }
        if (radMultipleAnswer.Checked == true)
        {
            questionType = 2;
        }
        if (chkComments.Checked == true)
        {
            comments = 1;
        }
        else
        {
            comments = 0;

        }
        if (ckbSingleVote.Checked)
        {
            uniqueRespondent = 1;
        }
        else
        {
            uniqueRespondent = 0;
        }
        if (chkEmail.Checked)
        {
            emailrequired = 1;
        }
        if (chkPhone.Checked)
        {
            phonerequired = 1;
        }
        if (ckbSubmit.Checked)
        {
            showsubmitbutton = 1;
        }
        
        if (chkSameAnswer.Checked)
        {
            issamethankyoumessageused = 1;
        }
        else
        {
            issamethankyoumessageused = 0;
        }
        
        //PollId, Question, VideoCode, Imagelink, Questiontype, Commentreq, uniqueRespondent, votebuttontext, invitationmessage, isemailrequired, isphonerequired, redirecturl, showsubmitbutton, submitbuttontext
        Pc.InsertintoPollInfo(pollId, txtQuestion.Text, videoText, imageText, questionType, comments, uniqueRespondent, txtVoteButtonLabel.Text, txtInvitationMessage.Text, emailrequired, phonerequired, txtRedirectURL.Text, showsubmitbutton, txtSubmitButtonLabel.Text, issamethankyoumessageused);
        
        questionInfo = Pc.GetQuestionInfo(pollId);
        if (questionInfo != null && questionInfo.Rows.Count > 0)
        {
            Pc.UpdateQuestion(pollId, txtQuestion.Text);
        }
        else
        {
            Pc.InsertintoQuestion(pollId, txtQuestion.Text);
        }

        AddColumns();

        DataSet dspollstattus = Pc.getPollInfo(pollId);
        if (dspollstattus.Tables[0].Rows[0]["status"].ToString() != "Closed")
        {
            int repitems = 0;
            if (dspollstattus.Tables[0].Rows[0]["status"].ToString() == "Draft")
            {
                Pc.deleteAnswerOptions(pollId);
                for (int i = 0; i <= (Convert.ToInt32(hdnRepeaterTxts.Value)); i++)
                {
                    string id = "txtAnswerOption_" + i;
                    string AnswerOption = Request.Form[id];
                    string thankyouoption = "";
                    if (chkSameAnswer.Checked)
                    {
                        thankyouoption = txtInvitationMessage.Text;
                    }
                    else
                    {
                        thankyouoption = Request.Form["txtThankYouOption_" + i.ToString()];
                    }

                    if (AnswerOption != null && AnswerOption != "")
                    {
                        Pc.SaveAnswerOptions(AnswerOption, pollId, thankyouoption);
                    }
                }
            }
            else
            {
                tbAnswerOption = Pc.getAnswerOptions(pollId);
                hdnRepeaterTxts.Value = Convert.ToString(tbAnswerOption.Rows.Count);

                if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
                {
                    hdnRepeaterTxts.Value = Convert.ToString(tbAnswerOption.Rows.Count);
                    int i = 0;
                    foreach (DataRow row in tbAnswerOption.Rows)
                    {
                        dtAnswerOptions.Rows.Add(i, System.Web.HttpUtility.HtmlEncode(row["answeroption"].ToString().Replace("\r", "").Replace("\n", "")), "Remove", "Add", Convert.ToInt32(row["pk_answerid"].ToString()), row["InvitationThankYouMessage"].ToString());
                        i++;
                    }
                }

                dtAnswerids = dtAnswerOptions;

                for (int i = 0; i <= (Convert.ToInt32(hdnRepeaterTxts.Value)); i++)
                {
                    string id = "txtAnswerOption_" + i;
                    string AnswerOption = Request.Form[id];
                    string thankyouoption = "";
                    if (chkSameAnswer.Checked)
                    {
                        thankyouoption = txtInvitationMessage.Text;
                    }
                    else
                    {
                        thankyouoption = Request.Form["txtThankYouOption_" + i.ToString()];
                    }
                    if (AnswerOption != null && AnswerOption != "")
                    {

                        DataRow[] drEdit = dtAnswerids.Select("AnswerId=" + i);
                        try
                        {
                            Pc.UpdateAnswerOptions(Convert.ToInt32(drEdit[0][4].ToString()), AnswerOption, thankyouoption );

                        }
                        catch (Exception)
                        {
                        }

                    }
                }
            }
        }


        tbAnswerOption = Pc.getAnswerOptions(pollId);
        int reitems = addRemoveControls.Items.Count;

        strRootURL = ConfigurationSettings.AppSettings["RootURL"].ToString();
        string logourl = strRootURL + "Polls/images/Sample_logo.png";

        DataSet dspollInfo = Pc.getpollchartinfo(pollId);

        HtmlTable qt3tableques = new HtmlTable();
        qt3tableques.Width = "100%";
        int rowscount;

        if ((tbAnswerOption.Rows.Count == 0) || (tbAnswerOption.Rows.Count == 1))
        {
            rowscount = 2;
        }
        else
        {
            rowscount = tbAnswerOption.Rows.Count;
        }

        if (pollInfo.Tables[0].Rows[0][8].ToString() == "" || pollInfo.Tables[0].Rows[0][8].ToString() == null)
        {
            Pc.SaveIframeInfo(pollId, Convert.ToInt32(txtWidthPoll.Text), Convert.ToInt32(txtHeightPoll.Text), Convert.ToInt32(lblRecThemeWidth.Text), Convert.ToInt32(hdnRecHeightne.Value));
            if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
            {
                Pc.SaveImageSpec(pollId, Convert.ToInt32(hdnImageRecWidth.Value), Convert.ToInt32(hdnImageRecHeight.Value));
            }
        }

        dtAnswerOptions.Clear();
        tbAnswerOption = Pc.getAnswerOptions(pollId);
        int noofans = tbAnswerOption.Rows.Count;
        int diff = 0;
        if (noofans < Convert.ToInt32(hdnRepeaterTxts.Value))
        {
            diff = Convert.ToInt32(hdnRepeaterTxts.Value) - noofans;
            for (int i = 0; i < diff; i++)
            {
                hdnRecHeightne.Value = Convert.ToString(Convert.ToInt32(hdnRecHeightne.Value) - 20);
            }
            Pc.SaveModiyIframeSpec(pollId, Convert.ToInt32(hdnRecHeightne.Value));
        }
        hdnRepeaterTxts.Value = Convert.ToString(tbAnswerOption.Rows.Count);
        if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
        {
            int i = 0;
            foreach (DataRow row in tbAnswerOption.Rows)
            {
                dtAnswerOptions.Rows.Add(i, System.Web.HttpUtility.HtmlEncode(row["answeroption"].ToString().Replace("\r", "").Replace("\n", "")), "Remove", "Add");
                count++;
                i++;
            }
            if (count == 1)
            {
                dtAnswerOptions.Rows.Add(1, "", "Remove", "Add");
            }
        }

        if (hdnCurrentPage.Value == "create")
        {
            divcreatebutton.Style.Add("background-color", bgcolour);
            divdesignbutton.Style.Add("background-color", "#FFF");
            divlaunchbutton.Style.Add("background-color", "#FFF");
            create.Style.Add("display", "block");
            design.Style.Add("display", "none");
            launchafter.Style.Add("display", "none");
            launch.Style.Add("display", "none");
            CreateNew.ForeColor = Color.White;
            DesignNew.ForeColor = Color.Red;
            LaunchNew.ForeColor = Color.Red;
        }
        else if (hdnCurrentPage.Value == "design")
        {
            divcreatebutton.Style.Add("background-color", "#FFF");
            divdesignbutton.Style.Add("background-color", bgcolour);
            divlaunchbutton.Style.Add("background-color", "#FFF");
            create.Style.Add("display", "none");
            design.Style.Add("display", "block");
            launchafter.Style.Add("display", "none");
            launch.Style.Add("display", "none");
            CreateNew.ForeColor = Color.Red;
            DesignNew.ForeColor = Color.White;
            LaunchNew.ForeColor = Color.Red;
        }
        else if (hdnCurrentPage.Value == "launch")
        {
            divcreatebutton.Style.Add("background-color", "#FFF");
            divdesignbutton.Style.Add("background-color", "#FFF");
            divlaunchbutton.Style.Add("background-color", bgcolour);
            create.Style.Add("display", "none");
            design.Style.Add("display", "none");
            if (hdnPollStatus.Value == "Active")
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "LaunchAfter" + DateTime.Now.Millisecond.ToString(), "<script>$(window).load(function() { $('#MainContent_launchafter').show(); $('#launchoptions').show(); $('#hrefweb').addClass('active'); } );</script>");
                launch.Style.Add("display", "none");
            }
            else
            {
                launchafter.Style.Add("display", "none");
                launch.Style.Add("display", "block");
            }
            CreateNew.ForeColor = Color.Red;
            DesignNew.ForeColor = Color.Red;
            LaunchNew.ForeColor = Color.White;
        }

        updateParentWindowJavascript(pollInfo, pollId.ToString());

        ClientScript.RegisterClientScriptBlock(this.GetType(), "SaveAndClose" + DateTime.Now.Millisecond.ToString(), "<script>$(document).ready(function() { Materialize.toast('Your edits have been saved',5000); closeIframe(); } );</script>");
    }

    protected void DesignNew_Click(object sender, EventArgs e)
    {
        if (txtWidthPoll.Text == "")
        {
            launchafter.Style.Add("display", "none");
            launch.Style.Add("display", "none");
            create.Style.Add("display", "block");
            return;
        }
        if (txtHeightPoll.Text == "")
        {
            launchafter.Style.Add("display", "none");
            launch.Style.Add("display", "none");
            create.Style.Add("display", "block");
            return;
        }

        if (ckbVotes.Checked == true)
        {
            votecount = 1;
        }
        else
        {
            votecount = 0;
        }
        questionbold = hdnQuestionBold.Value;
        questionitalic = hdnQuestionItalic.Value;
        questionul = hdnQuestionUL.Value;
        if (ckbGoogle.Checked == true)
        {
            googletext = "google";
        }
        if (ckbTwitter.Checked == true)
        {
            twittertext = "twitter";
        }
        if (ckbFb.Checked == true)
        {
            fbtext = "fb";
        }
        if (hdnQuestionAlign.Value != "")
        {
            questionalign = hdnQuestionAlign.Value;
        }
        else
        {
            questionalign = "left";
        }
        if (ckbGoogle.Checked == true)
        {
            googletext = "google";
        }
        if (ckbTwitter.Checked == true)
        {
            twittertext = "twitter";
        }
        if (ckbFb.Checked == true)
        {
            fbtext = "fb";
        }
        questionfsize = hdnQuestionFSize.Value;
        answerbold = hdnAnswerBold.Value;
        answeritalic = hdnAnswerItalic.Value;
        answerul = hdnAnswerUL.Value;
        answerfsize = hdnAnswerFSize.Value;
        if (hdnAnswerAlign.Value != "")
        {
            answeralign = hdnAnswerAlign.Value;
        }
        else
        {
            answeralign = "left";
        }
        votebold = hdnVoteBold.Value;
        voteitalic = hdnVoteItalic.Value;
        voteul = hdnVoteUL.Value;
        votefsize = hdnVoteFSize.Value;
        if (hdnVoteAlign.Value != "")
        {
            votealign = hdnVoteAlign.Value;
        }
        else
        {
            votealign = "center";
        }
        if (hdnMsgAlign.Value != "")
        {
            msgalign = hdnMsgAlign.Value;
        }
        else
        {
            msgalign = "left";
        }
        msgbold = hdnMsgBold.Value;
        if (hdnMsgFSize.Value != "")
        {
            msgfsize = hdnMsgFSize.Value;
        }
        else
        {
            msgfsize = "12";
        }
        msgitalic = hdnMsgItalic.Value;
        msgul = hdnMsgUL.Value;
        if (txtPollBgColor.Value != "")
        {
            pollbgcolor = txtPollBgColor.Value;
        }
        else
        {
            pollbgcolor = "ffffff";
        }
        if (txtQuestionBgColor.Value != "")
        {
            questionbgcolor = txtQuestionBgColor.Value;
        }
        else
        {
            questionbgcolor = "E7E7E7";
        }
        if (txtQuestionColor.Value != "")
        {
            questioncolor = txtQuestionColor.Value;
        }
        else
        {
            questioncolor = "000000";
        }
        if (txtVoteColor.Value != "")
        {
            votecolor = txtVoteColor.Value;
        }
        else
        {
            votecolor = "ffffff";
        }
        if (txtVoteBgColor.Value != "")
        {
            votebgcolor = txtVoteBgColor.Value;
        }
        else
        {
            votebgcolor = "6C89E1";
        }
        if (txtAnswerColor.Value != "")
        {
            answercolor = txtAnswerColor.Value;
        }
        else
        {
            answercolor = "000000";
        }
        if (txtMessageBgColor.Text != "")
        {
            msgbgcolor = txtMessageBgColor.Text;
        }
        else
        {
            msgbgcolor = "cccccc";
        }
        if (txtMessageColor.Text != "")
        {
            msgcolor = txtMessageColor.Text;
        }
        else
        {
            msgcolor = "000000";
        }
        if (ckbInsightoBrand.Checked == true)
        {
            insightoLink = "no";
        }
        else
        {
            insightoLink = "yes";
        }
        if (radLeadGen.Checked == true)
        {
            resultaccess = "private";
        }
        else
        {
            resultaccess = "public";
        }
        dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
        if (ckbAddSponsor.Checked == true)
        {
            if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "")
            {
                logoFileName = dtSettingsInfo.Rows[0]["sponsorlogo"].ToString();
                lblLogoName.Visible = true;
                lblLogoName.Value = logoFileName;
                hdnLogoLink.Value = "exists";
                if (txtSponsorText.Text == "")
                {
                    txtSponsorText.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                }
            }
        }
        else
        {
            txtSponsorLink.Text = "";
            txtSponsorText.Text = "";
        }
        if (dtSettingsInfo != null && dtSettingsInfo.Rows.Count > 0)
        {
            mode = "edit";
        }
        else
        {
            mode = "add";
        }

        chartwidth = 0;
        chartheight = 0;
        Int32.TryParse(hdnChartWidth.Value, out chartwidth);
        Int32.TryParse(hdnChartHeight.Value, out chartheight);

        if ((ckbAutoAdjustDesign.Checked == true) && (ckbAutoAdjust.Checked == true))
        {
            autoadjustdimensions = 1;
        }
        else
        {
            autoadjustdimensions = 0;
        }

        if (mode == "add")
        {
            Pc.InsertintoPollSettings(pollId, pollbgcolor, ddlQuestionFont.SelectedItem.Value, questionbgcolor, questioncolor, questionitalic, questionbold, questionul, questionalign, questionfsize, ddlAnswerFont.SelectedItem.Value, answercolor, answerbold, answerul, answeritalic, answeralign, answerfsize, votebgcolor, ddlVoteFont.SelectedItem.Text, votefsize, voteitalic, voteul, votecolor, votebold, votealign, logoFileName, txtSponsorText.Text, txtSponsorLink.Text, msgbgcolor, msgcolor, ddlMsgFont.SelectedItem.Value, msgitalic, msgul, msgalign, msgfsize, msgbold, googletext, twittertext, fbtext, votecount, txtSharePoll.Text, txtVoteButtonLabel.Text, chartwidth, chartheight, txtWebLinkBgColor.Value);
        }
        else
        {
            Pc.UpdatePollSettings(pollId, pollbgcolor, ddlQuestionFont.SelectedItem.Value, questionbgcolor, questioncolor, questionitalic, questionbold, questionul, questionalign, questionfsize, ddlAnswerFont.SelectedItem.Value, answercolor, answerbold, answerul, answeritalic, answeralign, answerfsize, votebgcolor, ddlVoteFont.SelectedItem.Text, votefsize, voteitalic, voteul, votecolor, votebold, votealign, logoFileName, txtSponsorText.Text, txtSponsorLink.Text, msgbgcolor, msgcolor, ddlMsgFont.SelectedItem.Value, msgitalic, msgul, msgalign, msgfsize, msgbold, googletext, twittertext, fbtext, votecount, txtSharePoll.Text, txtVoteButtonLabel.Text, chartwidth, chartheight, autoadjustdimensions, txtWebLinkBgColor.Value);
        }
        if (ckbVotes.Checked == true)
        {
            votecount = 1;
        }
        else
        {
            votecount = 0;
        }
        if (ckbGoogle.Checked == true)
        {
            googletext = "google";
        }
        if (ckbTwitter.Checked == true)
        {
            twittertext = "twitter";
        }
        if (ckbFb.Checked == true)
        {
            fbtext = "fb";
        }
        txtQuestion.Attributes.Add("Style", "border-color:#CDCDCD");
        pollInfo = Pc.getPollInfo(pollId);
        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
        {
            if (txtVideo.Text != "")
            {
                videopreviewcreate.Style.Add("display", "block");
                List<string> iframe = new List<string>();
                string pattern = @"<(iframe)\b[^>]*>";
                string style = @"width=""(.*?)""";
                string styleheight = @"height=""(.*?)""";
                string iframewidth = txtWidthPoll.Text;

                string content = txtVideo.Text;
                Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                MatchCollection matches = rgx.Matches(content);
                if (matches.Count > 0)
                {
                    for (int i = 0, l = matches.Count; i < l; i++)
                    {
                        iframe.Add(matches[i].Value);
                        if (i > 0)
                            content = content.Replace(iframe[i], string.Empty);
                    }
                }
                if (iframe.Count > 0)
                {
                    string value = string.Empty;
                    string valueheight = string.Empty;
                    Regex styleRgx = new Regex(style, RegexOptions.IgnoreCase);
                    Regex styleRgxHeight = new Regex(styleheight, RegexOptions.IgnoreCase);
                    MatchCollection styleMatches = styleRgx.Matches(iframe[0]);
                    MatchCollection styleMatchesHeight = styleRgxHeight.Matches(iframe[0]);
                    string width = "";
                    if (styleMatches.Count > 0)
                    {
                        value = styleMatches[0].Value;
                        int widthif = Convert.ToInt32(Regex.Replace(value, @"\D", ""));
                        width = "width=" + "\"" + 850 + "\"";
                        content = content.Replace(iframe[0], iframe[0].Replace(value, width));

                    }
                    if (styleMatchesHeight.Count > 0)
                    {
                        int widthnew = Convert.ToInt32(Regex.Replace(width, @"\D", ""));
                        valueheight = styleMatchesHeight[0].Value;
                        int heightif = Convert.ToInt32(Regex.Replace(valueheight, @"\D", ""));
                        var ratio = (double)Convert.ToDouble(widthnew) / Convert.ToDouble(16);
                        var newHeight = (int)(ratio * 9);
                        string height = "height=" + "\"" + 480 + "\"";
                        content = content.Replace(valueheight, height);
                    }
                }
                videopreviewcreate.InnerHtml = content;
                videoText = content;
                txtVideo.Text = videoText;
            }
        }
        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
        {

            imageText = pollInfo.Tables[0].Rows[0]["imagelink"].ToString();
            hdnImageLink.Value = imageText;
            hdnImageName.Value = imageText;
        }
        if (radMultipleAnswer.Checked == true)
        {
            questionType = 2;
        }
        if (chkComments.Checked == true)
        {
            comments = 1;
        }
        else
        {
            comments = 0;

        }

        if (ckbSingleVote.Checked == true)
        {
            uniqueRespondent = 1;
        }
        else
        {
            uniqueRespondent = 0;

        }

        if (chkEmail.Checked)
        {
            emailrequired = 1;
        }
        if (chkPhone.Checked)
        {
            phonerequired = 1;
        }
        if (ckbSubmit.Checked)
        {
            showsubmitbutton = 1;
        }
        if (chkSameAnswer.Checked)
        {
            issamethankyoumessageused = 1;
        }
        else
        {
            issamethankyoumessageused = 0;
        }

        //PollId, Question, VideoCode, Imagelink, Questiontype, Commentreq, uniqueRespondent, votebuttontext, invitationmessage, isemailrequired, isphonerequired, redirecturl, showsubmitbutton, submitbuttontext
        Pc.InsertintoPollInfo(pollId, txtQuestion.Text, videoText, imageText, questionType, comments, uniqueRespondent, txtVoteButtonLabel.Text, txtInvitationMessage.Text, emailrequired, phonerequired, txtRedirectURL.Text, showsubmitbutton, txtSubmitButtonLabel.Text, issamethankyoumessageused);
        
        questionInfo = Pc.GetQuestionInfo(pollId);
        if (questionInfo != null && questionInfo.Rows.Count > 0)
        {
            Pc.UpdateQuestion(pollId, txtQuestion.Text);
        }
        else
        {
            Pc.InsertintoQuestion(pollId, txtQuestion.Text);
        }
        if (radLeadGen.Checked)
        {
            resultaccess = "private";
        }
        else
        {
            resultaccess = "public";
        }
        Pc.UpdatePollinfo(pollId, resultaccess, txtResultText.Text, insightoLink);

        AddColumns();

        DataSet dspollstattus = Pc.getPollInfo(pollId);
        if (dspollstattus.Tables[0].Rows[0]["status"].ToString() != "Closed")
        {
            int repitems = 0;
            if (dspollstattus.Tables[0].Rows[0]["status"].ToString() == "Draft")
            {
                Pc.deleteAnswerOptions(pollId);
                for (int i = 0; i <= (Convert.ToInt32(hdnRepeaterTxts.Value)); i++)
                {
                    string id = "txtAnswerOption_" + i;
                    string AnswerOption = Request.Form[id];
                    string thankyouoption = "";
                    if (AnswerOption != null && AnswerOption != "")
                    {
                        if (chkSameAnswer.Checked)
                        {
                            thankyouoption = txtInvitationMessage.Text;
                        }
                        else
                        {
                            thankyouoption = Request.Form["txtThankYouOption_" + i.ToString()];
                        }
                        Pc.SaveAnswerOptions(AnswerOption, pollId, thankyouoption);
                    }
                }
            }
            else
            {
                tbAnswerOption = Pc.getAnswerOptions(pollId);
                hdnRepeaterTxts.Value = Convert.ToString(tbAnswerOption.Rows.Count);

                if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
                {
                    hdnRepeaterTxts.Value = Convert.ToString(tbAnswerOption.Rows.Count);
                    int i = 0;
                    foreach (DataRow row in tbAnswerOption.Rows)
                    {
                        dtAnswerOptions.Rows.Add(i, System.Web.HttpUtility.HtmlEncode(row["answeroption"].ToString().Replace("\r", "").Replace("\n", "")), "Remove", "Add", Convert.ToInt32(row["pk_answerid"].ToString()), row["InvitationThankYouMessage"].ToString());
                        i++;
                    }
                }

                dtAnswerids = dtAnswerOptions;

                for (int i = 0; i <= (Convert.ToInt32(hdnRepeaterTxts.Value)); i++)
                {
                    string id = "txtAnswerOption_" + i;
                    string AnswerOption = Request.Form[id];
                    string thankyouoption = "";
                    if (chkSameAnswer.Checked)
                    {
                        thankyouoption = txtInvitationMessage.Text;
                    }
                    else
                    {
                        thankyouoption = Request.Form["txtThankYouOption_" + i.ToString()];
                    }

                    if (AnswerOption != null && AnswerOption != "")
                    {

                        DataRow[] drEdit = dtAnswerids.Select("AnswerId=" + i);
                        try
                        {
                            Pc.UpdateAnswerOptions(Convert.ToInt32(drEdit[0][4].ToString()), AnswerOption, thankyouoption );

                        }
                        catch (Exception)
                        {
                        }

                    }
                }
            }
        }


        tbAnswerOption = Pc.getAnswerOptions(pollId);
        int reitems = addRemoveControls.Items.Count;

        strRootURL = ConfigurationSettings.AppSettings["RootURL"].ToString();
        string logourl = strRootURL + "Polls/images/Sample_logo.png";

        DataSet dspollInfo = Pc.getpollchartinfo(pollId);

        HtmlTable qt3tableques = new HtmlTable();
        qt3tableques.Width = "100%";
        int rowscount;

        if ((tbAnswerOption.Rows.Count == 0) || (tbAnswerOption.Rows.Count == 1))
        {
            rowscount = 2;
        }
        else
        {
            rowscount = tbAnswerOption.Rows.Count;
        }

        if (pollInfo.Tables[0].Rows[0][8].ToString() == "" || pollInfo.Tables[0].Rows[0][8].ToString() == null)
        {
            Pc.SaveIframeInfo(pollId, Convert.ToInt32(txtWidthPoll.Text), Convert.ToInt32(txtHeightPoll.Text), Convert.ToInt32(lblRecThemeWidth.Text), Convert.ToInt32(hdnRecHeightne.Value));
            if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
            {
                Pc.SaveImageSpec(pollId, Convert.ToInt32(hdnImageRecWidth.Value), Convert.ToInt32(hdnImageRecHeight.Value));
            }
        }

        dtAnswerOptions.Clear();
        tbAnswerOption = Pc.getAnswerOptions(pollId);
        int noofans = tbAnswerOption.Rows.Count;
        int diff = 0;
        if (noofans < Convert.ToInt32(hdnRepeaterTxts.Value))
        {
            diff = Convert.ToInt32(hdnRepeaterTxts.Value) - noofans;
            for (int i = 0; i < diff; i++)
            {
                hdnRecHeightne.Value = Convert.ToString(Convert.ToInt32(hdnRecHeightne.Value) - 20);
            }
            Pc.SaveModiyIframeSpec(pollId, Convert.ToInt32(hdnRecHeightne.Value));
        }
        hdnRepeaterTxts.Value = Convert.ToString(tbAnswerOption.Rows.Count);
        if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
        {
            int i = 0;
            foreach (DataRow row in tbAnswerOption.Rows)
            {
                dtAnswerOptions.Rows.Add(i, System.Web.HttpUtility.HtmlEncode(row["answeroption"].ToString().Replace("\r", "").Replace("\n", "")), "Remove", "Add",null, row["InvitationThankYouMessage"].ToString());
                
                count++;
                i++;
            }
            if (count == 1)
            {
                dtAnswerOptions.Rows.Add(1, "", "Remove", "Add",null, "");
            }
        }

        divcreatebutton.Style.Add("background-color", "#FFF");
        divdesignbutton.Style.Add("background-color", bgcolour);
        divlaunchbutton.Style.Add("background-color", "#FFF");
        create.Style.Add("display", "none");
        design.Style.Add("display", "block");
        launchafter.Style.Add("display", "none");
        launch.Style.Add("display", "none");
        CreateNew.ForeColor = Color.Red;
        DesignNew.ForeColor = Color.White;
        LaunchNew.ForeColor = Color.Red;
        updateParentWindowJavascript(pollInfo, pollId.ToString());
        BindWithRepeater();
        BindDesignStyles();

        ClientScript.RegisterClientScriptBlock(this.GetType(), "EditsSaved3" + DateTime.Now.Millisecond.ToString(), "<script>$(document).ready(function() { Materialize.toast('Your edits have been saved',5000); } );</script>");

        hdnCurrentPage.Value = "";
        createUpdatePanel.Update();
    }

    //WHEN YOU CLICK THE LAUNCH TAB ON THE TOP
    protected void LaunchNew_Click(object sender, EventArgs e)
    {
        try
        {

            if (txtWidthPoll.Text == "")
            {
                launcharrow.Style.Add("display", "none");
                launchafter.Style.Add("display", "none");
                launch.Style.Add("display", "none");
                create.Style.Add("display", "block");
                createarrow.Style.Add("display", "block");
                return;
            }
            if (txtHeightPoll.Text == "")
            {
                launcharrow.Style.Add("display", "none");
                launchafter.Style.Add("display", "none");
                launch.Style.Add("display", "none");
                create.Style.Add("display", "block");
                createarrow.Style.Add("display", "block");
                return;
            }
            if (ckbVotes.Checked == true)
            {
                votecount = 1;
            }
            else
            {
                votecount = 0;
            }
            txtQuestion.Attributes.Add("Style", "border-color:#CDCDCD");
            pollInfo = Pc.getPollInfo(pollId);

            HiddenField2.Value = pollInfo.Tables[0].Rows[0]["status"].ToString();

            if (ckbGoogle.Checked == true)
            {
                googletext = "google";
            }
            if (ckbTwitter.Checked == true)
            {
                twittertext = "twitter";
            }
            if (ckbFb.Checked == true)
            {
                fbtext = "fb";
            }
            if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
            {
                if (txtVideo.Text != "")
                {
                    videopreviewcreate.Style.Add("display", "block");
                    List<string> iframe = new List<string>();
                    string pattern = @"<(iframe)\b[^>]*>";
                    string style = @"width=""(.*?)""";
                    string styleheight = @"height=""(.*?)""";
                    string iframewidth = txtWidthPoll.Text;

                    string content = txtVideo.Text;
                    Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                    MatchCollection matches = rgx.Matches(content);
                    if (matches.Count > 0)
                    {
                        for (int i = 0, l = matches.Count; i < l; i++)
                        {
                            iframe.Add(matches[i].Value);
                            if (i > 0)
                                content = content.Replace(iframe[i], string.Empty);
                        }
                    }
                    if (iframe.Count > 0)
                    {
                        string value = string.Empty;
                        string valueheight = string.Empty;
                        Regex styleRgx = new Regex(style, RegexOptions.IgnoreCase);
                        Regex styleRgxHeight = new Regex(styleheight, RegexOptions.IgnoreCase);
                        MatchCollection styleMatches = styleRgx.Matches(iframe[0]);
                        MatchCollection styleMatchesHeight = styleRgxHeight.Matches(iframe[0]);
                        string width = "";
                        if (styleMatches.Count > 0)
                        {
                            value = styleMatches[0].Value;
                            int widthif = Convert.ToInt32(Regex.Replace(value, @"\D", ""));
                            width = "width=" + "\"" + 850 + "\"";
                            content = content.Replace(iframe[0], iframe[0].Replace(value, width));

                        }
                        if (styleMatchesHeight.Count > 0)
                        {
                            int widthnew = Convert.ToInt32(Regex.Replace(width, @"\D", ""));
                            valueheight = styleMatchesHeight[0].Value;
                            int heightif = Convert.ToInt32(Regex.Replace(valueheight, @"\D", ""));
                            var ratio = (double)Convert.ToDouble(widthnew) / Convert.ToDouble(16);
                            var newHeight = (int)(ratio * 9);
                            string height = "height=" + "\"" + 480 + "\"";
                            content = content.Replace(valueheight, height);
                        }
                    }
                    videopreviewcreate.InnerHtml = content;
                    videoText = content;
                    txtVideo.Text = videoText;
                }
            }

            if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
            {

                imageText = pollInfo.Tables[0].Rows[0]["imagelink"].ToString();
                hdnImageLink.Value = imageText;
                hdnImageName.Value = imageText;
            }

            if (radMultipleAnswer.Checked == true)
            {
                questionType = 2;
            }

            if (chkComments.Checked == true)
            {
                comments = 1;
            }
            else
            {
                comments = 0;
            }
            if (ckbSingleVote.Checked)
            {
                uniqueRespondent = 1;
            }
            else
            {
                uniqueRespondent = 0;
            }

            if (chkEmail.Checked)
            {
                emailrequired = 1;
            }
            if (chkPhone.Checked)
            {
                phonerequired = 1;
            }
            if (ckbSubmit.Checked)
            {
                showsubmitbutton = 1;
            }
            if (chkSameAnswer.Checked)
            {
                issamethankyoumessageused = 1;
            }
            else
            {
                issamethankyoumessageused = 0;
            }

            //PollId, Question, VideoCode, Imagelink, Questiontype, Commentreq, uniqueRespondent, votebuttontext, invitationmessage, isemailrequired, isphonerequired, redirecturl, showsubmitbutton, submitbuttontext
            Pc.InsertintoPollInfo(pollId, txtQuestion.Text, videoText, imageText, questionType, comments, uniqueRespondent, txtVoteButtonLabel.Text, txtInvitationMessage.Text, emailrequired, phonerequired, txtRedirectURL.Text, showsubmitbutton, txtSubmitButtonLabel.Text, issamethankyoumessageused);
            
            questionInfo = Pc.GetQuestionInfo(pollId);
            if (questionInfo != null && questionInfo.Rows.Count > 0)
            {
                Pc.UpdateQuestion(pollId, txtQuestion.Text);
            }
            else
            {
                Pc.InsertintoQuestion(pollId, txtQuestion.Text);
            }

            questionbold = hdnQuestionBold.Value;
            questionitalic = hdnQuestionItalic.Value;
            questionul = hdnQuestionUL.Value;
            if (hdnQuestionAlign.Value != "")
            {
                questionalign = hdnQuestionAlign.Value;
            }
            else
            {
                questionalign = "left";
            }
            questionfsize = hdnQuestionFSize.Value;
            answerbold = hdnAnswerBold.Value;
            answeritalic = hdnAnswerItalic.Value;
            answerul = hdnAnswerUL.Value;
            answerfsize = hdnAnswerFSize.Value;
            if (hdnAnswerAlign.Value != "")
            {
                answeralign = hdnAnswerAlign.Value;
            }
            else
            {
                answeralign = "left";
            }
            votebold = hdnVoteBold.Value;
            voteitalic = hdnVoteItalic.Value;
            voteul = hdnVoteUL.Value;
            votefsize = hdnVoteFSize.Value;
            if (hdnVoteAlign.Value != "")
            {
                votealign = hdnVoteAlign.Value;
            }
            else
            {
                votealign = "center";
            }
            if (hdnMsgAlign.Value != "")
            {
                msgalign = hdnMsgAlign.Value;
            }
            else
            {
                msgalign = "left";
            }
            msgbold = hdnMsgBold.Value;
            msgfsize = hdnMsgFSize.Value;
            msgitalic = hdnMsgItalic.Value;
            msgul = hdnMsgUL.Value;
            if (txtPollBgColor.Value != "")
            {
                pollbgcolor = txtPollBgColor.Value;
            }
            else
            {
                pollbgcolor = "ffffff";
            }
            if (txtQuestionBgColor.Value != "")
            {
                questionbgcolor = txtQuestionBgColor.Value;
            }
            else
            {
                questionbgcolor = "E7E7E7";
            }
            if (txtQuestionColor.Value != "")
            {
                questioncolor = txtQuestionColor.Value;
            }
            else
            {
                questioncolor = "000000";
            }
            if (txtVoteColor.Value != "")
            {
                votecolor = txtVoteColor.Value;
            }
            else
            {
                votecolor = "ffffff";
            }
            if (txtVoteBgColor.Value != "")
            {
                votebgcolor = txtVoteBgColor.Value;
            }
            else
            {
                votebgcolor = "6C89E1";
            }
            if (txtAnswerColor.Value != "")
            {
                answercolor = txtAnswerColor.Value;
            }
            else
            {
                answercolor = "000000";
            }
            if (txtMessageBgColor.Text != "")
            {
                msgbgcolor = txtMessageBgColor.Text;
            }
            else
            {
                msgbgcolor = "cccccc";
            }
            if (txtMessageColor.Text != "")
            {
                msgcolor = txtMessageColor.Text;
            }
            else
            {
                msgcolor = "000000";
            }
            if (ckbInsightoBrand.Checked == true)
            {
                insightoLink = "no";
            }
            else
            {
                insightoLink = "yes";
            }
            if (radLeadGen.Checked == true)
            {
                resultaccess = "private";
            }
            else
            {
                resultaccess = "public";
            }
            dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
            if (ckbAddSponsor.Checked == true)
            {
                if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "")
                {
                    logoFileName = dtSettingsInfo.Rows[0]["sponsorlogo"].ToString();
                    lblLogoName.Value = logoFileName;
                    lblLogoName.Visible = true;
                    hdnLogoLink.Value = "exist";
                    //txtSponsorText.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                    if (txtSponsorText.Text == "")
                    {
                        txtSponsorText.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                    }
                }
                else
                {
                    create.Style.Add("display", "none");
                    design.Style.Add("display", "block");
                    launch.Style.Add("display", "none");
                    divcreatebutton.Style.Add("background-color", "#FFF");
                    divdesignbutton.Style.Add("background-color", bgcolour);
                    CreateNew.ForeColor = Color.Red;
                    DesignNew.ForeColor = Color.White;
                    LaunchNew.ForeColor = Color.Red;
                    divlaunchbutton.Style.Add("background-color", "#FFF");
                    SponsorBox.Style.Add("display", "block");
                    ckbAddSponsor.Checked = true;
                    sponsorpreviw.Style.Add("display", "block");
                    sponsorResults.Style.Add("display", "block");
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "sponsorAlert", @"<script>$(document).ready(function(){Materialize.toast('Please select a sponsor image',4000);});</script>");
                    return;
                }
            }
            else
            {
                txtSponsorLink.Text = "";
                txtSponsorText.Text = "";
            }
            if (dtSettingsInfo != null && dtSettingsInfo.Rows.Count > 0)
            {
                mode = "edit";
            }
            else
            {
                mode = "add";
            }

            chartwidth = 0;
            chartheight = 0;
            Int32.TryParse(hdnChartWidth.Value, out chartwidth);
            Int32.TryParse(hdnChartHeight.Value, out chartheight);

            if ((ckbAutoAdjust.Checked == true) && (ckbAutoAdjustDesign.Checked == true))
            {
                autoadjustdimensions = 1;
            }
            else
            {
                autoadjustdimensions = 0;
            }

            if (mode == "add")
            {
                Pc.InsertintoPollSettings(pollId, pollbgcolor, ddlQuestionFont.SelectedItem.Value, questionbgcolor, questioncolor, questionitalic, questionbold, questionul, questionalign, questionfsize, ddlAnswerFont.SelectedItem.Value, answercolor, answerbold, answerul, answeritalic, answeralign, answerfsize, votebgcolor, ddlVoteFont.SelectedItem.Text, votefsize, voteitalic, voteul, votecolor, votebold, votealign, logoFileName, txtSponsorText.Text, txtSponsorLink.Text, msgbgcolor, msgcolor, ddlMsgFont.SelectedItem.Value, msgitalic, msgul, msgalign, msgfsize, msgbold, googletext, twittertext, fbtext, votecount, txtSharePoll.Text, txtVoteButtonLabel.Text, chartwidth, chartheight, txtWebLinkBgColor.Value);
            }
            else
            {
                Pc.UpdatePollSettings(pollId, pollbgcolor, ddlQuestionFont.SelectedItem.Value, questionbgcolor, questioncolor, questionitalic, questionbold, questionul, questionalign, questionfsize, ddlAnswerFont.SelectedItem.Value, answercolor, answerbold, answerul, answeritalic, answeralign, answerfsize, votebgcolor, ddlVoteFont.SelectedItem.Text, votefsize, voteitalic, voteul, votecolor, votebold, votealign, logoFileName, txtSponsorText.Text, txtSponsorLink.Text, msgbgcolor, msgcolor, ddlMsgFont.SelectedItem.Value, msgitalic, msgul, msgalign, msgfsize, msgbold, googletext, twittertext, fbtext, votecount, txtSharePoll.Text, txtVoteButtonLabel.Text, chartwidth, chartheight, autoadjustdimensions, txtWebLinkBgColor.Value);
            }
            Pc.UpdatePollinfo(pollId, resultaccess, txtResultText.Text, insightoLink);

            hdnRecWidth.Value = txtWidthIframe.Text;
            if (hdnRecHeightne.Value == "")
            {
                hdnRecHeightne.Value = txtHeightIframe.Text;
            }
            if (lblHeight.Text == "")
            {
                lblHeight.Text = hdnRecHeightne.Value;
            }
            if (lblWidth.Text == "")
            {
                lblWidth.Text = txtWidthIframe.Text;
            }
            if (pollInfo.Tables[0].Rows[0][8].ToString() == "" || pollInfo.Tables[0].Rows[0][8].ToString() == null)
            {
                Pc.SaveIframeInfo(pollId, Convert.ToInt32(txtWidthPoll.Text), Convert.ToInt32(txtHeightPoll.Text), Convert.ToInt32(lblRecThemeWidth.Text), Convert.ToInt32(hdnRecHeightne.Value));
                if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
                {
                    Pc.SaveImageSpec(pollId, Convert.ToInt32(hdnImageRecWidth.Value), Convert.ToInt32(hdnImageRecHeight.Value));
                }
            }


            AddColumns();

            DataSet dspollstattus = Pc.getPollInfo(pollId);
            if (dspollstattus.Tables[0].Rows[0]["status"].ToString() != "Closed")
            {
                int repitems = 0;
                if (dspollstattus.Tables[0].Rows[0]["status"].ToString() == "Draft")
                {
                    Pc.deleteAnswerOptions(pollId);
                    for (int i = 0; i <= (Convert.ToInt32(hdnRepeaterTxts.Value)); i++)
                    {
                        string id = "txtAnswerOption_" + i;
                        string AnswerOption = Request.Form[id];
                        string thankyouoption = "";
                        if (chkSameAnswer.Checked)
                        {
                            thankyouoption = txtInvitationMessage.Text;
                        }
                        else
                        {
                            thankyouoption = Request.Form["txtThankYouOption_" + i.ToString()];
                        }

                        if (AnswerOption != null && AnswerOption != "")
                        {
                            Pc.SaveAnswerOptions(AnswerOption, pollId, thankyouoption);
                        }
                    }
                }
                else
                {
                    tbAnswerOption = Pc.getAnswerOptions(pollId);
                    hdnRepeaterTxts.Value = Convert.ToString(tbAnswerOption.Rows.Count);

                    if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
                    {
                        hdnRepeaterTxts.Value = Convert.ToString(tbAnswerOption.Rows.Count);
                        int i = 0;
                        foreach (DataRow row in tbAnswerOption.Rows)
                        {
                            dtAnswerOptions.Rows.Add(i, System.Web.HttpUtility.HtmlEncode(row["answeroption"].ToString().Replace("\r", "").Replace("\n", "")), "Remove", "Add",Convert.ToInt32(row["pk_answerid"].ToString()), row["InvitationThankYouMessage"].ToString());
                            i++;
                        }
                    }

                    dtAnswerids = dtAnswerOptions;

                    for (int i = 0; i <= (Convert.ToInt32(hdnRepeaterTxts.Value)); i++)
                    {
                        string id = "txtAnswerOption_" + i;
                        string AnswerOption = Request.Form[id];
                        string thankyouoption = "";
                        if (chkSameAnswer.Checked)
                        {
                            thankyouoption = txtInvitationMessage.Text;
                        }
                        else
                        {
                            thankyouoption = Request.Form["txtThankYouOption_" + i.ToString()];
                        }
                        if (AnswerOption != null && AnswerOption != "")
                        {

                            DataRow[] drEdit = dtAnswerids.Select("AnswerId=" + i);
                            Pc.UpdateAnswerOptions(Convert.ToInt32(drEdit[0][4].ToString()), AnswerOption, thankyouoption);

                        }
                    }
                }
            }

            tbAnswerOption = Pc.getAnswerOptions(pollId);
            strRootURL = ConfigurationSettings.AppSettings["RootURL"].ToString();
            string logourl = strRootURL + "Polls/images/Sample_logo.png";

            DataSet dspollInfo = Pc.getpollchartinfo(pollId);

            HtmlTable qt3tableques = new HtmlTable();
            qt3tableques.Width = "100%";
            int rowscount;

            if ((tbAnswerOption.Rows.Count == 0) || (tbAnswerOption.Rows.Count == 1))
            {
                rowscount = 2;
            }
            else
            {
                rowscount = tbAnswerOption.Rows.Count;
            }

            dtAnswerOptions.Clear();
            tbAnswerOption = Pc.getAnswerOptions(pollId);
            int noofans = tbAnswerOption.Rows.Count;
            int diff = 0;
            if (noofans < Convert.ToInt32(hdnRepeaterTxts.Value))
            {
                diff = Convert.ToInt32(hdnRepeaterTxts.Value) - noofans;
                for (int i = 0; i < diff; i++)
                {
                    hdnRecHeightne.Value = Convert.ToString(Convert.ToInt32(hdnRecHeightne.Value) - 20);
                }
            }
            hdnRepeaterTxts.Value = Convert.ToString(tbAnswerOption.Rows.Count);
            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                int i = 0;
                foreach (DataRow row in tbAnswerOption.Rows)
                {
                    dtAnswerOptions.Rows.Add(i, System.Web.HttpUtility.HtmlEncode(row["answeroption"].ToString().Replace("\r", "").Replace("\n", "")), "Remove", "Add", null, row["InvitationThankYouMessage"].ToString());
                    count++;
                    i++;
                }
                if (count == 1)
                {
                    dtAnswerOptions.Rows.Add(1, "", "Remove", "Add",null ,"");
                }
            }
            BindWithRepeater();
            BindDesignStyles();

            string EmbedUrl1 = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId + "&Embed=" + "Emb&pollsource-slider");
            //EmbedUrl1 = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId + "");
            EmbedUrl1 = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "source=embed&PollId=" + pollId + "");
            string pollLaunchUrl1 = rootURL + EmbedUrl1;

            previewurl = "<iframe id='IframeLaunch' height='" + txtHeightIframe.Text + "' width='" + txtWidthIframe.Text + "'  scrolling='auto' src='" + pollLaunchUrl1 + "' frameborder='0'   style='-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px; -moz-box-shadow: 4px 4px 14px #000; -webkit-box-shadow: 4px 4px 14px #000; box-shadow: 4px 4px 14px #000;'  ></iframe>";


            if (pollInfo.Tables[0].Rows[0]["status"].ToString() == "Active")
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "LaunchAfter" + DateTime.Now.Millisecond.ToString(), "<script>$(window).load(function() { $('#MainContent_launchafter').show(); $('#launchoptions').show(); $('#hrefweb').addClass('active'); } );</script>");
                launch.Style.Add("display", "none");
                ckbAddSponsor.Enabled = false;
                ckbInsightoBrand.Enabled = false;
                chkComments.Enabled = true;
                chkComments.Style.Add("disabled", "disabled");
                txtHeightPoll.Enabled = false;
                txtWidthPoll.Enabled = false;
                lblRecThemeHeight.Enabled = false;
                lblRecThemeWidth.Enabled = false;
                recheightincreate.Style.Add("display", "none");
            }
            divcreatebutton.Style.Add("background-color", "#FFF");
            divdesignbutton.Style.Add("background-color", "#FFF");
            divlaunchbutton.Style.Add("background-color", bgcolour);
            CreateNew.ForeColor = Color.Red;
            DesignNew.ForeColor = Color.Red;
            LaunchNew.ForeColor = Color.White;
            create.Style.Add("display", "none");
            design.Style.Add("display", "none");
            launchafter.Style.Add("display", "none");
            launch.Style.Add("display", "block");
            if (pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() != pollId.ToString())
            {
                btnActivateParentPoll.Style.Add("display", "block");
                btnActivatePoll.Visible = false;
            }
            else
            {
                btnActivateParentPoll.Style.Add("display", "none");
                btnActivatePoll.Visible = true;
            }
            updateParentWindowJavascript(pollInfo, pollId.ToString());
            if (pollInfo.Tables[0].Rows[0]["status"].ToString() == "Active")
            {
                create.Style.Add("display", "none");
                design.Style.Add("display", "none");
                launch.Style.Add("display", "none");
                ClientScript.RegisterClientScriptBlock(this.GetType(), "LaunchAfter" + DateTime.Now.Millisecond.ToString(), "<script>$(window).load(function() { $('#MainContent_launchafter').show(); $('#launchoptions').show(); $('#hrefweb').addClass('active'); } );</script>");
            }
            ClientScript.RegisterClientScriptBlock(this.GetType(), "EditsSaved1" + DateTime.Now.Millisecond.ToString(), "<script>$(document).ready(function() { Materialize.toast('Your edits have been saved',5000); } );</script>");
            hdnCurrentPage.Value = "launch";
            createUpdatePanel.Update();
        }
        catch (Exception)
        {
        }
    }

    public static string StripTagsRegex(string source)
    {
        return Regex.Replace(source, "<.*?>", string.Empty);
    }

    protected void btnPreview_Click(object sender, EventArgs e)
    {
        // MthdGenerateIframe();
        var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
        var userActivationDetails = ServiceFactory<UsersService>.Instance.GetUserActivationFlag(userSessionDetails.UserId);

        if (userActivationDetails.ACTIVATION_FLAG == 1)
        {
            lblErrorMsg.Visible = false;
            lblErrorMsg1.Visible = false;


            pollInfo = Pc.getPollInfo(pollId);

            dtSettingsInfo = Pc.getPollSettingsInfo(pollId);

            if (pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() != "")
            {
                pollLink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()));
                //pollLink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "source=embed&PollId=" + Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()));
            }
            else
            {
                pollLink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId);
                //pollLink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "source=embed&PollId=" + pollId);
            }
            
            pollLaunchUrl = rootURL + pollLink;
            lblWidth.Text = lblWidth.Text;
            IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
            string shortened = "";
            try
            {
                s.Shorten(pollLaunchUrl);
                if (shortened != null)
                {
                    pollLaunchUrl = shortened;
                }
            }
            catch (Exception)
            {
            }
            pollLink = pollLaunchUrl;

            string EmbedUrl = "";
            if (pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() != "")
            {
                EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()) + "&Embed=Emb&pollsource=slider");
                //EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "source=embed&PollId=" + Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()) + "&Embed=Emb&pollsource=slider");
            }
            else
            {
                EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId + "&Embed=Emb&pollsource=slider");
                //EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "source=embed&PollId=" + pollId + "&Embed=Emb&pollsource=slider");
            }

            //  string EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId + "&Embed=" + "Emb");
            pollLaunchUrl = rootURL + EmbedUrl;
            //shortened = s.Shorten(pollLaunchUrl);
            //if (shortened != null)
            //{
            //    pollLaunchUrl = shortened;
            //}
            //lblWidth1.Text = hdnRecWidth.Value;
            //lblHeight1.Text = hdnRecHeightne.Value;

            string iframeembedcode = "";//<iframe id='IframeLaunch' height='" + //txtHeightIframe1.Text + "' width='" + //txtWidthIframe1.Text + "'  scrolling='auto' src='" + pollLaunchUrl + "' frameborder='0'   style='-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px; -moz-box-shadow: 4px 4px 14px #000; -webkit-box-shadow: 4px 4px 14px #000; box-shadow: 4px 4px 14px #000;'  ></iframe>";

            Pc.SaveLaunchInfo(pollId, uniqueRespondent, pollLink, iframeembedcode, 1);

            string strposition = "";
            string strsliderdisplay = "";

            for (int i = 0; i < rblposition.Items.Count; i++)
            {
                if (rblposition.Items[i].Selected)
                {
                    strposition = rblposition.Items[i].Value;
                }
            }

            for (int i = 0; i < rblspeed.Items.Count; i++)
            {
                if (rblspeed.Items[i].Selected)
                {
                    strsliderdisplay = rblspeed.Items[i].Text;
                }
            }

            string devicename = "";

            for (int i = 0; i < chklstDevice.Items.Count; i++)
            {
                if (chklstDevice.Items[i].Selected)
                {
                    devicename += chklstDevice.Items[i].Text + ",";
                }
            }

            if (devicename != "")
            {
                devicename = devicename.Remove(devicename.Length - 1, 1);
            }


            Pc.UpdateSliderPollsettings(strposition, strsliderdisplay, Convert.ToInt32(txtFreqvisit.Text), pollId, tbCountries.Text, tbcities.Text, devicename);

            pollInfo = Pc.getPollInfo(pollId);

            string lbliframe = pollInfo.Tables[0].Rows[0][7].ToString();

            StringBuilder sb = new StringBuilder(30000);

            DataSet dscountrycode = Pc.getPollCountries();

            string countries = tbCountries.Text;
            var arrCountries = countries.Split(new char[] { ',' });
            countries = "";
            for (int x = 0; x < arrCountries.Length; x++)
            {
                countries += "'" + arrCountries[x] + "',";
            }

            //DataRow[] drcc = dscountrycode.Tables[0].Select("CountryName = '" + tbCountries.Text + "'");
            DataRow[] drcc = dscountrycode.Tables[0].Select("CountryName IN (" + countries.Substring(0, countries.Length - 1) + ")");

            sb.Append("jQuery(document).ready(function () { ");

            countries = "";
            for (int r = 0; r < drcc.Length; r++)
            {
                countries += drcc[r][3].ToString().Trim() + ",";
            }

            sb.Append("var countryname='" + countries + "';");
            sb.Append("var cityname='" + tbcities.Text + "';");

            sb.Append("var clientcountryname='';");
            sb.Append("var clientcityname='';");
            sb.Append("var user = getCookie('insightoslidername');");
            sb.Append("var cnttimes = getCookie('counttimes');");
            sb.Append("var apid = '" + pollId + "';");
            sb.Append("var bspid = '" + pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() + "';");
            sb.Append("var cookieapid = getCookie('apid');");
            sb.Append("var cookiebspid = getCookie('bspid');");
            sb.Append("if(cookieapid == ''){document.cookie = 'apid=' + apid;cookieapid = apid;cnttimes=0;}");
            sb.Append("else if(cookieapid != apid){document.cookie = 'apid=' + apid; cookieapid = apid;cnttimes=0;}");

            sb.Append("if(cookiebspid == ''){document.cookie = 'bspid=' + bspid; cookiebspid = bspid; cnttimes = 0;}");
            sb.Append("else if(cookiebspid != bspid){document.cookie = 'bspid=' + bspid;cookiebspid = bspid;}");

            sb.Append("if (cnttimes < " + txtFreqvisit.Text + "){");
            sb.Append("jQuery.ajax({");
            sb.Append("url: '//freegeoip.net/json/',");
            sb.Append("type: 'POST',");
            sb.Append("dataType: 'jsonp',");
            sb.Append("success: function (location) {");
            sb.Append("clientcountryname = location.country_code;");
            sb.Append("clientcityname = location.city;");
            sb.Append("var valcnt = countryname.indexOf(clientcountryname);");
            sb.Append("var valcity = cityname.indexOf(clientcityname);");

            //CONDITIONS TO CHECK INCLUDE IF COUNTRY+CITY ARE PROVIDED OR COUNTRY ALONE IS PROVIDED OR NEITHER IS PROVIDED
            sb.Append("if (((valcnt >= 0) && (valcity >= 0) && (countryname != '') && (cityname != '')) || ((valcnt >= 0) && (countryname != '') && (cityname == '')) || ((countryname == '') && (cityname == ''))) {");
            sb.Append("var output = document.createElement('div');");


            if (rblposition.Items[0].Selected == true)
            {
                sb.Append("output.setAttribute('id', 'pSlider');");
                sb.Append("output.setAttribute('class', 'pprev_style pprev_CM');");
            }
            else if (rblposition.Items[1].Selected == true)
            {
                sb.Append("output.setAttribute('id', 'pSliderBL');");
                sb.Append("output.setAttribute('class', 'pprev_style pprev_BL');");
            }
            else if (rblposition.Items[2].Selected == true)
            {
                sb.Append("output.setAttribute('id', 'pSliderBR');");
                sb.Append("output.setAttribute('class', 'pprev_style pprev_BR');");
            }
            else if (rblposition.Items[3].Selected == true)
            {
                sb.Append("output.setAttribute('id', 'pSliderCL');");
                sb.Append("output.setAttribute('class', 'pprev_style pprev_LC');");
            }
            else if (rblposition.Items[4].Selected == true)
            {
                sb.Append("output.setAttribute('id', 'pSliderCR');");
                sb.Append("output.setAttribute('class', 'pprev_style pprev_RC');");
            }
            sb.Append("var ele = document.createElement('div');");
            sb.Append("ele.setAttribute('id', 'timedrpact1');");

            if (rblposition.Items[0].Selected == true)
            {
                sb.Append("ele.setAttribute('class', 'pprev_style pprev_CM');");
            }
            else if (rblposition.Items[1].Selected == true)
            {
                sb.Append("ele.setAttribute('class', 'pprev_style pprev_BL');");
            }
            else if (rblposition.Items[2].Selected == true)
            {
                sb.Append("ele.setAttribute('class', 'pprev_style pprev_BR');");
            }
            else if (rblposition.Items[3].Selected == true)
            {
                sb.Append("ele.setAttribute('class', 'pprev_style pprev_LC');");
            }
            else if (rblposition.Items[4].Selected == true)
            {
                sb.Append("ele.setAttribute('class', 'pprev_style pprev_RC');");
            }

            //COMMENTED BY SATISH AS THE IFRAME WILL BE ADDED VIA IFR VARIABLE BELOW
            //sb.Append("ele.innerHTML = " + "\"" + lbliframe + "\"" + ";");

            sb.Append("var alclose = document.createElement('div');");
            sb.Append("alclose.setAttribute('id', 'alertclose');");
            sb.Append("alclose.setAttribute('class', 'closeFlyoutPromo');");
            sb.Append("ele.appendChild(alclose);");

            sb.Append("output.appendChild(ele);");
            sb.Append("jQuery('body').append(output);");

            int intmillsecs = 0;

            if (rblspeed.Items[0].Selected == true)
            {
                intmillsecs = 5000;
            }
            else if (rblspeed.Items[1].Selected == true)
            {
                intmillsecs = 2500;
            }

            if (rblspeed.Items[2].Selected == true)
            {
                intmillsecs = 1000;
            }

            //ADDED BY SATISH TO LOAD IFRAME COMPLETELY BEFORE DISPLAYING
            sb.Append("var ifr = jQuery('<iframe/>', {");
            sb.Append("id: 'IframeLaunch',");
            sb.Append("src: '" + pollLaunchUrl + "',");
            sb.Append("style: 'width:" + pollInfo.Tables[1].Rows[0]["iframewidth"].ToString() + "px;height:" + pollInfo.Tables[1].Rows[0]["iframeheight"].ToString() + "px;-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px;border-style:none;',");
            sb.Append("load: function () {");
            sb.Append("cookieapid = getCookie('apid');cookiebspid = getCookie('bspid');if (cookieapid != apid) { cnttimes = 0; document.cookie = 'counttimes=' + (cnttimes).toString();document.cookie='apid=' + cookieapid;}");
            if (rblposition.Items[0].Selected == true)
            {
                sb.Append("if (user == '') {");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', 0);");
                sb.Append("jQuery('.pprev_CM').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");
                sb.Append("else if (cnttimes < " + txtFreqvisit.Text + ")");
                sb.Append("{");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', cnttimes);");
                sb.Append("jQuery('.pprev_CM').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");

                sb.Append("jQuery('#alertclose').click(function () { ");
                sb.Append("jQuery('.pprev_CM').hide();");
                sb.Append("jQuery('.pprev_CM').css('bottom', '-600px');");
                sb.Append("});");
                sb.Append("jQuery('.pprev_CM').show();");
            }
            else if (rblposition.Items[1].Selected == true)
            {
                sb.Append("if (user == '') {");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', 0);");
                sb.Append("jQuery('.pprev_BL').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");
                sb.Append("else if (cnttimes < " + txtFreqvisit.Text + ")");
                sb.Append("{");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', cnttimes);");
                sb.Append("jQuery('.pprev_BL').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");

                sb.Append("jQuery('#alertclose').click(function () { jQuery('.pprev_BL').hide(); jQuery('.pprev_BL').css('bottom', '-600px');  });");
                sb.Append("jQuery('.pprev_BL').show();");
            }
            else if (rblposition.Items[2].Selected == true)
            {
                sb.Append("if (user == '') {");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', 0);");
                sb.Append("jQuery('.pprev_BR').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");
                sb.Append("else if (cnttimes < " + txtFreqvisit.Text + ")");
                sb.Append("{");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', cnttimes);");
                sb.Append("jQuery('.pprev_BR').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");

                sb.Append("jQuery('#alertclose').click(function () { jQuery('.pprev_BR').hide(); jQuery('.pprev_BR').css('bottom', '-600px');});");
                sb.Append("jQuery('.pprev_BR').show();");
            }
            else if (rblposition.Items[3].Selected == true)
            {
                sb.Append("if (user == '') {");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', 0);");
                sb.Append(" jQuery('.pprev_LC').animate({left:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");
                sb.Append("else if (cnttimes < " + txtFreqvisit.Text + ")");
                sb.Append("{");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', cnttimes);");
                sb.Append(" jQuery('.pprev_LC').animate({left:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");

                sb.Append("jQuery('#alertclose').click(function () { jQuery('.pprev_LC').hide(); jQuery('.pprev_LC').css('left', '- 42%');});");
                sb.Append("jQuery('.pprev_LC').show();");
            }
            else if (rblposition.Items[4].Selected == true)
            {
                sb.Append("if (user == '') {");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', 0);");
                sb.Append("jQuery('.pprev_RC').animate({right:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");
                sb.Append("else if (cnttimes < " + txtFreqvisit.Text + ")");
                sb.Append("{");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', cnttimes);");
                sb.Append("jQuery('.pprev_RC').animate({right:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");

                sb.Append("jQuery('#alertclose').click(function () { jQuery('.pprev_RC').hide();  jQuery('.pprev_RC').css('right', '-42%'); });");
                sb.Append("jQuery('.pprev_RC').show();");
            }

            sb.Append("}");
            sb.Append("});");
            sb.Append("jQuery('#timedrpact1').append(ifr);");

            sb.Append("}");
            sb.Append("}");
            sb.Append("});");
            sb.Append("}");
            sb.Append("});");


            sb.Append("function setCookie(cname, cvalue, counttimes, exdays) {");
            sb.Append("var expires = parseInt(exdays) + 1;");
            sb.Append("document.cookie = cname + '=' + cvalue;");
            sb.Append("document.cookie = counttimes + '=' + expires;");
            sb.Append("}");

            sb.Append("function getCookie(cname) {");
            sb.Append("var name = cname + '=';");
            sb.Append("var ca1 = document.cookie.split(';');");
            sb.Append("for (var i = 0; i < ca1.length; i++) {");
            sb.Append("var c1 = ca1[i];");
            sb.Append("while (c1.charAt(0) == ' ') c1 = c1.substring(1);");
            sb.Append("if (c1.indexOf(name) == 0) return c1.substring(name.length, c1.length);");
            sb.Append("}");
            sb.Append("return '';");
            sb.Append("}");

            string strsliderjs = ConfigurationManager.AppSettings["sliderjsfilepath"].ToString();
            string file_name = strsliderjs + "insightoslider" + pollId + ".js";
            System.IO.StreamWriter objwriter;
            objwriter = new System.IO.StreamWriter(file_name);
            objwriter.Write(sb);
            objwriter.Close();

            if ((Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()) < pollId) && (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()) > 0) && (pollInfo.Tables[0].Rows[0]["polltype"].ToString() == "slider"))
            {

                DataSet parentPollInfo = Pc.getPollInfo(Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()));
                if (pollInfo.Tables[0].Rows[0]["polltype"].ToString() == parentPollInfo.Tables[0].Rows[0]["polltype"].ToString())
                {
                    strsliderjs = ConfigurationManager.AppSettings["sliderjsfilepath"].ToString();
                    file_name = strsliderjs + "insightoslider" + pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() + ".js";
                    objwriter = new System.IO.StreamWriter(file_name);
                    objwriter.Write(sb);
                    objwriter.Close();
                }
            }

            txtIframeSlider.Text = "<script src='" + rootURL + "Polls/WebScripts/insightocheckJS.min.js' type='text/javascript'></script><link href='" + rootURL + "Polls/css/SliderStyleSheet.css' rel='stylesheet' type='text/css' /><script src='" + rootURL + "Polls/WebScripts/insightoslider" + pollId + ".js" + "' type='text/javascript'></script><script src='" + rootURL + "/Polls/WebScripts/jquery.easing.min.js' type='text/javascript'></script>";

            HiddenField2.Value = pollInfo.Tables[0].Rows[0]["status"].ToString();
            HiddenField3.Value = "launchframe";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "LaunchAfter" + DateTime.Now.Millisecond.ToString(), "<script>$(window).load(function() { $('#MainContent_launchafter').show(); $('#launchoptions').show(); $('#hrefweb').addClass('active'); } );</script>");
            //launchafter.Style.Add("display", "block");
            launch.Style.Add("display", "none");
            create.Style.Add("display", "none");
            design.Style.Add("display", "none");
        }
        else
        {
            Page.RegisterStartupScript("Upgrade", @"<script>Activationalert(); </script>");
        }

    }

    protected void tbCountries_TextChanged(object sender, EventArgs e)
    {
        ArrayList Arraycities = new ArrayList();

        DataSet dscountries = Pc.getPollCities(tbCountries.Text);

        for (int i = 0; i < dscountries.Tables[0].Rows.Count; i++)
        {
            Arraycities.Add(dscountries.Tables[0].Rows[i][2].ToString());
        }


        HiddenField1.Value = ArrayListToString(ref Arraycities);
    }

    [WebMethod]
    public static string getCities(string countryname)
    {
        ArrayList Arraycities = new ArrayList();
        PollCreation pccity = new PollCreation();

        DataSet dscountries;
        int intCount;
        string strFinal = "";


        try
        {
            dscountries = pccity.getPollCities(countryname);
            for (int i = 0; i < dscountries.Tables[0].Rows.Count; i++)
            {
                //Arraycities.Add(dscountries.Tables[0].Rows[i][1].ToString());
                strFinal += dscountries.Tables[0].Rows[i][2].ToString() + ",";
            }

            //Arraycities.
            //return Arraycities;

            //  HiddenField2.Value = ArrayListToString(ref Arraycities);
            //for (intCount = 0; intCount <= Arraycities.Count - 1; intCount++)
            //for (intCount = 0; intCount <= 3000; intCount++)
            //{
            //    if (intCount > 0)
            //    {
            //        strFinal += ",";
            //    }

            //    strFinal += Arraycities[intCount].ToString();
            //}

        }
        catch (Exception e1)
        {
            strFinal = e1.ToString();
        }

        return strFinal;

    }

    public static int setFrameHeight()
    {
        int frameheight = 0;
        try
        {

        }
        catch (Exception)
        {

            throw;
        }
        return frameheight;
    }

    protected void setPollCode(DataSet thisPollInfo, string thisPollId)
    {
        string rootURL = ConfigurationSettings.AppSettings["RootURL"].ToString();
        string pollweblink = "";
        string embedcode = "";
        string slidercode = "";

        slidercode = "<script src='" + rootURL + "Polls/WebScripts/insightocheckJS.min.js' type='text/javascript'></script><link href='" + rootURL + "Polls/css/SliderStyleSheet.css' rel='stylesheet' type='text/css' /><script src='" + rootURL + "Polls/WebScripts/insightoslider" + thisPollId + ".js" + "' type='text/javascript'></script><script src='" + rootURL + "/Polls/WebScripts/jquery.easing.min.js' type='text/javascript'></script>";
        embedcode = thisPollInfo.Tables[0].Rows[0][7].ToString();

        pollweblink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "PollId=" + pollId + "&pollsource=weblink");
        //pollweblink = EncryptHelper.EncryptQuerystring("/polls/PollWebLink.aspx", "PollId=" + pollId + "&pollsource=weblink");
        pollweblink = rootURL + pollweblink;
        IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
        string shortened = "";
        try
        {
            shortened = s.Shorten(pollweblink);
            if (shortened != null)
            {
                pollweblink = shortened;
            }
        }
        catch (Exception e)
        {
            //pollweblink = e.ToString();
        }

        if (thisPollInfo.Tables[0].Rows[0]["status"].ToString() == "Active")
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "LaunchAfter" + DateTime.Now.Millisecond.ToString(), "<script>$(window).load(function() { $('#MainContent_launchafter').show(); $('#launchoptions').show();  $('#hrefweb').addClass('active'); } );</script>");
            //launchafter.Style.Add("display", "block");
            //launchafter.Style.Add("display", "none");
            launch.Style.Add("display", "none");
            design.Style.Add("display", "none");
            create.Style.Add("display", "none");
            txtIframeSlider.Text = slidercode;
            txtIframe.Text = embedcode;
            txtWebLink.Text = pollweblink;
            //pollcode.Style.Add("display", "inline");
            ////lnkPollWebLinkCopy.Style.Add("display", "inline");
            ////lblpollweblink.Visible = true;
            ////txtPollWebLink.Visible = true;
            //lblpollweblink.Text = "The web link for your poll is :";
            //txtPollWebLink.Text = pollweblink;
        }
        else
        {
            //pollcode.Style.Add("display", "none");
            //lnkPollWebLinkCopy.Style.Add("display", "none");
            //lblpollweblink.Visible = false;
            //txtPollWebLink.Visible = false;
        }
    }

    protected string setPollWebLink(DataSet thisPollInfo, string thisPollId)
    {

        string rootURL = ConfigurationSettings.AppSettings["RootURL"].ToString();
        string pollweblink = "";
        try
        {

            if (thisPollInfo.Tables[0].Rows[0]["status"].ToString() == "Active")
            {
                pollweblink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "PollId=" + pollId + "&pollsource=weblink");
                pollweblink = rootURL + pollweblink;
                IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
                string shortened = "";
                try
                {
                    shortened = s.Shorten(pollweblink);
                    if (shortened != null)
                    {
                        pollweblink = shortened;
                    }
                }
                catch (Exception e)
                {
                    //pollweblink = e.ToString();
                }
                //lblpollweblink.Visible = true;
                //txtPollWebLink.Visible = true;
            }
            else
            {
                //lblpollweblink.Visible = false;
                //txtPollWebLink.Visible = false;
            }
        }
        catch (Exception)
        {
        }
        return pollweblink;
    }

    protected void updateParentWindowJavascript(DataSet thisPollInfo, string thisPollId)
    {
    }

    protected void updatePollNameParentWindowJavascript(DataSet thisPollInfo, string thisPollId)
    {
        string thisquestion = "";
        string thispollname = "";
        try
        {

            thispollname = thisPollInfo.Tables[0].Rows[0]["name"].ToString();
            thispollname = thispollname.Replace("'", "\\'");
            thispollname = thispollname.Replace("\"", "\\\"");

            ClientScript.RegisterClientScriptBlock(this.GetType(), "NewName" + DateTime.Now.Ticks.ToString(), @"<script>parent.window.document.getElementById('newpollname').value='" + thispollname + "';</script>");
        }
        catch (Exception)
        {
        }
    }

    //WHEN YOU CLICK THE ACTIVATE POLL BUTTON ON THE LAUNCH TAB
    protected void btnActivate_Click(object sender, EventArgs e)
    {
        var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
        var userActivationDetails = ServiceFactory<UsersService>.Instance.GetUserActivationFlag(userSessionDetails.UserId);

        if (userActivationDetails.ACTIVATION_FLAG == 1)
        {
            launch.Visible = false;
            launchafter.Style.Add("display", "none");

            if (ckbVotes.Checked == true)
            {
                votecount = 1;
            }
            else
            {
                votecount = 0;
            }
            if (ckbGoogle.Checked == true)
            {
                googletext = "google";
            }
            if (ckbTwitter.Checked == true)
            {
                twittertext = "twitter";
            }
            if (ckbFb.Checked == true)
            {
                fbtext = "fb";
            }
            pollInfo = Pc.getPollInfo(pollId);
            if (pollInfo.Tables[0].Rows[0][16].ToString() == null || pollInfo.Tables[0].Rows[0][16].ToString() == "")
            {
                if (txtQuestion.Text == "")
                {
                    launcharrow.Style.Add("display", "none");
                    launchafter.Style.Add("display", "none");
                    launch.Style.Add("display", "none");
                    launchafter.Style.Add("display", "none");
                    create.Style.Add("display", "block");
                    createarrow.Style.Add("display", "block");
                    return;
                }
                if (txtWidthPoll.Text == "")
                {
                    launcharrow.Style.Add("display", "none");
                    launchafter.Style.Add("display", "none");
                    launch.Style.Add("display", "none");
                    launchafter.Style.Add("display", "none");
                    create.Style.Add("display", "block");
                    createarrow.Style.Add("display", "block");
                    return;
                }
                if (txtHeightPoll.Text == "")
                {
                    launcharrow.Style.Add("display", "none");
                    launchafter.Style.Add("display", "none");
                    launch.Style.Add("display", "none");
                    launchafter.Style.Add("display", "none");
                    create.Style.Add("display", "block");
                    createarrow.Style.Add("display", "block");
                    return;
                }
            }

            if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
            {
                if (txtVideo.Text != "")
                {
                    txtVideo.Attributes.Add("Style", "border-color:#CDCDCD");
                    videopreviewcreate.Style.Add("display", "block");
                    List<string> iframe = new List<string>();
                    string pattern = @"<(iframe)\b[^>]*>";
                    string style = @"width=""(.*?)""";
                    string styleheight = @"height=""(.*?)""";
                    string iframewidth = txtWidthPoll.Text;

                    string content = txtVideo.Text;
                    Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                    MatchCollection matches = rgx.Matches(content);
                    if (matches.Count > 0)
                    {
                        for (int i = 0, l = matches.Count; i < l; i++)
                        {
                            iframe.Add(matches[i].Value);
                            if (i > 0)
                                content = content.Replace(iframe[i], string.Empty);
                        }
                    }
                    if (iframe.Count > 0)
                    {
                        string value = string.Empty;
                        string valueheight = string.Empty;
                        Regex styleRgx = new Regex(style, RegexOptions.IgnoreCase);
                        Regex styleRgxHeight = new Regex(styleheight, RegexOptions.IgnoreCase);
                        MatchCollection styleMatches = styleRgx.Matches(iframe[0]);
                        MatchCollection styleMatchesHeight = styleRgxHeight.Matches(iframe[0]);
                        string width = "";
                        if (styleMatches.Count > 0)
                        {
                            value = styleMatches[0].Value;
                            int widthif = Convert.ToInt32(Regex.Replace(value, @"\D", ""));
                            width = "width=" + "\"" + 850 + "\"";
                            content = content.Replace(iframe[0], iframe[0].Replace(value, width));

                        }
                        if (styleMatchesHeight.Count > 0)
                        {
                            int widthnew = Convert.ToInt32(Regex.Replace(width, @"\D", ""));
                            valueheight = styleMatchesHeight[0].Value;
                            int heightif = Convert.ToInt32(Regex.Replace(valueheight, @"\D", ""));
                            var ratio = (double)Convert.ToDouble(widthnew) / Convert.ToDouble(16);
                            var newHeight = (int)(ratio * 9);
                            string height = "height=" + "\"" + 480 + "\"";
                            content = content.Replace(valueheight, height);
                        }
                    }
                    videopreviewcreate.InnerHtml = content;
                    videoText = content;
                    txtVideo.Text = videoText;
                }
                else
                {
                    create.Style.Add("display", "block");
                    design.Style.Add("display", "none");
                    createarrow.Style.Add("display", "block");
                    designarrow.Style.Add("display", "none");
                    launchafter.Style.Add("display", "none");
                    launch.Style.Add("display", "none");
                    launcharrow.Style.Add("display", "none");
                    txtVideo.Attributes.Add("Style", "border-color:red");
                    return;
                }
            }
            if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
            {
                imageText = pollInfo.Tables[0].Rows[0]["imagelink"].ToString();
                hdnImageLink.Value = imageText;
                hdnImageName.Value = imageText;
            }
            if (radMultipleAnswer.Checked == true)
            {
                questionType = 2;
            }
            if (chkComments.Checked == true)
            {
                comments = 1;
            }
            else
            {
                comments = 0;
            }
            if (ckbSingleVote.Checked)
            {
                uniqueRespondent = 1;
            }
            else
            {
                uniqueRespondent = 0;
            }

            if (chkEmail.Checked)
            {
                emailrequired = 1;
            }
            if (chkPhone.Checked)
            {
                phonerequired = 1;
            }
            if (ckbSubmit.Checked)
            {
                showsubmitbutton = 1;
            }
            if (chkSameAnswer.Checked)
            {
                issamethankyoumessageused = 1;
            }
            else
            {
                issamethankyoumessageused = 0;
            }

            //PollId, Question, VideoCode, Imagelink, Questiontype, Commentreq, uniqueRespondent, votebuttontext, invitationmessage, isemailrequired, isphonerequired, redirecturl, showsubmitbutton, submitbuttontext
            Pc.InsertintoPollInfo(pollId, txtQuestion.Text, videoText, imageText, questionType, comments, uniqueRespondent, txtVoteButtonLabel.Text, txtInvitationMessage.Text, emailrequired, phonerequired, txtRedirectURL.Text, showsubmitbutton, txtSubmitButtonLabel.Text, issamethankyoumessageused);
            
            questionInfo = Pc.GetQuestionInfo(pollId);
            if (questionInfo != null && questionInfo.Rows.Count > 0)
            {
                Pc.UpdateQuestion(pollId, txtQuestion.Text);
            }
            else
            {
                Pc.InsertintoQuestion(pollId, txtQuestion.Text);
            }
            tbAnswerOption = Pc.getAnswerOptions(pollId);
            questionbold = hdnQuestionBold.Value;
            questionitalic = hdnQuestionItalic.Value;
            questionul = hdnQuestionUL.Value;
            if (hdnQuestionAlign.Value != "")
            {
                questionalign = hdnQuestionAlign.Value;
            }
            else
            {
                questionalign = "left";
            }
            questionfsize = hdnQuestionFSize.Value;
            answerbold = hdnAnswerBold.Value;
            answeritalic = hdnAnswerItalic.Value;
            answerul = hdnAnswerUL.Value;
            answerfsize = hdnAnswerFSize.Value;
            if (hdnAnswerAlign.Value != "")
            {
                answeralign = hdnAnswerAlign.Value;
            }
            else
            {
                answeralign = "left";
            }
            votebold = hdnVoteBold.Value;
            voteitalic = hdnVoteItalic.Value;
            voteul = hdnVoteUL.Value;
            votefsize = hdnVoteFSize.Value;
            if (hdnVoteAlign.Value != "")
            {
                votealign = hdnVoteAlign.Value;
            }
            else
            {
                votealign = "center";
            }
            if (hdnMsgAlign.Value != "")
            {
                msgalign = hdnMsgAlign.Value;
            }
            else
            {
                msgalign = "left";
            }
            msgbold = hdnMsgBold.Value;
            msgfsize = hdnMsgFSize.Value;
            msgitalic = hdnMsgItalic.Value;
            msgul = hdnMsgUL.Value;
            if (txtPollBgColor.Value != "")
            {
                pollbgcolor = txtPollBgColor.Value;
            }
            else
            {
                pollbgcolor = "ffffff";
            }
            if (txtQuestionBgColor.Value != "")
            {
                questionbgcolor = txtQuestionBgColor.Value;
            }
            else
            {
                questionbgcolor = "E7E7E7";
            }
            if (txtQuestionColor.Value != "")
            {
                questioncolor = txtQuestionColor.Value;
            }
            else
            {
                questioncolor = "000000";
            }
            if (txtVoteColor.Value != "")
            {
                votecolor = txtVoteColor.Value;
            }
            else
            {
                votecolor = "ffffff";
            }
            if (txtVoteBgColor.Value != "")
            {
                votebgcolor = txtVoteBgColor.Value;
            }
            else
            {
                votebgcolor = "6C89E1";
            }
            if (txtAnswerColor.Value != "")
            {
                answercolor = txtAnswerColor.Value;
            }
            else
            {
                answercolor = "000000";
            }
            if (txtMessageBgColor.Text != "")
            {
                msgbgcolor = txtMessageBgColor.Text;
            }
            else
            {
                msgbgcolor = "cccccc";
            }
            if (txtMessageColor.Text != "")
            {
                msgcolor = txtMessageColor.Text;
            }
            else
            {
                msgcolor = "000000";
            }
            if (ckbInsightoBrand.Checked == true)
            {
                insightoLink = "no";
            }
            else
            {
                insightoLink = "yes";
            }
            if (radLeadGen.Checked == true)
            {
                resultaccess = "private";
            }
            else
            {
                resultaccess = "public";
            }
            dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
            if (ckbAddSponsor.Checked == true)
            {
                if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "")
                {
                    logoFileName = dtSettingsInfo.Rows[0]["sponsorlogo"].ToString();
                    lblLogoName.Visible = true;
                    lblLogoName.Value = logoFileName;
                    hdnLogoLink.Value = "exists";
                    txtSponsorText.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                }
                else
                {
                    lblDesignError.Text = "Please upload logo image";
                    lblDesignError.Visible = true;
                    createarrow.Style.Add("display", "none");
                    create.Style.Add("display", "none");
                    design.Style.Add("display", "block");
                    designarrow.Style.Add("display", "block");
                    launcharrow.Style.Add("display", "none");
                    launchafter.Style.Add("display", "none");
                    launch.Style.Add("display", "none");
                    SponsorBox.Style.Add("display", "block");
                    ckbAddSponsor.Checked = true;
                    sponsorpreviw.Style.Add("display", "block");
                    sponsorResults.Style.Add("display", "block");
                    sponsorLogo.Attributes.Add("Style", "color:Red");
                    sponsorLogo.Focus();
                    divcreatebutton.Style.Add("background-color", "#FFF");
                    divdesignbutton.Style.Add("background-color", bgcolour);
                    CreateNew.ForeColor = Color.Red;
                    DesignNew.ForeColor = Color.White;
                    LaunchNew.ForeColor = Color.Red;
                    divlaunchbutton.Style.Add("background-color", "#FFF");
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sponsorDiv", "<script>$(document).ready(function(){Materialize.toast('Please add a sponsor image',4000);});</script>");
                    return;
                }
            }
            else
            {
                txtSponsorLink.Text = "";
                txtSponsorText.Text = "";
            }
            if (dtSettingsInfo != null && dtSettingsInfo.Rows.Count > 0)
            {
                mode = "edit";
            }
            else
            {
                mode = "add";
            }

            chartwidth = 0;
            chartheight = 0;
            Int32.TryParse(hdnChartWidth.Value, out chartwidth);
            Int32.TryParse(hdnChartHeight.Value, out chartheight);

            if ((ckbAutoAdjust.Checked == true) && (ckbAutoAdjustDesign.Checked == true))
            {
                autoadjustdimensions = 1;
            }
            else
            {
                autoadjustdimensions = 0;
            }

            if (mode == "add")
            {
                Pc.InsertintoPollSettings(pollId, pollbgcolor, ddlQuestionFont.SelectedItem.Value, questionbgcolor, questioncolor, questionitalic, questionbold, questionul, questionalign, questionfsize, ddlAnswerFont.SelectedItem.Value, answercolor, answerbold, answerul, answeritalic, answeralign, answerfsize, votebgcolor, ddlVoteFont.SelectedItem.Text, votefsize, voteitalic, voteul, votecolor, votebold, votealign, logoFileName, txtSponsorText.Text, txtSponsorLink.Text, msgbgcolor, msgcolor, ddlMsgFont.SelectedItem.Value, msgitalic, msgul, msgalign, msgfsize, msgbold, googletext, twittertext, fbtext, votecount, txtSharePoll.Text, txtVoteButtonLabel.Text, chartwidth, chartheight, "#FFFFFF");
            }
            else
            {
                Pc.UpdatePollSettings(pollId, pollbgcolor, ddlQuestionFont.SelectedItem.Value, questionbgcolor, questioncolor, questionitalic, questionbold, questionul, questionalign, questionfsize, ddlAnswerFont.SelectedItem.Value, answercolor, answerbold, answerul, answeritalic, answeralign, answerfsize, votebgcolor, ddlVoteFont.SelectedItem.Text, votefsize, voteitalic, voteul, votecolor, votebold, votealign, logoFileName, txtSponsorText.Text, txtSponsorLink.Text, msgbgcolor, msgcolor, ddlMsgFont.SelectedItem.Value, msgitalic, msgul, msgalign, msgfsize, msgbold, googletext, twittertext, fbtext, votecount, txtSharePoll.Text, txtVoteButtonLabel.Text, chartwidth, chartheight, autoadjustdimensions, "#FFFFFF");
            }
            Pc.UpdatePollinfo(pollId, resultaccess, txtResultText.Text, insightoLink);

            if (ckbSingleVote.Checked == true)
            {
                uniqueRespondent = 1;
            }
            else
            {
                uniqueRespondent = 0;
            }

            if (pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() != "")
            {
                //pollLink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()));
                pollLink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "source=embed&PollId=" + Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()));
            }
            else
            {
                //pollLink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId);
                pollLink = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "source=embed&PollId=" + pollId);
            }
            pollLaunchUrl = rootURL + pollLink;
            lblWidth.Text = lblWidth.Text;
            IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
            string shortened = "";
            try
            {
                shortened = s.Shorten(pollLaunchUrl);
                if (shortened != null)
                {
                    pollLaunchUrl = shortened;
                }

            }
            catch (Exception)
            {
            }
            pollLink = pollLaunchUrl;

            string EmbedUrl = "";
            if (pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() != "")
            {
                EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()) + "&Embed=Emb&pollsource=embed");
                //EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "source=embed&PollId=" + Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()) + "&Embed=Emb&pollsource=embed");
            }
            else
            {
                EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId + "&Embed=Emb&pollsource=embed");
                //EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollWebLinkURL(), "source=embed&PollId=" + pollId + "&Embed=Emb&pollsource=embed");
            }
            pollLaunchUrl = rootURL + EmbedUrl;

            lblWidth.Text = hdnRecWidth.Value;
            lblHeight.Text = hdnRecHeightne.Value;

            txtIframe.Text = "<iframe id='IframeLaunch' height='" + txtHeightIframe.Text + "' width='" + txtWidthIframe.Text + "'  scrolling='auto' src='" + pollLaunchUrl + "' frameborder='0'   style='-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px; -moz-box-shadow: 4px 4px 14px #000; -webkit-box-shadow: 4px 4px 14px #000; box-shadow: 4px 4px 14px #000;'  ></iframe>";
            txtWebLink.Text = setPollWebLink(pollInfo, pollId.ToString());
            Pc.SaveLaunchInfo(pollId, uniqueRespondent, pollLink, txtIframe.Text, 1);

            setPollCode(pollInfo, pollId.ToString());

            if (pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() != "")
            {
                Pc.UpdateSuperpollStatus(Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()));
            }

            Pc.UpdateStatus(pollId, "Active");
            if (hdnParentPollID.Value != hdnPollID.Value && hdnParentPollID.Value != "")
            {
                int parentpollid = 0;
                Int32.TryParse(hdnParentPollID.Value, out parentpollid);
                Pc.UpdateStatus(parentpollid, "Closed");
            }

            BindDesignStyles();
            pollInfo = Pc.getPollInfo(pollId);

            string lbliframe = pollInfo.Tables[0].Rows[0][7].ToString();


            StringBuilder sb = new StringBuilder(30000);

            DataSet dscountrycode = Pc.getPollCountries();

            string countries = tbCountries.Text;
            var arrCountries = countries.Split(new char[] { ',' });
            countries = "";
            for (int x = 0; x < arrCountries.Length; x++)
            {
                countries += "'" + arrCountries[x] + "',";
            }

            DataRow[] drcc = dscountrycode.Tables[0].Select("CountryName IN (" + countries.Substring(0, countries.Length - 1) + ")");

            sb.Append("jQuery(document).ready(function () { ");

            countries = "";
            for (int r = 0; r < drcc.Length; r++)
            {
                countries += drcc[r][3].ToString().Trim() + ",";
            }
            //sb.Append("var countryname='" + drcc[0][3].ToString().Trim() + "';");
            sb.Append("var countryname='" + countries + "';");
            sb.Append("var cityname='" + tbcities.Text + "';");

            sb.Append("var clientcountryname='';");
            sb.Append("var clientcityname='';");
            sb.Append("var user = getCookie('insightoslidername');");
            sb.Append("var cnttimes = getCookie('counttimes');");
            sb.Append("var apid = '" + pollId + "';");
            sb.Append("var bspid = '" + pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() + "';");
            sb.Append("var cookieapid = getCookie('apid');");
            sb.Append("var cookiebspid = getCookie('bspid');");
            sb.Append("if(cookieapid == ''){document.cookie = 'apid=' + apid;cookieapid = apid;cnttimes=0;}");
            sb.Append("else if(cookieapid != apid){document.cookie = 'apid=' + apid; cookieapid = apid;cnttimes=0;}");

            sb.Append("if(cookiebspid == ''){document.cookie = 'bspid=' + bspid; cookiebspid = bspid; cnttimes = 0;}");
            sb.Append("else if(cookiebspid != bspid){document.cookie = 'bspid=' + bspid;cookiebspid = bspid;}");

            sb.Append("if (cnttimes < " + txtFreqvisit.Text + "){");
            sb.Append("jQuery.ajax({");
            sb.Append("url: '//freegeoip.net/json/',");
            sb.Append("type: 'POST',");
            sb.Append("dataType: 'jsonp',");
            sb.Append("success: function (location) {");
            sb.Append("clientcountryname = location.country_code;");
            sb.Append("clientcityname = location.city;");
            sb.Append("var valcnt = countryname.indexOf(clientcountryname);");
            sb.Append("var valcity = cityname.indexOf(clientcityname);");

            //  sb.Append("if ((countryname == clientcountryname) && (cityname == clientcityname)) {");

            //sb.Append("if (((valcnt >= 0) && (valcity >= 0)) || (valcnt == -1)) {");
            //CONDITIONS TO CHECK INCLUDE IF COUNTRY+CITY ARE PROVIDED OR COUNTRY ALONE IS PROVIDED OR NEITHER IS PROVIDED
            sb.Append("if (((valcnt >= 0) && (valcity >= 0) && (countryname != '') && (cityname != '')) || ((valcnt >= 0) && (countryname != '') && (cityname == '')) || ((countryname == '') && (cityname == ''))) {");
            sb.Append("var output = document.createElement('div');");


            if (rblposition.Items[0].Selected == true)
            {
                sb.Append("output.setAttribute('id', 'pSlider');");
                sb.Append("output.setAttribute('class', 'pprev_style pprev_CM');");
            }
            else if (rblposition.Items[1].Selected == true)
            {
                sb.Append("output.setAttribute('id', 'pSliderBL');");
                sb.Append("output.setAttribute('class', 'pprev_style pprev_BL');");
            }
            else if (rblposition.Items[2].Selected == true)
            {
                sb.Append("output.setAttribute('id', 'pSliderBR');");
                sb.Append("output.setAttribute('class', 'pprev_style pprev_BR');");
            }
            else if (rblposition.Items[3].Selected == true)
            {
                sb.Append("output.setAttribute('id', 'pSliderCL');");
                sb.Append("output.setAttribute('class', 'pprev_style pprev_LC');");
            }
            else if (rblposition.Items[4].Selected == true)
            {
                sb.Append("output.setAttribute('id', 'pSliderCR');");
                sb.Append("output.setAttribute('class', 'pprev_style pprev_RC');");
            }
            sb.Append("var ele = document.createElement('div');");
            sb.Append("ele.setAttribute('id', 'timedrpact1');");

            if (rblposition.Items[0].Selected == true)
            {
                sb.Append("ele.setAttribute('class', 'pprev_style pprev_CM');");
            }
            else if (rblposition.Items[1].Selected == true)
            {
                sb.Append("ele.setAttribute('class', 'pprev_style pprev_BL');");
            }
            else if (rblposition.Items[2].Selected == true)
            {
                sb.Append("ele.setAttribute('class', 'pprev_style pprev_BR');");
            }
            else if (rblposition.Items[3].Selected == true)
            {
                sb.Append("ele.setAttribute('class', 'pprev_style pprev_LC');");
            }
            else if (rblposition.Items[4].Selected == true)
            {
                sb.Append("ele.setAttribute('class', 'pprev_style pprev_RC');");
            }

            //COMMENTED BY SATISH AS THE IFRAME WILL BE ADDED VIA IFR VARIABLE BELOW
            //sb.Append("ele.innerHTML = " + "\"" + lbliframe + "\"" + ";");

            sb.Append("var alclose = document.createElement('div');");
            sb.Append("alclose.setAttribute('id', 'alertclose');");
            sb.Append("alclose.setAttribute('class', 'closeFlyoutPromo');");
            //  sb.Append("alclose.innerHTML = 'X';");
            sb.Append("ele.appendChild(alclose);");

            sb.Append("output.appendChild(ele);");
            sb.Append("jQuery('body').append(output);");

            int intmillsecs = 0;
            //= Convert.ToInt32(txtSecs.Text) * 1000;

            if (rblspeed.Items[0].Selected == true)
            {
                intmillsecs = 5000;
            }
            else if (rblspeed.Items[1].Selected == true)
            {
                intmillsecs = 2500;
            }

            if (rblspeed.Items[2].Selected == true)
            {
                intmillsecs = 1000;
            }

            //ADDED BY SATISH TO LOAD IFRAME COMPLETELY BEFORE DISPLAYING
            sb.Append("var ifr = jQuery('<iframe/>', {");
            sb.Append("id: 'IframeLaunch',");
            sb.Append("src: '" + pollLaunchUrl + "',");
            sb.Append("style: 'width:" + pollInfo.Tables[1].Rows[0]["iframewidth"].ToString() + "px;height:" + pollInfo.Tables[1].Rows[0]["iframeheight"].ToString() + "px;-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px;border-style:none;',");
            sb.Append("load: function () {");
            sb.Append("cookieapid = getCookie('apid');cookiebspid = getCookie('bspid');if (cookieapid != apid) { cnttimes = 0; document.cookie = 'counttimes=' + (cnttimes).toString();document.cookie='apid=' + cookieapid;}");
            if (rblposition.Items[0].Selected == true)
            {
                //sb.Append("jQuery('.pprev_CM').show();");

                sb.Append("if (user == '') {");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', 0);");
                sb.Append("jQuery('.pprev_CM').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");
                sb.Append("else if (cnttimes < " + txtFreqvisit.Text + ")");
                sb.Append("{");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', cnttimes);");
                sb.Append("jQuery('.pprev_CM').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");

                sb.Append("jQuery('#alertclose').click(function () { ");
                sb.Append("jQuery('.pprev_CM').hide();");
                sb.Append("jQuery('.pprev_CM').css('bottom', '-600px');");
                sb.Append("});");
                sb.Append("jQuery('.pprev_CM').show();");
            }
            else if (rblposition.Items[1].Selected == true)
            {
                //sb.Append("jQuery('.pprev_BL').show();");

                //sb.Append("var user = getCookie('insightoslidername');");
                //sb.Append("var cnttimes = getCookie('counttimes');");
                sb.Append("if (user == '') {");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', 0);");
                sb.Append("jQuery('.pprev_BL').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");
                sb.Append("else if (cnttimes < " + txtFreqvisit.Text + ")");
                sb.Append("{");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', cnttimes);");
                sb.Append("jQuery('.pprev_BL').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");

                sb.Append("jQuery('#alertclose').click(function () { jQuery('.pprev_BL').hide(); jQuery('.pprev_BL').css('bottom', '-600px');  });");
                sb.Append("jQuery('.pprev_BL').show();");
            }
            else if (rblposition.Items[2].Selected == true)
            {
                //sb.Append("jQuery('.pprev_BR').show();");

                //sb.Append("var user = getCookie('insightoslidername');");
                //sb.Append("var cnttimes = getCookie('counttimes');");
                sb.Append("if (user == '') {");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', 0);");
                sb.Append("jQuery('.pprev_BR').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");
                sb.Append("else if (cnttimes < " + txtFreqvisit.Text + ")");
                sb.Append("{");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', cnttimes);");
                sb.Append("jQuery('.pprev_BR').animate({bottom:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");

                sb.Append("jQuery('#alertclose').click(function () { jQuery('.pprev_BR').hide(); jQuery('.pprev_BR').css('bottom', '-600px');});");
                sb.Append("jQuery('.pprev_BR').show();");
            }
            else if (rblposition.Items[3].Selected == true)
            {
                //sb.Append("jQuery('.pprev_LC').show();");

                //sb.Append("var user = getCookie('insightoslidername');");
                //sb.Append("var cnttimes = getCookie('counttimes');");
                sb.Append("if (user == '') {");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', 0);");
                sb.Append(" jQuery('.pprev_LC').animate({left:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");
                sb.Append("else if (cnttimes < " + txtFreqvisit.Text + ")");
                sb.Append("{");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', cnttimes);");
                sb.Append(" jQuery('.pprev_LC').animate({left:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");

                sb.Append("jQuery('#alertclose').click(function () { jQuery('.pprev_LC').hide(); jQuery('.pprev_LC').css('left', '- 42%');});");
                sb.Append("jQuery('.pprev_LC').show();");
            }
            else if (rblposition.Items[4].Selected == true)
            {
                //sb.Append("jQuery('.pprev_RC').show();");

                //sb.Append("var user = getCookie('insightoslidername');");
                //sb.Append("var cnttimes = getCookie('counttimes');");
                sb.Append("if (user == '') {");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', 0);");
                sb.Append("jQuery('.pprev_RC').animate({right:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");
                sb.Append("else if (cnttimes < " + txtFreqvisit.Text + ")");
                sb.Append("{");
                sb.Append("setCookie('insightoslidername', 'insightoslidername', 'counttimes', cnttimes);");
                sb.Append("jQuery('.pprev_RC').animate({right:'0px'}," + intmillsecs + ", 'linear', 'easeInOutExpo');");
                sb.Append("}");

                sb.Append("jQuery('#alertclose').click(function () { jQuery('.pprev_RC').hide();  jQuery('.pprev_RC').css('right', '-42%'); });");
                sb.Append("jQuery('.pprev_RC').show();");
            }

            //  sb.Append("jQuery(this).show();");
            sb.Append("}");
            sb.Append("});");
            sb.Append("jQuery('#timedrpact1').append(ifr);");

            sb.Append("}");
            sb.Append("}");
            sb.Append("});");
            sb.Append("}");
            sb.Append("});");


            sb.Append("function setCookie(cname, cvalue, counttimes, exdays) {");
            sb.Append("var expires = parseInt(exdays) + 1;");
            sb.Append("document.cookie = cname + '=' + cvalue;");
            sb.Append("document.cookie = counttimes + '=' + expires;");
            sb.Append("}");

            sb.Append("function getCookie(cname) {");
            sb.Append("var name = cname + '=';");
            sb.Append("var ca1 = document.cookie.split(';');");
            sb.Append("for (var i = 0; i < ca1.length; i++) {");
            sb.Append("var c1 = ca1[i];");
            sb.Append("while (c1.charAt(0) == ' ') c1 = c1.substring(1);");
            sb.Append("if (c1.indexOf(name) == 0) return c1.substring(name.length, c1.length);");
            sb.Append("}");
            sb.Append("return '';");
            sb.Append("}");

            string strsliderjs = ConfigurationManager.AppSettings["sliderjsfilepath"].ToString();
            string file_name = strsliderjs + "insightoslider" + pollId + ".js";
            System.IO.StreamWriter objwriter;
            objwriter = new System.IO.StreamWriter(file_name);
            objwriter.Write(sb);
            objwriter.Close();

            if ((Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()) < pollId) && (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()) > 0) && (pollInfo.Tables[0].Rows[0]["polltype"].ToString() == "slider"))
            {

                DataSet parentPollInfo = Pc.getPollInfo(Convert.ToInt32(pollInfo.Tables[0].Rows[0]["parentpollId"].ToString()));
                if (pollInfo.Tables[0].Rows[0]["polltype"].ToString() == parentPollInfo.Tables[0].Rows[0]["polltype"].ToString())
                {
                    strsliderjs = ConfigurationManager.AppSettings["sliderjsfilepath"].ToString();
                    file_name = strsliderjs + "insightoslider" + pollInfo.Tables[0].Rows[0]["parentpollId"].ToString() + ".js";
                    objwriter = new System.IO.StreamWriter(file_name);
                    objwriter.Write(sb);
                    objwriter.Close();
                }
            }

            txtIframeSlider.Text = "<script src='" + rootURL + "Polls/WebScripts/insightocheckJS.min.js' type='text/javascript'></script><link href='" + rootURL + "Polls/css/SliderStyleSheet.css' rel='stylesheet' type='text/css' /><script src='" + rootURL + "Polls/WebScripts/insightoslider" + pollId + ".js" + "' type='text/javascript'></script><script src='" + rootURL + "/Polls/WebScripts/jquery.easing.min.js' type='text/javascript'></script>";
            txtWebLink.Text = setPollWebLink(pollInfo, pollId.ToString());

            HiddenField2.Value = pollInfo.Tables[0].Rows[0]["status"].ToString();
            HiddenField3.Value = "launchframe";

            bgcolour = "#8ABA3C";
            pollstatusicon = "favorite";
            string thispollname = pollInfo.Tables[0].Rows[0]["name"].ToString();
            lblPollName.Text = "" + thispollname;

            leftsidediv.Style.Add("background", "" + bgcolourstandard + "");
            polltitlediv.Style.Add("background-color", "" + bgcolour + "");
            pollanalyticsbar.Style.Add("background-color", "" + bgcolour + " !important");
            closeicon.Style.Add("background-color", "" + bgcolour + " !important");
            headerbar.Style.Add("background-color", "" + bgcolour + " !important");
            footer.Style.Add("background-color", "#000");
            lblPollName.Style.Add("color", "white");
            pollanalytics.Style.Add("color", "" + bgcolour + "");
            pollanalytics.Style.Add("border-color", "" + bgcolour + "");
            pollanalytics.Style.Add("display", "inline");
            pollanalytics.Style.Add("width", "127px");
            pollanalytics.Attributes.Add("href", EncryptHelper.EncryptQuerystring("PollReports.aspx", "PollId=" + pollId));

            closeicon.Style.Add("color", "" + bgcolour + "");
            closeicon.Style.Add("background-color", "#000");
            closeicon.Style.Add("border-color", "" + bgcolour + "");
            closeicon.Style.Add("display", "inline");
            closeicon.Style.Add("width", "77px");

            create.Style.Add("display", "none");
            design.Style.Add("display", "none");
            launch.Style.Add("display", "none");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "LaunchAfter" + DateTime.Now.Millisecond.ToString(), "<script>$(window).load(function() { $('#MainContent_launchafter').show(); $('#launchoptions').show(); $('#hrefweb').addClass('active'); } );</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Close" + DateTime.Now.Millisecond.ToString(), "<script>$(window).load(function() { $('#MainContent_launchafter').show(); $('#launchoptions').show(); $('#hrefweb').addClass('active'); } );</script>");
            launchafter.Style.Add("background-color", bgcolourstandard);

            divcreatebutton.Style.Add("background-color", "#FFF");
            divdesignbutton.Style.Add("background-color", "#FFF");
            divlaunchbutton.Style.Add("background-color", bgcolourstandard);

            CreateNew.ForeColor = Color.Red;
            DesignNew.ForeColor = Color.Red;
            LaunchNew.ForeColor = Color.Black;
            string launchUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollId.ToString() + "&launch=" + "launch");
            Response.Redirect(launchUrl);
        }
        else
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "UserActivation" + DateTime.Now.Millisecond.ToString(), @"<script>$(document).ready(function(){ActivationAlert();});</script>");
        }

    }

    //WHEN YOU CLICK THE CREATE TAB ON THE TOP
    public void CreateNew_Click(object sender, EventArgs e)
    {
        if (ckbVotes.Checked == true)
        {
            votecount = 1;
        }
        else
        {
            votecount = 0;
        }
        txtQuestion.Attributes.Add("Style", "border-color:#CDCDCD");
        pollInfo = Pc.getPollInfo(pollId);
        if (ckbGoogle.Checked == true)
        {
            googletext = "google";
        }
        if (ckbTwitter.Checked == true)
        {
            twittertext = "twitter";
        }
        if (ckbFb.Checked == true)
        {
            fbtext = "fb";
        }
        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
        {
            if (txtVideo.Text != "")
            {
                videopreviewcreate.Style.Add("display", "block");
                List<string> iframe = new List<string>();
                string pattern = @"<(iframe)\b[^>]*>";
                string style = @"width=""(.*?)""";
                string styleheight = @"height=""(.*?)""";
                string iframewidth = txtWidthPoll.Text;

                string content = txtVideo.Text;
                Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                MatchCollection matches = rgx.Matches(content);
                if (matches.Count > 0)
                {
                    for (int i = 0, l = matches.Count; i < l; i++)
                    {
                        iframe.Add(matches[i].Value);
                        if (i > 0)
                            content = content.Replace(iframe[i], string.Empty);
                    }
                }
                if (iframe.Count > 0)
                {
                    string value = string.Empty;
                    string valueheight = string.Empty;
                    Regex styleRgx = new Regex(style, RegexOptions.IgnoreCase);
                    Regex styleRgxHeight = new Regex(styleheight, RegexOptions.IgnoreCase);
                    MatchCollection styleMatches = styleRgx.Matches(iframe[0]);
                    MatchCollection styleMatchesHeight = styleRgxHeight.Matches(iframe[0]);
                    string width = "";
                    if (styleMatches.Count > 0)
                    {
                        value = styleMatches[0].Value;
                        int widthif = Convert.ToInt32(Regex.Replace(value, @"\D", ""));
                        width = "width=" + "\"" + 850 + "\"";
                        content = content.Replace(iframe[0], iframe[0].Replace(value, width));
                    }
                    if (styleMatchesHeight.Count > 0)
                    {
                        int widthnew = Convert.ToInt32(Regex.Replace(width, @"\D", ""));
                        valueheight = styleMatchesHeight[0].Value;
                        int heightif = Convert.ToInt32(Regex.Replace(valueheight, @"\D", ""));
                        var ratio = (double)Convert.ToDouble(widthnew) / Convert.ToDouble(16);
                        var newHeight = (int)(ratio * 9);
                        string height = "height=" + "\"" + 480 + "\"";
                        content = content.Replace(valueheight, height);
                    }
                }
                videopreviewcreate.InnerHtml = content;
                videoText = content;
                txtVideo.Text = videoText;
            }
        }
        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
        {

            imageText = pollInfo.Tables[0].Rows[0]["imagelink"].ToString();
            hdnImageLink.Value = imageText;
            hdnImageName.Value = imageText;
        }
        if (radMultipleAnswer.Checked == true)
        {
            questionType = 2;
        }
        if (chkComments.Checked == true)
        {
            comments = 1;
        }
        else
        {
            comments = 0;

        }
        if (ckbSingleVote.Checked)
        {
            uniqueRespondent = 1;
        }
        else
        {
            uniqueRespondent = 0;
        }
        if (chkEmail.Checked)
        {
            emailrequired = 1;
        }
        if (chkPhone.Checked)
        {
            phonerequired = 1;
        }
        if (ckbSubmit.Checked)
        {
            showsubmitbutton = 1;
        }
        if (chkSameAnswer.Checked)
        {
            issamethankyoumessageused = 1;
        }
        else
        {
            issamethankyoumessageused = 0;
        }

        //PollId, Question, VideoCode, Imagelink, Questiontype, Commentreq, uniqueRespondent, votebuttontext, invitationmessage, isemailrequired, isphonerequired, redirecturl, showsubmitbutton, submitbuttontext
        Pc.InsertintoPollInfo(pollId, txtQuestion.Text, videoText, imageText, questionType, comments, uniqueRespondent, txtVoteButtonLabel.Text, txtInvitationMessage.Text, emailrequired, phonerequired, txtRedirectURL.Text, showsubmitbutton, txtSubmitButtonLabel.Text, issamethankyoumessageused);
        
        questionbold = hdnQuestionBold.Value;
        questionitalic = hdnQuestionItalic.Value;
        questionul = hdnQuestionUL.Value;
        if (hdnQuestionAlign.Value != "")
        {
            questionalign = hdnQuestionAlign.Value;
        }
        else
        {
            questionalign = "left";
        }
        questionfsize = hdnQuestionFSize.Value;
        answerbold = hdnAnswerBold.Value;
        answeritalic = hdnAnswerItalic.Value;
        answerul = hdnAnswerUL.Value;
        answerfsize = hdnAnswerFSize.Value;
        if (hdnAnswerAlign.Value != "")
        {
            answeralign = hdnAnswerAlign.Value;
        }
        else
        {
            answeralign = "left";
        }
        votebold = hdnVoteBold.Value;
        voteitalic = hdnVoteItalic.Value;
        voteul = hdnVoteUL.Value;
        votefsize = hdnVoteFSize.Value;
        if (hdnVoteAlign.Value != "")
        {
            votealign = hdnVoteAlign.Value;
        }
        else
        {
            votealign = "center";
        }
        if (hdnMsgAlign.Value != "")
        {
            msgalign = hdnMsgAlign.Value;
        }
        else
        {
            msgalign = "left";
        }
        msgbold = hdnMsgBold.Value;
        msgfsize = hdnMsgFSize.Value;
        msgitalic = hdnMsgItalic.Value;
        msgul = hdnMsgUL.Value;
        if (txtPollBgColor.Value != "")
        {
            pollbgcolor = txtPollBgColor.Value;
        }
        else
        {
            pollbgcolor = "ffffff";
        }
        if (txtQuestionBgColor.Value != "")
        {
            questionbgcolor = txtQuestionBgColor.Value;
        }
        else
        {
            questionbgcolor = "E7E7E7";
        }
        if (txtQuestionColor.Value != "")
        {
            questioncolor = txtQuestionColor.Value;
        }
        else
        {
            questioncolor = "000000";
        }
        if (txtVoteColor.Value != "")
        {
            votecolor = txtVoteColor.Value;
        }
        else
        {
            votecolor = "ffffff";
        }
        if (txtVoteBgColor.Value != "")
        {
            votebgcolor = txtVoteBgColor.Value;
        }
        else
        {
            votebgcolor = "6C89E1";
        }
        if (txtAnswerColor.Value != "")
        {
            answercolor = txtAnswerColor.Value;
        }
        else
        {
            answercolor = "000000";
        }
        if (txtMessageBgColor.Text != "")
        {
            msgbgcolor = txtMessageBgColor.Text;
        }
        else
        {
            msgbgcolor = "cccccc";
        }
        if (txtMessageColor.Text != "")
        {
            msgcolor = txtMessageColor.Text;
        }
        else
        {
            msgcolor = "000000";
        }
        if (ckbInsightoBrand.Checked == true)
        {
            insightoLink = "no";
        }
        else
        {
            insightoLink = "yes";
        }
        if (radLeadGen.Checked == true)
        {
            resultaccess = "private";
        }
        else
        {
            resultaccess = "public";
        }
        dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
        if (ckbAddSponsor.Checked == true)
        {
            if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "")
            {
                logoFileName = dtSettingsInfo.Rows[0]["sponsorlogo"].ToString();
                lblLogoName.Visible = true;
                lblLogoName.Value = logoFileName;
                hdnLogoLink.Value = "exists";
                //txtSponsorText.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                if (txtSponsorText.Text == "")
                {
                    txtSponsorText.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                }
            }
            else
            {
                SponsorBox.Style.Add("display", "block");
                ckbAddSponsor.Checked = true;
                sponsorpreviw.Style.Add("display", "block");
                sponsorResults.Style.Add("display", "block");
            }
        }
        else
        {
            txtSponsorLink.Text = "";
            txtSponsorText.Text = "";
        }
        if (dtSettingsInfo != null && dtSettingsInfo.Rows.Count > 0)
        {
            mode = "edit";
        }
        else
        {
            mode = "add";
        }

        chartwidth = 0;
        chartheight = 0;
        Int32.TryParse(hdnChartWidth.Value, out chartwidth);
        Int32.TryParse(hdnChartHeight.Value, out chartheight);

        if ((ckbAutoAdjustDesign.Checked == true) && (ckbAutoAdjust.Checked == true))
        {
            autoadjustdimensions = 1;
        }
        else
        {
            autoadjustdimensions = 0;
        }

        if (mode == "add")
        {
            Pc.InsertintoPollSettings(pollId, pollbgcolor, ddlQuestionFont.SelectedItem.Value, questionbgcolor, questioncolor, questionitalic, questionbold, questionul, questionalign, questionfsize, ddlAnswerFont.SelectedItem.Value, answercolor, answerbold, answerul, answeritalic, answeralign, answerfsize, votebgcolor, ddlVoteFont.SelectedItem.Text, votefsize, voteitalic, voteul, votecolor, votebold, votealign, logoFileName, txtSponsorText.Text, txtSponsorLink.Text, msgbgcolor, msgcolor, ddlMsgFont.SelectedItem.Value, msgitalic, msgul, msgalign, msgfsize, msgbold, googletext, twittertext, fbtext, votecount, txtSharePoll.Text, txtVoteButtonLabel.Text, chartwidth, chartheight, txtWebLinkBgColor.Value);
        }
        else
        {
            Pc.UpdatePollSettings(pollId, pollbgcolor, ddlQuestionFont.SelectedItem.Value, questionbgcolor, questioncolor, questionitalic, questionbold, questionul, questionalign, questionfsize, ddlAnswerFont.SelectedItem.Value, answercolor, answerbold, answerul, answeritalic, answeralign, answerfsize, votebgcolor, ddlVoteFont.SelectedItem.Text, votefsize, voteitalic, voteul, votecolor, votebold, votealign, logoFileName, txtSponsorText.Text, txtSponsorLink.Text, msgbgcolor, msgcolor, ddlMsgFont.SelectedItem.Value, msgitalic, msgul, msgalign, msgfsize, msgbold, googletext, twittertext, fbtext, votecount, txtSharePoll.Text, txtVoteButtonLabel.Text, chartwidth, chartheight, autoadjustdimensions, txtWebLinkBgColor.Value);
        }
        Pc.UpdatePollinfo(pollId, resultaccess, txtResultText.Text, insightoLink);
        createarrow.Style.Add("display", "block");
        create.Style.Add("display", "block");
        design.Style.Add("display", "none");
        designarrow.Style.Add("display", "none");
        launcharrow.Style.Add("display", "none");
        launchafter.Style.Add("display", "none");
        launch.Style.Add("display", "none");
        hdnRecWidth.Value = txtWidthIframe.Text;
        if (hdnRecHeightne.Value == "")
        {
            hdnRecHeightne.Value = txtHeightIframe.Text;
        }
        if (lblHeight.Text == "")
        {
            lblHeight.Text = hdnRecHeightne.Value;
        }
        if (lblWidth.Text == "")
        {
            lblWidth.Text = txtWidthIframe.Text;
        }
        pollInfo = Pc.getPollInfo(pollId);
        if (pollInfo.Tables[0].Rows[0][8].ToString() == "" || pollInfo.Tables[0].Rows[0][8].ToString() == null)
        {
            Pc.SaveIframeInfo(pollId, Convert.ToInt32(txtWidthPoll.Text), Convert.ToInt32(txtHeightPoll.Text), Convert.ToInt32(lblRecThemeWidth.Text), Convert.ToInt32(hdnRecHeightne.Value));
            if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
            {
                Pc.SaveImageSpec(pollId, Convert.ToInt32(hdnImageRecWidth.Value), Convert.ToInt32(hdnImageRecHeight.Value));
            }
        }
        divcreatebutton.Style.Add("background-color", bgcolour);
        CreateNew.ForeColor = Color.White;
        DesignNew.ForeColor = Color.Red;
        LaunchNew.ForeColor = Color.Red;
        divdesignbutton.Style.Add("background-color", "#FFF");
        divlaunchbutton.Style.Add("background-color", "#FFF");
        updateParentWindowJavascript(pollInfo, pollId.ToString());
        //BindWithRepeater(); 
        BindWithRepeaterForThankYouOptions();
        BindDesignStyles();
        ClientScript.RegisterClientScriptBlock(this.GetType(), "EditsSaved4" + DateTime.Now.Millisecond.ToString(), "<script>$(document).ready(function() { Materialize.toast('Your edits have been saved',5000); } );</script>");
        hdnCurrentPage.Value = "";
    }

    protected void ddlQuestionFont_TextChanged(object sender, EventArgs e)
    {
        //ClientScript.RegisterClientScriptBlock(this.GetType(), "QuestionFontChanged" + DateTime.Now.Millisecond.ToString(), "<script>var questionfont = $('#MainContent_ddlQuestionFont').val();$('.questionpreviewcss').css({ 'font-family': questionfont + '' });$('.wrapquestion').css({ 'font-family': questionfont + '' });</script>");
        ClientScript.RegisterClientScriptBlock(this.GetType(), "QuestionFontChanged" + DateTime.Now.Millisecond.ToString(), "<script>var questionfont = $('#MainContent_ddlQuestionFont').val();$('.questionpreviewcss').css({ 'font-family': questionfont + '' });$('.wrapquestion').css({ 'font-family': questionfont + '' });renderChart();</script>");
    }

    protected void fuImage_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        pollInfo = Pc.getPollInfo(Convert.ToInt32(hdnPollID.Value));
        dtSettingsInfo = Pc.getPollSettingsInfo(Convert.ToInt32(hdnPollID.Value));
        //lblimageuploaderror1.Text = "File uploaded";
        string filename = System.IO.Path.GetFileName(e.FileName);
        string fileextension = System.IO.Path.GetExtension(e.FileName);
        if (filename.Length > 50)
        {
            filename = filename.Substring(0, 30);
            filename += fileextension;
        }
        string imagefilelocation = pollImagesFolder + "//" + pollId.ToString();
        if (!Directory.Exists(imagefilelocation))
        {
            Directory.CreateDirectory(imagefilelocation);
        }

        fuImage.SaveAs(imagefilelocation + "//" + filename);
        //fuImage.PersistFile = true;
        //Pc.SaveImageSpec(pollId, Convert.ToInt32(dtSettingsInfo.Rows[0]["imgrecwidth"].ToString()), Convert.ToInt32(dtSettingsInfo.Rows[0]["imgrecmheight"].ToString()));
        if (radMultipleAnswer.Checked == true)
        {
            questionType = 2;
        }
        if (chkComments.Checked == true)
        {
            comments = 1;
        }
        else
        {
            comments = 0;
        }
        if (ckbSingleVote.Checked)
        {
            uniqueRespondent = 1;
        }
        else
        {
            uniqueRespondent = 0;
        }
        if (chkEmail.Checked)
        {
            emailrequired = 1;
        }
        if (chkPhone.Checked)
        {
            phonerequired = 1;
        }
        if (ckbSubmit.Checked)
        {
            showsubmitbutton = 1;
        }
        if (chkSameAnswer.Checked)
        {
            issamethankyoumessageused = 1;
        }
        else
        {
            issamethankyoumessageused = 0;
        }

        //PollId, Question, VideoCode, Imagelink, Questiontype, Commentreq, uniqueRespondent, votebuttontext, invitationmessage, isemailrequired, isphonerequired, redirecturl, showsubmitbutton, submitbuttontext
        Pc.InsertintoPollInfo(pollId, txtQuestion.Text, videoText, filename, questionType, comments, uniqueRespondent, txtVoteButtonLabel.Text, txtInvitationMessage.Text, emailrequired, phonerequired, txtRedirectURL.Text, showsubmitbutton, txtSubmitButtonLabel.Text, issamethankyoumessageused);
        
        hdnImageName.Value = filename;
        hdnImageLink.Value = filename;
        fileUploadName.Value = filename;
        pollInfo = Pc.getPollInfo(pollId);
        
        ClientScript.RegisterClientScriptBlock(this.GetType(), "ImageUpload" + DateTime.Now.Millisecond.ToString(), "$('#MainContent_hdnImageLink').val('" + filename + "');  $('#MainContent_hdnImageName').val('" + filename + "');", false);

    }

    protected void sponsorLogo_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string filename = System.IO.Path.GetFileName(e.FileName);
        string fileextension = System.IO.Path.GetExtension(e.FileName);
        string imagefilelocation = Server.MapPath("//polls//images//" + pollId.ToString());// + "//" + filename;
        if (!Directory.Exists(imagefilelocation))
        {
            Directory.CreateDirectory(imagefilelocation);
        }
        if (filename.Length > 50)
        {
            filename = filename.Substring(0, 30);
            filename += fileextension;
        }
        string strlogolink = "";
        strlogolink = txtSponsorLink.Text;
        if(strlogolink.Length > 200)
        {
            strlogolink = strlogolink.Substring(0, 199);
        }
        sponsorLogo.SaveAs(imagefilelocation + "//" + filename);
        Pc.UpdateSponsorDetails(pollId, filename, txtSponsorText.Text, strlogolink);
        lblLogoName.Visible = true;
        lblLogoName.Value = filename;
        hdnLogoLink.Value = "exists";

    }

    protected void createUpdatePanel_Load(object sender, EventArgs e)
    {
        //ClientScript.RegisterClientScriptBlock(this.GetType(), "EditsSaved1" + DateTime.Now.Millisecond.ToString(), "<script>$(document).ready(function() { $('#placeholderinvitationmessage').css('display', 'block');$('#placeholdersubmitbuttonlabel').css('display', 'block');$('#placeholderthankyouoverlay').css('display', 'block');$('#placeholderredirecturl').css('display', 'block');$('#placeholderinvitationmessage').attr('for', 'MainContent_txtInvitationMessage');$('#placeholdersubmitbuttonlabel').attr('for', 'MainContent_txtSubmitButtonLabel');$('#placeholderredirecturl').attr('for', 'MainContent_txtRedirectURL');$('#placeholderthankyouoverlay').attr('for', 'MainContent_thankyoumessage'); } );</script>");
    }
}