<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pricing.aspx.cs" Inherits="new_polls_pricing" MasterPageFile="~/Home.master" %>
<%@ Register TagPrefix="uc" TagName="PollsHeader" Src="~/UserControls/PollsHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc:PollsHeader ID="PollsHeader" runat="server" />
    <div> 
      <div class="breadcrumbs-v3 img-v1 text-center">
        <div class="container">
       <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;">
            <span>Poll Plans </span></h2>
        </div>
      </div>

      <!--=== Content Part ===-->
      <div class="bg-grey content-sm pricing-page-intro">
        <div class="container text-center">
          <!--<h2 class="title-v2 title-center" style="margin-top:-20px;">PLANS TO SUIT YOUR UNIQUE POLL NEEDS</h2>-->
          <p class="space-lg-hor" style="font-size: 15px; margin-top:0px;">
          Your chance to experience Insighto Polls free, for 14 days.
          When you sign up for a free trial, you will automatically have access to the Publisher Plan. <br>
          You may select the Plan that suits you best after the free trial.</p>
          <a href="registration.aspx" class="btn-u">SIGN UP FOR A 14 DAY FREE TRIAL</a></div>
      </div>
  
      <!--=== Content Part ===-->
      <div id="plans" class="container content">
        <div class="row margin-bottom-10 pricing-rounded">
          <div class="col-md-3">
            <div class="pricing hover-effect" style="height:572px;">
              <div class="pricing-head">
                <h3 style="background:#1E73BE; text-shadow:none; font-size:20px !important; font-weight:normal !important;">Personal</h3>
                <h4><span id="spprof" style="font-size:35px;margin-top:22px;"><i class="fa fa-inr"></i> 0 </span><span style="margin-top:0px;">Lifetime Free </span></h4>
                <ul class="pricing-content list-unstyled">
                  <li class="bg-color" style="text-align:center;color:#fff !important;background:#1E73BE;font-size:20px;"> &nbsp; </li>
                </ul>
              </div>
              <ul class="pricing-content list-unstyled">
                <li class="bg-color"><i class="fa fa-check"></i> Text / Image / Video polls</li>
                <li><i class="fa fa-check"></i> Live preview while creating polls</li>
                <li class="bg-color"><i class="fa fa-check"></i> Public results </li>
                <li><i class="fa fa-check"></i> 500 responses / month</li>
                <li class="bg-color"><i class="fa fa-check"></i> No Sponsorship feature</li>
                
              </ul>
              <br>
              <div align="center"><a href="registration.aspx" class="btn-u-blue">SIGN UP</a></div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="pricing hover-effect" style="height:572px;">
              <div class="pricing-head">
                <h3 style="background:#1E73BE; text-shadow:none; font-size:20px !important; font-weight:normal !important;">Professional</h3>
                <h4><span id="spprof" style="font-size:35px;margin-top:22px;" runat="server"><i class="fa fa-inr"></i> 399 </span><span style="margin-top:0px;"> Per Month<br />Per Domain </span></h4>
                <ul class="pricing-content list-unstyled">
                  <li class="bg-color" style="text-align:center;color:#fff;background:#1E73BE;font-size:20px;">
                        <asp:LinkButton ID="lnkprof" runat="server" style="color:White;" CausesValidation="false"><%=AnnualProf %></asp:LinkButton>                  
                  </li>
                </ul>
              </div>
              <ul class="pricing-content list-unstyled">
                <li class="bg-color"><i class="fa fa-check"></i> Text / Image / Video polls</li>
                <li><i class="fa fa-check"></i> Live preview while creating polls</li>
                <li class="bg-color"><i class="fa fa-check"></i> Public results </li>
                <li><i class="fa fa-check"></i> 3,000 responses / month</li>
                <li class="bg-color"><i class="fa fa-check"></i> No Sponsorship feature</li>
                <li><i class="fa fa-check"></i> Lead generation feature</li>
                <li class="bg-color"><i class="fa fa-check"></i> Answer specific messaging</li>
              </ul>
              <br>
            </div>
          </div>
          <div class="col-md-3">
            <div class="pricing hover-effect" style="height:572px;">
              <div class="pricing-head">
                <h3 style="background:#1E73BE; text-shadow:none; font-size:20px !important; font-weight:normal !important;">Business</h3>
                <h4><span id="spBus" style="font-size:35px;margin-top:22px;" runat="server"><i class="fa fa-inr"></i> 1,299</span><span style="margin-top:0px;"> Per Month<br />Per Domain </span></h4>
                <ul class="pricing-content list-unstyled">
                  <li class="bg-color" style="text-align:center;color:#fff;background:#1E73BE;font-size:20px;"> 
                        <asp:LinkButton ID="lnkbus" runat="server" style="color:White;" CausesValidation="false"><%=AnnualBus %></asp:LinkButton>
                  </li>
                </ul>
              </div>
              <ul class="pricing-content list-unstyled">
                <li class="bg-color"><i class="fa fa-check"></i> Text / Image / Video polls</li>
                <li><i class="fa fa-check"></i> Live preview while creating polls</li>
                <li class="bg-color"><i class="fa fa-check"></i> Option to keep results private </li>
                <li><i class="fa fa-check"></i> 10,000 responses / month</li>
                <li class="bg-color" style="height:45px;"><i class="fa fa-check" style="height:100%;vertical-align:bottom"></i> <span style="width:90%;">Monetize Polls (Annual Plan only)</span></li>
                <li><i class="fa fa-check"></i> Lead generation feature</li>
                <li class="bg-color"><i class="fa fa-check"></i> Answer specific messaging</li>
              </ul>
            </div>
        
          </div>
          <div class="col-md-3">
            <div class="pricing hover-effect" style="height:572px;">
              <div class="pricing-head">
                <h3 style="background:#1E73BE; text-shadow:none; font-size:20px !important; font-weight:normal !important;">Publisher</h3>
                <h4><span id="spPub" style="font-size:35px;margin-top:22px;" runat="server"><i class="fa fa-inr"></i> 9,999</span><span style="margin-top:0px;"> Per Month<br />Per Domain </span></h4>
                <ul class="pricing-content list-unstyled">
                  <li class="bg-color" style="text-align:center;color:#fff;background:#1E73BE;font-size:20px;">
                    <asp:LinkButton ID="lnkpub" runat="server"  style="color:White;" CausesValidation="false"><%=AnnualPub %></asp:LinkButton>
                  </li>
                </ul>
              </div>
              <ul class="pricing-content list-unstyled">
                <li class="bg-color"><i class="fa fa-check"></i> Text / Image / Video polls</li>
                <li><i class="fa fa-check"></i> Live preview while creating polls</li>
                <li class="bg-color"><i class="fa fa-check"></i> Option to keep results private </li>
                <li><i class="fa fa-check"></i> 60,000 responses / month</li>
                <li class="bg-color" style="height:45px;"><i class="fa fa-check" style="height:100%;vertical-align:bottom"></i> <span style="width:90%;">Monetize Polls
                </span>
                <li><i class="fa fa-check"></i> Lead generation feature</li>
                    <li class="bg-color"><i class="fa fa-check"></i> Answer specific messaging</li>
                </li>
              </ul>
              <br>
            </div>
          </div>
        </div>
        <div class="row margin-bottom-10 pricing-rounded text-center grey">
            For Agency (multi-domain) pricing <a href="/contact.aspx">contact us</a> 
        </div>
      </div>
      <!--/container--> 
      <!--=== End Content Part ===--> 
    </div>
        <asp:HiddenField ID="hdnKey" runat="server" Value="" />
        <asp:HiddenField ID="hdnPrice" runat="server" Value="" />
        <asp:HiddenField ID="hdnTax" runat="server" Value="" />
        <asp:HiddenField ID="hdnTaxAmount" runat="server" Value="" />
        <asp:HiddenField ID="hdnType" runat="server" Value="" />
        <asp:HiddenField ID="hdnOrderNo" runat="server" Value="" />
        <asp:HiddenField ID="hdnTotal" runat="server" Value="" />
        <asp:HiddenField ID="hdnUpgrade" runat="server" Value="No" />
        <asp:HiddenField ID="hdnMerchantId" runat="server" Value="" />
</asp:Content>