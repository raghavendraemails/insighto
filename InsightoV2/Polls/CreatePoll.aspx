﻿    <%@ Page Title="" Language="C#" MasterPageFile="PollsMaster.master" AutoEventWireup="true" CodeFile="CreatePoll.aspx.cs" Inherits="Polls_new_design_CreatePoll"%>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Auto" EnablePartialRendering="true" AjaxFrameworkMode="Enabled"></asp:ScriptManager>

    <link href="css/SliderStyleSheet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/style.css?ver=<%=DateTime.Now.Millisecond.ToString() %>" />
    <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection" />

    <script src="/Polls/WebScripts/jquery.easing.min.js" type="text/javascript"></script>
    <script src="js/materialize.js"></script>
    <script src="js/init.js?ver=<%=DateTime.Now.Millisecond.ToString() %>"></script>

    <script src="js/chosen.proto.js" type="text/javascript"></script>
    <script src="js/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/prefixfree.min.js" type="text/javascript"></script>
   <link rel="Stylesheet" href="css/chosen.css" />

    <script type="text/javascript">

        function ShowCurrentCity(countryname) {
            var obj = {};
            var paramlist;
            obj.countryname = countryname;
            $.ajax({
                type: "POST",
                url: "CreatePoll.aspx/getCities",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                async: false,
                dataType: "json",
                success: function (r) {
                    //////console.log(r);
                    paramlist = r.d;

                }
            });
            return paramlist;
        }

        //NEED TO ADD CITIES AND COUNTRIES DROPDOWN CODE HERE
    </script>

    <script type="text/javascript">

        $(document).ready(function () {

            $('#pollSlider-button').click(function () {
                var polllink = $("#MainContent_hdnPollLink").val();
                var framecontent = "";
                framecontent = "<iframe id='IframeLaunch' height='" + $("#MainContent_txtHeightIframe").val() + "' width='" + $("#MainContent_txtWidthIframe").val() + "'  scrolling='auto' src='" + polllink + "' frameborder='0'   style='-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px; -moz-box-shadow: 4px 4px 14px #000; -webkit-box-shadow: 4px 4px 14px #000; box-shadow: 4px 4px 14px #000;'  ></iframe>";

                var elementid = "";
                var isCheckedBC = document.getElementById('MainContent_rblposition_0').checked;
                var isCheckedCL = document.getElementById('MainContent_rblposition_1').checked;
                var isCheckedBL = document.getElementById('MainContent_rblposition_2').checked;
                var isCheckedCR = document.getElementById('MainContent_rblposition_3').checked;
                var isCheckedBR = document.getElementById('MainContent_rblposition_4').checked;

                var isChecked1SD = document.getElementById('MainContent_rblspeed_0').checked;
                var isChecked2SD = document.getElementById('MainContent_rblspeed_1').checked;
                var isChecked3SD = document.getElementById('MainContent_rblspeed_2').checked;

                var today = new Date();
                var pollheight = 0;
                if (!isNaN(parseInt($('#MainContent_lblRecThemeHeight').html()))) {
                    if (parseInt($('#MainContent_lblRecThemeHeight').html()) > 0)
                        pollheight = parseInt($('#MainContent_lblRecThemeHeight').html());
                }
                if (pollheight == 0) {
                    pollheight = parseInt($('#MainContent_lblHeight').html());
                }
                var scalevalue = 0.90;

                $('#IframeLaunch').attr('height', pollheight);
                $('#IframeLaunch').css('height', pollheight);

                if (isCheckedBC) {
                    elementid = "pprev_CM";
                    $('#framecontent1').html(framecontent);
                    $('.pprev_CM').show();

                    if (isChecked1SD) {
                        $(".pprev_CM").animate({ bottom: "0px" }, 5000, "linear", "easeInOutExpo"); /*center center preview*/
                    }
                    else if (isChecked2SD) {
                        $(".pprev_CM").animate({ bottom: "0px" }, 2500, "linear", "easeInOutExpo");
                    }
                    else if (isChecked3SD) {
                        $(".pprev_CM").animate({ bottom: "0px" }, 800, "linear", "easeInOutExpo");
                    }
                    $('.pprev_BL').hide();
                    $('.pprev_BR').hide();
                    $('.pprev_LC').hide();
                    $('.pprev_RC').hide();

                    $('#alertclose').click(function () {
                        $('.pprev_CM').hide();
                        $('.pprev_CM').css('bottom', "-600px");
                    });
                }

                if (isCheckedBL) {
                    elementid = "pprev_BL";
                    $('#framecontent2').html(framecontent);
                    $('.pprev_BL').show();

                    if (isChecked1SD) {
                        $(".pprev_BL").animate({ bottom: "0px", left: "0px" }, 5000, "linear", "easeInOutExpo");
                    }
                    else if (isChecked2SD) {
                        $(".pprev_BL").animate({ bottom: "0px", left: "0px" }, 2500, "linear", "easeInOutExpo");
                    }
                    else if (isChecked3SD) {
                        $(".pprev_BL").animate({ bottom: "0px", left: "0px" }, 800, "linear", "easeInOutExpo");
                    }
                    $('.pprev_CM').hide();
                    $('.pprev_BR').hide();
                    $('.pprev_LC').hide();
                    $('.pprev_RC').hide();

                    $('#alertclose1').click(function () {
                        $('.pprev_BL').hide();
                        $('.pprev_BL').css('bottom', "-600px");
                    });

                }

                if (isCheckedBR) {
                    elementid = "pprev_BR";
                    $('#framecontent3').html(framecontent);
                    $('.pprev_BR').show();
                    //document.getElementById('IframeLaunch').contentWindow.location.reload();
                    //document.getElementById('IframeLaunch').src += '';
                    if (isChecked1SD) {
                        $(".pprev_BR").animate({ bottom: "0px" }, 5000, "linear", "easeInOutExpo");
                    }
                    else if (isChecked2SD) {
                        $(".pprev_BR").animate({ bottom: "0px" }, 2500, "linear", "easeInOutExpo");
                    }
                    else if (isChecked3SD) {
                        $(".pprev_BR").animate({ bottom: "0px" }, 800, "linear", "easeInOutExpo");
                    }
                    $('.pprev_CM').hide();
                    $('.pprev_BL').hide();
                    $('.pprev_LC').hide();
                    $('.pprev_RC').hide();

                    $('#alertclose2').click(function () {
                        $('.pprev_BR').hide();
                        $('.pprev_BR').css('bottom', "-600px");
                    });
                }

                if (isCheckedCL) {
                    elementid = "pprev_LC";
                    $('#framecontent4').html(framecontent);
                    $('.pprev_LC').show();

                    if (isChecked1SD) {
                        $(".pprev_LC").animate({ left: "0px" }, 5000, "linear", "easeInOutExpo");
                    }
                    else if (isChecked2SD) {
                        $(".pprev_LC").animate({ left: "0px" }, 2500, "linear", "easeInOutExpo");
                    }
                    else if (isChecked3SD) {
                        $(".pprev_LC").animate({ left: "0px" }, 800, "linear", "easeInOutExpo");
                    }

                    $('.pprev_CM').hide();
                    $('.pprev_BL').hide();
                    $('.pprev_BR').hide();
                    $('.pprev_RC').hide();

                    $('#alertclose3').click(function () {
                        $('.pprev_LC').hide();
                        $('.pprev_LC').css('left', "-42%");
                    });
                }

                if (isCheckedCR) {
                    elementid = "pprev_RC";
                    $('#framecontent5').html(framecontent);
                    $('.pprev_RC').show();

                    if (isChecked1SD) {
                        $(".pprev_RC").animate({ right: "0px" }, 5000, "linear", "easeInOutExpo");
                    }
                    else if (isChecked2SD) {
                        $(".pprev_RC").animate({ right: "0px" }, 2500, "linear", "easeInOutExpo");
                    }
                    else if (isChecked3SD) {
                        $(".pprev_RC").animate({ right: "0px" }, 800, "linear", "easeInOutExpo");
                    }
                    $('.pprev_CM').hide();
                    $('.pprev_BL').hide();
                    $('.pprev_BR').hide();
                    $('.pprev_LC').hide();

                    $('#alertclose4').click(function () {
                        $('.pprev_RC').hide();
                        $('.pprev_RC').css('right', "-42%");
                    });
                }
                if (isCheckedCR || isCheckedCL) {
                    if (pollheight > 500) {
                        $('.' + elementid).css('top', '10px');
                    }
                }
                $('.pprev_style ' + elementid).css('height', pollheight);
                document.cookie = "iframeheight=" + pollheight.toString() + "; domain=.insighto.com; path=/;";

                if (pollheight > 500) {
                    $('.' + elementid).css('-ms-transform', 'scale(' + scalevalue.toString() + ')');
                    $('.' + elementid).css('-moz-transform', 'scale(' + scalevalue.toString() + ')');
                    $('.' + elementid).css('-o-transform', 'scale(' + scalevalue.toString() + ')');
                    $('.' + elementid).css('-webkit-transform', 'scale(' + scalevalue.toString() + ')');
                    $('.' + elementid).css('transform', 'scale(' + scalevalue.toString() + ')');
                }
            });


        });

    </script>




    <script type="text/javascript">
        var previousCheckId;
        var previousCheckIdSD;

        function toggle(chkBox) {

            if (chkBox.checked) {
                if (previousCheckId) {

                    if (previousCheckId == chkBox.getAttribute('id')) {
                        document.getElementById(chkBox.getAttribute('id')).checked = true;
                    }
                    else {
                        document.getElementById(previousCheckId).checked = false;
                    }
                }
                previousCheckId = chkBox.getAttribute('id');
                if (previousCheckId == "MainContent_chkslider") {
                    $('#dvShowHide').slideUp();
                    $('.pprev_CM').hide();
                    $('.pprev_BL').hide();
                    $('.pprev_BR').hide();
                    $('.pprev_RC').hide();
                }
                else if (previousCheckId == "MainContent_chkEmbed") {
                    $('#dvsliderops').slideUp();
                }
            }
        }



    </script>


<style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @charset "UTF-8";
@import url(http://fonts.googleapis.com/css?family=Dosis:500,800);
body {
  background-color: #666;
}

.text {
  font-family: "微软雅黑", "Dosis", sans-serif;
  font-size: 50px;
  text-align: center;
  font-weight: bold;
  line-height: 200px;
  text-transform: uppercase;
  position: relative;
}

.effect01 {
  background-color: #333;
  color: #fefefe;
  text-shadow: 0px 1px 0px #c0c0c0, 0px 2px 0px #b0b0b0, 0px 3px 0px #a0a0a0, 0px 4px 0px #909090, 0px 5px 10px rgba(0, 0, 0, 0.6);
}

.effect02 {
  color: #333;
  background-color: #ddd;
  text-shadow: 1px -1px 0 #767676,  -1px 2px 1px #737272,  -2px 4px 1px #767474,  -3px 6px 1px #787777,  -4px 8px 1px #7b7a7a,  -5px 10px 1px #7f7d7d,  -6px 12px 1px #828181,  -7px 14px 1px #868585,  -8px 16px 1px #8b8a89,  -9px 18px 1px #8f8e8d,  -10px 20px 1px #949392,  -11px 22px 1px #999897,  -12px 24px 1px #9e9c9c,  -13px 26px 1px #a3a1a1,  -14px 28px 1px #a8a6a6,  -15px 30px 1px #adabab,  -16px 32px 1px #b2b1b0,  -17px 34px 1px #b7b6b5, -18px 36px 1px #bcbbba,  -19px 38px 1px #c1bfbf,  -20px 40px 1px #c6c4c4,  -21px 42px 1px #cbc9c8,  -22px 44px 1px #cfcdcd;
}

.effect03 {
  color: #202020;
  background-color: #2d2d2d;
  text-shadow: -1px -1px 1px #111111, 2px 2px 1px #363636;
}

.effect04 {
  background-color: #333;
  background-image: linear-gradient(45deg, transparent 45%, #ebe9e0 45%, #ebe9e0 55%, transparent 0);
  background-size: .05em .05em;
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  -webkit-text-stroke: 2px #111;
}

.effect05 {
  color: #DC554F;
  background-color: #27ae60;
  z-index: 3;
}

.effect05:before {
  content: attr(data-text);
  width: 100%;
  line-height: 200px;
  opacity: .5;
  position: absolute;
  top: 2px;
  left: 5px;
  background-image: linear-gradient(45deg, transparent 45%, #ebe9e0 45%, #ebe9e0 55%, transparent 0);
  z-index: -1;
  background-size: .05em .05em;
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  animation: shadowGo 20s linear infinite;
}

@keyframes shadowGo {
  0% {
    background-position: 0 0;
  }
  0% {
    background-position: -100% 100%;
  }
}
.effect06 {
  -webkit-text-fill-color: transparent;
  -webkit-text-stroke: 2px #d6d6d6;
  background: url(http://www.pencilscoop.com/demos/CSS_Text_Effects/images/galaxy.jpg);
  background-size: cover;
}

.effect07 {
  background: url(http://www.pencilscoop.com/demos/CSS_Text_Effects/images/galaxy.jpg) #333;
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  background-size: cover;
  animation: 10s infinite linear animate;
}

.effect07:before {
  content: "";
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  top: 0;
  background-color: #999;
  z-index: -1;
}

@keyframes animate {
  0% {
    background-position: 0;
  }
  100% {
    background-position: -1000px 0;
  }
}
.effect08 {
  color: #fff;
  height: 200px;
  transform-origin: center bottom;
  transform-style: preserve-3d;
  transition: all 1s;
  cursor: pointer;
}

.effect08:hover {
  transform: rotate3d(1, 0, 0, 40deg);
}

.effect08 h1 {
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
  text-shadow: 0 0 10px rgba(0, 0, 100, 0.5);
}

.effect08 h1:nth-child(1) {
  transform: translateZ(4px);
}

.effect08 h1:nth-child(2) {
  transform: translateZ(8px);
}

.effect08 h1:nth-child(3) {
  transform: translateZ(12px);
}

.effect08 h1:nth-child(4) {
  transform: translateZ(16px);
}

.effect08 h1:nth-child(5) {
  transform: translateZ(20px);
}

.effect08 h1:nth-child(6) {
  transform: translateZ(24px);
}

.effect08 h1:nth-child(7) {
  transform: translateZ(28px);
}

.effect09 {
  background: #acacac -webkit-gradient(linear, 100% 0, 0 0, from(#acacac), color-stop(0.5, #ffffff), to(#acacac));
  background-position: -4rem top;
  /*50px*/
  background-repeat: no-repeat;
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  animation: shimmer 2.2s infinite;
  -webkit-background-size: 4rem 100%;
  /*50px*/
  position: relative;
}

/*这么个东东就是为了弄背景*/
.effect09:after {
  content: "";
  width: 100%;
  height: 100%;
  background: url(http://www.pencilscoop.com/demos/CSS_Text_Effects/images/galaxy.jpg);
  position: absolute;
  left: 0;
  top: 0;
  z-index: -1;
}

@-webkit-keyframes shimmer {
  0% {
    background-position: -4rem top;
  }
  70% {
    background-position: 30rem top;
  }
  100% {
    background-position: 35rem top;
  }
}

    </style>

    <link rel="stylesheet" type="text/css" href="css/poll-preview.css?ver=<%=DateTime.Now.Millisecond.ToString() %>" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css"/>
    <script type="text/javascript">

        function CkKeyPress(e) {

            var evt = (e) ? e : window.event;
            var key = (evt.keyCode) ? evt.keyCode : evt.which;

            if (key == 13) {
                return false;
            }
            //renderChart();

            return true; //it should be true
        }

        $(document).ready(function () {

            //ADDED THIS SO ANY CITIES AND COUNTRIES SELECTED GO BACK TO THE OLD FORM VARIABLES ONLY FOR CONSISTENCY
            $("#selectcities").change(function () {
                $("#MainContent_tbcities").val("");
                $("#MainContent_tbcities").val($("#selectcities").val());
            });

            //ADDED THIS TO TRIGGER THE CITY DROPDOWN LIST selectcities
            $("#selectcountries").change(function () {
                RefreshCityList();
                $("#MainContent_tbCountries").val("");
                $("#MainContent_tbCountries").val($("#selectcountries").val());
            });

            function RefreshCityList() {
                var countriesselected = "";
                if ($("#selectcountries").val() != null)
                    countriesselected = $("#selectcountries").val().toString();

                var arraycountrieselected = countriesselected.split(",");

                var currentcities = "";
                var thiscitylist = "";
                for (var x = 0; x < arraycountrieselected.length; x++) {
                    thiscitylist = "";
                    thiscitylist = ShowCurrentCity(arraycountrieselected[x]);
                    if (thiscitylist != "") {
                        currentcities += ShowCurrentCity(arraycountrieselected[x]) + ",";
                    }
                }

                var arraycities = currentcities.split(',');
                arraycities.sort();

                var Cities = new Array(arraycities.length);
                var selectedcities = $("#MainContent_tbcities").val();
                $('#selectcities').find('option').remove();
                for (k = 0; k < arraycities.length; k++) {
                    if (arraycities[k] != "") {
                        if (selectedcities.indexOf(arraycities[k]) >= 0) {
                            $("#selectcities").append("<option value='" + arraycities[k] + "' selected>" + arraycities[k] + "</option>");
                        }
                        else {
                            $("#selectcities").append("<option value='" + arraycities[k] + "'>" + arraycities[k] + "</option>");
                        }
                    }
                }

                if ($("#selectcountries").val() != null) {
                }
                else {
                    $("#MainContent_tbCountries").val("");
                    $("#MainContent_tbcities").val("");
                    $("#selectcities").val("");
                    $("#selectcities").val("");
                }

                $("#selectcities").chosen({ width: "60%" });
                $("#selectcities").trigger("chosen:updated");
                $("#selectcountries").trigger("chosen:updated");

            }

            //MODIFIED THIS SO THE COUNTRIES ARE NOW POPULATED IN THE selectcountries DROPDOWN
            function BindControls() {

                var listString = $("#MainContent_HiddenField1").val();
                var arraycountries = listString.split(',');
                var preselectedcities = $("#MainContent_tbcities").val();
                var preselectedcountries = $("#MainContent_tbCountries").val();
                var arraypreselectedcities = preselectedcities.split(",");
                var preselectedcountries = preselectedcountries.split(",");

                var Countries = new Array(arraycountries.length);
                var thiscountry = "";
                for (k = 0; k < arraycountries.length; k++) {
                    thiscountry = arraycountries[k];
                    if (preselectedcountries.indexOf(thiscountry) >= 0) {
                        $("#selectcountries").append("<option value='" + arraycountries[k] + "' selected=selected>" + arraycountries[k] + "</option>");
                    }
                    else {
                        $("#selectcountries").append("<option value='" + arraycountries[k] + "'>" + arraycountries[k] + "</option>");
                    }
                }
                if (preselectedcities != "") {
                    RefreshCityList();
                }

                $("#selectcountries").chosen({ width: "60%" });
                $("#selectcountries").trigger("chosen:updated");
            }

            BindControls();
            RefreshCityList();

            $("#MainContent_tbCountries").hide();
            $("#MainContent_tbcities").hide();
        });


        var answercolour = "";
        var chartheight = 0;
        var arrColours = ['#AFD8F8', '#F6BD0F', '#8BBA00', '#A66EDD', '#F984A1', '#AFD8F8', '#F6BD0F', '#8BBA00', '#A66EDD', '#F984A1'];
        var chartwidth = 0;
        var thisrowpercentage = 0;
        var thisrowwidth = 0;
        var purejschartdata = "";
        var baseFont = "";
        var baseFontSize = "";
        var baseFontBold = "0";
        var baseFontItalic = "0";
        var baseFontUnderline = "0";
        var chartanswerrowcount = 0;
        var chartanswerrowcountnew = 0;
        var adjustedchartwidth = 0;
        var adjustedanswerpercentage = 0;

        function renderChart() {
            purejschartdata = "";
            chartwidth = parseInt($("#MainContent_txtWidthPoll").val());
            chartanswerrowcount = parseInt($("#MainContent_hdnRepeaterTxts").val());
            baseFontBold = $("#MainContent_hdnAnswerBold").val();
            baseFontUnderline = $("#MainContent_hdnAnswerBold").val();
            baseFontItalic = $("#MainContent_hdnAnswerItalic").val()
            adjustedchartwidth = Math.round(chartwidth * 0.75);
            adjustedanswerpercentage = chartwidth - adjustedchartwidth - 45;
            baseFont = $("#MainContent_hdnAnswerFont").val();
            var answercolour = $("#MainContent_txtAnswerColor").val();
            if (baseFont == "") {
                baseFont = "Arial";
            }
            baseFontSize = $("#MainContent_hdnAnswerFSize").val();

            chartanswerrowcountnew = 0;
            for (var row = 0; row < chartanswerrowcount; row++) {
                if (document.getElementById('txtAnswerOption_' + row) != null) {
                    chartanswerrowcountnew++;
                }
            }

            var noofcols = chartanswerrowcountnew;
            if (noofcols == 2) {
                var myArray = ['40', '60'];
            }
            else if (noofcols == 3) {
                var myArray = ['20', '30', '50'];
            }
            else if (noofcols == 4) {
                var myArray = ['10', '20', '30', '40'];
            }
            else if (noofcols == 5) {
                var myArray = ['10', '15', '20', '25', '30'];
            }
            else if (noofcols == 6) {
                var myArray = ['5', '10', '15', '20', '22', '28'];
            }
            else if (noofcols == 7) {
                var myArray = ['5', '10', '12', '13', '17', '20', '23'];
            }
            else if (noofcols == 8) {
                var myArray = ['5', '7', '9', '11', '13', '15', '19', '21'];
            }
            else if (noofcols == 9) {
                var myArray = ['1', '3', '6', '9', '10', '14', '17', '19', '21'];
            }
            else if (noofcols == 10) {
                var myArray = ['1', '3', '5', '7', '9', '11', '13', '15', '17', '19'];
            }
            for (var row1 = 0; row1 < chartanswerrowcountnew; row1++) {
                if (document.getElementById('txtAnswerOption_' + row1) != null) {

                    thisrowpercentage = parseInt(myArray[row1]);
                    thisrowwidth = Math.round(thisrowpercentage * chartwidth / 100);

                    thisanswer = "";
                    if ($("#txtAnswerOption_" + row1).val() != "") {
                        thisanswer = $("#txtAnswerOption_" + row1).val();
                    }
                    else {
                        thisanswer = "Answer " + (row1 + 1).toString();
                    }
                    $("#lblradText_" + row1).css("font-family", baseFont + " !important");

                    thisanswer = escape(thisanswer);
                    while (thisanswer.indexOf("%22") >= 0) {
                        thisanswer = thisanswer.replace("%22", "");
                    }
                    while (thisanswer.indexOf("%22") >= 0) {
                        thisanswer = thisanswer.replace("%22", "");
                    }
                    ////////console.log(thisanswer);
                    while (thisanswer.indexOf("%3C") >= 0) {
                        //////console.log(thisanswer);
                        thisanswer = thisanswer.replace("%3C", "&lt;");
                    }
                    ////////console.log(thisanswer);
                    while (thisanswer.indexOf("%3E") >= 0) {
                        thisanswer = thisanswer.replace("%3E", "&gt;");
                    }
                    thisanswer = unescape(thisanswer);
                    while (thisanswer.indexOf("'") >= 0) {
                        thisanswer = thisanswer.replace("'", "`");
                    }
                    purejschartdata += "<div style='max-width:100%; width:" + adjustedchartwidth + "px;text-align:left;font-size:" + baseFontSize + "px;font-weight:" + baseFontBold + ";font-family:" + baseFont + ";color:" + answercolour + ";' class='chartlabel'>" + thisanswer + "</div>";
                    purejschartdata += "<div style='max-width:100%; width:" + adjustedchartwidth + "px;float:left;background-color:lightgray;'>";
                    purejschartdata += "<div style='height:15px;width:0px;background-color:" + arrColours[row1] + "' id='div" + row1 + "'></div>";
                    purejschartdata += "</div>";
                    //purejschartdata += "<div style='float:right;width:" + adjustedanswerpercentage + "px;vertical-align:top;font-size:" + baseFontSize + "px;font-weight:" + baseFontBold + ";font-family:" + baseFont + ";color:" + answercolour + ";'>" + thisrowpercentage + "%</div>";
                    purejschartdata += "<div style='float:right;vertical-align:top;font-size:13px;font-weight:" + baseFontBold + ";font-family:" + baseFont + ";color:" + answercolour + ";'>" + thisrowpercentage + "%</div>";
                    purejschartdata += "<div style='width:" + (adjustedchartwidth) + "px;height:26px;'></div>";
                }
            }
            $('#purejschart').show();
            $("#MainContent_hdnChartWidth").val(chartwidth);
            $('#purejschart').html(purejschartdata);

            for (var x = 0; x < chartanswerrowcountnew; x++) {

                if (document.getElementById('txtAnswerOption_' + x) != null) {
                    thisrowpercentage = parseInt(myArray[x]);
                    thisrowwidth = Math.round(thisrowpercentage * adjustedchartwidth / 100);
                    $("#div" + x.toString()).animate({
                        width: thisrowwidth
                    }, 1000);
                }
            }
        }
    </script>
    
    <asp:HiddenField ID="hdnImageRecHeight" runat="server" />
    <asp:HiddenField ID="hdnImageRecWidth" runat="server" />
    <asp:HiddenField ID="hdnQuestionBold" runat="server" />
    <asp:HiddenField ID="hdnQuestionItalic" runat="server" />
    <asp:HiddenField ID="hdnQuestionUL" runat="server" />
    <asp:HiddenField ID="hdnQuestionAlign" runat="server" />
    <asp:HiddenField ID="hdnQuestionFSize" runat="server" />
    <asp:HiddenField ID="hdnAnswerFont" runat="server" />
    <asp:HiddenField ID="hdnAnswerBold" runat="server" />
    <asp:HiddenField ID="hdnAnswerItalic" runat="server" />
    <asp:HiddenField ID="hdnAnswerUL" runat="server" />
    <asp:HiddenField ID="hdnAnswerAlign" runat="server" />
    <asp:HiddenField ID="hdnAnswerFSize" runat="server" />
    <asp:HiddenField ID="hdnVoteBold" runat="server" />
    <asp:HiddenField ID="hdnVoteItalic" runat="server" />
    <asp:HiddenField ID="hdnVoteUL" runat="server" />
    <asp:HiddenField ID="hdnVoteFSize" runat="server" />
    <asp:HiddenField ID="hdnVoteAlign" runat="server" />
    <asp:HiddenField ID="hdnMsgBold" runat="server" />
    <asp:HiddenField ID="hdnMsgItalic" runat="server" />
    <asp:HiddenField ID="hdnMsgUL" runat="server" />
    <asp:HiddenField ID="hdnMsgFSize" runat="server" />
    <asp:HiddenField ID="hdnMsgAlign" runat="server" />
    <asp:HiddenField ID="hdnRecHeight" runat="server" />
    <asp:HiddenField ID="hdnRecWidth" runat="server" />
    <asp:HiddenField ID="hdnRecWidth1" runat="server" />
    <asp:HiddenField ID="hdnRecHeightne" runat="server" />
    <asp:HiddenField ID="hdnRecHeightne1" runat="server" />
    <asp:HiddenField ID="hdnRepeaterTxts" runat="server" />
    <asp:HiddenField ID="hdnLogoLink" runat="server" />
    <asp:HiddenField ID="hdnSetHeight" runat="server" />
    <asp:HiddenField ID="hdnImageHeight" runat="server" />
    <asp:HiddenField ID="hdnansHeight" runat="server" />
    <asp:HiddenField ID="hdnChartHeight" runat="server" />
    <asp:HiddenField ID="hdnChartWidth" runat="server" />
    <asp:HiddenField ID="hdnPollType" runat="server" />
    <asp:HiddenField ID="hdnPollID" runat="server" />
    <asp:HiddenField ID="hdnParentPollID" runat="server" />
    <asp:HiddenField ID="hdnActivationFlag" runat="server" />
    <asp:HiddenField ID="hdnPollLink" runat="server" />
    <asp:HiddenField ID="hdnCurrentPage" runat="server" />
    <asp:HiddenField ID="hdnPollStatus" runat="server" />
    <asp:HiddenField ID="hdnImageName" runat="server" />
    <asp:HiddenField ID="hdnPollResultsOption" runat="server" />
    <asp:HiddenField ID="hdnImageLink" runat="server" />
    <header>
        <div class="row nopad"  style="margin:0;" >
            <div id="editpollname" class="modal" style="height:40% !important;max-height:40% !important;min-height:300px !important;width:30%; max-width:30%;">
                <div class="modal-content text-center col m12 s12">
                        <h4 class="center-align">Edit poll name</h4>
                        <p class="col m12 center-align">
                            <span class="col m12  center-align">
                                Enter new name for poll
                            </span>
                        <asp:TextBox runat="server" onkeypress="return CkKeyPress(event);" ID="txtEditName" MaxLength="100" BorderColor="Black" BorderStyle="Solid" BorderWidth="1"  CssClass="col m6 nomar no-pad" Width="100%" ></asp:TextBox>
                        </p>
                </div>
                <div class="modal-footer center-align col m12 s12" style="text-align:center;">
                    <div class="col m6 s6" style="text-align:left;">
                        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-large red  white-text" style="float:none;width:60% !important;">Cancel</a>
                    </div>
                    <div class="col m6 s6" style="text-align:left;">
                        <asp:LinkButton ID="btnEditPollName" runat="server"
                                CausesValidation="false" OnClick="lnkEdit_Click" CssClass="modal-action modal-close waves-effect waves-green btn-large green white-text" Width="60%">Save</asp:LinkButton>            
                    </div>
                </div>
            </div>
            
            <div id="copydialog" class="modal image-container" style="z-index:999999; margin-top:200px; width: 40vw;min-height: inherit !important; min-width: 40vw;">
                <div class="modal-content col m12">
                    <h6>Copy the link below to share</h6>
                    <p><textarea ID="txtCopyWebLink" Class="iframe" runat="server" style="height: 100%; width: 100%;"></textarea></p>
                </div>
            </div>

            <div id="activatedialog" class="modal image-container" style="height:250px;max-height:250px;min-height:250px;z-index:999999;">
                <h5 class="center-align">Replace poll</h5>
                <div class="modal-content col m12">
                    <p>
                        Once you launch this replaced poll, the original poll will be automatically closed and in its place this poll will be displayed.
                        <br />Are you sure you want to proceed?            
                    </p>
                </div>
                <div class="modal-footer center-align col m12">
                    <div class="col m6 left-align">
                        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-large red  white-text" style="float:none;">No</a>
                    </div>
                    <div class="col m6 right-align">
                        <asp:LinkButton ID="btnActivate" runat="server"
                                CausesValidation="false" OnClick="btnActivate_Click" CssClass="modal-action modal-close waves-effect waves-green btn-large green white-text">Yes</asp:LinkButton>
                
                    </div>
                </div>
            </div>
            
            <div id="modalactivationalert" class="modal center-align" style="height:400px !important;max-height:400px !important;min-height:400px !important;z-index:999999 !important;">
                <div class="modal-content center-align" style="padding-bottom:0px;">
                  <h4>Account Activation</h4>
                  <p>
                      Congratulations! You are one just one step away from launching your first poll. However, you have not yet fully activated your Insighto account.
                      <br/><br/>
                      Please have a look the mail received from Insighto - with the subject "Please confirm your email address" and click on the Confirm button given in it. You would then be ready to launch your first poll.
                      <br/><br/>
                      In case you have not received the email <a href="javascript:void(0);" onclick="sendActivationEmail()">click here </a> and we will send it again. If you have any issues, please contact support@insighto.com.
                      <br /><br />
                      Happy Polling!
                  </p>
                </div>
                <div class="modal-footer center-align" style="text-align:center;">
                    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-large green white-text" style="float:none;">OK</a>
                </div>
              </div>


            <div class="col m12 s12 nopad" style="margin:0px !important; position:fixed; left:0; z-index:99999; background-color: #319AFA;height:6% !important;" id="headerbar" runat="server">
                <div class="col m5 s5 nopad nomar" id="polltitlediv" runat="server">
                    <div style="padding:10px; margin:0 0 10px 0;" class="row">
                        <i class="material-icons left pin-bottom white-text" runat="server" id="pollstatus">favorite</i>
                
                        <asp:Label ID="lblPollName" Text="" runat="server" Font-Size="Medium"></asp:Label>
                        <a id="btnOpenModalDialog" runat="server" class="modal-trigger" data-target="editpollname" style="padding-left:8px; ">
                            <i class="material-icons" style="font-size:16px;color:white; cursor:pointer;">border_color</i>
                        </a>
                    </div>
                </div>
                <div class="col m7 s7 white right-align" style="margin-top:10px !important;" id="pollanalyticsbar" runat="server">
                     <div class="col m12 s12">
                         <div class="col m12 s12">
                            <a id="pollanalytics" runat="server" style="color: #8ABA3C; border: 1px solid #8ABA3C; padding: 4px 8px; border-radius: 7px; background: #fff !important; font-size: 16px; font-weight: bold;">Poll Analytics</a>
                            &nbsp;
                             <a id="closeicon" runat="server" style="color: #8ABA3C; border: 1px solid #8ABA3C; padding: 4px 8px; border-radius: 7px; background: #fff !important; font-size: 16px; font-weight: bold;cursor:pointer;" onclick="closeIframe()" >Close X</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main>
    <div class="col m12 s12 nopad" style="border-style:none; margin-top:55px;">
        <div class="row nopad  pool-popup" style="background-color:white;">
        <div class="col m5 s12 pool-popup-left" style="border-style:none;" id="leftsidediv" runat="server">
                    <div class="col m12 s12">
                        <div class="tabs z-depth-2" style="padding-top:2px;background-color:<%=bgcolourstandard%>;z-index:99999;" id="nagivationtabs">
                            <div class="tab col s4" runat="server" id="divcreatebutton">
                                <asp:LinkButton ID="CreateNew" runat="server" Text="CREATE" OnClick="CreateNew_Click" />
                            </div>
                            <div class="tab col s4" runat="server" id="divdesignbutton">
                                <asp:LinkButton ID="DesignNew" runat="server" Text="DESIGN" OnClick="DesignNew_Click"/>
                            </div>
                            <div class="tab col s4" runat="server" id="divlaunchbutton">
                                <asp:LinkButton ID="LaunchNew" runat="server" Text="LAUNCH" OnClick="LaunchNew_Click"/>
                            </div>
                        </div>
                    </div>
           <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="createUpdatePanel" ChildrenAsTriggers="false" OnLoad="createUpdatePanel_Load">
               <Triggers>
                   <asp:AsyncPostBackTrigger ControlID="CreateNew" EventName="Click" />
                   <asp:AsyncPostBackTrigger ControlID="DesignNew" EventName="Click" />
                   <asp:AsyncPostBackTrigger ControlID="LaunchNew" EventName="Click" />
                   <asp:AsyncPostBackTrigger ControlID="SaveAndClose" EventName="Click" />
               </Triggers>
    <ContentTemplate>
     <div id="create" class="col s12 m12 active mCustomScrollbar" runat="server">
                    <div class="row"></div>
         <div class="card-panel col m12">
<div class="card-title"><h5>Poll Dimensions</h5></div>
                    <div class="row nopad nomar">
                        <div class="row nopad nomar">
                            <%--<h6>Poll Dimensions</h6>--%>
                            <div class="col m12 s12 row">
                                <input type="checkbox" id="ckbAutoAdjust" runat="server" class="input-field" onclick="autoAdjust('create');"/> 
                                <label for="MainContent_ckbAutoAdjust">Auto-adjust height of Poll</label>
                            </div>
                            <div class="col s4">
                                <label for="MainContent_txtWidthPoll">Width(px)</label>
                                <div class="input-field nopad nomar">
                                    <asp:TextBox ID="txtWidthPoll" CssClass="validate" onchange="javascript:iframewidthsetcreate();renderChart();" onkeypress="return CkKeyPress(event);"  runat="server" MaxLength="3"></asp:TextBox>
                                </div>
                            </div>
                            <div class=" col s4">
                                <label for="MainContent_txtHeightPoll">Height(px)</label>
                                <div class="input-field nomar">
                                    <asp:TextBox ID="txtHeightPoll" CssClass="validate" onchange="javascript:iframeheightsetcreate();setOverallPreviewHeight();renderChart();" onkeypress="return CkKeyPress(event);"  runat="server" MaxLength="3"></asp:TextBox>
                                </div>
                            </div>
                            <div class=" col s4">
                                <div id="recheightincreate"  runat="server" style="font-size:12px !important;" class="center-align">
                                    Recommended 
                                    
                             <div class="input-field">
                                    <p  class="center-align black-text"><asp:Label ID="lblRecThemeWidth" runat="server"></asp:Label> x <asp:Label ID="lblRecThemeHeight" runat="server" ></asp:Label></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
             </div>

                  <div class="card-panel col m12">
                    <div class="card-title"><h5>Poll Question & Answers</h5></div>
                    <div class="row nopad nomar" id="videoDiv" runat="server" style="display:none">
                        <h6>Paste the embed code of your YouTube video here</h6>
                        <div class="row">
                            <div class="input-field col s12">
                                <asp:TextBox ID="txtVideo" onkeypress="return CkKeyPress(event);" runat="server" CssClass="black-text" TextMode="MultiLine" Height="75" Rows="8" MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    
                    <hr style="border-style:dotted; display:none;" id="videoDivBorder"  runat="server"/>
                    
                    <div class="row nopad nomar" id="ImageDiv" style="display:none" runat="server" pollid="">
                        <h6>Upload a picture for your Poll</h6>
                        <div class="">
                               <div class="file-field input-field">
                                  <div class="btn-large">
                                    <span>File</span>
                                    <ajaxToolkit:AsyncFileUpload ID="fuImage" runat="server" MaximumNumberOfFiles="1" OnUploadedComplete="fuImage_UploadedComplete" OnClientUploadComplete="showimagepreview" ThrobberID="Loader"/>
                                  </div>
                                  <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" runat="server" id="fileUploadName"/>
                                  </div>
                                    <asp:Label ID="Loader" runat="server" Style="display: none">
                                        <img src="/polls/Images/loader.gif" align="absmiddle" alt="loading" />
                                    </asp:Label>
                                </div>
                        </div>
                        <div class="recom black-text" id="imageheightandwidth" runat="server">
                            Only image files of format JPG, GIF, and PNG are allowed.
                            <br />&nbsp;
                        </div>
                    </div>
                    
                    <hr style="border-style:dotted; display:none;" id="imageDivBorder" runat="server"/>
                    
                    <div class="row nopad nomar">
                        <h6>Poll Question (Maximum 100 characters)</h6>
                        <div class="row nopad">
                            <div class="col s12 nopad">
                                <asp:TextBox ID="txtQuestion" runat="server" onkeypress="return CkKeyPress(event);" MaxLength="100" placeholder="How is..." length="100"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    <div class="row nopad nomar">
                       <h6>Answer Options (Maximum 50 characters)</h6>
                       <div class="row nopad nomar">
                            <asp:Repeater ID="addRemoveControls" runat="server">
                                <HeaderTemplate>
                                    <table id="pollansweroptions" style="border-spacing:0px;padding:0 0 0 0;" class="nopad">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="txtRow col m12 s12 nopad">
                                        <td class="col s9 nopad">
                                            <input type="hidden" id="hdnAnswerId" value='<%# Eval("AnswerId") %>' />
                                            <input placeholder="Your Answer<%# (Convert.ToInt32(Eval("AnswerId")) + 1 ).ToString() %>" type="text" id="txtAnswerOption_<%# Eval("AnswerId") %>" name="txtAnswerOption_<%# Eval("AnswerId") %>" onkeyup="javascript:CkKeyPress(event);" maxlength="50" value='<%# DataBinder.Eval(Container.DataItem, "Answer") %>' class="txtAns" length="50"/>
                                        </td>
                                        <td class="input-field col s3 nopad">
                                             <a href="javascript:;" id="btnRemoveAnother_<%# Eval("AnswerId") %>" onclick="javascript:removeAnswerOption(event);">
                                                <i class="material-icons pin-bottom black-text">delete</i>
                                            </a>
                                            <a href="javascript:;" id="btnAddAnother_<%# Eval("AnswerId") %>" onclick="javascript:addAnswerOption(event);">
                                                <i class="material-icons black-text">add</i>
                                            </a>                            
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div> 
                    <div class="row">
                        <div id="answererr" style="display:none" ><asp:Label ID="lblAlreadyExist" CssClass="lblRequired" runat="server"></asp:Label></div>
                        <div>
                            <div id="answeroptions" runat="server"></div>
                        </div>
                        <div><asp:Panel ID="pnlAnswerOptions" runat="server"></asp:Panel></div>          
                    </div>
                    
                      <div class="row nopad">
                        <h6>Vote button label</h6>
                        <div class="col s12 nopad">
                            <asp:TextBox ID="txtVoteButtonLabel" Text="Vote Now" runat="server" MaxLength="20" placeholder="Vote Now" length="20"/>
                        </div>
                    </div>
                                  
                    <div class="row nopad">
                        <h6>Poll voting rule is to allow only one answer to be selected</h6>
                        <span>
                            <asp:RadioButton ID="radSingleAnswer" Text="Allow Single Answer" GroupName="AnswerType" runat="server" />
                        </span>
                        &nbsp;&nbsp;&nbsp;
                        <span>
                            <asp:RadioButton ID="radMultipleAnswer" Text="Allow Multiple Answers" GroupName="AnswerType" runat="server" />
                        </span>
                    </div>
                    
                    
                    <div class="row nopad">
                        <h6>Want to ask users to give Comments on the Poll?*</h6>
                        <div class="col s12 nopad">
                            <asp:CheckBox ID="chkComments" onclick="showCommentbox()" runat="server" Text="" />
                            <label for="MainContent_chkComments">Add Comment Box</label>
                        </div>
                    </div>
                    <div class="row nopad">
                        <h6>Restrict multiple answers from the same computer</h6>
                        <div class="col s12 nopad">
                            <asp:CheckBox ID="ckbSingleVote" Checked="false" Text="" runat="server" />
                            <label for="MainContent_ckbSingleVote">Allow only one vote per computer</label>

                        </div>
                    </div>
</div>
            
         <div class="card-panel col m12">
                <div class="card-title"><h5>Display to Poll respondents - after Voting</h5></div>                    

                    <div class="row nopad">
                        <h6>You have TWO options</h6>
                        <div class="col s12 nopad">
                            <ul class="" style="border:1px solid #ccc;">
                                <li id="showpollresults" onclick="pollResultsOption('results')">
                                  <div class="collapsible-header" id="resultsheader" onclick="pollResultsOption('results')">
                                      <asp:RadioButton ID="radShowResults" GroupName="ResultsDisplay" runat="server" Text="Show Poll results" onclick="pollResultsOption('results')"/>
                                  </div>
                                    <div class="collapsible-body blue-text col m12 grey lighten-4 pad-10" style="border: 1px solid #ccc;">
                                        <div  class="row" style="padding-left:50px !important;">
                                            <asp:CheckBox ID="ckbVotes" onclick="voteShow()"  CssClass="chek" Text="Show Votes Count" runat="server" />
                                        </div>
                                    </div>
                                </li>
                                 <li id="showleadgen" onclick="pollResultsOption('leadgen')">
                                  <div class="collapsible-header" id="leadgenheader"><asp:RadioButton ID="radLeadGen" GroupName="ResultsDisplay" runat="server" Text="Generate Leads and/or Promote Content" /></div>
                                  <div class="collapsible-body col m12 grey lighten-4" style="border: 1px solid #ccc;padding-bottom:5px;">
                                      <div class="row">
                                          <div class="left-align">
                                              <h6>
                                                Communicate with Poll respondents to get Leads
                                              </h6>
                                              <div id="leadgentooltipcontent">
                                                  <ul class="grey lighten-4" style="border:none;margin-left:20px;">
                                                      <li class="grey lighten-4" style="border:none;font-size:11px;list-style-type:disc;padding:0;font-style:italic;">
                                                          Invite users to Your/Sponsor's Promos, Offers, Contests, Sweepstakes
                                                      </li>
                                                      <li class="grey lighten-4" style="border:none;font-size:11px;list-style-type:disc;padding:0;font-style:italic;">
                                                          Seek their contacts for Leads or Referrals
                                                      </li>
                                                      <li class="grey lighten-4" style="border:none;font-size:11px;list-style-type:disc;padding:0;font-style:italic;">
                                                        Write any content and/or redirect users to Your/Sponsor's website or weblink
                                                      </li>
                                                  </ul>
                                                </div>
                                          </div>
                                      </div>
                                          
                                        <div class="row">

                                            <div class="input-field">
                                                <h6 id="placeholderinvitationmessage" style="display:block;">Message to Poll respondents after voting 
                                                </h6> 
                                                      <span class="grey lighten-4" style="border:none;font-size:11px;list-style-type:disc;padding:0;font-style:italic;">
                                                        Uncheck to put different messages for each answer option</span>
                                                    <p class="nopad" id="psameanswer">
                                                        <asp:CheckBox ID="chkSameAnswer" runat="server" Checked="true" Text="Use same message for all Answer Options" onclick="showThankYouMessages()"/>
                                                    </p>
                                                    <br />
                                                    <asp:TextBox ID="txtInvitationMessage" runat="server" Text="" length="200" MaxLength="200" placeholder="Thank you for your vote"></asp:TextBox>
                                                    <div class="row card" id="thankyoumessagesdiv" style="padding:2%;">
                                                        <asp:Repeater ID="addThankYouOptions" runat="server">
                                                            <HeaderTemplate>
                                                                <table id="pollanswerthankyouoptions" style="border-spacing:0px;padding:0 0 0 0;" class="nopad">
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr class="col m12 s12 nopad" id="trheader_ThankYouOption_<%# Eval("AnswerId") %>">
                                                                    <td class="col s9 nopad small">
                                                                        Option - <span id="lblThankYouOption_<%# Eval("AnswerId") %>"><%# DataBinder.Eval(Container.DataItem, "Answer") %></span>
                                                                    </td>
                                                                </tr>
                                                                <tr class="txtRow col m12 s12 nopad" id="trbody_ThankYouOption_<%# Eval("AnswerId") %>">
                                                                    <td class="col s9 nopad">
                                                                        <input placeholder="Add your custom message here" type="text" id="txtThankYouOption_<%# Eval("AnswerId") %>" name="txtThankYouOption_<%# Eval("AnswerId") %>" maxlength="100" value='<%# Eval("InvitationThankYouMessage") %>' length="100" onkeyup="updateThankYouOption('<%# Eval("AnswerId") %>')"/>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <h6>Seek Poll respondent's Contact info</h6>
                                            <p class="nopad"><asp:CheckBox ID="chkPhone" runat="server" Text="Ask Mobile Number" onclick="showContactOptions()"/></p>
                                            <p class="nopad"><asp:CheckBox ID="chkEmail" runat="server" Text="Ask Email Address" onclick="showContactOptions()"/></p>
                                        </div>
                                      <hr />
                                        <div class="row">
                                            <p class="nopad"><asp:CheckBox ID="ckbSubmit" runat="server" Text="Show Submit button" Checked="false" onclick="showSubmitButtonOptions()" /></p>
                                        </div>
                                      <div id="submitbuttondiv" style="display:none;padding-top:10px;" class="card col m12">
                                           <div class="input-field" style="clear:both;">
                                                <label id="placeholdersubmitbuttonlabel" style="display:block;" for="MainContent_txtSubmitButtonLabel"><h6>Customize Submit button label</h6></label>
                                                <asp:TextBox ID="txtSubmitButtonLabel" runat="server" Text="Submit Now" length="20" MaxLength="20"></asp:TextBox>
                                            </div>
                                           <div class="input-field">
                                                <label id="placeholderredirecturl" style="display:block;" for="MainContent_txtRedirectURL">Redirect URL</label>
                                                <asp:TextBox ID="txtRedirectURL" runat="server" Text="I'm In" length="200" MaxLength="200"></asp:TextBox>
                                            </div>
                                      </div>
                                      <div id="thankyoumessagediv">
                                           <div class="input-field" style="clear:both;">
                                                <h6 id="placeholderthankyouoverlay" style="display:block;"><h6>Message display upon Submit</h6></h6>
                                                <asp:TextBox ID="txtResultText" runat="server" Text="Thank you!" length="50" MaxLength="50" Width="300" placeholder="Thank you!"></asp:TextBox>
                                                <a href="javascript:void(0)" onclick="showOverlay()" id="overlayshow">Show</a>
                                            </div>
                                      </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
         </div>
</ContentTemplate></asp:UpdatePanel>

<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="designUpdatePanel" ChildrenAsTriggers="false" RenderMode="Inline"> 
               <Triggers>
                   <asp:AsyncPostBackTrigger ControlID="CreateNew" EventName="Click" />
                   <asp:AsyncPostBackTrigger ControlID="DesignNew" EventName="Click" />
                   <asp:AsyncPostBackTrigger ControlID="LaunchNew" EventName="Click" />
                   <asp:AsyncPostBackTrigger ControlID="SaveAndClose" EventName="Click" />
                   <asp:AsyncPostBackTrigger ControlID="ddlQuestionFont" EventName="SelectedIndexChanged" />
                   
               </Triggers>
    <ContentTemplate>
              <div id="design" runat="server" class="col s12 m12 mCustomScrollbar">

                    <div class="row"></div>
                    <div id="designerror" style="display:none"><asp:Label ID="lblDesignError"  CssClass="lblRequired" runat="server"></asp:Label></div>
         <div class="card-panel col m12">
<div class="card-title"><h5>Poll Dimensions</h5></div>
                    <div class="row">
                        <div class="col m12 s12 row">
                            <input type="checkbox" ID="ckbAutoAdjustDesign" runat="server" class="input-field" onclick="autoAdjust('design');"/> 
                            <label for="MainContent_ckbAutoAdjustDesign">Auto-adjust height of Poll</label>
                        </div>
                        <div class="col s4">
                            <label for="MainContent_txtWidthPoll">Width (px)</label>
                            <div class="input-field nomar">
                                <asp:TextBox ID="txtWidthIframe" onkeypress="return CkKeyPress(event);" CssClass="heighttxt" onchange="iframewidthset();renderChart();" Text="300"  runat="server" MaxLength="3"></asp:TextBox>
                            </div>
                        </div>
                        <div class=" col s4">
                            <label for="MainContent_txtHeightPoll">Height (px)</label>
                            <div class="input-field nomar">
                                <asp:TextBox ID="txtHeightIframe" onkeypress="return CkKeyPress(event);" CssClass="heighttxt" onchange="iframeheightset();setOverallPreviewHeight1();renderChart();" runat="server" MaxLength="3"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col s4">
                            <label>* Recommended </label>
                             <div class="input-field">
                            <asp:Label ID="lblWidth" runat="server" ></asp:Label> * <asp:Label ID="lblHeight" runat="server" ></asp:Label>
                            </div>
                        </div>

                    </div>
        </div>           
         <div class="card-panel col m12">
<div class="card-title"><h5>Background Colours and Fonts</h5></div>
                    <div class="row nopad">                   
                        <div class="col m12 s12 nopad">
                            <div class="col m6 nopad">
                                <h6>Poll Background</h6>
                                <input id="txtPollBgColor" runat="server"  onkeypress="return CkKeyPress(event);" style="width:70% !important" class="spectrumbutton"/>
                            </div>
                        </div> 
                   </div> 
                    
                    
                    <div class="row nopad">
                        <div class="col m12 s12 nopad">
                            <div class="col m6 nopad">
                                <h6>Question Background</h6>
                                <input id="txtQuestionBgColor" runat="server" onkeypress="return CkKeyPress(event);" style="width:70% !important;"/>
                            </div>
                            <div class="col m6">
                                <h6>Question Color</h6>
                                <input id="txtQuestionColor" runat="server"  onkeypress="return CkKeyPress(event);" style="width:70% !important;"/>
                            </div>
                        </div>
                        <div class="col m12 s12" style="background-color:#ECECEC !important;">
                            <div class="col m12 s12">
                                <div class="fonttype col m5 s5">
                                    <asp:DropDownList ID="ddlQuestionFont" runat="server" Font-Size="Small" CssClass="browser-default black-text" Height="30" BackColor="#ECECEC">
                                           <asp:ListItem Text="Arial" Value="Arial"></asp:ListItem>
                                           <asp:ListItem Text="Andale Mono" Value="Andale Mono"></asp:ListItem>
                                           <asp:ListItem Text="Arial Black" Value="Arial Black"></asp:ListItem>
                                           <asp:ListItem Text="Book Antique" Value="Book Antique"></asp:ListItem>
                                           <asp:ListItem Text="Comic SanSMS" Value="Comic SanSMS"></asp:ListItem>
                                           <asp:ListItem Text="Courier New" Value="Courier New"></asp:ListItem>
                                           <asp:ListItem Text="Georgia" Value="Georgia"></asp:ListItem>
                                           <asp:ListItem Text="Helvetica" Value="Helvetica"></asp:ListItem>
                                           <asp:ListItem Text="Impact" Value="Impact"></asp:ListItem>
                                           <asp:ListItem Text="Tahoma" Value="Tahoma"></asp:ListItem>
                                           <asp:ListItem Text="Terminal" Value="Terminal"></asp:ListItem>
                                           <asp:ListItem Text="Trebuchet MS" Value="Trebuchet MS"></asp:ListItem>
                                           <asp:ListItem Text="Sans serif" Value="Sans serif"></asp:ListItem>
                                           <asp:ListItem Text="Verdana" Value="Verdana"></asp:ListItem>
                                           <asp:ListItem Text="Times New Roman" Value="Times New Roman"></asp:ListItem>
                                     </asp:DropDownList>
                               </div>
                               <div class="fonticons col s2">
                                   <div class="col m12 s12">
                                       <button class="btnfontsize">
                                           <div class="ddl" id="questionfontsizediv"> 
                                                <img src="App_Themes/fontstyles/t_inactive.png" id="imgFontSize"  runat="server"/>
                                            </div>
                                       </button>
                                    </div>
                                   <div class="col m12 s12">
                                        <div id="fontsize" class="divalignfonsizediv" style="color:black !important;display:none;">
                                            <div class="divpaddingicons" id="questiontext">
                                                <div class="mfontnormal">8</div>
                                                <div class="mfontnormal">10</div>
                                                <div class="mfontnormal">12</div>
                                                <div class="mfontnormal">14</div>
                                                <div class="mfontnormal">16</div>
                                                <div class="mfontnormal">18</div>
                                                <div class="mfontnormal">20</div>
                                                <div class="mfontnormal">22</div>
                                                <div class="mfontnormal">24</div>
                                                <div class="mfontnormal">26</div>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class="fonticons col s1">
                                   <img src="App_Themes/fontstyles/bold_inactve.png" id="imgBold" onclick="questionboldactive()" runat="server" />
                                   <img src="App_Themes/fontstyles/bold_active.png" id="imgBoldActive" onclick="questionboldinactive()" runat="server" />
                               </div>
                               <div class="fonticons col s1 ">
                                   <img src="App_Themes/fontstyles/Italic_inactive.png" id="imgItalic" onclick="questionitalicactive()" runat="server" />
                                   <img src="App_Themes/fontstyles/Italic_active.png" id="imgItalicActive" onclick="questionitalicinactive()" runat="server" />
                               </div>
                               <div class="fonticons col s1">
                                  <img src="App_Themes/fontstyles/underline_inactive.png" id="imgUnderline" onclick="questionulactive()" runat="server" />
                                  <img src="App_Themes/fontstyles/underline_active.png" id="imgUnderlineActive" onclick="questionulinactive()" runat="server" />
                               </div>
                                <div class="fonticons col s1">
                                   <div class="col m12 s12">
                                        <button class="btnquestionalign">
                                            <div class="ddl">
                                                <img src="App_Themes/fontstyles/left_inactive.png" id="qleftinactive" runat="server" />
                                                <img src="App_Themes/fontstyles/left_active.png" id="qleftactive"  runat="server"/>
                                                <img src="App_Themes/fontstyles/right_active.png"  id="qrightactive" runat="server"/>
                                                <img src="App_Themes/fontstyles/center_active.png" id="qcenteractive"  runat="server"/>
                                            </div>

                                        </button>
                                    </div>
                                    <div class="col m12 s12">
                                        <div id="questionalign" class="divalignstyles" style="display:none;">
                                            <div class="divpaddingicons">
                                                <div>
                                                    <img src="App_Themes/fontstyles/right_active.png" id="rightactive" onclick="questionalignright()" />
                                                    <img src="App_Themes/fontstyles/right_inactive.png" id="rightinactive" onclick="questionalignright()" />
                                                </div>
                                                <div>
                                                    <img src="App_Themes/fontstyles/center_active.png" id="centeractive" onclick="questionaligncenter()" />
                                                    <img src="App_Themes/fontstyles/center_inactive.png" id="centerinactive" onclick="questionaligncenter()" />
                                                </div>
                                                <div>
                                                    <img src="App_Themes/fontstyles/left_active.png" id="leftactive" onclick="questionalignleft()" />
                                                    <img src="App_Themes/fontstyles/left_inactive.png" id="leftinactive" onclick="questionalignleft()" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
</div></div></div>
                    </div>      
                    
                   
                    <div class="row nopad">
                        <div class="col m12 s12 nopad">
                            <div class="col m6 nopad">
                               <h6> Answer Color</h6>
                                <input id="txtAnswerColor" runat="server"  onkeypress="return CkKeyPress(event);" style="width:70% !important" />
                            </div>
                        </div>
                        <div class="col m12 s12" style="background-color:#ECECEC !important;">
                            <div class="col m12 s12">
                                <div class="fonttype col m5 s5">
                                    <asp:DropDownList ID="ddlAnswerFont" onkeypress = "return CkKeyPress(event);" runat="server" Font-Size="Small" CssClass="browser-default black-text" Height="30" BackColor="#ECECEC">
                                            <asp:ListItem Text="Arial" Value="Arial"></asp:ListItem>
                                            <asp:ListItem Text="Andale Mono" Value="Andale Mono"></asp:ListItem>
                                            <asp:ListItem Text="Arial Black" Value="Arial Black"></asp:ListItem>
                                            <asp:ListItem Text="Book Antique" Value="Book Antique"></asp:ListItem>
                                            <asp:ListItem Text="Comic SanSMS" Value="Comic SanSMS"></asp:ListItem>
                                            <asp:ListItem Text="Courier New" Value="Courier New"></asp:ListItem>
                                            <asp:ListItem Text="Georgia" Value="Georgia"></asp:ListItem>
                                            <asp:ListItem Text="Helvetica" Value="Helvetica"></asp:ListItem>
                                            <asp:ListItem Text="Impact" Value="Impact"></asp:ListItem>
                                            <asp:ListItem Text="Tahoma" Value="Tahoma"></asp:ListItem>
                                            <asp:ListItem Text="Terminal" Value="Terminal"></asp:ListItem>
                                            <asp:ListItem Text="Trebuchet MS" Value="Trebuchet MS"></asp:ListItem>
                                            <asp:ListItem Text="Sans serif" Value="Sans serif"></asp:ListItem>
                                            <asp:ListItem Text="Verdana" Value="Verdana"></asp:ListItem>
                                            <asp:ListItem Text="Times New Roman" Value="Times New Roman"></asp:ListItem>
                                        </asp:DropDownList>
                                </div>
                                <div class="fonticons col m2 s2">
                                    <div class="col m12 s12">
                                        <button class="btnafontsize"><div class="ddl" id="answerfontsizediv"> 
                                        <img src="App_Themes/fontstyles/t_inactive.png" id="imgAnswerfs"  runat="server"/></div></button>
                                    </div>
                                    <div class="col m12 s12">
                                        <div id="afontsize" class="divaalignfonsizediv" style="display:none;color:black !important;">
                                            <div class="divpaddingicons" id="answertext">
                                                <div class="mfontnormal">8</div>
                                                <div class="mfontnormal">10</div>
                                                <div class="mfontnormal">12</div>
                                                <div class="mfontnormal">14</div>
                                                <div class="mfontnormal">16</div>
                                                <div class="mfontnormal">18</div>
                                                <div class="mfontnormal">20</div>
                                                <div class="mfontnormal">22</div>
                                                <div class="mfontnormal">24</div>
                                                <div class="mfontnormal">26</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="fonticons col m1 s1">
                                    <img src="App_Themes/fontstyles/bold_inactve.png" id="imgABold" onclick="answerboldactive()" runat="server" />
                                    <img src="App_Themes/fontstyles/bold_active.png" id="imgABoldActive" onclick="answerboldinactive()" runat="server" />
                                </div>
                                <div class="fonticons col m1 s1">
                                    <img src="App_Themes/fontstyles/Italic_inactive.png" id="imgAItalic" onclick="answeritalicactive()" runat="server" />
                                    <img src="App_Themes/fontstyles/Italic_active.png" id="imgAItalicActive" onclick="answeritalicinactive()" runat="server" />
                                </div>
                                <div class="fonticons col m1 s1">
                                    <img src="App_Themes/fontstyles/underline_inactive.png" id="imgAUL" onclick="answerulactive()" runat="server" />
                                    <img src="App_Themes/fontstyles/underline_active.png" id="imgAULActive" onclick="answerulinactive()" runat="server" />
                                </div>
                                <div class="fonticons col m1 s1">
                                    <div class="col m12 s12" id="divansweralignment">
                                        <button class="btnaalignment" id="btnansweralignment">
                                            <div class="ddl">
                                                <img src="App_Themes/fontstyles/left_inactive.png" id="aleftinactive" runat="server" />
                                                <img src="App_Themes/fontstyles/left_active.png" id="aleftactive"  runat="server"/>
                                                <img src="App_Themes/fontstyles/right_active.png"  id="arightactive" runat="server"/>
                                                <img src="App_Themes/fontstyles/center_active.png" id="acenteractive"  runat="server"/>
                                            </div>

                                        </button>
                                    </div>
                                    <div class="col m12 s12">
                                        <div id="answeralign" class="divaalignstyles" style="display:none;">
                                            <div class="divpaddingicons">
                                                <div>
                                                    <img src="App_Themes/fontstyles/right_active.png" id="answerrightactive" onclick="answeralignright()" />
                                                    <img src="App_Themes/fontstyles/right_inactive.png" id="answerrightinactive" onclick="answeralignright()" />
                                                </div>
                                                <div>
                                                    <img src="App_Themes/fontstyles/center_active.png" id="answercenteractive" onclick="answeraligncenter()" />
                                                    <img src="App_Themes/fontstyles/center_inactive.png" id="answercenterinactive" onclick="answeraligncenter()" />
                                                </div>
                                                <div>
                                                    <img src="App_Themes/fontstyles/left_active.png" id="answerleftactive" onclick="answeralignleft()" />
                                                    <img src="App_Themes/fontstyles/left_inactive.png" id="answerleftinactive" onclick="answeralignleft()" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                                                     

                            </div>
                        </div>
                        
                    </div>
                    
                   
                    <div class="row nopad">
                        <div class="col m12 s12 nopad">
                            <div class="col m6 nopad">
                                <h6>Vote Background</h6>
                                <input id="txtVoteBgColor" runat="server"  onkeypress = "return CkKeyPress(event);" style="width:70% !important;"/>
                            </div>
                            <div class="col m6 nopad">
                                <h6>Vote Colour</h6>
                                <input ID="txtVoteColor" runat="server"  onkeypress = "return CkKeyPress(event);" style="width:70% !important;"/>
                            </div>
                        </div>
                        <div class="col m12 s12" style="background-color:#ECECEC !important;">
                            <div class="col m12 s12">
                                <div class="fonttype col m5 s5">
                                    <asp:DropDownList ID="ddlVoteFont" onkeypress = "return CkKeyPress(event);" runat="server" Font-Size="Small" CssClass="browser-default black-text" Height="30" BackColor="#ECECEC">
                                            <asp:ListItem Text="Arial" Value="Arial"></asp:ListItem>
                                            <asp:ListItem Text="Andale Mono" Value="Andale Mono"></asp:ListItem>
                                            <asp:ListItem Text="Arial Black" Value="Arial Black"></asp:ListItem>
                                            <asp:ListItem Text="Book Antique" Value="Book Antique"></asp:ListItem>
                                            <asp:ListItem Text="Comic SanSMS" Value="Comic SanSMS"></asp:ListItem>
                                            <asp:ListItem Text="Courier New" Value="Courier New"></asp:ListItem>
                                            <asp:ListItem Text="Georgia" Value="Georgia"></asp:ListItem>
                                            <asp:ListItem Text="Helvetica" Value="Helvetica"></asp:ListItem>
                                            <asp:ListItem Text="Impact" Value="Impact"></asp:ListItem>
                                            <asp:ListItem Text="Tahoma" Value="Tahoma"></asp:ListItem>
                                            <asp:ListItem Text="Terminal" Value="Terminal"></asp:ListItem>
                                            <asp:ListItem Text="Trebuchet MS" Value="Trebuchet MS"></asp:ListItem>
                                            <asp:ListItem Text="Sans serif" Value="Sans serif"></asp:ListItem>
                                            <asp:ListItem Text="Verdana" Value="Verdana"></asp:ListItem>
                                            <asp:ListItem Text="Times New Roman" Value="Times New Roman"></asp:ListItem>
                                        </asp:DropDownList>
                                </div>
                               
                                <div class="fonticons col m2 s2">
                                    <div class="col m12 s12">
                                        <button class="btnvfontsize"><div class="ddl" id="votefontsizediv"> 
                                        <img src="App_Themes/fontstyles/t_inactive.png" id="imgVoteFs"  runat="server"/></div></button>
                                    </div>               
                                    <div class="col m12 s12">
                                        <div id="vfontsize" class="divvalignfonsizediv" style="display:none;color:black !important;">
                                            <div class="divpaddingicons" id="votetext">
                                                <div class="mfontnormal">8</div>
                                                <div class="mfontnormal">10</div>
                                                <div class="mfontnormal">12</div>
                                                <div class="mfontnormal">14</div>
                                                <div class="mfontnormal">16</div>
                                                <div class="mfontnormal">18</div>
                                                <div class="mfontnormal">20</div>
                                                <div class="mfontnormal">22</div>
                                                <div class="mfontnormal">24</div>
                                                <div class="mfontnormal">26</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="fonticons col m1 s1">
                                    <img src="App_Themes/fontstyles/bold_inactve.png" id="imgVBold" onclick="voteboldactive()" runat="server" />
                                    <img src="App_Themes/fontstyles/bold_active.png" id="imgVBoldActive" onclick="voteboldinactive()" runat="server" />
                                </div>
                                <div class="fonticons col m1 s1">
                                    <img src="App_Themes/fontstyles/Italic_inactive.png" id="imgVItalic" onclick="voteitalicactive()" runat="server" />
                                    <img src="App_Themes/fontstyles/Italic_active.png" id="imgVItalicActive" onclick="voteitalicinactive()" runat="server" />
                                </div>
                                <div class="fonticons col m1 s1">
                                    <img src="App_Themes/fontstyles/underline_inactive.png" id="imgVUL" onclick="voteulactive()" runat="server" />
                                    <img src="App_Themes/fontstyles/underline_active.png" id="imgVULActive" onclick="voteulinactive()" runat="server" />
                                </div>
                                <div class="fonticons col m1 s1">
                       <div class="col m12 s12">
                          <button class="btnvalignment"><div class="ddl"><img src="App_Themes/fontstyles/left_inactive.png" id="vleftinactive" runat="server" />
                          <img src="App_Themes/fontstyles/left_active.png" id="vleftactive"  runat="server"/>
                          <img src="App_Themes/fontstyles/right_active.png"  id="vrightactive" runat="server"/>
                          <img src="App_Themes/fontstyles/center_active.png" id="vcenteractive"  runat="server"/></div></button>
                       </div>
                       <div class="col m12 s12">
                            <div id="votealign" class="divvalignstyles" style="display:none;">
                                <div class="divpaddingicons">
                                    <div>
                                        <img src="App_Themes/fontstyles/right_active.png" id="voterightactive" onclick="votealignright()" />
                                        <img src="App_Themes/fontstyles/right_inactive.png" id="voterightinactive" onclick="votealignright()" />
                                    </div>
                                    <div>
                                        <img src="App_Themes/fontstyles/center_active.png" id="votecenteractive" onclick="votealigncenter()" />
                                        <img src="App_Themes/fontstyles/center_inactive.png" id="votecenterinactive" onclick="votealigncenter()" />
                                    </div>
                                    <div>
                                        <img src="App_Themes/fontstyles/left_active.png" id="voteleftactive" onclick="votealignleft()" />
                                        <img src="App_Themes/fontstyles/left_inactive.png" id="voteleftinactive" onclick="votealignleft()" />
                                    </div>
                                </div>
                            </div>
                       </div>
                   </div>
                            </div>
                        </div>
                        
                        
                       </div>     
</div>            
         <div class="card-panel col m12">
<div class="card-title"><h5>Poll Branding</h5></div>
           <div  class="question"><asp:CheckBox ID="ckbInsightoBrand"  onclick="showPoweredBy()"  CssClass="chek" Text="Remove Insighto Branding" runat="server" />
           </div>

           <div  id="sponsorshow" runat="server">
             <div class="question"><asp:CheckBox ID="ckbAddSponsor" CssClass="chek" onclick="showSponsorDetails()" Text="Add Poll Sponsor" runat="server" />
           </div>
           <div id="SponsorBox" style="display:none" runat="server" class="input-field">
               <div class="col m12">
                   <h6>Customise Sponsor Text</h6>
                <div class="input-field">
                    <asp:TextBox ID="txtSponsorText" runat="server" Text="Sponsored By"  maxlength="25" length="25"></asp:TextBox>
               </div>
                   </div>
               
               <div class="col m12">
                   <h6>Sponsor Logo</h6>
                   <div>
                               <div class="file-field input-field">
                                  <div class="btn-large">
                                    <span>File</span>
                                    <ajaxToolkit:AsyncFileUpload ID="sponsorLogo" runat="server" MaximumNumberOfFiles="1" OnUploadedComplete="sponsorLogo_UploadedComplete" OnClientUploadComplete="showlogopreview" ThrobberID="Throbber2" />
                                  </div>
                                  <div class="file-path-wrapper" id="sponsorfilepath">
                                    <input class="file-path validate" type="text" runat="server" id="lblLogoName" value="Select Sponsor Logo" style="display:block;"/>

                                  </div>
                                    <asp:Label ID="Throbber2" runat="server" Style="display: none">
                                        <img src="/polls/Images/loader.gif" align="absmiddle" alt="loading" />
                                    </asp:Label>

                                </div>
               </div></div>
               
               <div class="col m12">
                   <h6>Attach a weblink to above logo</h6>
                <div class="input-field">
                    <asp:TextBox ID="txtSponsorLink" Text="https://" runat="server" MaxLength="200" length="200"></asp:TextBox>
               </div>
               </div>
               
           </div></div>
</div>           
                  <div class="clear"></div>
              <div class="card-panel col m12">
<div class="card-title"><h5>Poll Social Sharing Options</h5></div>
           <div class="question" style="color:white;"><h6>Share on</h6></div>
           <div>
            <div>
            <asp:TextBox ID="txtSharePoll" placeholder="Your Message Here"  Text="" runat="server" MaxLength="25" length="25"></asp:TextBox>
           </div>
           <div class="clear"></div>
           <div class="fullwidth floatL"><div class="floatL"><div class="floatL"></div>
           <div class="floatL socialmargin">
               <p>
                   <asp:CheckBox ID="ckbGoogle" onclick="javascript:showGoogletxt();renderChart();" runat="server" />
                   <label for="MainContent_ckbGoogle"><img src="App_Themes/images/google.png" alt="Google" /></label>
               </p>
           </div>
           </div>
           </div>
           <div class="fullwidth floatL">
           <div class="reportviewbasics floatL"><div class="floatL">
           </div>
           <div class="floatL socialmargin">
               <p>
                   <asp:CheckBox ID="ckbFb" onclick="javascript:showFbtxt();renderChart();" runat="server" />
                   <label for="MainContent_ckbFb"><img src="App_Themes/images/facebook.png" alt="Facebook" /></label>
               </p>
                <div id="fb" runat="server" class="floatL" style="display:none"></div>
               </div>
           </div>
           </div>

           <div class="fullwidth floatL">
           <div class="reportviewbasics floatL"><div class="floatL"></div>
           <div class="col m12" style="padding: 0px 5px;">
                   <asp:CheckBox ID="ckbTwitter" onclick="javascript:showTwittertxt();renderChart();" runat="server" />
                   <label for="MainContent_ckbTwitter"><img src="App_Themes/images/twitter.png" alt="Twitter" /></label>
               </div>
               
           <div id="twitter" runat="server" class="col m12" style="display:none">
           <div class="input-field nomar">
               <asp:TextBox ID="txtTwitter" runat="server" Visible="false" style="display:none;" ></asp:TextBox>
               </div>
               </div>
           </div>
           </div> 
               
           </div>
            </div>
</div>
            <script>
                var answersize = $("#MainContent_hdnAnswerFSize").val();
                var votesize = $("#MainContent_hdnVoteFSize").val();
                var msgsize = $("#MainContent_hdnMsgFSize").val();
                var questsize = $("#MainContent_hdnMsgFSize").val();
                $("#questionfontsizediv").html(questsize);
                $("#votefontsizediv").html(votesize);
                $("#answerfontsizediv").html(answersize);
            </script>
</ContentTemplate></asp:UpdatePanel>
                
    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="launchUpdatePanel" ChildrenAsTriggers="false">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="CreateNew" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="DesignNew" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="LaunchNew" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnActivate" EventName="Click"/>
            <asp:AsyncPostBackTrigger ControlID="btnActivatePoll" EventName="Click"/>
            <asp:AsyncPostBackTrigger ControlID="SaveAndClose" EventName="Click" />
        </Triggers>
    <ContentTemplate>
            <div id="launch" class="col s12 m12" runat="server">
                    <div class="row">
                        <h5><small>You are all set to launch your poll.</small></h5>
                        <p>
                            <big>
                            Activate and launch your poll to your audience as a <span class="blue-text">web link</span> on Twitter,
                            Whatsapp, or as a post on LinkedIn, Facebook or any such media. If you own a web site, you can also launch your poll
                            as an <span class="blue-text">Embed</span> or as a <span class="blue-text">Slider</span> poll.
                            </big>
                        </p>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 right-align">
                            <div class="input-field col s7 right-align">
                            </div>
                            <div class="input-field col s5 right-align">
                                <asp:LinkButton ID="btnActivatePoll" runat="server" CausesValidation="false" OnClick="btnActivate_Click" CssClass="modal-action modal-close waves-effect waves-green btn-large green white-text" Text="Activate Poll" EnableViewState="false" ViewStateMode="Disabled"></asp:LinkButton>
                                <input type="button" ID="btnActivateParentPoll" class="btn-large white-text right-align" value="Activate poll" runat="server" onclick="launchReplacedPoll()"/>
                            </div>
                         <div class="input-field col s12 left-align">
                        
                            </div>
                        </div>
                    </div>                
                </div>

</ContentTemplate></asp:UpdatePanel>
            
                <div id="launchafter" runat="server" style="display:none;">
 <div class="row" id="launchoptions" style="display:none;">
    <h4 class="truncate green-text">Congrats, your poll is now active!</h4>
    <div class="col s12 nopad launch-tabs">
    <div class="card white lighten-5">
    <div class="col s12 nopad">
      <ul class="tabs gray lighten-5" id="launchtabs">
        <li class="tab col s3"><a class="active" href="#web" id="hrefweb">Web Link </a></li>
        <li class="tab col s3"><a  href="#embed" id="hrefembed">Embed Poll </a></li>
        <li class="tab col s3"><a href="#slider" id="hrefslider">Slider Poll</a></li>
      </ul>
    </div>
    <div id="web" class="col s12 active">
    <div class="row">
    <p>Use the link below to share this poll on your social media or by e-mail. You can even send the link on Whatsapp or as an SMS message to your customers. The Poll will be presented responsively i.e based on the device they open the poll in.</p>
      <div class="row">
            <div class="col m12 s12 card">
                <div class="col m12 s12 left left-align"><h6>Web Link Background</h6></div>
                <div class="col m12 s12 left left-align"><input id="txtWebLinkBgColor" runat="server"  onkeypress="return CkKeyPress(event);" style="width:40% !important" class="spectrumbutton"/></div>
            </div>
            <div class="col m12 s12 card">
            <div class="col m12 s12 left left-align"><h6>Web Link</h6></div>
            <p><asp:TextBox ID="txtWebLink" Width="90%" Height="80" CssClass="iframe" runat="server" TextMode="MultiLine"></asp:TextBox></p>
            <p class="right-align" style="width:90%;">
                <a class="waves-effect waves-light" onclick="previewWebLink();">View Poll</a>
                &nbsp;
                <a class="waves-effect waves-light" id="copyweblink" href="javascript:void(0)" onclick="copyWebLinkToClipboard()">Copy to Clipboard</a>
            </p>
          <div class="col m12 s12" style="padding:0;">
              <ul class="list-inline col m6 s6 left-align" style="padding:0;">
                  <li><a href="javascript:void(0);" onclick="fbshareCurrentPage();"><img src="/polls/images/circle-facebook.png" /></a></li>
                  <li><a href="javascript:void(0);" onclick="gPlus('');"><img src="/polls/images/circle-google_plus.png" /></a></li>
                  <li><a href="javascript:void(0);" onclick="clicked_twitter_button(this);" id="twitterbutton"><img src="/polls/images/circle-twitter.png" /></a></li>
              </ul>
                <ul class="list-inline  col m6 s6 right-align" style="padding:0;">
                  <li><a href="javascript:void(0);" onclick="openCopyModal();"><img src="/polls/images/circle-whatsapp.png" /></a></li>
                  <li><a href="javascript:void(0);" onclick="openCopyModal();"><img src="/polls/images/circle-facebook-messenger.png" /></a></li>
                  <li><a href="javascript:void(0);" onclick="openCopyModal();"><img src="images/circle-slack.png" /></a></li>
              </ul>          
          </div>
                </div>
    </div>
</div>
    </div>
    
    <div id="embed" class="col s12">
    <div class="row">
    <p>Use the script below to embed this poll on your web site. If you are not the Site Administrator, you can just send this code by e-mail to your Web Master.</p>

      <div class="row">
            <p><label for="MainContent_txtIframe">Embed Script</label></p>
            <p><asp:TextBox ID="txtIframe" Width="90%" Height="160" CssClass="iframe" runat="server" TextMode="MultiLine"></asp:TextBox></p>
            <p class="right-align" style="width:90%;">
               <a class="waves-effect waves-light" id="copyembedcode" href="javascript:copyEmbedCodeToClipboard" onclick="copyEmbedCodeToClipboard()">Copy to Clipboard</a>
            </p>
            </div>

  </div>

    </div>
    
    <div id="slider" class="col s12 mCustomScrollbar">

<div class="row">
    <p>Use the script below to slide this poll on your web site. If you are not the Site Administrator, you can just send this code by e-mail to your Web Master</p>

      <div class="row">
            
                  <p><label for="MainContent_txtIframeSlider">Slider Script</label></p>
              <p><asp:TextBox ID="txtIframeSlider" Width="100%" Height="170" CssClass="iframe" runat="server" TextMode="MultiLine" Rows="7" Columns="50"></asp:TextBox></p>
            
            <p class="right-align" style="width:100%;">
                   <a class="waves-effect waves-light" id="copysliderpoll" href="javascript:void(0)" onclick="copySliderPollToClipboard()">Copy to Clipboard</a>
                </p>
            </div>
      </div>
<p>Re-generote code if you want to make changes to these current settings</p>

  <div class="row">
   <label>Choose where the poll is presented:</label>
    <div class="col s12 table-item">
        <asp:RadioButtonList ID="rblposition" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2">
            <asp:ListItem Selected Value="BC">Bottom Centre</asp:ListItem>
            <asp:ListItem Value="CL">Left Centre</asp:ListItem>
            <asp:ListItem Value="BL">Bottom Left</asp:ListItem>
            <asp:ListItem Value="CR">Right Centre</asp:ListItem>
            <asp:ListItem Value="BR">Bottom Right</asp:ListItem>
        </asp:RadioButtonList>

        </div>
    </div>
<div class="row">
   <label>Poll entry animation speed:</label>
    <div class="col s12">

        <asp:RadioButtonList ID="rblspeed" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="3">
            <asp:ListItem Selected>Slow</asp:ListItem>
            <asp:ListItem>Medium</asp:ListItem>
            <asp:ListItem>Fast</asp:ListItem>
        </asp:RadioButtonList>


      </div>
    </div>
<div class="row">
    <div class="input-field col s12 right-align">
        <div id="pollSlider-button"  class="btn-large">Preview Slider</div>
    </div>
</div>
<div class="row">
       <label>Target</label>
        <div class="input-field col s12">
            <asp:Label ID="Label2" runat="server">Countries</asp:Label>
            <select class="chosen-select" style="width:200px;" id="selectcountries" multiple="multiple" size="1">
            </select>
            <br />
            <asp:Label ID="lblErrorMsg" ForeColor="Red" Visible="false" runat="server"></asp:Label>
            <asp:TextBox ID="tbCountries" runat="server" AutoPostBack="false" EnableViewState="false"></asp:TextBox>
        </div>
        <div class="input-field col s12">
            <asp:Label ID="Label3" runat="server">Cities</asp:Label>
            <select class="chosen-select" style="width:200px;" id="selectcities" multiple="multiple" size=1>
            </select>
            <br />
            <asp:Label ID="lblErrorMsg1" ForeColor="Red" Visible="false" runat="server"></asp:Label>
            <asp:TextBox ID="tbcities" runat="server" AutoPostBack="false" EnableViewState=false></asp:TextBox>

        </div>
</div>



         <div class="row">
   <label>Device:</label>
    <div class="col s12">
        <asp:CheckBoxList ID="chklstDevice" runat="server" RepeatDirection="Horizontal" RepeatColumns="3">
            <asp:ListItem>Desktop/Laptop</asp:ListItem>
            <asp:ListItem>Mobile</asp:ListItem>
            <asp:ListItem>Tablet</asp:ListItem>
        </asp:CheckBoxList>


      </div>
    </div>
        
        <div class="row">
            <label>Poll display for</label>
            <div class="input-field col s12">
                <asp:TextBox ID="txtFreqvisit" runat="server" Text="1" style="width:50px;"></asp:TextBox><asp:Label ID="lblfreqvisit" runat="server"  Text="visit(s) only"></asp:Label>
            </div>
        <div class="col s12 right-align">
               <asp:Button ID="btnPreview" runat="server" CssClass="btn-large"  Text="RE-Generate Code" onclick="btnPreview_Click" />
        </div>
        </div>
        
    </div>

        </div>
  </div>
        </div>
</div>
                   

        </div>
        <div class="col m7 s12 mCustomScrollbar" style="background-color:white !important;display:none;" id="pollpreviewinright">
            <div class="row">
        <div class="col m6 s6" id="pollpreviewmaindiv" style="margin-right:15px !important;">
       <div class="pollmanagerpreviewinpreview" id="pollpreviewheaderdiv">Your poll preview</div>
        <div class="textpollthemepreviewmain">

   <div class="textpollthemepreview row">
                 <div id="imagepreview" runat="server" style="display:none;width:100%;line-height:0;" class=""><img src="App_Themes/images/ImagePoll.jpg" class="responsive-img" id="imgPreview" runat="server" height="124"  style="width:100%;max-height:124px;"/></div>
                 <div id="videopreviewcreate" runat="server" class="video-container" style="display:none; width: 100%;height:auto;"></div>
                <div class="pad-10 questioncss" style="min-height:20px;margin-bottom:10px;padding-left:10px !important;" id="questiondiv">
                    <asp:Label ID="lblQuestion" CssClass="wrapquestion" Text="Question?" runat="server"></asp:Label>
                </div>
                <div id="mainanswerdiv2" class="row" style="padding-top:1%;margin-bottom:0px !important;">
                    <asp:Repeater ID="AnswerPreview" runat="server">
                        <HeaderTemplate>
                            <table id="previewansweroptions" class="no-pad" style="height:auto;">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="txtRow col m12 s12 extra-padding-bottom">
                                <td class="singleanswers col s2"><input type="radio" id="radButton_<%# Eval("AnswerId") %>" name="answeroptionradio" style="position:relative !important;left:0px !important;visibility:visible;" onclick="showThankYouOption('<%# Eval("AnswerId") %>')"/></td>
                                <td class="multipleanswers col s2"><input type="checkbox"  class="" id="ckbButton_<%# Eval("AnswerId") %>" style="position:relative !important;left:0px !important;visibility:visible;"/></td>
                                <td class="col s8"><div id="lblradText_<%# Eval("AnswerId") %>">Answer </div></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div id="commentbox" class="row left-align input-field" style="display:none;width:95%;height:auto;margin-top:0px !important; margin-bottom:5px !important;" runat="server">
                    <input type="text" id="MainContent_txtComments" runat="server"/>
                    <label for="MainContent_txtComments" id="placeholdercomments">Comments</label>
                </div>
                
                <div class="row no-pad" style="display:none;bottom:0; width:100%;position:absolute;" id="votebuttonwithsponsormaindiv" >
                    <div class="col m12 s12">
                        <div class="col m6 s6 left-align" id="votebuttonwithsponsor" style="padding-top:6px;">
                            <div runat="server" id="customvote" style="height:auto;vertical-align:middle;padding-top:2px;padding-bottom:2px;padding-left:5px;padding-right:5px;width:auto;text-align:center;background-color:#2196F3;color:white;">Vote and View Results</div>
                        </div>
                        <div class="col m6 s6 right" style="margin-top:0;">
                            <div class="sponsorimagepreview" id="sponsorpreviw" runat="server" style="display:none">
                                <div><asp:Label ID="lblSponsor" Text="Sponsored By" runat="server"></asp:Label></div>
                               <div><a href="#" id="logolink" target="_blank" runat="server"><img src="App_Themes/images/logoPlaceholder2.png" runat="server" id="logo"  width="43" height="43" /></a></div>
                           </div>
                        </div>
                    </div>
                </div>
                <div class="col m12 s12 no-pad center-align " id="votebuttonmaindiv">
                    <div class="input-field">
                    <asp:Label runat="server" ID="votebutton" Text="Vote and View Results" style="vertical-align:middle;padding-top:4px;padding-left:5px;padding-right:5px;background-color:#2196F3;color:white;" Height="30"></asp:Label></div>
                </div>
                <div id="poweredby" class="poweredby" runat="server">
                    Powered by 
                    <a href="https://www.insighto.com/Polls/" target="_blank" style="color: rgb(0, 0, 0); font-family: Arial; font-weight: normal; font-style: normal; text-decoration: none;">
                        <img src="https://test.insighto.com/assets/img/insighto-logo.png" style="max-height: 14px;margin: 3px 0 -5px 0;"> <span style="color: #ff6c00;"> Polls</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

            <div class="col m6 s6" id="pollresultspreviewmaindiv">
    <div class="pollmanagerpreviewinpreview" id="pollpreviewresultsheaderdiv">Your results preview</div>
            <div class="textpollthemepreviewresults">

            <div class="row">
                 <div id="imgResults" runat="server" style="display:none;line-height:0;" class="">
                     <img src="App_Themes/images/ImagePoll.jpg" id="imgPreviewResults" class="responsive-img" runat="server" height="124" style="width:100%;max-height:124px;"/>
                 </div>
                 <div id="videoResults" runat="server" class="video-container" style="display:none; width: 100%;height:auto;"></div>
                <div class="pad-10 questioncss" style="min-height:15px;padding-left:10px !important;" id="questiondivresults">
                    <asp:Label ID="lblQuestionResults" CssClass="wrapquestion" Text="Question?" runat="server"></asp:Label>
                </div>
                <div id="results" runat="server" class="row" style="margin-bottom:5px !important;">
                    <div id="purejschart" class="col m11 center-align" ></div>
                </div>
                <div class="col m12" id="pollleadgencontent" style="padding-top:5px;">
                    <div id="invitationmessagepreview" runat="server" style="display:none; padding: 0 8px; display: inline-table; font-size:12px;" class="chip"></div>
                    <div id="contactdetailscollectiondiv" runat="server" style="display:none;">
                        <div class="col m12" id="phonenumberdiv" runat="server" style="display:none;padding:0 !important;">
                            <div class="input-field nopad phn-icon col m12 nomar" style="padding:0 !important;margin: 0 auto;">
                                <label id="placeholdermobile">Mobile</label>
                                <asp:TextBox ID="phonenumber" runat="server" style="padding-bottom:0;margin-bottom:0 !important;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="input-field nopad phn-icon col m12 nomar" id="emaildiv" runat="server" style="padding:0 !important;margin: 0 auto;">
                            <label id="placeholderemail">Email</label>
                            <asp:TextBox ID="email" runat="server" style="padding-bottom:0;margin-bottom:0 !important;"></asp:TextBox>
                        </div>
                    </div>
                    <div id="submitbuttonpreviewdiv" class="row center center-align col m12 no-pad" style="display:none;">
                        <div class="input-field">
                            <input type="button" ID="btnSubmitLeadGen" runat="server" Class="viewandvoteimagepreview" style="display:block !important;margin: 0 auto;border:none;" value="Submit">
                        </div>
                    </div>
                </div>
                <div id="thankyouoverlay" style="display:none;width:100%;height:auto;padding-top:15%;">
                    <p id="thankyouoverlaymessage" class="chip"></p>
                </div>

                <div class="row col m12 s12" style="bottom:0; width:100%;position:absolute;" id="sponsorandsocialmediainresults">
                    <div class="col m6 s6 left left-align" style="margin:0 auto;" id="socialmediadiv">
                         <div class="col m12 s12 left-align left" style="width:100%;margin-left:-10px;">
                             <div id="socialsha" runat="server" style="display:none;margin-left:-10px;width:110%;text-align:left;margin-bottom:0px;font-size:12px;" class="row"><asp:Label ID="lblShare" runat="server" Text="Share this poll" Style="font-size:14px;" maxlength="25" length="25"></asp:Label></div>
                             <div style="display:none;width:100%;float:left;margin-left:-10px;margin-bottom:0;" id="social" runat="server" class="row left">
                                 <div style="width:110%;float:left;margin-left:-15px;margin-top:0;" id="social1" runat="server" class="col m12 s12 left-align">
                                    <div id="googleicon" runat="server" style="display:none;padding-right:1px;" class="col m4 s4 left-align"><img src="App_Themes/images/google.png"  class="cursor" /></div>
                                    <div id="twittericon" runat="server"  style="display:none;padding-right:1px;" class="col m4 s4 left-align"><img src="App_Themes/images/twitter.png" class="cursor" /></div>
                                    <div id="fbicon" runat="server" style="display:none;padding-right:1px;" class="col m4 s4 left-align"><img src="App_Themes/images/facebook.png" class="cursor" /></div>
                               </div>
                            </div>
                       </div>
                    </div>
                    <div class="col m6 s6 right" id="sponsorimageinresults">
                        <div class="sponsorimagepreview no-mar" id="sponsorResults" runat="server" style="display:none;">
                               <div><asp:Label ID="lblSponText" Text="Sponsored By" runat="server"></asp:Label></div>
                               <div><a href="#" id="logolinkres" target="_blank" runat="server"><img src="App_Themes/images/logoPlaceholder2.png" runat="server" id="imgSpoResults" width="43" height="43" /></a></div>
                        </div>
                    </div>
                </div>
            </div>                     
            <div id="resulttextpreview" runat="server" class="resultpreview"><asp:Label runat="server" ID="lblResultText"></asp:Label></div>


            <div class="clear" id="footerclear"></div>
            <div class="row">
                <div id="poweredbyResults" class="poweredbyrep col m12 s12 center-align" runat="server">
                    Powered by 
                    <a href="https://www.insighto.com/Polls/" target="_blank" style="color: rgb(0, 0, 0); font-family: Arial; font-weight: normal; font-style: normal; text-decoration: none;">
                        <img src="https://test.insighto.com/assets/img/insighto-logo.png" style="max-height: 14px;margin: 3px 0 -5px 0;"> <span style="color: #ff6c00;"> Polls</span>
                    </a>
                </div>
            </div>     
        </div>
    </div>    
            <div class="col m12 s12">
            <div id="showrec" runat="server" class="col-md-12 error-pool" style="display:none;">
                <div class="alert alert-danger red-text" role="alert">
                    <i class="material-icons medium" style="font-size: 16px; margin-top: 2px;">error</i>
                    <b>Error:</b> Poll content spilling over the iFrame. With the options selected, the recommended height for this Poll is
                    <asp:Label runat="server" ID="lblRecHeightbPre" Font-Bold="true"></asp:Label>
                    px.<br />
                    You could either increase the height of the iFrame or reduce the options.
                </div>
            </div>
            </div>
        </div>     
    </div>
</div>  
</div>


            <asp:UpdatePanel ID="updatepanel2"  runat="server">
            <ContentTemplate>
                    
                      </ContentTemplate>
            <Triggers>
            <asp:AsyncPostBackTrigger ControlID="tbCountries" EventName="TextChanged" />
            </Triggers>
             </asp:UpdatePanel>
           <div class="pprev_style pprev_CM" id="pSlider" style="display:none;"><div class="closeFlyoutPromo" id="alertclose">x</div><div id="framecontent1"></div></div>
            <div class="pprev_style pprev_BL" id="pSliderBL" style="display:none;"><div class="closeFlyoutPromo" id="alertclose1">x</div><div id="framecontent2"></div></div>
            <div class="pprev_style pprev_BR" id="pSliderBR" style="display:none;"><div class="closeFlyoutPromo" id="alertclose2">x</div><div id="framecontent3"></div></div>
            <div class="pprev_style pprev_LC" id="pSliderCL" style="display:none;"><div class="closeFlyoutPromo" id="alertclose3">x</div><div id="framecontent4"></div></div>
            <div class="pprev_style pprev_RC" id="pSliderCR" style="display:none;"><div class="closeFlyoutPromo" id="alertclose4">x</div><div id="framecontent5"></div></div>
            <div id="framecontent" style="display:none"></div>


                    <asp:HiddenField ID="HiddenField1" runat="server" />
           <asp:HiddenField ID="HiddenField2" runat="server" />
            <asp:HiddenField ID="HiddenField3" runat="server" />
        <asp:HiddenField ID="hdnPollDisplay" runat="server" />

    <script type="text/javascript" src="../scripts/ZeroClipBoard/jquery.zclip.min.js"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <link href="css/jquery.mCustomScrollbar.min.css" type="text/css" rel="stylesheet" media="screen,projection" />

    <script type="text/javascript">
        $(document).ready(function () {
            $("#copysliderpoll").zclip({ path: '../Scripts/ZeroClipBoard/ZeroClipboard.swf', copy: $('#MainContent_txtIframeSlider').val(), beforeCopy: function () { }, afterCopy: function () { } });
            $("#copyembedcode").zclip({ path: '../Scripts/ZeroClipBoard/ZeroClipboard.swf', copy: $('#MainContent_txtIframe').val(), beforeCopy: function () { }, afterCopy: function () { } });
            $("#copyweblink").zclip({ path: '../Scripts/ZeroClipBoard/ZeroClipboard.swf', copy: $('#MainContent_txtWebLink').val(), beforeCopy: function () { }, afterCopy: function () { } });

        });

        function copySliderPollToClipboard() {
            $("#copysliderpoll").zclip({ path: '../Scripts/ZeroClipBoard/ZeroClipboard.swf', copy: $('#MainContent_txtIframeSlider').val(), beforeCopy: function () { }, afterCopy: function () { }, clickAfter: false });
        }

        function copyEmbedCodeToClipboard() {
            $("#copyembedcode").zclip({ path: '../Scripts/ZeroClipBoard/ZeroClipboard.swf', copy: $('#MainContent_txtIframe').val(), beforeCopy: function () { }, afterCopy: function () { }, clickAfter: false });
        }

        function copyWebLinkToClipboard() {
            $("#copyweblink").zclip({ path: '../Scripts/ZeroClipBoard/ZeroClipboard.swf', copy: $('#MainContent_txtWebLink').val(), beforeCopy: function () { }, afterCopy: function () { }, clickAfter: false });
        }
    </script>

    <div id="mfontsize" class="divmalignfonsizediv" style="display:none;">
        <div class="divpaddingicons">
            <div class="mfontnormal">8px</div>
            <div class="mfontnormal">10px</div>
            <div class="mfontnormal">12px</div>
            <div class="mfontnormal">14px</div>
            <div class="mfontnormal">16px</div>
            <div class="mfontnormal">18px</div>
            <div class="mfontnormal">20px</div>
            <div class="mfontnormal">22px</div>
            <div class="mfontnormal">24px</div>
            <div class="mfontnormal">26px</div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#MainContent_txtTwitter").attr("size", "100");

            if (!isNaN(parseInt($("#MainContent_hdnChartHeight").val())))
                chartheight = parseInt($("#MainContent_hdnChartHeight").val());

            renderChart();

        });

    </script>

    <input type="hidden" runat="server" id="hdnactivation" />
    <script src="scripts/customscriptsAJAX.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ActivationAlert() {
            
            $("#modalactivationalert").css("margin-top", ($("#nagivationtabs").offset().top + 20).toString() + "px");
            $("#modalactivationalert").css("z-index", "999999");
            $('#modalactivationalert').openModal();
        }
        $(window).load(function () {
            if (window.parent.document.getElementById("preloader2") != null) {
                window.parent.document.getElementById("preloader2").style.display = "none";
            }
            $("#pollpreviewinright").show();

        });
        $(document).ready(function () {
            $(".select-dropdown, .caret").hide();

            load();
            setScrollBars();

            $("#MainContent_SaveAndClose").click(function () {

                Materialize.toast("Saving changes and closing...", 3000);
                setTimeout(function () {
                    updateParentWindow();
                    window.parent.closeFancybox();
                }, 3000);
            });
            $("#MainContent_txtIframeSlider").css("min-width", "100%");
            $("#MainContent_txtIframeSlider").css("max-width", "100%");
            $("#MainContent_txtIframeSlider").css("resize", "none");
            $("#MainContent_txtWebLink").css("min-width", "90%");
            $("#MainContent_txtWebLink").css("max-width", "90%");
            $("#MainContent_txtWebLink").css("resize", "none");
            $("#MainContent_txtIframe").css("min-width", "90%");
            $("#MainContent_txtIframe").css("max-width", "90%");
            $("#MainContent_txtIframe").css("resize", "none");
            

        });

        function closeIframe() {
            //window.parent.document.getElementsByClassName("fancybox-iframe").close();
            updateParentWindow();
            window.parent.closeFancybox();
        }

        function updateParentWindow() {

            window.parent.document.getElementById('parentpollid').value = $("#MainContent_hdnParentPollID").val();
            window.parent.document.getElementById('newpollquestion').value = $("#MainContent_txtQuestion").val();
            window.parent.document.getElementById('newpollname').value = $("#MainContent_lblPollName").val();
            window.parent.document.getElementById('newpollvideolink').value = $("#MainContent_txtVideo").val();
            window.parent.document.getElementById('newpollimagelink').value = $("#MainContent_fileUploadName").val();
            window.parent.document.getElementById('newpollstatus').value = $("#MainContent_hdnPollStatus").val();
        }

        function fbshareCurrentPage() {
            var videolink = "";
            var result = $("#MainContent_txtWebLink").val();
            window.open("https://www.facebook.com/sharer/sharer.php?u=" + result, 'popupwindow', 'scrollbars=yes,width=800,height=400').focus();
            return false;
        }

        function gPlus(url) {
            var result = $("#MainContent_txtWebLink").val();
            window.open('https://plus.google.com/share?url=' + result, 'popupwindow', 'scrollbars=yes,width=800,height=400').focus();
            return false;
        }
        
        var text = "Click to vote";
        function clicked_twitter_button(anchor_instance) {
            var result = "";
            result = $("#MainContent_txtQuestion").val() + " " + text + " " + $("#MainContent_txtWebLink").val();
            window.open("https://twitter.com/intent/tweet?text=" + result, 'popupwindow', 'scrollbars=yes,width=800,height=400').focus();
        }


    </script>
    
        </main>
    <footer class="page-footer fixed" style="position:absolute;bottom:0;left:0;width:100%;background-color:#319AFA;height: auto;margin: 0;padding: 10px 0;" id="footer" runat="server">
          <div class="row nomar">
            <div class="right-align col m12">
                <asp:Button ID="SaveAndClose" runat="server" CssClass=" btn-flat waves waves-orange orange  black-text no-pad"  style="padding-bottom:5px;" Text="Save and Close" OnClick="SaveAndClose_Click"/>
            </div>
          </div>
        </footer>
                  <!--
           <div id="ResultBox" class="col m12 nopad nomar" style="display:none" runat="server">
               
           <div class="col m6">
          <div class="input-field">
               <asp:TextBox ID="txtMessageBgColor" onkeypress = "return CkKeyPress(event);" runat="server"  name="background"></asp:TextBox>
               </div>
            </div>
          <div class="col m6">
              <h6>Message Color</h6>
          <div class="input-field">
               <asp:TextBox ID="txtMessageColor" runat="server"  onkeypress = "return CkKeyPress(event);" name="background"></asp:TextBox>
              </div>
            </div>
                <div class="clear"></div>
                    <div class="row nopad">
                        <div class="col m12 s12 nopad">
                <div class="col m12 s12" style="background-color:#ECECEC !important; margin-top: 20px;">
                    <div class="col m12 s12">
                        <div class="fonttype col m5 s5">
                            <asp:DropDownList ID="ddlMsgFont" onkeypress = "return CkKeyPress(event);" runat="server"  Font-Size="Small" CssClass="browser-default black-text" Height="30" BackColor="#ECECEC">
                                <asp:ListItem Text="Arial" Value="Arial"></asp:ListItem>
                               <asp:ListItem Text="Andale Mono" Value="Andale Mono"></asp:ListItem>
                               <asp:ListItem Text="Arial Black" Value="Arial Black"></asp:ListItem>
                               <asp:ListItem Text="Book Antique" Value="Book Antique"></asp:ListItem>
                               <asp:ListItem Text="Comic SanSMS" Value="Comic SanSMS"></asp:ListItem>
                               <asp:ListItem Text="Courier New" Value="Courier New"></asp:ListItem>
                               <asp:ListItem Text="Georgia" Value="Georgia"></asp:ListItem>
                               <asp:ListItem Text="Helvetica" Value="Helvetica"></asp:ListItem>
                               <asp:ListItem Text="Impact" Value="Impact"></asp:ListItem>
                               <asp:ListItem Text="Tahoma" Value="Tahoma"></asp:ListItem>
                               <asp:ListItem Text="Terminal" Value="Terminal"></asp:ListItem>
                               <asp:ListItem Text="Trebuchet MS" Value="Trebuchet MS"></asp:ListItem>
                               <asp:ListItem Text="Sans serif" Value="Sans serif"></asp:ListItem>
                               <asp:ListItem Text="Verdana" Value="Verdana"></asp:ListItem>
                               <asp:ListItem Text="Times New Roman" Value="Times New Roman"></asp:ListItem>
                         </asp:DropDownList>
                    </div>
                   <div class="fonticons col m2">
                       <button class="btnmfontsize"><div class="ddl"> 
                       <img src="App_Themes/fontstyles/t_inactive.png" id="imgMFSize"  runat="server"/></div></button>
                   </div>
                   <div class="fonticons col m1">
                       <img src="App_Themes/fontstyles/bold_inactve.png" id="imgMBold" onclick="msgboldactive()" runat="server" />
                       <img src="App_Themes/fontstyles/bold_active.png" id="imgMBoldActive" onclick="msgboldinactive()" runat="server" />
                   </div>
                   <div class="fonticons col m1">
                       <img src="App_Themes/fontstyles/Italic_inactive.png" id="imgMItalic" onclick="msgitalicactive()" runat="server" />
                       <img src="App_Themes/fontstyles/Italic_active.png" id="imgMItalicActive" onclick="msgitalicinactive()" runat="server" />
                   </div>
                   <div class="fonticons col m1">
                      <img src="App_Themes/fontstyles/underline_inactive.png" id="imgMUL" onclick="msgulactive()" runat="server" />
                      <img src="App_Themes/fontstyles/underline_active.png" id="imgMULActive" onclick="msgulinactive()" runat="server" />
                   </div>
                   <div class="fonticons col m1">
                       <div class="col m12">
                          <button class="btnmalignment"><div class="ddl"><img src="App_Themes/fontstyles/left_inactive.png" id="mleftinactive" runat="server" />
                          <img src="App_Themes/fontstyles/left_active.png" id="mleftactive"  runat="server"/>
                          <img src="App_Themes/fontstyles/right_active.png"  id="mrightactive" runat="server"/>
                          <img src="App_Themes/fontstyles/center_active.png" id="mcenteractive"  runat="server"/></div></button>
                        </div>
                       <div class="col m12">
                            <div id="msgalign" class="divmalignstyles" style="display:none;">
                                <div class="divpaddingicons">
                                    <div>
                                        <img src="App_Themes/fontstyles/right_active.png" id="msgrightactive" onclick="msgalignright()" />
                                        <img src="App_Themes/fontstyles/right_inactive.png" id="msgrightinactive" onclick="msgalignright()" />
                                    </div>
                                    <div>
                                        <img src="App_Themes/fontstyles/center_active.png" id="msgcenteractive" onclick="msgaligncenter()" />
                                        <img src="App_Themes/fontstyles/center_inactive.png" id="msgcenterinactive" onclick="msgaligncenter()" />
                                    </div>
                                    <div>
                                        <img src="App_Themes/fontstyles/left_active.png" id="msgleftactive" onclick="msgalignleft()" />
                                        <img src="App_Themes/fontstyles/left_inactive.png" id="msgleftinactive" onclick="msgalignleft()" />
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
           </div></div></div>
               </div>
            -->
    <!--
   <div class="col-md-5" style="background: #319AFA; padding: 30px 18px; margin-top: -3px;">
    
            <div class="clear"></div>
        <div class="col-md-12">
              <div class="col-md-4 col-sm-4 col-xs-4 text-center">
              <div id="createarrow" runat="server">
                  <img src="App_Themes/images/arrowdown.png" width="30" height="20" style="margin-top: -6px !important;" />
               </div>
               </div>
              <div class="col-md-4 col-sm-4 col-xs-4 text-center">
              <div id="designarrow"  style="display:none" runat="server">
                  <img src="App_Themes/images/arrowdown.png" width="30" height="20" style="margin-top: -6px !important;" />
               </div>
               </div>
              <div class="col-md-4 col-sm-4 col-xs-4 text-center">
              <div id="launcharrow"  runat="server" style="display:none">
                  <img src="App_Themes/images/arrowdown.png" width="30" height="20" style="margin-top: -6px !important;" />
               </div>
            </div>
        </div>
        <div class="clear"></div>
       <div id="create">

    -->
                <!--<asp:Label ID="setDisplayPollType" runat="server"></asp:Label>
                <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                    <div>
                        <asp:Label ID="lblSuccMsg" runat="server"
                            meta:resourcekey="lblSuccMsgResource1" />
                    </div>
                </div>-->
</asp:Content>
