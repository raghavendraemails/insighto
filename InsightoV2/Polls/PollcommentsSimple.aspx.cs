﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts.Web;
using Insighto.Business.Charts;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using sharp = iTextSharp.text;
using FeatureTypes;
using Insighto.Business.ValueObjects;
using System.Text;
using System.Linq;
using System.Threading;
using DevExpress.Web.ASPxClasses;
using System.Diagnostics;
using InfoSoftGlobal;
using System.Security.Cryptography;
using iTextSharp.text.html.simpleparser;
using System.Reflection;
using OfficeOpenXml;
using Office = Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.Runtime.InteropServices;
using Word = Microsoft.Office.Interop.Word;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
//using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Packaging.Extensions;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Threading.Tasks;

public partial class PollcommentsSimple : System.Web.UI.Page
{
    //int quesID = 0;
    int surveyID = 0;
    Hashtable ht = new Hashtable();
    SurveyCore surcore = new SurveyCore();

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 )
            {

                if (ht.Contains("PollId"))
                    surveyID = Convert.ToInt32(ht["PollId"]);



                var sessionStateService = ServiceFactory.GetService<SessionStateService>();
                LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

                DataSet dsRespondentLimit = surcore.getPollRespondentLimit(loggedInUserInfo.UserId);

                int responselimit = Convert.ToInt32(dsRespondentLimit.Tables[0].Rows[0][1].ToString());
                DataSet dspollinfo = surcore.getpollinfo(surveyID,responselimit);
                if (dspollinfo.Tables[5].Rows.Count > 0)
                {
                    grdResponses.DataSource = dspollinfo.Tables[5];
                    grdResponses.DataBind();
                                
                }
                   
                }
                
            }
        
    }
    protected void cmdBack_Click(object sender, EventArgs e)
    {
        string navReports = "";

        if (ht.Contains("Pollid"))
            surveyID = Convert.ToInt32(ht["Pollid"]);

        navReports = "PollId=" + surveyID;
        
        
        string urlstr = EncryptHelper.EncryptQuerystring("PollReports.aspx", navReports );
        Response.Redirect(urlstr);
    }
}