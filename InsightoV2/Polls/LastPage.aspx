﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LastPage.aspx.cs" Inherits="Poll_LastPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<style type="text/css">
.classmsg
{
    background-color: #6C89E1;
color: #FFF;
font-size: 13px;
margin: 50px 0px;
padding: 10px 0px;
font-weight: bold;
width: 100%;
float: left;
text-align: center;
vertical-align: middle;
font-family:Verdana;
}

</style>
    <title></title>
        <script type="text/javascript">
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-55613473-1', 'auto');
            ga('send', 'pageview');

</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="classmsg" ><asp:Label runat="server" Text="Exciting Poll coming up. Watch this space" ID="lblMsg"></asp:Label></div>
    </div>
    </form>
</body>
</html>
