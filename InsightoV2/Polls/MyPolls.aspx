﻿<%@ Page Title="" Language="C#" MasterPageFile="MyPolls.master" AutoEventWireup="true" CodeFile="MyPolls.aspx.cs" Inherits="Polls_new_design_MyPolls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="col m12" ng-controller="pollsListController as ctrl">

          <!--   Icon Section   -->
        <div class="row pool-page" style="background-color:#E4EFF5;">
            <div class="col m11 nopad">
                <div class="col s12 m4 drafts-block" id="draftsblock">
                    <div ng-include="'partials/newpoll.html'"></div>
                    <div ng-include="'partials/draftpolls.html'"></div>
                </div>
                <div class="col s12 m4 active-block" ng-include="'partials/activepolls.html'" id="activeblock"></div>
                <div class="col s12 m4 closed-block" ng-include="'partials/closedpolls.html'" id="closedblock"></div>
            </div>


        </div>
 
      </div>
    
    <div id="modalerror" class="modal">
        <div class="modal-content">
          <h4>Error</h4>
          <p>There was an error performing the action. Please try again or contact <a href="mailto:support@insighto.com">support@insighto.com</a></p>
        </div>
        <div class="modal-footer center-align" style="text-align:center;">
          <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
        </div>
      </div>
    
    <div id="modallogout" class="modal">
        <div class="modal-content">
          <h4>Session over</h4>
          <p>Your session has ended. Click OK to log back in.</p>
        </div>
        <div class="modal-footer center-align" style="text-align:center;">
          <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat" onclick="logout();">OK</a>
        </div>
      </div>
    
    <div id="modalerror1" class="modal">
        <div class="modal-content">
          <h4>Error</h4>
          <p>There was an error performing the action. Please try again or contact <a href="mailto:support@insighto.com">support@insighto.com</a></p>
        </div>
        <div class="modal-footer center-align" style="text-align:center;">
          <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
        </div>
      </div>
    
    <div id="modalerrorforreplacepollname" class="modal">
        <div class="modal-content">
          <h4>Error</h4>
          <p id="errormessagereplacepollname"></p>
        </div>
        <div class="modal-footer center-align" style="text-align:center;">
          <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
        </div>
      </div>
    <div id="modalfornewuser" class="modal center-align">
        <div class="modal-content center-align">
          <h4>Welcome!</h4>
          <p class="left-align">
              Congratulations! You are one just one step away from launching your first poll. However, you have not yet fully activated your Insighto account.
              <br/><br/>
              Please have a look at the mail received from Insighto - with the subject <i>"Please confirm your email address"</i> and click on the Confirm button given in it. 
              You would then be ready to launch your first poll.
              <br/><br/>
              For queries, please contact <a href="mailto:support@insighto.com">support@insighto.com</a>.
              <br/><br/>
              Happy Polling!
          </p>
        </div>
        <div class="modal-footer center-align" style="text-align:center;">
                    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn green white-text" style="float:none;">OK</a>
        </div>
      </div>
    <input type="hidden" name="pollactiontype" id="pollactiontype" />
    <input type="hidden" name="parentpollid" id="parentpollid" />
    <input type="hidden" name="pollid" id="pollid" />
    <input type="hidden" name="newpollvideolink" id="newpollvideolink" />
    <input type="hidden" name="newpollimagelink" id="newpollimagelink" />
    <input type="hidden" name="newpollstatus" id="newpollstatus" />
    <input type="hidden" name="newpollname" id="newpollname" />
    <input type="hidden" name="newpollquestion" id="newpollquestion" />
    <input type="hidden" name="firstquestion" id="firstquestion" value=""/>
    <input type="hidden" name="activationflag" id="activationflag" value="0" runat="server"/>
    <input type="hidden" name="activationalert" id="activationalert" value="0" runat="server"/>

    <script>
        function showFirstPollCreateBubble() {
            var pos = $("#newpollbutton").position();
            var toppos = pos.top;
            var leftpos = pos.left;
            var width = $("#newpollbutton").css("width");
            var intwidth = parseInt(width.replace("px"));
            $("#firstpollcreate").show("easing");
            $("#firstpollcreate").css({ "position": "absolute", "top": (toppos + 5), "left": (intwidth*0.70) })
        }

        function logout() {
            window.location.href = "/polls/login.aspx";
        }
    </script>
    <div class="chat blue-text" id="firstpollcreate" style="display:none;">
        <div class="bubble me">Click on 
            <b>+</b>
            icon to create your first poll.
        </div>
    </div>
    <div class="chat blue-text" id="firstpolledit" style="display:none;">
        <div class="bubble me">Click on the card to create, edit and design your poll.</div>
    </div>
</asp:Content>