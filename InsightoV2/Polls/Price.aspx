﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Price.aspx.cs" Inherits="Polls_Price" MasterPageFile="~/Home.master" %>
<%@ Register TagPrefix="uc" TagName="PollsHeader" Src="~/UserControls/PollsHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc:PollsHeader ID="PollsHeader" runat="server" />
    <div> 
      <div class="breadcrumbs-v3 img-v1 text-center">
        <div class="container">
       <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;">
            <span>Polls Plans </span></h2>
        </div>
      </div>

      <!--=== Content Part ===-->
      <div class="bg-grey content-sm pricing-page-intro">
        <div class="container text-center">
          <!--<h2 class="title-v2 title-center" style="margin-top:-20px;">PLANS TO SUIT YOUR UNIQUE POLL NEEDS</h2>-->
          <p class="space-lg-hor" style="font-size: 15px; margin-top:0px;">
          </div>
      </div>
  
      <!--=== Content Part ===-->
      <div id="plans" class="container content">
        <div class="row margin-bottom-10 pricing-rounded">
          <div class="col-md-3">
            <div class="pricing hover-effect" style="height:572px;">
              <div class="pricing-head">
                <h3 style="background:#1E73BE; text-shadow:none; font-size:20px !important; font-weight:normal !important;">Personal</h3>
                <h4><span id="spprof" style="font-size:35px;margin-top:22px;"><i class="fa fa-inr"></i> 0 </span><span style="margin-top:0px;">Lifetime Free </span></h4>
                <ul class="pricing-content list-unstyled">
                  <li class="bg-color" style="text-align:center;color:#fff !important;background:#1E73BE;font-size:20px;"> &nbsp; </li>
                </ul>
              </div>
              <ul class="pricing-content list-unstyled">
                <li class="bg-color"><i class="fa fa-check"></i> Text / Image / Video polls</li>
                <li><i class="fa fa-check"></i> Live preview while creating polls</li>
                <li class="bg-color"><i class="fa fa-check"></i> Public results </li>
                <li><i class="fa fa-check"></i> 500 responses / month</li>
                <li class="bg-color"><i class="fa fa-check"></i> No Sponsorship feature</li>
              </ul>
              <br>
              <div align="center"><a href="registration.aspx" class="btn-u-blue">SIGN UP</a></div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="pricing hover-effect" style="height:572px;">
              <div class="pricing-head">
                <h3 style="background:#1E73BE; text-shadow:none; font-size:20px !important; font-weight:normal !important;">Professional</h3>
                <h4><span id="spprof" style="font-size:35px;margin-top:22px;" runat="server"><i class="fa fa-inr"></i> 399 </span><span style="margin-top:0px;"> Per Month </span></h4>
                <ul class="pricing-content list-unstyled">
                  <li class="bg-color" style="text-align:center;color:#fff;background:#1E73BE;font-size:20px;">
                      <%--Annual Plan -  <!--<i class="fa fa-inr"></i>--><img src="../assets/img/rupee-symbol.png" alt=""> 3,990--%>
                        <asp:LinkButton ID="lnkprof" runat="server" 
                                style="text-decoration:underline;color:White;" onclick="lnkProf_Click" CausesValidation="false"><%=AnnualProf %>
                            </asp:LinkButton>                  </li>
                </ul>
              </div>
              <ul class="pricing-content list-unstyled">
                <li class="bg-color"><i class="fa fa-check"></i> Text / Image / Video polls</li>
                <li><i class="fa fa-check"></i> Live preview while creating polls</li>
                <li class="bg-color"><i class="fa fa-check"></i> Public results </li>
                <li><i class="fa fa-check"></i> 3,000 responses / month</li>
                <li class="bg-color"><i class="fa fa-check"></i> No Sponsorship feature</li>
              </ul>
              <br>
              <div align="center">
                  <%--<a href="#" class="btn-u-blue">SIGN UP</a>--%>
                   <asp:Button ID="btnprof2" runat="server" CssClass="btn-u-blue" Text="BUY NOW" 
                             style="font-weight:bold;" onclick="btnprof2_Click" CausesValidation="false"  />
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="pricing hover-effect" style="height:572px;">
              <div class="pricing-head">
                <h3 style="background:#1E73BE; text-shadow:none; font-size:20px !important; font-weight:normal !important;">Business</h3>
                <h4><span id="spBus" style="font-size:35px;margin-top:22px;" runat="server"><i class="fa fa-inr"></i> 1,299</span><span style="margin-top:0px;"> Per Month  </span></h4>
                <ul class="pricing-content list-unstyled">
                  <li class="bg-color" style="text-align:center;color:#fff;background:#1E73BE;font-size:20px;"> 
                      <%--Annual Plan -  <!--<i class="fa fa-inr"></i>--><img src="../assets/img/rupee-symbol.png" alt=""> 12,990--%>
                        <asp:LinkButton ID="lnkbus" runat="server" 
                                style="text-decoration:underline;color:White;" onclick="lnkBus_Click" CausesValidation="false"><%=AnnualBus %>
                            </asp:LinkButton>

                  </li>
                </ul>
              </div>
              <ul class="pricing-content list-unstyled">
                <li class="bg-color"><i class="fa fa-check"></i> Text / Image / Video polls</li>
                <li><i class="fa fa-check"></i> Live preview while creating polls</li>
                <li class="bg-color"><i class="fa fa-check"></i> Option to keep results private </li>
                <li><i class="fa fa-check"></i> 10,000 responses / month</li>
                <li class="bg-color" style="height:45px;"><i class="fa fa-check" style="height:100%;vertical-align:bottom"></i> <span style="width:90%;">Monetize Polls (Annual Plan only)</span></li>
              </ul>
              <br>
              <div align="center">
                  <%--<a href="#" class="btn-u-blue">SIGN UP</a>--%>
                   <asp:Button ID="btnbus2" runat="server" CssClass="btn-u-blue" Text="BUY NOW" CausesValidation="false"
                             style="font-weight:bold;" onclick="btnbus2_Click"  />
              </div>
            </div>
        
          </div>
          <div class="col-md-3">
            <div class="pricing hover-effect" style="height:572px;">
              <div class="pricing-head">
                <h3 style="background:#1E73BE; text-shadow:none; font-size:20px !important; font-weight:normal !important;">Publisher</h3>
                <h4><span id="spPub" style="font-size:35px;margin-top:22px;" runat="server"><i class="fa fa-inr"></i> 9,999</span><span style="margin-top:0px;"> Per Month </span></h4>
                <ul class="pricing-content list-unstyled">
                  <li class="bg-color" style="text-align:center;color:#fff;background:#1E73BE;font-size:20px;">
                      <%--Annual Plan -  <!--<i class="fa fa-inr"></i>--><img src="../assets/img/rupee-symbol.png" alt=""> 34,990--%>
                    <asp:LinkButton ID="lnkpub" runat="server"  style="text-decoration:underline;color:White;" onclick="lnkPub_Click" CausesValidation="false">
                        <%=AnnualPub %>
                    </asp:LinkButton>
                  </li>
                </ul>
              </div>
              <ul class="pricing-content list-unstyled">
                <li class="bg-color"><i class="fa fa-check"></i> Text / Image / Video polls</li>
                <li><i class="fa fa-check"></i> Live preview while creating polls</li>
                <li class="bg-color"><i class="fa fa-check"></i> Option to keep results private </li>
                <li><i class="fa fa-check"></i> 60,000 responses / month</li>
                <li class="bg-color" style="height:45px;"><i class="fa fa-check" style="height:100%;vertical-align:bottom"></i> <span style="width:90%;">Monetize Polls
                </span></li>
              </ul>
              <br>
              <div align="center">
                  <%--<a href="#" class="btn-u-blue">BUY NOW</a>--%>
                  <asp:Button ID="btnpub2" runat="server" CssClass="btn-u-blue" Text="BUY NOW" 
                             style="font-weight:bold;" onclick="btnpub2_Click" CausesValidation="false" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/container--> 
      <!--=== End Content Part ===--> 
    </div>
        <asp:HiddenField ID="hdnKey" runat="server" Value="" />
        <asp:HiddenField ID="hdnPrice" runat="server" Value="" />
        <asp:HiddenField ID="hdnTax" runat="server" Value="" />
        <asp:HiddenField ID="hdnTaxAmount" runat="server" Value="" />
        <asp:HiddenField ID="hdnType" runat="server" Value="" />
        <asp:HiddenField ID="hdnOrderNo" runat="server" Value="" />
        <asp:HiddenField ID="hdnTotal" runat="server" Value="" />
        <asp:HiddenField ID="hdnUpgrade" runat="server" Value="No" />
        <asp:HiddenField ID="hdnMerchantId" runat="server" Value="" />
</asp:Content>