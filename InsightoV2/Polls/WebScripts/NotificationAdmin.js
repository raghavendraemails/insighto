﻿(function () {

    var jQuery;

    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.6.2') {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type", "text/javascript");
        script_tag.setAttribute("src",
        "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js");
        if (script_tag.readyState) {
            script_tag.onreadystatechange = function () { // For old versions of IE
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    scriptLoadHandler();
                }
            };
        } else {
            script_tag.onload = scriptLoadHandler;
        }

        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    } else {
        jQuery = window.jQuery;
        main();
    }

    function scriptLoadHandler() {
        jQuery = window.jQuery.noConflict(true);
        main();
    }


    function main() {

        jQuery(document).ready(function ($) {

            $('head').append('<link rel="stylesheet" href="/Styles/notification.css" type="text/css" />');

            jQuery.fn.ShowNotification = function (html, mode, expiryTime, displayFreq, pageName) {
                var newdiv = document.createElement("div");
                //newdiv.className = "slideContent";
                //newdiv.id = "slider";

                newdiv.className = "box";
                newdiv.id = "slider";

                document.body.appendChild(newdiv);
                var sliderdiv = document.getElementById("slider");

                $(sliderdiv).html(html);
                $(sliderdiv).css({ left: $(window).width(), top: ($(window).height() - ($(sliderdiv).height())) / 2 });
                var desiredLeft = $(window).width() / 1.5 - ($(sliderdiv).width());

                $(sliderdiv).animate({ 'left': desiredLeft }, 3000);

                $(document.getElementById("boxclose")).click(function () {
                    $(newdiv).animate({ 'left': $(window).width() },
                {
                    duration: 3000,
                    complete: function () { $('.box').remove(); }
                });

              });
            };

        });
    }

})();
