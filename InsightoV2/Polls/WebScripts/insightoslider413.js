$(document).ready(function () {
    var countryname = '';
    var cityname = '';
    var clientcountryname = '';
    var clientcityname = '';
    jQuery.ajax({ url: '//freegeoip.net/json/',
        type: 'POST',
        dataType: 'jsonp',
        success: function (location) {
            clientcountryname = location.country_code;
            clientcityname = location.city;
            var valcnt = countryname.indexOf(clientcountryname);
            var valcity = cityname.indexOf(clientcityname);
            if (((valcnt >= 0) && (valcity >= 0)) || (valcnt == -1)) {
                var output = document.createElement('div');
                output.setAttribute('id', 'pSliderBR');
                output.setAttribute('class', 'pprev_style pprev_BR');
                var ele = document.createElement('div');
                ele.setAttribute('id', 'timedrpact1');
                ele.setAttribute('class', 'pprev_style pprev_BR');
                var alclose = document.createElement('div');
                alclose.setAttribute('id', 'alertclose');
                alclose.setAttribute('class', 'closeFlyoutPromo');
                ele.appendChild(alclose); output.appendChild(ele);
                $('body').append(output);
                var ifr = $('<iframe/>', { id: 'IframeLaunch',
                    src: 'http://s.insighto.com/1xPH777',
                    style: 'width:300px;height:220px;-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px;border-style:none;',
                    load: function () {
                        var user = getCookie('insightoslidername');
                        var cnttimes = getCookie('counttimes');
                        if (user == '') {
                            setCookie('insightoslidername', 'insightoslidername', 'counttimes', 0);
                            $('.pprev_BR').animate({ bottom: '0px' }, 1000, 'linear', function () { alert('done1'); });
                        }
                        else if (cnttimes < 20) {
                            setCookie('insightoslidername', 'insightoslidername', 'counttimes', cnttimes);
                            $('.pprev_BR').animate({ bottom: '0px' }, { duration: 1000, easing: 'linear', done: function () { if (getCookie('datasaved') == '') { setCookie('datasaved', 'datasaved', 'counttimes', 0); alert('done3'); } } });
                        }
                        $('#alertclose').click(function () {
                            $('.pprev_BR').hide();
                            $('.pprev_BR').css('bottom', '-600px');
                        });
                        $('.pprev_BR').show();
                        //alert('show');
                    }
                });
                $('#timedrpact1').append(ifr);
            }
        }
    });
});

function setCookie(cname, cvalue, counttimes, exdays) {
    var expires = parseInt(exdays) + 1;
    document.cookie = cname + '=' + cvalue;
    document.cookie = counttimes + '=' + expires;
}

function getCookie(cname) {
    var name = cname + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return '';
}