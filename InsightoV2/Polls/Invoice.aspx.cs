﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using CovalenseUtilities.Services;
using System.Globalization;
using System.Configuration;
using System.Data;
using App_Code;

namespace Insighto.Pages
{
    public partial class Invoice : BasePage
    {
        Hashtable ht = new Hashtable();
        string strsaasypayementorderid = "";
        SurveyCore surcore = new SurveyCore();
        PollCreation pc = new PollCreation();
        protected void Page_Load(object sender, EventArgs e)
        {
            int orderId = 0, userId = 0,historyId=0;
            int printConf = 0;
            string knowAccountno = "", knowFavouringAddress = "";
            string paymentInstructionsText = "";
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0)
                {
                    if (ht.Contains("OrderId"))
                        orderId = Convert.ToInt32(ht["OrderId"]);
                    if (ht.Contains("UserId"))
                        userId = Convert.ToInt32(ht["UserId"]);
                    if (ht.Contains("HistoryId"))
                        historyId = Convert.ToInt32(ht["HistoryId"]);
                    if (ht.Contains("Print"))
                    {
                        printConf = Convert.ToInt32(ht["Print"]);
                        divClose.Visible = false;
                    }
                }
            }
            if (orderId > 0)
            {
                lnkUrl.HRef = PathHelper.GetUserLoginPageURL();
                lnkContactus.HRef =PathHelper.GetUserContactusURL();

                DataSet dsif = pc.GetCCPollInvoiceInfo(userId, orderId, historyId);

                string order = dsif.Tables[3].Rows[0]["LICENSETYPE_OPTED"].ToString();

                lblAmountCurrency.Text = "(" + dsif.Tables[1].Rows[0]["CurrencyType"].ToString() + ")";
                lblRateCurrency.Text = "(" + dsif.Tables[1].Rows[0]["CurrencyType"].ToString() + ")";

                string licenseType = "";
                licenseType = dsif.Tables[3].Rows[0]["LICENSETYPE_OPTED"].ToString();

                string configkeyName = licenseType;
                string configkeyDesc = "";

                if ((licenseType == "PROFESSIONAL_MONTHLY") || (licenseType == "BUSINESS_MONTHLY") || (licenseType == "PUBLISHER_MONTHLY"))
                {
                    configkeyDesc = "Subscription to Insighto poll " + licenseType ;
                }
                else if ((licenseType == "PROFESSIONAL_YEARLY") || (licenseType == "BUSINESS_YEARLY") || (licenseType == "PUBLISHER_YEARLY"))
                {
                    configkeyDesc = "Subscription to Insighto poll " + licenseType ;
                }

                List<string> listConfig = new List<string>();
                 
                listConfig.Add(configkeyName);
                listConfig.Add(configkeyDesc);
                listConfig.Add(Constants.CURRENTACCOUNTNO);
                listConfig.Add(Constants.KNOWIENCEADDRESS);
                listConfig.Add(Constants.KNOWIENCEFAVOUROFADDRESS);
                var configService = ServiceFactory.GetService<ConfigService>();
                var configvalue = configService.GetConfigurationValues(listConfig);
                foreach (var cv in configvalue)
                {
                    if (cv.CONFIG_KEY.TrimEnd() == Constants.KNOWIENCEADDRESS.TrimEnd())
                    {
                        knowience_address.InnerHtml = cv.CONFIG_VALUE;
                    }
                    if (cv.CONFIG_KEY.TrimEnd() == Constants.KNOWIENCEFAVOUROFADDRESS.TrimEnd())
                    {
                        knowFavouringAddress = cv.CONFIG_VALUE;
                    }
                    if (cv.CONFIG_KEY.TrimEnd() == Constants.CURRENTACCOUNTNO.TrimEnd())
                    {
                        knowAccountno = cv.CONFIG_VALUE;
                    }

                    item_Name.InnerHtml = configkeyName;
                    item_Description.InnerHtml = configkeyDesc;
                }

                billaddress.InnerHtml = "<strong> " + dsif.Tables[0].Rows[0]["FIRST_NAME"].ToString() + " " + dsif.Tables[0].Rows[0]["LAST_NAME"].ToString() + " </strong><br />" + dsif.Tables[0].Rows[0]["ADDRESS_LINE1"].ToString() + "<br/> " + dsif.Tables[0].Rows[0]["ADDRESS_LINE2"].ToString();
                var invoiceid = "";
                invoiceid = dsif.Tables[1].Rows[0]["ORDERID"].ToString();
                if (licenseType.ToUpper().Contains("PROFESSIONAL"))
                {
                    invoiceid = invoiceid.Replace("POLL", "POLL-PRO");
                }
                else if (licenseType.ToUpper().Contains("BUSINESS"))
                {
                    invoiceid = invoiceid.Replace("POLL", "POLL-BUS");
                }
                else if (licenseType.ToUpper().Contains("PUBLISHER"))
                {
                    invoiceid = invoiceid.Replace("POLL", "POLL-PUB");
                }
                
                if (dsif.Tables[1].Rows.Count > 0)//BECAUSE FOR POLLS HISTORY IS NOT BEING SAVED
                {

                    if (dsif.Tables[1].Rows[0]["PAID_DATE"].ToString() != null)
                    {
                        DateTime paiddate = new DateTime();
                        DateTime expirydate = new DateTime();
                        paiddate = CommonMethods.GetConvertedDatetime(Convert.ToDateTime(dsif.Tables[1].Rows[0]["PAID_DATE"]), Convert.ToInt32(dsif.Tables[0].Rows[0]["STANDARD_BIAS"].ToString()), true);
                        if (licenseType.ToLower().Contains("monthly"))
                        {
                            expirydate = CommonMethods.GetConvertedDatetime(paiddate, Convert.ToInt32(dsif.Tables[0].Rows[0]["STANDARD_BIAS"].ToString()), true).AddMonths(1);
                        }
                        else if (licenseType.ToLower().Contains("yearly"))
                        {
                            expirydate = CommonMethods.GetConvertedDatetime(paiddate, Convert.ToInt32(dsif.Tables[0].Rows[0]["STANDARD_BIAS"].ToString()), true).AddYears(1);
                        }

                        int paymentorderid = Convert.ToInt32(dsif.Tables[1].Rows[0]["PK_ORDERID"].ToString());
                        DataSet dssaasypaymentorder = surcore.getSaasypaymentorderid(paymentorderid);

                        if (dssaasypaymentorder.Tables[0].Rows.Count > 0)
                        {
                            strsaasypayementorderid = dssaasypaymentorder.Tables[0].Rows[0][0].ToString();
                        }

                        invoice_info.InnerHtml = String.Format(Utilities.ResourceMessage("Invoice_Payment_Info"), invoiceid, strsaasypayementorderid, Convert.ToDateTime(dsif.Tables[0].Rows[0]["CREATED_ON"].ToString()).ToString("dd-MMM-yyyy"), paiddate.ToString("dd-MMM-yyyy"), expirydate.ToString("dd-MMM-yyyy"));
                    }
                    else
                    {
                        invoice_info.InnerHtml = String.Format(Utilities.ResourceMessage("Invoice_Payment_Info"), invoiceid, strsaasypayementorderid, Convert.ToDateTime(dsif.Tables[0].Rows[0]["CREATED_ON"].ToString()).ToString("dd-MMM-yyyy"), "NA", "NA");
                    }
                }
                else
                {
                    invoice_info.InnerHtml = String.Format(Utilities.ResourceMessage("Invoice_Payment_Info"), invoiceid, strsaasypayementorderid, Convert.ToDateTime(dsif.Tables[0].Rows[0]["CREATED_ON"].ToString()).ToString("dd-MMM-yyyy"), "NA", "NA");
                }
                item_Qty.InnerHtml = "1";
                item_Rate.InnerHtml = Math.Round(Convert.ToDouble(dsif.Tables[1].Rows[0]["PRODUCT_PRICE"].ToString()), 2).ToString("N", CultureInfo.InvariantCulture);
                item_Amt.InnerHtml = Math.Round(Convert.ToDouble(dsif.Tables[1].Rows[0]["PRODUCT_PRICE"].ToString()), 2).ToString("N", CultureInfo.InvariantCulture);
                subtotal.InnerHtml = Math.Round(Convert.ToDouble(dsif.Tables[1].Rows[0]["PRODUCT_PRICE"].ToString()), 2).ToString("N", CultureInfo.InvariantCulture);
                service_tax.InnerHtml = String.Format(Utilities.ResourceMessage("Invoice_Payment_ServiceTax"), dsif.Tables[1].Rows[0]["TAX_PERCENT"].ToString()); //"Service Tax (" + orderDetails.TAX_PERCENT.ToString() + "%)";
                taxt_amt.InnerHtml = Math.Round(Convert.ToDouble(dsif.Tables[1].Rows[0]["TAX_PRICE"].ToString()), 2).ToString("N", CultureInfo.InvariantCulture);

                total_amt.InnerHtml = "<strong>" + Utilities.FormatCurrency(Convert.ToDouble(dsif.Tables[1].Rows[0]["TOTAL_PRICE"].ToString()), false, true) + "</strong>";
                if (Convert.ToInt32(dsif.Tables[0].Rows[0]["USER_TYPE"].ToString()) == 2)
                {
                    paymentInstructionsText = Utilities.ResourceMessage("Invoice_Payment_Instructions");//"<p><strong>Payment instructions:</strong> </p>";
                    paymentInstructionsText += String.Format(Utilities.ResourceMessage("Invoice_Instruction_ChequeDD"), knowFavouringAddress); //"<p>1.Draw the cheque/DD in favour of &ldquo;" + knowFavouringAddress + " &rdquo; for the amount mentioned above.</p>";
                    paymentInstructionsText += Utilities.ResourceMessage("Invoice_Instruction_Email");    //"  <p>2.Send a mail to <a href='mailto:accounts@insighto.com'>accounts@insighto.com</a> with details of payment.</p>";
                    paymentInstructionsText += Utilities.ResourceMessage("Invoice_Instruction_Options"); //" <p>3.Options for making payment:</p>  <p>Option 1: Mail the Cheque/Demand Draft</p>";
                    paymentInstructionsText += Utilities.ResourceMessage("Invoice_Instruction_AttachChequeDD"); //" <p>Attach the cheque/DD to a copy of invoice generated at the time of registration and send it to the address given at the top right hand corner of the invoice.</p>";
                    paymentInstructionsText += Utilities.ResourceMessage("Invoice_Instruction_DepositHSBC");  //"<p>Option 2: Deposit Cheque/Demand Draft in HSBC Bank</p>";
                    paymentInstructionsText += String.Format(Utilities.ResourceMessage("Invoice_Instruction_DepositAccountNo"),knowFavouringAddress,knowAccountno); //" <p>Deposit the cheque/DD at any of the branches of HSBC Bank favoring " + knowFavouringAddress + " Current Account No." + knowAccountno + "</p>";
                    paymentInstructionsText += Utilities.ResourceMessage("Invoice_Instruction_Ignore"); //" <p style='color: #999999;font-style: italic;'>If you have already made the payment, please ignore the payment instructions.</p>";
                    payment_instructions.InnerHtml = paymentInstructionsText;
                    payment_instructions.Visible = true;
                }
                else
                {
                    payment_instructions.Visible = false;
                }
                if (printConf == 1)
                    Page.ClientScript.RegisterStartupScript(typeof(String), "print", @"<script>window.print();</script>");  //Page.RegisterStartupScript("print", @"<script>window.print();</script>"); 


            }
        }
    }
}