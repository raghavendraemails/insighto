﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Insighto.Business.Helpers;
using Insighto.Data;
using System.Web.Services;
using CovalenseUtilities.Services;
using CovalenseUtilities.Extensions;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Helpers;
using Insighto.Business.Enumerations;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Security.AccessControl;
using System.Net.Mail;
using System.Net;
using System.Web.Script.Serialization;

public partial class AjaxService : System.Web.UI.Page
{
    SurveyCore surcore = new SurveyCore();
    PollCreation Pc = new PollCreation();
        private MailMessage mailMessage = null;
        private SmtpClient smtpMail = null;
    
    public int SurveyFlag
    {
        get
        {
            var key = QueryStringHelper.GetString("key");
            if (String.IsNullOrEmpty(key))
                return 0;

            Hashtable ht = EncryptHelper.DecryptQuerystringParam(key);
            if (ht != null && ht.Contains("surveyFlag"))
            {
                return ValidationHelper.GetInteger(ht["surveyFlag"], 0);
            }
            return 0;
        }

    }
    
    protected void Page_Load(object sender, EventArgs e)
    {

        Response.Clear();
        Response.ContentType = "application/json";
        var responseData = "";
        if (Request["method"] != null)
        {
            var fnName = Request["method"];
            if (fnName == "GetPolls")
            {
                responseData = GetPolls();
            }
            else if (fnName == "GetAllPolls")
            {
                responseData = GetAllPolls();
            }
            else if (fnName == "GetPollsByUserID")
            {
                responseData = GetPolls(2162);
            }
            else if (fnName == "DeletePollByPollId")
            {
                responseData = DeletePollByPollId();
            }
            else if (fnName == "ClosePollByPollId")
            {
                responseData = ClosePollByPollId();
            }
            else if (fnName == "GetOrderInofByUserId")
            {
                responseData = GetOrderInofByUserId();
            }
            else if (fnName == "CopyPollByPollId")
            {
                responseData = CopyPollByPollId();
            }
            else if (fnName == "ReplacePollByPollId")
            {
                responseData = ReplacePollByPollId();
            }
            else if (fnName == "ActivatePollByPollId")
            {
                responseData = ActivatePollByPollId();
            }
            else if (fnName == "CheckPollNameExists")
            {
                responseData = CheckPollNameExists();
            }
            else if (fnName == "UpdateReplacedPollName")
            {
                responseData = UpdateReplacedPollName();
            }
            else if (fnName == "SavePollDetails")
            {
                responseData = SavePollDetails();
            }
            else if (fnName == "SavePollResponse")
            {
                responseData = SavePollResponse(Request["pollid"], Request["answeroptions"], Request["respondentid"], Request["comments"]);
            }
            else if (fnName == "SavePollNameAndType")
            {
                responseData = SavePollNameAndType(Request["Name"], Request["Type"]);
            }
            else if (fnName == "SavePollInteraction")
            {
                responseData = SavePollInteraction(Request["pollid"], Request["respondentid"], Request["email"], Request["phone"], Request["redirect"]);
            }
            else if (fnName == "SendEmail")
            {
                responseData = SendEmail();
            }
            else if (fnName == "SaveCompanyName")
            {
                responseData = SaveCompanyName();
            }
            else if (fnName == "SaveWeblinkBgColor")
            {
                responseData = SaveWeblinkBgColor();
            }
        }
        Response.ContentType = "text/plain";
        Response.Write(responseData);
        Response.End();
    }

    public SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    
    [WebMethod]
    private string GetPolls()
    {

        var searchKey = string.Empty;
        if (Request["keyword"] != null || Request["keyword"] != "")
        {
            searchKey = Server.UrlDecode(Request["keyword"]);
        }
        else
        {
            searchKey = "";
        }
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
       
        string type = "";
        if (Request["mode"] != null && Request["mode"] != "undefined")
        {
            type = Server.UrlDecode(Request["mode"]);
        }
        else
        {
            type = "All";
        }
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var getUserInfo = sessionService.GetLoginUserDetailsSession();
        //SqlConnection con = new SqlConnection();
        //con = strconnectionstring();
        //SqlCommand scom = new SqlCommand("sp_GetTeleUser", con);
        //scom.CommandType = CommandType.StoredProcedure;
        //scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = getUserInfo.UserId;
        //SqlDataAdapter sda = new SqlDataAdapter(scom);
        //DataSet dsteleuser = new DataSet();
        //sda.Fill(dsteleuser);
        //con.Close();

        var pickListService = ServiceFactory.GetService<PickListService>();

        if (getUserInfo != null)
        {
               var surveyData = surveyService.GetPolls(getUserInfo.UserId, type,searchKey, pagingCriteria);
                var gridData = new
                {
                    total = surveyData.TotalPages,
                    page = surveyData.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
                    records = surveyData.TotalCount,
                    rows = surveyData.Entities
                };
                return jsonSerializer.Serialize(gridData);
        }
        else
        {
            var gridData = new
            {
                total = 1,
                page = 1,
                records = -302, 
                rows = new List<SurveyInfo>()
            };
            return jsonSerializer.Serialize(gridData);
        }
    }

    private string GetAllPolls()
    {

        var searchKey = string.Empty;
        if (Request["keyword"] != null || Request["keyword"] != "")
        {
            searchKey = Server.UrlDecode(Request["keyword"]);
        }
        else
        {
            searchKey = "";
        }
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);
       
        string type = "";
        if (Request["mode"] != null && Request["mode"] != "undefined")
        {
            type = Server.UrlDecode(Request["mode"]);
        }
        else
        {
            type = "All";
        }
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var getUserInfo = sessionService.GetLoginUserDetailsSession();

        var pickListService = ServiceFactory.GetService<PickListService>();

        if (getUserInfo != null)
        {
               var surveyData = surveyService.GetPollsAll(getUserInfo.UserId, type,searchKey, pagingCriteria);
                var gridData = new
                {
                    total = surveyData.TotalPages,
                    page = surveyData.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
                    records = surveyData.TotalCount,
                    rows = surveyData.Entities
                };
                return jsonSerializer.Serialize(gridData);
        }
        else
        {
            var gridData = new
            {
                total = 1,
                page = 1,
                records = -302, 
                rows = new List<SurveyInfo>()
            };
            return jsonSerializer.Serialize(gridData);
        }
    }

    private string GetPolls(int userid)
    {

        var searchKey = string.Empty;
        if (Request["keyword"] != null || Request["keyword"] != "")
        {
            searchKey = Server.UrlDecode(Request["keyword"]);
        }
        else
        {
            searchKey = "";
        }
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);

        string type = "";
        if (Request["mode"] != null && Request["mode"] != "undefined")
        {
            type = Server.UrlDecode(Request["mode"]);
        }
        else
        {
            type = "All";
        }
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        //var getUserInfo = sessionService.GetLoginUserDetailsSession();
        SqlConnection con = new SqlConnection();
        con = strconnectionstring();
        SqlCommand scom = new SqlCommand("sp_GetTeleUser", con);
        scom.CommandType = CommandType.StoredProcedure;
        scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = userid;
        SqlDataAdapter sda = new SqlDataAdapter(scom);
        DataSet dsteleuser = new DataSet();
        sda.Fill(dsteleuser);
        con.Close();

        var pickListService = ServiceFactory.GetService<PickListService>();

        //if (getUserInfo != null)
        //{
        var surveyData = surveyService.GetPolls(userid, type, searchKey, pagingCriteria);
        var gridData = new
        {
            total = surveyData.TotalPages,
            page = surveyData.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
            records = surveyData.TotalCount,
            rows = surveyData.Entities
        };
        return jsonSerializer.Serialize(gridData);
        //}
        //else
        //{
        //    var gridData = new
        //    {
        //        total = 1,
        //        page = 1,
        //        records = -302,
        //        rows = new List<SurveyInfo>()
        //    };
        //    return jsonSerializer.Serialize(gridData);
        //}
    }

    [WebMethod]
    private string GetOrderInofByUserId()
    {
        var jsonSerializer = new JavaScriptSerializer();
        var pagingCriteria = GetPagingCriteria(Request);

        var orderInfoService = ServiceFactory.GetService<OrderInfoService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var getUserInfo = sessionService.GetLoginUserDetailsSession();
        if (getUserInfo != null)
        {
            var orderData = orderInfoService.GetPollOrderInofByUserId(getUserInfo.UserId, pagingCriteria);

            var gridData = new
            {
                total = orderData.TotalPages,
                page = orderData.TotalPages > 0 ? pagingCriteria.PageIndex : 0,
                records = orderData.TotalCount,
                rows = orderData.Entities
            };
            return jsonSerializer.Serialize(gridData);
        }
        return string.Empty;
    }

    private PagingCriteria GetPagingCriteria(HttpRequest request)
    {
        var pagingCriteria = new PagingCriteria
        {
            PageIndex = ValidationHelper.GetInteger(Request["page"], 1),
            PageSize = ValidationHelper.GetInteger(Request["rows"], 10),
            SortColumn = Request["sidx"],
            SortOrder = Request["sord"]
        };

        return pagingCriteria;
    }

    private PagingCriteria GetDefaultPagingCriteria()
    {
        var pagingCriteria = new PagingCriteria
        {
            PageIndex = 1,
            PageSize = 10000,
            SortColumn = "pk_pollid",
            SortOrder = "DESC"
        };

        return pagingCriteria;
    }

    private string DeletePollByPollId()
    {
        int returnvalue = 1;
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var getUserInfo = sessionService.GetLoginUserDetailsSession();
        try
        {
            if (getUserInfo != null)
            {
                returnvalue = Pc.DeletePollByUserId(getUserInfo.UserId, ValidationHelper.GetInteger(Request["Id"].ToString(), 0));
                returnvalue = 0;
            }
            else
            {
                return "loggedout";
            }
        }
        catch (Exception)
        {

            returnvalue = 1;
        }
        return returnvalue.ToString();
    }
    
    private string ClosePollByPollId()
    {
        int returnvalue = 1;
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var getUserInfo = sessionService.GetLoginUserDetailsSession();
        try
        {
            if (getUserInfo == null)
            {
                return "loggedout";
            }
            if (getUserInfo != null)
            {
                returnvalue = Pc.UpdateStatusByUser(getUserInfo.UserId, ValidationHelper.GetInteger(Request["Id"].ToString(), 0), "Closed");
                string strsliderjs = ConfigurationManager.AppSettings["sliderjsfilepath"].ToString();
                string file_name = strsliderjs + "insightoslider" + Request["Id"].ToString() + ".js";
                if (File.Exists(file_name))
                {
                    File.Copy(file_name, file_name + "del", true);
                    File.Delete(file_name);
                }
            }
        }
        catch (Exception)
        {

            returnvalue = 1;
        }
        return returnvalue.ToString();
    }

    private string ActivatePollByPollId()
    {
        var surveyService = ServiceFactory.GetService<SurveyService>();
        Pc.UpdateStatus(ValidationHelper.GetInteger(Request["Id"].ToString(), 0), "Active");
        string returnvalue = "Activated";
        return returnvalue.ToString();
    }

    private string CopyPollByPollId() {
        string strRootURL = ConfigurationManager.AppSettings["RootURL"].ToString();
        string pollimagesfolder = ConfigurationManager.AppSettings["pollimagesfolder"].ToString();
        
        string retval = "Error";
        int copyPollId = 0;
        int pollId = 0;
        int selPollId = 0;

        string pollType = "";
        PollCreation Pc = new PollCreation();
        DataTable dtsettings = new DataTable();
        DataTable dtSettingsInfo = new DataTable();
        string mode = "";
        
        DataSet pollInfo = new DataSet();
        Hashtable typeHt = new Hashtable();
        string questionType = "";
        string editPollName = "";
        int widthpan = 0;
        string foldername = "";
        string pollName = "";
        int userId = 0;
        bool pn = true;
        int x = 1;
        string newPollName = "";
        try
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            if (userDetails != null)
            {
                userId = userDetails.UserId;
            }
            else
            {
                return "loggedout";
            }

            copyPollId = ValidationHelper.GetInteger(Request["Id"].ToString(), 0);
            DataTable tbAnswerOption = new DataTable();
            //GET ALL INFORMATION OF THE CURRENT POLL TO BE COPIED
            pollInfo = Pc.getPollInfo(copyPollId);
            pollName = pollInfo.Tables[0].Rows[0]["name"].ToString();
            pollType = pollInfo.Tables[0].Rows[0]["type"].ToString();
            while (pn)
            {
                newPollName = pollName + " (" + x.ToString() + ")";
                pn = Pc.getPollName(userId, newPollName);
                x++;
            }

            tbAnswerOption = Pc.getAnswerOptions(copyPollId);
            dtSettingsInfo = Pc.getPollSettingsInfo(copyPollId);
            
            //CREATE BASIC POLL FROM NAME AND TYPE TAKEN FROM EXISTING POLL
            pollId = Pc.AddNameandType(newPollName, pollType, userId, 0);

            //INSERT ALL DETAILS NOW
            int comments = 0;
            //Response.Write(pollInfo.Tables[0].Rows[0]["commentreq"].ToString());
            if ((pollInfo.Tables[0].Rows[0]["commentreq"].ToString().ToLower() == "true") || (pollInfo.Tables[0].Rows[0]["commentreq"].ToString() == "1")) {
                comments = 1;
            }
            Pc.InsertintoPollInfo(pollId, pollInfo.Tables[0].Rows[0][16].ToString(), pollInfo.Tables[0].Rows[0][18].ToString(), pollInfo.Tables[0].Rows[0][19].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()), comments, Convert.ToInt32(pollInfo.Tables[0].Rows[0]["uniquerespondent"].ToString()), pollInfo.Tables[0].Rows[0]["votebuttontext"].ToString(), pollInfo.Tables[0].Rows[0]["invitationmessage"].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isemailrequired"].ToString()), Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isphonerequired"].ToString()), pollInfo.Tables[0].Rows[0]["redirecturl"].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0]["showsubmitbutton"].ToString()), pollInfo.Tables[0].Rows[0]["submitbuttontext"].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0]["issamethankyoumessageused"].ToString()));

            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                foreach (DataRow row in tbAnswerOption.Rows)
                {
                    string answeroption = row["answeroption"].ToString();
                    Pc.SaveAnswerOptions(answeroption, pollId, row["InvitationThankYouMessage"].ToString());
                }
            }
            //INSERT POLL SETTINGS FROM ORIGINAL POLL
            Pc.InsertPollSettingsFromExistingPoll(copyPollId, pollId);

            try
            {
                if (pollType.ToLower() == "image")
                {
                    Directory.CreateDirectory(pollimagesfolder + pollId.ToString());

                    List<string> directoryContents = Directory.EnumerateFiles(pollimagesfolder + copyPollId.ToString()).ToList();
                    foreach (var item in directoryContents)
                    {
                        var arr = item.Split(new char[] { '\\' });
                        int thisarrlen = arr.Length;
                        string thisfilename = arr[thisarrlen - 1].ToString();
                        File.Copy(item, pollimagesfolder + pollId.ToString() + "//" + thisfilename, true);
                    }
                }
            }
            catch (Exception e)
            {
            }
            retval = GetLatestPoll(pollId);
        }
        catch (Exception)
        {
            retval = "Error";
        }
        return retval;
    }

    private string ReplacePollByPollId() {
        string strRootURL = ConfigurationManager.AppSettings["RootURL"].ToString();
        string pollimagesfolder = ConfigurationManager.AppSettings["pollimagesfolder"].ToString();
        
        string retval = "Error";
        int copyPollId = 0;
        int pollId = 0;
        int selPollId = 0;

        string pollType = "";
        PollCreation Pc = new PollCreation();
        DataTable dtsettings = new DataTable();
        DataTable dtSettingsInfo = new DataTable();
        string mode = "";
        
        DataSet pollInfo = new DataSet();
        Hashtable typeHt = new Hashtable();
        string questionType = "";
        string editPollName = "";
        int widthpan = 0;
        string foldername = "";
        string pollName = "";
        int userId = 0;
        bool pn = true;
        int x = 1;
        string newPollName = "";
        try
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            if (userDetails == null)
            {
                return "loggedout";
            }
            userId = userDetails.UserId;

            if (!String.IsNullOrWhiteSpace(Request["pollname"].ToString()))
            {
                pollName = Request["pollname"].ToString();
            }
            //if (!String.IsNullOrWhiteSpace(Request["polltype"].ToString()))
            //{
            //    pollType = Request["polltype"].ToString();
            //}

            copyPollId = ValidationHelper.GetInteger(Request["pollid"].ToString(), 0);

            DataTable tbAnswerOption = new DataTable();
            
            //GET ALL INFORMATION OF THE CURRENT POLL TO BE COPIED
            pollInfo = Pc.getPollInfo(copyPollId);
            
            pollType = pollInfo.Tables[0].Rows[0]["type"].ToString();

            tbAnswerOption = Pc.getAnswerOptions(copyPollId);
            dtSettingsInfo = Pc.getPollSettingsInfo(copyPollId);

            //CREATE BASIC POLL FROM NAME AND TYPE TAKEN FROM EXISTING POLL
            //SENDING EXISTING POLL ID AS PARENT POLL ID
            pollId = Pc.AddNameandType(pollName, pollType, userId, copyPollId);

            //INSERT ALL DETAILS NOW
            int comments = 0;
            //Response.Write(pollInfo.Tables[0].Rows[0]["commentreq"].ToString());
            if ((pollInfo.Tables[0].Rows[0]["commentreq"].ToString().ToLower() == "true") || (pollInfo.Tables[0].Rows[0]["commentreq"].ToString() == "1")) {
                comments = 1;
            }
            Pc.InsertintoPollInfo(pollId, pollInfo.Tables[0].Rows[0][16].ToString(), pollInfo.Tables[0].Rows[0][18].ToString(), pollInfo.Tables[0].Rows[0][19].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()), comments, Convert.ToInt32(pollInfo.Tables[0].Rows[0]["uniquerespondent"].ToString()), pollInfo.Tables[0].Rows[0]["votebuttontext"].ToString(), pollInfo.Tables[0].Rows[0]["invitationmessage"].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isemailrequired"].ToString()), Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isphonerequired"].ToString()), pollInfo.Tables[0].Rows[0]["redirecturl"].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0]["showsubmitbutton"].ToString()), pollInfo.Tables[0].Rows[0]["submitbuttontext"].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0]["issamethankyoumessageused"].ToString()));

            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                foreach (DataRow row in tbAnswerOption.Rows)
                {
                    string answeroption = row["answeroption"].ToString();
                    Pc.SaveAnswerOptions(answeroption, pollId, row["InvitationThankYouMessage"].ToString());
                }
            }
            //INSERT POLL SETTINGS FROM ORIGINAL POLL
            //Pc.InsertintoPollSettings(pollId, dtSettingsInfo.Rows[0]["pollbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questionfont"].ToString(), dtSettingsInfo.Rows[0]["questionbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questioncolor"].ToString(), dtSettingsInfo.Rows[0]["questionstyle"].ToString(), dtSettingsInfo.Rows[0]["questionfontweight"].ToString(), dtSettingsInfo.Rows[0]["questionunderline"].ToString(), dtSettingsInfo.Rows[0]["questionalign"].ToString(), dtSettingsInfo.Rows[0]["questionfontsize"].ToString(), dtSettingsInfo.Rows[0]["answerfont"].ToString(), dtSettingsInfo.Rows[0]["answercolor"].ToString(), dtSettingsInfo.Rows[0]["answerfontweight"].ToString(), dtSettingsInfo.Rows[0]["answerunderline"].ToString(), dtSettingsInfo.Rows[0]["answerstyle"].ToString(), dtSettingsInfo.Rows[0]["answeralign"].ToString(), dtSettingsInfo.Rows[0]["answerfontsize"].ToString(), dtSettingsInfo.Rows[0]["votebgcolor"].ToString(), dtSettingsInfo.Rows[0]["votefont"].ToString(), dtSettingsInfo.Rows[0]["votefontsize"].ToString(), dtSettingsInfo.Rows[0]["votestyle"].ToString(), dtSettingsInfo.Rows[0]["voteunderline"].ToString(), dtSettingsInfo.Rows[0]["votecolor"].ToString(), dtSettingsInfo.Rows[0]["votefontweight"].ToString(), dtSettingsInfo.Rows[0]["votealign"].ToString(), dtSettingsInfo.Rows[0]["sponsorlogo"].ToString(), dtSettingsInfo.Rows[0]["sponsortext"].ToString(), dtSettingsInfo.Rows[0]["logolink"].ToString(), dtSettingsInfo.Rows[0]["messagebgcolor"].ToString(), dtSettingsInfo.Rows[0]["messagecolor"].ToString(), dtSettingsInfo.Rows[0]["messagefont"].ToString(), dtSettingsInfo.Rows[0]["messagestyle"].ToString(), dtSettingsInfo.Rows[0]["messageunderline"].ToString(), dtSettingsInfo.Rows[0]["messagealign"].ToString(), dtSettingsInfo.Rows[0]["messagefontsize"].ToString(), dtSettingsInfo.Rows[0]["messagefontweight"].ToString(), dtSettingsInfo.Rows[0]["googletext"].ToString(), dtSettingsInfo.Rows[0]["twittertext"].ToString(), dtSettingsInfo.Rows[0]["fbtext"].ToString(), Convert.ToInt32(dtSettingsInfo.Rows[0]["countvotes"].ToString()), dtSettingsInfo.Rows[0]["sharetext"].ToString(), dtSettingsInfo.Rows[0]["votetext"].ToString(), Convert.ToInt32(dtSettingsInfo.Rows[0]["chartwidth"]), Convert.ToInt32(dtSettingsInfo.Rows[0]["chartheight"]));
            Pc.InsertPollSettingsFromExistingPoll(copyPollId, pollId);
            
            try
            {
                Directory.CreateDirectory(pollimagesfolder + pollId.ToString());

                List<string> directoryContents = Directory.EnumerateFiles(pollimagesfolder + copyPollId.ToString()).ToList();
                foreach (var item in directoryContents)
                {
                    var arr = item.Split(new char[] { '\\' });
                    int thisarrlen = arr.Length;
                    string thisfilename = arr[thisarrlen - 1].ToString();
                    File.Copy(item, pollimagesfolder + pollId.ToString() + "//" + thisfilename, true);
                }

            }
            catch (Exception e)
            {
            }
            
            retval = GetLatestPoll(pollId);
        }
        catch (Exception)
        {
            retval = "Error";
        }
        return retval;
    }

    [WebMethod]
    private string SavePollResponse(string pollid, string answeroptions, string respondentid, string comments)
    {
        PollCreation pollCreation = new PollCreation();
        string resultaccess = "";
        string returnvalue = "";
        string showSocialSharing = "0";
        string showTwitter = "0";
        string showGooglePlus = "0";
        string showFacebook = "0";
        int questionType = 1;
        DataSet pollInfo;
        DataTable dtSettingsInfo;
        int pollid1 = 0;
        int respondentId1 = 0;
        int votecount = 0;
        DataSet dspollInfo;
        string answerstring = "";
        string privateresultsmessage = "";
        string[] answerarrayval = new string[10];
        int arrlength = 0;
        string socialShareText = "";
        List<AnswerResults> answerresults = new List<AnswerResults>();
        PollResponse pollresponse = new PollResponse();
        string invitationmessage = "";
        int singleoptionanswerid = 0;
        try
        {

            Int32.TryParse(pollid, out pollid1);
            Int32.TryParse(respondentid, out respondentId1);

            pollCreation.UpdateIntoRespondent(respondentId1, "complete", (comments ?? ""));

            pollInfo = pollCreation.getPollInfo(pollid1);
            dtSettingsInfo = pollCreation.getPollSettingsInfo(pollid1);
            if (pollInfo.Tables[0].Rows[0][9].ToString() == "private")
            {
                resultaccess = "private";
                privateresultsmessage = System.Web.HttpUtility.HtmlEncode(pollInfo.Tables[0].Rows[0][10].ToString());
            }
            else
            {
                resultaccess = "public";
            }
            //MULTIPLE ANSWER OPTION
            if (Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()) == 2)
            {
                questionType = 2;
                var arrAnswers = answeroptions.Split(new char[] { ',' });
                for (int i = 0; i < arrAnswers.Length; i++)
                {
                    var answerid = 0;
                    Int32.TryParse(arrAnswers[i].Trim(), out answerid);
                    if (answerid > 0)
                    {
                        pollCreation.InsertIntoResponse(respondentId1, answerid);
                    }
                }
            }
            else
            {
                questionType = 1;
                var arrAns = answeroptions.Split(new char[] { ',' });
                for (int i = 0; i < 1; i++)
                {
                    var ansid = 0;
                    Int32.TryParse(arrAns[i].Trim(), out ansid);
                    if (ansid > 0)
                    {
                        singleoptionanswerid = ansid;
                        pollCreation.InsertIntoResponse(respondentId1, ansid);
                    }
                }
            }
            dspollInfo = pollCreation.getpollchartinfo(pollid1);

            if (dtSettingsInfo.Rows[0]["googletext"].ToString() != "" || dtSettingsInfo.Rows[0]["twittertext"].ToString() != "" || dtSettingsInfo.Rows[0]["fbtext"].ToString() != "")
            {
                showSocialSharing = "1";
                socialShareText = dtSettingsInfo.Rows[0]["sharetext"].ToString();
                if (socialShareText == "") {
                    socialShareText = "Share this poll";
                }
            }

            if (dtSettingsInfo.Rows[0]["googletext"].ToString() != "")
            {
                showGooglePlus = "1";
            }

            if (dtSettingsInfo.Rows[0]["twittertext"].ToString() != "")
            {
                showTwitter = "1";
            }

            if (dtSettingsInfo.Rows[0]["fbtext"].ToString() != "")
            {
                showFacebook = "1";
            }

            if (questionType == 2)
            {
                invitationmessage = dspollInfo.Tables[1].Rows[0]["InvitationThankYouMessage"].ToString();
            }
            else
            {
                DataRow[] drrightanswer = dspollInfo.Tables[1].Select("pk_answerid = " + singleoptionanswerid);
                invitationmessage = drrightanswer[0]["InvitationThankYouMessage"].ToString();
            }
            DataRow[] drchart = dspollInfo.Tables[2].Select("pollid = " + pollid1);
            if (resultaccess == "public")
            {
                if (Convert.ToBoolean(dtSettingsInfo.Rows[0]["countvotes"].ToString()) == true)
                {
                    votecount = Convert.ToInt32(dspollInfo.Tables[4].Rows[0][0].ToString());
                }
                arrlength = drchart.Length;
                if (respondentId1 == 0) {
                    if (arrlength == 2)
                    {
                        answerarrayval[0] = "40";
                        answerarrayval[1] = "60";
                    }
                    else if (arrlength == 3)
                    {
                        answerarrayval[0] = "20";
                        answerarrayval[1] = "30";
                        answerarrayval[2] = "50";
                    }
                    else if (arrlength == 4)
                    {
                        answerarrayval[0] = "10";
                        answerarrayval[1] = "20";
                        answerarrayval[2] = "30";
                        answerarrayval[3] = "40";
                    }
                    else if (arrlength == 5)
                    {
                        answerarrayval[0] = "10";
                        answerarrayval[1] = "15";
                        answerarrayval[2] = "20";
                        answerarrayval[3] = "25";
                        answerarrayval[4] = "30";
                    }
                    else if (arrlength == 6)
                    {
                        answerarrayval[0] = "5";
                        answerarrayval[1] = "10";
                        answerarrayval[2] = "15";
                        answerarrayval[3] = "20";
                        answerarrayval[4] = "22";
                        answerarrayval[5] = "28";
                    }
                    else if (arrlength == 7)
                    {
                        answerarrayval[0] = "5";
                        answerarrayval[1] = "10";
                        answerarrayval[2] = "12";
                        answerarrayval[3] = "13";
                        answerarrayval[4] = "17";
                        answerarrayval[5] = "20";
                        answerarrayval[6] = "23";
                    }
                    else if (arrlength == 8)
                    {
                        answerarrayval[0] = "5";
                        answerarrayval[1] = "7";
                        answerarrayval[2] = "9";
                        answerarrayval[3] = "11";
                        answerarrayval[4] = "13";
                        answerarrayval[5] = "15";
                        answerarrayval[6] = "19";
                        answerarrayval[7] = "21";
                    }
                    else if (arrlength == 9)
                    {
                        answerarrayval[0] = "1";
                        answerarrayval[1] = "3";
                        answerarrayval[2] = "6";
                        answerarrayval[3] = "9";
                        answerarrayval[4] = "10";
                        answerarrayval[5] = "14";
                        answerarrayval[6] = "17";
                        answerarrayval[7] = "19";
                        answerarrayval[8] = "21";
                    }
                    else if (arrlength == 10)
                    {
                        answerarrayval[0] = "1";
                        answerarrayval[1] = "3";
                        answerarrayval[2] = "5";
                        answerarrayval[3] = "7";
                        answerarrayval[4] = "9";
                        answerarrayval[5] = "11";
                        answerarrayval[6] = "13";
                        answerarrayval[7] = "15";
                        answerarrayval[8] = "17";
                        answerarrayval[9] = "19";
                    }
                }
                for (int ic = 0; ic < drchart.Length; ic++)
                {
                    AnswerResults ansresult = new AnswerResults();
                    ansresult.answertext = System.Web.HttpUtility.HtmlEncode(drchart[ic][1].ToString().Replace("\r", "").Replace("\n", ""));
                    //answerstring += "{ \"answertext\" : \"" + System.Web.HttpUtility.HtmlEncode(drchart[ic][1].ToString().Replace("\r", "").Replace("\n", "")) + "\" , ";
                    if (respondentId1 == 0)
                    {
                        //answerstring += " \"answerpercentage\" : " + answerarrayval[ic] + "} , ";
                        ansresult.answerpercentage = answerarrayval[ic] ;
                    }
                    else {
                        //answerstring += " \"answerpercentage\" : " + drchart[ic][3].ToString() + "} , ";
                        ansresult.answerpercentage = drchart[ic][3].ToString();
                    }
                    answerresults.Add(ansresult);
                }
                
            }
            else
            {
            }
            if (answerstring.Length > 2) {
                answerstring = answerstring.Substring(0, answerstring.Length - 3);
            }
        }
        catch (Exception e1)    
        {
            returnvalue = "error";
            resultaccess = "private";
        }
        if (respondentId1 == -111) { 
        
        }
        if(invitationmessage == ""){
            invitationmessage = "Thank you for your vote.";
        }
        pollresponse.answeroptions = answerresults;
        pollresponse.votecount = votecount.ToString();
        pollresponse.showsocialsharing = showSocialSharing;
        pollresponse.resultaccess = resultaccess;
        pollresponse.privateresultsmessage = privateresultsmessage;
        pollresponse.showgoogleplus = showGooglePlus;
        pollresponse.showtwitter = showTwitter;
        pollresponse.showfacebook = showFacebook;
        pollresponse.socialsharetext = socialShareText;
        pollresponse.invitationthankyoumessage = invitationmessage;
        JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        returnvalue = js.Serialize(pollresponse);

        return returnvalue;
    }

    [WebMethod]
    public string SavePollNameAndType(string Name, string Type)
    {
        string mode = "";
        int userId = 0;
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        userId = userDetails.UserId;
        string retval = "";
        bool pn = Pc.getPollName(userId, Name);
        try
        {
            
            if (pn == false)
            {
                int PollId = Pc.AddNameandType(Name, Type, userId, 0);

                retval = GetLatestPoll(PollId);
            }
            else
            {
                retval = "Duplicate";
            }

        }
        catch (Exception)
        {
            retval = "Error";
        }
        return retval;
    }

    public string CheckPollNameExists()
    {
        string pollname = "";
        int userId = 0;
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        string retval = "";
        bool pn = false;

        try
        {
            pollname = Request.QueryString["pollname"];

            userId = userDetails.UserId;
            pn = Pc.getPollName(userId, pollname);
            if (pn == false)
            {
                retval = "Success";
            }

        }
        catch (Exception)
        {
            retval = "Error";
        }
        return retval;
    }

    public string UpdateReplacedPollName()
    {
        string strRootURL = ConfigurationManager.AppSettings["RootURL"].ToString();
        string pollimagesfolder = ConfigurationManager.AppSettings["pollimagesfolder"].ToString();
        string pollname = "";
        int userId = 0;
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        string retval = "";
        PollCreation pc = new PollCreation();
        int pollid = 0;
        int parentpollid = 0;
        DataSet pollinfo;
        try
        {
            if (userDetails.UserId > 0)
            {
                pollid = Convert.ToInt32(Request.QueryString["pollid"]);
                pollinfo = pc.getPollInfo(pollid);
                parentpollid = Convert.ToInt32( pollinfo.Tables[0].Rows[0]["parentpollid"].ToString());
                pollname = Request.QueryString["pollname"];

                userId = userDetails.UserId;
                Pc.updateReplacedPollNameOnlyForThisUser(userDetails.UserId, pollid, pollname);
                try
                {
                    Directory.CreateDirectory(pollimagesfolder + pollid.ToString());
                    List<string> directoryContents = Directory.EnumerateFiles(pollimagesfolder + parentpollid.ToString()).ToList();
                    foreach (var item in directoryContents)
                    {
                        var arr = item.Split(new char[] { '\\' });
                        int thisarrlen = arr.Length;
                        string thisfilename = arr[thisarrlen - 1].ToString();
                        File.Move(item, pollimagesfolder + pollid.ToString() + "//" + thisfilename);
                    }

                }
                catch (Exception)
                {
                }
                retval = "Success";
            }
        }
        catch (Exception e)
        {
            retval = e.ToString();
        }
        return retval;
    }

    public string SavePollDetails() {

        Pc = new PollCreation();
        string pollbgcolor = "";

        string questioncolor = "";
        string questionbgcolor = "";
        string questionstyle = "";
        string questionfont = "";
        string questionfontweight = "";
        string questionfontsize = "";
        string questionunderline = "";
        string questionalign = "";

        string answercolor = "";
        string answerstyle = "";
        string answerfont = "";
        string answerfontweight = "";
        string answerfontsize = "";
        string answerunderline = "";
        string answeralign = "";

        string votebgcolor = "";
        string votecolor = "";
        string votestyle = "";
        string votefont = "";
        string votefontweight = "";
        string votefontsize = "";
        string voteunderline = "";
        string votealign = "";

        string sponsorlogo = "";
        string sponsortext = "";
        string logolink = "";

        string insightobrand = "yes";

        string messagecolor = "";
        string messagebgcolor = "";
        string messagestyle = "";
        string messagefont = "";
        string messagefontweight = "";
        string messagefontsize = "";
        string messageunderline = "";
        string messagealign = "";

        int iframewidth = 0;
        int iframeheight = 0;
        string googletext = "";
        string twittertext = "";
        string fbtext = "";
        int countvotes = 0;
        string sharetext = "";
        string votetext = "";
        string imgrecmheight = "";
        string imgrecwidth = "";

        int recmheight = 0;
        string recwidth = "";
        int recmwidth = 0;
        string position = "";
        string sliderdisplay = "";
        string freqofvisit = "";
        string countryname = "";
        string cityname = "";
        string devicename = "";
        int chartwidth = 0;
        int chartheight = 0;
        int autoadjustdimensions = 1;
        string answeroption = "";

        string embedcode = "";
        string launchtypenumber = "";
        string resultaccess = "";
        string resulttext = "";
        string poweredby = "";
        int  questiontype = 0;
        string questiontext = "";
        int  commentreq = 0;
        string videolink = "";
        string imagelink = "";
        string uniquerespondent = "";

        int pollId = 0;
        int userId = 0;
        try
        {
            try
            {
                var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
                userId = userDetails.UserId;
            }
            catch (Exception)
            {
                return "SessionOver";
            }

            Int32.TryParse(Request["pollid"], out pollId);
            if (Request["votecount"] == "1")
            {
                countvotes = 1;
            }
            else
            {
                countvotes = 0;
            }

            questionfontweight = Request["questionbold"];
            questionstyle = Request["questionitalic"];
            questionunderline = Request["questionunderline"];
            questioncolor = Request["questioncolor"];
            questionbgcolor = Request["questionbgcolor"];
            questionstyle = Request["questionstyle"];
            questionfont = Request["questionfont"];
            questionfontsize = Request["questionfontsize"];
            questionalign = Request["questionalign"];

            pollbgcolor = Request["pollbgcolor"];

            answerfontweight = Request["answerbold"];
            answerstyle = Request["answeritalic"];
            answerunderline = Request["answerunderline"];
            answercolor = Request["answercolor"];
            answerstyle = Request["answerstyle"];
            answerfont = Request["answerfont"];
            answerfontsize = Request["answerfontsize"];
            answeralign = Request["answeralign"];

            votefontweight = Request["votebold"];
            votestyle = Request["voteitalic"];
            voteunderline = Request["voteunderline"];
            votecolor = Request["votecolor"];
            votebgcolor = Request["votebgcolor"];
            votestyle = Request["votestyle"];
            votefont = Request["votefont"];
            votefontsize = Request["votefontsize"];
            votealign = Request["votealign"];

            messagefontweight = Request["messagebold"];
            messagestyle = Request["messageitalic"];
            messageunderline = Request["messageunderline"];
            messagecolor = Request["messagecolor"];
            messagebgcolor = Request["messagebgcolor"];
            messagestyle = Request["messagestyle"];
            messagefont = Request["messagefont"];
            messagefontsize = Request["messagefontsize"];
            messagealign = Request["messagealign"];

            if (Request["addsponsor"] == "1")
            {
                sponsorlogo = Request["sponsorlogo"];
                sponsortext = Request["sponsortext"];
                logolink = Request["logolink"];
            }
            Int32.TryParse(Request["chartwidth"], out chartwidth);
            Int32.TryParse(Request["chartheight"], out chartheight);

            insightobrand = (Request["insightobrand"] ?? "");
            resultaccess = (Request["keepresultsprivate"] ?? "");
            Int32.TryParse(Request["autoadjustdimensions"], out autoadjustdimensions);

            if (Request["commentreq"] == "1")
            {
                commentreq = 1;
            }

            if (Request["questiontype"] == "1")
            {
                questiontype = 1;
            }

            resultaccess = Request["resultaccess"];
            resulttext = Request["resulttext"];
            poweredby = Request["poweredby"];
            questiontext = Request["questiontext"];
            videolink = Request["videolink"];
            imagelink = Request["imagelink"];
            uniquerespondent = Request["uniquerespondent"];

            Pc.UpdatePollDetails(pollId, userId, resultaccess, resulttext, poweredby,  questiontype, questiontext, commentreq, videolink, imagelink, uniquerespondent);
            Pc.UpdatePollSettings(pollId, pollbgcolor, questionfont, questionbgcolor, questioncolor, questionstyle, questionfontweight, questionunderline, questionalign, questionfontsize, answerfont, answercolor, answerfontweight, answerunderline, answerstyle, answeralign, answerfontsize, votebgcolor, votefont, votefontsize, votestyle, voteunderline, votecolor, votefontweight, votealign, sponsorlogo, sponsortext, logolink, messagebgcolor, messagecolor, messagefont, messagestyle, messageunderline, messagealign, messagefontsize, messagefontweight, googletext, twittertext, fbtext, countvotes, sharetext, votetext, chartwidth, chartheight, autoadjustdimensions,"");
            Pc.UpdatePollDimensions(pollId, userId, iframeheight, iframewidth, recmheight, recmwidth);
            return "Success";
        }
        catch (Exception)
        {
            return "Error";
        }
    }

    public string SavePollInteraction(string pollid, string respondentid, string email, string phone, string redirect){
        string retval = "";
        int poll = 0;
        int respondent = 0;
        int respondentidcookie = 0;
        int redirects = 0;
        try
        {
            Int32.TryParse(redirect, out redirects);
            Int32.TryParse(pollid, out poll);
            Int32.TryParse(respondentid, out respondent);
            Pc = new PollCreation();
            Pc.UpdatePollLeadGen(poll, respondent, email, phone, redirects.ToString());
            retval = "OK";
            //if(Request.Cookies["RespondentId" + pollid] != null){
            //    Int32.TryParse(Request.Cookies["RespondentId" + pollid].Value, out respondentidcookie);
            //    if (respondent == respondentidcookie)
            //    {
            //        Pc.UpdatePollLeadGen(poll, respondent, email, phone, redirects.ToString());
            //        retval = "OK";
            //    }
            //}
        }
        catch (Exception e1)
        {
            retval = "";
        }
        return retval;
    }

    public string SendEmail(){
            string templatename = "PollsConfirmationofEmailaddress";
            string viewData1;
            JavaScriptSerializer js1 = new JavaScriptSerializer();
            js1.MaxJsonLength = int.MaxValue;
            clsjsonmandrill prmjson1 = new clsjsonmandrill();
            int userId = 0;
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            string EmailID = "";
            string username = "";
            string strUrl = ConfigurationManager.AppSettings["RootURL"].ToString();
            string OpturlParams = "";//EncryptHelper.EncryptQuerystring("", "ID=" + MailAddress);
            string unsubscribeurl = "";//"<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";
            string loginurl = "";//EncryptHelper.EncryptQuerystring(strUrl + "/Login.aspx", "UserId=" + Userid);
            try
            {
                if (Convert.ToInt32(userDetails.ActivationFlag) > 0)
                {
                    return "";
                }
                username = userDetails.FirstName;
                userId = userDetails.UserId;
                EmailID = userDetails.Email;
                prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
                prmjson1.name = templatename;
                viewData1 = js1.Serialize(prmjson1);
                OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + EmailID);
                unsubscribeurl = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";
                loginurl = EncryptHelper.EncryptQuerystring(strUrl + "/Login.aspx", "UserId=" + userId);

                string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";


                WebClient request1 = new WebClient();
                request1.Encoding = System.Text.Encoding.UTF8;
                request1.Headers["Content-Type"] = "application/json";
                byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));

                string response1 = System.Text.Encoding.ASCII.GetString(resp1);


                clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);

                string bodycontent = jsmandrill11.code;


                string viewData;
                JavaScriptSerializer js = new JavaScriptSerializer();
                js.MaxJsonLength = int.MaxValue;
                var prmjson = new clsjsonmandrill.jsonmandrillmerge();
                prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
                prmjson.template_name = templatename;


                List<clsjsonmandrill.template_content> mtags = new List<clsjsonmandrill.template_content>();
                mtags.Add(new clsjsonmandrill.template_content { name = "std_content00", content = "<div> Dear *|FNAME|*,</div>" });
                if (templatename == "PollsForSurveyUserWhoSignedUpforPolls")
                {
                    //mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div><a href='*|ACTIVATION_LINK|*' style='font-size:14px;'><img src='http://insighto.com/images/BtnPollsPlan.png' alt=''/></a><br/></div>" });
                }
                else
                {
                    mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div><a href='*|ACTIVATION_LINK|*' style='font-size:14px;'><img src='http://insighto.com/images/BtnCnfmEmailAddYellow.png' alt=''/></a><br/></div>" });
                }
                mtags.Add(new clsjsonmandrill.template_content { name = "std_content02", content = "<div><p style='font-size:8pt;font-family:Arial,Sans-Serif'> *|UNSUBSCRIBEINSIGHTO|* </p></div>" });

                List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
                mvars.Add(new clsjsonmandrill.merge_vars { name = "FNAME", content = username });
                mvars.Add(new clsjsonmandrill.merge_vars { name = "ACTIVATION_LINK", content = loginurl });
                mvars.Add(new clsjsonmandrill.merge_vars { name = "UNSUBSCRIBEINSIGHTO", content = unsubscribeurl });
                prmjson.template_content = mtags;
                prmjson.merge_vars = mvars;


                viewData = js.Serialize(prmjson);


                string url = "https://mandrillapp.com/api/1.0/templates/render.json";


                WebClient request = new WebClient();
                request.Encoding = System.Text.Encoding.UTF8;
                request.Headers["Content-Type"] = "application/json";
                byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

                string response = System.Text.Encoding.ASCII.GetString(resp);

                string unesc = System.Text.RegularExpressions.Regex.Unescape(response);

                string first4 = unesc.Substring(0, 9);

                string last2 = unesc.Substring(unesc.Length - 2, 2);


                string unesc1 = unesc.Replace(first4, "");
                string unescf = unesc1.Replace(last2, "");


                clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);

                //   string bodycontent = jsmandrill1.code;


                mailMessage = new MailMessage();
                mailMessage.To.Clear();
                mailMessage.Sender = new MailAddress("getstarted@insighto.com", "Insighto.com");
                // mailMessage.Sender = new MailAddress(Sendmails.FROM_MAILADDRESS, "Insighto.com");
                mailMessage.From = new MailAddress("getstarted@insighto.com", "Insighto.com");

                mailMessage.Subject = "[Reminder - from Insighto.com] - Please confirm your email address";

                mailMessage.To.Add(EmailID);
                //  mailMessage.Headers.Add("survey_id", "6666");
                // mailMessage.Headers.Add("X-MC-Tags", "survey_respondent");
                mailMessage.Body = unescf;
                //if (Sendmails.SENDER_TOCCADDRESSES != null && Convert.ToString(Sendmails.SENDER_TOCCADDRESSES.Trim()).Length > 0)
                //{
                //    mailMessage.CC.Add(Sendmails.SENDER_TOCCADDRESSES.Trim());
                //}
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.IsBodyHtml = true;
                mailMessage.Priority = MailPriority.Normal;
                smtpMail = new SmtpClient();
                smtpMail.Host = "smtp.mandrillapp.com";
                smtpMail.Port = 587;
                smtpMail.EnableSsl = true;
                smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpMail.Credentials = new System.Net.NetworkCredential("ram@knowience.com", "T74TJc3EZPYaIcvdBg--Dg");

                try
                {

                    smtpMail.Send(mailMessage);
                }
                catch (SmtpFailedRecipientsException ex)
                {
                    return "";
                }

            }
            catch (Exception)
            {
                return "";
            }    

            mailMessage.Dispose();
            return "sent";
        }

    public string SaveWeblinkBgColor()
    {
        int pollid = 0;
        string color = "";
        int userid = 0;
        string retval = "";
        try
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            if (userDetails == null)
            {
                retval = "loggedout";
            }
            userid = userDetails.UserId;

            if (!String.IsNullOrWhiteSpace(Request["color"].ToString()))
            {
                color = Request["color"].ToString();
            }
            if (String.IsNullOrWhiteSpace(color))
            {
                color = "";
            }
            if (!String.IsNullOrWhiteSpace(Request["pollid"].ToString()))
            {
                Int32.TryParse(Request["pollid"].ToString(), out pollid);
            }

            if (userid > 0 && pollid > 0)
            {
                Pc = new PollCreation();
                Pc.UpdatePollWebLinkBgColor(pollid, userid, color);
            }
            retval = "saved";
        }
        catch (Exception)
        {
            retval = "Error";
        }
        return retval;
    }
    
    public string SaveCompanyName()
    {
        string retval = "";
        int userId = 0;
        string companyname = "";
        try
        {
            companyname = Request["companyname"];
            companyname = Server.HtmlEncode(companyname.Trim());
            if (companyname.Length > 0)
            {
                var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
                if (userSessionDetails != null)
                {
                    userId = userSessionDetails.UserId;
                    var userDetails = ServiceFactory<UsersService>.Instance.GetUserDetails(userId);
                    bool savecompanyname = ServiceFactory<UsersService>.Instance.UpdateCompanyName(userId, companyname);
                }
                retval = companyname;
            }
        }
        catch (Exception)
        {
            retval = "Error";
        }
        return retval;
    }
    
    public class PollResponse
    {
        public PollResponse(){}
        public string votecount { get; set; }
        public string showsocialsharing { get; set; }
        public string resultaccess { get; set; }
        public string privateresultsmessage { get; set; }
        public string showgoogleplus { get; set; }
        public string showtwitter { get; set; }
        public string showfacebook { get; set; }
        public string socialsharetext { get; set; }
        public string invitationthankyoumessage { get; set; }
        public List<AnswerResults> answeroptions { get; set; }
    }
    
    [Serializable]
    public class AnswerResults{
        public AnswerResults() { }
        public string answertext {get; set;}
        public string answerpercentage {get; set;}
    }

    public string GetLatestPoll(int pollid)
    {
        var surveyService = ServiceFactory.GetService<SurveyService>();
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var getUserInfo = sessionService.GetLoginUserDetailsSession();
        var pagingCriteria = GetDefaultPagingCriteria();
        var pickListService = ServiceFactory.GetService<PickListService>();
        string type = "";
        if (Request["mode"] != null && Request["mode"] != "undefined")
        {
            type = Server.UrlDecode(Request["mode"]);
        }
        else
        {
            type = "All";
        }
        var jsonSerializer = new JavaScriptSerializer();

        try
        {
            if (getUserInfo != null)
            {
                var surveyData = surveyService.GetPollsAll(getUserInfo.UserId, type, "", pagingCriteria);
                List<PollInfo> pollInfo = new List<PollInfo>();
                pollInfo = surveyData.Entities;
                return jsonSerializer.Serialize(pollInfo.Where(a => a.pk_pollid == pollid).ToList());
            }
        }
        catch (Exception)
        {
            return "Error";
        }
        return "";
    }
}
