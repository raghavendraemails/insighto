﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PollRespondentIframe.aspx.cs" Inherits="Polls_new_design_PollRespondentIframe" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="https://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
    <meta name="HandheldFriendly" content="true" />
    <meta name="viewport" content="width=device-width, maximum-scale=1.0" />
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="https://insightopollsssl-a59.kxcdn.com/scripts/jquery-1.11.1.min.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="css/styleleadgen.css" />
    <link rel="stylesheet" type="text/css" href="css/poll-iframe.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="https://insightopollsssl-a59.kxcdn.com/css/jquery.alerts.css" />
    <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.css" type="text/css" rel="stylesheet" media="screen,projection" />

    <link href="https://insightopollsssl-a59.kxcdn.com/jalerts/jAlert-v2.css" rel="stylesheet" type="text/css" />
    <meta property="og:type" content="website"/>
    <meta property="fb:app_id" content="390001971057863"/>
    <meta property="og:image:width" content="750"/>
    <meta property="og:image:height" content="200"/>
    <style>
        body{
            /*background-color: #9CCC65;*/
            background-color: #FFF;
            /* Remove space around window edge */
            margin-top: 0;
            margin-left: 0;
            margin-right: 0;
            margin-bottom: 0;
        }
                .dialogContainer {
            /*
  Drop Shadow
  a. Horizontal shadow
  b. Vertical shadow
  c. Blur
  d. Spread
  e. Color
  */
            -webkit-box-shadow: 0 -2px 25px 0 rgba(0, 0, 0, 0.15), 0 13px 25px 0 rgba(0, 0, 0, 0.3);
            -moz-box-shadow: 0 -2px 25px 0 rgba(0, 0, 0, 0.15), 0 13px 25px 0 rgba(0, 0, 0, 0.3);
            /*-moz-box-shadow: inherit;
            -webkit-box-shadow: inherit;
            box-shadow: inherit;*/
            box-shadow: 0 -2px 25px 0 rgba(0, 0, 0, 0.15), 0 13px 25px 0 rgba(0, 0, 0, 0.3);
            background-color: #9CCC65;
            /* Internal padding */
            padding-top: 0;
            padding-left: 0;
            padding-right: 0;
            padding-bottom: 0;
            /* External padding */
            margin-top: 0;
            margin-left: auto;
            margin-right: auto;
            margin-bottom: 0;
            text-align: center;
        }

    </style>
</head>
<body id="mainbody" style="overflow:hidden;">
 <form id="form1" runat="server" style="width:100%;height:100%;">

    <div id="innerdiv" class="center-align m12 s12 l12" style="padding-top:2%;margin:0 auto;float:none;padding-bottom:2%;max-width:650px;">
      <asp:ScriptManager ID="ScriptMgr1" runat="server" AsyncPostBackTimeout="100000">
    <Scripts>
    </Scripts>  
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnUniqueRes" runat="server" />
<asp:HiddenField ID="hdn1" runat="server" ClientIDMode="Static" Value="11111111"/>
<asp:HiddenField ID="hdnRespondentId" runat="server" />
<asp:HiddenField ID="hdnSrcUrl" runat="server" />
<asp:HiddenField ID="hdnQuestionFont" runat="server" />
<asp:HiddenField ID="hdnQuestionText" runat="server" />
<asp:HiddenField ID="hdnQuestionBold" runat="server" />
<asp:HiddenField ID="hdnQuestionItalic" runat="server" />
<asp:HiddenField ID="hdnQuestionUL" runat="server" />
<asp:HiddenField ID="hdnQuestionAlign" runat="server" />
<asp:HiddenField ID="hdnQuestionFSize" runat="server" />
<asp:HiddenField ID="hdnAnswerFont" runat="server" />
<asp:HiddenField ID="hdnAnswerBold" runat="server" />
<asp:HiddenField ID="hdnAnswerItalic" runat="server" />
<asp:HiddenField ID="hdnAnswerUL" runat="server" />
<asp:HiddenField ID="hdnAnswerAlign" runat="server" />
<asp:HiddenField ID="hdnAnswerFSize" runat="server" />
<asp:HiddenField ID="hdnVoteFont" runat="server" />
<asp:HiddenField ID="hdnVoteBold" runat="server" />
<asp:HiddenField ID="hdnVoteItalic" runat="server" />
<asp:HiddenField ID="hdnVoteUL" runat="server" />
<asp:HiddenField ID="hdnVoteFSize" runat="server" />
<asp:HiddenField ID="hdnVoteAlign" runat="server" />
<asp:HiddenField ID="hdnMsgBgColor" runat="server" />
<asp:HiddenField ID="hdnMsgColor" runat="server" />
<asp:HiddenField ID="hdnMsgFont" runat="server" />
<asp:HiddenField ID="hdnMsgBold" runat="server" />
<asp:HiddenField ID="hdnMsgItalic" runat="server" />
<asp:HiddenField ID="hdnMsgUL" runat="server" />
<asp:HiddenField ID="hdnMsgFSize" runat="server" />
<asp:HiddenField ID="hdnMsgAlign" runat="server" />
<asp:HiddenField ID="hdnPollBgColor" runat="server" />
<asp:HiddenField ID="hdnQuestBgColor" runat="server" />
<asp:HiddenField ID="hdnQuestColor" runat="server" />
<asp:HiddenField ID="hdnAnswerColor" runat="server" />
<asp:HiddenField ID="hdnVoteBgColor" runat="server" />
<asp:HiddenField ID="hdnVoteColor" runat="server" />
<asp:HiddenField ID="hdnWidth" runat="server" />
<asp:HiddenField ID="hdnHeight" runat="server" />
<asp:HiddenField ID="hdnRecHeight" runat="server" />
<asp:HiddenField ID="hdnRecWidth" runat="server" />
<asp:HiddenField ID="hdnFbText" runat="server" />
<asp:HiddenField ID="hdnTwitterText" runat="server" />
<asp:HiddenField ID="hdnPollSource" runat="server" Value="weblink" />
<asp:HiddenField ID="hdn" runat="server" />
<asp:HiddenField ID="hdnIsPoweredByShowing" runat="server" />
<asp:HiddenField ID="hdnIsSocialSharingShowing" runat="server" />
<asp:HiddenField ID="hdnIsSponsorLogoShowing" runat="server" />
        
<asp:HiddenField ID="hdnPollType" runat="server" />
<asp:HiddenField ID="hdnGoogleText" runat="server" />
<asp:HiddenField ID="hdnFBImage" runat="server" />
<asp:HiddenField ID="hdnLogo" runat="server" />
<asp:HiddenField ID="hdnPollURLLong" runat="server" />
<asp:HiddenField ID="hdnPollID" runat="server" />
<asp:HiddenField ID="hdnPollURL" runat="server" />
<asp:HiddenField runat="server" ID="hdnRedirectUrl" ClientIDMode="Static"/>
<asp:HiddenField runat="server" ID="hdnSource" ClientIDMode="Static"/>
<asp:HiddenField runat="server" ID="hdnHasRedirectUrl" ClientIDMode="Static"/>
    
        <div class="center-align dialogContainer card" id="polldiv" style="display:none;overflow:hidden !important;position:relative;">
        <div class="card-content"style="padding:0;" >
                    <div id="imagepreview" runat="server" style="display:none;margin-top:0px;" class=""><img src="images/ImagePoll.jpg" id="imgPreview" runat="server" class="responsive-img" style="width:100%;max-width:100%;max-height:132px;"/></div>
			        <div id="videopreview" runat="server" class="video-container" style="display:none;margin-top:0px;margin-left:0px;"></div>
                    <div class="questionpreviewcss row col m12" id="questiondiv" style="padding-left:5%;padding-right:3%;"><asp:Label ID="lblQuestion" runat="server"></asp:Label></div>
        

                     <div id="singleradbtns" runat="server" class="row" style="margin-left:2%;margin-bottom:0;">
                     <div class="col m12 row" style="margin-bottom:0;">
                         <asp:RadioButtonList ID="radAnswerOptions" runat="server">
                         </asp:RadioButtonList>
                         </div> 
                     </div>
                     <div id="multipleckbbtns"  style="margin-left:4%;display:none;margin-bottom:0;" runat="server">
                    <div class="radiopolltheme col m12 row" style="margin-bottom:0;">
                        <asp:CheckBoxList ID="ckbAnswerOptions" runat="server" style="margin-left:4%;">
                        </asp:CheckBoxList>
                    </div>
                     </div>
                     <div id="messagebox" style="display:none" runat="server">
                    <div class="messageprivate chip">
                        <asp:Label ID="lblMessage" runat="server" Text="Label"></asp:Label></div>
                     </div>
                    <div id="purejschart" style="display:none;float:left;margin-left:8px;margin-bottom:0px;margin-bottom:0px;" class="row pad-10"></div>

                    <div class="row" id="commentrow" runat="server" style="white-space:nowrap;">
                        <div class="col m12  s12 input-field" id="commentbox" style="display:none;padding-left:5%;padding-right:5%;" runat="server">
                            <input id="txtComments"  class="textBoxMedium txtAns txtcomments col m12" runat="server" style="height: 2rem;"/>
                            <label for="txtComments" id="lblTxtComments" style="padding-left:5%;text-align:left;" >Comments</label>
                        </div>
                     </div>
                    <div class="col m12 s12 center-align row pad-15 input-field" id="votebuttonwithnosponsor" runat="server">
                        <asp:Button id="btnVoteAn" runat="server" Text="Vote and View results" BorderStyle="None"/>
                    </div>
                   
                    </div> 
                         <div class="col m12 s12 pad-15 row hide" id="votebuttonwithsponsor" runat="server" style="position:fixed;bottom:40px;border-left-width:5px;width:100%;margin:0;">
                                 <div class="col m6 s6 left-align no-pad-bot" id="votebuttoncontent">
                                    <div id="socialshare" style="display:none;">
                                        <div style="padding-bottom:1px;margin:0;"><asp:Label runat="server" ID="lblShare" style="font-size:12x;"></asp:Label></div>
                                        <div class="floatL socialmargin" id="googleicon" runat="server" style="display:none;margin-top:0;">
                                            <a href="#" onclick="gPlus('');" title="+1"><img src="images/google.png" /></a>
                                        </div>
                                        <div class="floatL socialmargin" id="twittericon" runat="server" style="display:none;margin-top:0;">
                                            <a onclick="clicked_twitter_button(this);" href="#" runat="server" id="twitter"  data-url="false" data-size="large" data-count="none" data-dnt="true" ><img src="images/twitter.png" /></a>
                                        </div>
                                        <div class="floatL socialmargin" id="fbicon" runat="server" style="display:none;margin-top:0;">
                                            <a href="#" onclick="fbshareCurrentPage()" ><img src="images/facebook.png" alt="Share on Facebook"/></a>
                                          
                                        </div>
                                    </div>             
                             </div>
                         </div>
                    <div class="col m12 s12 center no-pad center-align" id="poweredbydiv" runat="server" style="display:none;margin-bottom:0;position:fixed;left:0;bottom:0;width:100%;max-height:20px;height:18px;background-color:#e7e7e7;line-height:1.5;">
                        <div id="poweredby" runat="server" style="width:100%;font-size:11px;">Powered by 
                        	<a href="https://www.insighto.com/Polls/" target="_blank" style="color: rgb(0, 0, 0); font-family: Arial; font-weight: normal; font-style: normal; text-decoration: none;">
                        		<img src="https://test.insighto.com/assets/img/insighto-logo.png" style="max-height: 14px;margin: 3px 0 -5px 0;"> <span style="color: #ff6c00;"> Polls</span>
				</a>
                        </div>
                    </div>
    </div>
     <div class="classmsg" style="display:none;" ><div class="msgres">You have already voted for this poll</div></div>
        <asp:HiddenField runat="server" id="resppollid" ClientIDMode="Static" />
        <asp:HiddenField runat="server" id="respid" ClientIDMode="Static" />
        <asp:Label runat="server" ID="dojavascript" ClientIDMode="Static"></asp:Label>
        <asp:HiddenField runat="server" ID="lblcontent" ClientIDMode="Static"/>
        <asp:Label runat="server" ID="fusionchartscontent" ClientIDMode="Static"></asp:Label>

        
        <div class="row" id="pollleadgen" style="display:none;margin:0 !important;">
            <div class="textpollthemeresults row" id="pollleadgenmain" style="width:100%;height:100%;margin:2% 0 !important;">
                <div class="col m12" id="pollleadgencontent" style="padding-top:5px;height:100vh;">
                    <div id="invitationmessagepreview" runat="server" style="display:none;margin-left:2%;margin-right:2%;text-align:center;margin-bottom:8px;" class="row col m12 chip"></div>
                    <div id="contactdetailscollectiondiv" runat="server" style="display:none;">
                        <div class="input-field phn-icon" id="phonenumberdiv" runat="server" style="display:none;padding-left:4%;padding-right:4%;margin-top:1px;">
                            <asp:TextBox ID="phonenumber" runat="server" MaxLength="13"></asp:TextBox>
                            <label for="phonenumber" id="lblphonenumber">Mobile Number </label>
                        </div>
                            
                        <div class="input-field phn-icon" id="emaildiv" runat="server" style="display:none;padding-left:4%;padding-right:4%;margin-top:1px;">
                            <asp:TextBox ID="email" runat="server" MaxLength="100"></asp:TextBox>
                            <label for="email" id="lblemail">Email </label>
                        </div>
                    </div>
                    <div id="submitbuttonpreviewdiv" class="row center center-align" style="display:none;margin-bottom:0% !important;" runat="server">
                        <div class="input-field" style="margin-top: 25px;">
                            <a href="#" ID="btnSubmitLeadGen" runat="server" class="viewandvoteimagepreview" style="margin: 0px auto;border: none;width: auto;display: inline;text-align: center;padding: 10px;" onclick="submitResponse()">Submit</a>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>   
                    <div id="thankyouoverlay" style="display:none;opacity:1;width:100%;height:100%;background-color:lightgreen;padding-top:15%;" class="chip">
                        <asp:Label ID="thankyouoverlaymessage" runat="server" CssClass="chip"></asp:Label>
                    </div>
     
                    <div class="col m6 s6 right right-align" id="sponsormaindiv" runat="server" style="display:none;">
                        <p class="center-align" style="margin:0 0 2px 0;"><asp:Label ID="lblSpoRes" Text="Sponsored By" runat="server" style="font-size:12px;"></asp:Label></p>
                        <div class="center-align">
                            <a href="#" id="logolink" target="_blank" runat="server">
                                <img src="images/logoPlaceholder2.png" runat="server" id="logo" style="max-width:90px;max-height:90px;height:auto;width:60% !important" class="responsive-img "/>
                            </a>
                        </div>
                    </div>

                    <div class="col m6 s6 left left-align" id="votebuttononleft" runat="server" style="display:none;">
                        <div class="center-align">
                                 <asp:Button id="btnVoteLeft" runat="server" Text="Vote and View results" BorderStyle="None"/>
                        </div>
                    </div>
    </div>


</form>
    <script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>
    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
    <script src="/polls/scripts/respondent-iframe.js?ver=<%=DateTime.Now.Ticks.ToString() %>" type="text/javascript"></script>
    <script src="/polls/scripts/leadgen.js" type="text/javascript"></script>
    <script src="https://insightopollsssl-a59.kxcdn.com/jalerts/jAlert-v2.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://insightopollsssl-a59.kxcdn.com/scripts/jquery.alerts.js"></script>
    <script type="text/javascript" src="https://insightopollsssl-a59.kxcdn.com/scripts/jquery-migrate-1.2.1.js"></script>
    <script src="js/materialize.js"></script>
    <script src="js/init.js"></script>


        <script type="text/jscript" src="https://connect.facebook.net/en_US/all.js"></script>

   <script>
       window.fbAsyncInit = function () {
           FB.init({
               appId: '390001971057863',
               xfbml: true,
               version: 'v2.6'
           });
       };

       (function (d, s, id) {
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) { return; }
           js = d.createElement(s); js.id = id;
           js.src = "//connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));

       function facebookShare() {
           result = $("#hdnSrcUrl").val() + "&title=" + $("#lblQuestion").html() + "&description=Click to vote&image=" + $("#hdnFBImage").val();

           FB.ui({
               method: 'share',
               href: $("#hdnSrcUrl").val() + "&" + result,
           }, function (response) {
               if (response.post_id != null) {
                   Materialize.toast("Shared Successfully on Facebook", 2000);
               }
           });
       }

</script>
     <script type="text/javascript">
         var text = "Click to vote";
         $(document).ready(function () {
             if ($("#hdnPollType").val() == "video") {
                 //videolink = $("#videopreview iframe").prop("src");
                 text = "WATCH THE VIDEO and give your opinion";
             }
         });

         function fbshareCurrentPage() {
             var videolink = "";
             var result = $("#hdnSrcUrl").val();
             window.open("https://www.facebook.com/sharer/sharer.php?u=" + result, 'popupwindow', 'scrollbars=yes,width=800,height=400').focus();
             return false;
         }

         function gPlus(url) {
             var result = $("#hdnSrcUrl").val();
             window.open('https://plus.google.com/share?url=' + result, 'popupwindow', 'scrollbars=yes,width=800,height=400').focus();
             return false;
         }

         function clicked_twitter_button(anchor_instance) {
             var result = document.getElementById('<%=hdnSrcUrl.ClientID %>').value;
             result = $("#hdnQuestionText").val() + " " + text + " " + $("#hdnSrcUrl").val();
             document.getElementById('<%= twitter.ClientID%>').href = "https://twitter.com/intent/tweet?text=" + result;
         }

    </script>

     <script type="text/javascript">
         (function (i, s, o, g, r, a, m) {
             i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                 (i[r].q = i[r].q || []).push(arguments)
             }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
         })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

         ga('create', 'UA-55613473-1', 'auto');
         ga('send', 'pageview');

         var thisone = "1";


         $(document).ready(function () {
             $("#metaogtitle").attr("content", $("#lblQuestion").html());
             $("#polldiv").show();
             $("#innerdiv").addClass(" zoomIn animated");
             if ($('#sponsorpreviw').is(':hidden')) {
                 $("#onlyvote").css("margin-left", "0")
                 $("#onlyvote").css("align-content", "center")
                 $("#onlyvote").css("width", "auto")
             }
             if ($("#hdnPollType").val() == "video") {
                 $("#polldiv").css("width", $("frame").prop("width"));
                 $("#polldiv").css("overflow-y", "hidden");
             }

         });

    </script>
    <style>
        .toast{
            top: 50%;
            left: 50%;
            position:relative;
        }
    </style>

</body>
</html>