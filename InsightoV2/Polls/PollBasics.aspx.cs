﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using System.Data;
using App_Code;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Media;
using System.Web.UI.HtmlControls;
using FeatureTypes;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;


public partial class PollBasics : System.Web.UI.Page
{
    string pollType="";
    PollCreation Pc = new PollCreation();
    DataTable dtsettings = new DataTable();
    DataTable dtSettingsInfo = new DataTable();
    int selPollId;
    int pollId = 0; string mode="";
    int copyPollId = 0;
    DataSet pollInfo = new DataSet();
    Hashtable typeHt = new Hashtable();
    string questionType = "";
    string editPollName = "";
    int widthpan = 0;
    string foldername = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            UserControl header = (UserControl)this.Master.FindControl("ucHeaderMenu");
            UserControl upgrade = (UserControl)header.FindControl("UpgradeNowNew1");
            HtmlGenericControl divpolls = (HtmlGenericControl)upgrade.FindControl("polls");
            HtmlGenericControl divfree = (HtmlGenericControl)upgrade.FindControl("divFreeUser");
            divfree.Style.Add("display", "none");
            divpolls.Style.Add("display", "block");
            string script = "$(document).ready(function () { $('[id*=btnSubmit]').click(); });";
            ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);
        }
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        dtsettings = Pc.getPollThemes(userDetails.UserId);
        int widthpan = 0;
        foreach (DataRow row in dtsettings.Rows)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl createmain = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            System.Web.UI.HtmlControls.HtmlGenericControl createdummy = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            System.Web.UI.HtmlControls.HtmlGenericControl createnew = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            System.Web.UI.HtmlControls.HtmlGenericControl createDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            System.Web.UI.HtmlControls.HtmlGenericControl create= new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            System.Web.UI.HtmlControls.HtmlGenericControl poweredby = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            System.Web.UI.HtmlControls.HtmlGenericControl comment = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            System.Web.UI.HtmlControls.HtmlGenericControl clear = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            System.Web.UI.HtmlControls.HtmlGenericControl answerop = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            System.Web.UI.HtmlControls.HtmlGenericControl createRight = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
            createdummy.ID = row["fk_pollid"].ToString();
            createDiv.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#" + row["pollbgcolor"].ToString());
            createDiv.Style.Add(HtmlTextWriterStyle.Color,"#" + row["questioncolor"].ToString());
            foldername = @"images\" + row["fk_pollid"].ToString();
            string iframeheight = "";
            string iframewidth = "";
            string iframeheightnew = "";
            pollInfo = Pc.getPollInfo(Convert.ToInt32(row["fk_pollid"].ToString()));
            if (row["type"].ToString() == "Text")
            {
                questionType = "";
                if (row["iframeheight"].ToString() != "")
                {
                    iframeheight = row["iframeheight"].ToString();
                }
                else
                {
                    iframeheight = "184";
                }
            }
            else if (row["type"].ToString() == "Image")
            {
                if (pollInfo.Tables[0].Rows[0][19].ToString() != null && pollInfo.Tables[0].Rows[0][19].ToString() != "")
                {
                    questionType = "<img src='"+ foldername +"\\"+ pollInfo.Tables[0].Rows[0][19].ToString()+ "' id='imgPreview' />";
                }
                else
                {
                    questionType = "<img src='App_Themes/images/ImagePoll.jpg' id='imgPreview' />";
                }
                if (row["iframeheight"].ToString() != "")
                {
                    iframeheight = row["iframeheight"].ToString();
                }
                else
                {
                    iframeheight = "319";
                }
            }
            else if (row["type"].ToString() == "Video")
            {
                if (pollInfo.Tables[0].Rows[0][18].ToString() != null && pollInfo.Tables[0].Rows[0][18].ToString() != "")
                {
                    questionType = pollInfo.Tables[0].Rows[0][18].ToString();
                }
                else
                {
                    questionType = "<img src='App_Themes/images/Video.jpg' id='imgPreview' height='138' />";
                }
                if (row["iframeheight"].ToString() != "")
                {
                    iframeheight = row["iframeheight"].ToString();
                }
                else
                {
                    iframeheight = "350";
                }
            }
            if (row["iframewidth"].ToString() != "")
            {
                iframewidth = Convert.ToString(Convert.ToInt32(row["iframewidth"].ToString()));
            }
            else
            {
                iframewidth = "300";
            }
            createDiv.Style.Add(HtmlTextWriterStyle.Height, iframeheight + "px");
            createDiv.Style.Add(HtmlTextWriterStyle.Width, iframewidth + "px");
            createdummy.Style.Add(HtmlTextWriterStyle.Color, "#" + row["questionbgcolor"].ToString());
            createdummy.Attributes.Add("class", "placeholdermain");
            createmain.Attributes.Add("class", "placeholdermainnew");
            poweredby.Attributes.Add("class", "poweredbybasics");
            createRight.Style.Add(HtmlTextWriterStyle.Color, "#" + row["questionbgcolor"].ToString());
            createRight.Attributes.Add("class", "placeholdermainside");
            createdummy.Style.Add(HtmlTextWriterStyle.Width, "230px");
            //createdummy.ID = "Id_" + row["pk_pollid"].ToString();
            createnew.Attributes.Add("class", "headingforpollbyme");
            createDiv.ID = "Id_" + row["fk_pollid"].ToString();
            int width = 900;
            if (row["iframewidth"].ToString() != "")
            {
                widthpan = widthpan + 400;
            }
            else
            {
                widthpan = widthpan + 400;
            }
            if (widthpan > width)
            {
                createDiv.Attributes.Add("class", "placeholdermain clear floatL");
                if (row["iframewidth"].ToString() != "")
                {
                    widthpan = 400;
                }
                else
                {
                    widthpan = 400;
                }
            }
            DataTable tbAnswerOption = new DataTable();
            tbAnswerOption = Pc.getAnswerOptions(Convert.ToInt32(row["fk_pollid"].ToString()));
            RadioButtonList answer = new RadioButtonList();
            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                foreach (DataRow rowan in tbAnswerOption.Rows)
                {
                    string answeroption = rowan["answeroption"].ToString();
                    answer.Items.Add(answeroption);
                }
            }

            string Vote = "";
            string Comment = "";
            string Powered = "";
            if (row["sponsorlogo"].ToString() != "")
            {
               create.Attributes.Add("class","votebasicsab");
               Vote = "<div style='text-align:" + row["votealign"].ToString() +
             ";text-decoration:" + row["voteunderline"].ToString() + ";font-family:" + row["votefont"].ToString() + ";font-size:" + row["votefontsize"].ToString() + "px;font-weight:" + row["votefontweight"].ToString() + ";font-style:" + row["votestyle"].ToString() + ";padding: 10px 0px 10px 0px;background-color:#" + row["votebgcolor"].ToString() + ";color:#" + row["votecolor"].ToString() + ";margin-left: 10px;margin-right: 3px; width:45%;float:left; margin-top:15px'>Vote<div style='font-size:10px;'>and view results</div></div><div class='sponsorimagecustom' style='margin-top:11px;'><div style='font-family:" + row["answerfont"].ToString() + ";color:#" + row["answercolor"].ToString() + ";font-size:" + row["answerfontsize"].ToString() + "px;'>Sponsored By</div><div style='text-align:center'><img src='" + foldername + "\\" + row["sponsorlogo"].ToString() + "' /></div></div><div class='clear'></div>";
            }
            else
            {
                Vote = "<div style='text-align:" + row["votealign"].ToString() +
                ";text-decoration:" + row["voteunderline"].ToString() + ";position:absolute; bottom:35px;font-family:" + row["votefont"].ToString() + ";font-size:" + row["votefontsize"].ToString() + "px;font-weight:" + row["votefontweight"].ToString() + ";font-style:" + row["votestyle"].ToString() + ";padding: 10px 0px 10px 0px;margin-top:10px;text-align: center;background-color:#" + row["votebgcolor"].ToString() + ";color:#" + row["votecolor"].ToString() + ";width:80%;margin-left: 10%; margin-right: 10%; '>Vote and View Results</div>";
            }
            if (pollInfo.Tables[0].Rows[0][9].ToString() == "private")
            {
                if (row["sponsorlogo"].ToString() != "")
                {
                    create.Attributes.Add("class", "votebasicsab");
                    Vote = "<div style='text-align:" + row["votealign"].ToString() +
                  ";text-decoration:" + row["voteunderline"].ToString() + ";font-family:" + row["votefont"].ToString() + ";font-size:" + row["votefontsize"].ToString() + "px;font-weight:" + row["votefontweight"].ToString() + ";font-style:" + row["votestyle"].ToString() + ";padding: 10px 0px 10px 0px;background-color:#" + row["votebgcolor"].ToString() + ";color:#" + row["votecolor"].ToString() + ";margin-left: 10px;margin-right: 3px; width:45%;float:left; margin-top:15px'>Vote</div><div class='sponsorimagecustom' style='margin-top:11px;'><div style='font-family:" + row["answerfont"].ToString() + ";color:#" + row["answercolor"].ToString() + ";font-size:" + row["answerfontsize"].ToString() + "px;'>Sponsored By</div><div style='text-align:center'><img src='images/polluploads/logos/" + row["sponsorlogo"].ToString() + "' /></div></div><div class='clear'></div>";
                }
                else
                {
                    Vote = "<div style='text-align:" + row["votealign"].ToString() +
                    ";text-decoration:" + row["voteunderline"].ToString() + ";position:absolute; bottom:35px;font-family:" + row["votefont"].ToString() + ";font-size:" + row["votefontsize"].ToString() + "px;font-weight:" + row["votefontweight"].ToString() + ";font-style:" + row["votestyle"].ToString() + ";padding: 10px 0px 10px 0px;margin-top:10px;text-align: center;background-color:#" + row["votebgcolor"].ToString() + ";color:#" + row["votecolor"].ToString() + ";width:75%;margin-left: 15%; margin-right: 15%; '>Vote</div>";
                }

            }
            if (pollInfo.Tables[0].Rows[0][14].ToString() == "no")
            {
                Powered = "";
            }
            else
            {
                Powered = "<div style='padding-top: 13px;color:#" + row["answercolor"].ToString() + ";position:absolute; width:100%; bottom:10px;text-align: center;'>Powered By <a href='http://www.insighto.com' style='color:#" + row["answercolor"].ToString() + ";' target='_blank'>Insighto</a></div>";
            }
            createnew.InnerHtml = "<div>" + pollInfo.Tables[0].Rows[0][2].ToString() + "</div><div class='clear'></div>";
            createDiv.InnerHtml = "<div class='imagefrombehind'>" + questionType + "</div><div style='padding-left: 20px;margin:3px;text-decoration:" + row["questionunderline"].ToString() + ";font-family:" + row["questionfont"].ToString() + ";font-size:" + row["questionfontsize"].ToString() + "px;font-weight:" + row["questionfontweight"].ToString() + ";font-style:" + row["questionstyle"].ToString() + ";line-height:15px; padding:8px;text-align:" + row["questionalign"].ToString() + 
            ";background-color:#" + row["questionbgcolor"].ToString() + ";color:#" +
            row["questioncolor"].ToString() + ";'> " + pollInfo.Tables[0].Rows[0][16].ToString() + " </div>" ;
            answer.Attributes.Add("style","margin:10px 15px;text-align:" + row["answeralign"].ToString() + ";text-decoration:" + row["answerunderline"].ToString() + ";font-family:" + row["answerfont"].ToString() + ";font-size:" + row["answerfontsize"].ToString() + "px;font-weight:" + row["answerfontweight"].ToString() + ";font-style:" + row["answerstyle"].ToString() + ";color:#" + row["answercolor"].ToString() + ";");
            answerop.Controls.Add(answer);
            if (row["type"].ToString() == "Text")
            {
                questionType = "";
                if (row["iframeheight"].ToString() != "")
                {
                    iframeheightnew = Convert.ToString(Convert.ToInt32(row["iframeheight"].ToString()) + 50);
                }
                else
                {
                    iframeheightnew = "184";
                }
            }
            else if (row["type"].ToString() == "Image")
            {
                if (row["iframeheight"].ToString() != "")
                {
                    iframeheightnew = Convert.ToString(Convert.ToInt32(row["iframeheight"].ToString()));
                }
                else
                {
                    iframeheightnew = "319";
                }
            }
            else if (row["type"].ToString() == "Video")
            {
                if (row["iframeheight"].ToString() != "")
                {
                    iframeheightnew = Convert.ToString(Convert.ToInt32(row["iframeheight"].ToString()));
                }
                else
                {
                    iframeheightnew = "350";
                }
            }
            createRight.InnerHtml = "<div style='padding-left: 20px;margin:3px 0px;text-decoration:" + row["questionunderline"].ToString() + ";font-family:" + row["questionfont"].ToString() + ";font-size:" + row["questionfontsize"].ToString() + "px;font-weight:" + row["questionfontweight"].ToString() + ";font-style:" + row["questionstyle"].ToString() + ";line-height:15px;padding:10px;text-align:" + row["questionalign"].ToString() +
            ";color:#" +
            row["questioncolor"].ToString() + ";'> " + pollInfo.Tables[0].Rows[0][16].ToString() + " </div><div class='dimensions'>Dimensions : " + iframewidth + " X " + iframeheightnew + "</div>";
            
            createDiv.Controls.Add(answerop);
            if (Convert.ToBoolean(pollInfo.Tables[0].Rows[0][17].ToString()) == true)
            {
                Comment = "<div class='commentboxpre'><div class='commentstxtbyme' style='color:#" + row["answercolor"].ToString() + ";font-size:" + row["answerfontsize"].ToString() + "px;font-family:" + row["answerfont"].ToString() + ";'>Comments</div><div><input type='text' name='comment' class='textBoxMedium txtAns txtcommentsbyme'/></div></div><div class='clear'></div>";
                comment.InnerHtml = Comment;
                createDiv.Controls.Add(comment);
            }
            create.InnerHtml = Vote;
          
            createDiv.Controls.Add(create);
            poweredby.InnerHtml = Powered;
            createDiv.Controls.Add(poweredby);
            Button btnSave = new Button();
            
            btnSave.ID = "btnSave" + row["pk_pollid"].ToString();
           // string clickevent = "click" + row["pk_pollid"].ToString();
            //String scripttype = "ButtonClickScript";
            //StringBuilder cstext = new StringBuilder();
            //cstext.Append("<script type=\"text/javascript\"> function DoClick() {");
            //cstext.Append("Form1.Message.value='Text from client script.'} </");
            //cstext.Append("script>");
            //Type cstype = this.GetType();
            //createDiv.Attributes["onclick"] = ClientScript.RegisterClientScriptBlock(cstype,scripttype,cstext.ToString(),false);
            //ClientScript.RegisterClientScriptBlock(GetType(), "sas", "<script> alert('Testing');</script>", true);
            createDiv.Attributes.Add("onclick", "ShowPopup(" + row["pk_pollid"].ToString() + ")");
            btnSave.Text = "Select";
            btnSave.Click += new EventHandler(button_Click);
            btnSave.CssClass = "placeholderbtn";
            createRight.Controls.Add(btnSave);
            createdummy.Controls.Add(createDiv);
            createmain.Controls.Add(createnew);
            createmain.Controls.Add(createdummy);
            createmain.Controls.Add(createRight);
            plcCustomThemes.Controls.Add(createmain);
        }
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());
            if (typeHt.Contains("PollId"))
                copyPollId = Convert.ToInt32(typeHt["PollId"].ToString());
            if (typeHt.Contains("Mode"))
            {
                mode = typeHt["Mode"].ToString();
            }
            if (typeHt.Contains("PollIdEdit"))
            {
                pollId = Convert.ToInt32(typeHt["PollIdEdit"].ToString());
                editPollName = typeHt["ModeName"].ToString();
            }
        }
        if (pollId > 0)
        {
            DataSet PollInfo = new DataSet();
            PollInfo = Pc.getPollInfo(pollId);
            lblCreate.Text = "Edit Poll";
            txtPollName.Text = PollInfo.Tables[0].Rows[0][2].ToString();
        }
    }
 
    protected void button_Click(object sender, EventArgs e)
    {
        Button button = sender as Button;
        string id = button.ID;
        string strRootURL = ConfigurationSettings.AppSettings["Fchartpathtest"].ToString();
        string pollimagesfolder = ConfigurationSettings.AppSettings["pollimagesfolder"].ToString();
        string imagelink = "";
        string sponsorlogo = "";
        string sponsortext = "";
        selPollId = Convert.ToInt32(Regex.Replace(id, @"\D", ""));
        if (txtPollName.Text == "")
        {
            return;
        }
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        dtSettingsInfo = Pc.getPollSettingsInfo(selPollId);
        pollInfo = Pc.getPollInfo(selPollId);
        if ((pollInfo.Tables[1].Rows[0]["sponsorlogo"].ToString() != "") && (pollInfo.Tables[1].Rows[0]["sponsortext"].ToString() != ""))
        {
            sponsorlogo = pollInfo.Tables[1].Rows[0]["sponsorlogo"].ToString();
            sponsortext = pollInfo.Tables[1].Rows[0]["sponsortext"].ToString();
        }
        
        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
        {
            pollType = "Image";
            imagelink = pollInfo.Tables[0].Rows[0]["imagelink"].ToString();
        }
        else if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
        {
            pollType = "Video";
        }
        else
        {
            pollType = "Text";
        }
         if (editPollName == "PollEdit")
        {
            Pc.UpdatePollName(txtPollName.Text, pollType, pollId);
            bool Exist = Pc.GetSettingsInfo(pollId);
            if (Exist == false)
            {
                //Pc.InsertintoPollSettings(pollId, dtSettingsInfo.Rows[0]["pollbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questionfont"].ToString(), dtSettingsInfo.Rows[0]["questionbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questioncolor"].ToString(), dtSettingsInfo.Rows[0]["questionstyle"].ToString(), dtSettingsInfo.Rows[0]["questionfontweight"].ToString(), dtSettingsInfo.Rows[0]["questionunderline"].ToString(), dtSettingsInfo.Rows[0]["questionalign"].ToString(), dtSettingsInfo.Rows[0]["questionfontsize"].ToString(), dtSettingsInfo.Rows[0]["answerfont"].ToString(), dtSettingsInfo.Rows[0]["answercolor"].ToString(), dtSettingsInfo.Rows[0]["answerfontweight"].ToString(), dtSettingsInfo.Rows[0]["answerunderline"].ToString(), dtSettingsInfo.Rows[0]["answerstyle"].ToString(), dtSettingsInfo.Rows[0]["answeralign"].ToString(), dtSettingsInfo.Rows[0]["answerfontsize"].ToString(), dtSettingsInfo.Rows[0]["votebgcolor"].ToString(), dtSettingsInfo.Rows[0]["votefont"].ToString(), dtSettingsInfo.Rows[0]["votefontsize"].ToString(), dtSettingsInfo.Rows[0]["votestyle"].ToString(), dtSettingsInfo.Rows[0]["voteunderline"].ToString(), dtSettingsInfo.Rows[0]["votecolor"].ToString(), dtSettingsInfo.Rows[0]["votefontweight"].ToString(), dtSettingsInfo.Rows[0]["votealign"].ToString(), dtSettingsInfo.Rows[0]["sponsorlogo"].ToString(), dtSettingsInfo.Rows[0]["sponsortext"].ToString(), dtSettingsInfo.Rows[0]["logolink"].ToString(), dtSettingsInfo.Rows[0]["messagebgcolor"].ToString(), dtSettingsInfo.Rows[0]["messagecolor"].ToString(), dtSettingsInfo.Rows[0]["messagefont"].ToString(), dtSettingsInfo.Rows[0]["messagestyle"].ToString(), dtSettingsInfo.Rows[0]["messageunderline"].ToString(), dtSettingsInfo.Rows[0]["messagealign"].ToString(), dtSettingsInfo.Rows[0]["messagefontsize"].ToString(), dtSettingsInfo.Rows[0]["messagefontweight"].ToString(), dtSettingsInfo.Rows[0]["googletext"].ToString(), dtSettingsInfo.Rows[0]["twittertext"].ToString(), dtSettingsInfo.Rows[0]["fbtext"].ToString(), Convert.ToInt32(dtSettingsInfo.Rows[0]["countvotes"].ToString()), dtSettingsInfo.Rows[0]["sharetext"].ToString(), dtSettingsInfo.Rows[0]["votetext"].ToString(), Convert.ToInt32(dtSettingsInfo.Rows[0]["chartwidth"]), Convert.ToInt32(dtSettingsInfo.Rows[0]["chartheight"]));
            }
            else
            {
                //Pc.UpdatePollSettings(pollId, dtSettingsInfo.Rows[0]["pollbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questionfont"].ToString(), dtSettingsInfo.Rows[0]["questionbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questioncolor"].ToString(), dtSettingsInfo.Rows[0]["questionstyle"].ToString(), dtSettingsInfo.Rows[0]["questionfontweight"].ToString(), dtSettingsInfo.Rows[0]["questionunderline"].ToString(), dtSettingsInfo.Rows[0]["questionalign"].ToString(), dtSettingsInfo.Rows[0]["questionfontsize"].ToString(), dtSettingsInfo.Rows[0]["answerfont"].ToString(), dtSettingsInfo.Rows[0]["answercolor"].ToString(), dtSettingsInfo.Rows[0]["answerfontweight"].ToString(), dtSettingsInfo.Rows[0]["answerunderline"].ToString(), dtSettingsInfo.Rows[0]["answerstyle"].ToString(), dtSettingsInfo.Rows[0]["answeralign"].ToString(), dtSettingsInfo.Rows[0]["answerfontsize"].ToString(), dtSettingsInfo.Rows[0]["votebgcolor"].ToString(), dtSettingsInfo.Rows[0]["votefont"].ToString(), dtSettingsInfo.Rows[0]["votefontsize"].ToString(), dtSettingsInfo.Rows[0]["votestyle"].ToString(), dtSettingsInfo.Rows[0]["voteunderline"].ToString(), dtSettingsInfo.Rows[0]["votecolor"].ToString(), dtSettingsInfo.Rows[0]["votefontweight"].ToString(), dtSettingsInfo.Rows[0]["votealign"].ToString(), dtSettingsInfo.Rows[0]["sponsorlogo"].ToString(), dtSettingsInfo.Rows[0]["sponsortext"].ToString(), dtSettingsInfo.Rows[0]["logolink"].ToString(), dtSettingsInfo.Rows[0]["messagebgcolor"].ToString(), dtSettingsInfo.Rows[0]["messagecolor"].ToString(), dtSettingsInfo.Rows[0]["messagefont"].ToString(), dtSettingsInfo.Rows[0]["messagestyle"].ToString(), dtSettingsInfo.Rows[0]["messageunderline"].ToString(), dtSettingsInfo.Rows[0]["messagealign"].ToString(), dtSettingsInfo.Rows[0]["messagefontsize"].ToString(), dtSettingsInfo.Rows[0]["messagefontweight"].ToString(), dtSettingsInfo.Rows[0]["googletext"].ToString(), dtSettingsInfo.Rows[0]["twittertext"].ToString(), dtSettingsInfo.Rows[0]["fbtext"].ToString(), Convert.ToInt32(dtSettingsInfo.Rows[0]["countvotes"].ToString()), dtSettingsInfo.Rows[0]["sharetext"].ToString(), dtSettingsInfo.Rows[0]["votetext"].ToString(), Convert.ToInt32(dtSettingsInfo.Rows[0]["chartwidth"]), Convert.ToInt32(dtSettingsInfo.Rows[0]["chartheight"]));
            }
            var redirectUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollId);
            Response.Redirect(redirectUrl);
        }
        bool pn = Pc.getPollName(userDetails.UserId, txtPollName.Text);
        if (pn == false)
        {
            if (mode == "Copy")
            {
                DataTable tbAnswerOption = new DataTable();
                pollInfo = Pc.getPollInfo(copyPollId);
                tbAnswerOption = Pc.getAnswerOptions(copyPollId);
                pollId = Pc.AddNameandType(txtPollName.Text, pollType, userDetails.UserId,0);
              //  pollId = Pc.AddNameandType(txtPollName.Text, pollType, userDetails.UserId);
                //Pc.InsertintoPollInfo(pollId, pollInfo.Tables[0].Rows[0][16].ToString(), pollInfo.Tables[0].Rows[0][18].ToString(), pollInfo.Tables[0].Rows[0][19].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()), 0);
                if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
                {
                    foreach (DataRow row in tbAnswerOption.Rows)
                    {
                        string answeroption = row["answeroption"].ToString();
                        Pc.SaveAnswerOptions(answeroption, pollId,"");
                    }
                }
                //Pc.InsertintoPollSettings(pollId, dtSettingsInfo.Rows[0]["pollbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questionfont"].ToString(), dtSettingsInfo.Rows[0]["questionbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questioncolor"].ToString(), dtSettingsInfo.Rows[0]["questionstyle"].ToString(), dtSettingsInfo.Rows[0]["questionfontweight"].ToString(), dtSettingsInfo.Rows[0]["questionunderline"].ToString(), dtSettingsInfo.Rows[0]["questionalign"].ToString(), dtSettingsInfo.Rows[0]["questionfontsize"].ToString(), dtSettingsInfo.Rows[0]["answerfont"].ToString(), dtSettingsInfo.Rows[0]["answercolor"].ToString(), dtSettingsInfo.Rows[0]["answerfontweight"].ToString(), dtSettingsInfo.Rows[0]["answerunderline"].ToString(), dtSettingsInfo.Rows[0]["answerstyle"].ToString(), dtSettingsInfo.Rows[0]["answeralign"].ToString(), dtSettingsInfo.Rows[0]["answerfontsize"].ToString(), dtSettingsInfo.Rows[0]["votebgcolor"].ToString(), dtSettingsInfo.Rows[0]["votefont"].ToString(), dtSettingsInfo.Rows[0]["votefontsize"].ToString(), dtSettingsInfo.Rows[0]["votestyle"].ToString(), dtSettingsInfo.Rows[0]["voteunderline"].ToString(), dtSettingsInfo.Rows[0]["votecolor"].ToString(), dtSettingsInfo.Rows[0]["votefontweight"].ToString(), dtSettingsInfo.Rows[0]["votealign"].ToString(), dtSettingsInfo.Rows[0]["sponsorlogo"].ToString(), dtSettingsInfo.Rows[0]["sponsortext"].ToString(), dtSettingsInfo.Rows[0]["logolink"].ToString(), dtSettingsInfo.Rows[0]["messagebgcolor"].ToString(), dtSettingsInfo.Rows[0]["messagecolor"].ToString(), dtSettingsInfo.Rows[0]["messagefont"].ToString(), dtSettingsInfo.Rows[0]["messagestyle"].ToString(), dtSettingsInfo.Rows[0]["messageunderline"].ToString(), dtSettingsInfo.Rows[0]["messagealign"].ToString(), dtSettingsInfo.Rows[0]["messagefontsize"].ToString(), dtSettingsInfo.Rows[0]["messagefontweight"].ToString(), dtSettingsInfo.Rows[0]["googletext"].ToString(), dtSettingsInfo.Rows[0]["twittertext"].ToString(), dtSettingsInfo.Rows[0]["fbtext"].ToString(), Convert.ToInt32(dtSettingsInfo.Rows[0]["countvotes"].ToString()), dtSettingsInfo.Rows[0]["sharetext"].ToString(), dtSettingsInfo.Rows[0]["votetext"].ToString(), Convert.ToInt32(dtSettingsInfo.Rows[0]["chartwidth"]), Convert.ToInt32(dtSettingsInfo.Rows[0]["chartheight"]));
                //if (pollType == "Image")
                //{
                //    Directory.CreateDirectory(strRootURL + "Polls/images/" + pollId.ToString());
                //    File.Move(strRootURL + "Polls/images/" + copyPollId.ToString() + "/" + imagelink, strRootURL + "Polls/images/" + pollId.ToString() + "/" + imagelink);
                //}
                //if (sponsorlogo != "" && sponsortext != "") {
                //    if (!Directory.Exists(strRootURL + "/Polls/images/" + pollId.ToString()))
                //    {
                //        Directory.CreateDirectory(strRootURL + "/Polls/images/" + pollId.ToString());
                //    }
                //    File.Move(strRootURL + "/Polls/images/" + copyPollId.ToString() + "/" + sponsorlogo, strRootURL + "Polls/images/" + pollId.ToString() + "/" + sponsorlogo);
                //}
                var redirectUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollId);
                Response.Redirect(redirectUrl);
            }
            else
            {
                pollId = Pc.AddNameandType(txtPollName.Text, pollType, userDetails.UserId,0);
              //  pollId = Pc.AddNameandType(txtPollName.Text, pollType, userDetails.UserId);
                DataTable tbAnswerOption = new DataTable();
                int comm = 0;
                if (Convert.ToBoolean(pollInfo.Tables[0].Rows[0][17].ToString()) == true)
                {
                    comm = 1;
                }
                ////Pc.InsertintoPollInfo(pollId, pollInfo.Tables[0].Rows[0][16].ToString(), pollInfo.Tables[0].Rows[0][18].ToString(), pollInfo.Tables[0].Rows[0][19].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()), comm, Convert.ToInt32(pollInfo.Tables[0].Rows[0]["uniquerespondent"].ToString()));
                tbAnswerOption = Pc.getAnswerOptions(selPollId);
                if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
                {
                    foreach (DataRow row in tbAnswerOption.Rows)
                    {
                        string answeroption = row["answeroption"].ToString();
                        Pc.SaveAnswerOptions(answeroption, pollId,"");
                    }
                }
                //Pc.InsertintoPollSettings(pollId, dtSettingsInfo.Rows[0]["pollbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questionfont"].ToString(), dtSettingsInfo.Rows[0]["questionbgcolor"].ToString(), dtSettingsInfo.Rows[0]["questioncolor"].ToString(), dtSettingsInfo.Rows[0]["questionstyle"].ToString(), dtSettingsInfo.Rows[0]["questionfontweight"].ToString(), dtSettingsInfo.Rows[0]["questionunderline"].ToString(), dtSettingsInfo.Rows[0]["questionalign"].ToString(), dtSettingsInfo.Rows[0]["questionfontsize"].ToString(), dtSettingsInfo.Rows[0]["answerfont"].ToString(), dtSettingsInfo.Rows[0]["answercolor"].ToString(), dtSettingsInfo.Rows[0]["answerfontweight"].ToString(), dtSettingsInfo.Rows[0]["answerunderline"].ToString(), dtSettingsInfo.Rows[0]["answerstyle"].ToString(), dtSettingsInfo.Rows[0]["answeralign"].ToString(), dtSettingsInfo.Rows[0]["answerfontsize"].ToString(), dtSettingsInfo.Rows[0]["votebgcolor"].ToString(), dtSettingsInfo.Rows[0]["votefont"].ToString(), dtSettingsInfo.Rows[0]["votefontsize"].ToString(), dtSettingsInfo.Rows[0]["votestyle"].ToString(), dtSettingsInfo.Rows[0]["voteunderline"].ToString(), dtSettingsInfo.Rows[0]["votecolor"].ToString(), dtSettingsInfo.Rows[0]["votefontweight"].ToString(), dtSettingsInfo.Rows[0]["votealign"].ToString(), dtSettingsInfo.Rows[0]["sponsorlogo"].ToString(), dtSettingsInfo.Rows[0]["sponsortext"].ToString(), dtSettingsInfo.Rows[0]["logolink"].ToString(), dtSettingsInfo.Rows[0]["messagebgcolor"].ToString(), dtSettingsInfo.Rows[0]["messagecolor"].ToString(), dtSettingsInfo.Rows[0]["messagefont"].ToString(), dtSettingsInfo.Rows[0]["messagestyle"].ToString(), dtSettingsInfo.Rows[0]["messageunderline"].ToString(), dtSettingsInfo.Rows[0]["messagealign"].ToString(), dtSettingsInfo.Rows[0]["messagefontsize"].ToString(), dtSettingsInfo.Rows[0]["messagefontweight"].ToString(), dtSettingsInfo.Rows[0]["googletext"].ToString(), dtSettingsInfo.Rows[0]["twittertext"].ToString(), dtSettingsInfo.Rows[0]["fbtext"].ToString(), Convert.ToInt32(Convert.ToBoolean(dtSettingsInfo.Rows[0]["countvotes"].ToString())), dtSettingsInfo.Rows[0]["sharetext"].ToString(), dtSettingsInfo.Rows[0]["votetext"].ToString(), Convert.ToInt32(dtSettingsInfo.Rows[0]["chartwidth"]), Convert.ToInt32(dtSettingsInfo.Rows[0]["chartheight"]));
                if(dtSettingsInfo.Rows[0]["iframewidth"].ToString()!="" && dtSettingsInfo.Rows[0]["iframeheight"].ToString() !="")
                {
                    Pc.SaveIframeInfo(pollId, Convert.ToInt32(dtSettingsInfo.Rows[0]["iframewidth"].ToString()), Convert.ToInt32(dtSettingsInfo.Rows[0]["iframeheight"].ToString()), Convert.ToInt32(dtSettingsInfo.Rows[0]["recmwidth"].ToString()), Convert.ToInt32(dtSettingsInfo.Rows[0]["recmheight"].ToString()));
                    if (pollType == "Image")
                    {
                        Directory.CreateDirectory(pollimagesfolder + pollId.ToString());
                        //File.Move(pollimagesfolder + selPollId.ToString() + "//" + imagelink, pollimagesfolder + pollId.ToString() + "//1" + imagelink);
                        File.Copy(pollimagesfolder + selPollId.ToString() + "//" + imagelink, pollimagesfolder + pollId.ToString() + "//" + imagelink, true);
                        Pc.SaveImageSpec(pollId, Convert.ToInt32(dtSettingsInfo.Rows[0]["imgrecwidth"].ToString()), Convert.ToInt32(dtSettingsInfo.Rows[0]["imgrecmheight"].ToString()));
                    }
                }
                
                if (sponsorlogo != "" && sponsortext != "")
                {
                    if (!Directory.Exists(pollimagesfolder + pollId.ToString()))
                    {
                        Directory.CreateDirectory(pollimagesfolder + pollId.ToString());
                    }
                    //File.Move(pollimagesfolder + selPollId.ToString() + "//" + sponsorlogo, pollimagesfolder + pollId.ToString() + "//" + sponsorlogo);
                    File.Copy(pollimagesfolder + selPollId.ToString() + "//" + sponsorlogo, pollimagesfolder + pollId.ToString() + "//" + sponsorlogo, true);
                }
                var redirectUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollId);
                Response.Redirect(redirectUrl);
            }
        }
        else
        {
            lblErrMsg.Text = "Poll " + txtPollName.Text + " Already Exists";
            lblErrMsg.Visible = true;
            return;
        }
    }
    
    protected void btnSelect_Click(object sender, EventArgs e)
    {
        pollType = "Text";
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (pollId > 0)
        {
            Pc.UpdatePollName(txtPollName.Text, pollType, pollId);
        }
        else
        {
            SavePollNameandType(txtPollName.Text, pollType, userDetails.UserId);
        }
    }
    
    protected void btnTextType_Click(object sender, EventArgs e)
    {
        pollType = "Text";
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (pollId > 0)
        {
            Pc.UpdatePollName(txtPollName.Text, pollType, pollId);
            var redirectUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollId);
            Response.Redirect(redirectUrl);
        }
        else
        {
            SavePollNameandType(txtPollName.Text, pollType, userDetails.UserId);
        }
    }
   
    protected void btnImageType_Click(object sender, EventArgs e)
   {
        pollType = "Image";
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (pollId > 0)
        {
            Pc.UpdatePollName(txtPollName.Text, pollType, pollId);
            var redirectUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollId);
            Response.Redirect(redirectUrl);
        }
        else
        {
            SavePollNameandType(txtPollName.Text, pollType, userDetails.UserId);
        }
   }
   
    protected void btnVideoType_Click(object sender, EventArgs e)
   {
       pollType = "Video";
       var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
       if (pollId > 0)
       {
           Pc.UpdatePollName(txtPollName.Text, pollType, pollId);
           var redirectUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollId);
           Response.Redirect(redirectUrl);
       }
       else
       {
           SavePollNameandType(txtPollName.Text, pollType, userDetails.UserId);
       }
   }
   
    public void SavePollNameandType(string Name,string Type, int userId)
   {
       bool pn = Pc.getPollName(userId, Name);
       if (pn == false)
       {
           if (mode == "Copy")
           {
               DataTable tbAnswerOption = new DataTable();
               pollInfo = Pc.getPollInfo(copyPollId);
               tbAnswerOption = Pc.getAnswerOptions(copyPollId);
               int PollId = Pc.AddNameandType(Name, Type, userId,0);
                //int PollId = Pc.AddNameandType(Name, Type, userId);
               //Pc.InsertintoPollInfo(PollId, pollInfo.Tables[0].Rows[0][16].ToString(), pollInfo.Tables[0].Rows[0][18].ToString(), pollInfo.Tables[0].Rows[0][19].ToString(), Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()), 0);
               if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
               {
                   foreach (DataRow row in tbAnswerOption.Rows)
                   {
                       string answeroption = row["answeroption"].ToString();
                       Pc.SaveAnswerOptions(answeroption, PollId,"");
                   }
               }
               var redirectUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + PollId);
               string strsliderjs = ConfigurationSettings.AppSettings["sliderjsfilepath"].ToString();
               string strRootURL = ConfigurationSettings.AppSettings["RootURL"].ToString();
               //string imageURL = strRootURL + "Polls/images/"+ copyPollId + "/";
               Directory.CreateDirectory(strRootURL + "Polls/images/" + PollId.ToString());
               //File.Copy(file_name, file_name + "del", true);
               //File.Delete(file_name);

               Response.Redirect(redirectUrl);
           }
           else
           {
                   int PollId = Pc.AddNameandType(Name, Type, userId,0);
              // int PollId = Pc.AddNameandType(Name, Type, userId);

                   var redirectUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + PollId);
                   Response.Redirect(redirectUrl);
           }
       }
           else
           {
                   lblErrMsg.Text = "Poll " + Name + " Already Exists";
                   lblErrMsg.Visible = true;
                   return;
           }
       }
}