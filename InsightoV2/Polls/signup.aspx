﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="signup.aspx.cs" Inherits="Poll_signup" MasterPageFile="~/Home.master"%>
<%@ Register TagPrefix="uc" TagName="PollsHeader" Src="~/UserControls/PollsHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc:PollsHeader ID="PollsHeader" runat="server" />

       <div class="bg-grey content-sm pricing-page-intro">
        <div class="container text-center">
            <%--<h2 class="title-v2 title-center">A PRICING PLAN TO SUIT YOUR UNIQUE NEEDS</h2>--%>
            <p class="space-lg-hor" style="font-size: 15px;">Your chance to experience Insighto Polls free, for 14 days. When you sign up for a free trial, you will automatically have access to the Publisher Plan. You may select the Plan that suits you best after the free trial.</p><br>
        </div>
    </div>

    
 <!--=== Content Part ===-->
    <div class="container content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" style="margin-top:-40px;">
            <div class="reg-header" style="border-bottom:none;display:none;">
                        <h2 class="rounded" style="color:#fff;font-weight:bold;background:#884092;margin:20px 0px;">Get a 14-Day Free Trial</h2>
                    </div>
            <div class="reg-page" style="margin-top:-20px;">
                    <div style="display:none;">
                    <asp:Label ID="lblErrorMsg" Visible="false" runat="server"></asp:Label>
                    <asp:TextBox ID="txtName" class="form-control" runat="server"  placeholder="Name*" ></asp:TextBox>
                    <asp:Label ID="lblvalidmsg" runat="server" style="color:Red;font-family:Arial;font-size:12px; font-weight:normal"></asp:Label> 
                    <div class=" margin-bottom-15"></div>
                     <input type="text" placeholder="Email Id*" class="form-control" runat="server" id="txtEmail"/>
                     <label id="lblvalidmsgemail" runat="server" style="color:Red;font-family:Arial;font-size:12px; font-weight:normal;"></label>
                     <div class=" margin-bottom-15"></div>
                    <input type="password" placeholder="Password*" class="form-control" runat="server" id="txtpwd"/>
                    <label id="lblvalidmsgpwd" runat="server" style="color:Red;font-family:Arial;font-size:12px;font-weight:normal "></label>
                    <div class=" margin-bottom-15"></div>
                    <input type="password" placeholder="Confirm Password*" class="form-control margin-bottom-5" runat="server" id="txtcpwd"/>
                       <label id="lblvalidmsgrepwd" runat="server" style="color:Red;font-family:Arial;font-size:12px;font-weight:normal"></label>
                    <div class="margin-bottom-15"></div>
					<!--<select name="Choose Your Plan*" id="selPlan" style="color:#9999a8;" class="form-control" runat="server">
					</select>
                    -->
                  
                    <div class="margin-bottom-15"></div>
                        <br />
                    </div>
                                   <div class="row center-block" style="text-align:center;">
                    <asp:Button ID="btnSFT" OnClick="btnSFT_Click" CausesValidation="false" runat="server" Text="Start Your 14-day Free Trial" class="btn-u"  />
                         
                            <!--<label class="checkbox" style="text-align:center;margin-top:0px;">
                               
                               <span style="padding-left:0px;font-weight:none;font-size:11px;">* No Credit Card required</span>  <span style="padding-left:40px;font-weight:none;font-size:11px;">* All fields are mandatory</span>

                            </label>     -->
                        
                    </div>
                    <div >By signing up, you agree to the <a href="/termsofuse.aspx">Terms & Conditions</a> and the <a href="/privacy-policy.aspx">Privacy Policy</a></div>
                </div>
            </div>
        </div>

        <div style="margin-top:-200px;margin-left:500px;">
        </div>
        
    </div><!--/container-->		
    <!--=== End Content Part ===-->

  

         <!-- Pricing Rounded -->
         <div id="boxesprice" style="display:none;">
         <div id="priceplan" class="window">
          <div class="modal-example-header">            
            <h4>Compare plans to take Free Trial</h4><a style="float: right; margin-top:-22px;" href="#" class="close popupCloseLink">&nbsp;</a>
        </div>
        <div class="container content">   
        <div class="row margin-bottom-40 pricing-rounded">
            <div class="col-md1-3 col-sm-6">
                <div class="pricing hover-effect" >
                   
                    <div class="pricing-head">
                        <h3>Professional</h3>                      
                         <h4 ><i><span id="spprof" runat="server" style="font-size:35px;margin-top:39px;"></span></i>
                         <% if (Request.Browser.Browser == "Firefox")
   { %>
   <span style="margin-top:-40px;">Per Month </span>
<%}
                            else if (Request.Browser.Browser == "Chrome")
                            { %>
                            <span style="margin-top:0px;">Per Month </span>

   <%} %>
               </h4>          
                <div class="profes">Save <span style="font-size:22px;"></span> with </div>
                        <div class="clear"></div>
                        <ul class="pricing-content list-unstyled">                        
                        <li class="bg-color" style="text-align:center;color:#fff;background:#489007; font-size:20px;">Annual Plan - <%=AnnualProf %></li>
                    </ul>
                        
                    </div>
                    <ul class="pricing-content list-unstyled">
                         <li><i class="fa fa-check"></i> Create Text / Image / Video polls</li>
                        <li><i class="fa fa-check"></i> Live preview while creating polls</li>
                        <li><i class="fa fa-check"></i> Public results </li>
                        <li><i class="fa fa-check"></i> 3,000 responses / month</li>
                        <li><i class="fa fa-check"></i> No Sponsorship feature</li>   
                    </ul>                                         
                </div>
            </div>
            <div class="col-md1-3 col-sm-6">
                <div class="pricing hover-effect" >
                   
                    <div class="pricing-head">
                        <h3 style="background:#44a3d5;text-shadow:none;">Business</h3>
                         <h4 ><i><span id="spBus" runat="server" style="font-size:35px;margin-top:39px;"></span></i>
                                  <% if (Request.Browser.Browser == "Firefox")
   { %>
   <span style="margin-top:-40px;">Per Month </span>
<%}
                            else if (Request.Browser.Browser == "Chrome")
                            { %>
                            <span style="margin-top:0px;">Per Month </span>

   <%} %>
                         </h4>
                      <div class="busfes">Save <span style="font-size:22px;"></span> with </div>
                        <div class="clear"></div>
                        <ul class="pricing-content list-unstyled">
                        
                        <li class="bg-color" style="text-align:center;color:#fff;background:#2477a3;font-size:20px;"> Annual Plan - <%=AnnualBus %></li>
                    </ul>
                      
                    </div>
                    <ul class="pricing-content list-unstyled">
                        <li><i class="fa fa-check"></i> Create Text / Image / Video polls</li>
                        <li><i class="fa fa-check"></i> Live preview while creating polls</li>
                        <li><i class="fa fa-check"></i> Option to keep results private </li>
                        <li><i class="fa fa-check"></i> 10,000 responses / month</li>
                        <li style="height:50px;"><i class="fa fa-check"></i> <span style="width:90%;">Monetize polls with 
feature – available only  with annual plan</span></li>   
                    </ul>                 
                                   
                </div>
            </div>
            <div class="col-md1-3 col-sm-6">
                <div class="pricing hover-effect">
                    
                   <div class="pricing-head">
                        <h3 style="background:#cc6600;">Publisher</h3>
                        <h4><i><span id="spPub" runat="server" style="font-size:35px;margin-top:39px;"></span></i>
                                 <% if (Request.Browser.Browser == "Firefox")
   { %>
   <span style="margin-top:-40px;">Per Month </span>
<%}
                            else if (Request.Browser.Browser == "Chrome")
                            { %>
                            <span style="margin-top:0px;">Per Month </span>

   <%} %>
                        </h4>
                         <div class="pubfes">Save <span style="font-size:22px;"></span> with </div>
                        <div class="clear"></div>
                       <ul class="pricing-content list-unstyled">
                        
                        <li class="bg-color" style="text-align:center;color:#fff;background:#9e5002;font-size:20px;">Annual Plan - <%=AnnualPub %></li>
                    </ul>
                    </div>
                    <ul class="pricing-content list-unstyled">
                        <li><i class="fa fa-check"></i> Create Text / Image / Video polls</li>
                        <li><i class="fa fa-check"></i> Live preview while creating polls</li>
                        <li><i class="fa fa-check"></i> Option to keep results private </li>
                        <li><i class="fa fa-check"></i> 60,000 responses / month</li>
                        <li style="height:50px;"><i class="fa fa-check"></i></i> <span style="width:90%;">Monetize polls with feature available with monthly and annual plans</span></li> 
                    </ul>                    
                         
                </div>
            </div>
          
        </div>
        </div>
        <!--/row-->
        <!-- End Pricing Rounded -->
        </div>
</div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#lisignup").attr("class", "active");
            $("#boxesprice").hide();
        });
    </script>
     <!-- CSS Page Style -->    
    <link rel="stylesheet" href="assets/css/pages/page_pricing.css">
</asp:Content>
