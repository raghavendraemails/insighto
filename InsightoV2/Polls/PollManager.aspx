﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="PollManager.aspx.cs" Inherits="Poll_PollManager" %>
<%@ Register Src="UserControls/PollPrint.ascx" TagPrefix="uc" TagName="SurveyPrint" %>
<%@ Register Src="UserControls/PollSurveyReportHeader.ascx" TagPrefix="uc" TagName="SurveyHeader" %>
<%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<link rel="stylesheet" type="text/css" href="http://insightopolls-a59.kxcdn.com/css/poll.css" />
<script src="http://insightopolls-a59.kxcdn.com/scripts/pollmanager.js" type="text/javascript"></script>
<asp:HiddenField ID="hdnQuestionBold" runat="server" />
<asp:HiddenField ID="hdnQuestionItalic" runat="server" />
<asp:HiddenField ID="hdnQuestionUL" runat="server" />
<asp:HiddenField ID="hdnQuestionAlign" runat="server" />
<asp:HiddenField ID="hdnQuestionFSize" runat="server" />
<asp:HiddenField ID="hdnQuestionFont" runat="server" />
<asp:HiddenField ID="hdnAnswerBold" runat="server" />
<asp:HiddenField ID="hdnAnswerItalic" runat="server" />
<asp:HiddenField ID="hdnAnswerUL" runat="server" />
<asp:HiddenField ID="hdnAnswerAlign" runat="server" />
<asp:HiddenField ID="hdnAnswerFSize" runat="server" />
<asp:HiddenField ID="hdnAnswerFont" runat="server" />
<asp:HiddenField ID="hdnVoteBold" runat="server" />
<asp:HiddenField ID="hdnVoteItalic" runat="server" />
<asp:HiddenField ID="hdnVoteUL" runat="server" />
<asp:HiddenField ID="hdnVoteFSize" runat="server" />
<asp:HiddenField ID="hdnVoteAlign" runat="server" />
<asp:HiddenField ID="hdnMsgBold" runat="server" />
<asp:HiddenField ID="hdnMsgItalic" runat="server" />
<asp:HiddenField ID="hdnMsgUL" runat="server" />
<asp:HiddenField ID="hdnMsgFSize" runat="server" />
<asp:HiddenField ID="hdnMsgAlign" runat="server" />
<asp:HiddenField ID="hdnPollBgColor" runat="server" />
<asp:HiddenField ID="hdnQuestBgColor" runat="server" />
<asp:HiddenField ID="hdnQuestColor" runat="server" />
<asp:HiddenField ID="hdnAnswerColor" runat="server" />
<asp:HiddenField ID="hdnVoteBgColor" runat="server" />
<asp:HiddenField ID="hdnVoteColor" runat="server" />
<asp:HiddenField ID="hdnHeight" runat="server" />
<asp:HiddenField ID="hdnWidth" runat="server" />
<asp:HiddenField ID="hdnRecHeight" runat="server" />
<asp:HiddenField ID="hdnRecWidth" runat="server" />
<div class="pollmanheight">
<div class="pollmanagermain">
<div class="contentPanelPoll">
<asp:Label ID="lblStatus" runat="server"></asp:Label>
<div><uc:SurveyHeader ID="SurveyHeader" runat="server" Mode="Header" /></div>
<div class="pollmanagerleftpanel"><!-- survey left menu -->
 <div class="pollmanagerpreview">Your poll Preview</div>
        <div class="textpollmanthemeresponse">
                 <div id="imagepreview" runat="server" style="display:none"><img src="App_Themes/images/ImagePoll.jpg" id="imgPreview" runat="server" /></div>
                 <div id="videopreview" runat="server" class="videopreview" style="display:none"></div>
                     <div class="questionpreviewcss">
                         <asp:Label ID="lblQuestion" runat="server"></asp:Label>
                      </div>
                     <div class="clear"></div>
                     <div id="singleradbtns" runat="server">
                     <div class="radiopolltheme"><asp:RadioButtonList CssClass="wrapman" ID="radAnswerOptions" runat="server">
                         </asp:RadioButtonList></div> 
                     </div>
                     <div id="multipleckbbtns" style="display:none" runat="server">
                    <div class="radiopolltheme"><asp:CheckBoxList ID="ckbAnswerOptions" runat="server">
                    </asp:CheckBoxList></div>
                     </div>
                      <div id="commentbox" style="display:none;" runat="server">
                      <div class="commentstxt" >Comments</div>
                     <div><asp:TextBox ID="txtComments"   CssClass="textBoxMedium txtAns txtcomments" runat="server"></asp:TextBox></div>
                     </div>
                     <div class="clear"></div>
                      <div class="viewtest">
                           <div class="viewandvoteimagepreview"><div class="vote">Vote&nbsp;</div> <div class="view"> and View Results</div></div>
                           <div class="sponsorimage" id="sponsorpreviw" runat="server" style="display:none">
                               <div><asp:Label ID="lblSponsor" Text="Sponsored By" runat="server"></asp:Label></div>
                               <div><a href="#" id="logolink" runat="server"><img src="App_Themes/images/logoPlaceholder2.png" runat="server" id="logo" /></a></div>
                           </div>
                       </div>
                       <div class="clear"></div>
                     <div id="poweredby" class="poweredby" runat="server">Powered by <a href="http://www.insighto.com/polls" target="_blank">Insighto Polls</a></div>
                 </div>
       </div>
<div class="pollmanagerrightpanel">
           <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                <div><asp:Label ID="lblSuccessMsg"  runat="server" 
                        meta:resourcekey="lblSuccessMsgResource2"></asp:Label></div>
           </div>
           <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">                
                <div><asp:Label ID="lblErrMsg"  runat="server" 
                        meta:resourcekey="lblErrMsgResource2" ></asp:Label></div>
            </div>
        <div class="buttoncontroldiv">
            <div class="manageLinks" style="border:solid 0 #000;">
        <%--    <asp:LinkButton ID="lBtnCopy" CssClass="copyLink-left"  
                    runat="server" onclick="lBtnCopy_Click">Copy</asp:LinkButton>--%>
            <asp:LinkButton ID="lBtnDelete" CssClass="deleteLink" runat="server" 
                    ToolTip="Delete" OnClientClick="javascript:return DeleteSurvey();" onclick="lBtnDelete_Click"></asp:LinkButton>
            <asp:LinkButton ID="lBtnClose" CssClass="closeLink" runat="server" ToolTip="Close" 
                    meta:resourcekey="lBtnCloseResource2" OnClientClick="javascript:return CloseSurvey();" onclick="lBtnClose_Click"></asp:LinkButton>
    <%--        <asp:LinkButton ID="lBtnActivate" CssClass="activeIconLink" runat="server"  
                    ToolTip="Activate" 
                    meta:resourcekey="lBtnActivateResource2" onclick="lBtnActivate_Click"></asp:LinkButton>--%>
            
            <asp:LinkButton ID ="lBtnReports" runat="server" ToolTip="Reports" 
                    CssClass="top-small-gap icon-Reports-active"  
                    meta:resourcekey="lBtnReportsResource1" onclick="lBtnReports_Click"></asp:LinkButton>  
            <asp:LinkButton ID="lBtnPollEdit" runat="server" ToolTip="Edit" 
                    CssClass="top-small-gap icon-Edit-active"
                    meta:resourcekey="lBtnSurveyEditResource1" onclick="lBtnPollEdit_Click"></asp:LinkButton>  
            <uc:SurveyPrint ID="ucSurveyPrint" runat="server" Mode="Preview" />
           
            <asp:LinkButton ID="lBtnPrint" visible="False" CssClass="printLink" ToolTip="Print" 
                    runat="server"  meta:resourcekey="lBtnPrintResource2" 
                    onclick="lBtnPrint_Click">Print</asp:LinkButton> 
            </div>
        </div>
        <div class="clear"></div>
            <div class="surveyUrlPanel">
               <div style="float:left; height:25px; line-height:25px; padding:0 3px 0 3px;"><asp:Label ID="lblembedslider" runat="server"></asp:Label> </div>
               <div class="clear"></div>
               <div style="float:left; height:35px; line-height:35px; padding:0 3px 0 3px;">
                    <asp:TextBox ID="txtEmbedCode" TextMode="MultiLine" width="400px" Height="60px" CssClass="txtEmbedCode textBoxMedium" 
                        Enabled="False" runat="server"  />
               </div>
               <div style="float:left; height:35px; line-height:35px; padding:47px 3px 0 3px; ">
               <a id="lnkEmbed" style="padding:0; font-weight:normal;" href="javascript:void(0)">Copy</a>
               </div>
            </div>
         <div class="clear"></div>
          <div style="float:left; margin-left:195px; padding-right:30px;" id="dvSurveyLunchText" runat="server" visible="false">

        <div class="informationPanelDefault">
        <div>
            <asp:Label ID="lblSurveyLaunchText" Visible="False"  runat="server" Text="As survey is launched through Insigto email system, to protect data integrity, 
 only preview URL is given." meta:resourcekey="lblSurveyLaunchTextResource1"></asp:Label></div>
</div>   
        </div>
        <div class="clear"></div>
        <div style="margin-top:20px;">
        <input type="hidden" runat="server" id="hdnactivation"  />
             
        </div>
 </div>
 </div>
</div></div>
<script type="text/javascript" src="scripts/jquery.zclip.min.js"></script>
    <script type="text/javascript">
        $("#lnkEmbed").zclip({
            path: '/Scripts/ZeroClipBoard/ZeroClipboard.swf',
            copy: $('.txtEmbedCode').val(),
            beforeCopy: function () {
            },
            afterCopy: function () {
            }

        });
    </script>
<script type="text/javascript" language="javascript">

    function DeleteSurvey() {
        jConfirm('Are you sure you want to delete?', 'Delete Confirmation', function (r) {
            r.preventDefault();
            if (r == '1') {
                //                $.post("AjaxService.aspx", { "method": "DeleteSurveyBySurveyId", "Id": surveyId },
                //           function (result) {
                //               if (result == '0') {
                //                   $('#divSuccess').show();
                //                   $('#lblMessage').html('Survey deleted successfully.');
                //                   var status = $('#hdnStatus').val();

                //                   gridReload(status);
                //               }
                //               else {
                //                   $('#divSuccess').hide();
                //                   $('#lblMessage').attr('style', 'disply:none;');
                //               }
                //           });
            }

        });
    }

    function DeleteSurvey() {
        var answer = confirm('Are you sure you want to delete?');
        if (answer) {
            return true
        }
        return false;
    }

    function CloseSurvey() {
        jConfirm('Are you sure you want to close?', 'Close confirmation', function (r) {
            r.preventDefault();
            if (r == '1') {
                //                $.post("AjaxService.aspx", { "method": "DeleteSurveyBySurveyId", "Id": surveyId },
                //           function (result) {
                //               if (result == '0') {
                //                   $('#divSuccess').show();
                //                   $('#lblMessage').html('Survey deleted successfully.');
                //                   var status = $('#hdnStatus').val();

                //                   gridReload(status);
                //               }
                //               else {
                //                   $('#divSuccess').hide();
                //                   $('#lblMessage').attr('style', 'disply:none;');
                //               }
                //           });
            }

        });
    }

    function CloseSurvey() {
        var answer = confirm('Are you sure you want to close?');
        if (answer) {
            return true
        }
        return false;
    }

    function Activationalert() {
        jConfirm('Are you sure you want to close the survey? Please not that you cannot reactivate the survey once it closed, until you purchase and apply a survey credit ', 'Survey Close', function (r) {

            document.getElementById('<%=hdnactivation.ClientID %>').value = r;

            var f1 = document.forms['Form1'];
            f1.submit();

            return false;
        });

    }

    function Activationalertfree() {
        jConfirm('Are you sure you want to close the survey?', 'Survey Close', function (r) {

            document.getElementById('<%=hdnactivation.ClientID %>').value = r;

            var f1 = document.forms['Form1'];
            f1.submit();

            return false;
        });

    }
    </script>
        <script type="text/javascript">
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-55613473-1', 'auto');
            ga('send', 'pageview');

</script>
</asp:Content>


