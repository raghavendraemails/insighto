﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;

public partial class Polls_paymentconfirmation : System.Web.UI.Page
{
    string paymentid = "";
    string status = "";
    int Usrid = 0;
    int ord_id = 0;
    int tran_status = 0;
    string polllicensetype = "";
    PollCreation pc;
    string urlstr = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        pc =  new PollCreation();
        paymentid = (Request.QueryString["payment_id"] ?? "");
        status = (Request.QueryString["status"] ?? "").ToUpper();
        //if (status == "SUCCESS") {
            GetPaymentInformation();
        //}
    }

    protected void GetPaymentInformation()
    {
        DataTable dt = new DataTable();
        string productslug = "";
        try
        {
            dt = pc.getPollPaymentInformation(paymentid, "Poll");
            if (dt.Rows.Count > 0)
            {
                Int32.TryParse(dt.Rows[0]["USERID"].ToString(), out Usrid);
                Int32.TryParse(dt.Rows[0]["PK_ORDERID"].ToString(), out ord_id);
                status = dt.Rows[0]["status"].ToString().ToUpper();

                productslug = dt.Rows[0]["ProductSlug"].ToString().ToLower();
                if (productslug == "insighto-polls-publisher-monthly")
                {
                    polllicensetype = "PUBLISHER_MONTHLY";
                }
                else if (productslug == "insighto-polls-publisher-yearly")
                {
                    polllicensetype = "PUBLISHER_YEARLY";
                }
                else if (productslug == "insighto-polls-business-monthly")
                {
                    polllicensetype = "BUSINESS_MONTHLY";
                }
                else if (productslug == "insighto-polls-business-yearly")
                {
                    polllicensetype = "BUSINESS_YEARLY";
                }
                else if (productslug == "insighto-polls-professional-monthly")
                {
                    polllicensetype = "PROFESSIONAL_MONTHLY";
                }
                else if (productslug == "insighto-polls-professional-yearly")
                {
                    polllicensetype = "PROFESSIONAL_YEARLY";
                }
                
                if (status == "CREDIT") {
                    urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserInvoiceConfirmationURL(), "ord_id=" + ord_id.ToString() + "&Usrid=" + Usrid.ToString() + "&tran_status=1" + "&polllicensetype=" + polllicensetype);
                    Response.Redirect(urlstr);
                }
                else
                {
                    urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserDeclinedCardURL(), "order_id=" + ord_id.ToString() + "&Usrid=" + Usrid.ToString() + "&Am=0&pk_order=" + ord_id.ToString());
                    Response.Redirect(urlstr);
                }
            }
            else
            {
                urlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserDeclinedCardURL(), "order_id=" + ord_id.ToString() + "&Usrid=" + Usrid.ToString() + "&Am=0&pk_order=" + ord_id.ToString());
                Response.Redirect(urlstr);
            }
        }
        catch (Exception e1)
        {
        }
    }
}