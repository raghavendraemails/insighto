﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PollRespondent.aspx.cs" Inherits="Poll_PollRespondent" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="https://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta property="og:type" content="website" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="viewport" content="width=device-width, maximum-scale=1.0" />
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/poll.css?ver=2.0.0.0" />
    <link href="https://insightopollsssl-a59.kxcdn.com/jalerts/jAlert-v2.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="https://insightopollsssl-a59.kxcdn.com/css/jquery.alerts.css" />
    <script src="https://insightopollsssl-a59.kxcdn.com/scripts/jquery-1.11.1.min.js" type="text/javascript"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.css" type="text/css" rel="stylesheet" media="screen,projection" />
</head>
<body style="overflow:hidden;">
 <form id="form1" runat="server">
    <asp:HiddenField ID="hdnUniqueRes" runat="server" />
<asp:HiddenField ID="hdn1" runat="server" ClientIDMode="Static" Value="11111111"/>
<asp:HiddenField ID="hdnRespondentId" runat="server" />
<asp:HiddenField ID="hdnSrcUrl" runat="server" />
<asp:HiddenField ID="hdnQuestionFont" runat="server" />
<asp:HiddenField ID="hdnQuestionBold" runat="server" />
<asp:HiddenField ID="hdnQuestionItalic" runat="server" />
<asp:HiddenField ID="hdnQuestionUL" runat="server" />
<asp:HiddenField ID="hdnQuestionAlign" runat="server" />
<asp:HiddenField ID="hdnQuestionFSize" runat="server" />
<asp:HiddenField ID="hdnAnswerFont" runat="server" />
<asp:HiddenField ID="hdnAnswerBold" runat="server" />
<asp:HiddenField ID="hdnAnswerItalic" runat="server" />
<asp:HiddenField ID="hdnAnswerUL" runat="server" />
<asp:HiddenField ID="hdnAnswerAlign" runat="server" />
<asp:HiddenField ID="hdnAnswerFSize" runat="server" />
<asp:HiddenField ID="hdnVoteFont" runat="server" />
<asp:HiddenField ID="hdnVoteBold" runat="server" />
<asp:HiddenField ID="hdnVoteItalic" runat="server" />
<asp:HiddenField ID="hdnVoteUL" runat="server" />
<asp:HiddenField ID="hdnVoteFSize" runat="server" />
<asp:HiddenField ID="hdnVoteAlign" runat="server" />
<asp:HiddenField ID="hdnMsgBgColor" runat="server" />
<asp:HiddenField ID="hdnMsgColor" runat="server" />
<asp:HiddenField ID="hdnMsgFont" runat="server" />
<asp:HiddenField ID="hdnMsgBold" runat="server" />
<asp:HiddenField ID="hdnMsgItalic" runat="server" />
<asp:HiddenField ID="hdnMsgUL" runat="server" />
<asp:HiddenField ID="hdnMsgFSize" runat="server" />
<asp:HiddenField ID="hdnMsgAlign" runat="server" />
<asp:HiddenField ID="hdnPollBgColor" runat="server" />
<asp:HiddenField ID="hdnQuestBgColor" runat="server" />
<asp:HiddenField ID="hdnQuestColor" runat="server" />
<asp:HiddenField ID="hdnAnswerColor" runat="server" />
<asp:HiddenField ID="hdnVoteBgColor" runat="server" />
<asp:HiddenField ID="hdnVoteColor" runat="server" />
<asp:HiddenField ID="hdnWidth" runat="server" />
<asp:HiddenField ID="hdnHeight" runat="server" />
<asp:HiddenField ID="hdnRecHeight" runat="server" />
<asp:HiddenField ID="hdnRecWidth" runat="server" />
<asp:HiddenField ID="hdnFbText" runat="server" />
<asp:HiddenField ID="hdnTwitterText" runat="server" />
<asp:HiddenField ID="hdn" runat="server" />
<asp:HiddenField ID="hdnQuestionText" runat="server" />
<asp:HiddenField ID="hdnIsSponsorLogoShowing" runat="server" />
<asp:HiddenField ID="hdnGoogleText" runat="server" />
      <asp:ScriptManager ID="ScriptMgr1" runat="server" AsyncPostBackTimeout="100000">
    <Scripts>
    </Scripts>  
    </asp:ScriptManager>
    <div id="mainpollcontentdiv">
<div class="textpollthemeresponse">
        <asp:Label ID="lblError" CssClass="lblRequired" runat="server" ></asp:Label>
                 <div id="imagepreview" runat="server" style="display:none;width:100%;"><img src="images/ImagePoll.jpg" id="imgPreview" runat="server" style="width:100%;max-width:100%;max-height:128px;"/></div>
                 <div id="videopreview" runat="server" class="videopreview" style="display:none"></div>
                     <div class="questionpreviewcss" id="questiondiv">
                         <asp:Label ID="lblQuestion" runat="server"></asp:Label></div>
                     <div class="clear" id="headerclear"></div>
                     <div id="singleradbtns" runat="server">
                     <div class="radiopolltheme"><asp:RadioButtonList ID="radAnswerOptions" runat="server">
                         </asp:RadioButtonList></div> 
                     </div>
                     <div id="multipleckbbtns" style="display:none" runat="server">
                    <div class="radiopolltheme"><asp:CheckBoxList ID="ckbAnswerOptions" runat="server">
                    </asp:CheckBoxList></div>
                     </div>
                     <div id="purejschart" style="display:none;float:left;margin-left:8px;"></div>
        <div class="messageprivate" id="leadgendiv" style="display:none;margin:0 auto; min-height:38vh;">
            <div style="text-align:center;margin-bottom:4px;" class="txtRow">
                <asp:Label ID="lblMessage" runat="server" Text="Label" style="border-radius:16px; line-height:32px;background-color:teal;"></asp:Label>
            </div>
            <div style="height:100%;text-align:center;" id="data">
                <div id="divphone" runat="server" style="float:left;width:30%;margin-bottom:5%;" class="txtRow">
                    Mobile
                </div>
                <div id="divphonetxt" runat="server" style="float:left;width:70%;margin-bottom:5%;margin-left:0;padding-left:0;text-align:left;">
                    <input type="text" size="30" name="phonenumber" id="phonenumber" maxlength="13" style="width:150px;" size="60"/>
                </div>
                <div id="divemail" runat="server" style="float:left;width:30%;margin-bottom:5%;" class="txtRow">
                    Email
                </div>
                <div id="divemailtxt" runat="server" style="float:left;width:70%;margin-bottom:5%;margin-left:0;padding-left:0;text-align:left;">
                    <input type="text" size="30" name="email" id="email" maxlength="200" size="20" style="width:150px;"/>
                </div>
                <div id="divsubmit" runat="server" style="width:100%;margin-bottom:2%;text-align:center;">
                    <asp:Button ID="btnsubmit" class="viewandvoteimageresponsebtn" runat="server" Text="Submit" Height="30" style="margin:0 auto;"/>
                </div>
            </div>                    
    
        </div>

</div> 

                      <div id="commentbox" style="display:none;margin-bottom:2px;" runat="server">
                      <div class="commentstxt" style="margin-top:0px;" id="lblComments">Comments</div>
                     <div style="margin-top:0px;"><asp:TextBox ID="txtComments" style="width: 270px;margin-left: 10px;height: 16px;margin-top: 0px;padding-top: 0px;font-size: 14px;" runat="server"></asp:TextBox></div>

                     </div>
                      <div class="voteres" runat="server" id="voteres">
                      <div class="viewandvoteimageresponse" id="onlyvote" runat="server"><div class="vote"><asp:Button ID="btnVoteAn" 
                                   CssClass="viewandvoteimageresponsebtn" runat="server" Text="Vote and View Results" 
                                   onclick="btnVote_Click" /></div>
                           </div>
                           <div style="display:none" id="voteandview" runat="server">
                           <asp:LinkButton ID="MyLink" runat="server" CssClass="viewandvoteimageresponse" OnClick="btnVote_Click">
                               <div class="viewandvoteimageresponsebtn"><asp:Label runat="server" Text="and View Results" ID="lblVotenew"></asp:Label>
                                </div>
                           <div class="viewandvoteimageresponsebtn" style="font-size:10px"><asp:Label runat="server" Text="and View Results" ID="lblVotenwView"></asp:Label></div></asp:LinkButton>
                         </div>
                       <div id="socialshares" class="socialshares" runat="server" style="display:none; margin-left:4%;"><asp:Label runat="server" ID="lblShare" style="font-size:14px;"></asp:Label></div>
                       <div class="fullwidth floatL classresultsocial" id="social" runat="server" style="display:none; margin-left:4%;">
                       <div class="floatL socialmargin" id="googleicon" runat="server" style="display:none"><a href="#" onclick="gPlus('https://example.com');" title="+1"><img src="images/google.png" /></a></div>
                       <div class="floatL socialmargin" id="twittericon" runat="server" style="display:none">
                       <a onclick="clicked_twitter_button(this);" href="#" runat="server" id="twitter"  data-url="false" data-size="large" data-count="none" data-dnt="true" ><img src="images/twitter.png" /></a></div>
                       <div class="floatL socialmargin" id="fbicon" runat="server" style="display:none">
                           <a href="#" onclick="fbshareCurrentPage()" ><img src="images/facebook.png" alt="Share on Facebook"/></a>
                       </div>
                       </div>
                        <div class="sponsorimage" id="sponsorpreviw" runat="server" style="display:block;margin-bottom:1%;right:4%;position:absolute;">
                               <div><asp:Label ID="lblSponsor" Text="Sponsored By" runat="server" style="font-size:14px;"></asp:Label></div>
                               <div><a href="#" id="logolink" target="_blank" runat="server"><img src="images/logoPlaceholder2.png" runat="server" id="logo" style="max-height:90px; /*max-width:43px;height:43px;*/ width:90px;"/></a></div>
                           </div>
                       </div>
                       <div class="voteresview" runat="server" id="voteresnew" style="margin-bottom:0px;">
                      <div class="viewandvoteimageresponse" id="onlyvoteres" runat="server" style="margin-left:2%;"><div class="vote"><asp:Button ID="btnVoteNew" 
                                   CssClass="viewandvoteimageresponsebtn" runat="server" Text="Vote" 
                                   onclick="btnVote_Click" /></div></div>
                           <div style="display:none;margin-left:2%;" id="voteviewres" runat="server">
                           <asp:LinkButton ID="LinkNew" runat="server" CssClass="viewandvoteimageresponse" OnClick="btnVote_Click"><div class="viewandvoteimageresponsebtn">Vote</div></asp:LinkButton>
                           </div>
                           <div class="sponsorimage" id="sponsorres" runat="server" style="display:none;right:3%;position:absolute;">
                               <div><asp:Label ID="lblSpoRes" Text="Sponsored By" runat="server"></asp:Label></div>
                               <div><a href="#" id="logolinknew" target="_blank" runat="server"><img src="images/logoPlaceholder2.png" runat="server" id="imgResultsLogo" style="max-height:90px; /*max-width:50px;height:50px;*/ width:90px;" /></a></div>
                       </div>

                       </div>
                       <div class="clear" style="height:15px;"></div>
                     <div id="poweredby" class="poweredbyrep" runat="server" style="font-size:13px;">Powered by <a href="https://www.insighto.com/Polls/" target="_blank">Insighto Polls</a></div>
                 
    </div>
        
       
<div id="messagebox" style="display:none;" runat="server">    

</div>
          <div class="classmsg" style="display:none;border-radius:16px; line-height:32px;background-color:teal;padding-top:0px !important;" id="thankyouoverlay" runat="server">
     <!--<asp:Label ID="thankyouoverlaymessage" runat="server"></asp:Label>--></div>
        <asp:HiddenField runat="server" id="resppollid" ClientIDMode="Static" />
        <asp:HiddenField runat="server" id="respid" ClientIDMode="Static" />
        <asp:Label runat="server" ID="dojavascript" ClientIDMode="Static"></asp:Label>
        <asp:HiddenField runat="server" ID="hdnRedirectUrl" ClientIDMode="Static"/>
        <asp:HiddenField runat="server" ID="hdnHasRedirectUrl" ClientIDMode="Static"/>
        <asp:HiddenField runat="server" ID="lblcontent" ClientIDMode="Static"/>
        <asp:Label runat="server" ID="fusionchartscontent" ClientIDMode="Static"></asp:Label>
    </form>
    <script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>
    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
    <script src="scripts/respondent.js?ver=2.0.0.0" type="text/javascript"></script>
    <script src="scripts/leadgen.js?ver=2.0.0.0" type="text/javascript"></script>
    <script src="https://insightopollsssl-a59.kxcdn.com/jalerts/jAlert-v2.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://insightopollsssl-a59.kxcdn.com/scripts/jquery.alerts.js"></script>
    <script type="text/javascript" src="https://insightopollsssl-a59.kxcdn.com/scripts/jquery-migrate-1.2.1.js"></script>
    <script type="text/jscript" src="https://connect.facebook.net/en_US/all.js"></script>

    <script type="text/javascript">
   
        function fbshareCurrentPage() {
            var videolink = "";
            //var result = document.getElementById('<%=hdnSrcUrl.ClientID %>').value;
            var result = $("#hdnSrcUrl").val() + "&title=" + $("#hdnQuestionText").val() + "&description=Click to Vote";
            //result = $("#hdnSrcUrl").val();//  + "&description=" + text;
            //if ($("#hdnPollType").val() == "image") {
            //    result += "&image=" + $("#hdnFBImage").val();
            //}
            window.open("https://www.facebook.com/sharer/sharer.php?u=" + result, 'popupwindow', 'scrollbars=yes,width=800,height=400').focus();
            //Materialize.toast("Shared Successfully Via Facebook", 3000);
            return false;
        }

        function gPlus(url) {
            var result = $("#hdnSrcUrl").val();
            //window.open('https://plus.google.com/share?title=' + $("#hdnQuestionText").val() + ' ' + text + '&description=' + text + '&url=' + result, 'popupwindow', 'scrollbars=yes,width=800,height=400').focus();
            window.open('https://plus.google.com/share?url=' + result, 'popupwindow', 'scrollbars=yes,width=800,height=400').focus();
            //Materialize.toast("Shared Successfully Via Google Plus", 3000);
            return false;
        }

          // Login in the current user via Facebook and ask for email permission
          function postToFB() {
              var i = 0;
              var error_no_option = 0, error_no_fb = 0;
              var error_msg;
              
              // Initialize the Facebook JavaScript SDK
              FB.init({
                  appId: '816696718359618',
                  xfbml: true,
                  status: true,
                  cookie: true
              });

              FB.login(checkLoginStatus, { scope: 'email' });
              return false;
          }

          // Check the result of the user status and display login button if necessary
          function checkLoginStatus(response) {
              var email;
              var shareURL = document.getElementById('<%=hdnSrcUrl.ClientID %>').value;
              var intro = document.getElementById('<%=hdnFbText.ClientID %>').value;
              if (response && response.status == 'connected') {
                  FB.api('/me', function (resp) {
                      email = resp.email;
                  });
                  FB.api('me/friends/', function (resp) {
                      var length = 0;
                      for (var dummy in resp.data) length++;
                      FB.ui(
                {
                    method: 'feed',
                    name: intro,
                    link: shareURL,
                    picture: ''
                },
                function (response) {
                    if (response && response.post_id) {
                        var parms = email + ";" + length;
                        jAlert("Shared Successfully Via Facebook");
                    }
                }
                );
                  });
              }

          }
    </script>
    <script type="text/javascript">

        function clicked_twitter_button(anchor_instance) {
            //var result = document.getElementById('<%=hdnTwitterText.ClientID %>').value;
            //var shareURL = document.getElementById('<%=hdnSrcUrl.ClientID %>').value;
            var result = document.getElementById('<%=hdnSrcUrl.ClientID %>').value;
            result = $("#hdnQuestionText").val() + " Click to vote " + $("#hdnSrcUrl").val();
            document.getElementById('<%= twitter.ClientID%>').href = "https://twitter.com/intent/tweet?text=" + result;
        }

        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to tweet message?")) {
                confirm_value.value = "Yes";
                window.open("https://twitter.com/share", 'Twitter', 'height=600,width=600,left=200,top=40,resizable=no,scrollbars=no,toolbar=no,menubar=no,status=no');

            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }


        //twttr.events.bind('tweet', function (event) {
        //    var parms = "TWITTER_USER" + ";" + 50;
        //    //jAlert("Shared Successfully via Twitter");
        //});

        function OnSucceededtwitter(result) {
            var a = result.split(";")
            displaySuccess("twitter", a[0], a[1]);
        }
    
 </script>
     <script type="text/javascript">
         (function (i, s, o, g, r, a, m) {
             i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                 (i[r].q = i[r].q || []).push(arguments)
             }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
         })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

         ga('create', 'UA-55613473-1', 'auto');
         ga('send', 'pageview');

         var thisone = "1";

</script>
    <script>
        $(document).ready(function () {
            $("#metaogtitle").attr("content", $("#lblQuestion").html());
        });

    </script>
</body>
</html>
