﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="superpoll.aspx.cs" Inherits="Polls_superpoll" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <link rel="stylesheet" type="text/css" href="https://insightopollsssl-a59.kxcdn.com/css/poll.css" />
    
<script src="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
<link href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/start/jquery-ui.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="https://insightopollsssl-a59.kxcdn.com/scripts/jquery.alerts.js"></script>
<script src="https://insightopollsssl-a59.kxcdn.com/scripts/customscripts.js" type="text/javascript"></script>
    <script src="WebScripts/tooltip.js" type="text/javascript"></script>
    <script src="WebScripts/ajax.js" type="text/javascript"></script>
    <link href="WebCss/stylethumb.css" rel="stylesheet" type="text/css" />

    <link href="WebCss/styles.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Arimo:400,700' rel='stylesheet' type='text/css' />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="WebScripts/jquery.easing.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/json2/20130526/json2.min.js"></script>

    <link href="WebCss/font-awesome.min.css" rel="stylesheet" type="text/css" />
    
<script type="text/javascript">
    var oldpollno;
    var pollno;
    var lpoll;
    var lpollout;
    function ShowCurrentTime(pollid) {
        var obj = {};
        var paramlist;
        obj.pollid = pollid;
        $.ajax({
            type: "POST",
            url: "superpoll.aspx/getPollembedcode",
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            async: false,
            dataType: "json",
            success: function (r) {           
              
                paramlist = r.d;
                             
            }
        });
     
        paramlist = JSON.stringify(paramlist).toString().replace('"', '');
        paramlist = paramlist.toString().replace('"', '');
   
       return paramlist;       
    }


    $(window).load(function () {
        $(function () {
            $(".polls_list_panle ul li label").hover(function (event) {


                var slide = $(this).attr("for");

                pollno = slide.toString().replace('#box', '');


                //                alert("oldpollno=" + oldpollno);
                //                alert("pollno=" + pollno);
                //                alert("lpoll=" + lpoll);
                //                alert("lpollout=" + lpollout);
                //                if (lpollout == pollno) {
                //                    var output = document.getElementById('pollcontent');
                //                    output.innerHTML = "";                  
                //                }


                if (oldpollno != pollno) {

                    var emblnkpoll = "";
                    emblnkpoll = ShowCurrentTime(pollno);

                    var res = emblnkpoll.split("-");

                    emblnk = res[0];
                    embwidth = res[1];
                    embheight = parseInt(res[2]) + 20;

                    var output = document.getElementById('pollcontent' + pollno);

                    var ifr = $('<iframe/>', {
                        id: 'IframeLaunch',
                        src: emblnk,
                        style: 'width:' + embwidth + 'px;height:' + embheight + 'px;-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px; -moz-box-shadow: 4px 4px 14px #000; -webkit-box-shadow: 4px 4px 14px #000; box-shadow: 4px 4px 14px #000;',
                        load: function () {
                            $(this).show();
                        }
                    });


                    $(output).html(ifr);

                    $(".poll_prev_panel").not(slide).css("display", "none");
                    $(slide).fadeIn();

                }

            });
        });

    });                 

    (function ($) {
        $.fn.vAlign = function () {
            return this.each(function (i) {
                var h = $(this).height();               
                var oh = $(this).outerHeight();             
              //  var mt = (h + (oh - h)) / 2;
              var mt = h;             
               // $(this).css("margin-top", "-" + mt + "px");
                $(this).css("margin-top", "60px");
                $(this).css("margin-left", "2px");
               // $(this).css("top", "50%");
                $(this).css("position", "absolute");
            });
        };
    })(jQuery);
    $(document).ready(function () {        
        $(".poll_prev_panel").vAlign();
    });

    $(function () {

        $(".polls_list_panle li label, .slider_panel").click(function () {

            $(".slider_panel").stop().animate({ right: "0px" }, 1500, "easeOutExpo");
            lpoll = pollno;
            return false;

        });
        $(".slider_panel_close_btn").click(function () {
            $(".slider_panel").stop().animate({ right: "-500px" }, 700, "easeOutExpo");
            oldpollno = pollno;
            lpollout = lpoll;
            return false;
        });

    });
    
</script>

<div id="dialog" style="display: none;">
<div id="dialogContent"></div>
</div>
  <div class="contentPanelPoll" style="background-color:White;">
    <div class="createpollbg">
    <asp:Label runat="server" Text="Create Poll" ID="lblCreate"></asp:Label>
    </div>
    <div class="clear"></div>        
  
  <!--Slide end-->
<div class="slider_panel" style="right: -499.084625244141px;">

	<!--box 1-->
     <%for (int i = 0; i < dtsettings.Rows.Count; i++)
       {
           pollid = Convert.ToInt32(dtsettings.Rows[i]["fk_pollid"].ToString());
      %>
	<div class="poll_prev_panel" id="box<%=pollid%>">    	
        <div class="poll_content" id="pollcontent<%=pollid%>" >
       	  	<%--<%=dtsettings.Rows[i]["embedcode"].ToString() %>--%>
        </div>    
  </div>
  <%} %> 
     <a class="slider_panel_close_btn" href="#"><i class="fa fa-times fa-2x"></i></a>
</div>

<!--Page Content start-->
<div class="top_btn_panel">
	<a href="<%=newpollpath %>" class="add_icn btn_green_style">Create New Poll</a>
    <label>Or Replace any Active poll</label>
</div>
<div class="polls_list_panle">
	<ul>
     <%for (int i = 0; i < dtsettings.Rows.Count; i++)
      {
          pollid = Convert.ToInt32(dtsettings.Rows[i]["fk_pollid"].ToString());
         string replaceurl = Insighto.Business.Helpers.EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + pollid + "&polltype=superpoll"); 
      %>
    	<li>
        	<label for="#box<%=pollid %>">
            <b><%=dtsettings.Rows[i]["name"].ToString() %></b>
            <p><%=dtsettings.Rows[i]["questiontext"].ToString() %></p>
            <a href="#"><i class="fa fa-eye"></i>  Preview</a>
            </label>
            <span><a href="<%=replaceurl%>" class="replace_icn"><i class="fa fa-check"></i>  Select</a></span>
        </li>              
        <%} %>
    </ul>
</div>


<center>
	<img src="WebImages/icon_loading.gif" alt="" id="loader"/>
	</center>
</div>

<style>
		html{background:#84eeee;}
		/* This only works with JavaScript, 
		   if it's not present, don't show loader */
		.no-js #loader { display: none;  }
		.js #loader {  display: block; position: fixed; left: 50%; top: 50%; right:50%; bottom:50%; }
		
	</style>
	
	<%--
	<script src="WebScripts/jquery.min.js"></script>--%>
	<script src="WebScripts/modernizr.js"></script>
	<script>
	    // Wait for window load
	    $(window).load(function () {
	        $('#loader').fadeOut(2000);
	    });
		
	</script>



      <script type="text/javascript">
          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-55613473-1', 'auto');
          ga('send', 'pageview');

</script>
</asp:Content>