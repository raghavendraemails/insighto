﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Business.geoip;
using System.Text;
using System.Configuration;
using Insighto.Data;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Net;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Serialization;
//using FiftyOne.Foundation.Mobile.Detection;


public partial class Poll_PollRespondent : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int pollId = 0;
    int parentpollId = 0;
    DataSet pollInfo = new DataSet();
    DataTable dtSettingsInfo = new DataTable();
    PollCreation Pc = new PollCreation();
    string resultaccess;
    DataTable tbAnswerOption = new DataTable();
    DataSet licInfo = new DataSet();
    int respondentId;
    int answerId = 0, questionType = 1;
    string device = "";
    string mobileBrand = "";
    ListItem answerOption;
    string platform = "", mobilePlatform = "";
    public String statusCode { get; set; }
    public String statusMessage { get; set; }
    public String IpAddress { get; set; }
    public String countryCode { get; set; }
    public String countryName { get; set; }
    public String regionName { get; set; }
    public String cityName { get; set; }
    public String zipCode { get; set; }
    public String latitude { get; set; }
    public String longitude { get; set; }
    public String timeZone { get; set; }
    // string globalurl = "http://insighto.com:8010";
    string globalurl;
    string foldername = "";
    bool isPoweredByShowing = false;
    bool isSponsorLogoShowing = false;
    bool isSocialSharingShowing = false;
    int chartheight = 0;
    int chartwidth = 0;
    string pollsource = "";
    public string facebooktitle = "Insighto Polls";
    public string facebookimage = "https://www.insighto.com/App_Themes/Classic/Images/insighto_logo_polls_FB.png";
    string rootURL = "";
    string pollsURL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //Request.Headers.Add("X-XSS-Protection", "allow");
        string ipAddress = "";
        string querystring = "";
        globalurl = ConfigurationSettings.AppSettings["RootURL"].ToString();

        rootURL = ConfigurationSettings.AppSettings["RootURL"].ToString();
        pollsURL = ConfigurationSettings.AppSettings["PollsURL"].ToString();

        if (Request.QueryString["key"] != null)
        {
            querystring = Request.QueryString["key"].ToString();
            querystring = WebUtility.HtmlEncode(querystring);
            if (Request.UrlReferrer != null)
            {
                if (Request.UrlReferrer.ToString().Contains("facebook.com"))
                {
                    querystring = querystring.Replace("%2F", "/");
                    querystring = querystring.Replace("+", "%2B");
                }
            }
            typeHt = EncryptHelper.DecryptQuerystringParam(querystring);

            if (typeHt.Contains("ParentPollId"))
            {
                Int32.TryParse(typeHt["ParentPollId"].ToString(), out parentpollId);
            }
            else if (typeHt.Contains("PollId"))
            {
                Int32.TryParse(typeHt["PollId"].ToString(), out parentpollId);
            }
            if (parentpollId > 0)
            {
                Response.Redirect("/" + EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenIframeURL(), "PollId=" + parentpollId + ""));
                Response.End();
            }
            if (typeHt.Contains("pollsource"))
            {
                pollsource = typeHt["pollsource"].ToString();
            }
            
            DataSet dspollid = Pc.getPollInfofromparent(parentpollId);
            if (dspollid.Tables[0].Rows.Count > 0)
            {
                pollId = Convert.ToInt32(dspollid.Tables[0].Rows[0][0].ToString());

                //DataSet thisPollInfo = Pc.getPollInfo(parentpollId);

                ////THIS MEANS DISPLAY TYPE IS SLIDER BUT POLL TYPE IS EMBED SO DON'T SHOW
                //if (dspollid.Tables[0].Rows[0][1].ToString() == "" && pollsource == "slider")
                //{
                //    Response.Redirect("lastpage.aspx");
                //}
                ////THIS MEANS DISPLAY TYPE IS EMBED BUT POLL TYPE IS SLIDER SO DON'T SHOW
                //else if (dspollid.Tables[0].Rows[0][1].ToString() == "slider" && pollsource == "embed")
                //{
                //    Response.Redirect("lastpage.aspx");
                //}
            }
            else
            {
                pollId = parentpollId;
            }
        }
        hdn.Value = pollId.ToString();
        foldername = @"images\" + Convert.ToString(pollId);
        form1.Action = globalurl + Request.RawUrl;
        lblError.Text = "";
        lblError.Attributes.Add("display", "none");
        Session["respondentId"] = "";
        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
        {
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        }
        //ipAddress = "183.83.200.83";

        HttpRequest httpRequest = HttpContext.Current.Request;
        if (httpRequest.Browser.IsMobileDevice)
        {
            device = "mobile";
            mobileBrand = httpRequest.Browser.MobileDeviceManufacturer;
            mobilePlatform = httpRequest.Browser.Platform;
        }
        else
        {
            device = "desktop";
            platform = httpRequest.Browser.Platform;
        }
        //hdnSrcUrl.Value = "http://www.vinserve.in";
        if (Request.UrlReferrer == null)
        {
        
        }
        else
        {
            hdnSrcUrl.Value = Request.UrlReferrer.ToString();
        }

        if (!IsPostBack)
        {
            Session["respondentId"] = "";
            if (Request.Cookies["respondent" + pollId.ToString()] == null)
            {
                HttpCookie respCookie = new HttpCookie("respondent" + pollId.ToString());
                respCookie.Value = respondentId.ToString();
                respCookie.Expires = DateTime.Now.AddDays(10);
                //respCookie.Domain = "127.0.0.1";
                respCookie.Path = "/";
                respCookie.HttpOnly = false;
                Response.Cookies.Add(respCookie);
            }

            BindDesignStyles();
            try
            {
                Location location = getIPDetails(ipAddress);
                if (location != null)
                {
                    IpAddress = (ipAddress ?? "");
                    countryCode = (location.countryCode ?? "");
                    countryName = (location.countryName ?? "");
                    regionName = (location.regionName ?? "");
                    cityName = (location.city ?? "");
                    zipCode = (location.postalCode ?? "");
                    latitude = (location.latitude.ToString() ?? "");
                    longitude = (location.longitude.ToString() ?? "");
                    timeZone = "";
                }
                else {
                    IpAddress = "";
                    countryCode = "";
                    countryName = "NOT FOUND";
                    regionName = "";
                    cityName = "";
                    zipCode = "";
                    latitude = "";
                    longitude = "";
                    timeZone = "";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //if (res != null)
                //{ res.Close(); }
            }
            licInfo = Pc.getPollLicenceInfo(pollId);

            if (!hdnSrcUrl.Value.Contains("CreatePoll.aspx"))
            {
                if (licInfo.Tables[2].Rows.Count > 0)
                {
                    // string domainname = Request.UrlReferrer.Host.ToString();
                    // string domainname = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.ToString();
                    //var uri = new Uri(Request.RawUrl); // to get the url from request or replace by your own
                    //var domainname = uri.GetLeftPart(UriPartial.Authority);

                    string domainname = Request.Url.Host;

                    if (licInfo.Tables[2].Rows[0][0].ToString() == "")
                    {
                        Pc.updateDomainname(Convert.ToInt32(licInfo.Tables[2].Rows[0][1].ToString()), domainname);
                    }
                    else if ((licInfo.Tables[2].Rows[0][0].ToString() != domainname) && (pollsource != "weblink") && (!domainname.Contains("localhost")))
                    {
                        Response.Write("SORRY! You would not be able to embed your Poll with another domain than the one you have already used earlier. Create another account in Insighto Polls to embed this poll in this domain. For any queries - support@insighto.com");
                        Response.End();
                    }
                }
            }

            if (licInfo.Tables[1].Rows.Count > 0)
            {
                if (licInfo.Tables[1].Rows[0][0].ToString() == "Professional_freetrial" || licInfo.Tables[1].Rows[0][0].ToString() == "Professional")
                {
                    if (Convert.ToInt32(licInfo.Tables[0].Rows[0][0].ToString()) > 3000)
                    {
                        Response.Redirect("lastpage.aspx");
                    }
                }
                else if (licInfo.Tables[0].Rows[0][0].ToString() == "Business_freetrial" || licInfo.Tables[0].Rows[0][0].ToString() == "Business")
                {
                    if (Convert.ToInt32(licInfo.Tables[0].Rows[0][0].ToString()) > 10000)
                    {
                        Response.Redirect("lastpage.aspx");
                    }
                }
            }
            pollInfo = Pc.getPollInfo(pollId);

            facebooktitle = pollInfo.Tables[0].Rows[0]["questiontext"].ToString();
            if (pollInfo.Tables[0].Rows[0]["type"].ToString() == "Image")
            {
                facebookimage = rootURL + "/polls/images/" + pollId.ToString() + "/" + pollInfo.Tables[0].Rows[0]["imagelink"].ToString();
            }
            hdnQuestionText.Value = facebooktitle;
            string sourceURL = "";
            sourceURL = hdnSrcUrl.Value;
            if (!String.IsNullOrWhiteSpace(Request.QueryString["utm_source"]))
            {
                sourceURL = Request.QueryString["utm_source"];
            }
            if (sourceURL.Length > 100)
            {
                sourceURL = sourceURL.Substring(0, 100);
            }
            hdnSrcUrl.Value = sourceURL;

            string facebookdescription = "Click to vote";
            if (pollInfo.Tables[0].Rows[0]["type"].ToString() == "video")
            {
                facebookdescription = "WATCH THE VIDEO and give your opinion";
            }
            HtmlMeta metaTag = new HtmlMeta();
            metaTag.Attributes.Add("property", "og:title");
            metaTag.Content = facebooktitle;// +" (" + facebookdescription + ")";
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("itemprop", "og:title");
            metaTag.Content = facebooktitle;// +" (" + facebookdescription + ")";
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("property", "og:description");
            metaTag.Content = facebookdescription;
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("itemprop", "og:description");
            metaTag.Content = facebookdescription;
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("property", "og:image");
            metaTag.Content = ConfigurationManager.AppSettings["socialmediashareimage"].ToString();
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("itemprop", "og:image");
            metaTag.Content = ConfigurationManager.AppSettings["socialmediashareimage"].ToString();
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("property", "og:url");
            metaTag.Content = hdnSrcUrl.Value + "&v=" + DateTime.Now.Ticks.ToString();
            this.Header.Controls.Add(metaTag);

            metaTag = new HtmlMeta();
            metaTag.Attributes.Add("itemprop", "og:url");
            metaTag.Content = hdnSrcUrl.Value + "&v=" + DateTime.Now.Ticks.ToString();
            this.Header.Controls.Add(metaTag);


            if (!hdnSrcUrl.Value.Contains("CreatePoll.aspx"))
            {
                if (pollInfo.Tables[0].Rows[0]["status"].ToString() == "Active")
                {
                    //DOING THIS CHECK SO THAT WHEN COOKIE IS CHECKED THE VISIT COUNT DOES NOT INCREASE TWICE
                    ///BUT THIS IS ALSO TO TAKE CARE FROM THE SAME USER BEING ABLE TO VOTE TWICE BY USING THE BACK BUTTON ON THE FRAME.
                    //if ((Request.Cookies["respondent" + pollId.ToString()].Value == "0") || (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["uniquerespondent"]) == 1))
                    if (Request.Cookies["respondent" + pollId.ToString()].Value == "0")
                    {
                        respondentId = Pc.InsertIntoRespondent(pollId, "visits", countryName, regionName, cityName, IpAddress, longitude, latitude, device, platform, mobileBrand, mobilePlatform, hdnSrcUrl.Value);
                        hdnRespondentId.Value = respondentId.ToString();
                        Session["respondentId"] = respondentId.ToString();
                        respid.Value = respid.ToString();
                    }
                    else //if(Convert.ToInt32(pollInfo.Tables[0].Rows[0]["uniquerespondent"]) == 0)
                    {
                        //Pc.InsertIntoRespondent(pollId, "visits", countryName, regionName, cityName, IpAddress, longitude, latitude, device, platform, mobileBrand, mobilePlatform, hdnSrcUrl.Value);
                        respid.Value = Request.Cookies["respondent" + pollId.ToString()].Value;
                        hdnRespondentId.Value = Convert.ToString(Request.Cookies["respondent" + pollId.ToString()].Value);
                        Session["respondentId"] = Request.Cookies["respondent" + pollId.ToString()].Value;
                    }
                }
            }
            else {
                hdnRespondentId.Value = "0";
            }
            respid.Value = Request.Cookies["respondent" + pollId.ToString()].Value;
        }
    }

    public class ResponseDetails
    {
        public string Ip { get; set; }
        public string Countrycode { get; set; }
        public string CountryName { get; set; }
        public string RegionCode { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string MetroCode { get; set; }
    }
    protected void BindDesignStyles()
    {
        pollInfo = Pc.getPollInfo(pollId);
        dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
        hdnUniqueRes.Value = pollInfo.Tables[0].Rows[0]["uniquerespondent"].ToString();
        if (pollInfo.Tables[0].Rows[0][5].ToString() == "Closed" || Convert.ToInt32(pollInfo.Tables[0].Rows[0][4].ToString()) == 1)
        {
            Response.Redirect("lastpage.aspx");
        }
        if (pollInfo.Tables[0].Rows[0][3].ToString() == "Image")
        {
            if (pollInfo.Tables[0].Rows[0][19].ToString() != "")
            {
                imagepreview.Style.Add("display", "block");
                imgPreview.Attributes.Add("src", pollsURL + foldername + @"/" + pollInfo.Tables[0].Rows[0]["imagelink"].ToString());
            }
        }
        else if (pollInfo.Tables[0].Rows[0][3].ToString() == "Video")
        {
            if (pollInfo.Tables[0].Rows[0][18].ToString() != "")
            {
                videopreview.Style.Add("display", "block");
                videopreview.InnerHtml = pollInfo.Tables[0].Rows[0][18].ToString();
            }
        }
        if (Convert.ToBoolean(dtSettingsInfo.Rows[0]["countvotes"].ToString()) == true)
        {
            DataSet dspollInfo = Pc.getpollchartinfo(pollId);
            //lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString() + " (Votes : " + dspollInfo.Tables[4].Rows[0][0].ToString() + ")";
            lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
        }
        else
        {
            lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
        }
        if (pollInfo.Tables[0].Rows[0][9].ToString() == "private")
        {
            resultaccess = "private";
            lblMessage.Text = pollInfo.Tables[0].Rows[0][10].ToString();
        }
        else
        {
            resultaccess = "public";
        }
        if (Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()) == 1)
        {
            singleradbtns.Style.Add("display", "block");
            multipleckbbtns.Style.Add("display", "none");
            tbAnswerOption = Pc.getAnswerOptions(pollId);
            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                foreach (DataRow row in tbAnswerOption.Rows)
                {
                    answerOption = new ListItem(HttpUtility.HtmlEncode(row["answeroption"].ToString()), row["pk_answerid"].ToString());
                    radAnswerOptions.Items.Add(answerOption);
                }
            }
            questionType = 1;
        }
        else if (Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()) == 2)
        {
            questionType = 2;
            singleradbtns.Style.Add("display", "none");
            multipleckbbtns.Style.Add("display", "block");
            tbAnswerOption = Pc.getAnswerOptions(pollId);
            if (tbAnswerOption != null && tbAnswerOption.Rows.Count > 0)
            {
                foreach (DataRow row in tbAnswerOption.Rows)
                {
                    answerOption = new ListItem(HttpUtility.HtmlEncode(row["answeroption"].ToString()), row["pk_answerid"].ToString());
                    ckbAnswerOptions.Items.Add(answerOption);
                }
            }
        }
        if (Convert.ToBoolean(pollInfo.Tables[0].Rows[0]["commentreq"].ToString()) == true)
        {
            commentbox.Style.Add("display", "block");
        }
        else
        {
            commentbox.Style.Add("display", "none");
        }

        if (dtSettingsInfo != null && dtSettingsInfo.Rows.Count > 0)
        {
            if (dtSettingsInfo.Rows[0]["pollbgcolor"].ToString() != "")
            {
                hdnPollBgColor.Value = dtSettingsInfo.Rows[0]["pollbgcolor"].ToString();
            }
            else
            {
                hdnPollBgColor.Value = "ffffff";
            }
            if (dtSettingsInfo.Rows[0]["sharetext"].ToString() != "")
            {
                lblShare.Text = dtSettingsInfo.Rows[0]["sharetext"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votetext"].ToString() != "")
            {
                string myString = dtSettingsInfo.Rows[0]["votetext"].ToString();
                //btnVoteAn.Text = myString;
                if (myString.IndexOf(" ") >= 0)
                {
                    string st = myString.Substring(0, myString.IndexOf(" "));
                    lblVotenew.Text = st;
                    var stn = myString.Substring(myString.IndexOf(" ") + 1);
                    lblVotenwView.Text = stn;
                }
                else
                {
                    lblVotenew.Text = myString;
                    lblVotenwView.Text = "";
                }
            }
            if (dtSettingsInfo.Rows[0]["questionfont"].ToString() != "")
            {
                hdnQuestionFont.Value = dtSettingsInfo.Rows[0]["questionfont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfont"].ToString() != "")
            {
                hdnAnswerFont.Value = dtSettingsInfo.Rows[0]["answerfont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefont"].ToString() != "")
            {
                hdnMsgFont.Value = dtSettingsInfo.Rows[0]["messagefont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["googletext"].ToString() != "")
            {
                hdnGoogleText.Value = dtSettingsInfo.Rows[0]["googletext"].ToString();
                googleicon.Style.Add("display", "block");
            }
            if (dtSettingsInfo.Rows[0]["twittertext"].ToString() != "")
            {
                hdnTwitterText.Value = dtSettingsInfo.Rows[0]["twittertext"].ToString();
                twittericon.Style.Add("display", "block");
            }
            if (dtSettingsInfo.Rows[0]["fbtext"].ToString() != "")
            {
                hdnFbText.Value = dtSettingsInfo.Rows[0]["fbtext"].ToString();
                fbicon.Style.Add("display", "block");
            }
            if (dtSettingsInfo.Rows[0]["messagebgcolor"].ToString() != "")
            {
                hdnMsgBgColor.Value = dtSettingsInfo.Rows[0]["messagebgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagecolor"].ToString() != "")
            {
                hdnMsgColor.Value = dtSettingsInfo.Rows[0]["messagecolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefont"].ToString() != "")
            {
                hdnVoteFont.Value = dtSettingsInfo.Rows[0]["votefont"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questioncolor"].ToString() != "")
            {
                hdnQuestColor.Value = dtSettingsInfo.Rows[0]["questioncolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionbgcolor"].ToString() != "")
            {
                hdnQuestBgColor.Value = dtSettingsInfo.Rows[0]["questionbgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answercolor"].ToString() != "")
            {
                hdnAnswerColor.Value = dtSettingsInfo.Rows[0]["answercolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votebgcolor"].ToString() != "")
            {
                hdnVoteBgColor.Value = dtSettingsInfo.Rows[0]["votebgcolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votecolor"].ToString() != "")
            {
                hdnVoteColor.Value = dtSettingsInfo.Rows[0]["votecolor"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionfontweight"].ToString() != "")
            {
                hdnQuestionBold.Value = dtSettingsInfo.Rows[0]["questionfontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionstyle"].ToString() != "")
            {
                hdnQuestionItalic.Value = dtSettingsInfo.Rows[0]["questionstyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionunderline"].ToString() != "")
            {
                hdnQuestionUL.Value = dtSettingsInfo.Rows[0]["questionunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionalign"].ToString() != "")
            {
                hdnQuestionAlign.Value = dtSettingsInfo.Rows[0]["questionalign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfontweight"].ToString() != "")
            {
                hdnAnswerBold.Value = dtSettingsInfo.Rows[0]["answerfontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerstyle"].ToString() != "")
            {
                hdnAnswerItalic.Value = dtSettingsInfo.Rows[0]["answerstyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerunderline"].ToString() != "")
            {
                hdnAnswerUL.Value = dtSettingsInfo.Rows[0]["answerunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answeralign"].ToString() != "")
            {
                hdnAnswerAlign.Value = dtSettingsInfo.Rows[0]["answeralign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefontweight"].ToString() != "")
            {
                hdnVoteBold.Value = dtSettingsInfo.Rows[0]["votefontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votestyle"].ToString() != "")
            {
                hdnVoteItalic.Value = dtSettingsInfo.Rows[0]["votestyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["voteunderline"].ToString() != "")
            {
                hdnVoteUL.Value = dtSettingsInfo.Rows[0]["voteunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votealign"].ToString() != "")
            {
                hdnVoteAlign.Value = dtSettingsInfo.Rows[0]["votealign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefontweight"].ToString() != "")
            {
                hdnMsgBold.Value = dtSettingsInfo.Rows[0]["messagefontweight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagestyle"].ToString() != "")
            {
                hdnMsgItalic.Value = dtSettingsInfo.Rows[0]["messagestyle"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messageunderline"].ToString() != "")
            {
                hdnMsgUL.Value = dtSettingsInfo.Rows[0]["messageunderline"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagealign"].ToString() != "")
            {
                hdnMsgAlign.Value = dtSettingsInfo.Rows[0]["messagealign"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["questionfontsize"].ToString() != "")
            {
                hdnQuestionFSize.Value = dtSettingsInfo.Rows[0]["questionfontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["answerfontsize"].ToString() != "")
            {
                hdnAnswerFSize.Value = dtSettingsInfo.Rows[0]["answerfontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["votefontsize"].ToString() != "")
            {
                hdnVoteFSize.Value = dtSettingsInfo.Rows[0]["votefontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["messagefontsize"].ToString() != "")
            {
                hdnMsgFSize.Value = dtSettingsInfo.Rows[0]["messagefontsize"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["iframewidth"].ToString() != "")
            {
                hdnWidth.Value = dtSettingsInfo.Rows[0]["iframewidth"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["iframeheight"].ToString() != "")
            {
                if (hdnSrcUrl.Value.Contains("CreatePoll.aspx"))
                {
                    if (Request.Cookies["iframeheight"] != null)
                    {
                        hdnHeight.Value = Request.Cookies["iframeheight"].Value;
                    }
                    else
                    {
                        hdnHeight.Value = dtSettingsInfo.Rows[0]["iframeheight"].ToString();
                    }
                }
                else
                {
                    hdnHeight.Value = dtSettingsInfo.Rows[0]["iframeheight"].ToString();
                }
            }
            if (dtSettingsInfo.Rows[0]["recmheight"].ToString() != "")
            {
                hdnRecHeight.Value = dtSettingsInfo.Rows[0]["recmheight"].ToString();
            }
            if (dtSettingsInfo.Rows[0]["recmwidth"].ToString() != "")
            {
                hdnRecWidth.Value = dtSettingsInfo.Rows[0]["recmwidth"].ToString();
            }
            if (pollInfo.Tables[0].Rows[0][9].ToString() == "private")
            {
                voteresnew.Style.Add("display", "block");
                voteres.Style.Add("display", "none");
            }
            else
            {
                voteresnew.Style.Add("display", "none");
                voteres.Style.Add("display", "block");
            }
            if (pollInfo.Tables[0].Rows[0][14].ToString() == "no")
            {
                isPoweredByShowing = false;
                poweredby.Style.Add("display", "none");
            }
            else
            {
                isPoweredByShowing = true;
                poweredby.Style.Add("display", "block");
                poweredby.Style.Add("color", "#" + hdnAnswerColor.Value);
            }
            if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "" && dtSettingsInfo.Rows[0]["sponsortext"].ToString() != "")
            {
                isSponsorLogoShowing = true;
                lblSponsor.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                lblSpoRes.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                sponsorpreviw.Style.Add("display", "block");
                sponsorres.Style.Add("display", "block");
                logo.Attributes.Add("src", foldername + @"\" + dtSettingsInfo.Rows[0]["sponsorlogo"].ToString());
                imgResultsLogo.Attributes.Add("src", foldername + @"\" + dtSettingsInfo.Rows[0]["sponsorlogo"].ToString());
                onlyvote.Style.Add("display", "none");
                voteandview.Style.Add("display", "block");
                onlyvoteres.Style.Add("display", "none");
                voteviewres.Style.Add("display", "block");
                if (dtSettingsInfo.Rows[0]["logolink"].ToString() != "")
                {
                    string sponsorlogolink = dtSettingsInfo.Rows[0]["logolink"].ToString();
                    if (sponsorlogolink.IndexOf("http") < 0)
                    {
                        sponsorlogolink = "http://" + sponsorlogolink;
                    }

                    logolink.Attributes.Add("href", sponsorlogolink);
                    logolinknew.Attributes.Add("href", sponsorlogolink);
                }
                else
                {
                    logolink.Attributes.Add("href", "#");
                    logolinknew.Attributes.Add("href", "#");
                }
            }
            else
            {
                isSponsorLogoShowing = false;
                sponsorpreviw.Style.Add("display", "none");
                sponsorres.Style.Add("display", "none");
            }
            hdnIsSponsorLogoShowing.Value = isSponsorLogoShowing.ToString().ToLower();
            btnVoteAn.Text = pollInfo.Tables[0].Rows[0]["votebuttontext"].ToString();
            LinkNew.Text = pollInfo.Tables[0].Rows[0]["votebuttontext"].ToString();
            
            btnsubmit.Text = pollInfo.Tables[0].Rows[0]["submitbuttontext"].ToString();
            
            if (pollInfo.Tables[0].Rows[0]["invitationmessage"].ToString() != "")
            {
                lblMessage.Text = pollInfo.Tables[0].Rows[0]["invitationmessage"].ToString();
                lblMessage.Visible = true;
                lblMessage.Style.Add("display", "block");
            }
            if (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isemailrequired"].ToString()) == 1)
            {
                divemail.Style.Add("display", "block");
                divemailtxt.Style.Add("display", "block");
            }
            else
            {
                divemail.Style.Add("display", "none");
                divemailtxt.Style.Add("display", "none");
            }
            if (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isphonerequired"].ToString()) == 1)
            {
                divphone.Style.Add("display", "block");
                divphonetxt.Style.Add("display", "block");
            }
            else 
            {
                divphone.Style.Add("display", "none");
                divphonetxt.Style.Add("display", "none");
            }
            if ((Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isemailrequired"].ToString()) == 1) || (Convert.ToInt32(pollInfo.Tables[0].Rows[0]["isphonerequired"].ToString()) == 1))
            {
                divsubmit.Visible = true;
                divsubmit.Style.Add("display", "block");
                btnsubmit.Visible = true;
                btnsubmit.Style.Add("display", "block");
            }
            
            if (pollInfo.Tables[0].Rows[0]["redirecturl"].ToString() != "")
            {
                string redurl = pollInfo.Tables[0].Rows[0]["redirecturl"].ToString();
                if (!redurl.Contains("http"))
                {
                    redurl = "http://" + pollInfo.Tables[0].Rows[0]["redirecturl"].ToString();
                }
                divsubmit.Visible = true;
                divsubmit.Style.Add("display", "block");
                btnsubmit.Visible = true;
                btnsubmit.Style.Add("display", "block");
                hdnHasRedirectUrl.Value = "1";
                hdnRedirectUrl.Value = redurl;
                
            }

            thankyouoverlaymessage.Text = pollInfo.Tables[0].Rows[0]["resulttext"].ToString();
            thankyouoverlay.InnerHtml = pollInfo.Tables[0].Rows[0]["resulttext"].ToString();
            if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "" && dtSettingsInfo.Rows[0]["sponsortext"].ToString() != "")
            {
                isSponsorLogoShowing = true;
                
                lblSpoRes.Text = dtSettingsInfo.Rows[0]["sponsortext"].ToString();
                logo.Attributes.Add("src", foldername + @"\" + dtSettingsInfo.Rows[0]["sponsorlogo"].ToString());
                if (dtSettingsInfo.Rows[0]["logolink"].ToString() != "")
                {
                    logolink.Attributes.Add("href", dtSettingsInfo.Rows[0]["logolink"].ToString());
                }
                else
                {
                    logolink.Attributes.Add("href", "#");
                }
            }
            hdnIsSponsorLogoShowing.Value = isSponsorLogoShowing.ToString().ToLower();

        }
        else
        {
            hdnAnswerFSize.Value = "12";
            hdnQuestionFSize.Value = "12";
            hdnVoteFSize.Value = "12";
            hdnMsgFSize.Value = "12";
            hdnQuestionBold.Value = "normal";
            hdnQuestionItalic.Value = "normal";
            hdnQuestionUL.Value = "none";
            hdnQuestionAlign.Value = "left";
            hdnAnswerBold.Value = "normal";
            hdnAnswerItalic.Value = "normal";
            hdnAnswerUL.Value = "none";
            hdnAnswerAlign.Value = "left"; ;
            hdnVoteBold.Value = "bold";
            hdnVoteItalic.Value = "normal";
            hdnVoteUL.Value = "none";
            hdnVoteAlign.Value = "center";
            hdnMsgAlign.Value = "left";
            hdnMsgBold.Value = "normal";
            hdnMsgItalic.Value = "normal";
            hdnMsgUL.Value = "none";
            hdnPollBgColor.Value = "ffffff";
            hdnAnswerColor.Value = "000000";
            hdnQuestBgColor.Value = "e7e7e7";
            hdnQuestColor.Value = "000000";
            hdnVoteBgColor.Value = "6C89E1";
            hdnVoteColor.Value = "ffffff";
        }
    }

    protected void WriteClicked()
    {
        HttpCookie acookie = new HttpCookie("PollUniqueRespondent");
        acookie.Values["PollId"] = pollId.ToString();
        acookie.Values["RespondentId"] = "Responded";
        Response.Cookies.Add(acookie);
    }

    protected void btnVote_Click(object sender, EventArgs e)
    {

        string jstring = "";
        string chartheightstring = "";
        string ipAddress = "";
        HttpRequest httpRequest = HttpContext.Current.Request;
        if (httpRequest.Browser.IsMobileDevice)
        {
            device = "mobile";
            mobileBrand = httpRequest.Browser.MobileDeviceManufacturer;
            mobilePlatform = httpRequest.Browser.Platform;
        }
        else
        {
            device = "desktop";
            platform = httpRequest.Browser.Platform;
        }

        if (Session["respondentId"].ToString() == "createvisit")
        {
            BindDesignStyles();

            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }

            var objClient = new System.Net.WebClient();
            var downloadedString = objClient.DownloadString("http://freegeoip.net/xml/" + ipAddress);
            XmlDocument xob = new XmlDocument();
            xob.LoadXml(downloadedString);

            try
            {

                if (xob != null)
                {
                    var x = xob.SelectSingleNode("Response");
                    if (x != null)
                    {
                        IpAddress = x["IP"].InnerText;
                        countryCode = x["CountryCode"].InnerText;
                        countryName = x["CountryName"].InnerText;
                        regionName = x["RegionName"].InnerText;
                        cityName = x["City"].InnerText;
                        zipCode = x["ZipCode"].InnerText;
                        latitude = x["Latitude"].InnerText;
                        longitude = x["Longitude"].InnerText;
                        timeZone = x["TimeZone"].InnerText;
                    }
                }
            }
            catch { }
            finally
            {
            }
            licInfo = Pc.getPollLicenceInfo(pollId);
            if (licInfo.Tables[1].Rows.Count > 0)
            {
                if (licInfo.Tables[1].Rows[0][0].ToString() == "Free" || Convert.ToDateTime(licInfo.Tables[1].Rows[0][1].ToString()) < DateTime.Now)
                {
                    if (Convert.ToInt32(licInfo.Tables[0].Rows[0][0].ToString()) > 500)
                    {
                        Response.Redirect("lastpage.aspx");
                    }
                }
                else if (licInfo.Tables[0].Rows[0][0].ToString() == "PremiumOne")
                {
                    if (Convert.ToInt32(licInfo.Tables[0].Rows[0][0].ToString()) > 5000)
                    {
                        Response.Redirect("lastpage.aspx");
                    }
                }
                else if (licInfo.Tables[0].Rows[0][0].ToString() == "PremiumTwo")
                {
                    if (Convert.ToInt32(licInfo.Tables[0].Rows[0][0].ToString()) > 10000)
                    {
                        Response.Redirect("lastpage.aspx");
                    }
                }
            }

            respondentId = Pc.InsertIntoRespondent(pollId, "visits", countryName, regionName, cityName, IpAddress, longitude, latitude, device, platform, mobileBrand, mobilePlatform, hdnSrcUrl.Value);
            hdnRespondentId.Value = Convert.ToString(respondentId);
            Session["respondentId"] = respondentId.ToString();
        }
        pollInfo = Pc.getPollInfo(pollId);
        dtSettingsInfo = Pc.getPollSettingsInfo(pollId);
        if (pollInfo.Tables[0].Rows[0][9].ToString() == "private")
        {
            resultaccess = "private";
            lblMessage.Text = pollInfo.Tables[0].Rows[0][10].ToString();
        }
        else
        {
            resultaccess = "public";
        }
        if (Convert.ToInt32(pollInfo.Tables[0].Rows[0][15].ToString()) == 1)
        {
            questionType = 1;
            if (radAnswerOptions.SelectedIndex == -1)
            {
                lblError.Text = "Please select an answer option";
                lblError.Attributes.Add("display", "none");
                return;
            }
            else
            {
                lblError.Text = "";
                lblError.Attributes.Add("display", "none");
            }
        }
        else
        {
            questionType = 2;
            if (ckbAnswerOptions.SelectedIndex == -1)
            {
                lblError.Text = "Please select answer option";
                lblError.Attributes.Add("display", "none");
                return;
            }
            else
            {
                lblError.Text = "";
                lblError.Attributes.Add("display", "none");
            }
        }
        respondentId = Convert.ToInt32(hdnRespondentId.Value);
        Location location = getIPDetails(ipAddress);
        if (location != null)
        {
            IpAddress = (ipAddress ?? "");
            countryCode = (location.countryCode ?? "");
            countryName = (location.countryName ?? "");
            regionName = (location.regionName ?? "");
            cityName = (location.city ?? "");
            zipCode = (location.postalCode ?? "");
            latitude = (location.latitude.ToString() ?? "");
            longitude = (location.longitude.ToString() ?? "");
            timeZone = "";
        }
        else
        {
            IpAddress = "";
            countryCode = "";
            countryName = "";
            regionName = "";
            cityName = "";
            zipCode = "";
            latitude = "";
            longitude = "";
            timeZone = "";

        }

        if (!hdnSrcUrl.Value.Contains("CreatePoll.aspx"))
        {
            respondentId = Pc.InsertIntoRespondent(pollId, "complete", countryName, regionName, cityName, IpAddress, longitude, latitude, device, platform, mobileBrand, mobilePlatform, hdnSrcUrl.Value);
            Pc.UpdateIntoRespondent(respondentId, "complete", txtComments.Text);
        }

        HttpCookie respCookie = new HttpCookie("respondentanswered" + pollId.ToString());
        Response.Cookies.Remove("respondent" + pollId.ToString());
        respCookie.Value = respondentId.ToString();
        respCookie.Expires = DateTime.Now.AddDays(10);
        respCookie.Domain = "127.0.0.1";
        respCookie.Path = "/";
        respCookie.HttpOnly = false;
        Response.Cookies.Add(respCookie);

        if (questionType == 2)
        {
            for (int i = 0; i < ckbAnswerOptions.Items.Count; i++)
            {
                if (ckbAnswerOptions.Items[i].Selected == true)
                {
                    answerId = Convert.ToInt32(ckbAnswerOptions.Items[i].Value);
                    Pc.InsertIntoResponse(respondentId, answerId);
                }
            }
        }
        else
        {
            answerId = Convert.ToInt32(radAnswerOptions.SelectedItem.Value);
            Pc.InsertIntoResponse(respondentId, answerId);
        }
        singleradbtns.Style.Add("display", "none");
        multipleckbbtns.Style.Add("display", "none");
        if (resultaccess == "public")
        {
            DataSet dspollInfo = Pc.getpollchartinfo(pollId);
            onlyvote.Style.Add("display", "none");
            voteandview.Style.Add("display", "none");
            //commentbox.Style.Add("display", "none");
            onlyvoteres.Style.Add("display", "none");
            voteviewres.Style.Add("display", "none");
            if (dtSettingsInfo.Rows[0]["googletext"].ToString() != "" || dtSettingsInfo.Rows[0]["twittertext"].ToString() != "" || dtSettingsInfo.Rows[0]["fbtext"].ToString() != "")
            {
                isSocialSharingShowing = true;
                social.Style.Add("display", "block");
                socialshares.Style.Add("display", "block");
            }
            else
            {
                isSocialSharingShowing = false;
                social.Style.Add("display", "none");
                socialshares.Style.Add("display", "none");
            }
            if (dtSettingsInfo.Rows[0]["sponsorlogo"].ToString() != "" && dtSettingsInfo.Rows[0]["sponsortext"].ToString() != "")
            {
                isSponsorLogoShowing = true;
            }
            else
            {
                isSponsorLogoShowing = false;
            }
            if (pollInfo.Tables[0].Rows[0][14].ToString() == "no")
            {
                isPoweredByShowing = false;
            }
            else
            {
                isPoweredByShowing = true;
            }
            if (Convert.ToBoolean(dtSettingsInfo.Rows[0]["countvotes"].ToString()) == true)
            {
                lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString() + " (" + dspollInfo.Tables[4].Rows[0][0].ToString() + " votes)";
            }
            else
            {
                lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
            }
            //btnVoteAn.Text = "Votes polled " + dspollInfo.Tables[3].Rows[0][3].ToString();
            HtmlTable qt3tableques = new HtmlTable();
            int hta;
            hta = 20 * dspollInfo.Tables[1].Rows.Count;
            qt3tableques.Height = hta.ToString();
            HtmlTableRow htrcharttemp = new HtmlTableRow();
            HtmlTableCell htccharttemp = new HtmlTableCell();
            System.Web.UI.WebControls.Label lblqt3cellpertotalcharttemp = new System.Web.UI.WebControls.Label();
            string jsstrchartname = "pollresponsechart" + pollId;
            StringBuilder strXML = new StringBuilder();
            string strchartname1 = System.Web.HttpUtility.HtmlEncode(StripTagsRegex(dspollInfo.Tables[0].Rows[0][1].ToString())).Replace("\r", "").Replace("\n", "");

            string strbold;

            if (dspollInfo.Tables[6].Rows[0][6].ToString() == "bold")
            {
                strbold = "1";
            }
            else
            {
                strbold = "0";
            }

            //string charttypename = "../Charts/Bar2D.swf";
            int iChartHeight = Convert.ToInt32(hdnHeight.Value);
            chartheightstring = "charttop = parseInt($('#headerclear').offset().top); chartbottom = 0;";
            if (isSocialSharingShowing)
            {
                chartheightstring += "chartbottom = parseInt($('#socialshares').offset().top);";
            }
            else if (isSponsorLogoShowing)
            {
                chartheightstring += "chartbottom = parseInt($('#sponsorpreviw').offset().top);";
            }
            else if (isPoweredByShowing)
            {
                chartheightstring += "chartbottom = parseInt($('#poweredby').offset().top);";
            }
            else {
                chartheightstring += "chartbottom = parseInt($('#fusionchartscontent').offset().top);";
            }
            //string chartwidth = hdnWidth.Value;
            //string chartheight = iChartHeight.ToString();
            //  baseFont='Arial' baseFontSize ='12'  baseFontColor ='000000'
            strXML.Append("<chart caption='' subcaption='' yaxisname='' maxLabelWidthPercent='30' borderAlpha='0'  chartTopMargin='0' chartBottomMargin='0' chartLeftMargin='5'  canvasbgAlpha='0' numDivLines='0' divLineAlpha='0' showYAxisValues='0' showXAxisValues='0' divLineColor='" + hdnPollBgColor.Value + "'  labelDisplay='' showvalues='1' xaxisname='' alternatevgridcolor='#" + hdnPollBgColor.Value + "'  tooltipbordercolor='" + dspollInfo.Tables[6].Rows[0][3].ToString() + "' tooltipbgcolor='" + dspollInfo.Tables[6].Rows[0][1].ToString() + "' useroundedges='1'  showborder='0' bgcolor='#" + hdnPollBgColor.Value + "' numbersuffix='%' palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1' baseFontColor='#" + dspollInfo.Tables[6].Rows[0][3].ToString() + "' bgAlpha='100'>");

            DataRow[] drchart = dspollInfo.Tables[2].Select("pollid = " + pollId);
            for (int ic = 0; ic < drchart.Length; ic++)
            {
                strXML.AppendFormat("<set label='' displayValue='{0}' value='0' alpha='0.1' labelFont='" + dspollInfo.Tables[6].Rows[0][4].ToString() + "' labelFontSize='" + dspollInfo.Tables[6].Rows[0][5].ToString() + "' labelFontColor='" + dspollInfo.Tables[6].Rows[0][3].ToString() + "' labelFontBold='" + strbold + "' labelFontItalic=''/>", System.Web.HttpUtility.HtmlEncode(StripTagsRegex(drchart[ic][1].ToString().Replace("\r", "").Replace("\n", ""))));
                //strXML.Append("<set label='' value=''/>");
                strXML.AppendFormat("<set label='          ' value='{0}' displayLabel='        '/>", drchart[ic][3].ToString());
                if (ic < (drchart.Length - 1))
                {
                    strXML.Append("<set label='' value=''/>");
                }
                //this line below is the working version
                //strXML.AppendFormat("<set label='{0}' value='{1}'/>", System.Web.HttpUtility.HtmlEncode(StripTagsRegex(drchart[ic][1].ToString().Replace("\r", "").Replace("\n", ""))), drchart[ic][3].ToString());
                //strXML.AppendFormat("<styles><definition><style name='myLabelsFont' type='font' font='' size='' color=''  bold='' underline='" + dspollInfo.Tables[6].Rows[0][7].ToString() + "'/>");
                //strXML.AppendFormat("</definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles>");
            }
            strXML.Append("</chart>");
            Int32.TryParse(pollInfo.Tables[1].Rows[0]["chartwidth"].ToString(), out chartwidth);
            Int32.TryParse(pollInfo.Tables[1].Rows[0]["chartheight"].ToString(), out chartheight);
            if (chartheight == 0)
            {
                chartheightstring += "chartheight = chartbottom - charttop - 4;console.log(chartheight);console.log(chartbottom);console.log(charttop);";
            }
            else {
                chartheightstring += "chartheight = " + chartheight.ToString() + ";";
            }
            if (chartwidth == 0)
            {
                chartheightstring += "chartwidth=" + hdnWidth.Value + ";";
            }
            else {
                chartheightstring += "chartwidth=" + chartwidth.ToString() + ";";
            }
            jstring = "<script type='text/javascript'>var baseFont='';var baseFontSize='';var baseFontBold='normal';var baseFontItalic='normal';var baseFontUnderline='normal';var chartheight=0; var chartwidth = 0;var charttop = 0;var chartbottom=0;";
            jstring += "baseFontBold=$('#hdnAnswerBold').val();baseFontItalic=$('#hdnAnswerItalic').val();baseFontUnderline=$('#hdnAnswerUL').val();baseFont = $('#hdnAnswerFont').val();baseFontSize = $('#hdnAnswerFSize').val();";
            jstring += "$(document).ready(function(){" + chartheightstring + "});$('#purejschart').show();FusionCharts.ready(function () {var pollChart = new FusionCharts({\"type\": \"bar2d\",\"renderAt\": \"purejschart\",\"width\": chartwidth, \"height\": chartheight,\"dataFormat\": \"xml\",\"dataSource\" :\"" + strXML.ToString() + "\"});";
            jstring += "pollChart.render(function(){";

            jstring += "$('.fusioncharts-label').css({'font-family':baseFont,'font-weight':baseFontBold,'font-style':baseFontItalic,'text-decoration':baseFontUnderline,'font-size':baseFontSize});";
            jstring += "});});</script>";
            fusionchartscontent.Text = jstring;
        }
        else
        {
            messagebox.Style.Add("display", "block");
            onlyvote.Style.Add("display", "none");
            voteandview.Style.Add("display", "none");
            //commentbox.Style.Add("display", "none");
            onlyvoteres.Style.Add("display", "none");
            voteviewres.Style.Add("display", "none");
            DataSet dspollInfo = Pc.getpollchartinfo(pollId);
            // lblVotes.Text = dspollInfo.Tables[3].Rows[0][3].ToString();
            //   lblVotes.Text = dspollInfo.Tables[4].Rows[0][0].ToString();
            if (dtSettingsInfo.Rows[0]["googletext"].ToString() != "" || dtSettingsInfo.Rows[0]["twittertext"].ToString() != "" || dtSettingsInfo.Rows[0]["fbtext"].ToString() != "")
            {
                isSocialSharingShowing = true;
                social.Style.Add("display", "block");
                socialshares.Style.Add("display", "block");
            }
            else
            {
                isSocialSharingShowing = false;
                social.Style.Add("display", "none");
                socialshares.Style.Add("display", "none");
            }
            if (Convert.ToBoolean(dtSettingsInfo.Rows[0]["countvotes"].ToString()) == true)
            {
                //votescount.Style.Add("display","block");              
                lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString() + " (Votes : " + dspollInfo.Tables[4].Rows[0][0].ToString() + ")";
            }
            else
            {
                lblQuestion.Text = pollInfo.Tables[0].Rows[0][16].ToString();
                //  votescount.Style.Add("display", "none");
            }
            voteres.Style.Add("display", "block");
            //voteres.Style.Add("height", "100px");
        }
        Session["respondentId"] = "createvisit";
    }

    public static string StripTagsRegex(string source)
    {
        return Regex.Replace(source, "<.*?>", string.Empty);
    }

    public Location getIPDetails(string IPAddress)
    {
        string filepath = ConfigurationManager.AppSettings["geolitepath"].ToString();
        //string filepath = "D://webapps//insighto//GeoLiteCity.dat";
        LookupService lookupservice = new LookupService(filepath);
        Location location = new Location();
        try
        {
            location = lookupservice.getLocation(IPAddress);
        }
        catch (Exception e1)
        {
            return null;
        }
        return location;
    }

}