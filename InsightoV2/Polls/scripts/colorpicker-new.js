﻿$(document).ready(function () {
    $("#MainContent_txtAnswerColor, #MainContent_txtPollBgColor, #MainContent_txtQuestionColor, #MainContent_txtQuestionBgColor, #MainContent_txtVoteColor, #MainContent_txtVoteBgColor, #MainContent_txtMessageBgColor, #MainContent_txtMessageColor").spectrum({
        color: "#f00",
        preferredFormat: "hex"
    });

    $("#MainContent_txtAnswerColor").spectrum("set", $("#MainContent_txtAnswerColor").val());
    $("#MainContent_txtPollBgColor").spectrum("set", $("#MainContent_txtPollBgColor").val());
    $("#MainContent_txtQuestionColor").spectrum("set", $("#MainContent_txtQuestionColor").val());
    $("#MainContent_txtQuestionBgColor").spectrum("set", $("#MainContent_txtQuestionBgColor").val());
    $("#MainContent_txtVoteColor").spectrum("set", $("#MainContent_txtVoteColor").val());
    $("#MainContent_txtVoteBgColor").spectrum("set", $("#MainContent_txtVoteBgColor").val());
    $("#MainContent_txtMessageColor").spectrum("set", $("#MainContent_txtMessageColor").val());
    $("#MainContent_txtMessageBgColor").spectrum("set", $("#MainContent_txtMessageBgColor").val());

    $("#MainContent_txtAnswerColor, #MainContent_txtPollBgColor, #MainContent_txtQuestionColor, #MainContent_txtQuestionBgColor, #MainContent_txtVoteColor, #MainContent_txtVoteBgColor, #MainContent_txtMessageBgColor, #MainContent_txtMessageColor").show();


    $("#MainContent_txtAnswerColor, #MainContent_txtPollBgColor, #MainContent_txtQuestionColor, #MainContent_txtQuestionBgColor, #MainContent_txtVoteColor, #MainContent_txtVoteBgColor, #MainContent_txtMessageBgColor, #MainContent_txtMessageColor").change(function (color) {
        var pollBg = $("#MainContent_txtPollBgColor").val();
        var pollAnswer = $("#MainContent_txtAnswerColor").val();
        var pollQuestionBg = $("#MainContent_txtQuestionBgColor").val();
        var pollQuestion = $("#MainContent_txtQuestionColor").val();
        var pollVote = $("#MainContent_txtVoteColor").val();
        var pollVoteBg = $("#MainContent_txtVoteBgColor").val();
        var pollMessageBg = $("#MainContent_txtMessageBgColor").val();
        var pollMessage = $("#MainContent_txtMessageColor").val();

        $("#colorvote").css("background-color", "" + pollVote);
        $(".viewandvoteimagepreview").css("color", "" + pollVote);

        $("#bgcolorvote").css("background-color", "" + pollVoteBg);
        $(".viewandvoteimagepreview").css("background-color", "" + pollVoteBg);

        $("#colorbgmessage").css("background-color", "" + pollMessageBg);
        $(".resultpreview").css("background-color", "" + pollMessageBg);

        $(".resultpreview").css("color", "" + pollMessage);
        $("#colormessage").css("background-color", "" + pollMessage);

        $("#colorquestion").css("background-color", "" + pollQuestion);
        $(".questionpreviewcss").css("color", "" + pollQuestion);

        $("#bgcolorquestion").css("background-color", "" + pollQuestionBg);
        $(".questionpreviewcss").css("background-color", "" + pollQuestionBg);

        $(".textpollthemepreview").css("background-color", "" + pollBg);
        $(".textpollthemepreviewresults").css("background-color", "" + pollBg);
        $("#colorbgpoll").css("background-color", "" + pollBg);

        $(".txtRow").css("color", pollAnswer);
        $("#coloranswer").css("background-color", "" + pollAnswer);
        $(".sponsorimagepreviewres1").css("color", "" + pollAnswer);
        $(".poweredby").css("color", "" + pollAnswer);
        $(".poweredby a").css("color", "" + pollAnswer);
        $(".poweredbyrep").css("color", "" + pollAnswer);
        $(".poweredbyrep a").css("color", "" + pollAnswer);
        $(".commentstxt").css("color", "" + pollAnswer);
        $('.sponsorimagepreview').css("color", "" + pollAnswer);
        $('.sponsorimagepreviewres').css("color", "" + pollAnswer);
        $('.socialnew').css("color", "" + pollAnswer);
        $('.socialnew1').css("color", "" + pollAnswer);
    });

    renderChart();

});

