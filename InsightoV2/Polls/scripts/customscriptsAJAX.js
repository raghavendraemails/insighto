﻿var singleansweroptionheight = 5;
var sponsorlogooptionheight = 60;
var commentboxheight = 60;
var showSponsor = false;
var hasvotebuttonlabelchanged = false;
var imagefound = false;
var currenttabname = "";
function load() {
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(
        function () {

            setScrollBars();
            setColorPickers();
            hideActivateButton();
            setColorPickerForWebLinkbg();
            
            $("#MainContent_btnOpenModalDialog").click(function () {
                $("#editpollname").css("height", "100px !important");
                $('.modal-trigger').leanModal();
                //$('#editpollname').openModal();
            });

            //THIS IS FOR A NEW AS WELL AS PARENT POLL ACTIVATION
            $("#MainContent_btnActivate,#MainContent_btnActivatePoll").click(function () {
                var allerrors = "";
                var height = 0;
                var width = 0;
                var recheight = 0;
                var recwidth = 0;

                if ($("#MainContent_hdnPollStatus").val() == "Closed") {
                    Materialize.toast("Closed polls cannot be launched", 4000);
                    return false;;
                }

                if ($("#MainContent_txtQuestion").val() == "") {
                    allerrors = "Please enter the question <br/>";
                }
                if ($("#MainContent_txtWidthPoll").val() == "") {
                    allerrors = "Please enter width <br/>";
                }
                else {
                }
                if ($("#MainContent_txtHeightPoll").val() == "") {
                    allerrors = "Please enter height <br/>";
                }
                else {
                    $("#errorid1").hide();
                }
                if (!isNaN(parseInt($("#MainContent_txtHeightPoll").val()))) {
                    height = parseInt($("#MainContent_txtHeightPoll").val());
                }
                if (!isNaN(parseInt($("#MainContent_txtWidthPoll").val()))) {
                    width = parseInt($("#MainContent_txtWidthPoll").val());
                }

                if (!isNaN(parseInt($("#MainContent_lblRecThemeWidth").html()))) {
                    recwidth = parseInt($("#MainContent_lblRecThemeWidth").html());
                }
                else {
                    recwidth = parseInt($("#MainContent_lblWidth").html());

                }

                if (!isNaN(parseInt($("#MainContent_lblRecThemeHeight").html()))) {
                    recheight = parseInt($("#MainContent_lblRecThemeHeight").html());
                }
                else {
                    recheight = parseInt($("#MainContent_lblHeight").html());

                }

                if (recheight > height) {
                    allerrors = "Height is lesser than recommended. <br/>";
                }

                if (recwidth > width) {
                    allerrors = "Width is lesser than recommended. <br/>";
                }


                if ($('#MainContent_hdnPollType').val() == "video") {
                    if ($("#MainContent_txtVideo").val() == "") {
                        allerrors = "Please enter video code <br/>";
                    }
                }
                else {
                    $("#errorid").hide();
                }
                if ($('#MainContent_hdnPollType').val() == "image") {
                    if ($('#MainContent_fileUploadName').val() == "") {
                        console.log("no image");
                        allerrors = "Please upload image";
                    }
                }
                else {
                }
                var ansno = $("#MainContent_hdnRepeaterTxts").val();
                ansno = parseInt(ansno) - 1;
                var i = parseInt(0);
                var count = 0;
                for (i = 0; i <= parseInt(ansno) ; i++) {
                    var id = "#txtAnswerOption_" + i;
                    if ($(id).val() == "" || $(id).val() == undefined) {

                    }
                    else {
                        var sameoptions = false;
                        var j = i + 1;
                        var jd = "#txtAnswerOption_" + j;
                        for (j = 0; j < parseInt(ansno) ; j++) {
                            if ($(id).val() == $(jd).val()) {
                                sameoptions = true;
                            }
                        }
                        count = parseInt(count) + 1;
                        if (sameoptions) {
                            allerrors = "Answer Options should not be same <br/>";
                        }
                    }
                }
                if (parseInt(count) < 2) {
                    allerrors = "Please add minimum answer options <br/>";
                }
                if (allerrors != "") {
                    Materialize.toast(allerrors, 8000);
                    return false;
                }
                if ($("#MainContent_hdnActivationFlag").val() == "0") {
                    ActivationAlert();
                    tabDisplay("launch");
                    return false;
                }
            });

            $('#MainContent_ddlAnswerFont').on('change', function () {
                var answerfont = $("#MainContent_ddlAnswerFont").val();
                console.log(answerfont);
                //$('td').css({ 'font-family': answerfont + ' !important' });
                //$('.wrapnew').css({ 'font-family': answerfont + ' !important' });
                $('.txtRow, #MainContent_invitationmessagepreview, #MainContent_lblsharethisleadgen').css({ 'font-family': answerfont + ' !important' });
                $('.commentstxt').css({ 'font-family': answerfont });
                $('.poweredby').css({ 'font-family': answerfont });
                $('.poweredbyrep').css({ 'font-family': answerfont });
                $('.sponsorimagepreview').css({ 'font-family': answerfont });
                $('.sponsorimagepreviewres1').css({ 'font-family': answerfont });
                $('.socialnew').css({ 'font-family': answerfont });
                $('.socialnew1').css({ 'font-family': answerfont });
                $('#mainanswerdiv2').css('font-family', answerfont + '');
                $("#MainContent_hdnAnswerFont").val(answerfont);
                var answerrowcount = parseInt($("#MainContent_hdnRepeaterTxts").val());

                $("#previewansweroptions").css('font-family', answerfont + ' !important');
                //for (var z = 0; z < answerrowcount; z++) {
                //    $("#lblradText_" + z.toString()).css({ 'font-family': answerfont + ' !important' });
                //   //console.log(  $("#lblradText_" + z.toString()).css('font-family'));
                //}
                renderChart();
            });

            $('#MainContent_ddlVoteFont').on("change", function () {
                var votefont = $("#MainContent_ddlVoteFont").val();
                $('#MainContent_customvote, #MainContent_votebutton,#MainContent_btnSubmitLeadGen').css({ 'font-family': votefont + '' });
            });

            $('#MainContent_ddlMsgFont').on("change", function () {
                var votefont = $("#MainContent_ddlMsgFont").val();
                $('.resultpreview').css({ 'font-family': votefont });
            });

            $('#MainContent_ddlQuestionFont').on("change", function () {
                var questionfont = $("#MainContent_ddlQuestionFont").val();
                $('.questioncss').css({ 'font-family': questionfont + '' });
                $('.wrapquestion').css({ 'font-family': questionfont + '' });
            });

            $(".btnquestionalign").click(function () {
                if ($('#questionalign').is(':hidden')) {
                    $("#questionalign").show();
                    return false;
                }
                else {
                    $("#questionalign").hide();
                    return false;
                }
            });

            $(".btnfontsize").click(function () {
                var position = $(".btnfontsize").position();
                if ($('#fontsize').is(':hidden')) {
                    $("#fontsize").show();
                    return false;
                }
                else {
                    $("#fontsize").hide();
                    return false;
                }
            });

            $(".btnaalignment,#MainContent_aleftinactive,#MainContent_aleftactive, #MainContent_arightactive, #MainContent_acenteractive, #btnansweralignment, #divansweralignment").on("click", function () {
                console.log("here 1");
                var position = $(".btnaalignment").position();
                console.log("here 11");

                if ($('#answeralign').is(':hidden')) {
                    $("#answeralign").show();
                    return false;
                }
                else {
                    $("#answeralign").hide();
                    return false;
                }
                console.log("here 111");

            });

            $(".btnvalignment").click(function () {
                var position = $(".btnvalignment").position();

                if ($('#votealign').is(':hidden')) {

                    $("#votealign").show();
                    return false;
                }
                else {
                    $("#votealign").hide();
                    return false;
                }
            });

            $(".btnmalignment").click(function () {
                var position = $(".btnmalignment").position();
                if ($('#msgalign').is(':hidden')) {
                    $("#msalign").css("top", position.top + 110);
                    $("#msalign").css("left", position.left);

                    $("#msgalign").show();
                    return false;
                }
                else {
                    $("#msgalign").hide();
                    return false;
                }
            });

            $(".fontsmall").click(function () {
                $("#MainContent_hdnQuestionFSize").val("12");
                $("#fontsize").hide();
                $('.questioncss').css({ 'font-size': '12px' });
            });

            $(".fontnormal").click(function () {
                $("#MainContent_hdnQuestionFSize").val("13");
                $("#fontsize").hide();
                $('.questioncss').css({ 'font-size': '13px' });
            });

            $(".btnafontsize").click(function () {
                if ($('#afontsize').is(':hidden')) {
                    $("#afontsize").show();
                    return false;
                }
                else {
                    $("#afontsize").hide();
                    return false;
                }
            });

            $(".afontsmall").click(function () {
                $("#MainContent_hdnAnswerFSize").val("12");
                $("#afontsize").hide();
                $('#mainanswerdiv2').css({ 'font-size': '12px' });
                $('.commentstxt').css({ 'font-size': '12px' });
                $('.poweredby').css({ 'font-size': '12px' });
                $('.poweredbyrep').css({ 'font-size': '12px' });
                $('.sponsorimagepreview').css({ 'font-size': '12px' });
                $('.sponsorimagepreviewres').css({ 'font-size': '12px' });
                $('.sponsorimagepreviewres1').css({ 'font-size': '12px' });
                $('.socialnew').css({ 'font-size': '12px' });
                $('.socialnew1').css({ 'font-size': '12px' });
                renderChart();
            });

            $(".afontnormal").click(function () {
                $("#MainContent_hdnAnswerFSize").val("13");
                $("#afontsize").hide();
                $('#mainanswerdiv2').css({ 'font-size': '13px' });
                $('.commentstxt').css({ 'font-size': '13px' });
                $('.poweredbyrep').css({ 'font-size': '13px' });
                $('.poweredby').css({ 'font-size': '13px' });
                $('.sponsorimagepreview').css({ 'font-size': '13px' });
                $('.sponsorimagepreviewres').css({ 'font-size': '13px' });
                $('.sponsorimagepreviewres1').css({ 'font-size': '13px' });
                $('.socialnew').css({ 'font-size': '13px' });
                $('.socialnew1').css({ 'font-size': '13px' });
                renderChart();
            });

            $(".btnvfontsize").click(function () {
                if ($('#vfontsize').is(':hidden')) {
                    $("#vfontsize").show();
                    return false;
                }
                else {
                    $("#vfontsize").hide();
                    return false;
                }
            });

            $(".vfontsmall").click(function () {
                $("#MainContent_hdnVoteFSize").val("13");
                $("#vfontsize").hide();
                $('.vote').css({ 'font-size': '13px' });
                $('.viewandvoteimagepreview').css({ 'font-size': '13px' });
                if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                    $('.view').css({ 'font-size': '13px' });
                }
                else {
                    $('.view').css({ 'font-size': '10px' });
                }
            });

            $(".vfontnormal").click(function () {
                $("#MainContent_hdnVoteFSize").val("14");
                $("#vfontsize").hide();
                $('.viewandvoteimagepreview').css({ 'font-size': '14px' });
                $('.vote').css({ 'font-size': '14px' });
                if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                    $('.view').css({ 'font-size': '14px' });
                }
                else {
                    $('.view').css({ 'font-size': '10px' });
                }
            });

            $(".btnmfontsize").click(function () {
                if ($('#mfontsize').is(':hidden')) {
                    $("#mfontsize").show();
                    return false;
                }
                else {
                    $("#mfontsize").hide();
                    return false;
                }
            });

            $(".mfontsmall").click(function () {
                $("#MainContent_hdnMsgFSize").val("12");
                $("#mfontsize").hide();
                $('.resultpreview').css({ 'font-size': '12px' });
            });

            $(".mfontnormal").click(function () {
                console.log($(this).html());
                var parentid = $(this).parent().prop("id");
                if (parentid == "questiontext") {
                    $("#MainContent_lblQuestion").css("font-size", $(this).html() + "px");
                    $("#MainContent_lblQuestionResults").css("font-size", $(this).html() + "px");
                    $("#questionfontsizediv").html($(this).html());
                    $("#MainContent_hdnQuestionFSize").val($(this).html());
                }
                else if (parentid == "votetext") {
                    $("#MainContent_customvote, #MainContent_votebutton, #MainContent_btnSubmitLeadGen").css("font-size", $(this).html() + "px");
                    $("#votefontsizediv").html($(this).html());
                    $("#MainContent_hdnVoteFSize").val($(this).html());
                }
                else if (parentid == "answertext") {
                    $(".txtRow,#MainContent_invitationmessagepreview").css("font-size", $(this).html() + "px");
                    $("#answerfontsizediv").html($(this).html());
                    $("#MainContent_hdnAnswerFSize").val($(this).html());
                }
                $("#" + parentid).parent().hide();
                $("#MainContent_hdnMsgFSize").val("13");
                $("#mfontsize").hide();
                $('.resultpreview').css({ 'font-size': '13px' });
                renderChart();
            });

            $("#MainContent_txtSponsorText").on('input', function () {
                var title = $(this).val();
                $("#MainContent_lblSponsor").text(title);
                $("#MainContent_lblSponText").text(title);
                return false;
            });

            $("#MainContent_txtSponsorLink").on('input', function () {
                var title = $(this).val();
                if (title.indexOf("http") < 0) {
                    title = "http://" + title;
                    $(this).val(title);
                }
                $("#MainContent_logolink").prop("href", title)
                $("#MainContent_logolinkres").prop("href", title)
                
                return false;
            });

            $("#MainContent_txtSharePoll").on('input', function () {
                var share = $('#MainContent_txtSharePoll').val();
                var no = $('#MainContent_txtSharePoll').val().length;
                if (parseInt(no) > 25) {
                    Materialize.toast('Share text cannot be more than 15 characters in length', 4000);
                    share = $("#MainContent_lblShare").text();
                    $('#MainContent_txtSharePoll').val(share);
                    return false;
                }
                else {
                    $("#MainContent_lblShare").text(share);
                }
            });

            pollResultsOption($("#MainContent_hdnPollResultsOption").val());

            Materialize.updateTextFields();
            showSubmitButtonOptions();
            showContactOptions();
            showInvitationMessage();
            showFbtxt();
            showSocialMediaDiv();
            showGoogletxt();
            showTwittertxt();
            showThankYouMessages();

            $("#mCSB_1_container").css("margin-left", "8px !important");
            $("#mCSB_1_container").css("padding-left", "8px !important");

            $("#mCSB_7_container").css("margin-left", "8px !important");
            $("#mCSB_7_container").css("padding-left", "8px !important");

            $('#MainContent_txtSharePoll, #MainContent_txtSponsorText').characterCounter();

            calculateIFrameHeight();
            $("#questionfontsizediv").html($("#MainContent_hdnQuestionFSize").val());
            $("#answerfontsizediv").html($("#MainContent_hdnAnswerFSize").val());
            $("#votefontsizediv").html($("#MainContent_hdnVoteFSize").val());
            if (currenttabname == "create") {
                $("#MainContent_txtVoteText").on('input', function () {
                    var no = $('#MainContent_txtVoteText').val().length;
                    if (parseInt(no) > 30) {
                        $("#MainContent_lblRecHeightbPre").text(heightrec);
                    }
                    else {
                        var designerName = $('#MainContent_txtVoteText').val();
                        var nnn = designerName.substr(0, designerName.indexOf(' '));
                        var nnnsdf = designerName.substr(designerName.indexOf(' ') + 1);
                        var neww = designerName.split(" ")[0];
                        if (nnn == "") {
                            nnn = designerName;
                            if (nnnsdf == "") {
                            }
                            else {
                                nnnsdf = "";
                            }
                        }
                        $("#MainContent_customvote").text(nnn);
                        var newr = designerName.split(" ")[1];
                        $("#MainContent_customview").text(nnnsdf);
                    }

                });

                $("#MainContent_txtQuestion").on('input', function () {
                    var title = $(this).val();
                    if ($("#MainContent_ckbVotes").is(":checked")) {
                        $("#MainContent_lblQuestion").text(title);
                        $("#MainContent_lblQuestionResults").text(title + ' ' + '(39 votes)');
                    }
                    else {
                        $("#MainContent_lblQuestion").text(title);
                        $("#MainContent_lblQuestionResults").text(title);
                    }
                    $("#MainContent_txtQuestion").css("border-color", "#CDCDCD");
                    return false;
                });

                $(".txtAns").on("input", function () {
                    var id = this.id;
                    var suffix = id.match(/\d+/);
                    id = parseInt(suffix);
                    var title = $(this).val();
                    var labelid = "lblradText_" + id;

                    document.getElementById(labelid).innerHTML = stripSpecialChars(title);
                    $(".txtAns").css("border-color", "#cdcdcd");
                    renderChart();
                });

                $("#MainContent_txtInvitationMessage").on("input", function () {
                    var invitemessage = $("#MainContent_txtInvitationMessage").val();
                    if (invitemessage.length > 0) {
                        $("#MainContent_invitationmessagepreview").show();
                        $("#MainContent_invitationmessagepreview").html(invitemessage);
                    }
                    else {
                        $("#MainContent_invitationmessagepreview").hide();

                    }
                    //console.log("INVITE MESSAGE HEIGHT---" + invitemessage);
                    console.log("INVITE MESSAGE HEIGHT---" + $("#MainContent_invitationmessagepreview").css("line-height"));
                    var sp = invitemessage.split("/r/n");
                    console.log("IINE BREAKS - " + sp.length);
                    calculateIFrameHeight();

                });

                $("#MainContent_txtVoteButtonLabel").on("input", function () {
                    var votebtnlabel = $("#MainContent_txtVoteButtonLabel").val();
                    if (votebtnlabel.length > 0) {
                    }
                    else {
                        votebtnlabel = "Vote";
                    }
                    $("#MainContent_customvote").html(votebtnlabel);
                    $("#MainContent_votebutton").html(votebtnlabel);
                    hasvotebuttonlabelchanged = true;
                });

                $("#MainContent_txtSubmitButtonLabel").on("input", function () {
                    var submitbtnlabel = $("#MainContent_txtSubmitButtonLabel").val();
                    if (submitbtnlabel.length > 0) {
                    }
                    else {
                        submitbtnlabel = "Submit";
                    }
                    $("#MainContent_btnSubmitLeadGen").attr("value", submitbtnlabel);
                });

                $("#MainContent_txtResultText").on("input", function () {
                    var thankyoumsg = $("#MainContent_txtResultText").val();
                    if (thankyoumsg.length > 0) {
                    }
                    else {
                        thankyoumsg = "Thank you for your vote!";
                    }
                    $("#thankyouoverlay").html(thankyoumsg);
                });

                $("#MainContent_radSingleAnswer").click(function () {
                    $(".multipleanswers").hide();
                    $(".singleanswers").show();
                    setMultiThankYouMessagesOption();

                });

                $("#MainContent_radMultipleAnswer").click(function () {
                    $(".multipleanswers").show();
                    $(".singleanswers").hide();
                    setMultiThankYouMessagesOption();
                });

                $('#MainContent_txtInvitationMessage, #MainContent_txtResultText, #MainContent_txtSubmitButtonLabel, #MainContent_thankyoumessage, #MainContent_txtRedirectURL, .txtAns, #MainContent_txtQuestion, #MainContent_txtVoteButtonLabel').characterCounter();
            }

            $("#radButton_0").prop("checked", "checked");
            showThankYouOption("0");

        });

    }
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

$(window).load(function () {

    $("#placeholderinvitationmessage").css("display", "block");
    $("#placeholdersubmitbuttonlabel").css("display", "block");
    $("#placeholderthankyouoverlay").css("display", "block");
    $("#placeholderredirecturl").css("display", "block");
    $("#placeholderinvitationmessage").attr("for", "MainContent_txtInvitationMessage");
    $("#placeholdersubmitbuttonlabel").attr("for", "MainContent_txtSubmitButtonLabel");
    $("#placeholderredirecturl").attr("for", "MainContent_txtRedirectURL");
    $("#placeholderthankyouoverlay").attr("for", "MainContent_thankyoumessage");

    $("#MainContent_txtVideo").on('input', function () {
        console.log("adding video");
        if ($('#MainContent_imgPreview').is(':hidden')) {
            var heightvideo = $(".video-container").height();
        }
        else {
            var heightvideo = $("#MainContent_imgPreview").height();
        }
        var title = $(this).val();
        var framecodes = title.split("<iframe");
        //console.log(framecodes.length);
        //console.log(title);
        var framescount = framecodes.length;
        if (framescount > 1) {
            title = "<iframe" + framecodes[1];
        }
        //console.log(title);
        $(this).val(title);
        $("#MainContent_txtVideo").css("border-color", "#CDCDCD");
        if (title == "") {
            console.log(title);
            $(".video-container").html(title);
            $('#MainContent_imgPreview').attr('src', 'App_Themes/images/video.jpg');
            $('#MainContent_imgPreviewResults').attr('src', 'App_Themes/images/video.jpg');
            var heightvideo = $("#MainContent_imgPreview").height();
            var width = $(".textpollthemepreview").width();
            var valueHeight = Math.round((width / 16) * 9);
            $('#MainContent_imgPreview').attr('height', valueHeight);
            $('#MainContent_imgPreview').attr('width', width);
            $('#MainContent_imgPreviewResults').attr('height', valueHeight);
            $('#MainContent_imgPreviewResults').attr('width', width);
            $('#MainContent_imagepreview').show();
            $("#MainContent_videoResults").hide();
            $('#MainContent_imgResults').show();
            calculateIFrameHeight();
        }
        else {
            if (title.indexOf("<iframe") >= 0) {
                console.log(title);
                $(".video-container").html("");
                $(".video-container").html(title);
                //$(".responsive-video").html("");
                //$(".responsive-video").html(title);
                //var width = $(".textpollthemepreview").width();
                //var valueHeight = Math.round((width / 16) * 9);
                ////$(".video-container iframe").css('height', valueHeight + 'px');
                ////$(".video-container iframe").css('width', width + 'px');
                ////$(".video-container").css('width', width + 'px');
                //var heightrecad = valueHeight - heightvideo;
                //var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
                //$("#MainContent_lblRecThemeHeight").text(heightrec);
                $('#MainContent_imagepreview').hide();
                $('#MainContent_imgResults').hide();
                $("#MainContent_videoResults").show();
                $("#MainContent_videopreviewcreate").show();
                calculateIFrameHeight();
            }
            else {
                $("#MainContent_txtVideo").val("");
                Materialize.toast('Please copy and paste the embed of your YouTube video', 4000);
                calculateIFrameHeight();
                return false;
            }
        }
        return true;
    });

    $("#leftactive").hide();
    $("#centeractive").hide();
    $("#rightactive").hide();
    $("#answerleftactive").hide();
    $("#answercenteractive").hide();
    $("#answerrightactive").hide();
    $("#voteleftactive").hide();
    $("#votecenteractive").hide();
    $("#voterightactive").hide();
    $("#msgleftactive").hide();
    $("#msgcenteractive").hide();
    $("#msgrightactive").hide();
    $("#questionalign").hide();
    $("#answeralign").hide();
    $("#votealign").hide();
    $("#msgalign").hide();
    $("#fontsize").hide();
    $("#afontsize").hide();
    $("#vfontsize").hide();
    $("#mfontsize").hide();

    var fixheight = $("#MainContent_txtHeightPoll").val();
    var heightrec = $("#MainContent_hdnRecHeightne").val();
    var fixwidth = $("#MainContent_txtWidthPoll").val();
    $("#pollpreviewmaindiv").css("min-width", fixwidth + 'px');
    $("#pollresultspreviewmaindiv").css("min-width", fixwidth + 'px');
    $("#pollpreviewheaderdiv").css("min-width", fixwidth + 'px');
    $("#pollpreviewresultsheaderdiv").css("min-width", fixwidth + 'px');

    $("#pollpreviewmaindiv").css("width", fixwidth + 'px');
    $("#pollresultspreviewmaindiv").css("width", fixwidth + 'px');
    $("#pollpreviewheaderdiv").css("width", fixwidth + 'px');
    $("#pollpreviewresultsheaderdiv").css("width", fixwidth + 'px');

    $("#pollpreviewmaindiv").css("max-width", fixwidth + 'px');
    $("#pollresultspreviewmaindiv").css("max-width", fixwidth + 'px');
    $("#pollpreviewheaderdiv").css("max-width", fixwidth + 'px');
    $("#pollpreviewresultsheaderdiv").css("max-width", fixwidth + 'px');

    $("#pollleadgenpreviewmaindiv").css("height", fixheight + 'px');
    
    calculateIFrameHeight();
    $(".textpollthemepreviewresults").css("min-height", fixheight + 'px');
    $(".textpollthemepreview").css("min-height", fixheight + 'px');
    $(".textpollthemepreviewresults").css("max-height", fixheight + 'px');
    $(".textpollthemepreview").css("max-height", fixheight + 'px');
    $(".textpollthemepreviewresults").css("height", fixheight + 'px');
    $(".textpollthemepreview").css("height", fixheight + 'px');
    var ansheight = parseInt(fixheight) - 125;
    if ($('#MainContent_poweredby').is(':hidden')) {
        $(".viewtest").css("bottom", '30px');
        ansheight = parseInt(ansheight) + 25;
    }
    else {
        $(".viewtest").css("bottom", '30px');
    }
    showSponsorDetails();
    showResultTextbox();

    if ($('#MainContent_videopreviewcreate').is(':hidden')) {
        if ($('#MainContent_imagepreview').is(':hidden')) {

        }
        else {
            var imgheight = $('#MainContent_imagepreview').height();
            if (imgheight == 0) {
                imgheight = $('#MainContent_hdnImageHeight').val();
            }
            ansheight = parseInt(ansheight) - parseInt(imgheight);

        }
    }
    else {
        var width = $("#MainContent_txtWidthPoll").val();
        var valueHeight = Math.round((width / 16) * 9);
        ansheight = parseInt(ansheight) - parseInt(valueHeight);
    }
    $("#MainContent_hdnansHeight").val(ansheight);
    $("#mainanswerdiv").css("height", ansheight + 'px');
    var count = 0;
    if ($("#MainContent_ckbVotes").is(":checked")) {
        $("#MainContent_votescount").show();
    }
    else {
        $("#MainContent_votescount").hide();
    }
    if ($("#MainContent_ckbTwitter").is(":checked")) {
        $("#MainContent_twitter").show();
        $("#MainContent_twittericon").show();
        $("#MainContent_twittericonvote").show();
        $("#MainContent_social").show();
        $("#MainContent_socialsha").show();
        count++;
    }
    else {
        $("#MainContent_twitter").hide();
        $("#MainContent_twittericon").hide();
        $("#MainContent_twittericonvote").hide();
    }
    if ($("#MainContent_ckbGoogle").is(":checked")) {
        $("#MainContent_google").show();
        $("#MainContent_googleicon").show();
        $("#MainContent_social").show();
        $("#MainContent_socialsha").show();
        $("#MainContent_googleiconvote").show();
        count++;
    }
    else {
        $("#MainContent_google").hide();
        $("#MainContent_googleicon").hide();
        $("#MainContent_googleiconvote").hide();
    }
    if ($("#MainContent_ckbFb").is(":checked")) {
        $("#MainContent_fb").show();
        $("#MainContent_fbicon").show();
        $("#MainContent_fbiconvote").show();
        $("#MainContent_social").show();
        $("#MainContent_socialsha").show();
        count++;
    }
    else {
        $("#MainContent_fb").hide();
        $("#MainContent_fbicon").hide();
        $("#MainContent_fbiconvote").hide();
    }

    var imagenew = $("#MainContent_hdnImageLink").val();
    if (imagenew == "") {

    }
    else {
        $(".imagetrans").css("color", 'transparent');
    }

    var imagelo = $("#MainContent_hdnLogoLink").val();
    if (imagelo == "") {

    }
    else {
        $(".imagetranslogo").css("color", 'transparent');
    }

    var widthpoll = $("#MainContent_txtWidthPoll").val();

    var imgrecheight = $("#MainContent_hdnImageRecHeight").val();
    if (imgrecheight != "") {
        $("#MainContent_lblImageHeight").text(imgrecheight);
    }
    if (widthpoll > 380) {
        $(".Previewres").css("float", 'left');
        $(".Previewres").css("padding-top", '10px');
        $(".createpollnew").css("float", 'left');
        if ($('#MainContent_launch').is(':hidden')) {
        }
        else {

        }
    }
    else {
        $(".Previewres").css("padding-left", '');
        $(".Previewres").css("padding-top", '');
    }

    var comwidth = widthpoll - 30;
    $(".txtcomments").css("width", comwidth + 'px');
    var answidth = widthpoll - 50;
    $(".wrapnew").css("width", answidth + 'px');
    var questiontext = $("#MainContent_txtQuestion").val();
    $(".textpollthemepreview").css("min-width", widthpoll + 'px');
    $(".textpollthemepreview").css("min-width", widthpoll + 'px');
    $(".Preview").css("width", widthpoll + 'px');
    $(".Previewres").css("width", widthpoll + 'px');
    $(".textpollthemepreviewresults").css("min-width", widthpoll + 'px');
    $(".textpollthemepreviewresults").css("min-width", widthpoll + 'px');
    $(".video-container").css("width", widthpoll + 'px');

    heightrec = $("#MainContent_hdnRecHeightne").val();

    $("#MainContent_lblRecThemeHeight").text(heightrec);

    if ($("#MainContent_ckbResultsPrivate").is(":checked")) {
        $("#MainContent_ResultBox").show();
        $("#MainContent_results").hide();
        $("#MainContent_resulttextpreview").show();
        var height = $(".textpollthemepreview").height();
        $(".textpollthemepreviewresults").css("height", height + 'px');
        $(".view").css("display", 'none');
        $(".vote").css("text-align", 'center');
        $(".vote").css("width", '100%');
        $(".viewandvoteimagepreview").css("height", '16px');
    }
    else {

    }

    if ($("#MainContent_radMultipleAnswer").is(":checked")) {
        $(".multipleanswers").show();
        $(".singleanswers").hide();
    }

    if ($("#MainContent_chkComments").is(":checked")) {
        $("#MainContent_commentbox").show();
    }
    else {
        $("#MainContent_commentbox").hide();
    }

    var pollBg = $("#MainContent_txtPollBgColor").val();
    var pollQuestion = $("#MainContent_txtQuestionColor").val();
    var pollQuestionBg = $("#MainContent_txtQuestionBgColor").val();
    var pollAnswer = $("#MainContent_txtAnswerColor").val();
    var pollVote = $("#MainContent_txtVoteColor").val();
    var pollVoteBg = $("#MainContent_txtVoteBgColor").val();
    var pollMessageBg = $("#MainContent_txtMessageBgColor").val();
    var pollMessage = $("#MainContent_txtMessageColor").val();
    var questionfont = $("#MainContent_ddlQuestionFont").val();
    var answerfont = $("#MainContent_ddlAnswerFont").val();
    var votefont = $("#MainContent_ddlVoteFont").val();
    var msgfont = $("#MainContent_ddlMsgFont").val();
    $(".poweredby").css("color", "" + pollAnswer);
    $(".poweredby a").css("color", "" + pollAnswer);
    $(".poweredbyrep").css("color", "" + pollAnswer);
    $(".poweredbyrep a").css("color", "" + pollAnswer);
    $(".resultpreview").css("color", "" + pollMessage);
    $(".resultpreview").css("background-color", "" + pollMessageBg);
    $('.questioncss').css({ 'font-family': questionfont });
    $('#mainanswerdiv2').css({ 'font-family': answerfont });
    $('.commentstxt').css({ 'font-family': answerfont });
    $('.poweredby').css({ 'font-family': answerfont });
    $('.poweredbyrep').css({ 'font-family': answerfont });
    $('.sponsorimagepreview, #MainContent_invitationmessagepreview, #MainContent_lblsharethisleadgen').css({ 'font-family': answerfont });
    $('.sponsorimagepreviewres').css({ 'font-family': answerfont });
    $('.sponsorimagepreviewres1').css({ 'font-family': answerfont });
    $('.socialnew').css({ 'font-family': answerfont });
    $('.socialnew1').css({ 'font-family': answerfont });
    $('.viewandvoteimagepreview').css({ 'font-family': votefont });
    $(".textpollthemepreview").css("background-color", "" + pollBg);
    $(".textpollthemepreviewresults").css("background-color", "" + pollBg);
    $("#colorbgpoll").css("background-color", "" + pollBg);
    $("#bgcolorquestion").css("background-color", "" + pollQuestionBg);
    $("#colorquestion").css("background-color", "" + pollQuestion);
    $(".questioncss").css("background-color", "" + pollQuestionBg);
    $(".questioncss").css("color", "" + pollQuestion);
    $("#coloranswer").css("background-color", "" + pollAnswer);
    $("#mainanswerdiv2").css("color", "" + pollAnswer);
    $('.commentstxt').css("color", "" + pollAnswer);
    $('.poweredby').css("color", "" + pollAnswer);
    $('.poweredbyrep').css("color", "" + pollAnswer);
    $('.sponsorimagepreview, #MainContent_invitationmessagepreview, #MainContent_lblsharethisleadgen, #placeholdercomments').css("color", "" + pollAnswer);
    $('.sponsorimagepreviewres').css("color", "" + pollAnswer);
    $('.sponsorimagepreviewres1').css("color", "" + pollAnswer);
    $('.socialnew').css("color", "" + pollAnswer);
    $('.socialnew1').css("color", "" + pollAnswer);
    $("#bgcolorvote").css("background-color", "" + pollVoteBg);
    $("#colorvote").css("background-color", "" + pollVote);
    $("#colorbgmessage").css("background-color", "" + pollMessageBg);
    $("#colormessage").css("background-color", "" + pollMessage);
    $(".viewandvoteimagepreview,#MainContent_btnSubmitLeadGen").css("background-color", "" + pollVoteBg);
    $(".viewandvoteimagepreview,#MainContent_btnSubmitLeadGen").css("color", "" + pollVote);

    var questweight = $("#MainContent_hdnQuestionBold").val();
    var answerweight = $("#MainContent_hdnAnswerBold").val();
    var voteweight = $("#MainContent_hdnVoteBold").val();
    var msgweight = $("#MainContent_hdnMsgBold").val();
    var queststyle = $("#MainContent_hdnQuestionItalic").val();
    var answerstyle = $("#MainContent_hdnAnswerItalic").val();
    var votestyle = $("#MainContent_hdnVoteItalic").val();
    var msgstyle = $("#MainContent_hdnMsgItalic").val();
    var questul = $("#MainContent_hdnQuestionUL").val();
    var answerul = $("#MainContent_hdnAnswerUL").val();
    var voteul = $("#MainContent_hdnVoteUL").val();
    var msgul = $("#MainContent_hdnMsgUL").val();
    var questalign = $("#MainContent_hdnQuestionAlign").val();
    var answeralign = $("#MainContent_hdnAnswerAlign").val();
    var votealign = $("#MainContent_hdnVoteAlign").val();
    var msgalign = $("#MainContent_hdnMsgAlign").val();
    var questsize = $("#MainContent_hdnQuestionFSize").val();
    var answersize = $("#MainContent_hdnAnswerFSize").val();
    var votesize = $("#MainContent_hdnVoteFSize").val();
    var msgsize = $("#MainContent_hdnMsgFSize").val();
    $("#questionfontsizediv").html(questsize);
    $("#votefontsizediv").html(votesize);
    $("#answerfontsizediv").html(answersize);
                        
    $('.resultpreview').css({ 'font-weight': msgweight });
    $('.resultpreview').css({ 'font-style': msgstyle });
    $('.resultpreview').css({ 'text-decoration': msgul });
    $('.resultpreview').css({ 'text-align': msgalign });
    $('.resultpreview').css({ 'font-size': msgsize + 'px' });
    $('.resultpreview').css({ 'font-family': msgfont });
    $('.questioncss').css({ 'font-weight': questweight });
    $('#mainanswerdiv2, #MainContent_invitationmessagepreview, #MainContent_lblsharethisleadgen').css({ 'font-weight': answerweight });
    $('.viewandvoteimagepreview').css({ 'font-weight': voteweight });
    $('.questioncss').css({ 'font-style': queststyle });
    $('#mainanswerdiv2, #MainContent_invitationmessagepreview, #MainContent_lblsharethisleadgen').css({ 'font-style': answerstyle });
    $('.viewandvoteimagepreview').css({ 'font-style': votestyle });
    $('.questioncss').css({ 'text-decoration': questul });
    $('#mainanswerdiv2, #MainContent_invitationmessagepreview, #MainContent_lblsharethisleadgen').css({ 'text-decoration': answerul });
    $('.viewandvoteimagepreview').css({ 'text-decoration': voteul });
    $('.questioncss').css({ 'text-align': questalign });
    $('#mainanswerdiv2').css({ 'text-align': answeralign });
    $('.viewandvoteimagepreview').css({ 'text-align': votealign });
    console.log(questsize);

    $('.questioncss').css('font-size', questsize + 'px');
    $('#mainanswerdiv2, #MainContent_invitationmessagepreview').css({ 'font-size': answersize + 'px' });
    $('.socialnew').css({ 'font-size': answersize + 'px' });
    $('.socialnew1').css({ 'font-size': answersize + 'px' });
    $('.txtRow, #MainContent_invitationmessagepreview').css({ 'font-size': answersize + 'px' });
    $('#purejschart').css({ 'font-size': answersize + 'px' });
    $('.sponsorimagepreview').css({ 'font-size': '14px' });
    $('#MainContent_votebutton, #MainContent_customvote, #MainContent_btnSubmitLeadGen').css({ 'font-size': votesize + 'px' });
    $('.vote').css({ 'font-size': votesize + 'px' });
    if ($('#MainContent_sponsorpreviw').is(':hidden')) {
        $('.view').css({ 'font-size': votesize + 'px' });
    }
    else {
        $('.view').css({ 'font-size': '10px' });
    }
    var ans0 = $("#txtAnswerOption_0").val();
    var ans1 = $("#txtAnswerOption_1").val();
    var ans2 = $("#txtAnswerOption_2").val();
    var ans3 = $("#txtAnswerOption_3").val();
    var ans4 = $("#txtAnswerOption_4").val();
    var ans5 = $("#txtAnswerOption_5").val();
    var ans6 = $("#txtAnswerOption_6").val();
    var ans7 = $("#txtAnswerOption_7").val();
    var ans8 = $("#txtAnswerOption_8").val();
    var ans9 = $("#txtAnswerOption_9").val();
    var videolink = $("#MainContent_txtVideo").val();
    if (videolink != null && videolink != "") {
        $(".video-container").html(videolink);
        $("#MainContent_videopreviewcreate").show();
        $("#MainContent_videoResults").show();
        $('#MainContent_imagepreview').hide();
        $('#MainContent_imgResults').hide();
    }
    else if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/video.jpg') {
        var height = $("#MainContent_imgPreview").height();
        var width = $(".textpollthemepreview").width();
        $('#MainContent_imgPreview').attr('height', height);
        $('#MainContent_imgPreview').attr('width', width);
        $('#MainContent_imgPreviewResults').attr('height', height);
        $('#MainContent_imgPreviewResults').attr('width', width);
    }
    else if ($("#MainContent_hdnPollType").val() == "image") {
        var height = $("#MainContent_imgPreview").height();
        var width = $(".textpollthemepreview").width();
        $('#MainContent_lblImageName').text($("MainContent_hdnImageName").val());
        $('#MainContent_imgPreview').attr('height', height);
        $('#MainContent_imgPreview').attr('width', width);
        $('#MainContent_imgPreviewResults').attr('height', height);
        $('#MainContent_imgPreviewResults').attr('width', width);
    }
    if (ans0 == "" || ans0 == undefined) {
    }
    else {
        document.getElementById("lblradText_0").innerHTML = stripSpecialChars(ans0);
    }
    if (ans1 == "" || ans1 == undefined) {

    }
    else {
        document.getElementById("lblradText_1").innerHTML = stripSpecialChars(ans1);
    }
    if (ans2 == "" || ans2 == undefined) {
    }
    else {
        document.getElementById("lblradText_2").innerHTML = stripSpecialChars(ans2);
    }
    if (ans3 == "" || ans3 == undefined) {
    }
    else {
        document.getElementById("lblradText_3").innerHTML = stripSpecialChars(ans3);
    }
    if (ans4 == "" || ans4 == undefined) {
    }
    else {
        document.getElementById("lblradText_4").innerHTML = stripSpecialChars(ans4);
    }
    if (ans5 == "" || ans5 == undefined) {
    }
    else {
        document.getElementById("lblradText_5").innerHTML = stripSpecialChars(ans5);
    }
    if (ans6 == "" || ans6 == undefined) {
    }
    else {
        document.getElementById("lblradText_6").innerHTML = stripSpecialChars(ans6);
    }
    if (ans7 == "" || ans7 == undefined) {
    }
    else {
        document.getElementById("lblradText_7").innerHTML = stripSpecialChars(ans7);
    }
    if (ans8 == "" || ans8 == undefined) {
    }
    else {
        document.getElementById("lblradText_8").innerHTML = stripSpecialChars(ans8);
    }
    if (ans9 == "" || ans9 == undefined) {
    }
    else {
        document.getElementById("lblradText_9").innerHTML = stripSpecialChars(ans9);
    }
    if (questiontext == "") {
        if ($("#MainContent_ckbVotes").is(":checked")) {
            $("#MainContent_lblQuestion").text('Question?');
            $("#MainContent_lblQuestionResults").text('Question? (39 votes)');
        }
        else {
            $("#MainContent_lblQuestion").text('Question?');
            $("#MainContent_lblQuestionResults").text('Question?');
        }
    }
    else {
        if ($("#MainContent_ckbVotes").is(":checked")) {
            $("#MainContent_lblQuestion").text(questiontext);
            $("#MainContent_lblQuestionResults").text(questiontext + ' ' + '(39 votes)');
        }
        else {
            $("#MainContent_lblQuestion").text(questiontext);
            $("#MainContent_lblQuestionResults").text(questiontext);
        }
    }

    heightcon = $(".textpollthemepreview").height() + 90;
    $(".contentPanelPollwithoutborder").css("min-height", heightcon);
    var resulttextpre = $("#MainContent_txtResultText").val();

    $("#MainContent_lblResultText").text(resulttextpre);

    $("#MainContent_txtVoteText").on('input', function () {
        var no = $('#MainContent_txtVoteText').val().length;
        if (parseInt(no) > 30) {
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            var designerName = $('#MainContent_txtVoteText').val();
            var nnn = designerName.substr(0, designerName.indexOf(' '));
            var nnnsdf = designerName.substr(designerName.indexOf(' ') + 1);
            var neww = designerName.split(" ")[0];
            if (nnn == "") {
                nnn = designerName;
                if (nnnsdf == "") {
                }
                else {
                    nnnsdf = "";
                }
            }
            $("#MainContent_customvote").text(nnn);
            var newr = designerName.split(" ")[1];
            $("#MainContent_customview").text(nnnsdf);
        }

    });

    $("#MainContent_txtQuestion").on('input', function () {
        var title = $(this).val();
        if ($("#MainContent_ckbVotes").is(":checked")) {
            $("#MainContent_lblQuestion").text(title);
            $("#MainContent_lblQuestionResults").text(title + ' ' + '(39 votes)');
        }
        else {
            $("#MainContent_lblQuestion").text(title);
            $("#MainContent_lblQuestionResults").text(title);
        }
        $("#MainContent_txtQuestion").css("border-color", "#CDCDCD");
        return false;
    });

    $(".txtAns").on("input", function () {
        var id = this.id;
        var suffix = id.match(/\d+/);
        id = parseInt(suffix);
        var title = $(this).val();
        var labelid = "lblradText_" + id;

        document.getElementById(labelid).innerHTML = stripSpecialChars(title);
        $(".txtAns").css("border-color", "#cdcdcd");
        $("#lblThankYouOption_" + id).html("" + stripSpecialChars(title));
        renderChart();
    });

    $("#MainContent_btnSave").click(function () {
        if ($("#MainContent_txtQuestion").val() == "") {
            $("#MainContent_lblErrorJquery").text("Please enter question");
            Materialize.toast("Please enter the question", 4000);
            $("#errorid").show();
            $("#MainContent_txtQuestion").css("border-color", "red");
            return false;
        }
        else {
            //parent.window.document.getElementById("newpollquestion").value = $("#MainContent_txtQuestion").val();
            $("#errorid").hide();
        }

        if ($("#MainContent_txtWidthPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter width");
            Materialize.toast("Please enter width", 4000);
            $("#errorid1").show();
            $("#MainContent_txtWidthPoll").css("border-color", "red");
            return false;
        }
        else {
            $("#errorid1").hide();
        }
        if ($("#MainContent_txtHeightPoll").val() == "") {
            Materialize.toast("Please enter height", 4000);
            $("#MainContent_lblErrorJquery1").text("Please enter height");
            $("#errorid1").show();
            $("#MainContent_txtHeightPoll").css("border-color", "red");
            return false;
        }
        else {
            $("#errorid1").hide();
        }
        if ($('#MainContent_hdnPollType').val() == "video") {
            if ($("#MainContent_txtVideo").val() == "") {
                Materialize.toast("Please enter video code", 4000);
                $("#MainContent_lblErrorJquery").text("Please enter video code");
                $("#errorid").show();
                $("#MainContent_txtVideo").css("border-color", "red");
                return false;
            }
        }
        if ($('#MainContent_hdnPollType').val() == "image") {
            var files = $("#MainContent_fuImage").get(0).files;
            if (files.length > 0) {
                $("#errorid").hide();
                $(".imagetrans").css("color", 'transparent');
            }
            else {
                if ($('#MainContent_lblImageName').text() == "") {
                    $("#MainContent_lblErrorJquery").text("Please upload poll image");
                    Materialize.toast("Please upload image", 4000);
                    $("#errorid").show();
                    $(".imagetrans").css("color", 'red');
                    return false;
                }
            }
        }

        var ansno = $("#MainContent_hdnRepeaterTxts").val();
        ansno = parseInt(ansno) - 1;
        var i = parseInt(0);
        var count = 0;
        for (i = 0; i <= parseInt(ansno) ; i++) {
            var id = "#txtAnswerOption_" + i;
            if ($(id).val() == "" || $(id).val() == undefined) {

            }
            else {
                var j = i + 1;
                var jd = "#txtAnswerOption_" + j;
                for (j = 0; j < parseInt(ansno) ; j++) {
                    if ($(id).val() == $(jd).val()) {
                        $("#MainContent_lblAlreadyExist").text("Answer Options should not be same");
                        Materialize.toast("Answer Options should not be same", 4000);
                        $("#answererr").show();
                        return false;
                    }
                }
                count = parseInt(count) + 1;
            }
        }
        if (parseInt(count) < 2) {
            $("#MainContent_lblAlreadyExist").text("Please add minimum answer options");
            Materialize.toast("A minimum of two answer options are required", 4000);
            $(".txtAns").css("border-color", "red");
            $("#answererr").show();
            return false;
        }


        var noanswers = true;
        if (ansno < 2) {
            noanswers = false; //means an answer was found so its OK
            for (i = 0; i <= parseInt(ansno) ; i++) {
                var id = "#txtAnswerOption_" + i;
                if ($(id).val() == "" || $(id).val() == undefined) {
                    Materialize.toast("Minimum of two answer options are required", 4000);
                    return false;
                }
            }
        }
        else {
            for (i = 0; i <= parseInt(ansno) ; i++) {
                var id = "#txtAnswerOption_" + i;
                if ($(id).val() == "" || $(id).val() == undefined) {
                }
                else if ($(id).val() != "") {
                    noanswers = false; //means an answer was found so its OK
                }
            }
        }
        if (noanswers == true) {
            Materialize.toast("Answer options cannot be blank", 4000);
            return false;
        }

        var height = 0;
        var width = 0;
        var recwidth = 300;
        var recheight = 180;

        if (!isNaN(parseInt($("#MainContent_txtWidthPoll").val()))) {
            width = parseInt($("#MainContent_txtWidthPoll").val());
        }

        if (!isNaN(parseInt($("#MainContent_txtHeightPoll").val()))) {
            height = parseInt($("#MainContent_txtHeightPoll").val());
        }

        if (!isNaN(parseInt($("#MainContent_lblRecThemeHeight").html()))) {
            recheight = parseInt($("#MainContent_lblRecThemeHeight").html());
        }

        if (!isNaN(parseInt($("#MainContent_lblRecThemeWidth").html()))) {
            recwidth = parseInt($("#MainContent_lblRecThemeWidth").html());
        }

        if (width < recwidth) {
            Materialize.toast('Width should be greater than or equal to recommended width of ' + recwidth + 'px.', 4000);
            return false;
        }

        if (height < recheight) {
            Materialize.toast('Height should be greater than or equal to recommended height of ' + recheight + 'px.', 4000);
            return false;
        }

    });

    $("#MainContent_CreateNew").click(function () {

        if ($("#MainContent_ckbAddSponsor").is(":checked")) {
            $("#MainContent_lblLogoName").show();
            if ($("#MainContent_lblLogoName").val() == "") {
                Materialize.toast("Please upload a sponsor image", 4000);
                tabDisplay("design");
                return false;
            }
        }
        Materialize.toast('Auto saving changes...', 2000);
        tabDisplay("create");
    });

    $("#MainContent_DesignNew").click(function () {
        if ($("#MainContent_txtQuestion").val() == "") {
            $("#MainContent_lblErrorJquery").text("Please enter question");
            Materialize.toast("Please enter the question", 4000);
            $("#errorid").show();
            $("#MainContent_txtQuestion").css("border-color", "red");
            return false;
        }
        else {
            $("#errorid").hide();
        }

        var ansno = $("#MainContent_hdnRepeaterTxts").val();
        ansno = parseInt(ansno) - 1;
        var i = parseInt(0);
        var count = 0;
        for (i = 0; i <= parseInt(ansno) ; i++) {
            var id = "#txtAnswerOption_" + i;
            if ($(id).val() == undefined)
            {
            }
            else if ($(id).val() == "") {
                Materialize.toast("Some Answer Options are blank. Please add answers or remove them.", 3000);
                return false;
            }
            else {
                var j = i + 1;
                var jd = "#txtAnswerOption_" + j;
                for (j = 0; j < parseInt(ansno) ; j++) {
                    if ($(id).val() == $(jd).val()) {
                        Materialize.toast("Answer Options should not be same", 3000);
                        return false;
                    }

                }
            }
            count++;

        }

        var noanswers = true;
        if (ansno < 2) {
            noanswers = false; //means an answer was found so its OK
            for (i = 0; i <= parseInt(ansno) ; i++) {
                var id = "#txtAnswerOption_" + i;
                if ($(id).val() == "" || $(id).val() == undefined) {
                    Materialize.toast("Minimum of two answer options are required", 3000);
                    return false;
                }
            }
        }
        else {
            for (i = 0; i <= parseInt(ansno) ; i++) {
                var id = "#txtAnswerOption_" + i;
                if ($(id).val() == undefined) {
                }
                else if ($(id).val() == "") {
                    Materialize.toast("Some Answer Options are blank. Please add answers or remove them.", 3000);
                    return false;
                }
                else if ($(id).val() != "") {
                    noanswers = false; //means an answer was found so its OK
                }
            }
        }
        if (noanswers == true) {
            Materialize.toast("Answer options cannot be blank", 3000);
            return false;
        }

        if (count < 2) {
            $("#MainContent_lblAlreadyExist").text("Please add minimum answer options");
            Materialize.toast("Minimum of two answer options are required", 3000);
            $("#answererr").show();
            return false;
        }
        if ($('#MainContent_hdnPollType').val() == "video") {
            if ($("#MainContent_txtVideo").val() == "") {
                Materialize.toast("Please enter video code", 3000);
                $("#MainContent_lblErrorJquery").text("Please enter video code");
                $("#errorid").show();
                $("#MainContent_txtVideo").css("border-color", "red");
                return false;
            }
        }
        else if ($('#MainContent_hdnPollType').val() == "image") {
            if ($('#MainContent_fileUploadName').val() == "") {
                Materialize.toast("Please upload image", 3000);
                tabDisplay('create');
                return false;
            }
        }

        var height = 0;
        var width = 0;
        var recwidth = 300;
        var recheight = 180;

        if (!isNaN(parseInt($("#MainContent_txtWidthPoll").val()))) {
            width = parseInt($("#MainContent_txtWidthPoll").val());
        }

        if (!isNaN(parseInt($("#MainContent_txtHeightPoll").val()))) {
            height = parseInt($("#MainContent_txtHeightPoll").val());
        }

        if (!isNaN(parseInt($("#MainContent_lblRecThemeHeight").html()))) {
            recheight = parseInt($("#MainContent_lblRecThemeHeight").html());
        }

        if (!isNaN(parseInt($("#MainContent_lblRecThemeWidth").html()))) {
            recwidth = parseInt($("#MainContent_lblRecThemeWidth").html());
        }
        
        if (width < recwidth) {
            Materialize.toast('Width should be greater than or equal to recommended width of ' + recwidth + 'px.', 4000);
            return false;
        }

        if (($("#MainContent_ckbAutoAdjust").is(":checked")) || ($("#MainContent_ckbAutoAdjustDesign").is(":checked")))
        {
            if (height < recheight) {
                Materialize.toast('Height should be greater than or equal to recommended height of ' + recheight + 'px.', 4000);
                return false;
            }
        }
        if ($("#MainContent_radLeadGen").is(":checked") && $("#MainContent_ckbSubmit").is(":checked")) {
            if ((!$("#MainContent_chkPhone").is(":checked")) && (!$("#MainContent_chkEmail").is(":checked")) && ($("#MainContent_txtRedirectURL").val() == "")) {
                Materialize.toast('Please add a redirect URL', 4000);
                $("#MainContent_txtRedirectURL").focus();
                return false;
            }
        }
        Materialize.toast('Auto saving changes...', 2000);
        tabDisplay("design");
        //setColorPickers();
    });

    $("#MainContent_LaunchNew").click(function () {
        if ($("#MainContent_txtQuestion").val() == "") {
            $("#MainContent_lblErrorJquery").text("Please enter question");
            Materialize.toast("Please enter the question", 4000);
            $("#errorid").show();
            $("#MainContent_txtQuestion").css("border-color", "red");
            return false;
        }
        else {
            $("#errorid").hide();
        }
        if ($("#MainContent_txtWidthPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter width");
            Materialize.toast("Please enter width", 4000);
            $("#errorid1").show();
            $("#MainContent_txtWidthPoll").css("border-color", "red");
            return false;
        }
        else {
            $("#errorid1").hide();
        }
        if ($("#MainContent_txtHeightPoll").val() == "") {
            Materialize.toast("Please enter height", 4000);
            $("#MainContent_lblErrorJquery1").text("Please enter height");
            $("#errorid1").show();
            $("#MainContent_txtHeightPoll").css("border-color", "red");
            return false;
        }
        else {
            $("#errorid1").hide();
        }
        if ($('#MainContent_hdnPollType').val() == "video") {
            if ($("#MainContent_txtVideo").val() == "") {
                Materialize.toast("Please enter video code", 4000);
                $("#MainContent_lblErrorJquery").text("Please enter video code");
                $("#errorid").show();
                $("#MainContent_txtVideo").css("border-color", "red");
                return false;
            }
        }
        if ($('#MainContent_hdnPollType').val() == "image") {
            if ($('#MainContent_fileUploadName').val() == "") {
                Materialize.toast("Please upload image", 4000);
                tabDisplay('create');
                return false;
            }
        }
        var ansno = $("#MainContent_hdnRepeaterTxts").val();
        ansno = parseInt(ansno) - 1;
        var i = parseInt(0);
        var count = 0;
        for (i = 0; i <= parseInt(ansno) ; i++) {
            var id = "#txtAnswerOption_" + i;
            if ($(id).val() == undefined) {
            }
            else if ($(id).val() == "") {
                Materialize.toast("Some Answer Options are blank. Please add answers or remove them.", 3000);
                return false;
            }
            else {
                var j = i + 1;
                var jd = "#txtAnswerOption_" + j;
                for (j = 0; j < parseInt(ansno) ; j++) {
                    if ($(id).val() == $(jd).val()) {
                        $("#MainContent_lblAlreadyExist").text("Answer Options should not be same");
                        Materialize.toast("Answer Options should not be same", 4000);
                        $("#answererr").show();
                        return false;
                    }
                }
                count = parseInt(count) + 1;
            }
        }
        if (parseInt(count) < 2) {
            $("#MainContent_lblAlreadyExist").text("Please add minimum answer options");
            Materialize.toast("A minimum of two answer options are required", 4000);
            $(".txtAns").css("border-color", "red");
            $("#answererr").show();
            return false;
        }

        var noanswers = true;
        if (ansno < 2) {
            noanswers = false; //means an answer was found so its OK
            for (i = 0; i <= parseInt(ansno) ; i++) {
                var id = "#txtAnswerOption_" + i;
                if ($(id).val() == "" || $(id).val() == undefined) {
                    Materialize.toast("Minimum of two answer options are required", 4000);
                    return false;
                }
            }
        }
        else {
            for (i = 0; i <= parseInt(ansno) ; i++) {
                var id = "#txtAnswerOption_" + i;
                if ($(id).val() == undefined) {
                }
                else if ($(id).val() == "") {
                    Materialize.toast("Some Answer Options are blank. Please add answers or remove them.", 3000);
                    return false;
                }
                else if ($(id).val() != "") {
                    noanswers = false; //means an answer was found so its OK
                }
            }
        }
        if (noanswers == true) {
            Materialize.toast("Answer options cannot be blank", 4000);
            return false;
        }

        var height = 0;
        var width = 0;
        var recwidth = 300;
        var recheight = 180;

        if (!isNaN(parseInt($("#MainContent_txtWidthPoll").val()))) {
            width = parseInt($("#MainContent_txtWidthPoll").val());
        }

        if (!isNaN(parseInt($("#MainContent_txtHeightPoll").val()))) {
            height = parseInt($("#MainContent_txtHeightPoll").val());
        }

        if (!isNaN(parseInt($("#MainContent_lblRecThemeHeight").html()))) {
            recheight = parseInt($("#MainContent_lblRecThemeHeight").html());
        }

        if (!isNaN(parseInt($("#MainContent_lblRecThemeWidth").html()))) {
            recwidth = parseInt($("#MainContent_lblRecThemeWidth").html());
        }

        if (width < recwidth) {
            Materialize.toast('Width should be greater than or equal to recommended width of ' + recwidth + 'px.', 4000);
            return false;
        }

        if (($("#MainContent_ckbAutoAdjust").is(":checked")) || ($("#MainContent_ckbAutoAdjustDesign").is(":checked")))
        {
            if (height < recheight) {
                Materialize.toast('Height should be greater than or equal to recommended height of ' + recheight + 'px.', 4000);
                return false;
            }
        }

        if ($("#MainContent_ckbAddSponsor").is(":checked")) {
            var url = "/polls/images/" + $("#MainContent_hdnPollID").val() + "/" + $("#MainContent_lblLogoName").val();
            console.log(url);
            $("#MainContent_lblLogoName").show();
            $.ajax({
                url: url,
                async: false,
                statusCode: {
                    200: function () {
                        imagefound = true;
                    }
                }
            });

            if (!imagefound) {
                Materialize.toast("Please upload a sponsor image", 4000);
                tabDisplay("design");
                return false;
            }
        }

        if ($("#MainContent_hdnPollStatus").val() == "Active") {
            $("#MainContent_launchafter").show();
            $("#MainContent_launch").show();
        }

        if ($("#MainContent_radLeadGen").is(":checked") && $("#MainContent_ckbSubmit").is(":checked")) {
            if ((!$("#MainContent_chkPhone").is(":checked")) && (!$("#MainContent_chkEmail").is(":checked")) && ($("#MainContent_txtRedirectURL").val() == "")) {
                Materialize.toast('Please add a redirect URL', 4000);
                $("#MainContent_txtRedirectURL").focus();
                return false;
            }
        }

        Materialize.toast('Auto saving changes...', 2000);

        tabDisplay("launch");
    });

    $("#MainContent_btnGenerateIframe").click(function () {
        if ($("#MainContent_txtQuestion").val() == "") {
            $("#MainContent_lblErrorJquery").text("Please enter question");
            Materialize.toast("Please enter question", 4000);
            $("#errorid").show();
            $("#MainContent_txtQuestion").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }
        if ($("#MainContent_txtWidthPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter width");
            Materialize.toast("Please enter width", 4000);
            $("#errorid1").show();
            $("#MainContent_txtWidthPoll").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }
        if ($("#MainContent_txtHeightPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter height");
            Materialize.toast("Please enter height", 4000);
            $("#errorid1").show();
            $("#MainContent_txtHeightPoll").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }

        $("#MainContent_create").show();
        if ($('#MainContent_videoDiv').is(':hidden')) {
            $("#errorid").hide();
        }
        else {
            if ($("#MainContent_txtVideo").val() == "") {
                $("#MainContent_lblErrorJquery").text("Please enter video code");
                Materialize.toast("Please enter video code", 4000);
                $("#errorid").show();
                $("#MainContent_launcharrow").hide();
                $("#MainContent_launch").hide();
                $("#MainContent_create").show();
                $("#MainContent_createarrow").show();
                $("#MainContent_txtVideo").css("border-color", "#CDCDCD");
                return false;
            }
        }
        if ($('#MainContent_ImageDiv').is(':hidden')) {
        }
        else {
            var files = $("#MainContent_fuImage").get(0).files;
            if (files.length > 0) {
                $("#errorid").hide();
                $(".imagetrans").css("color", 'transparent');
            }
            else {
                if ($('#MainContent_lblImageName').text() == "") {
                    $("#MainContent_lblErrorJquery").text("Please upload image");
                    Materialize.toast("Please upload an image", 4000);
                    $("#errorid").show();
                    $("#MainContent_launcharrow").hide();
                    $("#MainContent_launch").hide();
                    $("#MainContent_create").show();
                    $("#MainContent_createarrow").show();
                    $(".imagetrans").css("color", 'red');
                    return false;
                }
            }
        }
        $("#MainContent_create").hide();
        $("#MainContent_design").show();
        if ($('#MainContent_SponsorBox').is(':hidden')) {
        }
        else {
            var files = $("#MainContent_sponsorLogo").get(0).files;
            if (files.length > 0) {
            }
            else {
                if ($('#MainContent_lblLogoName').text() == "") {
                    $("#MainContent_lblDesignError").text("Please upload logo image");
                    Materialize.toast("Please upload logo image", 4000);
                    $("#designerror").show();
                    $('.imagetranslogo').css('color', 'red');
                    $("#MainContent_launcharrow").hide();
                    $("#MainContent_launch").hide();
                    $("#MainContent_design").show();
                    $("#MainContent_designarrow").show();
                    return false;
                }
            }
        }
        $("#MainContent_design").hide();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightmin = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightmin = iframeheight;
        ////console.log($("#MainContent_hdnPollID").val());
        ////console.log($("#MainContent_hdnParentPollID").val());
        if (parseInt(fixheight) < parseInt(heightmin)) {
            jAlert('Minimum height for this poll should be ' + heightmin, 'Activation Alert', function (r) {
                return false;
            });
            return false;
        }
        if (($("#MainContent_hdnPollID").val() != $("#MainContent_hdnParentPollID").val()) && ($("#MainContent_hdnParentPollID").val() != "Active")) {
            if (confirm('Once you launch this replaced poll, the original poll will be automatically closed and in its place this poll will be displayed.\nAre you sure you want to proceed?', 'Replace poll')) {
                return true;
            }
            else {
                return false;
            }
        }
    });

    $("#MainContent_btnPreview").click(function () {
        if ($("#MainContent_txtQuestion").val() == "") {
            $("#MainContent_lblErrorJquery").text("Please enter question");
            Materialize.toast("Please enter question", 4000);
            $("#errorid").show();
            $("#MainContent_txtQuestion").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }
        if ($("#MainContent_txtWidthPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter width");
            Materialize.toast("Please enter width", 4000);
            $("#errorid1").show();
            $("#MainContent_txtWidthPoll").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }
        if ($("#MainContent_txtHeightPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter height");
            Materialize.toast("Please enter height", 4000);
            $("#errorid1").show();
            $("#MainContent_txtHeightPoll").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }

        $("#MainContent_create").show();
        if ($('#MainContent_videoDiv').is(':hidden')) {
            $("#errorid").hide();
        }
        else {
            if ($("#MainContent_txtVideo").val() == "") {
                $("#MainContent_lblErrorJquery").text("Please enter video code");
                Materialize.toast("Please enter video code", 4000);
                $("#errorid").show();
                $("#MainContent_launcharrow").hide();
                $("#MainContent_launch").hide();
                $("#MainContent_create").show();
                $("#MainContent_createarrow").show();
                $("#MainContent_txtVideo").css("border-color", "#CDCDCD");
                return false;
            }
        }
        if ($('#MainContent_ImageDiv').is(':hidden')) {
        }
        else {
            var files = $("#MainContent_fuImage").get(0).files;
            if (files.length > 0) {
                $("#errorid").hide();
                $(".imagetrans").css("color", 'transparent');
            }
            else {
                if ($('#MainContent_lblImageName').text() == "") {
                    $("#MainContent_lblErrorJquery").text("Please upload image");
                    Materialize.toast("Please upload image", 4000);
                    $("#errorid").show();
                    $("#MainContent_launcharrow").hide();
                    $("#MainContent_launch").hide();
                    $("#MainContent_create").show();
                    $("#MainContent_createarrow").show();
                    $(".imagetrans").css("color", 'red');
                    return false;
                }
            }
        }
        $("#MainContent_create").hide();
        $("#MainContent_design").show();
        if ($('#MainContent_SponsorBox').is(':hidden')) {
        }
        else {
            var files = $("#MainContent_sponsorLogo").get(0).files;
            if (files.length > 0) {
            }
            else {
                if ($('#MainContent_lblLogoName').text() == "") {
                    $("#MainContent_lblDesignError").text("Please upload logo image");
                    Materialize.toast("Please upload logo image", 4000);
                    $("#designerror").show();
                    $('.imagetranslogo').css('color', 'red');
                    $("#MainContent_launcharrow").hide();
                    $("#MainContent_launch").hide();
                    $("#MainContent_design").show();
                    $("#MainContent_designarrow").show();
                    return false;
                }
            }
        }
        $("#MainContent_design").hide();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightmin = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightmin = iframeheight;
        if (parseInt(fixheight) < parseInt(heightmin)) {
            Materialize.toast('Minimum height for this poll should be ' + heightmin + ' px', 4000);
            return false;
        }
    });

    $("#MainContent_radSingleAnswer").click(function () {
        $(".multipleanswers").hide();
        $(".singleanswers").show();
        setMultiThankYouMessagesOption();
    });

    $("#MainContent_radMultipleAnswer").click(function () {
        $(".multipleanswers").show();
        $(".singleanswers").hide();
        setMultiThankYouMessagesOption();
    });

    $(".video-container").css("width", "auto");
    $(".video-container").css("height", "auto");
    $(".videopreview").css("width", "auto");
    $(".videopreview").css("height", "auto");

    if ($("#MainContent_ckbSubmit").is(":checked")) {
        $("#submitbuttoncontent").toggle();
    }

    $("#MainContent_txtInvitationMessage").on("input", function () {
        var invitemessage = $("#MainContent_txtInvitationMessage").val();
        if (invitemessage.length > 0) {
            $("#MainContent_invitationmessagepreview").show();
            $("#MainContent_invitationmessagepreview").html(invitemessage);
        }
        else {
            $("#MainContent_invitationmessagepreview").hide();

        }
        //console.log("INVITE MESSAGE HEIGHT---" + invitemessage);
        console.log("INVITE MESSAGE HEIGHT---" + $("#MainContent_invitationmessagepreview").css("line-height"));
        var sp = invitemessage.split("/r/n");
        console.log("IINE BREAKS - " + sp.length);
        calculateIFrameHeight();

    });

    $("#MainContent_txtVoteButtonLabel").on("input", function () {
        var votebtnlabel = $("#MainContent_txtVoteButtonLabel").val();
        if (votebtnlabel.length > 0) {
        }
        else {
            votebtnlabel = "Vote";
        }
        $("#MainContent_customvote").html(votebtnlabel);
        $("#MainContent_votebutton").html(votebtnlabel);
       hasvotebuttonlabelchanged = true;
    });

    $("#MainContent_customvote").html($("#MainContent_txtVoteButtonLabel").val());

    $("#MainContent_votebutton").html($("#MainContent_txtVoteButtonLabel").val());
    //$('.collapsible').collapsible({
    //    accordion: true
    //});

    $("#MainContent_txtSubmitButtonLabel").on("input", function () {
        var submitbtnlabel = $("#MainContent_txtSubmitButtonLabel").val();
        if (submitbtnlabel.length > 0) {
        }
        else {
            submitbtnlabel = "Submit";
        }
        $("#MainContent_btnSubmitLeadGen").attr("value", submitbtnlabel);
    });

    $("#MainContent_txtResultText").on("input", function () {
        var thankyoumsg = $("#MainContent_txtResultText").val();
        if (thankyoumsg.length > 0) {
        }
        else {
            thankyoumsg = "Thank you for your vote!";
        }
        $("#thankyouoverlaymessage").html(thankyoumsg);
    });

    pollResultsOption($("#MainContent_hdnPollResultsOption").val());

    showSubmitButtonOptions();
    showContactOptions();
    showInvitationMessage();
    showFbtxt();
    showSocialMediaDiv();
    showGoogletxt();
    showTwittertxt();
    showThankYouMessages();

    $("#mCSB_1_container").css("margin-left", "8px !important");
    $("#mCSB_1_container").css("padding-left", "8px !important");

    $("#mCSB_7_container").css("margin-left", "8px !important");
    $("#mCSB_7_container").css("padding-left", "8px !important");

    $("#MainContent_customvote, #MainContent_votebutton, #MainContent_btnSubmitLeadGen, #MainContent_invitationmessagepreview, #thankyouoverlaymessage").css({ "background-color": pollVoteBg, "color": pollVote });
    $("#MainContent_customvote, #MainContent_votebutton, #MainContent_btnSubmitLeadGen").css({ "font-size" : votesize, "font-style" : voteweight, "font-family" : votefont });
    $("#MainContent_customvote, #MainContent_votebutton, #MainContent_btnSubmitLeadGen").css({ "text-decoration" : voteul, "text-align" : votealign, "text-decoration" : votestyle });

    $(".poweredby, .poweredby a, .poweredbyrep, .poweredbyrep a, #placeholdercomments,  #placeholdermobile,  #placeholderemail, #MainContent_invitationmessagepreview, #thankyouoverlay").css("color", "" + $("#MainContent_txtAnswerColor").val());

    calculateIFrameHeight();

    $("#radButton_0").prop("checked", "checked");
    showThankYouOption("0");
    setColorPickerForWebLinkbg();
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function addPreviewAnswer(id) {

    var title = $("#txtAnswerOption_" + id).val();
    console.log(title);
    var labelid = "lblradText_" + id;

    document.getElementById(labelid).innerHTML = stripSpecialChars(title);
    $(".txtAns").css("border-color", "#cdcdcd");
}

function iframewidthset() {
    var widthpoll = $("#MainContent_txtWidthIframe").val();
    $('#MainContent_lblImageWidth').text(widthpoll);
    $('#MainContent_hdnImageRecWidth').val(widthpoll);
    if (widthpoll > 380) {
        $(".Previewres").css("float", 'left');
        $(".Previewres").css("padding-top", '10px');
        $(".createpollnew").css("float", 'left');
}
else {
    $(".Previewres").css("float", '');
    $(".Previewres").css("padding-top", '');
    $(".Previewres").css("padding-left", '');
}
    $(".textpollthemepreview").css("min-width", widthpoll + 'px');
    $(".textpollthemepreviewresults").css("min-width", widthpoll + 'px');

    $("#pollpreviewmaindiv").css("min-width", widthpoll + 'px');
    $("#pollresultspreviewmaindiv").css("min-width", widthpoll + 'px');
    $("#pollpreviewheaderdiv").css("min-width", widthpoll + 'px');
    $("#pollpreviewresultsheaderdiv").css("min-width", widthpoll + 'px');

    $("#pollpreviewmaindiv").css("width", widthpoll + 'px');
    $("#pollresultspreviewmaindiv").css("width", widthpoll + 'px');
    $("#pollpreviewheaderdiv").css("width", widthpoll + 'px');
    $("#pollpreviewresultsheaderdiv").css("width", widthpoll + 'px');

    $("#pollpreviewmaindiv").css("max-width", widthpoll + 'px');
    $("#pollresultspreviewmaindiv").css("max-width", widthpoll + 'px');
    $("#pollpreviewheaderdiv").css("max-width", widthpoll + 'px');
    $("#pollpreviewresultsheaderdiv").css("max-width", widthpoll + 'px');

    $("#MainContent_txtWidthPoll").val(widthpoll);
    var videolink = $("#MainContent_txtVideo").val();
    if (videolink != "") {
        var heightvideo = $(".video-container").height();
        $(".video-container").html(videolink);
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $(".video-container iframe").css('height', valueHeight + 'px');
        $(".video-container iframe").css('width', width + 'px');
        $(".video-container").css('width', width + 'px');
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblHeight").text()) + heightrecad;
        $("#MainContent_lblHeight").text(heightrec);
        $('#MainContent_hdnRecHeightne').val(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightIframe").val();
        heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightrec = iframeheight;

        if (parseInt(height) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
    }
    else if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/video.jpg') {
        var heightvideo = $("#MainContent_imgPreview").height();
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $('#MainContent_imgPreview').attr('height', valueHeight);
        $('#MainContent_imgPreview').attr('width', width);
        $('#MainContent_imgPreviewResults').attr('height', valueHeight);
        $('#MainContent_imgPreviewResults').attr('width', width);
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
        $("#MainContent_lblRecThemeHeight").text(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightsel = $("#MainContent_hdnSetHeight").val();
        heightrec = $("#MainContent_lblRecThemeHeight").text();
        $('#MainContent_hdnRecHeightne').val(heightrec);
        if (heightrec > fixheight) {
            fixheight = heightrec;
        }
        calculateIFrameHeight();
        heightrec = iframeheight;

        if (parseInt(height) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        $("#MainContent_txtHeightPoll").val(fixheight);
    }
    else {
            var maxwidth = widthpoll;
            heighttxt = $('.textpollthemepreview').height();
            //var maxheight = (40 * heighttxt) / 100;
            var maxheight = heighttxt;
            if (maxheight > 200) {
                maxheight = 200;
            }
            var screenImage = $("#MainContent_imgPreview");
            var theImage = new Image();
            theImage.src = screenImage.attr("src");
            var width = theImage.width;
            var height = theImage.height;
            if (width > maxwidth) {
                ratio = maxwidth / width;
                height = height * ratio;
                width = width * ratio;
            }
            if (height > maxheight) {
                ratio = maxheight / height;
                height = height * ratio;
                width = width * ratio;
            }
            if (height == 0) {
                //height = (40 * heighttxt) / 100;
                height = 40;
            }
            if (width == 0) {
                width = maxwidth;
            }
            width = Math.round(width);
            height = Math.round(height);
            $('#MainContent_imgPreview').attr('height', height);
            $('#MainContent_hdnImageHeight').val(height);
            $('#MainContent_imgPreview').attr('width', width);
            $("#MainContent_imgPreview").css("width", width + 'px');
            $("#MainContent_imgPreview").css("height", height + 'px');
            $("#MainContent_imagepreview").css("height", height + 'px');
            $('#MainContent_imgPreviewResults').attr('height', height);
            $('#MainContent_imgPreviewResults').attr('width', width);
            $(".video-container").css("width", widthpoll + 'px');
        }
    var comwidth = widthpoll - 30;
    $(".txtcomments").css("width", comwidth + 'px');
    var answidth = $("#MainContent_txtWidthIframe").val() - 50;
    $(".wrapnew").css("width", answidth + 'px');
    $(".Preview").css("width", widthpoll + 'px');
    $(".Previewres").css("width", widthpoll + 'px');
    return false;
}

function iframewidthset1() {
    var widthpoll = $("#MainContent_txtWidthIframe1").val();
    $('#MainContent_lblImageWidth').text(widthpoll);
    $('#MainContent_hdnImageRecWidth').val(widthpoll);
    if (widthpoll > 380) {
        $(".Previewres").css("float", 'left');
        $(".Previewres").css("padding-top", '10px');
        $(".createpollnew").css("float", 'left');
    }
    else {
        $(".Previewres").css("float", '');
        $(".Previewres").css("padding-top", '');
        $(".Previewres").css("padding-left", '');
    }
    $(".textpollthemepreview").css("min-width", widthpoll + 'px');
    $(".textpollthemepreviewresults").css("min-width", widthpoll + 'px');
    $("#MainContent_txtWidthPoll").val(widthpoll);
    var videolink = $("#MainContent_txtVideo").val();
    if (videolink != "") {
        var heightvideo = $(".video-container").height();
        $(".video-container").html(videolink);
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $(".video-container iframe").css('height', valueHeight + 'px');
        $(".video-container iframe").css('width', width + 'px');
        $(".video-container").css('width', width + 'px');
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblHeight1").text()) + heightrecad;
        $("#MainContent_lblHeight1").text(heightrec);
        $('#MainContent_hdnRecHeightne1').val(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightIframe1").val();
        heightrec = $("#MainContent_hdnRecHeightne1").val();
        calculateIFrameHeight();
        heightrec = iframeheight;
        if (parseInt(height) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
    }
    else if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/video.jpg') {
        var heightvideo = $("#MainContent_imgPreview").height();
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $('#MainContent_imgPreview').attr('height', valueHeight);
        $('#MainContent_imgPreview').attr('width', width);
        $('#MainContent_imgPreviewResults').attr('height', valueHeight);
        $('#MainContent_imgPreviewResults').attr('width', width);
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
        $("#MainContent_lblRecThemeHeight").text(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightsel = $("#MainContent_hdnSetHeight").val();
        heightrec = $("#MainContent_lblRecThemeHeight").text();
        $('#MainContent_hdnRecHeightne1').val(heightrec);
        calculateIFrameHeight();
        heightrec = iframeheight;
        if (parseInt(height) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        if (heightrec > fixheight) {
            fixheight = heightrec;
        }
        $("#MainContent_txtHeightPoll").val(fixheight);
    }
    else {
        var maxwidth = widthpoll;
        heighttxt = $('.textpollthemepreview').height();
        var maxheight = (40 * heighttxt) / 100;
        if (maxheight > 200) {
            maxheight = 200;
        }
        var screenImage = $("#MainContent_imgPreview");
        var theImage = new Image();
        theImage.src = screenImage.attr("src");
        var width = theImage.width;
        var height = theImage.height;
        if (width > maxwidth) {
            ratio = maxwidth / width;
            height = height * ratio;
            width = width * ratio;
        }
        if (height > maxheight) {
            ratio = maxheight / height;
            height = height * ratio;
            width = width * ratio;
        }
        if (height == 0) {
            height = (40 * heighttxt) / 100;
        }
        if (width == 0) {
            width = maxwidth;
        }
        width = Math.round(width);
        height = Math.round(height);
        $('#MainContent_imgPreview').attr('height', height);
        $('#MainContent_hdnImageHeight').val(height);
        $('#MainContent_imgPreview').attr('width', width);
        $("#MainContent_imgPreview").css("width", width + 'px');
        $("#MainContent_imgPreview").css("height", height + 'px');
        $("#MainContent_imagepreview").css("height", height + 'px');
        $('#MainContent_imgPreviewResults').attr('height', height);
        $('#MainContent_imgPreviewResults').attr('width', width);
        $(".video-container").css("width", widthpoll + 'px');
    }
    var comwidth = widthpoll - 30;
    $(".txtcomments").css("width", comwidth + 'px');
    var answidth = $("#MainContent_txtWidthIframe1").val() - 50;
    $(".wrapnew").css("width", answidth + 'px');
    $(".Preview").css("width", widthpoll + 'px');
    $(".Previewres").css("width", widthpoll + 'px');
    return false;
}

function iframeheightset() {
    var heightpoll = $("#MainContent_txtHeightIframe").val();
    $("#MainContent_txtHeightPoll").val(heightpoll);
    var fixheight = $("#MainContent_txtHeightPoll").val();
    var heightrec = $("#MainContent_hdnRecHeightne").val();
    $(".textpollthemepreview").css("max-height", heightpoll + 'px');
    $(".textpollthemepreviewresults").css("max-height", heightpoll + 'px');
    $(".textpollthemepreview").css("min-height", heightpoll + 'px');
    $(".textpollthemepreviewresults").css("min-height", heightpoll + 'px');
    $("#MainContent_hdnSetHeight").val(heightpoll);
        if (parseInt(heightpoll) == parseInt(heightrec)) {
            heightcon = $(".textpollthemepreview").height() + 90;
            $(".contentPanelPollwithoutborder").css("min-height", heightcon);
            $("#MainContent_showrec").hide();
            return false;
        }
        var ansheight = parseInt(heightpoll) - 125;
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '10px');
            ansheight = parseInt(ansheight) + 25;
        }
        else {
            $(".viewtest").css("bottom", '30px');
        }
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
            ansheight = parseInt(ansheight) - 20;
        }
        var imgheight ="";
        if ($('#MainContent_imagepreview').is(':hidden')) {

        }
        else {
            imgheight = $('#MainContent_imagepreview').height();
            ansheight = parseInt(ansheight) - parseInt(imgheight);
        }
        if ($('#MainContent_videopreviewcreate').is(':hidden')) {

        }
        else {
            var width = $("#MainContent_txtWidthPoll").val();
            var valueHeight = Math.round((width / 16) * 9);
            ansheight = parseInt(ansheight) - parseInt(valueHeight);
        }
        $("#MainContent_hdnansHeight").val(ansheight);
        $("#mainanswerdiv2").css("height", ansheight + 'px');
        heightcon = $(".textpollthemepreview").height() + 90;
        $(".contentPanelPollwithoutborder").css("min-height", heightcon);
        var widthpoll = $("#MainContent_txtWidthPoll").val();
        var maxwidth = widthpoll;
        heighttxt = $('.textpollthemepreview').height();
        var maxheight = (40 * heighttxt) / 100;
        if (maxheight > 200) {
            maxheight = 200;
        }
        var widthpre = $('.textpollthemepreview').width();
        $('#MainContent_lblImageWidth').text(widthpre);
        maxheight = Math.round(maxheight);
        $('#MainContent_lblImageHeight').text(maxheight);
        $('#MainContent_hdnImageRecWidth').text(widthpre);
        $('#MainContent_hdnImageRecHeight').val(maxheight);
        var screenImage = $("#MainContent_imgPreview"); 
        var theImage = new Image();
        theImage.src = screenImage.attr("src");
        var width = theImage.width;
        var height = theImage.height;
        if (width > maxwidth) {
            ratio = maxwidth / width;
            height = height * ratio;
            width = width * ratio;
        }
        if (height > maxheight) {
            ratio = maxheight / height;
            height = height * ratio;
            width = width * ratio;
        }
        if (height == 0) {
            height = (100 * heighttxt) / 100;
        }
        if (width == 0) {
            width = maxwidth;
        }
        width = Math.round(width);
        height = Math.round(height);
        if (imgheight == "" || imgheight == NaN) {

        }
        else {
            imgheight = parseInt(height) - parseInt(imgheight);
            var heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) + parseInt(imgheight);
            $("#MainContent_lblRecThemeHeight").text(heightg);
            $("#MainContent_hdnRecHeightne").val(heightg);
            $("#MainContent_lblHeight").text(heightg);
        }
        heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightrec = iframeheight;
        if (parseInt(fixheight) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        //$('#MainContent_imagepreview').css('height', height);
        //$('#MainContent_imgPreview').attr('height', height);
        //$('#MainContent_hdnImageHeight').val(height);
        //$('#MainContent_imgPreview').attr('width', width);
        //$("#MainContent_imgPreview").css("width", width + 'px');
        //$("#MainContent_imgPreview").css("height", height + 'px');
        //$('#MainContent_imgPreviewResults').attr('height', height);
        //$('#MainContent_imgPreviewResults').attr('width', width);
        //$(".video-container").css("width", widthpoll + 'px');
    }

function iframeheightset1() {
        var heightpoll = $("#MainContent_txtHeightIframe1").val();
        $("#MainContent_txtHeightPoll").val(heightpoll);
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightrec = $("#MainContent_hdnRecHeightne1").val();
        $(".textpollthemepreview").css("height", heightpoll + 'px');
        $(".textpollthemepreviewresults").css("height", heightpoll + 'px');
        $("#MainContent_hdnSetHeight").val(heightpoll);
        if (parseInt(heightpoll) == parseInt(heightrec)) {
            heightcon = $(".textpollthemepreview").height() + 90;
            $(".contentPanelPollwithoutborder").css("min-height", heightcon);
            $("#MainContent_showrec").hide();
            return false;
        }
        var ansheight = parseInt(heightpoll) - 125;
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '10px');
            ansheight = parseInt(ansheight) + 25;
        }
        else {
            $(".viewtest").css("bottom", '30px');
        }
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
            ansheight = parseInt(ansheight) - 20;
        }
        var imgheight = "";
        if ($('#MainContent_imagepreview').is(':hidden')) {

        }
        else {
            imgheight = $('#MainContent_imagepreview').height();
            ansheight = parseInt(ansheight) - parseInt(imgheight);
        }
        if ($('#MainContent_videopreviewcreate').is(':hidden')) {

        }
        else {
            var width = $("#MainContent_txtWidthPoll").val();
            var valueHeight = Math.round((width / 16) * 9);
            ansheight = parseInt(ansheight) - parseInt(valueHeight);
        }
        $("#MainContent_hdnansHeight").val(ansheight);
        $("#mainanswerdiv").css("height", ansheight + 'px');
        heightcon = $(".textpollthemepreview").height() + 90;
        $(".contentPanelPollwithoutborder").css("min-height", heightcon);
        var widthpoll = $("#MainContent_txtWidthPoll").val();
        var maxwidth = widthpoll;
        heighttxt = $('.textpollthemepreview').height();
        //var maxheight = (40 * heighttxt) / 100;
        var maxheight = heighttxt;
        if (maxheight > 200) {
            maxheight = 200;
        }
        var widthpre = $('.textpollthemepreview').width();
        $('#MainContent_lblImageWidth').text(widthpre);
        maxheight = Math.round(maxheight);
        $('#MainContent_lblImageHeight').text(maxheight);
        $('#MainContent_hdnImageRecWidth').text(widthpre);
        $('#MainContent_hdnImageRecHeight').val(maxheight);
        var screenImage = $("#MainContent_imgPreview");
        var theImage = new Image();
        theImage.src = screenImage.attr("src");
        var width = theImage.width;
        var height = theImage.height;
        if (width > maxwidth) {
            ratio = maxwidth / width;
            height = height * ratio;
            width = width * ratio;
        }
        if (height > maxheight) {
            ratio = maxheight / height;
            height = height * ratio;
            width = width * ratio;
        }
        if (height == 0) {
            //height = (40 * heighttxt) / 100;
            height = heighttxt;
        }
        if (width == 0) {
            width = maxwidth;
        }
        width = Math.round(width);
        height = Math.round(height);
        if (imgheight == "" || imgheight == NaN) {

        }
        else {
            imgheight = parseInt(height) - parseInt(imgheight);
            var heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) + parseInt(imgheight);
            $("#MainContent_lblRecThemeHeight").text(heightg);
            $("#MainContent_hdnRecHeightne1").val(heightg);
            $("#MainContent_lblHeight1").text(heightg);
        }
        heightrec = $("#MainContent_hdnRecHeightne1").val();       
        calculateIFrameHeight();
        heightrec = iframeheight;
        if (parseInt(fixheight) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        //document.cookie = "iframeheight=" + $("#MainContent_txtHeightIframe1").val();
        document.cookie = "iframeheight=" + $("#MainContent_txtHeightIframe1").val() + "; domain=.insighto.com; path=/;";
        //$('#MainContent_imagepreview').css('height', height);
        //$('#MainContent_imgPreview').attr('height', height);
        //$('#MainContent_hdnImageHeight').val(height);
        //$('#MainContent_imgPreview').attr('width', width);
        //$("#MainContent_imgPreview").css("width", width + 'px');
        //$("#MainContent_imgPreview").css("height", height + 'px');
        //$('#MainContent_imgPreviewResults').attr('height', height);
        //$('#MainContent_imgPreviewResults').attr('width', width);
        //$(".video-container").css("width", widthpoll + 'px');
    }

function iframewidthsetcreate() {
    var widthpoll = $("#MainContent_txtWidthPoll").val();
    $('#MainContent_lblImageWidth').text(widthpoll);
    $('#MainContent_hdnImageRecWidth').text(widthpoll);
    if (widthpoll > 380) {
        $(".Previewres").css("float", 'left');
        $(".Previewres").css("padding-top", '10px');
        $(".createpollnew").css("float", 'left');
    }
    else {  
        $(".Previewres").css("float", '');
        $(".Previewres").css("padding-top", '');
        $(".Previewres").css("padding-left", '');
    }
    $(".textpollthemepreview").css("min-width", widthpoll + 'px');
    $(".textpollthemepreviewresults").css("min-width", widthpoll + 'px');

    $("#pollpreviewmaindiv").css("min-width", widthpoll + 'px');
    $("#pollresultspreviewmaindiv").css("min-width", widthpoll + 'px');
    $("#pollpreviewheaderdiv").css("min-width", widthpoll + 'px');
    $("#pollpreviewresultsheaderdiv").css("min-width", widthpoll + 'px');

    $("#pollpreviewmaindiv").css("width", widthpoll + 'px');
    $("#pollresultspreviewmaindiv").css("width", widthpoll + 'px');
    $("#pollpreviewheaderdiv").css("width", widthpoll + 'px');
    $("#pollpreviewresultsheaderdiv").css("width", widthpoll + 'px');

    $("#pollpreviewmaindiv").css("max-width", widthpoll + 'px');
    $("#pollresultspreviewmaindiv").css("max-width", widthpoll + 'px');
    $("#pollpreviewheaderdiv").css("max-width", widthpoll + 'px');
    $("#pollpreviewresultsheaderdiv").css("max-width", widthpoll + 'px');

    var videolink = $("#MainContent_txtVideo").val();
    if (videolink != null && videolink != "") {
        var heightvideo = $(".video-container").height();
        $(".video-container").html(videolink);
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $(".video-container iframe").css('height', valueHeight + 'px');
        $(".video-container iframe").css('width', width + 'px');
        $(".video-container").css('width', width + 'px');
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
        $("#MainContent_lblRecThemeHeight").text(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightsel = $("#MainContent_hdnSetHeight").val();
        heightrec = $("#MainContent_lblRecThemeHeight").text();
        $('#MainContent_hdnRecHeightne').val(heightrec);
        heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightrec = iframeheight;
        ////console.log("HEIGHT:" + heightrec);
        if (parseInt(fixheight) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        $("#MainContent_txtHeightPoll").val(fixheight);
    }
    else if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/video.jpg') {
        var heightvideo = $("#MainContent_imgPreview").height();
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $('#MainContent_imgPreview').attr('height', valueHeight);
        $('#MainContent_imgPreview').attr('width', width);
        $('#MainContent_imgPreviewResults').attr('height', valueHeight);
        $('#MainContent_imgPreviewResults').attr('width', width);
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
        $("#MainContent_lblRecThemeHeight").text(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightsel = $("#MainContent_hdnSetHeight").val();
        heightrec = $("#MainContent_lblRecThemeHeight").text();
        $('#MainContent_hdnRecHeightne').val(heightrec);
        heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightrec = iframeheight;
        ////console.log("HEIGHT:" + heightrec);
        if (parseInt(fixheight) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        $("#MainContent_txtHeightPoll").val(fixheight);
    }
    else {
        if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/ImagePoll.jpg') {
        }
        else {
            var maxwidth = widthpoll;
            heighttxt = $('.textpollthemepreview').height();
            var maxheight = (40 * heighttxt) / 100;
            if (maxheight > 200) {
                maxheight = 200;
            }
            var screenImage = $("#MainContent_imgPreview");
            var theImage = new Image();
            theImage.src = screenImage.attr("src");
            var width = theImage.width;
            var height = theImage.height;
            if (parseInt(width) > parseInt(maxwidth)) {
                ratio = maxwidth / width;
                height = height * ratio;
                width = width * ratio;
            }
            if (parseInt(height) > parseInt(maxheight)) {
                ratio = maxheight / height;
                height = height * ratio;
                width = width * ratio;
            }
            if (height == 0) {
                height = (40 * heighttxt) / 100;
            }
            if (width == 0) {
                width = maxwidth;
            }
            width = Math.round(width);
            height = Math.round(height);
            $('#MainContent_imgPreview').attr('height', height);
            $('#MainContent_hdnImageHeight').val(height);
            $('#MainContent_imgPreview').attr('width', width);
            $("#MainContent_imgPreview").css("width", width + 'px');
            $("#MainContent_imgPreview").css("height", height + 'px');
            $("#MainContent_imagepreview").css("height", height + 'px');
            $('#MainContent_imgPreviewResults').attr('height', height);
            $('#MainContent_imgPreviewResults').attr('width', width);
            $(".video-container").css("width", maxwidth + 'px');
        }
    }
        $(".Preview").css("width", widthpoll + 'px');
        $(".Previewres").css("width", widthpoll + 'px');
        var comwidth = widthpoll - 30;
        $(".txtcomments").css("width", comwidth + 'px');
        var answidth = $("#MainContent_txtWidthPoll").val() - 50;
        $(".wrapnew").css("width", answidth + 'px');
        return false;
    }

function height() {
        var heightpoll = $("#MainContent_txtHeightPoll").val();
        var ansheight = parseInt(heightpoll) - 120;
        if ($('#MainContent_poweredby').is(':hidden')) {
               ansheight = parseInt(ansheight) + 25;
        }
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
        }
        else {
            ansheight = parseInt(ansheight) - 20;
        }
        $("#MainContent_hdnansHeight").val(ansheight);
        $("#mainanswerdiv").css("height", ansheight + 'px');
}

function iframeheightsetcreate() {
        var heightpoll = $("#MainContent_txtHeightPoll").val();
        $(".textpollthemepreview").css("max-height", heightpoll + 'px');
        $(".textpollthemepreviewresults").css("max-height", heightpoll + 'px');
        $(".textpollthemepreview").css("min-height", heightpoll + 'px');
        $(".textpollthemepreviewresults").css("min-height", heightpoll + 'px');
        $("#MainContent_hdnSetHeight").val(heightpoll);
        var heightrec = $("#MainContent_hdnRecHeightne").val();
        var recimgh = $("#MainContent_hdnImageRecHeight").val();
        calculateIFrameHeight();
        heightrec = iframeheight;
        if (parseInt(heightpoll) == parseInt(heightrec)) {
            heightcon = $(".textpollthemepreview").height() + 90;
            $(".contentPanelPollwithoutborder").css("min-height", heightcon);
            $("#MainContent_showrec").hide();
            return false;
        }
        var ansheight = parseInt(heightpoll) - 125;
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '10px');
            ansheight = parseInt(ansheight) + 25;
        }
        else {
            $(".viewtest").css("bottom", '30px');
        }
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
            ansheight = parseInt(ansheight) - 20;
        }
        var imgheight = "";
        if ($('#MainContent_imagepreview').is(':hidden')) {
            var heightpoll1 = parseInt($("#MainContent_txtHeightPoll").val());
            $(".textpollthemepreview").css("height", heightpoll1 + 'px');
            $(".textpollthemepreviewresults").css("height", heightpoll1 + 'px');
            $("#MainContent_hdnSetHeight").val(heightpoll1);
            var heightrec1 = parseInt($("#MainContent_hdnRecHeightne").val());
            calculateIFrameHeight();
            heightrec1 = iframeheight;

            if (parseInt(heightpoll1) < parseInt(heightrec1)) {
                $("#MainContent_showrec").show();
                $("#MainContent_lblRecHeightbPre").text(heightrec1);
                return;
            }
            else {
                $("#MainContent_showrec").hide();
            }
        }
        else {
            if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/video.jpg') {
                var heightvideo = $("#MainContent_imgPreview").height();
                var width = $(".textpollthemepreview").width();
                var valueHeight = Math.round((width / 16) * 9);

                var heightrecad = valueHeight - heightvideo;
                var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
                $("#MainContent_lblRecThemeHeight").text(heightrec);
                var height = $(".textpollthemepreview").height();
                var fixheight = $("#MainContent_txtHeightPoll").val();
                var heightsel = $("#MainContent_hdnSetHeight").val();
                heightrec = $("#MainContent_lblRecThemeHeight").text();
                $('#MainContent_hdnRecHeightne').val(heightrec);
                heightrec = $("#MainContent_hdnRecHeightne").val();
                calculateIFrameHeight();
                heightrec = iframeheight;
                if (parseInt(fixheight) < parseInt(heightrec)) {
                    $("#MainContent_showrec").show();
                    $("#MainContent_lblRecHeightbPre").text(heightrec);
                }
                else {
                    $("#MainContent_showrec").hide();
                }
                $("#MainContent_txtHeightPoll").val(fixheight);
            }
            else {
                imgheight = $('#MainContent_imagepreview').height();
                ansheight = parseInt(ansheight) - parseInt(imgheight);
                var widthpoll = $("#MainContent_txtWidthPoll").val();
                var maxwidth = widthpoll;
                heighttxt = $('.textpollthemepreview').height();
                var maxheight = (40 * heighttxt) / 100;
                if (maxheight > 200) {
                    maxheight = 200;
                }
                var widthpre = $('.textpollthemepreview').width();
                $('#MainContent_lblImageWidth').text(widthpre);
                maxheight = Math.round(maxheight);
                $('#MainContent_lblImageHeight').text(maxheight);
                $('#MainContent_hdnImageRecWidth').text(widthpre);
                $('#MainContent_hdnImageRecHeight').val(maxheight);
                var screenImage = $("#MainContent_imgPreview");
                var theImage = new Image();
                theImage.src = screenImage.attr("src");
                var width = theImage.width;
                var height = theImage.height;
                if (width > maxwidth) {
                    ratio = maxwidth / width;
                    height = height * ratio;
                    width = width * ratio;
                }
                if (height > maxheight) {
                    ratio = maxheight / height;
                    height = height * ratio;
                    width = width * ratio;
                }
                if (height == 0) {
                    //height = (40 * heighttxt) / 100;
                    height = heighttxt;
                }
                if (width == 0) {
                    width = maxwidth;
                }
                width = Math.round(width);
                height = Math.round(height);
                if (imgheight == "" || imgheight == NaN) {

                }
                else {
                    imgheight = parseInt(height) - parseInt(imgheight);
                    var heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) + parseInt(imgheight);
                    $("#MainContent_lblRecThemeHeight").text(heightg);
                    $("#MainContent_hdnRecHeightne").val(heightg);
                    $("#MainContent_lblHeight").text(heightg);
                }
                heightrec = $("#MainContent_hdnRecHeightne").val();
                calculateIFrameHeight();
                heightrec = iframeheight;
                if (parseInt(heightpoll) < parseInt(heightrec)) {
                    $("#MainContent_showrec").show();
                    $("#MainContent_lblRecHeightbPre").text(heightrec);
                }
                else {
                    $("#MainContent_showrec").hide();
                }
            }
        }
      
        if ($('#MainContent_videopreviewcreate').is(':hidden')) {

        }
        else {
            var width = $("#MainContent_txtWidthPoll").val();
            var valueHeight = Math.round((width / 16) * 9);
            ansheight = parseInt(ansheight) - parseInt(valueHeight);
        }

        $("#MainContent_hdnansHeight").val(ansheight);
        $("#mainanswerdiv").css("height", ansheight + 'px');
        heightcon = $(".textpollthemepreview").height() + 90;
        $(".contentPanelPollwithoutborder").css("min-height", heightcon);
  
    return false;
}

function activeleft(x) {
    $("#leftinactive").hide()
    $("#leftactive").show();
}

function inactiveleft(x) {
    $("#leftinactive").show();
    $("#leftactive").hide();
}

function activecenter(x) {
    $("#centerinactive").hide();
    $("#centeractive").show();
}

function inactivecenter(x) {
    $("#centerinactive").show();
    $("#centeractive").hide();
}

function activeright(x) {
    $("#rightinactive").hide();
    $("#rightactive").show();
}

function inactiveright(x) {
    $("#rightinactive").show();
    $("#rightactive").hide();
}

function anactiveleft(x) {
    $("#answerleftinactive").hide()
    $("#answerleftactive").show();
}

function aninactiveleft(x) {
    $("#answerleftinactive").show();
    $("#answerleftactive").hide();
}

function anactivecenter(x) {
    $("#answercenterinactive").hide();
    $("#answercenteractive").show();
}

function aninactivecenter(x) {
    $("#answercenterinactive").show();
    $("#answercenteractive").hide();
}

function anactiveright(x) {
    $("#answerrightinactive").hide();
    $("#answerrightactive").show();
}

function aninactiveright(x) {
    $("#answerrightinactive").show();
    $("#answerrightactive").hide();
}

function vtactiveleft(x) {
    $("#voteleftinactive").hide()
    $("#voteleftactive").show();
}

function vtinactiveleft(x) {
    $("#voteleftinactive").show();
    $("#voteleftactive").hide();
}

function vtactivecenter(x) {
    $("#votecenterinactive").hide();
    $("#votecenteractive").show();
}

function vtinactivecenter(x) {
    $("#votecenterinactive").show();
    $("#votecenteractive").hide();
}

function vtactiveright(x) {
    $("#voterightinactive").hide();
    $("#voterightactive").show();
}

function vtinactiveright(x) {
    $("#voterightinactive").show();
    $("#voterightactive").hide();
}

function mactiveleft(x) {
    $("#msgleftinactive").hide()
    $("#msgleftactive").show();
}

function minactiveleft(x) {
    $("#msgleftinactive").show();
    $("#msgleftactive").hide();
}

function mactivecenter(x) {
    $("#msgcenterinactive").hide();
    $("#msgcenteractive").show();
}

function minactivecenter(x) {
    $("#msgcenterinactive").show();
    $("#msgcenteractive").hide();
}

function mactiveright(x) {
    $("#msgrightinactive").hide();
    $("#msgrightactive").show();
}

function minactiveright(x) {
    $("#msgrightinactive").show();
    $("#msgrightactive").hide();
}

function questionboldactive() {
    $("#MainContent_imgBold").hide();
    $("#MainContent_imgBoldActive").show();
    $("#MainContent_hdnQuestionBold").val("bold");
    $('.questioncss').css({ 'font-weight': 'bold' });
}

function questionboldinactive() {
    $("#MainContent_imgBold").show();
    $("#MainContent_imgBoldActive").hide();
    $("#MainContent_hdnQuestionBold").val("unbold");
    $('.questioncss').css({ 'font-weight': 'normal' });
}

function questionitalicactive() {
    $("#MainContent_imgItalicActive").show();
    $("#MainContent_imgItalic").hide();
    $("#MainContent_hdnQuestionItalic").val("italic");
    $('.questioncss').css({ 'font-style': 'italic' });
}

function questionitalicinactive() {
    $("#MainContent_imgItalicActive").hide();
    $("#MainContent_imgItalic").show();
    $("#MainContent_hdnQuestionItalic").val("notitalic");
    $('.questioncss').css({ 'font-style': 'normal' });
}

function questionulactive() {
    $("#MainContent_imgUnderlineActive").show();
    $("#MainContent_imgUnderline").hide();
    $("#MainContent_hdnQuestionUL").val("underline");
    $('.questioncss').css({ 'text-decoration': 'underline' });
}

function questionulinactive() {
    $("#MainContent_imgUnderlineActive").hide();
    $("#MainContent_imgUnderline").show();
    $("#MainContent_hdnQuestionUL").val("notunderline");
    $('.questioncss').css({ 'text-decoration': 'none' });
}

function questionalignright() {
    $("#MainContent_hdnQuestionAlign").val("right");
    $("#questionalign").hide();
    $("#MainContent_qrightactive").show();
    $("#MainContent_qleftinactive").hide();
    $("#MainContent_qleftactive").hide();
    $("#MainContent_qcenteractive").hide();
    $('.questioncss').css({ 'text-align': 'right' });
}

function questionaligncenter() {
    $("#MainContent_hdnQuestionAlign").val("center");
    $("#questionalign").hide();
    $("#MainContent_qcenteractive").show();
    $("#MainContent_qleftinactive").hide();
    $("#MainContent_qrightactive").hide();
    $("#MainContent_qleftactive").hide();
    $('.questioncss').css({ 'text-align': 'center' });
}

function questionalignleft() {
    $("#MainContent_hdnQuestionAlign").val("left");
    $("#questionalign").hide();
    $("#MainContent_qleftactive").show();
    $("#MainContent_qleftinactive").hide();
    $("#MainContent_qrightactive").hide();
    $("#MainContent_qcenteractive").hide();
    $('.questioncss').css({ 'text-align': 'left' });
}

function answerboldactive() {
    $("#MainContent_imgABold").hide();
    $("#MainContent_imgABoldActive").show();
    $("#MainContent_hdnAnswerBold").val("bold");
    $('#mainanswerdiv2').css({ 'font-weight': 'bold' });
    renderChart();  
}

function answerboldinactive() {
    $("#MainContent_imgABold").show();
    $("#MainContent_imgABoldActive").hide();
    $("#MainContent_hdnAnswerBold").val("unbold");
    $('#mainanswerdiv2').css({ 'font-weight': 'normal' });
    renderChart();
}

function answeritalicactive() {
    $("#MainContent_imgAItalicActive").show();
    $("#MainContent_imgAItalic").hide();
    $("#MainContent_hdnAnswerItalic").val("italic");
    $('#mainanswerdiv2').css({ 'font-style': 'italic' });
    renderChart();
}

function answeritalicinactive() {
    $("#MainContent_imgAItalicActive").hide();
    $("#MainContent_imgAItalic").show();
    $("#MainContent_hdnAnswerItalic").val("notitalic");
    $('#mainanswerdiv2').css({ 'font-style': 'normal' });
    renderChart();
}

function answerulactive() {
    ////////console.log('underline');
    $("#MainContent_imgAULActive").show();
    $("#MainContent_imgAUL").hide();
    $("#MainContent_hdnAnswerUL").val("underline");
    ////////console.log($("#MainContent_hdnAnswerUL").val());
    $('#mainanswerdiv2').css({ 'text-decoration': 'underline' });
    renderChart();
}

function answerulinactive() {
    ////////console.log('notunderline');
    $("#MainContent_imgAULActive").hide();
    $("#MainContent_imgAUL").show();
    $("#MainContent_hdnAnswerUL").val("notunderline");
    ////////console.log($("#MainContent_hdnAnswerUL").val());
    $('#mainanswerdiv2').css({ 'text-decoration': 'none' });
    renderChart();
}

function answeralignright() {
    $("#MainContent_hdnAnswerAlign").val("right");
    $("#answeralign").hide();
    $("#MainContent_arightactive").show();
    $("#MainContent_aleftinactive").hide();
    $("#MainContent_aleftactive").hide();
    $("#MainContent_acenteractive").hide();
    $('#mainanswerdiv2').css({ 'text-align': 'right' });
    $('.txtRow, #MainContent_invitationmessagepreview').css({ 'text-align': 'right' });
    $('#previewansweroptions').css({ 'text-align': 'right' });
    $('#previewansweroptions tr').css({ 'text-align': 'right' });
    $('#previewansweroptions tr td').css({ 'text-align': 'right' });
    $('#purejschart').css({ 'text-align': 'right' });
    $('#purejschart div').css({ 'text-align': 'right !important' });
    $('.chartlabel').css({ 'text-align': 'right' });
}

function answeraligncenter() {
    $("#MainContent_hdnAnswerAlign").val("center");
    $("#answeralign").hide();
    $("#MainContent_acenteractive").show();
    $("#MainContent_aleftinactive").hide();
    $("#MainContent_arightactive").hide();
    $("#MainContent_aleftactive").hide();
    $('#mainanswerdiv2').css({ 'text-align': 'center' });
    $('.txtRow, #MainContent_invitationmessagepreview').css({ 'text-align': 'center' });
    $('#previewansweroptions').css({ 'text-align': 'center' });
    $('#previewansweroptions tr').css({ 'text-align': 'center' });
    $('#previewansweroptions tr td').css({ 'text-align': 'center' });
    $('#purejschart').css({ 'text-align': 'center' });
    $('#purejschart div').css({ 'text-align': 'center !important' });
    $('.chartlabel').css({ 'text-align': 'center' });
}

function answeralignleft() {
    $("#MainContent_hdnAnswerAlign").val("left");
    $("#answeralign").hide();
    $("#MainContent_aleftactive").show();
    $("#MainContent_aleftinactive").hide();
    $("#MainContent_arightactive").hide();
    $("#MainContent_acenteractive").hide();
    $('#mainanswerdiv2').css({ 'text-align': 'left' });
    $('.txtRow, #MainContent_invitationmessagepreview').css({ 'text-align': 'left' });
    $('#previewansweroptions').css({ 'text-align': 'left' });
    $('#previewansweroptions tr').css({ 'text-align': 'left' });
    $('#previewansweroptions tr td').css({ 'text-align': 'left' });
    $('#purejschart').css({ 'text-align': 'left' });
    $('#purejschart div').css({ 'text-align': 'left !important' });
    $('.chartlabel').css({ 'text-align': 'left' });
}

function voteboldactive() {
    $("#MainContent_imgVBold").hide();
    $("#MainContent_imgVBoldActive").show();
    $("#MainContent_hdnVoteBold").val("bold");
    $('#MainContent_customvote, #MainContent_votebutton,#MainContent_btnSubmitLeadGen').css({ 'font-weight': 'bold' });
}

function voteboldinactive() {
    $("#MainContent_imgVBold").show();
    $("#MainContent_imgVBoldActive").hide();
    $("#MainContent_hdnVoteBold").val("unbold");
    $('#MainContent_customvote, #MainContent_votebutton,#MainContent_btnSubmitLeadGen').css({ 'font-weight': 'normal' });
}

function voteitalicactive() {
    $("#MainContent_imgVItalicActive").show();
    $("#MainContent_imgVItalic").hide();
    $("#MainContent_hdnVoteItalic").val("italic");
    $('.viewandvoteimagepreview').css({ 'font-style': 'italic' });
}

function voteitalicinactive() {
    $("#MainContent_imgVItalicActive").hide();
    $("#MainContent_imgVItalic").show();
    $("#MainContent_hdnVoteItalic").val("notitalic");
    $('#MainContent_customvote, #MainContent_votebutton,#MainContent_btnSubmitLeadGen').css({ 'font-style': 'normal' });
}

function voteulactive() {
    $("#MainContent_imgVULActive").show();
    $("#MainContent_imgVUL").hide();
    $("#MainContent_hdnVoteUL").val("underline");
    $('#MainContent_customvote, #MainContent_votebutton,#MainContent_btnSubmitLeadGen').css({ 'text-decoration': 'underline' });
}

function voteulinactive() {
    $("#MainContent_imgVULActive").hide();
    $("#MainContent_imgVUL").show();
    $("#MainContent_hdnVoteUL").val("notunderline");
    $('#MainContent_customvote, #MainContent_votebutton,#MainContent_btnSubmitLeadGen').css({ 'text-decoration': 'none' });
}

function votealignright() {
    $("#MainContent_hdnVoteAlign").val("right");
    $("#votealign").hide();
    $("#MainContent_vrightactive").show();
    $("#MainContent_vleftinactive").hide();
    $("#MainContent_vleftactive").hide();
    $("#MainContent_vcenteractive").hide();
    $('#MainContent_customvote, #MainContent_votebutton,#MainContent_btnSubmitLeadGen').css({ 'text-align': 'right' });
}

function votealigncenter() {
    $("#MainContent_hdnVoteAlign").val("center");
    $("#votealign").hide();
    $("#MainContent_vcenteractive").show();
    $("#MainContent_vleftinactive").hide();
    $("#MainContent_vrightactive").hide();
    $("#MainContent_vleftactive").hide();
    $('#MainContent_customvote, #MainContent_votebutton,#MainContent_btnSubmitLeadGen').css({ 'text-align': 'center' });
}

function votealignleft() {
    $("#MainContent_hdnVoteAlign").val("left");
    $("#votealign").hide();
    $("#MainContent_vleftactive").show();
    $("#MainContent_vleftinactive").hide();
    $("#MainContent_vrightactive").hide();
    $("#MainContent_vcenteractive").hide();
    $('#MainContent_customvote, #MainContent_votebutton,#MainContent_btnSubmitLeadGen').css({ 'text-align': 'left' });
}

function msgboldactive() {
    $("#MainContent_imgMBold").hide();
    $("#MainContent_imgMBoldActive").show();
    $("#MainContent_hdnMsgBold").val("bold");
    $('.resultpreview').css({ 'font-weight': 'bold' });
}

function msgboldinactive() {
    $("#MainContent_imgMBold").show();
    $("#MainContent_imgMBoldActive").hide();
    $("#MainContent_hdnMsgBold").val("unbold");
    $('.resultpreview').css({ 'font-weight': 'normal' });
}

function msgitalicactive() {
    $("#MainContent_imgMItalicActive").show();
    $("#MainContent_imgMItalic").hide();
    $("#MainContent_hdnMsgItalic").val("italic");
    $('.resultpreview').css({ 'font-style': 'italic' });
}

function msgitalicinactive() {
    $("#MainContent_imgMItalicActive").hide();
    $("#MainContent_imgMItalic").show();
    $("#MainContent_hdnMsgItalic").val("notitalic");
    $('.resultpreview').css({ 'font-style': 'normal' });
}

function msgulactive() {
    $("#MainContent_imgMULActive").show();
    $("#MainContent_imgMUL").hide();
    $("#MainContent_hdnMsgUL").val("underline");
    $('.resultpreview').css({ 'text-decoration': 'underline' });
}

function msgulinactive() {
    $("#MainContent_imgMULActive").hide();
    $("#MainContent_imgMUL").show();
    $("#MainContent_hdnMsgUL").val("notunderline");
    $('.resultpreview').css({ 'text-decoration': 'none' });
}

function msgalignright() {
    $("#MainContent_hdnMsgAlign").val("right");
    $("#msgalign").hide();
    $("#MainContent_mrightactive").show();
    $("#MainContent_mleftinactive").hide();
    $("#MainContent_mleftactive").hide();
    $("#MainContent_mcenteractive").hide();
    $('.resultpreview').css({ 'text-align': 'right' });
}

function msgaligncenter() {
    $("#MainContent_hdnMsgAlign").val("center");
    $("#msgalign").hide();
    $("#MainContent_mcenteractive").show();
    $("#MainContent_mleftinactive").hide();
    $("#MainContent_mrightactive").hide();
    $("#MainContent_mleftactive").hide();
    $('.resultpreview').css({ 'text-align': 'center' });
}

function msgalignleft() {
    $("#MainContent_hdnMsgAlign").val("left");
    $("#msgalign").hide();
    $("#MainContent_mleftactive").show();
    $("#MainContent_mleftinactive").hide();
    $("#MainContent_mrightactive").hide();
    $("#MainContent_mcenteractive").hide();
    $('.resultpreview').css({ 'text-align': 'left' });
}

function showimagepreview() {
    console.log($('#MainContent_hdnImageName').val());
    console.log($('#MainContent_hdnImageLink').val());
    var imagepath = $("#MainContent_fuImage_ctl00").val();
    var imagename = "";
    $("#MainContent_lblImageName").show();
    if ((imagepath != "No file chosen") && (imagepath !="")) {
        var arr = imagepath.split('\\');
        imagename = arr[arr.length - 1];
    }
    else {
        imagename = $("#MainContent_lblImageName").html();
    }
    $("#MainContent_lblImageName").html(imagename);
    $("#MainContent_fileUploadName").val(imagename);
    var pollid = $("#MainContent_hdnPollID").val();
    //console.log(imagename + pollid);
    $("#MainContent_imgPreview").attr("src", "images/" + pollid + "/" + imagename);
    $("#MainContent_imgPreviewResults").attr("src", "images/" + pollid + "/" + imagename);
    window.parent.document.getElementById('newpollimagelink').value = $("#MainContent_lblImageName").val();

}

function showlogopreview() {

    var imagepath = $("#MainContent_sponsorLogo_ctl00").val();
    var splitpath = imagepath.split("\\");
    var imagename = splitpath[splitpath.length-1];
    console.log(imagename);

    $("#MainContent_lblLogoName").val(imagename);
    $("#sponsorimageinleadgen").show();
    $("#MainContent_sponsorimageinleadgendiv").show();
    $("#MainContent_lblLogoName").show();
    $("#MainContent_lblLogoName").show();

    var pollid = $("#MainContent_hdnPollID").val();

    $("#MainContent_logo").attr("src", "/polls/images/" + pollid + "/" + imagename);
    $("#MainContent_sponsorimageleadgenimg").attr("src", "/polls/images/" + pollid + "/" + imagename);
    $("#MainContent_imgSpoResults").attr("src", "/polls/images/" + pollid + "/" + imagename);

    $("#MainContent_logo").attr("height", "43");
    $("#MainContent_sponsorimageleadgenimg").attr("height", "43");
    $("#MainContent_imgSpoResults").attr("height", "43");

    $("#MainContent_logo").attr("width", "110");
    $("#MainContent_sponsorimageleadgenimg").attr("width", "110");
    $("#MainContent_imgSpoResults").attr("width", "110");

}

function showPoweredBy() {
    if ($('#MainContent_ckbInsightoBrand').is(':checked')) {
        $("#MainContent_poweredby").hide();
        $("#MainContent_poweredbyResults").hide();
        $("#MainContent_divpoweredbyleadgen").hide();
        $('.viewtest').css({ 'bottom': '10px' });
        $('.sponsorimagepreviewres').css({ 'bottom': '10px' });
        $('.newsponsocialdiv').css({ 'bottom': '10px' });
    }
    else {
        $("#MainContent_poweredby").show();
        $("#MainContent_poweredbyResults").show();
        $("#MainContent_divpoweredbyleadgen").show();
        $('.viewtest').css({ 'padding-bottom': '0px' });
        $('.sponsorimagepreviewres').css({ 'bottom': '30px' });
        $('.viewtest').css({ 'bottom': '30px' });
    }
    calculateIFrameHeight();
    //checkSocialMediaHeightAdjustment();
    renderChart();
}

function showCommentbox() {
    var numberofrows = parseInt($("#MainContent_hdnRepeaterTxts").val());
    if ($('#MainContent_hdnPollStatus') == "Active")
    {
        if (!$('#MainContent_commentbox').is(':hidden')) {
            $("#MainContent_chkComments").attr("checked", "checked");
        }
        else {
            $("#MainContent_chkComments").attr("checked", "");
        }
    }
    else{
    if ($('#MainContent_commentbox').is(':hidden')) {
        $("#MainContent_commentbox").show();
        //checkCommentsBoxHeightAdjustment();
        var heightg = 0;

        if (($('#MainContent_ckbAddSponsor').is('checked')) || ($("#MainContent_ckbGoogle").is(":checked")) || ($("#MainContent_ckbFb").is(":checked")) || ($("#MainContent_ckbTwitter").is(":checked")) || (numberofrows > 5))
        {
            heightg = parseInt($("#MainContent_lblRecThemeHeight").text());
        }
        else
        {
            heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) + commentboxheight;
        }
        $("#MainContent_lblRecThemeHeight").text(heightg);
        $("#MainContent_hdnRecHeightne").val(heightg);
        var answerno = $("#MainContent_hdnRepeaterTxts").val();
        answerno = 20 * parseInt(answerno);
        if ($("#MainContent_chkComments").is(":checked")) {
            answerno = parseInt(answerno) + commentboxheight;
        }
        var answerheight = $("#MainContent_hdnansHeight").val();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
    }
    else {
        $("#MainContent_commentbox").hide();
        var heightg = 0;
        if (($('#MainContent_ckbAddSponsor').is('checked')) || ($("#MainContent_ckbGoogle").is(":checked")) || ($("#MainContent_ckbFb").is(":checked")) || ($("#MainContent_ckbTwitter").is(":checked")) || (numberofrows > 5)) {
            heightg = parseInt($("#MainContent_lblRecThemeHeight").text());
        }
        else {
            heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) - commentboxheight;
        }

        $("#MainContent_lblRecThemeHeight").text(heightg);
        $("#MainContent_hdnRecHeightne").val(heightg);
        var answerno = $("#MainContent_hdnRepeaterTxts").val();
        answerno = 20 * parseInt(answerno);
        if ($("#MainContent_chkComments").is(":checked")) {
            answerno = parseInt(answerno) + commentboxheight;
        }
        var answerheight = $("#MainContent_hdnansHeight").val();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
    }
    renderChart();
    }
}

function showResultTextbox() {
    if ($('#MainContent_ckbResultsPrivate').is(':checked')) {
        $("#MainContent_ResultBox").show();
        $("#MainContent_results").hide();
        $("#MainContent_resulttextpreview").show();
        var height = $(".textpollthemepreview").height();
        $(".textpollthemepreviewresults").css("height", height + 'px');
        $("#MainContent_customvote").html("Vote");
        $("#MainContent_votebutton").html("Vote");
    }
    else {
        $("#MainContent_ResultBox").hide();
        $("#MainContent_results").show();
        $("#MainContent_resulttextpreview").hide();
        var height = $(".textpollthemepreview").height();
        $(".textpollthemepreviewresults").css("height", height + 'px');
        $("#MainContent_customvote").html("Vote and view results");
        $("#MainContent_votebutton").html("Vote and view results");
    }
    calculateIFrameHeight();
}

function showSponsorDetails() {
    if ($("#MainContent_ckbAddSponsor").is(":checked")) {
        
        $("#sponsorandsocialmediainleadgen").show();
        $("#MainContent_sponsorimageinleadgendiv").show();
        $("#votebuttonmaindiv").hide();
        $("#votebuttonwithsponsormaindiv").show();
        $("#MainContent_sponsorpreviw").show();
        $("#MainContent_SponsorBox").show();
        $("#sponsorandsocialmediainresults").show();
        $("#sponsorimageinresults").show();
        $("#MainContent_sponsorResults").show();
        
        $("#MainContent_SponsorBox").css("height", "300px");
        $("#sponsorfilepath").show();
        $("#MainContent_lblLogoName").show();
        if ($("#MainContent_txtSponsorText").val() == "") {
            $("#MainContent_txtSponsorText").val("Sponsored by");
        }
        //if ($("#MainContent_txtSponsorLink").val() == "") {
        //    $("#MainContent_txtSponsorLink").val("");
        //}
    }
    else {
        $("#MainContent_sponsorimageinleadgendiv").hide();
        $("#votebuttonmaindiv").show();

        $("#MainContent_SponsorBox").hide();
        $("#votebuttonwithsponsormaindiv").hide();
        $("#MainContent_sponsorpreviw").hide();
        if (($("#MainContent_ckbGoogle").is(":checked")) || ($("#MainContent_ckbTwitter").is(":checked")) || ($("#MainContent_ckbFb").is(":checked"))) {
            $("#sponsorandsocialmediainresults").show();
        }
        else {
            $("#sponsorandsocialmediainresults").hide();
        }
        $("#sponsorimageinresults").hide();
        $("#sponsorResults").hide();
    }
    calculateIFrameHeight();
    renderChart();
}

function showSponsorDetailsOnLoad() {
    $("#MainContent_SponsorBox").show();
    $("#MainContent_SponsorBox").css("display", "block");
    $("#MainContent_SponsorBox").css("height", "300px");
    $("#MainContent_lblLogoName").show();

   // $(".newsponsocialdiv").css("margin-top", '15px');
    $("#socialmediadiv").css("width", "50% !important");
    //$('.newsponsocialdiv').css({ 'width': '31%' });
    //$('.newsponsocialdiv').css({ 'margin-left': '0px' });
    //$('.newsponsocialdiv').css({ 'margin-bottom': '-15px' });
    $("#MainContent_sponsorpreviw").show();
    $("#MainContent_sponsorResults").show();
    checkLogoAndSponsorForHeightAdjustment();
   
    calculateIFrameHeight();
    renderChart();
}

function Showcreate() {
    $("#MainContent_create").show();
    $("#MainContent_design").hide();
    $("#MainContent_launch").hide();
    $("#MainContent_createarrow").show();
    $("#MainContent_designarrow").hide();
    $("#MainContent_launcharrow").hide();
    var heightm = $(".textpollthemepreview").height(); ;
    var heightrec = $("#MainContent_lblHeight").text();
    if (heightrec == "") {
        heightrec = $(".textpollthemepreview").height();
    }
    $('.textpollthemepreview').css('height', heightm);
    if (heightm <= heightrec) {
        $(".poweredby").css("position", '');
        $(".viewtest").css("position", '');
        $(".poweredby").css("text-align", 'center');
        $(".poweredby").css("width", '100%');
        $(".poweredby").css("margin-top", '');
        $(".poweredby").css("margin-bottom", '5px');
        $(".poweredby").css("padding-bottom", '10px');
        $(".viewtest").css("text-align", 'center');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
        }
    }
    else {
        $(".poweredby").css("position", 'absolute');
        $(".poweredby").css("bottom", '10px');
        $(".poweredby").css("text-align", 'center');
        $(".poweredby").css("width", '100%');
        $(".poweredby").css("margin-top", '0px');
        $(".poweredby").css("margin-bottom", '0px');
        $(".poweredby").css("padding-bottom", '0px');
        $(".viewtest").css("padding-bottom", '0px');
        $(".viewtest").css("position", 'absolute');
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '10px');
        }
        else {
            $(".viewtest").css("bottom", '30px');
        };
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
        }
    }
    renderChart();
}

function Showdesign() {
    $("#MainContent_create").hide();
    $("#MainContent_design").show();
    $("#MainContent_launch").hide();
    $("#MainContent_createarrow").hide();
    $("#MainContent_designarrow").show();
    $("#MainContent_launcharrow").hide();
    var heightm = $(".textpollthemepreview").height();
    var heightrec = $("#MainContent_lblHeight").text();
    if (heightrec == "") {
        heightrec = $(".textpollthemepreview").height();
    }
    $('.textpollthemepreview').css('height', heightm);
    if (heightm <= heightrec) {
        $(".poweredby").css("position", '');
        $(".viewtest").css("position", '');
        $(".poweredby").css("text-align", 'center');
        $(".poweredby").css("width", '100%');
        $(".poweredby").css("margin-top", '');
        $(".poweredby").css("margin-bottom", '5px');
        $(".poweredby").css("padding-bottom", '10px');
        $(".viewtest").css("text-align", 'center');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
        }
    }
    else {
        $(".poweredby").css("position", 'absolute');
        $(".poweredby").css("bottom", '10px');
        $(".poweredby").css("text-align", 'center');
        $(".poweredby").css("width", '100%');
        $(".poweredby").css("margin-top", '0px');
        $(".poweredby").css("margin-bottom", '0px');
        $(".poweredby").css("padding-bottom", '0px');
        $(".viewtest").css("position", 'absolute');
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '10px');
        }
        else {
            $(".viewtest").css("bottom", '30px');
        }
        $(".viewtest").css("padding-bottom", '0px');
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
        }
    }
}

function Showlaunch() {
    $(".poweredby").css("position", '');
    $(".viewtest").css("position", '');
    $(".viewtest").css("bottom", '');
    $(".poweredby").css("bottom", '');
    $(".poweredby").css("margin-top", '5px');
    $(".poweredby").css("padding-bottom", '10px');
  
    $("#textpollthemepreview").css("height", 'auto');
    var height = $(".textpollthemepreview").height();
    $("#textpollthemepreviewresults").css("height", 'auto');
    $("#textpollthemepreviewresults").css("height", height + 'px');
    
    $("#MainContent_txtHeightIframe").val(height);
    var heightrec = $("#MainContent_hdnRecHeight").val(); 
    var heightg = $("#MainContent_lblRecThemeHeight").text();
        $("#MainContent_hdnRecWidth").val(width);
        if (heightg == "") {
            $("#MainContent_hdnRecHeightne").val(height);
            $("#MainContent_lblHeight").text(height);
        }
        else {
            $("#MainContent_lblHeight").text(heightg);
        }
        $("#MainContent_lblWidth").text(width);
        $('.textpollthemepreview').css('height', 'auto');
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightsel = $("#MainContent_hdnSetHeight").val();
        if (heightsel == "") {
            if (fixheight > heightg) {
                $("#MainContent_txtHeightPoll").val(heightg);
                var fixheight = $("#MainContent_txtHeightPoll").val();
            }
            if (heightg > fixheight) {
                $(".textpollthemepreviewresults").css("height", heightg + 'px');
                $(".textpollthemepreview").css("height", heightg + 'px');
                $("#MainContent_txtHeightPoll").val(heightg);
                $(".poweredby").css("position", '');
                $(".viewtest").css("position", '');
                $(".viewtest").css("bottom", '');
                $(".poweredby").css("bottom", '');
                $(".poweredby").css("margin-top", '5px');
                $(".poweredby").css("padding-bottom", '10px');
                if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                    $(".viewtest").css("width", '70%');
                }
                else {
                    $(".viewtest").css("width", '100%');
                }
            }
            else {
                $(".textpollthemepreviewresults").css("height", fixheight + 'px');
                $(".textpollthemepreview").css("height", fixheight + 'px');
                $("#MainContent_txtHeightPoll").val(fixheight);
                $(".poweredby").css("position", 'absolute');
                $(".poweredby").css("bottom", '10px');
                $(".poweredby").css("text-align", 'center');
                $(".poweredby").css("width", '100%');
                $(".poweredby").css("margin-top", '0px');
                $(".poweredby").css("margin-bottom", '0px');
                $(".poweredby").css("padding-bottom", '0px');
                $(".viewtest").css("position", 'absolute');
                $(".viewtest").css("padding-bottom", '0px');
                $(".sponsorimagepreview").css("margin-top", '3px');
                if ($('#MainContent_poweredby').is(':hidden')) {
                    $(".viewtest").css("bottom", '10px');
                }
                else {
                    $(".viewtest").css("bottom", '30px');
                }
                if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                    $(".viewtest").css("width", '70%');
                }
                else {
                    $(".viewtest").css("width", '100%');
                }
            }
        }
        else {
            if (heightg > heightsel) {
                $(".textpollthemepreviewresults").css("height", heightg + 'px');
                $(".textpollthemepreview").css("height", heightg + 'px');
                $("#MainContent_txtHeightPoll").val(heightg);
                if (heightg == fixheight) {
                    $(".poweredby").css("position", 'absolute');
                    $(".poweredby").css("bottom", '10px');
                    $(".poweredby").css("text-align", 'center');
                    $(".poweredby").css("width", '100%');
                    $(".poweredby").css("margin-top", '0px');
                    $(".poweredby").css("margin-bottom", '0px');
                    $(".poweredby").css("padding-bottom", '0px');
                    $(".viewtest").css("position", 'absolute');
                    $(".viewtest").css("padding-bottom", '0px');
                    $(".sponsorimagepreview").css("margin-top", '3px');
                    if ($('#MainContent_poweredby').is(':hidden')) {
                        $(".viewtest").css("bottom", '10px');
                    }
                    else {
                        $(".viewtest").css("bottom", '30px');
                    }
                    if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                        $(".viewtest").css("width", '70%');
                    }
                    else {
                        $(".viewtest").css("width", '100%');
                    }
                }
                else
                {
                $(".poweredby").css("position", '');
                $(".viewtest").css("position", '');
                $(".viewtest").css("bottom", '');
                $(".poweredby").css("bottom", '');
                $(".poweredby").css("margin-top", '5px');
                $(".poweredby").css("padding-bottom", '10px');
                if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                    $(".viewtest").css("width", '70%');
                }
                else {
                    $(".viewtest").css("width", '100%');
                }
                }
            }
            else {
                $(".textpollthemepreviewresults").css("height", heightsel + 'px');
                $(".textpollthemepreview").css("height", heightsel + 'px');
                $("#MainContent_txtHeightPoll").val(heightsel);
                $(".poweredby").css("position", 'absolute');
                $(".poweredby").css("bottom", '10px');
                $(".poweredby").css("text-align", 'center');
                $(".poweredby").css("width", '100%');
                $(".poweredby").css("margin-top", '0px');
                $(".poweredby").css("margin-bottom", '0px');
                $(".poweredby").css("padding-bottom", '0px');
                $(".viewtest").css("position", 'absolute');
                $(".viewtest").css("padding-bottom", '0px');
                $(".sponsorimagepreview").css("margin-top", '3px');
                if ($('#MainContent_poweredby').is(':hidden')) {
                    $(".viewtest").css("bottom", '10px');
                }
                else {
                    $(".viewtest").css("bottom", '30px');
                }
                if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                    $(".viewtest").css("width", '70%');
                }
                else {
                    $(".viewtest").css("width", '100%');
                }
            }
        }
        return;
}

function setHeight() {
    var height = $(".textpollthemepreview").height();
    $("#MainContent_txtHeightIframe").val(height);
    var heightrec = $("#MainContent_hdnRecHeight").val();
    if (heightrec == "") {
        $("#MainContent_hdnRecWidth").val(width);
        $("#MainContent_hdnRecHeightne").val(height);
        $("#MainContent_lblWidth").text(width);
        $("#MainContent_lblHeight").text(height);
        return;
    }
}

function removeAnswerOption(e) {
    if (window.event) {
        e = window.event;
    }

    //target = e.srcElement ? e.srcElement : e.target;
    target = e.currentTarget;

    if ($('#MainContent_hdnPollStatus').val() == "Active") {
        $("#MainContent_lblAlreadyExist").text("Cannot delete Answer Option after launch");
        Materialize.toast("Answer Options cannot be deleted after launch", 4000);
        $("#answererr").show();
        return false;
    }
    var rows = document.getElementById("pollansweroptions").rows.length;
    if (parseInt(rows) <= 2) {
        $("#MainContent_lblAlreadyExist").text("Please enter minimum number of answer options");
        Materialize.toast("A minimum of two Answer Options are required", 4000);
        $("#answererr").show();
        return false;
    }
    else {
        $("#answererr").hide();
    }

    var ansOptionTable = document.getElementById("pollansweroptions");
    var previewtable = document.getElementById("previewansweroptions");
    var pollanswerthankyouoptions = document.getElementById("pollanswerthankyouoptions");

    var id = target.id;
    var suffix = id.match(/\d+/);
    id = parseInt(suffix);

    RearrangeAnswerOptionIdsAfterDelete(id);

    ansOptionTable.deleteRow(id);
    previewtable.deleteRow(id);

    $("#trheader_ThankYouOption_" + id + ", #trbody_ThankYouOption_" + id + "").remove();
    RearrangeAnswerThankYouOptionIdsAfterDelete(id + 1);

    $("#MainContent_hdnRepeaterTxts").val(document.getElementById("pollansweroptions").rows.length);

    renderChart();
    calculateIFrameHeight();

    return false;
}

function addAnswerOption(e) {
    if (window.event) {
        e = window.event;
    }

    //target = e.srcElement ? e.srcElement : e.target;
    target = e.currentTarget;

    if ($('#MainContent_recheightincreate').is(':hidden'))
    {
        $("#MainContent_lblAlreadyExist").text("Cannot add Answer Options after launch");
        Materialize.toast("Answer Options cannot be added after launch", 4000);
        $("#answererr").show();
        return false;
    }

    var rows = document.getElementById("pollansweroptions").rows.length;
    if (parseInt(rows) >= 10) {
        $("#MainContent_lblAlreadyExist").text("Maximum 10 Answer Options can be added");
        Materialize.toast("Answer Options cannot be more than 10", 4000);
        $("#answererr").show();
        return false;
    }
    else {
        $("#answererr").hide();
    }


    var ansOptionTable = document.getElementById("pollansweroptions");
    var previewtable = document.getElementById("previewansweroptions");
    var pollanswerthankyouoptionsTable = document.getElementById("pollanswerthankyouoptions");

    var id = target.id;
    var suffix = id.match(/\d+/);
    id = parseInt(suffix) + 1;
    console.log(target.id);
    console.log(id);
    console.log($("#pollanswerthankyouoptions tr").length);
    id = $("#previewansweroptions tr").length;
    RearrangeAnswerOptionIds(id);
    //id++;
    var previewrow = previewtable.insertRow(id);
    previewrow.className = "txtRow col m12 s12 extra-padding-bottom";

    var cellpreview1 = previewrow.insertCell(0);
    if ($("#MainContent_radMultipleAnswer").is(":checked")) {
        cellpreview1.innerHTML = '<input type="radio" id="radButton_' + id + '" name="answeroptionradio" style="position:relative !important;left:0px !important;visibility:visible;" onclick="showThankYouOption(\'' + id + '\')">';
        cellpreview1.className = "singleanswers col s2";
        $('.singleanswers').hide();
        var cellpreview1 = previewrow.insertCell(1);
        cellpreview1.innerHTML = '<input type="checkbox" id="radButton_' + id + '"  style="position:relative !important;left:0px !important;visibility:visible;"/>';
        cellpreview1.className = "multipleanswers col s2";
        $('.multipleanswers').show();
        cellpreview1 = previewrow.insertCell(2);
        cellpreview1.innerHTML = '<div id="lblradText_' + id + '"  class="wrapnew">Answer ' + (id + 1) + '</div>';
        cellpreview1.className = "col s8";
    }
    else {
        cellpreview1.innerHTML = '<input type="radio" id="radButton_' + id + '" name="answeroptionradio" style="position:relative !important;left:0px !important;visibility:visible;" onclick="showThankYouOption(\'' + id + '\')">';
        cellpreview1.className = "singleanswers col s2";
        $('.singleanswers').show();
        var cellpreview1 = previewrow.insertCell(1);
        cellpreview1.innerHTML = '<input type="checkbox" id="radButton_' + id + '"  style="position:relative !important;left:0px !important;visibility:visible;"/>';
        cellpreview1.className = "multipleanswers col s2";
        $('.multipleanswers').hide();
        cellpreview1 = previewrow.insertCell(2);
        cellpreview1.innerHTML = '<div id="lblradText_' + id + '">Answer ' + (id + 1) + '</div>';
        //cellpreview1.innerHTML = '<label id="lblradText_' + id + '"  class="wrapnew"/>';
        cellpreview1.className = "col s8";
    }
    var row = ansOptionTable.insertRow(id);
    row.className = "txtrow col m12 s12 nopad";
    var cell1 = row.insertCell(0);

    cell1.innerHTML = '<input type="hidden" id="hdnAnswerId" value="' + id + '">';
    cell1.innerHTML += '<input type="text" name="txtAnswerOption_' + id + '" id="txtAnswerOption_' + id + '" class="txtAns" onkeyup="javascript:addPreviewAnswer(\'' + id + '\');" onchange="javascript:addPreviewAnswer(\'' + id + '\');" onblur="renderChart();" maxlength="50" placeholder="Your Answer' + (id + 1) + '" length="50"/>';

    cell1.className = "col s9 nopad";

    var cell2 = row.insertCell(1);
    cell2.innerHTML = '<a href="javascript:;" id="btnRemoveAnother_' + id + '" onclick="return removeAnswerOption(event);"><i class="material-icons black-text pin-bottom">delete</i></a>';
    cell2.innerHTML += '<a href="javascript:;" id="btnAddAnother_' + id + '" onclick="return addAnswerOption(event);"><i class="material-icons black-text">add</i></a>';
    cell2.className = "input-field col s3 nopad";

    var thankyouoptionsheaderrow = pollanswerthankyouoptionsTable.insertRow((2*id));
    thankyouoptionsheaderrow.className = "txtRow col m12 s12 nopad";
    thankyouoptionsheaderrow.id = "trheader_ThankYouOption_" + id;

    var thankyouoptionsbodyrow = pollanswerthankyouoptionsTable.insertRow((2 * id) + 1);
    thankyouoptionsbodyrow.className = "txtRow col m12 s12 nopad";
    thankyouoptionsbodyrow.id = "trbody_ThankYouOption_" + id;

    var thankyouoptionheadercell = thankyouoptionsheaderrow.insertCell(0);
    var thankyouoptionbodycell = thankyouoptionsbodyrow.insertCell(0);

    thankyouoptionheadercell.innerHTML = "Option - <span ID='lblThankYouOption_" + id.toString() + "' Class='txtAns'>Answer " + id.toString() + "</span>";
    thankyouoptionheadercell.className = "col s9 nopad"
    thankyouoptionbodycell.innerHTML = "<input type='hidden' id='hdnAnswerId' value='" + id.toString() + "' /><input placeholder='Add your custom message here' type='text' id='txtThankYouOption_" + id.toString() + "' name='txtThankYouOption_" + id.toString() + "' maxlength='100' value='' length='100' onkeyup='updateThankYouOption(\"" + id + "\");'/>";
    thankyouoptionbodycell.className = "col s9 nopad"

    $("#MainContent_hdnRepeaterTxts").val(document.getElementById("pollansweroptions").rows.length);
    $("#txtAnswerOption_" + id).characterCounter();

    calculateIFrameHeight();
    renderChart();
    return true;
}

function RearrangeAnswerOptionIdsAfterDelete(id) {
    //Only Button id need to be changed since that determines the position
    if (id <= (document.getElementById("pollansweroptions").rows.length - 1)) {
        for (var i = id; i <= document.getElementById("pollansweroptions").rows.length - 1; i++) {
            var newId = parseInt(i) - 1;
            document.getElementById('btnAddAnother_' + i).id = "btnAddAnother_" + newId; 
            document.getElementById('btnRemoveAnother_' + i).id = "btnRemoveAnother_" + newId;
            document.getElementById('txtAnswerOption_' + i).setAttribute("name", "txtAnswerOption_" + newId);
            document.getElementById('txtAnswerOption_' + i).id = "txtAnswerOption_" + newId;
            document.getElementById('lblradText_' + i).id = "lblradText_" + newId;

            var element = document.getElementById('radButton_' + i);
            if (typeof (element) != 'undefined' && element != null) {
                document.getElementById('radButton_' + i).id = "radButton_" + newId;
            }
            else {
                    
            }
            element = document.getElementById('ckbButton_' + i);
            if (typeof (element) != 'undefined' && element != null) {
                document.getElementById('ckbButton_' + i).id = "ckbButton_" + newId;
            }
            else {
                
            }
        }
    }
}

function RearrangeAnswerThankYouOptionIdsAfterDelete(id) {
    //Only Button id need to be changed since that determines the position
    try {
        if (id <= (document.getElementById("pollanswerthankyouoptions").rows.length - 1)) {
            for (var i = id; i <= document.getElementById("pollanswerthankyouoptions").rows.length - 1; i++) {
                var newId = parseInt(i) - 1;
                if (newId < 0) newId = 0;
                console.log(id.toString() + "_" + i.toString() + "_" + newId.toString());
                document.getElementById('trheader_ThankYouOption_' + i).id = "trheader_ThankYouOption_" + newId;
                document.getElementById('trbody_ThankYouOption_' + i).id = "trbody_ThankYouOption_" + newId;
                document.getElementById('txtThankYouOption_' + i).setAttribute("name", "txtThankYouOption_" + newId);
                document.getElementById('txtThankYouOption_' + i).id = "txtThankYouOption_" + newId;
            }
        }

    } catch (e) {

    }
}

function RearrangeAnswerOptionIds(id) {
//Only Button id need to be changed since that determines the position

    if (id <= (document.getElementById("pollansweroptions").rows.length- 1)) {
        for (var i = document.getElementById("pollansweroptions").rows.length - 1; i >= id; i--) {
            var newId = parseInt(i) + 1;
            document.getElementById('btnAddAnother_' + i).id = "btnAddAnother_" + newId;
            document.getElementById('btnRemoveAnother_' + i).id = "btnRemoveAnother_" + newId;
            document.getElementById('txtAnswerOption_' + i).setAttribute("name", "txtAnswerOption_" + newId);
            document.getElementById('txtAnswerOption_' + i).id = "txtAnswerOption_" + newId;
            document.getElementById('lblradText_' + i).id = "lblradText_" + newId;
            var element = document.getElementById('radButton_' + i);
            if (typeof (element) != 'undefined' && element != null) {
                document.getElementById('radButton_' + i).id = "radButton_" + newId;
            }
            else {
                
            }
            element = document.getElementById('ckbButton_' + i);
            if (typeof (element) != 'undefined' && element != null) {
                document.getElementById('ckbButton_' + i).id = "ckbButton_" + newId;
            }
            else {
                
            }
        }
    }
}

function ShowDefaultThemes() {
    $("#customthemes").hide();
    $("#MainContent_defaultthemes").show();
    $(".PollDivsub a").css("color", "#7D2AC7");
    $(".PollDivsub a").css("font-weight", "bold");
    $(".PollDivsubinner a").css("color", "#948E8E");
    $(".PollDivsubinner a").css("font-weight", "normal");
}

function ShowCustomThemes() {
    $("#customthemes").show();  
    $("#MainContent_defaultthemes").hide();
    $(".PollDivsubinner a").css("color", "#7D2AC7");
    $(".PollDivsubinner a").css("font-weight", "bold");
    $(".PollDivsub a").css("color", "#948E8E");
    $(".PollDivsub a").css("font-weight", "normal");
}

function showSocialMediaDiv() {

    if (($("#MainContent_ckbGoogle").is(":checked")) || ($("#MainContent_ckbTwitter").is(":checked")) || ($("#MainContent_ckbFb").is(":checked"))) {
        $("#sponsorandsocialmediainresults").show();
        $("#sponsorandsocialmediainleadgen").show();
        $(".sponsorsocialmediamain").show();
        $("#MainContent_social").show();
        $("#MainContent_socialsharethisleadgen").show();
        $("#MainContent_lblShare").css("font-family", $("#MainContent_ddlAnswerFont").val());
    }
    else {
        $("#sponsorandsocialmediainresults").hide();
        $("#sponsorandsocialmediainleadgen").hide();
        $(".sponsorsocialmediamain").hide();
        $("#socialsharethisleadgen").hide();
        $("#MainContent_socialsharethisleadgen").hide();
    }
    showSponsorDetails();
    calculateIFrameHeight();
}

function showGoogletxt() {
    showSocialMediaDiv();
    if ($("#MainContent_ckbGoogle").is(":checked")) {
        $("#MainContent_googleicon").show();
        $("#MainContent_googleiconleadgen").show();
        $(".sponsorsocialmediamain").show();
        var count = 0;
        if ($("#MainContent_ckbTwitter").is(":checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").is(":checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_googleiconvote").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").is(":checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
    }
    else {
        $("#MainContent_twittericonvote").hide();
        $("#MainContent_twitter").hide();
        $("#MainContent_twittericon").hide();
        $("#MainContent_twittericonleadgen").hide();
        var count = 0;
        if ($("#MainContent_ckbTwitter").is(":checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").is(":checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            $("#MainContent_googleiconvote").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").is(":checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            if (count == 0) {
                $("#MainContent_socialsha").hide();
                $('.newvotecount').css({ 'position': '' });
            }
        }
        else {
            if (count == 0) {
                $("#MainContent_socialsha").hide();
            }
        }
    }

    calculateIFrameHeight();
}

function showTwittertxt() {
    showSocialMediaDiv();
    if ($("#MainContent_ckbTwitter").is(":checked")) {
        $("#MainContent_twitter").show();
        $("#MainContent_twittericon").show();
        $("#MainContent_twittericonleadgen").show();
        $("#MainContent_twittericonvote").show();
        $("#MainContent_social").show();
        $("#MainContent_socialsha").show();

        var count = 0;
        if ($("#MainContent_ckbTwitter").is(":checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").is(":checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_googleiconvote").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").is(":checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
    }
    else {
        $("#MainContent_twittericonvote").hide();
        $("#MainContent_twitter").hide();
        $("#MainContent_twittericon").hide();
        $("#MainContent_twittericonleadgen").hide();
        var count = 0;
        if ($("#MainContent_ckbTwitter").is(":checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").is(":checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            $("#MainContent_googleiconvote").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").is(":checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            if (count == 0) {
                $("#MainContent_socialsha").hide();
                $('.newvotecount').css({ 'position': '' });
            }
        }
        else {
            if (count == 0) {
                //$('.sponsorimagepreviewres').css({ 'width': '100%' });
                //$('.sponsorimagepreviewres').css({ 'margin': 'auto' });
                //$('.sponsorimagepreviewres').css({ 'margin-right': '0px' });
                 $("#MainContent_socialsha").hide();
                }
        }
    }

    calculateIFrameHeight();
}

function voteShow() {
     if ($("#MainContent_ckbVotes").is(":checked")) {
        
         var votesindex = $("#MainContent_lblQuestion").text().indexOf('Votes');
         //  $("#MainContent_votescount").show();
         if (votesindex == '-1') {
             $("#MainContent_lblQuestion").text($("#MainContent_lblQuestion").text());
             $("#MainContent_lblQuestionResults").text($("#MainContent_lblQuestionResults").text() + '(39 votes)');
         }
         else {
             $("#MainContent_lblQuestion").text($("#MainContent_lblQuestion").text());
             $("#MainContent_lblQuestionResults").text($("#MainContent_lblQuestionResults").text());
         }
     }
     else {
       
         // $("#MainContent_votescount").hide();
         $("#MainContent_lblQuestion").text($("#MainContent_lblQuestion").text());
         $("#MainContent_lblQuestionResults").text($("#MainContent_lblQuestionResults").text().replace('(39 votes)', ''));
     }
 }

function showFbtxt() {
    showSocialMediaDiv();
    if ($("#MainContent_ckbFb").is(":checked")) {
        $("#MainContent_fb").show();
        $("#MainContent_fbicon").show();
        $("#MainContent_fbiconleadgen").show();
        $("#MainContent_fbiconvote").show();
        $("#MainContent_social").show();
        $("#MainContent_socialsha").show();
        //$(".sponsorimagepreviewres").css("margin-right", '2%');
        //$(".sponsorimagepreviewres").css("width", '31%');
        var count = 0;
        if ($("#MainContent_ckbTwitter").is(":checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").is(":checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            $("#MainContent_googleiconvote").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").is(":checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
    }
    else {
        $("#MainContent_fb").hide();
        $("#MainContent_fbicon").hide();
        $("#MainContent_fbiconleadgen").hide();
        $("#MainContent_fbiconvote").hide();
        var count = 0;
        if ($("#MainContent_ckbTwitter").is(":checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").is(":checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            $("#MainContent_googleiconvote").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").is(":checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            if (count == 0) {
                $("#MainContent_socialsha").hide();
                $('.newvotecount').css({ 'position': '' });
            }
        }
        else {
            if (count == 0) {
                //$('.sponsorimagepreviewres').css({ 'width': '100%' });
                //$('.sponsorimagepreviewres').css({ 'margin': 'auto' });
                //$('.sponsorimagepreviewres').css({ 'margin-right': '0px' });
                $("#MainContent_socialsha").hide();
            }
        }
    }
    calculateIFrameHeight();
}

function generatechartRemove(id) {
    //    var chart_pollresponsechart41 = new FusionCharts({ "dataFormat": "xml", "scaleMode": "noScale", "renderAt": "pollresponsechart41Div", "id": "pollresponsechart41", "debugMode": "0", "lang": "EN", "swfUrl": "../Charts/Bar2D.swf", "wMode": "opaque", "width": "100%", "height": "40", "registerWithJS": "1", "dataSource": "<chart caption='' subcaption='' maxLabelWidthPercent='20'   LogoURL='http://127.0.0.1:8010/Polls/images/Insighto_logo.png' logoPosition='CC' logoAlpha='30' logoScale='100' chartTopMargin='0' chartBottomMargin='0' chartLeftMargin='5' yaxisname='' borderAlpha='0' canvasbgAlpha='0' numDivLines='0' divLineAlpha='0' showYAxisValues='0' showXAxisValues='0' divLineColor='#FFFFFF'  labelDisplay='' showvalues='1' xaxisname='' alternatevgridcolor='#FFFFFF'  useroundedges='1'  showborder='1' bgcolor='#FFFFFF' numbersuffix='%' baseFont='Arial' baseFontSize ='12'  baseFontColor ='000000' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'><set label='adfasdas' value='40' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles><set label='"+id+"' value='60' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles></chart>" }).render();

    var chartstr = "<chart caption='' subcaption='' maxLabelWidthPercent='20'   LogoURL='http://insighto.com:8010/Polls/images/Insighto_logo.png' logoPosition='CC' logoAlpha='30' logoScale='100' chartTopMargin='0' chartBottomMargin='0' chartLeftMargin='5' yaxisname='' borderAlpha='0' canvasbgAlpha='0' numDivLines='0' divLineAlpha='0' showYAxisValues='0' showXAxisValues='0' divLineColor='#FFFFFF'  labelDisplay='' showvalues='1' xaxisname='' alternatevgridcolor='#FFFFFF'  useroundedges='1'  showborder='1' bgcolor='#FFFFFF' numbersuffix='%' baseFont='Arial' baseFontSize ='12'  baseFontColor ='000000' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>";
    var setlbl = "";
    var strhgt = 20 * id;

    if (id == 2) {
        var myArray1 = ['40', '60'];
    }
    else if (id == 3) {
        var myArray1 = ['20', '30', '50'];
    }
    else if (id == 4) {
        var myArray1 = ['10', '20', '30', '40'];
    }
    else if (id == 5) {
        var myArray1 = ['10', '15', '20', '25', '30'];
    }
    else if (id == 6) {
        var myArray1 = ['5', '10', '15', '20', '22','28'];
    }
    else if (id == 7) {
        var myArray1 = ['5', '10', '12', '13', '17', '20','23'];
    }
    else if (id == 8) {
        var myArray1 = ['5', '7', '9', '11', '13', '15', '19','21'];
    }
    else if (id == 9) {
        var myArray1 = ['1', '3', '6', '9', '10', '14', '17', '19','21'];
    }
    else if (id == 10) {
        var myArray1 = ['1', '3', '5', '7', '9', '11', '13', '15', '17','19'];
    }
    for (i = 0; i < id; i++) {
        var currelement1 = myArray1[i];
    //  alert(currelement1);
        var colint1 = i+1;
        setlbl += "<set label='Answer" + colint1 + "' value='" + currelement1 + "' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles>";
    }
    chartstr = chartstr + setlbl;
    chartstr = chartstr + "</chart>";

   // alert(chartstr);
    //var chart_pollresponsechart = new FusionCharts({ "dataFormat": "xml", "scaleMode": "noScale", "renderAt": "pollresponsechartDiv", "id": "pollresponsechart", "debugMode": "0", "lang": "EN", "swfUrl": "../Charts/Bar2D.swf", "wMode": "opaque", "width": "100%", "height": strhgt, "registerWithJS": "1", "dataSource": chartstr }).render();
}

function generatechart(id) {
    //    var chart_pollresponsechart41 = new FusionCharts({ "dataFormat": "xml", "scaleMode": "noScale", "renderAt": "pollresponsechart41Div", "id": "pollresponsechart41", "debugMode": "0", "lang": "EN", "swfUrl": "../Charts/Bar2D.swf", "wMode": "opaque", "width": "100%", "height": "40", "registerWithJS": "1", "dataSource": "<chart caption='' subcaption='' maxLabelWidthPercent='20'   LogoURL='http://127.0.0.1:8010/Polls/images/Insighto_logo.png' logoPosition='CC' logoAlpha='30' logoScale='100' chartTopMargin='0' chartBottomMargin='0' chartLeftMargin='5' yaxisname='' borderAlpha='0' canvasbgAlpha='0' numDivLines='0' divLineAlpha='0' showYAxisValues='0' showXAxisValues='0' divLineColor='#FFFFFF'  labelDisplay='' showvalues='1' xaxisname='' alternatevgridcolor='#FFFFFF'  useroundedges='1'  showborder='1' bgcolor='#FFFFFF' numbersuffix='%' baseFont='Arial' baseFontSize ='12'  baseFontColor ='000000' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'><set label='adfasdas' value='40' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles><set label='"+id+"' value='60' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles></chart>" }).render();


    var chartstr = "<chart caption='' subcaption='' maxLabelWidthPercent='20'   LogoURL='http://insighto.com:8010/Polls/images/Insighto_logo.png' logoPosition='CC' logoAlpha='30' logoScale='100' chartTopMargin='0' chartBottomMargin='0' chartLeftMargin='5' yaxisname='' borderAlpha='0' canvasbgAlpha='0' numDivLines='0' divLineAlpha='0' showYAxisValues='0' showXAxisValues='0' divLineColor='#FFFFFF'  labelDisplay='' showvalues='1' xaxisname='' alternatevgridcolor='#FFFFFF'  useroundedges='1'  showborder='1' bgcolor='#FFFFFF' numbersuffix='%' baseFont='Arial' baseFontSize ='12'  baseFontColor ='000000' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>";
    var setlbl = "";
    var strhgt = 20 * id;

    var noofcols = id;
    if (noofcols == 2) {
        var myArray = ['40', '60'];
    }
    else if (noofcols == 3) {
        var myArray = ['20', '30', '50'];
    }
    else if (noofcols == 4) {
        var myArray = ['10', '20', '30', '40'];
    }
    else if (noofcols == 5) {
        var myArray = ['10', '15', '20', '25', '30'];
    }
    else if (noofcols == 6) {
        var myArray = ['5', '10', '15', '20', '22', '28'];
    }
    else if (noofcols == 7) {
        var myArray = ['5', '10', '12', '13', '17', '20', '23'];
    }
    else if (noofcols == 8) {
        var myArray = ['5', '7', '9', '11', '13', '15', '19', '21'];
    }
    else if (noofcols == 9) {
        var myArray = ['1', '3', '6', '9', '10', '14', '17', '19', '21'];
    }
    else if (noofcols == 10) {
        var myArray = ['1', '3', '5', '7', '9', '11', '13', '15', '17', '19'];
    }

    for (i = 0; i < noofcols; i++) {
        var currelement = myArray[i];
      //  alert(currelement);
        var colint = i + 1;
        setlbl += "<set label='Answer" + colint + "' value='" + currelement + "' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles>";
    }
    chartstr = chartstr + setlbl;
    chartstr = chartstr + "</chart>";

  //  alert(chartstr);
   // var chart_pollresponsechart = new FusionCharts({ "dataFormat": "xml", "scaleMode": "noScale", "renderAt": "pollresponsechartDiv", "id": "pollresponsechart", "debugMode": "0", "lang": "EN", "swfUrl": "../Charts/Bar2D.swf", "wMode": "opaque", "width": "100%", "height": strhgt, "registerWithJS": "1", "dataSource": chartstr }).render();
}

function getAnswerOptionsHeight() {
    var chartanswerrowcount = parseInt($("#MainContent_hdnRepeaterTxts").val());
    var chartanswerrowcountnew = 0;
    for (var row = 0; row < chartanswerrowcount; row++) {
        if (document.getElementById('txtAnswerOption_' + row) != null) {
            chartanswerrowcountnew++;
        }
    }
    var chartheight = chartanswerrowcountnew * 5;
    return chartheight;
}

function stripSpecialChars(inputval) {
    while (inputval.indexOf("<") >= 0) {
        inputval = inputval.replace("<", "&lt;");
    }
    while (inputval.indexOf(">") >= 0) {
        inputval = inputval.replace(">", "&gt;");
    }
    while (inputval.indexOf("'") >= 0) {
        inputval = inputval.replace("'", "`");
    }
    return inputval;
}

function checkRowsForHeightAdjustment(numberofrows) {
    var extraspacing = 0;
    if (numberofrows >= 5) {
        singleansweroptionheight = 25;
    }
    else if (numberofrows >= 4) {
        singleansweroptionheight = 12;
    }
    else {
        singleansweroptionheight = 5;
    }

    if ((!$('#MainContent_SponsorBox').is(':hidden')) || ($("#MainContent_ckbGoogle").is(":checked")) || ($("#MainContent_ckbFb").is(":checked")) || ($("#MainContent_ckbTwitter").is(":checked"))) {
        if (!$('#MainContent_commentbox').is(':hidden')) {
            if (numberofrows >= 5) {
                extraspacing = 15;
            }
            else if (numberofrows >= 4) {
                extraspacing = 5;
            }
            else {
                extraspacing = 0;
            }
        }
        else {
            if (numberofrows >= 5) {
                extraspacing = 25;
            }
            else if (numberofrows >= 4) {
                extraspacing = 35;
            }
            else {
                extraspacing = 50;
            }
        }
        singleansweroptionheight = singleansweroptionheight + extraspacing;
    }

}

function checkLogoAndSponsorForHeightAdjustment() {
    var numberofrows = parseInt($("#MainContent_hdnRepeaterTxts").val());
    if (($('#MainContent_ckbAddSponsor').is('checked')) || ($("#MainContent_ckbGoogle").is(":checked")) || ($("#MainContent_ckbFb").is(":checked")) || ($("#MainContent_ckbTwitter").is(":checked"))) {
        if ((!$('#MainContent_commentbox').is(':hidden')) && (numberofrows >= 5)) {
            sponsorlogooptionheight = 10;
        }
        else if ((!$('#MainContent_commentbox').is(':hidden')) && (numberofrows < 5)) {
            sponsorlogooptionheight = 0;
        }
    }
}

function checkCommentsBoxHeightAdjustment() {
    var numberofrows = parseInt($("#MainContent_hdnRepeaterTxts").val());
    if (($('#MainContent_ckbAddSponsor').is('checked')) || ($("#MainContent_ckbGoogle").is(":checked")) || ($("#MainContent_ckbFb").is(":checked")) || ($("#MainContent_ckbTwitter").is(":checked")) || (numberofrows > 6)) {
        commentboxheight = 0;
    }
}

function checkSocialMediaHeightAdjustment() {
    if ((!$('#MainContent_ckbAddSponsor').is('checked')) && ($('#MainContent_commentbox').is(':hidden'))) {
        var fixheight = parseInt($("#MainContent_txtHeightPoll").val());
        var heightrec = parseInt($("#MainContent_hdnRecHeightne").val());
        heightrec = heightrec + sponsorlogooptionheight;
        if (heightrec > fixheight) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
    }
    else {
    }
}

var iframeheight = 0;
var iframeheightwithchart = 0;
var iframewidth = 0;
var recheighttoastshowing = false;
function calculateIFrameHeight() {
    var numberofrows = parseInt($("#MainContent_hdnRepeaterTxts").val());
    var polltype = $("#MainContent_hdnPollType").val();
    var answerrowheight = 35;
    var chartrowheight = 45;
    var sponsorheight = 50;
    var socialmediaheight = 50;
    var poweredbyheight = 40;
    var commentbox = 70;
    var polltypeheight = 0;
    var votebuttonheight = 80;
    var bottompadding = 0;
    var imageheight = 0;
    var videoheight = 0;
    var videolink = $("#MainContent_txtVideo").val();
    var leadgenheight = $("#pollleadgencontent").css("height");
    var invitationmessage = $("#MainContent_invitationmessagepreview").css("line-height");
    var leadgensubmitbutton = 90;
    var leadgenphone = 80;
    var leadgenemail = 80;
    var contactdetailstop = 0;
    var submitbuttontop = 0;
    var invitemessagetop = 0;
    iframeheight = 0;
    iframeheightwithchart = 0;
    
    if (!isNaN(parseInt($("#MainContent_txtWidthPoll").val()))) {
        iframewidth = parseInt($("#MainContent_txtWidthPoll").val());
    }
    else if (!isNaN(parseInt($("#MainContent_txtWidthIframe1").val()))) {
        iframewidth = parseInt($("#MainContent_txtWidthIframe1").val());
    }
    else if (!isNaN(parseInt($("#MainContent_txtWidthIframe").val()))) {
        iframewidth = parseInt($("#MainContent_txtWidthIframe").val());
    }
    
    if (polltype == "video") {
        videoheight = parseInt($('#MainContent_videopreviewcreate').css("height"));
        if (videoheight == 0) {
            videoheight = 168;
        }
        answerrowheight = 38;
    }
    else if (polltype == "image") {

        imageheight = 124;//parseInt($('#MainContent_imgPreview').css("height"));
        answerrowheight = 40;
    }

    ////console.log(iframeheightwithchart);
    if (($('#MainContent_ckbAddSponsor').is(':checked')) || ($("#MainContent_ckbGoogle").is(":checked")) || ($("#MainContent_ckbFb").is(":checked")) || ($("#MainContent_ckbTwitter").is(":checked"))) {
        chartrowheight = 60;
    }
    iframeheight = ((numberofrows) * answerrowheight);
    //console.log(iframeheightwithchart);
    if ($('#MainContent_radShowResults').is(':checked')) {
        iframeheightwithchart = ((numberofrows) * chartrowheight);
    }
    else {
        if (! $("#pollleadgencontent").is(":hidden")) {
            if (! $("#invitationmessagepreview").is(":hidden")) {
                invitemessagetop = $("#MainContent_invitationmessagepreview").offset().top;
            }
            if ($("#MainContent_chkPhone").is(":checked")) {
                iframeheightwithchart = iframeheightwithchart + leadgenphone;
                contactdetailstop = $("#MainContent_contactdetailscollectiondiv").offset().top;
            }
            if ($("#MainContent_chkEmail").is(":checked")) {
                iframeheightwithchart = iframeheightwithchart + leadgenemail;
                contactdetailstop = $("#MainContent_contactdetailscollectiondiv").offset().top;
            }
            if ($("#MainContent_ckbSubmit").is(":checked")) {
                iframeheightwithchart = iframeheightwithchart + leadgensubmitbutton;
                submitbuttontop = $("#submitbuttonpreviewdiv").offset().top;
            }
            if (contactdetailstop > 0) {
                invitationmessage = contactdetailstop - invitemessagetop;
            }
            else {
                invitationmessage = submitbuttontop - invitemessagetop;
            }
            //console.log($("#MainContent_invitationmessagepreview").offset());
            //console.log($("#MainContent_invitationmessagepreview").position());
            //console.log($("#MainContent_invitationmessagepreview").innerHeight());
            console.log(invitemessagetop.toString() + "__" + contactdetailstop.toString() + "__" + submitbuttontop.toString());
            iframeheightwithchart = iframeheightwithchart + parseInt(invitationmessage);
        }
    }
    //console.log("CHART FRAME HEIGHT AFTER" + iframeheightwithchart);


    if (($('#MainContent_ckbAddSponsor').is(':checked')) || ($("#MainContent_ckbGoogle").is(":checked")) || ($("#MainContent_ckbFb").is(":checked")) || ($("#MainContent_ckbTwitter").is(":checked"))) {
        iframeheightwithchart = iframeheightwithchart + sponsorheight;
        if (numberofrows <= 3) {
            if ((!$('#MainContent_commentbox').is(':hidden')) && ($('#MainContent_ckbAddSponsor').is(':checked')))
            {
                iframeheight = iframeheight + 30;
                //iframeheightwithchart = iframeheightwithchart + sponsorheight;
            }
            else if ($('#MainContent_ckbAddSponsor').is(':checked'))
            {
                iframeheight = iframeheight + sponsorheight;
                //iframeheightwithchart = iframeheightwithchart + sponsorheight;
            }
        }
    }


    if (($('#MainContent_chkComments').is(':checked'))) {
        iframeheight = iframeheight + commentbox;
    }

    iframeheight = iframeheight + votebuttonheight + bottompadding;

    iframeheight = iframeheight + poweredbyheight;

    iframeheightwithchart = iframeheightwithchart + imageheight + videoheight;
    iframeheight = iframeheight + imageheight + videoheight;

    $("#previewansweroptions").css("height", (numberofrows * answerrowheight).toString() + "px")
    $("#previewansweroptions").css("margin-top", "-5px")
    //console.log($("#previewansweroptions").offset().top);
    //console.log($("#MainContent_commentbox").offset().top);
    //console.log("ROWS: " + numberofrows.toString() + " ROW HEIGHT: " + answerrowheight.toString() + " CHART ROW HEIGHT: " + chartrowheight.toString());
    //console.log("COMMENT HEIGHT: " + commentboxheight.toString() + " POWERED BY: " + poweredbyheight.toString());
    //console.log("BUTTON HEIGHT: " + votebuttonheight.toString() + " PADDING: " + bottompadding.toString());
    //console.log("IMAGE HEIGHT: " + imageheight.toString() + " VIDEO HEIGHT: " + videoheight.toString());
    //console.log("SPONSOR HEIGHT: " + sponsorheight.toString());
    //console.log("FRAME HEIGHT ANSWERS: " + iframeheight.toString() + " FRAME HEIGHT CHART: " + iframeheightwithchart.toString());

    if (!$('#MainContent_radLeadGen').is(':checked')) {
        //iframeheightwithchart = 0;
    }
    if (iframeheight < iframeheightwithchart) {
        iframeheight = iframeheightwithchart;
    }
    var fixheight = parseInt($("#MainContent_txtHeightPoll").val());

    $("#MainContent_lblRecThemeHeight").text(iframeheight);
    $("#MainContent_hdnRecHeightne").val(iframeheight);
    $("#MainContent_lblRecHeightbPre").html(iframeheight);
    $("#MainContent_lblHeight").html(iframeheight);
    $("#MainContent_lblHeight1").html(iframeheight);
    $("#MainContent_lblRecThemeWidth").html(iframewidth);
    $("#MainContent_lblWidth").html(iframewidth);
    $("#MainContent_lblWidth1").html(iframewidth);
    if (($("#MainContent_ckbAutoAdjust").is(":checked")) || ($("#MainContent_ckbAutoAdjustDesign").is(":checked"))) {
        autoAdjustPreviews();
        $("#MainContent_showrec").hide();
    }
    else
    {
        console.log(recheighttoastshowing);
        if ((fixheight < iframeheight) && (recheighttoastshowing == false)) {
            recheighttoastshowing = true;
            Materialize.toast("Recommended height of the poll is " + iframeheight + "px", 3000, "", function () { recheighttoastshowing = false; });
        }
        else {
            $("#MainContent_showrec").hide();
        }
    }
}

function setOverallPreviewHeight() {
    var pollpreviewheight = parseInt($("#MainContent_txtHeightPoll").val());
    var pollpreviewheightmain = pollpreviewheight + 60;
    $(".textpollthemepreview").css("height", pollpreviewheightmain + "px");
    $(".textpollthemepreviewresults").css("height", pollpreviewheightmain + "px");
}

function setOverallPreviewHeight1() {
    var pollpreviewheight = parseInt($("#MainContent_txtHeightIframe").val());
    var pollpreviewheightmain = pollpreviewheight + 60;
    $(".textpollthemepreviewmain").css("height", pollpreviewheightmain + "px");
    $(".textpollthemepreviewresultsmain").css("height", pollpreviewheightmain + "px");
}

function previewWebLink() {
    var url = $("#MainContent_txtWebLink").val();
    window.open(url, "_blank", "", "")
}

function autoAdjustPreviews() {
    var widthpoll = $("#MainContent_lblRecThemeWidth").html();
    var heightpoll = $("#MainContent_lblRecThemeHeight").html();

    $("#MainContent_txtWidthPoll").val(widthpoll);
    $("#MainContent_txtHeightPoll").val(heightpoll);
    $("#MainContent_txtWidthIframe").val(widthpoll);
    $("#MainContent_txtHeightIframe").val(heightpoll);

    $(".textpollthemepreview").css("min-width", widthpoll + 'px');
    $(".textpollthemepreviewresults").css("min-width", widthpoll + 'px');
    $(".textpollthemepreviewresults").css("min-width", widthpoll + 'px');
    
    $("#pollpreviewmaindiv").css("min-width", widthpoll + 'px');
    $("#pollresultspreviewmaindiv").css("min-width", widthpoll + 'px');
    $("#pollpreviewheaderdiv").css("min-width", widthpoll + 'px');
    $("#pollpreviewresultsheaderdiv").css("min-width", widthpoll + 'px');
    $("#pollleadgenpreviewmain").css("min-width", widthpoll + 'px');

    $("#pollpreviewmaindiv").css("width", widthpoll + 'px');
    $("#pollresultspreviewmaindiv").css("width", widthpoll + 'px');
    $("#pollpreviewheaderdiv").css("width", widthpoll + 'px');
    $("#pollpreviewresultsheaderdiv").css("width", widthpoll + 'px');
    $("#pollleadgenpreviewmain").css("width", widthpoll + 'px');

    $("#pollpreviewmaindiv").css("max-width", widthpoll + 'px');
    $("#pollresultspreviewmaindiv").css("max-width", widthpoll + 'px');
    $("#pollpreviewheaderdiv").css("max-width", widthpoll + 'px');
    $("#pollpreviewresultsheaderdiv").css("max-width", widthpoll + 'px');
    $("#pollleadgenpreviewmain").css("max-width", widthpoll + 'px');

    $(".textpollthemepreview").css("min-height", heightpoll + 'px');
    $(".textpollthemepreviewresults").css("min-height", heightpoll + 'px');
    $(".textpollthemepreview").css("height", heightpoll + 'px');
    $(".textpollthemepreviewresults").css("height", heightpoll + 'px');
    $("#pollleadgenpreviewmain").css("min-height", heightpoll + 'px');

}

function autoAdjust(page) {
    var adjustchecked = false;
    if (page == "create") {
        if ($("#MainContent_ckbAutoAdjust").is(":checked")) {
            $("#MainContent_ckbAutoAdjustDesign").prop("checked", true);
            adjustchecked = true;
        }
        else {
            $("#MainContent_ckbAutoAdjustDesign").prop("checked", false);
        }
    }
    else if (page == "design") {
        if ($("#MainContent_ckbAutoAdjustDesign").is(":checked")) {
            $("#MainContent_ckbAutoAdjust").prop("checked", true);
            adjustchecked = true;
        }
        else {
            $("#MainContent_ckbAutoAdjust").prop("checked", false);
        }
    }
    calculateIFrameHeight();
    $("#MainContent_txtHeightPoll").prop("readonly", adjustchecked);
    $("#MainContent_txtHeightIframe").prop("readonly", adjustchecked);
    if (adjustchecked) {
        $("#MainContent_txtHeightPoll").attr("readonly", "readonly");
        $("#MainContent_txtHeightIframe").attr("readonly", "readonly");
    }
    else {
        $("#MainContent_txtHeightPoll").removeAttr("readonly");
        $("#MainContent_txtHeightIframe").removeAttr("readonly");
    }
}

function autoAdjustOnLoad() {
    var adjustchecked = false;
    if ($("#MainContent_ckbAutoAdjust").is(":checked") || $("#MainContent_ckbAutoAdjustDesign").is(":checked")) {
        $("#MainContent_ckbAutoAdjustDesign").prop("checked", true);
        $("#MainContent_ckbAutoAdjust").prop("checked", true);
    }
    else {
        $("#MainContent_ckbAutoAdjust").prop("checked", false);
        $("#MainContent_ckbAutoAdjustDesign").prop("checked", false);
        adjustchecked = true;
    }

    calculateIFrameHeight();
    $("#MainContent_txtHeightPoll").prop("readonly", adjustchecked);
    $("#MainContent_txtHeightIframe").prop("readonly", adjustchecked);
    if (adjustchecked) {
        $("#MainContent_txtHeightPoll").attr("readonly", "readonly");
        $("#MainContent_txtHeightIframe").attr("readonly", "readonly");
        $(".textpollthemepreview").css("height", $("#MainContent_lblRecThemeHeight").html() + "px");
        $(".textpollthemepreviewresults").css("height", $("#MainContent_lblRecThemeHeight").html() + "px");
    }
    else {
        $("#MainContent_txtHeightPoll").removeAttr("readonly");
        $("#MainContent_txtHeightIframe").removeAttr("readonly");
    }
}

$("#tabcreate, #tabdesign, #tablaunch").on("click", function () {
    var pollbgcolor = $("#MainContent_txtPollBgColor").val();

    var questioncolor = $("#MainContent_txtQuestionColor").val();
    var questionbgcolor = $("#MainContent_txtQuestionBgColor").val();
    var questionfont = $("#MainContent_ddlQuestionFont").val();
    var questionfontweight = $("#MainContent_hdnQuestionBold").val();
    var questionfontsize = $("#MainContent_hdnQuestionFSize").val();
    var questionunderline = $("#MainContent_hdnQuestionUL").val();
    var questionalign = "left";
    if ($("#MainContent_hdnQuestionAlign").val() != "") {
        questionalign = $("#MainContent_hdnQuestionAlign").val();
    }
    var questionstyle = $("#hdnQuestionItalic").val();

    var answercolor = $("#MainContent_txtAnswerColor").val();
    var answerfont = $("#MainContent_ddlAnswerFont").val();
    var answerfontweight = $("#MainContent_hdnAnswerBold").val();
    var answerfontsize = $("#MainContent_hdnAnswerFSize").val();
    var answerunderline = $("#MainContent_hdnAnswerUL").val();
    var answeralign = "left";
    if ($("#MainContent_hdnAnswerAlign").val() != "") {
        questionalign = $("#MainContent_hdnAnswerAlign").val();
    }
    var answerstyle = $("#MainContent_hdnAnswerItalic").val();

    var votebgcolor = $("#MainContent_txtVoteBgColor").val();
    var votecolor = $("#MainContent_txtVoteColor").val();
    var votestyle = $("#MainContent_hdnVoteItalic").val();
    var votefont = $("#MainContent_ddlVoteFont").val();
    var votefontweight = $("#MainContent_hdnVoteBold").val();
    var votefontsize = $("#MainContent_hdnVoteFSize").val();
    var voteunderline = $("#MainContent_hdnVoteUL").val();
    var votealign = "left";
    if ($("#MainContent_hdnVoteAlign").val() != "") {
        votealign = $("#MainContent_hdnVoteAlign").val();
    }

    var sponsorlogo = $("#MainContent_lblLogoName").val();
    var sponsortext = $("#MainContent_txtSponsorText").val();
    var logolink = $("#MainContent_txtSponsorLink").val();

    var insightobrand = "yes";

    var messagecolor = $("#MainContent_txtMessageColor").val();
    var messagebgcolor = $("#MainContent_txtMessageBgColor").val();
    var messagestyle = $("#MainContent_hdnMsgItalic").val();
    var messagefont = $("#MainContent_ddlMsgFont").val();
    var messagefontweight = $("#MainContent_hdnMsgBold").val();
    var messagefontsize = $("#MainContent_hdnMsgFSize").val();
    var messageunderline = $("#MainContent_hdnMsgUL").val();
    var messagealign = "left";
    if ($("#MainContent_hdnMsgAlign").val() != "") {
        messagealign = $("#MainContent_hdnMsgAlign").val();
    }


    var thisiframewidth = 0;//iframewidth
    var thisiframeheight = 0;//iframeheight
    var googletext = "";
    var twittertext = "";
    var fbtext = "";

    if ($("#MainContent_ckbGoogle").is(":checked")) {
        googletext = "google";
    }

    if ($("#MainContent_ckbTwitter").is(":checked")) {
        twittertext = $("#MainContent_txtTwitter").val();
    }

    if ($("#MainContent_ckbFb").is(":checked")) {
        fbtext = "fb";
    }

    var countvotes = 0;
    var sharetext = $("#MainContent_txtSharePoll").val();
    var votetext = $("#MainContent_txtVoteText").val();
    var imgrecmheight = $("#").val();
    var imgrecwidth = $("#").val();

    var recmheight = 0;
    var recwidth = $("#").val();
    var recmwidth = 0;
    var position = $("#").val();
    var sliderdisplay = $("#").val();
    var freqofvisit = $("#").val();
    var countryname = $("#").val();
    var cityname = $("#").val();
    var devicename = $("#").val();
    var chartwidth = 0;
    var chartheight = 0;
    var autoadjustdimensions = 1;
    var answeroption = $("#").val();

    //var embedcode = $("#").val();
    //var launchtypenumber = $("#").val();
    var resultaccess = "public";
    var resulttext = $("#MainContent_txtResultText").val();
    var poweredby = "yes";
    var questiontype = 0;
    var questiontext = $("#MainContent_txtQuestion").val();
    var commentreq = 0;
    var videolink = $("#").val();
    var imagelink = $("#MainContent_lblImageName").html();
    var uniquerespondent = $("#").val();

    if ($("#MainContent_ckbResultsPrivate").is(":checked")) {
        resultaccess = "private";
    }

    if ($("#MainContent_chkComments").is(":checked")) {
        commentreq = 1;
    }
    if ($("#MainContent_ckbInsightoBrand").is(":checked")) {
        poweredby = "no";
    }

    if (!$("#MainContent_create").is(":hidden")) {
        thisiframewidth = parseInt($("#MainContent_txtWidthPoll").val());
        thisiframeheight = parseInt($("#MainContent_txtHeightPoll").val());
        recmwidth = parseInt($("#MainContent_lblRecThemeWidth").html());
        recmheight = parseInt($("#MainContent_lblRecThemeHeight").html());
        //if ($("#MainContent_radShowResults").is(":checked")) {
        //    pollResultsOption("results");
        //    $("#showpollresults .collapsible-body").css("display", "block");
        //    $("#showleadgen .collapsible-body").css("display","none");
        //}
        //else {
        //    pollResultsOption("leadgen");
        //    $("#showpollresults .collapsible-body").css("display", "none");
        //    $("#showleadgen .collapsible-body").css("display", "block");
        //}

    }
    else if (!$("#MainContent_design").is(":hidden")) {
        thisiframewidth = parseInt($("#MainContent_txtWidthIframe").val());
        thisiframeheight = parseInt($("#MainContent_txtHeightIframe").val());
        recmwidth = parseInt($("#MainContent_lblWidth").html());
        recmheight = parseInt($("#MainContent_lblHeight").html());
    }

    if ($("#MainContent_radMultipleAnswer").is(":checked")) {
        questiontype = 2;
    }
    else {
        questiontype = 1;
    }
});

function tabDisplay(tabname) {
    //return;
    var polltypecolor = $("#MainContent_leftsidediv").css("background-color");
    var currentpage = $("#MainContent_hdnCurrentPage").val();

    if (tabname == "create") {
        $("#MainContent_divcreatebutton").css("background-color", polltypecolor);
        $("#MainContent_divdesignbutton").css("background-color", "#FFF");
        $("#MainContent_divlaunchbutton").css("background-color", "#FFF");
        $("#MainContent_divcreatebutton").css("color", "#000");
        $("#MainContent_divdesignbutton").css("color", "red");
        $("#MainContent_divlaunchbutton").css("color", "red");
        $("#MainContent_divcreatebutton").css("font-weight", "bold");
        $("#MainContent_divdesignbutton").css("font-weight", "normal");
        $("#MainContent_divlaunchbutton").css("font-weight", "normal");
        $("#MainContent_CreateNew").css("color", "#000");
        $("#MainContent_DesignNew").css("color", "red");
        $("#MainContent_LaunchNew").css("color", "red");
        $("#MainContent_CreateNew").css("font-weight", "bold");
        $("#MainContent_DesignNew").css("font-weight", "normal");
        $("#MainContent_LaunchNew").css("font-weight", "normal");
        //$("#MainContent_create").show();
        //$("#maincontent_design").hide();
        //$("#maincontent_launch").hide();
        //$("#maincontent_launchafter").hide();
        pollResultsOption($("#MainContent_hdnPollResultsOption").val());

        $("#placeholderinvitationmessage").css("display", "block");
        $("#placeholdersubmitbuttonlabel").css("display", "block");
        $("#placeholderthankyouoverlay").css("display", "block");
        $("#placeholderredirecturl").css("display", "block");
        $("#placeholderinvitationmessage").attr("for", "MainContent_txtInvitationMessage");
        $("#placeholdersubmitbuttonlabel").attr("for", "MainContent_txtSubmitButtonLabel");
        $("#placeholderredirecturl").attr("for", "MainContent_txtRedirectURL");
        $("#placeholderthankyouoverlay").attr("for", "MainContent_thankyoumessage");
        currenttabname = "create";
    }
    else if (tabname == "design") {
        $("#MainContent_divcreatebutton").css("background-color", "#FFF");
        $("#MainContent_divdesignbutton").css("background-color", polltypecolor);
        $("#MainContent_divlaunchbutton").css("background-color", "#FFF");
        $("#MainContent_divcreatebutton").css("color", "red");
        $("#MainContent_divdesignbutton").css("color", "#000");
        $("#MainContent_divlaunchbutton").css("color", "red");
        $("#MainContent_divcreatebutton").css("font-weight", "normal");
        $("#MainContent_divdesignbutton").css("font-weight", "bold");
        $("#MainContent_divlaunchbutton").css("font-weight", "normal");
        $("#MainContent_CreateNew").css("color", "red");
        $("#MainContent_DesignNew").css("color", "#000");
        $("#MainContent_LaunchNew").css("color", "red");
        $("#MainContent_CreateNew").css("font-weight", "normal");
        $("#MainContent_DesignNew").css("font-weight", "bold");
        $("#MainContent_LaunchNew").css("font-weight", "normal");
        $("#MainContent_launchafter").css("display", "none");
        //$("#MainContent_create").hide();
        //$("#MainContent_design").show();
        //$("#MainContent_launch").hide();
        //$("#MainContent_launchafter").hide();
        currenttabname = "design";

        //setColorPickers();
    }
    else if (tabname == "launch") {
        hideActivateButton();
        $("#MainContent_divcreatebutton").css("background-color", "#FFF");
        $("#MainContent_divdesignbutton").css("background-color", "#FFF");
        $("#MainContent_divlaunchbutton").css("background-color", polltypecolor);
        $("#MainContent_divcreatebutton").css("color", "red");
        $("#MainContent_divdesignbutton").css("color", "red");
        $("#MainContent_divlaunchbutton").css("color", "#000");
        $("#MainContent_divcreatebutton").css("font-weight", "normal");
        $("#MainContent_divdesignbutton").css("font-weight", "normal");
        $("#MainContent_divlaunchbutton").css("font-weight", "bold");
        $("#MainContent_CreateNew").css("color", "red");
        $("#MainContent_DesignNew").css("color", "red");
        $("#MainContent_LaunchNew").css("color", "#000");
        $("#MainContent_CreateNew").css("font-weight", "normal");
        $("#MainContent_DesignNew").css("font-weight", "normal");
        $("#MainContent_LaunchNew").css("font-weight", "bold");
        //$("#MainContent_create").css("display", "none");
        //$("#MainContent_design").css("display", "none");
        if ($("#MainContent_hdnPollStatus").val() == "Active")
        {
            //$("#MainContent_launch").css("display", "none");
            //$("#MainContent_launchafter").css("display", "block");
            setColorPickerForWebLinkbg();
        }
        else
        {
            //$("#MainContent_launch").css("display", "block");
            //$("#MainContent_launchafter").css("display", "none");
        }
        currenttabname = "launch";
    }
   
}

function setColorPickers() {
    $("#MainContent_txtAnswerColor, #MainContent_txtPollBgColor, #MainContent_txtQuestionColor, #MainContent_txtQuestionBgColor, #MainContent_txtVoteColor, #MainContent_txtVoteBgColor, #MainContent_txtMessageBgColor, #MainContent_txtMessageColor").spectrum({
        color: "#f00",
        preferredFormat: "hex"
    });

    $("#MainContent_txtAnswerColor").spectrum("set", $("#MainContent_txtAnswerColor").val());
    $("#MainContent_txtPollBgColor").spectrum("set", $("#MainContent_txtPollBgColor").val());
    $("#MainContent_txtQuestionColor").spectrum("set", $("#MainContent_txtQuestionColor").val());
    $("#MainContent_txtQuestionBgColor").spectrum("set", $("#MainContent_txtQuestionBgColor").val());
    $("#MainContent_txtVoteColor").spectrum("set", $("#MainContent_txtVoteColor").val());
    $("#MainContent_txtVoteBgColor").spectrum("set", $("#MainContent_txtVoteBgColor").val());
    $("#MainContent_txtMessageColor").spectrum("set", $("#MainContent_txtMessageColor").val());
    $("#MainContent_txtMessageBgColor").spectrum("set", $("#MainContent_txtMessageBgColor").val());

    $("#MainContent_txtAnswerColor, #MainContent_txtPollBgColor, #MainContent_txtQuestionColor, #MainContent_txtQuestionBgColor, #MainContent_txtVoteColor, #MainContent_txtVoteBgColor, #MainContent_txtMessageBgColor, #MainContent_txtMessageColor").show();


    $("#MainContent_txtAnswerColor, #MainContent_txtPollBgColor, #MainContent_txtQuestionColor, #MainContent_txtQuestionBgColor, #MainContent_txtVoteColor, #MainContent_txtVoteBgColor, #MainContent_txtMessageBgColor, #MainContent_txtMessageColor").change(function (color) {
        var pollBg = $("#MainContent_txtPollBgColor").val();
        var pollAnswer = $("#MainContent_txtAnswerColor").val();
        var pollQuestionBg = $("#MainContent_txtQuestionBgColor").val();
        var pollQuestion = $("#MainContent_txtQuestionColor").val();
        var pollVote = $("#MainContent_txtVoteColor").val();
        var pollVoteBg = $("#MainContent_txtVoteBgColor").val();
        var pollMessageBg = $("#MainContent_txtMessageBgColor").val();
        var pollMessage = $("#MainContent_txtMessageColor").val();

        $("#colorvote").css("background-color", "" + pollVote);
        $("#MainContent_customvote, #MainContent_votebutton, #MainContent_btnSubmitLeadGen").css("color", "" + pollVote);

        $("#bgcolorvote").css("background-color", "" + pollVoteBg);
        $("#MainContent_customvote, #MainContent_votebutton, #MainContent_btnSubmitLeadGen, #MainContent_invitationmessagepreview").css("background-color", "" + pollVoteBg);
        
        $("#colorbgmessage").css("background-color", "" + pollMessageBg);
        $(".resultpreview").css("background-color", "" + pollMessageBg);

        $(".resultpreview").css("color", "" + pollMessage);
        $("#colormessage").css("background-color", "" + pollMessage);

        $("#colorquestion").css("background-color", "" + pollQuestion);
        $(".questioncss").css("color", "" + pollQuestion);

        $("#bgcolorquestion").css("background-color", "" + pollQuestionBg);
        $("#questiondiv").css("background-color", "" + pollQuestionBg + " ");
        $("#questiondivresults").css("background-color", "" + pollQuestionBg + " ");

        $(".textpollthemepreview").css("background-color", "" + pollBg);
        $(".textpollthemepreviewresults").css("background-color", "" + pollBg);
        $("#colorbgpoll").css("background-color", "" + pollBg);

        $(".poweredby, .poweredby a, .poweredbyrep, .poweredbyrep a, #placeholdercomments,  #placeholdermobile,  #placeholderemail, #MainContent_invitationmessagepreview, #thankyouoverlay, #thankyouoverlaymessage").css("color", "" + pollAnswer);

        $(".txtRow, #MainContent_lblsharethisleadgen").css("color", pollAnswer);
        $("#coloranswer").css("background-color", "" + pollAnswer);
        $(".sponsorimagepreviewres1").css("color", "" + pollAnswer);
        $('.sponsorimagepreview').css("color", "" + pollAnswer);
        $('.sponsorimagepreviewres').css("color", "" + pollAnswer);
        $('.socialnew').css("color", "" + pollAnswer);
        $('.socialnew1').css("color", "" + pollAnswer);
        $('#MainContent_lblShare').css("color", "" + pollAnswer);
        
        renderChart();
    });

}

function setColorPickerForWebLinkbg() {

    $("#MainContent_txtWebLinkBgColor").spectrum({
        color: "#f00",
        preferredFormat: "hex"
    });

    $("#MainContent_txtWebLinkBgColor").spectrum("set", $("#MainContent_txtWebLinkBgColor").val());

    $("#MainContent_txtWebLinkBgColor").show();
    $("#MainContent_txtWebLinkBgColor").change(function () {
        console.log($("#MainContent_txtWebLinkBgColor").val());
        saveWebLinkBgColor($("#MainContent_txtWebLinkBgColor").val());
    });
}

function setScrollBars() {
    var windowheight = parseInt(window.innerHeight);

    $.mCustomScrollbar.defaults.scrollButtons.enable = true; //enable scrolling buttons by default
    $.mCustomScrollbar.defaults.axis = "yx"; //enable 2 axis scrollbars by default

    if (windowheight < 500) {
        windowheight = 700;
    }
    $("#MainContent_create").css("height", (windowheight - 150).toString() + "px");
    $("#MainContent_create").css("max-height", (windowheight - 150).toString() + "px");
    $("#MainContent_design").css("height", (windowheight - 165).toString() + "px");
    $("#MainContent_design").css("max-height", (windowheight - 165).toString() + "px");
    $("#MainContent_launch").css("height", (windowheight - 125).toString() + "px");
    $("#MainContent_launch").css("max-height", (windowheight - 125).toString() + "px");
    $("#pollpreviewinright").css("height", (windowheight - 125).toString() + "px");
    $("#pollpreviewinright").css("max-height", (windowheight - 125).toString() + "px");
    $("#slider").css("height", (windowheight - 240).toString() + "px");
    $("#slider").css("max-height", (windowheight - 240).toString() + "px");
    $("#MainContent_create,#MainContent_design,#MainContent_launch,#pollpreviewinright,#slider").mCustomScrollbar({
        scrollButtons: { enable: true },
        theme: "rounded-dark",
        autoExpandScrollbar: false,
        autoHideScrollbar: true,
        scrollbarPosition: "inside"

    });
}

function hideActivateButton()
{
    if ($("#MainContent_hdnParentPollID").val() == $("#MainContent_hdnPollID").val()) {
        $("#MainContent_btnActivateParentPoll").hide();
        $("#MainContent_btnActivatePoll").show();
    }
    else {
        $("#MainContent_btnActivateParentPoll").show();
        $("#MainContent_btnActivatePoll").hide();
    }
}

function pollResultsOption(inputval) {
    $("#MainContent_hdnPollResultsOption").val(inputval);
    if (inputval == "results") {
        $("#pollpreviewresultsheaderdiv").html("Your results preview");
        $("#MainContent_radShowResults").prop("checked", true);
        $("#MainContent_radLeadGen").prop("checked", false);
        $("#showpollresults .collapsible-body").show("slow");
        $("#showleadgen .collapsible-body").css("display", "none");

        $("#pollresultspreviewmaindiv").show();
        $("#MainContent_results").show();
        $("#purejschart").show();
        $("#pollleadgencontent").hide();
        $("#MainContent_hdnResultAccess").val("public");
    }
    else {
        $("#pollpreviewresultsheaderdiv").html("Your leadgen preview");
        $("#MainContent_radShowResults").prop("checked", false);
        $("#MainContent_radLeadGen").prop("checked", true);
        $("#showpollresults .collapsible-body").css("display", "none");
        $("#showleadgen .collapsible-body").show("slow");

        $("#pollresultspreviewmaindiv").show();
        $("#MainContent_results").hide();
        $("#purejschart").hide();
        $("#pollleadgencontent").show();
        $("#MainContent_hdnResultAccess").val("private");
    }
    calculateIFrameHeight();
}

function showPollResultsOption() {
    var inputval = "";
    if ($("#MainContent_radShowResults").is(":checked")) {
        $("#pollresultspreviewmaindiv").show();

        $("#showleadgen").removeClass("active");
        $("#showleadgen .collapsible-header").removeClass("active");
        $("#showleadgen .collapsible-body").css("display", "none");

        $("#showpollresults").addClass("active");
        $("#showpollresults .collapsible-header").addClass("active");
        $("#showpollresults .collapsible-body").css("display","block");

        $("#MainContent_results").show();
        $("#purejschart").show();
        $("#pollleadgencontent").hide();
        inputval = "public";
        if (! hasvotebuttonlabelchanged) {
            $("#MainContent_votebutton").html("Vote and view results");
            $("#MainContent_customvote").html("Vote and view results");
        }
    }
    else {
        $("#pollresultspreviewmaindiv").show();

        $("#showleadgen").addClass("active");
        $("#showleadgen .collapsible-header").addClass("active");
        $("#showleadgen .collapsible-body").css("display", "block");

        $("#showpollresults").removeClass("active");
        $("#showpollresults .collapsible-header").removeClass("active");
        $("#showpollresults .collapsible-body").css("display", "none");

        $("#MainContent_results").hide();
        $("#purejschart").hide();
        $("#pollleadgencontent").show();
        inputval = "private";
        if (! hasvotebuttonlabelchanged) {
            $("#MainContent_votebutton").html("Vote");
            $("#MainContent_customvote").html("Vote");
        }
    }
    $("#MainContent_hdnResultAccess").val(inputval);
}

function showSubmitButtonOptions() {

    if ($("#MainContent_ckbSubmit").is(":checked")) {
        $("#submitbuttondiv").show();
        $("#submitbuttonpreviewdiv").show();
    }
    else {
        $("#submitbuttondiv").css("display", "none");
        $("#submitbuttonpreviewdiv").hide();
    }
    calculateIFrameHeight();

}

function showContactOptions() {
    var optionsvisible = false;
    if ($("#MainContent_chkEmail").is(":checked")) {
        optionsvisible = true;
        $("#MainContent_contactdetailscollectiondiv").show();
        $("#MainContent_emaildiv").show();
    }
    else
    {    
        $("#MainContent_emaildiv").hide();
    }

    if ($("#MainContent_chkPhone").is(":checked")) {
        optionsvisible = true;
        $("#MainContent_contactdetailscollectiondiv").show();
        $("#MainContent_phonenumberdiv").show();
    }
    else{
        $("#MainContent_phonenumberdiv").hide();

    }

    if(! optionsvisible)
    {
        $("#MainContent_contactdetailscollectiondiv").hide();
    }
    else{
        $("#MainContent_ckbSubmit").attr("checked","checked");
    }
    showSubmitButtonOptions();
    calculateIFrameHeight();
}

function showInvitationMessage() {
    var invitemessage = $("#MainContent_txtInvitationMessage").val();
    if (invitemessage.length > 0) {
        $("#MainContent_invitationmessagepreview").show();
        $("#MainContent_invitationmessagepreview").html(invitemessage);
    }
    else {
        $("#MainContent_invitationmessagepreview").hide();

    }
}

function showOverlay() {
    if ($("#thankyouoverlay").is(":hidden"))
    {
        $("#overlayshow").html("Hide");
        $("#thankyouoverlay").show("slow");
        $("#thankyouoverlay, #thankyouoverlaymessage").css("color", $("#MainContent_txtVoteColor").val());
        $("#thankyouoverlay").css("background-color", $("#MainContent_txtPollBgColor").val());
        $("#thankyouoverlaymessage").html($("#MainContent_txtResultText").val());
        $("#pollleadgencontent").hide("slow");
    }
    else {
        $("#overlayshow").html("Show");
        $("#thankyouoverlay").hide();
        $("#pollleadgencontent").show();
    }
    calculateIFrameHeight();
}

function launchReplacedPoll() {

        var allerrors = "";
        var height = 0;
        var width = 0;
        var recheight = 0;
        var recwidth = 0;
        if ($("#MainContent_hdnPollStatus").val() == "Closed") {
            Materialize.toast("Closed polls cannot be launched", 4000);
            return false;
        }
        if ($("#MainContent_txtQuestion").val() == "") {
            allerrors = "Please enter the question <br/>";
        }
        if ($("#MainContent_txtWidthPoll").val() == "") {
            allerrors = "Please enter width <br/>";
        }
        else {
        }
        if ($("#MainContent_txtHeightPoll").val() == "") {
            allerrors = "Please enter height <br/>";
        }
        else {
            $("#errorid1").hide();
        }
        if (!isNaN(parseInt($("#MainContent_txtHeightPoll").val()))) {
            height = parseInt($("#MainContent_txtHeightPoll").val());
        }
        if (!isNaN(parseInt($("#MainContent_txtWidthPoll").val()))) {
            width = parseInt($("#MainContent_txtWidthPoll").val());
        }

        if (!isNaN(parseInt($("#MainContent_lblRecThemeWidth").html()))) {
            recwidth = parseInt($("#MainContent_lblRecThemeWidth").html());
        }
        else {
            recwidth = parseInt($("#MainContent_lblWidth").html());

        }

        if (!isNaN(parseInt($("#MainContent_lblRecThemeHeight").html()))) {
            recheight = parseInt($("#MainContent_lblRecThemeHeight").html());
        }
        else {
            recheight = parseInt($("#MainContent_lblHeight").html());

        }

        if (recheight > height) {
            allerrors = "Height is lesser than recommended. <br/>";
        }

        if (recwidth > width) {
            allerrors = "Width is lesser than recommended. <br/>";
        }


        if ($('#MainContent_hdnPollType').val() == "video") {
            if ($("#MainContent_txtVideo").val() == "") {
                allerrors = "Please enter video code <br/>";
            }
        }
        else {
            $("#errorid").hide();
        }

        if ($('#MainContent_hdnPollType').val() == "image") {
            if ($('#MainContent_fileUploadName').val() == "") {
                console.log("no image");
                allerrors = "Please upload image";
            }
        }

        var ansno = $("#MainContent_hdnRepeaterTxts").val();
        ansno = parseInt(ansno) - 1;
        var i = parseInt(0);
        var count = 0;
        for (i = 0; i <= parseInt(ansno) ; i++) {
            var id = "#txtAnswerOption_" + i;
            if ($(id).val() == "" || $(id).val() == undefined) {

            }
            else {
                var sameoptions = false;
                var j = i + 1;
                var jd = "#txtAnswerOption_" + j;
                for (j = 0; j < parseInt(ansno) ; j++) {
                    if ($(id).val() == $(jd).val()) {
                        sameoptions = true;
                    }
                }
                count = parseInt(count) + 1;
                if (sameoptions) {
                    allerrors = "Answer Options should not be same <br/>";
                }
            }
        }
        if (parseInt(count) < 2) {
            allerrors = "Please add minimum answer options <br/>";
        }
        if (allerrors != "") {

            Materialize.toast(allerrors, 8000);
            return false;
        }
        if (($("#MainContent_hdnPollID").val() != $("#MainContent_hdnParentPollID").val()) && ($("#MainContent_hdnParentPollID").val() != "Active")) {
            $("#btnActivatePoll").leanModal();
            $("#activatedialog").openModal();
            $("#activatedialog").css("z-index", "999999");
        }
        else {
            $("#MainContent_btnActivatePoll").click();
        }
}

function sendActivationEmail() {
    Materialize.toast("Sending activation email", 2000);

    $('#modalactivationalert').closeModal();
    $.ajax(
        {
            url: "/polls/ajaxservice.aspx?method=SendEmail",
            method: "GET",
            success: function (data) {
                if (data == "sent") {
                    Materialize.toast("Activation email sent", 2000);
                }
                else {
                    Materialize.toast("Error sending email. Please contact support@insighto.com.", 10000);
                }
            }
        }
    );
    

}

function showThankYouMessages() {
    if ($("#MainContent_chkSameAnswer").is(":checked") || $("#MainContent_radMultipleAnswer").is(":checked")) {
        $("#MainContent_chkSameAnswer").prop("checked", "checked");
        $("#MainContent_txtInvitationMessage").show();
        $("#thankyoumessagesdiv").hide();
    }
    else {
        $("#MainContent_txtInvitationMessage").hide();
        $("#thankyoumessagesdiv").show();
}
}

function showThankYouOption(id) {
    if ((! $("#MainContent_chkSameAnswer").is(":checked")) && $("#MainContent_radLeadGen").is(":checked")) {
        var thisanswerthankyouoption = $("#txtThankYouOption_" + id + "").val();
        if (thisanswerthankyouoption == "") {
            thisanswerthankyouoption = "Thank you for your vote";
        }
        $("#MainContent_invitationmessagepreview").html(thisanswerthankyouoption);
        $("#MainContent_invitationmessagepreview").css("color", $("#MainContent_txtVoteColor").val());
    }
}

function updateThankYouOption(id) {
    if ((!$("#MainContent_chkSameAnswer").is(":checked")) && $("#MainContent_radLeadGen").is(":checked") && $("#radButton_"+ id).is(":checked")) {
        var thisanswerthankyouoption = $("#txtThankYouOption_" + id + "").val();
        if (thisanswerthankyouoption == "") {
            thisanswerthankyouoption = "Thank you for your vote";
        }
        $("#MainContent_invitationmessagepreview").html(thisanswerthankyouoption);
        $("#MainContent_invitationmessagepreview").css("color", $("#MainContent_txtVoteColor").val());
    }
}

function setMultiThankYouMessagesOption() {
    if ($("#MainContent_radMultipleAnswer").is(":checked")) {
        showThankYouMessages();
    }
    else {
        $("#MainContent_chkSameAnswer").attr("disabled", false);
    }

}

function openCopyModal() {
    $("#MainContent_txtCopyWebLink").val($("#MainContent_txtWebLink").val());
    $("#copydialog").openModal();
}

function saveWebLinkBgColor(color) {
    console.log(color);
    $.ajax(
       {
           url: "/polls/ajaxservice.aspx?method=SaveWeblinkBgColor",
           method: "GET",
           data: { "pollid": $("#MainContent_hdnPollID").val(), "color": color },
           success: function (data) {
               if (data == "saved") {
                   Materialize.toast("Web link background color saved", 1500);
               }
               else if(data == "loggedout"){
                   Materialize.toast("Your session has ended. Please login in again", 1500);
               }
                else{
                   Materialize.toast("Error setting web lknk background color", 1500);
               }
           }
       }
   );


}