﻿var innerdivwidth = "";
var polldivwidth = "";
var polldivheight = "";

$(window).load(function () {

    $("#btnVoteAn, #btnVoteLeft").click(function () {
        var element = document.getElementById("radAnswerOptions");
        if (typeof (element) != 'undefined' && element != null) {
            var no = document.getElementById("radAnswerOptions").rows.length - 1;
            if ($("input[name='radAnswerOptions']:checked").length < 1) {
                Materialize.toast('Please select an option', 3000);
                return false;
            }
            else {
                submitResults();
                return false;
                //return true;
            }
        }
        else {
            var count = 0;
            no = document.getElementById("ckbAnswerOptions").rows.length - 1;
            var selectedValues = [];
            if ($("[id*=ckbAnswerOptions] input:checkbox:checked").length < 1) {
                Materialize.toast('Please select an option', 3000);
                return false;
            }
            else {
                submitResults();
                return false;
                //return true;
            }
        }


    });

    $("#MyLink").click(function () {
        var element = document.getElementById("radAnswerOptions");
        if (typeof (element) != 'undefined' && element != null) {
            var no = document.getElementById("radAnswerOptions").rows.length - 1;
            if ($("input[name='radAnswerOptions']:checked").length < 1) {
                jAlert('Please select answer option', 'Polls', function (r) {
                    return false;
                });
                return false;
            }
            else {
                var resid = getCookie("RespondentId");
                if (resid != "") {
                    var unique = $("#hdnUniqueRes").val();
                    if (parseInt(unique) == 0) {
                        $(".textpollthemeresponse").css("display", "none");
                        $(".classmsg").css("display", "block");
                        return false;
                    }
                    else {
                        submitResults();
                        return false;
                        //return true;
                    }
                }
                else {
                    resid = $("#hdnRespondentId").val();
                    setCookie("RespondentId", resid);
                }
                submitResults();
                return false;
                //return true;
            }
        }
        else {
            var count = 0;
            no = document.getElementById("ckbAnswerOptions").rows.length - 1;
            var selectedValues = [];
            if ($("[id*=ckbAnswerOptions] input:checkbox:checked").length < 1) {
                jAlert('Please select answer option', 'Polls', function (r) {
                    return false;
                });
                return false;
            }
            else {

                var resid = getCookie("RespondentId");
                if (resid != "") {
                    var unique = $("#hdnUniqueRes").val();
                    if (parseInt(unique) == 0) {
                        $(".textpollthemeresponse").css("display", "none");
                        $(".classmsg").css("display", "block");
                        return false;
                    }
                    else {
                        submitResults();
                        return false;
                        //return true;
                    }
                }
                else {
                    resid = $("#hdnRespondentId").val();
                    setCookie("RespondentId", resid);
                }
                submitResults();
                return false;
                //return true;
            }
        }
    });

    $("#LinkNew").click(function () {
        var element = document.getElementById("radAnswerOptions");
        if (typeof (element) != 'undefined' && element != null) {
            var no = document.getElementById("radAnswerOptions").rows.length - 1;
            if ($("input[name='radAnswerOptions']:checked").length < 1) {
                jAlert('Please select answer option', 'Polls', function (r) {
                    return false;
                });
                return false;
            }
            else {
                var resid = getCookie("RespondentId");
                if (resid != "") {
                    var unique = $("#hdnUniqueRes").val();
                    if (parseInt(unique) == 0) {
                        $(".textpollthemeresponse").css("display", "none");
                        $(".classmsg").css("display", "block");
                        return false;
                    }
                    else {
                        submitResults();
                        return false;
                        //return true;
                    }
                }
                else {
                    resid = $("#hdnRespondentId").val();
                    setCookie("RespondentId", resid);
                }
                submitResults();
                return false;
                //return true;
            }
        }
        else {
            var count = 0;
            no = document.getElementById("ckbAnswerOptions").rows.length - 1;
            var selectedValues = [];
            if ($("[id*=ckbAnswerOptions] input:checkbox:checked").length < 1) {
                jAlert('Please select answer option', 'Polls', function (r) {
                    return false;
                });
                return false;
            }
            else {

                var resid = getCookie("RespondentId");
                if (resid != "") {
                    var unique = $("#hdnUniqueRes").val();
                    if (parseInt(unique) == 0) {
                        $(".textpollthemeresponse").css("display", "none");
                        $(".classmsg").css("display", "block");
                        return false;
                    }
                    else {
                        submitResults();
                        return false;
                        //return true;
                    }
                }
                else {
                    resid = $("#hdnRespondentId").val();
                    setCookie("RespondentId", resid);
                }
                submitResults();
                return false;
                //return true;
            }
        }
    });

    $(".card-content").css("height", "70%");
    //$(".card-action").css("height", "22%");
    if ($("#hdnIsSponsorLogoShowing").val() == "true") {
        console.log($("#sponsormaindiv").css("top"));
        console.log($("#sponsormaindiv").offset().top);
        $("#votebuttononleft").show();
        $("#votebuttonwithnosponsor").remove();
        $("#votebuttononleft").css("top", ($("#sponsormaindiv").offset().top + 5).toString() + "px");
    }
    else {
        $("#votebuttononleft").remove();
        //$("#votebuttonwithsponsor").remove();
        $("#votebuttonwithnosponsor").show();
    }
});

$(document).ready(function () {

    if ($("#hdnIsSponsorLogoShowing").val() == "false") {
        $("#sponsornav").hide();
        $("#sponsormaindiv").hide();
        $("#sponsormaindivleadgen").hide();
    }
    else {
        $("#sponsornav").show();
        $("#sponsormaindiv").show();
        $("#sponsormaindivleadgen").show();
    }

    if ($("#hdnIsPoweredByShowing").val() == "false") {
        $("#poweredbydiv").hide();
    }
    else {
        $("#poweredbydiv").show();
    }

    var numberofrows = parseInt($("tr").length);
    var answerrowheight = 35;
    var tableheight = numberofrows * answerrowheight;
    $("#radAnswerOptions").css("height", tableheight.toString() + "px");
    $("#radAnswerOptions").css("margin-top", "-2px");
    var pollBg = $("#hdnPollBgColor").val();
    var pollQuestion = $("#hdnQuestColor").val();
    var pollQuestionBg = $("#hdnQuestBgColor").val();
    var pollAnswer = $("#hdnAnswerColor").val();
    var pollVote = $("#hdnVoteColor").val();
    var pollVoteBg = $("#hdnVoteBgColor").val();
    var width = $("#hdnWidth").val();
    var height = $("#hdnHeight").val();
    var heightrec = $("#hdnRecHeight").val();
    var pollQuestionFont = $("#hdnQuestionFont").val();
    var answerfont = $("#hdnAnswerFont").val();
    var votefont = $("#hdnVoteFont").val();
    var msgfont = $("#hdnMsgFont").val();
    var questweight = $("#hdnQuestionBold").val();
    var answerweight = $("#hdnAnswerBold").val();
    var voteweight = $("#hdnVoteBold").val();
    var msgweight = $("#hdnMsgBold").val();
    var queststyle = $("#hdnQuestionItalic").val();
    var answerstyle = $("#hdnAnswerItalic").val();
    var votestyle = $("#hdnVoteItalic").val();
    var msgstyle = $("#hdnMsgItalic").val();
    var questul = $("#hdnQuestionUL").val();
    var answerul = $("#hdnAnswerUL").val();
    var voteul = $("#hdnVoteUL").val();
    var msgul = $("#hdnMsgUL").val();
    var questalign = $("#hdnQuestionAlign").val();
    var answeralign = $("#hdnAnswerAlign").val();
    var votealign = $("#hdnVoteAlign").val();
    var msgalign = $("#hdnMsgAlign").val();
    var questsize = $("#hdnQuestionFSize").val();
    var answersize = $("#hdnAnswerFSize").val();
    var votesize = $("#hdnVoteFSize").val();
    var msgsize = $("#hdnMsgFSize").val();
    var msgbgcolor = $("#hdnMsgBgColor").val();
    var msgcolor = $("#hdnMsgColor").val();
    var pollbgcolor = $("#hdnPollBgColor").val();

    if (pollBg.indexOf("#") < 0) {
        pollBg = "#" + pollBg;
    }

    if (pollbgcolor.indexOf("#") < 0) {
        pollbgcolor = "#" + pollbgcolor;
    }

    if (pollQuestion.indexOf("#") < 0) {
        pollQuestion = "#" + pollQuestion;
    }

    if (pollQuestionBg.indexOf("#") < 0) {
        pollQuestionBg = "#" + pollQuestionBg;
    }

    if (pollAnswer.indexOf("#") < 0) {
        pollAnswer = "#" + pollAnswer;
    }

    if (pollVote.indexOf("#") < 0) {
        pollVote = "#" + pollVote;
    }

    if (pollVoteBg.indexOf("#") < 0) {
        pollVoteBg = "#" + pollVoteBg;
    }
    //console.log(pollBg + "__" + pollbgcolor + "__" + pollQuestion + "__" + pollQuestionBg + "__" + pollAnswer + "__" + pollVote + "__" + pollVoteBg);
    
    //$("body").css("background-color", weblinkbgcolor);
    $(".lblRequired").css("display", "none");
    $(".classmsg").css("background-color", "#ffffff");
    $(".classmsg").css("width", width);
    var heightcl = parseInt(height) - 17;
    $(".classmsg").css("height", heightcl);
    $(".textpollthemeresponse").css("background-color", pollBg);
    $("#polldiv").css("background-color", pollBg + "");

    $(".textpollthemeresponse").css("width", width);
    $(".textpollthemeprint").css("width", width);
    $(".videopreview").css("width", width);
    var comwidth = width - 30;
    //$(".txtcomments").css("width", comwidth + 'px');
    $(".commentstxt, #lblTxtComments").css("color", pollAnswer);
    $(".textpollthemeresponse").css("height", height);
    $(".textpollthemeprint").css("height", height);

    console.log(pollQuestionBg);
    $("#questiondiv").css("background-color", pollQuestionBg + "");
    $(".questionpreviewcss").css("background-color", pollQuestionBg + "");
    $("#questiondiv").css("font-family", pollQuestionFont);
    $("#questiondiv").css("color", pollQuestion);
    $(".radiopolltheme").css("color", pollAnswer);


    $("td").css("color", pollAnswer);
    $("td label").css("color", pollAnswer);
    $("td span").css("color", pollAnswer);
    $("td").css("font-family", answerfont);
    $("td label").css("font-family", answerfont);
    $("td span").css("font-family", answerfont);

    //$("td").css("text-align", answeralign);
    $("td label").css("width", "100%");
    $("td label").css("text-align", answeralign);
    $("td span").css("text-align", answeralign);

    $("#lblSpoRes").css("color", pollAnswer);
    $("#lblShare").css("color", pollAnswer);
    $("#poweredby").css("color", pollAnswer + "");
    $("#poweredby a").css("color", pollAnswer);
    $(".sponsorimagepreview").css("font-family", answerfont);
    $(".socialshares").css("font-family", answerfont);
    $("#poweredby").css("font-family", answerfont);
    $("#poweredby a").css("font-family", answerfont);

    console.log(pollVoteBg + "_" + pollVote);
    $("#btnVoteAn, #btnVoteLeft, #btn").css("height", "10% !important");
    $("#btnVoteAn, #btnVoteLeft").css("width", "auto");
    $("#btnVoteAn, #btnVoteLeft").css("padding", "5px 5px 5px 5px");
    $("#btnVoteAn, #btnVoteLeft").css("background-color", pollVoteBg);
    $("#btnVoteAn, #btnVoteLeft").css("font-family", votefont);
    $("#btnVoteAn, #btnVoteLeft").css("text-decoration", voteul);

    $("#btnVoteAn, #btnVoteLeft, #invitationmessagepreview, #thankyouoverlay, .messageprivate").css("background-color", pollVoteBg);
    $("#btnVoteAn, #btnVoteLeft").css("font-family", votefont);
    $("#btnVoteAn, #btnVoteLeft").css("text-decoration", voteul);

    $("#btnVoteAn, #btnVoteLeftbtn").css("font-family", votefont);
    $("#btnVoteAn, #btnVoteLeftbtn").css("background-color", pollVoteBg);
    $("#btnVoteAn, #btnVoteLeftbtn").css("text-decoration", voteul);
    $("#btnVoteAn, #btnVoteLeftbtn").css("font-style", voteweight);
    $("#btnVoteAn, #btnVoteLeftbtn").css("color", pollVote);

    $("#btnVoteAn, #btnVoteLeftbtn").css("color", pollVote);
    $("#btnVoteAn, #btnVoteLeft").css("color", pollVote);

    $(".poweredbyres").css("color", pollAnswer);
    $(".poweredbyres a").css("color", pollAnswer);


    $(".sponsorimagepreview").css("font-size", answersize + 'px');
    $(".socialshares").css("font-size", answersize + 'px');
    $('.messageprivate').css({ 'font-weight': msgweight });
    $('.messageprivate').css({ 'font-family': msgfont });
    $('.messageprivate').css({ 'font-style': msgstyle });
    $('.messageprivate').css({ 'font-size': msgsize + 'px' });
    $('.messageprivate').css({ 'background-color': msgbgcolor });
    $('.messageprivate').css({ 'color': msgcolor });
    $('#questiondiv').css({ 'font-weight': questweight });
    $('.radiopolltheme').css({ 'font-weight': answerweight });
    $('#poweredby').css({ 'font-weight': answerweight });
    $('#poweredby a').css({ 'font-weight': answerweight });
    $('#btnVoteAn, #btnVoteLeft').css({ 'font-weight': voteweight });
    $('#questiondiv').css({ 'font-style': queststyle });
    $('.radiopolltheme').css({ 'font-style': answerstyle });
    $('#poweredby').css({ 'font-style': answerstyle });
    $('#poweredby a').css({ 'font-style': answerstyle });
    $('#btnVoteAn, #btnVoteLeft, #btnSubmitLeadGen').css({ 'font-style': votestyle });
    $('#questiondiv').css({ 'text-decoration': questul });
    $('.radiopolltheme').css({ 'text-decoration': answerul });
    $('#poweredby').css({ 'text-decoration': answerul });
    $('#poweredby a').css({ 'text-decoration': answerul });

    $('#btnVoteAn, #btnVoteLeft, #btnSubmitLeadGen').css({ 'text-decoration': voteul });
    $('#questiondiv').css({ 'text-align': questalign });
    $('.radiopolltheme').css({ 'text-align': answeralign });
    $('.sponsorimage').css({ 'font-family': answerfont });
    $('.sponsorimage').css({ 'font-size': answersize + 'px' });
    $('.sponsorimage').css({ 'color': pollAnswer });
    $('#btnVoteAn, #btnVoteLeft, #btnSubmitLeadGen').css({ 'text-align': votealign });
    $('#questiondiv').css({ 'font-size': questsize + 'px' });
    $('.radiopolltheme').css({ 'font-size': answersize + 'px' });
    $('#radAnswerOptions').css({ 'font-size': answersize + 'px' });
    $('#radAnswerOptions tr').css({ 'font-size': answersize + 'px' });
    $('#radAnswerOptions tr td').css({ 'font-size': answersize + 'px' });
    $('#radAnswerOptions tr td label').css({ 'font-size': answersize + 'px' });

    $('#btnVoteAn, #btnVoteLeft, #btnSubmitLeadGen').css({ 'font-size': votesize + 'px' });
    if ($('#social').is(':hidden')) {
        $(".sponsorimage").css("margin-right", '2%');
        $(".sponsorimage").css("float", 'right');
    }
    else {
        $(".sponsorimage").css("margin-right", '1%');
        $(".sponsorimage").css("float", 'right');
    }

    if ($('#messagebox').is(':hidden')) {

    }
    else {
        $(".poweredbyres").css("position", 'absolute');
        $(".poweredbyres").css("bottom", '10px');
        $(".poweredbyres").css("text-align", 'center');
        $(".poweredbyres").css("width", '100%');
        $(".poweredbyres").css("margin-top", '');
        $(".poweredbyres").css("margin-bottom", '');
        $(".poweredbyres").css("padding-bottom", '');
        $(".voteres").css("position", 'absolute');
        $(".voteresview").css("position", 'absolute');
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '7px');
        }
        else {
            $(".viewtest").css("bottom", '30px');
        }
        $(".sponsorimage").css("margin-left", '0px');
    }

    polldivwidth = parseInt($("#hdnWidth").val());
    polldivheight = parseInt($("#hdnHeight").val());
    console.log(polldivheight.toString() + "_" + polldivwidth.toString());

    //$("#polldiv, #innerdiv, .container").css("min-height", polldivheight + "px !important");
    $("#polldiv, #innerdiv").css("height", polldivheight + "px");
    $("#polldiv, #innerdiv, .container").css("width", "auto");
    $("#innerdiv").css("padding", "0");
    $("#questiondiv .row").css("margin-bottom", "8px !important");
    $("#questiondiv").removeClass("row");

    $("#invitationmessagepreview").css({ "color": pollAnswer, "font-size": answersize + "px", "font-family": answerfont, "font-weight": answerweight, "font-style": answerstyle });
    $("#lblSocialShareLeadGen, #sponsoredtextleadgen, #poweredbyleadgen").css({ "color": pollAnswer, "font-size": "14px", "font-family": answerfont, "font-weight": answerweight, "font-style": answerstyle });
    $("#btnRedirectURL, #btnSubmitLeadGen").css({ "color": pollVote, "background-color": pollVoteBg, "font-size": votesize + "px", "font-family": votefont, "font-weight": voteweight, "font-style": votestyle });
    $("#lblTxtComments, #poweredby, #lblphonenumber, #lblemail").css("color", $("#hdnAnswerColor").val());
    $("#invitationmessagepreview, #lblMessage").css("color", $("#hdnVoteColor").val());
    $("#votebuttonwithsponsor, #votebuttonwithnosponsor").hide();
});

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) != -1) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue) {
    document.cookie = cname + "=" + cvalue + ";";
}

function submitResults() {
    var resid = getCookie("RespondentId" + $("#hdnPollID").val());
    if (resid != "") {
        var unique = $("#hdnUniqueRes").val();
        if (parseInt(unique) == 1) {
            //Materialize.toast("You have already voted on this poll", 3000, '', function () { $("#toast-container").css("opacity", "1"); });
            Materialize.toast("You have already voted on this poll", 3000);
            return;
        }
        else {
        }
    }
    else {
        resid = $("#hdnRespondentId").val();
        setCookie("RespondentId" + $("#hdnPollID").val(), resid);

    }

    innerdivwidth = $("#innerdiv").css("width");
    polldivwidth = $("#polldiv").css("width");

    $("#sponsorres").hide();
    var respondentid = $("#hdnRespondentId").val();
    var comments = $("#txtComments").val();
    var answeroptions = "";
    var pollid = $("#hdn").val();
    var answerlength = 0;
    var minanswersheight = "";
    var element = document.getElementById("radAnswerOptions");
    if (typeof (element) != 'undefined' && element != null) {
        answerlength = $("input[name='radAnswerOptions']").length;
        element = $("input[name='radAnswerOptions']");
    }
    else {
        answerlength = $("[id*=ckbAnswerOptions] input:checkbox").length;
        element = $("[id*=ckbAnswerOptions] input:checkbox");
    }
    for (var p = 0; p < answerlength; p++) {
        if (element[p].checked) {
            answeroptions += element[p].value + ",";
        }
    }
    if ($("#singleradbtns").is(":hidden") == false) {
        minanswersheight = $("#singleradbtns").css("height");
    }
    else {
        minanswersheight = $("#multipleckbbtns").css("height");
    }

    $.ajax({
        type: "GET",
        url: "/Polls/AjaxService.aspx?method=SavePollResponse",
        data: { pollid: pollid, respondentid: respondentid, answeroptions: answeroptions, comments: comments },
        dataType: "json",
        success: function (data) {

            $("#votebuttonwithsponsor").show();
            $("#votebuttonswithsponsor").show();
            $("#votebuttoncontent").show();

            $("#btnVoteLeft").hide();
            $("#btnVoteAn").hide();

            if ($("#hdnIsSponsorLogoShowing").val() == "false") {
                $("#sponsormaindiv").hide();
                $("#sponsormaindivleadgen").hide();
            }
            else {
                $("#sponsormaindivleadgen").show();
            }
            $("#txtComments").hide();
            $("#lblTxtComments").hide();
            $('#singleradbtns').addClass("fadeOut animated");
            $('#multipleckbbtns').addClass("fadeOut animated");
            $("#singleradbtns").hide();
            $("#multipleckbbtns").hide();
            $("#voteandview").hide();
            $("#votebutton").hide();
            $("#onlyvote").hide();
            $("#onlyvoteres").hide();
            $("#voteviewres").hide();
            $("#sponsorres").hide();
            //$("#votebuttonwithsponsor").css("position", "abs");

            $('#polldiv').css("height", polldivheight + "px");
            $(".card-content").css("height", "85% !important");
            $(".card-action").css("height", "15% !important");
            $(".card-action").css({ "width": "100%"});
            if (data.resultaccess == "private") {

                $('#polldiv').addClass("flipInYX animated");
                var pollleadgencontent = $('#pollleadgencontent').html();
                $('#pollleadgencontent').remove();
                $('#purejschart').replaceWith(pollleadgencontent);
                $('#socialmediaandsponsorleadgendiv').css('display', 'none');
                $("#invitationmessagepreview").html(data.invitationthankyoumessage);
            }
            else {
                $("#messagebox").hide();
                $("#lblQuestion").append(" (" + data.votecount + " votes)");
            }
            if (data.showsocialsharing == "1") {

                $("#votebuttonwithsponsor").show();
                $("#votebuttonwithsponsor").removeClass("hide");
                $("#votebuttonwithsponsor").css("height", "30px");
                $("#btnVoteLeft").hide();
                $("#socialshare").show();
                $("#lblShare").show();
                $("#socialshareleadgen").show();
                $("#lblShare").html(data.socialsharetext);
                if (data.showtwitter == "1") {
                    $("#twittericon").show();
                    $("#twittericonleadgen").show();
                }
                else {
                    $("#twittericon").hide();
                    $("#twittericonleadgen").hide();
                }
                if (data.showgoogleplus == "1") {
                    $("#googleicon").show();
                    $("#googleiconleadgen").show();
                }
                else {
                    $("#googleicon").hide();
                    $("#googleiconleadgen").hide();
                }
                if (data.showfacebook == "1") {
                    $("#fbicon").show();
                    $("#fbiconleadgen").show();
                }
                else {
                    $("#fbicon").hide();
                    $("#fbiconleadgen").hide();
                }
            }
            else {
                $("#socialshares").hide();
                $("#social").hide();
                $("#twittericon").hide();
                $("#googleicon").hide();
                $("#fbicon").hide();
            }
            renderChart(data.answeroptions);
        }
    });
}

var arrColours = ['#AFD8F8', '#F6BD0F', '#8BBA00', '#A66EDD', '#F984A1', '#AFD8F8', '#F6BD0F', '#8BBA00', '#A66EDD', '#F984A1'];
var chartwidth = 0;
var thisrowpercentage = 0;
var thisrowwidth = 0;
var purejschartdata = "";
var baseFont = "";
var baseFontSize = "";
var baseFontBold = "0";
var baseFontItalic = "0";
var baseFontUnderline = "0";

var chartanswerrowcountnew = 0;
var adjustedchartwidth = 0;
var adjustedanswerpercentage = 0;

function renderChart(allanswers) {

    purejschartdata = "";
    if ($("#hdnPollSource").val() == "weblink") {
        chartwidth = parseInt($("#polldiv").css("width"));
    }
    else {
        chartwidth = parseInt($("#hdnWidth").val());
    }
    baseFontBold = $("#hdnAnswerBold").val();
    baseFontUnderline = $("#hdnAnswerUL").val();
    baseFontItalic = $("#hdnAnswerItalic").val()
    baseAlign = $("#hdnAnswerAlign").val()
    adjustedchartwidth = Math.round(chartwidth * 0.8);
    adjustedanswerpercentage = chartwidth - adjustedchartwidth - 20;
    baseFont = $("#hdnAnswerFont").val();
    var answercolour = $("#hdnAnswerColor").val();
    var answerbold = $("#hdnAnswerBold").val();
    var answeritalic = $("#hdnAnswerItalic").val();
    var answerUL = $("#hdnAnswerUL").val();
    var answerSize = $("#hdnAnswerFSize").val();
    var answerFont = $("#hdnAnswerFont").val();
    if (baseFont == "") {
        baseFont = "Arial";
    }
    baseFontSize = $("#hdnAnswerFSize").val();

    chartanswerrowcountnew = allanswers.length;


    for (var row1 = 0; row1 < chartanswerrowcountnew; row1++) {

        thisrowpercentage = parseInt(allanswers[row1].answerpercentage);
        thisrowwidth = Math.round(thisrowpercentage * chartwidth / 100);

        thisanswer = allanswers[row1].answertext;

        thisanswer = escape(thisanswer);
        while (thisanswer.indexOf("%22") >= 0) {
            thisanswer = thisanswer.replace("%22", "");
        }
        while (thisanswer.indexOf("%22") >= 0) {
            thisanswer = thisanswer.replace("%22", "");
        }

        while (thisanswer.indexOf("%3C") >= 0) {
            console.log(thisanswer);
            thisanswer = thisanswer.replace("%3C", "&lt;");
        }

        while (thisanswer.indexOf("%3E") >= 0) {
            thisanswer = thisanswer.replace("%3E", "&gt;");
        }
        thisanswer = unescape(thisanswer);
        while (thisanswer.indexOf("'") >= 0) {
            thisanswer = thisanswer.replace("'", "`");
        }
        thisrowpercentage = parseInt(allanswers[row1].answerpercentage);
        thisrowwidth = Math.round(thisrowpercentage * adjustedchartwidth / 100);
        purejschartdata += "<div style='width:100%;text-align:left;font-size:" + answerSize + "px;font-weight:" + answerbold + ";font-family:" + answerFont + ";color:" + answercolour + ";font-style:" + answeritalic + ";text-align:" + baseAlign + "'>" + thisanswer + "</div>";
        purejschartdata += "<div style='width:90%;float:left;background-color:lightgray;'>";
        purejschartdata += "<div style='height:15px;width:0%;background-color:" + arrColours[row1] + ";' id='div" + row1 + "'></div>";
        purejschartdata += "</div>";
        purejschartdata += "<div style='float:right;width:5%;vertical-align:top;font-size:14px;font-weight:normal;font-family:Arial;color:" + answercolour + ";font-style:" + answeritalic + ";'>" + thisrowpercentage + "%</div>";
        purejschartdata += "<div style='width:100%;height:22px;'></div>";
    }

    $('#polldiv').addClass("flipInYX animated");
    $('#purejschart').css("width", "92%");
    $('#purejschart').show();
    $('#purejschart').html(purejschartdata);
    $('#innerdiv, #mainbody').on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        //console.log(chartanswerrowcountnew);
        for (var x = 0; x < chartanswerrowcountnew; x++) {
            //console.log(chartanswerrowcountnew);
            thisrowpercentage = parseInt(allanswers[x].answerpercentage);
            if (thisrowpercentage > 100) {
                thisrowpercentage = 100;
            }
            thisrowwidth = Math.round(thisrowpercentage * adjustedchartwidth / 100);
            //console.log(thisrowpercentage.toString() + '--' + thisrowwidth.toString());
            $("#div" + x.toString()).animate({
                backgroundColor: "red",
                color: "#fff",
                width: thisrowpercentage + "%"
            }, 1000);
            $("#div" + x.toString()).css("width", thisrowpercentage + "%");
            $("#div" + x.toString()).css("color", arrColours[x]);

        }
    });
}

