﻿$(document).ready(function () {
    var pollBg = $("#MainContent_hdnPollBgColor").val();
    var pollQuestion = $("#MainContent_hdnQuestColor").val();
    var pollQuestionBg = $("#MainContent_hdnQuestBgColor").val();
    var pollAnswer = $("#MainContent_hdnAnswerColor").val();
    var pollVote = $("#MainContent_hdnVoteColor").val();
    var pollVoteBg = $("#MainContent_hdnVoteBgColor").val();
    $(".commentstxt").css("color", "#" + pollAnswer);
    $(".sponsorimage").css("color", "#" + pollAnswer);
    $(".textpollmanthemeresponse").css("background-color", "#" + pollBg);
    $(".questionpreviewcss").css("background-color", "#" + pollQuestionBg);
    $(".questionpreviewcss").css("color", "#" + pollQuestion);
    $(".radiopolltheme").css("color", "#" + pollAnswer);
    $(".viewandvoteimagepreview").css("background-color", "#" + pollVoteBg);
    $(".viewandvoteimagepreview").css("color", "#" + pollVote);
    var questweight = $("#MainContent_hdnQuestionBold").val();
    var answerweight = $("#MainContent_hdnAnswerBold").val();
    var voteweight = $("#MainContent_hdnVoteBold").val();
    var msgweight = $("#MainContent_hdnMsgBold").val();
    var queststyle = $("#MainContent_hdnQuestionItalic").val();
    var answerstyle = $("#MainContent_hdnAnswerItalic").val();
    var votestyle = $("#MainContent_hdnVoteItalic").val();
    var msgstyle = $("#MainContent_hdnMsgItalic").val();
    var questul = $("#MainContent_hdnQuestionUL").val();
    var answerul = $("#MainContent_hdnAnswerUL").val();
    var voteul = $("#MainContent_hdnVoteUL").val();
    var msgul = $("#MainContent_hdnMsgUL").val();
    var questalign = $("#MainContent_hdnQuestionAlign").val();
    var answeralign = $("#MainContent_hdnAnswerAlign").val();
    var votealign = $("#MainContent_hdnVoteAlign").val();
    var msgalign = $("#MainContent_hdnMsgAlign").val();
    var questsize = $("#MainContent_hdnQuestionFSize").val();
    var answersize = $("#MainContent_hdnAnswerFSize").val();
    var votesize = $("#MainContent_hdnVoteFSize").val();
    var msgsize = $("#MainContent_hdnMsgFSize").val();
    var height = $("#MainContent_hdnHeight").val(); 
    var width = $("#MainContent_hdnWidth").val();
    $(".textpollmanthemeresponse").css("width", width + 'px');
    $(".pollmanagerpreview").css("width", width + 'px');
    var manwidth = $(".pollmanagerpreview").width() + 50;
    $(".pollmanagerleftpanel").css("width", manwidth + 'px');
    var comwidth = $(".pollmanagerpreview").width() - 30;
    $(".txtcomments").css("width", comwidth + 'px');
    var answidth = $(".pollmanagerpreview").width() - 20;
    $(".wrapman").css("width", answidth + 'px');
    var heightpollman = Math.round(height) + 110;
    $(".pollmanheight").css("min-height", heightpollman + 'px');
    var heightrec = $("#MainContent_hdnRecHeight").val();
    if (height < heightrec || heightrec == "") {
        $(".textpollmanthemeresponse").css("height", height + 'px');
        $(".poweredby").css("position", '');
        $(".viewtest").css("position", '');
        $(".viewtest").css("bottom", '');
        $(".poweredby").css("bottom", '');
        $(".poweredby").css("margin-top", '5px');
        $(".poweredby").css("padding-bottom", '10px');
    }
    else {
        $(".textpollmanthemeresponse").css("height", height + 'px');
        $(".poweredby").css("position", 'absolute');
        $(".poweredby").css("bottom", '10px');
        $(".poweredby").css("text-align", 'center');
        $(".poweredby").css("width", '100%');
        $(".poweredby").css("margin-top", '0px');
        $(".poweredby").css("margin-bottom", '0px');
        $(".poweredby").css("padding-bottom", '0px');
        $(".viewtest").css("position", 'absolute');
        $(".viewtest").css("padding-bottom", '0px');
        $(".sponsorimagepreview").css("margin-top", '0px');
        $(".viewtest").css("bottom", '35px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '80%');
        }
        else {
            $(".viewtest").css("width", '100%');
        }
    }
    var questfont = $("#MainContent_hdnQuestionFont").val();
    var answerfont = $("#MainContent_hdnAnswerFont").val();
    $('.questionpreviewcss').css({ 'font-weight': questweight });
    $('.questionpreviewcss').css({ 'font-family': questfont });
    $('.radiopolltheme').css({ 'font-weight': answerweight });
    $('.viewandvoteimagepreview').css({ 'font-weight': voteweight });
    $('.questionpreviewcss').css({ 'font-style': queststyle });
    $('.radiopolltheme').css({ 'font-style': answerstyle });
    $('.viewandvoteimagepreview').css({ 'font-style': votestyle });
    $('.questionpreviewcss').css({ 'text-decoration': questul });
    $('.radiopolltheme').css({ 'text-decoration': answerul });
    $('.radiopolltheme').css({ 'font-family': answerfont });
    $('.viewandvoteimagepreview').css({ 'text-decoration': voteul });
    $('.questionpreviewcss').css({ 'text-align': questalign });
    $('.radiopolltheme').css({ 'text-align': answeralign });
    $('.viewandvoteimagepreview').css({ 'text-align': votealign });
    $('.questionpreviewcss').css({ 'font-size': questsize + 'px' });
    $('.radiopolltheme').css({ 'font-size': answersize + 'px' });
    $('.viewandvoteimagepreview').css({ 'font-size': votesize + 'px' });
    $(".poweredby").css("color", "#" + pollAnswer);
    $(".poweredby").css("font-family", answerfont);
    $(".poweredby").css("font-size", answersize + 'px');
    $(".poweredby a").css("color", "#" + pollAnswer);
    if ($('#MainContent_sponsorpreviw').is(':hidden')) {
        $('.viewandvoteimagepreview').css({ 'width': '100%' });
        $('.viewandvoteimagepreview').css({ 'margin-top': '10px' });
        $('.viewandvoteimagepreview').css({ 'margin-left': '0px' });
        $('.view').css({ 'font-size': votesize + 'px' });
        $('.view').css({ 'text-align': 'left' });
        $('.vote').css({ 'text-align': 'right' });
        $('.vote').css({ 'width': '34%' });
        $('.viewtest').css({ 'margin-left': '10%' });
        $('.viewtest').css({ 'margin-right': '10%' });
    }
    else {
        $('.viewandvoteimagepreview').css({ 'width': '45%' });
        $('.viewandvoteimagepreview').css({ 'float': 'left' });
        $('.viewandvoteimagepreview').css({ 'margin-left': '10px' });
        $('.viewandvoteimagepreview').css({ 'margin-top': '6px' });
        $('.view').css({ 'font-size': '10px' });
        $('.view').css({ 'text-align': 'center' });
        $('.vote').css({ 'text-align': 'center' });
        $('.vote').css({ 'width': '100%' });
    }
});

