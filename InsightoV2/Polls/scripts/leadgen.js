﻿function submitResponse() {

    if (!$("#phonenumber").is(":hidden")) {
        var phonenumber = $("#phonenumber").val();
        if (phonenumber == "") {
            Materialize.toast("Enter your mobile number", 2000);
            return;
        }
        if (isNaN(parseInt(phonenumber)) || (phonenumber == "")) {
            Materialize.toast("Enter only numbers for mobile", 2000);
            return;
        }
        if (phonenumber.length < 10) {
            Materialize.toast("Enter a valid mobile number", 2000);
            return;
        }
    }

    if (!$("#email").is(":hidden")) {
        var email = $("#email").val();
        if (email == "") {
            Materialize.toast("Enter your email", 2000);
            return;
        }
        if ((!emailCheck(email)) || (email == "")) {
            Materialize.toast("Enter a valid email", 2000);
            return;
        }
    }

    $.ajax({
        type: "GET",
        url: "/Polls/AjaxService.aspx?method=SavePollInteraction",
        method: "POST",
        data: { pollid: $("#hdnPollID").val(), respondentid: $("#hdnRespondentId").val(), phone: $("#phonenumber").val(), email: $("#email").val(), redirect: $("#hdnHasRedirectUrl").val() },
        done: function (data) {
        }
    });
    $("#btnSubmitLeadGen").hide();

    $("#messagebox").css("display", "block");
    $("#invitationmessagepreview,#contactdetailscollectiondiv").css("display", "none");

    setTimeout(function () {
        if ($("#hdnHasRedirectUrl").val() == "1") {
            var url = window.open(unescape($("#hdnRedirectUrl").val()), "_blank");
        }
    }, 2000);

}

function submitResponseEmbedOrSlider() {

    if (!$("#divphonetxt").is(":hidden")) {
        var phonenumber = $("#phonenumber").val();
        if (phonenumber == "") {
            alert("Enter your phone number");
            return;
        }
        if (isNaN(parseInt(phonenumber)) || (phonenumber == "")) {
            alert("Enter only numbers for phone");
            return;
        }
        if (phonenumber.length < 10) {
            alert("Enter a valid mobile number");
            return;
        }
    }

    if (!$("#divemailtxt").is(":hidden")) {
        var email = $("#email").val();
        if (email == "") {
            alert("Enter your email");
            return;
        }
        if ((!emailCheck(email)) || (email == "")) {
            alert("Enter a valid email");
            return;
        }
    }

    $.ajax({
        type: "GET",
        url: "/Polls/AjaxService.aspx?method=SavePollInteraction",
        method: "POST",
        data: { pollid: $("#hdn").val(), respondentid: $("#hdnRespondentId").val(), phone: $("#phonenumber").val(), email: $("#email").val(), redirect: $("#hdnHasRedirectUrl").val() },
        success: function (data) {
            $("#thankyouoverlay").css("position", "absolute");
            //$("#thankyouoverlay").css("top", "0");
            $("#leadgendiv").html("");
            $("#leadgendiv").css("min-height", "38vh");
            $("#leadgendiv").css("height", "38vh");
            $("#thankyouoverlay").css("padding-top", "0 !important");
            $("#thankyouoverlay").css("margin", "0 auto");
            //$("#thankyouoverlay").css("margin-top", $("#leadgendiv").offset().top + "px");
            $("#thankyouoverlay").css("top", $("#leadgendiv").offset().top +20 + "px");
            $("#thankyouoverlay").css("min-width", "60% !important");
            $("#thankyouoverlay").css("width", "100%");
            $("#thankyouoverlay").css("height", "auto");
            $("#thankyouoverlay").css("background-color", $("#hdnVoteBgColor").val());
            $("#thankyouoverlay").css("color", $("#hdnVoteColor").val());
            $("#thankyouoverlay").css("font-family", $("#hdnAnswerFont").val());
            $("#thankyouoverlay").css("font-size", $("#hdnAnswerFSize").val());
            $("#thankyouoverlay").css("font-weight", $("#hdnAnswerBold").val());
            $("#thankyouoverlay").css("font-style", $("#hdnAnswerItalic").val());
            $("#thankyouoverlay").css("text-decoration", $("#hdnAnswerUL").val());
            $("#thankyouoverlay").show();
        }
    });

    setTimeout(function () {
        if ($("#hdnHasRedirectUrl").val() == "1") {
            var url = window.open(unescape($("#hdnRedirectUrl").val()), "_blank");
        }
    }, 2000);

}

function goToRedirectURL() {
    var redurl = escape($("#redirecturl").val());
    window.location.href = redurl;
}

//email verify start
function emailCheck(str) {
    var at = "@";
    var dot = ".";
    var lat = str.indexOf(at);
    var lstr = str.length;
    var ldot = str.indexOf(dot);
    if ((str.indexOf(at) == -1) || (str.indexOf(dot) == -1)) {
        return (false);
    }

    return true;
}