﻿var singleansweroptionheight = 5;
var sponsorlogooptionheight = 60;
var commentboxheight = 60;
var showSponsor = false;

$(document).ready(function () {
    $("#leftactive").hide();
    $("#centeractive").hide();
    $("#rightactive").hide();
    $("#answerleftactive").hide();
    $("#answercenteractive").hide();
    $("#answerrightactive").hide();
    $("#voteleftactive").hide();
    $("#votecenteractive").hide();
    $("#voterightactive").hide();
    $("#msgleftactive").hide();
    $("#msgcenteractive").hide();
    $("#msgrightactive").hide();
    $("#questionalign").hide();
    $("#answeralign").hide();
    $("#votealign").hide();
    $("#msgalign").hide();
    $("#fontsize").hide();
    $("#afontsize").hide();
    $("#vfontsize").hide();
    $("#mfontsize").hide();

    var fixheight = $("#MainContent_txtHeightPoll").val();
    var heightrec = $("#MainContent_hdnRecHeightne").val();

    calculateIFrameHeight();
    $(".textpollthemepreviewresults").css("height", fixheight + 'px');
    $(".textpollthemepreview").css("height", fixheight + 'px');
    var ansheight = parseInt(fixheight) - 125;
    if ($('#MainContent_poweredby').is(':hidden')) {
        $(".viewtest").css("bottom", '10px');
        $(".newsponsocialdiv").css("bottom", '10px');
        ansheight = parseInt(ansheight) + 25;
    }
    else {
        $(".viewtest").css("bottom", '30px');
        $(".newsponsocialdiv").css("bottom", '30px');
    }
    $(".sponsorimagepreview").css("margin-top", '3px');
    if ($('#MainContent_sponsorpreviw').is(':hidden')) {
        $(".viewtest").css("width", '70%');
    }
    else {
        $(".viewtest").css("width", '100%');
        ansheight = parseInt(ansheight) - 20;
    }
    if ($('#MainContent_videopreviewcreate').is(':hidden')) {
        if ($('#MainContent_imagepreview').is(':hidden')) {

        }
        else {
            var imgheight = $('#MainContent_imagepreview').height();
            if (imgheight == 0) {
                imgheight = $('#MainContent_hdnImageHeight').val();
            }
            ansheight = parseInt(ansheight) - parseInt(imgheight);

        }
    }
    else {
        var width = $("#MainContent_txtWidthPoll").val();
        var valueHeight = Math.round((width / 16) * 9);
        ansheight = parseInt(ansheight) - parseInt(valueHeight);
    }
    $("#MainContent_hdnansHeight").val(ansheight);
    $(".mainanswer").css("height", ansheight + 'px');
    var count = 0;
    if ($("#MainContent_ckbVotes").attr("checked")) {
        $("#MainContent_votescount").show();
    }
    else {
        $("#MainContent_votescount").hide();
    }
    if ($("#MainContent_ckbTwitter").attr("checked")) {
        $("#MainContent_twitter").show();
        $("#MainContent_twittericon").show();
        $("#MainContent_twittericonvote").show();
        $("#MainContent_social").show();
        $("#MainContent_socialsha").show();
        count++;
    }
    else {
        $("#MainContent_twitter").hide();
        $("#MainContent_twittericon").hide();
        $("#MainContent_twittericonvote").hide();
    }
    if ($("#MainContent_ckbGoogle").attr("checked")) {
        $("#MainContent_google").show();
        $("#MainContent_googleicon").show();
        $("#MainContent_social").show();
        $("#MainContent_socialsha").show();
        $("#MainContent_googleiconvote").show();
        count++;
    }
    else {
        $("#MainContent_google").hide();
        $("#MainContent_googleicon").hide();
        $("#MainContent_googleiconvote").hide();
    }
    if ($("#MainContent_ckbFb").attr("checked")) {
        $("#MainContent_fb").show();
        $("#MainContent_fbicon").show();
        $("#MainContent_fbiconvote").show();
        $("#MainContent_social").show();
        $("#MainContent_socialsha").show();
        count++;
    }
    else {
        $("#MainContent_fb").hide();
        $("#MainContent_fbicon").hide();
        $("#MainContent_fbiconvote").hide();
    }

    var imagenew = $("#MainContent_hdnImageLink").val();
    if (imagenew == "") {

    }
    else {
        $(".imagetrans").css("color", 'transparent');
    }
    var imagelo = $("#MainContent_hdnLogoLink").val();
    if (imagelo == "") {

    }
    else {
        $(".imagetranslogo").css("color", 'transparent');
    }
    var widthpoll = $("#MainContent_txtWidthPoll").val();
    var imgrecheight = $("#MainContent_hdnImageRecHeight").val();
    if (imgrecheight != "") {
        $("#MainContent_lblImageHeight").text(imgrecheight);
    }
    if (widthpoll > 380) {
        $(".Previewres").css("float", 'left');
        $(".Previewres").css("padding-top", '10px');
        $(".createpollnew").css("float", 'left');
        if ($('#MainContent_launch').is(':hidden')) {
        }
        else {

        }
    }
    else {
        $(".Previewres").css("padding-left", '');
        $(".Previewres").css("padding-top", '');
    }

    var comwidth = widthpoll - 30;
    $(".txtcomments").css("width", comwidth + 'px');
    var answidth = widthpoll - 50;
    $(".wrapnew").css("width", answidth + 'px');
    var questiontext = $("#MainContent_txtQuestion").val();
    $(".textpollthemepreview").css("width", widthpoll + 'px');
    $(".Preview").css("width", widthpoll + 'px');
    $(".Previewres").css("width", widthpoll + 'px');
    $(".textpollthemepreviewresults").css("width", widthpoll + 'px');
    $(".videopreviewcreate").css("width", widthpoll + 'px');
    if ($('#MainContent_sponsorpreviw').is(':hidden')) {
        $('.viewandvoteimagepreview').css({ 'width': '100%' });
        $('.viewandvoteimagepreview').css({ 'margin-left': '0px' });
        $('.viewandvoteimagepreview').css({ 'margin-top': '10px' });
        $('.socialnew1').css({ 'margin-bottom': '0px' });
        if (count == 1) {
            ////$('.social').css({ 'margin-left': '10%' });
            $('.newvotecount').css({ 'position': 'absolute' });
            $('.newvotecount').css({ 'padding-top': '3px' });
            $('.socialnew1').css({ 'margin-bottom': '-15px' });
            $('.social').css({ 'margin-top': '15px' });
        }
        if (count == 2) {
            ////$('.social').css({ 'margin-left': '5%' });
            $('.newvotecount').css({ 'position': 'absolute' });
            $('.newvotecount').css({ 'padding-top': '3px' });
            $('.socialnew1').css({ 'margin-bottom': '-15px' });
            $('.social').css({ 'margin-top': '15px' });
        }
        if (count == 3) {
            //$('.social').css({ 'margin-left': '1%' });
            $('.newvotecount').css({ 'position': 'absolute' });
            $('.socialnew1').css({ 'margin-bottom': '-15px' });
            $('.social').css({ 'margin-top': '15px' });
            $('.newvotecount').css({ 'padding-top': '3px' });
        }
        $('.view').css({ 'text-align': 'left' });
        $('.vote').css({ 'text-align': 'right' });
        $('.vote').css({ 'width': '30%' });
        $('.viewtest').css({ 'margin-left': '15%' });
        $('.viewtest').css({ 'margin-right': '15%' });
        $('.viewtest').css({ 'width': '70%' });
        $("#MainContent_sponsorResults").hide();
        $('.viewtest').css({ 'padding-top': '10px' });
        $('.view').css({ 'font-size': '13px' });
    }
    else {
        $('.newvotecount').css({ 'position': 'absolute' });
        $('.newvotecount').css({ 'padding-top': '3px' });
        $('.socialnew1').css({ 'margin-bottom': '-15px' });
        if ($('#MainContent_social').is(':hidden')) {
            $(".sponsorimagepreviewres").css("width", '100%');
            $(".sponsorimagepreviewres").css("margin", 'auto');
            $(".sponsorimagepreviewres").css("margin-right", '0px');
        }
        else {
            $(".sponsorimagepreviewres").css("width", 'auto');
            $("#MainContent_socialsha").show();
        }
        $('.viewandvoteimagepreview').css({ 'width': '45%' });
        $('.viewandvoteimagepreview').css({ 'float': 'left' });
        $(".social").css("margin-top", '15px');
        $('.socialnew').css({ 'width': '31%' });
        $('.socialnew').css({ 'margin-left': '0px' });
        $('.socialnew').css({ 'margin-bottom': '-15px' });
        $('.viewandvoteimagepreview').css({ 'margin-left': '10px' });
        $('.viewandvoteimagepreview').css({ 'margin-top': '6px' });
        $('.viewtest').css({ 'padding-top': '10px' });
        $("#MainContent_sponsorResults").show();
        $('.view').css({ 'font-size': '10px' });
        $('.view').css({ 'text-align': 'center' });
        $('.vote').css({ 'text-align': 'center' });
        $('.vote').css({ 'width': '100%' });
        if ($('#MainContent_poweredby').is(':hidden')) {
            $('.sponsorimagepreviewres').css({ 'bottom': '10px' });
            $('.newsponsocialdiv').css({ 'bottom': '10px' });
        }
        else {
            $('.sponsorimagepreviewres').css({ 'bottom': '30px' });
        }
    }
    heightrec = $("#MainContent_hdnRecHeightne").val();
    $("#MainContent_lblRecThemeHeight").text(heightrec);
    if ($("#MainContent_ckbResultsPrivate").attr("checked")) {
        $("#MainContent_ResultBox").show();
        $("#MainContent_results").hide();
        $("#MainContent_resulttextpreview").show();
        var height = $(".textpollthemepreview").height();
        $(".textpollthemepreviewresults").css("height", height + 'px');
        $(".view").css("display", 'none');
        $(".vote").css("text-align", 'center');
        $(".vote").css("width", '100%');
        $(".viewandvoteimagepreview").css("height", '16px');
    }
    else {

    }
    if ($("#MainContent_radMultipleAnswer").attr("checked")) {
        $(".multipleanswers").show();
        $(".singleanswers").hide();
    }
    if ($("#MainContent_chkComments").attr("checked")) {
        $("#MainContent_commentbox").show();
    }
    else {
        $("#MainContent_commentbox").hide();
    }
    var pollBg = $("#MainContent_txtPollBgColor").val();
    var pollQuestion = $("#MainContent_txtQuestionColor").val();
    var pollQuestionBg = $("#MainContent_txtQuestionBgColor").val();
    var pollAnswer = $("#MainContent_txtAnswerColor").val();
    var pollVote = $("#MainContent_txtVoteColor").val();
    var pollVoteBg = $("#MainContent_txtVoteBgColor").val();
    var pollMessageBg = $("#MainContent_txtMessageBgColor").val();
    var pollMessage = $("#MainContent_txtMessageColor").val();
    var questionfont = $("#MainContent_ddlQuestionFont").val();
    var answerfont = $("#MainContent_ddlAnswerFont").val();
    var votefont = $("#MainContent_ddlVoteFont").val();
    var msgfont = $("#MainContent_ddlMsgFont").val();
    $(".poweredby").css("color", "#" + pollAnswer);
    $(".poweredby a").css("color", "#" + pollAnswer);
    $(".poweredbyrep").css("color", "#" + pollAnswer);
    $(".poweredbyrep a").css("color", "#" + pollAnswer);
    $(".resultpreview").css("color", "#" + pollMessage);
    $(".resultpreview").css("background-color", "#" + pollMessageBg);
    $('.questionpreviewcss').css({ 'font-family': questionfont });
    $('.radiopolltheme').css({ 'font-family': answerfont });
    $('.commentstxt').css({ 'font-family': answerfont });
    $('.poweredby').css({ 'font-family': answerfont });
    $('.poweredbyrep').css({ 'font-family': answerfont });
    $('.sponsorimagepreview').css({ 'font-family': answerfont });
    $('.sponsorimagepreviewres').css({ 'font-family': answerfont });
    $('.sponsorimagepreviewres1').css({ 'font-family': answerfont });
    $('.socialnew').css({ 'font-family': answerfont });
    $('.socialnew1').css({ 'font-family': answerfont });
    $('.viewandvoteimagepreview').css({ 'font-family': votefont });
    $(".textpollthemepreview").css("background-color", "#" + pollBg);
    $(".textpollthemepreviewresults").css("background-color", "#" + pollBg);
    $("#colorbgpoll").css("background-color", "#" + pollBg);
    $("#bgcolorquestion").css("background-color", "#" + pollQuestionBg);
    $("#colorquestion").css("background-color", "#" + pollQuestion);
    $(".questionpreviewcss").css("background-color", "#" + pollQuestionBg);
    $(".questionpreviewcss").css("color", "#" + pollQuestion);
    $("#coloranswer").css("background-color", "#" + pollAnswer);
    $(".radiopolltheme").css("color", "#" + pollAnswer);
    $('.commentstxt').css("color", "#" + pollAnswer);
    $('.poweredby').css("color", "#" + pollAnswer);
    $('.poweredbyrep').css("color", "#" + pollAnswer);
    $('.sponsorimagepreview').css("color", "#" + pollAnswer);
    $('.sponsorimagepreviewres').css("color", "#" + pollAnswer);
    $('.sponsorimagepreviewres1').css("color", "#" + pollAnswer);
    $('.socialnew').css("color", "#" + pollAnswer);
    $('.socialnew1').css("color", "#" + pollAnswer);
    $("#bgcolorvote").css("background-color", "#" + pollVoteBg);
    $("#colorvote").css("background-color", "#" + pollVote);
    $("#colorbgmessage").css("background-color", "#" + pollMessageBg);
    $("#colormessage").css("background-color", "#" + pollMessage);
    $(".viewandvoteimagepreview").css("background-color", "#" + pollVoteBg);
    $(".viewandvoteimagepreview").css("color", "#" + pollVote);
    var questweight = $("#MainContent_hdnQuestionBold").val();
    var answerweight = $("#MainContent_hdnAnswerBold").val();
    var voteweight = $("#MainContent_hdnVoteBold").val();
    var msgweight = $("#MainContent_hdnMsgBold").val();
    var queststyle = $("#MainContent_hdnQuestionItalic").val();
    var answerstyle = $("#MainContent_hdnAnswerItalic").val();
    var votestyle = $("#MainContent_hdnVoteItalic").val();
    var msgstyle = $("#MainContent_hdnMsgItalic").val();
    var questul = $("#MainContent_hdnQuestionUL").val();
    var answerul = $("#MainContent_hdnAnswerUL").val();
    var voteul = $("#MainContent_hdnVoteUL").val();
    var msgul = $("#MainContent_hdnMsgUL").val();
    var questalign = $("#MainContent_hdnQuestionAlign").val();
    var answeralign = $("#MainContent_hdnAnswerAlign").val();
    var votealign = $("#MainContent_hdnVoteAlign").val();
    var msgalign = $("#MainContent_hdnMsgAlign").val();
    var questsize = $("#MainContent_hdnQuestionFSize").val();
    var answersize = $("#MainContent_hdnAnswerFSize").val();
    var votesize = $("#MainContent_hdnVoteFSize").val();
    var msgsize = $("#MainContent_hdnMsgFSize").val();
    $('.resultpreview').css({ 'font-weight': msgweight });
    $('.resultpreview').css({ 'font-style': msgstyle });
    $('.resultpreview').css({ 'text-decoration': msgul });
    $('.resultpreview').css({ 'text-align': msgalign });
    $('.resultpreview').css({ 'font-size': msgsize + 'px' });
    $('.resultpreview').css({ 'font-family': msgfont });
    $('.questionpreviewcss').css({ 'font-weight': questweight });
    $('.radiopolltheme').css({ 'font-weight': answerweight });
    $('.viewandvoteimagepreview').css({ 'font-weight': voteweight });
    $('.questionpreviewcss').css({ 'font-style': queststyle });
    $('.radiopolltheme').css({ 'font-style': answerstyle });
    $('.viewandvoteimagepreview').css({ 'font-style': votestyle });
    $('.questionpreviewcss').css({ 'text-decoration': questul });
    $('.radiopolltheme').css({ 'text-decoration': answerul });
    $('.viewandvoteimagepreview').css({ 'text-decoration': voteul });
    $('.questionpreviewcss').css({ 'text-align': questalign });
    $('.radiopolltheme').css({ 'text-align': answeralign });
    $('.viewandvoteimagepreview').css({ 'text-align': votealign });
    $('.questionpreviewcss').css({ 'font-size': questsize + 'px' });
    $('.radiopolltheme').css({ 'font-size': answersize + 'px' });
    $('.socialnew').css({ 'font-size': answersize + 'px' });
    $('.socialnew1').css({ 'font-size': answersize + 'px' });
    $('.sponsorimagepreviewres').css({ 'font-size': answersize + 'px' });
    $('.sponsorimagepreviewres1').css({ 'font-size': answersize + 'px' });
    $('.sponsorimagepreview').css({ 'font-size': answersize + 'px' });
    $('.viewandvoteimagepreview').css({ 'font-size': votesize + 'px' });
    $('.vote').css({ 'font-size': votesize + 'px' });
    if ($('#MainContent_sponsorpreviw').is(':hidden')) {
        $('.view').css({ 'font-size': votesize + 'px' });
    }
    else {
        $('.view').css({ 'font-size': '10px' });
    }
    var ans0 = $("#txtAnswerOption_0").val();
    var ans1 = $("#txtAnswerOption_1").val();
    var ans2 = $("#txtAnswerOption_2").val();
    var ans3 = $("#txtAnswerOption_3").val();
    var ans4 = $("#txtAnswerOption_4").val();
    var ans5 = $("#txtAnswerOption_5").val();
    var ans6 = $("#txtAnswerOption_6").val();
    var ans7 = $("#txtAnswerOption_7").val();
    var ans8 = $("#txtAnswerOption_8").val();
    var ans9 = $("#txtAnswerOption_9").val();
    var videolink = $("#MainContent_txtVideo").val();
    if (videolink != null && videolink != "") {
        $(".videopreviewcreate").html(videolink);
        $("#MainContent_videopreviewcreate").show();
        $("#MainContent_videoResults").show();
        $('#MainContent_imagepreview').hide();
        $('#MainContent_imgResults').hide();
    }
    else if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/video.jpg') {
        var heightvideo = $("#MainContent_imgPreview").height();
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $('#MainContent_imgPreview').attr('height', valueHeight);
        $('#MainContent_imgPreview').attr('width', '100%');
        $('#MainContent_imgPreviewResults').attr('height', valueHeight);
        $('#MainContent_imgPreviewResults').attr('width', '100%');
    }
    if (ans0 == "" || ans0 == undefined) {
    }
    else {
        document.getElementById("lblradText_0").innerHTML = stripSpecialChars(ans0);
    }
    if (ans1 == "" || ans1 == undefined) {

    }
    else {
        document.getElementById("lblradText_1").innerHTML = stripSpecialChars(ans1);
    }
    if (ans2 == "" || ans2 == undefined) {
    }
    else {
        document.getElementById("lblradText_2").innerHTML = stripSpecialChars(ans2);
    }
    if (ans3 == "" || ans3 == undefined) {
    }
    else {
        document.getElementById("lblradText_3").innerHTML = stripSpecialChars(ans3);
    }
    if (ans4 == "" || ans4 == undefined) {
    }
    else {
        document.getElementById("lblradText_4").innerHTML = stripSpecialChars(ans4);
    }
    if (ans5 == "" || ans5 == undefined) {
    }
    else {
        document.getElementById("lblradText_5").innerHTML = stripSpecialChars(ans5);
    }
    if (ans6 == "" || ans6 == undefined) {
    }
    else {
        document.getElementById("lblradText_6").innerHTML = stripSpecialChars(ans6);
    }
    if (ans7 == "" || ans7 == undefined) {
    }
    else {
        document.getElementById("lblradText_7").innerHTML = stripSpecialChars(ans7);
    }
    if (ans8 == "" || ans8 == undefined) {
    }
    else {
        document.getElementById("lblradText_8").innerHTML = stripSpecialChars(ans8);
    }
    if (ans9 == "" || ans9 == undefined) {
    }
    else {
        document.getElementById("lblradText_9").innerHTML = stripSpecialChars(ans9);
    }
    if (questiontext == "") {
        if ($("#MainContent_ckbVotes").attr("checked")) {
            $("#MainContent_lblQuestion").text('Question?');
            $("#MainContent_lblQuestionResults").text('Question? (39 votes)');
        }
        else {
            $("#MainContent_lblQuestion").text('Question?');
            $("#MainContent_lblQuestionResults").text('Question?');
        }
    }
    else {
        if ($("#MainContent_ckbVotes").attr("checked")) {
            $("#MainContent_lblQuestion").text(questiontext);
            $("#MainContent_lblQuestionResults").text(questiontext + ' ' + '(39 votes)');
        }
        else {
            $("#MainContent_lblQuestion").text(questiontext);
            $("#MainContent_lblQuestionResults").text(questiontext);
        }
    }

    heightcon = $(".textpollthemepreview").height() + 90;
    $(".contentPanelPollwithoutborder").css("min-height", heightcon);
    var resulttextpre = $("#MainContent_txtResultText").val();
    $("#MainContent_lblResultText").text(resulttextpre);

    $(".btn").click(function () {
        if ($('#questionalign').is(':hidden')) {
            $("#questionalign").show();
            return false;
        }
        else {
            $("#questionalign").hide();
            return false;
        }
    });
    $(".btnfontsize").click(function () {
        if ($('#fontsize').is(':hidden')) {
            $("#fontsize").show();
            return false;
        }
        else {
            $("#fontsize").hide();
            return false;
        }
    });
    $(".btnaalignment").click(function () {
        if ($('#answeralign').is(':hidden')) {
            $("#answeralign").show();
            return false;
        }
        else {
            $("#answeralign").hide();
            return false;
        }
    });
    $(".btnvalignment").click(function () {
        if ($('#votealign').is(':hidden')) {
            $("#votealign").show();
            return false;
        }
        else {
            $("#votealign").hide();
            return false;
        }
    });
    $(".btnmalignment").click(function () {
        if ($('#msgalign').is(':hidden')) {
            $("#msgalign").show();
            return false;
        }
        else {
            $("#msgalign").hide();
            return false;
        }
    });
    $(".fontsmall").click(function () {
        $("#MainContent_hdnQuestionFSize").val("12");
        $("#fontsize").hide();
        $('.questionpreviewcss').css({ 'font-size': '12px' });
    });
    $(".fontnormal").click(function () {
        $("#MainContent_hdnQuestionFSize").val("13");
        $("#fontsize").hide();
        $('.questionpreviewcss').css({ 'font-size': '13px' });
    });

    $(".btnafontsize").click(function () {
        if ($('#afontsize').is(':hidden')) {
            $("#afontsize").show();
            return false;
        }
        else {
            $("#afontsize").hide();
            return false;
        }
    });
    $(".afontsmall").click(function () {
        $("#MainContent_hdnAnswerFSize").val("12");
        $("#afontsize").hide();
        $('.radiopolltheme').css({ 'font-size': '12px' });
        $('.commentstxt').css({ 'font-size': '12px' });
        $('.poweredby').css({ 'font-size': '12px' });
        $('.poweredbyrep').css({ 'font-size': '12px' });
        $('.sponsorimagepreview').css({ 'font-size': '12px' });
        $('.sponsorimagepreviewres').css({ 'font-size': '12px' });
        $('.sponsorimagepreviewres1').css({ 'font-size': '12px' });
        $('.socialnew').css({ 'font-size': '12px' });
        $('.socialnew1').css({ 'font-size': '12px' });
        renderChart();
    });
    $(".afontnormal").click(function () {
        $("#MainContent_hdnAnswerFSize").val("13");
        $("#afontsize").hide();
        $('.radiopolltheme').css({ 'font-size': '13px' });
        $('.commentstxt').css({ 'font-size': '13px' });
        $('.poweredbyrep').css({ 'font-size': '13px' });
        $('.poweredby').css({ 'font-size': '13px' });
        $('.sponsorimagepreview').css({ 'font-size': '13px' });
        $('.sponsorimagepreviewres').css({ 'font-size': '13px' });
        $('.sponsorimagepreviewres1').css({ 'font-size': '13px' });
        $('.socialnew').css({ 'font-size': '13px' });
        $('.socialnew1').css({ 'font-size': '13px' });
        renderChart();
    });
    $(".btnvfontsize").click(function () {
        if ($('#vfontsize').is(':hidden')) {
            $("#vfontsize").show();
            return false;
        }
        else {
            $("#vfontsize").hide();
            return false;
        }
    });
    $(".vfontsmall").click(function () {
        $("#MainContent_hdnVoteFSize").val("13");
        $("#vfontsize").hide();
        $('.vote').css({ 'font-size': '13px' });
        $('.viewandvoteimagepreview').css({ 'font-size': '13px' });
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $('.view').css({ 'font-size': '13px' });
        }
        else {
            $('.view').css({ 'font-size': '10px' });
        }
    });
    $(".vfontnormal").click(function () {
        $("#MainContent_hdnVoteFSize").val("14");
        $("#vfontsize").hide();
        $('.viewandvoteimagepreview').css({ 'font-size': '14px' });
        $('.vote').css({ 'font-size': '14px' });
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $('.view').css({ 'font-size': '14px' });
        }
        else {
            $('.view').css({ 'font-size': '10px' });
        }
    });
    $(".btnmfontsize").click(function () {
        if ($('#mfontsize').is(':hidden')) {
            $("#mfontsize").show();
            return false;
        }
        else {
            $("#mfontsize").hide();
            return false;
        }
    });
    $(".mfontsmall").click(function () {
        $("#MainContent_hdnMsgFSize").val("12");
        $("#mfontsize").hide();
        $('.resultpreview').css({ 'font-size': '12px' });
    });
    $(".mfontnormal").click(function () {
        $("#MainContent_hdnMsgFSize").val("13");
        $("#mfontsize").hide();
        $('.resultpreview').css({ 'font-size': '13px' });
    });

    $("#MainContent_txtVoteText").bind('input', function () {
        var no = $('#MainContent_txtVoteText').val().length;
        if (parseInt(no) > 30) {
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            var designerName = $('#MainContent_txtVoteText').val();
            var nnn = designerName.substr(0, designerName.indexOf(' '));
            var nnnsdf = designerName.substr(designerName.indexOf(' ') + 1);
            var neww = designerName.split(" ")[0];
            if (nnn == "") {
                nnn = designerName;
                if (nnnsdf == "") {
                }
                else {
                    nnnsdf = "";
                }
            }
            $("#MainContent_customvote").text(nnn);
            var newr = designerName.split(" ")[1];
            $("#MainContent_customview").text(nnnsdf);
        }

    });
    $("#MainContent_txtSharePoll").bind('input', function () {
        var share = $('#MainContent_txtSharePoll').val();
        var no = $('#MainContent_txtSharePoll').val().length;
        if (parseInt(no) > 15) {
            jAlert('Exceeding maximum no. of characters', 'Activation Alert', function (r) {
                share = $("#MainContent_lblShare").text();
                $('#MainContent_txtSharePoll').val(share);
                return false;
            });
        }
        else {
            $("#MainContent_lblShare").text(share);
        }
    });
    $("#MainContent_txtQuestion").bind('input', function () {
        var title = $(this).val();
        if ($("#MainContent_ckbVotes").attr("checked")) {
            $("#MainContent_lblQuestion").text(title);
            $("#MainContent_lblQuestionResults").text(title + ' ' + '(39 votes)');
        }
        else {
            $("#MainContent_lblQuestion").text(title);
            $("#MainContent_lblQuestionResults").text(title);
        }
        $("#MainContent_txtQuestion").css("border-color", "#CDCDCD");
        return false;
    });

    $("#MainContent_txtResultText").bind('input', function () {
        var title = $(this).val();
        $("#MainContent_lblResultText").text(title);
        return false;
    });
    $("#MainContent_txtSponsorText").bind('input', function () {
        var title = $(this).val();
        $("#MainContent_lblSponsor").text(title);
        $("#MainContent_lblSponText").text(title);
        return false;
    });
    $("#MainContent_txtSponsorLink").bind('input', function () {
        var title = $(this).val();
        $("#MainContent_logolink").attr("href", title)
        return false;
    });
    $(".txtAns").live("input", function () {
        var id = this.id;
        var suffix = id.match(/\d+/);
        id = parseInt(suffix);
        var title = $(this).val();
        var labelid = "lblradText_" + id;
        
        document.getElementById(labelid).innerHTML = stripSpecialChars(title);
        $(".txtAns").css("border-color", "#cdcdcd");
    });
    $("#MainContent_DesignNew").click(function () {
        var ansno = $("#MainContent_hdnRepeaterTxts").val();
        ansno = parseInt(ansno) - 1;
        var i = parseInt(0);
        var count = 0;
        for (i = 0; i <= parseInt(ansno); i++) {
            var id = "#txtAnswerOption_" + i;
            if ($(id).val() == "" || $(id).val() == undefined) {

            }
            else {
                var j = i + 1;
                var jd = "#txtAnswerOption_" + j;
                for (j = 0; j < parseInt(ansno); j++) {
                    if ($(id).val() == $(jd).val()) {
                        $("#MainContent_lblAlreadyExist").text("Answer Options should not be same");
                        $("#answererr").show();
                        return false;
                    }

                }
                count = parseInt(count) + 1;
            }
        }
        if (parseInt(count) < 2) {
            $("#MainContent_lblAlreadyExist").text("Please add minimum answer options");
            $("#answererr").show();
            return false;
        }
    });
    $("#MainContent_LaunchNew").click(function () {
        var ansno = $("#MainContent_hdnRepeaterTxts").val();
        ansno = parseInt(ansno) - 1;
        var i = parseInt(0);
        var count = 0;
        for (i = 0; i <= parseInt(ansno); i++) {
            var id = "#txtAnswerOption_" + i;
            if ($(id).val() == "" || $(id).val() == undefined) {

            }
            else {
                var j = i + 1;
                var jd = "#txtAnswerOption_" + j;
                for (j = 0; j < parseInt(ansno); j++) {
                    if ($(id).val() == $(jd).val()) {
                        $("#MainContent_lblAlreadyExist").text("Answer Options should not be same");
                        $("#answererr").show();
                        return false;
                    }
                }
                count = parseInt(count) + 1;
            }
        }
        if (parseInt(count) < 2) {
            $("#MainContent_lblAlreadyExist").text("Please add minimum answer options");
            $("#answererr").show();
            return false;
        }
    });
    $("#MainContent_btnDesignSave").click(function () {
        if ($('#MainContent_SponsorBox').is(':hidden')) {
        }
        else {
            var files = $("#MainContent_sponsorLogo").get(0).files;
            if (files.length > 0) {
            }
            else {
                if ($('#MainContent_lblLogoName').text() == "") {
                    $("#MainContent_lblDesignError").text("Please upload logo image");
                    $("#designerror").show();
                    $('.imagetranslogo').css('color', 'red');
                    return false;
                }
            }
        }
    });
    $("#MainContent_btnSave").click(function () {
        if ($("#MainContent_txtQuestion").val() == "") {
            $("#MainContent_lblErrorJquery").text("Please enter question");
            $("#errorid").show();
            $("#MainContent_txtQuestion").css("border-color", "red");
            return false;
        }
        else {
            $("#errorid").hide();
        }
        if ($("#MainContent_txtWidthPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter width");
            $("#errorid1").show();
            $("#MainContent_txtWidthPoll").css("border-color", "red");
            return false;
        }
        else {
            $("#errorid1").hide();
        }
        if ($("#MainContent_txtHeightPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter height");
            $("#errorid1").show();
            $("#MainContent_txtHeightPoll").css("border-color", "red");
            return false;
        }
        else {
            $("#errorid1").hide();
        }
        if ($('#MainContent_videoDiv').is(':hidden')) {
            $("#errorid").hide();
        }
        else {
            if ($("#MainContent_txtVideo").val() == "") {
                $("#MainContent_lblErrorJquery").text("Please enter video code");
                $("#errorid").show();
                $("#MainContent_txtVideo").css("border-color", "red");
                return false;
            }
        }
        if ($('#MainContent_ImageDiv').is(':hidden')) {
        }
        else {
            var files = $("#MainContent_fuImage").get(0).files;
            if (files.length > 0) {
                $("#errorid").hide();
                $(".imagetrans").css("color", 'transparent');
            }
            else {
                if ($('#MainContent_lblImageName').text() == "") {
                    $("#MainContent_lblErrorJquery").text("Please upload image");
                    $("#errorid").show();
                    $(".imagetrans").css("color", 'red');
                    return false;
                }
            }
        }
        var ansno = $("#MainContent_hdnRepeaterTxts").val();
        ansno = parseInt(ansno) - 1;
        var i = parseInt(0);
        var count = 0;
        for (i = 0; i <= parseInt(ansno); i++) {
            var id = "#txtAnswerOption_" + i;
            if ($(id).val() == "" || $(id).val() == undefined) {

            }
            else {
                var j = i + 1;
                var jd = "#txtAnswerOption_" + j;
                for (j = 0; j < parseInt(ansno); j++) {
                    if ($(id).val() == $(jd).val()) {
                        $("#MainContent_lblAlreadyExist").text("Answer Options should not be same");
                        $("#answererr").show();
                        return false;
                    }
                }
                count = parseInt(count) + 1;
            }
        }
        if (parseInt(count) < 2) {
            $("#MainContent_lblAlreadyExist").text("Please add minimum answer options");
            $(".txtAns").css("border-color", "red");
            $("#answererr").show();
            return false;
        }
    });
    $("#MainContent_btnGenerateIframe").click(function () {
        if ($("#MainContent_txtQuestion").val() == "") {
            $("#MainContent_lblErrorJquery").text("Please enter question");
            $("#errorid").show();
            $("#MainContent_txtQuestion").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }
        if ($("#MainContent_txtWidthPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter width");
            $("#errorid1").show();
            $("#MainContent_txtWidthPoll").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }
        if ($("#MainContent_txtHeightPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter height");
            $("#errorid1").show();
            $("#MainContent_txtHeightPoll").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }

        $("#MainContent_create").show();
        if ($('#MainContent_videoDiv').is(':hidden')) {
            $("#errorid").hide();
        }
        else {
            if ($("#MainContent_txtVideo").val() == "") {
                $("#MainContent_lblErrorJquery").text("Please enter video code");
                $("#errorid").show();
                $("#MainContent_launcharrow").hide();
                $("#MainContent_launch").hide();
                $("#MainContent_create").show();
                $("#MainContent_createarrow").show();
                $("#MainContent_txtVideo").css("border-color", "#CDCDCD");
                return false;
            }
        }
        if ($('#MainContent_ImageDiv').is(':hidden')) {
        }
        else {
            var files = $("#MainContent_fuImage").get(0).files;
            if (files.length > 0) {
                $("#errorid").hide();
                $(".imagetrans").css("color", 'transparent');
            }
            else {
                if ($('#MainContent_lblImageName').text() == "") {
                    $("#MainContent_lblErrorJquery").text("Please upload image");
                    $("#errorid").show();
                    $("#MainContent_launcharrow").hide();
                    $("#MainContent_launch").hide();
                    $("#MainContent_create").show();
                    $("#MainContent_createarrow").show();
                    $(".imagetrans").css("color", 'red');
                    return false;
                }
            }
        }
        $("#MainContent_create").hide();
        $("#MainContent_design").show();
        if ($('#MainContent_SponsorBox').is(':hidden')) {
        }
        else {
            var files = $("#MainContent_sponsorLogo").get(0).files;
            if (files.length > 0) {
            }
            else {
                if ($('#MainContent_lblLogoName').text() == "") {
                    $("#MainContent_lblDesignError").text("Please upload logo image");
                    $("#designerror").show();
                    $('.imagetranslogo').css('color', 'red');
                    $("#MainContent_launcharrow").hide();
                    $("#MainContent_launch").hide();
                    $("#MainContent_design").show();
                    $("#MainContent_designarrow").show();
                    return false;
                }
            }
        }
        $("#MainContent_design").hide();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightmin = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightmin = iframeheight;
        console.log($("#MainContent_hdnPollID").val());
        console.log($("#MainContent_hdnParentPollID").val());
        if (parseInt(fixheight) < parseInt(heightmin)) {
            jAlert('Minimum height for this poll should be ' + heightmin, 'Activation Alert', function (r) {
                return false;
            });
            return false;
        }
        if (($("#MainContent_hdnPollID").val() != $("#MainContent_hdnParentPollID").val()) && ($("#MainContent_hdnParentPollID").val() != "Active")) {
            if (jConfirm('Once you launch this replaced poll, the original poll will be automatically closed and in its place this poll will be displayed.\nAre you sure you want to proceed?', 'Replace poll')) {
                console.log('true');
            }
            else {
                console.log('false');
            }
            //if (!jConfirm('Once you launch this replaced poll, the original poll will be automatically closed and in its place this poll will be displayed.\nAre you sure you want to proceed?', 'Replace poll', function (r) { return r; })) {
            //    return false;
            //}
        }
    });

    $("#MainContent_btnPreview").click(function () {
        if ($("#MainContent_txtQuestion").val() == "") {
            $("#MainContent_lblErrorJquery").text("Please enter question");
            $("#errorid").show();
            $("#MainContent_txtQuestion").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }
        if ($("#MainContent_txtWidthPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter width");
            $("#errorid1").show();
            $("#MainContent_txtWidthPoll").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }
        if ($("#MainContent_txtHeightPoll").val() == "") {
            $("#MainContent_lblErrorJquery1").text("Please enter height");
            $("#errorid1").show();
            $("#MainContent_txtHeightPoll").css("border-color", "red");
            $("#MainContent_launcharrow").hide();
            $("#MainContent_launch").hide();
            $("#MainContent_create").show();
            $("#MainContent_createarrow").show();
            return false;
        }

        $("#MainContent_create").show();
        if ($('#MainContent_videoDiv').is(':hidden')) {
            $("#errorid").hide();
        }
        else {
            if ($("#MainContent_txtVideo").val() == "") {
                $("#MainContent_lblErrorJquery").text("Please enter video code");
                $("#errorid").show();
                $("#MainContent_launcharrow").hide();
                $("#MainContent_launch").hide();
                $("#MainContent_create").show();
                $("#MainContent_createarrow").show();
                $("#MainContent_txtVideo").css("border-color", "#CDCDCD");
                return false;
            }
        }
        if ($('#MainContent_ImageDiv').is(':hidden')) {
        }
        else {
            var files = $("#MainContent_fuImage").get(0).files;
            if (files.length > 0) {
                $("#errorid").hide();
                $(".imagetrans").css("color", 'transparent');
            }
            else {
                if ($('#MainContent_lblImageName').text() == "") {
                    $("#MainContent_lblErrorJquery").text("Please upload image");
                    $("#errorid").show();
                    $("#MainContent_launcharrow").hide();
                    $("#MainContent_launch").hide();
                    $("#MainContent_create").show();
                    $("#MainContent_createarrow").show();
                    $(".imagetrans").css("color", 'red');
                    return false;
                }
            }
        }
        $("#MainContent_create").hide();
        $("#MainContent_design").show();
        if ($('#MainContent_SponsorBox').is(':hidden')) {
        }
        else {
            var files = $("#MainContent_sponsorLogo").get(0).files;
            if (files.length > 0) {
            }
            else {
                if ($('#MainContent_lblLogoName').text() == "") {
                    $("#MainContent_lblDesignError").text("Please upload logo image");
                    $("#designerror").show();
                    $('.imagetranslogo').css('color', 'red');
                    $("#MainContent_launcharrow").hide();
                    $("#MainContent_launch").hide();
                    $("#MainContent_design").show();
                    $("#MainContent_designarrow").show();
                    return false;
                }
            }
        }
        $("#MainContent_design").hide();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightmin = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightmin = iframeheight;
        if (parseInt(fixheight) < parseInt(heightmin)) {
            jAlert('Minimum height for this poll should be ' + heightmin, 'Activation Alert', function (r) {
                return false;
            });
            return false;
        }

        console.log($("#MainContent_hdnPollID").val());
        console.log($("#MainContent_hdnParentPollID").val());
        if (($("#MainContent_hdnPollID").val() != $("#MainContent_hdnParentPollID").val()) && ($("#MainContent_hdnParentPollID").val() != "Active")) {
            if (confirm('Once you launch this replaced poll, the original poll will be automatically closed and in its place this poll will be displayed.\nAre you sure you want to proceed?')) {

            }
            else {
                return false;
            }
            //var bool = true;
            //if (jConfirm('Once you launch this replaced poll, the original poll will be automatically closed and in its place this poll will be displayed.\nAre you sure you want to proceed?', 'Replace poll', function (r) { bool = r; return r; })) {
            //    alert(bool);
            //    return bool;
            //}
            //jConfirm('Once you launch this replaced poll, the original poll will be automatically closed and in its place this poll will be displayed.\nAre you sure you want to proceed?', 'Replace poll', function (r) { bool = r; return r; });
        }
        //return false;
    });



    $("#MainContent_radSingleAnswer").click(function () {
        $(".multipleanswers").hide();
        $(".singleanswers").show();
    });
    $("#MainContent_radMultipleAnswer").click(function () {
        $(".multipleanswers").show();
        $(".singleanswers").hide();
    });
    $('#MainContent_ddlQuestionFont').change(function () {
        var questionfont = $("#MainContent_ddlQuestionFont").val();
        $('.questionpreviewcss').css({ 'font-family': questionfont });
    });
    $('#MainContent_ddlAnswerFont').change(function () {
        var answerfont = $("#MainContent_ddlAnswerFont").val();
        $('.radiopolltheme').css({ 'font-family': answerfont });
        $('.commentstxt').css({ 'font-family': answerfont });
        $('.poweredby').css({ 'font-family': answerfont });
        $('.poweredbyrep').css({ 'font-family': answerfont });
        $('.sponsorimagepreview').css({ 'font-family': answerfont });
        $('.sponsorimagepreviewres1').css({ 'font-family': answerfont });
        $('.socialnew').css({ 'font-family': answerfont });
        $('.socialnew1').css({ 'font-family': answerfont });
        $("#MainContent_hdnAnswerFont").val(answerfont);
        renderChart();
    });
    $('#MainContent_ddlVoteFont').change(function () {
        var votefont = $("#MainContent_ddlVoteFont").val();
        $('.viewandvoteimagepreview').css({ 'font-family': votefont });
    });
    $('#MainContent_ddlMsgFont').change(function () {
        var votefont = $("#MainContent_ddlMsgFont").val();
        $('.resultpreview').css({ 'font-family': votefont });
    });
    $("#MainContent_txtVideo").bind('input', function () {
        if ($('#MainContent_imgPreview').is(':hidden')) {
            var heightvideo = $(".videopreviewcreate").height();
        }
        else {
            var heightvideo = $("#MainContent_imgPreview").height();
        }
        var title = $(this).val();
        $("#MainContent_txtVideo").css("border-color", "#CDCDCD");
        if (title == "") {
            $(".videopreviewcreate").html(title);
            $('#MainContent_imgPreview').attr('src', 'App_Themes/images/video.jpg');
            $('#MainContent_imgPreviewResults').attr('src', 'App_Themes/images/video.jpg');
            var heightvideo = $("#MainContent_imgPreview").height();
            var width = $(".textpollthemepreview").width();
            var valueHeight = Math.round((width / 16) * 9);
            $('#MainContent_imgPreview').attr('height', valueHeight);
            $('#MainContent_imgPreview').attr('width', width);
            $('#MainContent_imgPreviewResults').attr('height', valueHeight);
            $('#MainContent_imgPreviewResults').attr('width', width);
            $('#MainContent_imagepreview').show();
            $("#MainContent_videoResults").hide();
            $('#MainContent_imgResults').show();
        }
        else {
            if (title.indexOf("<iframe") >= 0) {
                $(".videopreviewcreate").html(title);
                var width = $(".textpollthemepreview").width();
                var valueHeight = Math.round((width / 16) * 9);
                $(".videopreviewcreate iframe").css('height', valueHeight + 'px');
                $(".videopreviewcreate iframe").css('width', width + 'px');
                $(".videopreviewcreate").css('width', width + 'px');
                var heightrecad = valueHeight - heightvideo;
                var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
                $("#MainContent_lblRecThemeHeight").text(heightrec);
                $('#MainContent_imagepreview').hide();
                $("#MainContent_videoResults").show();
                $('#MainContent_imgResults').hide();
                $("#MainContent_videopreviewcreate").show();
            }
            else {
                $("#MainContent_txtVideo").val("");
                jAlert('Please copy and paste the embed of your YouTube video', 'Activation Alert', function (r) {
                    return false;
                });
            }
        }
        return true;
    });
}); 

function iframewidthset() {
    var widthpoll = $("#MainContent_txtWidthIframe").val();
    $('#MainContent_lblImageWidth').text(widthpoll);
    $('#MainContent_hdnImageRecWidth').val(widthpoll);
    if (widthpoll > 380) {
        $(".Previewres").css("float", 'left');
        $(".Previewres").css("padding-top", '10px');
        $(".createpollnew").css("float", 'left');
}
else {
    $(".Previewres").css("float", '');
    $(".Previewres").css("padding-top", '');
    $(".Previewres").css("padding-left", '');
}
    $(".textpollthemepreview").css("width", widthpoll + 'px');
    $(".textpollthemepreviewresults").css("width", widthpoll + 'px');
    $("#MainContent_txtWidthPoll").val(widthpoll);
    var videolink = $("#MainContent_txtVideo").val();
    if (videolink != "") {
        var heightvideo = $(".videopreviewcreate").height();
        $(".videopreviewcreate").html(videolink);
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $(".videopreviewcreate iframe").css('height', valueHeight + 'px');
        $(".videopreviewcreate iframe").css('width', width + 'px');
        $(".videopreviewcreate").css('width', width + 'px');
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblHeight").text()) + heightrecad;
        $("#MainContent_lblHeight").text(heightrec);
        $('#MainContent_hdnRecHeightne').val(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightIframe").val();
        heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightrec = iframeheight;

        if (parseInt(height) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
    }
    else if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/video.jpg') {
        var heightvideo = $("#MainContent_imgPreview").height();
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $('#MainContent_imgPreview').attr('height', valueHeight);
        $('#MainContent_imgPreview').attr('width', width);
        $('#MainContent_imgPreviewResults').attr('height', valueHeight);
        $('#MainContent_imgPreviewResults').attr('width', width);
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
        $("#MainContent_lblRecThemeHeight").text(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightsel = $("#MainContent_hdnSetHeight").val();
        heightrec = $("#MainContent_lblRecThemeHeight").text();
        $('#MainContent_hdnRecHeightne').val(heightrec);
        if (heightrec > fixheight) {
            fixheight = heightrec;
        }
        calculateIFrameHeight();
        heightrec = iframeheight;

        if (parseInt(height) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        $("#MainContent_txtHeightPoll").val(fixheight);
    }
    else {
            var maxwidth = widthpoll;
            heighttxt = $('.textpollthemepreview').height();
            //var maxheight = (40 * heighttxt) / 100;
            var maxheight = heighttxt;
            if (maxheight > 200) {
                maxheight = 200;
            }
            var screenImage = $("#MainContent_imgPreview");
            var theImage = new Image();
            theImage.src = screenImage.attr("src");
            var width = theImage.width;
            var height = theImage.height;
            if (width > maxwidth) {
                ratio = maxwidth / width;
                height = height * ratio;
                width = width * ratio;
            }
            if (height > maxheight) {
                ratio = maxheight / height;
                height = height * ratio;
                width = width * ratio;
            }
            if (height == 0) {
                //height = (40 * heighttxt) / 100;
                height = 40;
            }
            if (width == 0) {
                width = maxwidth;
            }
            width = Math.round(width);
            height = Math.round(height);
            $('#MainContent_imgPreview').attr('height', height);
            $('#MainContent_hdnImageHeight').val(height);
            $('#MainContent_imgPreview').attr('width', width);
            $("#MainContent_imgPreview").css("width", width + 'px');
            $("#MainContent_imgPreview").css("height", height + 'px');
            $("#MainContent_imagepreview").css("height", height + 'px');
            $('#MainContent_imgPreviewResults').attr('height', height);
            $('#MainContent_imgPreviewResults').attr('width', width);
            $(".videopreviewcreate").css("width", widthpoll + 'px');
        }
    var comwidth = widthpoll - 30;
    $(".txtcomments").css("width", comwidth + 'px');
    var answidth = $("#MainContent_txtWidthIframe").val() - 50;
    $(".wrapnew").css("width", answidth + 'px');
    $(".Preview").css("width", widthpoll + 'px');
    $(".Previewres").css("width", widthpoll + 'px');
    return false;
}

function iframewidthset1() {
    var widthpoll = $("#MainContent_txtWidthIframe1").val();
    $('#MainContent_lblImageWidth').text(widthpoll);
    $('#MainContent_hdnImageRecWidth').val(widthpoll);
    if (widthpoll > 380) {
        $(".Previewres").css("float", 'left');
        $(".Previewres").css("padding-top", '10px');
        $(".createpollnew").css("float", 'left');
    }
    else {
        $(".Previewres").css("float", '');
        $(".Previewres").css("padding-top", '');
        $(".Previewres").css("padding-left", '');
    }
    $(".textpollthemepreview").css("width", widthpoll + 'px');
    $(".textpollthemepreviewresults").css("width", widthpoll + 'px');
    $("#MainContent_txtWidthPoll").val(widthpoll);
    var videolink = $("#MainContent_txtVideo").val();
    if (videolink != "") {
        var heightvideo = $(".videopreviewcreate").height();
        $(".videopreviewcreate").html(videolink);
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $(".videopreviewcreate iframe").css('height', valueHeight + 'px');
        $(".videopreviewcreate iframe").css('width', width + 'px');
        $(".videopreviewcreate").css('width', width + 'px');
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblHeight1").text()) + heightrecad;
        $("#MainContent_lblHeight1").text(heightrec);
        $('#MainContent_hdnRecHeightne1').val(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightIframe1").val();
        heightrec = $("#MainContent_hdnRecHeightne1").val();
        calculateIFrameHeight();
        heightrec = iframeheight;
        if (parseInt(height) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
    }
    else if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/video.jpg') {
        var heightvideo = $("#MainContent_imgPreview").height();
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $('#MainContent_imgPreview').attr('height', valueHeight);
        $('#MainContent_imgPreview').attr('width', width);
        $('#MainContent_imgPreviewResults').attr('height', valueHeight);
        $('#MainContent_imgPreviewResults').attr('width', width);
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
        $("#MainContent_lblRecThemeHeight").text(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightsel = $("#MainContent_hdnSetHeight").val();
        heightrec = $("#MainContent_lblRecThemeHeight").text();
        $('#MainContent_hdnRecHeightne1').val(heightrec);
        calculateIFrameHeight();
        heightrec = iframeheight;
        if (parseInt(height) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        if (heightrec > fixheight) {
            fixheight = heightrec;
        }
        $("#MainContent_txtHeightPoll").val(fixheight);
    }
    else {
        var maxwidth = widthpoll;
        heighttxt = $('.textpollthemepreview').height();
        var maxheight = (40 * heighttxt) / 100;
        if (maxheight > 200) {
            maxheight = 200;
        }
        var screenImage = $("#MainContent_imgPreview");
        var theImage = new Image();
        theImage.src = screenImage.attr("src");
        var width = theImage.width;
        var height = theImage.height;
        if (width > maxwidth) {
            ratio = maxwidth / width;
            height = height * ratio;
            width = width * ratio;
        }
        if (height > maxheight) {
            ratio = maxheight / height;
            height = height * ratio;
            width = width * ratio;
        }
        if (height == 0) {
            height = (40 * heighttxt) / 100;
        }
        if (width == 0) {
            width = maxwidth;
        }
        width = Math.round(width);
        height = Math.round(height);
        $('#MainContent_imgPreview').attr('height', height);
        $('#MainContent_hdnImageHeight').val(height);
        $('#MainContent_imgPreview').attr('width', width);
        $("#MainContent_imgPreview").css("width", width + 'px');
        $("#MainContent_imgPreview").css("height", height + 'px');
        $("#MainContent_imagepreview").css("height", height + 'px');
        $('#MainContent_imgPreviewResults').attr('height', height);
        $('#MainContent_imgPreviewResults').attr('width', width);
        $(".videopreviewcreate").css("width", widthpoll + 'px');
    }
    var comwidth = widthpoll - 30;
    $(".txtcomments").css("width", comwidth + 'px');
    var answidth = $("#MainContent_txtWidthIframe1").val() - 50;
    $(".wrapnew").css("width", answidth + 'px');
    $(".Preview").css("width", widthpoll + 'px');
    $(".Previewres").css("width", widthpoll + 'px');
    return false;
}

function iframeheightset() {
    var heightpoll = $("#MainContent_txtHeightIframe").val();
    $("#MainContent_txtHeightPoll").val(heightpoll);
    var fixheight = $("#MainContent_txtHeightPoll").val();
    var heightrec = $("#MainContent_hdnRecHeightne").val();
      $(".textpollthemepreview").css("height", heightpoll + 'px');
        $(".textpollthemepreviewresults").css("height", heightpoll + 'px');
        $("#MainContent_hdnSetHeight").val(heightpoll);
        if (parseInt(heightpoll) == parseInt(heightrec)) {
            heightcon = $(".textpollthemepreview").height() + 90;
            $(".contentPanelPollwithoutborder").css("min-height", heightcon);
            $("#MainContent_showrec").hide();
            return false;
        }
        var ansheight = parseInt(heightpoll) - 125;
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '10px');
            ansheight = parseInt(ansheight) + 25;
        }
        else {
            $(".viewtest").css("bottom", '30px');
        }
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
            ansheight = parseInt(ansheight) - 20;
        }
        var imgheight ="";
        if ($('#MainContent_imagepreview').is(':hidden')) {

        }
        else {
            imgheight = $('#MainContent_imagepreview').height();
            ansheight = parseInt(ansheight) - parseInt(imgheight);
        }
        if ($('#MainContent_videopreviewcreate').is(':hidden')) {

        }
        else {
            var width = $("#MainContent_txtWidthPoll").val();
            var valueHeight = Math.round((width / 16) * 9);
            ansheight = parseInt(ansheight) - parseInt(valueHeight);
        }
        $("#MainContent_hdnansHeight").val(ansheight);
        $(".mainanswer").css("height", ansheight + 'px');
        heightcon = $(".textpollthemepreview").height() + 90;
        $(".contentPanelPollwithoutborder").css("min-height", heightcon);
        var widthpoll = $("#MainContent_txtWidthPoll").val();
        var maxwidth = widthpoll;
        heighttxt = $('.textpollthemepreview').height();
        var maxheight = (40 * heighttxt) / 100;
        if (maxheight > 200) {
            maxheight = 200;
        }
        var widthpre = $('.textpollthemepreview').width();
        $('#MainContent_lblImageWidth').text(widthpre);
        maxheight = Math.round(maxheight);
        $('#MainContent_lblImageHeight').text(maxheight);
        $('#MainContent_hdnImageRecWidth').text(widthpre);
        $('#MainContent_hdnImageRecHeight').val(maxheight);
        var screenImage = $("#MainContent_imgPreview"); 
        var theImage = new Image();
        theImage.src = screenImage.attr("src");
        var width = theImage.width;
        var height = theImage.height;
        if (width > maxwidth) {
            ratio = maxwidth / width;
            height = height * ratio;
            width = width * ratio;
        }
        if (height > maxheight) {
            ratio = maxheight / height;
            height = height * ratio;
            width = width * ratio;
        }
        if (height == 0) {
            height = (100 * heighttxt) / 100;
        }
        if (width == 0) {
            width = maxwidth;
        }
        width = Math.round(width);
        height = Math.round(height);
        if (imgheight == "" || imgheight == NaN) {

        }
        else {
            imgheight = parseInt(height) - parseInt(imgheight);
            var heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) + parseInt(imgheight);
            $("#MainContent_lblRecThemeHeight").text(heightg);
            $("#MainContent_hdnRecHeightne").val(heightg);
            $("#MainContent_lblHeight").text(heightg);
        }
        heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightrec = iframeheight;
        if (parseInt(fixheight) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        //$('#MainContent_imagepreview').css('height', height);
        //$('#MainContent_imgPreview').attr('height', height);
        //$('#MainContent_hdnImageHeight').val(height);
        //$('#MainContent_imgPreview').attr('width', width);
        //$("#MainContent_imgPreview").css("width", width + 'px');
        //$("#MainContent_imgPreview").css("height", height + 'px');
        //$('#MainContent_imgPreviewResults').attr('height', height);
        //$('#MainContent_imgPreviewResults').attr('width', width);
        //$(".videopreviewcreate").css("width", widthpoll + 'px');
    }

function iframeheightset1() {
        var heightpoll = $("#MainContent_txtHeightIframe1").val();
        $("#MainContent_txtHeightPoll").val(heightpoll);
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightrec = $("#MainContent_hdnRecHeightne1").val();
        $(".textpollthemepreview").css("height", heightpoll + 'px');
        $(".textpollthemepreviewresults").css("height", heightpoll + 'px');
        $("#MainContent_hdnSetHeight").val(heightpoll);
        if (parseInt(heightpoll) == parseInt(heightrec)) {
            heightcon = $(".textpollthemepreview").height() + 90;
            $(".contentPanelPollwithoutborder").css("min-height", heightcon);
            $("#MainContent_showrec").hide();
            return false;
        }
        var ansheight = parseInt(heightpoll) - 125;
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '10px');
            ansheight = parseInt(ansheight) + 25;
        }
        else {
            $(".viewtest").css("bottom", '30px');
        }
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
            ansheight = parseInt(ansheight) - 20;
        }
        var imgheight = "";
        if ($('#MainContent_imagepreview').is(':hidden')) {

        }
        else {
            imgheight = $('#MainContent_imagepreview').height();
            ansheight = parseInt(ansheight) - parseInt(imgheight);
        }
        if ($('#MainContent_videopreviewcreate').is(':hidden')) {

        }
        else {
            var width = $("#MainContent_txtWidthPoll").val();
            var valueHeight = Math.round((width / 16) * 9);
            ansheight = parseInt(ansheight) - parseInt(valueHeight);
        }
        $("#MainContent_hdnansHeight").val(ansheight);
        $(".mainanswer").css("height", ansheight + 'px');
        heightcon = $(".textpollthemepreview").height() + 90;
        $(".contentPanelPollwithoutborder").css("min-height", heightcon);
        var widthpoll = $("#MainContent_txtWidthPoll").val();
        var maxwidth = widthpoll;
        heighttxt = $('.textpollthemepreview').height();
        //var maxheight = (40 * heighttxt) / 100;
        var maxheight = heighttxt;
        if (maxheight > 200) {
            maxheight = 200;
        }
        var widthpre = $('.textpollthemepreview').width();
        $('#MainContent_lblImageWidth').text(widthpre);
        maxheight = Math.round(maxheight);
        $('#MainContent_lblImageHeight').text(maxheight);
        $('#MainContent_hdnImageRecWidth').text(widthpre);
        $('#MainContent_hdnImageRecHeight').val(maxheight);
        var screenImage = $("#MainContent_imgPreview");
        var theImage = new Image();
        theImage.src = screenImage.attr("src");
        var width = theImage.width;
        var height = theImage.height;
        if (width > maxwidth) {
            ratio = maxwidth / width;
            height = height * ratio;
            width = width * ratio;
        }
        if (height > maxheight) {
            ratio = maxheight / height;
            height = height * ratio;
            width = width * ratio;
        }
        if (height == 0) {
            //height = (40 * heighttxt) / 100;
            height = heighttxt;
        }
        if (width == 0) {
            width = maxwidth;
        }
        width = Math.round(width);
        height = Math.round(height);
        if (imgheight == "" || imgheight == NaN) {

        }
        else {
            imgheight = parseInt(height) - parseInt(imgheight);
            var heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) + parseInt(imgheight);
            $("#MainContent_lblRecThemeHeight").text(heightg);
            $("#MainContent_hdnRecHeightne1").val(heightg);
            $("#MainContent_lblHeight1").text(heightg);
        }
        heightrec = $("#MainContent_hdnRecHeightne1").val();       
        calculateIFrameHeight();
        heightrec = iframeheight;
        if (parseInt(fixheight) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        //document.cookie = "iframeheight=" + $("#MainContent_txtHeightIframe1").val();
        document.cookie = "iframeheight=" + $("#MainContent_txtHeightIframe1").val() + "; domain=.insighto.com; path=/;";
        //$('#MainContent_imagepreview').css('height', height);
        //$('#MainContent_imgPreview').attr('height', height);
        //$('#MainContent_hdnImageHeight').val(height);
        //$('#MainContent_imgPreview').attr('width', width);
        //$("#MainContent_imgPreview").css("width", width + 'px');
        //$("#MainContent_imgPreview").css("height", height + 'px');
        //$('#MainContent_imgPreviewResults').attr('height', height);
        //$('#MainContent_imgPreviewResults').attr('width', width);
        //$(".videopreviewcreate").css("width", widthpoll + 'px');
    }

function iframewidthsetcreate() {
    var widthpoll = $("#MainContent_txtWidthPoll").val();
    $('#MainContent_lblImageWidth').text(widthpoll);
    $('#MainContent_hdnImageRecWidth').text(widthpoll);
    if (widthpoll > 380) {
        $(".Previewres").css("float", 'left');
        $(".Previewres").css("padding-top", '10px');
        $(".createpollnew").css("float", 'left');
    }
    else {  
        $(".Previewres").css("float", '');
        $(".Previewres").css("padding-top", '');
        $(".Previewres").css("padding-left", '');
    }
    $(".textpollthemepreview").css("width", widthpoll + 'px');
    $(".textpollthemepreviewresults").css("width", widthpoll + 'px');
    var videolink = $("#MainContent_txtVideo").val();
    if (videolink != null && videolink != "") {
        var heightvideo = $(".videopreviewcreate").height();
        $(".videopreviewcreate").html(videolink);
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $(".videopreviewcreate iframe").css('height', valueHeight + 'px');
        $(".videopreviewcreate iframe").css('width', width + 'px');
        $(".videopreviewcreate").css('width', width + 'px');
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
        $("#MainContent_lblRecThemeHeight").text(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightsel = $("#MainContent_hdnSetHeight").val();
        heightrec = $("#MainContent_lblRecThemeHeight").text();
        $('#MainContent_hdnRecHeightne').val(heightrec);
        heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightrec = iframeheight;
        console.log("HEIGHT:" + heightrec);
        if (parseInt(fixheight) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        $("#MainContent_txtHeightPoll").val(fixheight);
    }
    else if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/video.jpg') {
        var heightvideo = $("#MainContent_imgPreview").height();
        var width = $(".textpollthemepreview").width();
        var valueHeight = Math.round((width / 16) * 9);
        $('#MainContent_imgPreview').attr('height', valueHeight);
        $('#MainContent_imgPreview').attr('width', width);
        $('#MainContent_imgPreviewResults').attr('height', valueHeight);
        $('#MainContent_imgPreviewResults').attr('width', width);
        var heightrecad = valueHeight - heightvideo;
        var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
        $("#MainContent_lblRecThemeHeight").text(heightrec);
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightsel = $("#MainContent_hdnSetHeight").val();
        heightrec = $("#MainContent_lblRecThemeHeight").text();
        $('#MainContent_hdnRecHeightne').val(heightrec);
        heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        heightrec = iframeheight;
        console.log("HEIGHT:" + heightrec);
        if (parseInt(fixheight) < parseInt(heightrec)) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
        $("#MainContent_txtHeightPoll").val(fixheight);
    }
    else {
        if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/ImagePoll.jpg') {
        }
        else {
            var maxwidth = widthpoll;
            heighttxt = $('.textpollthemepreview').height();
            var maxheight = (40 * heighttxt) / 100;
            if (maxheight > 200) {
                maxheight = 200;
            }
            var screenImage = $("#MainContent_imgPreview");
            var theImage = new Image();
            theImage.src = screenImage.attr("src");
            var width = theImage.width;
            var height = theImage.height;
            if (parseInt(width) > parseInt(maxwidth)) {
                ratio = maxwidth / width;
                height = height * ratio;
                width = width * ratio;
            }
            if (parseInt(height) > parseInt(maxheight)) {
                ratio = maxheight / height;
                height = height * ratio;
                width = width * ratio;
            }
            if (height == 0) {
                height = (40 * heighttxt) / 100;
            }
            if (width == 0) {
                width = maxwidth;
            }
            width = Math.round(width);
            height = Math.round(height);
            $('#MainContent_imgPreview').attr('height', height);
            $('#MainContent_hdnImageHeight').val(height);
            $('#MainContent_imgPreview').attr('width', width);
            $("#MainContent_imgPreview").css("width", width + 'px');
            $("#MainContent_imgPreview").css("height", height + 'px');
            $("#MainContent_imagepreview").css("height", height + 'px');
            $('#MainContent_imgPreviewResults').attr('height', height);
            $('#MainContent_imgPreviewResults').attr('width', width);
            $(".videopreviewcreate").css("width", maxwidth + 'px');
        }
    }
        $(".Preview").css("width", widthpoll + 'px');
        $(".Previewres").css("width", widthpoll + 'px');
        var comwidth = widthpoll - 30;
        $(".txtcomments").css("width", comwidth + 'px');
        var answidth = $("#MainContent_txtWidthPoll").val() - 50;
        $(".wrapnew").css("width", answidth + 'px');
        return false;
    }

function height() {
        var heightpoll = $("#MainContent_txtHeightPoll").val();
        var ansheight = parseInt(heightpoll) - 120;
        if ($('#MainContent_poweredby').is(':hidden')) {
               ansheight = parseInt(ansheight) + 25;
        }
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
        }
        else {
            ansheight = parseInt(ansheight) - 20;
        }
        $("#MainContent_hdnansHeight").val(ansheight);
        $(".mainanswer").css("height", ansheight + 'px');
}

function iframeheightsetcreate() {
        var heightpoll = $("#MainContent_txtHeightPoll").val();
        $(".textpollthemepreview").css("height", heightpoll + 'px');
        $(".textpollthemepreviewresults").css("height", heightpoll + 'px');
        $("#MainContent_hdnSetHeight").val(heightpoll);     
        var heightrec = $("#MainContent_hdnRecHeightne").val();
        var recimgh = $("#MainContent_hdnImageRecHeight").val();
        calculateIFrameHeight();
        heightrec = iframeheight;
        if (parseInt(heightpoll) == parseInt(heightrec)) {
            heightcon = $(".textpollthemepreview").height() + 90;
            $(".contentPanelPollwithoutborder").css("min-height", heightcon);
            $("#MainContent_showrec").hide();
            return false;
        }
        var ansheight = parseInt(heightpoll) - 125;
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '10px');
            ansheight = parseInt(ansheight) + 25;
        }
        else {
            $(".viewtest").css("bottom", '30px');
        }
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
            ansheight = parseInt(ansheight) - 20;
        }
        var imgheight = "";
        if ($('#MainContent_imagepreview').is(':hidden')) {
            var heightpoll1 = parseInt($("#MainContent_txtHeightPoll").val());
            $(".textpollthemepreview").css("height", heightpoll1 + 'px');
            $(".textpollthemepreviewresults").css("height", heightpoll1 + 'px');
            $("#MainContent_hdnSetHeight").val(heightpoll1);
            var heightrec1 = parseInt($("#MainContent_hdnRecHeightne").val());
            calculateIFrameHeight();
            heightrec1 = iframeheight;

            if (parseInt(heightpoll1) < parseInt(heightrec1)) {
                $("#MainContent_showrec").show();
                $("#MainContent_lblRecHeightbPre").text(heightrec1);
                return;
            }
            else {
                $("#MainContent_showrec").hide();
            }
        }
        else {
            if ($('#MainContent_imgPreview').attr('src') == 'App_Themes/images/video.jpg') {
                var heightvideo = $("#MainContent_imgPreview").height();
                var width = $(".textpollthemepreview").width();
                var valueHeight = Math.round((width / 16) * 9);
                //$('#MainContent_imgPreview').attr('height', valueHeight);
                //$('#MainContent_imgPreview').attr('width', width);
                //$('#MainContent_imgPreviewResults').attr('height', valueHeight);
                //$('#MainContent_imgPreviewResults').attr('width', width);
                var heightrecad = valueHeight - heightvideo;
                var heightrec = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightrecad;
                $("#MainContent_lblRecThemeHeight").text(heightrec);
                var height = $(".textpollthemepreview").height();
                var fixheight = $("#MainContent_txtHeightPoll").val();
                var heightsel = $("#MainContent_hdnSetHeight").val();
                heightrec = $("#MainContent_lblRecThemeHeight").text();
                $('#MainContent_hdnRecHeightne').val(heightrec);
                heightrec = $("#MainContent_hdnRecHeightne").val();
                calculateIFrameHeight();
                heightrec = iframeheight;
                if (parseInt(fixheight) < parseInt(heightrec)) {
                    $("#MainContent_showrec").show();
                    $("#MainContent_lblRecHeightbPre").text(heightrec);
                }
                else {
                    $("#MainContent_showrec").hide();
                }
                $("#MainContent_txtHeightPoll").val(fixheight);
            }
            else {
                imgheight = $('#MainContent_imagepreview').height();
                ansheight = parseInt(ansheight) - parseInt(imgheight);
                var widthpoll = $("#MainContent_txtWidthPoll").val();
                var maxwidth = widthpoll;
                heighttxt = $('.textpollthemepreview').height();
                var maxheight = (40 * heighttxt) / 100;
                if (maxheight > 200) {
                    maxheight = 200;
                }
                var widthpre = $('.textpollthemepreview').width();
                $('#MainContent_lblImageWidth').text(widthpre);
                maxheight = Math.round(maxheight);
                $('#MainContent_lblImageHeight').text(maxheight);
                $('#MainContent_hdnImageRecWidth').text(widthpre);
                $('#MainContent_hdnImageRecHeight').val(maxheight);
                var screenImage = $("#MainContent_imgPreview");
                var theImage = new Image();
                theImage.src = screenImage.attr("src");
                var width = theImage.width;
                var height = theImage.height;
                if (width > maxwidth) {
                    ratio = maxwidth / width;
                    height = height * ratio;
                    width = width * ratio;
                }
                if (height > maxheight) {
                    ratio = maxheight / height;
                    height = height * ratio;
                    width = width * ratio;
                }
                if (height == 0) {
                    //height = (40 * heighttxt) / 100;
                    height = heighttxt;
                }
                if (width == 0) {
                    width = maxwidth;
                }
                width = Math.round(width);
                height = Math.round(height);
                if (imgheight == "" || imgheight == NaN) {

                }
                else {
                    imgheight = parseInt(height) - parseInt(imgheight);
                    var heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) + parseInt(imgheight);
                    $("#MainContent_lblRecThemeHeight").text(heightg);
                    $("#MainContent_hdnRecHeightne").val(heightg);
                    $("#MainContent_lblHeight").text(heightg);
                }
                heightrec = $("#MainContent_hdnRecHeightne").val();
                calculateIFrameHeight();
                heightrec = iframeheight;
                if (parseInt(heightpoll) < parseInt(heightrec)) {
                    $("#MainContent_showrec").show();
                    $("#MainContent_lblRecHeightbPre").text(heightrec);
                }
                else {
                    $("#MainContent_showrec").hide();
                }
                //$('#MainContent_imagepreview').css('height', height);
                //$('#MainContent_imgPreview').attr('height', height);
                //$('#MainContent_hdnImageHeight').val(height);
                //$('#MainContent_imgPreview').attr('width', width);
                //$("#MainContent_imgPreview").css("width", width + 'px');
                //$("#MainContent_imgPreview").css("height", height + 'px');
                //$('#MainContent_imgPreviewResults').attr('height', height);
                //$('#MainContent_imgPreviewResults').attr('width', width);
                //$(".videopreviewcreate").css("width", widthpoll + 'px');
            }
        }
      
        if ($('#MainContent_videopreviewcreate').is(':hidden')) {

        }
        else {
            var width = $("#MainContent_txtWidthPoll").val();
            var valueHeight = Math.round((width / 16) * 9);
            ansheight = parseInt(ansheight) - parseInt(valueHeight);
        }

        $("#MainContent_hdnansHeight").val(ansheight);
        $(".mainanswer").css("height", ansheight + 'px');
        heightcon = $(".textpollthemepreview").height() + 90;
        $(".contentPanelPollwithoutborder").css("min-height", heightcon);
  
    return false;
}

function activeleft(x) {
    $("#leftinactive").hide()
    $("#leftactive").show();
}

function inactiveleft(x) {
    $("#leftinactive").show();
    $("#leftactive").hide();
}

function activecenter(x) {
    $("#centerinactive").hide();
    $("#centeractive").show();
}

function inactivecenter(x) {
    $("#centerinactive").show();
    $("#centeractive").hide();
}

function activeright(x) {
    $("#rightinactive").hide();
    $("#rightactive").show();
}

function inactiveright(x) {
    $("#rightinactive").show();
    $("#rightactive").hide();
}

function anactiveleft(x) {
    $("#answerleftinactive").hide()
    $("#answerleftactive").show();
}

function aninactiveleft(x) {
    $("#answerleftinactive").show();
    $("#answerleftactive").hide();
}

function anactivecenter(x) {
    $("#answercenterinactive").hide();
    $("#answercenteractive").show();
}

function aninactivecenter(x) {
    $("#answercenterinactive").show();
    $("#answercenteractive").hide();
}

function anactiveright(x) {
    $("#answerrightinactive").hide();
    $("#answerrightactive").show();
}

function aninactiveright(x) {
    $("#answerrightinactive").show();
    $("#answerrightactive").hide();
}

function vtactiveleft(x) {
    $("#voteleftinactive").hide()
    $("#voteleftactive").show();
}

function vtinactiveleft(x) {
    $("#voteleftinactive").show();
    $("#voteleftactive").hide();
}

function vtactivecenter(x) {
    $("#votecenterinactive").hide();
    $("#votecenteractive").show();
}

function vtinactivecenter(x) {
    $("#votecenterinactive").show();
    $("#votecenteractive").hide();
}

function vtactiveright(x) {
    $("#voterightinactive").hide();
    $("#voterightactive").show();
}

function vtinactiveright(x) {
    $("#voterightinactive").show();
    $("#voterightactive").hide();
}

function mactiveleft(x) {
    $("#msgleftinactive").hide()
    $("#msgleftactive").show();
}

function minactiveleft(x) {
    $("#msgleftinactive").show();
    $("#msgleftactive").hide();
}

function mactivecenter(x) {
    $("#msgcenterinactive").hide();
    $("#msgcenteractive").show();
}

function minactivecenter(x) {
    $("#msgcenterinactive").show();
    $("#msgcenteractive").hide();
}

function mactiveright(x) {
    $("#msgrightinactive").hide();
    $("#msgrightactive").show();
}

function minactiveright(x) {
    $("#msgrightinactive").show();
    $("#msgrightactive").hide();
}

function questionboldactive() {
    $("#MainContent_imgBold").hide();
    $("#MainContent_imgBoldActive").show();
    $("#MainContent_hdnQuestionBold").val("bold");
    $('.questionpreviewcss').css({ 'font-weight': 'bold' });
}

function questionboldinactive() {
    $("#MainContent_imgBold").show();
    $("#MainContent_imgBoldActive").hide();
    $("#MainContent_hdnQuestionBold").val("unbold");
    $('.questionpreviewcss').css({ 'font-weight': 'normal' });
}

function questionitalicactive() {
    $("#MainContent_imgItalicActive").show();
    $("#MainContent_imgItalic").hide();
    $("#MainContent_hdnQuestionItalic").val("italic");
    $('.questionpreviewcss').css({ 'font-style': 'italic' });
}

function questionitalicinactive() {
    $("#MainContent_imgItalicActive").hide();
    $("#MainContent_imgItalic").show();
    $("#MainContent_hdnQuestionItalic").val("notitalic");
    $('.questionpreviewcss').css({ 'font-style': 'normal' });
}

function questionulactive() {
    $("#MainContent_imgUnderlineActive").show();
    $("#MainContent_imgUnderline").hide();
    $("#MainContent_hdnQuestionUL").val("underline");
    $('.questionpreviewcss').css({ 'text-decoration': 'underline' });
}

function questionulinactive() {
    $("#MainContent_imgUnderlineActive").hide();
    $("#MainContent_imgUnderline").show();
    $("#MainContent_hdnQuestionUL").val("notunderline");
    $('.questionpreviewcss').css({ 'text-decoration': 'none' });
}

function questionalignright() {
    $("#MainContent_hdnQuestionAlign").val("right");
    $("#questionalign").hide();
    $("#MainContent_qrightactive").show();
    $("#MainContent_qleftinactive").hide();
    $("#MainContent_qleftactive").hide();
    $("#MainContent_qcenteractive").hide();
    $('.questionpreviewcss').css({ 'text-align': 'right' });
}

function questionaligncenter() {
    $("#MainContent_hdnQuestionAlign").val("center");
    $("#questionalign").hide();
    $("#MainContent_qcenteractive").show();
    $("#MainContent_qleftinactive").hide();
    $("#MainContent_qrightactive").hide();
    $("#MainContent_qleftactive").hide();
    $('.questionpreviewcss').css({ 'text-align': 'center' });
}

function questionalignleft() {
    $("#MainContent_hdnQuestionAlign").val("left");
    $("#questionalign").hide();
    $("#MainContent_qleftactive").show();
    $("#MainContent_qleftinactive").hide();
    $("#MainContent_qrightactive").hide();
    $("#MainContent_qcenteractive").hide();
    $('.questionpreviewcss').css({ 'text-align': 'left' });
}

function answerboldactive() {
    $("#MainContent_imgABold").hide();
    $("#MainContent_imgABoldActive").show();
    $("#MainContent_hdnAnswerBold").val("bold");
    $('.radiopolltheme').css({ 'font-weight': 'bold' });
    renderChart();  
}

function answerboldinactive() {
    $("#MainContent_imgABold").show();
    $("#MainContent_imgABoldActive").hide();
    $("#MainContent_hdnAnswerBold").val("unbold");
    $('.radiopolltheme').css({ 'font-weight': 'normal' });
    renderChart();
}

function answeritalicactive() {
    $("#MainContent_imgAItalicActive").show();
    $("#MainContent_imgAItalic").hide();
    $("#MainContent_hdnAnswerItalic").val("italic");
    $('.radiopolltheme').css({ 'font-style': 'italic' });
    renderChart();
}

function answeritalicinactive() {
    $("#MainContent_imgAItalicActive").hide();
    $("#MainContent_imgAItalic").show();
    $("#MainContent_hdnAnswerItalic").val("notitalic");
    $('.radiopolltheme').css({ 'font-style': 'normal' });
    renderChart();
}

function answerulactive() {
    ////console.log('underline');
    $("#MainContent_imgAULActive").show();
    $("#MainContent_imgAUL").hide();
    $("#MainContent_hdnAnswerUL").val("underline");
    ////console.log($("#MainContent_hdnAnswerUL").val());
    $('.radiopolltheme').css({ 'text-decoration': 'underline' });
    renderChart();
}

function answerulinactive() {
    ////console.log('notunderline');
    $("#MainContent_imgAULActive").hide();
    $("#MainContent_imgAUL").show();
    $("#MainContent_hdnAnswerUL").val("notunderline");
    ////console.log($("#MainContent_hdnAnswerUL").val());
    $('.radiopolltheme').css({ 'text-decoration': 'none' });
    renderChart();
}

function answeralignright() {
    $("#MainContent_hdnAnswerAlign").val("right");
    $("#answeralign").hide();
    $("#MainContent_arightactive").show();
    $("#MainContent_aleftinactive").hide();
    $("#MainContent_aleftactive").hide();
    $("#MainContent_acenteractive").hide();
    $('.radiopolltheme').css({ 'text-align': 'right' });
}

function answeraligncenter() {
    $("#MainContent_hdnAnswerAlign").val("center");
    $("#answeralign").hide();
    $("#MainContent_acenteractive").show();
    $("#MainContent_aleftinactive").hide();
    $("#MainContent_arightactive").hide();
    $("#MainContent_aleftactive").hide();
    $('.radiopolltheme').css({ 'text-align': 'center' });
}

function answeralignleft() {
    $("#MainContent_hdnAnswerAlign").val("left");
    $("#answeralign").hide();
    $("#MainContent_aleftactive").show();
    $("#MainContent_aleftinactive").hide();
    $("#MainContent_arightactive").hide();
    $("#MainContent_acenteractive").hide();
    $('.radiopolltheme').css({ 'text-align': 'left' });
}

function voteboldactive() {
    $("#MainContent_imgVBold").hide();
    $("#MainContent_imgVBoldActive").show();
    $("#MainContent_hdnVoteBold").val("bold");
    $('.viewandvoteimagepreview').css({ 'font-weight': 'bold' });
}

function voteboldinactive() {
    $("#MainContent_imgVBold").show();
    $("#MainContent_imgVBoldActive").hide();
    $("#MainContent_hdnVoteBold").val("unbold");
    $('.viewandvoteimagepreview').css({ 'font-weight': 'normal' });
}

function voteitalicactive() {
    $("#MainContent_imgVItalicActive").show();
    $("#MainContent_imgVItalic").hide();
    $("#MainContent_hdnVoteItalic").val("italic");
    $('.viewandvoteimagepreview').css({ 'font-style': 'italic' });
}

function voteitalicinactive() {
    $("#MainContent_imgVItalicActive").hide();
    $("#MainContent_imgVItalic").show();
    $("#MainContent_hdnVoteItalic").val("notitalic");
    $('.viewandvoteimagepreview').css({ 'font-style': 'normal' });
}

function voteulactive() {
    $("#MainContent_imgVULActive").show();
    $("#MainContent_imgVUL").hide();
    $("#MainContent_hdnVoteUL").val("underline");
    $('.viewandvoteimagepreview').css({ 'text-decoration': 'underline' });
}

function voteulinactive() {
    $("#MainContent_imgVULActive").hide();
    $("#MainContent_imgVUL").show();
    $("#MainContent_hdnVoteUL").val("notunderline");
    $('.viewandvoteimagepreview').css({ 'text-decoration': 'none' });
}

function votealignright() {
    $("#MainContent_hdnVoteAlign").val("right");
    $("#votealign").hide();
    $("#MainContent_vrightactive").show();
    $("#MainContent_vleftinactive").hide();
    $("#MainContent_vleftactive").hide();
    $("#MainContent_vcenteractive").hide();
    $('.viewandvoteimagepreview').css({ 'text-align': 'right' });
}

function votealigncenter() {
    $("#MainContent_hdnVoteAlign").val("center");
    $("#votealign").hide();
    $("#MainContent_vcenteractive").show();
    $("#MainContent_vleftinactive").hide();
    $("#MainContent_vrightactive").hide();
    $("#MainContent_vleftactive").hide();
    $('.viewandvoteimagepreview').css({ 'text-align': 'center' });
}

function votealignleft() {
    $("#MainContent_hdnVoteAlign").val("left");
    $("#votealign").hide();
    $("#MainContent_vleftactive").show();
    $("#MainContent_vleftinactive").hide();
    $("#MainContent_vrightactive").hide();
    $("#MainContent_vcenteractive").hide();
    $('.viewandvoteimagepreview').css({ 'text-align': 'left' });
}

function msgboldactive() {
    $("#MainContent_imgMBold").hide();
    $("#MainContent_imgMBoldActive").show();
    $("#MainContent_hdnMsgBold").val("bold");
    $('.resultpreview').css({ 'font-weight': 'bold' });
}

function msgboldinactive() {
    $("#MainContent_imgMBold").show();
    $("#MainContent_imgMBoldActive").hide();
    $("#MainContent_hdnMsgBold").val("unbold");
    $('.resultpreview').css({ 'font-weight': 'normal' });
}

function msgitalicactive() {
    $("#MainContent_imgMItalicActive").show();
    $("#MainContent_imgMItalic").hide();
    $("#MainContent_hdnMsgItalic").val("italic");
    $('.resultpreview').css({ 'font-style': 'italic' });
}

function msgitalicinactive() {
    $("#MainContent_imgMItalicActive").hide();
    $("#MainContent_imgMItalic").show();
    $("#MainContent_hdnMsgItalic").val("notitalic");
    $('.resultpreview').css({ 'font-style': 'normal' });
}

function msgulactive() {
    $("#MainContent_imgMULActive").show();
    $("#MainContent_imgMUL").hide();
    $("#MainContent_hdnMsgUL").val("underline");
    $('.resultpreview').css({ 'text-decoration': 'underline' });
}

function msgulinactive() {
    $("#MainContent_imgMULActive").hide();
    $("#MainContent_imgMUL").show();
    $("#MainContent_hdnMsgUL").val("notunderline");
    $('.resultpreview').css({ 'text-decoration': 'none' });
}

function msgalignright() {
    $("#MainContent_hdnMsgAlign").val("right");
    $("#msgalign").hide();
    $("#MainContent_mrightactive").show();
    $("#MainContent_mleftinactive").hide();
    $("#MainContent_mleftactive").hide();
    $("#MainContent_mcenteractive").hide();
    $('.resultpreview').css({ 'text-align': 'right' });
}

function msgaligncenter() {
    $("#MainContent_hdnMsgAlign").val("center");
    $("#msgalign").hide();
    $("#MainContent_mcenteractive").show();
    $("#MainContent_mleftinactive").hide();
    $("#MainContent_mrightactive").hide();
    $("#MainContent_mleftactive").hide();
    $('.resultpreview').css({ 'text-align': 'center' });
}

function msgalignleft() {
    $("#MainContent_hdnMsgAlign").val("left");
    $("#msgalign").hide();
    $("#MainContent_mleftactive").show();
    $("#MainContent_mleftinactive").hide();
    $("#MainContent_mrightactive").hide();
    $("#MainContent_mcenteractive").hide();
    $('.resultpreview').css({ 'text-align': 'left' });
}

function showimagepreview(input) {
    var fileextension = "";
    var errormsg = 'Please upload a valid file. \n Only image files of format JPG, GIF, and PNG are allowed.';
    if (input.files && input.files[0]) {
        var splitfilename = input.files[0].name.split(".");
        if (splitfilename.length == 0) {
            jAlert(errormsg, 'File Upload Error', function (r) {
                return false;
            });
            input.files = null;
            return;
        }
        else {
            fileextension = splitfilename[splitfilename.length - 1];
            if ((fileextension != "jpg") && (fileextension != "jpeg") && (fileextension != "png") && (fileextension != "gif") && (fileextension != "bmp")) {
                jAlert(errormsg, 'File Upload Error', function (r) {
                    return false;
                });
                input.files = null;
                return;
            }
        }
        console.log(input.files[0].name);
        var filerdr = new FileReader();
        
        filerdr.onload = function (e) {
            heighttxt = $('.textpollthemepreview').height();
            var maxheight = (40 * heighttxt) / 100;
            if (maxheight > 200) {
                maxheight = 200;
            }
            $('.imagetrans').css('color', '#000000');
            $("#errorid").hide();
            $('#MainContent_lblImageName').css('visibility', 'hidden');
            var heightimg = $("#MainContent_imgPreview").height();
            var maxwidth = $('.textpollthemepreview').width();
            $('#MainContent_imgPreview').attr('src', e.target.result);
            $('#MainContent_imgPreview').css({ 'height': 'auto' });
            $('#MainContent_imgPreview').css({ 'width': 'auto' });
            var screenImage = $("#MainContent_imgPreview");
            var theImage = new Image();
            theImage.src = screenImage.attr("src");
            theImage.onload = function () {
                var width = this.width;
                var height = this.height;
                if (width > maxwidth) {
                    ratio = maxwidth / width;
                    height = height * ratio;
                    width = width * ratio;
                }
                if (height > maxheight) {
                    ratio = maxheight / height;
                    height = height * ratio;
                    width = width * ratio;
                }
                height = Math.round(height);
                if (height == 0) {
                    height = (40 * heighttxt) / 100;
                }
                if (width == 0) {
                    width = $('.textpollthemepreview').width();
                }
                width = Math.round(width);
                height = Math.round(height);
                heightg = 0;//parseInt(height) - parseInt(heightimg);
                console.log(maxwidth.toString() + "--" + width.toString() + "--" + ratio.toString());
                console.log(heightg.toString() + "--" + height.toString() + "--" + heightimg.toString());
                heightg = Math.round(heightg);
                heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) + heightg;
                console.log(heightg.toString());
                $("#MainContent_lblRecThemeHeight").text(heightg);
                $("#MainContent_hdnRecHeightne").val(heightg);
                $('#MainContent_hdnImageHeight').val(height);
                $('#MainContent_imgPreview').attr('src', e.target.result);
                $('#MainContent_imgPreview').attr('height', height);
                $('#MainContent_imgPreview').attr('width', width);
                $('#MainContent_imgPreview').css('height', height);
                $('#MainContent_imgPreview').css('width', width);
                $('#MainContent_imagepreview').css({ 'height': height });
                $('#MainContent_imagepreview').show();
                $('#MainContent_imgPreviewResults').attr('src', e.target.result);
                $('#MainContent_imgPreviewResults').attr('height', height);
                $('#MainContent_imgPreviewResults').attr('width', width);
                $('#MainContent_imgResults').show();
                $('#MainContent_videopreviewcreate').hide();
                var imagenew = $("#MainContent_hdnImageLink").val();
                if (imagenew == "") {

                }
                else {
                    $("#MainContent_lblImageName").text(input.files[0].name);
                }
                calculateIFrameHeight();
            }
        }
        filerdr.readAsDataURL(input.files[0]);
    }
}

function showlogopreview(input) {
    if (input.files && input.files[0]) {
        var filerdr = new FileReader();
        filerdr.onload = function (e) {
            var maxwidth = 110;
            var maxheight = 40;
            $('#MainContent_logo').attr('src', e.target.result);
            var screenImage = $("#MainContent_logo");
            var theImage = new Image();
            theImage.src = screenImage.attr("src");
            theImage.onload = function () {
            var height = this.height;
            var width = this.width;
            if (height > maxheight) {
                ratio = maxheight / height;
                height = height * ratio;
                width = width * ratio;
            }
            if (width > maxwidth) {
                ratio = maxwidth / width;
                height = height * ratio;
                width = width * ratio;
            }
            if (height == 0) {
                height = 30;
            }
            if (width == 0) {
                width = 100;
            }
            $('#MainContent_logo').attr('src', e.target.result);
            $('#MainContent_logo').attr('height', height);
            $('#MainContent_logo').attr('width', width);
            $('#MainContent_imgSpoResults').attr('src', e.target.result);
            $('#MainContent_imgSpoResults').attr('height', height);
            $('#MainContent_imgSpoResults').attr('width', width);
            var imagelo = $("#MainContent_hdnLogoLink").val();
            $('.imagetranslogo').css('color', '#000000');
            $('#MainContent_lblLogoName').css('visibility', 'hidden');
            if (imagelo == "") {

            }
            else {
                $('#MainContent_lblLogoName').text(input.files[0].name);
            }
            if ($('#MainContent_social').is(':hidden')) {
                $(".sponsorimagepreviewres").css("width", '100%');
                $(".sponsorimagepreviewres").css("margin", 'auto');
                $(".sponsorimagepreviewres").css("margin-right", '0px');

            }
            else {
                $(".sponsorimagepreviewres").css("width", 'auto');
                $("#MainContent_socialsha").show();
            }
           
            }
        }
        filerdr.readAsDataURL(input.files[0]);
    }
}

function showPoweredBy() {
    if ($('#MainContent_poweredby').is(':hidden')) {
        $("#MainContent_poweredby").show();
        $('.viewtest').css({ 'padding-bottom': '0px' });
        $("#MainContent_poweredbyResults").show();
        $('.sponsorimagepreviewres').css({ 'bottom': '30px' });
        $('.viewtest').css({ 'bottom': '30px' });
        $('.newsponsocialdiv').css({ 'bottom': '30px' });
        var ansheight = $("#MainContent_hdnansHeight").val();
        ansheight = parseInt(ansheight) - 25;
        $("#MainContent_hdnansHeight").val(ansheight);
        $(".mainanswer").css("height", ansheight + 'px');
        var answerno = $("#MainContent_hdnRepeaterTxts").val();
        answerno = 20 * parseInt(answerno);
        if ($("#MainContent_chkComments").attr("checked")) {
            answerno = parseInt(answerno) + commentboxheight;
        }
        var answerheight = $("#MainContent_hdnansHeight").val();
        var diff = parseInt(answerno) - parseInt(ansheight); 
        var rechei = parseInt($("#MainContent_lblRecThemeHeight").text()) + 20;
        $("#MainContent_lblRecThemeHeight").text(rechei);
        $("#MainContent_lblHeight").text(rechei);
        $("#MainContent_hdnRecHeightne").val(rechei);
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
    }
    else {
        $("#MainContent_poweredby").hide();
        $("#MainContent_poweredbyResults").hide();
        $('.viewtest').css({ 'bottom': '10px' });
        $('.sponsorimagepreviewres').css({ 'bottom': '10px' });
        $('.newsponsocialdiv').css({ 'bottom': '10px' });
        var ansheight = $("#MainContent_hdnansHeight").val();
        ansheight = parseInt(ansheight) + 25;
        $("#MainContent_hdnansHeight").val(ansheight);
        $(".mainanswer").css("height", ansheight + 'px');
        var rechei = parseInt($("#MainContent_lblRecThemeHeight").text()) - 20;
        $("#MainContent_lblRecThemeHeight").text(rechei);
        $("#MainContent_lblHeight").text(rechei);
        $("#MainContent_hdnRecHeightne").val(rechei);
    }
    checkSocialMediaHeightAdjustment();
    renderChart();
}

function showCommentbox() {
    var numberofrows = parseInt($("#MainContent_hdnRepeaterTxts").val());
    if ($('#MainContent_commentbox').is(':hidden')) {
        $("#MainContent_commentbox").show();
        //checkCommentsBoxHeightAdjustment();
        var heightg = 0;

        if (($('#MainContent_ckbAddSponsor').is('checked')) || ($("#MainContent_ckbGoogle").attr("checked")) || ($("#MainContent_ckbFb").attr("checked")) || ($("#MainContent_ckbTwitter").attr("checked")) || (numberofrows > 5))
        {
            heightg = parseInt($("#MainContent_lblRecThemeHeight").text());
        }
        else
        {
            heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) + commentboxheight;
        }
        $("#MainContent_lblRecThemeHeight").text(heightg);
        $("#MainContent_hdnRecHeightne").val(heightg);
        var answerno = $("#MainContent_hdnRepeaterTxts").val();
        answerno = 20 * parseInt(answerno);
        if ($("#MainContent_chkComments").attr("checked")) {
            answerno = parseInt(answerno) + commentboxheight;
        }
        var answerheight = $("#MainContent_hdnansHeight").val();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
    }
    else {
        $("#MainContent_commentbox").hide();
        var heightg = 0;
        if (($('#MainContent_ckbAddSponsor').is('checked')) || ($("#MainContent_ckbGoogle").attr("checked")) || ($("#MainContent_ckbFb").attr("checked")) || ($("#MainContent_ckbTwitter").attr("checked")) || (numberofrows > 5)) {
            heightg = parseInt($("#MainContent_lblRecThemeHeight").text());
        }
        else {
            heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) - commentboxheight;
        }

        $("#MainContent_lblRecThemeHeight").text(heightg);
        $("#MainContent_hdnRecHeightne").val(heightg);
        var answerno = $("#MainContent_hdnRepeaterTxts").val();
        answerno = 20 * parseInt(answerno);
        if ($("#MainContent_chkComments").attr("checked")) {
            answerno = parseInt(answerno) + commentboxheight;
        }
        var answerheight = $("#MainContent_hdnansHeight").val();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
    }
    renderChart();
}

function showResultTextbox() {
    if ($('#MainContent_ResultBox').is(':hidden')) {
        $("#MainContent_ResultBox").show();
        $("#MainContent_results").hide();
        $("#MainContent_resulttextpreview").show();
        var height = $(".textpollthemepreview").height();
        $(".textpollthemepreviewresults").css("height", height + 'px');
        $(".view").css("display", 'none');
        $(".vote").css("text-align", 'center'); 
        $(".vote").css("width", '100%');
        $(".viewandvoteimagepreview").css("height", '16px'); 
    }
    else {
        $("#MainContent_ResultBox").hide();
        $("#MainContent_results").show();
        $("#MainContent_resulttextpreview").hide();
        var height = $(".textpollthemepreview").height();
        $(".textpollthemepreviewresults").css("height", height + 'px');
        $(".view").css("display", 'block');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".vote").css("width", '30%');
            $(".vote").css("text-align", '');
        }
        else {
            $(".vote").css("width", '100%');
            $(".vote").css("text-align", 'center');
            $(".viewandvoteimagepreview").css("height", '24px'); 
        }
    }
    calculateIFrameHeight();
}

function showSponsorDetails() {
    if ($('#MainContent_SponsorBox').is(':hidden')) {
        $("#MainContent_SponsorBox").show();
        $("#MainContent_sponsorResults").show();
        $(".social").css("margin-top", '15px');
        $('.socialnew').css({ 'width': '31%' });
        $('.socialnew').css({ 'margin-left': '0px' });
        $('.socialnew1').css({ 'margin-bottom': '-15px' });
        $("#MainContent_sponsorpreviw").show();
        $("#MainContent_sponsorResults").show();
        checkLogoAndSponsorForHeightAdjustment();
        var heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) + sponsorlogooptionheight;
        ////console.log(heightg);
        $("#MainContent_lblRecThemeHeight").text(heightg);
        $("#MainContent_hdnRecHeightne").val(heightg);
        var ansheight = $("#MainContent_hdnansHeight").val();
        ansheight = parseInt(ansheight) - 15;
        $("#MainContent_hdnansHeight").val(ansheight);
        $(".mainanswer").css("height", ansheight + 'px');
        var answerno = $("#MainContent_hdnRepeaterTxts").val();
        answerno = parseInt(answerno);
        answerno = 20 * parseInt(answerno);
        if ($("#MainContent_chkComments").attr("checked")) {
            answerno = parseInt(answerno) + commentboxheight;
        }
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        $('.newvotecount').css({ 'position': 'absolute' });
        $('.newvotecount').css({ 'padding-top': '3px' });
        if ($('#MainContent_social').is(':hidden')) {
            $(".sponsorimagepreviewres").css("width", '100%');
            $(".sponsorimagepreviewres").css("margin", 'auto');
            $(".sponsorimagepreviewres").css("margin-right", '0px');
        }
        else {
            $(".sponsorimagepreviewres").css("width", 'auto');
        }
        if ($('#MainContent_ResultBox').is(':hidden')) {
            $(".viewandvoteimagepreview").css("height", '24px'); 
        }
        else {
            $(".viewandvoteimagepreview").css("height", '16px'); 
        }
        var count = 0;
        if ($("#MainContent_ckbTwitter").attr("checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").attr("checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            $("#MainContent_googleiconvote").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").attr("checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
        if (count == 1) {
            //$('.social').css({ 'margin-left': '10%' });
        }
        if (count == 2) {
            //$('.social').css({ 'margin-left': '5%' });
        }
        if (count == 3) {
            //$('.social').css({ 'margin-left': '1%' });
        }
        $("#MainContent_txtSponsorText").val('Sponsored By');
        $("#MainContent_txtSponsorLink").val('http://');
        $('.viewandvoteimagepreview').css({ 'width': '45%' });
        $('.viewandvoteimagepreview').css({ 'float': 'left' });
        $('.viewandvoteimagepreview').css({ 'margin-left': '10px' });
        $('.viewandvoteimagepreview').css({ 'margin-top': '6px' });
        $('.view').css({ 'font-size': '10px' });
        $('.view').css({ 'text-align': 'center' });
        $('.vote').css({ 'text-align': 'center' });
        $('.vote').css({ 'width': '100%' });
        $('.viewtest').css({ 'margin-left': '0px' });
        $('.viewtest').css({ 'width': '100%' });
        $('.viewtest').css({ 'margin-right': '0px' });
        if ($("#MainContent_ckbResultsPrivate").attr("checked")) {
            $("#MainContent_ResultBox").show();
            $("#MainContent_results").hide();
            $("#MainContent_resulttextpreview").show();
            var height = $(".textpollthemepreview").height();
            $(".textpollthemepreviewresults").css("height", height + 'px');
            $(".view").css("display", 'none');
            $(".vote").css("text-align", 'center');
            $(".vote").css("width", '100%');
            $(".viewandvoteimagepreview").css("height", '20px');
        }
        else {

        }
    
    }
    else {
        $("#MainContent_SponsorBox").hide();
        $("#MainContent_sponsorpreviw").hide();
        $("#MainContent_sponsorResults").hide();
        $('.socialnew').css({ 'width': '100%' });
        $('.socialnew').css({ 'margin-left': '0px' });
        $('.socialnew').css({ 'margin': 'auto' });
        $('.socialnew').css({ 'margin-bottom': '-15px' });
        $('.socialnew1').css({ 'margin-bottom': '-15px' });
        var heightg = parseInt($("#MainContent_lblRecThemeHeight").text()) - sponsorlogooptionheight;
        $("#MainContent_lblRecThemeHeight").text(heightg);
        $("#MainContent_hdnRecHeightne").val(heightg);
        var ansheight = $("#MainContent_hdnansHeight").val();
        ansheight = parseInt(ansheight) + 15;
        $("#MainContent_hdnansHeight").val(ansheight);
        $(".mainanswer").css("height", ansheight + 'px');
        var answerno = $("#MainContent_hdnRepeaterTxts").val();
        answerno = parseInt(answerno);
        answerno = 20 * parseInt(answerno);
        if ($("#MainContent_chkComments").attr("checked")) {
            answerno = parseInt(answerno) + commentboxheight;
        }
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightrec = $("#MainContent_hdnRecHeightne").val();
        calculateIFrameHeight();
        $(".viewandvoteimagepreview").css("height", '16px');
        var count = 0;
        if ($("#MainContent_ckbTwitter").attr("checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").attr("checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            $("#MainContent_googleiconvote").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").attr("checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }

        if ($('#MainContent_social').is(':hidden')) {
            $(".sponsorimagepreviewres").css("width", '100%');
            $(".sponsorimagepreviewres").css("margin", 'auto');
            $(".sponsorimagepreviewres").css("margin-right", '0px');
            $('.newvotecount').css({ 'position': '' });
        }
        else {
            $(".sponsorimagepreviewres").css("width", 'auto');
            $('.newvotecount').css({ 'position': 'absolute' });
            $('.newvotecount').css({ 'padding-top': '3px' });
        }
        $('.viewandvoteimagepreview').css({ 'width': '100%' });
        $('.viewandvoteimagepreview').css({ 'margin-left': '0px' });
        $('.viewandvoteimagepreview').css({ 'margin-top': '10px' });
        $('.view').css({ 'font-size': '13px' });
        $('.view').css({ 'text-align': 'left' });
        $('.vote').css({ 'text-align': 'right' });
        $('.vote').css({ 'width': '30%' });
        $('.viewtest').css({ 'width': '70%' });
        $('.viewtest').css({ 'margin-left': '15%' });
        $('.viewtest').css({ 'margin-right': '15%' });
        
        if ($("#MainContent_ckbResultsPrivate").attr("checked")) {
            $("#MainContent_ResultBox").show();
            $("#MainContent_results").hide();
            $("#MainContent_resulttextpreview").show();
            var height = $(".textpollthemepreview").height();
            $(".textpollthemepreviewresults").css("height", height + 'px');
            $(".view").css("display", 'none');
            $(".vote").css("text-align", 'center');
            $(".vote").css("width", '100%');
            $(".viewandvoteimagepreview").css("height", '20px');
        }
        else {

        }
    }
    renderChart();
}

function Showcreate() {
    $("#MainContent_create").show();
    $("#MainContent_design").hide();
    $("#MainContent_launch").hide();
    $("#MainContent_createarrow").show();
    $("#MainContent_designarrow").hide();
    $("#MainContent_launcharrow").hide();
    var heightm = $(".textpollthemepreview").height(); ;
    var heightrec = $("#MainContent_lblHeight").text();
    if (heightrec == "") {
        heightrec = $(".textpollthemepreview").height();
    }
    $('.textpollthemepreview').css('height', heightm);
    if (heightm <= heightrec) {
        $(".poweredby").css("position", '');
        $(".viewtest").css("position", '');
        $(".poweredby").css("text-align", 'center');
        $(".poweredby").css("width", '100%');
        $(".poweredby").css("margin-top", '');
        $(".poweredby").css("margin-bottom", '5px');
        $(".poweredby").css("padding-bottom", '10px');
        $(".viewtest").css("text-align", 'center');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
        }
    }
    else {
        $(".poweredby").css("position", 'absolute');
        $(".poweredby").css("bottom", '10px');
        $(".poweredby").css("text-align", 'center');
        $(".poweredby").css("width", '100%');
        $(".poweredby").css("margin-top", '0px');
        $(".poweredby").css("margin-bottom", '0px');
        $(".poweredby").css("padding-bottom", '0px');
        $(".viewtest").css("padding-bottom", '0px');
        $(".viewtest").css("position", 'absolute');
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '10px');
        }
        else {
            $(".viewtest").css("bottom", '30px');
        };
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
        }
    }
    renderChart();
}

function Showdesign() {
    $("#MainContent_create").hide();
    $("#MainContent_design").show();
    $("#MainContent_launch").hide();
    $("#MainContent_createarrow").hide();
    $("#MainContent_designarrow").show();
    $("#MainContent_launcharrow").hide();
    var heightm = $(".textpollthemepreview").height();
    var heightrec = $("#MainContent_lblHeight").text();
    if (heightrec == "") {
        heightrec = $(".textpollthemepreview").height();
    }
    $('.textpollthemepreview').css('height', heightm);
    if (heightm <= heightrec) {
        $(".poweredby").css("position", '');
        $(".viewtest").css("position", '');
        $(".poweredby").css("text-align", 'center');
        $(".poweredby").css("width", '100%');
        $(".poweredby").css("margin-top", '');
        $(".poweredby").css("margin-bottom", '5px');
        $(".poweredby").css("padding-bottom", '10px');
        $(".viewtest").css("text-align", 'center');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
        }
    }
    else {
        $(".poweredby").css("position", 'absolute');
        $(".poweredby").css("bottom", '10px');
        $(".poweredby").css("text-align", 'center');
        $(".poweredby").css("width", '100%');
        $(".poweredby").css("margin-top", '0px');
        $(".poweredby").css("margin-bottom", '0px');
        $(".poweredby").css("padding-bottom", '0px');
        $(".viewtest").css("position", 'absolute');
        if ($('#MainContent_poweredby').is(':hidden')) {
            $(".viewtest").css("bottom", '10px');
        }
        else {
            $(".viewtest").css("bottom", '30px');
        }
        $(".viewtest").css("padding-bottom", '0px');
        $(".sponsorimagepreview").css("margin-top", '3px');
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            $(".viewtest").css("width", '70%');
        }
        else {
            $(".viewtest").css("width", '100%');
        }
    }
}

function Showlaunch() {
    $(".poweredby").css("position", '');
    $(".viewtest").css("position", '');
    $(".viewtest").css("bottom", '');
    $(".poweredby").css("bottom", '');
    $(".poweredby").css("margin-top", '5px');
    $(".poweredby").css("padding-bottom", '10px');
  
    $("#textpollthemepreview").css("height", 'auto');
    var height = $(".textpollthemepreview").height();
    $("#textpollthemepreviewresults").css("height", 'auto');
    $("#textpollthemepreviewresults").css("height", height + 'px');
    
    $("#MainContent_txtHeightIframe").val(height);
    var heightrec = $("#MainContent_hdnRecHeight").val(); 
    var heightg = $("#MainContent_lblRecThemeHeight").text();
        $("#MainContent_hdnRecWidth").val(width);
        if (heightg == "") {
            $("#MainContent_hdnRecHeightne").val(height);
            $("#MainContent_lblHeight").text(height);
        }
        else {
            $("#MainContent_lblHeight").text(heightg);
        }
        $("#MainContent_lblWidth").text(width);
        $('.textpollthemepreview').css('height', 'auto');
        var height = $(".textpollthemepreview").height();
        var fixheight = $("#MainContent_txtHeightPoll").val();
        var heightsel = $("#MainContent_hdnSetHeight").val();
        if (heightsel == "") {
            if (fixheight > heightg) {
                $("#MainContent_txtHeightPoll").val(heightg);
                var fixheight = $("#MainContent_txtHeightPoll").val();
            }
            if (heightg > fixheight) {
                $(".textpollthemepreviewresults").css("height", heightg + 'px');
                $(".textpollthemepreview").css("height", heightg + 'px');
                $("#MainContent_txtHeightPoll").val(heightg);
                $(".poweredby").css("position", '');
                $(".viewtest").css("position", '');
                $(".viewtest").css("bottom", '');
                $(".poweredby").css("bottom", '');
                $(".poweredby").css("margin-top", '5px');
                $(".poweredby").css("padding-bottom", '10px');
                if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                    $(".viewtest").css("width", '70%');
                }
                else {
                    $(".viewtest").css("width", '100%');
                }
            }
            else {
                $(".textpollthemepreviewresults").css("height", fixheight + 'px');
                $(".textpollthemepreview").css("height", fixheight + 'px');
                $("#MainContent_txtHeightPoll").val(fixheight);
                $(".poweredby").css("position", 'absolute');
                $(".poweredby").css("bottom", '10px');
                $(".poweredby").css("text-align", 'center');
                $(".poweredby").css("width", '100%');
                $(".poweredby").css("margin-top", '0px');
                $(".poweredby").css("margin-bottom", '0px');
                $(".poweredby").css("padding-bottom", '0px');
                $(".viewtest").css("position", 'absolute');
                $(".viewtest").css("padding-bottom", '0px');
                $(".sponsorimagepreview").css("margin-top", '3px');
                if ($('#MainContent_poweredby').is(':hidden')) {
                    $(".viewtest").css("bottom", '10px');
                }
                else {
                    $(".viewtest").css("bottom", '30px');
                }
                if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                    $(".viewtest").css("width", '70%');
                }
                else {
                    $(".viewtest").css("width", '100%');
                }
            }
        }
        else {
            if (heightg > heightsel) {
                $(".textpollthemepreviewresults").css("height", heightg + 'px');
                $(".textpollthemepreview").css("height", heightg + 'px');
                $("#MainContent_txtHeightPoll").val(heightg);
                if (heightg == fixheight) {
                    $(".poweredby").css("position", 'absolute');
                    $(".poweredby").css("bottom", '10px');
                    $(".poweredby").css("text-align", 'center');
                    $(".poweredby").css("width", '100%');
                    $(".poweredby").css("margin-top", '0px');
                    $(".poweredby").css("margin-bottom", '0px');
                    $(".poweredby").css("padding-bottom", '0px');
                    $(".viewtest").css("position", 'absolute');
                    $(".viewtest").css("padding-bottom", '0px');
                    $(".sponsorimagepreview").css("margin-top", '3px');
                    if ($('#MainContent_poweredby').is(':hidden')) {
                        $(".viewtest").css("bottom", '10px');
                    }
                    else {
                        $(".viewtest").css("bottom", '30px');
                    }
                    if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                        $(".viewtest").css("width", '70%');
                    }
                    else {
                        $(".viewtest").css("width", '100%');
                    }
                }
                else
                {
                $(".poweredby").css("position", '');
                $(".viewtest").css("position", '');
                $(".viewtest").css("bottom", '');
                $(".poweredby").css("bottom", '');
                $(".poweredby").css("margin-top", '5px');
                $(".poweredby").css("padding-bottom", '10px');
                if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                    $(".viewtest").css("width", '70%');
                }
                else {
                    $(".viewtest").css("width", '100%');
                }
                }
            }
            else {
                $(".textpollthemepreviewresults").css("height", heightsel + 'px');
                $(".textpollthemepreview").css("height", heightsel + 'px');
                $("#MainContent_txtHeightPoll").val(heightsel);
                $(".poweredby").css("position", 'absolute');
                $(".poweredby").css("bottom", '10px');
                $(".poweredby").css("text-align", 'center');
                $(".poweredby").css("width", '100%');
                $(".poweredby").css("margin-top", '0px');
                $(".poweredby").css("margin-bottom", '0px');
                $(".poweredby").css("padding-bottom", '0px');
                $(".viewtest").css("position", 'absolute');
                $(".viewtest").css("padding-bottom", '0px');
                $(".sponsorimagepreview").css("margin-top", '3px');
                if ($('#MainContent_poweredby').is(':hidden')) {
                    $(".viewtest").css("bottom", '10px');
                }
                else {
                    $(".viewtest").css("bottom", '30px');
                }
                if ($('#MainContent_sponsorpreviw').is(':hidden')) {
                    $(".viewtest").css("width", '70%');
                }
                else {
                    $(".viewtest").css("width", '100%');
                }
            }
        }
        return;
}

function setHeight() {
    var height = $(".textpollthemepreview").height();
    $("#MainContent_txtHeightIframe").val(height);
    var heightrec = $("#MainContent_hdnRecHeight").val();
    if (heightrec == "") {
        $("#MainContent_hdnRecWidth").val(width);
        $("#MainContent_hdnRecHeightne").val(height);
        $("#MainContent_lblWidth").text(width);
        $("#MainContent_lblHeight").text(height);
        return;
    }
}

function removeAnswerOption(e) {
    if (window.event) {
        e = window.event;
    }

    target = e.srcElement ? e.srcElement : e.target;

    if ($('#MainContent_recheightincreate').is(':hidden')) {
        $("#MainContent_lblAlreadyExist").text("Cannot delete Answer Option after launch");
        $("#answererr").show();
        return false;
    }
    var rows = document.getElementById("pollansweroptions").rows.length;
    if (parseInt(rows) <= 2) {
        $("#MainContent_lblAlreadyExist").text("Please enter minimum number of answer options");
        $("#answererr").show();
        return false;
    }
    else {
        $("#answererr").hide();
    }

    var ansOptionTable = document.getElementById("pollansweroptions");
    var previewtable = document.getElementById("previewansweroptions");

    var id = target.id;
    var suffix = id.match(/\d+/);
    id = parseInt(suffix);

    RearrangeAnswerOptionIdsAfterDelete(id);

    ansOptionTable.deleteRow(id);
    previewtable.deleteRow(id);

    $("#MainContent_hdnRepeaterTxts").val(document.getElementById("pollansweroptions").rows.length);

    calculateIFrameHeight();
    renderChart();

    return false;
}

function addAnswerOption(e) {
    if (window.event) {
        e = window.event;
    }

    target = e.srcElement ? e.srcElement : e.target;

    if ($('#MainContent_recheightincreate').is(':hidden'))
    {
        $("#MainContent_lblAlreadyExist").text("Cannot add Answer Options after launch");
        $("#answererr").show();
        return false;
    }

    var rows = document.getElementById("pollansweroptions").rows.length;
    if (parseInt(rows) >= 10) {
        $("#MainContent_lblAlreadyExist").text("Maximum 10 Answer Options can be added");
        $("#answererr").show();
        return false;
    }
    else {
        $("#answererr").hide();
    }


    var ansOptionTable = document.getElementById("pollansweroptions");
    var previewtable = document.getElementById("previewansweroptions");

    var id = target.id;
    var suffix = id.match(/\d+/);
    id = parseInt(suffix) + 1;

    RearrangeAnswerOptionIds(id);
    var previewrow = previewtable.insertRow(id);
    previewrow.className = "txtRow";
    var cellpreview1 = previewrow.insertCell(0);
    if ($("#MainContent_radMultipleAnswer").attr("checked")) {
        cellpreview1.innerHTML = '<input type="radio" id="radButton_' + id + '">';
        cellpreview1.className = "singleanswers";
        $('.singleanswers').hide();
        var cellpreview1 = previewrow.insertCell(1);
        cellpreview1.innerHTML = '<input type="checkbox" id="radButton_' + id + '" />';
        cellpreview1.className = "multipleanswers";
        $('.multipleanswers').show();
        cellpreview1 = previewrow.insertCell(2);
        cellpreview1.innerHTML = '<label id="lblradText_' + id + '"  class="wrapnew"/>';
    }
    else {
        cellpreview1.innerHTML = '<input type="radio" id="radButton_' + id + '">';
        cellpreview1.className = "singleanswers";
        $('.singleanswers').show();
        var cellpreview1 = previewrow.insertCell(1);
        cellpreview1.innerHTML = '<input type="checkbox" id="radButton_' + id + '" />';
        cellpreview1.className = "multipleanswers";
        $('.multipleanswers').hide();
        cellpreview1 = previewrow.insertCell(2);
        cellpreview1.innerHTML = '<label id="lblradText_' + id + '"  class="wrapnew"/>';
    }
    var row = ansOptionTable.insertRow(id);
    row.className = "txtrow";
    var cell1 = row.insertCell(0);

    cell1.innerHTML = '<input type="hidden" id="hdnAnswerId' + id + '" value="0">';
    var cell2 = row.insertCell(1);
    cell2.innerHTML = '<input type="text" name="txtAnswerOption_' + id + '" id="txtAnswerOption_' + id + '" class="textBoxMedium txtAns" />';
    cell1 = row.insertCell(2);
    cell1.innerHTML = '<input type="button" id="btnRemoveAnother_' + id + '" class="RemoveIcon" onclick="return removeAnswerOption(event);" />';
    cell1 = row.insertCell(3);
    cell1.innerHTML = '<input type="button" id="btnAddAnother_' + id + '" onclick="return addAnswerOption(event);" class="addedIcon" />';

    $("#MainContent_hdnRepeaterTxts").val(document.getElementById("pollansweroptions").rows.length);
    calculateIFrameHeight();
    renderChart();
    return true;
}

function RearrangeAnswerOptionIdsAfterDelete(id) {
    //Only Button id need to be changed since that determines the position
    if (id <= (document.getElementById("pollansweroptions").rows.length - 1)) {
        for (var i = id; i <= document.getElementById("pollansweroptions").rows.length - 1; i++) {
            var newId = parseInt(i) - 1;
            document.getElementById('btnAddAnother_' + i).id = "btnAddAnother_" + newId; 
            document.getElementById('btnRemoveAnother_' + i).id = "btnRemoveAnother_" + newId;
            document.getElementById('txtAnswerOption_' + i).setAttribute("name", "txtAnswerOption_" + newId);
            document.getElementById('txtAnswerOption_' + i).id = "txtAnswerOption_" + newId;
            document.getElementById('lblradText_' + i).id = "lblradText_" + newId;
            var element = document.getElementById('radButton_' + i);
            if (typeof (element) != 'undefined' && element != null) {
                document.getElementById('radButton_' + i).id = "radButton_" + newId;
            }
            else {
                
            }
            element = document.getElementById('ckbButton_' + i);
            if (typeof (element) != 'undefined' && element != null) {
                document.getElementById('ckbButton_' + i).id = "ckbButton_" + newId;
            }
            else {
                
            }
        }
    }
}

function RearrangeAnswerOptionIds(id) {
//Only Button id need to be changed since that determines the position

    if (id <= (document.getElementById("pollansweroptions").rows.length- 1)) {
        for (var i = document.getElementById("pollansweroptions").rows.length - 1; i >= id; i--) {
            var newId = parseInt(i) + 1;
            document.getElementById('btnAddAnother_' + i).id = "btnAddAnother_" + newId;
            document.getElementById('btnRemoveAnother_' + i).id = "btnRemoveAnother_" + newId;
            document.getElementById('txtAnswerOption_' + i).setAttribute("name", "txtAnswerOption_" + newId);
            document.getElementById('txtAnswerOption_' + i).id = "txtAnswerOption_" + newId;
            document.getElementById('lblradText_' + i).id = "lblradText_" + newId;
            var element = document.getElementById('radButton_' + i);
            if (typeof (element) != 'undefined' && element != null) {
                document.getElementById('radButton_' + i).id = "radButton_" + newId;
            }
            else {
                
            }
            element = document.getElementById('ckbButton_' + i);
            if (typeof (element) != 'undefined' && element != null) {
                document.getElementById('ckbButton_' + i).id = "ckbButton_" + newId;
            }
            else {
                
            }
        }
    }
}

function ShowDefaultThemes() {
    $("#customthemes").hide();
    $("#MainContent_defaultthemes").show();
    $(".PollDivsub a").css("color", "#7D2AC7");
    $(".PollDivsub a").css("font-weight", "bold");
    $(".PollDivsubinner a").css("color", "#948E8E");
    $(".PollDivsubinner a").css("font-weight", "normal");
}

function ShowCustomThemes() {
    $("#customthemes").show();  
    $("#MainContent_defaultthemes").hide();
    $(".PollDivsubinner a").css("color", "#7D2AC7");
    $(".PollDivsubinner a").css("font-weight", "bold");
    $(".PollDivsub a").css("color", "#948E8E");
    $(".PollDivsub a").css("font-weight", "normal");
}

function showGoogletxt() {
    if ($("#MainContent_ckbGoogle").attr("checked")) {
        $("#MainContent_google").show();
        $("#MainContent_social").show();
        $("#MainContent_googleicon").show();
        $("#MainContent_googleiconvote").show();
        $(".sponsorimagepreviewres").css("margin-right", '2%');
        $(".sponsorimagepreviewres").css("width", '31%');
        var count = 0;
        if ($("#MainContent_ckbTwitter").attr("checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").attr("checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_googleiconvote").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").attr("checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
    }
    else {
        $("#MainContent_google").hide();
        $("#MainContent_googleicon").hide();
        $("#MainContent_googleiconvote").hide();
        var count = 0;
        if ($("#MainContent_ckbTwitter").attr("checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").attr("checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_socialsha").show();
            $("#MainContent_social").show();
            $("#MainContent_googleiconvote").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").attr("checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_socialsha").show();
            $("#MainContent_social").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            if (count == 0) {
                $("#MainContent_socialsha").hide();
                $('.newvotecount').css({ 'position': '' });
            }
        }
        else {
            if (count == 0) {
                $('.sponsorimagepreviewres').css({ 'width': '100%' });
                $('.sponsorimagepreviewres').css({ 'margin': 'auto' });
                $('.sponsorimagepreviewres').css({ 'margin-right': '0px' });
                $("#MainContent_socialsha").hide();
            }
        }
    }
    calculateIFrameHeight();
}

function showTwittertxt() {
    if ($("#MainContent_ckbTwitter").attr("checked")) {
        $("#MainContent_twitter").show();
        $("#MainContent_twittericon").show();
        $("#MainContent_twittericonvote").show();
        $("#MainContent_social").show();
        $("#MainContent_socialsha").show();
        if ($("#MainContent_txtTwitter").val() == "") {
            $("#MainContent_txtTwitter").val('Check out this interesting Poll')
        }
        $(".sponsorimagepreviewres").css("margin-right", '2%');
        $(".sponsorimagepreviewres").css("width", '31%');
        var count = 0;
        if ($("#MainContent_ckbTwitter").attr("checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").attr("checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_googleiconvote").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").attr("checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
    }
    else {
        $("#MainContent_twittericonvote").hide();
        $("#MainContent_twitter").hide();
        $("#MainContent_twittericon").hide();
        var count = 0;
        if ($("#MainContent_ckbTwitter").attr("checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").attr("checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            $("#MainContent_googleiconvote").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").attr("checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            if (count == 0) {
                $("#MainContent_socialsha").hide();
                $('.newvotecount').css({ 'position': '' });
            }
        }
        else {
            if (count == 0) {
                $('.sponsorimagepreviewres').css({ 'width': '100%' });
                $('.sponsorimagepreviewres').css({ 'margin': 'auto' });
                $('.sponsorimagepreviewres').css({ 'margin-right': '0px' });
                 $("#MainContent_socialsha").hide();
                }
        }
    }

    calculateIFrameHeight();
}

function voteShow() {
     if ($("#MainContent_ckbVotes").attr("checked")) {
        
         var votesindex = $("#MainContent_lblQuestion").text().indexOf('Votes');
         //  $("#MainContent_votescount").show();
         if (votesindex == '-1') {
             $("#MainContent_lblQuestion").text($("#MainContent_lblQuestion").text());
             $("#MainContent_lblQuestionResults").text($("#MainContent_lblQuestionResults").text() + '(39 votes)');
         }
         else {
             $("#MainContent_lblQuestion").text($("#MainContent_lblQuestion").text());
             $("#MainContent_lblQuestionResults").text($("#MainContent_lblQuestionResults").text());
         }
     }
     else {
       
         // $("#MainContent_votescount").hide();
         $("#MainContent_lblQuestion").text($("#MainContent_lblQuestion").text());
         $("#MainContent_lblQuestionResults").text($("#MainContent_lblQuestionResults").text().replace('(39 votes)', ''));
     }
 }

function showFbtxt() {
    if ($("#MainContent_ckbFb").attr("checked")) {
        $("#MainContent_fb").show();
        $("#MainContent_fbicon").show();
        $("#MainContent_fbiconvote").show();
        $("#MainContent_social").show();
        $("#MainContent_socialsha").show();
        $(".sponsorimagepreviewres").css("margin-right", '2%');
        $(".sponsorimagepreviewres").css("width", '31%');
        var count = 0;
        if ($("#MainContent_ckbTwitter").attr("checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").attr("checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            $("#MainContent_googleiconvote").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").attr("checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
    }
    else {
        $("#MainContent_fb").hide();
        $("#MainContent_fbicon").hide();
        $("#MainContent_fbiconvote").hide();
        var count = 0;
        if ($("#MainContent_ckbTwitter").attr("checked")) {
            $("#MainContent_twitter").show();
            $("#MainContent_twittericon").show();
            $("#MainContent_twittericonvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_twitter").hide();
            $("#MainContent_twittericon").hide();
            $("#MainContent_twittericonvote").hide();
        }
        if ($("#MainContent_ckbGoogle").attr("checked")) {
            $("#MainContent_google").show();
            $("#MainContent_googleicon").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            $("#MainContent_googleiconvote").show();
            count++;
        }
        else {
            $("#MainContent_google").hide();
            $("#MainContent_googleicon").hide();
            $("#MainContent_googleiconvote").hide();
        }
        if ($("#MainContent_ckbFb").attr("checked")) {
            $("#MainContent_fb").show();
            $("#MainContent_fbicon").show();
            $("#MainContent_fbiconvote").show();
            $("#MainContent_social").show();
            $("#MainContent_socialsha").show();
            count++;
        }
        else {
            $("#MainContent_fb").hide();
            $("#MainContent_fbicon").hide();
            $("#MainContent_fbiconvote").hide();
        }
        if ($('#MainContent_sponsorpreviw').is(':hidden')) {
            if (count == 0) {
                $("#MainContent_socialsha").hide();
                $('.newvotecount').css({ 'position': '' });
            }
        }
        else {
            if (count == 0) {
                $('.sponsorimagepreviewres').css({ 'width': '100%' });
                $('.sponsorimagepreviewres').css({ 'margin': 'auto' });
                $('.sponsorimagepreviewres').css({ 'margin-right': '0px' });
                $("#MainContent_socialsha").hide();
            }
        }
    }
    calculateIFrameHeight();
}

function generatechartRemove(id) {
    //    var chart_pollresponsechart41 = new FusionCharts({ "dataFormat": "xml", "scaleMode": "noScale", "renderAt": "pollresponsechart41Div", "id": "pollresponsechart41", "debugMode": "0", "lang": "EN", "swfUrl": "../Charts/Bar2D.swf", "wMode": "opaque", "width": "100%", "height": "40", "registerWithJS": "1", "dataSource": "<chart caption='' subcaption='' maxLabelWidthPercent='20'   LogoURL='http://127.0.0.1:8010/Polls/images/Insighto_logo.png' logoPosition='CC' logoAlpha='30' logoScale='100' chartTopMargin='0' chartBottomMargin='0' chartLeftMargin='5' yaxisname='' borderAlpha='0' canvasbgAlpha='0' numDivLines='0' divLineAlpha='0' showYAxisValues='0' showXAxisValues='0' divLineColor='#FFFFFF'  labelDisplay='' showvalues='1' xaxisname='' alternatevgridcolor='#FFFFFF'  useroundedges='1'  showborder='1' bgcolor='#FFFFFF' numbersuffix='%' baseFont='Arial' baseFontSize ='12'  baseFontColor ='000000' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'><set label='adfasdas' value='40' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles><set label='"+id+"' value='60' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles></chart>" }).render();

    var chartstr = "<chart caption='' subcaption='' maxLabelWidthPercent='20'   LogoURL='http://insighto.com:8010/Polls/images/Insighto_logo.png' logoPosition='CC' logoAlpha='30' logoScale='100' chartTopMargin='0' chartBottomMargin='0' chartLeftMargin='5' yaxisname='' borderAlpha='0' canvasbgAlpha='0' numDivLines='0' divLineAlpha='0' showYAxisValues='0' showXAxisValues='0' divLineColor='#FFFFFF'  labelDisplay='' showvalues='1' xaxisname='' alternatevgridcolor='#FFFFFF'  useroundedges='1'  showborder='1' bgcolor='#FFFFFF' numbersuffix='%' baseFont='Arial' baseFontSize ='12'  baseFontColor ='000000' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>";
    var setlbl = "";
    var strhgt = 20 * id;

    if (id == 2) {
        var myArray1 = ['40', '60'];
    }
    else if (id == 3) {
        var myArray1 = ['20', '30', '50'];
    }
    else if (id == 4) {
        var myArray1 = ['10', '20', '30', '40'];
    }
    else if (id == 5) {
        var myArray1 = ['10', '15', '20', '25', '30'];
    }
    else if (id == 6) {
        var myArray1 = ['5', '10', '15', '20', '22','28'];
    }
    else if (id == 7) {
        var myArray1 = ['5', '10', '12', '13', '17', '20','23'];
    }
    else if (id == 8) {
        var myArray1 = ['5', '7', '9', '11', '13', '15', '19','21'];
    }
    else if (id == 9) {
        var myArray1 = ['1', '3', '6', '9', '10', '14', '17', '19','21'];
    }
    else if (id == 10) {
        var myArray1 = ['1', '3', '5', '7', '9', '11', '13', '15', '17','19'];
    }
    for (i = 0; i < id; i++) {
        var currelement1 = myArray1[i];
    //  alert(currelement1);
        var colint1 = i+1;
        setlbl += "<set label='Answer" + colint1 + "' value='" + currelement1 + "' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles>";
    }
    chartstr = chartstr + setlbl;
    chartstr = chartstr + "</chart>";

   // alert(chartstr);
    //var chart_pollresponsechart = new FusionCharts({ "dataFormat": "xml", "scaleMode": "noScale", "renderAt": "pollresponsechartDiv", "id": "pollresponsechart", "debugMode": "0", "lang": "EN", "swfUrl": "../Charts/Bar2D.swf", "wMode": "opaque", "width": "100%", "height": strhgt, "registerWithJS": "1", "dataSource": chartstr }).render();
}

function generatechart(id) {
    //    var chart_pollresponsechart41 = new FusionCharts({ "dataFormat": "xml", "scaleMode": "noScale", "renderAt": "pollresponsechart41Div", "id": "pollresponsechart41", "debugMode": "0", "lang": "EN", "swfUrl": "../Charts/Bar2D.swf", "wMode": "opaque", "width": "100%", "height": "40", "registerWithJS": "1", "dataSource": "<chart caption='' subcaption='' maxLabelWidthPercent='20'   LogoURL='http://127.0.0.1:8010/Polls/images/Insighto_logo.png' logoPosition='CC' logoAlpha='30' logoScale='100' chartTopMargin='0' chartBottomMargin='0' chartLeftMargin='5' yaxisname='' borderAlpha='0' canvasbgAlpha='0' numDivLines='0' divLineAlpha='0' showYAxisValues='0' showXAxisValues='0' divLineColor='#FFFFFF'  labelDisplay='' showvalues='1' xaxisname='' alternatevgridcolor='#FFFFFF'  useroundedges='1'  showborder='1' bgcolor='#FFFFFF' numbersuffix='%' baseFont='Arial' baseFontSize ='12'  baseFontColor ='000000' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'><set label='adfasdas' value='40' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles><set label='"+id+"' value='60' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles></chart>" }).render();


    var chartstr = "<chart caption='' subcaption='' maxLabelWidthPercent='20'   LogoURL='http://insighto.com:8010/Polls/images/Insighto_logo.png' logoPosition='CC' logoAlpha='30' logoScale='100' chartTopMargin='0' chartBottomMargin='0' chartLeftMargin='5' yaxisname='' borderAlpha='0' canvasbgAlpha='0' numDivLines='0' divLineAlpha='0' showYAxisValues='0' showXAxisValues='0' divLineColor='#FFFFFF'  labelDisplay='' showvalues='1' xaxisname='' alternatevgridcolor='#FFFFFF'  useroundedges='1'  showborder='1' bgcolor='#FFFFFF' numbersuffix='%' baseFont='Arial' baseFontSize ='12'  baseFontColor ='000000' exportFormats='JPEG=Export as JPEG|PNG=Export as PNG|PDF=Export as PDF'  palettecolors='#AFD8F8,#F6BD0F,#8BBA00,#A66EDD,#F984A1'>";
    var setlbl = "";
    var strhgt = 20 * id;

    var noofcols = id;
    if (noofcols == 2) {
        var myArray = ['40', '60'];
    }
    else if (noofcols == 3) {
        var myArray = ['20', '30', '50'];
    }
    else if (noofcols == 4) {
        var myArray = ['10', '20', '30', '40'];
    }
    else if (noofcols == 5) {
        var myArray = ['10', '15', '20', '25', '30'];
    }
    else if (noofcols == 6) {
        var myArray = ['5', '10', '15', '20', '22', '28'];
    }
    else if (noofcols == 7) {
        var myArray = ['5', '10', '12', '13', '17', '20', '23'];
    }
    else if (noofcols == 8) {
        var myArray = ['5', '7', '9', '11', '13', '15', '19', '21'];
    }
    else if (noofcols == 9) {
        var myArray = ['1', '3', '6', '9', '10', '14', '17', '19', '21'];
    }
    else if (noofcols == 10) {
        var myArray = ['1', '3', '5', '7', '9', '11', '13', '15', '17', '19'];
    }

    for (i = 0; i < noofcols; i++) {
        var currelement = myArray[i];
      //  alert(currelement);
        var colint = i + 1;
        setlbl += "<set label='Answer" + colint + "' value='" + currelement + "' /><styles><definition><style name='myLabelsFont' type='font' font='Arial' size='12' color='000000'  bold='0' underline='none'/></definition><application><apply toObject='DataLabels' styles='myLabelsFont' /></application></styles>";
    }
    chartstr = chartstr + setlbl;
    chartstr = chartstr + "</chart>";

  //  alert(chartstr);
   // var chart_pollresponsechart = new FusionCharts({ "dataFormat": "xml", "scaleMode": "noScale", "renderAt": "pollresponsechartDiv", "id": "pollresponsechart", "debugMode": "0", "lang": "EN", "swfUrl": "../Charts/Bar2D.swf", "wMode": "opaque", "width": "100%", "height": strhgt, "registerWithJS": "1", "dataSource": chartstr }).render();
}

function getAnswerOptionsHeight() {
    var chartanswerrowcount = parseInt($("#MainContent_hdnRepeaterTxts").val());
    var chartanswerrowcountnew = 0;
    for (var row = 0; row < chartanswerrowcount; row++) {
        if (document.getElementById('txtAnswerOption_' + row) != null) {
            chartanswerrowcountnew++;
        }
    }
    var chartheight = chartanswerrowcountnew * 5;
    return chartheight;
}

function stripSpecialChars(inputval) {
    while (inputval.indexOf("<") >= 0) {
        inputval = inputval.replace("<", "&lt;");
    }
    while (inputval.indexOf(">") >= 0) {
        inputval = inputval.replace(">", "&gt;");
    }
    while (inputval.indexOf("'") >= 0) {
        inputval = inputval.replace("'", "`");
    }
    return inputval;
}

function checkRowsForHeightAdjustment(numberofrows) {
    var extraspacing = 0;
    if (numberofrows >= 5) {
        singleansweroptionheight = 25;
    }
    else if (numberofrows >= 4) {
        singleansweroptionheight = 12;
    }
    else {
        singleansweroptionheight = 5;
    }

    if ((!$('#MainContent_SponsorBox').is(':hidden')) || ($("#MainContent_ckbGoogle").attr("checked")) || ($("#MainContent_ckbFb").attr("checked")) || ($("#MainContent_ckbTwitter").attr("checked"))) {
        if (!$('#MainContent_commentbox').is(':hidden')) {
            if (numberofrows >= 5) {
                extraspacing = 15;
            }
            else if (numberofrows >= 4) {
                extraspacing = 5;
            }
            else {
                extraspacing = 0;
            }
        }
        else {
            if (numberofrows >= 5) {
                extraspacing = 25;
            }
            else if (numberofrows >= 4) {
                extraspacing = 35;
            }
            else {
                extraspacing = 50;
            }
        }
        singleansweroptionheight = singleansweroptionheight + extraspacing;
    }

}

function checkLogoAndSponsorForHeightAdjustment() {
    var numberofrows = parseInt($("#MainContent_hdnRepeaterTxts").val());
    if (($('#MainContent_ckbAddSponsor').is('checked')) || ($("#MainContent_ckbGoogle").attr("checked")) || ($("#MainContent_ckbFb").attr("checked")) || ($("#MainContent_ckbTwitter").attr("checked"))) {
        if ((!$('#MainContent_commentbox').is(':hidden')) && (numberofrows >= 5)) {
            sponsorlogooptionheight = 10;
        }
        else if ((!$('#MainContent_commentbox').is(':hidden')) && (numberofrows < 5)) {
            sponsorlogooptionheight = 0;
        }
    }
}
function checkCommentsBoxHeightAdjustment() {
    var numberofrows = parseInt($("#MainContent_hdnRepeaterTxts").val());
    if (($('#MainContent_ckbAddSponsor').is('checked')) || ($("#MainContent_ckbGoogle").attr("checked")) || ($("#MainContent_ckbFb").attr("checked")) || ($("#MainContent_ckbTwitter").attr("checked")) || (numberofrows > 6)) {
        commentboxheight = 0;
    }
}

function checkSocialMediaHeightAdjustment() {
    if ((!$('#MainContent_ckbAddSponsor').is('checked')) && ($('#MainContent_commentbox').is(':hidden'))) {
        var fixheight = parseInt($("#MainContent_txtHeightPoll").val());
        var heightrec = parseInt($("#MainContent_hdnRecHeightne").val());
        heightrec = heightrec + sponsorlogooptionheight;
        if (heightrec > fixheight) {
            $("#MainContent_showrec").show();
            $("#MainContent_lblRecHeightbPre").text(heightrec);
        }
        else {
            $("#MainContent_showrec").hide();
        }
    }
    else {
    }
}

var iframeheight = 0;
var iframeheightwithchart = 0;
var iframewidth = 0;
function calculateIFrameHeight() {
    //var numberofrows = parseInt($("#MainContent_hdnRepeaterTxts").val());
    var numberofrows = document.getElementById("pollansweroptions").rows.length;
    var polltype = $("#MainContent_hdnPollType").val();
    var answerrowheight = 35;
    var chartrowheight = 50;
    var sponsorheight = 50;
    var socialmediaheight = 50;
    var poweredbyheight = 30;
    var commentbox = 60;
    var polltypeheight = 0;
    var votebuttonheight = 80;
    var bottompadding = 0;
    var imageheight = 0;
    var videoheight = 0;
    var videolink = $("#MainContent_txtVideo").val();
    iframeheight = 0;
    iframeheightwithchart = 0;
    
    if (!isNaN(parseInt($("#MainContent_txtWidthPoll").val()))) {
        iframewidth = parseInt($("#MainContent_txtWidthPoll").val());
    }
    else if (!isNaN(parseInt($("#MainContent_txtWidthIframe1").val()))) {
        iframewidth = parseInt($("#MainContent_txtWidthIframe1").val());
    }
    else if (!isNaN(parseInt($("#MainContent_txtWidthIframe").val()))) {
        iframewidth = parseInt($("#MainContent_txtWidthIframe").val());
    }
    
    if (polltype == "video") {
        videoheight = parseInt($('#MainContent_videopreviewcreate').height());
        if (videoheight == 0) {
            videoheight = 168;
        }
        answerrowheight = 36;
    }
    else if (polltype == "image") {
        imageheight = parseInt($('#MainContent_imagepreview').height());
        answerrowheight = 38;
    }
    iframeheight = ((numberofrows) * answerrowheight);
    iframeheightwithchart = ((numberofrows) * chartrowheight);
    //console.log(iframeheightwithchart);
    if (numberofrows == 5)
    {
        iframeheightwithchart = iframeheightwithchart - 20;
    }
    else if (numberofrows == 6) {
        iframeheightwithchart = iframeheightwithchart - 30;
    }
    else if (numberofrows == 7) {
        iframeheightwithchart = iframeheightwithchart - 40;
    }
    else if (numberofrows == 8) {
        iframeheightwithchart = iframeheightwithchart - 50;
    }
    else if (numberofrows >= 9) {
        iframeheightwithchart = iframeheightwithchart - 60;
    }
    console.log(iframeheightwithchart);

    if (($('#MainContent_ckbAddSponsor').attr('checked')) || ($("#MainContent_ckbGoogle").attr("checked")) || ($("#MainContent_ckbFb").attr("checked")) || ($("#MainContent_ckbTwitter").attr("checked"))) {
        //iframeheightwithchart = iframeheightwithchart + sponsorheight;
        if ((!$('#MainContent_commentbox').is(':hidden')) && ($('#MainContent_ckbAddSponsor').attr('checked'))) {
            iframeheight = iframeheight + 30;
        }
        else {
            iframeheight = iframeheight + sponsorheight;
            iframeheightwithchart = iframeheightwithchart + sponsorheight;
        }
    }

    if ((!$('#MainContent_commentbox').is(':hidden'))) {
        iframeheight = iframeheight + commentbox;
    }

    iframeheight = iframeheight + votebuttonheight + bottompadding;

    if (!$('#MainContent_poweredby').is(':hidden')) {
        iframeheight = iframeheight + poweredbyheight;
        iframeheightwithchart = iframeheightwithchart + poweredbyheight;
    }
   //console.log(iframeheightwithchart);

    iframeheightwithchart = iframeheightwithchart + imageheight + videoheight;
    iframeheight = iframeheight + imageheight + videoheight;

    $("#previewansweroptions").css("height", (numberofrows * answerrowheight).toString() + "px")
    $("#previewansweroptions").css("margin-top", "-10px")

    console.log("Number of rows: " + numberofrows.toString() + " ROW HEIGHT: " + answerrowheight.toString());
    console.log("COMMENT HEIGHT: " + commentboxheight.toString() + " POWERED BY: " + poweredbyheight.toString());
    console.log("BUTTON HEIGHT: " + votebuttonheight.toString() + " PADDING: " + bottompadding.toString());
    console.log("IMAGE HEIGHT: " + imageheight.toString() + " VIDEO HEIGHT: " + videoheight.toString());
    console.log("FRAME HEIGHT ANSWERS: " + iframeheight.toString() + " FRAME HEIGHT CHART: " + iframeheightwithchart.toString());

    if (!$('#MainContent_ResultBox').is(':hidden')) {
        iframeheightwithchart = 0;
    }
    if (iframeheight < iframeheightwithchart) {
        iframeheight = iframeheightwithchart;
    }
    var fixheight = parseInt($("#MainContent_txtHeightPoll").val());

    $("#MainContent_lblRecThemeHeight").text(iframeheight);
    $("#MainContent_hdnRecHeightne").val(iframeheight);
    $("#MainContent_lblRecHeightbPre").html(iframeheight);
    $("#MainContent_lblHeight").html(iframeheight);
    $("#MainContent_lblHeight1").html(iframeheight);
    $("#MainContent_lblRecThemeWidth").html(iframewidth);
    $("#MainContent_lblWidth").html(iframewidth);
    $("#MainContent_lblWidth1").html(iframewidth);
    
    if (fixheight < iframeheight) {
        $("#MainContent_showrec").show();
    }
    else {
        $("#MainContent_showrec").hide();
    }
}
