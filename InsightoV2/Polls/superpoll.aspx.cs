﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using System.Data;
using App_Code;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Media;
using System.Web.UI.HtmlControls;
using FeatureTypes;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


public partial class Polls_superpoll : System.Web.UI.Page
{
     //string pollType="";
     public string SAVE_PATH;
     public string SAVE_RETPATH;
    //PollCreation Pc = new PollCreation();
   public DataTable dtsettings = new DataTable();
   public string newpollpath;
    //DataTable dtSettingsInfo = new DataTable();
    //int selPollId;
    //int pollId = 0; string mode="";
    //int copyPollId = 0;
    //DataSet pollInfo = new DataSet();
    ////Hashtable typeHt = new Hashtable();
    //string questionType = "";
    //string editPollName = "";
    //int widthpan = 0;
    //string foldername = "";


    Hashtable typeHt = new Hashtable();
    //int pollId = 0;
    DataSet pollInfo = new DataSet();
    DataTable dtSettingsInfo = new DataTable();
    PollCreation Pc = new PollCreation();
    //string resultaccess;
    DataTable tbAnswerOption = new DataTable();
    DataSet licInfo = new DataSet();
    //int respondentId;
    //int answerId = 0, questionType = 1;
    //string device;
    //string mobileBrand = "";
    //ListItem answerOption;
    //string platform = "", mobilePlatform = "";
    public String statusCode { get; set; }
    public String statusMessage { get; set; }
    public String IpAddress { get; set; }
    public String countryCode { get; set; }
    public String countryName { get; set; }
    public String regionName { get; set; }
    public String cityName { get; set; }
    public String zipCode { get; set; }
    public String latitude { get; set; }
    public String longitude { get; set; }
    public String timeZone { get; set; }
    // string globalurl = "http://insighto.com:8010";
    //string globalurl;
    //string foldername = "";
    //private int panlerowcount = 0;
    public int pollid;
    DataTable dtpollpageurls = new DataTable();
    
    protected void Page_Load(object sender, EventArgs e)
    {

        ////SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();
        ////SAVE_RETPATH = ConfigurationSettings.AppSettings["downloadchartsret"].ToString();

       //// hlnkcreatepoll.NavigateUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetUserPollBasicPageURL(), "surveyFlag=0");
        newpollpath = EncryptHelper.EncryptQuerystring("PollBasics.aspx", "surveyFlag=0");
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        dtsettings = Pc.getsuperPollThemes(userDetails.UserId);
       
        
        
        ////if (dtsettings.Rows.Count == 0)
        ////{
           
        ////    lblexistpoll.Visible = false;
        ////   // btn1.Visible = false;
        ////   // Button1.Visible = false;
        ////}
        ////else
        ////{
            
        ////    lblexistpoll.Visible = true;
        ////  //  btn1.Visible = true;
        //// //   Button1.Visible = true;
        ////}

       //// dtpollpageurls = Pc.getsuperPollpageurls();
        ////if (ViewState["panlerowcount"] == null)
        ////{
        ////    Table tb = new Table();

        ////    int rowcnt = Convert.ToInt32(Math.Round(Convert.ToDecimal(dtsettings.Rows.Count) / 2));
        ////    int noofcellsinarow = 0;

        ////    for (int nrows = 0; nrows < rowcnt; nrows++)
        ////    {
        ////        TableRow tr = new TableRow();

        ////        panlerowcount = 0;

        ////        DataRow[] row = dtsettings.Select();

        ////        for (int dr1=0;dr1<row.Length;dr1++)
        ////        {
                   
        ////            if ((panlerowcount < 2) && (noofcellsinarow < row.Length)) 
        ////            {

        ////                pollInfo = Pc.getPollInfo(Convert.ToInt32(row[noofcellsinarow]["fk_pollid"].ToString()));
        ////                //System.Web.UI.HtmlControls.HtmlGenericControl createmain = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
        ////                //System.Web.UI.HtmlControls.HtmlGenericControl createnew = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
        ////                //createnew.InnerHtml = "<div>" + pollInfo.Tables[0].Rows[0][7].ToString() + "</div><div class='clear'></div>";
        ////                //createmain.Controls.Add(createnew);

        ////                DataRow[] dr = dtpollpageurls.Select("fk_pollid=" + Convert.ToInt32(row[noofcellsinarow]["fk_pollid"].ToString()));

        ////                //Label lbl1 = new Label();
        ////                //lbl1.Text = pollInfo.Tables[0].Rows[0][7].ToString();


        ////                createimg(pollInfo.Tables[0].Rows[0][16].ToString(), Convert.ToInt32(row[noofcellsinarow]["fk_pollid"].ToString()));

        ////                HyperLink hlnkrepoll = new HyperLink();
        ////                hlnkrepoll.CssClass = "myreplacebtn";
        ////                hlnkrepoll.Text = "Replace This Poll";
        ////                hlnkrepoll.NavigateUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + Convert.ToInt32(row[noofcellsinarow]["fk_pollid"].ToString()) + "&polltype=superpoll");  



        ////                HtmlImage img1 = new HtmlImage();
        ////                img1.Visible = true;
        ////                img1.Width = 100;
        ////                img1.Height = 100;
        ////                img1.Src = "~/images/ExportImages/" + Convert.ToInt32(row[noofcellsinarow]["fk_pollid"].ToString()) + "pollimage.Png";

                        

        ////                img1.Attributes.Add("onmouseover", "showtrail(" + Convert.ToInt32(pollInfo.Tables[1].Rows[0]["iframewidth"].ToString()) + "," + Convert.ToInt32(pollInfo.Tables[1].Rows[0]["iframeheight"].ToString()) + ",'" + pollInfo.Tables[0].Rows[0][6].ToString() + "')");

        ////                img1.Attributes.Add("onmouseout", "hidetrail();");



        ////                //// Label lbl2 = new Label();
        ////                //// lbl2.Font.Size = 8;
        ////                ////// lbl2.Text = pollInfo.Tables[0].Rows[0][6].ToString();
        ////                //// if (dr.Length > 0)
        ////                //// {
        ////                ////     for (int i = 0; i < dr.Length; i++)
        ////                ////     {
        ////                ////      string pgurls  = dr[i][0].ToString();

        ////                ////      lbl2.Text = lbl2.Text + pgurls + "</br>";
        ////                ////     }
        ////                //// }
        ////                //// else
        ////                //// {
        ////                ////     lbl2.Text = ""; 
        ////                //// }

        ////                //// HyperLink hlnkrepoll = new HyperLink();
        ////                //// hlnkrepoll.CssClass = "myreplacebtn";
        ////                //// hlnkrepoll.Text = "Replace This Poll";
        ////                //// hlnkrepoll.NavigateUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + Convert.ToInt32(row["fk_pollid"].ToString()) + "&polltype=superpoll");  
        ////                TableCell tc = new TableCell();
        ////                tc.VerticalAlign = VerticalAlign.Top;

        ////                Table tb1 = new Table();
        ////                tb1.Width = 100;
        ////                tb1.Height = 100;
        ////                tb1.BorderWidth = 1;
        ////                tb1.CssClass = "mytable";
        ////                // tb1.BorderColor = System.Drawing.Color.FromName("#81BEF7");


        ////                //// TableRow tr1 = new TableRow();
        ////                //// TableCell tc1 = new TableCell();
        ////                //// tc1.HorizontalAlign = HorizontalAlign.Center;
        ////                //// tc1.VerticalAlign = VerticalAlign.Top;
        ////                //// tc1.Controls.Add(img1);
        ////                //// tr1.Cells.Add(tc1);

        ////                //// TableRow tr2 = new TableRow();
        ////                //// TableCell tc2 = new TableCell();
        ////                //// tc2.HorizontalAlign = HorizontalAlign.Center;
        ////                //// tc2.Controls.Add(lbl2);
        ////                //// tr2.Cells.Add(tc2);

        ////                TableRow tr3 = new TableRow();
        ////                TableCell tc3 = new TableCell();
        ////                tc3.HorizontalAlign = HorizontalAlign.Center;
        ////                tc3.Controls.Add(hlnkrepoll);
        ////                tr3.Cells.Add(tc3);

        ////                //// tb1.Rows.Add(tr1);
        ////                //// tb1.Rows.Add(tr2);
        ////                 tb1.Rows.Add(tr3);

        ////                 TableRow tr4 = new TableRow();
        ////                 TableCell tc4 = new TableCell();
        ////                 tc4.HorizontalAlign = HorizontalAlign.Center;
        ////                 tc4.Controls.Add(img1);
        ////                 tr4.Cells.Add(tc4);

        ////                 tb1.Rows.Add(tr3);
        ////                 tb1.Rows.Add(tr4);


        ////                tc.Controls.Add(tb1);
        ////                tr.Cells.Add(tc);
        ////                panlerowcount = panlerowcount + 1;
        ////                noofcellsinarow = noofcellsinarow + 1;
        ////            }
                  
        ////            //  plcCustomThemes.Controls.Add(createmain);
        ////        }
        ////        ViewState["panlerowcount"] = panlerowcount;
        ////        tb.Rows.Add(tr);
        ////        plcCustomThemes.Controls.Add(tb);
        ////    }
        ////} 
      }
   
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Table tb = new Table();
        //TableRow tr = new TableRow();
        //int panlerowcountnxt = Convert.ToInt32(ViewState["panlerowcount"]); 
        //plcCustomThemes.Controls.Clear();
        //int tdcnt = 0;
        //foreach (DataRow row in dtsettings.Rows)
        //{
        //    int j = Convert.ToInt32(row.ItemArray[74].ToString());
        //    if (tdcnt < 3)
        //    {
        //        if (j > panlerowcountnxt)
        //        {
        //            pollInfo = Pc.getPollInfo(Convert.ToInt32(row["fk_pollid"].ToString()));

        //            DataRow[] dr = dtpollpageurls.Select("fk_pollid=" + Convert.ToInt32(row["fk_pollid"].ToString()));

        //            Label lbl1 = new Label();
        //            lbl1.Text = pollInfo.Tables[0].Rows[0][7].ToString();

        //            Label lbl2 = new Label();
        //            lbl2.Text = pollInfo.Tables[0].Rows[0][6].ToString();

        //            HyperLink hlnkrepoll = new HyperLink();
        //            hlnkrepoll.CssClass = "myreplacebtn";
        //            hlnkrepoll.Text = "Replace This Poll";
        //            hlnkrepoll.NavigateUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + Convert.ToInt32(row["fk_pollid"].ToString()) + "&polltype=superpoll");    

        //            //Button btnSave = new Button();
        //            //btnSave.ID = "btnSave" + Convert.ToInt32(row["fk_pollid"].ToString());
        //            //btnSave.Text = "Replace This Poll";
        //            //btnSave.Click += new EventHandler(button_Click);
        //            //btnSave.CssClass = "myreplacebtn";

        //            TableCell tc = new TableCell();
        //            tc.VerticalAlign = VerticalAlign.Top;

        //            Table tb1 = new Table();
        //            tb1.Width = 298;
        //            tb1.Height = 390;
        //            tb1.BorderWidth = 1;
        //            tb1.CssClass = "mytable";

        //            TableRow tr1 = new TableRow();
        //            TableCell tc1 = new TableCell();
        //            tc1.HorizontalAlign = HorizontalAlign.Center;
        //            tc1.VerticalAlign = VerticalAlign.Top;
        //            tc1.Controls.Add(lbl1);
        //            tr1.Cells.Add(tc1);

        //            TableRow tr2 = new TableRow();
        //            TableCell tc2 = new TableCell();
        //            tc2.HorizontalAlign = HorizontalAlign.Center;
        //            tc2.Controls.Add(lbl2);
        //            tr2.Cells.Add(tc2);

        //            TableRow tr3 = new TableRow();
        //            TableCell tc3 = new TableCell();
        //            tc3.HorizontalAlign = HorizontalAlign.Center;
        //            tc3.Controls.Add(hlnkrepoll);
        //            tr3.Cells.Add(tc3);

        //            tb1.Rows.Add(tr1);
        //            tb1.Rows.Add(tr2);
        //            tb1.Rows.Add(tr3);

        //            tc.Controls.Add(tb1);

        //            tr.Cells.Add(tc);
        //            panlerowcountnxt = panlerowcountnxt + 1;
        //            tdcnt = tdcnt + 1;
        //        }
        //    }
           
        //    //  plcCustomThemes.Controls.Add(createmain);
        //}
        //panlerowcount = panlerowcountnxt;
        //ViewState["panlerowcount"] = panlerowcount;
        //tb.Rows.Add(tr);
        //plcCustomThemes.Controls.Add(tb);
       
    }
    
    protected void btn1_Click(object sender, EventArgs e)
    {
        //Table tb = new Table();
        //TableRow tr = new TableRow();
        //int panlerowcountprv = Convert.ToInt32(ViewState["panlerowcount"])+1;
        //plcCustomThemes.Controls.Clear();
        //int tdcnt1 = 0;
        //int j1 = Convert.ToInt32(ViewState["panlerowcount"]) - 3;
        //foreach (DataRow row in dtsettings.Rows)
        //{
        //    int j = Convert.ToInt32(row.ItemArray[74].ToString());
           
        //    if (tdcnt1 < 3)
        //    {
        //        if ((j <= panlerowcountprv) && (j >= j1) )
        //        {
        //            pollInfo = Pc.getPollInfo(Convert.ToInt32(row["fk_pollid"].ToString()));

        //            DataRow[] dr = dtpollpageurls.Select("fk_pollid=" + Convert.ToInt32(row["fk_pollid"].ToString()));

        //            Label lbl1 = new Label();
        //            lbl1.Text = pollInfo.Tables[0].Rows[0][7].ToString();

        //            Label lbl2 = new Label();
        //            lbl2.Text = pollInfo.Tables[0].Rows[0][6].ToString();

        //            HyperLink hlnkrepoll = new HyperLink();
        //            hlnkrepoll.CssClass = "myreplacebtn";
        //            hlnkrepoll.Text = "Replace This Poll";
        //            hlnkrepoll.NavigateUrl = EncryptHelper.EncryptQuerystring("CreatePoll.aspx", "PollId=" + Convert.ToInt32(row["fk_pollid"].ToString()) + "&polltype=superpoll");  

        //            //Button btnSave = new Button();
        //            //btnSave.ID = "btnSave" + Convert.ToInt32(row["fk_pollid"].ToString());
        //            //btnSave.Text = "Replace This Poll";
        //            //btnSave.Click += new EventHandler(button_Click);
        //            //btnSave.CssClass = "myreplacebtn";

        //            TableCell tc = new TableCell();
        //            tc.VerticalAlign = VerticalAlign.Top;

        //            Table tb1 = new Table();
        //            tb1.Width = 298;
        //            tb1.Height = 390;
        //            tb1.BorderWidth = 1;
        //            tb1.CssClass = "mytable";

        //            TableRow tr1 = new TableRow();
        //            TableCell tc1 = new TableCell();
        //            tc1.HorizontalAlign = HorizontalAlign.Center;
        //            tc1.VerticalAlign = VerticalAlign.Top;
        //            tc1.Controls.Add(lbl1);
        //            tr1.Cells.Add(tc1);

        //            TableRow tr2 = new TableRow();
        //            TableCell tc2 = new TableCell();
        //            tc2.HorizontalAlign = HorizontalAlign.Center;
        //            tc2.Controls.Add(lbl2);
        //            tr2.Cells.Add(tc2);

        //            TableRow tr3 = new TableRow();
        //            TableCell tc3 = new TableCell();
        //            tc3.HorizontalAlign = HorizontalAlign.Center;
        //            tc3.Controls.Add(hlnkrepoll);
        //            tr3.Cells.Add(tc3);

        //            tb1.Rows.Add(tr1);
        //            tb1.Rows.Add(tr2);
        //            tb1.Rows.Add(tr3);

        //            tc.Controls.Add(tb1);
        //            tr.Cells.Add(tc);
        //            panlerowcountprv = panlerowcountprv - 1;
        //            tdcnt1 = tdcnt1 + 1;
        //        }
        //    }

        //    //  plcCustomThemes.Controls.Add(createmain);
        //}
        //panlerowcount = panlerowcountprv;
        //ViewState["panlerowcount"] = panlerowcount;

        //tb.Rows.Add(tr);
        //plcCustomThemes.Controls.Add(tb);

        
    }

    private void createimg(string qtext,int pollid)
    {
       
        //string Text = qtext;
        //Color FontColor = Color.Blue;
        //Color BackColor = Color.White;
        //String FontName = "Times New Roman";
        //int FontSize = 14;
        //int Height = 100;
        //int Width = 100;
        //Bitmap bitmap = new Bitmap(Width, Height);
        //Graphics graphics = Graphics.FromImage(bitmap);
        //Color color = Color.Gray; ;
        //Font font = new Font(FontName, FontSize);
        //PointF point = new PointF(5.0F, 5.0F);
        //SolidBrush BrushForeColor = new SolidBrush(FontColor);
        //SolidBrush BrushBackColor = new SolidBrush(BackColor);
        //Pen BorderPen = new Pen(color);
        //Rectangle displayRectangle = new Rectangle(new Point(0, 0), new Size(Width - 1, Height - 1));
        //graphics.FillRectangle(BrushBackColor, displayRectangle);
        //graphics.DrawRectangle(BorderPen, displayRectangle);
        //StringFormat format1 = new StringFormat(StringFormatFlags.NoClip);
        //StringFormat format2 = new StringFormat(format1);
        ////Draw text string using the text format
        //graphics.DrawString(Text, font, Brushes.Blue, (RectangleF)displayRectangle, format2);
        //bitmap.Save(SAVE_PATH+pollid+"pollimage.Png", System.Drawing.Imaging.ImageFormat.Png);
        ////Response.ContentType = "image/jpeg";
        ////bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
    }
    
    [WebMethod]
    public static string getPollembedcode(int pollid)
    {
        string rootURL = ConfigurationManager.AppSettings["RootURL"].ToString();
        PollCreation Pc1 = new PollCreation();
        DataSet dtsettingspoll = Pc1.getPollInfo(pollid);
        string emblink = "";
        //emblink=  dtsettingspoll.Tables[0].Rows[0][6].ToString();
        //emblink = emblink.Replace("http://s.insighto.com", "https://bit.ly");
        string EmbedUrl = "";

        if (dtsettingspoll.Tables[0].Rows[0]["parentpollId"].ToString() != "")
        {
            EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + Convert.ToInt32(dtsettingspoll.Tables[0].Rows[0]["parentpollId"].ToString()) + "&Embed=" + "Emb");
        }
        else
        {
            EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollid.ToString() + "&Embed=" + "Emb");
        }

        //  string EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetPollRespondenURL(), "PollId=" + pollId + "&Embed=" + "Emb");
        emblink = rootURL + EmbedUrl;
        
        string pollwidth = dtsettingspoll.Tables[1].Rows[0]["iframewidth"].ToString();
        string pollheight = dtsettingspoll.Tables[1].Rows[0]["iframeheight"].ToString();

        string dtpoll = emblink + "-" + pollwidth + "-" + pollheight;
        return dtpoll;
    }
}