﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using Resources;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;
using System.Net;
using System.Web.Routing;
using System.Web.UI;

public partial class new_polls_registration : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    private string licenseType = "";
    private int userid;
    string country;
    //string strsource;
    //string strterm;
    SurveyCore surcore = new SurveyCore();
    PollCreation Pc = new PollCreation();

    string countryName = "";
    public string AnnualProf;
    public string AnnualBus;
    public string AnnualPub;

    protected void Page_Load(object sender, EventArgs e)
    {
        string ipAddress = "";
        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
        {
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        }

        countryName = Utilities.GetCountryName(ipAddress);
        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(countryName);

        countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
    
        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (typeHt.Contains("LicenseType"))
                licenseType = typeHt["LicenseType"].ToString();
            if (typeHt.Contains("userid"))
                userid = Convert.ToInt32(typeHt["userid"].ToString());



            if (userid != 0)
            {
                txtName.Visible = false;
                lblvalidmsg.Visible = false;
                txtpwd.Visible = false;
                lblvalidmsgpwd.Visible = false;
                txtEmail.Visible = false;
                lblvalidmsgemail.Visible = false;
                txtcpwd.Visible = false;
                lblvalidmsgrepwd.Visible = false;
            }
            else
            {
                txtName.Visible = true;
                lblvalidmsg.Visible = true;
                txtpwd.Visible = true;
                lblvalidmsgpwd.Visible = true;
                txtEmail.Visible = true;
                lblvalidmsgemail.Visible = true;
                txtcpwd.Visible = true;
                lblvalidmsgrepwd.Visible = true;
            }
        }
    }

    public bool IsPasswordStrong(string p)
    {
        if (string.IsNullOrEmpty(p))
            return false;
        else
        {
            var regex = new Regex("^.{6,16}$");
            return regex.IsMatch(p) && !p.EndsWith(".");
        }
    }

    public bool Isvalidname(string a)
    {
        if (string.IsNullOrEmpty(a))
            return false;
        else
        {
            var regex = new Regex("^[a-zA-Z ]*$");
            return regex.IsMatch(a) && !a.EndsWith(".");
        }

    }

    public bool IsValidEmailAddress(string s)
    {
        if (string.IsNullOrEmpty(s))
            return false;
        else
        {
            var regex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            return regex.IsMatch(s) && !s.EndsWith(".");
        }
    }

    protected void btnSFT_Click(object sender, EventArgs e)
    {
        string val = "";
        if (ckbFreeTrial.Checked)
        {
            licenseType = "Publisher_freetrial";
        }
        else {
            licenseType = "FREE";
        }
        if (userid != 0)
        {

            Pc.UpdateUserSubscriptionType(2, "Poll", userid);
            Pc.UpdatePollLicenseType(userid, licenseType, 2);
            Pc.UpdateEmailtoSurveyUser(userid);
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyPollPageURL(), "UserId=" + userid));
            //  Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserWelcomePageURL(), "userId=" + userid));

        }
        else
        {
            if (txtName.Text == "")
            {
                lblvalidmsg.Text = "Please enter Name";
                lblvalidmsgemail.InnerHtml = "";
                lblvalidmsgpwd.InnerHtml = "";
                lblvalidmsgrepwd.InnerHtml = "";
                lblvalidmsg.Visible = true;
                return;
            }
            else if (!Isvalidname(txtName.Text))
            {
                lblvalidmsg.Text = "Please enter alphabets only";
                lblvalidmsgemail.InnerHtml = "";
                lblvalidmsgpwd.InnerHtml = "";
                lblvalidmsgrepwd.InnerHtml = "";
                lblvalidmsg.Visible = true;
                return;
            }
            else if (txtEmail.Value == "")
            {
                lblvalidmsgemail.InnerHtml = "Please enter email id";
                lblvalidmsg.Text = "";
                lblvalidmsgpwd.InnerHtml = "";
                lblvalidmsgrepwd.InnerHtml = "";
                lblvalidmsgemail.Visible = true;
                return;
            }
            else if (!IsValidEmailAddress(txtEmail.Value))
            {
                lblvalidmsgemail.InnerHtml = "Please enter valid Email";
                lblvalidmsg.Text = "";
                lblvalidmsgpwd.InnerHtml = "";
                lblvalidmsgrepwd.InnerHtml = "";
                lblvalidmsgemail.Visible = true;
                return;
            }
            else if (!IsPasswordStrong(txtpwd.Value))
            {
                lblvalidmsgpwd.InnerHtml = "Password should contain 6-16 characters";
                lblvalidmsgemail.InnerHtml = "";
                lblvalidmsg.Text = "";
                lblvalidmsgrepwd.InnerHtml = "";
                lblvalidmsgpwd.Visible = true;
                return;
            }
            else if (lblvalidmsgpwd.InnerHtml == "Password")
            {
                lblvalidmsgpwd.InnerHtml = "Enter Password";
                lblvalidmsgemail.InnerHtml = "";
                lblvalidmsg.Text = "";
                lblvalidmsgrepwd.InnerHtml = "";
                lblvalidmsgpwd.Visible = true;
                return;
            }
            else if (txtcpwd.Value == "Re-typePassword")
            {
                lblvalidmsgrepwd.InnerHtml = "Enter Re-typePassword";
                lblvalidmsg.Text = "";
                lblvalidmsgemail.InnerHtml = "";
                lblvalidmsgpwd.InnerHtml = "";
                lblvalidmsgrepwd.Visible = true;
                return;
            }
            else if (txtpwd.Value != txtcpwd.Value)
            {
                lblvalidmsgrepwd.InnerHtml = "Re-type Password not matching";
                lblvalidmsg.Text = "";
                lblvalidmsgemail.InnerHtml = "";
                lblvalidmsgpwd.InnerHtml = "";
                lblvalidmsgrepwd.Visible = true;
                return;
            }
            else
            {
                var userService = new UsersService();

                string ipAddress = string.Empty;
                if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
                }

                var user = userService.RegisterFreeUserForPolls(txtName.Text.Trim(), string.Empty, txtEmail.Value.Trim(), string.Empty, 0, ipAddress, txtpwd.Value.Trim());
                country = Utilities.GetCountryName(ipAddress);
                ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
                ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
                country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
                if (user == null)
                {
                    lblErrorMsg.Text = "You have already signed up! Just login";
                    lblErrorMsg.Visible = true;
                }
                else
                {
                }

                if (user == null)
                {
                    lblErrorMsg.Text = "You have already signed up! Just login";
                    lblErrorMsg.Visible = true;
                }
                else
                {
                    ServiceFactory.GetService<SessionStateService>().SaveUserToSession(user);
                    Pc.UpdatePollLicenseTypeOnSignUp(user.USERID, licenseType, 2);
                    // Response.Redirect(EncryptHelper.EncryptQuerystring("MyPolls.aspx", "UserId=" + user.USERID));
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetPollUserWelcomePageURL(), "userId=" + user.USERID));
                }
            }
        }
        //}
    }

}