﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FreeSubscription.aspx.cs" Inherits="Polls_FreeSubscription" MasterPageFile="~/Polls/Home.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <!--=== Content Part ===-->
    <div class="bg-grey content-sm pricing-page-intro">
        <div class="container text-center">
            <h2 class="title-v2 title-center">INSIGHTO POLLS PERSONAL SUBSCRIPTION PLAN</h2>
            <p class="space-lg-hor" style="font-size: 15px;">Ideal for personal websites and it’s Free!</p><br>
        </div>
    </div>

    <!--=== Content Part ===-->
    <div id="plans" class="container content">
        <div style="float:left;width:650px;">
            <br />
            <div style="font-size:18px;color:#7F7F7F;margin-left:-10px;font-family:Arial;margin-top:0px;">Get Insighto Polls’ Personal Subscription Plan and get the following benefits absolutely free of cost</div>
            <br />
            <div>
                <ul>
                    <li style="font-size:15px;color:#7F7F7F;margin-left:0px;font-family:Arial;margin-top:0px;list-style:circle;">Ability to create text, image and video polls</li>
                    <li style="font-size:15px;color:#7F7F7F;margin-left:0px;font-family:Arial;margin-top:0px;list-style:circle;">Unlimited number of polls</li>
                    <li style="font-size:15px;color:#7F7F7F;margin-left:0px;font-family:Arial;margin-top:0px;list-style:circle;">Live preview while creating polls</li>
                    <li style="font-size:15px;color:#7F7F7F;margin-left:0px;font-family:Arial;margin-top:0px;list-style:circle;">Public results</li>
                    <li style="font-size:15px;color:#7F7F7F;margin-left:0px;font-family:Arial;margin-top:0px;list-style:circle;">500 responses / month</li>
                </ul>
            </div>

        </div>
        <div style="float:left;">
            <div style="font-size:14px;color:#7F7F7F;margin-left:60px;font-family:Arial;font-weight:bold">Experience Insighto Polls Free. Upgrade Anytime!</div>
            <div class="reg-page" style="margin-top:-20px;">
                    <div>
                    <asp:Label ID="lblErrorMsg" Visible="false" runat="server"></asp:Label>
                    <asp:TextBox ID="txtName" class="form-control" runat="server"  placeholder="Name*" ></asp:TextBox>
                    <asp:Label ID="lblvalidmsg" runat="server" style="color:Red;font-family:Arial;font-size:12px; font-weight:normal"></asp:Label> 
                    <div class=" margin-bottom-15"></div>
                     <input type="text" placeholder="Email Id*" class="form-control" runat="server" id="txtEmail"/>
                     <label id="lblvalidmsgemail" runat="server" style="color:Red;font-family:Arial;font-size:12px; font-weight:normal;"></label>
                     <div class=" margin-bottom-15"></div>
                    <input type="password" placeholder="Password*" class="form-control" runat="server" id="txtpwd"/>
                    <label id="lblvalidmsgpwd" runat="server" style="color:Red;font-family:Arial;font-size:12px;font-weight:normal "></label>
                    <div class=" margin-bottom-15"></div>
                    <input type="password" placeholder="Confirm Password*" class="form-control margin-bottom-5" runat="server" id="txtcpwd"/>
                       <label id="lblvalidmsgrepwd" runat="server" style="color:Red;font-family:Arial;font-size:12px;font-weight:normal"></label>
                    <div class="margin-bottom-15"></div>
					<!--<select name="Choose Your Plan*" id="selPlan" style="color:#9999a8;" class="form-control" runat="server">
					</select>
                    -->
                  
                    <div class="margin-bottom-15"></div>
                    <asp:Button ID="btnSFT" OnClick="btnSFT_Click" CausesValidation="false" runat="server" style="width:100%;font-size:22px;font-weight:bold;" Text="Start Free Trial" class="btn-u"  />
                    </div>
                                   <div class="row">
                         
                            <label class="checkbox" style="text-align:center;margin-top:0px;">
                               
                               <span style="padding-left:0px;font-weight:none;font-size:11px;">* No Credit Card required</span>  <span style="padding-left:40px;font-weight:none;font-size:11px;">* All fields are mandatory</span>

                            </label>     
                        
                    </div>
                    <div >By signing up, you agree to the <a href="http://www.insighto.com/In/termsandconditions.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Terms & Conditions</a> and the <a href="http://www.insighto.com/In/privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy Policy</a></div>
                </div>
                
                                   <div class="row">
                         
                        
                    </div>
      

    </div><!--/container-->     
        </div>
    <!--=== End Content Part ===-->

</asp:Content>