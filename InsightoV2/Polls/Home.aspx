<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="new_polls_Home" MasterPageFile="~/Home.master"%>
<%@ Register TagPrefix="uc" TagName="PollsHeader" Src="~/UserControls/PollsHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc:PollsHeader ID="PollsHeader" runat="server" />
<div>
  <div class="interactive-slider-v1 img-v3">
    <div class="container" style=" text-align: center;">
      <div class="group-quote">
        <h2 style="text-transform:none !important; color:#FFF900 !important; font-size:38px !important; text-shadow:2px 2px 0px rgba(80, 80, 80, 0.25) !important;"> <span>Engage Better with Your <u class="slidingVertical-top">
      <div class="slidingVertical">
      <span>People</span>
      <span>Employees</span>
      <span>Members</span>
      <span>Visitors</span>
      <span>Audience</span>
    </div>
      </u>  </span> </h2>
        <h3 style="text-transform:none !important; color:#fff !important; text-shadow:2px 2px 0px rgba(80, 80, 80, 0.25) !important; padding-top: 18px; font-size: 26px; font-weight: bold;">Simple. Smart. Polls Software to Know their Minds!</h3>
        <div class="hero-banner">
          <a href="registration.aspx" class="btn-u"><b>Create Your Free Account</b></a>
        </div>
      </div>
    </div>
  </div>

  <!--=== Content Part ===-->
  <div class="purchase" id="mantra">
    <div class="container">
      <div class="row">
        <div class="col-md-12 animated fadeInLeft" align="center"> <span>Human Opinions - Emotions - Views - Reviews - Feedback</span></div>
      </div>
      <div class="row">
      	<div class="col-md-4"></div>
        <div class="col-md-4">
        	<div class="arrow-down">
        		<a href="#" data-scroll-nav="1"><img class="wow bounce" src="../assets/img/down-arrow1.png" alt="" width="48" height="39"></a>
        	</div>
        </div>
        <div class="col-md-4"></div>
      </div>
    </div>
  </div>

  <br>
    <div class="parallax-counter-v2 parallaxBg1" style="padding: 36px 0 60px;">
      <div class="container" data-scroll-index="1">

    <div class="row">
      <div class="col-md-12">
      <div class="text-center">
          <!--<h2 class="title-v2 title-center" style="color:#1035A2 !important;">CREATE A POLL AND SEND IT AS <a href="#">WEBLINK</a> OR EMBED IT OR SLIDE IT IN</h2>-->
          <h2 class="title-v2-new title-center"> CREATE VIRAL POLLS THAT CAN POWER YOUR ENGAGEMENT  </h2>
<h4 class="text-center" style="    text-transform: initial;"> Build and Manage Multiple Polls . Quick and Easy.</h4><br>
          
          
          
          
     <ul class="row list-row">
        <li class="col-md-4 col-sm-4 col-xs-12 md-margin-bottom-30">
          <div class="counters rounded">
            <h5 style="font-weight: bold;">Three Types of Polls</h5>
            <h5 style="color:#954f9d; font-weight: bold;">Text - Image - Video</h5>
          </div>
        </li>
        <li class="col-md-4 col-sm-4 col-xs-12 md-margin-bottom-30">
          <div class="counters rounded">
            <h5 style="font-weight: bold;">Three Ways to Display</h5>
            <h5 style="color:#954f9d; font-weight: bold;">Web link - Embed - Slider </h5>
          </div>
        </li>
        <li class="col-md-4 col-sm-4 col-xs-12 md-margin-bottom-30">
          <div class="counters rounded">
              <h5 style="font-weight: bold;">And a New method to </h5>
              <h5 style="color:#954f9d; font-weight: bold;"> Generate Revenue </h5>
          </div>
        </li>
      </ul>
          
          
          
          
        </div>
      </div>
    </div>
    </div>
    </div>
    
    
    
  <!--Embed Poll Section-->
  <div class="container" data-scroll-index="1" style="padding: 25px 0;">

    <div class="row" data-scroll-index="4">
    <div class="col-md-2"></div>
    <div class="col-md-8"><h4 style="color:#1035A2 !important; font-size:28px !important;" align="center">
    <img src="../assets/img/mockup/embed-icon3.png" alt="" width="32" height="32" align="absmiddle"> Embed Poll </h4></div>
    <div class="col-md-2"></div>
    </div>
    <div class="row">
    <div class="text-center"><h2 class="title-v2 title-center">RUN A POLL AS PART OF YOUR WEBPAGE!</p>
          <p class="text-center" style="font-size:16px !important;     text-transform: initial;">Take the Poll code and embed it on any Page/ Section/ Location of your Website. <br> Customize the look and feel to integrate it with the webpage.</p>
    </div>
    </div>
  </div>
  <div id="features" class="container content-sm features-content">
    <div class="row service-box-v1">
      <div class="col-md-4 col-sm-6 no-margin">
        <div class="service-block service-block-default no-margin-bottom box-height" style="height:100%;">
          <div class="row" style="margin-bottom:20px;">
          	<div class="col-md-6"><i class="fa fa-font embed-item-title-icon"></i><span class="embed-item-title embed-label">TEXT</span></div>
            <div class="col-md-6"><span class="embed-label-right">Live Poll. Vote Now</span></div>
          </div>
		    <iframe id='IframeLaunch1' height='455' width='300'  scrolling='auto' src='https://www.insighto.com/Polls/PollRespondent.aspx?key=ahXnbZlW8onf1g9yxfBId9dgEC6Nzsm5%2b7CQOnNPdr%2bvzbvs9vxANQ%3d%3d' frameborder='0'   style='-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px; -moz-box-shadow: 4px 4px 14px #000; -webkit-box-shadow: 4px 4px 14px #000; box-shadow: 4px 4px 14px #000;'  ></iframe>

        </div>
      </div>
      <div class="col-md-4 col-sm-6 md-margin-bottom-40">
        <div class="service-block service-block-default no-margin-bottom box-height" style="height:100%;">
          <div class="row" style="margin-bottom:20px;">
          	<div class="col-md-6"><i class="fa fa-image embed-item-title-icon"></i><span class="embed-item-title embed-label">IMAGE</span></div>
            <div class="col-md-6"><span class="embed-label-right">Live Poll. Vote Now</span></div>
          </div>
            <iframe id='IframeLaunch2' height='464' width='300'  scrolling='auto' src='https://www.insighto.com/Polls/PollRespondent.aspx?key=ahXnbZlW8omRn5KQKcyksDZDO%2f6014hHGk30x8XftvrohI9RIcH2Tg%3d%3d' frameborder='0'   style='-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px; -moz-box-shadow: 4px 4px 14px #000; -webkit-box-shadow: 4px 4px 14px #000; box-shadow: 4px 4px 14px #000;'  ></iframe>
        </div>
      </div>
      <div class="col-md-4 col-sm-12">
        <div class="service-block service-block-default no-margin-bottom box-height" style="height:100%;">
          <div class="row" style="margin-bottom:20px;">
          	<div class="col-md-6">
            <i class="fa fa-video-camera embed-item-title-icon"></i><span class="embed-item-title embed-label">VIDEO</span></div>
            <div class="col-md-6"><span class="embed-label-right">Live Poll. Vote Now</span></div>
        </div>
            <iframe id='IframeLaunch3' height='470' width='300'  scrolling='auto' src='https://www.insighto.com/Polls/PollRespondent.aspx?key=ahXnbZlW8om2EF11ycROa%2f4qd%2f9mc3IwRk4tiQMDoZBQA1atbJNlEw%3d%3d' frameborder='0'   style='-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px; -moz-box-shadow: 4px 4px 14px #000; -webkit-box-shadow: 4px 4px 14px #000; box-shadow: 4px 4px 14px #000;'  ></iframe>

        </div>
        </div>
    </div>
  </div>
  <!--End Embed Poll Section-->

  <!--Slider Poll Section-->
  <div class="one-page" data-scroll-index="3">
    <div class="one-page-inner one-blue slider-details" id="slider-poll-example">
      <div class="container text-center margin-bottom-5">
      <div class="row">
      	<div class="col-md-2"></div>
        <div class="col-md-8"><h4 style="color:#FFF !important; font-size:28px !important;">
        <img src="../assets/img/mockup/social-column.png" alt="" width="32" height="32" align="absmiddle"> Slider Poll </h4></div>
        <div class="col-md-2"></div>
      </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <div class="text-center margin-bottom-50">
              <h2 class="title-v2 title-center">LET YOUR POLL MAKE A DRAMATIC ENTRY!</h2>
            </div>
          </div>
          <div class="col-md-2">
            <!--<div style="float:right; margin-top: 12px;"><a href="#" class="btn btn-link" style="text-decoration:underline !important; color:#FFF !important;"> View a Slider Poll</a></div>-->
          </div>
        </div>
        <!--<h2 class="title-center">Slider Poll</h2>-->

      </div>
      <div class="row">
        <div class="container">
          <div class="col-md-5"> <img src="../assets/img/slider_illus.png" class="img-responsive" alt=""> </div>
          <div class="col-md-7">
            <!--<div>
              <h4 style="color:#FFF !important;"> # Create a Slider Poll</h4>
              <p> &bull; Build a Poll and configure it easily to slide into any page of your website, with complete control on design and frequency. </p>
            </div>-->
            <div>
              <h4 style="color:#FFF !important; text-transform:none !important;"> Wow your people !</h4>
              <h4 style="color:#FFF !important; text-transform:none !important;"> By having your poll slide from different directions of your web page</h4>
              <p> &bull; No need to have dedicated space for Polls on your webpage </p>
            </div>
            <div>
              <h4 style="color:#FFF !important; text-transform:none !important;"> Serve the poll to your target audience based on </h4>
              <p> <i class="fa fa-check"></i> Country / City <br>
                <i class="fa fa-check"></i> Device <br>
                <i class="fa fa-check"></i> Frequency of Visits to site </p>
            </div>
            <div style="padding:20px 0px 0px 10px;"><a class="btn-u btn-purple one-page-btn" href="javascript:void(0);" onclick="ShowPollWithDetails('300','454','pprev_BL','pSliderBL','https://www.insighto.com/Polls/PollRespondent.aspx?key=ahXnbZlW8omb7CDompP08Gz38u8H1v%2buy9Bq5jy%2bvzr1GBS%2f%2bBQkHg%3d%3d',2500,'1546');">Show me a Slider Poll</a></div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <!--End Slider Poll Section-->

  <!--Weblink Poll Section-->
  <div class="bg-grey1" data-scroll-index="2">
  <div class="container">
    <div id="features" class="content-sm features-content margin-top-20">
    <div class="row">
      	<div class="col-md-2"></div>
        <div class="col-md-8"><h4 style="color:#1035A2 !important; font-size:28px !important;" align="center">
        <img src="../assets/img/mockup/weblink-icon.png" alt="" width="32" height="32" align="absmiddle"> Weblink Poll </h4></div>
        <div class="col-md-2"></div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="text-center">
            <div class="col-md-1"></div>
            <div class="col-md-10">
              <h2 class="title-v2 title-center" style="color:#1035A2 !important;">BROADCAST YOUR POLL TO YOUR FRIENDS, FANS & FOLLOWERS!</h2>
              <p style="font-size:16px !important;">Choose the Poll Type you would like - Plain Text or Image/Picture based or Video based Poll
                </p>
            </div>
            <div class="col-md-1"></div>
            <!--<div style="float:right; margin-top: 12px;"><a href="#" class="btn btn-link" style="text-decoration:underline !important;"> View a Weblink Poll</a></div>-->
          </div>
        </div>
      </div>
      <div class="row weblink-para margin-top-20" style="text-align:left !important;">
        <div class="col-md-6">
          <div class="row">
          	<div class="social-icon-pos">
            <div class="col-md-6">
                <div><img src="../assets/img/mockup/small-icon-mail.png"> E-mail</div><br>

                <div><img src="../assets/img/mockup/small-icon-whatsapp.png"> Whatsapp</div>
            </div>
            <div class="col-md-6">
                <div><img src="../assets/img/mockup/small-icon-facebook.png"> Facebook</div><br>
                <div><img src="../assets/img/mockup/small-icon-twitter.png"> Twitter</div>
            </div>


          </div>
              <!--<ul class="small-icon" style="margin-top:15px !important;">
                <li><img src="../assets/img/mockup/small-icon-mail.png">E-mail</li>
                <li><img src="../assets/img/mockup/small-icon-whatsapp.png">Whatsapp</li>
                <li><img src="../assets/img/mockup/small-icon-facebook.png">Facebook</li>
                <li><img src="../assets/img/mockup/small-icon-twitter.png">Twitter</li>
              </ul>-->
          </div>

          <div class="row">
          </div>
          <div class="row">
          	<p style="font-size:16px !important;">Your poll will get displayed as per the device's dimensions!</p>
          </div>
          <div class="row">
          	<div style="padding:20px 0px 0px 10px;"><a class="btn-u btn-purple one-page-btn" href="http://s.insighto.com/1PYgjnf" target="_blank">Show me a Weblink Poll</a></div>
          </div>
         </div>
        <div class="col-md-6"> <img src="../assets/img/device-mockups-1.png" class="img-responsive" alt=""> </div>
      </div>
      <!--/end row-->
    </div>
  </div>
  </div>

  <!--End Weblink Poll Section-->
  <div class="parallax-counter-v2 parallaxBg1" style="background-position: 50% 32px;">
    <div class="container">
      <ul class="row list-row">
        <li class="col-md-3 col-sm-6 col-xs-12 md-margin-bottom-30">
          <div class="counters rounded">
            <h5 style="color:#1035A2 !important;">USERS FROM</h5>
            <span class="counter">44</span>
            <h4 style="color:#954f9d;">Countries</h4>
          </div>
        </li>
        <li class="col-md-3 col-sm-6 col-xs-12 md-margin-bottom-30">
          <div class="counters rounded">
            <h5 style="color:#1035A2 !important;">USED IN</h5>
            <span class="counter">16</span>
            <h4 style="color:#954f9d;">LANGUAGES</h4>
          </div>
        </li>
        <li class="col-md-3 col-sm-6 col-xs-12 sm-margin-bottom-30">
          <div class="counters rounded">
            <h5 style="color:#1035A2 !important;">GENERATED</h5>
            <span class="counter">5.2</span> Billion
            <h4 style="color:#954f9d;">INSIGHTS</h4>
          </div>
        </li>
        <li class="col-md-3 col-sm-6 col-xs-12">
          <div class="counters rounded">
            <h5 style="color:#1035A2 !important;">FROM</h5>
            <span class="counter">78</span>
            <h4 style="color:#954f9d;">NATIONS</h4>
          </div>
        </li>
      </ul>
    </div>
  </div>

  <div class="">
  <div id="features" class="container content-sm features-content" data-scroll-index="1">
    <div class="text-center margin-bottom-50">
      <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Highlights</h2>
    </div>
    <div class="row category margin-bottom-20">
      <div class="col-md-6 col-sm-6">
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"> <i class="wow wobble icon-ask"></i><!--<i class="icon-custom glyphicon glyphicon-export"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Simple &amp; Easy ways to Ask Questions - Get Answers</b></h3>
            <p class="para-updated">Build your questionnaire in few minutes<br>
              Without bothering your tech team</p>
          </div>
        </div>
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-question1"></i>
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Several Options to Send Questions</b></h3>
            <p class="para-updated">Post your Questions through multiple ways <br>
              Weblink, Email, Embed, Facebook, Whatsapp </p>
          </div>
        </div>
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-ans"></i> <!--<i class="icon-custom icon-sm rounded-x icon-bg-blue fa fa-bell-o"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Get to see Answers Instantly - in Real-time</b></h3>
            <p class="para-updated">View responses as soon as the respondent fills in <br>
              With built-in basic analytics</p>
          </div>
        </div>


      </div>
      <div class="col-md-6 col-sm-6 md-margin-bottom-40">

        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-chart"></i> <!--<i class="icon-custom icon-sm rounded-x icon-bg-yellow fa fa-thumbs-o-up"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Intuitive and Easy-to-read Charts</b></h3>
            <p class="para-updated">Clearly demonstrate your insights <br>
              With rich animated charts and analytics</p>
          </div>
        </div>

        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-export"></i> <!--<i class="icon-custom icon-sm rounded-x icon-bg-red fa fa-hdd-o"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Export Reports for Further Data Crunching</b></h3>
            <p class="para-updated">Including raw data for further analysis or presentations <br>
              Export to MS Excel, Powerpoint, Word or PDF</p>
          </div>
        </div>
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"> <i class="wow wobble icon-dollar"></i>
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Generate Income while collecting Opinions</b></h3>
            <p class="para-updated"> Monetise your Insight generation efforts <br>
              Add sponsorships and leverage space selling </p>
          </div>
        </div>

      </div>
    </div>

  </div>
  </div>
 <!-- ------------- Multi Language -------------- -->
  <div class="highlights-section content-sm pricing-page-intro" data-scroll-index='4'>
    <div class="container text-center">
    <div class="row">
    	<div class="col-md-12">
            <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Reach Out to your Multi-Language audience</h2>

            <p class="space-lg-hor para-updated" style="font-size: 15px;">
            Create questions in a language your people are comfortable with. <br> Just key in or copy the language content.
            </p>

        </div>
    </div>
      </div>
  </div>

 <!-- ------------- Multi Language -------------- -->

  <div class="one-page">
    <div class="one-page-inner one-blue-light">
      <div class="container one-blue-light">
        <div class="text-center margin-bottom-50">
          <!--<h2 class="title-v2 title-center" style="color:#1035A2 !important;">Serving Professionals and Business Owners since Five years</h2>-->
          <h2 class="title-v2 title-center" style="color:#1035A2 !important;">ENHANCING ENGAGEMENT EFFORTS OF PROFESSIONALS AND BUSINESS OWNERS</h2>
          <!--<p class="space-lg-hor para-updated">Insighto assists the Owners, Managers and Executives in any function or department -
            at any type of Organisation  to get insights from all the people who are critical for their success and growth</p>-->
        </div>

        <!-- Service Blcoks -->
        <div class="row service-box-v1 margin-bottom-40">
          <div class="col-md-3 col-sm-12 md-margin-bottom-40">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x icon glyphicon glyphicon-bullhorn"></i>
              <h2 class="heading-sm2">HR Professionals</h2>
              <p class="para-updated">Polls for constant engagement with employees</p>
              <ul class="list-unstyled">
                <li>Poll of the Day</li>
                <li>Gauge Moods</li>
                <li>Inject Humor & Fun</li>
                <li>Staff Preferences</li>
                <li>Menu Feedback</li>
                <li>Policy Feedback</li>
                <li>Capture Feelings</li>
                <li>Identify Motivators</li>
                <li>Procedure Feedback</li>
                <li>Trigger Positiveness</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 col-sm-12 md-margin-bottom-40">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x icon-line glyphicon glyphicon-briefcase"></i>
              <h2 class="heading-sm2">Marketing  Gurus</h2>
              <p class="para-updated">Polls to Continously engage all categories of Customers</p>
              <ul class="list-unstyled">
                <li>Poll of the Day</li>
                <li>Identify Preferences</li>
                <li>Know Habits</li>
                <li>Bring Humor & Fun</li>
                <li>Instill Positiveness</li>
                <li>Ad Feedback</li>
                <li>Product Feedback</li>
                <li>Service Feedback</li>
                <li>New concept Ideas</li>
                <li>Track Competitors</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 col-sm-12">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x icon-line glyphicon glyphicon-user"></i>
              <h2 class="heading-sm2">Content Managers</h2>
              <p class="para-updated">Polls that strengthen reader/visitor engagement</p>
              <ul class="list-unstyled">
                <li>Poll of the Day </li>
                <li>Reader Preferences</li>
                <li>Content Relevance </li>
                <li>Content Feedback</li>
                <li>Originality Feedback</li>
                <li>Editorial Feedback</li>
                <li>Improvement Ideas </li>
                <li>Readability Feedback</li>
                <li>Contributors’ Rating</li>
                <li>Reader Rating</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 col-sm-12">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x icon-line glyphicon glyphicon-globe"></i>
              <h2 class="heading-sm2">Web Publishers</h2>
              <p class="para-updated">Polls for enhancing site stickiness & monetize traffic</p>
              <ul class="list-unstyled">
                <li>Poll of the Day</li>
                <li>Visitors Profiling</li>
                <li>Track User Experience</li>
                <li>Know User Preferences</li>
                <li>Tech Aspect Feedback</li>
                <li>Identify Tech Issues </li>
                <li>Ads Feedback</li>
                <li>Pricing Feedback</li>
                <li>eCom Product Feedback</li>
                <li>Site Abandonments </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- End Service Blcoks -->
      </div>
    </div>
  </div>

    
    
  <div class="bg-grey content-sm pricing-page-intro" data-scroll-index='4' id="pricing">
    <div class="container text-center">
      <h2 class="title-v2 title-center" style="color:#1035A2 !important;">A PRICING PLAN TO SUIT YOUR UNIQUE NEEDS</h2>
      <p class="space-lg-hor para-updated" style="font-size: 15px;">Insighto Polls are available on demand and as per your budget</p>
      <br>
      <div class="row">
        <div class="col-md-6">
          <div class="service-block service-block-sea service-or">
            <div class="service-bg"></div>
            <!--<i class="icon-custom icon-color-light rounded-x fa fa-lightbulb-o"></i>-->
            <h2 class="heading-md pricing-color"><a href="pricing.aspx">Monthly Plans</a></h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="service-block service-block-blue service-or">
            <div class="service-bg"></div>
            <!--<i class="icon-custom icon-color-light rounded-x fa fa-lightbulb-o"></i>-->
            <h2 class="heading-md pricing-color"><a href="pricing.aspx">Annual Plans</a></h2>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <p class="para-updated-box" style="font-size:16px !important; margin-top:30px !important; text-align:center !important;"> Sign up for <a href="registration.aspx"><strong><u>Free plan</u></strong></a> (No Credit cards needed) and Upgrade anytime to any plan of your choice. </p>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </div>
  
    
    
</div>
    <script type="text/javascript">
        var sliderload = 0;
        function showSliderPoll() {
            //if (sliderload > 0) {
            //    $("#alertclose").remove();
            //    $("#IframeLaunch").remove();
            //    $("#pSlider").remove();
            //}
            $("#sliderpollsample").html("");
            var slidercontent = "";
            slidercontent += "<script src='https://www.insighto.com/Polls/WebScripts/insightoslider1546.js' type='text/javascript'\>\<\/script>";
            $("#sliderpollsample").html(slidercontent);
            $("#sliderpollsample").show();
            sliderload++;
        }

        function ShowPollWithDetails(framewidth, frameheight, frameObj, frameDiv, frameSource, frameAnimateTime, pollId) {
            $('#timedrpact2').remove();
            var output = document.createElement('div');
            output.setAttribute('id', frameDiv);
            output.setAttribute('class', 'pprev_style ' + frameObj + '');
            var ele = document.createElement('div');
            ele.setAttribute('id', 'timedrpact2');
            ele.setAttribute('class', 'pprev_style ' + frameObj + '');
            var alclose = document.createElement('div');
            alclose.setAttribute('id', 'alertclose');
            alclose.setAttribute('class', 'closeFlyoutPromo');
            ele.appendChild(alclose); output.appendChild(ele);
            $('body').append(output);
            var ifr = $('<iframe/>', {
                id: 'IframeLaunch6',
                src: frameSource,
                style: 'width:' + framewidth + 'px;height:' + frameheight + 'px;-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px;border-style:none;',
                load: function () {
                    $('.' + frameObj).animate({ bottom: '0px' }, frameAnimateTime, 'linear', function () { });

                    $('#alertclose').click(function () {
                        $('.' + frameObj).hide();
                        $('.' + frameObj).css('bottom', '-600px');
                    });
                    $('.' + frameObj).show();
                }
            });
            $('#timedrpact2').append(ifr);
        }

    </script>
    <%--<script src='https://www.insighto.com/Polls/WebScripts/insightocheckJS.min.js' type='text/javascript'></script>--%>
    <%--<link href='https://www.insighto.com/Polls/css/SliderStyleSheet.css' rel='stylesheet' type='text/css' />
    <script src='https://www.insighto.com//Polls/WebScripts/jquery.easing.min.js' type='text/javascript'></script>
    <div id="sliderpollsample"></div>--%>
        <%--<script src='http://localhost:8010/Polls/WebScripts/insightocheckJS.min.js' type='text/javascript'></script>--%>
    <link href='http://localhost:8010/Polls/css/SliderStyleSheet.css' rel='stylesheet' type='text/css' />
    <script src='http://localhost:8010/Polls/WebScripts/insightoslider1457.js' type='text/javascript'></script>
<%--    <script src='http://localhost:8010/Polls/WebScripts/insightoslider1456.js' type='text/javascript'></script>
    <script src='http://localhost:8010/Polls/WebScripts/insightoslider1455.js' type='text/javascript'></script>--%>
    <script src='http://localhost:8010//Polls/WebScripts/jquery.easing.min.js' type='text/javascript'></script>
    <iframe id='IframeLaunch' height='285' width='300'  scrolling='auto' src='http://localhost:8010/Polls/PollRespondent.aspx?key=ahXnbZlW8om9KKIt3OQ4DmsuIij4VtiEWwIR1xj8ttmTm%2fRsy3j1QQ%3d%3d' frameborder='0'   style='-moz-border-radius: 0px 0px 12px 12px; -webkit-border-radius: 0px 0px 12px 12px; border-radius: 0px 0px 12px 12px; -moz-box-shadow: 4px 4px 14px #000; -webkit-box-shadow: 4px 4px 14px #000; box-shadow: 4px 4px 14px #000;'  ></iframe>
</asp:Content>