﻿<%@ Page Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="AddOrEditQuestions.aspx.cs" Inherits="AddOrEditQuestions" %>

<%@ Register Src="~/UserControls/QuestionControls/AddOrEditQuestionsControl.ascx" TagName="AddOrEditQuestionsControl"
    TagPrefix="Insighto" %>
    <%@ Register Src="~/UserControls/QuestionsLeftMenu.ascx" TagName="QuestionsLeftMenu"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div class="popupHeading">
        <div class="popupTitle">
            <h3>
                <asp:Label ID="lblChangeQeustionMode" runat="server" Text="CREATE QUESTION"></asp:Label></h3>
        </div>
       <%-- <div class="popupClosePanel">
            <a href="#" id="hlnkPopUp" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(true,'');">
                &nbsp;</a>
        </div>--%>
    </div>
    <div class="popupContentPanel">
        <div class="clear">
        </div>
        <div class="popupLeftMenuPanel" runat="server" id="dvPopupLeftMenuPanel">
            <uc:questionsleftmenu id="ucQuestionsLeftMenu" runat="server" />
        </div>
         <div id="dvNoQuesionId" runat="server" class="informationControl" visible="false">
                <asp:Label ID="lblNoQuestion" runat="server" Visible="false" meta:resourcekey="lblNoQuestionResource1"></asp:Label>
                <div class="clear">
                </div>
            </div>
        <div class="createQuestionBody" runat="server" id="dvCreateQuestionBody">
            <Insighto:AddOrEditQuestionsControl ID="AddOrEditQuestionsControl1" runat="server" />
        </div>
          
    </div>
</asp:Content>
