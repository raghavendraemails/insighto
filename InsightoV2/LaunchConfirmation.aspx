﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="LaunchConfirmation.aspx.cs" Inherits="Insighto.Pages.LaunchConfirmation"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="clear">
    </div>
    <%-- <div class="contentPanel">
    <p class="defaultHeight"></p>    
    <div class="successPanel">
        <div><asp:Label ID="lblSurveyLable" runat="server" Text="" /></div>
    </div>
    <div class="launchConfirmationText" runat="server" id="divExternalMessage">       
                <p>Copy the link URL <asp:HyperLink ID="hlnkSurveyurl" runat="server" Target="_blank"></asp:HyperLink>    to your email system or website from where you intend to send the invites to your respondents. Once the invites get triggered, 
        Insighto would start tracking the responses from the moment your respondents submit their survey 
        *** Please note that the survey launch is subject to audit clearance for content.</p>
    </div>
    <p class="launchlbl"><asp:Label ID="lblSurveyLink" runat="server" Text="" /></p>
        
    </div>--%>
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblSurveyText" runat="server" Text="Survey Name :" 
                meta:resourcekey="lblSurveyTextResource1"></asp:Label>
            <asp:Label ID="lblSurveyName" runat="server" 
                meta:resourcekey="lblSurveyNameResource1"></asp:Label>
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="surveyCreateTextPanel" id="divExternalMessage" runat="server">
            <!-- survey created content panel -->
            <h1>
                 <asp:Label ID="lblCongrats" runat="server" Text="Congratulations!" 
                     meta:resourcekey="lblCongratsResource1"></asp:Label></h1>
            <p>
               
                &nbsp;</p>
            <div class="gridTopPanel">
                <p class="launchlbl">
                    <asp:Label ID="lblExternal" runat="server" 
                        meta:resourcekey="lblExternalResource1" /></p>
            </div>
            <div class="surveyUrlTextPanel">
                <p>
                    <asp:Label ID="lblSurveyLive" runat="server" 
                        Text=" Your survey is now live at the link." 
                        meta:resourcekey="lblSurveyLiveResource1"></asp:Label>
                    <br />
                    <asp:Label ID="lblCopyUrl" runat="server" 
                        Text="Copy the URL to anywhere you want to show the survey." 
                        meta:resourcekey="lblCopyUrlResource1"></asp:Label>
                    </p>
            </div>
            <div class="surveyUrlCopyPanel">
                <asp:TextBox ID="txtUrl" runat="server" CssClass="textBoxMedium txtUrl" 
                    ReadOnly="True" meta:resourcekey="txtUrlResource1"></asp:TextBox>&nbsp;&nbsp;
                <%--<a id="btnCopy" style="padding:0; font-weight:normal;" class="btnCopy" href="javascript:void(0);">Copy</a>--%>
                  <a id="btnCopy" style="padding:0; font-weight:normal;" class="btnCopy" href="javascript:void(0);" runat="server">Copy</a>
            </div>
            <div class="clear">
            </div>
            <%--     <div class="socialMediaPanel">
                <h3>
                    PUBLISH THROUGH</h3>
                <ul class="formList">
                    <li>
                        <label for="mailIcons">
                            Mail</label>
                        <a href="http://www.yahoo.com" target="_blank">
                            <img src="App_Themes/Classic/Images/icon-yahoo.gif" id="mailIcons" width="32" height="32"
                                alt="Yahoo" title="Yahoo" /></a> <a href="http://www.gmail.com" target="_blank">
                                    <img src="App_Themes/Classic/Images/icon-gmail.gif" width="32" height="32" alt="Gmail"
                                        title="Gmail" /></a> <a href="http://www.hotmail.com" target="_blank">
                                            <img src="App_Themes/Classic/Images/icon-windows.gif" width="32" height="32" alt="Windows"
                                                title="Windows" /></a> </li>
                    <li>
                        <label for="SocialIcons">
                            Social Network</label>
                        <a href="http://www.twitter.com" target="_blank">
                            <img src="App_Themes/Classic/Images/icon-twitter.gif" id="SocialIcons" width="32"
                                height="32" alt="Twitter" title="Twitter" /></a> <a href="http://www.linkedin.com"
                                    target="_blank">
                                    <img src="App_Themes/Classic/Images/icon-linkedin.gif" width="32" height="32" alt="Linkedin"
                                        title="Linkedin" /></a> <a href="http://www.facebook.com" target="_blank">
                                            <img src="App_Themes/Classic/Images/icon-facebook.gif" width="32" height="32" alt="Facebook"
                                                title="Facebook" /></a>
                    </li>
                    <li>
                        <label for="blogIcons">
                            Blog</label>
                        <a href="http://www.eblogger.com" target="_blank">
                            <img src="App_Themes/Classic/Images/icon-blogger.gif" id="blogIcons" width="32" height="32"
                                alt="eBlogger" title="eBlogger" /></a> <a href="http://wordpress.org/" target="_blank">
                                    <img src="App_Themes/Classic/Images/icon-wordpress.gif" width="32" height="32" alt="Wordpress"
                                        title="Wordpress" /></a> </li>
                </ul>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>--%>
            <!-- //survey created content panel -->
        </div>
        <div id="divInternalLaunch" runat="server">
            <div class="surveyCreateTextPanel" id="div1" runat="server">
                <!-- survey created content panel -->
                <h1>
                    <asp:Label ID="lblCongratsInternal" runat="server" Text="Congratulations!" 
                        meta:resourcekey="lblCongratsInternalResource1"></asp:Label>
                    </h1>
            </div>
            <div class="gridTopPanel">
                <h4>
                    <asp:Label ID="lblSurveyLable" runat="server" 
                        meta:resourcekey="lblSurveyLableResource1" /></h4>
                <br />
                <p class="launchlbl">
                    <asp:Label ID="lblSurveyLink" runat="server" 
                        meta:resourcekey="lblSurveyLinkResource1" /></p>
            </div>
        </div>
        <div id="divWidget" runat="server">
            <div class="surveyCreateTextPanel" id="div2" runat="server">
                <!-- survey created content panel -->
                <h1>
                   
            <asp:Label ID="lblCongratsWidget" runat="server" Text="Congratulations!" 
                        meta:resourcekey="lblCongratsWidgetResource1"></asp:Label> </h1>
            </div>
            <div class="gridTopPanel">
                <h4>
                    <asp:Label ID="lblWigetLabel" runat="server" 
                        meta:resourcekey="lblWigetLabelResource1" /></h4>
                <div class="defaultHeight">
                </div>
                <div class='informationPanelDefault'>
                    <div>
                        <asp:Label ID="lblWidgetText" runat="server" 
                            meta:resourcekey="lblWidgetTextResource1"></asp:Label></div>
                </div>
                <p>
                    <b><asp:Label ID="lblCopyScript" runat="server" Text="Copy the script" 
                        meta:resourcekey="lblCopyScriptResource1"></asp:Label></b>
                    
                </p>
                <asp:TextBox ID="txtWigetScript" runat="server" CssClass="launchtextarea" 
                    TextMode="MultiLine" meta:resourcekey="txtWigetScriptResource1"></asp:TextBox>
                <div class="clear">
                </div>
            </div>
        </div>
        <div class="crCardTypes">
            <asp:Button ID="btnMySurvey" CssClass="dynamicButton" runat="server" Text="My Surveys"
                ToolTip="My Surveys" OnClick="btnMySurvey_Click" 
                meta:resourcekey="btnMySurveyResource1" />
            &nbsp;&nbsp;<asp:Button ID="btnDashBoard" runat="server" CssClass="dynamicButton"
                OnClick="btnDashBoard_Click" Text="Survey Dashboard" 
                ToolTip="Survey Dashboard" meta:resourcekey="btnDashBoardResource1" />
            &nbsp;&nbsp;<asp:Button ID="btnReports" runat="server" CssClass="dynamicButton" OnClick="btnReports_Click"
                Text="Survey Reports" ToolTip="Survey Reports" 
                meta:resourcekey="btnReportsResource1" /></div>
        <!-- //content panel -->
    </div>
    <script type="text/javascript" src="Scripts/ZeroClipBoard/jquery.zclip.min.js"></script>
    <script type="text/javascript">
        $(".btnCopy").zclip({
            path: 'Scripts/ZeroClipBoard/ZeroClipboard.swf',
            copy: $('.txtUrl').val(),
            beforeCopy: function () {
               // $('.txtUrl').css('background', 'yellow');
                //$(this).css('color', 'orange');
            },
            afterCopy: function () {
                //$('.txtUrl').css('background', 'green');
                //$('.txtUrl').css('color', 'purple');
                //$(this).next('.check').show();
            }

        });
    </script>
</asp:Content>
