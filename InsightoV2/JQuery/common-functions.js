// clear form field
function clearDefault(el) {
  if (el.defaultValue==el.value) el.value = ""
}

// show navigation overlay (using jQuery instead)
//function showOverlay(overlayToShow,navToShow) {
//	document.getElementById(overlayToShow).style.display='block';
//	document.getElementById(navToShow).style.backgroundColor='#CFCFCF';
//}

// manually hide navigation overlay
function hideOverlay(overlayToHide,navToHide) {
	document.getElementById(overlayToHide).style.display='none';
	document.getElementById(navToHide).style.backgroundColor='transparent';
}

// navigation switcher (requires jquery)
	$(document).ready(function(){
	
		// This will hold our timer
		var myTimer1 = {};
		var myTimer2 = {};
		var myTimer3 = {};
		var myTimer4 = {};
	
		$("#navPlatform").mouseover(
		
			function () {
			
				document.getElementById('navPlatformA').style.backgroundColor='#FCFDEA';
				document.getElementById('navPlatformA').style.color='#000000';
				document.getElementById('navPlatformA').style.borderTop=1+'px solid #D8E335';
				document.getElementById('navPlatformA').style.borderRight=1+'px solid #D8E335';
				document.getElementById('navPlatformA').style.borderLeft=1+'px solid #D8E335';

				$.clearTimer(myTimer1);
				// Delay 500 second
				myTimer1 = $.timer(500,function(){
					$("#navOverlayPlatform").slideDown(350);
				});
				// end delay
			}
		);

		$("#navPlatform").mouseout(
		
			function () {
			
				$.clearTimer(myTimer1);
				
				// Delay .35 second
				myTimer1 = $.timer(350,function(){
					document.getElementById('navPlatformA').style.backgroundColor='transparent';
					document.getElementById('navPlatformA').style.color='#ffffff';
					document.getElementById('navPlatformA').style.borderTop=1+'px solid transparent';
					document.getElementById('navPlatformA').style.borderRight=1+'px solid #E8523F';
					document.getElementById('navPlatformA').style.borderLeft=1+'px solid #E8523F';
					$("#navOverlayPlatform").hide();
				});
				// end delay
			}
		);

		$("#navCustomerSuccess").mouseover(
		
			function () {
			
				document.getElementById('navCustomerSuccessA').style.backgroundColor='#FCFDEA';
				document.getElementById('navCustomerSuccessA').style.color='#000000';
				document.getElementById('navCustomerSuccessA').style.borderTop=1+'px solid #D8E335';
				document.getElementById('navCustomerSuccessA').style.borderRight=1+'px solid #D8E335';
				document.getElementById('navCustomerSuccessA').style.borderLeft=1+'px solid #D8E335';

				$.clearTimer(myTimer2);
				
				// Delay 500 second
				myTimer2 = $.timer(500,function(){
					$("#navOverlayCustomerSuccess").slideDown(350);
				});
				// end delay
			}
		);
		
		$("#navCustomerSuccess").mouseout(
		
			function () {
			
				$.clearTimer(myTimer2);
				
				// Delay .35 second
				myTimer2 = $.timer(350,function(){
					document.getElementById('navCustomerSuccessA').style.backgroundColor='transparent';
					document.getElementById('navCustomerSuccessA').style.color='#ffffff';
					document.getElementById('navCustomerSuccessA').style.borderTop=1+'px solid transparent';
					document.getElementById('navCustomerSuccessA').style.borderRight=1+'px solid #E8523F';
					document.getElementById('navCustomerSuccessA').style.borderLeft=1+'px solid transparent';
					$("#navOverlayCustomerSuccess").hide(0);
				});
				// end delay
			}
		);

		$("#navPartners").mouseover(
		
			function () {
			
				document.getElementById('navPartnersA').style.backgroundColor='#FCFDEA';
				document.getElementById('navPartnersA').style.color='#000000';
				document.getElementById('navPartnersA').style.borderTop=1+'px solid #D8E335';
				document.getElementById('navPartnersA').style.borderRight=1+'px solid #D8E335';
				document.getElementById('navPartnersA').style.borderLeft=1+'px solid #D8E335';
				
				$.clearTimer(myTimer3);
				
				// Delay 500 second
				myTimer3 = $.timer(500,function(){
					$("#navOverlayPartners").slideDown(350);
				});
				// end delay
			}
		);
		
		$("#navPartners").mouseout(
		
			function () {
			
				$.clearTimer(myTimer3);
				
				// Delay .35 second
				myTimer3 = $.timer(350,function(){
					document.getElementById('navPartnersA').style.backgroundColor='transparent';
					document.getElementById('navPartnersA').style.color='#ffffff';
					document.getElementById('navPartnersA').style.borderTop=1+'px solid transparent';
					document.getElementById('navPartnersA').style.borderRight=1+'px solid #E8523F';
					document.getElementById('navPartnersA').style.borderLeft=1+'px solid transparent';
					$("#navOverlayPartners").hide();
				});
				// end delay
			}
		);


		$("#navAboutEloqua").mouseover(		
			function () {
			
				document.getElementById('navAboutEloquaA').style.backgroundColor='#FCFDEA';
				document.getElementById('navAboutEloquaA').style.color='#000000';
				document.getElementById('navAboutEloquaA').style.borderTop=1+'px solid #D8E335';
				document.getElementById('navAboutEloquaA').style.borderRight=1+'px solid #D8E335';
				document.getElementById('navAboutEloquaA').style.borderLeft=1+'px solid #D8E335';

				$.clearTimer(myTimer4);
				
				// Delay 500 second
				myTimer4 = $.timer(500,function(){
					$("#navOverlayAbout").slideDown(350);
				});
				// end delay
			}
		);
		
		$("#navAboutEloqua").mouseout(
		
			function () {
			
				$.clearTimer(myTimer4);
				
				// Delay .35 second
				myTimer4 = $.timer(350,function(){
					document.getElementById('navAboutEloquaA').style.backgroundColor='transparent';
					document.getElementById('navAboutEloquaA').style.color='#ffffff';
					document.getElementById('navAboutEloquaA').style.borderTop=1+'px solid transparent';
					document.getElementById('navAboutEloquaA').style.borderRight=1+'px solid transparent';
					document.getElementById('navAboutEloquaA').style.borderLeft=1+'px solid transparent';
					$("#navOverlayAbout").hide();
				});
				// end delay
			}
		);
	});

function lgtbxSlideshowOn() {
	document.getElementById('slidescreen').style.display='block';
}

function lgtbxSlideshowOff() {
	document.getElementById('slidescreen').style.display='none';
}

