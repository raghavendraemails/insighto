﻿using System;
using System.Collections.Generic;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using Insighto.Data;
using System.Collections;
using System.Web.UI;
using System.Configuration;
using App_Code;
using System.Data;

namespace Insighto.Pages
{
   
    public partial class SurveyBasics : SecurePageBase
    {
        string tempsurvey = "";
        int tempsurveyid;
        public static int SurveyIdEdit;
        SurveyCore surcore = new SurveyCore();

        public string Mode
        {
            get
            {

                string mode = string.Empty;
                Hashtable ht1 = new Hashtable();
                ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (ht1 != null)
                {

                    if (ht1.Contains("Mode"))
                    {
                        mode = ht1["Mode"].ToString();
                    }

                }
                return mode;
            }
        }

        //public string temp {
        //    get;
        //    set{

        //    }
        //        }

        public int SurveyIdCopy
        {
            get
            {
                Hashtable ht1 = new Hashtable();
                int surveyId = 0;
                ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));

                if (ht1 != null)
                {
                    if (ht1.Contains(Constants.SURVEYID))
                    {
                        surveyId = ValidationHelper.GetInteger(ht1[Constants.SURVEYID], 0);
                    }
                }
                return surveyId;
            }
        }
        DataSet dsppscredits;
        protected void Page_Load(object sender, EventArgs e)
        { 
            Hashtable typeHt = new Hashtable();
        if (Request.QueryString["key"] != null)
        {

            tempsurvey = Request.QueryString["tempsurveyid"];
            if (tempsurvey != "" && tempsurvey != null)
            {
                tempsurveyid = Convert.ToInt32(tempsurvey);
                DataSet dssurveyname = surcore.getSurveyType(tempsurveyid);
               lbltemsurvey.Text = "You are using the following survey template : " + dssurveyname.Tables[0].Rows[0][2].ToString();
               divtemsurveyname.Visible = true;
               dvRadioButtons.Visible = false;
               rdoChooseaTemplate.Checked = true;
            }
        }
            if (!IsPostBack)
            {
                BindCategory();
                BindFolders();

               // SurveyIdEdit = QueryStringHelper.GetInt("SurveyId", 0);
               // if (SurveyIdEdit > 0)
               // {
                GetSuvrveyDetailsBasedOnSurveyId(SurveyIdCopy);
                    DisbaleRadioButtonOnEditSurvey();
                //}
                //else if (SurveyIdCopy > 0)
                //{
                //    GetSuvrveyDetailsBasedOnSurveyId(SurveyIdCopy);
                //    DisbaleRadioButtonOnEditSurvey();
                //}
                lblTitle.Text = String.Format(Utilities.ResourceMessage("lblTitleResource1.Text"), SurveyMode);
                lblSurveyName.Text = String.Format(Utilities.ResourceMessage("lblSurveyNameResource1.Text"), SurveyMode);
                reqSurveyName.ErrorMessage = String.Format(Utilities.ResourceMessage("ReqSurveyNameResource1.ErrorMessage"), SurveyMode.ToLower());                              
            }

            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            dsppscredits = surcore.getCreditsCount(userInfo.UserId);

            if (dsppscredits.Tables[0].Rows.Count > 0)
            {
                lblprocredits.Text = "( Available Credits: " + dsppscredits.Tables[0].Rows.Count.ToString() + ")";
            }
            else
            {
                lblprocredits.Text = "( Available Credits: 0)";
            }

            if (dsppscredits.Tables[1].Rows.Count > 0)
            {
                lblpremcredits.Text = "( Available Credits: " + dsppscredits.Tables[1].Rows.Count.ToString() + ")";
            }
            else
            {
                lblpremcredits.Text = "( Available Credits: 0)";
            }


            if (!IsPostBack)
            {
                if (userInfo.LicenseType == "FREE")
                {
                    trfree.Visible = false;
                    lblsurveyplan.Visible = false;
                }
                if (userInfo.LicenseType == "PREMIUM_YEARLY")
                {
                    divcredits.Visible = false;
                    trfree.Visible = false;
                }
                if (lblprocredits.Text == "( Available Credits: 0)")
                {
                    trprocredit.Visible = false;
                    trfree.Visible = false;
                }
                if (lblpremcredits.Text == "( Available Credits: 0)")
                {
                    trpremcredit.Visible = false;
                    trfree.Visible = false;
                }
            }
        }


        #region "Method : BindCategory"
        /// <summary>
        /// Used to get survey category list and bind values to dropdownlist.
        /// </summary>

        private void BindCategory()
        {
            // object on osm_picklist class
            osm_picklist osmPickList = new osm_picklist();
            //Assign vaule category
            osmPickList.CATEGORY = Constants.CATEGORY;
            // object on PickListService class
            var pickListService = ServiceFactory.GetService<PickListService>();
            //inovke method GetPickList
            var vPickList = pickListService.GetPickList(osmPickList);

            if (vPickList.Count > 0)
            {
                //Bind list values to dropdownlist
                this.ddlCategory.DataSource = vPickList;
                this.ddlCategory.DataTextField = Constants.PARAMETER;
                this.ddlCategory.DataValueField = Constants.PARAMETER;
                this.ddlCategory.DataBind();
                //Default selection (Others)
                this.ddlCategory.SelectedValue = Constants.OTHERS;
            }

        }
        #endregion

        #region "Method : BindFolders"
        /// <summary>   
        /// Used to get user folders list and bind values to dropdownlist.
        /// </summary>

        private void BindFolders()
        {
            // object on osm_cl_folders class
            osm_cl_folders osmFolder = new osm_cl_folders();
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            //Assign vaule userid
            if (loggedInUserInfo != null)
            {
                osmFolder.USERID = loggedInUserInfo.UserId;
                // object on FolderService class
                var folderService = ServiceFactory.GetService<FolderService>();
                //inovke method GetFolders
                var vfolder = folderService.GetFolders(osmFolder);

                if (vfolder.Count > 0)
                {
                    //Bind list values to dropdownlist
                    this.ddlFolder.DataSource = vfolder;
                    this.ddlFolder.DataTextField = "FOLDER_NAME";
                    this.ddlFolder.DataValueField = "FOLDER_ID";
                    this.ddlFolder.DataBind();
                    //Default selection (Others)
                    if (vfolder.Count > 0)
                    {
                        foreach (var folders in vfolder)
                        {
                            if (folders.FOLDER_NAME == "My Folder")
                            {
                                this.ddlFolder.SelectedValue = folders.FOLDER_ID.ToString();
                            }
                        }
                    }
                    else
                    {
                        this.ddlFolder.SelectedItem.Text = Constants.MYFOLDER;
                    }
                    
                }
            }
            else
                base.CloseModal("Home.aspx");
        }

        #endregion

        #region "btnSaveFolderClick"
        /// <summary>
        /// SaveFolder click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveFolder_Click(object sender, EventArgs e)
        {
            bool folderExists = default(bool);

            osm_cl_folders osmFolder = new osm_cl_folders();
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            var loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();

            if (loggedInUserInfo != null)
            {
                osmFolder.USERID = loggedInUserInfo.UserId;
                osmFolder.FOLDER_NAME =Server.HtmlEncode(txtNewFolder.Text.Trim());

                var folderService = ServiceFactory.GetService<FolderService>();
                folderExists = folderService.IsFolderExsists(osmFolder);
                if (folderExists == true)
                {
                    osmFolder = folderService.AddFolder(osmFolder);
                    BindFolders();
                    ddlFolder.SelectedValue = osmFolder.FOLDER_ID.ToString();
                    txtNewFolder.Text = string.Empty;
                    lblSucess.Text = "";
                    //lblSucess.Visible = false;
                }
                else
                {
                    lblSucess.Text = "Folder name already exists.";
                    string param = "addNewFolderLink";
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "test", "showContents('" + param + "');", true);
                }
            }
            else
            {
                base.CloseModal("Home.aspx");
            }
        }

        #endregion

        #region "Method:CreateSurveyClick"
        /// <summary>
        /// Create Survey Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCreateSurvey_Click(object sender, EventArgs e)
        {
           
            if (Mode == "Edit")
            {
                UpdateSurveyBySurveyId(SurveyIdCopy);
                 
            }
            else
            {
                AddSurvey();
            }
        }
        #endregion

        #region "Method : GetSuvrveyDetailsBasedOnSurveyId"
        /// <summary>
        /// GetSuvrveyDetailsBasedOnSurveyId
        /// </summary>
        /// <param name="SurveyId"></param>
        private void GetSuvrveyDetailsBasedOnSurveyId(int surveyId)
        {
            //osm_survey osmSurvey = new osm_survey();
            var surveyService = ServiceFactory.GetService<SurveyService>();
            List<osm_survey> sfolder = new List<osm_survey>();
            //var sfolder = new List<osm_survey>();

            sfolder = surveyService.GetSuvrveyDetailsBasedOnSurveyId(surveyId);

            if (sfolder.Count > 0)
            {
                if (Mode != "Copy")
                {
                    txtSurveyName.Text = sfolder[0].SURVEY_NAME;
                }
                else
                {
                    txtSurveyName.Text = "";
                }
                ddlCategory.SelectedItem.Text = sfolder[0].SURVEY_CATEGORY;
                ddlFolder.SelectedValue = sfolder[0].FOLDER_ID.ToString();
            }
        }
        #endregion

        #region "Method:UpdateSurveyBySurveyId"
        /// <summary>
        /// Update Survey by SurveyId
        /// </summary>
        /// <param name="SurveyId"></param>
        private void UpdateSurveyBySurveyId(int surveyId)
        {
            osm_survey osmSurvey = new osm_survey();
            SessionStateService sessionStateService = new SessionStateService();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            if (loggedInUserInfo != null)
            {
                osmSurvey.USERID = loggedInUserInfo.UserId;
                //osmSurvey.STATUS = "Draft";
                osmSurvey.SURVEY_ID = surveyId;
                osmSurvey.SURVEY_CATEGORY = ddlCategory.SelectedItem.Text;
                osmSurvey.SURVEY_NAME = txtSurveyName.Text.Trim();
                osmSurvey.FOLDER_ID = Convert.ToInt16(ddlFolder.SelectedItem.Value);
                var surveyService = ServiceFactory.GetService<SurveyService>();
                var retValue = surveyService.UpdateSurvey(osmSurvey);
                if (retValue == false)
                {
                    //lblErrMsg.Text = Utilites.ResourceMessage("lblErrMsg");
                    lblErrMsg.Text =  SurveyMode + " name already exists.";
                    lblErrMsg.Visible = true;
                    dvErrMsg.Visible = true;
                    
                }
                else
                {
                    lblErrMsg.Visible = false;
                    lblErrMsg.Text = "";
                    dvErrMsg.Visible = false;
                    base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + osmSurvey.SURVEY_ID + "&surveyName=" + osmSurvey.SURVEY_NAME + "&surveyFlag=" + base.SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                    //base.CloseModal("SurveyDrafts.aspx?surveyId=" + osmSurvey.SURVEY_ID + "&" + "surveyName=" + osmSurvey.SURVEY_NAME);
                }
            }
            else
            {
                base.CloseModal("Home.aspx");
            }
        }
        #endregion

        #region "Method : AddSurvey"
        /// <summary>
        /// Create New Survey
        /// </summary>
        private void AddSurvey()
        {

            osm_survey osmSurvey = new osm_survey();
            var sessionStateService = ServiceFactory.GetService<SessionStateService>();
            //SessionStateService sessionStateService = new SessionStateService();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            osm_surveysetting osmSurveySetting = new osm_surveysetting();

            if (loggedInUserInfo != null)
            {
                osmSurvey.USERID = loggedInUserInfo.UserId;
                osmSurvey.STATUS = "Draft";
                osmSurvey.SURVEY_CATEGORY = ddlCategory.SelectedItem.Text;
                osmSurvey.SURVEY_NAME = txtSurveyName.Text.Trim();
                osmSurvey.FOLDER_ID = Convert.ToInt16(ddlFolder.SelectedItem.Value);
                osmSurvey.SurveyWidgetFlag = 0;
                var surveyService = ServiceFactory.GetService<SurveyService>();

                if (rdoCopyExistingSurvey.Checked)
                {
                    var survey = surveyService.GetSurveyByUserID(loggedInUserInfo.UserId);
                    if (survey.Count > 0)
                    {

                        osmSurvey = surveyService.AddSurvey(osmSurvey);

                        if (osmSurvey == null)
                        {
                            //lblErrMsg.Text = Utilites.ResourceMessage("lblErrMsg");
                            lblErrMsg.Text = SurveyMode + " name already exists.";
                            lblErrMsg.Visible = true;
                            dvErrMsg.Visible = true;
                        }
                        else
                        {
                            var accountSettingService = ServiceFactory.GetService<AccountSettingService>();
                            var accountSetDet = accountSettingService.FindByUserId(loggedInUserInfo.UserId);
                            osmSurveySetting.SURVEY_ID = osmSurvey.SURVEY_ID;
                            osmSurveySetting.SURVEY_HEADER = accountSetDet.SURVEY_HEADER;
                            osmSurveySetting.SURVEY_LOGO = accountSetDet.SURVEY_LOGO;
                            osmSurveySetting.SURVEY_FOOTER = accountSetDet.SURVEY_FOOTER;
                            osmSurveySetting.SURVEY_FONT_TYPE = accountSetDet.SURVEY_FONTTYPE;
                            osmSurveySetting.SURVEY_FONT_SIZE = accountSetDet.SURVEY_FONTSIZE;
                            osmSurveySetting.SURVEY_FONT_COLOR = accountSetDet.SURVEY_FONTCOLOR;
                            osmSurveySetting.SURVEY_BUTTON_TYPE = accountSetDet.SURVEY_BUTTONTYPE;
                            osmSurveySetting.CUSTOMSTART_TEXT = accountSetDet.SURVEY_STARTTEXT;
                            osmSurveySetting.CUSTOMSUBMITTEXT = accountSetDet.SURVEY_SUBMITTEXT;
                            osmSurveySetting.PAGE_BREAK = accountSetDet.PAGE_BREAK;
                            osmSurveySetting.RESPONSE_REQUIRED = accountSetDet.RESPONSE_REQUIRED;
                            osmSurveySetting.BROWSER_BACKBUTTON = 1;
                            osmSurveySetting.DELETED = 0;
                            osmSurveySetting.BUTTON_BG_COLOR = "Purple";
                           // osmSurveySetting.SURVEY_LOGO = "<App_Themes/Classic/Images/insighto_logo_medium.jpg><224_81_medium_224_81><Left>";
                            osmSurveySetting.Show_Logo = true;
                            osmSurveySetting.ProgressbarText = "Questions viewed";
                            osmSurveySetting.Show_Progressbar = true;
                            osmSurveySetting.Progressbar_Type = "ProgressBarClassic";
                            osmSurveySetting.Show_Progressbartext = true;
                            surveyService.SetSurveySetting(osmSurveySetting);
                            lblErrMsg.Visible = false;
                            lblErrMsg.Text = "";
                            dvErrMsg.Visible = false;
                            base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("CopyExsistingSurvey.aspx", "SurveyId=" + osmSurvey.SURVEY_ID + "&surveyName=" + osmSurvey.SURVEY_NAME + "&surveyFlag=" + base.SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);

                            if (procredit.Checked == true && dsppscredits.Tables[0].Rows.Count > 0)
                            {
                                DataSet dsproupdate = surcore.updatesurveyType(loggedInUserInfo.UserId, "PPS_PRO", osmSurvey.SURVEY_ID, "PRO_YEARLY",osmSurvey.STATUS);
                               // DataSet dsapplycreditsurvey = surcore.applycreditssurvey(loggedInUserInfo.UserId, "PPS_PRO", osmSurvey.SURVEY_ID);
                            }
                            else if (premcredit.Checked == true && dsppscredits.Tables[1].Rows.Count > 0)
                            {
                                DataSet dsproupdate = surcore.updatesurveyType(loggedInUserInfo.UserId, "PPS_PREMIUM", osmSurvey.SURVEY_ID, "PREMIUM_YEARLY",osmSurvey.STATUS);
                              //  DataSet dsapplycreditsurvey = surcore.applycreditssurvey(loggedInUserInfo.UserId, "PPS_PREMIUM", osmSurvey.SURVEY_ID);
                            }
                        }
                    }
                    else
                    {
                        lblErrMsg.Visible = true;
                        lblErrMsg.Text = "You do not have any existing surveys to copy.";
                        dvErrMsg.Visible = true;   
                        
                    }
                }
                else  if (rdoCreateNewSurvey.Checked || (rdoChooseaTemplate.Checked) || Mode == "Copy")
                {
                    
                    osmSurvey = surveyService.AddSurvey(osmSurvey);
                    if (osmSurvey == null)
                    {
                        //lblErrMsg.Text = Utilites.ResourceMessage("lblErrMsg");
                        lblErrMsg.Text = SurveyMode +" name already exists.";
                        lblErrMsg.Visible = true;
                        dvErrMsg.Visible = true;
                        
                    }
                    else if (rdoCreateNewSurvey.Checked == true || Mode == "Copy" || rdoChooseaTemplate.Checked ==true || rdoCopyExistingSurvey.Checked ==true)
                    {
                        var accountSettingService = ServiceFactory.GetService<AccountSettingService>();
                        var accountSetDet = accountSettingService.FindByUserId(loggedInUserInfo.UserId);
                        osmSurveySetting.SURVEY_ID = osmSurvey.SURVEY_ID;
                        osmSurveySetting.SURVEY_HEADER = accountSetDet.SURVEY_HEADER;
                        osmSurveySetting.SURVEY_LOGO = accountSetDet.SURVEY_LOGO;
                        osmSurveySetting.SURVEY_FOOTER = accountSetDet.SURVEY_FOOTER;
                        osmSurveySetting.SURVEY_FONT_TYPE = accountSetDet.SURVEY_FONTTYPE;
                        osmSurveySetting.SURVEY_FONT_SIZE = accountSetDet.SURVEY_FONTSIZE;
                        osmSurveySetting.SURVEY_FONT_COLOR = accountSetDet.SURVEY_FONTCOLOR;
                        osmSurveySetting.SURVEY_BUTTON_TYPE = accountSetDet.SURVEY_BUTTONTYPE;
                        osmSurveySetting.CUSTOMSTART_TEXT = accountSetDet.SURVEY_STARTTEXT;
                        osmSurveySetting.CUSTOMSUBMITTEXT = accountSetDet.SURVEY_SUBMITTEXT;
                        osmSurveySetting.PAGE_BREAK = accountSetDet.PAGE_BREAK;
                        osmSurveySetting.RESPONSE_REQUIRED = accountSetDet.RESPONSE_REQUIRED;
                        osmSurveySetting.BROWSER_BACKBUTTON = 1;
                        osmSurveySetting.DELETED = 0;
                        osmSurveySetting.BUTTON_BG_COLOR = "Purple";
                        //osmSurveySetting.SURVEY_LOGO = "<App_Themes/Classic/Images/insighto_logo_medium.jpg><224_81_medium_224_81><Left>";
                        osmSurveySetting.Show_Logo = true;  
                        osmSurveySetting.ProgressbarText = "Questions viewed";
                        osmSurveySetting.Show_Progressbar = true;
                        osmSurveySetting.Progressbar_Type = "ProgressBarClassic";
                        osmSurveySetting.Show_Progressbartext = true;  
                        surveyService.SetSurveySetting(osmSurveySetting);
                        lblErrMsg.Visible = false;
                        lblErrMsg.Text = "";
                        dvErrMsg.Visible = false;
                        if (rdoChooseaTemplate.Checked == true)
                        {
                            if (tempsurvey != "" && tempsurvey != null)
                            {
                                //reateSurveyFromTemplate(tempsurveyid, osmSurvey.SURVEY_ID, osmSurvey.SURVEY_NAME, base.SurveyFlag);
                             string url=surcore.CreateSurveyFromTemplate(tempsurveyid, osmSurvey.SURVEY_ID, osmSurvey.SURVEY_NAME, base.SurveyFlag);
                             base.CloseModelForSpecificTime(url, ConfigurationManager.AppSettings["TimeOut"]);
                            }
                            else {
                                base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("SurveyTemplate.aspx", "SurveyId=" + osmSurvey.SURVEY_ID + "&surveyName=" + osmSurvey.SURVEY_NAME + "&surveyFlag=" + base.SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]); 
                            }
                           
                        }
                        //else if (rdoCopyExistingSurvey.Checked == true)
                        //{
                        //    base.CloseModal(EncryptHelper.EncryptQuerystring("CopyExsistingSurvey.aspx", "SurveyId=" + osmSurvey.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag));
                        //}
                        else if (rdoCreateNewSurvey.Checked ==true && Mode!="Copy")
                        {
                            var userSettings = ServiceFactory<AccountSettingService>.Instance.GetallUserSettingsByUserId(loggedInUserInfo.UserId);
                            var settingsTheme = userSettings.THEME != null ? userSettings.THEME : "Insighto Classic";
                            ServiceFactory<SurveyService>.Instance.ChangeSurveyTheme(settingsTheme, osmSurvey.SURVEY_ID);
                            base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + osmSurvey.SURVEY_ID + "&surveyName=" + osmSurvey.SURVEY_NAME + "&surveyFlag=" + base.SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                        }               
                        else if(Mode == "Copy")
                        {
                            surveyService.CopySurveyDet(SurveyIdCopy, osmSurvey.SURVEY_ID);
                            base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + osmSurvey.SURVEY_ID + "&surveyName=" + osmSurvey.SURVEY_NAME + "&surveyFlag=" + base.SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                        }

                        if (procredit.Checked==true )
                        {
                            DataSet dsproupdate = surcore.updatesurveyType(loggedInUserInfo.UserId, "PPS_PRO", osmSurvey.SURVEY_ID, "PRO_YEARLY",osmSurvey.STATUS);
                         //   DataSet dsapplycreditsurvey = surcore.applycreditssurvey(loggedInUserInfo.UserId, "PPS_PRO", osmSurvey.SURVEY_ID);
                            
                        }
                        else if (premcredit.Checked==true)
                        {
                            DataSet dsproupdate = surcore.updatesurveyType(loggedInUserInfo.UserId, "PPS_PREMIUM", osmSurvey.SURVEY_ID, "PREMIUM_YEARLY",osmSurvey.STATUS);
                           // DataSet dsapplycreditsurvey = surcore.applycreditssurvey(loggedInUserInfo.UserId, "PPS_PREMIUM", osmSurvey.SURVEY_ID);
                        }

                        //base.CloseModal("CreateNewSurveyBlank.aspx?surveyId=" + osmSurvey.SURVEY_ID + "&" + "surveyName=" + osmSurvey.SURVEY_NAME);
                    }



                }
                else
                {
                    lblErrMsg.Visible = true;
                    lblErrMsg.Text = "Copying this template will only copy questions and number of questions available for free subscription.";
                    dvErrMsg.Visible = true;
                }

            }
            else
            {
                base.CloseModal("Home.aspx");
            }
        }
        #endregion

        #region
        public void DisbaleRadioButtonOnEditSurvey()
        {


            if (Mode == "Edit")
            {
                dvRadioButtons.Visible = false;
                btnCreateSurvey.Text = "Save";
                if (SurveyFlag > 0)
                    dvRadioButtons.Visible = false;
            }
            else
            {
                if (tempsurvey != "" && tempsurvey != null)
                {
                    dvRadioButtons.Visible = false;
                }
                else
                {
                    dvRadioButtons.Visible = true;
                }
                
                btnCreateSurvey.Text = "Create";
            }

            if (Mode == "Copy")
            {
                dvRadioButtons.Visible = false;
            } 


        }
        #endregion
    }
}
