﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.master" AutoEventWireup="true"
    CodeFile="Welcome.aspx.cs" Inherits="Welcome" Culture="auto" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome - Insighto</title>
 <%--<link href="App_Themes/Classic/style.css" rel="stylesheet" type="text/css" />--%>
    <link href="Styles/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="wrapper">
    <div id="container_main" class="success_main">
        <div class="welcome_title_blk">
            <div class="welcome_title">
                <h3>Welcome to Insighto.</h3>
                Thank you for signing up for the Basic subscription.
            </div>
        </div>
            <div class="welcome_blk">
        	<div class="welcome_blkL">
            	<p class="underline">Important – please read</p>
                <p>A welcome email has been sent to you with a link to <strong>activate</strong> your account.</p>
                <p>While you can create your surveys, you would be required to activate your account before launching your survey.</p>
                <p>P.S: If you do not find the welcome email in your inbox, please do check for it in your spam folder. If you find the mail in your spam folder, please mark it as  'Not Spam' – thank you. </p>
                <p>If you have any issues, please report to <a href="mailto:support@insighto.com" >support@insighto.com</a></p>
            </div>
        	<div class="welcome_blkR">
            	<p class="underline">Please select your time zone</p>
                <p>
                	<%--<select name="" class="dropbox_timezone">
                    	<option>(+05:30) Chennai, Kolkata, Mumbai, New</option>
                    </select>--%>
                    <asp:DropDownList ID="drpTimeZone" runat="server" class="dropbox_timezone" 
                        meta:resourcekey="drpTimeZoneResource1" Width="360px">
                            <asp:ListItem Value="-12:00,0">(-12:00) International Date Line West</asp:ListItem>
                            <asp:ListItem Value="-11:00,0">(-11:00) Midway Island, Samoa</asp:ListItem>
                            <asp:ListItem Value="-10:00,0">(-10:00) Hawaii</asp:ListItem>
                            <asp:ListItem Value="-09:00,1">(-09:00) Alaska</asp:ListItem>
                            <asp:ListItem Value="-08:00,1">(-08:00) Pacific Time (US & Canada)</asp:ListItem>
                            <asp:ListItem Value="-07:00,0">(-07:00) Arizona</asp:ListItem>
                            <asp:ListItem Value="-07:00,1">(-07:00) Mountain Time (US & Canada)</asp:ListItem>
                            <asp:ListItem Value="-06:00,0">(-06:00) Central America, Saskatchewan</asp:ListItem>
                            <asp:ListItem Value="-06:00,1">(-06:00) Central Time (US & Canada), Guadalajara, Mexico city</asp:ListItem>
                            <asp:ListItem Value="-05:00,0">(-05:00) Indiana, Bogota, Lima, Quito, Rio Branco</asp:ListItem>
                            <asp:ListItem Value="-05:00,1">(-05:00) Eastern time (US & Canada)</asp:ListItem>
                            <asp:ListItem Value="-04:00,1">(-04:00) Atlantic time (Canada), Manaus, Santiago</asp:ListItem>
                            <asp:ListItem Value="-04:00,0">(-04:00) Caracas, La Paz</asp:ListItem>
                            <asp:ListItem Value="-03:30,1">(-03:30) Newfoundland</asp:ListItem>
                            <asp:ListItem Value="-03:00,1">(-03:00) Greenland, Brasilia, Montevideo</asp:ListItem>
                            <asp:ListItem Value="-03:00,0">(-03:00) Buenos Aires, Georgetown</asp:ListItem>
                            <asp:ListItem Value="-02:00,1">(-02:00) Mid-Atlantic</asp:ListItem>
                            <asp:ListItem Value="-01:00,1">(-01:00) Azores</asp:ListItem>
                            <asp:ListItem Value="-01:00,0">(-01:00) Cape Verde Is.</asp:ListItem>
                            <asp:ListItem Value="00:00,0">(00:00) Casablanca, Monrovia, Reykjavik</asp:ListItem>
                            <asp:ListItem Value="00:00,1">(00:00) GMT: Dublin, Edinburgh, Lisbon, London</asp:ListItem>
                            <asp:ListItem Value="+01:00,1">(+01:00) Amsterdam, Berlin, Rome, Vienna, Prague, Brussels</asp:ListItem>
                            <asp:ListItem Value="+01:00,0">(+01:00) West Central Africa</asp:ListItem>
                            <asp:ListItem Value="+02:00,1">(+02:00) Amman, Athens, Istanbul, Beirut, Cairo, Jerusalem</asp:ListItem>
                            <asp:ListItem Value="+02:00,0">(+02:00) Harare, Pretoria</asp:ListItem>
                            <asp:ListItem Value="+03:00,1">(+03:00) Baghdad, Moscow, St. Petersburg, Volgograd</asp:ListItem>
                            <asp:ListItem Value="+03:00,0">(+03:00) Kuwait, Riyadh, Nairobi, Tbilisi</asp:ListItem>
                            <asp:ListItem Value="+03:30,0">(+03:30) Tehran</asp:ListItem>
                            <asp:ListItem Value="+04:00,0">(+04:00) Abu Dhadi, Muscat</asp:ListItem>
                            <asp:ListItem Value="+04:00,1">(+04:00) Baku, Yerevan</asp:ListItem>
                            <asp:ListItem Value="+04:30,0">(+04:30) Kabul</asp:ListItem>
                            <asp:ListItem Value="+05:00,1">(+05:00) Ekaterinburg</asp:ListItem>
                            <asp:ListItem Value="+05:00,0">(+05:00) Islamabad, Karachi, Tashkent</asp:ListItem>
                            <asp:ListItem Value="+05:30,0">(+05:30) Chennai, Kolkata, Mumbai, New Delhi, Sri Jayawardenepura</asp:ListItem>
                            <asp:ListItem Value="+05:45,0">(+05:45) Kathmandu</asp:ListItem>
                            <asp:ListItem Value="+06:00,0">(+06:00) Astana, Dhaka</asp:ListItem>
                            <asp:ListItem Value="+06:00,1">(+06:00) Almaty, Nonosibirsk</asp:ListItem>
                            <asp:ListItem Value="+06:30,0">(+06:30) Yangon (Rangoon)</asp:ListItem>
                            <asp:ListItem Value="+07:00,1">(+07:00) Krasnoyarsk</asp:ListItem>
                            <asp:ListItem Value="+07:00,0">(+07:00) Bangkok, Hanoi, Jakarta</asp:ListItem>
                            <asp:ListItem Value="+08:00,0">(+08:00) Beijing, Hong Kong, Singapore, Taipei</asp:ListItem>
                            <asp:ListItem Value="+08:00,1">(+08:00) Irkutsk, Ulaan Bataar, Perth</asp:ListItem>
                            <asp:ListItem Value="+09:00,1">(+09:00) Yakutsk</asp:ListItem>
                            <asp:ListItem Value="+09:00,0">(+09:00) Seoul, Osaka, Sapporo, Tokyo</asp:ListItem>
                            <asp:ListItem Value="+09:30,0">(+09:30) Darwin</asp:ListItem>
                            <asp:ListItem Value="+09:30,1">(+09:30) Adelaide</asp:ListItem>
                            <asp:ListItem Value="+10:00,0">(+10:00) Brisbane, Guam, Port Moresby</asp:ListItem>
                            <asp:ListItem Value="+10:00,1">(+10:00) Canberra, Melbourne, Sydney, Hobart, Vladivostok</asp:ListItem>
                            <asp:ListItem Value="+11:00,0">(+11:00) Magadan, Solomon Is., New Caledonia</asp:ListItem>
                            <asp:ListItem Value="+12:00,1">(+12:00) Auckland, Wellington</asp:ListItem>
                            <asp:ListItem Value="+12:00,0">(+12:00) Fiji, Kamchatka, Marshall Is.</asp:ListItem>
                            <asp:ListItem Value="+13:00,0">(+13:00) Nuku'alofa</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="reqTimeZone" CssClass="lblRequired"
                            InitialValue=" " ControlToValidate="drpTimeZone" meta:resourcekey="lblReqTimeZoneResource1"
                            runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                </p>
              
                            
                <%--<p class="chk_txt"><input name="" type="checkbox" value="" /> I would like to receive periodic information about Insighto</p>--%>
                 <asp:CheckBox ID="chkInformation" runat="server" class="chk_txt" Checked="true" Font-Size="Small" 
                    Text="I would like to receive periodic information about Insighto" />
                    <p> <asp:Button ID="btnContinue" runat="server" class="btn_bg" 
                            Text="Create your first survey" onclick="btnContinue_Click" /></p>
               <%-- <p><input name="" type="button" value="Create your first survey" class="btn_bg" /></p>--%>
                <p>
                <asp:HiddenField ID="txtTimeZone" runat="server" />
                <asp:HiddenField ID="hdnLocation" runat="server" />
                </p>
            </div>
        </div>
  </div>
     <div class="clear">
        </div>
</div>
</body>
   <script type="text/javascript">
       function calculate_time_zone() {
           var rightNow = new Date();
           var jan1 = new Date(rightNow.getFullYear(), 0, 1, 0, 0, 0, 0);  // jan 1st
           var june1 = new Date(rightNow.getFullYear(), 6, 1, 0, 0, 0, 0); // june 1st
           var temp = jan1.toGMTString();
           var jan2 = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
           temp = june1.toGMTString();
           var june2 = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
           var std_time_offset = (jan1 - jan2) / (1000 * 60 * 60);
           var daylight_time_offset = (june1 - june2) / (1000 * 60 * 60);
           var dst;
           if (std_time_offset == daylight_time_offset) {
               dst = "0"; // daylight savings time is NOT observed
           } else {
               // positive is southern, negative is northern hemisphere
               var hemisphere = std_time_offset - daylight_time_offset;
               if (hemisphere >= 0)
                   std_time_offset = daylight_time_offset;
               dst = "1"; // daylight savings time is observed
           }
           var i;
           // check just to avoid error messages
           if (document.getElementById('ContentPlaceHolder1_drpTimeZone')) {
               for (i = 0; i < document.getElementById('ContentPlaceHolder1_drpTimeZone').options.length; i++) {
                   if (document.getElementById('ContentPlaceHolder1_drpTimeZone').options[i].value == convert(std_time_offset) + "," + dst) {
                       document.getElementById('ContentPlaceHolder1_drpTimeZone').selectedIndex = i;
                       //alert(convert(std_time_offset) + "," + dst);

                       document.getElementById('ContentPlaceHolder1_txtTimeZone').value = convert(std_time_offset).toString();
                       document.getElementById('ContentPlaceHolder1_hdnLocation').value = document.getElementById('ContentPlaceHolder1_drpTimeZone').options[i].text;
                       // alert(document.getElementById('ContentPlaceHolder1_txtTimeZone').value);

                       break;
                   }
               }
           }
       }

       function convert(value) {
           var hours = parseInt(value);
           value -= parseInt(value);
           value *= 60;

           var mins = parseInt(value);
           value -= parseInt(value);
           value *= 60;
           var secs = parseInt(value);
           var display_hours = hours;
           // handle GMT case (00:00)
           if (hours == 0) {
               display_hours = "00";
           } else if (hours > 0) {
               // add a plus sign and perhaps an extra 0
               display_hours = (hours < 10) ? "+0" + hours : "+" + hours;
           } else {
               // add an extra 0 if needed 
               display_hours = (hours > -10) ? "-0" + Math.abs(hours) : hours;
           }

           mins = (mins < 10) ? "0" + mins : mins;

           return display_hours + ":" + mins;
       }


       function changeTime() {
           if (document.getElementById('timezone').options[i].value == convert(std_time_offset) + "," + dst) {
               i = document.getElementById('timezone').selectedIndex;


               document.getElementById('ContentPlaceHolder1_txtTimeZone').value = convert(std_time_offset).toString();
               document.getElementById('ContentPlaceHolder1_hdnLocation').value = document.getElementById('timezone').options[i].text;
               alert(document.getElementById('ContentPlaceHolder1_txtTimeZone').value);

           }

       }
       onload = calculate_time_zone;
    </script>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-29026379-1']);
        _gaq.push(['_setDomainName', 'insighto.com']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script>



<!-- Google Code for Lead_signup Conversion Page --> 
<%--<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1027814833;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "dEWdCImw7gIQseuM6gM"; var google_conversion_value = 0; var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"  src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  src="//www.googleadservices.com/pagead/conversion/1027814833/?value=0&amp;label=dEWdCImw7gIQseuM6gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>--%>




</html>

</asp:Content>
