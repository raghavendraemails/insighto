﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Net;
using System.Web.Script.Serialization;
using System.Xml;
using System.Text;
using System.Security.Cryptography;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using CovalenseUtilities.Services;
using System.Configuration;
using Insighto.Business.ValueObjects;

public partial class linkedinshare : System.Web.UI.Page
{
    string twitterlink = ConfigurationManager.AppSettings["twitterurl"];
    string linkedinlink = ConfigurationManager.AppSettings["linkedinurl"];
    public string SAVE_PATH;
    public string struserid;
   // public int postuserid;
    protected void Page_Load(object sender, EventArgs e)
    {
        SAVE_PATH = ConfigurationSettings.AppSettings["downloadcharts"].ToString();

      
        // Passed back by LinkedIn
        string code = "";
       
        string inAuthDlgRedirect = "https://www.linkedin.com/uas/oauth2/authorization?response_type=code";
        string api_client_id = "8f7rrkzepihs";
        string state = Convert.ToBase64String(new System.Text.ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
        string redirect_uri = linkedinlink;
        string initial = "showpost";

        NameValueCollection pColl = Request.Params;

        

            for (int i = 0; i <= pColl.Count - 1; i++)
            {
                if (Session["initial"]== null)
                {
                    if (pColl.GetKey(i) == "error")
                    {
                        // Assume user has hit cancel key
                        // Close window
                        ClientScript.RegisterStartupScript(typeof(Page), "closePage", "<script type='text/JavaScript'>window.close();</script>");
                        return;
                    }

                    if (pColl.GetKey(i) == "initial")
                    {
                        initial = pColl.Get(i);
                        if (initial == "true")
                        {
                            Session["initial"] = "true";
                            string inRedirectURL = inAuthDlgRedirect + '&' + "client_id=" + api_client_id + '&' +
                                                    "state=" + state + '&' + "redirect_uri=" + redirect_uri;
                            Response.Redirect(inRedirectURL);
                            //  Page.Response.Redirect(inRedirectURL, true);
                        }
                    }
                    if (pColl.GetKey(i) == "code")
                        code = pColl.Get(i);
                    if (pColl.GetKey(i) == "state")
                        state = pColl.Get(i);
                   // Session["initial"] = "showpost";
                }
            }

        
        


        // Ready to show post message, check to make sure initial is not set to false
        if (initial == "showpost" && this.initial.Value=="")
        {
            if (code == "")
            {
                code = Request.QueryString["code"];
            }
            if (state == "")
            {
                state = Request.QueryString["state"];
            }
            // Ensure the state posted back is what we had sent to IN during authorization to prevent CSRF attack
            string accessToken = "https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code";
            accessToken = "https://www.linkedin.com/uas/oauth2/accessToken";
            string client_id = "8f7rrkzepihs";
            string client_secret = "Gzy3qaREiKeOTwAp";

            string accessTokenURL = accessToken + "&code=" + code + "&redirect_uri=" + redirect_uri +
                                    "&client_id=" + client_id + "&client_secret=" + client_secret;

            accessTokenURL = "grant_type=authorization_code&code=" + code + "&redirect_uri=" + redirect_uri +
                                    "&client_id=" + client_id + "&client_secret=" + client_secret;
            // Post to LinkedIn and get json data
            string postData = "";
            
            WebClient request = new WebClient();
            //request.Encoding = System.Text.Encoding.UTF8;
            //request.Headers["Content-Type"] = "application/json";
            request.Headers["Content-Type"] = "application/x-www-form-urlencoded";
            request.Headers["Host"] = "www.linkedin.com";
            //byte[] resp = request.UploadData(accessTokenURL, "POST", System.Text.Encoding.ASCII.GetBytes(postData));
            byte[] resp = request.UploadData(accessToken, "POST", System.Text.Encoding.ASCII.GetBytes(accessTokenURL));

            string response = System.Text.Encoding.ASCII.GetString(resp);

            // De-serialize json to get access token information
            var jss = new JavaScriptSerializer();
            Dictionary<string, string> sData = jss.Deserialize<Dictionary<string, string>>(response);
            string expires_in = sData["expires_in"].ToString();
            string access_token = sData["access_token"].ToString();
         
            string post = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                        "<share>" +
                        "<comment>Insighto has an amazing new free plan that puts other survey tools to shade. " +
                        "I just signed up&#33; Check this out &#8722; you will know what I am talking about. " +
                        "http://s.insighto.com/1gTlZ3X" + 
                        "</comment>" +
                        "<content>" +
                        "<title>Insighto&#8217;s new free plan offers a lot more than other online survey tools</title>" +
                        "<description>Insighto has launched a new free plan that now includes features that you " +
                        "otherwise pay for on other online survey tools.</description>" +
                        "<submitted-url>http://s.insighto.com/1gTlZ3X</submitted-url>" +
                        "<submitted-image-url>http://www.insighto.com/App_Themes/Classic/Images/insighto_logo.png</submitted-image-url>" +
                        "</content>" +
                        "<visibility>" +
                        "<code>anyone</code>" +
                        "</visibility>" +
                        "</share>";

            // Page.ClientScript.RegisterStartupScript(GetType(), "addScript", "ShowPost();", true);

            this.shareTxt.Value = post;
            this.AT.Value = access_token;
            this.initial.Value = "share";

            // Get User name
            string profile = "https://api.linkedin.com/v1/people/~:(first-name,last-name)?";
            string profile_url = profile + "oauth2_access_token=" + this.AT.Value;
            WebClient INrequest = new WebClient();
            Stream stream = request.OpenRead(profile_url);
            StreamReader sr = new StreamReader(stream);
            string INresponse = sr.ReadToEnd();
            XmlReader reader = XmlReader.Create(new StringReader(INresponse));
            reader.ReadToFollowing("first-name");
            string first_name = reader.ReadElementContentAsString();
            reader.ReadToFollowing("last-name");
            string last_name = reader.ReadElementContentAsString();

            this.INName.Value = first_name + ' ' + last_name;
            
            
            
            return;
        }

        
    }

    private static Object locker = new Object();
    public static void GenrateSendmailLog(string Content, string path)
    {
        lock (locker)
        {
            StreamWriter sw = null;
            try
            {
                if (File.Exists(path))
                {
                    FileStream file = new FileStream(path, FileMode.Append, FileAccess.Write);
                    sw = new StreamWriter(file);
                }
                else
                {
                    sw = File.CreateText(path);
                }
                sw.WriteLine(DateTime.Now.ToString() + " ***** " + Content.Trim());
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (sw != null) sw.Close();
            }
        }

    }

    protected void postShare(object sender, EventArgs e)
    {
        string profile = "";
        string profile_url = "";
        string response = "";
        string num_connections = "";
        string email_address = "";
        string shareURL = "";
        string error = "";
        string result = "success";
        string msg = "";

        HttpWebRequest hwr;// = new HttpWebRequest();
        if (Page.IsValid)
        {
            try
            {
                GenrateSendmailLog("poststarted" + DateTime.Now, SAVE_PATH + "LinkedInLog.txt");
                // Get User profile information for email and connection count
                profile = "https://api.linkedin.com/v1/people/~:(num-connections,email-address)?";
                profile_url = profile + "oauth2_access_token=" + this.AT.Value;
                WebClient request = new WebClient();
                Stream stream = request.OpenRead(profile_url);
                StreamReader sr = new StreamReader(stream);
                response = sr.ReadToEnd();

                XmlReader reader = XmlReader.Create(new StringReader(response));
                reader.ReadToFollowing("num-connections");
                num_connections = reader.ReadElementContentAsString();
                reader.ReadToFollowing("email-address");
                email_address = reader.ReadElementContentAsString();

                // Post to IN
                shareURL = "https://api.linkedin.com/v1/people/~/shares?oauth2_access_token=" + this.AT.Value;

            }
            catch (Exception e1)
            {
                error = e1.ToString() + "</br></br>";
            }
            try
            {
                // Instantiate a web request and populate the authorization header
                hwr = (HttpWebRequest)WebRequest.Create(@shareURL);

                GenrateSendmailLog("webrequest" + DateTime.Now, SAVE_PATH + "LinkedInLog.txt");

                // hwr.KeepAlive = false;
                hwr.ContentType = "text/xml;charset=UTF-8";
                hwr.ContentLength = this.shareTxt.Value.Length;
                hwr.Host = "api.linkedin.com";
                hwr.ProtocolVersion = HttpVersion.Version11;

                // POST off the request
                hwr.Method = "POST";
                Stream strm = hwr.GetRequestStream();
                byte[] bodyBytes = new System.Text.ASCIIEncoding().GetBytes(this.shareTxt.Value);
                strm.Write(bodyBytes, 0, bodyBytes.Length);
                strm.Flush();
                strm.Close();

                
                HttpWebResponse rsp = hwr.GetResponse() as HttpWebResponse;
                StreamReader resultStrm = new System.IO.StreamReader(rsp.GetResponseStream());
                string resultStr = resultStrm.ReadToEnd();
            }
            catch (WebException ex)
            {
                error += ex.ToString() + "</br></br>";
                // Post error
                result = "error";
                msg = "An error occured while posting to LinkedIn, please try again";
            }

            try
            {
                GenrateSendmailLog("beforeinserttodb" + DateTime.Now, SAVE_PATH + "LinkedInLog.txt");
                // Check for LinkedIn return data and update
                // Update user account to allow premium trial
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userInfo = sessionService.GetLoginUserDetailsSession();

                NetworkShareService NSS = new NetworkShareService();

                // SurveyCore scnw = new SurveyCore();
                // NetworkShareCodesLinkedin NSC = scnw.AddNetworkTrial(userInfo.UserId, "linkedIn", email_address, Convert.ToInt32(num_connections), 30);


                GenrateSendmailLog("beforeinserttodb", SAVE_PATH + "LinkedInLog.txt");


                NetworkShareCodes NSC = NSS.AddNetworkTrial(userInfo.UserId, "linkedIn", email_address, Convert.ToInt32(num_connections), 30);

                GenrateSendmailLog("afterinserttodb", SAVE_PATH + "LinkedInLog.txt");

                //  if (NSC == NetworkShareCodesLinkedin.Reshared)
                if (NSC == NetworkShareCodes.Reshared)
                {
                    msg = "You have already availed of our premium offer with your earlier LinkedIn share";
                    result = "errorcredit";
                }
                else if (NSC == NetworkShareCodes.Error)
                {
                    msg = "An error has occured, please try again.";
                    result = "error";
                }
                else
                {
                    msg = "<p>Your update is shared on LinkedIn successfully<br /><br /><h2>Congratulations!</h2><br />You have won yourself " +
                              "<b>30</b> days of Premium subscription.<br /><br />Details sent through email.</p>";
                }

                if (result == "error")
                    msg = msg + "Please try again.";
                // Post message to parent window success or retry if error
                Page.ClientScript.RegisterStartupScript(GetType(), "addScript", "ShowValue(\"linkedIn\", '" + result + "', '" + msg + "');", true);
                //return;
            }
            catch (Exception ex)
            {
                error += ex.ToString() + "</br></br>";
                msg = "Please ensure your cookies are enabled to allow us to get and save information from LinkedIn.";
                result = "error";
                //Response.Write(msg);
                //return;
                Page.ClientScript.RegisterStartupScript(GetType(), "addScript", "ShowValue(\"linkedIn\", '" + result + "', '" + msg + "');", true);
              //  ClientScript.RegisterStartupScript(typeof(Page), "closePage", "<script type='text/JavaScript'>window.close();</script>");
                //return;
            
            }
            
            lblerror.Text = error;
        }
    }
}