﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true"
    CodeFile="Logo.aspx.cs" Inherits="Logo"%>

<%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" runat="Server">
    <div class="surveyQuestionPanel">
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1" /></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" CssClass="errorMesg" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <!-- survey question panel -->
        <div class="surveyQuestionHeader">
            <div class="surveyQuestionTitle">
               <asp:Label ID="lblsurveyQuestiontitle" Text="Add Logo" runat="server" 
                    meta:resourcekey="lblsurveyQuestiontitleResource1"></asp:Label></div>
            <div class="previewPanel">
                <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
            </div>
        </div>
        <p class="defaultHeight"></p>
        <div class="informationPanelDefault" runat="server" id="dvinformationPanelDefault" >
               
            </div>
        <div class="surveyQuestionSubHeader">
            <!-- sub header -->
            <asp:CheckBox ID="chkSetDefault" Text="Set as default logo style"  
                runat="server" meta:resourcekey="chkSetDefaultResource1" />
           
            <!-- //sub header -->
             <asp:CheckBox ID="chkShowLogo" Text=" Show logo for respondents"  
                runat="server" meta:resourcekey="chkShowLogoResource1" />
          
        </div>
        <div class="surveyQuestionTabPanel">            
            <!-- survey question tabs -->
            <div class="con_login_pnl">
                <div class="loginlbl">
                    Select image to upload:
                    <div id="div_logo" runat="server" visible="false" style="color: Red">
                        Paid feature</div>
                </div>
                <div class="loginControls">
                    <asp:FileUpload ID="fuLogo" CssClass="textBoxMedium" runat="server" 
                        meta:resourcekey="fuLogoResource1" /><br />
                    <asp:Label ID="lblUploadNote" CssClass="note" 
                        Text="Please upload image of extension.jpg,.jpeg, .bmp, .png."   runat="server" 
                        meta:resourcekey="lblUploadNoteResource1"></asp:Label> 
                   
                    </div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <asp:RequiredFieldValidator SetFocusOnError="True" ID="rqEditorFooter" 
                        runat="server" ErrorMessage="Please select image"
                        CssClass="lblRequired" Display="Dynamic" ControlToValidate="fuLogo"
                        ValidationGroup="upload" meta:resourcekey="rqEditorFooterResource1"></asp:RequiredFieldValidator>
                    <asp:CustomValidator SetFocusOnError="True"  ID="cvLogUpload" runat="server" ClientValidationFunction="ValidateFileUpload"
                        ErrorMessage="Please upload image of extension.jpg,.jpeg, .bmp, .png."  
                        CssClass="lblRequired" Display="Dynamic" ControlToValidate="fuLogo"
                        ValidationGroup="upload" meta:resourcekey="cvLogUploadResource1"></asp:CustomValidator>
                </div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                </div>
                <div class="loginControls">
                    <asp:ImageButton ID="ibtnUpdalod" runat="server" Text="Upload" ToolTip="Upload" ImageUrl="App_Themes/Classic/Images/button_upload.gif"
                        ValidationGroup="upload" OnClick="ibtnUpdalod_Click" 
                        meta:resourcekey="ibtnUpdalodResource1" /></div>
            </div>
            <div class="con_login_pnl">
                <div class="loginlbl">
                    Enter Company Name:</div>
                <div class="loginControls">
                    <asp:TextBox ID="txtCompanyName" runat="server" CssClass="textBoxMedium" 
                        MaxLength="50" meta:resourcekey="txtCompanyNameResource1"></asp:TextBox></div>
            </div>
            <div class="con_login_pnl">
                <div class="reqlbl btmpadding">
                    <%--<asp:RequiredFieldValidator SetFocusOnError="true" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter company name."
                        CssClass="lblRequired" Display="Dynamic" ControlToValidate="txtCompanyName" Enabled="true"
                        ValidationGroup="company"></asp:RequiredFieldValidator>--%></div>
            </div>
            <div class="logoViewPanel">
                <p>
                    <asp:RadioButton ID="rbtnalignleft" runat="server" GroupName="rbalign" 
                        Checked="True" meta:resourcekey="rbtnalignleftResource1" />
                </p>
                <br />
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <img src="App_Themes/Classic/Images/sample_logo.gif" alt="logo" />
                        </td>
                        <td>
                         <asp:Label ID="lblLeftCompanyName" runat="server" Text="Company Name" 
                                meta:resourcekey="lblLeftCompanyNameResource1"></asp:Label>    
                        </td>
                    </tr>
                </table>
            </div>
            <div class="logoViewPanel">
                <p>
                    <asp:RadioButton ID="rbtnalignCenter" runat="server" GroupName="rbalign" 
                        meta:resourcekey="rbtnalignCenterResource1" /></p>
                <br />
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center">
                            <img src="App_Themes/Classic/Images/sample_logo.gif" alt="logo" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblCenterCompanyName" runat="server" Text="Company Name" 
                                meta:resourcekey="lblCenterCompanyNameResource1"></asp:Label>  
                        </td>
                    </tr>
                </table>
            </div>
            <div class="logoViewPanel">
                <p>
                    <asp:RadioButton ID="rbtnalignRight" runat="server" GroupName="rbalign" 
                        meta:resourcekey="rbtnalignRightResource1" /></p>
                <br />
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                          <asp:Label ID="lblRightCompanyName" runat="server" Text="Company Name" 
                                meta:resourcekey="lblRightCompanyNameResource1"></asp:Label>  
                        </td>
                        <td align="center">
                            <img src="App_Themes/Classic/Images/sample_logo.gif" alt="logo" />
                        </td>
                    </tr>
                </table>
            </div>
            <!-- //survey question tabs -->
        </div>
        <div class="clear">
        </div>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
            <div id="divLogoPreviews" runat="server" visible="false">
        <div class="surveyQuestionHeader">
            <!-- logo preview header -->
            <div class="surveyQuestionTitle">
                Logo Previews</div>
            <div class="previewPanel">
            <asp:LinkButton ID="btnRemoveLogo" runat="server" Text="Remove Logo" ToolTip="Remove Logo" OnClick="btnRemoveLogo_Click"></asp:LinkButton>
            </div>
            <!-- //logo preview header -->
        </div>
        <div class="logoOriginalSize">
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnSizeOriginal" Text="Original"  runat="server" 
                                        GroupName="rbsize" onclick="fnChangeLogo('original');" 
                                        meta:resourcekey="rbtnSizeOriginalResource1" />
                                 
                                </td>
                                <td>
                                    <asp:RadioButton ID="rbtnSizeLarge" Text="Large" runat="server" 
                                        GroupName="rbsize" onclick="fnChangeLogo('large');" 
                                        meta:resourcekey="rbtnSizeLargeResource1" />
                                   
                                </td>
                                <td>
                                    <asp:RadioButton ID="rbtnSizeMedium" Text="Medium"  runat="server" 
                                        GroupName="rbsize" onclick="fnChangeLogo('medium');" meta:resourcekey="rbtnSizeMediumResource1"
                                        />
                                   
                                </td>
                                <td>
                                    <asp:RadioButton ID="rbtnSizeSmall" Text="Small" runat="server" 
                                        GroupName="rbsize" onclick="fnChangeLogo('small');" 
                                        meta:resourcekey="rbtnSizeSmallResource1" />
                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divOriginal" style="display: none;" runat="server">
                            <asp:Image ID="imgOriginal" runat="server" alt="Size - 150 * 50" 
                                title="Size - 600 * 183" meta:resourcekey="imgOriginalResource1" /></div>
                        <div id="divLarge" style="display: none;" runat="server">
                            <asp:Image ID="imgLarge" runat="server" alt="Size - 300 * 100" 
                                title="Size - 300 * 100" meta:resourcekey="imgLargeResource1" /></div>
                        <div id="divMedium" style="display: none;" runat="server">
                            <asp:Image ID="imgMedium" runat="server" alt="Size - 200 * 80" 
                                title="Size - 200 * 80" meta:resourcekey="imgMediumResource1" /></div>
                        <div id="divSmall" style="display: none;" runat="server">
                            <asp:Image ID="imgSmall" runat="server" alt="Size - 150 * 50" 
                                title="Size - 150 * 50" meta:resourcekey="imgSmallResource1" /></div>
                    </td>
                </tr>
                <tr>
                <td>
                
                </td>
                </tr>
            </table>
        </div>
        </div>
        <div class="clear">
        </div>
        <div class="bottomButtonPanel">
            <asp:Button ID="btnSave" runat="server" Text="Save" ToolTip="Save" CssClass="dynamicButton"
                OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" />
        </div>
        <!-- //survey question panel -->
    </div>
    <script language="javascript" type="text/javascript">

        function fnChangeLogo(imgval) {
            if (imgval == "large") {
                document.getElementById('<%= divLarge.ClientID %>').style.display = 'block';
                document.getElementById('<%= divOriginal.ClientID %>').style.display = 'none';
                document.getElementById('<%= divMedium.ClientID %>').style.display = 'none';
                document.getElementById('<%= divSmall.ClientID %>').style.display = 'none';
            }
            else if (imgval == "original") {
                document.getElementById('<%= divLarge.ClientID %>').style.display = 'none';
                document.getElementById('<%= divOriginal.ClientID %>').style.display = 'block';
                document.getElementById('<%= divMedium.ClientID %>').style.display = 'none';
                document.getElementById('<%= divSmall.ClientID %>').style.display = 'none';
            }
            else if (imgval == "small") {
                document.getElementById('<%= divLarge.ClientID %>').style.display = 'none';
                document.getElementById('<%= divOriginal.ClientID %>').style.display = 'none';
                document.getElementById('<%= divMedium.ClientID %>').style.display = 'none';
                document.getElementById('<%= divSmall.ClientID %>').style.display = 'block';
            }
            else {
                document.getElementById('<%= divLarge.ClientID %>').style.display = 'none';
                document.getElementById('<%= divOriginal.ClientID %>').style.display = 'none';
                document.getElementById('<%= divMedium.ClientID %>').style.display = 'block';
                document.getElementById('<%= divSmall.ClientID %>').style.display = 'none';
            }

        }
        function ValidateFileUpload(Source, args) {
            var fuData = document.getElementById('<%= fuLogo.ClientID %>');
            var FileUploadPath = fuData.value;

            if (FileUploadPath == '') {
                // There is no file selected 
                args.IsValid = false;
            }
            else {
                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                if (Extension == "jpg" || Extension == "jpeg" || Extension == "png" || Extension == "bmp" || Extension == "JPG" || Extension == "JPEG" || Extension == "PNG" || Extension == "BMP") {
                    args.IsValid = true; // Valid file type
                }
                else {
                    args.IsValid = false; // Not valid file type
                }
            }
        }
    </script>
</asp:Content>
