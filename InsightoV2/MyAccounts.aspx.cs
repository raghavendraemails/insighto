﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Services;
using System.Web.Script.Serialization;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using CovalenseUtilities.Helpers;
using System.Collections;
using Insighto.Business.Helpers;
using System.Text;
using Insighto.Business.ValueObjects;
using App_Code;
using System.Data;

public partial class MyAccounts : BasePage
{
    Hashtable ht = new Hashtable();
    SurveyCore surcore = new SurveyCore();
    PollCreation Pc = new PollCreation();
    public string Headers
    {
        get
        {
            var headerBuilder = new StringBuilder();
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);

                if (ht != null && ht.Count > 0 && ht.Contains("surveyFlag"))
                {
                    hdnSurveyFlag.Value = ht["surveyFlag"].ToString();
                    if (ht["surveyFlag"].ToString() == "0")
                    {
                        headerBuilder.Append("['','Survey Name','','Status', 'Visits', 'Completed', 'Partial', 'Modified', 'Actions']");
                    }
                    else
                    {
                        headerBuilder.Append("['','Widget Name','', 'Status', 'Visits', 'Completed', 'Partial', 'Modified', 'Actions']");
                    }
                }
            }
            return headerBuilder.ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var loggedInUser = Session["UserInfo"] as LoggedInUserInfo;
        Session["defaultchart"] = null;
      
        if (loggedInUser.LicenseOpted == "SOCIALPREMIUM")
        {
            TimeSpan ts = Convert.ToDateTime(loggedInUser.ExpiryDate) - DateTime.UtcNow;
            if (IsLicenseExpiringSoon(loggedInUser))
            {
                NotificationControl.AddSuccessMessage(string.Format("You are currently subscribed to Insighto Premium. Expires on {0} ", Convert.ToDateTime
                   (loggedInUser.ExpiryDate).ToString("dd-MMM-yyyy")), false);
            }
            else
            {
                if(ts.Days <= 0)
                {
                    string msg = null;
                }
                else
                {
                    NotificationControl.AddSuccessMessage(string.Format("You are currently subscribed to Insighto Premium.", ""), false);
                }
                
            }
        }
        else
        {
            if (IsLicenseExpiringSoon(loggedInUser))
            {
                if (loggedInUser.ExpiryDate > DateTime.Now)
                {
                    NotificationControl.AddSuccessMessage(string.Format("Your subscription is coming to an end on {0}. Please renew immediately.", Convert.ToDateTime
                       (loggedInUser.ExpiryDate).ToString("dd-MMM-yyyy")), false);
                }
            }
        }

        Pc.UpdateUserLastActivity("survey", loggedInUser.UserId);

        DisplayNotification();

        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);

            if (ht != null && ht.Count > 0 && ht.Contains("surveyFlag"))
            {
                // hdnSurveyFlag.Value = base.SurveyFlag.ToString();
                hdnSurveyFlag.Value = ht["surveyFlag"].ToString();
                if (ht["surveyFlag"].ToString() == "0")
                {
                    spnHeading.InnerHtml = "CREATED SURVEYS";
                  //  lnkCreate.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyBasicsURL(), "surveyFlag=" + SurveyFlag);
                    //lnkCreate.InnerText = "Create your first survey";
                }
                else if (ht["surveyFlag"].ToString() == "1")
                {
                    spnHeading.InnerHtml = "CREATED WIDGETS";
                 //   lnkCreate.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetWidgetBasicsURL(), "surveyFlag=" + SurveyFlag);
                    //lnkCreate.InnerText = "Create your first widget";
                }
            }
        }
        DataSet dsppscredits = surcore.getCreditsCount(loggedInUser.UserId);

        if (loggedInUser.LicenseType == "PREMIUM_YEARLY")
        {
            lblcredits.Visible = false;
            tolltip.Visible = false;
        }
        else
        {
            lblcredits.Text = "&nbsp;&nbsp;<img src='App_Themes/Classic/Images/Free_Pic1.png' title= 'Basic' width='17px' height='16px' />&nbsp; Free - Unlimited &nbsp;&nbsp;" + " <img src='App_Themes/Classic/Images/Pro_Pic1.png' title= 'Pro-Single' width='17px' height='16px' />&nbsp; Pro - " + dsppscredits.Tables[0].Rows.Count.ToString() + " &nbsp;&nbsp;" + " <img src='App_Themes/Classic/Images/Prem_Pic1.png' title= 'Premium-Single' width='17px' height='16px' />&nbsp; Premium - " + dsppscredits.Tables[1].Rows.Count.ToString() + "&nbsp;&nbsp;";
        }

        lnkCreate.HRef = lnkCreate.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyBasicsURL(), "surveyFlag=" + SurveyFlag);
    }

    /// <summary>
    /// Determines whether [is license expiring soon] [the specified logged in user].
    /// </summary>
    /// <param name="loggedInUser">The logged in user.</param>
    /// <returns>
    ///   <c>true</c> if [is license expiring soon] [the specified logged in user]; otherwise, <c>false</c>.
    /// </returns>
    private bool IsLicenseExpiringSoon(LoggedInUserInfo loggedInUser)
    {
        if (loggedInUser == null)
            return false;

        TimeSpan ts = Convert.ToDateTime(loggedInUser.ExpiryDate) - DateTime.UtcNow;
        return Utilities.GetUserType() != UserType.FREE && ts.Days <= 15;
    }

    private void DisplayNotification()
    {
        var configService = ServiceFactory.GetService<ConfigService>();
        var configValue = configService.GetConfigurationValueByDate(DateTime.Now, "SERVER_MAINTAINANCE");
        if (configValue.Length > 0)
        NotificationControl.AddInfoMessage(configValue,false,false);
    }
}