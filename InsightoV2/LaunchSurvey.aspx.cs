﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using App_Code;
using System.Data;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Media;
using System.Web.UI.HtmlControls;
using FeatureTypes;
using BitlyDotNET.Interfaces;
using BitlyDotNET.Implementations;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace Insighto.Pages
{
    public partial class LaunchSurvey : SurveyPageBase
    {
        string countryName = "";
        string surveyLaunchURL = "";
        string surveyLaunchURL1 = "";
        int surveyURLtype = 1;
        int launch_value = 0;
        Hashtable ht = new Hashtable();
        int contactListId = 0;
        int EmailLimit = 0;
        string _surveyLaunchUrl = "";
        string introText = Utilities.ResourceMessage("introText"); //"<p>Dear $$FIRST_NAME$$  $$LAST_NAME$$</p><p>We are conducting a survey to evaluate / assess / seek your opinion on _________________.</p><p>We cordially invite you to participate in this survey and give your valuable inputs.</p><p>This will only take a few minutes of your precious time. Your inputs are very important and would be highly appreciated.</p><p>Please do not forward this email to any other person as your email id is uniquely tied to this survey link.</p><p>Thank you in advance for your participation and your inputs.</p><p>Please click on the survey link given below to start your survey.<br /></p><p><i>(Insighto will pick the survey URL automatically)</i></p>";
        string navresurlstr = "";
        public string procredits = "";
        public string premiumcredits = "";
        [Flags]
        enum LaunchMethod
        {
            Emailthr = 1,
            Webthr = 2,
            Embedthr = 4,
            Fbthr = 8,
            Twthr = 16,
            Linthr = 32
        }
        int surveyId = 0;
        string serveraudiofilepath = ConfigurationManager.AppSettings["serveraudiofilepath"].ToString();
        string strUrl = ConfigurationManager.AppSettings["RootURL"].ToString();
        Hashtable typeHt = new Hashtable();
        SurveyCore surcore = new SurveyCore();

        public string applycreditval;

        private MailMessage mailMessage = null;
        private SmtpClient smtpMail = null;

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            string strDisAbleBackButton;
            strDisAbleBackButton = "<script language='javascript'>\n";
            strDisAbleBackButton += "window.history.forward(1);\n";
            strDisAbleBackButton += "\n</script>";
            ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "clientScript", strDisAbleBackButton);
        }
        DataSet dsppscredits;

        protected void Page_Load(object sender, EventArgs e)
        {

            string ipAddress = "";

            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            //ipAddress = "24.236.224.131";

            countryName = Utilities.GetCountryName(ipAddress);
            ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(countryName);

            countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();


            if (countryName.ToUpper() != "INDIA")
            {

                buyprolnk.Text = "Buy - $9/Survey";
                buypremlnk.Text = "Buy - $29/Survey";
               

            }
            else
            {

                buyprolnk.Text = "Buy - " + "&#8377;" + "399" + "/Survey";
                buypremlnk.Text = "Buy - " + "&#8377;" + "1,299" + "/Survey";
               


            }


            applycreditval = hdfreecheck.Value;
            
            iframealert.Visible = false;
            web.Style.Add("display", "none");
            Embed.Style.Add("display", "none");

            lblSurveyLable.Visible = false;
             divSurveyLable.Style.Add("display", "none");
             hdnaccess.Value = "1";
             hdnlayout.Value = "1";
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
                hdnSurveyId.Value = surveyId.ToString();
                bindgrdSurvey();
            }
            int launchtypenumber = surcore.getLaunchTypeNumber(surveyId);
            LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
            if ((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr)
            {
                    lblWebLaunchAlready.Visible = true;
                    lnkSurveyUrl.Visible = true;
                    lnkWeblaunchother.Style.Add("display", "none");
                    btnLaunchWeb1.Style.Add("display", "none");
                    string surveyurl = surcore.getSurveyUrl(surveyId);
            }
            else if (((LaunchMethod.Emailthr & launchnew) == LaunchMethod.Emailthr) || ((LaunchMethod.Embedthr & launchnew) == LaunchMethod.Embedthr) || ((LaunchMethod.Fbthr & launchnew) == LaunchMethod.Fbthr) || ((LaunchMethod.Twthr & launchnew) == LaunchMethod.Twthr))
            {
                btnLaunchWeb1.Style.Add("display", "none");
                lnkWeblaunchother.Style.Add("display", "block");
            }
            else
            {
                lnkWeblaunchother.Style.Add("display", "none");
                btnLaunchWeb1.Style.Add("display", "block");
            }
            if ((LaunchMethod.Embedthr & launchnew) == LaunchMethod.Embedthr)
            {
                lblEmbedLaunchedAlready.Visible = true;
                btnLaunchEmbed.Visible = false;
                lnkEmbedCode.Visible = true;
            }
            if (((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr) && ((LaunchMethod.Embedthr & launchnew) == LaunchMethod.Embedthr) && ((LaunchMethod.Fbthr & launchnew) == LaunchMethod.Fbthr) && ((LaunchMethod.Twthr & launchnew)==LaunchMethod.Twthr))
            {
                rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "show");
            }
            
            if (((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr) || ((LaunchMethod.Fbthr & launchnew) == LaunchMethod.Fbthr) || ((LaunchMethod.Embedthr & launchnew) == LaunchMethod.Embedthr) || ((LaunchMethod.Emailthr & launchnew) == LaunchMethod.Emailthr) || ((LaunchMethod.Twthr & launchnew) == LaunchMethod.Twthr))
            {
                prelabtn.Style.Add("display", "none");
                mysurvebtn.Style.Add("display", "block");
            }
            else
            {
                prelabtn.Style.Add("display", "block");
                mysurvebtn.Style.Add("display", "none");
            }

            DataSet dsintrocnt = surcore.getSurveyType(base.SurveyID);
          // if (base.SurveyBasicInfoView.SURVEY_INTRO == null || base.SurveyBasicInfoView.SURVEY_INTRO.ToString().Length <= 0)  
            if (Convert.ToInt32(dsintrocnt.Tables[3].Rows[0][0].ToString()) == 0)
            {
                Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetAddIntroductionURL(), "SurveyId=" + base.SurveyID + "&Flag=AddIntro&surveyFlag=" + base.SurveyFlag));
            }
            else
            {
                if (!IsPostBack)
                {
                    SetPremiumPermissions();
                    rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "hidden");
                    internalmailingSystem.Style.Add("display", "none");
                    EditorQuesIntroParams.Value = base.SurveyBasicInfoView.SURVEY_FONT_TYPE + "_" + BuildQuestions.GetFontSizeValue(base.SurveyBasicInfoView.SURVEY_FONT_SIZE) + "_" + BuildQuestions.GetFontColor(base.SurveyBasicInfoView.SURVEY_FONT_COLOR);
                    if (base.SurveyFlag == 1)
                    {
                        rbtnWidgetLaunch.Visible = true;
                        webLinkSystem.Style.Add("display", "block");
                    }
                    else
                    {
                        rbtnWidgetLaunch.Visible = false;
                    }

                    var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
                    BindContactLists(userDetails.UserId, base.SurveyBasicInfoView.SURVEY_ID);
                    if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0)
                    {
                        string strInsightoSurveyLink = EncryptHelper.EncryptQuerystring(PathHelper.GetInsightoSurveyLinkURL(), Constants.SURVEYID + "=" + SurveyID + "&surveyFlag=" + SurveyFlag);
                        hlnkInsightoSurveyLink.NavigateUrl = strInsightoSurveyLink;


                        Upgradelnk.NavigateUrl = "#";
                        applyprolnk.NavigateUrl = "#";
                        buyprolnk.NavigateUrl = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userDetails.UserId + "&FromPartner=No" + "&SurveyId=" + SurveyID + "&status=" + SurveyBasicInfoView.STATUS);
                        applypremlnk.NavigateUrl = "#";
                        buypremlnk.NavigateUrl = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userDetails.UserId + "&FromPartner=No" + "&SurveyId=" + SurveyID + "&status=" + SurveyBasicInfoView.STATUS);
                    }
                    if (base.SurveyBasicInfoView != null)
                    {
                        PopulateSurveyOptions(base.SurveyBasicInfoView);
                    }
                    PopulateFeatures();
                    if (Request.QueryString["key"] != null)
                    {
                        ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);

                        if (ht != null && ht.Count > 0 && ht.Contains("ContID"))
                        {
                            contactListId = Convert.ToInt32(ht["ContID"]);
                            internalmailingSystem.Visible = true;
                            internalmailingSystem.Style.Add("display", "block");
                            rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "show");
                            if (contactListId > 0)
                                if (ddlEmailList.Items.FindByValue(contactListId.ToString()) != null)
                                {
                                    ddlEmailList.SelectedValue = contactListId.ToString();

                                }
                                else
                                {
                                    ddlEmailList.SelectedIndex = 0;
                                }
                            bindSurveyType();
                        }
                        if (ht != null && ht.Count > 0 && ht.Contains("Flag"))
                        {
                            DisableControls();

                        }
                        else
                        {
                            EnableControls();
                        }
                        if (ht != null && ht.Count > 0 && ht.Contains("Page"))
                        {
                            if (ht["Page"].ToString() == "PEI" || ht["Page"].ToString() == "PSEL")
                            {
                                btnBack.Visible = true;

                            }
                            else
                            {
                                btnBack.Visible = false;
                            }

                        }
                        else
                        {
                            btnBack.Visible = false;
                        }

                    }
                    rbtnLayout.SelectedValue = Convert.ToString(base.SurveyBasicInfoView.SURVEY_LAYOUT);
                }

                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userInfo = sessionService.GetLoginUserDetailsSession();
                var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
                var listFeatures = ServiceFactory.GetService<FeatureService>();

                UserType userType = 0;
                DataSet dssurveytype = surcore.getSurveyType(surveyId);

                if (dssurveytype.Tables[0].Rows[0][1].ToString() != "")
                {
                    userType = (UserType)Enum.Parse(typeof(UserType), dssurveytype.Tables[0].Rows[0][1].ToString());
                }
                else
                {
                    userType = Utilities.GetUserType();
                }

              //  UserType userType = Utilities.GetUserType();
                if (userInfo.LicenseType == "PREMIUM_YEARLY")
                {
                    divcredits.Visible = false;
                }

                EmailLimit = ValidationHelper.GetInteger(Utilities.GetFeatureValue(userType, (int)FeatureTypes.EmailCount.EmailLimit), 0);
                if (listFeatures.Find(licenceType, FeatureDeploymentType.UNIQUE_RESPONDENT_PASSWORD).Value == 0)
                {
                    // rbtnInternakAccessSetting.Items.FindByValue("2").Attributes.Add("onclick", "javascript:return restricOptionSelection();");
                }
                hdnIntroText.Value = base.SurveyBasicInfoView.SURVEY_INTRO;
                BindResourceMessages();

                dsppscredits = surcore.getCreditsCount(userInfo.UserId);

                if (dsppscredits.Tables[0].Rows.Count > 0)
                {
                    lblprocredits.Text = "(Available Credits: " + dsppscredits.Tables[0].Rows.Count.ToString() + ")";
                }
                else
                {
                    lblprocredits.Text = "(Available Credits: 0)";
                }

                if (dsppscredits.Tables[1].Rows.Count > 0)
                {
                    lblpremcredits.Text = "(Available Credits: " + dsppscredits.Tables[1].Rows.Count.ToString() + ")";
                }
                else
                {
                    lblpremcredits.Text = "(Available Credits: 0)";
                }



                DataSet dsppstype = surcore.getSurveyType(base.SurveyID);

                string launchcnt = dsppstype.Tables[2].Rows[0][0].ToString();
                int launchcountval = Convert.ToInt32(launchcnt);

                if (applycreditval == "")
                {
                    applycreditval = dsppstype.Tables[0].Rows[0][0].ToString();
                }
            

                string srvystatus = dsppstype.Tables[0].Rows[0][3].ToString();

                if ((applycreditval == "") || (applycreditval == "FREE"))
                {
                    imgchkfree.Style["visibility"] = "visible";
                    imgchkpro.Style["visibility"] = "hidden";
                    imgchkprem.Style["visibility"] = "hidden";
                    Upgradelnk.Enabled = false;
                    Upgradelnk.Text = "Applied";
                    applyprolnk.Enabled = true;
                    applyprolnk.Text = "Apply";
                    applypremlnk.Enabled = true;
                    applypremlnk.Text = "Apply";
                }
                else if (applycreditval == "PPS_PRO")
                {
                    imgchkpro.Style["visibility"] = "visible";
                    imgchkfree.Style["visibility"] = "hidden";
                    imgchkprem.Style["visibility"] = "hidden";

                    if (srvystatus != "Active")
                    {
                        Upgradelnk.Style["visibility"] = "visible";
                        Upgradelnk.Enabled = true;
                        Upgradelnk.Text = "Apply";
                    }
                    else
                    {
                        Upgradelnk.Style["visibility"] = "hidden";
                    }


                    applyprolnk.Enabled = false;
                    applyprolnk.Text = "Applied";
                    applypremlnk.Enabled = true;
                    applypremlnk.Text = "Apply";
                }
                else if (applycreditval == "PPS_PREMIUM")
                {
                    imgchkprem.Style["visibility"] = "visible";
                    imgchkfree.Style["visibility"] = "hidden";
                    imgchkpro.Style["visibility"] = "hidden";


                    applypremlnk.Enabled = false;
                    applypremlnk.Text = "Applied";

                    if (srvystatus != "Active")
                    {
                        Upgradelnk.Style["visibility"] = "visible";
                        Upgradelnk.Enabled = true;
                        Upgradelnk.Text = "Apply";

                        applyprolnk.Style["visibility"] = "visible";
                        applyprolnk.Enabled = true;
                        applyprolnk.Text = "Apply";
                    }
                    else
                    {
                        Upgradelnk.Style["visibility"] = "hidden";
                        applyprolnk.Style["visibility"] = "hidden";
                        buyprolnk.Style["visibility"] = "hidden";
                        tdprosingle.Visible = false;
                        lblprocredits.Visible = false;
                    }
                }

                //if ((applycreditval == "PPS_PRO") && (dsppscredits.Tables[0].Rows.Count > 0))
                //{
                //    DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", srvystatus);
                //}
                //else if ((applycreditval == "PPS_PREMIUM") && (dsppscredits.Tables[1].Rows.Count > 0))
                //{
                //    DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", srvystatus);
                //}
                //else if (applycreditval == "FREE")
                //{
                //    DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "FREE", base.SurveyBasicInfoView.SURVEY_ID, "FREE", srvystatus);
                //}

                if (!IsPostBack)
                {
                    if (dsppstype.Tables[0].Rows[0][0].ToString() == null || dsppstype.Tables[0].Rows[0][0].ToString() == "FREE" || dsppstype.Tables[0].Rows[0][0].ToString() == "BASIC" || dsppstype.Tables[0].Rows[0][0].ToString() == "")
                    {

                        tdfreemsg.Visible = true;
                        Upgradelnk.Visible = true;
                        tdprosingle.Visible = true;
                        lblprocredits.Visible = true;

                        if (dsppscredits.Tables[0].Rows.Count > 0)
                        {
                            applyprolnk.Visible = true;
                            buyprolnk.Visible = false;
                        }
                        else
                        {
                            if (applycreditval == "PPS_PRO")
                            {
                                applyprolnk.Visible = true;
                                buyprolnk.Visible = false;
                            }
                            else
                            {
                                applyprolnk.Visible = false;
                                buyprolnk.Visible = true;
                            }
                        }

                        tdpremsignle.Visible = true;
                        lblpremcredits.Visible = true;

                        if (dsppscredits.Tables[1].Rows.Count > 0)
                        {
                            applypremlnk.Visible = true;
                            buypremlnk.Visible = false;
                        }
                        else
                        {
                            if (applycreditval == "PPS_PREMIUM")
                            {
                                applypremlnk.Visible = true;
                                buypremlnk.Visible = false;
                            }
                            else
                            {
                                applypremlnk.Visible = false;
                                buypremlnk.Visible = true;
                            }

                        }
                    }
                    else if (dsppstype.Tables[0].Rows[0][0].ToString() == "PPS_PRO")
                    {
                        tblcrediturvey.Visible = true;

                        if (srvystatus != "Active")
                        {
                            tdfreemsg.Visible = true;
                        }
                        else
                        {
                            tdfreemsg.Visible = false;
                        }
    
                        imgchkpro.Style["visibility"] = "visible";
                        imgchkfree.Style["visibility"] = "hidden";
                        imgchkprem.Style["visibility"] = "hidden";

                        if (lblpremcredits.Text == "(Available Credits: 0)")
                        {
                            buypremlnk.Visible = true;
                            applypremlnk.Visible = false;
                            imgchkprem.Style["visibility"] = "hidden";
                        }

                        if (dsppscredits.Tables[0].Rows.Count > 0)
                        {
                            applyprolnk.Visible = true;
                            buyprolnk.Visible = false;
                        }
                        else
                        {
                            if (applycreditval == "PPS_PRO")
                            {
                                applyprolnk.Visible = true;
                                buyprolnk.Visible = false;
                            }
                            else
                            {
                                applyprolnk.Visible = false;
                                buyprolnk.Visible = true;
                            }
                        }


                        if (dsppscredits.Tables[1].Rows.Count > 0)
                        {
                            applypremlnk.Visible = true;
                            buypremlnk.Visible = false;
                        }
                        else
                        {
                            if (applycreditval == "PPS_PREMIUM")
                            {
                                applypremlnk.Visible = true;
                                buypremlnk.Visible = false;
                            }
                            else
                            {
                                applypremlnk.Visible = false;
                                buypremlnk.Visible = true;
                            }
                        }


                    }
                    else if (dsppstype.Tables[0].Rows[0][0].ToString() == "PPS_PREMIUM")
                    {
                        tblcrediturvey.Visible = true;

                        if (srvystatus != "Active")
                        {
                            tdfreemsg.Visible = true;
                        }
                        else
                        {
                            tdfreemsg.Visible = false;
                        }
                        imgchkpro.Style["visibility"] = "hidden";
                        imgchkfree.Style["visibility"] = "hidden";
                        imgchkprem.Style["visibility"] = "visible";
                        if (lblprocredits.Text == "(Available Credits: 0)")
                        {
                            if (srvystatus != "Active")
                            {
                                buyprolnk.Visible = true;
                            }
                            else
                            {
                                buyprolnk.Visible = false;
                            }
                            applyprolnk.Visible = false;
                            imgchkpro.Style["visibility"] = "hidden";
                        }
                        if (dsppscredits.Tables[0].Rows.Count > 0)
                        {
                            if (srvystatus != "Active")
                            {
                                applyprolnk.Visible = true;
                            }
                            else
                            {
                                applyprolnk.Visible = false;
                            }
                            buyprolnk.Visible = false;
                        }
                        else
                        {
                            if (applycreditval == "PPS_PRO")
                            {

                                if (srvystatus != "Active")
                                {
                                    applyprolnk.Visible = true;
                                }
                                else
                                {
                                    applyprolnk.Visible = false;
                                }
                                buyprolnk.Visible = false;
                            }
                            else
                            {
                                applyprolnk.Visible = false;
                                if (srvystatus != "Active")
                                {
                                    buyprolnk.Visible = true;
                                }
                                else
                                {
                                    buyprolnk.Visible = false;
                                }
                            }
                        }


                        if (dsppscredits.Tables[1].Rows.Count > 0)
                        {
                            applypremlnk.Visible = true;
                            buypremlnk.Visible = false;
                        }
                        else
                        {
                            if (applycreditval == "PPS_PREMIUM")
                            {
                                applypremlnk.Visible = true;
                                buypremlnk.Visible = false;
                            }
                            else
                            {
                                applypremlnk.Visible = false;
                                buypremlnk.Visible = true;
                            }
                        }

                    }
                }
                if (hdnactivation.Value == "true")
                {
                   // SendEmail(userInfo.LoginName, userInfo.UserId, userInfo.FirstName);

                    DataSet dsauth = surcore.updatelaunchactivationemail(userInfo.UserId);

                    lblSuccMsg.Text = "An activation email has been resent to your registered email id.";
                    dvSuccessMsg.Visible = true;
                }

            }
            lblSurveyNameConfirm.Text = SurveyBasicInfoView.SURVEY_NAME;
            lblSurveyTwName.Text=SurveyBasicInfoView.SURVEY_NAME;
            //lblSurveyLnName.Text = SurveyBasicInfoView.SURVEY_NAME;
            if (!IsPostBack)
            {
                if (base.SurveyBasicInfoView.SURVEY_LOGO == null || base.SurveyBasicInfoView.SURVEY_LOGO == "")
                {
                    imagechk.Checked = false;
                }
                else
                {
                    imagechk.Checked = true;
                }
            }
            if (base.SurveyBasicInfoView.SURVEY_LOGO != null || base.SurveyBasicInfoView.SURVEY_LOGO != "")
            {
                surcore.Logochecking(imagechk.Checked, base.SurveyBasicInfoView.SURVEY_ID);

            }
        }

        private void BindResourceMessages()
        {
            btnLaunchLater.Text = String.Format(Utilities.ResourceMessage("btnLaunchLaterResource1.Text"), SurveyMode);
            lblExternalNote.Text = String.Format(Utilities.ResourceMessage("lblExternalNoteResource1.Text"), SurveyMode.ToLower());
            lblInternalNote.Text = String.Format(Utilities.ResourceMessage("lblInternalNoteResource1.Text"), SurveyMode.ToLower());
            lblTitle.Text = String.Format(Utilities.ResourceMessage("lblTitleResource1.Text"), SurveyMode.ToUpper());
            lblReadyLaunch.Text = String.Format(Utilities.ResourceMessage("lblReadyLaunchResource1.Text"), SurveyMode.ToLower());
            lblLayoutSettings.Text = String.Format(Utilities.ResourceMessage("lblLayoutSettingsResource1.Text"), SurveyMode);
            lblSurveyQ1.Text = String.Format(Utilities.ResourceMessage("lblSurveyQ1Resource1.Text"), SurveyMode.ToLower());
            lblConfirmAccessSettings.Text = String.Format(Utilities.ResourceMessage("lblConfirmAccessSettings2Resource1.Text"), SurveyMode.ToLower());
        }
        
        /// <summary>
        /// Sets the premium permissions.
        /// </summary>

        private void SetPremiumPermissions()
        {
            UserType userType = Utilities.GetUserType();
            bool showTwoColumn = Utilities.ShowOrHideFeature(userType, (int)Deployment.Two_Column_Layout);

            List<ListItem> lstItems = new List<ListItem>();
            lstItems.Add(new ListItem { Text = "Single Column Layout", Value = "1", Selected = true });
            lstItems.Add(new ListItem { Text = "Two Column Layout", Value = "2" });
            if (showTwoColumn)
            {

                Utilities.FillRadioButtonList<ListItem>(rbtnLayout, lstItems, "Text", "Value");
            }
            else
            {
                Utilities.FillRadioButtonList<ListItem>(rbtnLayout, lstItems, "Text", "Value");
            }
        }

        public bool CheckForBlockedWords(string questionStr)
        {
            // Srini, 10/24/2013, Moved to Insighto Helper for reuse
            UrlQueryStringHelper uQStrHelper = new UrlQueryStringHelper();
            return uQStrHelper.BlockedWordInQuestionStr(questionStr);
        }

        public string ContentParser(string Contnt, Hashtable ht)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Contnt);
            if (ht.Count > 0 && Contnt.Trim().Length > 0)
            {
                foreach (string key in ht.Keys)
                {
                    while (sb.ToString().Contains(key))
                    {
                        sb.Replace(key, Convert.ToString(ht[key]));
                    }
                }
            }
            return sb.ToString();
        }

        public Hashtable GetGeneralLinks(Hashtable ht, int userid)
        {

            string urlstr_contactus = EncryptHelper.EncryptQuerystring(strUrl + "/In/contactus.aspx", "UserId=" + userid + "&Qugm=1");
            string faqsurl = string.Format("{0}{1}", strUrl, "/Faqcms/CNS_AboutSurvey.aspx?key=O1IiHYYZiuqe67HGRNzwF7j%2btdpCU6IQFvQJRPSFZFVdz1jysLODHqPiBlwB3os07lCVCHakwMBdvY2JVZRBS7CpbynHlOEZ");
            ht.Add("$$FEEDBACKURL$$", urlstr_contactus);
            ht.Add("$$FAQURL$$", faqsurl);
            return ht;
        }
        //protected void SendEmail(string loginname, int userid, string name)
        //{
        //    try
        //    {
        //        DataSet dsauth = surcore.getemailAuthentication();

        //        string urlstr = EncryptHelper.EncryptQuerystring(strUrl + "/Home.aspx", "UserId=" + userid);
        //        string homeUrl = string.Format("{0}{1}", strUrl, "/Home.aspx");

        //        string loginurl = EncryptHelper.EncryptQuerystring(strUrl + "/Login.aspx", "UserId=" + userid);


        //        string OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + loginname);

        //        string unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";


        //        WelcomeUser("WelcomeEmail", name, loginname, loginurl, unsubscribe);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //public void WelcomeUser(string templatename, string username, string EmailID, string stractivationlink, string unsubscribeurl)
        //{

        //    string viewData1;
        //    JavaScriptSerializer js1 = new JavaScriptSerializer();
        //    js1.MaxJsonLength = int.MaxValue;
        //    clsjsonmandrill prmjson1 = new clsjsonmandrill();
        //    //  var prmjson1 = new jsonmandrill();
        //    prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
        //    prmjson1.name = templatename;
        //    viewData1 = js1.Serialize(prmjson1);

        //    string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";


        //    WebClient request1 = new WebClient();
        //    request1.Encoding = System.Text.Encoding.UTF8;
        //    request1.Headers["Content-Type"] = "application/json";
        //    byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));

        //    string response1 = System.Text.Encoding.ASCII.GetString(resp1);


        //    clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);

        //    string bodycontent = jsmandrill11.code;


        //    string viewData;
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    js.MaxJsonLength = int.MaxValue;
        //    var prmjson = new clsjsonmandrill.jsonmandrillmerge();
        //    prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
        //    prmjson.template_name = templatename;
        //    List<clsjsonmandrill.template_content> mtags = new List<clsjsonmandrill.template_content>();
        //    mtags.Add(new clsjsonmandrill.template_content { name = "std_content00", content = "<div> Dear *|FNAME|*,</div>" });
        //    mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div><a href='*|ACTIVATION_LINK|*' style='font-size:14px;'><img src='http://insighto.com/images/BtnCnfmEmailAddYellow.png' alt=''/></a><br/></div>" });
        //    mtags.Add(new clsjsonmandrill.template_content { name = "std_content02", content = "<div><p style='font-size:8pt;font-family:Arial,Sans-Serif'> *|UNSUBSCRIBEINSIGHTO|* </p></div>" });

        //    List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
        //    mvars.Add(new clsjsonmandrill.merge_vars { name = "FNAME", content = username });
        //    mvars.Add(new clsjsonmandrill.merge_vars { name = "ACTIVATION_LINK", content = stractivationlink });
        //    mvars.Add(new clsjsonmandrill.merge_vars { name = "UNSUBSCRIBEINSIGHTO", content = unsubscribeurl });
        //    prmjson.template_content = mtags;
        //    prmjson.merge_vars = mvars;


        //    viewData = js.Serialize(prmjson);


        //    string url = "https://mandrillapp.com/api/1.0/templates/render.json";


        //    WebClient request = new WebClient();
        //    request.Encoding = System.Text.Encoding.UTF8;
        //    request.Headers["Content-Type"] = "application/json";
        //    byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

        //    string response = System.Text.Encoding.ASCII.GetString(resp);

        //    string unesc = System.Text.RegularExpressions.Regex.Unescape(response);

        //    string first4 = unesc.Substring(0, 9);

        //    string last2 = unesc.Substring(unesc.Length - 2, 2);


        //    string unesc1 = unesc.Replace(first4, "");
        //    string unescf = unesc1.Replace(last2, "");


        //    clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);

        //    //   string bodycontent = jsmandrill1.code;


        //    mailMessage = new MailMessage();
        //    mailMessage.To.Clear();
        //    mailMessage.Sender = new MailAddress("getstarted@insighto.com", "Insighto.com");
        //    mailMessage.From = new MailAddress("getstarted@insighto.com", "Insighto.com");

        //    mailMessage.Subject = "[Important - from Insighto.com] - Please confirm your email address";

        //    mailMessage.To.Add(EmailID);
        //    mailMessage.Body = unescf;

        //    mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
        //    mailMessage.IsBodyHtml = true;
        //    mailMessage.Priority = MailPriority.Normal;
        //    smtpMail = new SmtpClient();
        //    smtpMail.Host = "smtp.mandrillapp.com";
        //    smtpMail.Port = 587;
        //    smtpMail.EnableSsl = true;
        //    smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
        //    smtpMail.Credentials = new System.Net.NetworkCredential("ram@knowience.com", "T74TJc3EZPYaIcvdBg--Dg");

        //    try
        //    {

        //        smtpMail.Send(mailMessage);
        //    }
        //    catch (SmtpFailedRecipientsException ex)
        //    {
        //        for (int i = 0; i < ex.InnerExceptions.Length; i++)
        //        {
        //            SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
        //            if (status == SmtpStatusCode.MailboxBusy ||
        //                status == SmtpStatusCode.MailboxUnavailable)
        //            {
        //                System.Threading.Thread.Sleep(5000);
        //                smtpMail.Send(mailMessage);
        //            }
        //            else
        //            {
        //                throw ex;
        //            }
        //        }
        //    }
        //    mailMessage.Dispose();
        //}

        protected void btnSaveContinue_Click(object sender, EventArgs e)
        {
        }

        public void Sendblockemail(string templatename, string username, string EmailID, string survey_name, string launchtype, string unsubscribeurl)
        {

            string viewData1;
            JavaScriptSerializer js1 = new JavaScriptSerializer();
            js1.MaxJsonLength = int.MaxValue;
            clsjsonmandrill prmjson1 = new clsjsonmandrill();
            prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
            prmjson1.name = templatename;
            viewData1 = js1.Serialize(prmjson1);

            string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";


            WebClient request1 = new WebClient();
            request1.Encoding = System.Text.Encoding.UTF8;
            request1.Headers["Content-Type"] = "application/json";
            byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));

            string response1 = System.Text.Encoding.ASCII.GetString(resp1);


            clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);

            string bodycontent = jsmandrill11.code;


            string viewData;
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = int.MaxValue;
            var prmjson = new clsjsonmandrill.jsonmandrillmerge();
            prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
            prmjson.template_name = templatename;



            List<clsjsonmandrill.template_content> mtags = new List<clsjsonmandrill.template_content>();
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content00", content = "<div> Dear *|FNAME|*,</div>" });
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div><ul><li>&nbsp;Survey Name: *|Survey_Name|*</li><li>&nbsp;Launch Type: *|Launch_Type|*</li></ul></div>" });
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content02", content = "<div><p style='font-size:8pt;font-family:Arial,Sans-Serif'> *|UNSUBSCRIBEINSIGHTO|* </p></div>" });

            List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
            mvars.Add(new clsjsonmandrill.merge_vars { name = "FNAME", content = username });
            mvars.Add(new clsjsonmandrill.merge_vars { name = "Survey_Name", content = survey_name });
            mvars.Add(new clsjsonmandrill.merge_vars { name = "Launch_Type", content = launchtype });
            mvars.Add(new clsjsonmandrill.merge_vars { name = "UNSUBSCRIBEINSIGHTO", content = unsubscribeurl });
            prmjson.template_content = mtags;
            prmjson.merge_vars = mvars;

            viewData = js.Serialize(prmjson);


            string url = "https://mandrillapp.com/api/1.0/templates/render.json";


            WebClient request = new WebClient();
            request.Encoding = System.Text.Encoding.UTF8;
            request.Headers["Content-Type"] = "application/json";
            byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

            string response = System.Text.Encoding.ASCII.GetString(resp);

            string unesc = System.Text.RegularExpressions.Regex.Unescape(response);

            string first4 = unesc.Substring(0, 9);

            string last2 = unesc.Substring(unesc.Length - 2, 2);


            string unesc1 = unesc.Replace(first4, "");
            string unescf = unesc1.Replace(last2, "");


            clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);


            mailMessage = new MailMessage();
            mailMessage.To.Clear();
            mailMessage.Sender = new MailAddress("alerts@insighto.com", "Insighto.com");
            mailMessage.From = new MailAddress("alerts@insighto.com", "Insighto.com");

            mailMessage.Subject = "[Important –  From Insighto.com] Your survey is under review";

            mailMessage.To.Add(EmailID);
            mailMessage.Body = unescf;
            mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.Normal;
            smtpMail = new SmtpClient();
            smtpMail.Host = "smtp.mandrillapp.com";
            smtpMail.Port = 587;
            smtpMail.EnableSsl = true;
            smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpMail.Credentials = new System.Net.NetworkCredential("ram@knowience.com", "T74TJc3EZPYaIcvdBg--Dg");

            try
            {

                smtpMail.Send(mailMessage);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        System.Threading.Thread.Sleep(5000);
                        smtpMail.Send(mailMessage);
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            mailMessage.Dispose();
        }

        protected void btnPreLaunch_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var sessionService = ServiceFactory.GetService<SessionStateService>();
                var userDetails = sessionService.GetLoginUserDetailsSession();
                var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
                if (licenceType != null)
                {
                    var listFeatures = ServiceFactory.GetService<FeatureService>();
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.PRELAUNCH_CHECK).Value == 0)
                    {
                        Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL() + "' ,690, 400, 'yes');</script>");
                    }
                    else
                    {
                        PreLaunchFunction();
                        Response.Redirect(EncryptHelper.EncryptQuerystring("~/PreLaunchQuestionnaire.aspx", "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag));

                    }
                }
            }
        }

        protected void btnLaunchLater_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                PreLaunchFunction();
                Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag));
            }
        }

        private void PopulateSurveyOptions(SurveyBasicInfoView controls)
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            BindIntroText();
            if ((controls.LAUNCHOPTIONS_STATUS == 1) || (controls.CONTACTLIST_ID > 0 || controls.EMAILLIST_TYPE > 0 || controls.SURVEYURL_TYPE > 0))
            {
                if (controls.EMAILLIST_TYPE == 0 || controls.EMAILLIST_TYPE == 1)
                {
                    if (controls.EMAIL_INVITATION != null)
                    {
                        EditorQuestionIntro.Value = controls.EMAIL_INVITATION;
                    }
                    else
                    {
                        BindIntroText();
                    }
                    Launchtypesettings(1, controls);
                }
                else if (controls.EMAILLIST_TYPE == 2)
                {

                    Launchtypesettings(2, controls);
                }
                if (controls.CONTACTLIST_ID > 0)
                {
                    if (ddlEmailList.Items.FindByValue(controls.CONTACTLIST_ID.ToString()) != null)
                    {
                        ddlEmailList.SelectedItem.Value = controls.CONTACTLIST_ID.ToString();
                    }
                    else
                    {
                        ddlEmailList.SelectedIndex = 0;
                        if (controls.EMAIL_INVITATION != null)
                        {
                            EditorQuestionIntro.Value = controls.EMAIL_INVITATION;
                        }
                        else
                        {
                            BindIntroText();
                        }
                    }
                }
                if (controls.FROM_MAILADDRESS != null && controls.FROM_MAILADDRESS.Trim().Length > 0)
                    txtSenderEmail.Text = controls.FROM_MAILADDRESS;
                else
                {
                    if (userDetails != null && userDetails.UserId > 0)
                        txtSenderEmail.Text = userDetails.LoginName;
                }
                if (controls.MAIL_SUBJECT != null && controls.MAIL_SUBJECT.Trim().Length > 0)
                    txtSubject.Text = controls.MAIL_SUBJECT;
                if (controls.SENDER_NAME != null && controls.SENDER_NAME.Trim().Length > 0)
                {
                    txtSenderName.Text = controls.SENDER_NAME;
                }
                else
                {
                    if (userDetails != null && userDetails.UserId > 0)
                    {
                        txtSenderName.Text = userDetails.FirstName;
                    }
                }
                if (controls.REPLY_TO_EMAILADDRESS != null && controls.REPLY_TO_EMAILADDRESS.Trim().Length > 0)
                {
                    txtReplyTo.Text = controls.REPLY_TO_EMAILADDRESS;
                }
                else
                {
                    if (userDetails != null && userDetails.UserId > 0)
                        txtReplyTo.Text = userDetails.LoginName;
                }
                txtReplyTo.Text = userDetails.LoginName;
                txtSenderName.Text = userDetails.FirstName;
                txtSenderEmail.Text = userDetails.LoginName;
            }
            else
            {
                if (controls.EMAILLIST_TYPE == 1)
                {
                    Launchtypesettings(1, controls);
                    
                }
                else if (controls.EMAILLIST_TYPE == 2)
                {
                    Launchtypesettings(2, controls);
                }
                else if (controls.EMAILLIST_TYPE == 0 && base.SurveyFlag == 0)
                {
                    rbtnInternakAccessSetting.Items.FindByValue("1").Selected = true;
                    rbtnInternakAccessSetting.Items.FindByValue("1").Selected = true;
                    internalNote.Style.Add("display", "none");
                    externalNote.Style.Add("display", "none");
                    internalmailingSystem.Style.Add("display", "none");
                    webLinkSystem.Style.Add("display", "block");
                    btnLaunchLater.Text = "Launch Survey Later";
                }
                else if (base.SurveyFlag == 1)
                {
                    rbtnInternakAccessSetting.Items.FindByValue("1").Selected = true;
                    internalNote.Style.Add("display", "none");
                    externalNote.Style.Add("display", "none");
                    internalmailingSystem.Style.Add("display", "none");
                    webLinkSystem.Style.Add("display", "block");
                    divLayout.Style.Add("display", "block");
                    btnLaunchLater.Text = "Launch Widget Later";
                }
                txtReplyTo.Text = userDetails.LoginName;
                txtSenderName.Text = userDetails.FirstName;
                txtSenderEmail.Text = userDetails.LoginName;
            }
        }

        protected void ValidateAccessSettings(object sender, ServerValidateEventArgs args)
        {
            args.IsValid = !(rbtnInternakAccessSetting.SelectedItem == null);
        }

        private void bindSurveyType()
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();

            DataSet dsppstype = surcore.getSurveyType(base.SurveyID);
            dsppscredits = surcore.getCreditsCount(userDetails.UserId);

            if (applycreditval == "")
            {
                applycreditval = dsppstype.Tables[0].Rows[0][0].ToString();
            }

            if ((applycreditval == "") || (applycreditval == "FREE"))
            {
                imgchkfree.Style["visibility"] = "visible";
                imgchkpro.Style["visibility"] = "hidden";
                imgchkprem.Style["visibility"] = "hidden";
            }
            else if (applycreditval == "PPS_PRO")
            {
                imgchkpro.Style["visibility"] = "visible";
                imgchkfree.Style["visibility"] = "hidden";
                imgchkprem.Style["visibility"] = "hidden";
            }
            else if (applycreditval == "PPS_PREMIUM")
            {
                imgchkprem.Style["visibility"] = "visible";
                imgchkfree.Style["visibility"] = "hidden";
                imgchkpro.Style["visibility"] = "hidden";
            }
        }

        #region "Method : BindEmailList"
        /// <summary>
        /// Used to get country list and bind values to dropdownlist.
        /// </summary>

        private void BindContactLists(int userId, int surveyId)
        {
            var contactListDetails = ServiceFactory.GetService<PickListService>().GetContactLists(userId, 1, surveyId);
            if (contactListDetails.Count > 0)
            {
                //Bind list values to dropdownlist
                this.ddlEmailList.DataSource = contactListDetails;
                this.ddlEmailList.DataTextField = "CONTACTLIST_NAME";
                this.ddlEmailList.DataValueField = "CONTACTLIST_ID";
                this.ddlEmailList.DataBind();
                ddlEmailList.Items.Insert(0, new ListItem("Choose Email List", ""));
            }
        }
        #endregion

        public void PopulateFeatures()
        {

            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            if (userDetails != null)
            {
                var listFeatures = ServiceFactory.GetService<FeatureService>();


                if (userDetails.UserId > 0 && listFeatures != null)
                {
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.SENDER_NAME).Value == 5)
                    {
                        txtSenderName.Text = userDetails.FirstName;
                    }

                    if (listFeatures.Find(licenceType, FeatureDeploymentType.SENDER_EMAILADDRESS).Value == 5)
                    {
                        txtSenderEmail.Text = userDetails.LoginName;
                        // txtSenderEmail.Enabled = false;
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.REPLY_TO_EMAILADDRESS).Value == 5)
                    {
                        txtReplyTo.Text = userDetails.LoginName;
                        // txtReplyTo.Enabled = false;
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.EMAIL_SUBJECT).Value == 5)
                    {
                        txtSubject.Text = "";
                        //  txtSubject.Enabled = false;
                    }
                    if (listFeatures.Find(licenceType, FeatureDeploymentType.UNIQUE_RESPONDENT).Value == 0)
                    {
                        rbtnInternakAccessSetting.Items.FindByValue("0").Enabled = false;
                        rbtnInternakAccessSetting.Items.FindByValue("0").Enabled = false;
                    }
                }
            }
            else
            {
                Response.Redirect(PathHelper.GetUserLoginPageURL());
            }
            if (userDetails != null)
            {
                SurveyBasicInfoView.FROM_MAILADDRESS = userDetails.LoginName;
            }
            if (txtSenderEmail.Text.Length > 0)
            {
                SurveyBasicInfoView.FROM_MAILADDRESS = txtSenderEmail.Text.Trim();
            }
            if (txtSubject.Text.Length > 0)
            {
                SurveyBasicInfoView.MAIL_SUBJECT = txtSubject.Text.Trim();
            }
            if (txtSenderName.Text.Length > 0)
            {
                SurveyBasicInfoView.SENDER_NAME = txtSenderName.Text.Trim();
            }
            if (txtReplyTo.Text.Length > 0)
            {
                SurveyBasicInfoView.REPLY_TO_EMAILADDRESS = txtReplyTo.Text.Trim();
            }
            SurveyBasicInfoView.SURVEYLAUNCH_URL = _surveyLaunchUrl;

        }

        private void Launchtypesettings(int launchtype, SurveyBasicInfoView surveyControls)
        {
            if (launchtype == 1)
            {
                if (surveyControls.UNIQUE_RESPONDENT == 0)
                {
                    rbtnInternakAccessSetting.Items.FindByValue("0").Selected = true;
                    externalNote.Style.Add("display", "none");
                    internalNote.Style.Add("display", "block");
                }
                else if (surveyControls.UNIQUE_RESPONDENT == 1)
                {
                    rbtnInternakAccessSetting.Items.FindByValue("1").Selected = true;
                    externalNote.Style.Add("display", "none");
                    internalNote.Style.Add("display", "none");

                }
                else if (surveyControls.UNIQUE_RESPONDENT == 2)
                {
                    rbtnInternakAccessSetting.Items.FindByValue("2").Selected = true;
                    externalNote.Style.Add("display", "none");
                    internalNote.Style.Add("display", "block");
                }
                if (surveyControls.CONTACTLIST_ID > 0)
                {
                    if (ddlEmailList.Items.FindByValue(surveyControls.CONTACTLIST_ID.ToString()) != null)
                    {
                        ddlEmailList.SelectedValue = surveyControls.CONTACTLIST_ID.ToString();
                         internalmailingSystem.Style.Add("display", "block");
                         rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "show");
                    }
                    else
                    {
                        ddlEmailList.SelectedIndex = 0;
                    }
                    //btnAddEmail.Style.Add("display", "none");
                    // btnEditEmail.Style.Add("display", "block");
                }
                rbtnInternakAccessSetting.Visible = true;
               
                webLinkSystem.Style.Add("display", "block");
                reqEmaillist.ValidationGroup = "validate";
                reqSubject.ValidationGroup = "validate";
                reqName.ValidationGroup = "validate";
                reqSenderEmail.ValidationGroup = "validate";
                regSenderEmail.ValidationGroup = "validate";
                reqReplyTo.ValidationGroup = "validate";
                regReplyto.ValidationGroup = "validate";
                reqInternalsettings.ValidationGroup = "validate";
                divLayout.Style.Add("display", "block");
            }
            else if (launchtype == 2 && SurveyFlag == 0)
            {
                if (surveyControls.UNIQUE_RESPONDENT == 0)
                {
                    rbtnInternakAccessSetting.Items.FindByValue("0").Selected = true;
                    externalNote.Style.Add("display", "block");
                    internalNote.Style.Add("display", "none");
                }
                else if (surveyControls.UNIQUE_RESPONDENT == 1)
                {
                    rbtnInternakAccessSetting.Items.FindByValue("1").Selected = true;
                    externalNote.Style.Add("display", "none");
                    internalNote.Style.Add("display", "none");
                }
                ddlEmailList.SelectedIndex = 0;
                txtSubject.Text = "";
                txtSenderName.Text = "";
                txtSenderEmail.Text = "";
                txtReplyTo.Text = "";
                //internalmailingSystem.Style.Add("display", "none");
                webLinkSystem.Style.Add("display", "block");
                reqEmaillist.ValidationGroup = "notval";
                reqSubject.ValidationGroup = "notval";
                reqName.ValidationGroup = "notval";
                reqSenderEmail.ValidationGroup = "notval";
                regSenderEmail.ValidationGroup = "notval";
                reqReplyTo.ValidationGroup = "notval";
                regReplyto.ValidationGroup = "notval";
                reqInternalsettings.ValidationGroup = "notval";
                divLayout.Style.Add("display", "block");
                //btnBack.ValidationGroup = "notval";
            }
            else if (SurveyFlag == 1)
            {
                if (surveyControls.UNIQUE_RESPONDENT == 0)
                {
                    rbtnInternakAccessSetting.Items.FindByValue("0").Selected = true;
                    externalNote.Style.Add("display", "none");
                    internalNote.Style.Add("display", "none");
                }
                else if (surveyControls.UNIQUE_RESPONDENT == 1)
                {
                    rbtnInternakAccessSetting.Items.FindByValue("1").Selected = true;
                    externalNote.Style.Add("display", "none");
                    internalNote.Style.Add("display", "none");
                }
                ddlEmailList.SelectedIndex = 0;
                txtSubject.Text = "";
                txtSenderName.Text = "";
                txtSenderEmail.Text = "";
                txtReplyTo.Text = "";
                internalmailingSystem.Style.Add("display", "none");
                webLinkSystem.Style.Add("display", "block");
                //rbtnSurveyType.Visible = false;
                rbtnWidgetLaunch.Visible = true;
                reqEmaillist.ValidationGroup = "notval";
                reqSubject.ValidationGroup = "notval";
                reqName.ValidationGroup = "notval";
                reqSenderEmail.ValidationGroup = "notval";
                regSenderEmail.ValidationGroup = "notval";
                reqReplyTo.ValidationGroup = "notval";
                regReplyto.ValidationGroup = "notval";
                reqInternalsettings.ValidationGroup = "notval";
                divLayout.Style.Add("display", "block");
            }
        }

        public void PreLaunchFunction()
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            if (SurveyFlag == 0)
            {
                if (txtSubject.Text == "")
                {
                    SurveyBasicInfoView.EMAILLIST_TYPE = ValidationHelper.GetInteger(2, 0);
                }
                else
                {
                    SurveyBasicInfoView.EMAILLIST_TYPE = ValidationHelper.GetInteger(1, 0);
                }
            }
            else
            {
                SurveyBasicInfoView.EMAILLIST_TYPE = 2;
            }
            SurveyBasicInfoView.CONTACTLIST_ID = ValidationHelper.GetInteger(ddlEmailList.SelectedValue, 0);
            if (txtSenderEmail.Text.Length > 0)
            {
                SurveyBasicInfoView.FROM_MAILADDRESS = txtSenderEmail.Text.Trim();
            }
            else
            {
                SurveyBasicInfoView.FROM_MAILADDRESS = "";
            }
            if (txtSubject.Text.Length > 0)
            {
                SurveyBasicInfoView.MAIL_SUBJECT = txtSubject.Text.Trim();
            }
            else
            {
                SurveyBasicInfoView.MAIL_SUBJECT = "";
            }
            if (txtSenderName.Text.Length > 0)
            {
                SurveyBasicInfoView.SENDER_NAME = txtSenderName.Text.Trim();
            }
            else
            {
                SurveyBasicInfoView.SENDER_NAME = "";
            }
            if (txtReplyTo.Text.Length > 0)
            {
                SurveyBasicInfoView.REPLY_TO_EMAILADDRESS = txtReplyTo.Text.Trim();
            }
            else
            {
                SurveyBasicInfoView.REPLY_TO_EMAILADDRESS = "";
            }
            SurveyBasicInfoView.SURVEY_URL = surveyLaunchURL;
            SurveyBasicInfoView.SURVEYLAUNCH_URL = surveyLaunchURL;
            SurveyBasicInfoView.SURVEYURL_TYPE = 1;
            if (SurveyBasicInfoView.EMAILLIST_TYPE == 1)
            {
                SurveyBasicInfoView.EMAIL_INVITATION = EditorQuestionIntro.Value.Replace("&nbsp;", "");
            }
            else
            {
                SurveyBasicInfoView.EMAIL_INVITATION = "";
            }
            SurveyBasicInfoView.LAUNCHOPTIONS_STATUS = 1;
            if (SurveyBasicInfoView.EMAILLIST_TYPE == 1)
            {
                SurveyBasicInfoView.UNIQUE_RESPONDENT = ValidationHelper.GetInteger(rbtnInternakAccessSetting.SelectedItem.Value, 0);
            }
            else
            {
                SurveyBasicInfoView.UNIQUE_RESPONDENT = ValidationHelper.GetInteger(rbtnInternakAccessSetting.SelectedItem.Value, 0);
            }
            SurveyBasicInfoView.SURVEY_LAYOUT = ValidationHelper.GetInteger(rbtnLayout.SelectedItem.Value, 0);
            ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 1);

        }

        protected void btnAddEmail_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["Key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["Key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("SurveyId"))
                {
                    int SurveyID = Convert.ToInt32(ht["SurveyId"]);
                    if (base.IsSurveyActive)
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetAddressBookURL(), "SurveyId=" + SurveyID + "&Flag=Active&ContId=" + ddlEmailList.SelectedValue + "&surveyFlag=" + SurveyFlag));
                    }
                    else
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetAddressBookURL(), "SurveyId=" + SurveyID + "&ContId=" + ddlEmailList.SelectedValue + "&surveyFlag=" + SurveyFlag));
                    }

                }

            }
        }

        protected void btnManageEmail_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["Key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["Key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("SurveyId"))
                {
                    int SurveyID = Convert.ToInt32(ht["SurveyId"]);
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetAddressBookURL(), "surveyId=" + SurveyID + "&surveyFlag=" + SurveyFlag));

                }


            }
        }

        protected void btnEditEmail_Click(object sender, EventArgs e)
        {
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetCreateEmailListURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&ContactId=" + ddlEmailList.SelectedValue + "&surveyFlag=" + SurveyFlag));
        }

        public void BindIntroText()
        {
            EditorQuestionIntro.Value = introText;
        }

        protected void lnkCopyintro_Click(object sender, EventArgs e)
        {
            EditorQuestionIntro.Value = hdnIntroText.Value + "<p></p><p>Please click on the survey link given below to start your survey.</p>";
        }

        protected void lnkReset_Click(object sender, EventArgs e)
        {
            EditorQuestionIntro.Value = introText;
        }

        protected void lnkPreview_Click(object sender, EventArgs e)
        {
            string previewUrl = "";
            SurveyBasicInfoView.EMAIL_INVITATION = EditorQuestionIntro.Value.Trim();
            ServiceFactory.GetService<SurveyService>().UpdateSurveyControlsBasedonInfo(SurveyBasicInfoView);
            if (imagechk.Checked && EditorQuestionIntro.InnerText != "")
            {
                previewUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetEmailInvitationPreviewLink().Replace("~/", ""), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag + "&image=" + "available");
                Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + previewUrl + "',910, 430, 'yes');</script>");
            }
            else if (EditorQuestionIntro.InnerText != "" && !imagechk.Checked)
            {
                previewUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetEmailInvitationPreviewLink().Replace("~/", ""), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag + "&image=" + "notavailable");
                Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + previewUrl + "',910, 430, 'yes');</script>");
            }


        }

        protected void DisableControls()
        {
            DataSet dsppstype = surcore.getSurveyType(base.SurveyID);

            string launchcnt = dsppstype.Tables[2].Rows[0][0].ToString();
            int launchcountval = Convert.ToInt32(launchcnt);
            reqEmaillist.ValidationGroup = "validate";

        }

        protected void EnableControls()
        {

            //btnLaunchLater.Visible = true;
            //btnPreLaunch.Visible = true;
            //if (SurveyFlag == 0)
            //{
            //    btnSaveContinue.Text = "Launch Survey Now";
            //    btnSaveContinue.ToolTip = "Launch Survey Now";
            //}
            //else if (SurveyFlag == 1)
            //{
            //    btnSaveContinue.Text = "Launch Widget Now";
            //    btnSaveContinue.ToolTip = "Launch Widget Now";
            //}

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("Page"))
                {
                    if (ht["Page"].ToString() == "PEI")
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetPreLaunchEmailInvitationURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag));
                    }
                    else if (ht["Page"].ToString() == "PSEL")
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetPreLaunchSurveyEmailListURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag));
                    }
                }
            }
        }

        public SqlConnection strconnectionstring()
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection strconn = new SqlConnection(connStr);

                strconn.Open();
                return strconn;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet bindsurveyQA(int surveyID)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetSurveysQA", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@surveyID", SqlDbType.Int)).Value = surveyID;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dsSurveyQA = new DataSet();
                sda.Fill(dsSurveyQA);
                return dsSurveyQA;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void bindgrdSurvey()
        {


            DataSet dsSqa = new DataSet();
            dsSqa = bindsurveyQA(surveyId);
            grdSurveyQA.DataSource = dsSqa;

            if (dsSqa.Tables[0].Rows.Count > 0)
            {

                grdSurveyQA.DataBind();

            }

            if (dsSqa.Tables[1].Rows.Count > 0)
            {
                txttollfreenumber.Text = dsSqa.Tables[1].Rows[0][0].ToString();
                txtkookoonumber.Text = dsSqa.Tables[1].Rows[0][1].ToString();
                txtkookoopath.Text = dsSqa.Tables[1].Rows[0][2].ToString();
                txtmaxduration.Text = dsSqa.Tables[1].Rows[0][3].ToString();
            }


            if (dsSqa.Tables[2].Rows.Count > 0)
            {
                if (dsSqa.Tables[2].Rows[0][2].ToString() == "")
                {
                    lblintroduction.Visible = true;
                    lblintro.Visible = true;
                    lblintro.Text = dsSqa.Tables[2].Rows[0][1].ToString();
                    fileuploadforintro.Visible = true;
                    //  btnintroupload.Visible = true;
                    lisintroaudio.Visible = false;
                    btnintrosurvey.Visible = true;

                }
                else
                {
                    lblintroduction.Visible = true;
                    lblintro.Visible = true;
                    lblintro.Text = dsSqa.Tables[2].Rows[0][1].ToString();
                    fileuploadforintro.Visible = false;
                    //btnintroupload.Visible = false;
                    lisintroaudio.Visible = true;
                    btnintrosurvey.Visible = false;
                    lisintroaudio.Text = dsSqa.Tables[2].Rows[0][2].ToString();

                    lisintroaudio.NavigateUrl = serveraudiofilepath + lisintroaudio.Text;


                }


            }
            if (dsSqa.Tables[3].Rows.Count > 0)
            {
                if (dsSqa.Tables[3].Rows[0][2].ToString() == "")
                {
                    lblend.Visible = true;
                    lblthanks.Visible = true;
                    lblthanks.Text = dsSqa.Tables[3].Rows[0][1].ToString();
                    fileuploadthanks.Visible = true;
                    //  btnuploadthanks.Visible = true;
                    lnkbtnthanks.Visible = false;
                    btnThanksSurvey.Visible = true;


                }
                else
                {
                    lblend.Visible = true;
                    lblthanks.Visible = true;
                    lblthanks.Text = dsSqa.Tables[3].Rows[0][1].ToString();
                    fileuploadthanks.Visible = false;
                    //  btnuploadthanks.Visible = false;
                    lnkbtnthanks.Visible = true;
                    btnThanksSurvey.Visible = false;
                    lnkbtnthanks.Text = dsSqa.Tables[3].Rows[0][2].ToString();
                    lnkbtnthanks.NavigateUrl = serveraudiofilepath + lnkbtnthanks.Text;
                }
            }

            if (dsSqa.Tables[4].Rows.Count > 0)
            {
                if (dsSqa.Tables[4].Rows[0][1].ToString() == "")
                {

                    fileuploadnoresponse.Visible = true;
                    lnkbtnnoresponse.Visible = false;
                    btnnoresponse.Visible = true;

                }
                else
                {

                    fileuploadnoresponse.Visible = false;
                    lnkbtnnoresponse.Visible = true;
                    btnnoresponse.Visible = false;
                    lnkbtnnoresponse.Text = dsSqa.Tables[4].Rows[0][1].ToString();
                    lnkbtnnoresponse.NavigateUrl = serveraudiofilepath + lnkbtnnoresponse.Text;

                }
            }
            if (dsSqa.Tables[5].Rows.Count > 0)
            {
                if (dsSqa.Tables[5].Rows[0][1].ToString() == "")
                {


                    fileuploadinvalidentry.Visible = true;
                    lnkbtninvalidentry.Visible = false;
                    btninvalidentry.Visible = true;

                }
                else
                {


                    fileuploadinvalidentry.Visible = false;
                    lnkbtninvalidentry.Visible = true;
                    btninvalidentry.Visible = false;
                    lnkbtninvalidentry.Text = dsSqa.Tables[5].Rows[0][1].ToString();
                    lnkbtninvalidentry.NavigateUrl = serveraudiofilepath + lnkbtninvalidentry.Text;

                }
            }
        }

        protected void grdSurveyQA_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            DataSet dsgrd2 = new DataSet();
            GridView grd2 = new GridView();

            Label lblqID = (Label)e.Row.FindControl("QID");

            grd2 = (GridView)e.Row.FindControl("grdQAnsOption");


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblaudiofilename = (Label)e.Row.FindControl("AudioID");

                if (lblaudiofilename.Text == "")
                {
                    e.Row.FindControl("fileupload1").Visible = true;
                    //e.Row.FindControl("btnupload").Visible = true;
                }
                else
                {

                    e.Row.FindControl("lisaudio").Visible = true;

                }


            }



            if (lblqID != null)
            {
                SqlConnection con = new SqlConnection();

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetSurveysQAnsOptions", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@QuestionID", SqlDbType.Int)).Value = Convert.ToInt32(lblqID.Text);

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dsAnsoptions = new DataSet();
                sda.Fill(dsAnsoptions);
                con.Close();
                if (dsAnsoptions.Tables[0].Rows.Count > 0)
                {
                    grd2.DataSource = dsAnsoptions;
                    grd2.DataBind();

                }
                else
                {
                    grd2.EmptyDataText = "Voice Answer";
                    grd2.DataBind();
                }

            }

        }

        //protected void btnupload_Click(Object sender, EventArgs e)
        //{

        //    if (Request.QueryString["key"] != null)
        //    {
        //        typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

        //        if (typeHt.Contains(Constants.SURVEYID))
        //            surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
        //    }

        //    if (fileuploadforintro.HasFile)
        //    {


        //        fileuploadforintro.SaveAs(HttpContext.Current.Server.MapPath("AudioFiles") + "//" + fileuploadforintro.FileName);


        //        string strfilename = fileuploadforintro.FileName;

        //        if (strfilename != "")
        //        {
        //            int intsurveyid = surveyId;

        //            insertIntroAudiofilename(intsurveyid, strfilename);


        //        }
        //    }
        //    bindgrdSurvey();
        //}

        protected void btnuploadthanks_Click(Object sender, EventArgs e)
        {
            if (fileuploadthanks.HasFile)
            {
                fileuploadthanks.SaveAs(HttpContext.Current.Server.MapPath("AudioFiles") + "\\" + fileuploadthanks.FileName);


                string strfilename = fileuploadthanks.FileName;

                if (strfilename != "")
                {
                    int intsurveyid = surveyId;

                    insertThanksAudiofilename(intsurveyid, strfilename);


                }
            }
            bindgrdSurvey();


        }

        protected void lnkaudioIntro_click(Object sender, EventArgs e)
        {
            try
            {

                string filepath = lisintroaudio.Text;

                //  string strfullpath = HttpContext.Current.Server.MapPath("AudioFiles") + "\\" + filepath;



                string strfullpath = serveraudiofilepath + filepath;


                //SoundPlayer mplayer = new SoundPlayer();
                //mplayer.SoundLocation = strfullpath;

                //mplayer.Play();

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        protected void lnkaudiothanks_click(Object sender, EventArgs e)
        {
            try
            {

                string filepath = lnkbtnthanks.Text;

                //   string strfullpath = HttpContext.Current.Server.MapPath("AudioFiles") + "\\" + filepath;
                string strfullpath = serveraudiofilepath + filepath;

                SoundPlayer mplayer = new SoundPlayer();
                mplayer.SoundLocation = strfullpath;

                mplayer.Play();

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        protected void lnkaudio_click(Object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkbtn = sender as LinkButton;
                GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
                string filepath = grdSurveyQA.DataKeys[gvrow.RowIndex].Values["Audiofilename"].ToString();

                // string strfullpath = HttpContext.Current.Server.MapPath("AudioFiles") + "\\" + filepath;
                string strfullpath = serveraudiofilepath + filepath;

                SoundPlayer mplayer = new SoundPlayer();
                mplayer.SoundLocation = strfullpath;

                mplayer.Play();

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        protected void grdSurveyQA_SelectedIndexChanged(object sender, EventArgs e)
        {
            string str1 = grdSurveyQA.SelectedRow.Cells[4].Text;



        }

        protected void grdSurveyQA_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            string str = grdSurveyQA.DataKeys[e.RowIndex].Value.ToString();


        }

        protected void grdSurveyQA_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            }

            if (e.CommandName == "InsertRecord")
            {
                for (int i = 0; i < grdSurveyQA.Rows.Count; i++)
                {

                    string str1 = grdSurveyQA.DataKeys[i].Value.ToString();

                    FileUpload uploadfilename = (FileUpload)grdSurveyQA.Rows[i].Cells[3].FindControl("fileupload1"); //Gets the data value in the grid;
                    if (uploadfilename.HasFile)
                    {
                        uploadfilename.SaveAs(HttpContext.Current.Server.MapPath("AudioFiles") + "\\" + uploadfilename.FileName);


                        string strfilename = uploadfilename.FileName;




                        if (strfilename != "")
                        {
                            int intQuestionID = int.Parse(str1);

                            insertAudiofilename(intQuestionID, strfilename);
                        }

                    }
                }
                bindgrdSurvey();
            }


        }

        public int insertAudiofilename(int questionid, string filename)
        {

            SqlConnection con = new SqlConnection();
            try
            {

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_InsertFileNameQuesAns", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@Question_ID", SqlDbType.Int)).Value = questionid;
                scom.Parameters.Add(new SqlParameter("@filename", SqlDbType.VarChar)).Value = filename;


                int rows = scom.ExecuteNonQuery();

                return rows;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public int insertIntroAudiofilename(int surveyid, string filename)
        {

            SqlConnection con = new SqlConnection();
            try
            {

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_InsertFileNameSurveyIntro", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@Survey_ID", SqlDbType.Int)).Value = surveyid;
                scom.Parameters.Add(new SqlParameter("@filename", SqlDbType.VarChar)).Value = filename;


                int rows = scom.ExecuteNonQuery();

                return rows;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public int insertThanksAudiofilename(int surveyid, string filename)
        {

            SqlConnection con = new SqlConnection();
            try
            {

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_InsertFileNameSurveyThanks", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@Survey_ID", SqlDbType.Int)).Value = surveyid;
                scom.Parameters.Add(new SqlParameter("@filename", SqlDbType.VarChar)).Value = filename;


                int rows = scom.ExecuteNonQuery();

                return rows;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        protected void saveKookoo_Click(object sender, EventArgs e)
        {
            try
            {
                string strtollfreeno = txttollfreenumber.Text;
                string strkookoonos = txtkookoonumber.Text;
                string strwavpath = txtkookoopath.Text;
                int intsurveyID = surveyId;
                string strmaxduration = txtmaxduration.Text;
                insertKooKooInfo(strtollfreeno, strkookoonos, strwavpath, intsurveyID, strmaxduration);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //insert the selected options
        public int insertKooKooInfo(string tollfreeno, string kookoonos, string Audiofilepath, int surveyID, string maxduration)
        {

            SqlConnection con = new SqlConnection();
            try
            {

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_InsertKooKooInfo", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@TollFreeNumber", SqlDbType.VarChar)).Value = tollfreeno;
                scom.Parameters.Add(new SqlParameter("@kookoonos", SqlDbType.VarChar)).Value = kookoonos;
                scom.Parameters.Add(new SqlParameter("@wavfilepath", SqlDbType.VarChar)).Value = Audiofilepath;
                scom.Parameters.Add(new SqlParameter("@Survey_ID", SqlDbType.VarChar)).Value = surveyID;
                scom.Parameters.Add(new SqlParameter("@voicemaxduration", SqlDbType.VarChar)).Value = maxduration;

                int rows = scom.ExecuteNonQuery();

                return rows;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        protected void lnkbutton_Click(object sender, EventArgs e)
        {
            int surveyIdlcl = surveyId;
            // string Navurlstr = EncryptHelper.EncryptQuerystring("~/MyAccounts.aspx", "SurveyId=" + surveyId + "&surveyFlag=" + 0);
            //  string Navurlstr = EncryptHelper.EncryptQuerystring("MyAccounts.aspx", "surveyFlag=" + 0);
            //  string strurl = "http://insighto.com/" + Navurlstr;
            //  Response.Redirect(strurl);

            // Response.Redirect("Reports.aspx");
        }

        protected void btnintrosurvey_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            }

            if (fileuploadforintro.HasFile)
            {


                fileuploadforintro.SaveAs(HttpContext.Current.Server.MapPath("AudioFiles") + "//" + fileuploadforintro.FileName);


                string strfilename = fileuploadforintro.FileName;

                if (strfilename != "")
                {
                    int intsurveyid = surveyId;

                    insertIntroAudiofilename(intsurveyid, strfilename);


                }
            }
            bindgrdSurvey();
        }

        protected void btnThanksSurvey_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            }
            if (fileuploadthanks.HasFile)
            {
                fileuploadthanks.SaveAs(HttpContext.Current.Server.MapPath("AudioFiles") + "\\" + fileuploadthanks.FileName);


                string strfilename = fileuploadthanks.FileName;

                if (strfilename != "")
                {
                    int intsurveyid = surveyId;

                    insertThanksAudiofilename(intsurveyid, strfilename);


                }
            }
            bindgrdSurvey();
        }

        protected void btnsurveygrid_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            }


            foreach (GridViewRow row in grdSurveyQA.Rows)
            {
                FileUpload myfile = (FileUpload)row.FindControl("fileupload1");
            }

            for (int i = 0; i < grdSurveyQA.Rows.Count; i++)
            {

                string str1 = grdSurveyQA.DataKeys[i].Value.ToString();

                FileUpload uploadfilename = (FileUpload)grdSurveyQA.Rows[i].Cells[3].FindControl("fileupload1"); //Gets the data value in the grid;
                if (uploadfilename.HasFile)
                {
                    uploadfilename.SaveAs(HttpContext.Current.Server.MapPath("AudioFiles") + "\\" + uploadfilename.FileName);


                    string strfilename = uploadfilename.FileName;

                    string strfilepath = serveraudiofilepath + strfilename;

                    if (strfilepath != "")
                    {
                        int intQuestionID = int.Parse(str1);

                        insertAudiofilename(intQuestionID, strfilepath);
                    }

                }
                bindgrdSurvey();
            }
        }

        protected void SurveyKookooInfo_Click(object sender, EventArgs e)
        {
            try
            {
                string strtollfreeno = txttollfreenumber.Text;
                string strkookoonos = txtkookoonumber.Text;
                string strwavpath = txtkookoopath.Text;
                int intsurveyID = surveyId;
                string strmaxduration = txtmaxduration.Text;
                insertKooKooInfo(strtollfreeno, strkookoonos, strwavpath, intsurveyID, strmaxduration);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int insertNoResponseSurvey(int surveyid, string filename)
        {

            SqlConnection con = new SqlConnection();
            try
            {

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_InsertFileNameNoResponseSurvey", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@Survey_ID", SqlDbType.Int)).Value = surveyid;
                scom.Parameters.Add(new SqlParameter("@filename", SqlDbType.VarChar)).Value = filename;


                int rows = scom.ExecuteNonQuery();

                return rows;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnnoresponse_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            }

            if (fileuploadnoresponse.HasFile)
            {
                fileuploadnoresponse.SaveAs(HttpContext.Current.Server.MapPath("AudioFiles") + "\\" + fileuploadnoresponse.FileName);


                string strfilename = fileuploadnoresponse.FileName;

                if (strfilename != "")
                {
                    int intsurveyid = surveyId;

                    insertNoResponseSurvey(intsurveyid, strfilename);


                }
            }
            bindgrdSurvey();
        }

        public int insertInvalidEntry(int surveyid, string filename)
        {

            SqlConnection con = new SqlConnection();
            try
            {

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_InsertFileNameInvalidentrySurvey", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@Survey_ID", SqlDbType.Int)).Value = surveyid;
                scom.Parameters.Add(new SqlParameter("@filename", SqlDbType.VarChar)).Value = filename;


                int rows = scom.ExecuteNonQuery();

                return rows;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

        }

        protected void btninvalidentry_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains(Constants.SURVEYID))
                    surveyId = int.Parse(typeHt[Constants.SURVEYID].ToString());
            }


            if (fileuploadinvalidentry.HasFile)
            {
                fileuploadinvalidentry.SaveAs(HttpContext.Current.Server.MapPath("AudioFiles") + "\\" + fileuploadinvalidentry.FileName);


                string strfilename = fileuploadinvalidentry.FileName;

                if (strfilename != "")
                {
                    int intsurveyid = surveyId;

                    insertInvalidEntry(intsurveyid, strfilename);


                }
            }
            bindgrdSurvey();
        }

        protected void imagechk_CheckedChanged(object sender, EventArgs e)
        {
            if (base.SurveyBasicInfoView.SURVEY_LOGO == null || base.SurveyBasicInfoView.SURVEY_LOGO == "")
            {
                string popupurl = ConfigurationManager.AppSettings["RootURL"].ToString() + "Help/Logohelp.htm";

                Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + popupurl + "',350, 210, 'No');</script>");
                imagechk.Checked = false;
            }
        }

        protected void btnLaunchEmail_Click(object sender, EventArgs e)
        {
            rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "show");
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();

            DataSet dsppstype = surcore.getSurveyType(base.SurveyID);
            dsppscredits = surcore.getCreditsCount(userDetails.UserId);

            if (applycreditval == "")
            {
                applycreditval = dsppstype.Tables[0].Rows[0][0].ToString();
            }

            if ((applycreditval == "") || (applycreditval == "FREE"))
            {
                //imgchkfree.Visible = true;
                imgchkfree.Style["visibility"] = "visible";
                imgchkpro.Style["visibility"] = "hidden";
                imgchkprem.Style["visibility"] = "hidden";
            }
            else if (applycreditval == "PPS_PRO")
            {
                imgchkpro.Style["visibility"] = "visible";
                imgchkfree.Style["visibility"] = "hidden";
                imgchkprem.Style["visibility"] = "hidden";
            }
            else if (applycreditval == "PPS_PREMIUM")
            {
                imgchkprem.Style["visibility"] = "visible";
                imgchkfree.Style["visibility"] = "hidden";
                imgchkpro.Style["visibility"] = "hidden";
            }
            dvtelesurvey.Visible = false;
          
            if ((applycreditval == "PPS_PRO") && (dsppscredits.Tables[0].Rows.Count > 0))
            {
                DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", base.SurveyBasicInfoView.STATUS);
            }
           
            else if ((applycreditval == "PPS_PREMIUM") && (dsppscredits.Tables[1].Rows.Count > 0))
            {
                DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", base.SurveyBasicInfoView.STATUS);
            }
            else if (applycreditval == "FREE")
            {
                DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "FREE", base.SurveyBasicInfoView.SURVEY_ID, "FREE", base.SurveyBasicInfoView.STATUS);
                rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "hidden");
                Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "' ,690, 500, 'yes');</script>");
                webLinkSystem.Style.Add("display", "block");
                divLayout.Style.Add("display", "block");
            }

            if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_THROUGHINSIGHTO).Value == 0 && (applycreditval != "PPS_PRO" && applycreditval != "PPS_PREMIUM" && applycreditval != ""))
            {
                if (dsppstype.Tables[0].Rows[0][0].ToString() == null || dsppstype.Tables[0].Rows[0][0].ToString() == "FREE" || dsppstype.Tables[0].Rows[0][0].ToString() == "BASIC" || dsppstype.Tables[0].Rows[0][0].ToString() == "")
                {
                    Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "' ,690, 500, 'yes');</script>");
                    webLinkSystem.Style.Add("display", "block");
                    divLayout.Style.Add("display", "block");
                }
            }
            else if ((applycreditval == "") && (licenceType.ToString() != "PREMIUM_YEARLY"))
            {
                Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "' ,690, 500, 'yes');</script>");
                webLinkSystem.Style.Add("display", "block");
                rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "hidden");
                divLayout.Style.Add("display", "block");
            }
            else
            {

                if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0 && base.SurveyBasicInfoView.STATUS == "Draft")
                {
                    if (base.SurveyBasicInfoView.EMAIL_INVITATION == null || base.SurveyBasicInfoView.EMAIL_INVITATION == "")
                    {
                        BindIntroText();

                    }
                    else
                    {
                        EditorQuestionIntro.Value = base.SurveyBasicInfoView.EMAIL_INVITATION;
                    }
                }

                if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0 && base.SurveyBasicInfoView.STATUS == "Active")
                {
                    if (base.SurveyBasicInfoView.EMAIL_INVITATION == null || base.SurveyBasicInfoView.EMAIL_INVITATION == "")
                    {
                        BindIntroText();

                    }
                    else
                    {
                        EditorQuestionIntro.Value = base.SurveyBasicInfoView.EMAIL_INVITATION;
                    }
                }


                if (contactListId > 0)
                {
                    if (ddlEmailList.Items.FindByValue(contactListId.ToString()) != null)
                    {
                        ddlEmailList.SelectedValue = contactListId.ToString();
                        EditorQuestionIntro.Value = base.SurveyBasicInfoView.EMAIL_INVITATION;
                    }
                    else
                    {
                        ddlEmailList.SelectedIndex = 0;
                    }
                }
                else if (base.SurveyBasicInfoView.CONTACTLIST_ID > 0)
                {
                    if (ddlEmailList.Items.FindByValue(base.SurveyBasicInfoView.CONTACTLIST_ID.ToString()) != null)
                    {
                        ddlEmailList.SelectedValue = base.SurveyBasicInfoView.CONTACTLIST_ID.ToString();
                    }
                    else
                    {
                        ddlEmailList.SelectedIndex = 0;
                    }
                }
                if (base.SurveyBasicInfoView.FROM_MAILADDRESS != null && base.SurveyBasicInfoView.FROM_MAILADDRESS.Trim().Length > 0)
                    txtSenderEmail.Text = base.SurveyBasicInfoView.FROM_MAILADDRESS;
                else
                {
                    if (userDetails != null && userDetails.UserId > 0)
                        txtSenderEmail.Text = userDetails.LoginName;
                }
                if (base.SurveyBasicInfoView.MAIL_SUBJECT != null && base.SurveyBasicInfoView.MAIL_SUBJECT.Trim().Length > 0)
                    txtSubject.Text = base.SurveyBasicInfoView.MAIL_SUBJECT;

                if (base.SurveyBasicInfoView.SENDER_NAME != null && base.SurveyBasicInfoView.SENDER_NAME.Trim().Length > 0)
                {
                    txtSenderName.Text = base.SurveyBasicInfoView.SENDER_NAME;
                }
                else
                {
                    if (userDetails != null && userDetails.UserId > 0)
                    {
                        txtSenderName.Text = userDetails.FirstName;
                    }
                }
                if (base.SurveyBasicInfoView.REPLY_TO_EMAILADDRESS != null && base.SurveyBasicInfoView.REPLY_TO_EMAILADDRESS.Trim().Length > 0)
                {
                    txtReplyTo.Text = base.SurveyBasicInfoView.REPLY_TO_EMAILADDRESS;
                }
                else
                {
                    if (userDetails != null && userDetails.UserId > 0)
                        txtReplyTo.Text = userDetails.LoginName;
                }
                rbtnLayout.SelectedValue = Convert.ToString(base.SurveyBasicInfoView.SURVEY_LAYOUT);
                rbtnInternakAccessSetting.Items.FindByValue("1").Selected = true;
                externalNote.Style.Add("display", "none");
                internalNote.Style.Add("display", "none");
                internalmailingSystem.Style.Add("display", "block");
                webLinkSystem.Style.Add("display", "block");
                divLayout.Style.Add("display", "block");
                rbtnInternakAccessSetting.Visible = true;
                reqEmaillist.Enabled = true;
                reqSubject.Enabled = true;
                reqName.Enabled = true;
                reqSenderEmail.Enabled = true;
                regSenderEmail.Enabled = true;
                reqReplyTo.Enabled = true;
                regReplyto.Enabled = true;
                reqEmaillist.ValidationGroup = "validate";
                reqSubject.ValidationGroup = "validate";
                reqName.ValidationGroup = "validate";
                reqSenderEmail.ValidationGroup = "validate";
                regSenderEmail.ValidationGroup = "validate";
                reqReplyTo.ValidationGroup = "validate";
                regReplyto.ValidationGroup = "validate";
                reqInternalsettings.ValidationGroup = "validate";
            }


        }

        protected void btnLaunchWeb_Click(object sender, EventArgs e)
        {

            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            int launchtypenumber = surcore.getLaunchTypeNumber(base.SurveyBasicInfoView.SURVEY_ID);
            LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
            DataSet dsppstype = surcore.getSurveyType(base.SurveyID);
                dsppscredits = surcore.getCreditsCount(userDetails.UserId);

                if (applycreditval == "")
                {
                    applycreditval = dsppstype.Tables[0].Rows[0][0].ToString();
                }

                if ((applycreditval == "") || (applycreditval == "FREE"))
                {
                    //imgchkfree.Visible = true;
                    imgchkfree.Style["visibility"] = "visible";
                    imgchkpro.Style["visibility"] = "hidden";
                    imgchkprem.Style["visibility"] = "hidden";
                }
                else if (applycreditval == "PPS_PRO")
                {
                    imgchkpro.Style["visibility"] = "visible";
                    imgchkfree.Style["visibility"] = "hidden";
                    imgchkprem.Style["visibility"] = "hidden";
                }
                else if (applycreditval == "PPS_PREMIUM")
                {
                    imgchkprem.Style["visibility"] = "visible";
                    imgchkfree.Style["visibility"] = "hidden";
                    imgchkpro.Style["visibility"] = "hidden";
                }
                dvErrMsg.Visible = false;
                lblErrMsg.Visible = false;
                dvtelesurvey.Visible = false;
                if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_THROUGHEXTERNAL).Value == 0)
                {
                    if (dsppstype.Tables[0].Rows[0][0].ToString() == null || dsppstype.Tables[0].Rows[0][0].ToString() == "FREE" || dsppstype.Tables[0].Rows[0][0].ToString() == "BASIC" || dsppstype.Tables[0].Rows[0][0].ToString() == "")
                    {
                        Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "' ,690, 500, 'yes');</script>");
                        webLinkSystem.Style.Add("display", "block");
                        divLayout.Style.Add("display", "block");
                    }
                }
                else
                {
                    EditorQuestionIntro.Value = "";
                    ddlEmailList.SelectedIndex = 0;
                    txtSenderEmail.Text = "";
                    txtSubject.Text = "";
                    txtSenderName.Text = "";
                    txtReplyTo.Text = "";
                 //   rbtnInternakAccessSetting.Items.FindByValue("1").Selected = true;
                    internalNote.Style.Add("display", "none");
                    externalNote.Style.Add("display", "none");
                    internalmailingSystem.Style.Add("display", "none");
                    webLinkSystem.Style.Add("display", "block");
                    divLayout.Style.Add("display", "block");
                    rbtnInternakAccessSetting.Visible = true;
                    rbtnInternakAccessSetting.Items.FindByValue("2").Enabled = false;
                    reqEmaillist.Enabled = false;
                    reqSubject.Enabled = false;
                    reqName.Enabled = false;
                    reqSenderEmail.Enabled = false;
                    regSenderEmail.Enabled = false;
                    reqReplyTo.Enabled = false;
                    regReplyto.Enabled = false;
                    reqEmaillist.ValidationGroup = "notval";
                    reqSubject.ValidationGroup = "notval";
                    reqName.ValidationGroup = "notval";
                    reqSenderEmail.ValidationGroup = "notval";
                    regSenderEmail.ValidationGroup = "notval";
                    reqReplyTo.ValidationGroup = "notval";
                    regReplyto.ValidationGroup = "notval";
                    reqInternalsettings.ValidationGroup = "notval";
                }
                bool IsBlockedWordExists = false;
                var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();

                var surveyOptionDetails = ServiceFactory.GetService<SurveyService>().GetSurveyOptions(base.SurveyBasicInfoView.SURVEY_ID);
                var surveyQuestions = ServiceFactory.GetService<QuestionService>().GetSurveyquestionsBySurveyId(base.SurveyBasicInfoView.SURVEY_ID);

                foreach (osm_blockedwords obw in blockedWords)
                {
                    for (int i = 0; i < surveyQuestions.Count; i++)
                    {
                        string[] appblockwords = surveyQuestions[i].QUSETION_LABEL.ToString().Split(' ');
                        foreach (string word in appblockwords)
                        {

                            string str1 = word.Replace("<p>", "");

                            string strblockword = str1.Replace("</p>", "");

                            if (strblockword.ToUpper() == obw.BLOCKEDWORD_NAME.ToUpper())
                            {

                                IsBlockedWordExists = true;
                            }
                        }
                    }
                }

                if (!IsBlockedWordExists)
                {
                    var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
                    var userActivationDetails = ServiceFactory<UsersService>.Instance.GetUserActivationFlag(userSessionDetails.UserId);
                    UpdateCreditCount(userSessionDetails.UserId);

                    if (userActivationDetails.ACTIVATION_FLAG == 1)
                    {

                        if (base.SurveyBasicInfoView.QuestionCount != null && base.SurveyBasicInfoView.QuestionCount > 0)
                        {
                            //if (base.SurveyBasicInfoView.SURVEY_INTRO != null)
                            //{
                                if (SurveyFlag == 0)
                                {
                                    SurveyBasicInfoView.EMAILLIST_TYPE = ValidationHelper.GetInteger(2, 0);
                                }
                                else
                                {
                                    SurveyBasicInfoView.EMAILLIST_TYPE = 2;
                                }
                                SurveyBasicInfoView.CONTACTLIST_ID = ValidationHelper.GetInteger(ddlEmailList.SelectedValue, 0);
                                if (userDetails != null)
                                {
                                    SurveyBasicInfoView.FROM_MAILADDRESS = userDetails.LoginName;
                                }
                                if (txtSenderEmail.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.FROM_MAILADDRESS = txtSenderEmail.Text.Trim();
                                }
                                if (txtSubject.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.MAIL_SUBJECT = txtSubject.Text.Trim();
                                }
                                if (txtSenderName.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.SENDER_NAME = txtSenderName.Text.Trim();
                                }
                                if (txtReplyTo.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.REPLY_TO_EMAILADDRESS = txtReplyTo.Text.Trim();
                                }
                                if (SurveyFlag == 0)
                                {

                                    navresurlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&Layout=" + rbtnLayout.SelectedItem.Value);

                                }
                                else
                                {
                                    navresurlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetOpenWidgetURL().Replace("~/", ""), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&Layout=" + rbtnLayout.SelectedItem.Value);
                                }
                                surveyLaunchURL = ConfigurationManager.AppSettings["RootURL"].ToString() + navresurlstr;
                                // Srini, 10/22/2013, Use Bitly Shortening service
                                try
                                {
                                    IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
                                    string shortened = s.Shorten(surveyLaunchURL);
                                    if (shortened != null)
                                    {
                                        surveyLaunchURL = shortened;
                                    }

                                }
                                catch (Exception)
                                {
                                }

                                SurveyBasicInfoView.SURVEY_URL = surveyLaunchURL;
                                SurveyBasicInfoView.SURVEYLAUNCH_URL = surveyLaunchURL;
                                SurveyBasicInfoView.SURVEYURL_TYPE = 1;
                                SurveyBasicInfoView.EMAIL_INVITATION = EditorQuestionIntro.Value.Replace("<p><em>(Insighto will pick the survey URL automatically)</em></p>", "");
                                SurveyBasicInfoView.LAUNCHOPTIONS_STATUS = 1;
                                SurveyBasicInfoView.SURVEY_LAYOUT = ValidationHelper.GetInteger(rbtnLayout.SelectedItem.Value, 0);
                              
                                if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_THROUGHINSIGHTO).Value == 0 && (applycreditval != "PPS_PRO" && applycreditval != "PPS_PREMIUM"))
                                {
                                    dsppstype = surcore.getSurveyType(base.SurveyID);
                                   
                                    if (userActivationDetails.ACTIVATION_FLAG == 0)
                                    {
                                        if (dsppstype.Tables[0].Rows[0][0].ToString() == null || dsppstype.Tables[0].Rows[0][0].ToString() == "FREE" || dsppstype.Tables[0].Rows[0][0].ToString() == "BASIC" || dsppstype.Tables[0].Rows[0][0].ToString() == "")
                                        {
                                            Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "' ,690, 500, 'yes');</script>");
                                            webLinkSystem.Style.Add("display", "block");
                                            divLayout.Style.Add("display", "block");
                                        }
                                    }
                                    else if (userActivationDetails.ACTIVATION_FLAG == 1)
                                    {
                                        SurveyBasicInfoView.UNIQUE_RESPONDENT = ValidationHelper.GetInteger(rbtnInternakAccessSetting.SelectedItem.Value, 0);
                                        if (launchtypenumber != 0)
                                        {
                                            ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0, "Active");
                                        }
                                        else
                                        {
                                            ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0);
                                        }
                                        if (SurveyBasicInfoView != null)
                                        {
                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "2");
                                            lblWebLaunchAlready.Visible = true;
                                            btnLaunchWeb1.Style.Add("display", "none");
                                            lnkWeblaunchother.Style.Add("display", "none");
                                            string surveyurl = surcore.getSurveyUrl(surveyId);
                                            txtWebLink.Text = surveyurl;
                                            web.Style.Add("display", "block");
                                            lblSurveyName.Text = SurveyBasicInfoView.SURVEY_NAME;
                                            prelabtn.Style.Add("display", "none");
                                            mysurvebtn.Style.Add("display", "block");
                                        }
                                    }
                                }
                                else
                                {
                                    if (base.IsSurveyActive)
                                    {
                                        ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0, "Active");

                                        if (applycreditval == "PPS_PRO")
                                        {
                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", base.SurveyBasicInfoView.STATUS);
                                        }
                                        else if (applycreditval == "PPS_PREMIUM")
                                        {
                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", base.SurveyBasicInfoView.STATUS);
                                        }

                                        if (SurveyBasicInfoView != null)
                                        {
                                            launchtypenumber = surcore.getLaunchTypeNumber(base.SurveyBasicInfoView.SURVEY_ID);
                                            launchnew = (LaunchMethod)launchtypenumber;
                                            if ((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr)
                                            {
                                                DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                            }
                                            else
                                            {
                                                DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "2");
                                            }
                                            lblWebLaunchAlready.Visible = true;
                                            btnLaunchWeb1.Style.Add("display", "none");
                                            lnkWeblaunchother.Style.Add("display", "none");
                                            string surveyurl = surcore.getSurveyUrl(surveyId);
                                            txtWebLink.Text = surveyurl;
                                            web.Style.Add("display", "block");
                                            lblSurveyName.Text = SurveyBasicInfoView.SURVEY_NAME;
                                            prelabtn.Style.Add("display", "none");
                                            mysurvebtn.Style.Add("display", "block");
                                        }
                                    }
                                    else
                                    {

                                        SurveyBasicInfoView.UNIQUE_RESPONDENT = ValidationHelper.GetInteger(rbtnInternakAccessSetting.SelectedItem.Value, 0);
                                        ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0);

                                        if (applycreditval == "PPS_PRO")
                                        {
                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", base.SurveyBasicInfoView.STATUS);
                                        }
                                        else if (applycreditval == "PPS_PREMIUM")
                                        {
                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", base.SurveyBasicInfoView.STATUS);
                                        }

                                        if (SurveyBasicInfoView != null)
                                        {
                                            launchtypenumber = surcore.getLaunchTypeNumber(base.SurveyBasicInfoView.SURVEY_ID);
                                            launchnew = (LaunchMethod)launchtypenumber;
                                            if ((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr)
                                            {
                                                DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                            }
                                            else
                                            {
                                                DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "2");
                                            }
                                            string surveyurl = surcore.getSurveyUrl(surveyId);
                                            txtWebLink.Text = surveyurl;
                                            web.Style.Add("display", "block");
                                            lblWebLaunchAlready.Visible = true;
                                            btnLaunchWeb1.Style.Add("display", "none");
                                            lnkWeblaunchother.Style.Add("display", "none");
                                            lblSurveyName.Text = SurveyBasicInfoView.SURVEY_NAME;
                                            prelabtn.Style.Add("display", "none");
                                            mysurvebtn.Style.Add("display", "block");
                                        }
                                    }
                                }
                            //}
                            //else
                            //{
                            //    lblErrMsg.Text = Utilities.ResourceMessage("errNoIntro");//"There is no introduction for this survey.";
                            //    lblErrMsg.Visible = true;
                            //    dvErrMsg.Visible = true;
                            //}

                        }
                        else
                        {
                            lblErrMsg.Text = Utilities.ResourceMessage("errNoQuestions");//"There is no questions in this survey.";
                            lblErrMsg.Visible = true;
                            dvErrMsg.Visible = true;
                        }

                    }
                    else
                    {
                        Page.RegisterStartupScript("Upgrade", @"<script>Activationalert(); </script>");
                    }
                }
                else
                {
                    lblErrMsg.Text = Utilities.ResourceMessage("errBlockedWords"); //"Introduction contains blocked words.";
                    lblErrMsg.Visible = true;
                    dvErrMsg.Visible = true;

                    var surveyFlag = 0;
                    var Layout = 0;

                    string fromEmail = ConfigurationManager.AppSettings["emailActivation"];

                    string Preview = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewURL().Replace("~/", ""), Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&Mode=" + RespondentDisplayMode.PreviewWithPagination.ToString() + "&surveyFlag=" + surveyFlag + "&Layout=" + Layout);

                    string prevurl = ConfigurationManager.AppSettings["RootURL"].ToString() + Preview;
                    var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();

                    string OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + userSessionDetails.LoginName);

                    string unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";

                    Sendblockemail("Blocked Survey", userSessionDetails.FirstName, userSessionDetails.LoginName, base.SurveyBasicInfoView.SURVEY_NAME, "Launch through web link", unsubscribe);

                }
            }

        protected void btnEmailLaunch_Click(object sender, EventArgs e)
        {
                bool check = true;
                bool IsBlockedWordExists = false;
                var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();
                var surveyOptionDetails = ServiceFactory.GetService<SurveyService>().GetSurveyOptions(base.SurveyBasicInfoView.SURVEY_ID);
                var surveyQuestions = ServiceFactory.GetService<QuestionService>().GetSurveyquestionsBySurveyId(base.SurveyBasicInfoView.SURVEY_ID);
                int launchtypenumber = surcore.getLaunchTypeNumber(base.SurveyBasicInfoView.SURVEY_ID);
                LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                foreach (osm_blockedwords obw in blockedWords)
                {
                    for (int i = 0; i < surveyQuestions.Count; i++)
                    {

                        string[] appblockwords = surveyQuestions[i].QUSETION_LABEL.ToString().Split(' ');

                        foreach (string word in appblockwords)
                        {

                            string str1 = word.Replace("<p>", "");

                            string strblockword = str1.Replace("</p>", "");

                            if (strblockword.ToUpper() == obw.BLOCKEDWORD_NAME.ToUpper())
                            {

                                IsBlockedWordExists = true;
                            }
                        }
                    }
                }
                if (!IsBlockedWordExists)
                {
                    var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
                    var userActivationDetails = ServiceFactory<UsersService>.Instance.GetUserActivationFlag(userSessionDetails.UserId);
                    UpdateCreditCount(userSessionDetails.UserId);

                    var licenceType = GenericHelper.ToEnum<UserType>(userSessionDetails.LicenseType);
                    var listFeatures = ServiceFactory.GetService<FeatureService>();

                    if (userActivationDetails.ACTIVATION_FLAG == 1)
                    {
                        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();

                        if (base.SurveyBasicInfoView.QuestionCount != null && base.SurveyBasicInfoView.QuestionCount > 0)
                        {
                            if (base.SurveyBasicInfoView.SURVEY_INTRO != null)
                            {

                                if (SurveyFlag == 0)
                                {
                                    SurveyBasicInfoView.EMAILLIST_TYPE = ValidationHelper.GetInteger(1, 0);
                                }
                                else
                                {
                                    SurveyBasicInfoView.EMAILLIST_TYPE = 2;
                                }
                                SurveyBasicInfoView.CONTACTLIST_ID = ValidationHelper.GetInteger(ddlEmailList.SelectedValue, 0);
                                if (userDetails != null)
                                {
                                    SurveyBasicInfoView.FROM_MAILADDRESS = userDetails.LoginName;
                                }
                                if (txtSenderEmail.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.FROM_MAILADDRESS = txtSenderEmail.Text.Trim();
                                }
                                if (txtSubject.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.MAIL_SUBJECT = txtSubject.Text.Trim();
                                }
                                if (txtSenderName.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.SENDER_NAME = txtSenderName.Text.Trim();
                                }
                                if (txtReplyTo.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.REPLY_TO_EMAILADDRESS = txtReplyTo.Text.Trim();
                                }
                                if (SurveyFlag == 0)
                                {
                                    // Srini, URL Shortening
                                    navresurlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID);

                                }
                                else
                                {
                                    navresurlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetOpenWidgetURL().Replace("~/", ""), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&Layout=" + rbtnLayout.SelectedItem.Value);
                                }
                                surveyLaunchURL = ConfigurationManager.AppSettings["RootURL"].ToString() + navresurlstr;
                                // Srini, 10/22/2013, Use Bitly Shortening service
                                IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
                                string shortened = s.Shorten(surveyLaunchURL);
                                if (shortened != null)
                                {
                                    surveyLaunchURL = shortened;
                                }

                                SurveyBasicInfoView.SURVEY_URL = surveyLaunchURL;
                                SurveyBasicInfoView.SURVEYLAUNCH_URL = surveyLaunchURL;
                                SurveyBasicInfoView.SURVEYURL_TYPE = 1;
                                SurveyBasicInfoView.EMAIL_INVITATION = EditorQuestionIntro.Value.Replace("<p><em>(Insighto will pick the survey URL automatically)</em></p>", "");
                                SurveyBasicInfoView.LAUNCHOPTIONS_STATUS = 1;
                                SurveyBasicInfoView.SURVEY_LAYOUT = ValidationHelper.GetInteger(rbtnLayout.SelectedItem.Value, 0);
                               
                                if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_THROUGHINSIGHTO).Value == 0 && (applycreditval != "PPS_PRO" && applycreditval != "PPS_PREMIUM"))
                                {
                                    DataSet dsppstype = surcore.getSurveyType(base.SurveyID);
                                    if (dsppstype.Tables[0].Rows[0][0].ToString() == null || dsppstype.Tables[0].Rows[0][0].ToString() == "FREE" || dsppstype.Tables[0].Rows[0][0].ToString() == "BASIC" || dsppstype.Tables[0].Rows[0][0].ToString() == "")
                                    {
                                        Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "' ,690, 500, 'yes');</script>");
                                        webLinkSystem.Style.Add("display", "block");
                                        divLayout.Style.Add("display", "block");
                                    }
                                }
                                else
                                {

                                    SurveyBasicInfoView.UNIQUE_RESPONDENT = ValidationHelper.GetInteger(rbtnInternakAccessSetting.SelectedItem.Value, 0);
                                    check = ServiceFactory.GetService<SurveyService>().CheckEmailLimit(SurveyBasicInfoView, EmailLimit);
                                    if (check)
                                    {

                                        var emalIds = ServiceFactory.GetService<EmailService>().GetEmailsByContactListId(ValidationHelper.GetInteger(ddlEmailList.SelectedValue, 0));
                                        if (emalIds.Count > 0)
                                        {
                                            if ((LaunchMethod.Emailthr & launchnew) == LaunchMethod.Emailthr)
                                            {
                                                if (base.IsSurveyActive)
                                                {
                                                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0, "Active");
                                                    if (applycreditval == "PPS_PRO")
                                                    {
                                                            // Launched through email once
                                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", base.SurveyBasicInfoView.STATUS);
                                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                                    }
                                                    else if (applycreditval == "PPS_PREMIUM")
                                                    {
                                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", base.SurveyBasicInfoView.STATUS);
                                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                                    }

                                                    if (SurveyBasicInfoView != null)
                                                    {
                                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                                            lblSurveyLable.Text = "You have Successfully Launched Survey through EmailList&nbsp;" + ddlEmailList.SelectedItem.Text;
                                                            BindContactLists(userDetails.UserId, base.SurveyBasicInfoView.SURVEY_ID);
                                                            lblSurveyLable.Visible = true;
                                                            lblSurveyName2.Text = SurveyBasicInfoView.SURVEY_NAME;
                                                            divSurveyLable.Style.Add("display", "block");
                                                            prelabtn.Style.Add("display", "none");
                                                            mysurvebtn.Style.Add("display", "block");
                                                            internalmailingSystem.Style.Add("display", "none");
                                                            rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "hidden");
                                                            if ((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr)
                                                            {
             
                                                                btnLaunchWeb1.Style.Add("display", "none");
                                                                lnkWeblaunchother.Style.Add("display", "none");
                                                            }
                                                            else
                                                            {
              
                                                                btnLaunchWeb1.Style.Add("display", "none");
                                                                lnkWeblaunchother.Style.Add("display", "block");
                                                            }
                                                    }
                                                }
                                                else
                                                {
                                                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0);

                                                    if (applycreditval == "PPS_PRO")
                                                    {
                                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", base.SurveyBasicInfoView.STATUS);
                                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                                    }
                                                    else if (applycreditval == "PPS_PREMIUM")
                                                    {

                                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", base.SurveyBasicInfoView.STATUS);
                                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                                    }

                                                    if (SurveyBasicInfoView != null)
                                                    {
                                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                                            lblSurveyLable.Text = "You have Successfully Launched your Survey through EmailList&nbsp;" + ddlEmailList.SelectedItem.Text;
                                                            BindContactLists(userDetails.UserId, base.SurveyBasicInfoView.SURVEY_ID);
                                                            lblSurveyLable.Visible =  true;
                                                            lblSurveyName2.Text = SurveyBasicInfoView.SURVEY_NAME;
                                                            divSurveyLable.Style.Add("display", "block");
                                                            prelabtn.Style.Add("display", "none");
                                                            mysurvebtn.Style.Add("display", "block");
                                                            internalmailingSystem.Style.Add("display", "none");
                                                            rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "hidden");
                                                            if ((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr)
                                                            {

                                                                btnLaunchWeb1.Style.Add("display", "none");
                                                                lnkWeblaunchother.Style.Add("display", "none");
                                                            }
                                                            else
                                                            {

                                                                btnLaunchWeb1.Style.Add("display", "none");
                                                                lnkWeblaunchother.Style.Add("display", "block");
                                                            }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (base.IsSurveyActive)
                                                {
                                                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0, "Active");
                                                    if (applycreditval == "PPS_PRO")
                                                    {
                                                        DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", base.SurveyBasicInfoView.STATUS);
                                                    }
                                                    else if (applycreditval == "PPS_PREMIUM")
                                                    {
                                                        DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", base.SurveyBasicInfoView.STATUS);
                                                    }

                                                    if (SurveyBasicInfoView != null)
                                                    {
                                                         launchtypenumber = surcore.getLaunchTypeNumber(base.SurveyBasicInfoView.SURVEY_ID);
                                                         launchnew = (LaunchMethod)launchtypenumber;
                                                         if ((LaunchMethod.Emailthr & launchnew) == LaunchMethod.Emailthr)
                                                         {
                                                         DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                                         }
                                                         else
                                                          {
                                                           DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "1");
                                                          }
                                                         lblSurveyLable.Text = "You have Successfully Launched your Survey through EmailList&nbsp;" + ddlEmailList.SelectedItem.Text;
                                                        BindContactLists(userDetails.UserId, base.SurveyBasicInfoView.SURVEY_ID);
                                                        lblSurveyLable.Visible = true;
                                                        lblSurveyName2.Text = SurveyBasicInfoView.SURVEY_NAME;
                                                        divSurveyLable.Style.Add("display", "block");
                                                        prelabtn.Style.Add("display", "none");
                                                        mysurvebtn.Style.Add("display", "block");
                                                        internalmailingSystem.Style.Add("display", "none");
                                                        rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "hidden");
                                                        if ((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr)
                                                        {

                                                            btnLaunchWeb1.Style.Add("display", "none");
                                                            lnkWeblaunchother.Style.Add("display", "none");
                                                        }
                                                        else
                                                        {

                                                            btnLaunchWeb1.Style.Add("display", "none");
                                                            lnkWeblaunchother.Style.Add("display", "block");
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0);
                                                    if (applycreditval == "PPS_PRO")
                                                    {
                                                        DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", base.SurveyBasicInfoView.STATUS);
                                                    }
                                                    else if (applycreditval == "PPS_PREMIUM")
                                                    {

                                                        DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", base.SurveyBasicInfoView.STATUS);
                                                    }

                                                    if (SurveyBasicInfoView != null)
                                                    {
                                                        launchtypenumber = surcore.getLaunchTypeNumber(base.SurveyBasicInfoView.SURVEY_ID);
                                                        launchnew = (LaunchMethod)launchtypenumber;
                                                        if ((LaunchMethod.Emailthr & launchnew) == LaunchMethod.Emailthr)
                                                        {
                                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                                        }
                                                        else
                                                        {
                                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "1");
                                                        }
                                                        lblSurveyLable.Text = "You have Successfully Launched your Survey through EmailList&nbsp;" + ddlEmailList.SelectedItem.Text;
                                                        BindContactLists(userDetails.UserId, base.SurveyBasicInfoView.SURVEY_ID);
                                                        lblSurveyLable.Visible = true;
                                                        lblSurveyName2.Text = SurveyBasicInfoView.SURVEY_NAME;
                                                        divSurveyLable.Style.Add("display", "block");
                                                        prelabtn.Style.Add("display", "none");
                                                        mysurvebtn.Style.Add("display", "block");
                                                        internalmailingSystem.Style.Add("display", "none");
                                                        rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "hidden");
                                                        if ((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr)
                                                        {

                                                            btnLaunchWeb1.Style.Add("display", "none");
                                                            lnkWeblaunchother.Style.Add("display", "none");
                                                        }
                                                        else
                                                        {

                                                            btnLaunchWeb1.Style.Add("display", "none");
                                                            lnkWeblaunchother.Style.Add("display", "block");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            lblErrMsg.Text = Utilities.ResourceMessage("errNoEmails");//"There are no emails in the selected list. Please select a new list / add emails to the existing list.";
                                            lblErrMsg.Visible = true;
                                            dvErrMsg.Visible = true;
                                        }

                                    }
                                    else
                                    {
                                        lblErrMsg.Text = Utilities.ResourceMessage("errLimitExceeded");//"Your Email Limit has Exceeded. Please ugrade your license with regard to get Additional Email Limit.";
                                        lblErrMsg.Visible = true;
                                        dvErrMsg.Visible = true;
                                    }
                                }
                            }
                            else
                            {
                                lblErrMsg.Text = Utilities.ResourceMessage("errNoIntro");//"There is no introduction for this survey.";
                                lblErrMsg.Visible = true;
                                dvErrMsg.Visible = true;
                            }
                        }
                        else
                        {
                            lblErrMsg.Text = Utilities.ResourceMessage("errNoQuestions");//"There is no questions in this survey.";
                            lblErrMsg.Visible = true;
                            dvErrMsg.Visible = true;
                        }
                    }
                    else
                    {
                        Page.RegisterStartupScript("Upgrade", @"<script>Activationalert(); </script>");
                    }
                }
                else
                {
                    lblErrMsg.Text = Utilities.ResourceMessage("errBlockedWords"); //"Introduction contains blocked words.";
                    lblErrMsg.Visible = true;
                    dvErrMsg.Visible = true;

                    var surveyFlag = 0;
                    var Layout = 0;

                    string fromEmail = ConfigurationManager.AppSettings["emailActivation"];

                    string Preview = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewURL().Replace("~/", ""), Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&Mode=" + RespondentDisplayMode.PreviewWithPagination.ToString() + "&surveyFlag=" + surveyFlag + "&Layout=" + Layout);

                    string prevurl = ConfigurationManager.AppSettings["RootURL"].ToString() + Preview;
                    var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();

                    string OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + userSessionDetails.LoginName);

                    string unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";

                    Sendblockemail("Blocked Survey", userSessionDetails.FirstName, userSessionDetails.LoginName, base.SurveyBasicInfoView.SURVEY_NAME, "Launch via Email through Insighto", unsubscribe);

                }
            }
        
        protected void btnLaunchEmbed_Click(object sender, EventArgs e)
        {
                Embed.Style.Add("display", "block");
                internalmailingSystem.Style.Add("display", "none");
                rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "hidden");
        }

        protected void btnGenerateIframe_click(object sender, EventArgs e)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();

            DataSet dsppstype = surcore.getSurveyType(base.SurveyID);
            dsppscredits = surcore.getCreditsCount(userDetails.UserId);

                if (applycreditval == "")
                {
                    applycreditval = dsppstype.Tables[0].Rows[0][0].ToString();
                }

                if ((applycreditval == "") || (applycreditval == "FREE"))
                {
                    imgchkfree.Style["visibility"] = "visible";
                    imgchkpro.Style["visibility"] = "hidden";
                    imgchkprem.Style["visibility"] = "hidden";
                }
                else if (applycreditval == "PPS_PRO")
                {
                    imgchkpro.Style["visibility"] = "visible";
                    imgchkfree.Style["visibility"] = "hidden";
                    imgchkprem.Style["visibility"] = "hidden";
                }
                else if (applycreditval == "PPS_PREMIUM")
                {
                    imgchkprem.Style["visibility"] = "visible";
                    imgchkfree.Style["visibility"] = "hidden";
                    imgchkpro.Style["visibility"] = "hidden";
                }
                dvErrMsg.Visible = false;
                lblErrMsg.Visible = false;
                dvtelesurvey.Visible = false;
                if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_THROUGHEXTERNAL).Value == 0)
                {
                    if (dsppstype.Tables[0].Rows[0][0].ToString() == null || dsppstype.Tables[0].Rows[0][0].ToString() == "FREE" || dsppstype.Tables[0].Rows[0][0].ToString() == "BASIC" || dsppstype.Tables[0].Rows[0][0].ToString() == "")
                    {
                        Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "' ,690, 500, 'yes');</script>");
                        webLinkSystem.Style.Add("display", "block");
                        divLayout.Style.Add("display", "block");
                    }
                }
                else
                {
                    EditorQuestionIntro.Value = "";
                    ddlEmailList.SelectedIndex = 0;
                    txtSenderEmail.Text = "";
                    txtSubject.Text = "";
                    txtSenderName.Text = "";
                    txtReplyTo.Text = "";
                 //   rbtnInternakAccessSetting.Items.FindByValue("1").Selected = true;
                    internalNote.Style.Add("display", "none");
                    externalNote.Style.Add("display", "none");
                    internalmailingSystem.Style.Add("display", "none");
                    webLinkSystem.Style.Add("display", "block");
                    divLayout.Style.Add("display", "block");
                    rbtnInternakAccessSetting.Visible = true;
                    reqEmaillist.Enabled = false;
                    reqSubject.Enabled = false;
                    reqName.Enabled = false;
                    reqSenderEmail.Enabled = false;
                    regSenderEmail.Enabled = false;
                    reqReplyTo.Enabled = false;
                    regReplyto.Enabled = false;
                    reqEmaillist.ValidationGroup = "notval";
                    reqSubject.ValidationGroup = "notval";
                    reqName.ValidationGroup = "notval";
                    reqSenderEmail.ValidationGroup = "notval";
                    regSenderEmail.ValidationGroup = "notval";
                    reqReplyTo.ValidationGroup = "notval";
                    regReplyto.ValidationGroup = "notval";
                    reqInternalsettings.ValidationGroup = "notval";
                }
                bool IsBlockedWordExists = false;
                var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();

                var surveyOptionDetails = ServiceFactory.GetService<SurveyService>().GetSurveyOptions(base.SurveyBasicInfoView.SURVEY_ID);
                var surveyQuestions = ServiceFactory.GetService<QuestionService>().GetSurveyquestionsBySurveyId(base.SurveyBasicInfoView.SURVEY_ID);
                foreach (osm_blockedwords obw in blockedWords)
                {
                    for (int i = 0; i < surveyQuestions.Count; i++)
                    {

                        string[] appblockwords = surveyQuestions[i].QUSETION_LABEL.ToString().Split(' ');

                        foreach (string word in appblockwords)
                        {

                            string str1 = word.Replace("<p>", "");
                            string strblockword = str1.Replace("</p>", "");
                            if (strblockword.ToUpper() == obw.BLOCKEDWORD_NAME.ToUpper())
                            {

                                IsBlockedWordExists = true;
                            }
                        }
                    }
                }

                if (!IsBlockedWordExists)
                {
                    var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
                    var userActivationDetails = ServiceFactory<UsersService>.Instance.GetUserActivationFlag(userSessionDetails.UserId);
                    UpdateCreditCount(userSessionDetails.UserId);
                    if (userActivationDetails.ACTIVATION_FLAG == 1)
                    {
                        if (base.SurveyBasicInfoView.QuestionCount != null && base.SurveyBasicInfoView.QuestionCount > 0)
                        {
                            if (base.SurveyBasicInfoView.SURVEY_INTRO != null)
                            {
                                if (SurveyFlag == 0)
                                {
                                    SurveyBasicInfoView.EMAILLIST_TYPE = ValidationHelper.GetInteger(2, 0);
                                }
                                else
                                {
                                    SurveyBasicInfoView.EMAILLIST_TYPE = 2;
                                }
                                SurveyBasicInfoView.CONTACTLIST_ID = ValidationHelper.GetInteger(ddlEmailList.SelectedValue, 0);
                                if (userDetails != null)
                                {
                                    SurveyBasicInfoView.FROM_MAILADDRESS = userDetails.LoginName;
                                }
                                if (txtSenderEmail.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.FROM_MAILADDRESS = txtSenderEmail.Text.Trim();
                                }
                                if (txtSubject.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.MAIL_SUBJECT = txtSubject.Text.Trim();
                                }
                                if (txtSenderName.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.SENDER_NAME = txtSenderName.Text.Trim();
                                }
                                if (txtReplyTo.Text.Length > 0)
                                {
                                    SurveyBasicInfoView.REPLY_TO_EMAILADDRESS = txtReplyTo.Text.Trim();
                                }
                                if (SurveyFlag == 0)
                                {
                                    navresurlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&Layout=" + rbtnLayout.SelectedItem.Value);

                                }
                                else
                                {
                                    navresurlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetOpenWidgetURL().Replace("~/", ""), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&Layout=" + rbtnLayout.SelectedItem.Value);
                                }
                                surveyLaunchURL = ConfigurationManager.AppSettings["RootURL"].ToString() + navresurlstr;
                                // Srini, 10/22/2013, Use Bitly Shortening service
                                IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
                                string shortened = s.Shorten(surveyLaunchURL);
                                if (shortened != null)
                                {
                                    surveyLaunchURL = shortened;
                                }

                                SurveyBasicInfoView.SURVEY_URL = surveyLaunchURL;
                                SurveyBasicInfoView.SURVEYLAUNCH_URL = surveyLaunchURL;
                                SurveyBasicInfoView.SURVEYURL_TYPE = 1;
                                SurveyBasicInfoView.EMAIL_INVITATION = EditorQuestionIntro.Value.Replace("<p><em>(Insighto will pick the survey URL automatically)</em></p>", "");
                                SurveyBasicInfoView.LAUNCHOPTIONS_STATUS = 1;
                                SurveyBasicInfoView.SURVEY_LAYOUT = ValidationHelper.GetInteger(rbtnLayout.SelectedItem.Value, 0);
                                string EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&Layout=" + rbtnLayout.SelectedItem.Value + "&Embed=" + "Emb");
                                surveyLaunchURL1 = ConfigurationManager.AppSettings["RootURL"].ToString() + EmbedUrl;
                                shortened = s.Shorten(surveyLaunchURL1);
                                if (shortened != null)
                                {
                                    surveyLaunchURL1 = shortened;
                                }
                          
                                txtIframe.Text = "<iframe id='IframeLaunch' height='" + txtHeightIframe.Text + "' width='" + txtWidthIframe.Text + "'  scrolling='auto' src='" + surveyLaunchURL1 + "' frameborder='0' ></iframe>";
                                int launchtypenumber = surcore.getLaunchTypeNumber(base.SurveyBasicInfoView.SURVEY_ID);
                                if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_THROUGHINSIGHTO).Value == 0 && (applycreditval != "PPS_PRO" && applycreditval != "PPS_PREMIUM"))
                                {
                                    dsppstype = surcore.getSurveyType(base.SurveyID);
                                    if (userActivationDetails.ACTIVATION_FLAG == 0)
                                    {
                                        if (dsppstype.Tables[0].Rows[0][0].ToString() == null || dsppstype.Tables[0].Rows[0][0].ToString() == "FREE" || dsppstype.Tables[0].Rows[0][0].ToString() == "BASIC" || dsppstype.Tables[0].Rows[0][0].ToString() == "")
                                        {
                                            Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "' ,690, 500, 'yes');</script>");
                                            webLinkSystem.Style.Add("display", "block");
                                            divLayout.Style.Add("display", "block");
                                        }
                                    }
                                    else if (userActivationDetails.ACTIVATION_FLAG == 1)
                                    {
                                            SurveyBasicInfoView.UNIQUE_RESPONDENT = ValidationHelper.GetInteger(rbtnInternakAccessSetting.SelectedItem.Value, 0);
                                            if (launchtypenumber != 0)
                                            {
                                                ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0, "Active");
                                            }
                                            else
                                            {
                                                ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0);
                                            }
                                        if (SurveyBasicInfoView != null)
                                        {
                                            launchtypenumber = surcore.getLaunchTypeNumber(base.SurveyBasicInfoView.SURVEY_ID);
                                            LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "4");
                                            surcore.updateembedcode(txtIframe.Text, base.SurveyBasicInfoView.SURVEY_ID);
                                            btnLaunchEmbed.Visible = false;
                                            lblSurveyName1.Text = SurveyBasicInfoView.SURVEY_NAME;
                                            lblEmbedLaunchedAlready.Visible = true;
                                            iframealert.Visible = true;
                                            prelabtn.Style.Add("display", "none");
                                            mysurvebtn.Style.Add("display", "block");
                                            if ((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr)
                                            {

                                                btnLaunchWeb1.Style.Add("display", "none");
                                                lnkWeblaunchother.Style.Add("display", "none");
                                            }
                                            else
                                            {

                                                btnLaunchWeb1.Style.Add("display", "none");
                                                lnkWeblaunchother.Style.Add("display", "block");
                                            }
                                        }
                                    }

                                }
                                else
                                {

                                    if (base.IsSurveyActive)
                                    {
                                        ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0, "Active");

                                        if (applycreditval == "PPS_PRO")
                                        {
                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", base.SurveyBasicInfoView.STATUS);
                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                        }
                                        else if (applycreditval == "PPS_PREMIUM")
                                        {
                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", base.SurveyBasicInfoView.STATUS);
                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "4");
                                        }

                                        if (SurveyBasicInfoView != null)
                                        {
                                            launchtypenumber = surcore.getLaunchTypeNumber(base.SurveyBasicInfoView.SURVEY_ID);
                                            LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                                            if ((LaunchMethod.Embedthr & launchnew) == LaunchMethod.Embedthr)
                                            {
                                                DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                            }
                                            else
                                            {
                                                DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "4");
                                            }
                                            surcore.updateembedcode(txtIframe.Text, base.SurveyBasicInfoView.SURVEY_ID);
                                            btnLaunchEmbed.Visible = false;
                                            lblSurveyName1.Text = SurveyBasicInfoView.SURVEY_NAME;
                                            lblEmbedLaunchedAlready.Visible = true;
                                            iframealert.Visible = true;
                                            prelabtn.Style.Add("display", "none");
                                            mysurvebtn.Style.Add("display", "block");
                                            if ((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr)
                                            {

                                                btnLaunchWeb1.Style.Add("display", "none");
                                                lnkWeblaunchother.Style.Add("display", "none");
                                            }
                                            else
                                            {

                                                btnLaunchWeb1.Style.Add("display", "none");
                                                lnkWeblaunchother.Style.Add("display", "block");
                                            }
                                        }
                                    }
                                    else
                                    {

                                        SurveyBasicInfoView.UNIQUE_RESPONDENT = ValidationHelper.GetInteger(rbtnInternakAccessSetting.SelectedItem.Value, 0);
                                        ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(SurveyBasicInfoView, 0);

                                        if (applycreditval == "PPS_PRO")
                                        {
                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", base.SurveyBasicInfoView.STATUS);
                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                        }
                                        else if (applycreditval == "PPS_PREMIUM")
                                        {
                                            DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", base.SurveyBasicInfoView.STATUS);
                                            DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "4");
                                        }

                                        if (SurveyBasicInfoView != null)
                                        {
                                            launchtypenumber = surcore.getLaunchTypeNumber(base.SurveyBasicInfoView.SURVEY_ID);
                                            LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                                            if ((LaunchMethod.Embedthr & launchnew) == LaunchMethod.Embedthr)
                                            {
                                                DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "0");
                                            }
                                            else
                                            {
                                                DataSet dslaunchopt = surcore.updatesurveylaunch(base.SurveyBasicInfoView.SURVEY_ID, "4");
                                            }
                                            surcore.updateembedcode(txtIframe.Text, base.SurveyBasicInfoView.SURVEY_ID);
                                            lblEmbedLaunchedAlready.Visible = true;
                                            btnLaunchEmbed.Visible = false;
                                            lblSurveyName1.Text = SurveyBasicInfoView.SURVEY_NAME;
                                            iframealert.Visible = true;
                                            prelabtn.Style.Add("display", "none");
                                            mysurvebtn.Style.Add("display", "block");
                                            if ((LaunchMethod.Webthr & launchnew) == LaunchMethod.Webthr)
                                            {

                                                btnLaunchWeb1.Style.Add("display", "none");
                                                lnkWeblaunchother.Style.Add("display", "none");
                                            }
                                            else
                                            {

                                                btnLaunchWeb1.Style.Add("display", "none");
                                                lnkWeblaunchother.Style.Add("display", "block");
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                lblErrMsg.Text = Utilities.ResourceMessage("errNoIntro");//"There is no introduction for this survey.";
                                lblErrMsg.Visible = true;
                                dvErrMsg.Visible = true;
                            }


                        }
                        else
                        {
                            lblErrMsg.Text = Utilities.ResourceMessage("errNoQuestions");//"There is no questions in this survey.";
                            lblErrMsg.Visible = true;
                            dvErrMsg.Visible = true;
                        }
                    }
                    else
                    {


                        Page.RegisterStartupScript("Upgrade", @"<script>Activationalert(); </script>");

                    }
                }
                else
                {
                    lblErrMsg.Text = Utilities.ResourceMessage("errBlockedWords"); //"Introduction contains blocked words.";
                    lblErrMsg.Visible = true;
                    dvErrMsg.Visible = true;

                    var surveyFlag = 0;
                    var Layout = 0;

                    string fromEmail = ConfigurationManager.AppSettings["emailActivation"];

                    string Preview = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewURL().Replace("~/", ""), Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&Mode=" + RespondentDisplayMode.PreviewWithPagination.ToString() + "&surveyFlag=" + surveyFlag + "&Layout=" + Layout);

                    string prevurl = ConfigurationManager.AppSettings["RootURL"].ToString() + Preview;
                    var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();

                    string OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + userSessionDetails.LoginName);

                    string unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";

                    Sendblockemail("Blocked Survey", userSessionDetails.FirstName, userSessionDetails.LoginName, base.SurveyBasicInfoView.SURVEY_NAME, "Launch Through Embed", unsubscribe);

                }
            }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetReportsURL(), "SurveyId=" + base.SurveyID + "&surveyFlag=" + SurveyFlag));
        }

        protected void btnDashBoard_Click(object sender, EventArgs e)
        {
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyManagerURL(), "SurveyId=" + base.SurveyID + "&surveyFlag=" + SurveyFlag));
        }

        protected void btnMySurvey_Click(object sender, EventArgs e)
        {
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + SurveyFlag));
        }

        protected void lblSurveyUrl_Click(object sender, EventArgs e)
        {
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyManagerURL(), "SurveyId=" + base.SurveyID + "&surveyFlag=" + SurveyFlag));
        }

        protected void lnkFb_Click(object sender, EventArgs e)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();

            DataSet dsppstype = surcore.getSurveyType(base.SurveyID);
            dsppscredits = surcore.getCreditsCount(userDetails.UserId);
            hdnaccess.Value = rbtnInternakAccessSetting.SelectedItem.Value;
            hdnlayout.Value = rbtnLayout.SelectedItem.Value;
            lblSurveyfb.Text = SurveyBasicInfoView.SURVEY_NAME;
            if (applycreditval == "")
            {
                applycreditval = dsppstype.Tables[0].Rows[0][0].ToString();
            }

            if ((applycreditval == "") || (applycreditval == "FREE"))
            {
                imgchkfree.Style["visibility"] = "visible";
                imgchkpro.Style["visibility"] = "hidden";
                imgchkprem.Style["visibility"] = "hidden";
            }
            else if (applycreditval == "PPS_PRO")
            {
                imgchkpro.Style["visibility"] = "visible";
                imgchkfree.Style["visibility"] = "hidden";
                imgchkprem.Style["visibility"] = "hidden";
            }
            else if (applycreditval == "PPS_PREMIUM")
            {
                imgchkprem.Style["visibility"] = "visible";
                imgchkfree.Style["visibility"] = "hidden";
                imgchkpro.Style["visibility"] = "hidden";
            }
            dvErrMsg.Visible = false;
            lblErrMsg.Visible = false;
            dvtelesurvey.Visible = false;
            if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_THROUGHEXTERNAL).Value == 0)
            {
                if (dsppstype.Tables[0].Rows[0][0].ToString() == null || dsppstype.Tables[0].Rows[0][0].ToString() == "FREE" || dsppstype.Tables[0].Rows[0][0].ToString() == "BASIC" || dsppstype.Tables[0].Rows[0][0].ToString() == "")
                {
                    Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "' ,690, 500, 'yes');</script>");
                    webLinkSystem.Style.Add("display", "block");
                    divLayout.Style.Add("display", "block");
                }
            }
            else
            {
                EditorQuestionIntro.Value = "";
                ddlEmailList.SelectedIndex = 0;
                txtSenderEmail.Text = "";
                txtSubject.Text = "";
                txtSenderName.Text = "";
                txtReplyTo.Text = "";
           //     rbtnInternakAccessSetting.Items.FindByValue("1").Selected = true;
                internalNote.Style.Add("display", "none");
                externalNote.Style.Add("display", "none");
                internalmailingSystem.Style.Add("display", "none");
                webLinkSystem.Style.Add("display", "block");
                divLayout.Style.Add("display", "block");
                rbtnInternakAccessSetting.Visible = true;
                rbtnInternakAccessSetting.Items.FindByValue("2").Enabled = false;
                reqEmaillist.Enabled = false;
                reqSubject.Enabled = false;
                reqName.Enabled = false;
                reqSenderEmail.Enabled = false;
                regSenderEmail.Enabled = false;
                reqReplyTo.Enabled = false;
                regReplyto.Enabled = false;
                reqEmaillist.ValidationGroup = "notval";
                reqSubject.ValidationGroup = "notval";
                reqName.ValidationGroup = "notval";
                reqSenderEmail.ValidationGroup = "notval";
                regSenderEmail.ValidationGroup = "notval";
                reqReplyTo.ValidationGroup = "notval";
                regReplyto.ValidationGroup = "notval";
                reqInternalsettings.ValidationGroup = "notval";
            }
            bool IsBlockedWordExists = false;
            var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();

            var surveyOptionDetails = ServiceFactory.GetService<SurveyService>().GetSurveyOptions(base.SurveyBasicInfoView.SURVEY_ID);
            var surveyQuestions = ServiceFactory.GetService<QuestionService>().GetSurveyquestionsBySurveyId(base.SurveyBasicInfoView.SURVEY_ID);
            foreach (osm_blockedwords obw in blockedWords)
            {
                for (int i = 0; i < surveyQuestions.Count; i++)
                {

                    string[] appblockwords = surveyQuestions[i].QUSETION_LABEL.ToString().Split(' ');

                    foreach (string word in appblockwords)
                    {

                        string str1 = word.Replace("<p>", "");
                        string strblockword = str1.Replace("</p>", "");
                        if (strblockword.ToUpper() == obw.BLOCKEDWORD_NAME.ToUpper())
                        {

                            IsBlockedWordExists = true;
                        }
                    }
                }
            }
            if (!IsBlockedWordExists)
            {
                var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
                var userActivationDetails = ServiceFactory<UsersService>.Instance.GetUserActivationFlag(userSessionDetails.UserId);
                if (userActivationDetails.ACTIVATION_FLAG == 1)
                {
                    if (base.SurveyBasicInfoView.QuestionCount != null && base.SurveyBasicInfoView.QuestionCount > 0)
                    {
                        if (base.SurveyBasicInfoView.SURVEY_INTRO != null)
                        {
                            
                            if (SurveyFlag == 0)
                            {
                                navresurlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&Layout=" + rbtnLayout.SelectedItem.Value);

                            }
                            else
                            {
                                navresurlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetOpenWidgetURL().Replace("~/", ""), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&Layout=" + rbtnLayout.SelectedItem.Value);
                            }
                            surveyLaunchURL = ConfigurationManager.AppSettings["RootURL"].ToString() + navresurlstr;
                            // Srini, 10/22/2013, Use Bitly Shortening service
                            IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
                            string shortened = s.Shorten(surveyLaunchURL);
                            if (shortened != null)
                            {
                                surveyLaunchURL = shortened;
                            }
                            hdnTestValue.Value = surveyLaunchURL;

                            string EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&Layout=" + rbtnLayout.SelectedItem.Value + "&Embed=" + "FB");
                            surveyLaunchURL1 = ConfigurationManager.AppSettings["RootURL"].ToString() + EmbedUrl;
                            shortened = s.Shorten(surveyLaunchURL1);
                            if (shortened != null)
                            {
                                surveyLaunchURL1 = shortened;
                            }
                            hdnFbUrl.Value = surveyLaunchURL1;
                            var str = SurveyBasicInfoView.SURVEY_INTRO;
                            var charsToRemove = new string[] { "<p>", "</p>" };
                            foreach (var c in charsToRemove)
                            {
                                str = str.Replace(c, string.Empty);
                            }
                            hdnIntoNew.Value = str;
                         
                            hdnSurveyName.Value = base.SurveyBasicInfoView.SURVEY_NAME;
                            int launchtypenumber = surcore.getLaunchTypeNumber(base.SurveyBasicInfoView.SURVEY_ID);
                            if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_THROUGHINSIGHTO).Value == 0 && (applycreditval != "PPS_PRO" && applycreditval != "PPS_PREMIUM"))
                            {
                                dsppstype = surcore.getSurveyType(base.SurveyID);
                                if (userActivationDetails.ACTIVATION_FLAG == 0)
                                {
                                    if (dsppstype.Tables[0].Rows[0][0].ToString() == null || dsppstype.Tables[0].Rows[0][0].ToString() == "FREE" || dsppstype.Tables[0].Rows[0][0].ToString() == "BASIC" || dsppstype.Tables[0].Rows[0][0].ToString() == "")
                                    {
                                        Page.RegisterStartupScript("Upgrade", @"<script>OpenModalPopUP('" + PathHelper.GetUpgradeLicenseURL().Replace("~/", "") + "' ,690, 500, 'yes');</script>");
                                        webLinkSystem.Style.Add("display", "block");
                                        divLayout.Style.Add("display", "block");
                                    }
                                }
                                else if (userActivationDetails.ACTIVATION_FLAG == 1)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "postToFB();", true);
                                }
                            }
                            else
                            {

                                if (base.IsSurveyActive)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "postToFB();", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "postToFB();", true);
                                }
                            }
                        }
                        else
                        {
                            lblErrMsg.Text = Utilities.ResourceMessage("errNoIntro");//"There is no introduction for this survey.";
                            lblErrMsg.Visible = true;
                            dvErrMsg.Visible = true;
                        }


                    }
                    else
                    {
                        lblErrMsg.Text = Utilities.ResourceMessage("errNoQuestions");//"There is no questions in this survey.";
                        lblErrMsg.Visible = true;
                        dvErrMsg.Visible = true;
                    }
                }
                else
                {


                    Page.RegisterStartupScript("Upgrade", @"<script>Activationalert(); </script>");

                }
            }
            else
            {
                lblErrMsg.Text = Utilities.ResourceMessage("errBlockedWords"); //"Introduction contains blocked words.";
                lblErrMsg.Visible = true;
                dvErrMsg.Visible = true;

                var surveyFlag = 0;
                var Layout = 0;

                string fromEmail = ConfigurationManager.AppSettings["emailActivation"];

                string Preview = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondentPreviewURL().Replace("~/", ""), Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&Mode=" + RespondentDisplayMode.PreviewWithPagination.ToString() + "&surveyFlag=" + surveyFlag + "&Layout=" + Layout);

                string prevurl = ConfigurationManager.AppSettings["RootURL"].ToString() + Preview;
                var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();

                string OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + userSessionDetails.LoginName);

                string unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";

                Sendblockemail("Blocked Survey", userSessionDetails.FirstName, userSessionDetails.LoginName, base.SurveyBasicInfoView.SURVEY_NAME, "Launch Through Embed", unsubscribe);

            }
        }

        [WebMethod]
        public static string MyMethod(string name, string shareurl, int layout, int access, int surveyid)
        {
            string result = name;
            SurveyCore surcore = new SurveyCore();
            int launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
            SurveyPageBase sp = new SurveyPageBase();
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            UserType userType = Utilities.GetUserType();

            DataSet dsppstype = surcore.getSurveyType(surveyid);
            DataSet dsstatus = surcore.getSurveystatus(surveyid);
            string status = dsstatus.Tables[0].Rows[0][0].ToString();
            string launchcnt = dsppstype.Tables[2].Rows[0][0].ToString();
            int launchcountval = Convert.ToInt32(launchcnt);
            string applycreditval= "";
            if (applycreditval == "")
            {
                applycreditval = dsppstype.Tables[0].Rows[0][0].ToString();
            }
            if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_THROUGHINSIGHTO).Value == 0 && (applycreditval != "PPS_PRO" && applycreditval != "PPS_PREMIUM"))
            {
                if (launchtypenumber != 0)
                {
                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptionswithId(surveyid, access, layout, shareurl, 0, "Active");
                }
                else
                {
                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptionswithId(surveyid, access, layout, shareurl, 0);
                }

                    launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
                    LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                    if ((LaunchMethod.Fbthr & launchnew) == LaunchMethod.Fbthr)
                    {
                        DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "0");
                    }
                    else
                    {
                        DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "8");

                    }
            }
            else
            {

                if (status=="Active")
                {

                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptionswithId(surveyid, access, layout, shareurl, 0, "Active");
                    if (applycreditval == "PPS_PRO")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(userInfo.UserId, "PPS_PRO", surveyid, "PRO_YEARLY", "Active");
                    }
                    else if (applycreditval == "PPS_PREMIUM")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(userInfo.UserId, "PPS_PREMIUM", surveyid, "PREMIUM_YEARLY", "Active");
                    }

                        launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
                        LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                        if ((LaunchMethod.Fbthr & launchnew) == LaunchMethod.Fbthr)
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "0");
                        }
                        else
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "8");
                        }
                }
                else
                {
                    //SurveyBasicInfoView.UNIQUE_RESPONDENT = ValidationHelper.GetInteger(rbtnInternakAccessSetting.SelectedItem.Value, 0);
                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptionswithId(surveyid, access, layout, shareurl, 0);

                    if (applycreditval == "PPS_PRO")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(userInfo.UserId, "PPS_PRO", surveyid , "PRO_YEARLY", "Active");
                    }
                    else if (applycreditval == "PPS_PREMIUM")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(userInfo.UserId, "PPS_PREMIUM", surveyid, "PREMIUM_YEARLY", "Active");
                    }

                        launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
                        LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                        if ((LaunchMethod.Fbthr & launchnew) == LaunchMethod.Fbthr)
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "0");
                        }
                        else
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid , "8");
                        }
                }
            }
            result = "success";
            return result;
        }

        [WebMethod]
        public static string twittermethod(string name, string shareurl,int surveyid,int layout, int access)
        {
            string result = name;
            SurveyCore surcore = new SurveyCore();
            int launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
            SurveyPageBase sp = new SurveyPageBase();
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            UserType userType = Utilities.GetUserType();
            string navresurlstr = "";
            string surveyLaunchURL = "";
            navresurlstr = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "SurveyId=" + surveyid + "&Layout=" + layout);
            surveyLaunchURL = ConfigurationManager.AppSettings["RootURL"].ToString() + navresurlstr;
            // Srini, 10/22/2013, Use Bitly Shortening service
            IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
            string shortened = s.Shorten(surveyLaunchURL);
            if (shortened != null)
            {
                surveyLaunchURL = shortened;
            }
            DataSet dsppstype = surcore.getSurveyType(surveyid);
            string launchcnt = dsppstype.Tables[2].Rows[0][0].ToString();
            int launchcountval = Convert.ToInt32(launchcnt);
            DataSet dsstatus = surcore.getSurveystatus(surveyid);
            string status = dsstatus.Tables[0].Rows[0][0].ToString();
            string applycreditval = "";
            if (applycreditval == "")
            {
                applycreditval = dsppstype.Tables[0].Rows[0][0].ToString();
            }
            if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_THROUGHINSIGHTO).Value == 0 && (applycreditval != "PPS_PRO" && applycreditval != "PPS_PREMIUM"))
            {
                if (launchtypenumber != 0)
                {
                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptionswithId(surveyid,access,layout,surveyLaunchURL, 0, "Active");
                }
                else
                {
                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptionswithId(surveyid, access, layout, surveyLaunchURL, 0);
                }

                    launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
                    LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                    if ((LaunchMethod.Twthr & launchnew) == LaunchMethod.Twthr)
                    {
                        DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "0");
                    }
                    else
                    {
                        DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "16");
                    }
            }
            else
            {
                if (status=="Active")
                {

                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptionswithId(surveyid, access, layout, surveyLaunchURL, 0, "Active");
                    if (applycreditval == "PPS_PRO")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(userInfo.UserId, "PPS_PRO", surveyid, "PRO_YEARLY", "Active");
                    }
                    else if (applycreditval == "PPS_PREMIUM")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(userInfo.UserId, "PPS_PREMIUM", surveyid, "PREMIUM_YEARLY", "Active");
                    }

                        launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
                        LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                        if ((LaunchMethod.Twthr & launchnew) == LaunchMethod.Twthr)
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "0");
                        }
                        else
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "16");
                        }
                }
                else
                {
                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptionswithId(surveyid, access, layout, surveyLaunchURL, 0);

                    if (applycreditval == "PPS_PRO")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(userInfo.UserId, "PPS_PRO", surveyid, "PRO_YEARLY", "Active");
                    }
                    else if (applycreditval == "PPS_PREMIUM")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(userInfo.UserId, "PPS_PREMIUM", surveyid, "PREMIUM_YEARLY", "Active");
                    }

                        launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
                        LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                        if ((LaunchMethod.Twthr & launchnew) == LaunchMethod.Twthr)
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "0");
                        }
                        else
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "16");
                        }
                }
            }
            result = "success";
            return result;
        }

        [WebMethod]
        public static string twitterfun(int surveyid, int layout, string access)
        {
            SurveyCore surcore = new SurveyCore();
            int launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
            SurveyPageBase sp = new SurveyPageBase();
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            UserType userType = Utilities.GetUserType();
            string surveyLaunchURL = "";
            string EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "SurveyId=" + surveyid + "&Layout=" + layout + "&Embed=" + "Tw");
            surveyLaunchURL = ConfigurationManager.AppSettings["RootURL"].ToString() + EmbedUrl;
            IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
            string shortened = s.Shorten(surveyLaunchURL);
            if (shortened != null)
            {
                surveyLaunchURL = shortened;
            }
            var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
            var userActivationDetails = ServiceFactory<UsersService>.Instance.GetUserActivationFlag(userSessionDetails.UserId);
            int questioncount = surcore.QuestionCount(surveyid);
            if (questioncount != null && questioncount > 0)
            {
                if (userActivationDetails.ACTIVATION_FLAG == 1)
                {
                    return surveyLaunchURL;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return "fgh";
            }
        }

        [WebMethod]
        public static string linkedin(int surveyid, int layout)
        {
            SurveyCore surcore = new SurveyCore();
            int launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
            SurveyPageBase sp = new SurveyPageBase();
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            UserType userType = Utilities.GetUserType();
            string surveyLaunchURL = "";
            string EmbedUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "SurveyId=" + surveyid + "&Layout=" + layout + "&Embed=" + "Ln");
            surveyLaunchURL = ConfigurationManager.AppSettings["RootURL"].ToString() + EmbedUrl;
            IBitlyService s = new BitlyService("sparuchu", "R_a0576ab532ab0503656bb0a695d51395");
            string shortened = s.Shorten(surveyLaunchURL);
            if (shortened != null)
            {
                surveyLaunchURL = shortened;
            }
            var userSessionDetails = ServiceFactory<SessionStateService>.Instance.GetLoginUserDetailsSession();
            var userActivationDetails = ServiceFactory<UsersService>.Instance.GetUserActivationFlag(userSessionDetails.UserId);
            if (sp.SurveyBasicInfoViewWithId(surveyid).QuestionCount != null && sp.SurveyBasicInfoViewWithId(surveyid).QuestionCount > 0)
            {
                if (userActivationDetails.ACTIVATION_FLAG == 1)
                {
                    return surveyLaunchURL;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return "fgh";
            }
        }

        protected void rbtnLayout_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnlayout.Value = rbtnLayout.SelectedItem.Value;
        }

        protected void rbtnInternakAccessSetting_SelectedIndexChanged(object sender, EventArgs e)
        {
            //hdnaccess.Value = rbtnInternakAccessSetting.SelectedItem.Value;
            //if (rbtnInternakAccessSetting.SelectedItem.Value == "2")
            //{
            //    internalmailingSystem.Style.Add("display", "block");
            //    rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "show");
            //}
            hdnaccess.Value = rbtnInternakAccessSetting.SelectedItem.Value;
            if (internalmailingSystem.Style["display"] == "block")
            {
                internalmailingSystem.Style.Add("display", "block");
                rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "show");
            }
            else
            {
                internalmailingSystem.Style.Add("display", "none");
                rbtnInternakAccessSetting.Items[2].Attributes.Add("class", "hidden");
            }
        }

        [WebMethod]
        public static string LinkedinLaunch(string name, string shareurl, int layout, int access, int surveyid)
        {
            string result = name;
            SurveyCore surcore = new SurveyCore();
            int launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
            SurveyPageBase sp = new SurveyPageBase();
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userInfo = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userInfo.LicenseType);
            var listFeatures = ServiceFactory.GetService<FeatureService>();
            UserType userType = Utilities.GetUserType();
            sp.SurveyBasicInfoViewWithId(surveyid).SURVEYLAUNCH_URL = shareurl;
            sp.SurveyBasicInfoViewWithId(surveyid).SURVEY_URL = shareurl;
            sp.SurveyBasicInfoViewWithId(surveyid).CONTACTLIST_ID = ValidationHelper.GetInteger(0, 0);
            sp.SurveyBasicInfoViewWithId(surveyid).MAIL_SUBJECT = "";
            sp.SurveyBasicInfoViewWithId(surveyid).EMAIL_INVITATION = "";
            sp.SurveyBasicInfoViewWithId(surveyid).SURVEYURL_TYPE = 2;
            sp.SurveyBasicInfoViewWithId(surveyid).LAUNCHOPTIONS_STATUS = 1;
            sp.SurveyBasicInfoViewWithId(surveyid).SURVEY_LAYOUT = layout;
            sp.SurveyBasicInfoViewWithId(surveyid).UNIQUE_RESPONDENT = access;
            sp.SurveyBasicInfoViewWithId(surveyid).EMAILLIST_TYPE = 2;

            DataSet dsppstype = surcore.getSurveyType(surveyid);

            string launchcnt = dsppstype.Tables[2].Rows[0][0].ToString();
            int launchcountval = Convert.ToInt32(launchcnt);
            string applycreditval = "";
            if (applycreditval == "")
            {
                applycreditval = dsppstype.Tables[0].Rows[0][0].ToString();
            }
            if (listFeatures.Find(licenceType, FeatureDeploymentType.LAUNCH_THROUGHINSIGHTO).Value == 0 && (applycreditval != "PPS_PRO" && applycreditval != "PPS_PREMIUM"))
            {
                if (launchtypenumber != 0)
                {
                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(sp.SurveyBasicInfoViewWithId(surveyid), 0, "Active");
                }
                else
                {
                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(sp.SurveyBasicInfoViewWithId(surveyid), 0);
                }
                if (sp.SurveyBasicInfoViewWithId(surveyid) != null)
                {
                    launchtypenumber = surcore.getLaunchTypeNumber(sp.SurveyBasicInfoViewWithId(surveyid).SURVEY_ID);
                    LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                    if ((LaunchMethod.Linthr & launchnew) == LaunchMethod.Linthr)
                    {
                        DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "0");
                    }
                    else
                    {
                        DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "32");

                    }
                }
            }
            else
            {

                if (sp.IsSurveyActiveFb(surveyid))
                {

                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(sp.SurveyBasicInfoViewWithId(surveyid), 0, "Active");
                    if (applycreditval == "PPS_PRO")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(sp.SurveyBasicInfoViewWithId(surveyid).USERID, "PPS_PRO", surveyid, "PRO_YEARLY", sp.SurveyBasicInfoViewWithId(surveyid).STATUS);
                    }
                    else if (applycreditval == "PPS_PREMIUM")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(sp.SurveyBasicInfoViewWithId(surveyid).USERID, "PPS_PREMIUM", surveyid, "PREMIUM_YEARLY", sp.SurveyBasicInfoViewWithId(surveyid).STATUS);
                    }

                    if (sp.SurveyBasicInfoViewWithId(surveyid) != null)
                    {
                        launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
                        LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                        if ((LaunchMethod.Linthr & launchnew) == LaunchMethod.Linthr)
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "0");
                        }
                        else
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "32");
                        }
                    }
                }
                else
                {
                    //SurveyBasicInfoView.UNIQUE_RESPONDENT = ValidationHelper.GetInteger(rbtnInternakAccessSetting.SelectedItem.Value, 0);
                    ServiceFactory.GetService<SurveyService>().UpdateSurveyLaunchOptions(sp.SurveyBasicInfoViewWithId(surveyid), 0);

                    if (applycreditval == "PPS_PRO")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(sp.SurveyBasicInfoViewWithId(surveyid).USERID, "PPS_PRO", surveyid, "PRO_YEARLY", sp.SurveyBasicInfoViewWithId(surveyid).STATUS);
                    }
                    else if (applycreditval == "PPS_PREMIUM")
                    {
                        DataSet dsproupdate = surcore.updatesurveyType(sp.SurveyBasicInfoViewWithId(surveyid).USERID, "PPS_PREMIUM", surveyid, "PREMIUM_YEARLY", sp.SurveyBasicInfoViewWithId(surveyid).STATUS);
                    }

                    if (sp.SurveyBasicInfoViewWithId(surveyid) != null)
                    {
                        launchtypenumber = surcore.getLaunchTypeNumber(surveyid);
                        LaunchMethod launchnew = (LaunchMethod)launchtypenumber;
                        if ((LaunchMethod.Linthr & launchnew) == LaunchMethod.Linthr)
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "0");
                        }
                        else
                        {
                            DataSet dslaunchopt = surcore.updatesurveylaunch(surveyid, "32");
                        }
                    }
                }
            }
            result = "success";
            return result;
        }

        protected void UpdateCreditCount(int userid) {

            //THIS WAS MOVED OUT FROM THE PAGE_LOAD AND PUT HERE AS OTHERWISE THIS GETS FIRED EVERYTIME AND CAUSES PROBLEMS IN THE CREDIT COUNT
            //SATISH 02-OCT-2015
            DataSet dscredits = surcore.getCreditsCount(userid);

            if ((applycreditval == "PPS_PRO") && (dscredits.Tables[0].Rows.Count > 0))
            {
                //DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", base.SurveyBasicInfoView.STATUS);
                DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PRO", base.SurveyBasicInfoView.SURVEY_ID, "PRO_YEARLY", "Active");
            }
            else if ((applycreditval == "PPS_PREMIUM") && (dscredits.Tables[1].Rows.Count > 0))
            {
                //DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", base.SurveyBasicInfoView.STATUS);
                DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "PPS_PREMIUM", base.SurveyBasicInfoView.SURVEY_ID, "PREMIUM_YEARLY", "Active");
            }
            else if (applycreditval == "FREE")
            {
                //DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "FREE", base.SurveyBasicInfoView.SURVEY_ID, "FREE", base.SurveyBasicInfoView.STATUS);
                DataSet dsproupdate = surcore.updatesurveyType(base.SurveyBasicInfoView.USERID, "FREE", base.SurveyBasicInfoView.SURVEY_ID, "FREE", "Active");
            }

        }
    }

}