﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="AddressBook.aspx.cs" Inherits="Insighto.Pages.AddressBook"%>

<%@ Register Src="UserControls/AddressBookSideNav.ascx" TagName="AddressBookSideNav"
    TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="contentPanelHeader">
        <!-- form header panel -->
        <div class="pageTitle">
            <!-- page title -->
            <asp:Label ID="lblTitle" runat="server" Text="ADDRESS BOOK" 
                meta:resourcekey="lblTitleResource2"></asp:Label>
            <!-- //page title -->
        </div>
        <!-- //form header panel -->
    </div>
    <div class="clear">
    </div>
    <div class="contentPanel">
        <!-- content panel -->
        <div class="surveyCreateTextPanel">
            <!-- survey created content panel -->
            <div class="surveyCreateLeftMenu">
                <!-- survey left menu -->
                <!-- //survey left menu -->
                <uc1:AddressBookSideNav ID="AddressBookSideNav1" runat="server" />
            </div>
            <div class="surveyQuestionPanel">
                <!-- survey question panel -->
                <div class="errorPanel" id="recordDeleted" style="display: none;">
                    <span class="successMesg">
                        <asp:Label ID="lblDeleted" runat="server" 
                        Text="Record deleted successfully" meta:resourcekey="lblDeletedResource2"></asp:Label></span>
                </div>
                <div id="ManageEmailList">
                    <div style="padding: 1px 10px 20px 10px;" id="Div2">
                        <div style="padding: 1px 0 8px 0;text-align: right;">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="leftAlign">
                                        <div class="informationPanel">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <p class="fontSize">
                                                            <asp:Label ID="lblHelp" runat="server" 
                                                                Text="Create new email list or view and manage any existing list." 
                                        ></asp:Label>
                                                            &nbsp;</p>
                                                    </td>
                                                    <td>
                                                        <span><a href="#" class="helpLink2" onclick="javascript: window.open('help/5_2.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;"
                                                            title="Help">&nbsp</a></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="float: right">
                                            
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div>
                            &nbsp;
                            </div>
                            <table width="100%">
                            <tr>
                            <td>
                            <asp:HyperLink ID="hlnkAddList" CssClass="dynamicHyperLink" h="250"
                                                w="650" scrolling='yes' title='Copy' 
                                                Style="float: left; margin-left: 5px;" onclick='javascript: return ApplyFrameBox(this);' 
                                                ToolTip="Create / Manage Email List" runat="server" Text="Create New Email List" 
                                                ></asp:HyperLink>

                            
                            </td>
                            <td  align="right">
                            <asp:Button ID="btnBacktoLaunchPage" Visible="False" OnClick="btnBacktoLaunchPage_Click"
                                CssClass="prelaunchbackbtn" Text="Back to Launch Page" 
                                Style="float:right" ToolTip="Back to Launch Page"
                                runat="server" meta:resourcekey="btnBacktoLaunchPageResource1" />
                            </td>
                            </tr>
                            </table>
                        </div>
                        <div class="successPanel" id="dvSuccessMsg" runat="server" style="display: none;">
                            <div>
                                <asp:Label ID="lblSuccessMsg" CssClass="successMsg" runat="server" Style="display: none;"
                                    Text="Email list deleted successfully." 
                                    meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
                        </div>
                        <div class="defaultH">
                        </div>
                        <div id="ptoolbar">
                        </div>
                        <table id="tblAddressBook">
                        </table>
                        <div class="defaultH">
                        </div>
                        <div style="float: right">
                            <asp:HiddenField ID="hdnContactId" runat="server" />
                            
                            <asp:Button ID="btnBacktoManager" Visible="False" OnClick="btnBacktoManager_Click"
                                CssClass="prelaunchbackbtn" Text="Survey Manager" ToolTip="Back To Survey Manager Page"
                                runat="server" meta:resourcekey="btnBacktoManagerResource1" />
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <!-- //survey question panel -->
            </div>
            <div class="clear">
            </div>
            <!-- //survey created content panel -->
        </div>
        <div class="clear">
        </div>
        <!-- //content panel -->
    </div>    
    <script type="text/javascript">
        var serviceUrl = 'AjaxService.aspx';
        $(document).ready(function () {
            $("#tblAddressBook").jqGrid({
                url: serviceUrl,
                postData: { method: "GetAddressBook" },
                datatype: 'json',
                colNames: ['Name Of Email List', 'Email Count', 'Created', 'Modified', '', '', ''],
                colModel: [
                { name: 'contactUrl', index: 'CONTACTLIST_NAME', width: 100, resizable: false },
                { name: 'NO_EMAILIDS', index: 'NO_EMAILIDS', width: 45, align: 'center', resizable: false },
                { name: 'CreatedDate', index: 'CreatedDate', width: 50, align: 'center' },
                { name: 'ModifiedDate', index: 'ModifiedDate', width: 60, align: 'center', resizable: false },
                { name: 'editUrl', index: 'editUrl', width: 15, resizable: false, align: 'center', formatter: EditLinkFormatter, sortable: false },
                { name: 'CONTACTLIST_ID', index: 'CONTACTLIST_ID', width: 15, align: 'center', resizable: false, formatter: DeleteLinkFormatter, sortable: false },
                { name: 'passHistoryUrl', index: 'passHistoryUrl', width: 15, align: 'center', resizable: false, formatter: ViewLinkFormatter, sortable: false },

                 ],
                rowNum: <%=_recordsCount %>,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'CONTACTLIST_NAME',
                sortorder: "asc",

                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 625,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {                   
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
            $("#tblAddressBook").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false,refresh:false });
            $("#tblAddressBook").jqGrid('setLabel', 'FOLDER_NAME', '', 'textalignleft');
        });

        function EditLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='" + cellvalue + "'><img src='App_Themes/Classic/Images/icon-pen-active.gif' title='Edit'/></a>";
        }
//        function EmailLinkFormatter(cellvalue, options, rowObject) {
//            return "<a href='" + cellvalue + "' rel='framebox' w='700' h='250' ><img src='App_Themes/Classic/Images/icon-pen-active.gif' title='Edit'/></a>";
//        }


        function ViewLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='" + cellvalue + "' rel='framebox' w='700' h='350'  scrolling='yes'><img src='App_Themes/Classic/Images/icon-percent-active.gif' title='Past History'/></a>";
        }


        function DeleteLinkFormatter(cellvalue, options, rowObject) {
            //return "<a href='#' class='button-small-gap' id='lnkDelete' onclick='javascript: return DeleteContact(" + cellvalue + ");'><img src='App_Themes/Classic/Images/icon-delete-active.gif' title='Delete' /></a>";
            return "<a href='javascript:void(0)' class='button-small-gap' id='lnkDelete' onclick='javascript: return DeleteContact(" + cellvalue + ");'><img src='App_Themes/Classic/Images/icon-delete-active.gif' title='Delete' /></a>";
        }

        function gridReload() {
            var keyword = $("#txtKeyword").val();
            $("#tblAddressBook").jqGrid('setGridParam', { url: serviceUrl + "?keyword=" + keyword, page: 1 }).trigger("reloadGrid");
        }
       

        function DeleteContact(ContactListId) {
            jConfirm('Are you sure you want to delete ?', 'Delete Confirmation', function (r) {
                if (r == '1') { 
                $.post("AjaxService.aspx", { "method": "DeleteContactByContactListId", "Id": ContactListId },
                  function (result) {
                  if(result >= '1')
                   {
                        $('.successPanel').show();
                        $('.successMsg').show();
                        $('.successMsg').html('Email list deleted successfully.');
                    gridReload(); 
                   }
                   
                  });
                }
            });
        }

        function ndateFormatter(cellval, opts, rwdat, _act) {

            if (cellval != "" && cellval != null) {
                var time = cellval.replace(/\/Date\(([0-9]*)\)\//, '$1');
                var date = new Date();
                date.setTime(time);
                return date.toDateString();
            } else return "-";
        }

      
    </script>
</asp:Content>
