﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SurveyReport.master" AutoEventWireup="true"
    CodeFile="CrossTabReports.aspx.cs" Inherits="CrossTabReports" %>

<%@ Register Src="~/UserControls/SurveyReportHeader.ascx" TagName="SurveyReportHeader"
    TagPrefix="uc" %>
<%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v9.1, Version=9.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
    <%@ Register src="~/UserControls/ErrorSuccessNotifier.ascx" tagname="ErrorSuccessNotifier" tagprefix="Insighto" %>
<%@ Register assembly="DevExpress.Web.v9.1" namespace="DevExpress.Web.ASPxPanel" tagprefix="dxp" %>


<asp:Content ID="surveyReportHeader" ContentPlaceHolderID="SurveyReportHeader" runat="Server">
    <uc:SurveyReportHeader ID="ucSurveyReportHeader" runat="server" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SurveyReportMainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel runat="server" ID="pnlLoading" Style="height: 450px; vertical-align: middle;"
        HorizontalAlign="Center">
        <div style="width: 100%; height: 220px;">
        </div>
        <img src="App_Themes/Classic/Images/ajax-loader.gif" alt="LOADING..." /><br />
        <div style="width: 100%; height: 220px;">
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlMain" Style="display: none;">
        <div class="surveyCreateLeftMenu">
            <!-- survey left menu -->
            <ul class="reportsMenu">
                <li><a href="Reports.aspx?Key=<%= Key %>">Reports</a></li>
                <li><a id="CustomReports" runat="server">Customise Charts</a></li>
            </ul>
            <ul class="reportsMenu">
                <li><a href="#" class="activeLink">Cross Tab Report</a></li>
                <div class="reportExpandPanel">
                    <p >
                        <b><span>Cross Tab Report:</span></b>
                        <!--<a href="#" title="Help" class="helpLink1" onclick="javascript: window.open('help/4_3.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;"></a>
                        <a id="hlCrossTabReport" runat="server" href="#"></a>-->
                    </p>
                    <p class="defaultHeight"></p>
                    
                    <p >
                        &nbsp;<dxp:ASPxPanel ID="pnl_savedcreports" runat="server" Width="200px">
                            <PanelCollection>
                                <dxp:PanelContent ID="PanelContent1" runat="server">
                                </dxp:PanelContent>
                            </PanelCollection>
                        </dxp:ASPxPanel>
                        <p>
                        </p>
                        <p class="defaultHeight">
                        </p>
                        <p>
                            <dxe:ASPxButton ID="lbn_addnewcrosstab" runat="server" BackColor="transparent" 
                                CssClass="devxButton" OnClick="lbn_addnewcrosstab_Click" 
                                Text="Add New Cross Tab">
                                <Border BorderStyle="None" />
                                <HoverStyle ForeColor="#D9D9D9" />
                            </dxe:ASPxButton>
                        </p>
                    </p>
                </div>
            </ul>
            <ul class="reportsMenu">
                <li><a id="IndividualRes" runat="server">Individual Responses</a></li>
            </ul>
            <!-- //survey left menu -->
        </div>
        <div class="surveyQuestionPanel">
         <div class="quationtxtPanel" id="divSendReport" runat="server" visible="false">
  
             <Insighto:ErrorSuccessNotifier ID="esSendReportMail" runat="server" />
            </div>
            <!-- survey question panel -->
            <div class="surveyQuestionHeader">
                <div class="surveyQuestionTitle">
                    CROSS TAB REPORT</div>
            </div>
            <!-- //survey question panel -->

            <div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left" valign="top">
                            <dxrp:ASPxRoundPanel ID="rnd_questionspanel" runat="Server" Width="100%" HeaderStyle-Height="0"
                                BackColor="white" ShowHeader="False" Border-BorderWidth="0">
                                <PanelCollection>
                                    <dxp:PanelContent ID="PanelContent2" runat="server">
                                    </dxp:PanelContent>
                                </PanelCollection>
                                <HeaderStyle Height="0px" />
                                <NoHeaderTopLeftCorner Url="none" />
                                <Border BorderStyle="None" BorderWidth="0px" />
                            </dxrp:ASPxRoundPanel>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 10px 0 10px 0">
                    <tr>
                        <td>
                            <dxe:ASPxLabel ID="lbl_message" runat="server" Visible="False" ForeColor="Red">
                            </dxe:ASPxLabel>
                            <dxe:ASPxLabel ID="lbl_confirm" runat="server" ForeColor="Tomato" Visible="False">
                            </dxe:ASPxLabel>
                        </td>
                        <td align="right">
                            <dxe:ASPxButton ID="btngenerate" runat="server" Text="Generate" OnClick="btngenerate_Click"
                                BackColor="transparent" CssClass="devxButton">
                                <Border BorderStyle="None" />
                                <HoverStyle ForeColor="#D9D9D9" />
                            </dxe:ASPxButton>
                        </td>
                    </tr>
                </table>
                <asp:Panel CssClass="aspPanelScroll" id="divCrosstab" runat="server" visible="false" ScrollBars="Auto">
                     <table id="rpnl_crosstabbing_tbl" runat="server" width="100%" border="0" cellspacing="0"
                    cellpadding="0" visible="false">
                    <tr>
                        <td valign="top">
                            <asp:Panel ID="rpnl_crosstabbing" runat="server" Width="100%" CssClass="padno">
                                <span id="Crosshead" runat="server" class="boxheads">Cross Tab Analysis</span>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                </asp:Panel>
              
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="190" height="42" align="left" valign="middle">
                            <dxe:ASPxLabel ID="ASPxLabel1" runat="server" Text="Enter Cross Tab Report Name :"
                                Width="220px" Visible="False">
                            </dxe:ASPxLabel>
                        </td>
                        <td width="334" height="42" align="left" valign="middle">
                            <dxe:ASPxTextBox ID="txt_crosstabreportname" runat="server" Width="160px" Visible="False">
                                <ValidationSettings CausesValidation="True" ErrorTextPosition="Right" ValidationGroup="crosstab">
                                    <RequiredField ErrorText="Enter Cross Tab Report Name" IsRequired="True" />
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                        <td width="150" height="42" align="right" valign="middle">
                            <dxe:ASPxButton ID="btnsavecrosstab" runat="server" CssClass="devxButton" Text="Save Cross Tab"
                                OnClick="btnsavecrosstab_Click" Visible="False" ValidationGroup="crosstab" BackColor="transparent">
                                <Border BorderStyle="None" />
                            </dxe:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left" valign="middle">
                        </td>
                        <td align="right" valign="middle">
                            <dxe:ASPxButton ID="btnexporttoexcel" runat="server" CssClass="devxButton" Text="Export To Excel"
                                OnClick="btnexporttoexcel_Click" Visible="False" BackColor="Transparent">
                                <Border BorderStyle="None" />
                            </dxe:ASPxButton>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>

 
    <script type="text/javascript" language="javascript">

        function pageLoad(sender, args) {
            //alert('hai');
            TogglePannel('<%= pnlMain.ClientID %>', true);
            TogglePannel('<%= pnlLoading.ClientID %>', false);
        }

        function TogglePannel(target, OnOff) {
            obj = document.getElementById(target);

            if (obj) {
                obj.style.display = (OnOff) ? 'inline' : 'none';
            }
        }
        $(document).ready(function () {
            $('img').each(function () {
                var str = this.src.split('/');
                if (str[3] == 'none') {
                    $(this).parent().hide();
                }
            });
        });


    </script>
</asp:Content>
