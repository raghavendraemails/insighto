﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using Insighto.Data;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using System.Collections;
using App_Code;

namespace Insighto.Pages
{
    public partial class Contactus : SecurePageBase
    {
        Hashtable ht = new Hashtable();
        int userId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCountry();
            }
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                 if (ht != null && ht.Count > 0)
                 {
                     if (ht.Contains("Userid"))
                         userId = Convert.ToInt32(ht["Userid"]);
                 }
            }
            if (userId > 0)
            {
                var userDetails = ServiceFactory.GetService<UsersService>().GetUserDetails(userId).FirstOrDefault();
                if (userDetails != null)
                {
                    txtName.Text = userDetails.FIRST_NAME;
                    if (userDetails.LAST_NAME != null)
                        txtName.Text += " " + userDetails.LAST_NAME;
                    txtMail.Text = userDetails.LOGIN_NAME;
                    txtPhno.Text = userDetails.PHONE;
                    ddlAttentionvalues.SelectedValue = ddlAttentionvalues.Items.FindByText("Report a bug").Value;

                }
            }
        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            InsertContactDetails();
        }

        #region "Method:InsertContactDetails"
        public void InsertContactDetails()
        {
            osm_contactusdetails osmContactUs = new osm_contactusdetails();
            osmContactUs.USERNAME = txtName.Text.Trim();
            osmContactUs.EMAILADDRESS = txtMail.Text.Trim();
            osmContactUs.PHONENO = txtPhno.Text.Trim();
            osmContactUs.MAILMESSAGE = txtMessage.Text.Trim();
            osmContactUs.ATTENTIONVALUE = ddlAttentionvalues.SelectedItem.Value;
            var contactusService = ServiceFactory.GetService<ContactUsService>();
            contactusService.InsertContactUs(osmContactUs);
            if (osmContactUs.CONTACTUSUSERID > 0)
            {
                var configService = ServiceFactory.GetService<ConfigService>();
                List<string> configValue = new List<string>();
                configValue.Add("ContactusEmailAddress");
                var configDet = configService.GetConfigurationValues(configValue);

                string subject = String.Format(Utilities.ResourceMessage("Contactus_Mail_Subject"), osmContactUs.USERNAME, osmContactUs.ATTENTIONVALUE, osmContactUs.EMAILADDRESS, osmContactUs.PHONENO); //"UserName: " + osmContactUs.USERNAME + "- Issue Type: " + osmContactUs.ATTENTIONVALUE + "- EmailID: " + osmContactUs.EMAILADDRESS + "- Phone No: " + osmContactUs.PHONENO;
               
                //string body = osmContactUs.MAILMESSAGE;
                //string emailID = configDet[0].CONFIG_KEY;
                Hashtable ht = new Hashtable();
                MailHelper.SendMailMessage(osmContactUs.EMAILADDRESS, configDet[0].CONFIG_KEY, "", "", subject, osmContactUs.MAILMESSAGE);
            }
        }
        #endregion

        #region "Method : BindCountry"
        /// <summary>
        /// Used to get country list and bind values to dropdownlist.
        /// </summary>

        private void BindCountry()
        {
            // object on osm_picklist class
            osm_picklist osmPickList = new osm_picklist();
            //Assign vaule category
            osmPickList.CATEGORY = Constants.Attentionvalues;
            // object on PickListService class
            PickListService pickListService = new PickListService();
            //inovke method GetPickList
            var vPickList = pickListService.GetPickList(osmPickList);

            if (vPickList.Count > 0)
            {
                //Bind list values to dropdownlist
                this.ddlAttentionvalues.DataSource = vPickList;
                this.ddlAttentionvalues.DataTextField = Constants.PARAMETER;
                this.ddlAttentionvalues.DataValueField = Constants.PARAMETER;
                this.ddlAttentionvalues.DataBind();
                ddlAttentionvalues.Items.Insert(0, new ListItem("--Select--", ""));
            }

        }
        #endregion
        
    }
}
