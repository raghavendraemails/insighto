﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_Code;
using App_Code.SurveyRespondentHelpers;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using System.Text;

namespace Insighto.Pages
{
    public partial class PreLaunchQuestionnaire : SurveyPageBase
    {
        List<SurveyQuestionAndAnswerOptions> surveyQuestionsList;
        Hashtable typeHt = new Hashtable();
        string strPage;
        string strQSeq;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            dvErrMsg.Visible = false;
            dvSuccessMsg.Visible = false;
            dvErrMsg1.Visible = false;
            dvSuccessMsg1.Visible = false;

            if (base.SurveyBasicInfoView != null)
            {
                if (base.SurveyBasicInfoView.SURVEY_ID > 0)
                {
                    SurveyBasicInfoView surveyBasicInfo = ServiceFactory.GetService<RespondentService>().GetSurveyViewInformation(base.SurveyBasicInfoView.SURVEY_ID);

                    if (surveyBasicInfo.SURVEY_INTRO != "" || surveyBasicInfo.SURVEY_INTRO.Length > 0)
                    {
                       // lblSurveyIntro.Text = surveyBasicInfo.SURVEY_INTRO;
                        string strEmailInvitation = "";
                        strEmailInvitation += surveyBasicInfo.SURVEY_INTRO;
                        //strEmailInvitation += "<p> <a  target='_blank' href='" + ConfigurationManager.AppSettings["RootURL"].ToString() + review_url + "'><span style='font-weight: bold;'>START SURVEY<span></a> here </p>";
                        lblSurveyIntro.InnerHtml = strEmailInvitation;                       
                    }
                    else
                    {
                        lblSurveyIntro.InnerHtml = "<p> You are invited to join a short survey being held among the customers of COMPANY/BRAND X. There are only (X No. of questions) and it will take about X minutes of your time. We will keep all your information confidential and will not transmit it to any third parties. If you have any questions about this survey, please contact me directly at my email address provided below. <br> Your Feedback is important!</br>";
                    }
                    lblSurveyIntro.Style.Add("family", base.SurveyBasicInfoView.SURVEY_FONT_TYPE);
                    lblSurveyIntro.Style.Add("font-size", BuildQuestions.GetFontSizeValue(base.SurveyBasicInfoView.SURVEY_FONT_SIZE));
                    lblSurveyIntro.Style.Add("color", BuildQuestions.GetFontColor(base.SurveyBasicInfoView.SURVEY_FONT_COLOR));

                    string strQuestionnaireLink = EncryptHelper.EncryptQuerystring("~/SurveyDrafts.aspx", "SurveyId=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
                    hlnkBTQuestionnaire.NavigateUrl = strQuestionnaireLink;

                    string strAddIntroLink = EncryptHelper.EncryptQuerystring("~/AddIntroduction.aspx", "Page=PLQI&" + Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
                    hlnkEditIntro.NavigateUrl = strAddIntroLink;

                    BindQuestionnaire();
                }
            }

            if (Request.QueryString["key"] != null)
            {
                typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

                if (typeHt.Contains("Page"))
                    strPage = typeHt["Page"].ToString();

                if (typeHt.Contains("QSeq"))
                    strQSeq = typeHt["QSeq"].ToString();


                if (strPage == "PQ")
                {
                    dvSuccessMsg1.Visible = true;
                    lblSuccMsg1.Text = "Question " + strQSeq + " saved successfully.";
                }
            }

            SetStyles();
            if (!IsPostBack)
            { 
            //lblIntroTitleResource1.Text
                lblIntroTitle.Text = String.Format(Utilities.ResourceMessage("lblIntroTitleResource1.Text"), SurveyMode);
                lblTitle.Text = String.Format(Utilities.ResourceMessage("lblTitleResource1.Text"), SurveyMode);
            }
        }

        /// <summary>
        /// Binds the questionnaire.
        /// </summary>
        private void BindQuestionnaire()
        {
            var surveyService = ServiceFactory.GetService<RespondentService>();
            surveyQuestionsList = surveyService.GetSurveyQuestionAndAnswerOptionsList(base.SurveyBasicInfoView.SURVEY_ID, true);

            lstQuestionTypesItems.DataSource = surveyQuestionsList;
            lstQuestionTypesItems.DataBind();

        }

        #region "Event : lstQuestionTypesItems_ItemDataBound"

        /// <summary>
        /// In this event to bind the values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lstQuestionTypesItems_ItemDataBound(object sender, ListViewItemEventArgs e)
        {


            if (e.Item.ItemType == ListViewItemType.DataItem)
            {

                ListViewDataItem dataItem = (ListViewDataItem)e.Item;
                int questionSequence = ValidationHelper.GetInteger(((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_TYPE_ID.ToString(), 0);
                if (questionSequence == 16)
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = "N";
                }
                else if (questionSequence == 17)
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = "P";
                }
                else if (questionSequence == 18)
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = "Q";
                }
                else if (questionSequence == 19)
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = "I";
                }
                else if (questionSequence == 21)
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = "V";
                }
                else
                {
                    ((Label)e.Item.FindControl("lblSequence")).Text = ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).RankIndex.ToString();
                }        
                
                HtmlGenericControl divQuesAnswers = e.Item.FindControl("divQuesAnswers") as HtmlGenericControl;
                TextBox txtOther = e.Item.FindControl("txtOther") as TextBox;
                Label lblOther = e.Item.FindControl("lblOther") as Label;

                HyperLink hlnkEdit = e.Item.FindControl("hlnkEdit") as HyperLink;

              
                int questionId = ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_ID;
                var questionType = GenericHelper.ToEnum<QuestionType>(ValidationHelper.GetInteger(((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_TYPE_ID, 0));

                string strQuerystring = Constants.SURVEYID + "=" + ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.SURVEY_ID.ToString() + "&QuestionType=" + ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_TYPE_ID.ToString() + "&QuestionId=" + ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_ID.ToString() + "&QuestionSeq=" + ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.QUESTION_SEQ.ToString() + "&RankIndex=" + ((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).RankIndex.ToString(); 
                string strPage = "PQ";
                string strEditLink = EncryptHelper.EncryptQuerystring(PathHelper.GetAddNewQuestionURL(), "Page=" + strPage + "&Mode=Edit&" + strQuerystring + "&surveyFlag=" + base.SurveyFlag);
                //if (IsSurveyActive && questionType == QuestionType.ConstantSum)
                //{
                //    hlnkEdit.NavigateUrl = "javascript:void(0)";
                //    hlnkEdit.Attributes.Remove("rel");
                //    hlnkEdit.Style.Add("cursor", "default");
                //}
                //else
                //{
                //    hlnkEdit.NavigateUrl = strEditLink;
                //}
                hlnkEdit.NavigateUrl = strEditLink;
                if (questionType == QuestionType.MultipleChoice_SingleSelect || questionType == QuestionType.Choice_YesNo || questionType == QuestionType.Menu_DropDown)
                {
                    if (((Insighto.Business.ValueObjects.SurveyQuestionAndAnswerOptions)(dataItem.DataItem)).Question.SKIP_LOGIC.ToString() == "1")
                    {
                      
                        hlnkEdit.NavigateUrl = "javascript:void(0);";
                        hlnkEdit.Attributes.Add("onclick", "javascript:return ShowWarningForEdit()");
                        hlnkEdit.Attributes.Remove("rel");

                    }
                    else
                    {            
                        hlnkEdit.Attributes.Add("rel", "framebox");
                        hlnkEdit.Attributes.Remove("onclick");
                    }
                }


                BindAnswerControls(divQuesAnswers, txtOther, lblOther, questionId, questionType);
            }
        }

        #endregion

        /// <summary>
        /// Binds the answer controls.
        /// </summary>
        /// <param name="divQuesAnswers">The div ques answers.</param>
        /// <param name="txtOther">The TXT other.</param>
        /// <param name="lblOther">The LBL other.</param>
        /// <param name="questionId">The question id.</param>
        /// <param name="questionType">Type of the question.</param>
        private void BindAnswerControls(HtmlGenericControl divQuesAnswers, TextBox txtOther, Label lblOther, int questionId, Insighto.Business.Enumerations.QuestionType questionType)
        {
            SurveyCore othertext = new SurveyCore();
            var surveyQuestion = surveyQuestionsList.Where(s => s.Question.QUESTION_ID == questionId).FirstOrDefault();
            //TextBox txtOther = new TextBox();
            HtmlTable tblContactInfo = new HtmlTable();
            tblContactInfo.CellPadding = 2;
            tblContactInfo.CellSpacing = 2;
            HtmlGenericControl divAnswers = new HtmlGenericControl();
            //tblContactInfo.Width = "100%";

            switch (questionType)
            {
                case QuestionType.MultipleChoice_MultipleSelect:
                    divAnswers = new HtmlGenericControl("div"); 
                    divAnswers.Attributes.Add("class", "singleSelectRadio");
                    CheckBoxList chkMultipleSelect = new CheckBoxList();
                    ServiceFactory<RespondentMultipleQuestions>.Instance.CreateMOMSQuestion(chkMultipleSelect,
                                                                                            surveyQuestion, "chk", null,
                                                                                            txtOther,
                                                                                            surveyQuestion.Question.
                                                                                                RANDOMIZE_ANSWERS > 0);
                    divAnswers.Controls.Add(chkMultipleSelect);
                    txtOther.Visible = surveyQuestion.Question.OTHER_ANS > 0;
                    lblOther.Visible = txtOther.Visible;
                    lblOther.Text = othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID));
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.Menu_DropDown:
                    DropDownList ddlMenu = new DropDownList();
                    ServiceFactory<RespondentMultipleQuestions>.Instance.CreateDropdownMenuQuestion(ddlMenu, surveyQuestion,
                                                                                                    "ddl", null, txtOther,
                                                                                                    surveyQuestion.Question.
                                                                                                        RANDOMIZE_ANSWERS >
                                                                                                    0, true);
                    divAnswers.Controls.Add(ddlMenu);
                    txtOther.Visible = surveyQuestion.Question.OTHER_ANS > 0;
                    lblOther.Visible = txtOther.Visible;
                    lblOther.Text = othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID));
                    divQuesAnswers.Controls.Add(divAnswers);

                    break;

                case QuestionType.Choice_YesNo:
                    RadioButtonList rbChoiceYesNo = new RadioButtonList();
                    ServiceFactory<RespondentMultipleQuestions>.Instance.CreateYesNoOptionQuestion(rbChoiceYesNo,
                                                                                                   surveyQuestion, "rbl",
                                                                                                   null, txtOther,
                                                                                                   surveyQuestion.Question.
                                                                                                       RANDOMIZE_ANSWERS > 0);

                    divAnswers.Controls.Add(rbChoiceYesNo);
                    txtOther.Visible = surveyQuestion.Question.OTHER_ANS > 0;
                    lblOther.Visible = txtOther.Visible;
                    lblOther.Text = othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID));
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.MultipleChoice_SingleSelect:
                    divAnswers = new HtmlGenericControl("div");
                    divAnswers.Attributes.Add("class", "singleSelectRadio");
                    RadioButtonList rbSingleSelect = new RadioButtonList();
                    ServiceFactory<RespondentMultipleQuestions>.Instance.CreateMossQuestion(rbSingleSelect, surveyQuestion,
                                                                                            "rbl", null, txtOther,
                                                                                            surveyQuestion.Question.
                                                                                                RANDOMIZE_ANSWERS > 0);
                    divAnswers.Controls.Add(rbSingleSelect);
                    txtOther.Visible = surveyQuestion.Question.OTHER_ANS > 0;
                    lblOther.Visible = txtOther.Visible;
                    lblOther.Text = othertext.Selectsurveyques_other(Convert.ToInt32(surveyQuestion.Question.QUESTION_ID));
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.ContactInformation:
                    divQuesAnswers.Controls.Add(
                        ServiceFactory<RespondentMultipleQuestions>.Instance.CreateContactInfoQuestion(tblContactInfo,
                                                                                                       surveyQuestion));
                    break;

                case QuestionType.Image:
                    foreach (var imageItem in surveyQuestion.AnswerOption)
                    {
                        Image imgType = new Image();
                        imgType.ImageUrl = ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateImageQuestion(imageItem.ANSWER_OPTIONS);
                        divAnswers.Controls.Add(imgType);
                        divQuesAnswers.Controls.Add(divAnswers);
                    }
                    break;

                case QuestionType.Narration:
                    Label lblNarration = new Label();
                    lblNarration.Text =
                        ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateNarrationQuestion(
                            surveyQuestion.Question.QUSETION_LABEL);
                    break;

                case QuestionType.PageHeader:
                    Label lblPageHeader = new Label();
                    lblPageHeader.Text =
                        ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreatePageHeaderQuestion(
                            surveyQuestion.Question.QUSETION_LABEL);
                    break;

                case QuestionType.QuestionHeader:
                    Label lblQuestionHeader = new Label();
                    lblQuestionHeader.Text =
                        ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateQuestionHeaderQuestion(
                            surveyQuestion.Question.QUSETION_LABEL);
                    break;

                case QuestionType.Matrix_MultipleSelect:
                    ServiceFactory<RespondentMatrixQuestions>.Instance.CreateMatrixMultipleSelectQuestion(tblContactInfo,
                                                                                                          surveyQuestion);
                    tblContactInfo.Attributes.Add("class", "tblMatrix2D");
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.Matrix_SideBySide:
                    ServiceFactory<RespondentMatrixQuestions>.Instance.CreateMatrixSideBySideQuestion(surveyQuestion,
                                                                                                      tblContactInfo);
                    tblContactInfo.Attributes.Add("class", "tblMatrixSideBySide");
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.Matrix_SingleSelect:
                    ServiceFactory<RespondentMatrixQuestions>.Instance.CreateMatrixSingleSelectQuestion(tblContactInfo,
                                                                                                          surveyQuestion);
                    tblContactInfo.Attributes.Add("class", "tblMatrix2D");
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.Text_CommentBox:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateTextCommentBoxQuestion(this.Page, divAnswers, surveyQuestion);
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.Text_MultipleBoxes:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateTextMultipleBoxesQuestion(this.Page, divAnswers, surveyQuestion);
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.Text_SingleRowBox:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateTextSingleRowBoxQuestion(this.Page, divAnswers, surveyQuestion);
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.EmailAddress:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateEmailAddressQuestion(this.Page, divAnswers, surveyQuestion);
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.Numeric_SingleRowBox:
                    ServiceFactory<RespondentTextQuestions>.Instance.CreateNumericSingleRowBoxQuestion(this.Page, divAnswers, surveyQuestion);
                    divQuesAnswers.Controls.Add(divAnswers);
                    break;

                case QuestionType.ConstantSum:
                    ServiceFactory<RespondentNumericQuestions>.Instance.CreateConstantSumQuestion(tblContactInfo, surveyQuestion);
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.DateAndTime:
                    ServiceFactory<RespondentOtherTypeQuestions>.Instance.CreateDateTimeQuestion(tblContactInfo, surveyQuestion);
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.Ranking:
                    ServiceFactory<RespondentNumericQuestions>.Instance.CreateRankingQuestion(tblContactInfo, surveyQuestion);
                    divQuesAnswers.Controls.Add(tblContactInfo);
                    break;

                case QuestionType.Video:
                    foreach (var videoItem in surveyQuestion.AnswerOption)
                    {
                        divAnswers.Controls.Add(ServiceFactory<RespondentOtherTypeQuestions>.Instance.DisplayVideoQuestion(videoItem.ANSWER_OPTIONS, videoItem.ANSWER_ID.ToString()));
                        divQuesAnswers.Controls.Add(divAnswers);
                    }
                    break;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (base.SurveyBasicInfoView.SURVEY_ID > 0)
            {
                string strLaunchUrl = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
                Response.Redirect(strLaunchUrl);
            }
        }

        /// <summary>
        /// Sets the styles.
        /// </summary>
        private void SetStyles()
        {
            if (Page.Master == null) return;
            dynamic dynamicStyle = Page.Master.Master.FindControl("dynamicStyle");
            if (dynamicStyle == null)
                dynamicStyle = Page.FindControl("dynamicStyle");

            if (dynamicStyle == null) return;
            const string fontFormat = "font:normal {0}pt {1}, Helvetica, sans-serif; ";
            const string fontColor = "color:{0};";
            var fontStringBuilder = new StringBuilder(".customDynamicCss {");
            fontStringBuilder.AppendFormat(fontFormat, base.SurveyBasicInfoView.SURVEY_FONT_SIZE, base.SurveyBasicInfoView.SURVEY_FONT_TYPE);
            //fontStringBuilder.AppendFormat(fontColor, ColorCodeHelper.GetColorCodes(base.SurveyBasicInfoView.SURVEY_FONT_COLOR));
            fontStringBuilder.Append("}");
            fontStringBuilder.AppendLine();
            fontStringBuilder.Append(".dynamicButton {");
            fontStringBuilder.AppendFormat("background:{0};", ColorCodeHelper.GetColorCodes(base.SurveyBasicInfoView.BUTTON_BG_COLOR));
            fontStringBuilder.Append("}");
            dynamicStyle.InnerText = fontStringBuilder.ToString();
        }
    }
}

