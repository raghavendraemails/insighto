﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpgradeLicense.aspx.cs" Inherits="Insighto.Pages.UpgradeLicense"
    MasterPageFile="~/ModelMaster.master"%>

<%@ Register Src="~/UserControls/Header.ascx" TagPrefix="uc" TagName="Header" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
        <div class="popupClosePanel">
            <a href="#"  alt="Close" title="Close" runat="server" id="lnkClose"
                onclick="closeModalSelf(false,'');">&nbsp;</a>
        
    </div>
    <div class="popupContentPanel">
        <table width="100%" align="center" border="0" cellspacing="0">
            <tr>
                <td align="left" valign="middle">
                    <uc:Header ID="Header1" runat="server" />
                </td>
            </tr>
            <tr>
                <td height="1" bgcolor="#994ea3">
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td>
                    <div class="informationPanelDefault">
                        <div>
                            <p>
                                <b>
                        <asp:Label ID="lblFeature" runat="server" 
                                    Text="This is a paid feature!" 
                                    ></asp:Label></b></p>
                       <p>
                                <b>  <asp:Label ID="lblprofreatures" runat="server" 
                                    Text="This feature is available only for Insighto Premium Single / premium Annual subscriptions." 
                                    ></asp:Label></b></p>
                        
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-left:40px;">
                    <h4>
                        <asp:Label ID="lblExperience" runat="server" 
                            Text="Experience Insighto Paid plans and get access to these Powerful features:" 
                           ></asp:Label></h4>
                            <h4> <asp:Label ID="lblproexperience" runat="server" 
                            Text="Buy Insighto Premium and get access to these powerful features:" 
                           ></asp:Label>
                        </h4>
                </td>
            </tr>
            <tr>
                <td class="defaultHeight">
                </td>
            </tr>
            <tr>
                <td id="Td1">
                    <asp:Label ID="lblDetails" runat="server" 
                        Text="<ul class='liInsighoIcon'> <li><b>Download results to PPT, Excel, Word or PDF</b></li> <li><b>Launch survey through insighto's email system</b></li>  <li><b>Password protected survey</b></li> <li><b>Schedule survey launch time and more </b></li></ul>" 
                        ></asp:Label>
                <asp:Label ID="lblprodetails" runat="server" 
                        Text="<ul class='liInsighoIcon'> <li><b>All features of Pro +</b></li> <li><b>Export raw data</b></li><li><b>View individual responses</b></li><li><b>Redirection of URL</b></li> <li><b>Remove Insighto branding on your survey end page </b></li></ul>" 
                        ></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="upgradeNowBtnPanel">
                        <ul class="HyperlinkBtn">
                            <li><a href="#" id="hyp_priclink" runat="server" onclick="opener.focus()">
                                <asp:Label ID="lblUpgrade" runat="server" Text="Upgrade Now!" 
                                    meta:resourcekey="lblUpgradeResource1"></asp:Label></a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>   
    <script type="text/javascript">
        $(document).ready(function () {
            var a = document.getElementById('linkClose');           
        });
    </script>
</asp:Content>
