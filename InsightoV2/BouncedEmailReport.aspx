﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="BouncedEmailReport.aspx.cs" Inherits="BouncedEmailReport" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div class="popupHeading">
    <div class="popupTitle">
        <h3><asp:Label ID="lblTitle" runat="server" Text="Bounced Email Report" 
                meta:resourcekey="lblTitleResource1"></asp:Label></h3>
    </div>
    
  <%--  <div class="popupClosePanel">
            <a href="#" id="hlnkPopUp" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(true,'');">
                &nbsp;</a>
        </div>--%>
</div>
<div class="popupContentPanel">
<div class="clear"></div>
    <p class="defaultHeight"></p>
    <input type="hidden" class="hdnSurveyId" runat ="server" id="hdnSurveyId"  />

    <asp:DropDownList ID="ddlSurvey" CssClass="ddlSurvey dropDown" runat="server" 
        meta:resourcekey="ddlSurveyResource1" />
    <p class="defaultHeight"></p>
    <div class="surveysGridPanel" id="ptoolbar">
    </div>
    <table id="tblJobKits">
    </table>
    <script type="text/javascript">
        var serviceUrl = 'AjaxService.aspx';
        $(document).ready(function () {
           // $('.hdnSurveyId').val($('.ddlSurvey').val());
            $("#tblJobKits").jqGrid({
                url: serviceUrl,
                postData: { method: "GetBouncedEmails",surveyId: $('.hdnSurveyId').val() },
                datatype: 'json',
                colNames: ['First Name', 'Last Name', 'Email', 'Status'],
                colModel: [
                { name: 'FIRST_NAME', index: 'FIRST_NAME', width: 130, align: 'left', editable: false, resizable: false, size: 100 },
                { name: 'LAST_NAME', index: 'LAST_NAME', width: 130, align: 'left', editable: false, size: 100, resizable: false },
                { name: 'EMAIL_ADDRESS', index: 'EMAIL_ADDRESS', width: 260, align: 'left', editable: false, resizable: false, size: 100 },
                { name: 'STATUS', index: 'STATUS', width: 120, align: 'center', editable: false, resizable: false, size: 100 }
                ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'MODIFIED_ON',
                sortorder: "desc",
                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 650,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('#ptoolbar').show();
                    $('#gbox_tblJobKits').show();
                    $('.text').hide();
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }
            });
        });
        $("#tblJobKits").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false,refresh:false, caption: "Add" });
//        $('.ddlSurvey').change(function () {
//            gridReload($('.ddlSurvey').val());
//        });
        function gridReload(surveyId) {
            $("#tblJobKits").jqGrid('setGridParam', { url: serviceUrl + "?surveyId=" + surveyId, page: 1 }).trigger("reloadGrid");

        }
    </script>
</div>
</asp:Content>
