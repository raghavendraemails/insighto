﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using Insighto.Data;
using CovalenseUtilities.Helpers;
using System.Web.UI.HtmlControls;
using System.Collections;
using Insighto.Business.Helpers;
using System.Configuration;


namespace Insighto.Pages
{
    public partial class ManageLogin : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                BindEmails();
            }
                      
        }




        protected void BindEmails()
        {

               
                List<string> lstStr = new List<string>();
               
                var sessionState = ServiceFactory.GetService<SessionStateService>();
                var loginUser = sessionState.GetLoginUserDetailsSession();
                var userService = ServiceFactory.GetService<UsersService>();
                var userEmails = userService.GetUserAlternateEmails(loginUser.UserId);
                var userInfo = userService.FindUserByUserId(loginUser.UserId);
                txtEmail.Text = userInfo.LOGIN_NAME;
                foreach (var email in userEmails)
                {
                    lstStr.Add(email.EMAIL);
                }


                if (lstStr.Count > 0)
                {
                    ucManageLogin.Value = lstStr;                    
                }
                else
                {
                    lstStr.Add("");
                    ucManageLogin.Value = lstStr;
                }
               
                

               // hdnContactListId.Value = ValidationHelper.GetInteger(columnList.CONTACTLIST_ID, 0).ToString();
            

        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManageLogin.aspx");
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<string> list = new List<string>();
            list = ucManageLogin.Value;
            var emails = new osm_useralternateemails();
            int rtnValue = 0;
            var sessionSerivce = ServiceFactory.GetService<SessionStateService>();
            var userinfo = sessionSerivce.GetLoginUserDetailsSession();
            var userService = ServiceFactory.GetService<UsersService>();
            var alternateEmailService = ServiceFactory.GetService<UserAlternateEmails>();
            alternateEmailService.DeleteEmailsByUserId(userinfo.UserId);
            for (int i = 0; i < list.Count; i++)
            {               
                emails.USERID = userinfo.UserId;
                emails.ISAUTHENTICATED = 0;
                emails.EMAIL = list[i].ToString().Trim();
                if (!alternateEmailService.FindEmailExist(list[i].ToString().Trim()))
                {
                     rtnValue = alternateEmailService.SaveAlternateEmails(emails);
                     string fromEmail = ConfigurationManager.AppSettings["emailActivation"];
                     string subject = (String)GetGlobalResourceObject("CommonMessages", "emailSubjectAccountActivation");
                     string ActivationURL = ConfigurationManager.AppSettings["RootVirtualURL"] + EncryptHelper.EncryptQuerystring("Home.aspx", "AlternateId=" + rtnValue + "&Email=" + emails.EMAIL);
                     string MsgBody = string.Format("URL:{0}<br/>UserName: {1}", ActivationURL, emails.EMAIL);
                     MailHelper.SendMailMessage(fromEmail, emails.EMAIL, "", "", subject, MsgBody);
                }
            }
            if (rtnValue > 0)
            {
                 BindEmails();
                 dvSuccessMsg.Visible = true;
                 dvErrMsg.Visible = false;
                 lblSuccessMsg.Text = "Email(s) saved successfully.";

            }
            else
            {
                dvErrMsg.Visible = true;
                dvSuccessMsg.Visible = false;
                lblErrorMsg.Text = "email(s) already exist.";
            }
                
        }
}
}