﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Insighto.Business.Helpers;
using System.Data;
using System.Configuration;
using App_Code;
using System.Net;
using System.Net.Mail;
using System.Web.Services;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.IO;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;

public partial class UserControls_SurveyCredits : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int surveyid;
    int userid;
    int surveyFlag;
    string SurveyName;
    string Survey_type = "";
    string status = "";
    SurveyCore surcore = new SurveyCore();
    BasePage timebase =new BasePage();
    DataSet dsppscredits;
    //DataSet surveydetails;
    private MailMessage mailMessage = null;
    private SmtpClient smtpMail = null;

    public string prosingleAmt;
    public string premsingleAmt;
    string countryName;
    public string templatefreetopro = "SurveysActiveSurveyUpgradeFreetoProSingle";
    public string templatefreetopremium = "SurveysActiveSurveyUpgradeFreetoPremiumSingle";
    public string templateprotopremium = "SurveysActiveSurveyUpgradeProSingleToPremiumSingle";

    protected void Page_Load(object sender, EventArgs e)
    {

        string ipAddress = "";
        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
        {
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
        }

        countryName = Utilities.GetCountryName(ipAddress);
        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(countryName);

        countryName = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();

        

        if (countryName.ToUpper() != "INDIA")
        {

            //prosingleAmt =  "$9";
            //premsingleAmt = "$29";

            Rbl_pro.Visible = true;
            Rbl_buynow.Visible = true;
            Rbl_prem.Visible = true;

            Rbl_proindia.Visible = false;
            Rbl_buynowindia.Visible = false;
            Rbl_premindia.Visible = false;
        }
        else
        {
           
            //prosingleAmt = "&#8377;" + "399";
            //premsingleAmt = "&#8377;" + "1,299";

            Rbl_pro.Visible = false;
            Rbl_buynow.Visible = false;
            Rbl_prem.Visible = false;

            Rbl_proindia.Visible = true;
            Rbl_buynowindia.Visible = true;
            Rbl_premindia.Visible = true;
       
        }

        

        if (Request.QueryString["key"] != null)
        {
           
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (typeHt.Contains("SurveyId"))
                surveyid = Convert.ToInt32(typeHt["SurveyId"].ToString());
            if (typeHt.Contains("Userid"))
                userid = Convert.ToInt32(typeHt["Userid"].ToString());
            if (typeHt.Contains("surveyFlag"))
                surveyFlag = Convert.ToInt32(typeHt["surveyFlag"].ToString());
            if (typeHt.Contains("surveyname"))
                SurveyName = typeHt["surveyname"].ToString();
            if(typeHt.Contains("Survey_Type"))
                Survey_type = typeHt["Survey_Type"].ToString();
            if (typeHt.Contains("status"))
                status = typeHt["status"].ToString();

            dsppscredits = surcore.getCreditsCount(userid);



            if (Survey_type == "BASIC" && (dsppscredits.Tables[0].Rows.Count == 0 && dsppscredits.Tables[1].Rows.Count == 0))
            {
                lblcredittext.Text = "Choose one of the options below to upgrade your Free Survey";
                pps_Basic.Visible = false;
                pps_pro.Visible = false;
                pps_prem.Visible = false;
                pps_buynow.Visible = true;
            }
            else if (Survey_type == "BASIC" && (dsppscredits.Tables[0].Rows.Count > 0 && dsppscredits.Tables[1].Rows.Count > 0))
            {
                lblcredittext.Text = "Choose one of the options below to upgrade your Free Survey";
                pps_Basic.Visible = true;
                pps_pro.Visible = false;
                pps_prem.Visible = false;
                pps_buynow.Visible = false;
                lblapplypro1.Text = "(Available: " + dsppscredits.Tables[0].Rows.Count + ")";
                lblapplyprem1.Text = "(Available: " + dsppscredits.Tables[1].Rows.Count + ")";
                lblapplypro1.Visible = true;
                lblapplyprem1.Visible = true;
            }

            else if (Survey_type == "BASIC" && (dsppscredits.Tables[0].Rows.Count > 0 && dsppscredits.Tables[1].Rows.Count == 0))
            {
                lblcredittext.Text = "Choose one of the options below to upgrade your Free Survey";
                pps_Basic.Visible = false;
                pps_pro.Visible = true;
                pps_prem.Visible = false;
                pps_buynow.Visible = false;
                lblapplypro.Text = "(Available: " + dsppscredits.Tables[0].Rows.Count + ")";
                lblapplypro.Visible = true;
            }
            else if (Survey_type == "BASIC" && (dsppscredits.Tables[1].Rows.Count > 0 && dsppscredits.Tables[0].Rows.Count == 0))
            {
                lblcredittext.Text = "Choose one of the options below to upgrade your Free Survey";
                pps_Basic.Visible = false;
                pps_pro.Visible = false;
                pps_prem.Visible = true;
                pps_buynow.Visible = false;
                lblapplyprem.Text = "(Available: " + dsppscredits.Tables[1].Rows.Count + ")";
                lblapplyprem.Visible = true;
            }

            else if (Survey_type == "PPS_PRO" && (dsppscredits.Tables[0].Rows.Count == 0 && dsppscredits.Tables[1].Rows.Count == 0))
            {
                if(status!="Closed")
                {
                lblcredittext.Text = "You have Zero survey credits. Select one of the options below to proceed";

                if (countryName.ToUpper() != "INDIA")
                {
                    Rbl_buynow.Items[0].Attributes.Add("style", "display:none");
                }
                else
                {
                    Rbl_buynowindia.Items[0].Attributes.Add("style", "display:none");
                }
                pps_Basic.Visible = false;
                pps_pro.Visible = false;
                pps_prem.Visible = false;
                pps_buynow.Visible = true;
                }
                else
                {
                    lblcredittext.Text = "You have Zero survey credits. Select one of the options below to proceed";

                    pps_Basic.Visible = false;
                    pps_pro.Visible = false;
                    pps_prem.Visible = false;
                    pps_buynow.Visible = true;
                }
                

            }
            else if (Survey_type == "PPS_PRO" && (dsppscredits.Tables[0].Rows.Count > 0 && dsppscredits.Tables[1].Rows.Count > 0))
            {
                if (status != "Closed")
                {
                    lblcredittext.Text = "Choose one of the options below to proceed";
                    if (countryName.ToUpper() != "INDIA")
                    {
                        Rbl_prem.Items[0].Attributes.Add("style", "display:none");
                    }
                    else
                    {
                        Rbl_premindia.Items[0].Attributes.Add("style", "display:none");
                    }
                    pps_Basic.Visible = false;
                    pps_pro.Visible = false;
                    pps_prem.Visible = true;
                    pps_buynow.Visible = false;
                    lblapplyprem.Text = "(Available: " + dsppscredits.Tables[1].Rows.Count + ")";
                    lblapplyprem.Visible = true;
                }
                else
                {
                    lblcredittext.Text = "Choose one of the options below to proceed";
                    //Rbl_prem.Items[0].Attributes.Add("style", "visibility:hidden");
                    pps_Basic.Visible = true;
                    pps_pro.Visible = false;
                    pps_prem.Visible = false;
                    pps_buynow.Visible = false;
                    lblapplypro1.Text = "(Available: " + dsppscredits.Tables[0].Rows.Count + ")";
                    lblapplyprem1.Text = "(Available: " + dsppscredits.Tables[1].Rows.Count + ")";
                    lblapplypro1.Visible = true;
                    lblapplyprem1.Visible = true;
                }
               
            }
            else if (Survey_type == "PPS_PRO" && (dsppscredits.Tables[0].Rows.Count > 0 && dsppscredits.Tables[1].Rows.Count == 0))
            {
                if (status != "Closed")
                {
                    lblcredittext.Text = "You have Zero survey credits. Select one of the options below to proceed";
                    pps_Basic.Visible = false;

                    if (countryName.ToUpper() != "INDIA")
                    {
                        Rbl_pro.Items[0].Attributes.Add("style", "display:none");
                    }
                    else
                    {
                        Rbl_proindia.Items[0].Attributes.Add("style", "display:none");
                    }
                    pps_pro.Visible = true;
                    pps_prem.Visible = false;
                    pps_buynow.Visible = false;
                }
                else
                {
                    lblcredittext.Text = "You have Zero survey credits. Select one of the options below to proceed";
                    pps_Basic.Visible = false;

                   // Rbl_pro.Items[0].Attributes.Add("style", "visibility:hidden");
                    pps_pro.Visible = true;
                    pps_prem.Visible = false;
                    pps_buynow.Visible = false;
                }
                
            }
            else if (Survey_type == "PPS_PRO" && (dsppscredits.Tables[1].Rows.Count > 0 && dsppscredits.Tables[0].Rows.Count == 0))
            {
                if (status != "Closed")
                {
                    lblcredittext.Text = "Choose one of the options below to proceed";
                    if (countryName.ToUpper() != "INDIA")
                    {
                        Rbl_prem.Items[0].Attributes.Add("style", "display:none");
                    }
                    else
                    {
                        Rbl_premindia.Items[0].Attributes.Add("style", "display:none");
                    }
                    pps_Basic.Visible = false;
                    pps_pro.Visible = false;
                    pps_prem.Visible = true;
                    pps_buynow.Visible = false;
                    lblapplyprem.Text = "(Available: " + dsppscredits.Tables[1].Rows.Count + ")";
                    lblapplyprem.Visible = true;
                }
                else
                {
                    lblcredittext.Text = "Choose one of the options below to proceed";
                   // Rbl_prem.Items[0].Attributes.Add("style", "visibility:hidden");
                    pps_Basic.Visible = false;
                    pps_pro.Visible = false;
                    pps_prem.Visible = true;
                    pps_buynow.Visible = false;
                    lblapplyprem.Text = "(Available: " + dsppscredits.Tables[1].Rows.Count + ")";
                    lblapplyprem.Visible = true;
                }
                
            }
            else if ((Survey_type == "PPS_PREMIUM" && (dsppscredits.Tables[0].Rows.Count == 0 && dsppscredits.Tables[1].Rows.Count == 0) && status == "Closed") || (Survey_type == "PPS_PREMIUM" && (dsppscredits.Tables[0].Rows.Count > 0 && dsppscredits.Tables[1].Rows.Count == 0) && status == "Closed"))
            {
                lblcredittext.Text = "You have Zero survey credits. Select one of the options below to proceed";
                pps_Basic.Visible = false;
                if (countryName.ToUpper() != "INDIA")
                {
                    Rbl_pro.Items[0].Attributes.Add("style", "display:none");
                }
                else
                {
                    Rbl_proindia.Items[0].Attributes.Add("style", "display:none");
                }
                pps_pro.Visible = true;
                pps_prem.Visible = false;
                pps_buynow.Visible = false;
            }
            else if ((Survey_type == "PPS_PREMIUM" && (dsppscredits.Tables[0].Rows.Count > 0 && dsppscredits.Tables[1].Rows.Count > 0) && status == "Closed") || (Survey_type == "PPS_PREMIUM" && (dsppscredits.Tables[1].Rows.Count > 0 && dsppscredits.Tables[0].Rows.Count == 0) && status == "Closed"))
            {
                lblcredittext.Text = "Choose one of the options below to proceed";
                if (countryName.ToUpper() != "INDIA")
                {
                    Rbl_prem.Items[0].Attributes.Add("style", "display:none");
                }
                else
                {
                    Rbl_premindia.Items[0].Attributes.Add("style", "display:none");
                }
                pps_Basic.Visible = false;
                pps_pro.Visible = false;
                pps_prem.Visible = true;
                pps_buynow.Visible = false;
                lblapplyprem.Text = "(Available: " + dsppscredits.Tables[1].Rows.Count + ")";
                lblapplyprem.Visible = true;
             }
           
            else
            {
                pps_Basic.Visible = false;
                pps_pro.Visible = false;
                pps_prem.Visible = false;
                pps_buynow.Visible = true;
            }
        }
        //EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + osmSurvey.SURVEY_ID + "&surveyName=" + osmSurvey.SURVEY_NAME + "&surveyFlag=" + base.SurveyFlag)
    }
    protected void btnconform_Click(object sender, EventArgs e)
    {

        //string strUrl = EncryptHelper.EncryptQuerystring("price.aspx", "&Survey_type=" + Survey_type + "&UserId=" + userid + "&upgrade=upgrade" + "&SurveyId=" + surveyid + "&status=" + status);
        

        string surveyuser = EncryptHelper.EncryptQuerystring("myaccounts.aspx", "surveyFlag=" + surveyFlag);


    if (Rbl_basic.SelectedValue == "apply_pro")
    {
        DataSet dsproupdate = surcore.updatesurveyType(userid, "PPS_PRO", surveyid, "PRO_YEARLY",status);

      //  DataSet dsapplycreditsurvey = surcore.applycreditssurvey(userid, "PPS_PRO", surveyid);

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + surveyuser + "','_parent')", true);
    }
    else if ((Rbl_buynow.SelectedValue == "buypro") || (Rbl_buynowindia.SelectedValue == "buypro"))
    {

        string buypro = EncryptHelper.EncryptQuerystring("In/Register.aspx", Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userid + "&FromPartner=No" + "&SurveyId=" + surveyid + "&status=" + status + "&surveytype=" + Survey_type);

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + buypro + "','_parent')", true);
    }
    else if ((Rbl_buynow.SelectedValue == "buyprem") || (Rbl_buynowindia.SelectedValue == "buyprem"))
    {
        string buyprem = EncryptHelper.EncryptQuerystring("In/Register.aspx", Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userid + "&FromPartner=No" + "&SurveyId=" + surveyid + "&status=" + status + "&surveytype=" + Survey_type);
      ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + buyprem + "','_parent')", true);
    
    }
    else if (Rbl_basic.SelectedValue == "apply_prem")
    {
        DataSet dsproupdate = surcore.updatesurveyType(userid, "PPS_PREMIUM", surveyid, "PREMIUM_YEARLY", status);
        // DataSet dsapplycreditsurvey = surcore.applycreditssurvey(userid, "PPS_PREMIUM", surveyid);
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + surveyuser + "','_parent')", true);
    }
    else if (Rbl_basic.SelectedValue == "basic_buynow")
    {
      //  ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strUrl + "','_parent')", true);
    }
    else if ((Rbl_pro.SelectedValue == "apply_pro") || (Rbl_proindia.SelectedValue == "apply_pro"))
    {
        DataSet dsproupdate = surcore.updatesurveyType(userid, "PPS_PRO", surveyid, "PRO_YEARLY", status);
        // DataSet dsapplycreditsurvey = surcore.applycreditssurvey(userid, "PPS_PRO", surveyid);
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + surveyuser + "','_parent')", true);
    }

    else if ((Rbl_pro.SelectedValue == "buy_prem") || (Rbl_proindia.SelectedValue == "buy_prem"))
    {
       // ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strUrl + "','_parent')", true);
        string buy_prem = EncryptHelper.EncryptQuerystring("In/Register.aspx", Constants.LicenseType + "=" + "PPS_PREMIUM" + "&UserId=" + userid + "&FromPartner=No" + "&SurveyId=" + surveyid + "&status=" + status + "&surveytype=" + Survey_type);
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + buy_prem + "','_parent')", true);
    
    }

    else if ((Rbl_prem.SelectedValue == "apply_prem") || (Rbl_premindia.SelectedValue == "apply_prem"))
    {
        DataSet dsproupdate = surcore.updatesurveyType(userid, "PPS_PREMIUM", surveyid, "PREMIUM_YEARLY", status);
        // DataSet dsapplycreditsurvey = surcore.applycreditssurvey(userid, "PPS_PREMIUM", surveyid);
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + surveyuser + "','_parent')", true);
    }
    else if ((Rbl_prem.SelectedValue == "buy_pro") || (Rbl_premindia.SelectedValue == "buy_pro"))
    {


        string buy_pro = EncryptHelper.EncryptQuerystring("In/Register.aspx", Constants.LicenseType + "=" + "PPS_PRO" + "&UserId=" + userid + "&FromPartner=No" + "&SurveyId=" + surveyid + "&status=" + status + "&surveytype=" + Survey_type);
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + buy_pro + "','_parent')", true);
    
    }
    else if ((Rbl_buynow.SelectedValue == "prem_buynow") || (Rbl_buynowindia.SelectedValue == "prem_buynow"))
    {

      //  ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strUrl + "','_parent')", true);

    }

    if (status == "Active")
    {
            //if ((Survey_type == "BASIC") && (Rbl_basic.SelectedValue == "apply_pro"))
            //{
            //    sendFreetoPROUpgradeEmail("Active survey upgrade - free to pro single");
            //}
            //else if ((Survey_type == "BASIC") && ((Rbl_prem.SelectedValue == "apply_prem") ||(Rbl_premindia.SelectedValue == "apply_prem")) )
            //{
            //    sendFreetoPROUpgradeEmail("Active survey upgrade - free to premium single");
            //}
            //else if ((Survey_type == "PPS_PRO") && ((Rbl_prem.SelectedValue == "apply_prem") || (Rbl_premindia.SelectedValue == "apply_prem")))
            //{
            //    sendFreetoPROUpgradeEmail("Active survey upgrade - pro - single to premium single");
            //}
            if ((Survey_type == "BASIC") && (Rbl_basic.SelectedValue == "apply_pro"))
            {
                sendFreetoPROUpgradeEmail(templatefreetopro);
            }
            else if ((Survey_type == "BASIC") && ((Rbl_prem.SelectedValue == "apply_prem") || (Rbl_premindia.SelectedValue == "apply_prem")))
            {
                sendFreetoPROUpgradeEmail(templatefreetopremium);
            }
            else if ((Survey_type == "PPS_PRO") && ((Rbl_prem.SelectedValue == "apply_prem") || (Rbl_premindia.SelectedValue == "apply_prem")))
            {
                sendFreetoPROUpgradeEmail(templateprotopremium);
            }
        }

    }

    public void sendFreetoPROUpgradeEmail(string templatename)
    {
        var loggedInUser = Session["UserInfo"] as LoggedInUserInfo;
        string viewData1;
        JavaScriptSerializer js1 = new JavaScriptSerializer();
        js1.MaxJsonLength = int.MaxValue;
        clsjsonmandrill prmjson1 = new clsjsonmandrill();
      //  var prmjson1 = new jsonmandrill();
        prmjson1.key = "T74TJc3EZPYaIcvdBg--Dg";
        prmjson1.name = templatename;
        prmjson1.id = "";
        prmjson1.code = "";
        prmjson1.tag = "";
        prmjson1.template_name = "";
        prmjson1.content = "";

        viewData1 = js1.Serialize(prmjson1);

        string url1 = "https://mandrillapp.com/api/1.0/templates/info.json";


        WebClient request1 = new WebClient();
        request1.Encoding = System.Text.Encoding.UTF8;
        request1.Headers["Content-Type"] = "application/json";
        byte[] resp1 = request1.UploadData(url1, "POST", System.Text.Encoding.ASCII.GetBytes(viewData1));

        string response1 = System.Text.Encoding.ASCII.GetString(resp1);


        clsjsonmandrill jsmandrill11 = js1.Deserialize<clsjsonmandrill>(response1);

        string bodycontent = jsmandrill11.code;


        string viewData;
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = int.MaxValue;
        var prmjson = new clsjsonmandrill.jsonmandrillmerge();
        prmjson.key = "T74TJc3EZPYaIcvdBg--Dg";
        prmjson.template_name = templatename;

        string strUrl = ConfigurationManager.AppSettings["RootURL"].ToString();
        string OpturlParams = EncryptHelper.EncryptQuerystring("", "ID=" + loggedInUser.LoginName); 

        string unsubscribe = "<p style='font-size:8pt;'><a target='_blank' href='" + strUrl + "/Optout.aspx" + OpturlParams + "'>Unsubscribe</a></p>";


        List<clsjsonmandrill.template_content> mtags = new List<clsjsonmandrill.template_content>();
        mtags.Add(new clsjsonmandrill.template_content { name = "std_content00", content = "<div> Dear *|FNAME|*,</div>" });

        if (templatename == templatefreetopro)
        {
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div>Your survey *|SURVEY_NAME|* has been upgraded from Free Survey to Pro - Single Survey. </div>" });
        }
        else if (templatename == templatefreetopremium)
        {
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div>Your survey *|SURVEY_NAME|* has been upgraded from Free Survey to Premium - Single Survey. </div>" });
        }
        else if (templatename == templateprotopremium)
        {
            mtags.Add(new clsjsonmandrill.template_content { name = "std_content01", content = "<div>Your survey *|SURVEY_NAME|* has been upgraded from Pro - Single Survey to Premium - Single Survey. </div>" });
        }

       
        mtags.Add(new clsjsonmandrill.template_content { name = "std_content02", content = "<div><p style='font-size:8pt;font-family:Arial,Sans-Serif'> *|UNSUBSCRIBEINSIGHTO|* </p></div>" });
                    
        List<clsjsonmandrill.merge_vars> mvars = new List<clsjsonmandrill.merge_vars>();
        mvars.Add(new clsjsonmandrill.merge_vars { name = "FNAME", content = loggedInUser.FirstName });
        mvars.Add(new clsjsonmandrill.merge_vars { name = "SURVEY_NAME", content = SurveyName });
        mvars.Add(new clsjsonmandrill.merge_vars { name = "UNSUBSCRIBEINSIGHTO", content = unsubscribe });
        
        prmjson.template_content = mtags;
        prmjson.merge_vars = mvars;


        viewData = js.Serialize(prmjson);


        string url = "https://mandrillapp.com/api/1.0/templates/render.json";

        
        WebClient request = new WebClient();
        request.Encoding = System.Text.Encoding.UTF8;
        request.Headers["Content-Type"] = "application/json";
        byte[] resp = request.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(viewData));

        string response = System.Text.Encoding.ASCII.GetString(resp);

        string unesc = System.Text.RegularExpressions.Regex.Unescape(response);

        string first4 = unesc.Substring(0, 9);

        string last2 = unesc.Substring(unesc.Length - 2, 2);


        string unesc1 = unesc.Replace(first4, "");
        string unescf = unesc1.Replace(last2, "");

        
        clsjsonmandrill.jsonmandrillmerge jsmandrill1 = js.Deserialize<clsjsonmandrill.jsonmandrillmerge>(response);

        //   string bodycontent = jsmandrill1.code;

     
        mailMessage = new MailMessage();
        mailMessage.To.Clear();
        mailMessage.Sender = new MailAddress("Takeastart@Insighto.com", "Insighto.com");
        // mailMessage.Sender = new MailAddress(Sendmails.FROM_MAILADDRESS, "Insighto.com");
        mailMessage.From = new MailAddress("Takeastart@Insighto.com", "Insighto.com");
        if (templatename == templatefreetopro)
        {
        mailMessage.Subject = "[From Insighto] - Your survey " + SurveyName + " is now upgraded to Pro - Single survey";
        }
        else if (templatename == templatefreetopremium)
        {
        mailMessage.Subject = "[From Insighto] - Your survey " + SurveyName + " is now upgraded to Premium - Single";
        }
        else if (templatename == templateprotopremium)
        {
            mailMessage.Subject = "[From Insighto] - Your survey " + SurveyName + " is now upgraded to Premium - Single survey";
        }
        mailMessage.To.Add(loggedInUser.LoginName);
      //  mailMessage.Headers.Add("survey_id", "6666");
       // mailMessage.Headers.Add("X-MC-Tags", "survey_respondent");
        mailMessage.Body = unescf;
        //if (Sendmails.SENDER_TOCCADDRESSES != null && Convert.ToString(Sendmails.SENDER_TOCCADDRESSES.Trim()).Length > 0)
        //{
        //    mailMessage.CC.Add(Sendmails.SENDER_TOCCADDRESSES.Trim());
        //}
        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;
        smtpMail = new SmtpClient();
        smtpMail.Host = "smtp.mandrillapp.com";
        smtpMail.Port = 587;
        smtpMail.EnableSsl = true;
        smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
        smtpMail.Credentials = new System.Net.NetworkCredential("ram@knowience.com", "T74TJc3EZPYaIcvdBg--Dg");

        try
        {

            smtpMail.Send(mailMessage);
        }
        catch (SmtpFailedRecipientsException ex)
        {
            for (int i = 0; i < ex.InnerExceptions.Length; i++)
            {
                SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                if (status == SmtpStatusCode.MailboxBusy ||
                    status == SmtpStatusCode.MailboxUnavailable)
                {
                    //  Console.WriteLine("Delivery failed - retrying in 5 seconds.");


                    System.Threading.Thread.Sleep(5000);
                    smtpMail.Send(mailMessage);
                }
                else
                {
                    throw ex;
                }
            }
        }
        //catch (System.Net.Mail.SmtpException ex)
        //{
        //    throw ex;
        //}
        mailMessage.Dispose();
    }
}