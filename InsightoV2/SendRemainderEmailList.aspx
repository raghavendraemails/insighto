﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="SendRemainderEmailList.aspx.cs" Inherits="SendRemainderEmailList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupHeading">
    <!-- popup heading -->
        <div class="popupTitle">
            <h3><asp:Label ID="lblTitle" runat="server" Text="Email list"></asp:Label></h3>
        </div>
      <%--   <div class="popupClosePanel">
        <a href="#" id="hlnkPopUp" class="popupCloseLink" alt="Close" title="Close" onclick="closeModalSelf(true,'');">&nbsp;</a>
    </div>--%>
    <!-- //popup heading -->
    </div>
   <div class="defaultHeight"> </div>
    <div class="popupContentPanel">
        <div class="formPanel">
        <table border="0" cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td width="30%" align="right">
                    <asp:Label ID="ASPxLabel6" runat="server" Visible="false"  AssociatedControlID="ddlSurvey" Text="Select the survey to send reminder : " />
                    <asp:Label ID="lbl_customerid" runat="server" CssClass="bcolor" />
                </td>
                <td width="70%">
                    <asp:DropDownList ID="ddlSurvey" CssClass="dropDown" Visible="false"   runat="server" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlSurvey_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <p class="defaultHeight">
        </p>
        <div>
            <asp:ListView ID="lvSurveyReminders" runat="server" OnItemDataBound="lvSurveyReminders_ItemDataBound">
                <LayoutTemplate>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class="headerRwoGray">
                            <td align="left">
                                <asp:Label ID="lblDateProcessed" runat="server" Text="Date Processed"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblList" runat="server" Text="List"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblUniqueRespondent" runat="server" Text="Unique Respondent"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblInviteSent" runat="server" Text="Invites Sent"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblCompletes" runat="server" Text="Completes"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblNoofReminders" runat="server" Text="No of Reminders"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblReminders" Visible="false"  runat="server" Text="Reminders"></asp:Label>
                            </td>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr class="rowStyleGray">
                        <td>
                            <asp:Label runat="server" ID="lblLAUNCHEDON"><%# Convert.ToDateTime(Eval("LAUNCHED_ON")).ToString("dd-MMM-yyyy")%></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblCName"><%#Eval("CONLIST_CONTACTLIST_NAME")%></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblUNIQUERESPONDENT"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label runat="server" ID="lblSENDTO"><%#Eval("EmailCount")%></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label runat="server" ID="lblCOMPLETES"><%#Eval("COMPLETES")%></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label runat="server" ID="lblTOTAL_RESPONSES"><%#Eval("REMINDER_COUNT")%></asp:Label>
                        </td>
                        <td>
                            <%--  <asp:HyperLink ID="hlnkReminder" runat="server" Text="Send Reminders" 
                                       ></asp:HyperLink>--%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
        </div>      
    </div>
</asp:Content>
