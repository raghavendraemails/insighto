﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="ManageCategoryPopup.aspx.cs" Inherits="ManageCategoryPopup"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<div class="popupHeading">
		<!-- popup heading -->
		<div class="popupTitle">
			<h3>
				<asp:Label ID="lblCategoryHeading" runat="server" 
                    meta:resourcekey="lblCategoryHeadingResource1"></asp:Label>
            </h3>
		</div>
	 <%--  <div class="popupClosePanel">
			<a href="#" class="popupCloseLink" title="Close" onclick ="closeModalSelf(false,'');"></a>
		</div>--%>
		<!-- //popup heading -->
	</div>
	<div class="popupContentPanel">
		<!-- popup content panel -->

		 <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
             <div> <asp:Label ID="lblSuccessMsg" runat="server" Visible="False" 
                     meta:resourcekey="lblSuccessMsgResource1"></asp:Label>
             </div>
         </div>

	      <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div><asp:Label ID="lblErrMsg" runat="server" Visible="False" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label>
		    </div>
          </div>
		<div class="formPanel">
			<!-- form -->
            <p class="defaultHeight"></p>
            <div >
            
            <asp:Label ID="lblMoveSurvey" Text=" " runat="server" 
                   ></asp:Label>
            <asp:DropDownList ID="ddlSurvey" runat="server" CssClass="dropdownMedium"
                onselectedindexchanged="ddlSurvey_SelectedIndexChanged" AutoPostBack="True" 
                    meta:resourcekey="ddlSurveyResource1">
			</asp:DropDownList></div>
            <p class="defaultHeight"></p>			 
            <%-- <asp:LinkButton ID="linkbtn" CssClass="MoveSurveysIcon" OnClick="btnMoveSurveys_Click" runat="server" Text="Move Surveys to category"></asp:LinkButton>--%>
            <div class="clear"></div>
		  <div>
			<div id="ptoolbar">
			</div>
			<table id="tblJobKits">
			</table>
		</div>
			 <asp:HiddenField ID="hdntxtValue" runat="server" />
			<!-- //form -->
		</div>
		<div class="clear">
		</div>
		<!-- //popup content panel -->
	</div>
   <script type="text/javascript">
   	   var serviceUrl = 'AjaxService.aspx';
       var categoryName = '<%= Category %>';
        var surveyFlag = '<%= SurveyFlag %>';
        var surveyName = '<%= SurveyMode %>';
	   var displayMode = 'All';
	   $(document).ready(function () {
	       $("#tblJobKits").jqGrid({
	           url: serviceUrl,
	           postData: { method: "FindSurveysByCategory", mode: "All", cid: categoryName, reqType: "category",Surveyflag: surveyFlag },
	           datatype: 'json',
	           colNames: ['', surveyName , 'Folder', 'Created By', 'Sent To', 'Responses', 'Status'],
	           colModel: [
                { name: 'SURVEY_ID', index: 'SURVEY_ID', hidden: true },
				{ name: 'SurveyUrl', index: 'SURVEY_NAME', width: 230, align: 'left', editable: false, size: 300 },
				{ name: 'FolderUrl', index: 'FOLDER_NAME', width: 130, align: 'left', editable: false, size: 300 },
                { name: 'NAME', index: 'NAME', width: 130, align: 'left', editable: false, size: 300 },

				{ name: 'SEND_TO', index: 'SEND_TO', width: 130, align: 'center', editable: false, size: 300 },
				{ name: 'TOTAL_RESPONSES', index: 'TOTAL_RESPONSES', width: 130, align: 'center', editable: false, size: 300 },
				{ name: 'STATUS', index: 'STATUS', width: 130, align: 'center', editable: false, size: 300 }

                 ],
	           rowNum: <%=_recordsCount %>,
	            rowList: [10, 20, 30, 40, 50],
	           pager: '#ptoolbar',
	           gridview: true,
	           sortname: 'SURVEY_ID',
	           sortorder: "DESC",
	           viewrecords: true,
	           multiselect: true,
	           jsonReader: { repeatitems: false },
	           width: 660,
	           caption: '',
	           height: '100%',
	           loadComplete: function () {
	               $("#ContentPlaceHolder1_hdntxtValue").val('');	              
	           }
	       });

	       $("#tblJobKits").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false,refresh:false });

	       jQuery("#cb_tblJobKits").click(function () {
	           var grid = jQuery("#tblJobKits");

	           var ids = grid.jqGrid('getGridParam', 'selarrrow');

	           if (ids.length > 0) {
	               var names = [];
	               for (var i = 0, il = ids.length; i < il; i++) {
	                   var name = grid.jqGrid('getCell', ids[i], 'SURVEY_ID');
	                   names.push(name);
	               }
	               $("#ContentPlaceHolder1_hdntxtValue").val(names);

	           } else {
	               $("#ContentPlaceHolder1_hdntxtValue").val("");
	           }

	       });

	       jQuery("#tblJobKits").click(function () {
	           var s;
	           $('#cb_tblJobKits').removeAttr('checked');
	           s = jQuery("#tblJobKits").jqGrid('getGridParam', 'selarrrow');

	           var grid = jQuery("#tblJobKits");
	           var ids = grid.jqGrid('getGridParam', 'selarrrow');

	           if (ids.length > 0) {
	               var names = [];
	               for (var i = 0, il = ids.length; i < il; i++) {
	                   var name = grid.jqGrid('getCell', ids[i], 'SURVEY_ID');
	                   names.push(name);
	               }
	               $("#ContentPlaceHolder1_hdntxtValue").val(names);
	           } else {
	               $("#ContentPlaceHolder1_hdntxtValue").val("");

	           }

	       });

	   });


	   function gridReload() {
	       $("#tblJobKits").jqGrid('setGridParam', { url: serviceUrl + "?keyword=" + keyword, page: 1 }).trigger("reloadGrid");	      
	   }

</script>
</asp:Content>

