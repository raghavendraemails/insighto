﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="SurveyBasics.aspx.cs" Inherits="Insighto.Pages.SurveyBasics" Culture="auto" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            <h3>
                <asp:Label ID="lblTitle" runat="server" Text="Create Survey" 
                    meta:resourcekey="lblTitleResource1"></asp:Label>
                </h3>
                
        </div>
    <%--   <div class="popupClosePanel">
			<a href="#" class="popupCloseLink" alt="Close" title="Close" onclick ="closeModalSelf(false,'');">&nbsp;</a>
		</div>--%>
        <!-- //popup heading -->
    </div>
    <div class="defaultHeight"> </div>
    <div class="popupContentPanel">
        <!-- popup content panel -->
        <div class="formPanel">
         <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
                <div><asp:Label ID="lblSuccessMsg"  runat="server"  
                    Text="Contact list has been created succesfully." 
                        meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
                   </div>
            <!-- form -->
            <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible = "false">
					<div><asp:Label ID="lblErrMsg" runat="server" CssClass="errorMesg" 
                            Visible="False"   meta:resourcekey="lblErrMsgResource1"
					></asp:Label></div>
				</div>
                <div class="successPanel" id="divtemsurveyname" runat="server" visible="false">
                <div><asp:Label ID="lbltemsurvey"  runat="server"  
                    Text="" 
                        meta:resourcekey="lblSuccessMsgResource1"></asp:Label></div>
                   </div>
            <div><!-- ///////////////// -->
            <div class="con_login_pnl"><!-- row -->


                        <div class="loginlbl">
                           <asp:Label ID="lblSurveyName" CssClass="requirelbl" runat="server" 
                                Text="Survey Name" meta:resourcekey="lblSurveyNameResource1"  />
                        </div>
                        <div class="loginControls">
                           <asp:TextBox ID="txtSurveyName" runat="server" CssClass="textBoxMedium" 
                                MaxLength = "25" meta:resourcekey="txtSurveyNameResource1" />
                        </div>
                    </div>
                    <div class="clear"><!-- validation -->
                        <div class="reqlbl btmpadding">
                            <asp:RequiredFieldValidator SetFocusOnError="True" ID="reqSurveyName" 
                                ControlToValidate="txtSurveyName"  ErrorMessage ="Please enter survey name."
							CssClass="lblRequired"   meta:resourcekey="ReqSurveyNameResource1"  runat="server" 
                                Display="Dynamic" ></asp:RequiredFieldValidator>
                         </div>
                   <!-- //row --> 
                    </div>
                   <div class="con_login_pnl"><!-- row -->
                        <div class="loginlbl">
                           <asp:Label ID="lblCategory" runat="server" Text="Category" 
                                meta:resourcekey="lblCategoryResource1"  />
                        </div>
                        <div class="loginControls">
                        <table cellpadding="0" cellspacing="0"><tr><td><asp:DropDownList ID="ddlCategory" 
                                runat="server"  CssClass="dropdownMedium" 
                                meta:resourcekey="ddlCategoryResource1" /></td><td class="lt_padding"><%--<a href="#" title="Help"  onclick="javascript: window.open('help/2_1.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');"  class="helpLink2">&nbsp;</a>--%></td></tr></table>

                        </div>
                    <!-- //row --></div> 
                   <div class="con_login_pnl"><!-- row -->
                        <div class="loginlbl">
                           <asp:Label ID="lblFolder" runat="server" Text="Folder" 
                                meta:resourcekey="lblFolderResource1"  />
                        </div>
                        <div class="loginControls">
                            <table cellpadding="0" cellspacing="0"><tr><td><asp:DropDownList ID="ddlFolder" 
                                    runat="server" CssClass="dropdownMedium" 
                                    meta:resourcekey="ddlFolderResource1" /></td><td class="lt_padding"><%--<a href="#" title="Help"  onclick="javascript: window.open('help/2_1.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');" class="helpLink2">&nbsp;</a>--%></td></tr></table>
                        </div>
                    <!-- //row --></div>
                    <div class="con_login_pnl addNewFolderLink" id="addNewFolderLink" ><!-- row -->
                        <div class="loginlbl">
                           <asp:Label ID="lblAddFolder" runat="server" AssociatedControlID="lnkFolder" 
                                meta:resourcekey="lblAddFolderResource1" />
                        </div>
                        <div class="loginControls" >
                            <asp:HyperLink ID="lnkFolder" Text="+ Add Folder" runat="server" onclick="showContents('addNewFolderLink')"
                            NavigateUrl="#" ToolTip = "Add Folder" meta:resourcekey="lnkFolderResource1" />
                        </div>
                    <!-- //row --></div>
                    <div class="con_login_pnl" id="successMessage" style="display: none;"><!-- row -->
                        <div class="loginlbl">
                        </div>
                        <div class="loginControls">
                            
                            <span class="successMesg">Folder added successfully</span>
                        </div>
                    <!-- //row --></div> 
            <!-- //////////// --></div>

            <div id="addNewFolderUL" class="addNewFolderUL" style="display: none;"><!-- ********* -->
                <div class="con_login_pnl"><!-- row -->
                        <div class="loginlbl">
                           <asp:Label ID="lblNewFolder" CssClass="requirelbl" runat="server" 
                                Text="New Folder Name" meta:resourcekey="lblNewFolderResource1"  />
                        </div>
                        <div class="loginControls">
                            <asp:TextBox ID="txtNewFolder" runat="server" 
                                CssClass="textBoxMedium txtNewFolder" MaxLength = "25" 
                                meta:resourcekey="txtNewFolderResource1" />
                            <a id="lnkClear" class="lnkClear" runat="server" href="javascript:void(0);" title="Cancel">
                            <img src="App_Themes/Classic/Images/icon-close.gif" alt="Clear" />
                            </a>
                        </div>
                    </div>
                    <div class="con_login_pnl"><!-- validation -->
                        <div class="reqlbl">
                          <div class=" btmpadding"><asp:RequiredFieldValidator SetFocusOnError="True" 
                                  ID="reqNewFolder" ControlToValidate="txtNewFolder" ValidationGroup="vgSave" 
							CssClass="lblRequired"   meta:resourcekey="ReqNewFolderResource1"  runat="server" Display="Dynamic" /></div>
                            <p><asp:Button ID="btnSave" runat="server" Text="Save Folder" ToolTip="Save Folder" 
                                    CssClass="btn_small" OnClick ="btnSaveFolder_Click" ValidationGroup="vgSave" 
                                    meta:resourcekey="btnSaveResource1"/></p>
                         </div>
                   <!-- //row --> </div>
                  
                    <div class="con_login_pnl"><!-- validation -->
                        <div class="reqlbl btmpadding">
                          <asp:Label ID="lblSucess" runat="server" CssClass="errorMesg lblSucess" 
                                meta:resourcekey="lblSucessResource1"></asp:Label>
                         </div>
                   <!-- //row --> </div>
            <!-- ********* --></div>
            <div class="clear"></div>
             <div id="divcredits" runat="server">
         <div class="loginlbl">
                 
                <asp:Label ID="lblsurveyplan" runat="server" 
                    Text="Survey Plan" 
                     ></asp:Label>
                
         </div>
         <div class="loginControls">
         <table><tr><td>
         <table><tr><td>
             <table border="0">
                            <tr id="trfree" runat="server" style=" width:200px">
                                <td style=" width:110px"><asp:RadioButton ID="freecredit" runat="server" Checked="True" 
                                        GroupName="survey"  />Free</td>
                               <td style=" width:130px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>
                            <tr id="trprocredit" runat="server" style=" width:200px">
                                <td ><asp:RadioButton ID="procredit" runat="server" GroupName="survey" 
                                         />Pro Single</td> 
                             
                                        <td style=" width:130px">
                                         <asp:Label runat="server" ID="lblprocredits"></asp:Label>
                                        </td>
                                
                            </tr>
                            <tr id="trpremcredit" runat="server" style=" width:200px">
                                <td><asp:RadioButton ID="premcredit" runat="server" GroupName="survey" 
                                        />Premium Single</td>
                               
                                        <td style=" width:130px">
                                         <asp:Label runat="server" ID="lblpremcredits"></asp:Label>
                                        </td>
                                
                            </tr>
                        </table>





         </td></tr>
        
          </table>
          </td>
         <td> &nbsp;&nbsp;</td>
          <td>
         <%-- <span> &nbsp<a href="#" class="helpLink1"
                    title="Help" onclick="javascript: window.open('help/2_6_2.html','Help','height=460,width=715,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');return false;"></a>&nbsp;</span>
         --%> </td>
          </tr></table>
          
         </div>
         </div>
        <div class="clear"></div>
        <div id="dvRadioButtons" runat="server">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="style1"></td>
                            <td valign="top" align="left">
                            <fieldset class="fieldsetCreateSurevy">
                                <table border="0">
                            <tr>
                                <td><asp:RadioButton ID="rdoCreateNewSurvey" runat="server" Checked="True" 
                                        GroupName="Create" meta:resourcekey="rdoCreateNewSurveyResource1"  /></td>
                                <td>
                                    <asp:Label ID="lblCreateNew" runat="server" Text="Create New Survey" 
                                        meta:resourcekey="lblCreateNewResource1"></asp:Label></td>
                            </tr>
                            <tr>
                                <td><asp:RadioButton ID="rdoChooseaTemplate" runat="server" GroupName="Create" 
                                        meta:resourcekey="rdoChooseaTemplateResource1" /></td> 
                                <td><asp:Label ID="lblChooseTemplate" runat="server" Text="Choose a Template" 
                                        meta:resourcekey="lblChooseTemplateResource1"></asp:Label></td>  
                                
                            </tr>
                            <tr>
                                <td><asp:RadioButton ID="rdoCopyExistingSurvey" runat="server" GroupName="Create" 
                                        meta:resourcekey="rdoCopyExistingSurveyResource1" /></td>
                                <td><asp:Label ID="lblCopyExisting" runat="server" Text="Copy Existing Survey" 
                                        meta:resourcekey="lblCopyExistingResource1"></asp:Label></td>
                                
                            </tr>
                        </table>
                            </fieldset>
                            
                            </td>
                        </tr>
                    </table>
                        
                    
                
               </div>
               

               <div class="con_login_pnl" id="radioNewSurvey"><!-- row -->
                    <div class="loginlbl">
                        <asp:Label ID="lblbtnCreateSurvey" runat="server" 
                            meta:resourcekey="lblbtnCreateSurveyResource1"  />
                    </div>
                    <div class="loginControls">
                        <asp:Button ID="btnCreateSurvey" runat="server" CssClass="btn_small"
                        ToolTip="Create" OnClick = "btnCreateSurvey_Click" Text="Create" 
                            meta:resourcekey="btnCreateSurveyResource2"  />
                    </div>
                <!-- //row --></div>
          
            <!-- //form -->
        </div>
        <div class="clear">
        </div>
        <!-- //popup content panel -->
    </div>
    <script language="javascript" type="text/javascript">
        function showContents(itemName) {
            if (itemName == 'addNewFolderLink') {
                document.getElementById('addNewFolderLink').style.display = 'none';
                document.getElementById('addNewFolderUL').style.display = 'block';
                document.getElementById('successMessage').style.display = 'none';
            } else if (itemName == 'addNewFolderUL') {
                document.getElementById('addNewFolderLink').style.display = 'block';
                document.getElementById('addNewFolderUL').style.display = 'none';
                document.getElementById('successMessage').style.display = 'block';
            }
        }
//        function openhelpmodel() {
//            var link2 = "help/2_1.html"; 
//            OpenModalPopUP(link2, '700', '490', 'yes');
//            return false;
        //        }
        $('.lnkClear').click(function () {
            $('.addNewFolderUL').hide();
            $('.lblSucess').hide();            
            $('.addNewFolderLink').show();
            $('.txtNewFolder').val('');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeaderPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 170px;
        }
        .style2
        {
            width: 95px;
        }
    </style>
</asp:Content>

