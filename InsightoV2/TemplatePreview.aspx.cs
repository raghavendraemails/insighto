﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using App_Code;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Helpers;

namespace Insighto.Pages
{
    public partial class TemplatePreview : SurveyPageBase
    {
        public int surveyId;
        #region Private Properties

        private List<SurveyQuestionAndAnswerOptions> _surveyoptionsList;
        private readonly string _qtypePrefix = string.Empty;
        

        #endregion

        #region Event handlers

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            surveyId = QueryStringHelper.GetInt("SurveyId", 0);
            //get basic survey info
            ViewAllQuestions(e);
        }

        #endregion

        #region Private Methods


        /// <summary>
        /// Views all questions.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ViewAllQuestions(EventArgs e)
        {
            //get basic survey questions and options list.
            GetSurveyQuestionsAndOptionsList();
            foreach (var surveyoption in _surveyoptionsList.Where(surveyoption => surveyoption.Question != null))
                switch (surveyoption.QuestionType)
                {
                    case QuestionType.Choice_YesNo:
                    case QuestionType.MultipleChoice_SingleSelect:
                        LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                     PathHelper.GetRespondentControlPath(
                                                                         QuestionType.MultipleChoice_SingleSelect));
                        break;

                    case QuestionType.MultipleChoice_MultipleSelect:
                        LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                     PathHelper.GetRespondentControlPath(
                                                                         QuestionType.MultipleChoice_MultipleSelect));
                        break;

                    case QuestionType.Menu_DropDown:
                        LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                     PathHelper.GetRespondentControlPath(
                                                                         QuestionType.Menu_DropDown));
                        break;

                    case QuestionType.Matrix_SideBySide:
                        LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                     PathHelper.GetRespondentControlPath(
                                                                         QuestionType.Matrix_SideBySide));
                        break;

                    case QuestionType.Matrix_MultipleSelect:
                    case QuestionType.Matrix_SingleSelect:
                        LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                     PathHelper.GetRespondentControlPath(
                                                                         QuestionType.Matrix_SingleSelect));
                        break;

                    case QuestionType.EmailAddress:
                    case QuestionType.Numeric_SingleRowBox:
                    case QuestionType.Text_SingleRowBox:
                    case QuestionType.Text_CommentBox:
                    case QuestionType.Text_MultipleBoxes:
                        LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                     PathHelper.GetRespondentControlPath(
                                                                         QuestionType.Text_CommentBox));
                        break;

                    case QuestionType.ConstantSum:
                    case QuestionType.Ranking:
                        LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                     PathHelper.GetRespondentControlPath(
                                                                         QuestionType.ConstantSum));
                        break;

                    case QuestionType.QuestionHeader:
                    case QuestionType.PageHeader:
                    case QuestionType.Narration:
                        LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                     PathHelper.GetRespondentControlPath(
                                                                         QuestionType.Narration));
                        break;

                    case QuestionType.DateAndTime:
                        LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                     PathHelper.GetRespondentControlPath(
                                                                         QuestionType.DateAndTime));
                        break;

                    case QuestionType.ContactInformation:
                        LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                     PathHelper.GetRespondentControlPath(
                                                                         QuestionType.ContactInformation));
                        break;

                    case QuestionType.Image:
                        LoadUserControl<SurveyRespondentControlBase>(surveyoption,
                                                                     PathHelper.GetRespondentControlPath(
                                                                         QuestionType.Image));
                        break;

                    case QuestionType.Video:
                        break;
                }

            base.OnInit(e);
        }

        /// <summary>
        /// Gets the survey questions and options list.
        /// </summary>
        private void GetSurveyQuestionsAndOptionsList()
        {
            _surveyoptionsList = ServiceFactory.GetService<RespondentService>().GetSurveyQuestionAndAnswerOptionsList(surveyId, false);
        }

        /// <summary>
        /// Loads the contr fol.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="surveyoption">The surveyoption.</param>
        /// <param name="path">The path.</param>
        private void LoadUserControl<T>(SurveyQuestionAndAnswerOptions surveyoption, string path) where T : SurveyRespondentControlBase
        {
            //Load Control
            var control = Page.LoadControl(path) as T;
            if (control != null)
            {
                control.ClientIDMode = ClientIDMode.Predictable;
                control.ID = SurveyRespondentHelper.FormatControlId(_qtypePrefix, surveyoption.Question.QUESTION_TYPE_ID, surveyoption.Question.QUESTION_ID);
                control.SurveyQuestionAndAnswerOptions = surveyoption;
            }
            if (control == null) return;
            plhQuestions.Controls.Add(control);
        }

        #endregion
    }
}