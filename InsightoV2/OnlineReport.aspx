﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlineReport.aspx.cs" Inherits="OnlineReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <style type="text/css">
     .onlinebutton
      {
          background-color:Purple;
          border-radius:5px;
          font-size:20px;
          font-weight:bold;
          color:Yellow;
          width:40px;
          height:30px;
          border-color:Purple;        
      }
      
      </style>
</head>

<body style="margin-top:0px">

   
 <script src="Charts/FusionCharts.js" type="text/javascript"></script>
     <script type="text/javascript" src="Charts/jquery.min.js"></script>
      <script type="text/javascript" src="FusionCharts/FusionCharts.js"></script>   
    <script src="Charts/FusionChartsExportComponent.js" type="text/javascript"></script>

	<script type="text/javascript">
	    //The Preloader is loaded here by checking the Initialization of JavaScript Class.
	    FusionCharts.addEventListener("initialized", function (e, a) {

	        if (e.sender.id == '<%=jsstrchartname%>') {

	            $("#chartContainerParent #floader").show();
	        }

	    });
	    FusionCharts.addEventListener("Rendered", function (e, a) {

//	        if (e.sender.id == '<%=jsstrchartname%>') {
//	            FusionCharts('<%=jsstrchartname%>').exportChart();
//	        }

	        $("#chartContainerParent #floader").hide();
	    });

</script>

    <form id="form1" runat="server" style="margin-top:0px">
     <asp:ScriptManager ID="ScriptMgr1" runat="server">
    <Scripts>
   <asp:ScriptReference Path="~/UpdatePanel/updatepanelhook.fusioncharts.js" />
    </Scripts>
    </asp:ScriptManager>

      <asp:UpdatePanel ID="updpan1" runat="server">
    
    <ContentTemplate>
    

    <div>
    <table width="100%" border="0px" style="border-bottom-color:Purple;border-left-color:Purple;border-right-color:Purple;border-top-color:Purple;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-width:1px">
    <tr>
    <td width="10%" style="vertical-align:middle" align="right" >
        <asp:Button ID="btnprev"  runat="server" Text="<" CssClass="onlinebutton"  onclick="btnprev_Click" />
    </td>
    <td>
    <table width="100%" border="1px" style="border-bottom-color:Purple;border-radius:5px;border-left-color:Purple;border-right-color:Purple;border-top-color:Purple;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-width:1px">
    <tr>
    <td>
    
   
    <table border="0px" width="100%" height="565px" align="center" style="border-bottom-color:Purple;border-left-color:Purple;border-right-color:Purple;border-top-color:Purple;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-width:1px" >
    <tr>
    
    <td width="100%" align="center" >
        <table width="100%" style="vertical-align:top">
            <tr>
                <td align="left" width="50%"><asp:Label ID="lblsurveyname" runat="server" Font-Bold="true" Font-Size="Small" ForeColor="Purple"></asp:Label></td>
                <td align="right" width="50%"><asp:Image ID="imgLogo" runat="server"  /></td>
            </tr>
        </table> 
    </td>
    
    </tr>
    <tr>
    
    <td width="100%" style="background-color:white;vertical-align:top" >
    <table width="100%" border="0px" >
  
    <tr>
    <td width="100%" height="565px" valign="top">
     <asp:Panel ID="rpnl_reportsdata" runat="server"  CssClass="padno"   Width="100%" >
                         </asp:Panel>  
     <asp:Literal ID="LiteralQTInitial" runat="server" ></asp:Literal>
            <asp:Literal ID="LiteralQT1" runat="server"></asp:Literal>
            <asp:Literal ID="LiteralQT2" runat="server"></asp:Literal>
             <asp:Literal ID="LiteralQT3" runat="server"></asp:Literal>
              <asp:Literal ID="LiteralQT4" runat="server"></asp:Literal>
               <asp:Literal ID="LiteralQT13" runat="server"></asp:Literal>
               <asp:Literal ID="LiteralQT10" runat="server"></asp:Literal>
               <asp:Literal ID="LiteralQT11" runat="server"></asp:Literal>
               <asp:Literal ID="LiteralQT12" runat="server"></asp:Literal>
               <asp:Literal ID="LiteralQT15" runat="server"></asp:Literal>
                 <asp:Literal ID="Literalgrid" runat="server"></asp:Literal>
                 </td>
    </tr>
      <tr>
    <td align="left" width="100%" valign="top">
       <table width="100%">
            <tr>
                <td width="50%" align="left"><asp:Label ID="lbldisplay" runat="server" Text="" Font-Bold="true" Font-Size="Small" ForeColor="Purple" ></asp:Label> </td>
                <td width="50%" align="right"><div id="divFooter" class="pwdbylogopading" style="font-size: 12px; font-weight: 700;">Powered by <a href="http://insighto.com">Insighto.com</a></div></td>
            </tr>
       </table>
    </td>
    </tr>
    </table>
    </td>
     
    </tr>
    
    </table>

     </td>
    </tr>
    </table>
    </td>
     <td width="10%" style="vertical-align:middle" align="left">
         <asp:Button ID="btnnxt" runat="server" Text=">"  CssClass="onlinebutton" onclick="btnnxt_Click" />
     </td>
    </tr>
    </table>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
