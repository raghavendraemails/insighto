﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true"
    CodeFile="ReorderQuestions.aspx.cs" Inherits="ReorderQuestions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="popupHeading">
        <div class="popupTitle">
            <h3>
                <asp:Label ID="lblTitle" runat="server" Text="Survey questions reorder" meta:resourcekey="lblTitleResource1"></asp:Label>
            </h3>
        </div>
       <%-- <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick="closeModalSelf(false,'');" alt="Close"
                title="Close">&nbsp;</a>
        </div>--%>
        <!-- //popup heading -->
    </div>
    <div class="popupContentPanel">
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" meta:resourcekey="lblSuccMsgResource1" /></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1" /></div>
        </div>
        <div class="leftPad_0">
            <b>
                <asp:Label runat="server" Text="Select Question and  use up and down arrows to reorder questions. Please re-order the questions as desired. The question numbers will automatically get adjusted once you click SAVE"
                    ID="lblreorderNote" meta:resourcekey="lblreorderNoteResource1" />
            </b>
        </div>
        <%-- This is shown as the information text, now the error message style is implemented - 
        THis can be commented while refactoring
        <div class="informationPanelDefault" id="dvReorderNotpossible" runat="server" visible="false">
            <div>
                <asp:Label runat="server" Text="Reorder is not possible because you have applied skip logic in some of the questions in this survey. Remove Skip Logic by clicking on “Remove” link on Survey Draft to reorder your questions "
                    ID="lblReorderAlert" Visible="False" /></div>
        </div>--%>
        <div class="errorMessagePanel" id="dvReorderNotpossible" runat="server" visible="false">
            <div>
                <asp:Label runat="server" Text="Reordering questions is not possible as you have applied skip logic for some of the questions in this survey. Remove skip logic by going back to the questionnaire page and then by clicking on skip logic action icon."
                    ID="lblReorderAlert" Visible="False" meta:resourcekey="lblReorderAlertResource1" /></div>
        </div>
        <asp:UpdatePanel ID="uPanelReorder" runat="server">
            <ContentTemplate>
                <div class="formPanel" id="dvQuestionReorder" runat="server">
                    <asp:Panel runat="server" ID="pnlReorderPopupcc" meta:resourcekey="pnlReorderPopupccResource1">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="listboxDiv">
                                            <asp:ListBox runat="server" CssClass="copylistboxpnl" ID="lbQuesList" meta:resourcekey="lbQuesListResource1" />
                                        </div>
                                    </td>
                                    <td valign="middle">
                                        <asp:Button runat="server" ImageUrl="" ID="cmdMoveQuesUp" ToolTip="Up" CssClass="reorder_upimg"
                                            OnClick="cmdMoveQuesUp_Click" meta:resourcekey="cmdMoveQuesUpResource1" /><br />
                                        <asp:Button runat="server" ID="cmdMoveQuesDown" CssClass="reorder_downimg" ToolTip="Down"
                                            ImageUrl="" OnClick="cmdMoveQuesDown_Click" meta:resourcekey="cmdMoveQuesDownResource1" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="cmdMoveQuesUp" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="cmdMoveQuesDown" EventName="Click" />               
            </Triggers>
        </asp:UpdatePanel>
        <table>
            <tr>
                <td colspan="2" valign="top">
                    <div class="bottomButtonPanel">
                        <asp:Button runat="server" Text="Save" CssClass="dynamicButton" ID="btnSaveReorder"
                            OnClick="btnSaveReorder_Click" ToolTip="Save" meta:resourcekey="btnSaveReorderResource1" /></div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
