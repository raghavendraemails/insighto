﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="new_Home" MasterPageFile="~/Home.Master" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/UserControls/HomeHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc:Header ID="Header" runat="server" />


<div>
  <!--=== Slider ===-->

<div class="interactive-slider-v1 img-v3">
<div class="container" style=" text-align: center;">
<!--<div><img src="assets/img/sliders/hands_up0.jpg" alt="" width="100%"></div>-->
  <div class="group-quote">
  <h2 style="text-transform:none !important; color:#FFF900 !important; font-size:38px !important; text-shadow:2px 2px 0px rgba(80, 80, 80, 0.25) !important;">
  <span>Your   <u class="slidingVertical-top">
      <div class="slidingVertical">
      <span>People</span>
      <span>Members</span>
      <span>Partners</span>
      <span>Employees</span>
      <span>Customers</span>
    </div>
      </u> are trying to tell you something! </span> </h2>
  <h3 class="hero-h2Line"> Reach out. Engage with them - Any time. Any where.</h3>

  <div class="hero-banner">
  <!--<a href="index.html#mantra" class="btn-u btn-u-white"><b>Explore</b></a> -->
  <a href="registration.aspx" class="btn-u"><b>Create Your Free Account</b></a>
  <!--<div align="center" style="color:#FFF;">(No credit cards needed)</div>-->
  </div>
</div>
</div>
</div>


  <!--=== End Slider ===-->
  <div class="purchase" id="mantra">
    <div class="container">
      <div class="row">
        <div class="col-md-12 animated fadeInLeft" align="center"> <span>Human Opinions - Views - Responses - Emotions - Feedback</span></div>
          <!--<div>Find your <span class="informal-font">Mantra</span> to Make Informed Decisions. Strengthen your Engagement.</span> </div>-->
      </div>
      <div class="row">
      	<div class="col-md-4"></div>
        <div class="col-md-4">
        	<div class="arrow-down">
        		<a href="#" data-scroll-nav="5"><img class="wow bounce" src="assets/img/down-arrow1.png" alt="" width="48" height="39"></a>
        	</div>
        </div>
        <div class="col-md-4"></div>
      </div>
    </div>
  </div>
  <!--<div class="container">
  	<div class="row">
      	<div class="col-md-4"></div>
        <div class="col-md-4">
        	<div class="arrow-down">
        		<a href="#" data-scroll-nav="5"><img src="assets/img/down-arrow1.png" alt="" width="48" height="39"></a>
        	</div>
        </div>
        <div class="col-md-4"></div>
      </div>
  </div>-->
  <!-- Content Start -->
  <div class="container content-sm" data-scroll-index="5">
    <div class="row">
        <div class="col-md-12">
        <div class="text-center margin-bottom-10">
          <h2 class="title-v2 title-center" style="color:#1035A2 !important;">SIMPLE, EASY, INTUITIVE. INSIGHTO ENABLES YOU TO GET CRITICAL INSIGHTS.</h2>
        </div>
        <center>
        <!--<h4 style="text-transform:capitalize !important;"><i>On your Business. Market. Product. Work. Service.</i></h4>-->
        <p class="para-updated" style="font-size:16px !important;">
        From people who contribute to your success and growth and step up your engagement with them.</p>
        </center>
        </div>
        <div class="row">
        <div class="col-md-5"> <img class="wow rotateIn img-responsive" src="assets/img/mockup/imac2.png" alt=""> </div>
        <div class="col-md-7">
        <div class="headline-left margin-top-20">
          <img class="img-responsive" src="assets/img/mockup/cloud-tag.png" alt="">
        </div>
        </div>
        </div>
    </div>
  </div>


    <!-- Counter Section -->
  <div class="parallax-counter-v2 parallaxBg1" style="background-position: 50% 32px;">
    <div class="container">
      <ul class="row list-row">
        <li class="col-md-3 col-sm-6 col-xs-12 md-margin-bottom-30">
          <div class="counters rounded">
            <h5>USERS FROM</h5>
            <span class="counter">44</span>
            <h4 style="color:#954f9d;">Countries</h4>
          </div>
        </li>
        <li class="col-md-3 col-sm-6 col-xs-12 md-margin-bottom-30">
          <div class="counters rounded">
          <h5>USED IN</h5>
            <span class="counter">16</span>
            <h4 style="color:#954f9d;">LANGUAGES</h4>
          </div>
        </li>
        <li class="col-md-3 col-sm-6 col-xs-12 sm-margin-bottom-30">
          <div class="counters rounded">
          <h5>GENERATED</h5>
           <span class="counter">5.2</span> Billion
            <h4 style="color:#954f9d;">INSIGHTS</h4>
          </div>
        </li>
        <li class="col-md-3 col-sm-6 col-xs-12">
          <div class="counters rounded">
          <h5>FROM</h5>
           <span class="counter">78</span>
            <h4 style="color:#954f9d;">NATIONS</h4>
          </div>
        </li>
      </ul>
    </div>
  </div>
  <!-- Counter Section -->

  <div class="">
  <div id="features" class="container content-sm features-content" data-scroll-index="1">
    <div class="text-center margin-bottom-50">
      <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Highlights</h2>
    </div>
    <div class="row category margin-bottom-20">
      <div class="col-md-6 col-sm-6">
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"> 
            <i class="wow wobble icon-ask"></i>
            <!--<i class="wow wobble icon-custom glyphicon glyphicon-export"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Simple &amp; Easy ways to Ask Questions - Get Answers</b></h3>
            <p class="para-updated">Build your questionnaire in few minutes<br>
              Without bothering your tech team</p>
          </div>
        </div>
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-question1"></i>
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Several Options to Send Questions</b></h3>
            <p class="para-updated">Post your Questions through multiple ways <br>
              Weblink, Email, Embed, Facebook, Whatsapp </p>
          </div>
        </div>
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-ans"></i> <!--<i class="wow wobble icon-custom icon-sm rounded-x icon-bg-blue fa fa-bell-o"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Get to see Answers Instantly - in Real-time</b></h3>
            <p class="para-updated">View responses as soon as the respondent fills in <br>
              With built-in basic analytics</p>
          </div>
        </div>


      </div>
      <div class="col-md-6 col-sm-6 md-margin-bottom-40">

        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-chart"></i> <!--<i class="wow wobble icon-custom icon-sm rounded-x icon-bg-yellow fa fa-thumbs-o-up"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Intuitive and Easy-to-read Charts</b></h3>
            <p class="para-updated">Clearly demonstrate your insights <br>
              With rich animated charts and analytics</p>
          </div>
        </div>

        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"><i class="wow wobble icon-export"></i> <!--<i class="wow wobble icon-custom icon-sm rounded-x icon-bg-red fa fa-hdd-o"></i>-->
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Export Reports for Further Data Crunching</b></h3>
            <p class="para-updated">Including raw data for further analysis or presentations <br>
              Export to MS Excel, Powerpoint, Word or PDF</p>
          </div>
        </div>
        <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20"> <i class="wow wobble icon-dollar"></i>
          <div class="content-boxes-in-v3">
            <h3 class="para-updated" style="color: #1035A2 !important;"><b>Generate Income while collecting Opinions</b></h3>
            <p class="para-updated"> Monetise your Insight generation efforts <br>
              Add sponsorships and leverage space selling </p>
          </div>
        </div>

      </div>
    </div>

  </div>
  </div>
 <!-- ------------- Multi Language -------------- -->
  <div class="highlights-section content-sm pricing-page-intro">
    <div class="container text-center">
    <div class="row">
    	<div class="col-md-12">
            <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Reach Out to your Multi-Language audience</h2>

            <p class="space-lg-hor para-updated" style="font-size: 15px;">
            Create questions in a language your people are comfortable with. <br> Just key in or copy the language content.
            </p>

        </div>
    </div>
      </div>
  </div>

 <!-- ------------- Multi Language -------------- -->
 <!-- Social icon Section -->
  <div class="content-sm pricing-page-intro">
    <div class="container text-center">
    <div class="text-center margin-bottom-50">
      <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Some of the channels Insighto Surveys and Polls can be shared</h2>
    </div>
        <div class="row">
    	<img class="wow bounceInUp img-responsive" src="assets/img/mockup/social-icons.png" alt="">
    </div>
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <p class="para-updated-box" style="font-size:16px !important; margin-top:10px !important; text-align:center !important;"> Sign up for <a href="registration.aspx"><strong><u>Free plan</u></strong></a> (No Credit cards needed) and Upgrade anytime to any plan of your choice. </p>
      </div>
      <div class="col-md-2"></div>
    </div>
      </div>
  </div>
  <!-- Social icon Section -->

  <div class="one-page">
    <div class="one-page-inner one-blue-light">
      <div class="container one-blue-light">
        <div class="text-center margin-bottom-50">
          <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Serving Professionals and Business Owners since Five years</h2>
          <!--<p class="space-lg-hor para-updated">Insighto assists the Owners, Managers and Executives in any function or department -
            at any type of Organisation  to get insights from all the people who are critical for their success and growth</p>-->
        </div>

        <!-- Service Blcoks -->
        <div class="row service-box-v1 margin-bottom-40">
          <div class="col-md-3 col-sm-12 md-margin-bottom-40">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x icon glyphicon glyphicon-bullhorn"></i>
              <h2 class="heading-sm2">Marketing</h2>
              <p class="para-updated">Marketers use Insighto to generate tremendous customer insights</p>
              <!--<p class="para-updated">Marketers use Insighto to undertake programs that generate tremendous insights</p>-->
              <ul class="list-unstyled">
                <li>Voice of Customers</li>
                <li>Customer Needs & Issues</li>
                <li>Product/ Service Quality</li>
                <li>Concept/ Product Testing</li>
                <li>Customer Satisfaction</li>
                <li>Pre & Post Event Feedback</li>
                <li>Campaign Evaluation</li>
                <li>Pre-Advertisement Testing</li>
                <li>Advertisement Effectiveness</li>
                <li>Customer Validation</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 col-sm-12 md-margin-bottom-40">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x icon-line glyphicon glyphicon-briefcase"></i>
              <h2 class="heading-sm2">Sales</h2>
              <p class="para-updated">Insighto assists Sales Professionals to get insights related to </p>
              <ul class="list-unstyled">
                <li>Customer Buying Habits </li>
                <li>Product Strengths</li>
                <li>New Product/Service ideas</li>
                <li>New ways of Product usage</li>
                <li>Competitor Intelligence</li>
                <li>Sales Growth Suggestions</li>
                <li>Lead Farming</li>
                <li>Cross-sell Opportunities</li>
                <li>Up-sell Possibilities</li>
                <li>New Geos Identification</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 col-sm-12">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x icon-line glyphicon glyphicon-user"></i>
              <h2 class="heading-sm2">Human Resources</h2>
              <p class="para-updated">HR Managers use Insighto at every stage of Employee engagement</p>
              <ul class="list-unstyled">
                <li>Employees' Needs </li>
                <li>Hiring Process Evaluation</li>
                <li>New Hire Onboarding </li>
                <li>Employee Satisfaction</li>
                <li>Employee Aspirations</li>
                <li>Employee Happiness</li>
                <li>Self-Assessment Forms </li>
                <li>Employee Training Needs</li>
                <li>Post-Training Effectiveness</li>
                <li>Exit Interviews</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 col-sm-12">
            <div class="service-block service-block-default no-margin-bottom"> <i class="icon-lg rounded-x icon-line glyphicon glyphicon-globe"></i>
              <h2 class="heading-sm2">Web Publishers</h2>
              <p class="para-updated">Website managers use Insighto to increase their site's stickiness</p>
              <ul class="list-unstyled">
                <li>Monetise the Web Traffic</li>
                <li>Engagement with visitors</li>
                <li>Visitors Profiling</li>
                <li>Page Content Feedback</li>
                <li>Track Website Experience</li>
                <li>User Preferences</li>
                <li>Site Satisfaction Surveys</li>
                <li>Site Abandonment Studies</li>
                <li>Competitor Attractiveness</li>
                <li>E-commerce Feedback</li>
              </ul>
            </div>
          </div>
        </div>
        <!-- End Service Blcoks -->
      </div>
    </div>
  </div>


  <!-- Testimonial Section -->
  <div class="bg-color-light">
    <div class="container content-md">
      <div class="headline-center margin-bottom-60">
        <h2 class="title-v2 title-center" style="color:#1035A2 !important;">CUSTOMER SPEAK</h2>
        <!--<h2>WHAT PEOPLE SAY</h2>-->
        <!--<p class="para-updated">Phasellus vitae ipsum ex. Etiam eu vestibulum ante. <br>
          Lorem ipsum <strong>dolor</strong> sit amet, consectetur adipiscing elit. Morbi libero libero, imperdiet fringilla </p>-->
      </div>
      <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <div class="testimonials-v4 md-margin-bottom-50">
            <div class="testimonials-v4-in">
              <p>We used Insighto.com to research customers for an important business pitch. Their survey engine is very easy to use – my team was not only able to build the questionnaire with ease but also analyze the results in real time. Extracting consumer insights from the data was a breeze with Insighto.com and helped us win high appreciation from our Client.</p>
              <div align="right"><img src="assets/img/testimonials/testimonial-client1.png" alt="" class="" width="178" height="25"></div><br>
            </div>
            <!--<img class="rounded-x" src="assets/img/testimonials/user.jpg" alt="thumb">--> <span class="testimonials-author"> Pradeep Ramakrishnan <br>
            - Vice President, Data Analytics, Research, Tools. MudraMax Media </span> </div>
        </div>
        <div class="col-sm-2"></div>
      </div>
    </div>

    <!-- Testimonial Section -->
  </div>
  <div class="one-page-inner one-blue">
    <div class="container content">
      <div class="row">
        <h2 class="title-v2 title-center" style="color:#1035A2 !important;">THESE FOLKS LOVE US. SO WILL YOU!</h2>
        <div class="client-list-item">
          <ul>
            <li><img src="assets/img/clients/1.jpg" alt=""></li>
            <li><img src="assets/img/clients/2.jpg" alt=""></li>
            <li><img src="assets/img/clients/3.jpg" alt=""></li>
            <li><img src="assets/img/clients/4.jpg" alt=""></li>
            <li><img src="assets/img/clients/17.jpg" alt=""></li>

            <!--<li><img src="assets/img/clients/6.jpg" alt=""></li>-->
            <li><img src="assets/img/clients/10.jpg" alt=""></li>
            <li><img src="assets/img/clients/20.jpg" alt=""></li>
            <li><img src="assets/img/clients/19.jpg" alt=""></li>
            <li><img src="assets/img/clients/18.jpg" alt=""></li>
            <li><img src="assets/img/clients/5.jpg" alt=""></li>
            <li><img src="assets/img/clients/16.jpg" alt=""></li>
            <li><img src="assets/img/clients/15.jpg" alt=""></li>
            <li><img src="assets/img/clients/7.jpg" alt=""></li>
            <li><img src="assets/img/clients/9.jpg" alt=""></li>
            <li><img src="assets/img/clients/11.jpg" alt=""></li>
            <li><img src="assets/img/clients/14.jpg" alt=""></li>
            <li><img src="assets/img/clients/8.jpg" alt=""></li>
            <li><img src="assets/img/clients/6.jpg" alt=""></li>
            <li><img src="assets/img/clients/13.jpg" alt=""></li>
            <li><img src="assets/img/clients/12.jpg" alt=""></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Client Section -->
  <!-- Product Section -->
  <div class="bg-grey">
    <div class="container">
      <div class="row">
        <div class="headline-center">
          <h2 class="title-v2 title-center" style="color:#1035A2 !important;">Insighto Offerings</h2>
          <!--<h2>WHAT PEOPLE SAY</h2>-->
          <!--<p class="para-updated">Insighto  enables every professional in any organisation to generate
          invaluable insights and strengthen the engagement with the key stakeholders.</p>-->
        </div>
      </div>
    </div>
  </div>
  <div class="content-sm" data-scroll-index='2' id="productslist">
    <div class="container">
    <div class="row">
    	<div class="col-md-2"></div>
        <div class="col-md-8" align="center"><img src="assets/img/device-mockups-1.png" alt="" class="img-responsive"></div>
        <div class="col-md-2"></div>
    </div>
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-5 mobi-center">
          <h2 class="title-v2"><span class="font01">Insightful</span> <span style="color:#1035A2;">Surveys</span></h2>
          <!--<img class="img-responsive" src="assets/img/mockup/mockup-surveys.png" alt="">-->
         <p class="para-updated" style="font-size:16px !important;">
          Simple, Easy and Better Web Surveys
          </p>
          <ul class="list-unstyled lists-v2 margin-bottom-30">
              <li><i class="fa fa-check color-green"></i>Build Questions within no time</li>
              <li><i class="fa fa-check color-green"></i>Sample Templates that are of great help</li>
              <li><i class="fa fa-check color-green"></i>Preview the Questionnaire before sending</li>
              <li><i class="fa fa-check color-green"></i>Send Questions through multiple ways</li>
              <li><i class="fa fa-check color-green"></i>Instant display of responses with base analytics</li>
              <li><i class="fa fa-check color-green"></i>View every individual response</li>
              <li><i class="fa fa-check color-green"></i>Export data for further analysis</li>
          </ul>
          <br>
          <div style="margin:2px 0px 6px 0px;">
          <!--<a href="surveys/index.html" class="btn-u btn-brd btn-brd-hover btn-u-dark" target="_blank"></a>-->
          <a href="/surveys/Home.aspx" class="btn-u">Learn More</a>
          </div>
          </div>
        <div class="col-md-6 mobi-center">
          <h2 class="title-v2"><span class="font01">Smart</span> <span style="color:#1035A2;">Polls</span></h2>
          <!--<img class="img-responsive" src="assets/img/mockup/mockup-polls.png" alt="">-->
          <p class="para-updated" style="font-size:16px !important;">
          Simple - Innovative yet Versatile Polls
          </p>
          <ul class="list-unstyled lists-v2 margin-bottom-30">
              <li><i class="fa fa-check color-green"></i>Text  or Image or Video Polls</li>
              <li><i class="fa fa-check color-green"></i>Slide In your Poll or Embed it or send as Weblink</li>
              <li><i class="fa fa-check color-green"></i>Question creation with instant preview</li>
              <li><i class="fa fa-check color-green"></i>Many options to customize the Poll</li>
              <li><i class="fa fa-check color-green"></i>Instant results display - with charming charts</li>
              <li><i class="fa fa-check color-green"></i>Reports giving Location, Device and Web pages</li>
              <li><i class="fa fa-check color-green"></i>Run and Manage multiple polls - on different pages and sections</li>
          </ul>

        <br>
          <div style="margin:7px 0px 6px 0px;">
          <!--<a href="polls/index.html" class="btn-u btn-brd btn-brd-hover btn-u-dark" target="_blank">Learn More</a>-->
          <a href="/polls/Home.aspx" class="btn-u">Learn More</a></div> </div>
		<div class="col-md-1"></div>
      </div><br><br>
    </div>
  </div>
  <!--<div class="bg-grey content-sm" data-scroll-index='3'>
    <div class="container">
      <div class="row">
        <div class="col-md-6">  </div>

      </div>
      <br>
    </div>
  </div>-->
  <!-- Product Section -->
  <!-- Pricing Section -->
  <div class="bg-grey content-sm pricing-page-intro" data-scroll-index='4' id="pricing">
    <div class="container text-center">
    <div class="row">
    	<div class="col-md-12">
            <h2 class="title-v2 title-center" style="color:#1035A2 !important;">A PRICING PLAN TO SUIT YOUR UNIQUE NEEDS</h2>
            <p class="space-lg-hor para-updated" style="font-size: 15px;">With economical and innovative plans and pricing, Insighto products are pure Value for Money spent.</p>
            <p class="space-lg-hor para-updated" style="font-size: 15px;">Sign up for Free plan (No Credit cards needed) and Upgrade anytime to any plans of your choice.</p>
        </div>
    </div>
    <br>
    <div class="row">
    <div class="col-md-2">&nbsp;</div>
    <div class="col-md-4"><center><a href="surveys/pricing.aspx" class="btn-u">Plans & Pricing of Insighto Surveys</a> </center></div>
    <div class="col-md-3"><center><a href="polls/pricing.aspx" class="btn-u">Plans & Pricing of Insighto Polls</a></center></div>
    <div class="col-md-3">&nbsp;</div>
    </div>
      </div>
  </div>

  <!-- Pricing Section -->
  <div class="rajesh-band" id="mantra" data-scroll-index="5">
    <div class="container">
      <div class="row">
        <div class="col-md-12 animated fadeInLeft" align="center">
        	<h3 style="color:#fff !important; text-transform:none !important;">"Having an insight is to see what everyone is seeing but notice what few else do"</h3>
            <h4 style="color:#fff !important">- Rajesh Setty   <span style="font-size:14px;">Entrepreneur, Author, Alchemist</span></h4>
        </div>
      </div>
    </div>
  </div>
  <!--end container-->

</div>
<!--/wrapper-->
<!--Start of Zopim Live Chat Script-->

<script type="text/javascript">

    function activateZopim() {

        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) { z._.push(c) }, $ = z.s =

d.createElement(s), e = d.getElementsByTagName(s)[0]; z.set = function (o) {
    z.set.

_.push(o)
}; z._ = []; z.set._ = []; $.async = !0; $.setAttribute('charset', 'utf-8');

            $.src = '//cdn.zopim.com/?bYM7XxoDiH1Gv57aQNwmeiatKZbOCdA7'; z.t = +new Date; $.

type = 'text/javascript'; e.parentNode.insertBefore($, e)
        })(document, 'script');

    }

</script>

<!--End of Zopim Live Chat Script-->

</asp:Content>
