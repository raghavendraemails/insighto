﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Helpers;
using CovalenseUtilities.Services;
using App_Code;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Helpers;
using System.Collections;
using Insighto.Data;
using System.Globalization;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class inter_pricing : System.Web.UI.Page
{
    Hashtable typeHt = new Hashtable();
    int userId = 0;
    string transType = "New";
    string country;
    LoggedInUserInfo loggedInUserInfo;
    private string licenseType = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        
            string ipAddress="";
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
              //  ipAddress = "93.186.23.87";
            }
           
            var country = Utilities.GetCountryName(ipAddress);
            ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
         //   ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
            country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
     
     


        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (typeHt.Contains("Type"))
                licenseType = typeHt["Type"].ToString();
            if (typeHt.Contains(Constants.USERID))
                userId = ValidationHelper.GetInteger(typeHt[Constants.USERID].ToString(), 0);
            if (typeHt.Contains("Flag"))
                transType = Convert.ToString(typeHt["Flag"]);

        }

        if (!IsPostBack)
        {
            SessionStateService sessionStateService = new SessionStateService();
            loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            if (loggedInUserInfo == null)
            {
                Response.Redirect("Home.aspx");
            }

            string NavUrl = Constants.LicenseType + "=" + "PRO_MONTHLY" + "&UserId=" + userId;

            PayMonth.HRef = EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl);

            if (country.ToUpper() == "INDIA")
            {
                lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "9,900");
                lblpremium.Attributes.Add("Class", "priceRs");
                lblpro.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "4,500");
                lblpro.Attributes.Add("Class", "priceRs");
                // lblMonthly.Text = string.Format("{0}{1}{2}{3}", "","", "","450");
                //  lblMonthly.Attributes.Add("Class", "priceRs");


                lblfree.Text = string.Format("{0}{1}{2}{3}", "", "`", "", "0");



                //  lblMonthly.Attributes.Add("Class", "priceRs");

                // divFaqOtherContent.Visible = false;

                //  divFaqIndiaContent.Visible = true;

            }

            else
            {

                ////   Label1.Text = string.Format("{0}{1}{2}{3}","", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), "", GetPriceByCountryName(country, "Yearly"));

                //////  Label1.Attributes.Add("Class", "monthlyHeading");
                ////   // lnkBtnMonthly.Text = string.Format("{0}{1}{2}{3}", "SAVE ON ANNUAL PLAN - ", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), " ", GetPriceByCountryName(country, "Yearly"));

                ////   lblMonthly.Text = string.Format("{0}{1}{2}{3}","", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), "", GetPriceByCountryName(country, "MONTHLY"));

                ////   lblMonthly.Attributes.Add("Class", "monthlyHeading");

                ////   Label2.Text = string.Format("{0}{1}{2}{3}", "", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), "", "0");

                ////   // divFaqOtherContent.Visible = true;

                ////   //  divFaqIndiaContent.Visible = false;

                //lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "299");
                //lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "159");


                ////  Label1.Attributes.Add("Class", "monthlyHeading");
                //// lnkBtnMonthly.Text = string.Format("{0}{1}{2}{3}", "SAVE ON ANNUAL PLAN - ", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), " ", GetPriceByCountryName(country, "Yearly"));

                //lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $15");

                //// lblMonthly.Attributes.Add("Class", "monthlyHeading");

                //lblfree.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");
                string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
                SqlConnection strconn = new SqlConnection(connStr);
                strconn.Open();
                SqlCommand scom = new SqlCommand("sp_getPriceLicenseCountry", strconn);
                scom.CommandType = CommandType.StoredProcedure;
                scom.Parameters.Add(new SqlParameter("@countryname", SqlDbType.VarChar)).Value = country;
                scom.Parameters.Add(new SqlParameter("@licensetype", SqlDbType.VarChar)).Value = "";
                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet pricedetails = new DataSet();
                sda.SelectCommand = scom;
                sda.Fill(pricedetails);
                string pro_monthlicencetype = pricedetails.Tables[0].Rows[0][1].ToString();

                //  string pro_monthprice = pricedetails.Tables[0].Rows[0][4].ToString();
                string pro_yearlicencetype = pricedetails.Tables[0].Rows[1][1].ToString();
                // string pro_yearprice = pricedetails.Tables[0].Rows[1][4].ToString();

                string prem_yearlicencetype = pricedetails.Tables[0].Rows[2][1].ToString();
                //  string prem_yearprice = pricedetails.Tables[0].Rows[2][4].ToString();
                strconn.Close();

                DataRow[] dr = pricedetails.Tables[0].Select("License_Type = '" + "PRO_MONTHLY'");

                string pro_monthprice = "";
                if (dr.Length > 0)
                {
                    pro_monthprice = dr[0].ItemArray[4].ToString();
                }
                else
                {
                    pro_monthprice = "";
                }


                DataRow[] dr1 = pricedetails.Tables[0].Select("License_Type = '" + "PRO_YEARLY'");

                string pro_yearprice = "";
                if (dr1.Length > 0)
                {
                    pro_yearprice = dr1[0].ItemArray[4].ToString();
                }
                else
                {
                    pro_yearprice = "";
                }


                DataRow[] dr2 = pricedetails.Tables[0].Select("License_Type = '" + "PREMIUM_YEARLY'");

                string prem_yearprice = "";
                if (dr2.Length > 0)
                {
                    prem_yearprice = dr2[0].ItemArray[4].ToString();
                }
                else
                {
                    prem_yearprice = "";
                }

                lblpremium.Text = string.Format("{0}{1}{2}{3}", "", "$", "", prem_yearprice);
                lblpro.Text = string.Format("{0}{1}{2}{3}", "", "$", "", pro_yearprice);
                //  Label1.Attributes.Add("Class", "monthlyHeading");
                // lnkBtnMonthly.Text = string.Format("{0}{1}{2}{3}", "SAVE ON ANNUAL PLAN - ", GetCurrencySymbol(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(country.ToLower())), " ", GetPriceByCountryName(country, "Yearly"));
                lblMonthly.Text = string.Format("{0}{1}{2}{3}", "", "", "", "Monthly Plan - $" + pro_monthprice);

                // lblMonthly.Attributes.Add("Class", "monthlyHeading");

                lblfree.Text = string.Format("{0}{1}{2}{3}", "", "$", "", "0");

            }



            // dvPageInfo.InnerHtml = Utilities.ResourceMessage("dvPageInfo");

            //FormatCurrencyAndSymbol();

            //    SetPermissions();


        }

    }
    protected void button3_Click(object sender, EventArgs e)
    {
        string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId;


        Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));

    }
    private string GetCurrencySymbol(string countryName)
    {
        var ci = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.EnglishName.Contains(countryName)).FirstOrDefault();
        return ci.NumberFormat.CurrencySymbol;
    }

    protected void button4_Click(object sender, EventArgs e)
    {
        string NavUrl = Constants.LicenseType + "=" + "PRO_YEARLY" + "&UserId=" + userId;


        Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));

        // Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", "Type=PRO_YEARLY"));

    }
    //private void FormatCurrencyAndSymbol()
    // {
    //     rbtnMonthly.Text = Utilities.FormatCurrency(Math.Round(450.00, 2), false, true);
    //     rbtnQuarterly.Text = Utilities.FormatCurrency(Math.Round(1300.00, 2), false, true);
    //     rbtnYear.Text = Utilities.FormatCurrency(Math.Round(4500.00, 2), false, true);
    // }

    private double GetPriceByCountryName(string countryNAme, string subscription)
    {
        var price = ServiceFactory<OrderInfoService>.Instance.GetPriceByCountryName(countryNAme);
        if (subscription == "MONTHLY")
            return Convert.ToDouble(price.MONTHLY_PRICE);
        else
            return Convert.ToDouble(price.YEARLY_PRICE);
    }
    //private void SetPermissions()
    //   {
    //       var userType = Utilities.GetUserType();
    //       if (userType != UserType.None)
    //       {
    //           imgTryItFree.Enabled = false;
    //           imgTryItFree.Style.Add("cursor", "default");
    //           if (userType == UserType.PRO_MONTHLY)
    //           {
    //               rbtnMonthly.Checked = true;
    //           }
    //           else if (userType == UserType.PRO_QUARTERLY)
    //           {
    //               rbtnQuarterly.Checked = true;
    //           }
    //           else if (userType == UserType.PRO_YEARLY)
    //           {
    //               rbtnYear.Checked = true;
    //           }
    //       }
    //       else
    //       {
    //           rbtnMonthly.Enabled = true;
    //           imgTryItFree.Enabled = true;
    //           rbtnYear.Checked = true;
    //       }
    //   }



    protected void button1_Click(object sender, EventArgs e)
    {
        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=0"));

    }
    protected void button2_Click(object sender, EventArgs e)
    {
        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=0"));

    }
    protected void button6_Click(object sender, EventArgs e)
    {
        string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId;


        Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));

    }
    protected void button5_Click(object sender, EventArgs e)
    {
        string NavUrl = Constants.LicenseType + "=" + "PREMIUM_YEARLY" + "&UserId=" + userId;


        Response.Redirect(EncryptHelper.EncryptQuerystring("~/In/Register.aspx", NavUrl));

    }
}