﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ManageCategory.aspx.cs" Inherits="ManageCategory"%>

<%@ Register Src="UserControls/ProfileRightPanel.ascx" TagName="ProfileRightPanel"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="myProfileLeftPanel">
        <div class="marginNewPanel" style="margin-top:0;">
            <div class="signUpPnl">
                <!-- customer details -->
                <h3>
                    <asp:Label ID="lblTitle" runat="server" Text="Manage Category" 
                        meta:resourcekey="lblTitleResource1"></asp:Label>
                </h3>
                <div class="messagePanelProfile">
                   <%-- <div class="errorPanel" id="recordDeleted" style="display: none;">
                        <span class="successMesg">Record deleted successfully</span>
                    </div>--%>
                    <div class="informationPanelDefault">
                        <div>
                            <asp:Label ID="lblHelpText" runat="server" 
                                Text="Click on the view icon to move surveys to another Category." 
                                meta:resourcekey="lblHelpTextResource1"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="surveysGridPanel">
                    <div>
                        <div id="ptoolbar">
                        </div>
                        <table id="tblJobKits">
                        </table>
                    </div>
                      <input type="hidden" runat="server" id="hdnSurveyFlag" class="hdnSurveyFlag" />
                </div>

                <!-- //customer details -->
            </div>
        </div>
    </div>
    <uc:ProfileRightPanel ID="ucProfileRightPanel" runat="server" />
    <div class="clear">
    </div>
    <script type="text/javascript">

        var serviceUrl = 'AjaxService.aspx';
        var displayMode = 'Active';
         var surveyFlag = $(".hdnSurveyFlag").val();
        $(document).ready(function () {
            $("#tblJobKits").jqGrid({
                url: serviceUrl,
                postData: { method: "GetCategory", mode: displayMode,Surveyflag: surveyFlag },
                datatype: 'json',
                colNames: ['CATEGORY', ''],
                colModel: [
                { name: 'PARAM_VALUE', index: 'PARAM_VALUE', width: 240, align: 'left', editable: false, resizable: false, size: 100 },
                { name: 'ViewLinkUrl', index: 'ViewLinkUrl', width: 30, align: 'right', sortable: false, size: 30, formatter: ViewLinkFormatter }
                ],
                rowNum: <%=_recordsCount %>,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'PARAM_VALUE',
                sortorder: "asc",

                viewrecords: true,
                jsonReader: { repeatitems: false },
                width: 625,
                caption: '',
                height: '100%',
                loadComplete: function () {
                    $('a[rel*=framebox]').click(function (e) {
                        e.preventDefault();
                        ApplyFrameBox($(this));
                    });
                }

            });

            $("#tblJobKits").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false,refresh:false });
            $("#tblJobKits").jqGrid('setLabel', 'PARAM_VALUE', '', 'textalignleft');


        });

        function ViewLinkFormatter(cellvalue, options, rowObject) {
            return "<a href='" + cellvalue + "'  class='button-small-gap icon-View-active' rel='framebox' w='700' h='480' scrolling='yes' title='View & Move'></a>";
        }


        function gridReload() {
            var keyword = $("#txtKeyword").val();
            $("#tblJobKits").jqGrid('setGridParam', { url: serviceUrl + "?keyword=" + keyword, page: 1 }).trigger("reloadGrid");
        }

//    $(document).ready( function()
//    {
//        //Left side buttons
//        $(".jqGrid #first_ptoolbar").attr('title', 'First');
//        $(".jqGrid #prev_ptoolbar").attr('title', 'Previous');

//        //Right Side buttons
//        $("#next_ptoolbar").attr('title', 'Next');
//        $("#last_ptoolbar").attr('title', 'Last');
//    });

    </script>
</asp:Content>
