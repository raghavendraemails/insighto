﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="contact.aspx.cs" Inherits="new_Contact" MasterPageFile="~/Home.Master" %>
<%@ Register TagPrefix="uc" TagName="OtherHeader" Src="~/UserControls/OtherHeader.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc:OtherHeader ID="OtherHeader" runat="server" />
<div> 

  <!--=== Content Part ===-->
  <div class="breadcrumbs-v3 img-v1 text-center">
    <div class="container">
      <h2 style="text-transform: none !important; color: rgb(255, 249, 0) !important; font-size: 38px !important; text-shadow: rgba(80, 80, 80, 0.247059) 2px 2px 0px !important; display: block;"> <span>Contact Insighto </span></h2>
      <!--<h1 style="color:#fff900 !important; text-transform:none !important;">Survey Services</h1>
      <h5 style="color:#FFF !important;">Don’t let your lack of time or skill to create surveys abstain you from useful insights. </h5>
      <h5 style="color:#FFF !important;">Let Insighto Experts build powerful surveys for you.</h5>--> 
    </div>
  </div>
  
<!--=== Content Part ===-->
  <div class="container">
    <div class="row" id="divcontactus" runat="server">
      <h4 id="indic" runat="server"><span style="color:#F00;">*</span>indicates required fields</h4>

        <div class="col-md-1"></div>
        <div class="col-md-8">
            <div class="form-group margin-bottom-20">
                <label for="inputEmail1" class="col-lg-3 control-label">Your Name <span class="color-red">*</span></label>
                <div class="col-lg-9">
                    <asp:TextBox CssClass="form-control" ID="txtName" runat="server"></asp:TextBox>
                    <asp:Label id="invalidname" runat="server" ForeColor="Red" Font-Size="12" Text="Invalid name" Visible="false"></asp:Label>
                </div>
            </div>
            <div class="form-group margin-bottom-10">
                <label for="inputEmail1" class="col-lg-3 control-label">Your Email <span class="color-red">*</span></label>
                <div class="col-lg-9">
                    <asp:TextBox CssClass="form-control" ID="txtMail" runat="server"></asp:TextBox>
                    <asp:Label id="invalidemail" runat="server" ForeColor="Red" Font-Size="12" Text="Invalid email" Visible="false"></asp:Label>
                </div>
            </div>
            <div class="form-group margin-bottom-10" id="divphonenumber" runat="server">
                <label for="inputEmail1" class="col-lg-3 control-label">Phone Number </label>
                <div class="col-lg-9">
                    <asp:TextBox CssClass="form-control" ID="txtPhno" runat="server"></asp:TextBox>
                    <asp:Label id="invalidphone" runat="server" ForeColor="Red" Font-Size="12" Text="Invalid phone" Visible="false"></asp:Label>
                </div>
            </div>
            <div class="form-group margin-bottom-10">
                <label for="inputEmail1" class="col-lg-3 control-label">Attention <span class="color-red">*</span></label>
                <div class="col-lg-9">
                    <asp:DropDownList ID="ddlAttentionvalues" CssClass="form-control"  runat="server" TabIndex="13" meta:resourcekey="ddlAttentionvaluesResource1">
                    </asp:DropDownList>                
                    <asp:Label id="invalidattentionvalue" runat="server" ForeColor="Red" Font-Size="12" Text="Select an attention option" Visible="false"></asp:Label>
                </div>
            </div>
            <div class="form-group margin-bottom-10">
                <label for="inputEmail1" class="col-lg-3 control-label">Your Message <span class="color-red">*</span></label>
                <div class="col-lg-9">
                    <asp:TextBox CssClass="form-control" ID="txtMessage" runat="server" TextMode="MultiLine" Text=""></asp:TextBox>
                    <h5>Maximum 500 Characters</h5>
                    <asp:Label id="invalidmessage" runat="server" ForeColor="Red" Font-Size="12" Text="Enter your message" Visible="false"></asp:Label>
                </div>
            </div>
            
            <div class="form-group margin-bottom-10" id="divcaptcha" runat="server">
                <label for="inputEmail1" class="col-lg-3 control-label">&nbsp;</label>
                <div class="col-lg-9">
                    <label class="input input-captcha">
                    <img src="/In/Captcha.aspx" width="100" height="32" alt="Captcha image">
                    <h5>Type the above number</h5>
                    <asp:TextBox CssClass="form-control" ID="captcha" runat="server"></asp:TextBox>
                    <asp:label ID="captchaerror" ForeColor="Red" Font-Bold="true" Font-Size="12" Visible="false" runat="server" Text="Invalid captcha text"></asp:label>
                </label>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail1" class="col-lg-3 control-label">&nbsp;</label>
                <div class="col-lg-9">
                     <asp:Button ID="btnsubmit" runat="server" CssClass="btn-u-blue" ToolTip="Submit"
                            Text="Send" onclick="btnsubmit_Click" 
                        meta:resourcekey="btnsubmitResource1" />
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
    <%--<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>--%>
  </div>
  <!--/container--> 
  <div class="container center-block" id="divsuccess" runat="server" style="display:none;">
    <div class="row">
        <div class="col-md-12 center-block text-center">
            <div class="form-group">
                <label class="alert-success" runat="server" id="lblthankyou">Thank you for contacting us. We will get back to you within 24 hrs.</label>
            </div>
        </div>
        </div>
    </div>

</div>
    <script>
        $(document).ready(function () {
            $(".footer-v1").css("position", "absolute");
        });
    </script>
<!--/wrapper--> 
</asp:Content>
