﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SurveyRespondentPreview.aspx.cs"
    MasterPageFile="~/SurveyRespondent.master" Inherits="Insighto.Pages.SurveyRespondentPreview" %>
    
<%@ Register Src="~/UserControls/RespondentControls/RespondentPopupHeader.ascx" TagName="RespondentPopupHeader"
    TagPrefix="uc" %>
<%@ Register Src="~/UserControls/RespondentControls/SurveyRespondentControl.ascx"
    TagName="SurveyRespondentControl" TagPrefix="uc" %>
<asp:Content ContentPlaceHolderID="ContentHead" runat="server" ID="headerPlaceHolder">
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPopupHeader" runat="server" ID="popupHeaderPlaceHolder">
    <uc:RespondentPopupHeader ID="ucRespondentPopupHeader" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentMain" runat="server" ID="Content1">
    <div class="questionViewpanel">
        <div class="informationPanelDefault">
            <div>
                Preview of the survey that would be seen by respondent. Responses to the preview
                will not be recorded</div>
        </div>
        <div class="clear"></div>
        <div class="questionIncludePanel">
        <uc:SurveyRespondentControl ID="ucSurveyRespondentControl" runat="server" />
        </div>
    </div>
</asp:Content>
