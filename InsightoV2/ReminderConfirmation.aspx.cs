﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Exceptions;
using App_Code;


public partial class ReminderConfirmation : SurveyPageBase
{

    #region "Variables"

    Hashtable typeHt = new Hashtable();
    int reminderId = 0;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = false;

        if (Request.QueryString["key"] != null)
        {
            typeHt = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"].ToString());

            if (typeHt.Contains("ReminderId"))
                reminderId = int.Parse(typeHt["ReminderId"].ToString());
        }

        if (reminderId > 0)
        {
            var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            var surveyService = ServiceFactory.GetService<SurveyService>();
            var surveyremainderList = surveyService.GetSendReminderInfo(reminderId);            

            if (surveyremainderList.Any())
            {
                var bouncedCount = surveyService.GetBouncedMailCountByLaunchId(surveyremainderList[0].LAUNCH_ID);
                if (surveyremainderList[0].SEND_COUNT > 0)
                {

                    lblSuccMsg.Text = Utilities.ResourceMessage("ReminderConfirmation_Success_Sent"); //"Reminder Sent Successfully.";
                    dvSuccessMsg.Visible = true;
                    litMessagesent.Text = Utilities.ResourceMessage("ReminderConfirmation_InComplete_Survey");//"<b>Your reminder email has been sent to all those who have not yet completed the survey.</b>";
                    if (surveyremainderList[0].OPT_COUNT > 0)
                    {
                        lbl_messagecount.Text = Convert.ToString(surveyremainderList[0].OPT_COUNT) + " message(s) could not be sent due to opt-out";
                    }
                    lblrecpcount.Text = Convert.ToString(surveyremainderList[0].SEND_COUNT-bouncedCount);
                    DateTime dt = Convert.ToDateTime( surveyremainderList[0].CREATED_ON);
                    
                    dt = CommonMethods.GetConvertedDatetime(dt, userDetails.StandardBIAS, true);
                    lbl_remdate.Text = Convert.ToDateTime(dt).ToString("dd-MMM-yyyy") + " at " + dt.ToShortTimeString();

                }

                else
                {
                    lblErrMsg.Text = Utilities.ResourceMessage("ReminderConfirmation_Error_Sending"); //"Reminder Send Failed";
                    dvErrMsg.Visible = true;
                    litMessagesent.Text = Utilities.ResourceMessage("ReminderConfirmation_Fail_Sending"); //"<b>Your reminder email has not been sent to any recipients.</b>";
                    tr_messagecount.Visible = false;
                    lblrecpcount.Text = "0";
                    lbl_remdate.Text = "N/A";
                }
            }
        }
    }
    protected void btnMyInsighto_Click(object sender, EventArgs e)
    {
        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(),"surveyFlag="+SurveyFlag));
    }
}