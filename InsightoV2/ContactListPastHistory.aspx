﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="ContactListPastHistory.aspx.cs" Inherits="Insighto.Pages.ContactListPastHistory" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" Runat="Server">
    <div class="popupHeading">
        <!-- popup heading -->
        <div class="popupTitle">
            
               <h3><asp:Label ID="lblTitle" runat="server" Text="List of Surveys "></asp:Label></h3>
            
        </div>
      <%--  <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick ="closeModalSelf(false,'');">&nbsp;</a>
        </div>--%>
       <%--<div class="popupClosePanel">
            <a href="ManageFolders.aspx">X</a>
        </div>--%>
        <!-- //popup heading -->
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="popupContentPanel">
		<!-- popup content panel -->
		<div class="errorPanel" id="dvErrMsg" runat="server">
		    <asp:Label ID="lblSuccessMsg" runat="server" Visible="False" 
                Text="Changed Succesfully" meta:resourcekey="lblSuccessMsgResource1"></asp:Label>
		    <asp:Label ID="lblErrMsg" runat="server" Visible="False" 
                Text="Please select survey" meta:resourcekey="lblErrMsgResource1"></asp:Label>
		</div>
		<div class="formPanel">
			<!-- form -->
            
			

           
		  <div>
			<div id="ptoolbar">
			</div>
			<table id="tblJobKits">
			</table>
		</div>
            <asp:HiddenField ID="hdntxtValue" runat="server" />

			<!-- //form -->
		</div>
		<div class="clear">
		</div>
		<!-- //popup content panel -->
	</div>
    <script type="text/javascript">
        

        var serviceUrl = 'AjaxService.aspx';
        var contactId = <%= ListId %>;
        $(document).ready(function () {
            $("#tblJobKits").jqGrid({
                url: serviceUrl,
                postData: { method: "GetSurveysByContactId",  cid: contactId },
                datatype: 'json',
                colNames: ['Survey Name', 'Created On', 'Status' ],
                colModel: [
				{ name: 'SURVEY_NAME', index: 'SURVEY_NAME', width: 130, align: 'left', editable: false, resizable: false, size: 300 },
                 { name: 'CREATED_ON', index: 'CREATED_ON', width: 50, align: 'center', formatter: ndateFormatter },
                { name: 'STATUS', index: 'STATUS', width: 35, align:'center', resizable: false },
              
				 ],
                rowNum: 10,
                rowList: [10, 20, 30, 40, 50],
                pager: '#ptoolbar',
                gridview: true,
                sortname: 'SURVEY_NAME',
                sortorder: "asc",
                viewrecords: true,               

                jsonReader: { repeatitems: false },
                width: 650,
                caption: '',
                height: '100%',
                loadComplete: function () {
                }
            });
              $("#tblJobKits").jqGrid('setLabel', 'SURVEY_NAME', '', 'textalignleft');
            $("#tblJobKits").jqGrid('navGrid', '#ptoolbar', { del: false, add: false, edit: false, search: false,refresh:false });
            jQuery("#tblJobKits").click(function () {
                var s; s = jQuery("#tblJobKits").jqGrid('getGridParam', 'selarrrow');
                var grid = jQuery("#tblJobKits");
                var ids = grid.jqGrid('getGridParam', 'selarrrow');
                if (ids.length > 0) {
                    var names = [];
                    for (var i = 0, il = ids.length; i < il; i++) {
                        var name = grid.jqGrid('getCell', ids[i], 'SURVEY_NAME');
                        names.push(name);
                    }
                    $("#ContentPlaceHolder1_hdntxtValue").val(names);

                }


            });

        });

        function gridReload() {
            $("#tblJobKits").jqGrid('setGridParam', { url: serviceUrl, page: 1 }).trigger("reloadGrid");
        }

        function ndateFormatter(cellval, opts, rwdat, _act) {
          
            if (cellval != "" && cellval!=null) {
            var time = cellval.replace(/\/Date\(([0-9]*)\)\//, '$1');
            var date = new Date();
            date.setTime(time);
            return date.toDateString();
            } else return "-";
        }

</script>
</asp:Content>

