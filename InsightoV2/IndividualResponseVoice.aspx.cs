﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using Insighto.Business.Charts;
using System.Data;
using System.Data.SqlClient;

public partial class IndividualResponseVoice : SurveyPageBase
{
    int SurveyID = 0;
    DataSet ds_responsedetails = new DataSet();
    int i, ds_count;
    DataSet ds_excludedrespondents = new DataSet();
    DataSet ds_includedrespondents = new DataSet();
    SurveyCoreVoice surresponse = new SurveyCoreVoice();
    SurveyCoreVoice obj1 = new SurveyCoreVoice();
    Hashtable ht = new Hashtable();

    public string Key
    {
        get
        {
            return HttpUtility.UrlEncode(QueryStringHelper.GetString("Key"));
        }
    }
    public SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = false;




        i = Convert.ToInt32(Session["ds_resp_row_id"]);
        lbl_message.Visible = false;
        if (!IsPostBack)
        {

            Session["resp_count"] = 0;
            Session["ds_resp_row_id"] = 0;

            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
                {
                    SurveyID = Convert.ToInt32(ht[Constants.SURVEYID]);
                    SelectedSurvey = surresponse.GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                }
            }
            else if (SurveyBasicInfoView != null)
            {
                SurveyID = SurveyBasicInfoView.SURVEY_ID;
                SelectedSurvey = surresponse.GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
            }


            ds_responsedetails = obj1.GetIndiviudalRespIds(SurveyBasicInfoView.SURVEY_ID, false, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
            if (ds_responsedetails != null && ds_responsedetails.Tables.Count > 0)
            {
                if (ds_responsedetails.Tables[0].Rows.Count == 0)
                {
                    ASPxPanel3.Visible = false;
                    lbl_message.Text = "No responses to display.";
                    lbl_message.Visible = true;
                    lbn_ExcludeResponse.Visible = false;
                }
                else
                {
                    Individual_Res();
                    panelfunction();
                    ASPxPanel3.Visible = true;
                }
            }
            CheckUserFeatureLinks();



            SqlConnection con = new SqlConnection();

            con = strconnectionstring();
            SqlCommand scom = new SqlCommand("sp_GetTeleSurvey", con);
            scom.CommandType = CommandType.StoredProcedure;

            scom.Parameters.Add(new SqlParameter("@SurveyID", SqlDbType.Int)).Value = SurveyID;

            SqlDataAdapter sda = new SqlDataAdapter(scom);
            DataSet dstelesurvey = new DataSet();
            sda.Fill(dstelesurvey);
            con.Close();

            if (dstelesurvey.Tables[0].Rows.Count == 0)
            {
                Responseview.Visible = true;
            }
            else
            {
                Responseview.Visible = false;
            }

        }
    }

    #region "Method : SetFeatures"
    /// <summary>
    /// This method returns bool value, if the feature flag is one.
    /// </summary>
    /// <param name="strFeature"></param>
    /// <returns></returns>

    public int SetFeatures(string strFeature)
    {
        int iVal = 0;
        var session = ServiceFactory.GetService<SessionStateService>();
        var userService = ServiceFactory.GetService<UsersService>();
        var sessdet = session.GetLoginUserDetailsSession();
        if (sessdet != null)
        {

            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));

            if (sessdet.UserId > 0 && listFeatures != null)
            {
                foreach (var listFeat in listFeatures)
                {

                    if (listFeat.Feature.Contains(strFeature))
                    {
                        iVal = (int)listFeat.Value;
                    }
                }
            }
        }

        else
        {
            Response.Redirect(PathHelper.GetUserLoginPageURL());
        }
        return iVal;
    }

    #endregion

    public void CheckUserFeatureLinks()
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (userDetails != null && userDetails.UserId > 0)
        {

            string Navurlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag);
            if (SetFeatures("CUSTOMCHART") == 0)
            {
                CustomReports.HRef = "#";
                CustomReports.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
            else
            {
                string strCustomTab = EncryptHelper.EncryptQuerystring("CustomizeReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
                CustomReports.HRef = strCustomTab;
            }
            if (SetFeatures("CROSS_TAB") == 0)
            {
                CrossTabRpts.HRef = "#";
                CrossTabRpts.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
            else
            {
                string strCrossTab = EncryptHelper.EncryptQuerystring("CrossTabReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
                CrossTabRpts.HRef = strCrossTab;
            }
            if (SetFeatures("RAWDATA_EXPORT") == 0)
            {
                rawdatavalid.Attributes.Add("Onclick", "javascript: OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                rawdatapartial.Attributes.Add("Onclick", "javascript: OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
            }
            if (SetFeatures("EXCLUDE_BLANKRESPONSES") == 0)
            {
                chk_blankresponse.ClientSideEvents.CheckedChanged = "function(s, e) { OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');e.processOnServer = false;  }";
            }
            if (SetFeatures("TRACKEMAIL_ID") == 0)
            {
                chk_respondentemail.ClientSideEvents.CheckedChanged = "function(s, e) { OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');e.processOnServer = false;  }";
            }
            if (SetFeatures("PARTIALS") == 0)
            {
                chk_partialresponses.ClientSideEvents.CheckedChanged = "function(s, e) { OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');e.processOnServer = false;  }";
            }
        }
    }

    public void SetSubNaviLinks()
    {
        string strCrossTab = EncryptHelper.EncryptQuerystring("CrossTabReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
        string strCustomTab = EncryptHelper.EncryptQuerystring("CustomizeReports.aspx", Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID.ToString() + "&surveyFlag=" + SurveyFlag);
        CrossTabRpts.HRef = strCrossTab;
        CustomReports.HRef = strCustomTab;
    }

    void Individual_Res()
    {
        SurveyCoreVoice cor = new SurveyCoreVoice();
        if (Request.QueryString["key"] != null)
        {
            Hashtable ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 && ht.Contains(Constants.SURVEYID))
            {
                SurveyID = Convert.ToInt32(ht[Constants.SURVEYID]);
                SelectedSurvey = surresponse.GetSurveyWithResponsesCount(SurveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                ds_responsedetails = obj1.GetIndiviudalRespIds(SurveyID, false, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
            }
        }

        filteroptionsfunction();
        if (ds_responsedetails != null && ds_responsedetails.Tables.Count > 0 && ds_responsedetails.Tables[0].Rows.Count > 0)
        {
            ds_count = ds_responsedetails.Tables[0].Rows.Count;
            ASPxPanel3.Visible = true;
            lbn_ExcludeResponse.Visible = true;
        }
        else
        {
            lbl_message.Text = "No responses to display.";
            lbl_message.Visible = true;
            ASPxPanel3.Visible = false;
            lbn_ExcludeResponse.Visible = false;
        }
        Session["resp_count"] = ds_count;
        int j = 0;
        if (Session["ds_resp_row_id"] != null)
        {
            j = Convert.ToInt32(Session["ds_resp_row_id"]);
            if (j == ds_count || j > ds_count)
            {
                j = 0;
                Session["ds_resp_row_id"] = 0;

            }
        }
        for (i = j; i < ds_count; i++)
        {
            //Survey sur = SelectedSurvey;
            if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
            {
                SelectedSurvey.surveyQues.Sort();
            }
            SurveyResponse surRes = null;

            if (SelectedSurvey.SURVEY_ID > 0 && ds_responsedetails != null && ds_responsedetails.Tables.Count > 0)
            {
                if (ds_responsedetails.Tables[0].Rows[i].ItemArray[0] != null)
                {
                    surRes = cor.GetSurveyResponses(SelectedSurvey.SURVEY_ID, Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i].ItemArray[0].ToString()));
                    respondent_idvalue.Value = ds_responsedetails.Tables[0].Rows[i].ItemArray[0].ToString();

                    if ((ds_responsedetails.Tables[0].Rows[i]["STATUS"] != DBNull.Value) && (ds_responsedetails.Tables[0].Rows[i]["DELETED"] != DBNull.Value) && (ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"] != DBNull.Value))
                    {
                        if (Convert.ToString(ds_responsedetails.Tables[0].Rows[i]["STATUS"]) == "Complete" && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["DELETED"]) == 0 && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"]) == 0)
                        {
                            if (SetFeatures("RESTEXCL_RESPTOCOMPLETES") == 0)
                            {
                                lbn_ExcludeResponse.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                            }
                            lbn_ExcludeResponse.Text = "Exclude this response";

                        }
                        else if ((ds_responsedetails.Tables[0].Rows[i]["STATUS"] != null) && (ds_responsedetails.Tables[0].Rows[i]["DELETED"] != null) && (ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"] != null) && Convert.ToString(ds_responsedetails.Tables[0].Rows[i]["STATUS"]) == "Partial" && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["DELETED"]) == 0 && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"]) == 0)
                        {
                            if (SetFeatures("RESTEXCL_RESPPARTTOCOMPLETES") == 0)
                            {
                                lbn_ExcludeResponse.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                            }
                            lbn_ExcludeResponse.Text = "Take this response to complete";

                        }
                        else if ((ds_responsedetails.Tables[0].Rows[i]["STATUS"] != null) && (ds_responsedetails.Tables[0].Rows[i]["DELETED"] != null) && (ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"] != null) && Convert.ToString(ds_responsedetails.Tables[0].Rows[i]["STATUS"]) == "Complete" && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["DELETED"]) == 1 && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"]) == 0)
                        {
                            if (SetFeatures("RESTEXCL_RESPTOCOMPLETES") == 0)
                            {
                                lbn_ExcludeResponse.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                            }
                            lbn_ExcludeResponse.Text = "Restore this response to complete";

                        }
                        else if ((ds_responsedetails.Tables[0].Rows[i]["STATUS"] != null) && (ds_responsedetails.Tables[0].Rows[i]["DELETED"] != null) && (ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"] != null) && Convert.ToString(ds_responsedetails.Tables[0].Rows[i]["STATUS"]) == "Complete" && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["DELETED"]) == 0 && Convert.ToInt32(ds_responsedetails.Tables[0].Rows[i]["FLAG_COMPLETESTATUS"]) == 1)
                        {
                            if (SetFeatures("RESTEXCL_RESPPARTTOCOMPLETES") == 0)
                            {
                                lbn_ExcludeResponse.Attributes.Add("onclick", "javascript:OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');return false;");
                            }
                            lbn_ExcludeResponse.Text = "Restore this response as partial";

                        }
                    }

                }
            }
            if (SelectedSurvey.surveyQues != null)
            {
                List<SurveyQuestion> QuesList = SelectedSurvey.surveyQues;
                int c = 0;
                int qid = 0;
                int flag = 1;
                int[] ans_id = new int[ds_count];
                int aid;
                int ques_count = 0;
                if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
                {
                    SelectedSurvey.surveyQues.Sort();
                    foreach (SurveyQuestion ques in QuesList)
                    {

                        ques_count = ques_count + 1;

                    }
                }
                if (surRes != null && surRes.ResponseQuestion.Count > 0)
                {
                    foreach (ResponseQuestion resp in surRes.ResponseQuestion)
                    {
                        c = c + 1;

                    }
                }
                string[] ques_array = new string[ques_count];
                int[] quesid_array = new int[ques_count];
                string[] ans_array = new string[ques_count];
                c = 0;

                if (surRes != null && surRes.ResponseQuestion.Count > 0)
                {
                    foreach (ResponseQuestion resp in surRes.ResponseQuestion)
                    {

                        if (resp.QUESTION_ID == qid)
                        {
                            flag = 0;

                        }
                        else
                        {
                            flag = 1;
                            qid = resp.QUESTION_ID;
                            c = c + 1;

                        }
                        aid = resp.ANSWER_ID;
                        if (QuesList != null)
                        {
                            foreach (SurveyQuestion ques in QuesList)
                            {
                                if (ques.QUESTION_ID == Convert.ToInt32(qid))
                                {

                                    if (flag == 1)
                                    {

                                        ques_array[c - 1] = System.Text.RegularExpressions.Regex.Replace(ques.QUESTION_LABEL, "<span[^>]*>", "");
                                        //ques_array[c - 1] = ques.QUESTION_LABEL;
                                        quesid_array[c - 1] = ques.QUESTION_ID;

                                    }


                                    if (aid != 0)
                                    {
                                        List<SurveyAnswers> anslist = ques.surveyAnswers;
                                        foreach (SurveyAnswers ans in anslist)
                                        {
                                            if (aid == Convert.ToInt32(ans.ANSWER_ID))
                                            {
                                                if (flag == 1)
                                                {
                                                    if (ques.QUESTION_TYPE != 20)
                                                    {
                                                        if (ques.QUESTION_TYPE != 4)

                                                            ans_array[c - 1] = ans.ANSWER_OPTIONS.Replace("$--$", " ") + "<p>" + resp.ANSWER_TEXT + "</p>";
                                                        //ans_array[c - 1] = ans.ANSWER_OPTIONS.Replace("$--$", " ");
                                                        else
                                                            ans_array[c - 1] = " " + ans.ANSWER_OPTIONS + " " + "<p>" + resp.ANSWER_TEXT + "</p>";
                                                        //ans_array[c - 1] = " " + ans.ANSWER_OPTIONS;


                                                    }
                                                    else
                                                        ans_array[c - 1] = ans.ANSWER_OPTIONS + "  :  " + resp.ANSWER_TEXT;
                                                    // ans_array[c - 1] = ans.ANSWER_OPTIONS;
                                                }
                                                else
                                                {
                                                    if (ques.QUESTION_TYPE == 20)
                                                        ans_array[c - 1] = ans_array[c - 1] + "<p>" + ans.ANSWER_OPTIONS + "  :  " + resp.ANSWER_TEXT + "</p>";
                                                    // ans_array[c - 1] = ans_array[c - 1] + "<p>" + ans.ANSWER_OPTIONS;
                                                    else
                                                    {
                                                        ans_array[c - 1] = ans_array[c - 1] + ans.ANSWER_OPTIONS.Replace("$--$", " ") + "<p>" + resp.ANSWER_TEXT + "</p>";
                                                        //ans_array[c - 1] = ans_array[c - 1] + ans.ANSWER_OPTIONS.Replace("$--$", " ");
                                                    }
                                                }

                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (flag == 1)
                                            ans_array[c - 1] = "<p>" + resp.ANSWER_TEXT + "</p>";
                                        else
                                            ans_array[c - 1] = ans_array[c - 1] + "<p>" + resp.ANSWER_TEXT + "</p>";
                                    }
                                    break;
                                }
                            }
                        }

                    }

                }
                int ques_resp_check_flag = 0;
                int quesNo = 1;
                if (QuesList != null && QuesList.Count > 0)
                {
                    rndpanel_surveyresponses.Controls.Clear();
                    foreach (SurveyQuestion ques in QuesList)
                    {

                        if (ques.QUESTION_TYPE != 16 && ques.QUESTION_TYPE != 17 && ques.QUESTION_TYPE != 18 && ques.QUESTION_TYPE != 19 && ques.QUESTION_TYPE != 21)
                        {
                            ques_resp_check_flag = 0;
                            for (int q = 0; q < ques_array.Length; q++)
                            {
                                if (ques.QUESTION_ID == quesid_array[q])
                                {
                                    HtmlTable ques_resp = new HtmlTable();
                                    ques_resp.Width = "100%";
                                    ques_resp.CellPadding = 1;
                                    ques_resp.CellSpacing = 0;
                                    //ques_resp.Border = 0;
                                    ques_resp.Border = 1;
                                    ques_resp.BorderColor = "Purple";
                                    ques_resp.Align = "Center";

                                    HtmlTableRow quesrow = new HtmlTableRow();
                                    HtmlTableCell quescell = new HtmlTableCell();
                                    //quescell.BgColor = "#d6e7ff";
                                    quescell.Attributes.Add("style", "background:#F4F4F4; padding:10px 8px 10px 8px; font-size:14px;");

                                    //quescell.Height = "24px";
                                    System.Web.UI.WebControls.Label lbl_ques = new System.Web.UI.WebControls.Label();
                                    ques_array[q] = ques_array[q].Replace("<p>", "");
                                    ques_array[q] = ques_array[q].Replace("</p>", "");
                                    lbl_ques.Text = "Q" + quesNo + ". " + Convert.ToString(ques_array[q]);
                                    lbl_ques.CssClass = "indiquestxt";
                                    lbl_ques.EnableTheming = false;
                                    lbl_ques.BorderStyle = BorderStyle.None;
                                    quescell.Controls.Add(lbl_ques);
                                    quesrow.Cells.Add(quescell);
                                    ques_resp.Rows.Add(quesrow);
                                    //  HtmlTableRow ansrow = new HtmlTableRow();
                                    // ansrow.Height = "40px";

                                    HtmlTableCell anscell = new HtmlTableCell();
                                    anscell.Width = "50%";
                                    anscell.Attributes.Add("style", "background:#F4F4F4; padding:10px 8px 10px 8px; font-size:14px;");

                                    //  System.Web.UI.WebControls.Label lbl_answer = new System.Web.UI.WebControls.Label();

                                    System.Web.UI.WebControls.Label lbl_answer = new System.Web.UI.WebControls.Label();
                                    System.Web.UI.WebControls.HyperLink hl_answer = new System.Web.UI.WebControls.HyperLink();
                                    if (ans_array.Length > 0)
                                    {
                                        if (ans_array[q] != null)
                                        {
                                            ans_array[q] = ans_array[q].Replace("&nbsp;", "");
                                        }
                                        //    lbl_answer.Text = Convert.ToString(ans_array[q]);
                                        //}
                                        //anscell.Controls.Add(lbl_answer);
                                        //lbl_answer.CssClass = "indianswtxt";
                                        //ansrow.Cells.Add(anscell);
                                        //ques_resp.Rows.Add(ansrow);
                                        //rndpanel_surveyresponses.Controls.Add(ques_resp);
                                        //ques_resp_check_flag = 1;
                                        //quesNo += 1;
                                        if (Convert.ToString(ans_array[q]).Contains("http"))
                                        {


                                            string[] splitstring = Convert.ToString(ans_array[q]).Split(',', ' ', '<', '>');

                                            foreach (string itemurl in splitstring)
                                            {

                                                if ((itemurl != "") && (itemurl != "p") && (itemurl != "/p"))
                                                {
                                                    hl_answer.CssClass = "indianswtxt";
                                                    hl_answer.Visible = true;
                                                    //hl_answer.Text = Convert.ToString(ans_array[q]);
                                                    //hl_answer.ResolveUrl(Convert.ToString(ans_array[q]));
                                                    //hl_answer.NavigateUrl = Convert.ToString(ans_array[q]);
                                                    //if (itemurl != "<" || itemurl != "p" || itemurl != ">" || itemurl != "" || itemurl != "/p")
                                                    //{
                                                    hl_answer.Text = "AudioResponse";
                                                    hl_answer.ResolveUrl(itemurl);
                                                    hl_answer.NavigateUrl = itemurl;
                                                    //}
                                                }
                                            }

                                        }
                                        else
                                        {
                                            lbl_answer.Text = Convert.ToString(ans_array[q]);
                                        }



                                    }
                                    anscell.Controls.Add(hl_answer);

                                    anscell.Controls.Add(lbl_answer);




                                    lbl_answer.CssClass = "indianswtxt";
                                    hl_answer.CssClass = "indianswtxt";
                                    //  ansrow.Cells.Add(anscell);
                                    quesrow.Cells.Add(anscell);

                                    //   ques_resp.Rows.Add(ansrow);
                                    ques_resp.Rows.Add(quesrow);
                                    rndpanel_surveyresponses.Controls.Add(ques_resp);
                                    ques_resp_check_flag = 1;
                                    quesNo += 1;
                                    break;
                                }
                            }
                            if (ques_resp_check_flag == 0)
                            {
                                HtmlTable ques_resp = new HtmlTable();
                                ques_resp.Width = "100%";
                                ques_resp.CellPadding = 1;
                                ques_resp.CellSpacing = 0;
                                // ques_resp.Border = 0;
                                ques_resp.Border = 1;
                                ques_resp.BorderColor = "Purple";
                                ques_resp.Align = "Center";
                                HtmlTableRow quesrow = new HtmlTableRow();
                                HtmlTableCell quescell = new HtmlTableCell();
                                quescell.Width = "50%";
                                quescell.Attributes.Add("style", "background:#F4F4F4; padding:10px 8px 10px 8px; font-size:14px;");

                                System.Web.UI.WebControls.Label lbl_ques = new System.Web.UI.WebControls.Label();
                                string temp_ques_str = "";
                                temp_ques_str = ques.QUESTION_LABEL.Replace("<p>", "");
                                temp_ques_str = temp_ques_str.Replace("</p>", "");
                                lbl_ques.CssClass = "indiquestxt";
                                lbl_ques.Text = "Q" + quesNo + "." + temp_ques_str;
                                lbl_ques.EnableTheming = false;
                                lbl_ques.BorderStyle = BorderStyle.None;
                                quescell.Controls.Add(lbl_ques);
                                quesrow.Cells.Add(quescell);
                                ques_resp.Rows.Add(quesrow);
                                // HtmlTableRow ansrow = new HtmlTableRow();
                                HtmlTableCell anscell = new HtmlTableCell();
                                anscell.Width = "50%";
                                System.Web.UI.WebControls.Label lbl_answer = new System.Web.UI.WebControls.Label();
                                lbl_answer.CssClass = "indianswtxt";
                                lbl_answer.Text = "&nbsp;";

                                anscell.Controls.Add(lbl_answer);
                                // ansrow.Cells.Add(anscell);
                                quesrow.Cells.Add(anscell);
                                // ques_resp.Rows.Add(ansrow);
                                ques_resp.Rows.Add(quesrow);
                                rndpanel_surveyresponses.Controls.Add(ques_resp);
                                quesNo += 1;
                            }

                        }
                    }
                }
            }

            Session["ds_resp_row_id"] = i;
            EmailIDsHeaderFunction();
            break;
        }
    }
    protected void btn_first_Click(object sender, EventArgs e)
    {
        Session["ds_resp_row_id"] = 0;
        Individual_Res();
        panelfunction();

    }
    protected void btn_prev_Click(object sender, EventArgs e)
    {
        if (Session["ds_resp_row_id"] != null)
            Session["ds_resp_row_id"] = Convert.ToInt32(Session["ds_resp_row_id"]) - 1;
        Individual_Res();
        panelfunction();
    }
    protected void btn_nxt_Click(object sender, EventArgs e)
    {
        if (Session["ds_resp_row_id"] != null)
            Session["ds_resp_row_id"] = Convert.ToInt32(Session["ds_resp_row_id"]) + 1;
        Individual_Res();
        panelfunction();

    }
    protected void btn_last_Click(object sender, EventArgs e)
    {
        if (Session["resp_count"] != null)
            ds_count = Convert.ToInt32(Session["resp_count"]);
        Session["ds_resp_row_id"] = ds_count - 1;
        Individual_Res();
        panelfunction();
    }
    protected void btn_go_Click(object sender, EventArgs e)
    {

        ds_count = Convert.ToInt32(Session["resp_count"]);
        if (txt_req.Text.Length == 0 || txt_req.Text == null || txt_req.Text == "" || txt_req.Text.Trim().Length == 0)
        {
            Response.Write("<script language='javascript'>alert('Please select page number ');</script>");
            Session["ds_resp_row_id"] = i;

        }
        else
        {

            if (txt_req.Text != null)
            {
                if (Convert.ToInt32(txt_req.Text) > ds_count || Convert.ToInt32(txt_req.Text) == 0)
                {
                    ClientScript.RegisterStartupScript(typeof(System.Web.UI.Page), "Individual Response", "<script>alert('Please select valid page number');</script>");
                    Session["ds_resp_row_id"] = i;
                }
                else
                {
                    if (txt_req.Text != null && txt_req.Text.Trim().Length > 0)
                        Session["ds_resp_row_id"] = Convert.ToInt32(txt_req.Text) - 1;
                }
            }

        }
        Individual_Res();
        panelfunction();
    }

    void panelfunction()
    {
        if (Session["ds_resp_row_id"] != null)
        {
            int l_value = 0;
            int value_ds_count = ds_count;
            if (Convert.ToInt32(Session["ds_resp_row_id"]) == 0)
                l_value = Convert.ToInt32(Session["ds_resp_row_id"]) + 1;
            else
                l_value = Convert.ToInt32(Session["ds_resp_row_id"]) + 1;
            lbl_currentpage.Text = Convert.ToString(l_value + "/" + ds_count);
            if (Convert.ToInt32(Session["ds_resp_row_id"]) == 0)
            {
                btn_first.Enabled = false;
                btn_prev.Enabled = false;
                if (value_ds_count == 1)
                {
                    btn_nxt.Enabled = false;
                    btn_last.Enabled = false;
                }
                else
                {
                    btn_nxt.Enabled = true;
                    btn_last.Enabled = true;
                }
            }


            if (Convert.ToInt32(Session["ds_resp_row_id"]) == (ds_count - 1))
            {
                btn_last.Enabled = false;
                btn_nxt.Enabled = false;

                if (value_ds_count == 1)
                {
                    btn_first.Enabled = false;
                    btn_prev.Enabled = false;
                }
                else
                {
                    btn_first.Enabled = true;
                    btn_prev.Enabled = true;
                }
            }
            int temp_value = Convert.ToInt32(Session["ds_resp_row_id"]);
            if (temp_value != ds_count - 1 && temp_value != 0)
            {
                btn_first.Enabled = true;
                btn_last.Enabled = true;
                btn_prev.Enabled = true;
                btn_nxt.Enabled = true;
            }
        }
    }
    protected void btn_first_down_Click(object sender, EventArgs e)
    {
        Session["ds_resp_row_id"] = 0;
        Individual_Res();
        panelfunction();
    }
    protected void btn_prev_down_Click(object sender, EventArgs e)
    {
        if (Session["ds_resp_row_id"] != null)
        {
            Session["ds_resp_row_id"] = Convert.ToInt32(Session["ds_resp_row_id"]) - 1;
        }
        Individual_Res();
        panelfunction();

    }
    protected void btn_next_down_Click(object sender, EventArgs e)
    {
        if (Session["ds_resp_row_id"] != null)
        {
            Session["ds_resp_row_id"] = Convert.ToInt32(Session["ds_resp_row_id"]) + 1;
        }
        Individual_Res();
        panelfunction();
    }
    protected void btn_last_down_Click(object sender, EventArgs e)
    {
        if (Session["resp_count"] != null)
        {
            ds_count = Convert.ToInt32(Session["resp_count"]);
        }
        Session["ds_resp_row_id"] = ds_count - 1;
        Individual_Res();
        panelfunction();
    }


    protected void ASPxButton5_Click(object sender, EventArgs e)
    {
    }
    protected void cbo_responses_SelectedIndexChanged(object sender, EventArgs e)
    {
        filteroptionsfunction();
        Session["ds_resp_row_id"] = 0;
        txt_req.Text = "";
        Individual_Res();
        panelfunction();
    }
    protected void chk_respondentemail_CheckedChanged(object sender, EventArgs e)
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (userDetails != null && userDetails.UserId > 0)
        {
            if (SetFeatures("TRACKEMAIL_ID") == 0)
            {
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Upgrade", "<script>OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');</script>", true);
            }
            else
            {
                txt_req.Text = "";
                Individual_Res();
                panelfunction();
            }
        }
    }
    public void EmailIDsHeaderFunction()
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        string str_mailid = "";
        string telephonenumbertxt = "";
        //if (ds_responsedetails.Tables[0].Rows[i].ItemArray[4] != DBNull.Value)
        //{
        //    str_mailid = ds_responsedetails.Tables[0].Rows[i].ItemArray[4].ToString();
        //}
        if (ds_responsedetails.Tables[0].Rows[i].ItemArray[4] != DBNull.Value)
        {
            str_mailid = ds_responsedetails.Tables[0].Rows[i].ItemArray[4].ToString();
            telephonenumbertxt = "Email Address :";

        }
        else
        {
            str_mailid = ds_responsedetails.Tables[0].Rows[i].ItemArray[7].ToString();
            telephonenumbertxt = "Caller Id :";
        }
        string heading = "";
        if (str_mailid != null)
        {
            if (str_mailid == "")
            {
                if (chk_respondentemail.Checked == true)
                    // heading += "Email Address :" + " " + "Unknown/WebDeployment || ";
                    heading += telephonenumbertxt + " " + "Unknown/WebDeployment || ";

                heading += "     Completion Time :  ";
                if (ds_responsedetails.Tables[0].Rows[i]["CREATED_ON"] != DBNull.Value)
                {
                    DateTime dt = Convert.ToDateTime(ds_responsedetails.Tables[0].Rows[i]["CREATED_ON"]);
                    dt = BuildQuetions.GetConvertedDatetime(dt, userDetails.StandardBIAS, SurveyBasicInfoView.TIME_ZONE, true);
                    heading += Convert.ToString(dt);
                }
            }
            else
            {
                if (chk_respondentemail.Checked == true)
                    //heading += "Email Address :" + " " + str_mailid + "|| ";
                    heading += telephonenumbertxt + " " + str_mailid + "|| ";

                heading += "Completion Time :  ";
                if (ds_responsedetails.Tables[0].Rows[i]["CREATED_ON"] != DBNull.Value)
                {
                    DateTime dt1 = Convert.ToDateTime(ds_responsedetails.Tables[0].Rows[i]["CREATED_ON"]);
                    dt1 = BuildQuetions.GetConvertedDatetime(dt1, userDetails.StandardBIAS, SurveyBasicInfoView.TIME_ZONE, true);
                    heading += Convert.ToString(dt1);
                }

            }
        }

        rndpanel_surveyresponses.HeaderText = heading.ToString();

    }
    protected void chk_blankresponse_CheckedChanged(object sender, EventArgs e)
    {
        //if (SelectedSurvey != null && SelectedSurvey.SURVEY_ID > 0)
        //{
        Session["ds_resp_row_id"] = 0;
        Individual_Res();
        panelfunction();
        //}
    }

    public void filteroptionsfunction()
    {
        if (SelectedSurvey != null && SelectedSurvey.SURVEY_ID > 0)
        {
            if (chk_blankresponse.Checked == true)
            {
                if (chk_validresponses.Checked || chk_excludedresponses.Checked || chk_partialresponses.Checked || chk_partialtocompletes.Checked)

                    ds_responsedetails = obj1.GetIndiviudalRespIds(SelectedSurvey.SURVEY_ID, true, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                else
                {
                    ds_responsedetails = obj1.GetIndiviudalRespIds(SelectedSurvey.SURVEY_ID, true, true, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                }

            }

            else
            {
                if (chk_validresponses.Checked || chk_excludedresponses.Checked || chk_partialresponses.Checked || chk_partialtocompletes.Checked)
                    ds_responsedetails = obj1.GetIndiviudalRespIds(SelectedSurvey.SURVEY_ID, false, chk_validresponses.Checked, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                else
                {
                    ds_responsedetails = obj1.GetIndiviudalRespIds(SelectedSurvey.SURVEY_ID, false, true, chk_excludedresponses.Checked, chk_partialresponses.Checked, chk_partialtocompletes.Checked);
                }

            }

        }

    }

    protected void lbn_ExcludeResponse_Click(object sender, EventArgs e)
    {
        string strRootUrl = ConfigurationManager.AppSettings["RootURL"].ToString();
        int delete_rowid = 0;
        int restoretype = 0;
        if (Session["ds_resp_row_id"] != null)
            delete_rowid = Convert.ToInt32(Session["ds_resp_row_id"]);

        if (lbn_ExcludeResponse.Text == "Exclude this response")
        {
            surresponse.ExcludeIndividualResp(Convert.ToInt32(respondent_idvalue.Value), base.SurveyBasicInfoView.SURVEY_ID, 0);
        }
        else if (lbn_ExcludeResponse.Text == "Take this response to complete")
        {
            bool resstat = surresponse.IncludeResponsetoComplete(Convert.ToInt32(respondent_idvalue.Value), base.SurveyBasicInfoView.SURVEY_ID, 0);
            if (!resstat)
            {

                lbl_message.Text = "You have exceeded the response limit set for your account. <br/>To include this and more responses, upgrade your account now ";
                lbl_message.Text += "<a href='" + strRootUrl + "Pricing.aspx' target='_blank' style='color:#49649A;'>Upgrade Now!</a>";
                lbl_message.Visible = true;
            }
        }
        else if (lbn_ExcludeResponse.Text == "Restore this response to complete")
        {
            bool resstat = surresponse.IncludeResponsetoComplete(Convert.ToInt32(respondent_idvalue.Value), base.SurveyBasicInfoView.SURVEY_ID, 1);
            if (!resstat)
            {
                lbl_message.Text = "You have exceeded the response limit set for your account. <br/>To include this and more responses, upgrade your account now ";
                lbl_message.Text += "<a href='" + strRootUrl + "Pricing.aspx' target='_blank' style='color:#49649A;'>Upgrade Now!</a>";
                lbl_message.Visible = true;
            }
        }
        else if (lbn_ExcludeResponse.Text == "Restore this response as partial")
        {
            surresponse.ExcludeIndividualResp(Convert.ToInt32(respondent_idvalue.Value), base.SurveyBasicInfoView.SURVEY_ID, 1);
        }

        txt_req.Text = "";
        //txt_req_down.Text = "";
        Individual_Res();
        panelfunction();
    }
    protected void rbncomplete_CheckedChanged(object sender, EventArgs e)
    {


    }
    protected void rbnpartialResponses_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void rawdatapartial_Click(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            bool flag = false;
            flag = surresponse.GetRawDataExportResponses(base.SurveyBasicInfoView.SURVEY_ID, "Partial");
            ExportResponsestoExcel(flag, "Partial");
            Individual_Res();
        }

    }


    public void SaveDialogBox(string sourcePath, string SurveyName, string fileExtension)
    {
        if (File.Exists(sourcePath))
        {
            // divSendReport.Visible = true;
            //ErrorSuccessNotifier.AddSuccessMessage(Resources.CommonMessages.SendReportMessage); 
            string name_survey = SurveyName.Replace(" ", "");
            name_survey = Regex.Replace(name_survey, "[^\\w\\.@-]", "");
            name_survey = name_survey.Replace("/", "");
            Response.ContentType = "application/ms-excel";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + name_survey + fileExtension);
            Response.TransmitFile(sourcePath);
            Response.End();
        }
        else
        {
            lblErrMsg.Text = "File not created by service.";
            dvErrMsg.Visible = true;
        }
    }
    protected void cmdviewResponses_Click(object sender, EventArgs e)
    {

        Session["ds_resp_row_id"] = 0;
        Individual_Res();
        panelfunction();

    }
    protected void rawdatavalid_Click(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            bool flag = false;
            flag = surresponse.GetRawDataExportResponses(base.SurveyBasicInfoView.SURVEY_ID, "Complete");
            ExportResponsestoExcel(flag, "Complete");
            Individual_Res();
        }
    }

    private void CreateExportFile(int rep_type, string exp_type)
    {
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
        if (exp_type != "" && SurveyBasicInfoView != null && SurveyBasicInfoView.SURVEY_ID > 0 && userDetails != null && userDetails.UserId > 0)
        {
            int exp_id = surresponse.InsertExportInfo(SurveyBasicInfoView.SURVEY_ID, rep_type, exp_type, userDetails.UserId);
            if (exp_id > 0)
            {
                //System.Threading.Thread.Sleep(5000 * 10);
                DataSet ds = new DataSet();
                int flag = 0;
                for (int i = 0; i < 4; i++)
                {
                    System.Threading.Thread.Sleep(5000 * 10);
                    ds = surresponse.GetExportInfo(exp_id);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[0]["REPORT_STATUS"]) == 2)
                        {
                            string strDirector = ConfigurationManager.AppSettings["RootPath"].ToString();
                            if (!Directory.Exists(strDirector + base.SurveyBasicInfoView.USERID + @"\" + base.SurveyBasicInfoView.SURVEY_ID))
                            {
                                Directory.CreateDirectory(strDirector + base.SurveyBasicInfoView.USERID + @"\" + base.SurveyBasicInfoView.SURVEY_ID);
                            }
                            string strXLFileName = strDirector + base.SurveyBasicInfoView.USERID + "\\" + base.SurveyBasicInfoView.SURVEY_ID + "\\" + Convert.ToString(ds.Tables[0].Rows[0]["REPORT_FILENAME"]);

                            SaveDialogBox(strXLFileName, base.SurveyBasicInfoView.SURVEY_NAME, ".xls");
                            flag = 1;
                            break;

                        }
                        else if (Convert.ToInt32(ds.Tables[0].Rows[0]["REPORT_STATUS"]) == 4)
                        {
                            flag = 2;
                            break;
                        }
                    }


                }
                if (flag == 2)
                {
                    string Naurlstr = EncryptHelper.EncryptQuerystring("IndividualResponse.aspx", Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + SurveyFlag);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "myscript", "alert('No Responses has been Recorded for this survey');window.location.href='" + Naurlstr + "';", true);
                }
            }
        }
    }
    private void ExportResponsestoExcel(bool flag, string Resp_type)
    {
        if (flag)
        {
            int exptype = 4;
            string rawdatatype = "RawDataComplete";
            if (Resp_type == "Partial")
            {
                exptype = 5;
                rawdatatype = "RawDataPartial";
            }
            CreateExportFile(exptype, rawdatatype);
        }
        else
        {
            string Naurlstr = EncryptHelper.EncryptQuerystring("IndividualResponse.aspx", Constants.SURVEYID + "=" + SurveyBasicInfoView.SURVEY_ID + "&Alert=" + "noerror&surveyFlag=" + SurveyFlag);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "myscript", "alert('No Responses has been Recorded for this survey');window.location.href='" + Naurlstr + "';", true);
        }

    }
    protected void chk_validresponses_CheckedChanged(object sender, EventArgs e)
    {
        Session["ds_resp_row_id"] = 0;
        Individual_Res();
        panelfunction();
    }
}