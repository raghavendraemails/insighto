﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using Insighto.Business.Services;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Data;
using App_Code;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Resources;

public partial class SignUp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        var userService = new UsersService();

        lnkWhyInsighto.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetWhyInsightoUrl(), "Page=WhyInsighto");

        string txtName = string.Empty;
        string txtEmail = string.Empty;
        string txttempwd = string.Empty;
        string txtpassword1 = string.Empty;
        string ipAddress = string.Empty;
        if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else
            ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();


        NameValueCollection pColl = Request.Params;
        for (int i = 0; i <= pColl.Count - 1; i++)
        {
            if (pColl.GetKey(i) == "txtName")
                txtName = pColl.Get(i);
            if (pColl.GetKey(i) == "txtEmail")
                txtEmail = pColl.Get(i);
            if (pColl.GetKey(i) == "txttempwd")
                txttempwd = pColl.Get(i);
            if (pColl.GetKey(i) == "txtpassword1")
                txtpassword1 = pColl.Get(i);

        }

        // Prevent page processing if called without supplying any values
        if (txtName == string.Empty && txtEmail == string.Empty && txtpassword1 == string.Empty)
            // Redirect to home page
            Response.Redirect("http://www.insighto.com");

        var user = userService.RegisterFreeUser(txtName.Trim(), string.Empty, txtEmail.Trim(), string.Empty, 0, ipAddress, txtpassword1.Trim());
        var country = Utilities.GetCountryName(ipAddress);
        ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
        ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
        country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();

        if (user == null)
        {

            lblError.Text = "You are already signed up! Please Sign-in using your email id or click browser back button to return to Sign Up form.";
            lblError.Visible = true;
        }
        else
        {
            ServiceFactory.GetService<SessionStateService>().SaveUserToSession(user);
            // Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserSignUpFreeNextURL(),"userId="+user.USERID));
            // Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "userId=" + user.USERID));
            Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.Getsocialnetwork(), "UserId=" + user.USERID));
            // Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserPricingpageintermediate(), "UserId=" + user.USERID));
        }
    }


    protected void ibtnLogin_Click(object sender, EventArgs e)
    {
        if (!IsPostBack)
            return;

        string alrt = "Please enter";
        this.lblInvaildAlert.Visible = true;
        bool flag = false;
        if (string.IsNullOrEmpty(txtEmailAdd.Text.Trim()) || txtEmailAdd.Text.ToLower() == "email address")
        {
            alrt += " email address.";
            lblInvaildAlert.Text = alrt;
        }
        else
        {
            Regex reLenient = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            bool isEmail = reLenient.IsMatch(txtEmailAdd.Text);
            if (!isEmail)
            {
                alrt += " valid email address.";
                lblInvaildAlert.Text = alrt;
                signin_menu.Style.Add("display", "block");
            }
            else if (Convert.ToString(txtPassword.Text).Length == 0)
            {
                alrt += " password.";
                lblInvaildAlert.Text = alrt;
                signin_menu.Style.Add("display", "block");
            }
            flag = true;
        }

        if (flag && Convert.ToString(txtEmailAdd.Text).Length > 0 && Convert.ToString(txtPassword.Text).Length > 0)
        {
            // To check authenticated user       
            var userService = ServiceFactory.GetService<UsersService>();
            var user = userService.getAuthenticatedUser(txtEmailAdd.Text, txtPassword.Text);

            if (user == null)
            {
                lblInvaildAlert.Text = "Invalid Email Address or Password.";
                lblInvaildAlert.Visible = true;
                signin_menu.Style.Add("display", "block");
                return;
            }
            else if (Utilities.ToEnum<SurveyStatus>(user.STATUS) == SurveyStatus.InActive || ValidationHelper.GetInteger(user.DELETED, 0) == 1)
            {
                lblInvaildAlert.Text = "Your Account is de-activated.";
                lblInvaildAlert.Visible = true;
                signin_menu.Style.Add("display", "block");
                return;
            }
            //else if (user.ACTIVATION_FLAG == 0 || user.ACTIVATION_FLAG == null)
            //{
            //    lblInvaildAlert.Text = " Your Account is not activated.";
            //    lblInvaildAlert.Visible = true;
            //    signin_menu.Style.Add("display", "block");
            //    return;

            //}
            else
            {
                userService.ProcessUserLogin(user);

                //Timezone is null for first user login. hence redirecting to welcome page
                if (user.ACTIVATION_FLAG == null || user.ACTIVATION_FLAG == 0)
                {
                    userService.ActivateUser(user.USERID);
                    // Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=" + SurveyFlag));
                }

                SqlConnection con = new SqlConnection();

                con = strconnectionstring();
                SqlCommand scom = new SqlCommand("sp_GetTeleUser", con);
                scom.CommandType = CommandType.StoredProcedure;

                scom.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = user.USERID;

                SqlDataAdapter sda = new SqlDataAdapter(scom);
                DataSet dsteleuser = new DataSet();
                sda.Fill(dsteleuser);
                con.Close();

                if (dsteleuser.Tables[0].Rows[0]["VOICEOPTION"].ToString() == "VOICE")
                {
                    Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    //  Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetReportsURL(), "SurveyId=" + base.SurveyID + "&surveyFlag=" + SurveyFlag));
                }
                else
                {
                    if (user.TIME_ZONE == null) //Timezone is null for first user login. hence redirecting to welcome page
                    {
                        //create folder with userid 
                        //CreateUserFolders(user); this function is moved to register free user .. as every user must have this.
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                        //  Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.Getsocialnetwork(), "UserId=" + user.USERID));
                    }
                    else if (user.RESET == 1) // Password reset value is 1 for first login. hence redirectiong to chanepassword page
                    {
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserChangePasswordPageURL(), "surveyFlag=" + user.SURVEY_FLAG));
                    }
                    else // on successful login user is redirected to Myaccounts page
                    {
                        Session[CommonMessages.SurveyFlag] = user != null ? user.SURVEY_FLAG : 0;
                        Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserMyAccountPageURL(), "surveyFlag=" + user.SURVEY_FLAG));


                    }
                }
            }

        }
        else
        {


        }
    }

    public SqlConnection strconnectionstring()
    {
        try
        {
            string connStr = ConfigurationManager.ConnectionStrings["InsightoConnString"].ConnectionString;
            SqlConnection strconn = new SqlConnection(connStr);

            strconn.Open();
            return strconn;


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


}