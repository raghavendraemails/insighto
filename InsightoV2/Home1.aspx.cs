﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Services;
using Insighto.Exceptions;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using Insighto.Business.Helpers;
using System.Configuration;
using System.Collections;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;

namespace Insighto.Pages
{
    public partial class Home1 : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["key"] != null)
                {
                    Hashtable ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                    if (ht != null && ht.Count > 0 && ht.Contains("UserId"))
                    {
                        int UserId = ValidationHelper.GetInteger(ht["UserId"].ToString(), 0);
                        UsersService userService = new UsersService();
                        var user = userService.ActivateUser(UserId);
                        if (user != null)
                        {
                            string fromEmail = ConfigurationManager.AppSettings["emailAcknowlegement"];
                            string subject = (String)GetGlobalResourceObject("CommonMessages", "emailSubjectAcknowledgeAccountActivation");
                            MailHelper.SendMailMessage(fromEmail, user.LOGIN_NAME, "", "", subject, subject);
                        }
                    }

                    string NavurlYear = EncryptHelper.EncryptQuerystring("In/Registration.aspx", "Pay=Year");
                    PayYear.HRef = NavurlYear;
                    string NavurlMonth = EncryptHelper.EncryptQuerystring("In/Registration.aspx", "Pay=Month");
                    PayMonth.HRef = NavurlMonth;
                    //string NavurlQuarterly = EncryptHelper.EncryptQuerystring("In/CraftRegistration.aspx", "Pay=Quarterly");
                    hlinkCraft.HRef = "#";

                    if (ht != null && ht.Count > 0 && ht.Contains("AlternateId"))
                    {
                        int Id = ValidationHelper.GetInteger(ht["AlternateId"].ToString(), 0);
                        UsersService userService = new UsersService();
                        var user = userService.ActivateAlternateUser(Id);
                        if (user != null)
                        {
                            string fromEmail = ConfigurationManager.AppSettings["emailAcknowlegement"];
                            string subject = (String)GetGlobalResourceObject("CommonMessages", "emailSubjectAcknowledgeAccountActivation");
                            MailHelper.SendMailMessage(fromEmail, ht["Email"].ToString(), "", "", subject, subject);
                        }
                    }

                }
                lblInvaildAlert.Visible = false;
            }
        }
        protected void ibtnLogin_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(txtEmailAdd.Value).Length > 0 && Convert.ToString(txtPassword.Value).Length > 0)
            {
                
                    var userService = ServiceFactory.GetService<UsersService>();

                    // To check authenticated user            
                    var user = userService.getAuthenticatedUser(txtEmailAdd.Value, txtPassword.Value);

                    if (user == null)
                    {
                        lblInvaildAlert.Text = "In valid User Name or Password";
                        lblInvaildAlert.Visible = true;
                        return;
                    }
                    userService.ProcessUserLogin(user);

                    //Timezone is null for first user login. hence redirecting to welcome page
                    if (user.TIME_ZONE == null)
                    {
                        //create folder with userid 
                        //CreateUserFolders(user); this function is moved to register free user .. as every user must have this.
                        Response.Redirect(PathHelper.GetUserWelcomePageURL());
                    }
                    // Password reset value is 0 for first login. hence redirectiong to chanepassword page
                    else if (user.RESET == 0)
                    {
                        Response.Redirect(PathHelper.GetUserChangePasswordPageURL());
                    }


                    // on successful login user is redirected to Myaccounts page
                    else
                    {
                        Response.Redirect(PathHelper.GetUserMyAccountPageURL());
                    }
                
            }
            else
            {
                string alrt = "Please Enter";
                this.lblInvaildAlert.Visible = true;
                if ((Convert.ToString(txtEmailAdd.Value).Length == 0) || (Convert.ToString(txtEmailAdd.Value) == "Email Address"))
                {
                    alrt += " Email Address ";
                }
                else
                {
                    if (Convert.ToString(txtPassword.Value).Length == 0)
                        alrt += " Password";
                }
                lblInvaildAlert.Text = alrt;

            }
                

        }
    }
}