﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Utilities;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;
using System.Configuration;
using System.Net.Mail;
using SendGrid;
using SendGrid.SmtpApi;


public partial class bitbucket_pushcommit2340902 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CaptureCommitRequestAndEmail();
    }

    public void CaptureCommitRequestAndEmail()
    {
        string rootURL = ConfigurationManager.AppSettings["Fchartpath"].ToString();
        try
        {
            Stream stream = Request.InputStream;
            StreamReader sr = new StreamReader(stream);

            using (StreamReader file = sr )
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                string displayname = "";
                string commitmessage = "";
                string commitdate = "";
                string emailcontent = "";
                string emailsubject = "";
                try 
	            {
                    JObject o2 = (JObject)JToken.ReadFrom(reader);
		
                    displayname = o2["actor"]["display_name"].ToString();
                    if (o2["push"]["changes"] != null)
                    {
                        if (o2["push"]["changes"].Count() > 0)
                        {
                            commitmessage = o2["push"]["changes"][0]["commits"][0]["message"].ToString();
                            commitdate = o2["push"]["changes"][0]["commits"][0]["date"].ToString();
                            
                            emailsubject = "Commit at " + DateTime.UtcNow.ToString() + " UTC";
                            
                            emailcontent = "<html><body>";
                            emailcontent += "<p><b>Commit By: ";
                            emailcontent += "</b>" + displayname + "<p>";
                            emailcontent += "<p><b>Commit At: ";
                            emailcontent += "</b>" + commitdate + "<p>";
                            emailcontent += "<p><b>Commit Message: ";
                            emailcontent += "</b>" + commitmessage + "<p>";
                            emailcontent += "</body></html>";
                            
                            lblheaderslist.Text = "Success";
                            try
                            {
                                SendGridMessage mailmessage = new SendGridMessage();
                                mailmessage.From = new MailAddress("satish@knowience.com", "Dev Team");
                                mailmessage.Subject = emailsubject;
                                mailmessage.Html = (emailcontent ?? "");
                                List<String> recipients = new List<String>
                                {
                                    @"Satish Kumar <satish@knowience.com>",
                                    @"Dasarath Ram <ram@knowience.com>",
                                    @"Rajesh Ch <rajesh@knowience.com>",
                                    @"Vamsi Varma <vamsi@knowience.com>"
                                };
                                mailmessage.AddTo(recipients);

                                var creds = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SendGridUID"].ToString(), ConfigurationManager.AppSettings["SendGridPWD"].ToString());
                                var transportWeb = new Web(creds);
                                transportWeb.Deliver(mailmessage);
                                 
                            }
                            catch (Exception ex)
                            {
                                lblreqbody.Text = "Error";
                            }
                        }
                    }
                }
                catch (Exception e2)
                {
                    lblheaderslist.Text = "Error";
                }
            }
        }
        catch (Exception e1)
        {
            lblreqbody.Text = "Error";
        }
    }
}