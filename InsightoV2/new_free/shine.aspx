﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="shine.aspx.cs" Inherits="new_free_shine" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Free Online Survey Software. Sign Up Now.</title>
 <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="images/insighto.ico" />
<meta name="description" content="Free Online Survey Software. Sign Up Now." />
 <meta name="keywords" content="Free Online Surveys, Online Survey Tool, Customer Feedback, Employee Feedback, Product Feedback, Website Feedback, Exit Interview, Event Feedback, Ad Effectiveness Study, Shopper  Perception, Service Evaluation, Salesperson Evaluation, Pre-event Planning, Citizen opinion " />

<link href="css/style.css" rel="stylesheet" type="text/css" />

  <!-- Google Analytics Content Experiment code -->
<script type="text/javascript">
    function utmx_section() { } function utmx() { } (function () {
        var 
k = '55965047-0', d = document, l = d.location, c = d.cookie;
        if (l.search.indexOf('utm_expid=' + k) > 0) return;
        function f(n) {
            if (c) {
                var i = c.indexOf(n + '='); if (i > -1) {
                    var j = c.
indexOf(';', i); return escape(c.substring(i + n.length + 1, j < 0 ? c.
length : j))
                }
            }
        } var x = f('__utmx'), xx = f('__utmxx'), h = l.hash; d.write(
'<sc' + 'ript src="' + 'http' + (l.protocol == 'https:' ? 's://ssl' :
'://www') + '.google-analytics.com/ga_exp.js?' + 'utmxkey=' + k +
'&utmx=' + (x ? x : '') + '&utmxx=' + (xx ? xx : '') + '&utmxtime=' + new Date().
valueOf() + (h ? '&utmxhash=' + escape(h.substr(1)) : '') +
'" type="text/javascript" charset="utf-8"><\/sc' + 'ript>')
    })();
</script>
<script type="text/javascript">
    utmx('url', 'A/B');
</script>
<!-- End of Google Analytics Content Experiment code -->

<script language="javascript" type="text/javascript">
    function showHide(EL, PM) {
        ELpntr = document.getElementById(EL);
        if (ELpntr.style.display == 'none') {
            document.getElementById(PM).innerHTML = "-";
            ELpntr.style.display = 'block';
        }
        else {
            document.getElementById(PM).innerHTML = "+";
            ELpntr.style.display = 'none';
        }
    }
    function RedirectToLoginPage() {
        window.location.href = "../Home.aspx";
        return false;
    }

    function showHideFAQPanel() {
        divContent = document.getElementById('divFaqContent');
        if (divContent.style.display == 'none') {
            // document.getElementById(PM).innerHTML = "-";
            divContent.style.display = 'block';
        }
        else {
            //document.getElementById(PM).innerHTML = "+";
            divContent.style.display = 'none';
        }
    }
        
    </script>
<script language="javascript" type="text/javascript">

    function WaterMark(objtxt, event) {
        //        alert("event="+event);
        //        alert("objid=" + objtxt.id);
        //        alert(document.getElementById("<%=lblvalidmsg.ClientID%>").innerText);
        var defaultText = "Name";
        var defaultEmailText = "Email";
        var defaultpwdText = "Password";
        var defaultrpwdText = "Re-type Password";
        // Condition to check textbox length and event type
        if (objtxt.id == "txtName" || objtxt.id == "txtEmail" || objtxt.id == "txtpassword1" || objtxt.id == "txtrepassword") {
            if (objtxt.value.length == 0 & event.type == "blur") {
                //  alert(event.type);
                //if condition true then setting text color and default text in textbox
                if (objtxt.id == "txtName") {
                    objtxt.style.color = "Gray";
                    objtxt.value = defaultText;
                    document.getElementById("<%=lblvalidmsg.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtEmail") {
                    objtxt.style.color = "Gray";
                    objtxt.value = defaultEmailText;
                    document.getElementById("<%=lblvalidmsgemail.ClientID%>").innerText = "";

                }
                else if (objtxt.id == "txtpassword1") {
                    document.getElementById("<%= txttempwd.ClientID %>").style.display = "block";
                    objtxt.style.display = "none";
                    document.getElementById("<%=lblvalidmsgpwd.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtrepassword") {
                    document.getElementById("<%= txttemrpwd.ClientID %>").style.display = "block";
                    objtxt.style.display = "none";
                    document.getElementById("<%=lblvalidmsgrepwd.ClientID%>").innerText = "";
                }
            }
            else if (objtxt.value.length != 0 & event.type == "blur") {

                // alert("length>0" + event.type);

                if (objtxt.id == "txtName") {
                    document.getElementById("<%=lblvalidmsg.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtEmail") {
                    document.getElementById("<%=lblvalidmsgemail.ClientID%>").innerText = "";

                }
                else if (objtxt.id == "txtpassword1") {
                    document.getElementById("<%=lblvalidmsgpwd.ClientID%>").innerText = "";
                }
                else if (objtxt.id == "txtrepassword") {
                    document.getElementById("<%=lblvalidmsgrepwd.ClientID%>").innerText = "";
                }
            }
        }

        // Condition to check textbox value and event type

        if ((objtxt.value == defaultText || objtxt.value == defaultEmailText || objtxt.value == defaultpwdText || objtxt.value == defaultrpwdText) & event.type == "focus") {

            //  alert(event.type);

            if (objtxt.id == "txtName") {
                objtxt.style.color = "black";
                objtxt.value = "";
            }
            if (objtxt.id == "txtEmail") {
                objtxt.style.color = "black";
                objtxt.value = "";
            }
            if (objtxt.id == "txttempwd") {
                objtxt.style.display = "none";
                document.getElementById("<%= txtpassword1.ClientID %>").style.display = "";
                document.getElementById("<%= txtpassword1.ClientID %>").focus();
            }
            if (objtxt.id == "txttemrpwd") {
                objtxt.style.display = "none";
                document.getElementById("<%= txtrepassword.ClientID %>").style.display = "";
                document.getElementById("<%= txtrepassword.ClientID %>").focus();
            }
        }
    }

</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29026379-1']);
    _gaq.push(['_setDomainName', 'insighto.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</head>

<body>
<form id="Form1" runat="server">
<div id="wrapper">
	
<table width="100%">
    <tr>
    <td>
    <div id="header_main">
    	<a href="http://insighto.com" class="header_logo"></a>
        </div>
    </td>
    <td align="center">
    
        <asp:Image Height="100px" ID="shine_logo" src="images/Shine logo-new.png" runat="server" />
   
&nbsp;</td>
    </tr>
    </table>
    <div class="clr"></div>
    
    <div id="banner_main">
    	<div class="bannerL_blk">
          	<div class="bannerL">
        		<h1>&nbsp;Easy to use employee surveys now get smarter.<br />Get 4 premium features 
                    absolutely free.</h1>
                
				<iframe width="480" height="315" src="//www.youtube.com/embed/7xHGLyowVRk?autoplay=0&loop=0&playlist=7xHGLyowVRk" frameborder="0"  allowfullscreen></iframe>
            </div>
    
    <center>
            <div class="prcingFaqHeading" id="divFaq" style="text-align:center;font-weight:bold; font-size:14px"> See what else you get in the feature rich         
            <a href="javascript:void(0)" onclick="showHideFAQPanel()"><span class="plusExpandFAQ"><u>Basic plan</u></span></a>
            </div>
         
  <div class="prcingFaqContent" style="display:none;text-align:left;" id="divFaqContent">  
   <table class="uses_list_blk1" width="85%" > 
   <tr>
   <td >
   <ul>
   <li>
   Unlimited number of surveys
   </li>
   </ul>
   </td>
    <td >
   <ul>
   <li>
   Unlimited number of questions
   </li>
   </ul>
   </td>
    <td >
   <ul>
   <li>
   100 responses per survey
   </li>
   </ul>
   </td>
    <td >
   <ul>
   <li>
  21 question types
   </li>
   </ul>
   </td>
     </tr>
     <tr>
    <td >
   <ul>
   <li>
 Survey templates
   </li>
   </ul>
   </td>
    <td >
   <ul>
   <li>
  Skip logic / branching
   </li>
   </ul>
   </td>
   <td >
   <ul>
   <li>
  Print survey
   </li>
   </ul>
   </td>
   <td >
   <ul>
   <li>
   Save survey to Word
  
   </li>
   </ul>
   </td>
  </tr>
  <tr>
  <td >
   <ul>
   <li>
   Choice of themes

   </li>
   </ul>
   </td>
    <td >
   <ul>
   <li>
    Brand logo on survey

   </li>
   </ul>
   </td>
   <td >
   <ul>
   <li>
    Customize survey end messages
 
   </li>
   </ul>
   </td>
   <td >
   <ul>
   <li>
   Launch through weblink
  
   </li>
   </ul>
   </td>
  </tr>
  <tr>
  <td >
   <ul>
   <li>
   Results display in animated graphs
   </li>
   </ul>
   </td>
   <td >
   <ul>
   <li>
  Cross-tab
   </li>
   </ul>
   </td>
  </tr>
   </table>
   
  </div> 

  </center>
         
        </div>
        <div id="bannerR">
                <div class="signup_blk">
                   <h2>
                        Get smart. <br />
                        Get a free plan that lets you do a lot more.
                    </h2>
                    <div style=" height:15px">
                    <asp:Label ID="lblErrorMsg" ForeColor="Red" CssClass="lblRequired" Font-Size="14px" runat="server" Visible="false"></asp:Label>
                    </div>
                    <div class="frm_blk">
                        <asp:TextBox ID="txtName" Text="Name" runat="server"  onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/></div>
                        <div style=" height:15px" >
                            <asp:Label ID="lblvalidmsg" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/></div>

                    <div class="frm_blk">
                        <asp:TextBox ID="txtEmail" Text="Email" runat="server" onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/></div>
                         <div style=" height:15px">
                                       <asp:Label ID="lblvalidmsgemail" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/>
</div>
                    <div class="frm_blk">
                        <asp:TextBox ID="txttempwd" Text="Password" runat="server"  onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/> 
                        <asp:TextBox ID="txtpassword1" MaxLength="16" TextMode="password" Text="Password" runat="server" Style="display:none" onblur="WaterMark(this, event);" Font-Names="Arial" CssClass="txtbox"/>
                         </div>
                         <div style=" height:15px">
                        
                            <asp:Label ID="lblvalidmsgpwd" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/></div>
                    <div class="frm_blk_txt">
                        <asp:TextBox ID="txttemrpwd" Text="Re-type Password" runat="server"  onfocus="WaterMark(this, event);" ForeColor="Gray" Font-Names="Arial" CssClass="txtbox"/> 
                        <asp:TextBox ID="txtrepassword" TextMode="password" Text="Re-type Password" runat="server"  Style="display:none" onblur="WaterMark(this, event);" Font-Names="Arial" CssClass="txtbox"/>
                        </div>
                         <div style=" height:15px" >
                        
                            <asp:Label ID="lblvalidmsgrepwd" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/></div>
                    <div class="frm_blk_txt">By signing up, you agree to the <a href="../In/termsandconditions.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Terms & Conditions</a> and the <a href="../In/privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy Policy</a></div>
                   <div>
                   &nbsp;
                   </div>
                    <div class="frm_blk_txt">
                        <asp:Button ID="signup_free" runat="server" Text="Sign Up" Class="btn_bg" 
                            onclick="signup_free_Click"  /> <span class="txt_free">It's Free</span></div>
                          
                          <div>&nbsp;</div>
                    <div class="frm_blk_txt note last">*No credit card details required</div>
                </div>
            </div>
        
    </div>
    
    <div id="container_main">
    <div class="clr">&nbsp;</div>
    	<div class="uses_list_blk">
            <h4>Several uses for Insighto. Find yours!</h4>
            <ul>
                <li>Customer Feedback</li>
                <li>Employee Feedback</li>
                <li>Product Feedback</li>
                <li>Website Feedback</li>
                <li>Exit Interview</li>
                <li>Event Feedback</li>
                <li>Ad Effectiveness Study</li>
                <li>Shopper  Perception</li>
                <li>Service Evaluation</li>
                <li>Salesperson Evaluation</li>
                <li>Pre-event Planning</li>
                <li>Citizen opinion</li>
            </ul>
   			<div class="clr"></div>
          
            <p style="text-align:right;">…and many more</p>
        </div>
    	
   		<div class="clr"></div>
        
        
   		<div class="clr"></div>
  </div>
    
    
</div>
<div id="footer_main">
    <div id="footer_blk">
            <div id="footerL">Copyright &copy; 2014 Insights</div>
            <div id="footerR">
                <ul>
                    <li class="last"><a href="../In/ContactUs.aspx">Contact Us</a></li>
                    <li><a id="lnkAntiSpam" runat="server">Anti spam policy</a></li>
                    <li><a id="lnkFooterPrivacyPolicy" runat="server">Privacy policy</a></li>
                    
                </ul>
            </div>
            <div class="clr"></div>
        </div>
</div>
</form>
</body>
</html>
