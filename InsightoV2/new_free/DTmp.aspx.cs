﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Insighto.Business.Services;
using App_Code;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;

public partial class new_free_DTmp : System.Web.UI.Page
{
    string country;
    string querystring;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void signup_free_Click(object sender, EventArgs e)
    {
      //  lblErrorMsg.Visible = false;
        if (txtName.Text == "Name")
        {
            //lblvalidmsg.Text = "Enter Name";
            //lblvalidmsgemail.Text = "";
            //lblvalidmsgpwd.Text = "";
            //lblvalidmsgrepwd.Text = "";

        }
        //else if (!Isvalidname(txtName.Text))
        //{
        //    lblvalidmsg.Text = "Please enter alphabets only";
        //    lblvalidmsgemail.Text = "";
        //    lblvalidmsgpwd.Text = "";
        //    lblvalidmsgrepwd.Text = "";
        //}

        else if (txtEmail.Text == "Email")
        {

            //lblvalidmsgemail.Text = "Enter email";
            //lblvalidmsg.Text = "";
            //lblvalidmsgpwd.Text = "";
            //lblvalidmsgrepwd.Text = "";
        }
        //else if (!IsValidEmailAddress(txtEmail.Text))
        //{
        //    lblvalidmsgemail.Text = "Enter valid Email";
        //    lblvalidmsg.Text = "";
        //    lblvalidmsgpwd.Text = "";
        //    lblvalidmsgrepwd.Text = "";
        //}

        //else if (!IsPasswordStrong(txtpassword1.Text))
        //{
        //    lblvalidmsgpwd.Text = "Password should contain 6-16 characters";
        //    lblvalidmsgemail.Text = "";
        //    lblvalidmsg.Text = "";
        //    lblvalidmsgrepwd.Text = "";
        //}

        //else if (lblvalidmsgpwd.Text == "Password")
        //{

        //    lblvalidmsgpwd.Text = "Enter Password";
        //    lblvalidmsgemail.Text = "";
        //    lblvalidmsg.Text = "";
        //    lblvalidmsgrepwd.Text = "";
        //}
        //else if (txtrepassword.Text == "Re-typePassword")
        //{

        //    lblvalidmsgrepwd.Text = "Enter Re-typePassword";
        //    lblvalidmsg.Text = "";
        //    lblvalidmsgemail.Text = "";
        //    lblvalidmsgpwd.Text = "";
        //}


        //else if (txtpassword1.Text != txtrepassword.Text)
        //{
        //    lblvalidmsgrepwd.Text = "Password and Retype Password should match";
        //    lblvalidmsg.Text = "";
        //    lblvalidmsgemail.Text = "";
        //    lblvalidmsgpwd.Text = "";
        //}


        else
        {


            if (Page.IsValid)
            {
                //if (txtCaptcha.Text == ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession())
                //{
                var userService = new UsersService();
                string ipAddress = string.Empty;
                if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                    ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
                var user = userService.RegisterFreeUser(txtName.Text.Trim(), string.Empty, txtEmail.Text.Trim(), string.Empty, 0, ipAddress, txttemrpwd.Text.Trim());
                country = Utilities.GetCountryName(ipAddress);
                ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
                ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
                country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();

                if (user == null)
                {

                    //lblErrorMsg.Text = "User already exists.";
                    //lblErrorMsg.Visible = true;
                    //lblvalidmsg.Visible = false;
                    //lblvalidmsgemail.Visible = false;

                }
                else
                {
                    ServiceFactory.GetService<SessionStateService>().SaveUserToSession(user);
                    //  Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserSignUpFreeNextURL(),"userId="+user.USERID));
                    // Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "userId=" + user.USERID));
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.Getsocialnetwork(), "UserId=" + user.USERID));
                    //   Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserPricingpageintermediate(), "UserId=" + user.USERID));
                }


                //}
                //else
                //{
                //    txtCaptcha.Focus();
                //    lblErrMsg.Text = Utilities.ResourceMessage("lblCaptchaErrMsg");
                //    lblErrMsg.Visible = true;
                //    dvErrMsg.Visible = false;
                //}
            }

        }


    }
}