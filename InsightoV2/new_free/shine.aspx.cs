﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Insighto.Business.Services;
using App_Code;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;
using System.Data;

public partial class new_free_shine : System.Web.UI.Page
{
    string country;
    string strsource;
    string strterm;
    string currurl;
    string querystring;
    SurveyCore surcore = new SurveyCore();
    public bool IsPasswordStrong(string p)
    {
        if (string.IsNullOrEmpty(p))
            return false;
        else
        {
            var regex = new Regex("^.{6,16}$");
            return regex.IsMatch(p) && !p.EndsWith(".");
        }
    }

    public bool Isvalidname(string a)
    {
        if (string.IsNullOrEmpty(a))
            return false;
        else
        {
            var regex = new Regex("^[a-zA-Z ]*$");
            return regex.IsMatch(a) && !a.EndsWith(".");
        }

    }

    public bool IsValidEmailAddress(string s)
    {
        if (string.IsNullOrEmpty(s))
            return false;
        else
        {
            var regex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            return regex.IsMatch(s) && !s.EndsWith(".");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        currurl = HttpContext.Current.Request.RawUrl;
        querystring = HttpContext.Current.Request.QueryString.ToString();

        if (querystring != "")
        {
            if (Request.Cookies["Partner"] != null)
            {
                HttpCookie aCookie = Request.Cookies["Partner"];

                strsource = Server.HtmlEncode(aCookie.Values["utm_source"]);
                // strterm = Server.HtmlEncode(aCookie.Values["utm_term"]);

                //if (strterm.ToString() != Request.QueryString["utm_term"].ToString())
                //{
                if (Request.QueryString["utm_source"].ToString().ToUpper() != "INSIGHTO")
                {
                    WriteClicked();
                }
              //   }

            }
            else
            {

                if (Request.QueryString["utm_source"].ToString().ToUpper() != "INSIGHTO")
                {
                WriteClicked();
                 }

            }
        }
        
        lnkAntiSpam.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetAntiSpamUrl(), "Page=AntiSpam");
        lnkFooterPrivacyPolicy.HRef = EncryptHelper.EncryptQuerystring(PathHelper.GetPrivacyPolicyUrl(), "Page=PrivacyPolicy");
    }
    protected void signup_free_Click(object sender, EventArgs e)
    {
        lblErrorMsg.Visible = false;
        if (txtName.Text == "Name")
        {
            lblvalidmsg.Text = "Enter Name";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";

        }
        else if (!Isvalidname(txtName.Text))
        {
            lblvalidmsg.Text = "Please enter alphabets only";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";
        }

        else if (txtEmail.Text == "Email")
        {

            lblvalidmsgemail.Text = "Enter email";
            lblvalidmsg.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";
        }
        else if (!IsValidEmailAddress(txtEmail.Text))
        {
            lblvalidmsgemail.Text = "Enter valid Email";
            lblvalidmsg.Text = "";
            lblvalidmsgpwd.Text = "";
            lblvalidmsgrepwd.Text = "";
        }

        else if (!IsPasswordStrong(txtpassword1.Text))
        {
            lblvalidmsgpwd.Text = "Password should contain 6-16 characters";
            lblvalidmsgemail.Text = "";
            lblvalidmsg.Text = "";
            lblvalidmsgrepwd.Text = "";
        }

        else if (lblvalidmsgpwd.Text == "Password")
        {

            lblvalidmsgpwd.Text = "Enter Password";
            lblvalidmsgemail.Text = "";
            lblvalidmsg.Text = "";
            lblvalidmsgrepwd.Text = "";
        }
        else if (txtrepassword.Text == "Re-typePassword")
        {

            lblvalidmsgrepwd.Text = "Enter Re-typePassword";
            lblvalidmsg.Text = "";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
        }


        else if (txtpassword1.Text != txtrepassword.Text)
        {
            lblvalidmsgrepwd.Text = "Password and Retype Password should match";
            lblvalidmsg.Text = "";
            lblvalidmsgemail.Text = "";
            lblvalidmsgpwd.Text = "";
        }


        else
        {


            if (Page.IsValid)
            {
                //if (txtCaptcha.Text == ServiceFactory.GetService<SessionStateService>().GetCaptchaFromSession())
                //{
                var userService = new UsersService();
                string ipAddress = string.Empty;
                if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                    ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
                var user = userService.RegisterFreeUser(txtName.Text.Trim(), string.Empty, txtEmail.Text.Trim(), string.Empty, 0, ipAddress, txtpassword1.Text.Trim());
                country = Utilities.GetCountryName(ipAddress);
                ServiceFactory<SessionStateService>.Instance.AddCountryNameToSession(country);
                ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();
                country = ServiceFactory<SessionStateService>.Instance.GetCountryNameFromSession();

                if (querystring != "")
                {
                    strsource = Request.QueryString["utm_source"].ToString();
                    strterm = "";
                }
                else
                {
                    strsource = "";
                    strterm = "";
                }
                DataSet dsRefby = surcore.updateReferredby(user.USERID, strsource, strterm);


                Response.Cookies["Partner"].Expires = DateTime.Now.AddDays(-730);


                if (user == null)
                {

                    lblErrorMsg.Text = "User already exists.";
                    lblErrorMsg.Visible = true;
                    lblvalidmsg.Visible = false;
                    lblvalidmsgemail.Visible = false;
                }
                else
                {
                   
                    ServiceFactory.GetService<SessionStateService>().SaveUserToSession(user);
                    //  Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserSignUpFreeNextURL(),"userId="+user.USERID));
                    // Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserWelcomePageURL(), "userId=" + user.USERID));
                    Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.Getsocialnetwork(), "UserId=" + user.USERID));
                    //   Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.GetUserPricingpageintermediate(), "UserId=" + user.USERID));
                }

                //}
                //else
                //{
                //    txtCaptcha.Focus();
                //    lblErrMsg.Text = Utilities.ResourceMessage("lblCaptchaErrMsg");
                //    lblErrMsg.Visible = true;
                //    dvErrMsg.Visible = false;
                //}
            }

        }


    }


    protected void WriteClicked()
    {

        //HttpCookie aCookie = new HttpCookie("lastVisit");
        //aCookie.Value = DateTime.Now.ToString();
        //aCookie.Expires = DateTime.Now.AddDays(1);
        //Response.Cookies.Add(aCookie);


        //Create a new cookie, passing the name into the constructor
        HttpCookie acookie = new HttpCookie("Partner");

        //Set the cookies value
        //if (querystring != "")
        //{
            acookie.Value = Request.QueryString["utm_source"].ToString();
        //}
        //else
        //{
        //    acookie.Value = "shine";
        //}


        //if (querystring != "")
        //{
            acookie.Values["utm_source"] = Request.QueryString["utm_source"].ToString();
          //  acookie.Values["utm_term"] = Request.QueryString["utm_term"].ToString();
        //}
        //else
        //{
        //    acookie.Values["utm_source"] = "shine";
        //  //  acookie.Values["utm_term"] = "";
        //}

        //Set the cookie to expire in 1 minute
        //DateTime dtNow = DateTime.Now;
        //TimeSpan tsMinute = new TimeSpan(1, 1, 1, 1);
        //cookie.Expires = dtNow + tsMinute;

       // Session["Category"] = acookie.Values["utm_term"];

          acookie.Expires = DateTime.Now.AddDays(730);
       // acookie.Expires = Convert.ToDateTime("11/08/2015 4:00:05 PM");
        //Add the cookie
        Response.Cookies.Add(acookie);

        // Response.Write("Cookie written. <br><hr>");
    } 
}