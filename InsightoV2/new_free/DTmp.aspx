﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DTmp.aspx.cs" Inherits="new_free_DTmp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->  <!--<![endif]-->
<html lang="en" class=" js cssanimations csstransitions">
<head runat="server"> 
     <title>Free Online Survey Software. Sign Up Now.</title>
    <!-- Meta -->
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  <meta name="description" content="Free Online Survey Software. Sign Up Now." />
 <meta name="keywords" content="Free Online Surveys, Online Survey Tool, Customer Feedback, Employee Feedback, Product Feedback, Website Feedback, Exit Interview, Event Feedback, Ad Effectiveness Study, Shopper  Perception, Service Evaluation, Salesperson Evaluation, Pre-event Planning, Citizen opinion " />
 
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/plugins/owl-carousel/assets/ico/insighto.ico"/>
    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/style.css"/>

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css"/>
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css"/>
    
    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/themes/default.css"/>
   

</head>
<body>
    <form id="form1" runat="server">

<div class="style-switcher animated fadeInRight">
    <div class="theme-close"><i class="icon-close"></i></div>
    <div class="theme-heading">Theme Colors</div>
    <ul class="list-unstyled">
        <li class="theme-default theme-active" data-style="default" data-header="light"></li>
        <li class="theme-blue" data-style="blue" data-header="light"></li>
        <li class="theme-orange" data-style="orange" data-header="light"></li>
        <li class="theme-red" data-style="red" data-header="light"></li>
        <li class="theme-light last" data-style="light" data-header="light"></li>

        <li class="theme-purple" data-style="purple" data-header="light"></li>
        <li class="theme-aqua" data-style="aqua" data-header="light"></li>
        <li class="theme-brown" data-style="brown" data-header="light"></li>
        <li class="theme-dark-blue" data-style="dark-blue" data-header="light"></li>
        <li class="theme-light-green last" data-style="light-green" data-header="light"></li>
    </ul>
    <div style="margin-bottom:18px;"></div>
    <div class="theme-heading">Layouts</div>
    <div class="text-center">
        <a href="javascript:void(0);" class="btn-u btn-u-green btn-block active-switcher-btn wide-layout-btn">Wide</a>
        <a href="javascript:void(0);" class="btn-u btn-u-green btn-block boxed-layout-btn">Boxed</a>
    </div>
</div><!--/style-switcher-->
<!--=== End Style Switcher ===-->    

<div class="wrapper">
    <!--=== Header ===-->    
    <div class="header">
        <!-- Topbar -->
        <div class="topbar">
            <div class="container">
                <!-- Topbar Navigation -->
                <ul class="loginbar pull-right">
                    <li>                      
                        <a>&nbsp;</a>
                        <ul class="lenguages">
                            <li class="active">
                                <a href="#">&nbsp; <i class="fa fa-check"></i></a> 
                            </li>
                            <li><a href="#">&nbsp;</a></li>
                            <li><a href="#">&nbsp;</a></li>
                            <li><a href="#">&nbsp;</a></li>
                        </ul>
                    </li>             
                    <li><a href="page_faq.html">&nbsp;</a></li>              
                    <li><a href="page_login.html">&nbsp;</a></li>   
                </ul>
                <!-- End Topbar Navigation -->
            </div>
        </div>
        <!-- End Topbar -->
    <button class="btn-u" data-target="#myModal" data-toggle="modal">Modal Sample</button>
<div id="myModal" class="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
<h4 id="myModalLabel" class="modal-title"> Modal Heading</h4>
</div>
<div class="modal-body">
<h4>Text in a modal</h4>
<p>  Duis mollis, est non commodo luctus, nisi erat por…</p>
</div>
<div class="modal-footer">
<button class="btn-u btn-u-default" type="button" data-dismiss="modal">Close</button>
<button class="btn-u" type="button">Save changes</button>
</div>

</div>
</div>
</div>
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                    <a class="navbar-brand" href="http://www.insighto.com">
                        <img id="Img1" src="assets/img/Insighto_logo.png" alt="Logo">
                    </a>
                </div>

            </div>    
        </div>            
        <!-- End Navbar -->
    </div>
    <!--=== End Header ===-->    

    <!--=== Content Part ===-->
    <div class="container content">		
        <!-- Service Blcoks -->
        <div class="row service-v1 margin-bottom-20">
            <div class="col-md-8">
       <div class="panel margin-bottom-20">   
        <div style="text-align:center">
            <h1>The tool that gives you smarter online surveys. Free.<br /></h1><h3>Get features you don't get elsewhere.</h3>
             </div>   
               <%-- <table class="table table-hover">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th style="font-size:large">Other Online Survey Tools</th>
				<th><img width="120px" src="images/Insighto_logo__transparent.png"/></th>
			</tr>
		</thead>
		
		<tbody>
			<tr>
				<td style="text-align:left;font-size:large">Number of responses / survey</td>
				<td style="font-size:large" >100</td>
				<td style="font-size:large">100</td>				
			</tr>			
			<tr>
				<td  style="text-align:left;font-size:large">Unlimited # of questions</td>
				<td ><img src="images/icon_cross1.png"/></td>
				<td ><img src="images/right.png"/></td>				
			</tr>			
			<tr>
				<td  style="text-align:left;font-size:large">Advanced features such as skip logic</td>
				<td ><img src="images/icon_cross1.png"/></td>
				<td ><img src="images/right.png"/></td>
			</tr>			
			<tr>
				<td  style="text-align:left;font-size:large">Your logo on the survey</td>
				<td ><img src="images/icon_cross1.png"/></td>
				<td ><img src="images/right.png"/></td>
			</tr>			
			<tr>
				<td  style="text-align:left;font-size:large">Image / Video questions</td>
				<td ><img src="images/icon_cross1.png"/></td>
				<td ><img src="images/right.png"/></td>
			</tr>			
			<tr>
				<td  style="text-align:left;font-size:large">Animated graphs with results</td>
				<td ><img src="images/icon_cross1.png"/></td>
				<td ><img src="images/right.png"/></td>
			</tr>
		</tbody>	
	</table>   --%>
           <img src="assets/img/Comparison%20Chart%20Pic.PNG" />
              </div>               
            </div>
            <div class="col-md-4 md-margin-bottom-20">
                 <div class="reg-block">
                 <div class="reg-block-header">
                    <h2>Create smarter,</br> more professional surveys. Today.</h2>                   
                <div class="input-group margin-bottom-20">
    <span class="input-group-addon">
        <i class="fa fa-user"></i>
    </span>
    <asp:TextBox ID="txtName" Text="Name" runat="server" class="form-control"/>
</div>
<div class="input-group margin-bottom-20">
    <span class="input-group-addon">
    <i class="fa fa-envelope"></i></span>  
     <asp:TextBox ID="txtEmail" Text="Email" runat="server" class="form-control"/>
</div>
<div class="input-group margin-bottom-20">
    <span class="input-group-addon"><i class="fa fa-lock"></i></span> 
    <asp:TextBox ID="txttempwd" Text="Password" runat="server"  class="form-control"/> 
</div>
<div class="input-group margin-bottom-30">
    <span class="input-group-addon"><i class="fa fa-key"></i></span>
    <asp:TextBox ID="txttemrpwd" Text="Confirm Password" runat="server"  class="form-control"/>
</div>
    <p>
        By signing up, you agree to the <a href="../In/termsandconditions.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Terms & Conditions</a> and the <a href="../In/privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy Policy</a>
    </p>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">           
              <asp:Button ID="signup_free" runat="server" Text="Sign Up. It's Free!" class="btn-u btn-block" 
                            onclick="signup_free_Click"  />
        </div>
    </div>
</div>
               </div>
               
            </div>
        </div>
        

         <div class="rowbelow">

    <div class="col-md-4">
        <h2>15 survey templates</h2>
    <p>
       Jumpstart your surveys with 15 thoughtfully designed, ready-to-use templates      
    </p>
</div>
<div class="col-md-4">
<h2>Brand your survey</h2>
<p>
Add your logo and make your survey exude professionalism
</p>
</div>
<div class="col-md-4">
<h2>Animated graphs</h2>
<p>
See your survey results come to life with well-designed frequency tables and animated graphs
</p>
</div>
    </div>


       <div class="rowbelow">

    <div class="col-md-4">
        <h2>21 question types</h2>
    <p>
       Ask any type of question with access to 21 question types including image and video based questions      
    </p>
</div>

    </div>

    
        <!-- End Service Blcoks -->

        <hr class="margin-bottom-20"/>

        <!-- Info Blcoks -->
        <div class="row">
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-thumbs-up"></i>
                <div class="info-blocks-in">
                    <h3>Customer Feedback</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-hand-o-up"></i>
                <div class="info-blocks-in">
                    <h3>Employee Feedback</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-dropbox"></i>
                <div class="info-blocks-in">
                    <h3>Product Feedback</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
        </div>
        <!-- End Info Blcoks -->

        <!-- Info Blcoks -->
        <div class="row">
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-sitemap"></i>
                <div class="info-blocks-in">
                    <h3>Website Feedback</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-thumbs-o-up"></i>
                <div class="info-blocks-in">
                    <h3>Exit Interview</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-comments-o"></i>
                <div class="info-blocks-in">
                    <h3>Event Feedback</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
        </div>
        <!-- End Info Blcoks -->

        <!-- Info Blcoks -->
        <div class="row">
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-code"></i>
                <div class="info-blocks-in">
                    <h3>Ad Effectiveness Study</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-compress"></i>
                <div class="info-blocks-in">
                    <h3>Shopper Perception</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-html5"></i>
                <div class="info-blocks-in">
                    <h3>Service Evaluation</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
        </div>
        <!-- End Info Blcoks -->

          <!-- Info Blcoks -->
        <div class="row">
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-code"></i>
                <div class="info-blocks-in">
                    <h3>Salesperson Evaluation</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-compress"></i>
                <div class="info-blocks-in">
                    <h3>Pre-event Planning</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-html5"></i>
                <div class="info-blocks-in">
                    <h3>Citizen opinion</h3>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum animi..</p>
                </div>
            </div>
        </div>
        <!-- End Info Blcoks -->
         
        <hr class="margin-bottom-50">

        <!--Testimonials and Info-->
        <div class="row">
            <div class="col-md-5">
                <div class="carousel slide testimonials testimonials-v1" id="testimonials-1">
                    <div class="carousel-inner">
                        <div class="item active">
                            <p>Crasjusto ducimus qui cupiditate non provident, similique sunt in culpaid est anditiis praesentium praesentium blanditiis praesentium non provident, similique sunt in culpaid est anditiis praesentium praesentium..</p>
                            <div class="testimonial-info">
                                <img class="rounded-x" src="assets/img/testimonials/img1.jpg" alt="">
                                <span class="testimonial-author">
                                    Jeremy Asigner 
                                    <em>Web Developer, Unify Theme.</em>
                                </span>
                            </div>
                        </div>
                        <div class="item">
                            <p>Crasjusto ducimus qui cupiditate non provident, similique sunt in culpaid est anditiis praesentium praesentium blanditiis praesentium non provident, similique sunt in culpaid est anditiis praesentium praesentium..</p>
                            <div class="testimonial-info">
                                <img class="rounded-x" src="assets/img/testimonials/user.jpg" alt="">
                                <span class="testimonial-author">
                                    User 
                                    <em>Java Developer, Htmlstream</em>
                                </span>
                            </div>
                        </div>
                        <div class="item">
                            <p>Crasjusto ducimus qui cupiditate non provident, similique sunt in culpaid est anditiis praesentium praesentium blanditiis praesentium non provident, similique sunt in culpaid est anditiis praesentium praesentium..</p>
                            <div class="testimonial-info">
                                <img class="rounded-x" src="assets/img/testimonials/img2.jpg" alt="">
                                <span class="testimonial-author">
                                    Kate Davenport 
                                    <em>Web Designer, Google Inc.</em>
                                </span>
                            </div>                                
                        </div>
                    </div>
                    
                    <div class="carousel-arrow">
                        <a data-slide="prev" href="#testimonials-1" class="left carousel-control">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a data-slide="next" href="#testimonials-1" class="right carousel-control">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <!-- Bordered Funny Boxes -->
                <div class="funny-boxes funny-boxes-top-sea">
                    <div class="row">
                        <div class="col-md-4 funny-boxes-img">
                            <img alt="" src="assets/img/new/img10.jpg" class="img-responsive">
                            <ul class="list-unstyled">
                               <li><i class="fa fa-briefcase"></i> Dell, Google</li>
                               <li><i class="fa fa-map-marker"></i> New York, US</li>
                            </ul>
                        </div>
                        <div class="col-md-8">
                            <h2><a href="#">Turquoise Bordered Box</a></h2>
                            <ul class="list-unstyled funny-boxes-rating">
                               <li><i class="fa fa-star"></i></li>
                               <li><i class="fa fa-star"></i></li>
                               <li><i class="fa fa-star"></i></li>
                               <li><i class="fa fa-star"></i></li>
                               <li><i class="fa fa-star"></i></li>
                            </ul>
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.</p>
                        </div>
                    </div>                            
                </div>
                <!-- End Bordered Funny Boxes -->
            </div>
        </div>
        <!--End Testimonials and Info-->

        <!-- Our Clients -->
        <div id="clients-flexslider" class="flexslider home clients">
            <div class="headline"><h2>Our Clients</h2></div>    
            <ul class="slides">
                <li>
                    <a href="#">
                        <img src="assets/img/clients/Hero Mindmine.png" alt="" width="144px" height="144px"/> 
                        <img src="assets/img/clients/Hero Mindmine.png" class="color-img" alt="" width="144px" height="144px"/>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="assets/img/clients/IMRB.jpg" alt="" width="144px" height="144px"/> 
                        <img src="assets/img/clients/IMRB.jpg" class="color-img" alt="" width="144px" height="144px"/>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="assets/img/clients/Krea.jpg" alt="" width="144px" height="144px"/> 
                        <img src="assets/img/clients/Krea.jpg" class="color-img" alt="" width="144px" height="144px"/>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="assets/img/clients/PeopleMatters.jpg" alt="" width="144px" height="144px"/> 
                        <img src="assets/img/clients/PeopleMatters.jpg" class="color-img" alt="" width="144px" height="144px"/>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="assets/img/clients/Quick Heal cropped.png" alt="" width="144px" height="144px"/> 
                        <img src="assets/img/clients/Quick Heal cropped.png" class="color-img" alt="" width="144px" height="144px"/>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="assets/img/clients/The Strategist.png" alt="" width="144px" height="144px"/> 
                        <img src="assets/img/clients/The Strategist.png" class="color-img" alt="" width="144px" height="144px"/>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="assets/img/clients/University of Manchester.png" alt="" width="144px" height="144px"/> 
                        <img src="assets/img/clients/University of Manchester.png" class="color-img" alt="" width="144px" height="144px"/>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="assets/img/clients/White BG.png" alt=""  width="144px" height="144px"/> 
                        <img src="assets/img/clients/White BG.png" class="color-img" alt="" width="144px" height="144px" />
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="assets/img/clients/WNC_Logo_LR.jpg" alt="" width="144px" height="144px" /> 
                        <img src="assets/img/clients/WNC_Logo_LR.jpg" class="color-img" alt="" width="144px" height="144px"/>
                    </a>
                </li>
                
            </ul>
        </div><!--/flexslider-->
        <!-- End Our Clients -->
    </div><!--/container-->		
    </div>
    <!--=== End Content Part ===-->

    <!--=== Footer ===-->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 md-margin-bottom-40">
                    <!-- About -->
                    <div class="headline"><h2>About</h2></div>  
                    <p class="margin-bottom-25 md-margin-bottom-40">Unify is an incredibly beautiful responsive Bootstrap Template for corporate and creative professionals.</p>    
                    <!-- End About -->

                    <!-- Monthly Newsletter -->
                    <div class="headline"><h2>Monthly Newsletter</h2></div> 
                    <p>Subscribe to our newsletter and stay up to date with the latest news and deals!</p>
                    <form class="footer-subsribe">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Email Address">                            
                            <span class="input-group-btn">
                                <button class="btn-u" type="button">Subscribe</button>
                            </span>
                        </div>                  
                    </form>                         
                    <!-- End Monthly Newsletter -->
                </div><!--/col-md-4-->  
                
                <div class="col-md-4 md-margin-bottom-40">
                    <!-- Recent Blogs -->
                    <div class="posts">
                        <div class="headline"><h2>Recent Blog Entries</h2></div>
                        <dl class="dl-horizontal">
                            <dt><a href="#"><img src="assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                            <dd>
                                <p><a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p> 
                            </dd>
                        </dl>
                        <dl class="dl-horizontal">
                        <dt><a href="#"><img src="assets/img/sliders/elastislide/10.jpg" alt="" /></a></dt>
                            <dd>
                                <p><a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p> 
                            </dd>
                        </dl>
                        <dl class="dl-horizontal">
                        <dt><a href="#"><img src="assets/img/sliders/elastislide/11.jpg" alt="" /></a></dt>
                            <dd>
                                <p><a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p> 
                            </dd>
                        </dl>
                    </div>
                    <!-- End Recent Blogs -->                    
                </div><!--/col-md-4-->

                <div class="col-md-4">
                    <!-- Contact Us -->
                    <div class="headline"><h2>Contact Us</h2></div> 
                    <address class="md-margin-bottom-40">
                        25, Lorem Lis Street, Orange <br />
                        California, US <br />
                        Phone: 800 123 3456 <br />
                        Fax: 800 123 3456 <br />
                        Email: <a href="mailto:info@anybiz.com" class="">info@anybiz.com</a>
                    </address>
                    <!-- End Contact Us -->

                    <!-- Social Links -->
                    <div class="headline"><h2>Stay Connected</h2></div> 
                    <ul class="social-icons">
                        <li><a href="#" data-original-title="Feed" class="social_rss"></a></li>
                        <li><a href="#" data-original-title="Facebook" class="social_facebook"></a></li>
                        <li><a href="#" data-original-title="Twitter" class="social_twitter"></a></li>
                        <li><a href="#" data-original-title="Goole Plus" class="social_googleplus"></a></li>
                        <li><a href="#" data-original-title="Pinterest" class="social_pintrest"></a></li>
                        <li><a href="#" data-original-title="Linkedin" class="social_linkedin"></a></li>
                        <li><a href="#" data-original-title="Vimeo" class="social_vimeo"></a></li>
                    </ul>
                    <!-- End Social Links -->
                </div><!--/col-md-4-->
            </div>
        </div> 
    </div><!--/footer-->
    <!--=== End Footer ===-->

    <!--=== Copyright ===-->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">                     
                    <p>
                        2014 &copy; Unify. ALL Rights Reserved. 
                        <a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>
                    </p>
                </div>
                <div class="col-md-6">  
                    <a href="index.html">
                        <img class="pull-right" id="logo-footer" src="assets/img/logo2-default.png" alt="">
                    </a>
                </div>
            </div>
        </div> 
    </div><!--/copyright--> 
    <!--=== End Copyright ===-->
</div><!--/wrapper-->

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="assets/plugins/hover-dropdown.min.js"></script> 
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init();
        App.initSliders();
    });
</script>
<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
<![endif]-->


    </form>
</body>
</html>
