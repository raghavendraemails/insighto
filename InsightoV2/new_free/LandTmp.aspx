﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LandTmp.aspx.cs" Inherits="new_free_LandTmp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--><!--<![endif]-->  
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Free Online Survey Software | Insighto.com – surveys, templates, polls</title>

    <!-- Meta -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/plugins/owl-carousel/assets/ico/insighto.ico"/>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/style.css"/>

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css"/>
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css"/>     
    <link rel="stylesheet" href="assets/plugins/parallax-slider/css/parallax-slider.css"/>
    <!-- CSS Page Style -->    
    <link rel="stylesheet" href="assets/css/pages/page_log_reg_v2.css"/>   

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets/css/themes/default.css" id="style_color"/>

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css"/>
    <link href='https://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'/>
      <!-- Google Analytics Content Experiment code -->
<script type="text/javascript">
    function utmx_section() { } function utmx() { } (function () {
        var 
k = '55965047-0', d = document, l = d.location, c = d.cookie;
        if (l.search.indexOf('utm_expid=' + k) > 0) return;
        function f(n) {
            if (c) {
                var i = c.indexOf(n + '='); if (i > -1) {
                    var j = c.
indexOf(';', i); return escape(c.substring(i + n.length + 1, j < 0 ? c.
length : j))
                }
            }
        } var x = f('__utmx'), xx = f('__utmxx'), h = l.hash; d.write(
'<sc' + 'ript src="' + 'http' + (l.protocol == 'https:' ? 's://ssl' :
'://www') + '.google-analytics.com/ga_exp.js?' + 'utmxkey=' + k +
'&utmx=' + (x ? x : '') + '&utmxx=' + (xx ? xx : '') + '&utmxtime=' + new Date().
valueOf() + (h ? '&utmxhash=' + escape(h.substr(1)) : '') +
'" type="text/javascript" charset="utf-8"><\/sc' + 'ript>')
    })();
</script>
<script type="text/javascript">
    utmx('url', 'A/B');
</script>
<!-- End of Google Analytics Content Experiment code -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29026379-1']);
    _gaq.push(['_setDomainName', 'insighto.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
    <script type="text/javascript">     
    
         function validuserdata() {

          //   var userex = document.getElementById('hidden1').value;
             var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

       
        if ((document.getElementById('txtName').value == "Name") || (document.getElementById('txtName').value == "")) {
            document.getElementById('lblvalidmsg').innerHTML = "Enter name";
            document.getElementById('lblvalidmsgemail').innerHTML = "";
            document.getElementById('lblvalidmsgpwd').innerHTML = "";
            document.getElementById('lblvalidmsgrepwd').innerHTML = "";
            document.getElementById('txtName').focus();

        }
        else if ((document.getElementById('txtEmail').value == "Email") || (document.getElementById('txtEmail').value == "")) {

            document.getElementById('lblvalidmsgemail').innerHTML = "Enter email";
            document.getElementById('lblvalidmsg').innerHTML = "";
            document.getElementById('lblvalidmsgpwd').innerHTML = "";
            document.getElementById('lblvalidmsgrepwd').innerHTML = "";
            document.getElementById('txtEmail').focus();
        }
//        else if ((document.getElementById('txttempwd').value == "Password") || (document.getElementById('txttempwd').value == "")) {
//            document.getElementById('lblvalidmsgpwd').innerHTML = "Enter password";           
//            document.getElementById('lblvalidmsg').innerHTML = "";
//            document.getElementById('lblvalidmsgemail').innerHTML = "";
//            document.getElementById('lblvalidmsgrepwd').innerHTML = "";
//            document.getElementById('txttempwd').focus();
//        }
        else if ((document.getElementById('txtPwd').value == "Password") || (document.getElementById('txtPwd').value == "")) {       
            document.getElementById('lblvalidmsgpwd').innerHTML = "Enter password";           
            document.getElementById('lblvalidmsg').innerHTML = "";
            document.getElementById('lblvalidmsgemail').innerHTML = "";
            document.getElementById('lblvalidmsgrepwd').innerHTML = "";
            document.getElementById('txtPwd').focus();
        }
        else if ((document.getElementById('txtPwd').value.length < 6) || (document.getElementById('txtPwd').value.length > 16)) {
            document.getElementById('lblvalidmsgpwd').innerHTML = "Password should contain 6-16 characters";
            document.getElementById('lblvalidmsg').innerHTML = "";
            document.getElementById('lblvalidmsgemail').innerHTML = "";
            document.getElementById('lblvalidmsgrepwd').innerHTML = "";
            document.getElementById('txtPwd').focus();
        }      
//        else if ((document.getElementById('txttempwd').value.length < 6) || (document.getElementById('txttempwd').value.length > 16)) {
//            document.getElementById('lblvalidmsgpwd').innerHTML = "Password should contain 6-16 characters";                     
//            document.getElementById('lblvalidmsg').innerHTML = "";
//            document.getElementById('lblvalidmsgemail').innerHTML = "";
//            document.getElementById('lblvalidmsgrepwd').innerHTML = "";
//            document.getElementById('txttempwd').focus();
//        }
        else if ((document.getElementById('txtCPwd').value == "Confirm Password") || (document.getElementById('txtCPwd').value == "")) {
            document.getElementById('lblvalidmsgrepwd').innerHTML = "Enter confirm password";
            document.getElementById('lblvalidmsg').innerHTML = "";
            document.getElementById('lblvalidmsgemail').innerHTML = "";
            document.getElementById('lblvalidmsgpwd').innerHTML = "";
            document.getElementById('txtCPwd').focus();
        }
        else if (!document.getElementById('txtEmail').value.match(mailformat)) {

            document.getElementById('lblvalidmsgemail').innerHTML = "Please enter a valid email address";
            document.getElementById('lblvalidmsg').innerHTML = "";
            document.getElementById('lblvalidmsgpwd').innerHTML = "";
            document.getElementById('lblvalidmsgrepwd').innerHTML = "";
            document.getElementById('txtEmail').focus();

        }
        else if (document.getElementById('txtPwd').value != document.getElementById('txtCPwd').value) {
            document.getElementById('lblvalidmsgrepwd').innerHTML = "Password and confirm password should match";
            document.getElementById('lblvalidmsg').innerHTML = "";
            document.getElementById('lblvalidmsgemail').innerHTML = "";
            document.getElementById('lblvalidmsgpwd').innerHTML = "";
            document.getElementById('txttemrpwd').focus();
        }
//        else if (userex == "existuser") {        
//             document.getElementById('lblErrorMsg').innerHTML = "User already exists.";
//             document.getElementById('txtEmail').select();
//             document.getElementById('txtEmail').focus();           
//        }
        else {
            form1.submit();
        }   
        }
    </script>
     <script language="javascript" type="text/javascript">
         function WaterMark(objtxt, event) {
             var defaultText = "Name";
             var defaultpwdText = "Password";
             var defaultemailText = "Email";
             var defaultconfirmemail = "Confirm Password";
             // Condition to check textbox length and event type
             if (objtxt.id == "txtName" || objtxt.id == "txtPwd" || objtxt.id == "txtEmail" || objtxt.id == "txtCPwd") {
                 if (objtxt.value.length == 0 & event.type == "blur") {
                     //if condition true then setting text color and default text in textbox
                     if (objtxt.id == "txtName") {
                         objtxt.style.color = "Gray";
                         objtxt.value = defaultText;
                     }
                     if (objtxt.id == "txtEmail") {
                         objtxt.style.color = "Gray";
                         objtxt.value = defaultemailText;
                     }
                     if (objtxt.id == "txtPwd") {
                         document.getElementById("<%= txttempwd.ClientID %>").style.display = "block";
                         objtxt.style.display = "none";
                     }
                     if (objtxt.id == "txtCPwd") {
                         document.getElementById("<%= txttemrpwd.ClientID %>").style.display = "block";
                         objtxt.style.display = "none";
                     }
                 }
             }
             // Condition to check textbox value and event type
             if ((objtxt.value == defaultText || objtxt.value == defaultpwdText || objtxt.value == defaultemailText || objtxt.value == defaultconfirmemail) & event.type == "focus") {
                 if (objtxt.id == "txtName") {
                     objtxt.style.color = "black";
                     objtxt.value = "";
                 }
                 if (objtxt.id == "txtEmail") {
                     objtxt.style.color = "black";
                     objtxt.value = "";
                 }
                 if (objtxt.id == "txttempwd") {
                     objtxt.style.display = "none";
                     document.getElementById("<%= txtPwd.ClientID %>").style.display = "";
                     document.getElementById("<%= txtPwd.ClientID %>").focus();
                 }
                 if (objtxt.id == "txttemrpwd") {
                     objtxt.style.display = "none";
                     document.getElementById("<%= txtCPwd.ClientID %>").style.display = "";
                     document.getElementById("<%= txtCPwd.ClientID %>").focus();
                 }
                
             }
         }
</script>
</head>	

<body>

<div class="wrapper">
    <!--=== Header ===-->    
    <div class="header">
        <!-- Topbar -->
        <div class="topbar">
            <div class="container">
                <!-- Topbar Navigation -->
               &nbsp;
                <!-- End Topbar Navigation -->
            </div>
        </div>
        <!-- End Topbar -->
    
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header" style="margin-top:-15px;">                  
                    <a class="navbar-brand" href="http://www.insighto.com">
                        <img id="logo-header" src="assets/img/Insighto_logo.png" alt="Logo"/>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav">
                <li ><a  style="color:Gray;font-size:14px;font-weight:bold;"  href="#surveytype">Survey Types</a></li>
                <li ><a  style="color:Gray;font-size:14px;font-weight:bold;"  href="#clients-flexslider">Clients</a></li>
                </ul>
               
                </div><!--/navbar-collapse-->
            </div>    
        </div>            
        <!-- End Navbar -->
    </div>
    <!--=== End Header ===-->    
    <!--=== Slider ===-->
    <div class="slider-innerm">
        <div id="da-slider" class="da-sliderm">
            <div class="da-slidem"> 
                <h2><i>The tool that gives you</i> <br /> <i> smarter online surveys.</i> <br /> <i>Free of Cost.</i></h2>                         
                <div class="da-imgm">
              <%--  <div class="da-slidem da-slide-current">
            <p><i>How Insighto's Free Plan Trumps Those Of Other Online Survey Tools</i></p> 
            </div>  --%>         
                <img src="assets/plugins/parallax-slider/img/ComparisonChartPic.png" alt=""/>
                </div>               
            </div>
        <button class="btn-u" data-toggle="modal" data-target="#responsive" style="position:absolute;top:326px;left:103px;width:250px;" >Sign Up. It's Free!</button>
        </div>
    </div><!--/slider-->
    <!--=== End Slider ===-->                        
                        <!-- Modal -->                       
                          <div class="modal fade" id="responsive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        
                                                <form runat="server" id="form1">
                                                <div class="reg-block" style="width:420px;">
                                            <div class="modal-dialog" style="width:330px;">
                                                <div class="reg-block-header" style="width:330px;">
                                                <div style="position:absolute;top:-40px;left:350px;">
                                                <button class="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                                                </div>
            <h3 style="text-align:center">Create smarter, more professional surveys. Today.</h3>   
            <asp:Label ID="lblErrorMsg" ForeColor="Red" CssClass="lblRequired" Font-Size="14px" runat="server" Visible="false" ></asp:Label>        
        </div>

        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <%--<asp:TextBox ID="txtName" Text="Name" name="txtName" runat="server"  class="form-control"  /> --%>        
            <asp:TextBox ID="txtName" runat="server" Text="Name"  ForeColor="Gray" onblur = "WaterMark(this, event);" onfocus = "WaterMark(this, event);" class="form-control" /> 
            <div style="position:absolute;top:35px;"> 
            <asp:Label ID="lblvalidmsg" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"  />
            </div>
            
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <%--<asp:TextBox ID="txtEmail" Text="Email" runat="server" class="form-control"   />--%>
             <asp:TextBox ID="txtEmail" runat="server" Text="Email"  ForeColor="Gray" onblur = "WaterMark(this, event);" onfocus = "WaterMark(this, event);" class="form-control" /> 
             <div style="position:absolute;top:35px;"> 
             <asp:Label ID="lblvalidmsgemail" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/>     
             </div>  
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
           <%-- <asp:TextBox ID="txttempwd" Text="Password" runat="server"   class="form-control" />--%>
             <asp:TextBox ID="txttempwd" Text="Password" runat="server" onfocus="WaterMark(this, event);" ForeColor="Gray" class="form-control"  />
            <asp:TextBox ID="txtPwd" TextMode="Password" Text="Password" runat="server" Style="display:none" onblur="WaterMark(this, event);" class="form-control"/>
               
               <div style="position:absolute;top:35px;"> 
             <asp:Label ID="lblvalidmsgpwd" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/>     
             </div>  
             
        </div>
        <div class="input-group margin-bottom-30">
            <span class="input-group-addon"><i class="fa fa-key"></i></span>
            <%--<asp:TextBox ID="txttemrpwd" Text="Confirm Password" runat="server"  class="form-control"  /> --%>
             <asp:TextBox ID="txttemrpwd" Text="Confirm Password" runat="server" onfocus="WaterMark(this, event);"  ForeColor="Gray" class="form-control"  />
            <asp:TextBox ID="txtCPwd" TextMode="Password" Text="Confirm Password" runat="server"  Style="display:none" onblur="WaterMark(this, event);" class="form-control"/>
               
              <div style="position:absolute;top:35px;"> 
             <asp:Label ID="lblvalidmsgrepwd" runat="server" ForeColor="Red" text-align="top" Font-Size="12px" font-family="Arial"/>     
             </div>             
        </div>
        <p>
        By signing up, you agree to the <a href="../In/termsandconditions.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Terms & Conditions</a> and the <a href="../In/privacypolicy.aspx?key=FpDe6vRglqx0Y2YybIzvzEPlt0fNdHpI">Privacy Policy</a>
    </p>
                                
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
             
                <button type="button" class="btn-u btn-block" onclick="validuserdata()">Sign Up. It's Free!</button>              
                            <br />   
                            <h6 style="text-align:center">* No Credit card details required</h6>            
            </div>
        </div>         
                             </div>
                                            </div>
                                            <input type="hidden" id="hidden1" runat="server" />
                                                    </form>                                          
                                          
                            </div>
                       
                       
                        <!-- End Modal -->


    <!--=== Purchase Block ===-->
    <div class="purchase">
        <div class="container">   
            
        
          <div class="row">
    <div class="col-md-6">
    
        <h2><i class="icon-info-blocks fa fa-check" style="color:#72C02C;"></i>15 survey templates</h2>
    <h5><p>
       Jumpstart your surveys with 15 thoughtfully designed, ready-to-use templates      
    </p></h5>
</div>
<div class="col-md-6">
<h2><i class="icon-info-blocks fa fa-check" style="color:#72C02C;"></i>Brand your survey</h2>
<h5><p>
Add your logo and make your survey exude professionalism
</p></h5>
</div>

    </div>
     <div class="row">
     <div class="col-md-6">
<h2><i class="icon-info-blocks fa fa-check" style="color:#72C02C;"></i>Animated graphs</h2>
<h5><p>
See your survey results come to life with well-designed frequency tables and animated graphs
</p></h5>
</div>
 <div class="col-md-6">
        <h2><i class="icon-info-blocks fa fa-check" style="color:#72C02C;"></i>21 question types</h2>
   <h5><p>
       Ask any type of question with access to 21 question types including image and video based questions      
    </p></h5>
</div>
     </div>


    </div><!--/row-->
    <!-- End Purchase Block -->
</div>
    <!--=== Content Part ===-->
    <div class="container content" id="surveytype">	
    	<!-- Service Blocks -->
        <div class="row" style="margin-left:0px;"> 
         <div class="headlinem"><h2>Users from around the world are creating surveys for</h2></div>      
        </div>

    	<div class="row margin-bottom-5" style="margin-left:-33px;">
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-bullseye service-icon"></i>
        			<div class="desc">
        				<h4>Customer Feedback</h4>                       
        			</div>
        		</div>	
        	</div>
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-asterisk service-icon"></i>
        			<div class="desc">
        				<h4>Employee Feedback</h4>                        
        			</div>
        		</div>	
        	</div>
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-rocket service-icon"></i>
        			<div class="desc">
        				<h4>Product Feedback</h4>                        
        			</div>
        		</div>	
        	</div>			    
    	</div>

        <div class="row margin-bottom-5" style="margin-left:-33px;" >
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-star service-icon"></i>
        			<div class="desc">
        				<h4>Website Feedback</h4>                       
        			</div>
        		</div>	
        	</div>
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-star service-icon"></i>
        			<div class="desc">
        				<h4>Exit Interview</h4>                      
        			</div>
        		</div>	
        	</div>
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-arrows-alt service-icon"></i>
        			<div class="desc">
        				<h4>Event Feedback</h4>                        
        			</div>
        		</div>	
        	</div>			    
    	</div>

          <div class="row margin-bottom-5" style="margin-left:-33px;">
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-chain service-icon"></i>
        			<div class="desc">
        				<h4>Ad Effectiveness Study</h4>                        
        			</div>
        		</div>	
        	</div>
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-shopping-cart service-icon"></i>
        			<div class="desc">
        				<h4>Shopper Perception</h4>                       
        			</div>
        		</div>	
        	</div>
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-th-large service-icon"></i>
        			<div class="desc">
        				<h4>Service Evaluation</h4>                       
        			</div>
        		</div>	
        	</div>			    
    	</div>

         <div class="row margin-bottom-5" style="margin-left:-33px;">
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-check-circle service-icon"></i>
        			<div class="desc">
        				<h4>Salesperson Evaluation</h4>                      
        			</div>
        		</div>	
        	</div>
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-leaf service-icon"></i>
        			<div class="desc">
        				<h4>Pre-event Planning</h4>                       
        			</div>
        		</div>	
        	</div>
        	<div class="col-md-4">
        		<div class="service">
                    <i class="fa fa-globe service-icon"></i>
        			<div class="desc">
        				<h4>Citizen opinion</h4>                        
        			</div>
        		</div>	
        	</div>			    
    	</div>
         <div class="row margin-bottom-5" style="margin-left:-33px;">
         <div class="col-md-4">  
            <div class="desc">&nbsp;  
            </div>
            </div>
        	<div class="col-md-4">  
            <div class="desc">  
         <button class="btn-u" data-toggle="modal" data-target="#responsive" style="position:absolute;width:250px;left:75px;" >Sign Up. It's Free!</button>
    	 </div>
         </div>  
          <div class="col-md-4">  
            <div class="desc">&nbsp;  
            </div>
            </div>    
        </div>
        <!-- End Service Blokcs -->
        
           <!-- Our Clients -->
        <div id="clients-flexslider" class="flexslider home clients">
            <div class="headline"><h2>Trusted by users from</h2></div>             
            <ul>                              
                <li style="width: 136.5px;left:-40px;">
                    <a href="#">
                        <img src="assets/img/clients/IMRB_grey1.png" alt="" /> 
                        <img src="assets/img/clients/IMRB1.png" class="color-img" alt="" />
                    </a>
                </li>
                <li style="width: 136.5px;left:-40px;">
                    <a href="#">
                        <img src="assets/img/clients/wipro_grey.png" alt="" /> 
                        <img src="assets/img/clients/wipro.png" class="color-img" alt="" />
                    </a>
                </li>  
                <li style="width: 136.5px;left:-40px;">
                    <a href="#">
                        <img src="assets/img/clients/Knowience_grey1.png" alt="" /> 
                        <img src="assets/img/clients/Knowience1.png" class="color-img" alt="" />
                    </a>
                </li>
                <li style="width: 136.5px;left:-40px;">
                    <a href="#">
                        <img src="assets/img/clients/PeopleMatters_grey1.png" alt="" /> 
                        <img src="assets/img/clients/PeopleMatters1.png" class="color-img" alt="" />
                    </a>
                </li>
                <li style="width: 136.5px;left:-40px;">
                    <a href="#">
                        <img src="assets/img/clients/QuickHeal_grey1.png" alt="" /> 
                        <img src="assets/img/clients/QuickHeal1.png" class="color-img" alt="" />
                    </a>
                </li>
                <li style="width: 136.5px;left:-40px;">
                    <a href="#">
                        <img src="assets/img/clients/SchoolPage_grey1.png" alt="" /> 
                        <img src="assets/img/clients/SchoolPage1.png" class="color-img" alt="" />
                    </a>
                </li>             
              <li style="width: 136.5px;left:-40px;">
                    <a href="#">
                        <img src="assets/img/clients/TheStrategist_grey1.png" alt="" /> 
                        <img src="assets/img/clients/TheStrategist1.png" class="color-img" alt="" />
                    </a>
                </li>      
                 <li style="width: 136.5px;left:-40px;">
                    <a href="#">
                        <img src="assets/img/clients/UniversityofManchester_grey1.png" alt="" /> 
                        <img src="assets/img/clients/UniversityofManchester1.png" class="color-img" alt="" />
                    </a>
                </li>      
            </ul>
           
        </div><!--/flexslider-->
        <!-- End Our Clients -->
    </div><!--/container-->		
    <!-- End Content Part -->

   

    <!--=== Copyright ===-->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">                     
                    <p>
                        2014 &copy; Insighto. ALL Rights Reserved.                  
                    </p>
                </div>
            
            </div>
        </div> 
    </div><!--/copyright--> 
    <!--=== End Copyright ===-->
</div><!--/wrapper-->

<!-- JS Global Compulsory -->			
<script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="assets/plugins/parallax-slider/js/modernizr.js"></script>
<script type="text/javascript" src="assets/plugins/parallax-slider/js/jquery.cslider.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/pages/index.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init();
        App.initSliders();
        Index.initParallaxSlider();
    });
</script>

<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
<![endif]-->

</body>
</html>	
