﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Insighto.Business.Services;
using App_Code;
using CovalenseUtilities.Services;
using Insighto.Business.Helpers;

public partial class new_free_LandTmp : System.Web.UI.Page
{
    string country;
    string querystring;

    public bool IsPasswordStrong(string p)
    {
        if (string.IsNullOrEmpty(p))
            return false;
        else
        {
            var regex = new Regex("^.{6,16}$");
            return regex.IsMatch(p) && !p.EndsWith(".");
        }
    }

    public bool Isvalidname(string a)
    {
        if (string.IsNullOrEmpty(a))
            return false;
        else
        {
            var regex = new Regex("^[a-zA-Z ]*$");
            return regex.IsMatch(a) && !a.EndsWith(".");
        }

    }

    public bool IsValidEmailAddress(string s)
    {
        if (string.IsNullOrEmpty(s))
            return false;
        else
        {
            var regex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            return regex.IsMatch(s) && !s.EndsWith(".");
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //txttempwd.Attributes.Add("onfocus", "this.type='text';");
        //txttempwd.Attributes.Add("onblur", "this.type='password';");
        if (!IsPostBack)
        {
            //Response.Write("success");
        }
        else
        {
           // Response.Write("saved");
            var userService = new UsersService();
            string ipAddress = string.Empty;
            if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else
                ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
            var user = userService.RegisterFreeUser(txtName.Text.Trim(), string.Empty, txtEmail.Text.Trim(), string.Empty, 0, ipAddress, txtPwd.Text.Trim());
            if (user == null)
            {
                lblErrorMsg.Text = "User already exists.";
               
              //  hidden1.Value = "existuser";

             // ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "validuserdata();", true);
                lblErrorMsg.Visible = true;
                

            }
            else
            {
                ServiceFactory.GetService<SessionStateService>().SaveUserToSession(user);
                Response.Redirect(EncryptHelper.EncryptQuerystring(PathHelper.Getsocialnetwork(), "UserId=" + user.USERID));
            }
        }
    }
    protected void signup_free_Click(object sender, EventArgs e)
    {
      ////  lblErrorMsg.Visible = false;
      //  if (txtName.Text == "Name")
      //  {
      //      lblvalidmsg.Text = "Enter Name";
      //      //lblvalidmsgemail.Text = "";
      //      //lblvalidmsgpwd.Text = "";
      //      //lblvalidmsgrepwd.Text = "";

      //  }
    }
}