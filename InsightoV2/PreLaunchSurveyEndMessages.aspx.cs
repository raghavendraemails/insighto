﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;

public partial class PreLaunchSurveyEndMessages : SurveyPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        dvErrMsg.Visible = false;
        dvSuccessMsg.Visible = false;

        if (!IsPostBack)
        {

            if (base.SurveyBasicInfoView != null)
            {
                if (base.SurveyBasicInfoView.FINISHOPTIONS_STATUS == 1)
                {
                    rbtnThankyou.SelectedValue = base.SurveyBasicInfoView.THANKUPAGE_TYPE.ToString();
                    rbtnClose.SelectedValue = base.SurveyBasicInfoView.CLOSEPAGE_TYPE.ToString();
                    rbtnOverQuota.SelectedValue = base.SurveyBasicInfoView.OVERQUOTAPAGE_TYPE.ToString();
                    rbtnScreenOut.SelectedValue = base.SurveyBasicInfoView.SCREENOUTPAGE_TYPE.ToString();
                    if (base.SurveyBasicInfoView.THANKUPAGE_TYPE.ToString() == "1")
                    {
                        txtThankyouMsg.Text = base.SurveyBasicInfoView.THANKYOU_PAGE;
                    }
                    //else
                    //{
                    //    lblDefaultMsg.Text = base.SurveyBasicInfoView.THANKYOU_PAGE;
                    //}
                    if (base.SurveyBasicInfoView.CLOSEPAGE_TYPE.ToString() == "1")
                    {
                        txtCloseCustom.Text = base.SurveyBasicInfoView.CLOSE_PAGE;
                    }
                    //else
                    //{
                    //    lblCloseDefaultMsg.Text = base.SurveyBasicInfoView.CLOSE_PAGE;
                    //}
                    if (base.SurveyBasicInfoView.SCREENOUTPAGE_TYPE.ToString() == "1")
                    {
                        txtScreenoutCustom.Text = base.SurveyBasicInfoView.SCREENOUT_PAGE;
                    }
                    else
                    //{
                    //    lblScreenoutMsg.Text = base.SurveyBasicInfoView.SCREENOUT_PAGE;
                    //}
                    if (base.SurveyBasicInfoView.OVERQUOTAPAGE_TYPE.ToString() == "1")
                    {
                        txtOverQuota.Text = base.SurveyBasicInfoView.OVERQUOTA_PAGE;
                    }
                    //else
                    //{
                    //    lblOverQuotaMsg.Text = base.SurveyBasicInfoView.OVERQUOTA_PAGE;
                    //}
                }
                else
                {
                    rbtnThankyou.SelectedValue = base.SurveyBasicInfoView.THANKUPAGE_TYPE.ToString();
                    rbtnClose.SelectedValue = base.SurveyBasicInfoView.CLOSEPAGE_TYPE.ToString();
                    rbtnOverQuota.SelectedValue = base.SurveyBasicInfoView.OVERQUOTAPAGE_TYPE.ToString();
                    rbtnScreenOut.SelectedValue = base.SurveyBasicInfoView.SCREENOUTPAGE_TYPE.ToString();
                    lblDefaultMsg.Text = base.SurveyBasicInfoView.THANKYOU_PAGE;
                    lblCloseDefaultMsg.Text = base.SurveyBasicInfoView.CLOSE_PAGE;
                    lblScreenoutMsg.Text = base.SurveyBasicInfoView.SCREENOUT_PAGE;
                    lblOverQuotaMsg.Text = base.SurveyBasicInfoView.OVERQUOTA_PAGE;
                }

                string fileName = Server.MapPath("~/TemplateFolder/SurveyEndPageContent.txt");
                var dic = new Dictionary<string, string>
                      {
                          {"##LogoImageUrl##", PathHelper.SurveyLogoImageUrl() },
                          {"##ContentImageUrl##", PathHelper.SurveyAlertThankyouUrl() },
                          {"##InsightoUrl##",ConfigurationManager.AppSettings["RootURL"].ToString()}
                      };
                var templateContent = ServiceFactory.GetService<TemplateService>().GetDataFromTempalte(fileName, dic);
                //thankyouLogo.InnerHtml = templateContent[0];
                //closeLogo.InnerHtml = templateContent[0];
                //screenOutLogo.InnerHtml = templateContent[0];
                //overQuotaLogo.InnerHtml = templateContent[0];
                ThankyouContent.InnerHtml = templateContent[1];
                CloseContent.InnerHtml = templateContent[1];
                ScreenoutContent.InnerHtml = templateContent[1];
                OverQuotaContent.InnerHtml = templateContent[1];
                SetThankuPage();
                SetClosePage();
                SetOverQuotaPage();
                SetScreenOutPage();
            }
            else
            {
                if (base.SurveyBasicInfoView.SURVEY_ID == null || base.SurveyBasicInfoView.SURVEY_ID <= 0)
                {
                    Response.Redirect(PathHelper.GetSurveyBasicsURL());
                }
                else if (base.SurveyBasicInfoView.SURVEYCONTROL_ID == null || base.SurveyBasicInfoView.SURVEYCONTROL_ID <= 0)
                {
                    Response.Redirect(PathHelper.GetSurveyControlsURL());
                }
            }

        }

        //if (base.SurveyBasicInfoView != null && base.SurveyBasicInfoView.SURVEY_ID > 0 && (base.SurveyBasicInfoView.STATUS == "Active" || base.SurveyBasicInfoView.STATUS == "Closed"))
        //{
        //    if (base.SurveyBasicInfoView.STATUS == "Closed")
        //    {
        //        string alertText = "";
        //        if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE == 1)
        //            alertText = "The survey is closed because it has crossed the schedule survey close date. If you want to edit the survey, please activate the survey from your closed survey list and select a future close date from your Survey Controls settings.";
        //        else if (base.SurveyBasicInfoView.SCHEDULE_ON_CLOSE == 0)
        //            alertText = "The survey is closed due to achievement of response quota. If you want to edit the survey, please activate the survey  from your closed survey list and increase the number of response quota from your Survey Controls settings.";
        //        this.ClientScript.RegisterStartupScript(this.GetType(), "Some Title", "<script language=\"javaScript\">" + "alert('" + alertText + "');</script>");
        //    }
        //}
    }


    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            string strLaunchUrl = EncryptHelper.EncryptQuerystring("SurveyMessages.aspx", "Page=PSEM&" + Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
            Response.Redirect(strLaunchUrl);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            string strLaunchUrl = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx", "Page=PSEM&" + Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
            Response.Redirect(strLaunchUrl);
        }
    }

    public void SetThankuPage()
    {
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userDetails = sessionService.GetLoginUserDetailsSession();
        var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
        var listFeatures = ServiceFactory.GetService<FeatureService>();
        if (rbtnThankyou.SelectedItem != null)
        {
            if (rbtnThankyou.SelectedItem.Value.ToString() == "1")
            {

                if (listFeatures.Find(licenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 1)
                {

                    if (base.SurveyBasicInfoView.FINISHOPTIONS_STATUS == 1 && base.SurveyBasicInfoView.THANKUPAGE_TYPE == 1)
                    {
                        if (base.SurveyBasicInfoView.THANKYOU_PAGE != "You can put your custom text here")
                        {
                            txtThankyouMsg.ForeColor = System.Drawing.Color.FromName("#F67803");
                        }
                        txtThankyouMsg.Text = base.SurveyBasicInfoView.THANKYOU_PAGE;

                    }
                    else
                    {
                        txtThankyouMsg.Text = "You can put your custom text here";
                    }

                }
                divThankyouDefault.Style.Add("display", "none");
                divThankyouCustom.Style.Add("display", "block");

            }
            else if (rbtnThankyou.SelectedValue == "0")
            {
                rbtnThankyou.SelectedValue = "0";

                divThankyouDefault.Style.Add("display", "block");
                divThankyouCustom.Style.Add("display", "none");
                //Page.RegisterStartupScript("Upgrade", @"<script>window.open('Upgradelicense.aspx','Upgradelicense','height=300,width=600,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');</script>");
            }
        }
        else
        {
            rbtnThankyou.SelectedValue = "0";
            divThankyouDefault.Style.Add("display", "block");
            divThankyouCustom.Style.Add("display", "none");
            // Page.RegisterStartupScript("Upgrade", @"<script>window.open('Upgradelicense.aspx','Upgradelicense','height=300,width=600,left=100,top=100,resizable=yes,scrollbars=yes,titlebar=no,toolbar=no,status=no');</script>");
        }

    }

    public void SetClosePage()
    {
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userDetails = sessionService.GetLoginUserDetailsSession();
        var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
        var listFeatures = ServiceFactory.GetService<FeatureService>();
        if (rbtnClose.SelectedItem != null)
        {
            if (rbtnClose.SelectedItem.Value.ToString() == "1")
            {
                if (listFeatures.Find(licenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 1)
                {
                    if (base.SurveyBasicInfoView.FINISHOPTIONS_STATUS == 1 && base.SurveyBasicInfoView.THANKUPAGE_TYPE == 1)
                    {
                        if (base.SurveyBasicInfoView.CLOSE_PAGE != "You can put your custom text here")
                        {
                            txtCloseCustom.ForeColor = System.Drawing.Color.FromName("#F67803");
                        }
                        txtCloseCustom.Text = base.SurveyBasicInfoView.CLOSE_PAGE;

                    }
                    else
                    {
                        txtCloseCustom.Text = "You can put your custom text here";

                    }

                }
                divCloseDefault.Style.Add("display", "none");
                divCloseCustom.Style.Add("display", "block");

            }
            else if (rbtnClose.SelectedValue == "0")
            {
                divCloseDefault.Style.Add("display", "block");
                divCloseCustom.Style.Add("display", "none");
            }
        }
        else
        {
            rbtnClose.SelectedValue = "0";
            divCloseDefault.Style.Add("display", "block");
            divCloseCustom.Style.Add("display", "none");
        }

    }

    public void SetScreenOutPage()
    {
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userDetails = sessionService.GetLoginUserDetailsSession();
        var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
        var listFeatures = ServiceFactory.GetService<FeatureService>();
        if (rbtnScreenOut.SelectedItem != null)
        {
            if (rbtnScreenOut.SelectedItem.Value.ToString() == "1")
            {
                if (listFeatures.Find(licenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 1)
                {
                    if (base.SurveyBasicInfoView.FINISHOPTIONS_STATUS == 1 && base.SurveyBasicInfoView.SCREENOUTPAGE_TYPE == 1)
                    {
                        if (base.SurveyBasicInfoView.SCREENOUT_PAGE != "You can put your custom text here")
                        {
                            txtScreenoutCustom.ForeColor = System.Drawing.Color.FromName("#F67803");
                        }
                        txtScreenoutCustom.Text = base.SurveyBasicInfoView.SCREENOUT_PAGE;

                    }
                    else
                    {
                        txtScreenoutCustom.Text = "You can put your custom text here";
                    }
                }
                divScreenoutcustom.Style.Add("display", "block");
                divScreenoutdefault.Style.Add("display", "none");

            }
            else if (rbtnScreenOut.SelectedValue == "0")
            {
                divScreenoutcustom.Style.Add("display", "none");
                divScreenoutdefault.Style.Add("display", "block");
            }
        }
        else
        {
            rbtnScreenOut.SelectedValue = "0";
            divScreenoutdefault.Style.Add("display", "block");
            divThankyouCustom.Style.Add("display", "none");
        }

    }

    public void SetOverQuotaPage()
    {
        var sessionService = ServiceFactory.GetService<SessionStateService>();
        var userDetails = sessionService.GetLoginUserDetailsSession();
        var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
        var listFeatures = ServiceFactory.GetService<FeatureService>();
        if (rbtnOverQuota.SelectedItem != null)
        {
            if (rbtnOverQuota.SelectedItem.Value.ToString() == "1")
            {

                if (listFeatures.Find(licenceType, FeatureSurveyCreation.CUSTOMIZE_LASTPAGE).Value == 1)
                {
                    if (base.SurveyBasicInfoView.FINISHOPTIONS_STATUS == 1 && base.SurveyBasicInfoView.OVERQUOTAPAGE_TYPE == 1)
                    {
                        if (base.SurveyBasicInfoView.OVERQUOTA_PAGE != "You can put your custom text here")
                        {
                            txtOverQuota.ForeColor = System.Drawing.Color.FromName("#F67803");
                        }
                        txtOverQuota.Text = base.SurveyBasicInfoView.OVERQUOTA_PAGE;

                    }
                    else
                    {
                        txtOverQuota.Text = "You can put your custom text here";

                    }

                }
                divOverQuotaCustom.Style.Add("display", "block");
                divOverQuotaDefault.Style.Add("display", "none");

            }
            else if (rbtnOverQuota.SelectedValue == "0")
            {
                divOverQuotaCustom.Style.Add("display", "none");
                divOverQuotaDefault.Style.Add("display", "block");
            }
        }
        else
        {
            rbtnOverQuota.SelectedValue = "0";
            divOverQuotaCustom.Style.Add("display", "none");
            divOverQuotaDefault.Style.Add("display", "block");
        }

    }
}