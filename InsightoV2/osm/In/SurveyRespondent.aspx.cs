﻿using System;
using System.Collections;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;

namespace Insighto.Osm.In
{
    /// <summary>
    /// For support of backward compatibility included this page(Especially for migrated surveys only.)
    /// If url re-writing  implemented then this page can be removed from application.
    /// </summary>
    public partial class SurveyRespondent : System.Web.UI.Page
    {
        Hashtable ht = new Hashtable();
        protected override void OnInit(EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Convert.ToString(Request.QueryString["key"]));
                if (ht != null && ht.Contains("SurId"))
                {
                    var latestSurvey = ServiceFactory<SurveyService>.Instance.GetMigratedAuditInfoByV1Survey(ValidationHelper.GetInteger(ht["SurId"].ToString(), 0));
                    if (latestSurvey != null)
                        Response.Redirect(EncryptHelper.EncryptQuerystring("~/" + PathHelper.GetRespondentPage(RespondentDisplayMode.Respondent), "SurveyId=" + latestSurvey.V2SurveyId + "&Layout=1"));
                }
            }
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
    }
}