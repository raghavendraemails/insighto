﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using Insighto.Business.Enumerations;
using Insighto.Business.Helpers;
using Insighto.Business.Services;
using Insighto.Data;
using Insighto.UserControls;
using System.Data;
using System.Data.Sql;
using System.Configuration;

namespace Insighto.Pages
{
    public partial class AddNewQuestion : SurveyPageBase
    {
        public static int surveyId;
        public int questID = -1;
        public int copyQuestID = -1;
        List<string> list = new List<string>();
        bool testcol = true, testrow = true, sbsMatrixTest = true;
        bool test_surveystat = false;
        int quesVal;
        string mode = "";
        string pageName = "";
        int questionSeq, expQuestionNo;
        SurveyCore surveymxseq = new SurveyCore();
        string domainurl = ConfigurationManager.AppSettings["RootURL"].ToString();
        public QuestionType QuesType
        {
            get
            {
                Hashtable ht1 = new Hashtable();
                ht1 = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (ht1.Contains("QuestionType"))
                {
                    quesVal = ValidationHelper.GetInteger(ht1["QuestionType"].ToString(), 0);

                }
                else
                {
                    quesVal = 0;
                }

                return GenericHelper.ToEnum<QuestionType>(quesVal);
            }
        }

        public int SurveyID
        {
            get
            {
                int surveyId = 0;
                surveyId = QueryStringHelper.GetInt("SurveyId", 0);
                Hashtable htSurvey = new Hashtable();
                htSurvey = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (htSurvey.Contains("SurveyId"))
                {
                    surveyId = ValidationHelper.GetInteger(htSurvey["SurveyId"].ToString(), 0);

                }
                return surveyId;

            }
        }

        public string Mode
        {
            get
            {
                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (ht.Contains("Mode"))
                {
                    mode = Convert.ToString(ht["Mode"]);

                }
                return mode;
            }
        }

        public string ParentPage
        {
            get
            {
                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (ht.Contains("Page"))
                {
                    pageName = Convert.ToString(ht["Page"]);

                }
                return pageName;
            }
        }

        public int QuestionId
        {
            get
            {
                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (mode.Equals("Edit"))
                {
                    if (ht.Contains("QuestionId"))
                    {
                        questID = ValidationHelper.GetInteger(ht["QuestionId"].ToString(), 0);
                    }
                    else
                    {
                        questID = 0;
                    }
                }

                return questID;
            }
        }

        public int QuestionSeq
        {
            get
            {
                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (mode.Equals("Add"))
                {
                    questionSeq = ValidationHelper.GetInteger(ht["QuestionSeq"].ToString(), 0);
                }
                return questionSeq;
            }
        }
        public int RankIndex
        {
            get
            {
                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (ht.Contains("RankIndex"))
                {
                    return ValidationHelper.GetInteger(ht["RankIndex"].ToString(), 0);
                }
                else
                {
                    return 0;
                }

            }
        }
        public int ExpQuestionNo
        {
            get
            {
                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (ht != null && ht.Contains("ExpQuestion"))
                    expQuestionNo = ValidationHelper.GetInteger(ht["ExpQuestion"].ToString(), 0);
                return expQuestionNo;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var session = ServiceFactory.GetService<SessionStateService>();
            var sessdet = session.GetLoginUserDetailsSession();
            if (chkOtherAnswer.Checked == true)
            {
                Txt_other.Visible = true;
            }
            else {
                Txt_other.Visible = false;
                Txt_other.Text = "Other";
            }

            if (sessdet != null)
            {
                string renewLink = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + sessdet.UserId);
                hlnkPricelink.Target = "_blank";
                hlnkPricelink.HRef = renewLink;
            }

            if (Request.QueryString["key"] != null)
            {

                Hashtable ht = new Hashtable();
                ht = EncryptHelper.DecryptQuerystringParam((Request.QueryString["key"]));
                if (ht.Contains("SurveyId"))
                {
                    surveyId = ValidationHelper.GetInteger(ht["SurveyId"].ToString(), 0);
                }
                if (Mode == "Copy")
                {
                    copyQuestID = ValidationHelper.GetInteger(ht["QuestionId"].ToString(), 0);

                }
              
                if (mode.Equals("Add"))
                {
                    if (ht.Contains("QuestionSeq"))
                    {

                      int questioncount=int.Parse( ht["QuestionSeq"].ToString());

                        DataSet dsmxseq = surveymxseq.getmaxseqcntsurvey(surveyId);

                        if (dsmxseq.Tables[0].Rows.Count ==  0)
                        {
                            hdnQuesSeq.Value = "0";
                        }
                        else if (questioncount != 0 && questioncount.ToString() != dsmxseq.Tables[0].Rows[0][0].ToString())
                        {
                          
                            hdnQuesSeq.Value = questioncount.ToString();

                        }
                        else
                        {
                            hdnQuesSeq.Value = dsmxseq.Tables[0].Rows[0][0].ToString();
                        }

                      //  hdnQuesSeq.Value = ht["QuestionSeq"].ToString();
                    }
                }
            }
            dvErrMsg.Visible = false;
            dvSuccessMsg.Visible = false;
            lblErrMsg.Visible = false;
            lblSuccMsg.Visible = false;
            hdnImageCount.Value = ConfigurationManager.AppSettings["ImagesCount"];
            hdnVideoCount.Value = ConfigurationManager.AppSettings["VideoCount"];
            if (mode != "Edit")
            {
                QuestionService osmSurveyQuestion = new QuestionService();
                if (quesVal == 19 || quesVal == 21)
                {
                    var surveyDet = osmSurveyQuestion.GetImageVideoCount(surveyId, quesVal);
                    hdnImagVideoCount.Value = Convert.ToString(surveyDet.Count);
                    hdnImageType.Value = Convert.ToString(quesVal);
                }
            }
            if (mode == "Edit")
            {
                if (quesVal == 21)
                {

                    var videoControl = CommonHelper.FindControlRecursive<SurveyVideoControl>(pnlSurveyAnswerControl.Controls);
                    Utilities.FindControl<RequiredFieldValidator>(videoControl, "reqYoutube").Enabled = false;
                }
                //else if(quesVal == 13 )
                //{
                //    var videoControl = CommonHelper.FindControlRecursive<SurveyVideoControl>(pnlSurveyAnswerControl.Controls);
                //    Utilities.FindControl<RequiredFieldValidator>(videoControl, "reqYoutube").Enabled = false;
                //}
            }
            if (quesVal == 21)
            {

                var editorControl = Utilities.FindControl<SurveyAnswerControlBase>(Page, "ucSurveyQuestion");
                Utilities.FindControl<Label>(editorControl, "lblTypeYpurQuestion").CssClass = "purpleColor";
                Utilities.FindControl<Label>(editorControl, "lblTypeYpurQuestion").Font.Bold = true;
                Utilities.FindControl<Label>(editorControl, "lblTypeYpurQuestion").Text = (String)GetLocalResourceObject("stringVideoText");   //"Description of video.";
            }
            if (quesVal == 19)
            {

                var editorControl = Utilities.FindControl<SurveyAnswerControlBase>(Page, "ucSurveyQuestion");
                Utilities.FindControl<Label>(editorControl, "lblTypeYpurQuestion").CssClass = "purpleColor";
                Utilities.FindControl<Label>(editorControl, "lblTypeYpurQuestion").Font.Bold = true;
                Utilities.FindControl<Label>(editorControl, "lblTypeYpurQuestion").Text = (String)GetLocalResourceObject("stringImageText"); //"Description of image.";
            }
            if (!IsPostBack)
            {

                SetPageOptions();
                SetTextToQuestionByMode();
                SetAnswerChoiceTextByQuesType();

                if (QuestionId > 0)
                {
                    FillDetails(QuestionId);
                }
                if (Mode == "Copy" && copyQuestID > 0)
                {
                    FillDetails(copyQuestID);
                    var surveyService = ServiceFactory.GetService<SurveyService>();
                    var surveyDetails = surveyService.GetSuvrveyDetailsBasedOnSurveyId(surveyId);

                    var surveyQuesDet = ServiceFactory.GetService<QuestionService>();
                    var lstQuestions = surveyQuesDet.GetSurveyquestionsDet(surveyDetails[0].SURVEY_ID);
                    foreach (var questions in lstQuestions)
                    {
                        if (questID == questions.QUESTION_ID && QuesType == QuestionType.Text_MultipleBoxes)
                        {
                            dvchkRandomAnswer.Visible = false;
                            dvchkPageBreak.Visible = false;
                            dvchkResponseRequired.Visible = false;
                        }

                    }

                }
            }

            SelectMenuByQuesType();
            SetFeatures();
            bool checkQuestionStatus = CheckQuestionCount();
            SetDivByQestion(checkQuestionStatus);

            SetControlWhenActive();

            //if (QuesType == QuestionType.Video || QuesType == QuestionType.Image)
            //  hlnkPopUp.Attributes.Add("onclick", "closeModalSelf(true,'');"
            //else
            //     hlnkPopUp.Attributes.Add("onclick", "closeModalSelf(false,'');"

        }

        protected override void OnInit(EventArgs e)
        {
            AddControls();
            base.OnInit(e);
        }

        #region "Method:CreateQuestion"
        /// <summary>
        /// CreateQuestion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
              //  string questionStr = string.Empty;
              //  questionStr = ucSurveyQuestion.value;
              //bool IsBlockedWordExists = CheckForBlockedWords(questionStr);            

              //  if (!IsBlockedWordExists)
              //  {
                    AddOrUpdateQuestion();
                    if (lblErrMsg.Visible == false)
                    {
                        ClearControls();
                    }
                    lblSuccMsg.Text = (String)GetLocalResourceObject("lblSuccessMessage"); //"Question saved successfully.";
                    lblSuccMsg.Visible = true;
                //}
                //else
                //{
                //    lblErrMsg.Text = Utilities.ResourceMessage("errBlockedWords"); //"Introduction contains blocked words.";
                //    lblErrMsg.Visible = true;
                //    dvErrMsg.Visible = true;
                //}
            }

        }
        #endregion

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        public void ClearControls()
        {

            if (quesVal == 19)
            {
                var imageControl = CommonHelper.FindControlRecursive<SurveyImageControl>(pnlSurveyAnswerControl.Controls);
                imageControl.Clear();
            }

            else if (quesVal == 13 || quesVal == 9)
            {
                var textControl = CommonHelper.FindControlRecursive<SurveyAnsTextControl>(pnlSurveyAnswerControl.Controls);
                textControl.Clear();
            }

            else if (quesVal == 1 || quesVal == 2 || quesVal == 3 || quesVal == 4 || quesVal == 5 || quesVal == 6 || quesVal == 10 || quesVal == 11 || quesVal == 12 || quesVal == 20 || quesVal == 15)
            {
                if (lblErrMsg.Visible != true)
                {
                    var control = CommonHelper.FindControlRecursive<SurveyAnswerControlBase>(pnlSurveyAnswerControl.Controls);
                    control.Clear();
                }

            }
            if (quesVal == 13 || quesVal == 9)
            {
                var control = CommonHelper.FindControlRecursive<SurveyAnswerControlBase>(pnlSurveyAnswerControl.Controls);
                control.Clear();
            }
            if (QuesType == QuestionType.MultipleChoice_SingleSelect || QuesType == QuestionType.MultipleChoice_MultipleSelect)
            {
                var control = CommonHelper.FindControlRecursive<SuveyColumnControl>(pnlSurveyAnswerControl.Controls);
                control.Clear();
            }
            if (lblErrMsg.Visible != true)
            {
                ucSurveyQuestion.Clear();
            }
            chkOtherAnswer.Checked = false;
            chkPageBreak.Checked = false;
            chkRandomAnswer.Checked = false;
            chkResponseRequired.Checked = false;
            lblSuccMsg.Text = "";
            lblSuccMsg.Visible = false;
        }
        #region "Method:AddOrUpdateQuestion"
        /// <summary>
        /// AddOrUpdateQuestion
        /// </summary>
        public void AddOrUpdateQuestion()
        {
            bool checkQuestionStatus = CheckQuestionCount();
            if (checkQuestionStatus || Mode == "Edit")
            {
                SetFeatures();
                string questionStr = string.Empty;
                questionStr = ucSurveyQuestion.value;
                List<string> lstalignStyle = new List<string>(); int charLimits = 0;
                int testCharLimit = 0;
                int? questionSeq = 0;
                int skip = 0;
                int insertQuesSeq = 0;
                bool testskip = false;
                var control = CommonHelper.FindControlRecursive<SurveyAnswerControlBase>(pnlSurveyAnswerControl.Controls);
                var surveyService = ServiceFactory.GetService<SurveyService>();
                var surveyDetails = surveyService.GetSuvrveyDetailsBasedOnSurveyId(surveyId);
                if (QuestionId > 0)
                {
                    var surveyQuesDet = ServiceFactory.GetService<QuestionService>();
                    var lstQuestions = surveyQuesDet.GetSurveyquestionsDet(surveyDetails[0].SURVEY_ID);
                    foreach (var questions in lstQuestions)
                    {
                        if (QuestionId == questions.QUESTION_ID)
                        {
                            questionSeq = questions.QUESTION_SEQ;
                        }

                    }

                }
                if (quesVal == 10 || quesVal == 11 || quesVal == 12)
                {
                    list = control.Value;
                }
                if (quesVal == 1 || quesVal == 2 || quesVal == 3 || quesVal == 9 || quesVal == 13 || quesVal == 15)
                {
                    list = control.Value; //EditorItemList(control.Value);
                    //list = control.Value; //RemoveDupItems(list);
                    if (list.Count == 0)
                        testrow = false;
                }


                if (quesVal == 4)
                {
                    lstalignStyle = control.Value;
                }

                if (quesVal == 4 || quesVal == 5 || quesVal == 6 || quesVal == 7 || quesVal == 8 || quesVal == 16 || quesVal == 18 || quesVal == 14 || quesVal == 20)
                {
                    skip = 0;
                }

                if (quesVal == 5 || quesVal == 6 || quesVal == 9 || quesVal == 13)
                {
                    string charsCount;
                    var textControl = CommonHelper.FindControlRecursive<SurveyAnsTextControl>(pnlSurveyAnswerControl.Controls);
                    charsCount = textControl.Value.FirstOrDefault();
                    if (quesVal == 5 && ValidationHelper.GetInteger(charsCount, 0) > 2000)
                    {
                        testCharLimit = 2000;
                    }
                    else if ((quesVal == 6 || quesVal == 9) && ValidationHelper.GetInteger(charsCount, 0) > 500)
                    {
                        testCharLimit = 500;
                    }
                    charLimits = ValidationHelper.GetInteger(charsCount, 0);
                }
                if (quesVal == 1 || quesVal == 2)
                {
                    var columnControl = CommonHelper.FindControlRecursive<SuveyColumnControl>(pnlSurveyAnswerControl.Controls);
                    lstalignStyle = columnControl.Value;//rndAlignStyle.SelectedItem.Value.ToString();//this is radio button column in the version 1
                }

                if (quesVal == 16 || quesVal == 17 || quesVal == 18)
                {
                    chkResponseRequired.Checked = false;
                }
                if (quesVal == 17 || quesVal == 18)
                {
                    chkPageBreak.Checked = false;
                }

                if (quesVal == 19)
                {

                    var imageControl = CommonHelper.FindControlRecursive<SurveyImageControl>(pnlSurveyAnswerControl.Controls);
                    AddOrUpdateQuestionTypeImage(charLimits, questionSeq, insertQuesSeq, surveyService, surveyDetails, imageControl);
                    if (mode != "Edit")
                    {
                        int count = ValidationHelper.GetInteger(hdnImagVideoCount.Value, 0);
                        hdnImagVideoCount.Value = Convert.ToString(count + 1);
                    }
                    if (mode == "Edit" && dvErrMsg.Visible == false)
                    {
                        base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                    }
                }
                if (QuesType == QuestionType.Video)
                {
                    var videoControl = CommonHelper.FindControlRecursive<SurveyVideoControl>(pnlSurveyAnswerControl.Controls);
                    string path = string.Empty;
                    string guId = System.Guid.NewGuid().ToString() + surveyId.ToString();
                    osm_images osmImages = new osm_images();
                    var quesService = ServiceFactory.GetService<QuestionService>();
                    int videoSize = ValidationHelper.GetInteger(ConfigurationManager.AppSettings["videoSize"], 0);
                    string vidoeSizeMsg = ConfigurationManager.AppSettings["videoMsg"];
                    if (videoControl.TypeVideo == "1")
                    {

                        if (videoControl.HasFile || (Mode == "Copy" || Mode == "Add"))
                        {
                            if (videoControl.FileSize <= videoSize)
                            {
                                //string question = videoControl.QuestionValue;

                                string fileName = videoControl.FileName;
                                string[] ArrayfileName = fileName.Split('.');
                                string url = Request.Url.Host;
                            //    path = "http://" + url + "/Media/Videos/ConvertVideo/" + guId + ArrayfileName[0] + ".flv";

                              //  path = "http://" + url + "/Media/Videos/OriginalVideo/" + guId + ArrayfileName[0] + "." + ArrayfileName[1];
                                //path = videoControl.TypeVideo + "$--$" + path;
                                //osmImages.IMAGE_LOCATION = path;
                                //osmImages.IMAGE_NAME = videoControl.FileName;
                                //osmImages.IMAGE_TYPE = videoControl.VideoType; 
                                //osmImages.SURVEY_ID = surveyDetails[0].SURVEY_ID;
                                string filepath = "/Media/Videos/OriginalVideo/" + guId + fileName;
                                ((FileUpload)videoControl.FindControl("fileViedoUpload")).PostedFile.SaveAs(Server.MapPath(filepath));
                               
                           //   ReturnVideo(videoControl.FileName, guId);

                               // string vpath1 = "<iframe width='420' height='315' src='http://" + url + ":8010" + filepath + "'" + " frameborder='0' allowfullscreen></iframe>";


                                string vpath1 = "<embed src='" + domainurl + filepath + "'" + "width='200' height='200' autoplay='false'></embed>";

                           

                                 path = vpath1.Replace("'", "\"");
                                    
                                if (path != "")
                                {
                                    int widthIndex = path.IndexOf("width", 0);
                                    int heightIndex = path.IndexOf("height", 0);
                                 

                                    if (widthIndex != -1 && heightIndex != -1)
                                    {
                                        string width = path.Substring(widthIndex, 11);
                                        string height = path.Substring(heightIndex, 12);
                                    
                                        path = path.Replace(width, @"width=""250""");
                                        path = path.Replace(height, @"height=""200""");


                                    }

                                    path = videoControl.TypeVideo + "$--$" + path;
                                    osmImages.IMAGE_LOCATION = path;
                                    osmImages.IMAGE_NAME = "";
                                    osmImages.IMAGE_TYPE = videoControl.TypeVideo;
                                    osmImages.SURVEY_ID = surveyDetails[0].SURVEY_ID;
                                }

                            
                            }
                            else
                            {
                                dvErrMsg.Visible = true;
                                lblErrMsg.Text = (String)GetLocalResourceObject("lblErrMsgVideo") + " " + vidoeSizeMsg + ".";
                                lblErrMsg.Visible = true;
                                dvSuccessMsg.Visible = false;
                                lblSuccMsg.Visible = false;
                                return;
                            }

                        }
                    }
                    else if (videoControl.TypeVideo == "2" && (Mode == "Copy" || Mode == "Add" || Mode == "" || Mode == "Edit"))
                    {
                        path = videoControl.YoutubePath;
                        if (path != "")
                        {
                            int widthIndex = path.IndexOf("width", 0);
                            int heightIndex = path.IndexOf("height", 0);

                            if (widthIndex != -1 && heightIndex != -1)
                            {
                                string width = path.Substring(widthIndex, 11);
                                string height = path.Substring(heightIndex, 12);

                                path = path.Replace(width, @"width=""250""");
                                path = path.Replace(height, @"height=""200""");

                            }

                            path = videoControl.TypeVideo + "$--$" + path;
                            osmImages.IMAGE_LOCATION = path;
                            osmImages.IMAGE_NAME = "";
                            osmImages.IMAGE_TYPE = videoControl.TypeVideo;
                            osmImages.SURVEY_ID = surveyDetails[0].SURVEY_ID;
                        }
                    }
                    osm_surveyquestion osmQuestion = new osm_surveyquestion();
                    if (Mode == "Copy" || Mode == "Add" || Mode == "" || Mode == "Edit")
                    {
                        int imageId;
                        var imageService = ServiceFactory.GetService<ImageService>();
                        if (path != "")
                        {
                            imageId = imageService.AddImage(osmImages);
                        }
                        osmQuestion = SetQuestionsToSaveImage(charLimits, questionSeq, path, "", true);
                        if (QuestionId > 0)
                        {
                            osmQuestion.QUESTION_ID = QuestionId;
                        }

                        quesService.AddORUpdateSurveyQuestion(QuestionId, list, osmQuestion, insertQuesSeq);
                        if (QuestionId > 0)
                        {
                            if (list.Count > 0)
                            {
                                var ansService = ServiceFactory.GetService<SurveyAnswerService>();
                                ansService.UpdateSurveyQuesAnswers(list, osmQuestion, SurveyID);
                            }
                        }
                        if (Mode == "Add")
                        {
                            int questSeq;
                            if (!string.IsNullOrEmpty(hdnQuesSeq.Value))
                                questSeq = ValidationHelper.GetInteger(hdnQuesSeq.Value, 0);
                            else
                                questSeq = QuestionSeq;

                            osmQuestion.QUESTION_SEQ = questSeq + 1;
                            quesService.UpdateQuestionSeqBySurveyId(osmQuestion);
                            var surveyQuestionsList = quesService.GetAllSurveyQuestionsBySurveyId(surveyId).Where(quests => quests.QUESTION_ID != osmQuestion.QUESTION_ID && quests.QUESTION_SEQ > questSeq);

                            if (surveyQuestionsList.Any())
                            {
                                foreach (var questsList in surveyQuestionsList)
                                {
                                    questsList.QUESTION_SEQ = questsList.QUESTION_SEQ + 1;

                                    quesService.UpdateQuestionSeqBySurveyId(questsList);
                                    questSeq++;
                                }
                            }
                            hdnQuesSeq.Value = ValidationHelper.GetString(ValidationHelper.GetInteger(hdnQuesSeq.Value, 0) + 1);
                        }

                    }
                    else if (Mode == "Edit")
                    {

                        osmQuestion.PAGE_BREAK = Convert.ToInt32(chkPageBreak.Checked);
                        quesService.UpdatePageBreakOrReposeRequired(surveyId, QuestionId, Convert.ToInt32(chkPageBreak.Checked), 1);
                    }
                    SuccessMessageAfterQuestionSave(surveyService, osmQuestion.QUESTION_SEQ);
                    if (mode != "Edit")
                    {
                        int count = ValidationHelper.GetInteger(hdnImagVideoCount.Value, 0);
                        hdnImagVideoCount.Value = Convert.ToString(count + 1);

                    }
                    if (mode == "Edit")
                    {
                        base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                    }

                }
                if (quesVal == 20)
                {
                    bool mandatory = control.GetStatusOfIncludedFields;
                    if (mandatory)
                    {
                        list.Clear();
                        list = control.Value;
                    }
                }
                if ((quesVal != 19) && (quesVal != 21))
                {
                    if (testcol && testrow && questionStr.Trim().Length > 0 && sbsMatrixTest && testCharLimit == 0)
                    {
                        bool test = true;
                        if ((quesVal == 20 && list.Count > 0) || (quesVal != 20))
                        {
                            var surveyQuesDet = ServiceFactory.GetService<QuestionService>();
                            var lstQuestions = surveyQuesDet.GetSurveyquestionsDet(surveyId);
                            //if (surveyDetails[0].SURVEY_ID > 0 && surveyDetails[0].STATUS == "Active")
                            //{
                            //    var ansService = ServiceFactory.GetService<SurveyAnswerService>();

                            //    foreach (var questions in lstQuestions)
                            //    {
                            //        var ansDet = ansService.GetAnswersbyQuestionId(questions.QUESTION_ID);
                            //        if (QuestionId == questions.QUESTION_ID && quesVal != 4)
                            //        {
                            //            if (ansDet != null && ansDet.Count > 0)
                            //            {
                            //                if (list.Count != ansDet.Count)
                            //                {
                            //                    test = false;
                            //                }
                            //            }
                            //        }
                            //    }
                            //} commented by krishna reddy

                            if (test)
                            {
                                bool IsBlockedWordExists = CheckForBlockedWords(questionStr);

                                if (!IsBlockedWordExists)
                                {
                                    foreach (var questions in lstQuestions)
                                    {
                                        if (QuestionId == questions.QUESTION_ID)
                                        {
                                            if (questions.SKIP_LOGIC == 1)
                                            {
                                                testskip = true;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    lblErrMsg.Text = Utilities.ResourceMessage("errBlockedWords"); //"Introduction contains blocked words.";
                                    dvErrMsg.Visible = true;
                                }
                            }
                            if (test)
                            {

                                if (test_surveystat && testskip)
                                    skip = 1;
                                osm_surveyquestion osmQuestion = new osm_surveyquestion();
                                //osmQuestion.QUESTION_ID = questID;
                                osmQuestion.SURVEY_ID = surveyId;
                                osmQuestion.PAGE_BREAK = Convert.ToInt32(chkPageBreak.Checked);
                                osmQuestion.QUESTION_SEQ = questionSeq;
                                osmQuestion.RESPONSE_REQUIRED = Convert.ToInt32(chkResponseRequired.Checked);
                                if (quesVal == 16)
                                {
                                    osmQuestion.RANDOMIZE_ANSWERS = 0;
                                    osmQuestion.OTHER_ANS = 0;
                                }
                                else
                                {
                                    osmQuestion.OTHER_ANS = Convert.ToInt32(chkOtherAnswer.Checked);
                                    osmQuestion.RANDOMIZE_ANSWERS = Convert.ToInt32(chkRandomAnswer.Checked);
                                }

                                osmQuestion.SKIP_LOGIC = skip;

                                if (QuestionId > 0)
                                {
                                    osmQuestion.QUESTION_ID = QuestionId;
                                }
                                osmQuestion.ANSWER_ALIGNSTYLE = lstalignStyle.FirstOrDefault();
                                osmQuestion.CHAR_LIMIT = charLimits;
                                osmQuestion.QUESTION_TYPE_ID = quesVal;
                                osmQuestion.QUSETION_LABEL = questionStr;

                                var quesService = ServiceFactory.GetService<QuestionService>();
                                quesService.AddORUpdateSurveyQuestion(QuestionId, list, osmQuestion, insertQuesSeq);

                                if (QuestionId > 0)
                                {
                                    var ansService = ServiceFactory.GetService<SurveyAnswerService>();
                                    ansService.UpdateSurveyQuesAnswers(list, osmQuestion, SurveyID);
                                }
                                if (Mode == "Add")
                                {
                                    int questSeq;
                                    if (!string.IsNullOrEmpty(hdnQuesSeq.Value))
                                        questSeq = ValidationHelper.GetInteger(hdnQuesSeq.Value, 0);
                                    else
                                        questSeq = QuestionSeq;

                                    osmQuestion.QUESTION_SEQ = questSeq + 1;
                                    quesService.UpdateQuestionSeqBySurveyId(osmQuestion);
                                    
                                    var surveyQuestionsList = quesService.GetAllSurveyQuestionsBySurveyId(surveyId).Where(quests => quests.QUESTION_ID != osmQuestion.QUESTION_ID && quests.QUESTION_SEQ > questSeq);

                                    if (surveyQuestionsList.Any())
                                    {
                                        foreach (var questsList in surveyQuestionsList)
                                        {
                                            questsList.QUESTION_SEQ = questsList.QUESTION_SEQ + 1;

                                            quesService.UpdateQuestionSeqBySurveyId(questsList);
                                            questSeq++;
                                        }
                                    }
                                    hdnQuesSeq.Value = ValidationHelper.GetString(ValidationHelper.GetInteger(hdnQuesSeq.Value, 0) + 1);
                                    
                                }
                                if (osmQuestion.QUESTION_ID > 0)
                                {
                                    if (chkOtherAnswer.Checked == true)
                                    {
                                        surveymxseq.Updatesurveyques_other(Convert.ToInt32(osmQuestion.SURVEY_ID),Convert.ToInt32(osmQuestion.QUESTION_TYPE_ID), Convert.ToInt32(osmQuestion.QUESTION_SEQ), Txt_other.Text);
                                    }
                                    
                                    SuccessMessageAfterQuestionSave(surveyService, RankIndex);
                                }
                                else
                                {
                                    lblSuccMsg.Text = "";
                                    dvSuccessMsg.Visible = false;
                                    lblSuccMsg.Visible = false;
                                    dvErrMsg.Visible = false;
                                    lblErrMsg.Visible = false;

                                }
                            }
                        }
                        else
                        {
                            dvErrMsg.Visible = true;
                            lblErrMsg.Text = (String)GetLocalResourceObject("lblErrMsgContact");  //"Please select atleast one checkbox of contact information.";
                            lblErrMsg.Visible = true;
                            dvSuccessMsg.Visible = false;
                            lblSuccMsg.Visible = false;
                        }

                    }

                    else
                    {
                        dvErrMsg.Visible = true;
                        lblErrMsg.Text = (String)GetLocalResourceObject("lblErrMsgCharactresLimit") + "  " + testCharLimit + ".";
                        if (QuesType == QuestionType.Text_CommentBox || QuesType == QuestionType.Text_MultipleBoxes || QuesType == QuestionType.Text_SingleRowBox)
                        {
                            var textControl = CommonHelper.FindControlRecursive<SurveyAnsTextControl>(pnlSurveyAnswerControl.Controls);
                            textControl.focus();
                        }
                        lblErrMsg.Visible = true;
                        dvSuccessMsg.Visible = false;
                        lblSuccMsg.Visible = false;
                    }

                }
            }
            else
            {
                //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Upgrade", "OpenModalPopUP('UpgradeLicense.aspx', 690, 515, 'yes');", true);
                //btnCreate.Visible = false;
                //SetDivByQestion(true);
            }

        }
        #endregion

        public bool CheckForBlockedWords(string questionStr)
        {
            bool IsBlockedWordExists = false;
            var blockedWords = ServiceFactory.GetService<BlockedWordsService>().GetBlockedList();
            var contentWords = questionStr.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            foreach (osm_blockedwords obw in blockedWords)
            {
                for (int i = 0; i < contentWords.Length; i++)
                {
                    if (contentWords[i].ToString().Contains(obw.BLOCKEDWORD_NAME))  {
                       
                        IsBlockedWordExists = true;
                    }
                    }
            }
           
                //var existedBlockedWords = blockedWords.Where(b => contentWords.Where(c => c.ToString() == b.BLOCKEDWORD_NAME).Any());

                //IsBlockedWordExists = existedBlockedWords.Any();
           
            return IsBlockedWordExists;
        }
        #region
        /// <summary>
        /// Add or update image question type when selects image question type.
        /// </summary>
        /// <param name="charLimits"></param>
        /// <param name="questionSeq"></param>
        /// <param name="insertQuesSeq"></param>
        /// <param name="surveyService"></param>
        /// <param name="surveyDetails"></param>
        /// <param name="imageControl"></param>
        public void AddOrUpdateQuestionTypeImage(int charLimits, int? questionSeq, int insertQuesSeq, SurveyService surveyService, List<osm_survey> surveyDetails, SurveyImageControl imageControl)
        {
            osm_surveyquestion osmQuestion = new osm_surveyquestion();
            var quesService = ServiceFactory.GetService<QuestionService>();
            int imgSize = ValidationHelper.GetInteger(ConfigurationManager.AppSettings["imgSize"], 0);
            string imgMsg = ConfigurationManager.AppSettings["imgMsg"];
            if (imageControl.HasFile || Mode == "Copy")
            {

                if (imageControl.FileSize > imgSize)
                {
                    dvErrMsg.Visible = true;
                    lblErrMsg.Text = (String)GetLocalResourceObject("lblErrMsgImage") + "  " + imgMsg + ".";
                    lblErrMsg.Visible = true;
                    dvSuccessMsg.Visible = false;
                    lblSuccMsg.Visible = false;
                    return;
                }
                else
                {
                    string path = string.Empty;
                    string save = string.Empty;
                    string copyImagePath = string.Empty;
                    bool checkImageHasFile = imageControl.HasFile;
                    if (Mode == "Copy" && checkImageHasFile == false)

                        copyImagePath = SaveImageFromCopyMode();
                    else
                    {
                        int imageId = AddImageFortheImageTypeQuestion(surveyDetails, imageControl, ref path, ref save);
                        if (imageId > 0)
                        {
                            var userService = ServiceFactory.GetService<UsersService>();
                            if (imageControl.HasFile)
                                userService.updateImageSizeInUser(imageControl.FileSize, surveyDetails[0].USERID);
                        }
                    }
                    osmQuestion = SetQuestionsToSaveImage(charLimits, questionSeq, save, copyImagePath, checkImageHasFile);
                    if (QuestionId > 0)
                    {
                        osmQuestion.QUESTION_ID = QuestionId;
                    }

                    quesService.AddORUpdateSurveyQuestion(QuestionId, list, osmQuestion, insertQuesSeq);

                    if (QuestionId > 0)
                    {
                        var ansService = ServiceFactory.GetService<SurveyAnswerService>();
                        ansService.UpdateSurveyQuesAnswers(list, osmQuestion, SurveyID);
                    }
                    if (Mode == "Add")
                    {
                        int questSeq;
                        if (!string.IsNullOrEmpty(hdnQuesSeq.Value))
                            questSeq = ValidationHelper.GetInteger(hdnQuesSeq.Value, 0);
                        else
                            questSeq = QuestionSeq;

                        osmQuestion.QUESTION_SEQ = questSeq + 1;
                        quesService.UpdateQuestionSeqBySurveyId(osmQuestion);
                        var surveyQuestionsList = quesService.GetAllSurveyQuestionsBySurveyId(surveyId).Where(quests => quests.QUESTION_ID != osmQuestion.QUESTION_ID && quests.QUESTION_SEQ > questSeq);

                        if (surveyQuestionsList.Any())
                        {
                            foreach (var questsList in surveyQuestionsList)
                            {
                                questsList.QUESTION_SEQ = questsList.QUESTION_SEQ + 1;

                                quesService.UpdateQuestionSeqBySurveyId(questsList);
                                questSeq++;
                            }
                        }
                        hdnQuesSeq.Value = ValidationHelper.GetString(ValidationHelper.GetInteger(hdnQuesSeq.Value, 0) + 1);
                    }
                    if (osmQuestion.QUESTION_ID > 0)
                    {
                        SuccessMessageAfterQuestionSave(surveyService, osmQuestion.QUESTION_SEQ);
                    }
                }
            }
            else if (Mode == "Edit")
            {
                string path = string.Empty;
                string save = string.Empty;
                string copyImagePath = string.Empty;
                bool checkImageHasFile = imageControl.HasFile;

                osmQuestion = SetQuestionsToSaveImage(charLimits, questionSeq, save, copyImagePath, checkImageHasFile);
                if (QuestionId > 0)
                {
                    osmQuestion.QUESTION_ID = QuestionId;
                }
                quesService.AddORUpdateSurveyQuestion(QuestionId, list, osmQuestion, insertQuesSeq);
                if (QuestionId > 0)
                {
                    if (list.Count > 0)
                    {
                        var ansService = ServiceFactory.GetService<SurveyAnswerService>();
                        ansService.UpdateSurveyQuesAnswers(list, osmQuestion, SurveyID);
                    }
                }
                if (osmQuestion.QUESTION_ID > 0)
                {
                    SuccessMessageAfterQuestionSave(surveyService, osmQuestion.QUESTION_SEQ);
                }
            }
        }
        #endregion



        public osm_surveyquestion SetQuestionsToSaveImage(int charLimits, int? questionSeq, string save, string copyImagePath, bool checkImageHasFile)
        {
            osm_surveyquestion osmQuestion = new osm_surveyquestion();
            osmQuestion.SURVEY_ID = surveyId;
            osmQuestion.QUESTION_TYPE_ID = quesVal;
            osmQuestion.QUESTION_SEQ = questionSeq;
            osmQuestion.SKIP_LOGIC = 0;
            osmQuestion.OTHER_ANS = 0;
            osmQuestion.RESPONSE_REQUIRED = Convert.ToInt32(chkResponseRequired.Checked);
            osmQuestion.PAGE_BREAK = Convert.ToInt32(chkPageBreak.Checked);
            if (Mode == "Copy" && checkImageHasFile == false)
            {
                //osmQuestion.QUSETION_LABEL = copyImagePath;
                osmQuestion.QUSETION_LABEL = ucSurveyQuestion.value;
                list.Add(copyImagePath);
            }
            else if (quesVal != 21)
            {
                //osmQuestion.QUSETION_LABEL = "~" + save;
                osmQuestion.QUSETION_LABEL = ucSurveyQuestion.value;
                if (!string.IsNullOrEmpty(save))
                {
                    list.Add("~" + save);
                }
            }
            else
            {
                osmQuestion.QUSETION_LABEL = ucSurveyQuestion.value;
                if (!string.IsNullOrEmpty(save))
                {
                    list.Add(save);
                }
            }
            osmQuestion.CHAR_LIMIT = charLimits;
            osmQuestion.RANDOMIZE_ANSWERS = 0;

            return osmQuestion;
        }

        public int AddImageFortheImageTypeQuestion(List<osm_survey> surveyDetails, SurveyImageControl imageControl, ref string path, ref string save)
        {
            if (!Directory.Exists(Server.MapPath("/Media/Images")))
            {
                // Directory.CreateDirectory(Server.MapPath("/In/UserImages/" + surveyDetails[0].USERID + "/" + surveyDetails[0].SURVEY_ID));
                Directory.CreateDirectory(Server.MapPath("/Media/Images"));
            }
            path = "/Media/Images/" + System.Guid.NewGuid().ToString() + "_" + "original" + "_" + imageControl.FileNAme;
            ((FileUpload)imageControl.FindControl("fuImageUpload")).PostedFile.SaveAs(Server.MapPath(path));
            save += path;
            save = save.Replace("original", "reSize");

            int imgWidth = ValidationHelper.GetInteger(ConfigurationManager.AppSettings["imgWidth"], 0);
            int imgHeight = ValidationHelper.GetInteger(ConfigurationManager.AppSettings["imgHeight"], 0);
            System.Drawing.Image fullSizeImage = System.Drawing.Image.FromFile(Server.MapPath(path));
            System.Drawing.Image reSizedImage = Utilities.ResizeImage(fullSizeImage, new Size(imgWidth, imgHeight));
            reSizedImage.Save(Server.MapPath(save));

            osm_images osmImages = new osm_images();
            osmImages.IMAGE_LOCATION = path;
            osmImages.IMAGE_NAME = imageControl.FileNAme;
            osmImages.IMAGE_TYPE = imageControl.ImageType;
            osmImages.SURVEY_ID = surveyDetails[0].SURVEY_ID;
            var imageService = ServiceFactory.GetService<ImageService>();
            int imageId = imageService.AddImage(osmImages);
            return imageId;
        }


    
        public void SuccessMessageAfterQuestionSave(SurveyService surveyService, int? QuestionSeq)
        {

            if (ParentPage == "Questionnaire")
            {
                surveyService.UpdateModifedDateBySurId(surveyId);
                dvSuccessMsg.Visible = true;
                lblSuccMsg.Text = (String)GetLocalResourceObject("lblSuccessMessage"); //"Question saved successfully.";
                lblSuccMsg.Visible = true;
                dvErrMsg.Visible = false;
                lblErrMsg.Visible = false;
                if (mode == "Add")
                {
                    if (QuesType != QuestionType.Image && QuesType != QuestionType.Video && QuesType != QuestionType.QuestionHeader)
                    {
                        base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                    }
                    else
                    {
                        if (mode != "")
                        {
                            Response.Redirect(EncryptHelper.EncryptQuerystring("AddNewQuestion.aspx", "SurveyId=" + base.SurveyID + "&Mode=Add" + "&QuestionSeq=" + hdnQuesSeq.Value + "&Page=Questionnaire&surveyFlag=" + base.SurveyFlag));
                        }
                        else
                        {
                            Response.Redirect(EncryptHelper.EncryptQuerystring("AddNewQuestion.aspx", "SurveyId=" + base.SurveyID + "&Page=Questionnaire&surveyFlag=" + base.SurveyFlag));
                        }
                    }
                }
                else if (Mode == "Edit" || Mode == "Copy")
                {

                    base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag + "&QuestionSeq=" + QuestionSeq), ConfigurationManager.AppSettings["TimeOut"]);
                }
                else
                {
                    base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag + "&QuestionSeq=" + QuestionSeq), ConfigurationManager.AppSettings["TimeOut"]);
                }
            }
            else if (ParentPage == "PQ")
            {
                base.CloseModal(EncryptHelper.EncryptQuerystring("PreLaunchQuestionnaire.aspx", "SurveyId=" + surveyId + "&Page=" + ParentPage + "&QSeq=" + QuestionSeq.ToString() + "&surveyFlag=" + SurveyFlag));
            }
            else if (ParentPage == "")
            {
                surveyService.UpdateModifedDateBySurId(surveyId);
                dvSuccessMsg.Visible = true;
                lblSuccMsg.Text = (String)GetLocalResourceObject("lblSuccessMessage");  //"Question saved successfully.";
                lblSuccMsg.Visible = true;
                dvErrMsg.Visible = false;
                lblErrMsg.Visible = false;
                if (mode != "Edit")
                {
                    if (QuesType != QuestionType.Image && QuesType != QuestionType.Video && QuesType != QuestionType.QuestionHeader)
                    {
                        base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("SurveyDrafts.aspx", "SurveyId=" + surveyId + "&surveyFlag=" + base.SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                    }
                    else
                    {
                        if (mode != "")
                        {
                            Response.Redirect(EncryptHelper.EncryptQuerystring("AddNewQuestion.aspx", "SurveyId=" + base.SurveyID + "&Mode=Add" + "&QuestionSeq=" + hdnQuesSeq.Value + "&Page=Questionnaire&surveyFlag=" + base.SurveyFlag));
                        }
                        else
                        {
                            Response.Redirect(EncryptHelper.EncryptQuerystring("AddNewQuestion.aspx", "SurveyId=" + base.SurveyID + "&Page=Questionnaire&surveyFlag=" + base.SurveyFlag));
                        }
                    }
                }
            }
        }

        #region "AddControls"
        /// <summary>
        /// Add User Controls Dynamically
        /// </summary>
        public void AddControls()
        {
            var isSurveyAnswerControlType = SurveyQuestionTypeHelper.SurveyAnswerControlQuestionTypes().FindAll(qt => qt == QuesType).Any();
            var isSurveyNonSkippableQuestionTypes = SurveyQuestionTypeHelper.SurveyNonSkippableQuestionTypes().FindAll(qt => qt == QuesType).Any();
            var isSurveyAnswerMatrixQuestionTypes = SurveyQuestionTypeHelper.SurveyAnswerMatrixQuestionTypes().FindAll(qt => qt == QuesType).Any();
            var isSurveyAnswerTextQuestionTypes = SurveyQuestionTypeHelper.SurveyAnswerTextQuestionTypes().FindAll(qt => qt == QuesType).Any();

            string controlPath = string.Empty;
            string controlPathText = string.Empty;
            if (isSurveyAnswerControlType)
            {
                controlPath = PathHelper.GetAnswerContolURL();
            }
            else if (QuesType == QuestionType.Choice_YesNo)
            {
                controlPath = PathHelper.GetYesNoControl();
            }
            else if (isSurveyAnswerMatrixQuestionTypes)
            {
                controlPath = PathHelper.GetMatrixControlURL();
            }
            else if (isSurveyAnswerTextQuestionTypes)
            {
                controlPath = PathHelper.GetTextBoxControlURl();
            }
            else if (QuesType == QuestionType.Image)
            {
                controlPath = PathHelper.GetImageControlURL();
            }
            else if (QuesType == QuestionType.ContactInformation)
            {
                controlPath = PathHelper.GetContactInfoURL();

            }
            else if (QuesType == QuestionType.Video)
            {
                controlPath = PathHelper.GetVideoControlURL();
            }
            //Load Control
            controlPathText = LoadControlBasedOnQuestype(controlPath, controlPathText);
        }

        #endregion

        #region
        /// <summary>
        /// Load the controls based on the question selected from the menu.
        /// </summary>
        /// <param name="controlPath"></param>
        /// <param name="controlPathText"></param>
        /// <returns></returns>
        public string LoadControlBasedOnQuestype(string controlPath, string controlPathText)
        {
            if (controlPath != "")
            {
                var survAnsControl = (SurveyAnswerControlBase)Page.LoadControl(controlPath);
                survAnsControl.ClientIDMode = System.Web.UI.ClientIDMode.Inherit;
                //survAnsControl.Display();
                survAnsControl.ID = "SurveyAnswerControl"; //String.Format("{0}_{1}", QuesType.ToString(), surveyId);
                pnlSurveyAnswerControl.Controls.Add(survAnsControl);

            }

            if (QuesType == QuestionType.ConstantSum || QuesType == QuestionType.Text_MultipleBoxes)
            {
                controlPathText = PathHelper.GetTextBoxControlURl();
                var surTextControl = (SurveyAnswerControlBase)Page.LoadControl(controlPathText);
                surTextControl.ClientIDMode = System.Web.UI.ClientIDMode.Inherit;
                surTextControl.ID = "SurveyTextboxControl";//String.Format("{0}_{1}_{2}", QuesType.ToString(), surveyId, 1);
                pnlSurveyAnswerControl.Controls.Add(surTextControl);
            }

            if (QuesType == QuestionType.MultipleChoice_SingleSelect || QuesType == QuestionType.MultipleChoice_MultipleSelect)
            {
                controlPathText = "UserControls/SuveyColumnControl.ascx";
                var surTextControl = (SurveyAnswerControlBase)Page.LoadControl(controlPathText);
                surTextControl.ClientIDMode = System.Web.UI.ClientIDMode.Inherit;
                surTextControl.ID = "SuveyColumnControl"; //String.Format("{0}_{1}_{2}", QuesType.ToString(), surveyId, 2);
                pnlSurveyAnswerControl.Controls.Add(surTextControl);
            }
            return controlPathText;
        }
        #endregion

        #region "Method : SetFeatures"
        /// <summary>
        /// SetFeatures according to licencetype of user.
        /// </summary>
        public void SetFeatures()
        {

            var session = ServiceFactory.GetService<SessionStateService>();
            var userService = ServiceFactory.GetService<UsersService>();
            var serviceFeature = ServiceFactory.GetService<FeatureService>();
            var sessdet = session.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(sessdet.LicenseType);
            var listFeatures = serviceFeature.Find(GenericHelper.ToEnum<UserType>(sessdet.LicenseType));
            if (sessdet != null)
            {

                if (sessdet.UserId > 0 && listFeatures != null)
                {
                    if (serviceFeature.Find(licenceType, FeatureSurveyCreation.RANDOMIZE_ANSWER).Value == 0)
                    {
                        chkRandomAnswer.Enabled = false;
                    }
                    else if (serviceFeature.Find(licenceType, FeatureSurveyCreation.RESPONSE_REQUIRED).Value == 0)
                    {
                        chkResponseRequired.Enabled = false;
                    }
                    else if (serviceFeature.Find(licenceType, FeatureSurveyCreation.PAGEBREAK).Value == 0)
                    {
                        chkPageBreak.Enabled = false;
                    }
                    else if (serviceFeature.Find(licenceType, FeatureSurveyCreation.OTHERS_TEXT).Value == 0)
                    {
                        chkOtherAnswer.Enabled = false;
                    }
                    else if (serviceFeature.Find(licenceType, FeatureSurveyCreation.CHARACTER_LIMIT).Value == 0)
                    {

                    }
                }
            }

            else
            {
                Response.Redirect(PathHelper.GetUserLoginPageURL());
            }

        }
        #endregion

        #region "Method:SelectMenuByQuesType"
        /// <summary>
        /// Selected the menuname to display by selecting questiontype
        /// </summary>
        public void SelectMenuByQuesType()
        {
            QuestionTypeService QTService = new QuestionTypeService();
            var lstQuestionType = QTService.GetQuestionType();

            foreach (var qt in lstQuestionType)
            {
                if (qt.QUESTION_TYPE_ID == quesVal)
                {
                    lblQuestionName.Text = qt.QUESTION_TYPE;
                }
            }
        }
        #endregion

        #region "Method : SetPageOptions"
        /// <summary>
        /// Set page options according to questiontype
        /// </summary>
        public void SetPageOptions()
        {
            var sessionState = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();
            if (surveyId > 0)
            {
                chkPageBreak.Checked = Convert.ToBoolean(sessionState.AccountSetting.PAGE_BREAK);
                chkResponseRequired.Checked = Convert.ToBoolean(sessionState.AccountSetting.RESPONSE_REQUIRED);
            }

            if (quesVal == 16 || quesVal == 17 || quesVal == 18 || quesVal == 19)
            {
                dvchkResponseRequired.Visible = false;
            }

            if (quesVal == 4 || quesVal == 5 || quesVal == 6 || quesVal == 7 || quesVal == 8 || quesVal == 14 || quesVal == 20 || quesVal == 10 || quesVal == 11)
            {
                dvchkResponseRequired.Visible = true;
            }
            if (quesVal == 4 || quesVal == 5 || quesVal == 6 || quesVal == 7 || quesVal == 8 || quesVal == 14 || quesVal == 20 || quesVal == 16 || quesVal == 10 || quesVal == 11)
            {
                dvchkPageBreak.Visible = true;
            }

            if (quesVal == 4 || quesVal == 5 || quesVal == 6 || quesVal == 7 || quesVal == 8 || quesVal == 9 || quesVal == 10 || quesVal == 11 || quesVal == 12 || quesVal == 13 || quesVal == 14 || quesVal == 15 || quesVal == 16 || quesVal == 19 || quesVal == 20)
            {
                dvchkRandomAnswer.Visible = false;
            }

            if (quesVal == 1 || quesVal == 2 || quesVal == 3 || quesVal == 4)
            {
                chkOtherAnswer.Visible = true;
                divCheckOther.Visible = true;
                //dvchkRandomAnswer.Visible = true;
            }
            else
            {
                chkOtherAnswer.Visible = false;
                divCheckOther.Visible = false;
            }

            if (quesVal == 17 || quesVal == 18)
            {
                chkOtherAnswer.Visible = false;
                dvchkRandomAnswer.Visible = false;
                dvchkPageBreak.Visible = false;
                dvchkResponseRequired.Visible = false;
                divCheckOther.Visible = false;
            }

            if (quesVal == 21)
            {
                dvchkRandomAnswer.Visible = false;
                dvchkResponseRequired.Visible = false;
                dvchkPageBreak.Visible = false;
                divImageText.Visible = true;
            }
            if (quesVal == 19)
            {
                //dvchkRandomAnswer.Visible = false;
                dvchkResponseRequired.Visible = false;
                dvchkPageBreak.Visible = false;
                if (mode != "Edit")
                {
                    divImageText.Visible = true;
                }
            }
            if (quesVal == 18)
            {
                if (mode != "Edit")
                {
                    divImageText.Visible = true;
                }
            }

        }
        #endregion

        #region "Method:Fill details"
        /// <summary>
        /// Fill details in related controls by questionid
        /// </summary>
        /// <param name="questionId"></param>
        public void FillDetails(int questionId)
        {
            var surveyService = ServiceFactory.GetService<SurveyService>();
            var ansService = ServiceFactory.GetService<SurveyAnswerService>();
            var surveyDetails = surveyService.GetSuvrveyDetailsBasedOnSurveyId(surveyId);
            var surveyQuesDet = ServiceFactory.GetService<QuestionService>();
            var lstQuestions = surveyQuesDet.GetSurveyquestionsDet(surveyId);
            var ansDet = ansService.GetAnswersbyQuestionId(questionId);
            var control = CommonHelper.FindControlRecursive<SurveyAnswerControlBase>(pnlSurveyAnswerControl.Controls);
            int tempQuestID = questionId;
            List<string> lstStr = new List<string>();
            foreach (var questions in lstQuestions)
            {
                if (tempQuestID == questions.QUESTION_ID)
                {
                    if (quesVal == questions.QUESTION_TYPE_ID)
                    {
                        if (questions.QUESTION_TYPE_ID != 19)
                        {
                            ucSurveyQuestion.value = questions.QUSETION_LABEL;
                        }
                        if (questions.QUESTION_TYPE_ID == 1 || questions.QUESTION_TYPE_ID == 2 || questions.QUESTION_TYPE_ID == 3 || questions.QUESTION_TYPE_ID == 9 || questions.QUESTION_TYPE_ID == 13 || questions.QUESTION_TYPE_ID == 15 || QuesType == QuestionType.Matrix_SingleSelect || QuesType == QuestionType.Matrix_SideBySide || QuesType == QuestionType.Matrix_MultipleSelect || QuesType == QuestionType.ContactInformation)
                        {
                            foreach (var ans in ansDet)
                            {
                                if (ans.QUESTION_ID == tempQuestID)
                                {
                                    if (QuesType == QuestionType.ContactInformation)
                                    {
                                        lstStr.Add(ans.ANSWER_OPTIONS + "~" + ans.RESPONSE_REQUIRED + "~" + Convert.ToString(ans.DEMOGRAPIC_BLOCKID));
                                    }
                                    else
                                    {
                                        lstStr.Add(ans.ANSWER_OPTIONS);
                                    }

                                }

                            }
                            control.Value = lstStr;
                            //foreach (var lst  in lstStr)
                            //{
                            //    control.Value.Add(lst);
                            //}

                        }


                        if (QuesType == QuestionType.Text_CommentBox || QuesType == QuestionType.Text_MultipleBoxes || QuesType == QuestionType.Text_SingleRowBox || QuesType == QuestionType.ConstantSum)
                        {
                            var textControl = CommonHelper.FindControlRecursive<SurveyAnsTextControl>(pnlSurveyAnswerControl.Controls);
                            List<String> lstChalrLimit = new List<String>();

                            string charLimit = Convert.ToString(questions.CHAR_LIMIT);
                            lstChalrLimit.Add(charLimit);
                            textControl.Value = lstChalrLimit;
                        }
                        if (QuesType == QuestionType.MultipleChoice_SingleSelect || QuesType == QuestionType.MultipleChoice_MultipleSelect)
                        {
                            var columnControl = CommonHelper.FindControlRecursive<SuveyColumnControl>(pnlSurveyAnswerControl.Controls);
                            List<String> lstColumn = new List<String>();
                            columnControl.Display();
                            //string temp = Utilities.ToEnum<ColumnType>(questions.ANSWER_ALIGNSTYLE).ToString();
                            lstColumn.Add(questions.ANSWER_ALIGNSTYLE == "0" ? "1" : questions.ANSWER_ALIGNSTYLE);
                            columnControl.Value = lstColumn;
                        }

                        if (QuesType == QuestionType.Image)
                        {
                            var imageControl = CommonHelper.FindControlRecursive<SurveyImageControl>(pnlSurveyAnswerControl.Controls);
                            string temp = questions.QUSETION_LABEL;
                            //string[] image = temp.Split();
                            string impagePath = "";
                            //List<String> lstImage = new List<String>();
                            //lstImage.Add(image);
                            foreach (var ans in ansDet)
                            {
                                if (ans.QUESTION_ID == tempQuestID)
                                {
                                    impagePath = ans.ANSWER_OPTIONS;
                                }
                            }
                            imageControl.SetImagePath = impagePath;//questions.QUSETION_LABEL;
                            ucSurveyQuestion.value = questions.QUSETION_LABEL;
                        }

                        if (QuesType == QuestionType.Choice_YesNo)
                        {
                            var choiceYesControl = CommonHelper.FindControlRecursive<SurveyAnsYesNoControl>(pnlSurveyAnswerControl.Controls);
                            lstStr.Add(questions.ANSWER_ALIGNSTYLE);
                            choiceYesControl.Value = lstStr;
                        }

                        if (QuesType == QuestionType.Video)
                        {
                            var videoControl = CommonHelper.FindControlRecursive<SurveyVideoControl>(pnlSurveyAnswerControl.Controls);
                            string videoPath = "";
                            foreach (var ans in ansDet)
                            {
                                if (ans.QUESTION_ID == tempQuestID)
                                {
                                    videoPath = ans.ANSWER_OPTIONS;
                                }
                            }
                            ucSurveyQuestion.value = questions.QUSETION_LABEL;
                            //videoPath = questions.QUSETION_LABEL;
                            string[] videoName = videoPath.Split(new[] { "$--$" }, StringSplitOptions.RemoveEmptyEntries);  // videoPath.Split("$--$");
                            videoControl.Display();
                            if (videoName.Length > 0)
                            {
                                if (videoName[0] == "0")
                                    videoControl.Value = videoName[0];
                                else
                                    videoControl.Value = videoName[0];

                                videoControl.VideoPath = videoName[1];
                            }
                        }

                        chkOtherAnswer.Checked = Convert.ToBoolean(questions.OTHER_ANS);
                        SurveyCore othertext = new SurveyCore();
                        if (chkOtherAnswer.Checked == true)
                        {
                            Txt_other.Visible = true;
                            Txt_other.Text = othertext.Selectsurveyques_other(Convert.ToInt32(questions.QUESTION_ID));
                        }
                        chkPageBreak.Checked = Convert.ToBoolean(questions.PAGE_BREAK);
                        chkRandomAnswer.Checked = Convert.ToBoolean(questions.RANDOMIZE_ANSWERS);
                        chkResponseRequired.Checked = Convert.ToBoolean(questions.RESPONSE_REQUIRED);

                        if (surveyDetails[0].SURVEY_ID > 0 && surveyDetails[0].STATUS == "Active")
                        {
                            if (questions.SKIP_LOGIC == 1)
                            {
                                chkResponseRequired.Enabled = false;
                                chkPageBreak.Enabled = false;
                            }
                            chkOtherAnswer.Enabled = false;
                        }


                    }

                }// end of if 
            }

        }
        #endregion

        #region "Method :SetDivByQestion"
        /// <summary>
        /// Set Default message when click on AddNew Question
        /// </summary>
        public void SetDivByQestion(bool status)
        {
            var sessionService = ServiceFactory.GetService<SessionStateService>();
            var userDetails = sessionService.GetLoginUserDetailsSession();
            var licenceType = GenericHelper.ToEnum<UserType>(userDetails.LicenseType);
            var featureService = ServiceFactory.GetService<FeatureService>();
            var userService = ServiceFactory.GetService<UsersService>();
            var licenceValue = featureService.Find(licenceType, FeatureQuestionType.Q_IMAGE).Value;
            var licenceVideoValue = featureService.Find(licenceType, FeatureQuestionType.Q_Video).Value;
            string renew_link = "";
            renew_link = EncryptHelper.EncryptQuerystring(PathHelper.GetPriceURL(), "UserId=" + userDetails.UserId + "&surveyFlag=" + SurveyFlag);
            var userInfo = userService.GetUserDetails(userDetails.UserId);
            hlnkPricelink.Target = "_blank";
            hlnkPricelink.HRef = renew_link;


            if (quesVal == 0)
            {
                dvNoQuesionId.Visible = true;
                dvExsistsQuestionId.Visible = false;
                // lblNoQuestion.Text = "<ul class='nolisticon'><li>Please select a Question Type from among the list given on the left menu.</li><li>You can view an example of the chosen question type and then start framing the question.</li></ul>";
                lblNoQuestion.Text = (String)GetLocalResourceObject("lblNoQuestionMessage");  //"<ul class='nolisticon'><li>Please select a question type from the list given on the left menu.</li><li>An example of the selected question type can be seen on the bottom left of the screen.</li></ul>";
                lblNoQuestion.Visible = true;
            }
            else
            {
                dvExsistsQuestionId.Visible = true;
                dvPaidSubscription.Visible = false;
                dvNoQuesionId.Visible = false;
                lblErrMsg.Text = "";
                lblErrMsg.Visible = false;
                // hlnkPricelink.Target = "";
                //  hlnkPricelink.HRef = ""; ;
            }

            if ((QuesType == QuestionType.Image && licenceValue == 0) || (QuesType == QuestionType.Video && licenceVideoValue == 0) || (QuesType == QuestionType.Matrix_SideBySide && featureService.Find(licenceType, FeatureQuestionType.QM_SIDEBYSIDE).Value == 0))
            {
                dvNoOfQuestions.Visible = false;
                dvImagePaidSubscription.Visible = true;
                dvPaidSubscription.Visible = true;
                dvExsistsQuestionId.Visible = false;
            }
            else if (licenceType.ToString() == "FREE" && !status && Mode != "Edit")
            {
                dvImagePaidSubscription.Visible = false;
                dvNoOfQuestions.Visible = true;
                dvPaidSubscription.Visible = true;
                dvExsistsQuestionId.Visible = false;
                dvNoQuesionId.Visible = false;
            }


        }
        #endregion


        #region "Method: SetControlWhenActive"
        /// <summary>
        /// Set the Controls state when the survey status is Active.
        /// </summary>
        public void SetControlWhenActive()
        {
            if (base.IsSurveyActive || Mode == "Edit")
            {
                if (base.IsSurveyActive)
                {
                    chkOtherAnswer.Enabled = false;
                    chkRandomAnswer.Enabled = false;
                }
                dvPopupLeftMenuPanel.Visible = false;
                ucQuestionsLeftMenu.Visible = false;
                dvCreateQuestionBody.Attributes["class"] = "createQuestionBody_Cln";

                if (QuesType == QuestionType.Image && base.IsSurveyActive)
                {
                    var imageControl = CommonHelper.FindControlRecursive<SurveyImageControl>(pnlSurveyAnswerControl.Controls);
                    imageControl.Visibility();
                }
                else if (QuesType == QuestionType.Video && (base.IsSurveyActive || Mode == "Edit"))
                {
                    var videoControl = CommonHelper.FindControlRecursive<SurveyVideoControl>(pnlSurveyAnswerControl.Controls);
                    videoControl.Visibility();
                }
                else if (QuesType == QuestionType.ConstantSum && base.IsSurveyActive && Mode == "Edit")
                {
                    var constantSumControl = CommonHelper.FindControlRecursive<SurveyAnsTextControl>(pnlSurveyAnswerControl.Controls);
                    Utilities.FindControl<TextBox>(constantSumControl, "txtNoofCharacters").Enabled = false;
                }
            }
        }
        #endregion

        #region "Method : SetTextToQuestionByMode"
        /// <summary>
        ///Change the text of the question by Mode(add,edit & copy)
        /// </summary>
        public void SetTextToQuestionByMode()
        {
            if (Mode == "Edit")
            {
                lblChangeQeustionMode.Text = (String)GetLocalResourceObject("lblChangeQeustionModeEdit");   //"EDIT QUESTION";
            }
            else if (Mode == "Copy")
            {
                lblChangeQeustionMode.Text = (String)GetLocalResourceObject("lblChangeQeustionModeCopy"); //"COPY QUESTION";
            }
        }
        #endregion

        public void SetAnswerChoiceTextByQuesType()
        {

            if (QuesType == QuestionType.EmailAddress || QuesType == QuestionType.DateAndTime || QuesType == QuestionType.Text_CommentBox || QuesType == QuestionType.Text_SingleRowBox
                || QuesType == QuestionType.Numeric_SingleRowBox)
            {
                lblanswerChoice.Text = (String)GetLocalResourceObject("lblanswerChoiceAuto");  //"Answer for this question is automatically taken as displayed in preview.";
            }
            else if (QuesType == QuestionType.MultipleChoice_SingleSelect || QuesType == QuestionType.MultipleChoice_MultipleSelect
                || QuesType == QuestionType.Matrix_SingleSelect || QuesType == QuestionType.Matrix_MultipleSelect || QuesType == QuestionType.Matrix_SideBySide
                || QuesType == QuestionType.Menu_DropDown || QuesType == QuestionType.ConstantSum)
            {
                lblanswerChoice.Text = (String)GetLocalResourceObject("lblanswerChoiceType"); // "Answer Options : Type your answer choice in the spaces below.";
                lblanswerChoice.CssClass = "purpleColor";
                lblanswerChoice.Font.Bold = true;
            }
            else if (QuesType == QuestionType.Choice_YesNo)
            {
                lblanswerChoice.Text = "";
            }
            if (QuesType == QuestionType.Ranking)
            {
                dvrankText.Visible = true;
            }
            else
            {
                dvrankText.Visible = false;
            }
        }

        #region "CheckQuestionCount"
        public bool CheckQuestionCount()
        {
            var questionService = ServiceFactory.GetService<QuestionService>();
            bool checkQuestCount = questionService.CheckQuestionCount(surveyId);
            return checkQuestCount;
        }
        #endregion

        #region "Method:SaveImageFromCopyMode"
        /// <summary>
        /// Copy the image when he user click on copy image.
        /// </summary>
        /// <returns></returns>
        public string SaveImageFromCopyMode()
        {
            var imageSerivce = ServiceFactory.GetService<ImageService>();
            var imageDet = imageSerivce.GetImageDetailsBySurveyId(surveyId);
            osm_images images = new osm_images();
            images.IMAGE_LOCATION = imageDet.IMAGE_LOCATION;
            images.IMAGE_NAME = imageDet.IMAGE_NAME;
            images.IMAGE_TYPE = imageDet.IMAGE_TYPE;
            images.SURVEY_ID = imageDet.SURVEY_ID;
            imageSerivce.AddImage(images);
            var questionService = ServiceFactory.GetService<QuestionService>();
            var questionDet = questionService.GetAllSurveyQuestionsBySurveyId(surveyId);
            return questionDet.Where(ques => ques.QUESTION_TYPE_ID == 19).OrderByDescending(quest => quest.QUESTION_ID).Select(ques => ques.QUSETION_LABEL).First();
        }
        #endregion

        public string SaveVideoFromCopyMode()
        {
            var imageSerivce = ServiceFactory.GetService<ImageService>();
            var imageDet = imageSerivce.GetImageDetailsBySurveyId(surveyId);
            osm_images images = new osm_images();
            images.IMAGE_LOCATION = imageDet.IMAGE_LOCATION;
            images.IMAGE_NAME = imageDet.IMAGE_NAME;
            images.IMAGE_TYPE = imageDet.IMAGE_TYPE;
            images.SURVEY_ID = imageDet.SURVEY_ID;
            imageSerivce.AddImage(images);
            var questionService = ServiceFactory.GetService<QuestionService>();
            var questionDet = questionService.GetAllSurveyQuestionsBySurveyId(surveyId);
            return questionDet.Where(ques => ques.QUESTION_TYPE_ID == 21).OrderByDescending(quest => quest.QUESTION_ID).Select(ques => ques.QUSETION_LABEL).First();
        }

        #region "Method: AddOrUpdateVideoQuestion"
        /// <summary>
        /// 
        /// </summary>
        public void AddOrUpdateVideoQuestion()
        {

        }
        #endregion

        public bool ReturnVideo(string fileName, string guId)
        {
       
            string html = string.Empty;
            //rename if file already exists

            int j = 0;
            string AppPath;
            string inputPath;
            string outputPath;
            string imgpath;
            //string guId = System.Guid.NewGuid().ToString();
            AppPath = HttpContext.Current.Request.PhysicalApplicationPath;
            //Get the application path 
            inputPath = AppPath + "Media/Videos/OriginalVideo";
            //Path of the original file 
            outputPath = AppPath + "Media/Videos/ConvertVideo";
            //Path of the converted file 
            imgpath = AppPath + "Media/Videos/Thumbs/" + guId;
            //Path of the preview file 


            string filepath = "Media/Videos/OriginalVideo/" + guId + fileName;
            // path = "/In/UserImages/" + surveyDetails[0].USERID + "/" + surveyDetails[0].SURVEY_ID + "/" + System.Guid.NewGuid().ToString() + "_" + "original" + "_" + imageControl.FileNAme;


            while (File.Exists(filepath))           
            {
                j = j + 1;
                int dotPos = fileName.LastIndexOf(".");
                string namewithoutext = fileName.Substring(0, dotPos);
                string ext = fileName.Substring(dotPos + 1);
                fileName = namewithoutext + j + "." + ext;
                filepath = "Media/Videos/OriginalVideo/" + System.Guid.NewGuid().ToString() + fileName;
            }
            try
            {
                // path = "/Media/Videos/ConvertedVideo" + surveyDetails[0].USERID + "/" + surveyDetails[0].SURVEY_ID + "/" + System.Guid.NewGuid().ToString() + "_" + "original" + "_" + videoControl.FileName;

            }
            catch
            {
                return false;
            }
            string outPutFile;
            outPutFile = "/Media/Videos/OriginalVideo/" + fileName;
            //int i = this.fileuploadImageVideo.PostedFile.ContentLength;
            System.IO.FileInfo a = new System.IO.FileInfo(Server.MapPath(outPutFile));
            //while (a.Exists == false)
            //{

            //}
            //long b = a.Length;
            //while (i != b)
            //{

            //}
            string cmd = " -i \"" + inputPath + "\\" + guId + fileName + "\" \"" + outputPath + "\\" + guId + fileName.Remove(fileName.IndexOf(".")) + ".flv" + "\"";
            CommonMethods.ConvertNow(cmd);
            string imgargs = " -i \"" + outputPath + "\\" + fileName.Remove(fileName.IndexOf(".")) + ".flv" + "\" -f image2 -ss 1 -vframes 1 -s 280x200 -an \"" + imgpath + "\\" + fileName.Remove(fileName.IndexOf(".")) + ".jpg" + "\"";
            CommonMethods.ConvertNow(imgargs);
            return true;
        }
    }
}
