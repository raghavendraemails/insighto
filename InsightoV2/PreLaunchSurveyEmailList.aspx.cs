﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CovalenseUtilities.Services;
using CovalenseUtilities.Helpers;
using Insighto.Data;
using Insighto.Business.Services;
using Insighto.Business.Helpers;
using Insighto.Business.ValueObjects;
using Insighto.Business.Enumerations;
using Insighto.Exceptions;
using App_Code;


public partial class PreLaunchSurveyEmailList : SurveyPageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        dvSuccessMsg.Visible = false;
        dvErrMsg.Visible = false;

        if (base.SurveyBasicInfoView != null)
        {
            if (base.SurveyBasicInfoView.SURVEY_ID > 0)
            {
                SurveyBasicInfoView surveyBasicInfo = ServiceFactory.GetService<RespondentService>().GetSurveyViewInformation(base.SurveyBasicInfoView.SURVEY_ID);

                if (surveyBasicInfo.CONTACTLIST_ID != 0)
                {
                    var emailService = ServiceFactory.GetService<EmailService>();
                    var contactInfo = emailService.GetContactListByContactListId(surveyBasicInfo.CONTACTLIST_ID);
                    if (contactInfo.Any())
                    {
                        tblEmailList.Visible = true;
                        hlnkEmailList.Text = contactInfo[0].CONTACTLIST_NAME;
                        string strContactUrl = EncryptHelper.EncryptQuerystring("CreateEmailList.aspx", "Page=PSEL&" + Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&" + Constants.CONTACTID + "=" + contactInfo[0].CONTACTLIST_ID + "&surveyFlag=" + base.SurveyFlag);
                        hlnkEmailList.NavigateUrl = strContactUrl;
                    }
                }
                else
                {
                    dvErrMsg.Visible = true;
                    lblErrMsg.Text = Utilities.ResourceMessage("lblErrMsg");
                    tblEmailList.Visible = false;
                }
            }
        }
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            string strLaunchUrl = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx", "Page=PSEL&" + Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
            Response.Redirect(strLaunchUrl);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (base.SurveyBasicInfoView.SURVEY_ID > 0)
        {
            string strLaunchUrl = EncryptHelper.EncryptQuerystring("LaunchSurvey.aspx",  Constants.SURVEYID + "=" + base.SurveyBasicInfoView.SURVEY_ID + "&surveyFlag=" + base.SurveyFlag);
            Response.Redirect(strLaunchUrl);
        }
    }
}