﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insighto.Business.Services;
using Insighto.Data;
using Insighto.Business.ValueObjects;
using System.Collections;
using Insighto.Business.Helpers;
using CovalenseUtilities.Helpers;
using System.Configuration;
using App_Code;

namespace Insighto.Pages
{
    
    public partial class ManageFolderPopUp : BasePage
    {
        Hashtable ht = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {

            lblTitle.Text = Utilities.ResourceMessage("ManageFolder_Title_Add");
            if (Request.QueryString["key"] != null && !IsPostBack)
            {
                ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                if (ht != null && ht.Count > 0 && ht.Contains("FolderId"))
                {
                    FolderService folderService = new FolderService();
                    SessionStateService sessionStateService = new SessionStateService();
                    LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
                    int folderId = ValidationHelper.GetInteger(ht["FolderId"].ToString(),0);
                    if (loggedInUserInfo != null && folderId > 0)
                    {
                        osm_cl_folders folders = new osm_cl_folders();
                        folders.USERID = loggedInUserInfo.UserId;
                        folders.FOLDER_ID = folderId;
                        var folder = folderService.GetFoldersByFolderID(folders);
                        txtFolderName.Text = HttpContext.Current.Server.HtmlDecode(folder.FOLDER_NAME);
                        lblTitle.Text = Utilities.ResourceMessage("ManageFolder_Title_Edit"); //"Edit Folder";
                    }                   

                }  
              
                
            }
        }
        protected void ibtnSave_Click(object sender, ImageClickEventArgs e)
        {
            SessionStateService sessionStateService = new SessionStateService();
            LoggedInUserInfo loggedInUserInfo = sessionStateService.GetLoginUserDetailsSession();
            if (loggedInUserInfo != null)
            {
                osm_cl_folders folders = new osm_cl_folders();
                folders.FOLDER_NAME =  HttpContext.Current.Server.HtmlEncode(txtFolderName.Text.Trim());
                folders.USERID = loggedInUserInfo.UserId;
                FolderService folderService = new FolderService();
                if (Request.QueryString["key"] != null)
                {
                     ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
                     if (ht != null && ht.Count > 0 && ht.Contains("FolderId"))
                     {
                         int folderId = ValidationHelper.GetInteger(ht["FolderId"].ToString(), 0);
                         folders.FOLDER_NAME = HttpContext.Current.Server.HtmlEncode(txtFolderName.Text.Trim());
                         folders.FOLDER_ID = folderId;
                         folders.USERID = loggedInUserInfo.UserId;
                         var osmfolder = folderService.UpdateFolder(folders);
                         if (osmfolder != null)
                         {
                             lblSuccessMsg.Text = Utilities.ResourceMessage("ManageFolder_Success_Update"); //"Folder name updated succesfully.";
                             divSucessPanel.Visible = true;
                             divErrMsg.Visible = false;
                             base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("ManageFolders.aspx","surveyFlag="+SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                         }
                         else
                         {
                             divErrMsg.Visible = true;
                             divSucessPanel.Visible = false;
                         }
                     }
                     else if (folderService.IsFolderExsists(folders))
                     {
                         var osmfolder = folderService.AddFolder(folders);
                         divSucessPanel.Visible = true;
                         divErrMsg.Visible = false;
                         base.CloseModelForSpecificTime(EncryptHelper.EncryptQuerystring("ManageFolders.aspx", "surveyFlag=" + SurveyFlag), ConfigurationManager.AppSettings["TimeOut"]);
                     }
                     else
                     {
                         divErrMsg.Visible = true;
                         divSucessPanel.Visible = false;
                     }
                }
                
            }
        }
    }
}