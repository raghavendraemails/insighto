﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Insighto.Business.Charts;
using Insighto.Business.Helpers;
using Insighto.Business.Enumerations;
using Insighto.Business.Services;
using Insighto.Business.ValueObjects;
using CovalenseUtilities.Helpers;
using CovalenseUtilities.Services;
using FeatureTypes;
using App_Code;
using System.Windows;
using System.Windows.Forms;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Media;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Configuration;
using System.IO.Packaging;


public partial class DescriptiveResponsesVoice : SurveyPageBase
{
    int quesID = 0;
    int surveyID = 0;
    Hashtable ht = new Hashtable();
    SurveyCoreVoice surcore = new SurveyCoreVoice();
    DataSet ds;

    protected void Page_Load(object sender, EventArgs e)
    {

        var sessionService = ServiceFactory.GetService<SessionStateService>();
        //var userInfo = sessionService.GetLoginUserDetailsSession();
        var userDetails = ServiceFactory.GetService<SessionStateService>().GetLoginUserDetailsSession();

        Session["TempChart_det"] = null;
        ValidationCore vcore = new ValidationCore();
        if (Request.QueryString["key"] != null)
        {
            ht = EncryptHelper.DecryptQuerystringParam(Request.QueryString["key"]);
            if (ht != null && ht.Count > 0 && ht.Contains("QuesID"))
            {
                quesID = Convert.ToInt32(ht["QuesID"]);

                if (ht.Contains(Constants.SURVEYID))
                    surveyID = Convert.ToInt32(ht[Constants.SURVEYID]);


                int i = 0;
                int flag = 0;
                SurveyCoreVoice cor = new SurveyCoreVoice();
                ArrayList ans_ids = new ArrayList();
                ArrayList ans_opt = new ArrayList();
              //  DataSet ds;
                SelectedSurvey = surcore.GetSurveyWithResponsesCount(surveyID, Convert.ToDateTime("1/1/1800 12:00:00 AM"), Convert.ToDateTime("1/1/1800 12:00:00 AM"));
                Insighto.Business.Charts.Survey sur = SelectedSurvey;
                if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
                {
                    foreach (SurveyQuestion ques in SelectedSurvey.surveyQues)
                    {
                        if (ques.QUESTION_ID == quesID)
                        {
                            string str = "";
                            str = Utilities.ClearHTML(Convert.ToString(ques.QUESTION_LABEL));
                            str = System.Text.RegularExpressions.Regex.Replace(str, "<span[^>]*>", "");
                            str = str.Replace("<p>", "");
                            str = str.Replace("</p>", "");
                            str = str.Replace("\r\n", "");
                            str = str.Replace("<br>", "");
                            str = str.Replace("</br>", "");
                            lblHeader.Text = "<b>Responses for the Question:</b> " + str;
                            if (ques.QUESTION_TYPE == 9 || ques.QUESTION_TYPE == 13 || ques.QUESTION_TYPE == 15 || ques.QUESTION_TYPE == 20)
                                flag = 1;
                            if (ques.QUESTION_TYPE == 1 || ques.QUESTION_TYPE == 2 || ques.QUESTION_TYPE == 3 || ques.QUESTION_TYPE == 4)
                            {
                                flag = 2;
                                lblHeader.Text = "\"Others\" Responses for the Question : " + str;
                            }
                        }
                    }
                }
                if (flag == 1)
                {
                    if (SelectedSurvey.surveyQues != null && SelectedSurvey.surveyQues.Count > 0)
                    {
                        foreach (SurveyQuestion surques in SelectedSurvey.surveyQues)
                        {
                            if (surques.QUESTION_ID == quesID)
                            {
                                if (surques.surveyAnswers != null)
                                {
                                    foreach (SurveyAnswers surans in surques.surveyAnswers)
                                    {
                                        ans_ids.Add(surans.ANSWER_ID);
                                        ans_opt.Add(surans.ANSWER_OPTIONS);
                                    }
                                }
                            }
                        }
                    }
                }
                if (!IsPostBack)
                {
                    if (flag != 1)
                    {
                        if (flag == 2)
                            ds = cor.GetTextResponses(quesID, 1, SelectedSurvey.SURVEY_ID);
                        else
                            ds = cor.GetTextResponses(quesID, 0, SelectedSurvey.SURVEY_ID);                       

                        BoundField bFieldbalnk = new BoundField();
                        bFieldbalnk.HeaderText = "";
                        bFieldbalnk.ItemStyle.Width = Unit.Percentage(20);

                    }
                }

                if (flag == 1)
                {
                    DataSet ds_responselist = new DataSet();
                    ds_responselist = cor.GetRankingConstanSumCommentBoxTexResponses(SelectedSurvey.SURVEY_ID, quesID, ans_ids);
                    if (ds_responselist != null && ds_responselist.Tables.Count > 0)
                    {
                        if (ds_responselist.Tables[0].Columns.Count > 0)
                        {
                            if (!IsPostBack)
                            {
                                foreach (DataColumn col in ds_responselist.Tables[0].Columns)
                                {                                    
                                    BoundField bFieldtxtcol = new BoundField();
                                    bFieldtxtcol.DataField = col.ColumnName;                                   
                                    //if (i == 4)
                                    //{
                                    //    bFieldtxtcol.HeaderText = "S.No";                                        
                                    //    bFieldtxtcol.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                                    //}
                                    if (i == 1)
                                    {
                                        bFieldtxtcol.HeaderText = "S.No";
                                        bFieldtxtcol.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                                    }
                                    if (i == 5)
                                    {
                                        bFieldtxtcol.HeaderText = "CallerID/EmailID";
                                        bFieldtxtcol.ItemStyle.HorizontalAlign = HorizontalAlign.Center;

                                    }

                                    ++i;
                                   // if (i == 1 || i == 2 || i == 3 || i == 4)
                                    if (i == 1 || i == 3 || i == 4 || i == 5 || i == 7 || i == 8) 
                                        bFieldtxtcol.Visible = false;
                                  //  if (i != 1 && i != 2 && i != 3 && i != 4 && i != 5)
                                    if (i != 1 && i != 2 && i != 3 && i != 4 && i != 5 && i != 6 && i != 7 && i != 8)
                                    {
                                        DataColumn col1 = new DataColumn();
                                      //  col1.Caption = Convert.ToString(ans_opt[i - 6]);
                                        col1.Caption = Convert.ToString(ans_opt[i - 9]);

                                        bFieldtxtcol.HeaderText = col1.Caption;                                        
                                        bFieldtxtcol.ItemStyle.HorizontalAlign = HorizontalAlign.Left;

                                    }
                                    grdResponses.Columns.Add(bFieldtxtcol);

                                }
                            }
                            if (ds_responselist.Tables[0].Rows.Count > 0)
                            {
                                grdResponses.DataSource = ds_responselist.Tables[0];
                                grdResponses.DataBind();
                            }
                        }
                    }
                }
                else
                {
                    if (flag == 2)
                        ds = cor.GetTextResponses(quesID, 1, SelectedSurvey.SURVEY_ID);
                    else
                        ds = cor.GetTextResponses(quesID, 0, SelectedSurvey.SURVEY_ID);

                    ds.Tables[0].Columns.Add(new DataColumn("ResponseDateTime", typeof(string)));

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Columns.Count != null && ds.Tables[0].Columns.Count > 0)
                        {
                            if (!IsPostBack)
                            {
                                int seq = 0;
                              //  ds.Tables[0].Columns.Add(new DataColumn("ResponseDateTime", typeof(string)));
                                foreach (DataColumn col in ds.Tables[0].Columns)
                                {                                    
                                    BoundField bFieldtxtcol = new BoundField();
                                    //if (seq == 0)
                                    //{
                                    //    bFieldtxtcol.HeaderText = "S.No";
                                    //    bFieldtxtcol.HeaderStyle.Width = Unit.Percentage(7);
                                    //    bFieldtxtcol.ItemStyle.Width = Unit.Percentage(7);
                                    //}
                                    //else
                                    //    bFieldtxtcol.HeaderText = "Answers";

                                    //bFieldtxtcol.DataField = col.ColumnName;
                                    //grdResponses.Columns.Add(bFieldtxtcol);
                                    if (seq == 4)
                                    {
                                        bFieldtxtcol.HeaderText = "Date&Time";

                                        for (i = 0; i < ds.Tables[0].Rows.Count; i++)
                                        {
                                            bFieldtxtcol.DataField = col.ColumnName;

                                            if (ds.Tables[0].Rows[i][bFieldtxtcol.DataField].ToString() != "")
                                            {

                                                DateTime dt1 = Convert.ToDateTime(ds.Tables[0].Rows[i][bFieldtxtcol.DataField]);
                                                DateTime dt2 = BuildQuetions.GetConvertedDatetime(dt1, userDetails.StandardBIAS, SurveyBasicInfoView.TIME_ZONE, true);

                                               // bFieldtxtcol.DataField = Convert.ToString(dt2);

                                                bFieldtxtcol.DataField = dt2.ToString("dd-MMM-yyyy hh:mm:ss tt");

                                                ds.Tables[0].Rows[i]["ResponseDateTime"] = bFieldtxtcol.DataField;

                                            }
                                            
                                        }
                                        
                                    }
                                  
                                  
                                    seq += 1;
                                }
                            }
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                grdResponses.DataSource = ds.Tables[0];
                                grdResponses.DataBind();                                
                            }

                        }
                    }
                }
            }
        }
    }

    protected void cmdBack_Click(object sender, EventArgs e)
    {
        string navReports = "";

        if (ht.Contains(Constants.SURVEYID))
            surveyID = Convert.ToInt32(ht[Constants.SURVEYID]);

        if (ht != null && ht.Count > 0 && ht.Contains("Veiw"))
        {
            string ViewAllQues = Convert.ToString(ht["Veiw"]);
            if (ViewAllQues == "All")
                navReports += "Veiw=All&" + Constants.SURVEYID + "=" + surveyID;
        }
        else if (quesID > 0)
        {
            navReports += "QuesID=" + quesID + "&" + Constants.SURVEYID + "=" + surveyID;
        }

        if (ht != null && ht.Count > 0 && ht.Contains("Mode"))
        {
            int Mode = Convert.ToInt32(ht["Mode"]);
            navReports += "&Mode=" + Mode;
        }
        if (ht != null && ht.Count > 0 && ht.Contains("Date_Start"))
        {
            navReports += "&Date_Start=" + ht["Date_Start"];
        }
        if (ht != null && ht.Count > 0 && ht.Contains("Date_Start"))
        {           
            navReports += "&Date_End=" + ht["Date_End"];
        }
        string urlstr = EncryptHelper.EncryptQuerystring("Reports.aspx", navReports+"&surveyFlag="+SurveyFlag);
        Response.Redirect(urlstr);
    }
    protected void grdResponses_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
           

        }
    }
    protected void btndownloadfiles_Click(object sender, EventArgs e)
    {
        string strQuesSeq = "";
        foreach (SurveyQuestion quesdownload in SelectedSurvey.surveyQues)
        {
            if (quesdownload.QUESTION_ID == quesID)
            {
                strQuesSeq = Convert.ToString(quesdownload.QUESTION_SEQ);
            }
        }

        string surveyName = SelectedSurvey.SURVEY_NAME.Replace(" ", string.Empty);
        string strdownloadtime = surveyName + "_Q" + strQuesSeq + "_" + DateTime.Now.ToString("ddMMMyyyyhhmmsstt");

        string downloadaudiofilepath = ConfigurationManager.AppSettings["downloadaudiofilepath"].ToString();

        string desktoppath = downloadaudiofilepath + "InsightoVoice/";

        if (!Directory.Exists(desktoppath))
        {
            Directory.CreateDirectory(desktoppath);
        }

        DataSet dsdownloadtoclientlocation = ds;       
        
        if (dsdownloadtoclientlocation.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsdownloadtoclientlocation.Tables[0].Rows.Count; i++)
            {
                string _url = dsdownloadtoclientlocation.Tables[0].Rows[i][2].ToString();

                string callerID = dsdownloadtoclientlocation.Tables[0].Rows[i][1].ToString();               

                string strdatetimestamp = Convert.ToDateTime(dsdownloadtoclientlocation.Tables[0].Rows[i][4]).ToString("ddMMMyyyyhhmmsstt");


                string localfilename = callerID + "_" + strdatetimestamp + ".mp3";
                //  string filename = _url.Substring(_url.LastIndexOf("/") + 1);


                // string _saveas = _url + filename;



                string _saveas = desktoppath + localfilename;


                WebClient _webclient = new WebClient();
                _webclient.DownloadFile(_url, _saveas);

               // File.Move(@"C:\MyFile.txt", @"C:\MyNewerFile.txt");
                AddFileToZip(desktoppath + strdownloadtime + ".zip", _saveas);

                File.Delete(_saveas);


            }

            //AddFileToZip(desktoppath + "Output.zip", @"C:\Windows\Notepad.exe");
            //AddFileToZip(desktoppath + "Output.zip", @"C:\Windows\System32\Calc.exe"); 
            
            string zippath =  desktoppath + strdownloadtime  + ".zip";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" +  strdownloadtime  + ".zip");
            Response.TransmitFile(zippath);
            Response.End();
           // Page.ClientScript.RegisterStartupScript(this.GetType(), @"CloseProgressbar", @"HideProgress();", true);
        }
    }

    private const long BUFFER_SIZE = 4096;

    private static void AddFileToZip(string zipFilename, string fileToAdd)
    {
        using (Package zip = System.IO.Packaging.Package.Open(zipFilename, FileMode.OpenOrCreate))
        {
            string destFilename = ".\\" + Path.GetFileName(fileToAdd);
            Uri uri = PackUriHelper.CreatePartUri(new Uri(destFilename, UriKind.Relative));
            if (zip.PartExists(uri))
            {
                zip.DeletePart(uri);
            }
            PackagePart part = zip.CreatePart(uri, "", CompressionOption.Normal);
            using (FileStream fileStream = new FileStream(fileToAdd, FileMode.Open, FileAccess.Read))
            {
                using (Stream dest = part.GetStream())
                {
                    CopyStream(fileStream, dest);
                }
            }
        }
    }

    private static void CopyStream(System.IO.FileStream inputStream, System.IO.Stream outputStream)
    {
        long bufferSize = inputStream.Length < BUFFER_SIZE ? inputStream.Length : BUFFER_SIZE;
        byte[] buffer = new byte[bufferSize];
        int bytesRead = 0;
        long bytesWritten = 0;
        while ((bytesRead = inputStream.Read(buffer, 0, buffer.Length)) != 0)
        {
            outputStream.Write(buffer, 0, bytesRead);
            bytesWritten += bufferSize;
        }
    } 

}