﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ModelMaster.master" AutoEventWireup="true" CodeFile="EmailPreview.aspx.cs" Inherits="EmailPreview" meta:resourcekey="PageResource1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div class="popupHeading" style="background-color:#F4F4F4">
        <div class="popupTitle" style="background-color:#F4F4F4">

            <h3>
                <asp:Label ID="lblTitle" runat="server" Text="Preview of email invitation" style="color:#B765C8; font-family:Arial,sans-serif; font-weight:normal"
                    meta:resourcekey="lblTitleResource1"></asp:Label>
                </h3>
        </div>
     <%--   <div class="popupClosePanel">
            <a href="#" class="popupCloseLink" onclick="closeModalSelf(false,'');" title="Close">
                &nbsp;</a>
        </div>--%>
    </div>
    <div class="popupContentPanel" style="background-color:#F4F4F4">
        <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div>
                <asp:Label ID="lblSuccMsg" runat="server" 
                    meta:resourcekey="lblSuccMsgResource1" /></div>
        </div>
        <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div><asp:Label ID="lblErrMsg" runat="server" CssClass="errorMesg" 
                    meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <div class="formPanel">
           <%-- <asp:Literal ID="litEmailContent" runat="server"></asp:Literal>--%>
           <div id="divEmailContent" runat="server" style=" padding-top:40px" >
           </div>
        </div>
    </div>
</asp:Content>

