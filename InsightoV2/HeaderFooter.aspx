﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Survey.master" AutoEventWireup="true"
    CodeFile="HeaderFooter.aspx.cs" Inherits="HeaderFooter"%>

<%@ Register Src="~/UserControls/SurveyPreviewLinkControl.ascx" TagPrefix="uc" TagName="SurveyPreview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SurveyMainContent" runat="Server">
    <script type="text/javascript" src="Scripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            // General options
            mode: "specific_textareas",
            editor_selector: "mceEditor",
            theme: "advanced",

            plugins: "paste",

            // Theme options
            theme_advanced_buttons1: "bold,italic,underline,fontselect,fontsizeselect,forecolor,pasteword",
            theme_advanced_buttons2: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            //theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing: false,

            // Example content CSS (should be your site CSS)
            content_css: "css/content.css",
            oninit: "postInitWork",
            // Replace values for the template plugin
            template_replace_values: {
                username: "Some User",
                staffid: "991234"
            }
        });

        function postInitWork() {

            var val = document.getElementById("<%=EditorHeaderParams.ClientID%>").value;
            var sty_val = val.split("_");
            if (sty_val.length == 3) {
                tinyMCE.getInstanceById('<%=EditorHeader.ClientID%>').getWin().document.body.style.fontFamily = sty_val[0];
                tinyMCE.getInstanceById('<%=EditorHeader.ClientID%>').getWin().document.body.style.fontSize = sty_val[1];
                tinyMCE.getInstanceById('<%=EditorHeader.ClientID%>').getWin().document.body.style.color = sty_val[2];

                tinyMCE.getInstanceById('<%=EditorFooter.ClientID%>').getWin().document.body.style.fontFamily = sty_val[0];
                tinyMCE.getInstanceById('<%=EditorFooter.ClientID%>').getWin().document.body.style.fontSize = sty_val[1];
                tinyMCE.getInstanceById('<%=EditorFooter.ClientID%>').getWin().document.body.style.color = sty_val[2];
            }

        }

        function cleartext() {
            tinyMCE.getInstanceById('<%= EditorHeader.ClientID %>').setContent('');
            return false;
        }

        function resetText() {
            tinyMCE.getInstanceById('<%= EditorHeader.ClientID %>').setContent('');

        }

    </script>
    <div class="surveyQuestionPanel">
         <div class="successPanel" id="dvSuccessMsg" runat="server" visible="false">
            <div><asp:Label ID="lblSuccMsg" runat="server" meta:resourcekey="lblSuccMsgResource1" ></asp:Label></div>
        </div>
         <div class="errorMessagePanel" id="dvErrMsg" runat="server" visible="false">
            <div><asp:Label ID="lblErrMsg" runat="server" meta:resourcekey="lblErrMsgResource1"></asp:Label></div>
        </div>
        <!-- survey question panel -->
        <div class="surveyQuestionHeader">
            <div class="surveyQuestionTitle">
              <asp:Label ID="lblHeaderAndFooter" Text="Header and Footer" runat="server" 
                    meta:resourcekey="lblHeaderAndFooterResource1"></asp:Label> </div>
            <div class="previewPanel">
                 <uc:SurveyPreview ID="ucSurveyPreview" runat="server" />
            </div>
        </div>
        <div class="surveyQuestionTabPanel">
        <div class="informationPanelDefault" runat="server" id ="dvInformationPanelDefault">
            <div>
              <asp:Label ID="lblInfromationpanel" runat="server" meta:resourcekey="lblInfromationpanel"></asp:Label> <%= SurveyMode %> <asp:Label ID="lblInfromationpanel1" runat="server" meta:resourcekey="lblInfromationpanel1"></asp:Label>.
            </div>
        </div>
            <!-- survey question tabs -->
           
                <p><asp:Label ID="lblHeader" runat="server" meta:resourcekey="lblHeader"> </asp:Label></p>
                <div id="div_header" runat="server" visible="false" style="color: Red">
                  <asp:Label ID="lblHeaderPaidFeature" runat="server" meta:resourcekey="lblHeaderPaidFeature"></asp:Label></div>
               
                    <input type="hidden" runat="server" id="EditorHeaderParams" value="0" />
                    <textarea id="EditorHeader" runat="server" cols="10" rows="8" class="mceEditor" style="width: 643px;
                        height: 150px;"></textarea>
                    <br />
                    <br />
                   
                        <p><asp:Label ID="lblFooter" runat="server" meta:resourcekey="lblFooter"></asp:Label></p>
                        <div id="div_footer" runat="server" visible="false" style="color: Red">
                          <asp:Label ID="lblFooterPaidFeature" runat="server" meta:resourcekey="lblFooterPaidFeature"></asp:Label></div>
                    
                    <textarea id="EditorFooter" runat="server" cols="10" rows="8" class="mceEditor" style="width: 643px;
                        height: 150px;"></textarea>
                    <!-- //survey question tabs -->
        </div>
        <div class="clear">
        </div>
        <div class="bottomButtonPanel">
            <asp:Button ID="btnSave" runat="server" Text="Save" ToolTip="Save" OnClientClick="tinyMCE.triggerSave(false,true);"
                CssClass="dynamicButton" OnClick="btnSave_Click" 
                meta:resourcekey="btnSaveResource1" />
        </div>
        <!-- //survey question panel -->
    </div>
</asp:Content>
