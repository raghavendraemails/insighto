﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Utilities;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;
using System.Configuration;
using System.Net.Mail;
using SendGrid;
using SendGrid.SmtpApi;
public partial class SendGridTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TestSend();
    }

    public void TestSend()
    {
        string emailcontent = "<p>TEST THIS <b>EMAIL</b> " + DateTime.Now.ToString() + "</p>";
        string emailsubject = "TEST THIS SUBJECT";
        try
        {
            SendGridMessage mailmessage = new SendGridMessage();
            mailmessage.From = new MailAddress("satish@knowience.com", "Dev Team");
            mailmessage.Subject = emailsubject;
            mailmessage.Html = (emailcontent ?? "");
            List<String> recipients = new List<String>
            {
                @"Satish Kumar <satish@knowience.com>",
                @"Satish Putcha <psatishkumar@hotmail.com>"
            };
            mailmessage.AddTo(recipients);
            var creds = new System.Net.NetworkCredential("satishputcha", "S@tish1234");
            // Create an Web transport for sending email.
            var transportWeb = new Web(creds);

            // Send the email.
            transportWeb.Deliver(mailmessage);
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}