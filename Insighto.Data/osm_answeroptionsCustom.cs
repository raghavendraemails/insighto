﻿namespace Insighto.Data
{
    using System;
    using System.Collections.Generic;

    public partial class osm_answeroptions
    {
        public string AnswerOptionUniqueId { get; set; }
        public string SequenceId { get; set; }

    }
}