﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Insighto.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Objects;
    
    public partial class InsightoV2Entities : DbContext
    {
        public InsightoV2Entities()
            : base("name=InsightoV2Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<oms_config> oms_config { get; set; }
        public DbSet<osm_adminreports> osm_adminreports { get; set; }
        public DbSet<osm_admintracking> osm_admintracking { get; set; }
        public DbSet<osm_adminuser> osm_adminuser { get; set; }
        public DbSet<osm_alertmanagement> osm_alertmanagement { get; set; }
        public DbSet<osm_answeroptions> osm_answeroptions { get; set; }
        public DbSet<osm_blockedwords> osm_blockedwords { get; set; }
        public DbSet<osm_cl_folders> osm_cl_folders { get; set; }
        public DbSet<osm_contactlist> osm_contactlist { get; set; }
        public DbSet<osm_contactusdetails> osm_contactusdetails { get; set; }
        public DbSet<osm_crossreports> osm_crossreports { get; set; }
        public DbSet<osm_emaildet> osm_emaildet { get; set; }
        public DbSet<osm_emaillist> osm_emaillist { get; set; }
        public DbSet<osm_featurelist> osm_featurelist { get; set; }
        public DbSet<osm_features> osm_features { get; set; }
        public DbSet<osm_images> osm_images { get; set; }
        public DbSet<osm_logininfo> osm_logininfo { get; set; }
        public DbSet<osm_picklist> osm_picklist { get; set; }
        public DbSet<osm_questiontypes> osm_questiontypes { get; set; }
        public DbSet<osm_responsequestions> osm_responsequestions { get; set; }
        public DbSet<osm_responsesurvey> osm_responsesurvey { get; set; }
        public DbSet<osm_sessionmgmt> osm_sessionmgmt { get; set; }
        public DbSet<osm_survey> osm_survey { get; set; }
        public DbSet<osm_surveyblockwords> osm_surveyblockwords { get; set; }
        public DbSet<osm_surveyIntro> osm_surveyIntro { get; set; }
        public DbSet<osm_surveyquestion> osm_surveyquestion { get; set; }
        public DbSet<osm_surveyreminder> osm_surveyreminder { get; set; }
        public DbSet<osm_surveyreports> osm_surveyreports { get; set; }
        public DbSet<osm_unsubscribelist> osm_unsubscribelist { get; set; }
        public DbSet<osm_useraccounthistory> osm_useraccounthistory { get; set; }
        public DbSet<osm_userlinkaccount> osm_userlinkaccount { get; set; }
        public DbSet<osm_widget> osm_widget { get; set; }
        public DbSet<osm_WidgetHistory> osm_WidgetHistory { get; set; }
        public DbSet<osm_WidgetSettings> osm_WidgetSettings { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<UserAccountSettingView> UserAccountSettingViews { get; set; }
        public DbSet<osm_useralternateemails> osm_useralternateemails { get; set; }
        public DbSet<osm_themes> osm_themes { get; set; }
        public DbSet<ResponseCountView> ResponseCountViews { get; set; }
        public DbSet<SurveyManagerView> SurveyManagerViews { get; set; }
        public DbSet<osm_surveycontrols_log> osm_surveycontrols_log { get; set; }
        public DbSet<osm_user> osm_user { get; set; }
        public DbSet<osm_roles> osm_roles { get; set; }
        public DbSet<osm_surveycontrols> osm_surveycontrols { get; set; }
        public DbSet<osm_HelpPage> osm_HelpPage { get; set; }
        public DbSet<osm_progressbar> osm_progressbar { get; set; }
        public DbSet<osm_surveysetting> osm_surveysetting { get; set; }
        public DbSet<osm_userpaymentinfo> osm_userpaymentinfo { get; set; }
        public DbSet<osm_surveylaunch> osm_surveylaunch { get; set; }
        public DbSet<SurveyBasicInfoView> SurveyBasicInfoViews { get; set; }
        public DbSet<osm_orderinfo> osm_orderinfo { get; set; }
        public DbSet<AnswerOptionsAuditInfo> AnswerOptionsAuditInfo { get; set; }
        public DbSet<MigrationAuditInfo> MigrationAuditInfo { get; set; }
        public DbSet<QuestionsAuditInfo> QuestionsAuditInfo { get; set; }
        public DbSet<ResponseQuestionsAuditInfo> ResponseQuestionsAuditInfo { get; set; }
        public DbSet<ResponsesAuditInfo> ResponsesAuditInfo { get; set; }
        public DbSet<osm_bouncedmailList> osm_bouncedmailList { get; set; }
        public DbSet<osm_faqpages> osm_faqpages { get; set; }
        public DbSet<SurveyDashboardView> SurveyDashboardView { get; set; }
        public DbSet<osm_BouncedMails> osm_BouncedMails { get; set; }
        public DbSet<osm_accountsetting> osm_accountsetting { get; set; }
        public DbSet<osm_countriespricing> osm_countriespricing { get; set; }
    
        public virtual ObjectResult<UserAccountInfo> sp_GetUserAccount(string eMAIL, string fIRST_NAME, string lAST_NAME, string cITY, string sTATE, string cOMPANY, string wORK_INDUSTRY, string jOB_FUNCTION, Nullable<System.DateTime> cREATED_ON, string lICENSE_TYPE, string sTATUS, ObjectParameter returncode, ObjectParameter returnmessage)
        {
            ((IObjectContextAdapter)this).ObjectContext.MetadataWorkspace.LoadFromAssembly(typeof(UserAccountInfo).Assembly);
    
            var eMAILParameter = eMAIL != null ?
                new ObjectParameter("EMAIL", eMAIL) :
                new ObjectParameter("EMAIL", typeof(string));
    
            var fIRST_NAMEParameter = fIRST_NAME != null ?
                new ObjectParameter("FIRST_NAME", fIRST_NAME) :
                new ObjectParameter("FIRST_NAME", typeof(string));
    
            var lAST_NAMEParameter = lAST_NAME != null ?
                new ObjectParameter("LAST_NAME", lAST_NAME) :
                new ObjectParameter("LAST_NAME", typeof(string));
    
            var cITYParameter = cITY != null ?
                new ObjectParameter("CITY", cITY) :
                new ObjectParameter("CITY", typeof(string));
    
            var sTATEParameter = sTATE != null ?
                new ObjectParameter("STATE", sTATE) :
                new ObjectParameter("STATE", typeof(string));
    
            var cOMPANYParameter = cOMPANY != null ?
                new ObjectParameter("COMPANY", cOMPANY) :
                new ObjectParameter("COMPANY", typeof(string));
    
            var wORK_INDUSTRYParameter = wORK_INDUSTRY != null ?
                new ObjectParameter("WORK_INDUSTRY", wORK_INDUSTRY) :
                new ObjectParameter("WORK_INDUSTRY", typeof(string));
    
            var jOB_FUNCTIONParameter = jOB_FUNCTION != null ?
                new ObjectParameter("JOB_FUNCTION", jOB_FUNCTION) :
                new ObjectParameter("JOB_FUNCTION", typeof(string));
    
            var cREATED_ONParameter = cREATED_ON.HasValue ?
                new ObjectParameter("CREATED_ON", cREATED_ON) :
                new ObjectParameter("CREATED_ON", typeof(System.DateTime));
    
            var lICENSE_TYPEParameter = lICENSE_TYPE != null ?
                new ObjectParameter("LICENSE_TYPE", lICENSE_TYPE) :
                new ObjectParameter("LICENSE_TYPE", typeof(string));
    
            var sTATUSParameter = sTATUS != null ?
                new ObjectParameter("STATUS", sTATUS) :
                new ObjectParameter("STATUS", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<UserAccountInfo>("sp_GetUserAccount", eMAILParameter, fIRST_NAMEParameter, lAST_NAMEParameter, cITYParameter, sTATEParameter, cOMPANYParameter, wORK_INDUSTRYParameter, jOB_FUNCTIONParameter, cREATED_ONParameter, lICENSE_TYPEParameter, sTATUSParameter, returncode, returnmessage);
        }
    
        public virtual int sp_DeleteSurveyBySurveyId(Nullable<int> surveyID, ObjectParameter returnvalue)
        {
            var surveyIDParameter = surveyID.HasValue ?
                new ObjectParameter("SurveyID", surveyID) :
                new ObjectParameter("SurveyID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_DeleteSurveyBySurveyId", surveyIDParameter, returnvalue);
        }
    
        public virtual ObjectResult<SurveyDashboardInfo> sp_SurveyDashboard(Nullable<int> uSERID, string sTATUS, Nullable<int> surveyWidgetFlag, Nullable<int> tOP)
        {
            ((IObjectContextAdapter)this).ObjectContext.MetadataWorkspace.LoadFromAssembly(typeof(SurveyDashboardInfo).Assembly);
    
            var uSERIDParameter = uSERID.HasValue ?
                new ObjectParameter("USERID", uSERID) :
                new ObjectParameter("USERID", typeof(int));
    
            var sTATUSParameter = sTATUS != null ?
                new ObjectParameter("STATUS", sTATUS) :
                new ObjectParameter("STATUS", typeof(string));
    
            var surveyWidgetFlagParameter = surveyWidgetFlag.HasValue ?
                new ObjectParameter("SurveyWidgetFlag", surveyWidgetFlag) :
                new ObjectParameter("SurveyWidgetFlag", typeof(int));
    
            var tOPParameter = tOP.HasValue ?
                new ObjectParameter("TOP", tOP) :
                new ObjectParameter("TOP", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SurveyDashboardInfo>("sp_SurveyDashboard", uSERIDParameter, sTATUSParameter, surveyWidgetFlagParameter, tOPParameter);
        }
    
        public virtual int sp_DeleteQuestion(Nullable<int> qUESTION_ID)
        {
            var qUESTION_IDParameter = qUESTION_ID.HasValue ?
                new ObjectParameter("QUESTION_ID", qUESTION_ID) :
                new ObjectParameter("QUESTION_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_DeleteQuestion", qUESTION_IDParameter);
        }
    
        public virtual ObjectResult<ResponseQuestionsInfo> GETRESPONSEQUESTIONS(Nullable<int> surveyId)
        {
            ((IObjectContextAdapter)this).ObjectContext.MetadataWorkspace.LoadFromAssembly(typeof(ResponseQuestionsInfo).Assembly);
    
            var surveyIdParameter = surveyId.HasValue ?
                new ObjectParameter("SurveyId", surveyId) :
                new ObjectParameter("SurveyId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<ResponseQuestionsInfo>("GETRESPONSEQUESTIONS", surveyIdParameter);
        }
    
        public virtual ObjectResult<osm_user> GetUserAdvanceSearchDetails(string firstName, string lastName, string city, string state, string company, string satus, string licenseType, string jobFunction, string workIndustry, string companySize, string emailId, string createDateFrom, string createDateTo, string licenseExpiryDateFrom, string licenseExpiryDateTo, string customerId, Nullable<int> licenseExpiryPeriod)
        {
            ((IObjectContextAdapter)this).ObjectContext.MetadataWorkspace.LoadFromAssembly(typeof(osm_user).Assembly);
    
            var firstNameParameter = firstName != null ?
                new ObjectParameter("FirstName", firstName) :
                new ObjectParameter("FirstName", typeof(string));
    
            var lastNameParameter = lastName != null ?
                new ObjectParameter("LastName", lastName) :
                new ObjectParameter("LastName", typeof(string));
    
            var cityParameter = city != null ?
                new ObjectParameter("City", city) :
                new ObjectParameter("City", typeof(string));
    
            var stateParameter = state != null ?
                new ObjectParameter("State", state) :
                new ObjectParameter("State", typeof(string));
    
            var companyParameter = company != null ?
                new ObjectParameter("Company", company) :
                new ObjectParameter("Company", typeof(string));
    
            var satusParameter = satus != null ?
                new ObjectParameter("Satus", satus) :
                new ObjectParameter("Satus", typeof(string));
    
            var licenseTypeParameter = licenseType != null ?
                new ObjectParameter("LicenseType", licenseType) :
                new ObjectParameter("LicenseType", typeof(string));
    
            var jobFunctionParameter = jobFunction != null ?
                new ObjectParameter("JobFunction", jobFunction) :
                new ObjectParameter("JobFunction", typeof(string));
    
            var workIndustryParameter = workIndustry != null ?
                new ObjectParameter("WorkIndustry", workIndustry) :
                new ObjectParameter("WorkIndustry", typeof(string));
    
            var companySizeParameter = companySize != null ?
                new ObjectParameter("CompanySize", companySize) :
                new ObjectParameter("CompanySize", typeof(string));
    
            var emailIdParameter = emailId != null ?
                new ObjectParameter("EmailId", emailId) :
                new ObjectParameter("EmailId", typeof(string));
    
            var createDateFromParameter = createDateFrom != null ?
                new ObjectParameter("CreateDateFrom", createDateFrom) :
                new ObjectParameter("CreateDateFrom", typeof(string));
    
            var createDateToParameter = createDateTo != null ?
                new ObjectParameter("CreateDateTo", createDateTo) :
                new ObjectParameter("CreateDateTo", typeof(string));
    
            var licenseExpiryDateFromParameter = licenseExpiryDateFrom != null ?
                new ObjectParameter("LicenseExpiryDateFrom", licenseExpiryDateFrom) :
                new ObjectParameter("LicenseExpiryDateFrom", typeof(string));
    
            var licenseExpiryDateToParameter = licenseExpiryDateTo != null ?
                new ObjectParameter("LicenseExpiryDateTo", licenseExpiryDateTo) :
                new ObjectParameter("LicenseExpiryDateTo", typeof(string));
    
            var customerIdParameter = customerId != null ?
                new ObjectParameter("CustomerId", customerId) :
                new ObjectParameter("CustomerId", typeof(string));
    
            var licenseExpiryPeriodParameter = licenseExpiryPeriod.HasValue ?
                new ObjectParameter("LicenseExpiryPeriod", licenseExpiryPeriod) :
                new ObjectParameter("LicenseExpiryPeriod", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<osm_user>("GetUserAdvanceSearchDetails", firstNameParameter, lastNameParameter, cityParameter, stateParameter, companyParameter, satusParameter, licenseTypeParameter, jobFunctionParameter, workIndustryParameter, companySizeParameter, emailIdParameter, createDateFromParameter, createDateToParameter, licenseExpiryDateFromParameter, licenseExpiryDateToParameter, customerIdParameter, licenseExpiryPeriodParameter);
        }
    
        public virtual ObjectResult<osm_user> GetUserAdvanceSearchDetails(string firstName, string lastName, string city, string state, string company, string satus, string licenseType, string jobFunction, string workIndustry, string companySize, string emailId, string createDateFrom, string createDateTo, string licenseExpiryDateFrom, string licenseExpiryDateTo, string customerId, Nullable<int> licenseExpiryPeriod, MergeOption mergeOption)
        {
            ((IObjectContextAdapter)this).ObjectContext.MetadataWorkspace.LoadFromAssembly(typeof(osm_user).Assembly);
    
            var firstNameParameter = firstName != null ?
                new ObjectParameter("FirstName", firstName) :
                new ObjectParameter("FirstName", typeof(string));
    
            var lastNameParameter = lastName != null ?
                new ObjectParameter("LastName", lastName) :
                new ObjectParameter("LastName", typeof(string));
    
            var cityParameter = city != null ?
                new ObjectParameter("City", city) :
                new ObjectParameter("City", typeof(string));
    
            var stateParameter = state != null ?
                new ObjectParameter("State", state) :
                new ObjectParameter("State", typeof(string));
    
            var companyParameter = company != null ?
                new ObjectParameter("Company", company) :
                new ObjectParameter("Company", typeof(string));
    
            var satusParameter = satus != null ?
                new ObjectParameter("Satus", satus) :
                new ObjectParameter("Satus", typeof(string));
    
            var licenseTypeParameter = licenseType != null ?
                new ObjectParameter("LicenseType", licenseType) :
                new ObjectParameter("LicenseType", typeof(string));
    
            var jobFunctionParameter = jobFunction != null ?
                new ObjectParameter("JobFunction", jobFunction) :
                new ObjectParameter("JobFunction", typeof(string));
    
            var workIndustryParameter = workIndustry != null ?
                new ObjectParameter("WorkIndustry", workIndustry) :
                new ObjectParameter("WorkIndustry", typeof(string));
    
            var companySizeParameter = companySize != null ?
                new ObjectParameter("CompanySize", companySize) :
                new ObjectParameter("CompanySize", typeof(string));
    
            var emailIdParameter = emailId != null ?
                new ObjectParameter("EmailId", emailId) :
                new ObjectParameter("EmailId", typeof(string));
    
            var createDateFromParameter = createDateFrom != null ?
                new ObjectParameter("CreateDateFrom", createDateFrom) :
                new ObjectParameter("CreateDateFrom", typeof(string));
    
            var createDateToParameter = createDateTo != null ?
                new ObjectParameter("CreateDateTo", createDateTo) :
                new ObjectParameter("CreateDateTo", typeof(string));
    
            var licenseExpiryDateFromParameter = licenseExpiryDateFrom != null ?
                new ObjectParameter("LicenseExpiryDateFrom", licenseExpiryDateFrom) :
                new ObjectParameter("LicenseExpiryDateFrom", typeof(string));
    
            var licenseExpiryDateToParameter = licenseExpiryDateTo != null ?
                new ObjectParameter("LicenseExpiryDateTo", licenseExpiryDateTo) :
                new ObjectParameter("LicenseExpiryDateTo", typeof(string));
    
            var customerIdParameter = customerId != null ?
                new ObjectParameter("CustomerId", customerId) :
                new ObjectParameter("CustomerId", typeof(string));
    
            var licenseExpiryPeriodParameter = licenseExpiryPeriod.HasValue ?
                new ObjectParameter("LicenseExpiryPeriod", licenseExpiryPeriod) :
                new ObjectParameter("LicenseExpiryPeriod", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<osm_user>("GetUserAdvanceSearchDetails", mergeOption, firstNameParameter, lastNameParameter, cityParameter, stateParameter, companyParameter, satusParameter, licenseTypeParameter, jobFunctionParameter, workIndustryParameter, companySizeParameter, emailIdParameter, createDateFromParameter, createDateToParameter, licenseExpiryDateFromParameter, licenseExpiryDateToParameter, customerIdParameter, licenseExpiryPeriodParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> sp_SurveySendReminderMail(Nullable<int> launchId, string fromAddress, string mailSubject, string emailInvitation, Nullable<System.DateTime> createdOn, string senderName, string replytoemailaddress, Nullable<int> surveyId, Nullable<int> reminderID)
        {
            var launchIdParameter = launchId.HasValue ?
                new ObjectParameter("LaunchId", launchId) :
                new ObjectParameter("LaunchId", typeof(int));
    
            var fromAddressParameter = fromAddress != null ?
                new ObjectParameter("FromAddress", fromAddress) :
                new ObjectParameter("FromAddress", typeof(string));
    
            var mailSubjectParameter = mailSubject != null ?
                new ObjectParameter("MailSubject", mailSubject) :
                new ObjectParameter("MailSubject", typeof(string));
    
            var emailInvitationParameter = emailInvitation != null ?
                new ObjectParameter("EmailInvitation", emailInvitation) :
                new ObjectParameter("EmailInvitation", typeof(string));
    
            var createdOnParameter = createdOn.HasValue ?
                new ObjectParameter("CreatedOn", createdOn) :
                new ObjectParameter("CreatedOn", typeof(System.DateTime));
    
            var senderNameParameter = senderName != null ?
                new ObjectParameter("SenderName", senderName) :
                new ObjectParameter("SenderName", typeof(string));
    
            var replytoemailaddressParameter = replytoemailaddress != null ?
                new ObjectParameter("Replytoemailaddress", replytoemailaddress) :
                new ObjectParameter("Replytoemailaddress", typeof(string));
    
            var surveyIdParameter = surveyId.HasValue ?
                new ObjectParameter("SurveyId", surveyId) :
                new ObjectParameter("SurveyId", typeof(int));
    
            var reminderIDParameter = reminderID.HasValue ?
                new ObjectParameter("ReminderID", reminderID) :
                new ObjectParameter("ReminderID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("sp_SurveySendReminderMail", launchIdParameter, fromAddressParameter, mailSubjectParameter, emailInvitationParameter, createdOnParameter, senderNameParameter, replytoemailaddressParameter, surveyIdParameter, reminderIDParameter);
        }
    
        public virtual int RestoreDeletedSurveyBySurveyId(Nullable<int> surveyID, ObjectParameter returnvalue)
        {
            var surveyIDParameter = surveyID.HasValue ?
                new ObjectParameter("SurveyID", surveyID) :
                new ObjectParameter("SurveyID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("RestoreDeletedSurveyBySurveyId", surveyIDParameter, returnvalue);
        }
    }
}
