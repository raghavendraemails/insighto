﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
namespace DataMigrationConsole
{
    public class SurveyAnswerOptions
    {
        static List<kn.osm_answeroptions> optionsFromV1 = new List<kn.osm_answeroptions>();
        static List<v2.osm_answeroptions> optionsFromV2 = new List<v2.osm_answeroptions>();
        static List<v2.osm_answeroptions> cleanedOptionsForV2 = new List<v2.osm_answeroptions>();

        /// <summary>
        /// Gets the questions from v1.
        /// </summary>
        public static List<kn.osm_answeroptions> AnswersFromV1
        {
            get
            {
                if (optionsFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        var v1QuestionIds = Surveys.CurrentSurveyInfo.QuestionsInfo.Select(q => q.V1QuestionId).ToList();
                        optionsFromV1 = (from v1s in knCon.osm_answeroptions
                                         where v1QuestionIds.Contains(v1s.QUESTION_ID)
                                         select v1s).ToList();
                    }
                }
                return optionsFromV1;
            }
            set
            {
                optionsFromV1 = value;
            }
        }

        /// <summary>
        /// Gets the questions from v2.
        /// </summary>
        public static List<v2.osm_answeroptions> AnswersFromV2
        {
            get
            {
                if (optionsFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        optionsFromV2 = v2Con.osm_answeroptions.ToList();
                    }
                }
                return optionsFromV2;
            }
        }


        public static List<v2.osm_answeroptions> CleanedOptionsForV2
        {
            get
            {
                if (cleanedOptionsForV2.Count == 0)
                {
                    GetCleanedAnswerOptionsList(AnswersFromV1);
                    return cleanedOptionsForV2;
                }
                return cleanedOptionsForV2;
            }
            set
            {
                cleanedOptionsForV2 = value;
            }
        }

        /// <summary>
        /// Gets the cleaned questions list.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static void GetCleanedAnswerOptionsList(List<kn.osm_answeroptions> source)
        {
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_answeroptions, v2.osm_answeroptions>(AnswersFromV1, cleanedOptionsForV2);

            //modify required feilds for ready to insert .. 1. folder id 2. userid.
            SetUserAndQuestionsIds(cleanedOptionsForV2);
        }

        /// <summary>
        /// Sets the user and questions ids.
        /// </summary>
        /// <param name="destinationQuestions">The destination questions.</param>
        private static void SetUserAndQuestionsIds(List<v2.osm_answeroptions> destinationAnswerOptions)
        {
            int ctr = 0;
            foreach (var item in destinationAnswerOptions)
            {
                Console.WriteLine("{0} ---- GetCleanedAnswerOptionsList(). ", ++ctr);
                var currentSurvey = Surveys.CurrentSurveyInfo.QuestionsInfo.FirstOrDefault(si => si.V1QuestionId == item.QUESTION_ID);
                item.QUESTION_ID = currentSurvey.V2QuestionId; //currentSurvey.V2SurveyId;

                if (currentSurvey.IsSkipEnabled)
                {
                    var skipQuestion = Surveys.CurrentSurveyInfo.QuestionsInfo.FirstOrDefault(si => si.V1QuestionId == item.SKIP_QUESTION_ID);
                    item.SKIP_QUESTION_ID = skipQuestion != null ? skipQuestion.V2QuestionId : item.SKIP_QUESTION_ID;
                }
            }
            //timer.Stop();
        }

        /// <summary>
        /// Inserts the question.
        /// </summary>
        /// <param name="v2Question">The v2 question.</param>
        /// <returns></returns>
        public static v2.osm_answeroptions InsertAnswerOption(v2.osm_answeroptions v2Option)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                v2Con.osm_answeroptions.Add(v2Option);
                v2Con.SaveChanges();
                return v2Option;
            }
        }

        internal static void ProcessAnswerOptions(int questionId)
        {
            //Insert survey and get new Id and assign to surveyInfo object
            var surveyQuestions = SurveyAnswerOptions.CleanedOptionsForV2.Where(intro => intro.QUESTION_ID == questionId).ToList();
            var lstOptionsInfo = Surveys.CurrentSurveyInfo.QuestionsInfo.FirstOrDefault(q => q.V2QuestionId == questionId);
            if (surveyQuestions.Count > 0)
            {
                foreach (var ao in surveyQuestions)
                {
                    var answerInfo = new AnswerInfo { V1AnswerId = ao.ANSWER_ID };
                    var insertedAnswerInfo = SurveyAnswerOptions.InsertAnswerOption(ao);
                    answerInfo.V2AnswerId = insertedAnswerInfo.ANSWER_ID;

                    Console.WriteLine("S.No: {0} ---- is question Id . ", insertedAnswerInfo.QUESTION_ID);
                    lstOptionsInfo.AnswersInfo.Add(answerInfo);
                } 
            }
            else
                Console.WriteLine(" ----- Answer option not inserted -----");

            // log into answer options audit table
            InsertIntoLog(questionId, lstOptionsInfo.AnswersInfo.Count);
        }

        private static void InsertIntoLog(int v2QuestionId, int answersCount)
        {
            using (v2.InsightoV2Entities dbCon = new v2.InsightoV2Entities())
            {
                var currentMigrationSurveyInfo = new v2.AnswerOptionsAuditInfo { V2QuestionId = v2QuestionId, NoOfAnswers = answersCount, CreatedOn = DateTime.UtcNow };
                dbCon.AnswerOptionsAuditInfo.Add(currentMigrationSurveyInfo);
                dbCon.SaveChanges();
            }
        }
    }
}
