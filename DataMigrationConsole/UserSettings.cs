﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;
using CovalenseUtilities.Services;
using Insighto.Business.Services;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace DataMigrationConsole
{
    public class UserSettings
    {
        public static void SetDefaultAccountSettings(int userId)
        {
            ServiceFactory.GetService<AccountSettingService>().SaveAccountSetting(userId);
        }

        public static void SetDefaultFolderSetiings(osm_user user, string applicationPath)
        {
            using (var dbCon = new InsightoV2Entities())
            {
                osm_cl_folders folder = new osm_cl_folders();
                folder.USERID = user.USERID;
                folder.FOLDER_NAME = "My Folder";
                dbCon.osm_cl_folders.Add(folder);
                dbCon.SaveChanges();
            }
            ServiceFactory.GetService<UsersService>().CreateUserFolders(user, applicationPath);
        }

        internal static void ProcessUserSettingsAndFolders()
        {
            using (var dbCon = new InsightoV2Entities())
            {
                int usersCount = 1;
                var applicationPath = ConfigurationManager.AppSettings["ExecutionPath"];

                var lstUsers = dbCon.osm_user.ToList().OrderByDescending(usr => usr.USERID).Take(520);
                foreach (var user in lstUsers)
                {
                    UserSettings.SetDefaultFolderSetiings(user, applicationPath);
                    UserSettings.SetDefaultAccountSettings(user.USERID);
                    Console.WriteLine("Inserted: {0} and UserId: {1}", usersCount, user.USERID);
                    usersCount++;
                }
                Console.ReadLine();
            }
        }

        internal static void SetUserDefaultFiles()
        {
            try
            {
                Console.WriteLine(" ----- SetUserDefaultFiles  ----- ");
                string basePath = Path.GetDirectoryName(Assembly.GetAssembly(typeof(Program)).CodeBase).Replace("\\bin\\Debug", "").Replace("file:\\", "");
                var users = Users.UsersFromV2;
                foreach (var item in users)
                {
                    if (item.USERID <= 0)
                        continue;

                    string sourcePath = Path.Combine(basePath, "DefaultFiles");
                    var applicationPath = ConfigurationManager.AppSettings["DestinationPath"];
                    DirectoryInfo dirInfo = new DirectoryInfo(Path.Combine(applicationPath, item.USERID.ToString()));
                    if (!dirInfo.Exists)
                        dirInfo.Create();

                    CommonMethods.ReadandWriteFile(Path.Combine(sourcePath, "sample.ppt"), Path.Combine(applicationPath, item.USERID.ToString(), "sample.ppt"));
                    CommonMethods.ReadandWriteFile(Path.Combine(sourcePath, "sample.png"), Path.Combine(applicationPath, item.USERID.ToString(), "sample.png"));
                    Console.WriteLine(" File Created for User: {0}", item.LOGIN_NAME);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            Console.WriteLine("-------------------------------------------------------------");
        }
    }
}
