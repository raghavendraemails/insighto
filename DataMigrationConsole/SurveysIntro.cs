﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
namespace DataMigrationConsole
{
    public class SurveyIntro
    {
        static List<kn.osm_surveyIntro> surveysFromV1 = new List<kn.osm_surveyIntro>();
        static List<v2.osm_surveyIntro> surveysFromV2 = new List<v2.osm_surveyIntro>();
        static List<v2.osm_surveyIntro> cleanedSurveyIntrosForV2 = new List<v2.osm_surveyIntro>();

        /// <summary>
        /// Gets the survey intros from v1.
        /// </summary>
        public static List<kn.osm_surveyIntro> SurveyIntrosFromV1
        {
            get
            {
                if (surveysFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        surveysFromV1 = (from v1s in knCon.osm_surveyIntro.ToList()
                                         where v1s.SURVEY_ID == Surveys.CurrentSurveyInfo.V1SuveyId
                                         select v1s).ToList();
                    }
                }
                return surveysFromV1;
            }
            set
            {
                surveysFromV1 = value;
            }
        }

        /// <summary>
        /// Gets the survey intros from v2.
        /// </summary>
        public static List<v2.osm_surveyIntro> SurveyIntrosFromV2
        {
            get
            {
                if (surveysFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        surveysFromV2 = v2Con.osm_surveyIntro.ToList();
                    }
                }
                return surveysFromV2;
            }
            set
            {
                surveysFromV2 = value;
            }
        }

        /// <summary>
        /// Gets the cleaned survey intros for v2.
        /// </summary>
        public static List<v2.osm_surveyIntro> CleanedSurveyIntrosForV2
        {
            get
            {
                if (cleanedSurveyIntrosForV2.Count == 0)
                {
                    GetCleanedSurveyIntroList(SurveyIntrosFromV1);
                    return cleanedSurveyIntrosForV2;
                }
                return cleanedSurveyIntrosForV2;
            }
            set
            {
                cleanedSurveyIntrosForV2 = value;
            }
        }

        /// <summary>
        /// Gets the cleaned survey intro list.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static void GetCleanedSurveyIntroList(List<kn.osm_surveyIntro> source)
        {
            List<v2.osm_surveyIntro> destinationSurveyIntros = new List<v2.osm_surveyIntro>();
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_surveyIntro, v2.osm_surveyIntro>(SurveyIntrosFromV1, destinationSurveyIntros);

            //modify required feilds for ready to insert .. 1. folder id 2. userid.
            SetUserAndSurveyIntroIds(destinationSurveyIntros);
            cleanedSurveyIntrosForV2 = destinationSurveyIntros;
        }

        /// <summary>
        /// Sets the user and survey intro ids.
        /// </summary>
        /// <param name="destinationSurveyIntros">The destination survey intros.</param>
        private static void SetUserAndSurveyIntroIds(List<v2.osm_surveyIntro> destinationSurveyIntros)
        {
            foreach (var item in destinationSurveyIntros)
            {
                var currentSurvey = Surveys.SurveyInfoList.FirstOrDefault(si => si.V1SuveyId == item.SURVEY_ID);
                item.USERID = currentSurvey.V2UserId;
                item.SURVEY_ID = currentSurvey.V2SurveyId;
            }
            //timer.Stop();
        }

        /// <summary>
        /// Inserts the survey intro.
        /// </summary>
        /// <param name="v2SurveyIntro">The v2 survey intro.</param>
        /// <returns></returns>
        public static v2.osm_surveyIntro InsertSurveyIntro(v2.osm_surveyIntro v2SurveyIntro)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                v2Con.osm_surveyIntro.Add(v2SurveyIntro);
                v2Con.SaveChanges();
                return v2SurveyIntro;
            }
        }

        /// <summary>
        /// Processes the survey introduction.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        public static void ProcessSurveyIntroduction(int surveyId)
        {
            //Insert survey and get new Id and assign to surveyInfo object
            var surveyIntro = SurveyIntro.CleanedSurveyIntrosForV2.FirstOrDefault(intro => intro.SURVEY_ID == surveyId);
            if (surveyIntro != null)
            {
                var insertedSurveyIntro = SurveyIntro.InsertSurveyIntro(surveyIntro);
                Console.WriteLine("S.No: {0} ---- Survey Introduction. ", 1);
            }
            else
                Console.WriteLine(" ----- Not inserted -----");
        }
    }
}
