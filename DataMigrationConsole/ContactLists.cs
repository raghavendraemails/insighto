﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
using System.ComponentModel;
namespace DataMigrationConsole
{
    public class ContactList
    {
        static List<kn.osm_contactlist> contactsFromV1 = new List<kn.osm_contactlist>();
        static List<v2.osm_contactlist> contactsFromV2 = new List<v2.osm_contactlist>();

        public static List<kn.osm_contactlist> ContactsFromV1
        {
            get
            {
                if (contactsFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        var usersFromV1 = Users.UsersFromV1.Select(user => user.USERID).ToList();

                        contactsFromV1 = (from contact in knCon.osm_contactlist
                                          where contact.DELETED == 0 && usersFromV1.Contains(contact.CREATED_BY ?? 0)
                                          select contact).ToList();
                    }
                }
                return contactsFromV1;
            }
        }

        public static List<v2.osm_contactlist> ContactsFromV2
        {
            get
            {
                if (contactsFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        contactsFromV2 = v2Con.osm_contactlist.ToList();
                    }
                }
                return contactsFromV2;
            }
        }

        public static List<v2.osm_contactlist> CleanedContacts()
        {
            List<v2.osm_contactlist> destinationList = new List<v2.osm_contactlist>();
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_contactlist, v2.osm_contactlist>(ContactsFromV1, destinationList);
            return destinationList;
        }

        public static List<v2.osm_contactlist> CleanedContacts(int userId)
        {
            List<v2.osm_contactlist> destinationList = new List<v2.osm_contactlist>();
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_contactlist, v2.osm_contactlist>(ContactsFromV1, destinationList);
            return destinationList;
        }
    }

    public class ContactInfo
    {
        [DefaultValue(0)]
        public int V1ContactId { get; set; }

        [DefaultValue(0)]
        public int V2ContactId { get; set; }
    }
}
