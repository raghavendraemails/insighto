﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataMigrationConsole
{
    public class ResponseInfo
    {
        public int V1ResponseId { get; set; }

        public int V2ResponseId { get; set; }
    }
}
