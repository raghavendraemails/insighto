﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
using System.Configuration;
namespace DataMigrationConsole
{
    public class Users
    {
        static List<kn.osm_user> usersFromV1 = new List<kn.osm_user>();
        static List<v2.osm_user> usersFromV2 = new List<v2.osm_user>();

        const string deletedUsers = @"ruchikaratna@gmail.com,ruchika@knowience.com,ruchika@dummyy.com,ruchikaratna@dummy.com,template@dummy.com,manikandan@srisaa.com,l_sandeepreddy@yahoo.co.in,sandeep_523@hotmail.com,reddysantest2@rediffmail.com,reddysan_test1@yahoo.com,sanreddytest527@gmail.com,sanreddytestoct1@gmail.com,reddysantest533@gmail.com,reddysantest561@yahoo.com,reddysantest572@yahoo.com,reddysantest101@gmail.com,reddsantest544@gmail.com,sandeepltest601@gmail.com,reddysantest560@yahoo.com";
        static string[] deletedUsersList = deletedUsers.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
        public static List<kn.osm_user> UsersFromV1
        {
            get
            {
                if (usersFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        var survey = (from s in knCon.osm_survey
                                      select s.USERID);

                        var filtereUsers = (from user in knCon.osm_user
                                            where survey.Contains(user.USERID)
                                            select user).ToList();

                        usersFromV1 = (from user in filtereUsers
                                      where !deletedUsersList.Contains(user.LOGIN_NAME)
                                      select user).ToList();

                    }
                }
                return usersFromV1;
            }
        }

        public static List<v2.osm_user> UsersFromV2
        {
            get
            {
                if (usersFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        usersFromV2 = v2Con.osm_user.ToList();
                    }
                }
                return usersFromV2;
            }
        }

        public static List<v2.osm_user> GetCleanedUsers(List<kn.osm_user> source)
        {
            List<v2.osm_user> destinationList = new List<v2.osm_user>();
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_user, v2.osm_user>(source, destinationList);
            return destinationList;
        }

        public static void SetDefaultFolderSettings()
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                int usersCount = 1;
                var applicationPath = ConfigurationManager.AppSettings["ExecutionPath"];

                var lstUsers = v2Con.osm_user.ToList().OrderByDescending(usr => usr.USERID).Take(520);
                foreach (var user in lstUsers)
                {
                    UserSettings.SetDefaultFolderSetiings(user, applicationPath);
                    UserSettings.SetDefaultAccountSettings(user.USERID);
                    Console.WriteLine("Inserted: {0} and UserId: {1}", usersCount, user.USERID);
                    usersCount++;
                }
            }
        }
    }
}
