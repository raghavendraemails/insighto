﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insighto.Data;
using System.Configuration;
using v2 = Insighto.Data;

namespace DataMigrationConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ////UserSettings.SetUserDefaultFiles();
            ////MatrixQuestions.UpdateMigratedSurves();
            //// UserSettings.ProcessUserSettingsAndFolders();

            if (Convert.ToInt32(ConfigurationManager.AppSettings["ExecutionType"]) == 0)
            {
                //Process Surveys...
                Console.WriteLine(" ----- Inserting into Survey's table ----- ");
                ProcessSurvey();

                Console.WriteLine(" ----- Surveys Migrated successfully  ----- ");
            }
            else if (Convert.ToInt32(ConfigurationManager.AppSettings["ExecutionType"]) == 1)
            {
                Console.WriteLine(" ----- {0}  ----- ", ConfigurationManager.AppSettings["ExecutionType"]);
                UserSettings.SetUserDefaultFiles();

                Console.WriteLine(" ----- Folders and Sample files create for new users done successfully  ----- ");
            }
            else if (Convert.ToInt32(ConfigurationManager.AppSettings["ExecutionType"]) == 2)
            {
                Console.WriteLine(" ----- {0}  ----- ", ConfigurationManager.AppSettings["ExecutionType"]);
                SurveyControls.ProcessSurveyLaunchUrl(2);

                Console.WriteLine(" ----- ProcessSurveyLaunchUrl  ----- ");
            }
            else
            {
                Console.WriteLine("\r\n \r\n Please choose right option by changing execution type key in config file.  ");
            }
            Console.ReadLine();
        }

        const string LogPath = @"C:\SurveysLog.csv";
        private static void ProcessSurvey()
        {
            int indexCtr = 0;
            var surveyInfoList = Surveys.SurveyInfoList;

            var surveys = Surveys.CleanedSurveysForV2;
            foreach (var survey in surveys)
            {
                if (Surveys.IsSurveyExisted(surveyInfoList[indexCtr].V1SuveyId, surveyInfoList[indexCtr].LoginName))
                {
                    Console.WriteLine("S.No: {0} ---- For User: {1} ---- Survey : {2} is already existed in V2 ", indexCtr + 1, surveyInfoList[indexCtr].LoginName, survey.SURVEY_NAME);
                    indexCtr++;

                    continue;
                }

                //Insert survey and get new Id and assign to surveyInfo object
                int v1SurveyId = survey.SURVEY_ID;
                var insertedSurvey = Surveys.InsertSurvey(survey);

                surveyInfoList[indexCtr].V2SurveyId = insertedSurvey != null ? insertedSurvey.SURVEY_ID : 0;
                Surveys.NewSurveyId = surveyInfoList[indexCtr].V2SurveyId;
                Console.WriteLine("S.No: {0} ---- User: {1} ---- SurveyV1: {2} ---- SurveyV2: {3}", indexCtr, "user", survey.SURVEY_ID, insertedSurvey != null ? insertedSurvey.SURVEY_ID : 0);

                //log required info for audit purpose.
                InsertIntoLog(v1SurveyId, Surveys.NewSurveyId, Surveys.CurrentSurveyInfo.LoginName);




                Console.WriteLine(" ----- Inserting into Survey introduction table ----- ");
                SurveyIntro.SurveyIntrosFromV1 = new List<Knoweince.Data.osm_surveyIntro>();
                SurveyIntro.CleanedSurveyIntrosForV2 = new List<Insighto.Data.osm_surveyIntro>();
                SurveyIntro.ProcessSurveyIntroduction(Surveys.NewSurveyId);


                Console.WriteLine(" ----- Inserting into Survey Settings table ----- ");
                SurveySettings.SettingsFromV1 = new List<Knoweince.Data.osm_surveysetting>();
                SurveySettings.CleanedSettingsForV2 = new List<Insighto.Data.osm_surveysetting>();
                SurveySettings.ProcessSurveySetting(Surveys.NewSurveyId);


                Console.WriteLine(" ----- Inserting into Survey Controls table ----- ");
                SurveyControls.ControlsFromV1 = new List<Knoweince.Data.osm_surveycontrols>();
                SurveyControls.CleanedControlsForV2 = new List<Insighto.Data.osm_surveycontrols>();
                SurveyControls.ProcessSurveyControls(Surveys.NewSurveyId);

                Console.WriteLine(" ----- Inserting into Survey Launch table ----- ");
                SurveyLaunch.LaunchFromV1 = new List<Knoweince.Data.osm_surveylaunch>();
                SurveyLaunch.CleanedLaunchForV2 = new List<Insighto.Data.osm_surveylaunch>();
                SurveyLaunch.ProcessSurveyLaunch(Surveys.NewSurveyId);


                Console.WriteLine(" ----- Inserting into Survey Question table ----- ");
                SurveyQuestions.QuestionsFromV1 = new List<Knoweince.Data.osm_surveyquestion>();
                SurveyQuestions.CleanedQuestionsForV2 = new List<Insighto.Data.osm_surveyquestion>();
                SurveyQuestions.ProcessSurveyQuestions(Surveys.NewSurveyId);

                Console.WriteLine(" ----- Inserting into Answer Options table ----- ");
                SurveyAnswerOptions.AnswersFromV1 = new List<Knoweince.Data.osm_answeroptions>();
                SurveyAnswerOptions.CleanedOptionsForV2 = new List<Insighto.Data.osm_answeroptions>();
                foreach (var question in Surveys.CurrentSurveyInfo.QuestionsInfo)
                {
                    SurveyAnswerOptions.ProcessAnswerOptions(question.V2QuestionId);
                }


                Console.WriteLine(" ----- Inserting into Responses table ----- ");
                ResponseSurvey.ResponsesFromV1 = new List<Knoweince.Data.osm_responsesurvey>();
                //ResponseSurvey.ResponsesFromV2 = new List<osm_responsesurvey>();
                ResponseSurvey.CleanedResponsesForV2 = new List<osm_responsesurvey>();
                ResponseSurvey.ProcessSurveyResponses(Surveys.NewSurveyId);


                Console.WriteLine(" ----- Inserting into Response Questions Options table ----- ");
                ResponseQuestions.ResponseQuestionsesFromV1 = new List<Knoweince.Data.osm_responsequestions>();
                //ResponseQuestions.ResponseQuestionsesFromV2 = new List<Insighto.Data.osm_responsequestions>();
                ResponseQuestions.CleanedResponseQuestionsesForV2 = new List<Insighto.Data.osm_responsequestions>();
                foreach (var response in Surveys.CurrentSurveyInfo.ResponsesInfo)
                {
                    ResponseQuestions.ProcessResponseQuestionses(response.V2ResponseId);
                }


                indexCtr++;
            }
        }

        private static void InsertIntoLog(int v1SurveyId, int v2SurveyId, string userName)
        {
            using (InsightoV2Entities dbCon = new InsightoV2Entities())
            {
                var currentMigrationSurveyInfo = new MigrationAuditInfo { V1SurveyId = v1SurveyId, V2SurveyId = v2SurveyId, CreatedOn = DateTime.UtcNow, UserName = userName };
                dbCon.MigrationAuditInfo.Add(currentMigrationSurveyInfo);
                dbCon.SaveChanges();
            }
        }
    }
}