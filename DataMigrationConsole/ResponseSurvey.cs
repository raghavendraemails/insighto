﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
namespace DataMigrationConsole
{
    public class ResponseSurvey
    {
        static List<kn.osm_responsesurvey> responsesFromV1 = new List<kn.osm_responsesurvey>();
        static List<v2.osm_responsesurvey> responsesFromV2 = new List<v2.osm_responsesurvey>();
        static List<v2.osm_responsesurvey> cleanedResponsesV2 = new List<v2.osm_responsesurvey>();

        /// <summary>
        /// Gets the responses from v1.
        /// </summary>
        public static List<kn.osm_responsesurvey> ResponsesFromV1
        {
            get
            {
                if (responsesFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        responsesFromV1 = (from v1s in knCon.osm_responsesurvey.ToList()
                                           where v1s.DELETED == 0 && v1s.SURVEY_ID == Surveys.CurrentSurveyInfo.V1SuveyId
                                           select v1s).ToList();
                    }
                }
                return responsesFromV1;
            }
            set
            {
                responsesFromV1 = value;
            }
        }

        /// <summary>
        /// Gets the responses from v2.
        /// </summary>
        public static List<v2.osm_responsesurvey> ResponsesFromV2
        {
            get
            {
                if (responsesFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        responsesFromV2 = v2Con.osm_responsesurvey.ToList();
                    }
                }
                return responsesFromV2;
            }
            set
            {
                responsesFromV2 = value;
            }
        }


        public static List<v2.osm_responsesurvey> CleanedResponsesForV2
        {
            get
            {
                if (cleanedResponsesV2.Count == 0)
                {
                    GetCleanedResponsesList(ResponsesFromV1);
                    return cleanedResponsesV2;
                }
                return cleanedResponsesV2;
            }
            set
            {
                cleanedResponsesV2 = value;
            }
        }

        /// <summary>
        /// Gets the cleaned responses list.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static void GetCleanedResponsesList(List<kn.osm_responsesurvey> source)
        {
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_responsesurvey, v2.osm_responsesurvey>(ResponsesFromV1, cleanedResponsesV2);

            //modify required feilds for ready to insert .. 1. folder id 2. userid.
            SetUserAndResponsesIds(cleanedResponsesV2);
        }

        /// <summary>
        /// Sets the user and responses ids.
        /// </summary>
        /// <param name="destinationResponses">The destination responses.</param>
        private static void SetUserAndResponsesIds(List<v2.osm_responsesurvey> destinationResponses)
        {
            int ctr = 0;
            foreach (var item in destinationResponses)
            {
                Console.WriteLine("{0} ---- GetCleanedResponsesList(). ", ++ctr);
                var currentSurvey = Surveys.SurveyInfoList.FirstOrDefault(si => si.V1SuveyId == item.SURVEY_ID);
                item.SURVEY_ID = Surveys.CurrentSurveyInfo.V2SurveyId; //currentSurvey.V2SurveyId;

                if (currentSurvey.V1ContactId > 0 && item.EMAIL_ID > 0)
                {
                    var cleanedEmails = EmailLists.CleanedEmails(currentSurvey.V1ContactId);

                    var newEmails = (from cv1 in EmailLists.EmailsFromV1.ToList()
                                     join cv2 in EmailLists.EmailsFromV2.ToList()
                                     on cv1.EMAIL_ADDRESS equals cv2.EMAIL_ADDRESS
                                     where cv1.EMAIL_ID == item.EMAIL_ID
                                     select new { V1EmailId = cv1.EMAIL_ID, V2EmailId = cv2.EMAIL_ID }).ToList();
                    item.EMAIL_ID = newEmails.Count > 0 ? newEmails.FirstOrDefault().V2EmailId : item.EMAIL_ID;
                }
            }
            //timer.Stop();
        }

        /// <summary>
        /// Inserts the question.
        /// </summary>
        /// <param name="v2Response">The v2 question.</param>
        /// <returns></returns>
        public static v2.osm_responsesurvey InsertResponse(v2.osm_responsesurvey v2Response)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                v2Con.osm_responsesurvey.Add(v2Response);
                v2Con.SaveChanges();
                return v2Response;
            }
        }

        /// <summary>
        /// Processes the survey responses.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        internal static void ProcessSurveyResponses(int surveyId)
        {
            //Insert survey and get new Id and assign to surveyInfo object
            var surveyResponses = ResponseSurvey.CleanedResponsesForV2.Where(intro => intro.SURVEY_ID == surveyId).ToList();
            if (surveyResponses.Count > 0)
            {
                foreach (var response in surveyResponses)
                {
                    var responseInfo = new ResponseInfo { V1ResponseId = response.RESPONDENT_ID };
                    var insertedResponseInfo = ResponseSurvey.InsertResponse(response);
                    responseInfo.V2ResponseId = insertedResponseInfo.RESPONDENT_ID;

                    Console.WriteLine("S.No: {0} ---- Survey Response. ", insertedResponseInfo.RESPONDENT_ID);
                    Surveys.CurrentSurveyInfo.ResponsesInfo.Add(responseInfo);

                    //log into questions audit table
                    InsertIntoLog(responseInfo.V1ResponseId, responseInfo.V2ResponseId, surveyId);
                }
            }
            else
                Console.WriteLine(" ----- Response not inserted -----");
        }

        private static void InsertIntoLog(int v1RespondentId, int v2RespondentId, int v2SurveyId)
        {
            using (v2.InsightoV2Entities dbCon = new v2.InsightoV2Entities())
            {
                var currentMigrationSurveyInfo = new v2.ResponsesAuditInfo { V2SurveyId = v2SurveyId, V1RespondentId = v1RespondentId, V2RespondentId = v2RespondentId, CreatedOn = DateTime.UtcNow };
                dbCon.ResponsesAuditInfo.Add(currentMigrationSurveyInfo);
                dbCon.SaveChanges();
            }
        }
    }
}
