﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using v2 = Insighto.Data;
using System.Drawing;
using System.Configuration;
namespace DataMigrationConsole
{

    public class MatrixQuestions
    {
        public static void UpdateMigratedSurves()
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                var surveys = v2Con.MigrationAuditInfo.ToList();
                var logPath = ConfigurationManager.AppSettings["LogPath"];
                foreach (var surveyId in surveys)
                {
                    CommonMethods.GenrateLog("Strated shuffling SurveyId:" + surveyId.V2SurveyId, logPath);  
                    var questionDetails = v2Con.osm_surveyquestion.Where(s => s.SURVEY_ID == surveyId.V2SurveyId).ToList();
                    foreach (var question in questionDetails)
                    {
                        if (question.QUESTION_TYPE_ID == 10 || question.QUESTION_TYPE_ID == 11 || question.QUESTION_TYPE_ID == 12)
                        {
                            UpdateMatrixQuestion(question);
                        }                        
                        continue;
                      
                    }
                    CommonMethods.GenrateLog("Updated shuffling SurveyId Id:" + surveyId.V2SurveyId, logPath); 
                }
            }
            
            Console.WriteLine("Finished the Updating Migrated Surveys......."); 
        }

        private static void UpdateMatrixQuestion(v2.osm_surveyquestion questionDetails)
        {
            var ansOption = GetAnsweOption(questionDetails);
            foreach (var ans in ansOption)
            {
                var finalAnswerOption = ShuffleAttributes(ans.ANSWER_OPTIONS, Convert.ToInt32(questionDetails.QUESTION_TYPE_ID));
                UpdateAnswerOption(ans.ANSWER_ID, finalAnswerOption);
            }
        }

        private static void UpdateAnswerOption(int AnswerId, string finalAnswerOption)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                var ans = v2Con.osm_answeroptions.Where(a => a.ANSWER_ID == AnswerId).FirstOrDefault();
                ans.ANSWER_OPTIONS = finalAnswerOption;
                v2Con.SaveChanges();
            }

        }

        private static List<v2.osm_answeroptions> GetAnsweOption(v2.osm_surveyquestion qns)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                return (from answeroption in v2Con.osm_answeroptions
                        where answeroption.QUESTION_ID == qns.QUESTION_ID 
                        select answeroption).ToList();
            }
        }

        private static string ShuffleAttributes(string answerOption, int questionType)
        {
            string finalAnswers = string.Empty;
            string[] answers = answerOption.Split(new string[] { "$--$" }, StringSplitOptions.None);

            if(questionType == 12)
            finalAnswers = answers[0] + "$--$" + answers[2] + "$--$" + answers[1];
            else
                finalAnswers = answers[1] + "$--$" + answers[0];
            return finalAnswers;
        }
    }
}
