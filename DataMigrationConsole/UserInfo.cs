﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataMigrationConsole
{
    public class UserInfo : FoldersInfo
    {
        public int V1UserId { get; set; }
        public int V2UserId { get; set; }
        public string LoginName { get; set; }
    }
}
