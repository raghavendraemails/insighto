﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
using Insighto.Business.Helpers;
namespace DataMigrationConsole
{
    public class SurveyControls
    {
        static List<kn.osm_surveycontrols> controlsFromV1 = new List<kn.osm_surveycontrols>();
        static List<v2.osm_surveycontrols> controlsFromV2 = new List<v2.osm_surveycontrols>();
        static List<v2.osm_surveycontrols> cleanedControlsForV2 = new List<v2.osm_surveycontrols>();

        /// <summary>
        /// Gets the controls from v1.
        /// </summary>
        public static List<kn.osm_surveycontrols> ControlsFromV1
        {
            get
            {
                if (controlsFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        controlsFromV1 = (from v1s in knCon.osm_surveycontrols.ToList()
                                          where v1s.SURVEY_ID == Surveys.CurrentSurveyInfo.V1SuveyId
                                          select v1s).OrderByDescending(cont => cont.SURVEYCONTROL_ID).ToList();
                    }
                }
                return controlsFromV1;
            }
            set
            {
                controlsFromV1 = value;
            }
        }

        /// <summary>
        /// Gets the controls from v2.
        /// </summary>
        public static List<v2.osm_surveycontrols> ControlsFromV2
        {
            get
            {
                if (controlsFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        controlsFromV2 = v2Con.osm_surveycontrols.ToList();
                    }
                }
                return controlsFromV2;
            }
        }

        /// <summary>
        /// Gets the cleaned controls for v2.
        /// </summary>
        public static List<v2.osm_surveycontrols> CleanedControlsForV2
        {
            get
            {
                if (cleanedControlsForV2.Count == 0)
                {
                    GetCleanedControlsList(ControlsFromV1);
                    return cleanedControlsForV2;
                }
                return cleanedControlsForV2;
            }
            set
            {
                cleanedControlsForV2 = value;
            }
        }

        /// <summary>
        /// Gets the cleaned controls list.
        /// </summary>
        /// <param name="source">The source.</param>
        public static void GetCleanedControlsList(List<kn.osm_surveycontrols> source)
        {
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_surveycontrols, v2.osm_surveycontrols>(source, cleanedControlsForV2);

            //modify required feilds for ready to insert .. 1. folder id 2. userid.
            SetUserAndSurveyIds(cleanedControlsForV2);
        }

        /// <summary>
        /// Sets the user and contact ids.
        /// </summary>
        /// <param name="destinationcontrols">The destination controls.</param>
        private static void SetUserAndSurveyIds(List<v2.osm_surveycontrols> destinationControls)
        {
            int ctr = 0;
            foreach (var item in destinationControls)
            {
                Console.WriteLine("{0} ---- GetCleanedControlsList(). ", ++ctr);
                var currentSurvey = Surveys.SurveyInfoList.FirstOrDefault(si => si.V1SuveyId == item.SURVEY_ID);
                item.SURVEY_ID = currentSurvey.V2SurveyId;
                item.THANKYOU_PAGE = CommonMethods.ClearHTML(item.THANKYOU_PAGE);
                item.CLOSE_PAGE = CommonMethods.ClearHTML(item.CLOSE_PAGE);
                item.SCREENOUT_PAGE = CommonMethods.ClearHTML(item.SCREENOUT_PAGE);

                if (item.EMAILLIST_TYPE == 1)
                {
                    var newContacts = from cv1 in ContactList.ContactsFromV1.ToList()
                                      join cv2 in ContactList.ContactsFromV2.ToList()
                                      on cv1.CONTACTLIST_NAME equals cv2.CONTACTLIST_NAME
                                      where cv2.CREATED_BY == currentSurvey.V2UserId
                                      select new { V1ContactId = cv1.CONTACTLIST_ID, V2ContactId = cv2.CONTACTLIST_ID };

                    var currentContactList = newContacts.FirstOrDefault(c => c.V1ContactId == item.CONTACTLIST_ID);
                    item.CONTACTLIST_ID = currentContactList != null ? currentContactList.V2ContactId : item.CONTACTLIST_ID;
                    currentSurvey.V1ContactId = currentContactList != null ? currentContactList.V1ContactId : item.CONTACTLIST_ID;
                    currentSurvey.V2ContactId = currentContactList != null ? currentContactList.V2ContactId : item.CONTACTLIST_ID;
                }
            }
            //timer.Stop();
        }

        /// <summary>
        /// Inserts the survey controls.
        /// </summary>
        /// <param name="v2controls">The v2 controls.</param>
        /// <returns></returns>
        public static v2.osm_surveycontrols InsertSurveyControls(v2.osm_surveycontrols v2Controls)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                v2Con.osm_surveycontrols.Add(v2Controls);
                v2Con.SaveChanges();
                return v2Controls;
            }
        }

        /// <summary>
        /// Processes the survey controls.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        public static void ProcessSurveyControls(int surveyId)
        {
            //Insert survey and get new Id and assign to surveyInfo object
            var surveyControls = SurveyControls.CleanedControlsForV2.FirstOrDefault(intro => intro.SURVEY_ID == surveyId);
            if (surveyControls != null)
            {
                var insertedSurveyControl = SurveyControls.InsertSurveyControls(surveyControls);
                Console.WriteLine("S.No: {0} ---- Survey controls. ", 1);
            }
            else
                Console.WriteLine(" ----- Not inserted -----");
        }

        internal static void ProcessSurveyLaunchUrl(int executionType = 0)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                var migrateSurveys = v2Con.MigrationAuditInfo.ToList();
                foreach (var item in migrateSurveys)
                {
                    var survey = v2Con.osm_survey.Where(s => s.SURVEY_ID == item.V2SurveyId).FirstOrDefault();
                    var surveyControl = v2Con.osm_surveycontrols.Where(s => s.SURVEY_ID == item.V2SurveyId).FirstOrDefault();
                    string navigationUrl = string.Empty;

                    if (!survey.SurveyWidgetFlag.HasValue || survey.SurveyWidgetFlag == 0)
                        navigationUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetSurveyRespondenURL(), "SurveyId=" + survey.SURVEY_ID + "&Layout=" + 1);
                    else
                        navigationUrl = EncryptHelper.EncryptQuerystring(PathHelper.GetOpenWidgetURL().Replace("~/", ""), "SurveyId=" + survey.SURVEY_ID + "&Layout=" + 1);

                    surveyControl.SURVEYLAUNCH_URL = string.Format("{0}/{1}",  "http://insighto.com", navigationUrl);
                    surveyControl.SURVEY_URL = string.Format("{0}/{1}", "http://insighto.com/", navigationUrl);
                    if (executionType > 0)
                    {
                        v2Con.SaveChanges();
                        Console.WriteLine(" ----- Updated successfully for:  {0} ", item.V2SurveyId);
                    }
                    else
                    {
                        Console.WriteLine(" ----- SKIPPED FOR:  {0} ", item.V2SurveyId);
                    }

                }
            }
        }
    }
}
