﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
namespace DataMigrationConsole
{
    public class Folders
    {
        static List<kn.osm_cl_folders> foldersFromV1 = new List<kn.osm_cl_folders>();
        static List<v2.osm_cl_folders> foldersFromV2 = new List<v2.osm_cl_folders>();
        static List<FoldersInfo> foldersFromV1AndV2 = new List<FoldersInfo>();

        public static List<kn.osm_cl_folders> FoldersFromV1
        {
            get
            {
                if (foldersFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        var survey = (from s in knCon.osm_survey
                                      select s.USERID);
                        var usersFromV1 = (from user in knCon.osm_user
                                           where survey.Contains(user.USERID)
                                           select user.USERID).ToList();

                        foldersFromV1 = (from folder in knCon.osm_cl_folders
                                         where folder.DELETED == 0 && usersFromV1.Contains(folder.USERID ?? 0)
                                         select folder).ToList();


                        //foldersFromV1 = knCon.osm_cl_folders.ToList();
                    }
                }
                return foldersFromV1;
            }
        }

        public static List<v2.osm_cl_folders> FoldersFromV2
        {
            get
            {
                if (foldersFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        foldersFromV2 = v2Con.osm_cl_folders.ToList();
                    }
                }
                return foldersFromV2;
            }
        }

        public static List<v2.osm_cl_folders> CleanedFolders()
        {
            List<v2.osm_cl_folders> destinationList = new List<v2.osm_cl_folders>();
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_cl_folders, v2.osm_cl_folders>(FoldersFromV1, destinationList);
            return destinationList;
        }
    }

    public class FoldersInfo : ContactInfo
    {
        public int V1FolderId { get; set; }

        public int V2FolderId { get; set; }
    }
}
