﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
namespace DataMigrationConsole
{
    public class SurveyQuestions
    {
        static List<kn.osm_surveyquestion> questionsFromV1 = new List<kn.osm_surveyquestion>();
        static List<v2.osm_surveyquestion> questionsFromV2 = new List<v2.osm_surveyquestion>();
        static List<v2.osm_surveyquestion> cleanedQuestionsV2 = new List<v2.osm_surveyquestion>();

        /// <summary>
        /// Gets the questions from v1.
        /// </summary>
        public static List<kn.osm_surveyquestion> QuestionsFromV1
        {
            get
            {
                if (questionsFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        questionsFromV1 = (from v1s in knCon.osm_surveyquestion.ToList()
                                           where v1s.DELETED == 0 && v1s.SURVEY_ID == Surveys.CurrentSurveyInfo.V1SuveyId
                                           select v1s).ToList();
                    }
                }
                return questionsFromV1;
            }
            set
            {
                questionsFromV1 = value;
            }
        }

        /// <summary>
        /// Gets the questions from v2.
        /// </summary>
        public static List<v2.osm_surveyquestion> QuestionsFromV2
        {
            get
            {
                if (questionsFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        questionsFromV2 = v2Con.osm_surveyquestion.ToList();
                    }
                }
                return questionsFromV2;
            }
            set
            {
                questionsFromV2 = value;
            }
        }


        public static List<v2.osm_surveyquestion> CleanedQuestionsForV2
        {
            get
            {
                if (cleanedQuestionsV2.Count == 0)
                {
                    GetCleanedQuestionsList(QuestionsFromV1);
                    return cleanedQuestionsV2;
                }
                return cleanedQuestionsV2;
            }
            set
            {
                cleanedQuestionsV2 = value;
            }
        }

        /// <summary>
        /// Gets the cleaned questions list.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static void GetCleanedQuestionsList(List<kn.osm_surveyquestion> source)
        {
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_surveyquestion, v2.osm_surveyquestion>(QuestionsFromV1, cleanedQuestionsV2);

            //modify required feilds for ready to insert .. 1. folder id 2. userid.
            SetUserAndQuestionsIds(cleanedQuestionsV2);
        }

        /// <summary>
        /// Sets the user and questions ids.
        /// </summary>
        /// <param name="destinationQuestions">The destination questions.</param>
        private static void SetUserAndQuestionsIds(List<v2.osm_surveyquestion> destinationQuestions)
        {
            int ctr = 0;
            foreach (var item in destinationQuestions)
            {
                Console.WriteLine("{0} ---- GetCleanedQuestionsList(). ", ++ctr);
                var currentSurvey = Surveys.SurveyInfoList.FirstOrDefault(si => si.V1SuveyId == item.SURVEY_ID);
                item.SURVEY_ID = Surveys.CurrentSurveyInfo.V2SurveyId; //currentSurvey.V2SurveyId;
            }
            //timer.Stop();
        }

        /// <summary>
        /// Inserts the question.
        /// </summary>
        /// <param name="v2Question">The v2 question.</param>
        /// <returns></returns>
        public static v2.osm_surveyquestion InsertQuestion(v2.osm_surveyquestion v2Question)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                v2Con.osm_surveyquestion.Add(v2Question);
                v2Con.SaveChanges();
                return v2Question;
            }
        }

        internal static void ProcessSurveyQuestions(int surveyId)
        {
            //Insert survey and get new Id and assign to surveyInfo object
            var surveyQuestions = SurveyQuestions.CleanedQuestionsForV2.Where(intro => intro.SURVEY_ID == surveyId).ToList();
            if (surveyQuestions.Count > 0)
            {
                foreach (var question in surveyQuestions)
                {
                    var questionInfo = new QuestionInfo { V1QuestionId = question.QUESTION_ID, IsSkipEnabled = question.SKIP_LOGIC > 0 };
                    var insertedQuestionInfo = SurveyQuestions.InsertQuestion(question);
                    questionInfo.V2QuestionId = insertedQuestionInfo.QUESTION_ID;
                    
                    Console.WriteLine("S.No: {0} ---- Survey Question. ", insertedQuestionInfo.QUESTION_ID);
                    Surveys.CurrentSurveyInfo.QuestionsInfo.Add(questionInfo);

                    //log into questions audit table
                    InsertIntoLog(questionInfo.V1QuestionId, questionInfo.V2QuestionId, surveyId);

                }
            }
            else
                Console.WriteLine(" ----- Question not inserted -----");
        }

        private static void InsertIntoLog(int v1QuestionId, int v2QuestionId, int v2SurveyId)
        {
            using (v2.InsightoV2Entities dbCon = new v2.InsightoV2Entities())
            {
                var currentMigrationSurveyInfo = new v2.QuestionsAuditInfo { V2SurveyId = v2SurveyId, V1QuestionId = v1QuestionId, V2QuestionId = v2QuestionId, CreatedOn = DateTime.UtcNow };
                dbCon.QuestionsAuditInfo.Add(currentMigrationSurveyInfo);
                dbCon.SaveChanges();
            }
        }
    }
}
