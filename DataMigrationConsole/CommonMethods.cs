﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;

namespace DataMigrationConsole
{
    public class CommonMethods
    {
        public static void MergeObjectsFromV1ToV2<T, K>(List<T> source, List<K> target)
            where T : class
            where K : class
        {
            foreach (var sourceItem in source)
            {
                var destinationInstance = Activator.CreateInstance(typeof(K));
                MergeFrom(destinationInstance, sourceItem);
                target.Add(destinationInstance as K);

                Console.WriteLine("{0} is merging into V2. ", destinationInstance.GetType().FullName);
            }
        }

        /// <summary>
        /// Merges the equivalent properties from the source to this object.
        /// </summary>
        /// <typeparam name="T">Any reference type.</typeparam>
        /// <param name="destination">This object, which receives the values.</param>
        /// <param name="source">The source object that the values are taken from.</param>
        public static void MergeFrom<T>(object destination, T source)
        {
            Type destinationType = destination.GetType();
            PropertyInfo[] propertyInfos = source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var propertyInfo in propertyInfos)
            {
                PropertyInfo destinationPropertyInfo = destinationType.GetProperty(propertyInfo.Name, BindingFlags.Public | BindingFlags.Instance);
                if (destinationPropertyInfo != null)
                {
                    if (destinationPropertyInfo.CanWrite && propertyInfo.CanRead)
                    {
                        object o = propertyInfo.GetValue(source, null);
                        destinationPropertyInfo.SetValue(destination, o, null);
                    }
                }
            }
        }

        /// <summary>
        /// URLs the extension.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        public static string URLExtension(string url)
        {
            try
            {
                return System.IO.Path.GetExtension(new Uri(url).AbsolutePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Invalid Uri format: {0}", ex.Message);
                return "JPG";
            }
        }

        /// <summary>
        /// Downloads the specified image URL.
        /// </summary>
        /// <param name="imageUrl">The image URL.</param>
        /// <returns></returns>
        public static Bitmap Download(string imageUrl)
        {
            Bitmap bitmap = null;
            try
            {
                WebClient client = new WebClient();
                Stream stream = client.OpenRead(imageUrl);
                bitmap = new Bitmap(stream);
                stream.Flush();
                stream.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return bitmap;
        }

        /// <summary>
        /// Saves the image.
        /// </summary>
        /// <param name="bitmap">The bitmap.</param>
        /// <param name="filename">The filename.</param>
        /// <param name="format">The format.</param>
        public static void SaveImage(Bitmap bitmap, string filename, ImageFormat format)
        {
            if (bitmap != null)
            {
                bitmap.Save(filename, format);
                bitmap.Dispose();
            }
        }

        public static string GetImageUrl(string fullPath)
        {
            if (string.IsNullOrEmpty(fullPath))
                return string.Empty;

            string[] pathparts = fullPath.Split(new[] { ">" }, StringSplitOptions.RemoveEmptyEntries);
            pathparts = pathparts[0].Split(new[] { Path.PathSeparator }, StringSplitOptions.RemoveEmptyEntries);
            return pathparts[pathparts.Length - 1].Substring(1);
        }

        internal static ImageFormat GetImageFormat(string extension)
        {
            switch (extension.ToLower())
            {

                case ".jpg":
                case ".jpeg":
                    return ImageFormat.Jpeg;

                case ".gif":
                    return ImageFormat.Gif;

                case ".png":
                    return ImageFormat.Png;

                case ".bmp":
                    return ImageFormat.Bmp;

                default:
                    return ImageFormat.Jpeg;

            }
        }

        /// <summary>
        /// Clears the HTML.
        /// </summary>
        /// <param name="htmlContent">Content of the HTML.</param>
        /// <returns></returns>
        public static string ClearHTML(string htmlContent)
        {
            string plainHTML = "";
            if (htmlContent != null)
            {
                string pattern1html = "<(.|\n)+?>";
                plainHTML = Regex.Replace(htmlContent, pattern1html, string.Empty);
                string patternHtml = "<.*?>";
                plainHTML = Regex.Replace(plainHTML, patternHtml, string.Empty);
                plainHTML = plainHTML.Replace("&lt;", "<");
                plainHTML = plainHTML.Replace("&gt;", ">");
                plainHTML = plainHTML.Replace("&quot;", "\"");
                plainHTML = plainHTML.Replace("&apos;", "'");
                plainHTML = plainHTML.Replace("&amp;", "&");
                plainHTML = plainHTML.Replace("&nbsp;", " ");
                plainHTML = plainHTML.Replace("&copy;", "\u00a9");

            }
            var splitted = plainHTML.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Where(s => s.Trim().Length > 0).Select(s => s.Trim()).ToArray();
            if (splitted.Length > 0)
                return splitted[0];
            else
                return plainHTML;
        }

        public static void GenrateLog(string Content, string path)
        {
           
                StreamWriter sw = null;
                try
                {
                    if (File.Exists(path))
                    {
                        FileStream file = new FileStream(path, FileMode.Append, FileAccess.Write);
                        sw = new StreamWriter(file);
                    }
                    else
                    {
                        sw = File.CreateText(path);
                    }
                    sw.WriteLine(DateTime.Now.ToString() + " ***** " + Content.Trim());
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (sw != null) sw.Close();
                }
            

        }

        public static void ReadandWriteFile(string sourcePath, string targetPath)
        {
            MemoryStream ms = new MemoryStream();
            FileStream file = new FileStream(sourcePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            byte[] bytes = new byte[file.Length];
            file.Read(bytes, 0, (int)file.Length);
            ms.Write(bytes, 0, (int)file.Length);
            file.Close();

            WriteFile(ms, targetPath);
            //ms.Close();
        }
        public static void WriteFile(MemoryStream ms, string targetPath)
        {
            FileStream file = new FileStream(targetPath, FileMode.Create, System.IO.FileAccess.Write);
            byte[] bytes = new byte[ms.Length];
            ms.Read(bytes, 0, (int)ms.Length);
            file.Write(bytes, 0, bytes.Length);
            file.Close();
            ms.Close();
        }
    }
}
