﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataMigrationConsole
{
    public class QuestionInfo
    {
        public int V1QuestionId { get; set; }

        public int V2QuestionId { get; set; }

        public bool IsSkipEnabled { get; set; }

        List<AnswerInfo> answerInfo = new List<AnswerInfo>();
        public List<AnswerInfo> AnswersInfo
        {
            get
            {
                return answerInfo;
            }

            set
            {
                answerInfo = value;
            }
        }
    }

    public class AnswerInfo
    {
        public int V1AnswerId { get; set; }

        public int V2AnswerId { get; set; }
    }
}
