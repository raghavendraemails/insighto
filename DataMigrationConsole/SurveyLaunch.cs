﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
using System.Drawing;
using System.Configuration;
namespace DataMigrationConsole
{
    public class SurveyLaunch
    {
        static List<kn.osm_surveylaunch> launchFromV1 = new List<kn.osm_surveylaunch>();
        static List<v2.osm_surveylaunch> launchFromV2 = new List<v2.osm_surveylaunch>();
        static List<v2.osm_surveylaunch> cleanedLaunchForV2 = new List<v2.osm_surveylaunch>();

        /// <summary>
        /// Gets the launch from v1.
        /// </summary>
        public static List<kn.osm_surveylaunch> LaunchFromV1
        {
            get
            {
                if (launchFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        launchFromV1 = (from v1s in knCon.osm_surveylaunch.ToList()
                                        where v1s.SURVEY_ID == Surveys.CurrentSurveyInfo.V1SuveyId
                                        select v1s).OrderByDescending(launch => launch.LAUNCH_ID).ToList();
                    }
                }
                return launchFromV1;
            }
            set
            {
                launchFromV1 = value;
            }
        }

        /// <summary>
        /// Gets the launch from v2.
        /// </summary>
        public static List<v2.osm_surveylaunch> LaunchFromV2
        {
            get
            {
                if (launchFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        launchFromV2 = v2Con.osm_surveylaunch.ToList();
                    }
                }
                return launchFromV2;
            }
            set
            {
                launchFromV2 = value;
            }
        }

        /// <summary>
        /// Gets the cleaned launch for v2.
        /// </summary>
        public static List<v2.osm_surveylaunch> CleanedLaunchForV2
        {
            get
            {
                if (cleanedLaunchForV2.Count == 0)
                {
                    GetCleanedLaunchList(launchFromV1);
                    return cleanedLaunchForV2;
                }
                return cleanedLaunchForV2;
            }
            set
            {
                cleanedLaunchForV2 = value;
            }
        }

        /// <summary>
        /// Gets the cleaned launch list.
        /// </summary>
        /// <param name="source">The source.</param>
        public static void GetCleanedLaunchList(List<kn.osm_surveylaunch> source)
        {
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_surveylaunch, v2.osm_surveylaunch>(source, cleanedLaunchForV2);

            //modify required feilds for ready to insert .. 1. folder id 2. userid.
            SetUserAndSurveyIds(cleanedLaunchForV2);
        }

        /// <summary>
        /// Sets the user and survey ids.
        /// </summary>
        /// <param name="destinationLaunch">The destination launch.</param>
        private static void SetUserAndSurveyIds(List<v2.osm_surveylaunch> destinationLaunch)
        {
            int ctr = 0;
            foreach (var item in destinationLaunch)
            {
                Console.WriteLine("{0} ---- GetCleanedLaunchList(). ", ++ctr);
                var currentSurvey = Surveys.SurveyInfoList.FirstOrDefault(si => si.V1SuveyId == item.SURVEY_ID);
                item.CONTACTLIST_ID = currentSurvey.V2ContactId;
                item.SURVEY_ID = currentSurvey.V2SurveyId;
            }
        }

        /// <summary>
        /// Inserts the survey launch.
        /// </summary>
        /// <param name="v2Setting">The v2 setting.</param>
        /// <returns></returns>
        public static v2.osm_surveylaunch InsertSurveyLaunch(v2.osm_surveylaunch v2Launch)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                v2Con.osm_surveylaunch.Add(v2Launch);
                v2Con.SaveChanges();
                return v2Launch;
            }
        }

        /// <summary>
        /// Processes the survey launch.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        public static void ProcessSurveyLaunch(int surveyId)
        {
            //Insert survey and get new Id and assign to surveyInfo object
            var surveyLaunch = SurveyLaunch.CleanedLaunchForV2.FirstOrDefault(intro => intro.SURVEY_ID == surveyId);
            if (surveyLaunch != null)
            {
                var insertedSurveyLaunch = SurveyLaunch.InsertSurveyLaunch(surveyLaunch);
                Console.WriteLine("S.No: {0} ---- Survey Launch. ", 1);
            }
            else
                Console.WriteLine(" ----- Launch Not inserted -----");
        }
    }
}
