﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;

namespace DataMigrationConsole
{
    public class Surveys
    {
        static List<kn.osm_survey> surveysFromV1 = new List<kn.osm_survey>();
        static List<v2.osm_survey> surveysFromV2 = new List<v2.osm_survey>();
        static List<v2.osm_survey> cleanedSurveysV2 = new List<v2.osm_survey>();

        /// <summary>
        /// Gets the surveys from v1.
        /// </summary>
        /// <returns></returns>
        public static List<kn.osm_survey> SurveysFromV1
        {
            get
            {
                if (surveysFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        var surveysAndUsersFromV1 = (from v1s in knCon.osm_survey.ToList()
                                                     join uv1 in Users.UsersFromV1
                                                     on v1s.USERID equals uv1.USERID
                                                     where v1s.DELETED == 0
                                                     select v1s);

                        var groupedSurveysDbSet = (from sur in surveysAndUsersFromV1
                                                   join sq in knCon.osm_surveyquestion
                                                   on sur.SURVEY_ID equals sq.SURVEY_ID
                                                   group sur by sur.SURVEY_ID into groupedSurveys
                                                   where groupedSurveys.ToList().Count > 0
                                                   select groupedSurveys.Select(s => (kn.osm_survey)s)).ToList();

                        surveysFromV1 = groupedSurveysDbSet.Select(s => s.FirstOrDefault()).ToList();

                    }
                }
                return surveysFromV1;
            }
        }

        public static List<v2.osm_survey> SurveysFromV2
        {
            get
            {
                if (surveysFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        surveysFromV2 = v2Con.osm_survey.ToList();
                    }
                }
                return surveysFromV2;
            }
        }

        /// <summary>
        /// Gets the cleaned survey intros for v2.
        /// </summary>
        public static List<v2.osm_survey> CleanedSurveysForV2
        {
            get
            {
                if (cleanedSurveysV2.Count == 0)
                {
                    GetCleanedSurveys(SurveysFromV1);
                    return cleanedSurveysV2;
                }
                return cleanedSurveysV2;
            }
        }

        /// <summary>
        /// Gets the cleaned surveys.
        /// </summary>
        /// <param name="source">The source.</param>
        public static void GetCleanedSurveys(List<kn.osm_survey> source)
        {
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_survey, v2.osm_survey>(SurveysFromV1, cleanedSurveysV2);

            //modify required feilds for ready to insert .. 1. folder id 2. userid.
            SetUserAndFolderIds(cleanedSurveysV2);
        }

        static List<SurveyInfo> surveysInfo = new List<SurveyInfo>();
        public static List<SurveyInfo> SurveyInfoList
        {
            get
            {
                return surveysInfo;
            }
        }

        /// <summary>
        /// Sets the user and folder ids.
        /// </summary>
        /// <param name="destinationList">The destination list.</param>
        private static void SetUserAndFolderIds(List<v2.osm_survey> destinationSurveys)
        {
            var newUsers = from uv1 in Users.UsersFromV1
                           join uv2 in Users.UsersFromV2
                           on uv1.LOGIN_NAME equals uv2.LOGIN_NAME
                           select new UserInfo { LoginName = uv1.LOGIN_NAME, V1UserId = uv1.USERID, V2UserId = uv2.USERID };


            int ctr = 0;
            foreach (var item in destinationSurveys)
            {
                Console.WriteLine("{0} ---- GetCleanedSurveys(). ", ++ctr);
                var v1UserId = item.USERID;
                var currentUser = newUsers.Where(us => us.V1UserId == item.USERID).ToList();

                int userid = currentUser.Count > 0 ? currentUser.FirstOrDefault().V2UserId : 0;
                var newFolders = from fv1 in Folders.FoldersFromV1.ToList()
                                 join fv2 in Folders.FoldersFromV2.ToList()
                                 on fv1.FOLDER_NAME equals fv2.FOLDER_NAME
                                 where fv2.USERID == userid
                                 select new FoldersInfo { V1FolderId = fv1.FOLDER_ID, V2FolderId = fv2.FOLDER_ID };



                var currentFolder = newFolders.Where(f => f.V1FolderId == item.FOLDER_ID).ToList();
                item.USERID = currentUser.Count > 0 ? currentUser.FirstOrDefault().V2UserId : item.USERID;
                item.FOLDER_ID = currentFolder.Count > 0 ? currentFolder.FirstOrDefault().V2FolderId : item.FOLDER_ID;

                surveysInfo.Add(new SurveyInfo { V1SuveyId = item.SURVEY_ID, V1UserId = v1UserId, LoginName = currentUser.ToList().Count > 0 ? currentUser.FirstOrDefault().LoginName : string.Empty, V2UserId = item.USERID, V1FolderId = currentFolder.Count > 0 ? currentFolder.FirstOrDefault().V1FolderId : item.FOLDER_ID, V2FolderId = currentFolder.Count > 0 ? currentFolder.FirstOrDefault().V2FolderId : item.FOLDER_ID });
                //item.SURVEY_ID = 0;
            }
            //timer.Stop();
        }

        public static v2.osm_survey InsertSurvey(v2.osm_survey v2Survey)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                v2Con.osm_survey.Add(v2Survey);
                v2Con.SaveChanges();
                return v2Survey;
            }
        }

        public static SurveyInfo CurrentSurveyInfo
        {
            get
            {
                if (NewSurveyId > 0)
                    return SurveyInfoList.FirstOrDefault(info => info.V2SurveyId == NewSurveyId);

                return default(SurveyInfo);
            }
        }

        public static int NewSurveyId { get; set; }

        private const string proUsers = @"dasarath.ram@gmail.com,sivakumar.mv27@gmail.com,nasscom@insighto.com,ram@knowience.com,venu1108@gmail.com,vamshi.jaligama@yahoo.co.in,
dhruv@knowience.com,kim.chen@gmail.com,vijayanand.bhaktha@imrbint.com,d_shenoy@yahoo.com,airtel@knowience.com,venky.jandhyala@gmail.com,
velichala.srikanth71@gmail.com,pairsandy@yahoo.com,pramod.chaganti@gmail.com";
        static string[] proUsersArray = proUsers.Split(new[] { "," }, StringSplitOptions.None);
        public static bool IsSurveyExisted(int v1SurveyId, string userName)
        {
            //if (!proUsersArray.Contains(userName))
            //    return true;

            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                var surveys = v2Con.MigrationAuditInfo.Where(su => su.V1SurveyId == v1SurveyId && su.UserName == userName).ToList();
                return (surveys != null && surveys.Count > 0);
            }
        }
    }


    public class SurveyInfo : UserInfo
    {
        public int V1SuveyId { get; set; }

        public int V2SurveyId { get; set; }

        List<QuestionInfo> questionInfo = new List<QuestionInfo>();
        public List<QuestionInfo> QuestionsInfo
        {
            get
            {
                return questionInfo;
            }

            set
            {
                questionInfo = value;
            }
        }


        List<ResponseInfo> responsesInfo = new List<ResponseInfo>();
        public List<ResponseInfo> ResponsesInfo
        {
            get
            {
                return responsesInfo;
            }

            set
            {
                responsesInfo = value;
            }
        }
    }
}
