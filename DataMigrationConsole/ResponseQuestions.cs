﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
namespace DataMigrationConsole
{
    public class ResponseQuestions
    {
        static List<kn.osm_responsequestions> responseQuestionsesFromV1 = new List<kn.osm_responsequestions>();
        static List<v2.osm_responsequestions> responseQuestionsesFromV2 = new List<v2.osm_responsequestions>();
        static List<v2.osm_responsequestions> cleanedResponseQuestionsesForV2 = new List<v2.osm_responsequestions>();

        /// <summary>
        /// Gets the questions from v1.
        /// </summary>
        public static List<kn.osm_responsequestions> ResponseQuestionsesFromV1
        {
            get
            {
                if (responseQuestionsesFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        var v1RespondentIds = Surveys.CurrentSurveyInfo.ResponsesInfo.Select(q => q.V1ResponseId).ToList();
                        responseQuestionsesFromV1 = (from v1s in knCon.osm_responsequestions
                                                     where v1s.QUESTION_ID != 0 && v1RespondentIds.Contains(v1s.RESPONDENT_ID)
                                                     select v1s).ToList();
                    }
                }
                return responseQuestionsesFromV1;
            }
            set
            {
                responseQuestionsesFromV1 = value;
            }
        }

        /// <summary>
        /// Gets the questions from v2.
        /// </summary>
        public static List<v2.osm_responsequestions> ResponseQuestionsesFromV2
        {
            get
            {
                if (responseQuestionsesFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        responseQuestionsesFromV2 = v2Con.osm_responsequestions.ToList();
                    }
                }
                return responseQuestionsesFromV2;
            }
            set
            {
                responseQuestionsesFromV2 = value;
            }
        }


        public static List<v2.osm_responsequestions> CleanedResponseQuestionsesForV2
        {
            get
            {
                if (cleanedResponseQuestionsesForV2.Count == 0)
                {
                    GetCleanedAnswerResponseQuestionsesList(ResponseQuestionsesFromV1);
                    return cleanedResponseQuestionsesForV2;
                }
                return cleanedResponseQuestionsesForV2;
            }
            set
            {
                cleanedResponseQuestionsesForV2 = value;
            }
        }

        /// <summary>
        /// Gets the cleaned questions list.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static void GetCleanedAnswerResponseQuestionsesList(List<kn.osm_responsequestions> source)
        {
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_responsequestions, v2.osm_responsequestions>(ResponseQuestionsesFromV1, cleanedResponseQuestionsesForV2);

            //modify required feilds for ready to insert .. 1. folder id 2. userid.
            SetUserAndQuestionsIds(cleanedResponseQuestionsesForV2);
        }

        /// <summary>
        /// Sets the user and questions ids.
        /// </summary>
        /// <param name="destinationQuestions">The destination questions.</param>
        private static void SetUserAndQuestionsIds(List<v2.osm_responsequestions> destinationResponseQuestions)
        {
            int ctr = 0;
            List<v2.osm_responsequestions> tempList = new List<v2.osm_responsequestions>();
            foreach (var item in destinationResponseQuestions)
            {
                Console.WriteLine("{0} ---- GetCleanedAnswerResponseQuestionsesList(). ", ++ctr);
                var currentResponse = Surveys.CurrentSurveyInfo.ResponsesInfo.FirstOrDefault(r => r.V1ResponseId == item.RESPONDENT_ID);
                item.RESPONDENT_ID = currentResponse.V2ResponseId; //currentSurvey.V2SurveyId;

                var currentQuestion = Surveys.CurrentSurveyInfo.QuestionsInfo.FirstOrDefault(q => q.V1QuestionId == item.QUESTION_ID);
                if (currentQuestion == null)
                    continue;

                item.QUESTION_ID = currentQuestion.V2QuestionId;

                if (item.ANSWER_ID <= 0)
                    continue;

                var currentAnswerOption = currentQuestion.AnswersInfo.FirstOrDefault(a => a.V1AnswerId == item.ANSWER_ID);
                item.ANSWER_ID = currentAnswerOption.V2AnswerId;

                if (currentQuestion != null)
                    tempList.Add(item);
            }
            destinationResponseQuestions = tempList;
        }

        /// <summary>
        /// Inserts the question.
        /// </summary>
        /// <param name="v2Question">The v2 question.</param>
        /// <returns></returns>
        public static v2.osm_responsequestions InsertResponseQuestion(v2.osm_responsequestions v2Option)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                v2Con.osm_responsequestions.Add(v2Option);
                v2Con.SaveChanges();
                return v2Option;
            }
        }

        internal static void ProcessResponseQuestionses(int v2RespondentId)
        {
            //Insert survey and get new Id and assign to surveyInfo object
            var responseQuestions = ResponseQuestions.CleanedResponseQuestionsesForV2.Where(intro => intro.RESPONDENT_ID == v2RespondentId).ToList();
            if (responseQuestions.Count > 0)
            {
                foreach (var rq in responseQuestions)
                {
                    var insertedresponseQuestion = ResponseQuestions.InsertResponseQuestion(rq);
                    Console.WriteLine("S.No: {0} ---- is response question Id . ", insertedresponseQuestion.QUESTION_ID);
                }
            }
            else
                Console.WriteLine(" ----- Response Questions not inserted -----");

            // log into answer options audit table
            InsertIntoLog(v2RespondentId, responseQuestions.Count);
        }

        private static void InsertIntoLog(int v2RespondentId, int answersCount)
        {
            using (v2.InsightoV2Entities dbCon = new v2.InsightoV2Entities())
            {
                var currentMigrationSurveyInfo = new v2.ResponseQuestionsAuditInfo { V2RespondentId = v2RespondentId, NoOfResponseQuestions = answersCount, CreatedOn = DateTime.UtcNow };
                dbCon.ResponseQuestionsAuditInfo.Add(currentMigrationSurveyInfo);
                dbCon.SaveChanges();
            }
        }
    }
}
