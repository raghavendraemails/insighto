﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
using System.Drawing;
using System.Configuration;
namespace DataMigrationConsole
{
    public class SurveySettings
    {
        static List<kn.osm_surveysetting> settingsFromV1 = new List<kn.osm_surveysetting>();
        static List<v2.osm_surveysetting> settingsFromV2 = new List<v2.osm_surveysetting>();
        static List<v2.osm_surveysetting> cleanedSettingsForV2 = new List<v2.osm_surveysetting>();

        /// <summary>
        /// Gets the settings from v1.
        /// </summary>
        public static List<kn.osm_surveysetting> SettingsFromV1
        {
            get
            {
                if (settingsFromV1.Count == 0)
                {
                    using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                    {
                        settingsFromV1 = (from v1s in knCon.osm_surveysetting.ToList()
                                          where v1s.SURVEY_ID == Surveys.CurrentSurveyInfo.V1SuveyId
                                          select v1s).OrderByDescending(sett => sett.SURVEYSETTING_ID).ToList();
                    }
                }
                return settingsFromV1;
            }
            set
            {
                settingsFromV1 = value;
            }
        }

        /// <summary>
        /// Gets the settings from v2.
        /// </summary>
        public static List<v2.osm_surveysetting> SettingsFromV2
        {
            get
            {
                if (settingsFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        settingsFromV2 = v2Con.osm_surveysetting.ToList();
                    }
                }
                return settingsFromV2;
            }
            set
            {
                settingsFromV2 = value;
            }
        }

        /// <summary>
        /// Gets the cleaned settings for v2.
        /// </summary>
        public static List<v2.osm_surveysetting> CleanedSettingsForV2
        {
            get
            {
                if (cleanedSettingsForV2.Count == 0)
                {
                    GetCleanedSettingsList(SettingsFromV1);
                    return cleanedSettingsForV2;
                }
                return cleanedSettingsForV2;
            }
            set
            {
                cleanedSettingsForV2 = value;
            }
        }

        /// <summary>
        /// Gets the cleaned settings list.
        /// </summary>
        /// <param name="source">The source.</param>
        public static void GetCleanedSettingsList(List<kn.osm_surveysetting> source)
        {
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_surveysetting, v2.osm_surveysetting>(source, cleanedSettingsForV2);

            //modify required feilds for ready to insert .. 1. folder id 2. userid.
            SetUserAndSurveyIds(cleanedSettingsForV2);
        }

        /// <summary>
        /// Sets the user and contact ids.
        /// </summary>
        /// <param name="destinationSettings">The destination settings.</param>
        private static void SetUserAndSurveyIds(List<v2.osm_surveysetting> destinationSettings)
        {
            int ctr = 0;
            foreach (var item in destinationSettings)
            {
                Console.WriteLine("{0} ---- GetCleanedSettingsList(). ", ++ctr);
                var currentSurvey = Surveys.SurveyInfoList.FirstOrDefault(si => si.V1SuveyId == item.SURVEY_ID);
                //GetTheImageAndInsertIntoUsersFolder(item.SURVEY_LOGO, currentSurvey.V2UserId, currentSurvey);
                item.SURVEY_ID = currentSurvey.V2SurveyId;
            }
            //timer.Stop();
        }

        private static void GetTheImageAndInsertIntoUsersFolder(string surveyLogo, int userId, SurveyInfo currentSurvey)
        {
            if (string.IsNullOrEmpty(surveyLogo))
                return;

            surveyLogo = surveyLogo.Replace("\"", "");
            surveyLogo.Replace("\\", "");
            string relativePath = CommonMethods.GetImageUrl(surveyLogo);
            string absoluteUrl = string.Format("{0}/{1}", "http://insighto.com/osm", relativePath);
            Bitmap bitMap = CommonMethods.Download(absoluteUrl);

            //save to corresponding user location
            relativePath = currentSurvey != null ? relativePath.Replace(currentSurvey.V1FolderId.ToString(), currentSurvey.V2FolderId.ToString()) : relativePath;
            string fileName = string.Format("{0}{1}", ConfigurationManager.AppSettings["ImagesPath"], relativePath);

            fileName = fileName.Replace("/", @"\");
            fileName = fileName.Replace("//", @"\");
            CommonMethods.SaveImage(bitMap, fileName, CommonMethods.GetImageFormat(CommonMethods.URLExtension(relativePath)));
            GC.Collect();
        }

        /// <summary>
        /// Inserts the survey setting.
        /// </summary>
        /// <param name="v2Setting">The v2 setting.</param>
        /// <returns></returns>
        public static v2.osm_surveysetting InsertSurveySetting(v2.osm_surveysetting v2Setting)
        {
            using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
            {
                v2Con.osm_surveysetting.Add(v2Setting);
                v2Con.SaveChanges();
                return v2Setting;
            }
        }

        /// <summary>
        /// Processes the survey setting.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        public static void ProcessSurveySetting(int surveyId)
        {
            //Insert survey and get new Id and assign to surveyInfo object
            var surveySetting = SurveySettings.CleanedSettingsForV2.FirstOrDefault(intro => intro.SURVEY_ID == surveyId);
            if (surveySetting != null)
            {
                var insertedSurveyIntro = SurveySettings.InsertSurveySetting(surveySetting);
                Console.WriteLine("S.No: {0} ---- Survey Setting. ", 1);
            }
            else
                Console.WriteLine(" ----- Not inserted -----");
        }
    }
}
