﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kn = Knoweince.Data;
using v2 = Insighto.Data;
namespace DataMigrationConsole
{
    public class EmailLists
    {
        static List<kn.osm_emaillist> emailsFromV1 = new List<kn.osm_emaillist>();
        static List<v2.osm_emaillist> emailsFromV2 = new List<v2.osm_emaillist>();

        public static List<kn.osm_emaillist> EmailsFromV1
        {
            get
            {
                using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
                {
                    var usersFromV1 = Users.UsersFromV1.Select(user => user.USERID).ToList();

                    emailsFromV1 = (from contact in knCon.osm_emaillist
                                    where contact.DELETED == 0 && usersFromV1.Contains(contact.CREATED_BY ?? 0)
                                    select contact).ToList();
                }
                return emailsFromV1;
            }
        }

        public static List<v2.osm_emaillist> EmailsFromV2
        {
            get
            {
                if (emailsFromV2.Count == 0)
                {
                    using (v2.InsightoV2Entities v2Con = new v2.InsightoV2Entities())
                    {
                        emailsFromV2 = v2Con.osm_emaillist.ToList();
                    }
                }
                return emailsFromV2;
            }
        }

        public static List<v2.osm_emaillist> CleanedEmails(int contactListOldId)
        {
            using (kn.KnowinceEntities knCon = new kn.KnowinceEntities())
            {
                emailsFromV1 = (from contact in knCon.osm_emaillist
                                where contact.DELETED == 0 && contact.CONTACTLIST_ID == contactListOldId
                                select contact).ToList();
            }
            List<v2.osm_emaillist> destinationList = new List<v2.osm_emaillist>();
            CommonMethods.MergeObjectsFromV1ToV2<kn.osm_emaillist, v2.osm_emaillist>(emailsFromV1, destinationList);
            return destinationList;
        }

        internal static void ProcessEmailList()
        {
            //var newEmails = (from cv1 in EmailLists.EmailsFromV1.ToList()
            //                 join cv2 in EmailLists.EmailsFromV2.ToList()
            //                 on cv1.EMAIL_ADDRESS equals cv2.EMAIL_ADDRESS
            //                 where cv1.EMAIL_ID == item.EMAIL_ID
            //                 select new { V1EmailId = cv1.EMAIL_ID, V2EmailId = cv2.EMAIL_ID }).ToList();
        }
    }
}
