﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;

namespace Insighto.Web.UI.Controls
{
    public class InsightoCheckBox : CheckBox
    {
        /// <summary>
        /// Gets or sets a value indicating whether the CheckBox is [read only].
        /// </summary>
        /// <value><c>true</c> if [read only]; otherwise, <c>false</c>.</value>
        public bool ReadOnly
        {
            get
            {
                object readOnlyObject = this.ViewState["ReadOnly"];
                return ((readOnlyObject != null) && ((bool)readOnlyObject));
            }
            set
            {
                this.ViewState["ReadOnly"] = value;
            }
        }

        /// <summary>
        /// Registers client script for generating postback prior to rendering on the client if <see cref="P:System.Web.UI.WebControls.CheckBox.AutoPostBack"/> is true.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            Text = HttpUtility.HtmlEncode(Text);
            if (ReadOnly)
            {
                this.Style.Add("display", "none");
            }
            else
            {
                this.Style.Add("display", "inline");
            }
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            Text = HttpUtility.HtmlDecode(Text);
        }

        /// <summary>
        /// Displays the <see cref="T:System.Web.UI.WebControls.CheckBox"/> on the client.
        /// </summary>
        /// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that contains the output stream to render on the client.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
            if (ReadOnly)
            {
                writer.Write("<input id=\"{0}DisabledCheckBox\" type=\"checkbox\" disabled=\"disabled\" ", this.ClientID);
                if (this.Checked)
                {
                    writer.Write("checked=\"checked\" ");
                }
                writer.Write("/>");
                if (!String.IsNullOrEmpty(this.Text))
                {
                    writer.WriteLine("<span id=\"{0}ValueLabel\" title=\"{2}\">{1}</span>", this.ClientID, this.Text, this.Text);
                }
            }
        }
    }
}
